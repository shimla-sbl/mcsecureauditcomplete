﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Filters;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Notice;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.User;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.DomainModel.ComplexModel;
using System.Linq;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Models;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.LOB;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Models;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Filters;
using SBL.DomainModel.Models.Departmentpdfpath;
using SBL.DomainModel.Models.UserModule;
using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Areas.AccountsAdmin.Models;
//using System.Security.Cryptography;
//using System.Text;

using System.Net;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.AssemblyFileSystem;
using System.Text;
using EvoPdf;
using SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.Grievance;
using SBL.eLegistrator.HouseController.Web.Areas.Grievances.Models;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;
//using SBL.eLegistrator.HouseController.Web.Areas.Constituency.Models;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Controllers
{
	[SBLAuthorize(Allow = "Authenticated")]
	[Audit]
	[NoCache]
	public class PaperLaidMinisterController : Controller
	{
		// GET: /PaperLaidMinister/PaperLaidMinister/      
		public ActionResult Index()
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			tPaperLaidV paperLaidModel = new tPaperLaidV();

			mSession mSessionModel = new mSession();
			mAssembly mAssemblymodel = new mAssembly();
			paperLaidModel = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", paperLaidModel);
			paperLaidModel.mSession = (ICollection<mSession>)Helper.ExecuteService("Session", "GetAllSessionData", mSessionModel);
			paperLaidModel.mAssembly = (ICollection<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssembly", mAssemblymodel);

			return View(paperLaidModel);

		}

		public ActionResult PaperHide()
		{
			return View();
		}

		public ActionResult GetPaperEntry(string sId, string aId)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				tPaperLaidV paperLaidModel = new tPaperLaidV();
				mDepartment departmentModel = new mDepartment();
				mEvent mEventModel = new mEvent();
				paperLaidModel.DepartmentId = CurrentSession.DeptID;
				tPaperLaidV ministerDetails = new tPaperLaidV();
				ministerDetails.DepartmentId = paperLaidModel.DepartmentId;
				paperLaidModel.SessionId = Convert.ToInt32(sId);
				paperLaidModel.AssemblyCode = Convert.ToInt32(aId);

				mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
				paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaidMinister", "GetSessionDates", paperLaidModel);

				paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaidMinister", "GetDocumentType", paperLaidModel);
				paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
				paperLaidModel.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);

				paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
				//   paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("PaperLaidMinister", "GetMinisterMinistryName", paperLaidModel.DepartmentId);
				//   paperLaidModel.MinistryId = mMinisteryMinisterModel.MinistryId;
				return PartialView("_GetPaperEntry", paperLaidModel);
			}
			return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");
		}

		public ActionResult GetPaperEntryForDashBord(string questionNumber, string QuestionID, string QuestionTypeID)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				tPaperLaidV paperLaidModel = new tPaperLaidV();
				mDepartment departmentModel = new mDepartment();
				mEvent mEventModel = new mEvent();

				tPaperLaidV ministerDetails = new tPaperLaidV();
				paperLaidModel.DepartmentId = CurrentSession.DeptID;
				ministerDetails.DepartmentId = paperLaidModel.DepartmentId;

				mSession mSessionModel = new mSession();
				mAssembly mAssemblymodel = new mAssembly();
				//paperLaidModel = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", paperLaidModel);
				//paperLaidModel.SessionId = Convert.ToInt32(paperLaidModel.SessionCode);
				//paperLaidModel.AssemblyId = Convert.ToInt32(paperLaidModel.AssemblyCode);
				SiteSettings siteSettingMod = new SiteSettings();
				siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

				if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
				{
					paperLaidModel.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
				}

				if (!string.IsNullOrEmpty(CurrentSession.SessionId))
				{
					paperLaidModel.SessionId = Convert.ToInt16(CurrentSession.SessionId);
				}

				mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
				paperLaidModel.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaidMinister", "GetSessionDates", paperLaidModel);

				paperLaidModel.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaidMinister", "GetDocumentType", paperLaidModel);
				paperLaidModel.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
				paperLaidModel.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);

				paperLaidModel.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);

				tQuestion mdl = new tQuestion();
				tQuestion questionModel = new tQuestion();
				//int Id = Convert.ToInt32(questionNumber);
				questionModel.QuestionType = Convert.ToInt32(QuestionTypeID);
				questionModel.QuestionNumber = Convert.ToInt32(questionNumber);
				questionModel.QuestionID = Convert.ToInt32(QuestionID);

				mdl = (tQuestion)Helper.ExecuteService("PaperLaidMinister", "GetQuestionDetails", questionModel);
				mEvent mEvents = new mEvent();
				mEvents = (mEvent)Helper.ExecuteService("PaperLaidMinister", "GetQuetionEventID", mEvents);

				paperLaidModel.Title = mdl.Subject;
				paperLaidModel.Description = mdl.MainQuestion;
				paperLaidModel.EventId = mEvents.EventId;
				paperLaidModel.QuestionID = mdl.QuestionID;
				paperLaidModel.DepartmentId = CurrentSession.DeptID;
				paperLaidModel.QuestionNumber = Convert.ToInt32(questionNumber);

				//New
				paperLaidModel.DesireLayingDate = mdl.QuestionLayingDate;
				paperLaidModel.QuestionTypeId = mdl.QuestionType;

				return PartialView("_GetPaperEntryForDashBoard", paperLaidModel);
			}
			return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");
		}

		public ActionResult CategoryView(string categoryId)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				int id = Convert.ToInt32(categoryId);
				mEvent mEventModel = new mEvent();
				mEventModel.EventId = id;
				int[] Ids = new int[2];
				int papertypeId = (int)Helper.ExecuteService("Events", "GetPaperCategoryID", mEventModel);
				tQuestionTypeModel questionModel = new tQuestionTypeModel();
				tPaperLaidV paperModel = new tPaperLaidV();
				Ids[0] = papertypeId;
				Ids[1] = Convert.ToInt32(categoryId);
				paperModel = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPaperLaidCategoryDetails", Ids);
				if (paperModel.categoryType.Equals("bill"))
				{
					ICollection<int> yearList = new List<int>();
					int currentYear = DateTime.Now.Year;
					for (int i = currentYear - 5; i < currentYear; i++)
					{
						yearList.Add(i);
					}
					for (int i = currentYear; i < currentYear + 5; i++)
					{
						yearList.Add(i);
					}
					//paperModel.yearList = yearList;
				}
				return PartialView("_CategoryView", paperModel);
			}
			return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");
		}

		public JsonResult callQuestionNumber(int Id)
		{
			tPaperLaidV mdel = new tPaperLaidV();
			mdel.QuestionTypeId = Id;
			mdel.DepartmentId = CurrentSession.DeptID;
			if (CurrentSession.UserID != null)
			{
				tPaperLaidV mdl = new tPaperLaidV();
				mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetQuestionNumber", mdel);
				return Json(mdl.tQuestion, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		public JsonResult CheckBillNumber(string number, string year)
		{
			if (CurrentSession.UserID != null)
			{
				tBillRegister mdl = new tBillRegister();
				string billNumber = number + " of " + year;
				mdl = (tBillRegister)Helper.ExecuteService("PaperLaidMinister", "CheckBillNUmber", billNumber);
				return Json(mdl, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		public JsonResult GetQuestionDetails(int Id)
		{
			if (CurrentSession.UserID != null)
			{
				tQuestion mdl = new tQuestion();
				mdl = (tQuestion)Helper.ExecuteService("PaperLaidMinister", "GetQuestionDetails", Id);
				return Json(mdl, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		string noticeNumber = string.Empty;
		public JsonResult GetNoticeDetails(string Id)
		{
			if (CurrentSession.UserID != null)
			{
				noticeNumber = Id;
				tMemberNotice noticeModel = new tMemberNotice();
				noticeModel = (tMemberNotice)Helper.ExecuteService("PaperLaidMinister", "GetNoticeDetails", Id);
				return Json(noticeModel, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		public JsonResult callNoticeNumber(int Id)
		{
			if (CurrentSession.UserID != null)
			{
				tPaperLaidV mdl = new tPaperLaidV();
				mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNotices", Id);
				return Json(mdl.tMemberNotice, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult PaperLaidSubmitt(HttpPostedFileBase file, tPaperLaidV obj, string submitButton)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				submitButton = submitButton.Replace(" ", "");
				tPaperLaidV mdl = new tPaperLaidV();
				tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
				switch (submitButton)
				{
					case "Save":
						{
							obj.AssemblyId = obj.AssemblyId;
							obj.SessionId = obj.SessionId;
							int questionId = obj.QuestionID;
							int noticeId = obj.NoticeID;
							tPaperLaidV billDetails = new tPaperLaidV();

							obj = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
							billDetails = obj;
							mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "SubmittPaperLaidEntry", obj);
							mdl.EventId = obj.EventId;
							int paperLaidId = (int)mdl.PaperLaidId;

							mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetFileSaveDetails", mdl);
							string categoryType = mdl.RespectivePaper;

							tQuestion tQuestions = new tQuestion();
							tQuestions.PaperLaidId = paperLaidId;
							tQuestions.QuestionID = questionId;
							tQuestions = (tQuestion)Helper.ExecuteService("PaperLaidMinister", "UpdateQuestionPaperLaidId", tQuestions);

							if (file != null)
							{
								mdlTemp = SaveFileToDirectory(tQuestions, file, mdlTemp, mdl, obj);
								mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaidMinister", "SaveDetailsInTempPaperLaid", mdlTemp);
							}
							break;
						}
					case "Save&Send":
						{
							obj.AssemblyId = obj.AssemblyCode;
							obj.SessionId = obj.SessionId;
							int questionId = obj.QuestionID;
							int noticeId = obj.NoticeID;
							tPaperLaidV billDetails = new tPaperLaidV();

							obj = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
							billDetails = obj;
							mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "SubmittPaperLaidEntry", obj);
							mdl.EventId = obj.EventId;
							int paperLaidId = (int)mdl.PaperLaidId;
							mdl = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetFileSaveDetails", mdl);
							string categoryType = mdl.RespectivePaper;

							tQuestion tQuestions = new tQuestion();
							tQuestions.PaperLaidId = paperLaidId;
							tQuestions.QuestionID = questionId;
							tQuestions = (tQuestion)Helper.ExecuteService("PaperLaidMinister", "UpdateQuestionPaperLaidId", tQuestions);

							if (file != null)
							{
								mdlTemp = SaveFileToDirectory(tQuestions, file, mdlTemp, mdl, obj);

								//File is uploaded save the details.
								mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaidMinister", "SaveDetailsInTempPaperLaid", mdlTemp);
							}

							tPaperLaidTemp model = new tPaperLaidTemp();
							tPaperLaidV mod = new tPaperLaidV();
							model.DeptSubmittedBy = CurrentSession.UserName;
							model.DeptSubmittedDate = DateTime.Now;

							model.PaperLaidId = Convert.ToInt32(paperLaidId);
							model.DeptSubmittedBy = CurrentSession.UserName;
							model.DeptSubmittedDate = DateTime.Now;
							model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaidMinister", "SubmitPendingDepartmentPaperLaid", model);

							mod = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpdateDeptActiveId", model);
							break;
						}
					case "Send":
						{
							tPaperLaidTemp model = new tPaperLaidTemp();
							tPaperLaidV mod = new tPaperLaidV();
							model.PaperLaidId = Convert.ToInt32(obj.PaperLaidId);
							model.DeptSubmittedBy = CurrentSession.UserName;
							model.DeptSubmittedDate = DateTime.Now;
							model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaidMinister", "SubmitPendingDepartmentPaperLaid", model);
							mod = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpdateDeptActiveId", model);

							break;
						}
				}
				if (Request.IsAjaxRequest())
				{
					var data = submitButton;
					var result = Json(data, JsonRequestBehavior.AllowGet);
					return result;
				}
				else
				{
					return RedirectToAction("DepartmentDashboard");
				}
			}

			return RedirectToAction("DepartmentDashboard");
			//return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/DepartmentDashboard");
		}

		public tPaperLaidTemp SaveFileToDirectory(tQuestion tQuestions, HttpPostedFileBase file, tPaperLaidTemp mdlTemp, tPaperLaidV mdl, tPaperLaidV obj)
		{
			string QType = string.Empty;

			if (tQuestions.QuestionType == (int)QuestionType.StartedQuestion)
			{
				QType = "S";
			}
			else if (tQuestions.QuestionType == (int)QuestionType.UnstaredQuestion)
			{
				QType = "US";
			}
			string Url = "/PaperLaid/" + obj.AssemblyId + "/" + obj.SessionId + "/";
			DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
			if (!Dir.Exists)
			{
				Dir.Create();
			}
			string ext = Path.GetExtension(file.FileName);
			string fileName = file.FileName.Replace(ext, "");
			string actualFileName = obj.AssemblyId + "_" + obj.SessionId + "_" + QType + "_" + tQuestions.QuestionNumber + "_V" + mdl.Count + ext;
			//+ mdl.Count 
			file.SaveAs(Server.MapPath(Url + actualFileName));
			mdlTemp.FileName = actualFileName;
			mdlTemp.FilePath = Url;
			mdlTemp.Version = mdl.Count;
			mdlTemp.PaperLaidId = mdl.PaperLaidId;

			return mdlTemp;
		}

		public ActionResult FilterPendingForSubmissionList(int AssemblyId, int SessionId, int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{

				PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
				ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
				tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

				#region Paper Category Type Information

				mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
				List<mPaperCategoryType> PaperCategoryTypeList = Helper.ExecuteService("PaperLaidMinister", "GetAllPaperCategoryType", PaperCategoryTypeListMod) as List<mPaperCategoryType>;
				tPaperLaidDepartmentModel.mPaperCategoryTypeList = PaperCategoryTypeList;

				#endregion

				tPaperLaidV objMovement = new tPaperLaidV();
				objMovement.AssemblyId = AssemblyId;
				objMovement.SessionId = SessionId;
				objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
				objMovement.EventId = CategoryTypeId;
				PaperMovementModel objPaperModel = new PaperMovementModel();
				objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
				objPaperModel.PageIndex = int.Parse(PageNumber);
				objPaperModel.ResultCount = int.Parse(ResultCount);
				objPaperModel.PaperCategoryTypeId = PaperCategoryTypeId;
				tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaidMinister", "GetPendingDepartmentPaperLaidList", objPaperModel);
				if (tPaperLaidDepartmentModel.DepartmentPendingList.Count > 0)
				{
					objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentPendingList.Count;
				}
				tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
				tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


				tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
				if (PageNumber != null && PageNumber != "")
				{
					tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
				}
				else
				{
					tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
				}

				if (RowsPerPage != null && RowsPerPage != "")
				{
					tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
				}
				else
				{
					tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("10");
				}
				if (PageNumber != null && PageNumber != "")
				{
					tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
				}
				else
				{
					tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
				}

				if (loopStart != null && loopStart != "")
				{
					tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
				}
				else
				{
					tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
				}

				if (loopEnd != null && loopEnd != "")
				{
					tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
				}
				else
				{
					tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
				}
				return PartialView("_GetPendingForSubmission", tPaperLaidDepartmentModel);
			}
			return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");
		}

		public ActionResult GetPendingForSubmission(int AssemblyId, int SessionId, int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
				ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
				tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

				#region Paper Category Type Information

				mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
				List<mPaperCategoryType> PaperCategoryTypeList = Helper.ExecuteService("PaperLaidMinister", "GetAllPaperCategoryType", PaperCategoryTypeListMod) as List<mPaperCategoryType>;
				tPaperLaidDepartmentModel.mPaperCategoryTypeList = PaperCategoryTypeList;

				#endregion

				tPaperLaidV objMovement = new tPaperLaidV();
				objMovement.AssemblyId = AssemblyId;
				objMovement.SessionId = SessionId;
				objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
				objMovement.EventId = CategoryTypeId;
				PaperMovementModel objPaperModel = new PaperMovementModel();
				objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
				objPaperModel.PageIndex = int.Parse(PageNumber);
				objPaperModel.ResultCount = int.Parse(ResultCount);
				tPaperLaidDepartmentModel.DepartmentPendingList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaidMinister", "GetPendingDepartmentPaperLaidList", objPaperModel);
				if (tPaperLaidDepartmentModel.DepartmentPendingList.Count > 0)
				{
					objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentPendingList.Count;
				}
				tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
				tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


				tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
				if (PageNumber != null && PageNumber != "")
				{
					tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
				}
				else
				{
					tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
				}

				if (RowsPerPage != null && RowsPerPage != "")
				{
					tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
				}
				else
				{
					tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("10");
				}
				if (PageNumber != null && PageNumber != "")
				{
					tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
				}
				else
				{
					tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
				}

				if (loopStart != null && loopStart != "")
				{
					tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
				}
				else
				{
					tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
				}

				if (loopEnd != null && loopEnd != "")
				{
					tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
				}
				else
				{
					tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
				}
				return PartialView("_GetPendingForSubmission", tPaperLaidDepartmentModel);
			}
			return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");

		}

		[HttpPost]
		public JsonResult GetCategoryByPaperCategoryTypeId(int PaperCategoryTypeId)
		{
			if (CurrentSession.UserID != null)
			{
				tPaperLaidV model = new tPaperLaidV();
				mEvent objEventModel = new mEvent();

				objEventModel.PaperCategoryTypeId = PaperCategoryTypeId;
				model.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetEventsByPaperCategoryID", objEventModel);

				return Json(model.mEvents, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		public ActionResult FilterSubmittedForSubmissionList(int AssemblyId, int SessionId, int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
				ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
				tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

				#region Paper Category Type Information

				mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
				List<mPaperCategoryType> PaperCategoryTypeList = Helper.ExecuteService("PaperLaidMinister", "GetAllPaperCategoryType", PaperCategoryTypeListMod) as List<mPaperCategoryType>;
				tPaperLaidDepartmentModel.mPaperCategoryTypeList = PaperCategoryTypeList;

				#endregion

				tPaperLaidV objMovement = new tPaperLaidV();
				objMovement.AssemblyId = AssemblyId;
				objMovement.SessionId = SessionId;
				objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
				objMovement.EventId = CategoryTypeId;
				PaperMovementModel objPaperModel = new PaperMovementModel();
				objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
				objPaperModel.PageIndex = int.Parse(PageNumber);
				objPaperModel.ResultCount = int.Parse(ResultCount);
				objPaperModel.PaperCategoryTypeId = PaperCategoryTypeId;
				tPaperLaidDepartmentModel.DepartmentSubmitList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaidMinister", "GetSubmittedPaperLaidList", objPaperModel);
				if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
				{
					objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
				}
				tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
				tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


				tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
				if (PageNumber != null && PageNumber != "")
				{
					tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
				}
				else
				{
					tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
				}

				if (RowsPerPage != null && RowsPerPage != "")
				{
					tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
				}
				else
				{
					tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("10");
				}
				if (PageNumber != null && PageNumber != "")
				{
					tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
				}
				else
				{
					tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
				}

				if (loopStart != null && loopStart != "")
				{
					tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
				}
				else
				{
					tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
				}

				if (loopEnd != null && loopEnd != "")
				{
					tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
				}
				else
				{
					tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
				}
				return PartialView("_GetPaperLaidSubmittList", tPaperLaidDepartmentModel);
			}
			return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");
		}

		public ActionResult GetPaperLaidSubmittList(int AssemblyId, int SessionId, int PaperCategoryTypeId, int CategoryTypeId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
				RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
				loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
				loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
				ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
				tPaperLaidV tPaperLaidDepartmentModel = new tPaperLaidV();

				#region Paper Category Type Information

				mPaperCategoryType PaperCategoryTypeListMod = new mPaperCategoryType();
				List<mPaperCategoryType> PaperCategoryTypeList = Helper.ExecuteService("PaperLaidMinister", "GetAllPaperCategoryType", PaperCategoryTypeListMod) as List<mPaperCategoryType>;
				tPaperLaidDepartmentModel.mPaperCategoryTypeList = PaperCategoryTypeList;

				#endregion

				tPaperLaidV objMovement = new tPaperLaidV();
				objMovement.AssemblyId = AssemblyId;
				objMovement.SessionId = SessionId;
				objMovement.PaperCategoryTypeId = PaperCategoryTypeId;
				objMovement.EventId = CategoryTypeId;
				PaperMovementModel objPaperModel = new PaperMovementModel();
				objPaperModel.PAGE_SIZE = int.Parse(RowsPerPage);
				objPaperModel.PageIndex = int.Parse(PageNumber);
				objPaperModel.ResultCount = int.Parse(ResultCount);
				tPaperLaidDepartmentModel.DepartmentSubmitList = (ICollection<PaperMovementModel>)Helper.ExecuteService("PaperLaidMinister", "GetSubmittedPaperLaidList", objPaperModel);
				if (tPaperLaidDepartmentModel.DepartmentSubmitList.Count > 0)
				{
					objPaperModel.ResultCount = tPaperLaidDepartmentModel.DepartmentSubmitList.Count;
				}
				tPaperLaidDepartmentModel.PAGE_SIZE = int.Parse(RowsPerPage);
				tPaperLaidDepartmentModel.PageIndex = int.Parse(PageNumber);


				tPaperLaidDepartmentModel.ResultCount = objPaperModel.ResultCount;
				if (PageNumber != null && PageNumber != "")
				{
					tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32(PageNumber);
				}
				else
				{
					tPaperLaidDepartmentModel.PageNumber = Convert.ToInt32("1");
				}

				if (RowsPerPage != null && RowsPerPage != "")
				{
					tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32(RowsPerPage);
				}
				else
				{
					tPaperLaidDepartmentModel.RowsPerPage = Convert.ToInt32("10");
				}
				if (PageNumber != null && PageNumber != "")
				{
					tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32(PageNumber);
				}
				else
				{
					tPaperLaidDepartmentModel.selectedPage = Convert.ToInt32("1");
				}

				if (loopStart != null && loopStart != "")
				{
					tPaperLaidDepartmentModel.loopStart = Convert.ToInt32(loopStart);
				}
				else
				{
					tPaperLaidDepartmentModel.loopStart = Convert.ToInt32("1");
				}

				if (loopEnd != null && loopEnd != "")
				{
					tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32(loopEnd);
				}
				else
				{
					tPaperLaidDepartmentModel.loopEnd = Convert.ToInt32("5");
				}
				return PartialView("_GetPaperLaidSubmittList", tPaperLaidDepartmentModel);
			}
			return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");

		}

		public ActionResult SubmitSelectedPendingList(string DetailIds)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != "")
			{
				tPaperLaidTemp model = new tPaperLaidTemp();
				string[] ids = DetailIds.Split(',');
				foreach (var id in ids)
				{
					string[] pdId = id.Split('-');
					model.PaperLaidId = Convert.ToInt32(pdId[0]);
					model.PaperLaidTempId = Convert.ToInt32(pdId[1]);
					model.DeptSubmittedBy = CurrentSession.UserName;
					model.DeptSubmittedDate = DateTime.Now;
					model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaidMinister", "SubmitPendingDepartmentPaperLaid", model);
				}

				return Json(model, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		public ActionResult SubmittPendingRecord(string paperLaidId, string depId)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != "")
			{
				tPaperLaidTemp model = new tPaperLaidTemp();
				tPaperLaidV mod = new tPaperLaidV();
				model.PaperLaidId = Convert.ToInt32(paperLaidId);
				model.DeptSubmittedBy = CurrentSession.UserName;
				model.DeptSubmittedDate = DateTime.Now;
				model = (tPaperLaidTemp)Helper.ExecuteService("PaperLaidMinister", "SubmitPendingDepartmentPaperLaid", model);
				mod = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpdateDeptActiveId", model);

				return Json(model, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		public ActionResult DeletePendingRecord(string paperLaidId)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != "")
			{
				tPaperLaidV model = new tPaperLaidV();
				model.PaperLaidId = Convert.ToInt32(paperLaidId);
				model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "DeletePaperLaidByID", model);

				return Json(model, JsonRequestBehavior.AllowGet);
			}
			return null;
		}

		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult UpdatePaperLaidFile(HttpPostedFileBase file, tPaperLaidV tPaper)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != "")
			{
				tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
				PaperMovementModel obj = new PaperMovementModel();
				obj.PaperLaidId = tPaper.PaperLaidId;
				obj.FilePath = tPaper.FilePath;
				tPaperLaidV model = new tPaperLaidV();
				model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetFileVersion", obj);
				if (model != null)
				{
					//Get Question details by Paper Laid ID.
					tQuestion tQuestions = new tQuestion();
					tQuestions.PaperLaidId = Convert.ToInt32(tPaper.PaperLaidId);
					tQuestions.DepartmentID = CurrentSession.DeptID;
					tQuestions = (tQuestion)Helper.ExecuteService("PaperLaidMinister", "GetQuestionByPaperLaidID", tQuestions) as tQuestion;
					model.PaperLaidId = tPaper.PaperLaidId;

					var paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPaperLaidByPaperLaidId", model) as tPaperLaidV;


					if (file != null)
					{
						tPaperTemp = SaveFileToDirectory(tQuestions, file, tPaperTemp, model, paperLaid);
						tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaidMinister", "UpdatePaperLaidFileByID", tPaperTemp);
					}

					tPaperLaidTemp modelPLTemp = new tPaperLaidTemp();
					tPaperLaidV mod = new tPaperLaidV();
					modelPLTemp.DeptSubmittedBy = CurrentSession.UserName;
					modelPLTemp.DeptSubmittedDate = DateTime.Now;

					modelPLTemp.PaperLaidId = Convert.ToInt32(tPaper.PaperLaidId);
					modelPLTemp.DeptSubmittedBy = CurrentSession.UserName;
					modelPLTemp.DeptSubmittedDate = DateTime.Now;
					modelPLTemp = (tPaperLaidTemp)Helper.ExecuteService("PaperLaidMinister", "SubmitPendingDepartmentPaperLaid", modelPLTemp);

					mod = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpdateDeptActiveId", modelPLTemp);

					TempData["valID"] = 1;
				}
			}
			return RedirectToAction("DepartmentDashboard");
		}

		public ActionResult EditPendingRecord(string paperLaidId, string SessionId)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != "")
			{
				tPaperLaidV model = new tPaperLaidV();

				model.PaperLaidId = Convert.ToInt32(paperLaidId);
				model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "EditPendingPaperLaidDetials", model);

				mDepartment departmentModel = new mDepartment();
				mEvent mEventModel = new mEvent();

				model.SessionId = Convert.ToInt32(SessionId);
				mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
				model.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("PaperLaidMinister", "GetSessionDates", model);
				model.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("PaperLaidMinister", "GetDocumentType", model);
				model.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);
				model.mEvents = (ICollection<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
				//model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistry", mMinisteryMinisterModel);

				tPaperLaidV paperLaidModel = new tPaperLaidV();
				paperLaidModel.DepartmentId = CurrentSession.DeptID;
				model.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", paperLaidModel);

				SiteSettings siteSettingMod = new SiteSettings();
				siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
				model.AssemblyId = Convert.ToInt32(siteSettingMod.AssemblyCode);
				model.mEvents.Add(mEventModel);

				//Get Question details by Paper Laid ID.
				tQuestion questionDet = new tQuestion();
				questionDet.PaperLaidId = Convert.ToInt32(paperLaidId);
				questionDet.DepartmentID = CurrentSession.DeptID;
				questionDet = (tQuestion)Helper.ExecuteService("PaperLaidMinister", "GetQuestionByPaperLaidID", questionDet) as tQuestion;
				if (questionDet != null)
				{
					model.QuestionNumber = questionDet.QuestionNumber;
					model.QuestionID = questionDet.QuestionID;
					model.QuestionTypeId = questionDet.QuestionType;
				}

				return PartialView("_GetPaperEntryForDashBoard", model);
			}
			return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");
		}

		public ActionResult ShowPaperLaidDetailByID(string paperLaidId)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				PaperMovementModel movementModel = new PaperMovementModel();
				movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
				movementModel = (PaperMovementModel)Helper.ExecuteService("PaperLaidMinister", "ShowPaperLaidDetailByID", movementModel);
				return PartialView("_GetFullRecord", movementModel);
			}
			return null;
		}

		public ActionResult GetQuestionDetailsByQuestionID(string questionID)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				tQuestionModel tQuestionModel = new tQuestionModel();
				tQuestionModel.QuestionID = Convert.ToInt32(questionID);
				tQuestionModel = (tQuestionModel)Helper.ExecuteService("Questions", "GetQuestionDetailsByID", tQuestionModel);
				return PartialView("_GetFullRecordForDashboard", tQuestionModel);
			}
			return null;
		}

		public ActionResult ShowNoticeInboxById(int noticeId)
		{
			MinisterNoticecustom Noticecustom = new MinisterNoticecustom();
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				Noticecustom.NoticeID = Convert.ToInt32(noticeId);
				Noticecustom = (MinisterNoticecustom)Helper.ExecuteService("PaperLaidMinister", "ShowNoticeInboxById", Noticecustom);
				return PartialView("_ShowNoticeInboxById", Noticecustom);
			}
			return null;

		}

		public ActionResult GetSubmittedPaperLaidByID(string paperLaidId)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				tPaperLaidV PaperLaidModel = new tPaperLaidV();
				PaperLaidModel.PaperLaidId = Convert.ToInt64(paperLaidId);
				PaperLaidModel = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetSubmittedPaperLaidByID", PaperLaidModel);

				//New.
				PaperMovementModel movementModel = new PaperMovementModel();
				movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
				movementModel = (PaperMovementModel)Helper.ExecuteService("PaperLaidMinister", "ShowPaperLaidDetailByID", movementModel);
				PaperLaidModel.paperMovementModel = movementModel;
				PaperLaidModel.QuestionTypeId = movementModel.QuestionTypeId;

				return PartialView("_GetSubmittedListById", PaperLaidModel);
			}
			return null;
		}

		//public ActionResult MinisterDashboard() 
		//{
		//    if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

		//    tPaperLaidV model = new tPaperLaidV();

		//    mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
		//    if (CurrentSession.UserName != null && CurrentSession.UserName != "")
		//    {
		//        Obj.LoginId = CurrentSession.UserName;
		//    }
		//    //    Obj.LoginId=
		//    model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);

		//    //Get the Total count of All Type of question.
		//    SiteSettings siteSettingMod = new SiteSettings();
		//    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
		//    model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
		//    model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

		//    model.QuestionTypeId = (int)QuestionType.StartedQuestion;
		//    model.MinistryId = Obj.MinistryId;
		//    model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounter", model);

		//    model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
		//    model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnstarredQuestionCounter", model);

		//    model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForOtherPaperLaidTypes", model);
		//    model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPaperLaidCounters", model);
		//    mDepartment deptInfo = new mDepartment();
		//    deptInfo.deptId = CurrentSession.DeptID;
		//    deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
		//    model.DeparmentName = deptInfo.deptname;
		//    model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForBillsandNotice", model);

		//    model.CurrentUserName = CurrentSession.UserName;
		//    model.UserDesignation = CurrentSession.MemberDesignation;

		//    model.SessionId = Convert.ToInt32(siteSettingMod.SessionCode);
		//    model.AssemblyCode = Convert.ToInt32(siteSettingMod.AssemblyCode);

		//    mSession Mdl = new mSession();
		//    Mdl.SessionCode = model.SessionId;
		//    Mdl.AssemblyID = model.AssemblyCode;
		//    mAssembly assmblyMdl = new mAssembly();
		//    assmblyMdl.AssemblyID = model.AssemblyCode;

		//    model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
		//    model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);


		//    return View(model);
		//}


		//Added code venkat for dynamic 
		public ActionResult MinisterDashboard()
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			tPaperLaidV model = new tPaperLaidV();
			//Added code venkat for dynamic 
			model.MemberId = Convert.ToInt32(CurrentSession.UserName);
			//end------------------------------------
			mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			if (CurrentSession.UserID != null && CurrentSession.UserID != "")
			{
				model.UserID = new Guid(CurrentSession.UserID);
			}

			//    Obj.LoginId=
			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);

			//model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetHashKeyByUIserId", model);


			//Get the Total count of All Type of question.
			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
			{
				model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model);
			}
			else
			{
				model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValueLocal", model);
			}

			if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				CurrentSession.AssemblyId = model.AssemblyId.ToString();
			}

			if (string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				CurrentSession.SessionId = model.SessionId.ToString();
			}
			model.DSCApplicable = siteSettingMod.DSCApplicable;

			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.MinistryId = model.MinistryId;
			model.DepartmentId = CurrentSession.DeptID;
            //Commnet Code Start
            //model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounter", model);

            //model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            //model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetUnstarredQuestionCounter", model);

            //model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForOtherPaperLaidTypes", model);
            //model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetOtherPaperLaidCounters", model);

			//mDepartment deptInfo = new mDepartment();
			//deptInfo.deptId = CurrentSession.DeptID;
			//deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
			//model.DeparmentName = deptInfo.deptname;

			//model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetCountForBillsandNotice", model);

			model.CurrentUserName = CurrentSession.UserName;
			model.UserDesignation = CurrentSession.MemberDesignation;


            var Heading = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetHeadingSetting", null);

            model.Heading = Heading.SettingValue;
            model.HeadingLocal = Heading.SettingValueLocal;
           


			// model.SessionId = Convert.ToInt32(siteSettingMod.SessionCode);
			//  model.AssemblyCode = Convert.ToInt32(siteSettingMod.AssemblyCode);

			mSession Mdl = new mSession();
			Mdl.SessionCode = model.SessionId;
			Mdl.AssemblyID = model.AssemblyId;
			mAssembly assmblyMdl = new mAssembly();
			assmblyMdl.AssemblyID = model.AssemblyId;

			if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
			{
                //model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                //model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

			}
			else
			{
                //model.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameLocalBySessionCode", Mdl);
                //model.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameLocalByAssemblyCode", assmblyMdl);
			}

			if (Convert.ToString(TempData["lSM"]) != "")
			{
				model.lSM = Convert.ToString(TempData["lSM"]);

			}
			else
			{
				model.lSM = "";
			}
			if (Convert.ToString(TempData["Msg"]) != "")
			{
				if (Convert.ToString(TempData["Msg"]) == "Save")
				{
					model.CustomMessage = "Data Save Successfully !!";
					model.RequestMessage = "Your Request ID : " + Convert.ToString(TempData["ReqMesg"]);
				}
				if (Convert.ToString(TempData["Msg"]) == "Update")
				{
					model.CustomMessage = "Data Update Successfully !!";
				}
				if (Convert.ToString(TempData["Msg"]) == "File")
				{
					model.CustomMessage = "Paper Replacement Successfully !!";
				}
				if (Convert.ToString(TempData["Msg"]) == "Sign")
				{
					model.CustomMessage = "Paper Signed Successfully !!";
				}

			}
			else
			{
				model.CustomMessage = "";
			}


			return View(model);
		}

		#region Stared Questions

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult GetAllStarredQuestions(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex + 1;


			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}



			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;


			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetAllStarredQuestions", model);


			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult GetAllStarredQuestions(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{

			if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

			tPaperLaidV model = new tPaperLaidV();
			return PartialView("_AllQuestionsGrid", model);

			//if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			////PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			////RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			////loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			////loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
			//tPaperLaidV model = new tPaperLaidV();
			//mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
			//if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			//{
			//    model.LoginId = CurrentSession.UserName;
			//}

			////Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", Obj);

			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			//model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);

			//foreach (var item in model.mMinisteryMinister)
			//{
			//    model.MinisterName = item.MinisterName;

			//    break;
			//}
			//model.loopStart = Convert.ToInt16(loopStart);
			//model.loopEnd = Convert.ToInt16(loopEnd);
			//model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			//model.PageIndex = Convert.ToInt16(PageNumber);





			////Get the Questions related to Stared Question.
			//model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			//model.MinistryId = model.MinistryId;
			//model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetAllStarredQuestions", model);

			//if (PageNumber != null && PageNumber != "")
			//{
			//    model.PageNumber = Convert.ToInt32(PageNumber);
			//}
			//else
			//{
			//    model.PageNumber = Convert.ToInt32("1");
			//}
			//if (RowsPerPage != null && RowsPerPage != "")
			//{
			//    model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			//}
			//else
			//{
			//    model.RowsPerPage = Convert.ToInt32("10");
			//}
			//if (PageNumber != null && PageNumber != "")
			//{
			//    model.selectedPage = Convert.ToInt32(PageNumber);
			//}
			//else
			//{
			//    model.selectedPage = Convert.ToInt32("1");
			//}
			//if (loopStart != null && loopStart != "")
			//{
			//    model.loopStart = Convert.ToInt32(loopStart);
			//}
			//else
			//{
			//    model.loopStart = Convert.ToInt32("1");
			//}
			//if (loopEnd != null && loopEnd != "")
			//{
			//    model.loopEnd = Convert.ToInt32(loopEnd);
			//}
			//else
			//{
			//    model.loopEnd = Convert.ToInt32("5");
			//}

			//return PartialView("_AllQuestionsGrid", model);
		}


		public ActionResult PendingStaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{

			if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }
			tPaperLaidV model = new tPaperLaidV();
			return PartialView("_PendingStaredQuestion", model);
			
		}


		//Added code venkat for dynamic 
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult PendingStaredQuestion(JqGridRequest request, string Diarynumber, string Subject, int? QNum, int? Name)
		{

			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex + 1;


			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
			
			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}


			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.MinistryId = model.MinistryId;
			//Added code venkat for dynamic 
			model.DiaryNumber = Diarynumber;
			model.Subject = Subject;
			model.QuestionNumber = QNum ?? 0;
			model.MinID = Name ?? 0;
            if (CurrentSession.RoleID.ToUpper() == "0FC60E41-C78E-46B0-B8AC-F6EB709BE999")
            {
                model.RequestMessage = "ChiefMinisterAdmin";
            }
			//end-----------------------------
			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult SubmittedStaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{

			if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

			tPaperLaidV model = new tPaperLaidV();
			return PartialView("_SubmittedStaredQuestion", model);

		}
		//Added code venkat for dynamic 
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult SubmittedStaredQuestion(JqGridRequest request, string Diarynumber, string Subject, int? QNum, int? Name)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex + 1;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;
			//Added code venkat for dynamic 
			model.Subject = Subject;
			model.DiaryNumber = Diarynumber;
			model.MinID = Name;
			model.QuestionNumber = QNum;

            if (CurrentSession.RoleID.ToUpper() == "0FC60E41-C78E-46B0-B8AC-F6EB709BE999")
            {
                model.RequestMessage = "ChiefMinisterAdmin";
            }
			//end------------------------------------------
			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetSubmittedQuestionsByType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionSendModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}


		public ActionResult PendingForsubmissionStared(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

			tPaperLaidV model = new tPaperLaidV();

			model.loopStart = Convert.ToInt16(loopStart);
			model.loopEnd = Convert.ToInt16(loopEnd);
			model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			model.PageIndex = Convert.ToInt16(PageNumber);

			//Get the Questions related to Stared Question.
			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.DepartmentId = CurrentSession.DeptID;
			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingForSubQuestionsByType", model);

			if (PageNumber != null && PageNumber != "")
			{
				model.PageNumber = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.PageNumber = Convert.ToInt32("1");
			}
			if (RowsPerPage != null && RowsPerPage != "")
			{
				model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			}
			else
			{
				model.RowsPerPage = Convert.ToInt32("10");
			}
			if (PageNumber != null && PageNumber != "")
			{
				model.selectedPage = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.selectedPage = Convert.ToInt32("1");
			}
			if (loopStart != null && loopStart != "")
			{
				model.loopStart = Convert.ToInt32(loopStart);
			}
			else
			{
				model.loopStart = Convert.ToInt32("1");
			}
			if (loopEnd != null && loopEnd != "")
			{
				model.loopEnd = Convert.ToInt32(loopEnd);
			}
			else
			{
				model.loopEnd = Convert.ToInt32("5");
			}

			return PartialView("_PendingForsubmissionStared", model);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult StarredUpcomingLOB(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;


			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpcomingLOBByQuestionType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionSendModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult StarredUpcomingLOB(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

			mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
			tPaperLaidV model = new tPaperLaidV();
			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}
			//Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", Obj);



			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
			foreach (var item in model.mMinisteryMinister)
			{
				model.MinisterName = item.MinisterName;
				break;
			}
			model.loopStart = Convert.ToInt16(loopStart);
			model.loopEnd = Convert.ToInt16(loopEnd);
			model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			model.PageIndex = Convert.ToInt16(PageNumber);

			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			//Get the Questions related to Stared Question.
			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.MinistryId = model.MinistryId;

			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpcomingLOBByQuestionType", model);

			if (PageNumber != null && PageNumber != "")
			{
				model.PageNumber = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.PageNumber = Convert.ToInt32("1");
			}
			if (RowsPerPage != null && RowsPerPage != "")
			{
				model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			}
			else
			{
				model.RowsPerPage = Convert.ToInt32("10");
			}
			if (PageNumber != null && PageNumber != "")
			{
				model.selectedPage = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.selectedPage = Convert.ToInt32("1");
			}
			if (loopStart != null && loopStart != "")
			{
				model.loopStart = Convert.ToInt32(loopStart);
			}
			else
			{
				model.loopStart = Convert.ToInt32("1");
			}
			if (loopEnd != null && loopEnd != "")
			{
				model.loopEnd = Convert.ToInt32(loopEnd);
			}
			else
			{
				model.loopEnd = Convert.ToInt32("5");
			}

			return PartialView("_StarredUpcomingLOB", model);
		}

		public ActionResult UnstarredUpcomingLOB(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

			tPaperLaidV model = new tPaperLaidV();

			model.loopStart = Convert.ToInt16(loopStart);
			model.loopEnd = Convert.ToInt16(loopEnd);
			model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			model.PageIndex = Convert.ToInt16(PageNumber);

			mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}
			//Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", Obj);

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
			foreach (var item in model.mMinisteryMinister)
			{
				model.MinisterName = item.MinisterName;
				break;
			}
			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			//Get the Questions related to Stared Question.
			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.MinistryId = model.MinistryId;
			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);

			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpcomingLOBByQuestionType", model);

			if (PageNumber != null && PageNumber != "")
			{
				model.PageNumber = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.PageNumber = Convert.ToInt32("1");
			}
			if (RowsPerPage != null && RowsPerPage != "")
			{
				model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			}
			else
			{
				model.RowsPerPage = Convert.ToInt32("10");
			}
			if (PageNumber != null && PageNumber != "")
			{
				model.selectedPage = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.selectedPage = Convert.ToInt32("1");
			}
			if (loopStart != null && loopStart != "")
			{
				model.loopStart = Convert.ToInt32(loopStart);
			}
			else
			{
				model.loopStart = Convert.ToInt32("1");
			}
			if (loopEnd != null && loopEnd != "")
			{
				model.loopEnd = Convert.ToInt32(loopEnd);
			}
			else
			{
				model.loopEnd = Convert.ToInt32("5");
			}

			return PartialView("_UnstarredUpcomingLOB", model);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult UnstarredUpcomingLOB(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;


			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "UpcomingLOBByQuestionType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionSendModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult LaidInHouseStared(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);


			mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
			tPaperLaidV model = new tPaperLaidV();
			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}
			//Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", Obj);


			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
			foreach (var item in model.mMinisteryMinister)
			{
				model.MinisterName = item.MinisterName;
				break;
			}
			model.loopStart = Convert.ToInt16(loopStart);
			model.loopEnd = Convert.ToInt16(loopEnd);
			model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			model.PageIndex = Convert.ToInt16(PageNumber);

			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			//Get the Questions related to Stared Question.
			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.MinistryId = model.MinistryId;

			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetLaidInHouseQuestionsByType", model);

			if (PageNumber != null && PageNumber != "")
			{
				model.PageNumber = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.PageNumber = Convert.ToInt32("1");
			}
			if (RowsPerPage != null && RowsPerPage != "")
			{
				model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			}
			else
			{
				model.RowsPerPage = Convert.ToInt32("10");
			}
			if (PageNumber != null && PageNumber != "")
			{
				model.selectedPage = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.selectedPage = Convert.ToInt32("1");
			}
			if (loopStart != null && loopStart != "")
			{
				model.loopStart = Convert.ToInt32(loopStart);
			}
			else
			{
				model.loopStart = Convert.ToInt32("1");
			}
			if (loopEnd != null && loopEnd != "")
			{
				model.loopEnd = Convert.ToInt32(loopEnd);
			}
			else
			{
				model.loopEnd = Convert.ToInt32("5");
			}

			//return PartialView("_SubmittedStaredQuestion", model);
			return PartialView("_LaidInHouseStared", model);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult LaidInHouseStared(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;


			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetLaidInHouseQuestionsByType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionSendModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult PendingToLayStared(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);


			mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
			tPaperLaidV model = new tPaperLaidV();
			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}
			//Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", Obj);



			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
			foreach (var item in model.mMinisteryMinister)
			{
				model.MinisterName = item.MinisterName;
				break;
			}
			model.loopStart = Convert.ToInt16(loopStart);
			model.loopEnd = Convert.ToInt16(loopEnd);
			model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			model.PageIndex = Convert.ToInt16(PageNumber);

			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			//Get the Questions related to Stared Question.
			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.MinistryId = model.MinistryId;

			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingToLayQuestionsByType", model);

			if (PageNumber != null && PageNumber != "")
			{
				model.PageNumber = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.PageNumber = Convert.ToInt32("1");
			}
			if (RowsPerPage != null && RowsPerPage != "")
			{
				model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			}
			else
			{
				model.RowsPerPage = Convert.ToInt32("10");
			}
			if (PageNumber != null && PageNumber != "")
			{
				model.selectedPage = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.selectedPage = Convert.ToInt32("1");
			}
			if (loopStart != null && loopStart != "")
			{
				model.loopStart = Convert.ToInt32(loopStart);
			}
			else
			{
				model.loopStart = Convert.ToInt32("1");
			}
			if (loopEnd != null && loopEnd != "")
			{
				model.loopEnd = Convert.ToInt32(loopEnd);
			}
			else
			{
				model.loopEnd = Convert.ToInt32("5");
			}

			return PartialView("_PendingToLayStared", model);
		}

		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult PendingToLayStared(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			model.QuestionTypeId = (int)QuestionType.StartedQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;


			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingToLayQuestionsByType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionSendModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}
		#endregion

		#region Un-Stared Questions.

		public ActionResult AllUnstaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{

			if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

			tPaperLaidV model = new tPaperLaidV();
			return PartialView("_GetAllUnstaredQuestion", model);			
		}


		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult AllUnstaredQuestion(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex + 1;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}



			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;
            if (CurrentSession.RoleID.ToUpper() == "0FC60E41-C78E-46B0-B8AC-F6EB709BE999")
            {
                model.RequestMessage = "ChiefMinisterAdmin";
            }

			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetAllUnstaredQuestionByType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult PendingUnstaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{


			if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

			tPaperLaidV model = new tPaperLaidV();
			return PartialView("_PendingUnstaredQuestion", model);			
		}

		//Added code venkat for dynamic 
		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult PendingUnstaredQuestion(JqGridRequest request, string DiaryNumber, string Subject, int? QNum, int? Name)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex + 1;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;
			//Added code venkat for dynamic 
			model.DiaryNumber = DiaryNumber;
			model.Subject = Subject;
			model.QuestionNumber = QNum;
			model.MinID = Name;
            if (CurrentSession.RoleID.ToUpper() == "0FC60E41-C78E-46B0-B8AC-F6EB709BE999")
            {
                model.RequestMessage = "ChiefMinisterAdmin";
            }
			//end------------------------------------------
			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<QuestionModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<QuestionModelCustom>(Convert.ToString(questionList.QuestionID), new QuestionModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult SubmittedUnstaredQuestion(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{

			//if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

			//tPaperLaidV model = new tPaperLaidV();
			//return PartialView("_SubmittedUnstaredQuestion", model);

			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

			tPaperLaidV model = new tPaperLaidV();
			

			return PartialView("_SubmittedUnstaredQuestion", model);
		}


		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult SubmittedUnstaredQuestion(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex + 1;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}


			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;


			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetSubmittedQuestionsByType", model);
            if (CurrentSession.RoleID.ToUpper() == "0FC60E41-C78E-46B0-B8AC-F6EB709BE999")
            {
                model.RequestMessage = "ChiefMinisterAdmin";
            }

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionSendModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult PendingForsubmissionUnstared(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

			tPaperLaidV model = new tPaperLaidV();

			model.loopStart = Convert.ToInt16(loopStart);
			model.loopEnd = Convert.ToInt16(loopEnd);
			model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			model.PageIndex = Convert.ToInt16(PageNumber);

			//Get the Questions related to Stared Question.
			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.DepartmentId = CurrentSession.DeptID;
			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingForSubQuestionsByType", model);

			if (PageNumber != null && PageNumber != "")
			{
				model.PageNumber = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.PageNumber = Convert.ToInt32("1");
			}
			if (RowsPerPage != null && RowsPerPage != "")
			{
				model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			}
			else
			{
				model.RowsPerPage = Convert.ToInt32("10");
			}
			if (PageNumber != null && PageNumber != "")
			{
				model.selectedPage = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.selectedPage = Convert.ToInt32("1");
			}
			if (loopStart != null && loopStart != "")
			{
				model.loopStart = Convert.ToInt32(loopStart);
			}
			else
			{
				model.loopStart = Convert.ToInt32("1");
			}
			if (loopEnd != null && loopEnd != "")
			{
				model.loopEnd = Convert.ToInt32(loopEnd);
			}
			else
			{
				model.loopEnd = Convert.ToInt32("5");
			}

			return PartialView("_PendingForsubmissionUnstared", model);
		}

		public ActionResult LaidInHouseUnstared(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

			tPaperLaidV model = new tPaperLaidV();
			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}
			model.loopStart = Convert.ToInt16(loopStart);
			model.loopEnd = Convert.ToInt16(loopEnd);
			model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			model.PageIndex = Convert.ToInt16(PageNumber);

			mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				Obj.LoginId = CurrentSession.UserName;
			}
			//Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", Obj);

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
			foreach (var item in model.mMinisteryMinister)
			{
				model.MinisterName = item.MinisterName;
				break;
			}
			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}


			//Get the Questions related to Stared Question.
			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.MinistryId = model.MinistryId;
			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);

			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetLaidInHouseQuestionsByType", model);

			if (PageNumber != null && PageNumber != "")
			{
				model.PageNumber = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.PageNumber = Convert.ToInt32("1");
			}
			if (RowsPerPage != null && RowsPerPage != "")
			{
				model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			}
			else
			{
				model.RowsPerPage = Convert.ToInt32("10");
			}
			if (PageNumber != null && PageNumber != "")
			{
				model.selectedPage = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.selectedPage = Convert.ToInt32("1");
			}
			if (loopStart != null && loopStart != "")
			{
				model.loopStart = Convert.ToInt32(loopStart);
			}
			else
			{
				model.loopStart = Convert.ToInt32("1");
			}
			if (loopEnd != null && loopEnd != "")
			{
				model.loopEnd = Convert.ToInt32(loopEnd);
			}
			else
			{
				model.loopEnd = Convert.ToInt32("5");
			}

			return PartialView("_LaidInHouseUnstared", model);
		}


		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult LaidInHouseUnstared(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);


			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}


			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;

			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);



			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetLaidInHouseQuestionsByType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionSendModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		public ActionResult PendingToLayUnstared(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }
			PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
			RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
			loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
			loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

			tPaperLaidV model = new tPaperLaidV();
			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model.loopStart = Convert.ToInt16(loopStart);
			model.loopEnd = Convert.ToInt16(loopEnd);
			model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
			model.PageIndex = Convert.ToInt16(PageNumber);

			mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				Obj.LoginId = CurrentSession.UserName;
			}
			//Obj = (mMinisteryMinisterModel)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", Obj);
			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
			foreach (var item in model.mMinisteryMinister)
			{
				model.MinisterName = item.MinisterName;
				break;
			}

			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}


			//Get the Questions related to Stared Question.
			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.MinistryId = model.MinistryId;
			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);

			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingToLayQuestionsByType", model);

			if (PageNumber != null && PageNumber != "")
			{
				model.PageNumber = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.PageNumber = Convert.ToInt32("1");
			}
			if (RowsPerPage != null && RowsPerPage != "")
			{
				model.RowsPerPage = Convert.ToInt32(RowsPerPage);
			}
			else
			{
				model.RowsPerPage = Convert.ToInt32("10");
			}
			if (PageNumber != null && PageNumber != "")
			{
				model.selectedPage = Convert.ToInt32(PageNumber);
			}
			else
			{
				model.selectedPage = Convert.ToInt32("1");
			}
			if (loopStart != null && loopStart != "")
			{
				model.loopStart = Convert.ToInt32(loopStart);
			}
			else
			{
				model.loopStart = Convert.ToInt32("1");
			}
			if (loopEnd != null && loopEnd != "")
			{
				model.loopEnd = Convert.ToInt32(loopEnd);
			}
			else
			{
				model.loopEnd = Convert.ToInt32("5");
			}
			return PartialView("_PendingToLayUnstared", model);
		}


		[AcceptVerbs(HttpVerbs.Post)]
		public ActionResult PendingToLayUnstared(JqGridRequest request, tPaperLaidV customModel)
		{
			tPaperLaidV model = new tPaperLaidV();
			model.PAGE_SIZE = request.RecordsCount;

			model.PageIndex = request.PageIndex;
			//model.AssemblyId = 12;
			//model.SessionId = 4;
			model.PaperLaidId = 5;

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);


			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//model.SessionId = model.SessionCode = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}


			model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
			model.DepartmentId = CurrentSession.DeptID;

			model.MinistryId = model.MinistryId;




			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingQuestionsByType", model);


			var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetPendingToLayQuestionsByType", model);

			JqGridResponse response = new JqGridResponse()
			{
				TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
				PageIndex = request.PageIndex,
				TotalRecordsCount = result.ResultCount
			};

			var resultToSort = result.tQuestionSendModel.AsQueryable();

			var resultForGrid = resultToSort.OrderBy<PaperSendModelCustom>(request.SortingName, request.SortingOrder.ToString());

			response.Records.AddRange(from questionList in resultForGrid
									  select new JqGridRecord<PaperSendModelCustom>(Convert.ToString(questionList.QuestionID), new PaperSendModelCustom(questionList)));

			return new JqGridJsonResult() { Data = response };
		}

		#endregion

		public ActionResult GetFullQuestionDetails(string paperLaidId)
		{
			if (CurrentSession.UserID == null) { return RedirectToAction("LoginUP", "Account"); }

			if (CurrentSession.UserID != null)
			{
				PaperMovementModel movementModel = new PaperMovementModel();
				movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
				movementModel = (PaperMovementModel)Helper.ExecuteService("PaperLaidMinister", "ShowPaperLaidDetailByID", movementModel);
				return PartialView("_GetFullQuestionDetails", movementModel);
			}
			return null;
		}

		#region "DSC Sign"

		public ActionResult SubmitSelectedPaper(string Ids, string lSMCode)
		{

			tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

			if (lSMCode != null && lSMCode != "")
			{
				if (lSMCode == "SQ")
				{
					TempData["lSM"] = "SQ";
				}
				else if (lSMCode == "USQ")
				{
					TempData["lSM"] = "USQ";
				}
				else if (lSMCode == "NTC")
				{
					TempData["lSM"] = "NTC";
				}
				else if (lSMCode == "BLL")
				{
					TempData["lSM"] = "BLL";
				}
				else if (lSMCode == "OPT")
				{
					TempData["lSM"] = "OPT";
				}
			}

			if (Ids != null && Ids != "")
			{

				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

				string path = "";
				string Files = "";
				string Prifix = "";
				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{
					path = item.FilePath + item.FileName;
					path = path.Replace(" ", "%20");
					var test = Request.Url.AbsoluteUri;
					string[] abc = test.Split('/');
					Prifix = abc[0] + "//" + abc[2];
					if (Files == "")
					{
						Files = Prifix + path;
					}
					else
					{
						Files = Files + "," + Prifix + path;
					}
				}

				mMinisteryMinisterModel.FilePath = Files;
				mMinisteryMinisterModel.HashKey = CurrentSession.UserHashKey;
				mMinisteryMinisterModel.ForSave = Prifix + "/PaperLaidMinister/PaperLaidMinister/SubmitedSignIntention?PaperLaidId=" + Ids;

				mMinisteryMinisterModel.Message = Prifix + "/PaperLaidMinister/PaperLaidMinister/UpdateInfoSignPDF?PaperLaidId=" + Ids + "&Uid=" + CurrentSession.MemberID;

				string session = Convert.ToString(Session["Ids"]);
			}
			return PartialView("_SignMinisterFile", mMinisteryMinisterModel);
		}

		public ActionResult SubmitSignedSelectedPaper(string Ids, string lSMCode)
		{

			tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
			tPaperLaidTemp pT = new tPaperLaidTemp();
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

			if (lSMCode != null && lSMCode != "")
			{
				if (lSMCode == "SQ")
				{
					TempData["lSM"] = "SQ";
				}
				else if (lSMCode == "USQ")
				{
					TempData["lSM"] = "USQ";
				}
				else if (lSMCode == "NTC")
				{
					TempData["lSM"] = "NTC";
				}
				else if (lSMCode == "BLL")
				{
					TempData["lSM"] = "BLL";
				}
				else if (lSMCode == "OPT")
				{
					TempData["lSM"] = "OPT";
				}
			}

			if (Ids != null && Ids != "")
			{

				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

				string path = "";
				string Files = "";
				string Prifix = "";
				string OldFileName = "";
				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{
					path = item.FilePath + item.FileName;
					OldFileName = item.FileName;
					path = path.Replace(" ", "%20");
					var test = Request.Url.AbsoluteUri;
					string[] abc = test.Split('/');
					Prifix = abc[0] + "//" + abc[2];
					if (Files == "")
					{
						Files = Prifix + path;
					}
					else
					{
						Files = Files + "," + Prifix + path;
					}
				}

				string AssemblyId = "";
				string SessionId = "";

				//SiteSettings siteSettingMod = new SiteSettings();
				//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
				//SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
				//AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);

				SiteSettings siteSettingMod = new SiteSettings();
				siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

				if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
				{
					PaperLaidAttachmentVS.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
				}

				if (!string.IsNullOrEmpty(CurrentSession.SessionId))
				{
					PaperLaidAttachmentVS.SessionId = Convert.ToInt16(CurrentSession.SessionId);
				}

				string[] Afile = OldFileName.Split('.');
				OldFileName = Afile[0];
				string Newpath = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/" + OldFileName + "_signed.pdf";

				pT.FilePath = Ids;
				pT.MinisterSubmittedDate = DateTime.Now;
				pT.MinisterSubmittedBy = Convert.ToInt32(CurrentSession.UserName);
				pT.SessionId = SessionId;
				pT.AssemblyId = AssemblyId;

				//string Newpath = System.IO.Path.Combine(Server.MapPath("~/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/"), OldFileName);

				//int indexof = path.LastIndexOf("\\");

				//string directory = path.Substring(0, indexof);
				////  string directory = Server.MapPath(path);
				//if (!System.IO.Directory.Exists(directory))
				//{
				//    System.IO.Directory.CreateDirectory(directory);
				//}


				//using (Stream FilesStream = new FileStream(Server.MapPath(path), FileMode.Open))
				//using (Stream NewpathStream = new FileStream(Server.MapPath(Newpath), FileMode.Create, FileAccess.ReadWrite))


				System.IO.File.Copy(Server.MapPath(path), Server.MapPath(Newpath));

				mMinisteryMinisterModel.FilePath = Files;

				var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;
				var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids) as tPaperLaidTemp;


			}
			TempData["Msg"] = "Sign";
			return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister", message = "Sign completed" });

		}

		public ActionResult SubmitHandWrittenSignature(string Ids, string lSMCode, int SignatureType, int PagesToApplySignatureID, int PositionToApplySignatureID)
		{

			tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

			if (lSMCode != null && lSMCode != "")
			{
				if (lSMCode == "SQ")
				{
					TempData["lSM"] = "SQ";
				}
				else if (lSMCode == "USQ")
				{
					TempData["lSM"] = "USQ";
				}
				else if (lSMCode == "NTC")
				{
					TempData["lSM"] = "NTC";
				}
				else if (lSMCode == "BLL")
				{
					TempData["lSM"] = "BLL";
				}
				else if (lSMCode == "OPT")
				{
					TempData["lSM"] = "OPT";
				}
			}

			if (Ids != null && Ids != "")
			{

				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

				string path = "";
				string Files = "";
				string Prifix = "";
				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{
					path = item.FilePath + item.FileName;




					// string imageFileLoc =  Server.MapPath("/assets/avatars/newsignatureu.png");

					string imageFileLoc = Server.MapPath(CurrentSession.SignaturePath);
					string newFileLocation = item.FilePath + "HWSigned/" + Path.GetFileNameWithoutExtension(path) + "_HWSigned" + Path.GetExtension(path);

					string textLine1 = "P.Mitra";
					string textLine2 = "Principle Secretory";

					PDFExtensions.InsertImageToPdf(Server.MapPath(path), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);

					path = path.Replace(" ", "%20");
					var test = Request.Url.AbsoluteUri;
					string[] abc = test.Split('/');
					Prifix = abc[0] + "//" + abc[2];
					if (Files == "")
					{
						Files = Prifix + path;
					}
					else
					{
						Files = Files + "," + Prifix + path;
					}
				}

				mMinisteryMinisterModel.FilePath = Files;
				mMinisteryMinisterModel.HashKey = CurrentSession.UserHashKey;
				mMinisteryMinisterModel.ForSave = Prifix + "/PaperLaidMinister/PaperLaidMinister/SubmitedSignIntention?PaperLaidId=" + Ids;

				mMinisteryMinisterModel.Message = Prifix + "/PaperLaidMinister/PaperLaidMinister/UpdateInfoSignPDF?PaperLaidId=" + Ids + "&Uid=" + CurrentSession.MemberID;

				string session = Convert.ToString(Session["Ids"]);
			}
			return PartialView("_SignMinisterFile", mMinisteryMinisterModel);
		}


		public ActionResult SubmitHandWrittenSignatureSelectedPaperWithoutSign(string Ids, string lSMCode, int SignatureType, int PagesToApplySignatureID, int PositionToApplySignatureID)
		{
			tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
			tPaperLaidTemp pT = new tPaperLaidTemp();
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

			if (lSMCode != null && lSMCode != "")
			{
				if (lSMCode == "SQ")
				{
					TempData["lSM"] = "SQ";
				}
				else if (lSMCode == "USQ")
				{
					TempData["lSM"] = "USQ";
				}
				else if (lSMCode == "NTC")
				{
					TempData["lSM"] = "NTC";
				}
				else if (lSMCode == "BLL")
				{
					TempData["lSM"] = "BLL";
				}
				else if (lSMCode == "OPT")
				{
					TempData["lSM"] = "OPT";
				}
			}

			if (Ids != null && Ids != "")
			{

				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

				string path = "";
				string Files = "";
				string Prifix = "";
				string OldFileName = "";
				string newFileLocation = "";
				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{
					path = item.FilePath + item.FileName;
					OldFileName = item.FileName;
					path = path.Replace(" ", "%20");
					var test = Request.Url.AbsoluteUri;
					string[] abc = test.Split('/');
					Prifix = abc[0] + "//" + abc[2];
					if (Files == "")
					{
						Files = Prifix + path;
					}
					else
					{
						Files = Files + "," + Prifix + path;
					}


#pragma warning disable CS0219 // The variable 'AssemblyId' is assigned but its value is never used
					string AssemblyId = "";
#pragma warning restore CS0219 // The variable 'AssemblyId' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'SessionId' is assigned but its value is never used
					string SessionId = "";
#pragma warning restore CS0219 // The variable 'SessionId' is assigned but its value is never used

					//SiteSettings siteSettingMod = new SiteSettings();
					//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
					//SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
					//AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);

					SiteSettings siteSettingMod = new SiteSettings();
					siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

					if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
					{
						PaperLaidAttachmentVS.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
					}

					if (!string.IsNullOrEmpty(CurrentSession.SessionId))
					{
						PaperLaidAttachmentVS.SessionId = Convert.ToInt16(CurrentSession.SessionId);
					}


					pT.FilePath = Ids;
					pT.MinisterSubmittedDate = DateTime.Now;
					pT.MinisterSubmittedBy = Convert.ToInt32(CurrentSession.UserName);
					pT.SessionId = CurrentSession.SessionId;
					pT.AssemblyId = CurrentSession.AssemblyId;

					string imageFileLoc = Server.MapPath(CurrentSession.SignaturePath);

					string[] Afile = OldFileName.Split('.');
					OldFileName = Afile[0];
					newFileLocation = "/PaperLaid/" + pT.AssemblyId + "/" + pT.SessionId + "/Signed/" + OldFileName + "_signed.pdf";
					// string newFileLocation = item.FilePath + "HWSigned/" + Path.GetFileNameWithoutExtension(path) + "_HWSigned" + Path.GetExtension(path);

					string textLine1 = "";
					string textLine2 = "";

					if (CurrentSession.UserName != "")
					{
						if (CurrentSession.IsMember != null)
						{
							if (CurrentSession.IsMember.ToUpper() == "FALSE")
							{

								mEmployee empMod = new mEmployee();
								if (CurrentSession.DeptID != "")
								{
									empMod.deptid = CurrentSession.DeptID;
								}
								empMod.empcd = CurrentSession.UserName;
								empMod = (mEmployee)Helper.ExecuteService("Employee", "GetEmployeeDetialsByEmpID", siteSettingMod);
								if (empMod != null)
								{
									textLine1 = empMod.empfname + "" + empMod.emplname;
								}
								else
								{
									textLine1 = "";
								}
							}
							else
							{

								if (CurrentSession.UserName != null && CurrentSession.UserName != "")
								{
									PaperLaidAttachmentVS.LoginId = CurrentSession.UserName;
								}

								PaperLaidAttachmentVS = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", PaperLaidAttachmentVS);
								foreach (var item1 in PaperLaidAttachmentVS.mMinisteryMinister)
								{
									PaperLaidAttachmentVS.MinisterName = item1.MinisterName;
									PaperLaidAttachmentVS.MinistryId = item1.MinistryID;
								}

								textLine1 = PaperLaidAttachmentVS.MinisterName;

							}
						}

					}
					else
					{
						textLine1 = "";
					}
					if (CurrentSession.Designation != "")
					{
						textLine2 = CurrentSession.MemberDesignation;
					}
					else
					{
						textLine2 = "";
					}


					PDFExtensions.InsertImageToPdf(Server.MapPath(path), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);



				}



				//string Newpath = System.IO.Path.Combine(Server.MapPath("~/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/"), OldFileName);

				//int indexof = path.LastIndexOf("\\");

				//string directory = path.Substring(0, indexof);
				////  string directory = Server.MapPath(path);
				//if (!System.IO.Directory.Exists(directory))
				//{
				//    System.IO.Directory.CreateDirectory(directory);
				//}


				//using (Stream FilesStream = new FileStream(Server.MapPath(path), FileMode.Open))
				//using (Stream NewpathStream = new FileStream(Server.MapPath(Newpath), FileMode.Create, FileAccess.ReadWrite))


				//System.IO.File.Copy(Server.MapPath(path), Server.MapPath(Newpath));

				mMinisteryMinisterModel.FilePath = Files;

				var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

				Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids);

				///For LOB File Replace
				string[] obja = Ids.Split(',');



				foreach (var item in obja)
				{
					tPaperLaidV paperLaid = new tPaperLaidV();
					paperLaid.PaperLaidId = Convert.ToInt16(item);
					paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperLaidId", paperLaid);

					if (paperLaid != null)
					{
						if (paperLaid.LOBRecordId != 0)
						{
							tPaperLaidTemp ptT = new tPaperLaidTemp();
							ptT.PaperLaidTempId = Convert.ToInt16(paperLaid.MinisterActivePaperId);
							ptT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", ptT);
							if (ptT != null)
							{
								DraftLOB LobModel = new DraftLOB();
								LobModel.Id = Convert.ToInt16(paperLaid.LOBRecordId);

								//(from Draftlob in obj1.DraftLOB where Draftlob.Id == LOBRecordId select Draftlob).FirstOrDefault();
								LobModel = (DraftLOB)Helper.ExecuteService("LOB", "GetDraftLOBById", LobModel);
								if (LobModel != null)
								{
									string copyPath = Server.MapPath(ptT.SignedFilePath);
									string pastePath = Server.MapPath(LobModel.PDFLocation);
									System.IO.File.Copy(copyPath, pastePath, true);
								}
							}
						}
					}
				}

			}

			TempData["Msg"] = "Sign";
			return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister", message = "Sign completed" });
		}




		public ActionResult SubmitHandWrittenSignatureSelectedPaperWithoutSignImage(string Ids, string lSMCode)
		{
			tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
			tPaperLaidTemp pT = new tPaperLaidTemp();
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

			if (lSMCode != null && lSMCode != "")
			{
				if (lSMCode == "SQ")
				{
					TempData["lSM"] = "SQ";
				}
				else if (lSMCode == "USQ")
				{
					TempData["lSM"] = "USQ";
				}
				else if (lSMCode == "NTC")
				{
					TempData["lSM"] = "NTC";
				}
				else if (lSMCode == "BLL")
				{
					TempData["lSM"] = "BLL";
				}
				else if (lSMCode == "OPT")
				{
					TempData["lSM"] = "OPT";
				}
			}

			if (Ids != null && Ids != "")
			{

				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

				string path = "";
				string Files = "";
				string Prifix = "";
				string OldFileName = "";

				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{
					path = item.FilePath + item.FileName;
					OldFileName = item.FileName;
					path = path.Replace(" ", "%20");
					var test = Request.Url.AbsoluteUri;
					string[] abc = test.Split('/');
					Prifix = abc[0] + "//" + abc[2];
					if (Files == "")
					{
						Files = Prifix + path;
					}
					else
					{
						Files = Files + "," + Prifix + path;
					}


					string AssemblyId = "";
					string SessionId = "";

					//SiteSettings siteSettingMod = new SiteSettings();
					//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
					//SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
					//AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);


					SiteSettings siteSettingMod = new SiteSettings();
					siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

					if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
					{
						PaperLaidAttachmentVS.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
					}

					if (!string.IsNullOrEmpty(CurrentSession.SessionId))
					{
						PaperLaidAttachmentVS.SessionId = Convert.ToInt16(CurrentSession.SessionId);
					}

					pT.FilePath = Ids;
					pT.MinisterSubmittedDate = DateTime.Now;
					pT.MinisterSubmittedBy = Convert.ToInt32(CurrentSession.UserName);
					AssemblyId = CurrentSession.AssemblyId;
					SessionId = CurrentSession.SessionId;
					pT.SessionId = SessionId;
					pT.AssemblyId = AssemblyId;



#pragma warning disable CS0219 // The variable 'imageFileLoc' is assigned but its value is never used
					string imageFileLoc = "";
#pragma warning restore CS0219 // The variable 'imageFileLoc' is assigned but its value is never used

					if (CurrentSession.SignaturePath == "")
					{
						imageFileLoc = "";
						string[] Afile = OldFileName.Split('.');
						OldFileName = Afile[0];

						string newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/";
						DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(newFileLocation));
						if (!Dir.Exists)
						{
							Dir.Create();
						}
						string PasteLoc = Server.MapPath(newFileLocation + OldFileName + "_Signed.pdf");
						string CopyLoc = Server.MapPath(path);

						System.IO.File.Copy(CopyLoc, PasteLoc, true);

					}



					//string imageFileLoc = Server.MapPath(CurrentSession.SignaturePath);

					//string[] Afile = OldFileName.Split('.');
					//OldFileName = Afile[0];
					//newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/" + OldFileName + "_signed.pdf";
					//// string newFileLocation = item.FilePath + "HWSigned/" + Path.GetFileNameWithoutExtension(path) + "_HWSigned" + Path.GetExtension(path);

					string textLine1 = "";
					string textLine2 = "";

					if (CurrentSession.UserName != "")
					{
						if (CurrentSession.IsMember != null)
						{
							if (CurrentSession.IsMember.ToUpper() == "FALSE")
							{

								mEmployee empMod = new mEmployee();
								if (CurrentSession.DeptID != "")
								{
									empMod.deptid = CurrentSession.DeptID;
								}
								empMod.empcd = CurrentSession.UserName;
								empMod = (mEmployee)Helper.ExecuteService("Employee", "GetEmployeeDetialsByEmpID", siteSettingMod);
								if (empMod != null)
								{
									textLine1 = empMod.empfname + "" + empMod.emplname;
								}
								else
								{
									textLine1 = "";
								}
							}
							else
							{

								if (CurrentSession.UserName != null && CurrentSession.UserName != "")
								{
									PaperLaidAttachmentVS.LoginId = CurrentSession.UserName;
								}

								PaperLaidAttachmentVS = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", PaperLaidAttachmentVS);
								foreach (var item1 in PaperLaidAttachmentVS.mMinisteryMinister)
								{
									PaperLaidAttachmentVS.MinisterName = item1.MinisterName;
									PaperLaidAttachmentVS.MinistryId = item1.MinistryID;
								}

								textLine1 = PaperLaidAttachmentVS.MinisterName;

							}
						}

					}
					else
					{
						textLine1 = "";
					}
					if (CurrentSession.Designation != "")
					{
						textLine2 = CurrentSession.MemberDesignation;
					}
					else
					{
						textLine2 = "";
					}


					// PDFExtensions.InsertImageToPdf(Server.MapPath(path), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);



				}

				mMinisteryMinisterModel.FilePath = Files;

				var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

				Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids);

				///For LOB File Replace
				string[] obja = Ids.Split(',');



				foreach (var item in obja)
				{
					tPaperLaidV paperLaid = new tPaperLaidV();
					paperLaid.PaperLaidId = Convert.ToInt16(item);
					paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperLaidId", paperLaid);

					if (paperLaid != null)
					{
						if (paperLaid.LOBRecordId != 0)
						{
							tPaperLaidTemp ptT = new tPaperLaidTemp();
							ptT.PaperLaidTempId = Convert.ToInt16(paperLaid.MinisterActivePaperId);
							ptT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", ptT);
							if (ptT != null)
							{
								DraftLOB LobModel = new DraftLOB();
								LobModel.Id = Convert.ToInt16(paperLaid.LOBRecordId);

								//(from Draftlob in obj1.DraftLOB where Draftlob.Id == LOBRecordId select Draftlob).FirstOrDefault();
								LobModel = (DraftLOB)Helper.ExecuteService("LOB", "GetDraftLOBById", LobModel);
								if (LobModel != null)
								{
									string copyPath = Server.MapPath(ptT.SignedFilePath);
									string pastePath = Server.MapPath(LobModel.PDFLocation);
									System.IO.File.Copy(copyPath, pastePath, true);
								}
							}
						}
					}
				}

			}

			TempData["Msg"] = "Sign";
			return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister", message = "Sign completed" });
		}

		//public ActionResult SubmitHandWrittenSignatureSelectedPaperWithoutSignImage(string Ids, string lSMCode)
		//{
		//    tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
		//    tPaperLaidTemp pT = new tPaperLaidTemp();

		//    mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

		//    tQuestion ques = new tQuestion();

		//    if (lSMCode != null && lSMCode != "")
		//    {
		//        if (lSMCode == "SQ")
		//        {
		//            TempData["lSM"] = "SQ";
		//        }
		//        else if (lSMCode == "USQ")
		//        {
		//            TempData["lSM"] = "USQ";
		//        }
		//        else if (lSMCode == "NTC")
		//        {
		//            TempData["lSM"] = "NTC";
		//        }
		//        else if (lSMCode == "BLL")
		//        {
		//            TempData["lSM"] = "BLL";
		//        }
		//        else if (lSMCode == "OPT")
		//        {
		//            TempData["lSM"] = "OPT";
		//        }
		//    }

		//    if (Ids != null && Ids != "")
		//    {

		//        PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("Minister", "GetMinisterPaperAttachmentByIds", Ids);

		//        string path = "";
		//        string Files = "";
		//        string Prifix = "";
		//        string OldFileName = "";
		//        foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
		//        {
		//            path = item.FilePath + item.FileName;
		//            OldFileName = item.FileName;

		//            path = path.Replace(" ", "%20");
		//            var test = Request.Url.AbsoluteUri;
		//            string[] abc = test.Split('/');
		//            Prifix = abc[0] + "//" + abc[2];
		//            if (Files == "")
		//            {
		//                Files = Prifix + path;
		//            }
		//            else
		//            {
		//                Files = Files + "," + Prifix + path;
		//            }

		//            string AssemblyId = "";
		//            string SessionId = "";

		//            SiteSettings siteSettingMod = new SiteSettings();
		//            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
		//            SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
		//            AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);


		//            pT.FilePath = Ids;
		//            pT.MinisterSubmittedDate = DateTime.Now;
		//            pT.DeptSubmittedDate = DateTime.Now;
		//            pT.DeptSubmittedBy = CurrentSession.UserName;
		//            pT.SessionId = SessionId;
		//            pT.AssemblyId = AssemblyId;

		//                string[] Afile = OldFileName.Split('.');
		//                OldFileName = Afile[0];

		//                string newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/";
		//                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(newFileLocation));
		//                if (!Dir.Exists)
		//                {
		//                    Dir.Create();
		//                }
		//                string PasteLoc = Server.MapPath(newFileLocation + OldFileName + "_Signed.pdf");
		//                string CopyLoc = Server.MapPath(path);

		//                System.IO.File.Copy(CopyLoc, PasteLoc, true);

		//            }

		//        mMinisteryMinisterModel.FilePath = Files;


		//        var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

		//        Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids);

		//        //var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

		//        //var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids) as tPaperLaidTemp;

		//        //var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;
		//        //var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids) as tPaperLaidTemp;

		//        ///For LOB File Replace
		//        string[] obja = Ids.Split(',');



		//        foreach (var item in obja)
		//        {
		//            tPaperLaidV paperLaid = new tPaperLaidV();
		//            paperLaid.PaperLaidId = Convert.ToInt16(item);
		//            paperLaid = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetPaperLaidByPaperLaidId", paperLaid);

		//            if (paperLaid != null)
		//            {
		//                if (paperLaid.LOBRecordId != 0)
		//                {
		//                    tPaperLaidTemp ptT = new tPaperLaidTemp();
		//                    ptT.PaperLaidTempId = Convert.ToInt16(paperLaid.MinisterActivePaperId);
		//                    ptT = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetPaperLaidTempById", ptT);
		//                    if (ptT != null)
		//                    {
		//                        DraftLOB LobModel = new DraftLOB();
		//                        LobModel.Id = Convert.ToInt16(paperLaid.LOBRecordId);

		//                        //(from Draftlob in obj1.DraftLOB where Draftlob.Id == LOBRecordId select Draftlob).FirstOrDefault();
		//                        LobModel = (DraftLOB)Helper.ExecuteService("LOB", "GetDraftLOBById", LobModel);
		//                        if (LobModel != null)
		//                        {
		//                            string copyPath = Server.MapPath(ptT.SignedFilePath);
		//                            string pastePath = Server.MapPath(LobModel.PDFLocation);
		//                            System.IO.File.Copy(copyPath, pastePath, true);
		//                        }
		//                    }
		//                }
		//            }
		//        }

		//    }
		//    TempData["Msg"] = "Sign";
		//    return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister", message = "Sign completed" });

		//}

		[SBLAuthorize(Allow = "ALL")]
		public ActionResult SubmitedSignIntention(HttpPostedFileBase file, string PaperLaidId)
		{
			PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);
			tPaperLaidV model = new tPaperLaidV();
			if (file != null)
			{


				if (CurrentSession.UserID != null && CurrentSession.UserID != "")
				{
					model.UserID = new Guid(CurrentSession.UserID);
				}

				//    Obj.LoginId=


				model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetHashKeyByUIserId", model);

				model.HashKey = model.HashKey;
				string filename = System.IO.Path.GetFileName(file.FileName);
				bool folderExists = Directory.Exists(Server.MapPath(file.FileName));

				string AssemblyId = "";
				string SessionId = "";

				//SiteSettings siteSettingMod = new SiteSettings();
				//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
				//SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
				//AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);

				SiteSettings siteSettingMod = new SiteSettings();
				siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

				if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
				{
					model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
				}

				if (!string.IsNullOrEmpty(CurrentSession.SessionId))
				{
					model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
				}

				string path = System.IO.Path.Combine(Server.MapPath("~/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/"), filename);

				int indexof = path.LastIndexOf("\\");

				string directory = path.Substring(0, indexof);
				//  string directory = Server.MapPath(path);
				if (!System.IO.Directory.Exists(directory))
				{
					System.IO.Directory.CreateDirectory(directory);
				}

				file.SaveAs(path);

				tPaperLaidTemp PaperLaidAttachmentVS = new tPaperLaidTemp();

				mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

				return null;
			}

			return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister", message = "Sign completed" });
		}

		[SBLAuthorize(Allow = "ALL")]
		public ActionResult UpdateInfoSignPDF(string PaperLaidId, string Uid, string Hash)
		{
			PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);

			Uid = Sanitizer.GetSafeHtmlFragment(Uid);
			tPaperLaidV model = new tPaperLaidV();
			tPaperLaidTemp pT = new tPaperLaidTemp();
			if (CurrentSession.UserID != null && CurrentSession.UserID != "")
			{
				model.UserID = new Guid(CurrentSession.UserID);
			}

			//    Obj.LoginId=


			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetHashKeyByUIserId", model);

			model.HashKey = model.HashKey;

			pT.FilePath = PaperLaidId;
			pT.MinisterSubmittedDate = DateTime.Now;
			pT.MinisterSubmittedBy = Convert.ToInt32(CurrentSession.UserName);
			string AssemblyId = "";
			string SessionId = "";

			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
			//SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
			//AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);
			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			pT.AssemblyId = AssemblyId;
			pT.SessionId = SessionId;
			var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

			var modelReturned1 = Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", PaperLaidId) as tPaperLaidTemp;

			TempData["Msg"] = "Sign";
			return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister", message = "Sign completed" });
		}


		public ActionResult BillsHandWrittenSignDocument(string SIds)
		{
			HandwrittenSignatureModel model = new HandwrittenSignatureModel();
			model.PagesToApplySignatureList = StaticControlBinder.GetPagesToApplySignature();
			model.PositionToApplySignatureList = StaticControlBinder.GetPositionToApplySignature();
			return View("_BillsHandWrittenSignDocument", model);
		}


		public ActionResult SaveHandWrittenSignature()
		{
			return View();
		}

		private string alert(string p)
		{
			throw new NotImplementedException();
		}

		#endregion


		#region Details

		[HttpGet]
		public ActionResult MinisterPaperLaidDetails(string PaperLaidId, string Module)
		{
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
			mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
			mMinisteryMinisterModel.MinisterName = CurrentSession.MemberID;
			mMinisteryMinisterModel.QuesNumber = mMinisteryMinisterModel.QuesNumber;
			mMinisteryMinisterModel = (mMinisteryMinisterModel)Helper.ExecuteService("PaperLaidMinister", "MinisterPaperLaidDetails", mMinisteryMinisterModel);
			mMinisteryMinisterModel.UserDesignation = Module;
			return PartialView("_MinisterPaperLaidDetails", mMinisteryMinisterModel);
		}

		[HttpGet]
		public ActionResult SubmittedMinisterPaperLaidDetails(string PaperLaidId, string Module)
		{
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
			mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
			mMinisteryMinisterModel.MinisterName = CurrentSession.MemberID;

			mMinisteryMinisterModel = (mMinisteryMinisterModel)Helper.ExecuteService("PaperLaidMinister", "SubmittedMinisterPaperLaidDetails", mMinisteryMinisterModel);
			mMinisteryMinisterModel.UserDesignation = Module;
			return PartialView("_SubmittedMinisterPaperLaidDetails", mMinisteryMinisterModel);
		}

		[HttpGet]
		public ActionResult ChangedPaperDetails(string PaperLaidId, string DeptActivePaperId)
		{
			PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);

			tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
			if (PaperLaidId != null && PaperLaidId != "")
			{

				string PaperLaidID = PaperLaidId;
				mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);

				PaperLaidAttachmentVS.PaperLaidId = Convert.ToInt16(PaperLaidID);

				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("PaperLaidMinister", "GetChangedPaperDetails", PaperLaidId);
				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{

					PaperLaidAttachmentVS.PaperCategoryTypeId = item.PaperCategoryTypeId;
				}
			}

			return PartialView("_ChangedPaperDetails", PaperLaidAttachmentVS);

		}

		[HttpGet]
		public ActionResult GetSubmitChangedPaperDetails(string PaperLaidId, string DeptActivePaperId)
		{
			PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);

			tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
			if (PaperLaidId != null && PaperLaidId != "")
			{

				string PaperLaidID = PaperLaidId;
				mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);

				PaperLaidAttachmentVS.PaperLaidId = Convert.ToInt16(PaperLaidID);

				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("PaperLaidMinister", "GetSubmitChangedPaperDetails", PaperLaidId);

				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{

					PaperLaidAttachmentVS.PaperCategoryTypeId = item.PaperCategoryTypeId;


				}
			}

			return PartialView("_SubmittedChangedPaperLaid", PaperLaidAttachmentVS);

		}

		public ActionResult ChangedViewPdfFromServerSignIntention(string PaperLaidTempId)
		{
			PaperLaidTempId = Sanitizer.GetSafeHtmlFragment(PaperLaidTempId);
			if (PaperLaidTempId != null && PaperLaidTempId != "")
			{

				mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
				tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
				int paperlaidid = Convert.ToInt16(PaperLaidTempId);
				PaperLaidAttachmentVS.PaperLaidId = paperlaidid;
				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("PaperLaidMinister", "ChangedGetMinisterPaperAttachment", paperlaidid);

				string path = "";
				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{
					path = item.FilePath;

				}

				if (path == "")
				{
					return null;
				}
				return File(path, "application/pdf");
			}
			else
			{
				return null;
			}
		}

		public ActionResult ChangedSubmittedViewPdfFromServerSignIntention(string PaperLaidTempId)
		{
			PaperLaidTempId = Sanitizer.GetSafeHtmlFragment(PaperLaidTempId);
			if (PaperLaidTempId != null && PaperLaidTempId != "")
			{

				mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
				tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
				int paperlaidid = Convert.ToInt16(PaperLaidTempId);
				PaperLaidAttachmentVS.PaperLaidId = paperlaidid;
				PaperLaidAttachmentVS.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helpers.Helper.ExecuteService("PaperLaidMinister", "ChangedGetMinisterPaperAttachment", paperlaidid);

				string path = "";
				foreach (var item in PaperLaidAttachmentVS.mMinisteryMinister)
				{
					path = item.SignedFilePath;

				}

				if (path == "")
				{
					return null;
				}
				return File(path, "application/pdf");

			}
			else
			{
				return null;
			}
		}

		public JsonResult CheckDSCHaskKey(string key)
		{
			string HashKey = key;
			mUserDSHDetails DSCDetails = new mUserDSHDetails();
			DSCDetails.HashKey = HashKey;
			mUsers user = new mUsers();
			DSCDetails = (mUserDSHDetails)Helper.ExecuteService("User", "GetDSCDetailsByHashKey", DSCDetails);
			return Json(DSCDetails, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public ActionResult BillsPaperLaidDetails(string PaperLaidId, string Module)
		{
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
			mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
			mMinisteryMinisterModel.MinisterName = CurrentSession.MemberID;
			mMinisteryMinisterModel.QuesNumber = mMinisteryMinisterModel.QuesNumber;
			mMinisteryMinisterModel = (mMinisteryMinisterModel)Helper.ExecuteService("PaperLaidMinister", "BillsPaperLaidDetails", mMinisteryMinisterModel);
			mMinisteryMinisterModel.UserDesignation = Module;
			return PartialView("_BillsPaperLaidDetails", mMinisteryMinisterModel);
		}

		[HttpGet]
		public ActionResult SubmittedBillsPaperLaidDetails(string PaperLaidId, string Module)
		{
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
			mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
			mMinisteryMinisterModel.MinisterName = CurrentSession.MemberID;

			mMinisteryMinisterModel = (mMinisteryMinisterModel)Helper.ExecuteService("PaperLaidMinister", "SubmittedBillsPaperLaidDetails", mMinisteryMinisterModel);
			mMinisteryMinisterModel.UserDesignation = Module;
			return PartialView("_SubmittedBillsPaperLaidDetails", mMinisteryMinisterModel);
		}

		[HttpGet]
		public ActionResult NoticePaperLaidDetails(string PaperLaidId, string Module)
		{
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
			mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
			mMinisteryMinisterModel.MinisterName = CurrentSession.MemberID;
			mMinisteryMinisterModel.QuesNumber = mMinisteryMinisterModel.QuesNumber;
			mMinisteryMinisterModel = (mMinisteryMinisterModel)Helper.ExecuteService("PaperLaidMinister", "NoticePaperLaidDetails", mMinisteryMinisterModel);
			mMinisteryMinisterModel.UserDesignation = Module;
			return PartialView("_NoticePaperLaidDetails", mMinisteryMinisterModel);
		}

		[HttpGet]
		public ActionResult SubmittedNoticePaperLaidDetails(string PaperLaidId, string Module)
		{
			mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
			mMinisteryMinisterModel.PaperLaidId = Convert.ToInt16(PaperLaidId);
			mMinisteryMinisterModel.MinisterName = CurrentSession.MemberID;

			mMinisteryMinisterModel = (mMinisteryMinisterModel)Helper.ExecuteService("PaperLaidMinister", "SubmittedNoticePaperLaidDetails", mMinisteryMinisterModel);
			mMinisteryMinisterModel.UserDesignation = Module;
			return PartialView("_SubmittedNoticePaperLaidDetails", mMinisteryMinisterModel);
		}

		public ActionResult MainDepartmentDashboard()
		{
			tPaperLaidV model = new tPaperLaidV();
			mSessionDate sessDate = new mSessionDate();
			//Get the Total count of All Type of question.
			//SiteSettings siteSettingMod = new SiteSettings();
			//siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			//model.SessionId = model.SessionCode = model.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
			//model.AssemblyId = model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

			SiteSettings siteSettingMod = new SiteSettings();
			siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}

			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				model.LoginId = CurrentSession.UserName;
			}
			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				sessDate.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				sessDate.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}
			model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDynamicMainMenuByUserId", model);
			if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				CurrentSession.AssemblyId = model.AssemblyId.ToString();
			}

			if (string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				CurrentSession.SessionId = model.SessionId.ToString();
			}
			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetStarredQuestionCounter", model);

			model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
			model.MinistryId = model.MinistryId;


			model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetDepartmentMinisterDashboardItemsCounter", model);
			//added code venkat for presesent sessiondate
			List<mSessionDate> mSessionDate = new List<mSessionDate>();
			List<tPaperLaidV> ListtPaperLaidV = new List<tPaperLaidV>();
			mSessionDate = (List<mSessionDate>)Helper.ExecuteService("PaperLaid", "GetSessionDate", null);
			for (int i = 0; i < mSessionDate.Count; i++)
			{
				tPaperLaidV tpaperlaid = new tPaperLaidV();
				tpaperlaid.SessionDate = mSessionDate[i].SessionDate;
				tpaperlaid.ShowLOB = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "GetShowLob", mSessionDate[i].Id);
				tpaperlaid.ShowSQuestion = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showSQuestion", mSessionDate[i].Id);
				tpaperlaid.ShowUnQuestion = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showUnQuestion", mSessionDate[i].Id);
				tpaperlaid.ShowBillPassedorIntroduced = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showBillPassedorIntroduced", mSessionDate[i].Id);
				tpaperlaid.ShowBreifOfProceeding = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showBreifOfProceeding", mSessionDate[i].Id);
				tpaperlaid.ShowHouseProceeding = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showHouseProceeding", mSessionDate[i].Id);
				tpaperlaid.PostUponQuestion = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showPostUpon", mSessionDate[i].Id);

				model.ListtPaperLaidV.Add(tpaperlaid);
			}

			var assemblyid = (List<SiteSettings>)Helper.ExecuteService("PaperLaid", "GetAssemblyId", null);

			var sessionid = (List<SiteSettings>)Helper.ExecuteService("PaperLaid", "GetSessionId", null);
			StringBuilder sb = new StringBuilder();
			sb.Append(sessionid.FirstOrDefault().SettingName + ",");
			sb.Append(assemblyid.FirstOrDefault().SettingName + ",");

			model.ShowProvisonalCalender = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showProvisonalCalender", sessDate);
			model.ShowRotationalMinister = (AssemblySessionFilePath)Helper.ExecuteService("PaperLaid", "_showRotationalMinister", sessDate);//sb.ToString());
			//end---------------------------------------
			mUsers user = new mUsers();
			user.UserId = new Guid(CurrentSession.UserID);
			user.IsMember = CurrentSession.IsMember;

			user = (mUsers)Helper.ExecuteService("User", "GetIsMemberDetails", user);
			if (user != null)
			{

				model.CurrentUserName = user.Name;

			}
			return PartialView("_MainDashboard", model);
		}

		public ActionResult GetAssemblyFilePdf(string filepathName)
		{
			try
			{

				//   var filephyacesSetting = _siteRepository.GetFileAcessPhySettings().SettingValue;
				//  var filephyfileacessingpath = filepathName.Replace('/', '\\');
				Stream stream = null;

				//int bytesToRead = 10000;
				//byte[] buffer = new Byte[bytesToRead];
				HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filepathName);
				fileReq.Method = "GET";
				HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

				//if (fileReq.ContentLength > 0)
				//  fileResp.ContentLength = fileReq.ContentLength;

				stream = fileResp.GetResponseStream();
				return File(stream, "application/pdf");
			}
#pragma warning disable CS0168 // The variable 'e' is declared but never used
			catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
			{
				return View("FileNotFound");
			}
		}

		public ActionResult StarredBeforeFixation()
		{
			PassesViewModel model = new PassesViewModel();
			tPaperLaidV objPaperLaid = new tPaperLaidV();
			//model.GenderList = ExtensionMethods.GetGender();
			//model.TimeTypeList = ExtensionMethods.GetTimeTypeList();
			mDepartmentPasses model1 = new mDepartmentPasses();


			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				objPaperLaid.LoginId = CurrentSession.UserName;
			}

			objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);

			DiaryModel DMdl = new DiaryModel();
			DMdl.MinistryId = objPaperLaid.MinistryId;
			DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

			if (DMdl.DiaryList != null)
			{
				List<mDepartment> objDept = new List<mDepartment>();
				foreach (var item in DMdl.DiaryList.Distinct())
				{
					model1.DepartmentID += item.DepartmentId + ",";
				}
				model1.DepartmentID = model1.DepartmentID.Substring(0, model1.DepartmentID.LastIndexOf(","));
			}
			else
			{
				model1.DepartmentID = string.Empty;
			}

			model1.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);
			model1.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
			var model2 = (List<mDepartmentPdfPath>)Helper.ExecuteService("PaperLaidMinister", "BeforeFixation", model1);


			return PartialView("_StarredBeforeFixation", model2);
		}

		//public ActionResult NoticesBeforeFixation()
		//{
		//    PassesViewModel model = new PassesViewModel();
		//    tPaperLaidV objPaperLaid = new tPaperLaidV();
		//    //model.GenderList = ExtensionMethods.GetGender();
		//    //model.TimeTypeList = ExtensionMethods.GetTimeTypeList();
		//    mDepartmentPasses model1 = new mDepartmentPasses();


		//    if (CurrentSession.UserName != null && CurrentSession.UserName != "")
		//    {
		//        objPaperLaid.LoginId = CurrentSession.UserName;
		//    }

		//    objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);

		//    DiaryModel DMdl = new DiaryModel();
		//    DMdl.MinistryId = objPaperLaid.MinistryId;
		//    DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

		//    if (DMdl.DiaryList != null)
		//    {
		//        List<mDepartment> objDept = new List<mDepartment>();
		//        foreach (var item in DMdl.DiaryList.Distinct())
		//        {
		//            model1.DepartmentID += item.DepartmentId + ",";
		//        }
		//        model1.DepartmentID = model1.DepartmentID.Substring(0, model1.DepartmentID.LastIndexOf(","));
		//    }
		//    else
		//    {
		//        model1.DepartmentID = string.Empty;
		//    }


		//    var model2 = (List<mDepartment>)Helper.ExecuteService("PaperLaidMinister", "BeforeFixation", model1);


		//    return PartialView("_StarredBeforeFixation", model2);
		//}

		public ActionResult UnStarredBeforeFixation()
		{
			PassesViewModel model = new PassesViewModel();
			tPaperLaidV objPaperLaid = new tPaperLaidV();
			//model.GenderList = ExtensionMethods.GetGender();
			//model.TimeTypeList = ExtensionMethods.GetTimeTypeList();
			mDepartmentPasses model1 = new mDepartmentPasses();


			if (CurrentSession.UserName != null && CurrentSession.UserName != "")
			{
				objPaperLaid.LoginId = CurrentSession.UserName;
			}

			objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);

			DiaryModel DMdl = new DiaryModel();
			DMdl.MinistryId = objPaperLaid.MinistryId;
			DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

			if (DMdl.DiaryList != null)
			{
				List<mDepartment> objDept = new List<mDepartment>();
				foreach (var item in DMdl.DiaryList.Distinct())
				{
					model1.DepartmentID += item.DepartmentId + ",";
				}
				model1.DepartmentID = model1.DepartmentID.Substring(0, model1.DepartmentID.LastIndexOf(","));
			}
			else
			{
				model1.DepartmentID = string.Empty;
			}

			model1.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);
			model1.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
			var model2 = (List<mDepartmentPdfPath>)Helper.ExecuteService("PaperLaidMinister", "BeforeFixation", model1);


			return PartialView("_UnStarredBeforeFixation", model2);
		}

		public ActionResult StarredAfterFixation()
		{
			tPaperLaidV obj = new tPaperLaidV();
			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}
			var model = (List<mSessionDate>)Helper.ExecuteService("PaperLaidMinister", "AfterFixation", obj);
			return PartialView("_StarredAfterFixation", model);
		}

		public ActionResult UnStarredAfterFixation()
		{
			tPaperLaidV obj = new tPaperLaidV();
			if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
			{
				obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			}

			if (!string.IsNullOrEmpty(CurrentSession.SessionId))
			{
				obj.SessionId = Convert.ToInt16(CurrentSession.SessionId);
			}
			var model = (List<mSessionDate>)Helper.ExecuteService("PaperLaidMinister", "AfterFixation", obj);
			return PartialView("_UnStarredAfterFixation", model);
		}

		#endregion
		#region Added Code venkat for Dynamic Menu
		public ActionResult GetSubMenu(int ModuleId, int MemberId, string ActIds)
		{


			mUserSubModules SM = new mUserSubModules();

			SM.ModuleId = ModuleId;
			//MinisterDashboard();

			if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
			{
				SM = (mUserSubModules)Helper.ExecuteService("PaperLaidMinister", "GetSubMenuByMenuId", SM);
			}
			else
			{
				SM = (mUserSubModules)Helper.ExecuteService("PaperLaidMinister", "GetSubMenuByMenuIdLocal", SM);
			}
			SM.MemberId = MemberId;
			SM.ActionIds = ActIds;

			return PartialView("_GetSubMenu", SM);
		}
		public ActionResult GetActionView(string PaperType, int AID, int SID, int Count, string ModulName, int ModuleId)
		{
			DiaryModel model = new DiaryModel();
			model.PaperEntryType = PaperType;
			model.SessionID = SID;
			model.AssemblyID = AID;
			model.ResultCount = Count;
			model.ModuleName = ModulName;
			model.ModuleId = ModuleId;

			//model.RIds = ActionId;
			CurrentSession.SPSubject = "";
			CurrentSession.SPMinId = "";
			CurrentSession.SPFDate = "";
			CurrentSession.SPTDate = "";
			CurrentSession.SPDNum = "";

			if (model.ModuleId == 64)
			{
				if (PaperType == "eFile")
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodeFile.cshtml");
				}
				else if (PaperType == "Receive Paper")
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");
				}
				else if (PaperType == "Send Paper")
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodSend.cshtml");
				}
				else if (PaperType == "Pendency Report") //Pendency Report
				{

					return PartialView("/Areas/eFile/Views/eFile/_MethodPendencyReport.cshtml");

				}
              
			}
            //if (model.ModuleId == 83)
            //{
            //    if (PaperType == "MLA Diary")
            //    {
            //        return PartialView("/Areas/PaperLaidMinister/Views/PaperLaidMinister/_MlaDiaryForCM.cshtml");
            //    }

            //}
            //added by robin
            if (model.ModuleId == 58)
            {
                if (PaperType == "Manage Works") //Manage works for cm
                {
                    return PartialView("/Areas/Constituency/Views/Constituency/_CmFilter.cshtml");
                }
            }


			return PartialView("_GetActionView", model);


		}
		public ActionResult GetListByPaperType(string PaperType, int AssemId, int SessId, int ModuleId)
		{
			// ModuleName = ModuleName.Trim();
			//, string ModuleName
			PaperType = PaperType.Trim();
			tPaperLaidV model1 = new tPaperLaidV();
			//model.MemberId = Convert.ToInt16(CurrentSession.UserName);
			//model.AssemblyID = AssemId;
			//model.SessionID = SessId;
			//model1.ModuleId = Convert.ToInt16(CurrentSession.MenuId);
			model1.ModuleId = ModuleId;
			if (CurrentSession.UserID != null && CurrentSession.UserID != "")
			{
				model1.UserID = new Guid(CurrentSession.UserID);
			}

			if (model1.ModuleId == 13)
			{

				if (PaperType == "Pending By Department" || PaperType == "विभाग द्वारा अपूर्ण")
				{
					return RedirectToAction("PendingStaredQuestion");
				}
				else if (PaperType == "Sent By Department" || PaperType == "विभाग द्वारा भेजे गए")
				{
					return RedirectToAction("SubmittedStaredQuestion");
				}


			}

			else if (model1.ModuleId == 14)
			{

				if (PaperType == "Pending By Department" || PaperType == "विभाग द्वारा अपूर्ण")
				{
					return RedirectToAction("PendingUnstaredQuestion");

				}
				else if (PaperType == "Sent By Department" || PaperType == "विभाग द्वारा भेजे गए")
				{
					return RedirectToAction("SubmittedUnstaredQuestion");
				}
				else if (PaperType == "Upcoming Lob")
				{
					return RedirectToAction("UnstarredUpcomingLOB");
				}
				else if (PaperType == "Laid In The House")
				{
					return RedirectToAction("LaidInHouseUnstared");
				}
			}
			else if (model1.ModuleId == 15)
			{
				if (PaperType == "Pending By Department" || PaperType == "विभाग द्वारा अपूर्ण")
				{
					return PartialView("_GetNoticeInBoxList", model1);

				}
				else if (PaperType == "Sent By Department" || PaperType == "विभाग द्वारा भेजे गए")
				{
					return PartialView("_GetNoticeReplySentList", model1);


				}
				else if (PaperType == "Upcoming Lob")
				{
					return RedirectToAction("UnstarredUpcomingLOB");
				}
				else if (PaperType == "Laid In The House")
				{
					return RedirectToAction("LaidInHouseUnstared");
				}

			}
			else if (model1.ModuleId == 9)
			{
				if (PaperType == "Pending By Department" || PaperType == "विभाग द्वारा अपूर्ण")
				{
					return PartialView("_GetDraftBills", model1);

				}
				else if (PaperType == "Sent By Department" || PaperType == "विभाग द्वारा भेजे गए")
				{
					return PartialView("_SentBills", model1);
				}
				else if (PaperType == "Upcoming Lob")
				{
					return PartialView("_UpcomingLob", model1);
				}
				else if (PaperType == "Laid In The House")
				{
					return PartialView("_GetBillLIHList", model1);
				}
				else if (PaperType == "Pending To Lay")
				{
					return PartialView("_GetBillPLIHLists", model1);
				}

			}
			else if (model1.ModuleId == 17)
			{
				if (PaperType == "Pending By Department" || PaperType == "विभाग द्वारा अपूर्ण")
				{
					return PartialView("_GetOtherPaperLaidPendings", model1);

				}
				else if (PaperType == "Sent By Department" || PaperType == "विभाग द्वारा भेजे गए")
				{
					return PartialView("_GetOtherPaperLaidSubmitList", model1);

				}
				else if (PaperType == "Upcoming Lob")
				{
					return PartialView("_GetUpComingOtherPaperLaid", model1);

				}
				else if (PaperType == "Laid In The House")
				{
					return PartialView("_OtherPaperLaidInHouse", model1);

				}
				else if (PaperType == "Pending To Lay")
				{
					return PartialView("_OtherPaperPendingToLay", model1);

				}

			}
			else if (model1.ModuleId == 12)
			{
				if (PaperType == "Pending Request" || PaperType == "लंबित अनुरोध")
				{
					return PartialView("_GetPendingListUserAccess", model1);
				}
				else if (PaperType == "Accepted Request" || PaperType == "स्वीकृत अनुरोध")
				{

					return PartialView("_GetAcceptedListUserAccess", model1);
				}
				else if (PaperType == "Rejected Request" || PaperType == "अस्वीकृत अनुरोध")
				{

					return PartialView("_GetRejectedListUserAccess", model1);

				}

			}
			else if (model1.ModuleId == 19)
			{

				if (PaperType == "Reimbursement Status")
				{

					return PartialView("_ReimbursementBills", model1);
				}



			}
			else if (model1.ModuleId == 64)
			{
				if (PaperType == "eFile")
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodeFile.cshtml");
				}
				else if (PaperType == "Receive Paper")
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");
				}
				else if (PaperType == "Send Paper")
				{
					return PartialView("/Areas/eFile/Views/eFile/_MethodSend.cshtml");
				}
			}
          //  if (model.ModuleId == 83)
            //{
                else if (PaperType == "Counsellor Diary")
                {
                   // return PartialView("/Areas/PaperLaidMinister/Views/PaperLaidMinister/_MlaDiaryForCM.cshtml");
                    ViewBag.PageSize = 25;
                    ViewBag.CurrentPage = 1;
                    MlaDiary model = new MlaDiary();
                    if (CurrentSession.MemberCode != "")
                    {
                        model.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
                    }
                    else
                    {
                        model.MlaCode = 0;
                    }
                    model.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                    model.DocmentType = "1"; //Development
                    model.ActionCode = 0; //Development
                    model.UserDeptId = CurrentSession.DeptID;
                    model.UserofcId = CurrentSession.OfficeId;
                    model.CreatedBy = CurrentSession.UserID;
                    if (CurrentSession.SubDivisionId != "")
                    {
                        model.SubDivisionId = Convert.ToInt16(CurrentSession.SubDivisionId);
                    }
                    else
                    {
                        model.SubDivisionId = 0;
                    }
                    model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    model.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
                    model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryList", model);
                    ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCount", model) as string;
                    ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCount", model) as string;
                    ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCount", model) as string;
                    ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCount", model) as string;
                    var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                    var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                    model.DocumentTypelist = DocumentTypelist;
                    model.ActionTypeList = ActionTypelist;
                    model.FinanCialYearList = GetFinancialYearList();

                    model.PaperEntryType = "Counsellor Diary";
                    ViewBag.DocType = "1";
                    ViewBag.ActionType = "0";
                    ViewBag.PendencySince = "0";
                    return PartialView("_MlaDiaryListForCM", model);
                }
            else if (PaperType == "Offices/Institutions")
            {
               // return PartialView("_PMISReportForCM");
            //    return PartialView("/Areas/PaperLaidMinister/Views/PaperLaidMinister/_list2ForCM.cshtml", report.List2);

                return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_pmisReports.cshtml");
               
            }
            else if (PaperType == "Ward and Office")
            {
                try
                {
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }

                    var GetData1 = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);

                    SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

                    model.mDepartment = GetData1;

                    return PartialView("_GetAllDepartmentOfficeForCM", model);
                }
                catch (Exception ex)
                {
                    RecordError(ex, "Error on Data Fetching");
                    ViewBag.ErrorMessage = "Error on Data Fetching";
                    return RedirectToAction("AdminErrorPage");
                }
               // return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/Index.cshtml");

            }
            else if (PaperType == "Ward and Panchayat")
            {
                try
                {
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                  //  int Id = Convert.ToInt32(CurrentSession.UserName);
                    int Id = Convert.ToInt32(CurrentSession.UserName);
                    string[] str = new string[2];
                    str[0] = Id.ToString();
                    str[1] = CurrentSession.AssemblyId;
                    var GetData1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetDistricts", str);

                    var DName1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetDName", str);

                    //  var model = Gallery.ToViewModel();
                    // return View(model);
                    // var model = GetData;
                    PanchayatConstituencyModal model = new PanchayatConstituencyModal();

                    model.ShowData = GetData1;
                    model.DNamePanchayat = DName1;

                    return PartialView("_GetAllDisrictPanchayatForCM", model);
                }
                catch (Exception ex)
                {
                    RecordError(ex, "Create Gallery Category");
                    ViewBag.ErrorMessage = "There was an Error While Create the Gallery Categroy";
                    return RedirectToAction("AdminErrorPage");
                }
               // return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/Index.cshtml");

            }
            else if (PaperType == "View/Update")  //  Public Grievances
            {
               // return PartialView("_GetGrievanceListForCM.cshtml");
                // return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_GetGrievanceList.cshtml");
                int pageId = 1;
                int pageSize = 20;
                try
                {
                    int Status = SBL.DomainModel.Models.Enums.GrievanceStatus.Pending.GetHashCode();
                    string MemberCode = CurrentSession.MemberCode;
                    var GrievanceList = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).OrderByDescending(m => m.SubmittedDate).ToList();

                    ViewBag.PageSize = pageSize;
                    List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
                    if (pageId == 1)
                    {
                        pagedRecord = GrievanceList.Take(pageSize).ToList();
                    }
                    else
                    {
                        int r = (pageId - 1) * pageSize;
                        pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
                    }

                    ViewBag.CurrentPage = pageId;
                    ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


                    if (TempData["Msg"] != null)
                        ViewBag.Msg = TempData["Msg"].ToString();

                    ViewBag.GrievanceCount = GrievanceList.Count;

                    // return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceIndexForCM", pagedRecord);
                    return PartialView("_GrievanceIndexForCM", pagedRecord);
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = "Their is a Error While Getting Grievances List";
                    return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                    throw ex;
                }
            }

            //}

			return null;
		}
        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "galleryError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {
                }

                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }

        public ActionResult NewMlaCMEntryForm()
        {
            MlaDiary model = new MlaDiary();

            model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            model.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
            var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
            var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
            var a = CurrentSession.SPMinId;
            model.memMinList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;  //GetDepartmentByMinistery
            model.DeptList = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment_maaped", model);   //   GetAllDepartment
            model.Districtist = (List<SBL.DomainModel.Models.District.DistrictModel>)Helper.ExecuteService("District", "GetAllDistrict", null);
            string[] str = new string[3];
            str[0] = CurrentSession.AadharId;
            str[1] = Convert.ToString(model.SubDivisionId);
            str[2] = Convert.ToString(model.MlaCode);
            string[] strOutput = (string[])Helper.ExecuteService("Diary", "GetDistrict_ByAAdharID", str);
            if (strOutput != null)
            {
                model.distcd = Convert.ToInt16(strOutput[0]);

            }
            //model.SubDivisionList = (List<SBL.DomainModel.Models.SubDivision.mSubDivision>)Helper.ExecuteService("Diary", "Get_Subdivision_ByDistrictndCon", null);
            // model.DeptId = CurrentSession.DeptID;
            //model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOffice", null);
            model.DocumentTypelist = DocumentTypelist;
            model.ActionTypeList = ActionTypelist;
            model.FinanCialYearList = GetFinancialYearList();
            model.PaperEntryType = "NewEntry";
            model.BtnCaption = "Save";
            model.UserDeptId = CurrentSession.DeptID;
            model.UserofcId = CurrentSession.OfficeId;
            return PartialView("_MlaDiaryEntryForCM", model);

        }
        public ActionResult GetList_MlaCMForConstituency()
        {
            var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
            var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
            MlaDiary model1 = new MlaDiary();
            model1.DocmentType = "1";
            model1.ActionCode = 0;
            model1.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            model1.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
            model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryList", model1);

            ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCount", model1) as string;
            ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCount", model1) as string;
            ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCount", model1) as string;
            ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCount", model1) as string;
            model1.FinanCialYearList = GetFinancialYearList();
            model1.DocumentTypelist = DocumentTypelist;
            model1.ActionTypeList = ActionTypelist;
            model1.PaperEntryType = "Counsellor Diary";
            ViewBag.DocType = "1";
            ViewBag.ActionType = "0";
            ViewBag.PendencySince = "0";
            return PartialView("_MlaDiaryListForCM", model1);
        }
        public FileStreamResult GetDiaryPrint(string DocType, int ActionType, int PendencySince)  //GenerateDiaryPrint
        {
            string msg = string.Empty;
            MlaDiary model = new MlaDiary();
            model.PaperEntryType = "Counsellor Diary";
            model.DocmentType = DocType;
            model.ActionCode = ActionType;
            model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            model.PendencySince = PendencySince;
            model.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);

            // SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("in");
            mTypeOfDocument md1 = new mTypeOfDocument();
            md1.DocumentTypeId = Convert.ToInt16(DocType);
            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchDataByType", model);
            //   var documenttypename = Helper.ExecuteService("Diary", "Get_DocumentTypeName", md1.DocumentTypeId);
            md1.DocumentTypeName = (string)Helper.ExecuteService("Diary", "Get_DocumentTypeName", md1.DocumentTypeId);
            var documenttypename = md1.DocumentTypeName;
            MemoryStream output = new MemoryStream();


            int crntyear = DateTime.Now.Year;
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";


            //if (diary.DiaryLanguage == 1)
            // {
            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
            outXml += @"<style>table {border: solid 1px black; }table, th{border-left:1px solid black;font-size:18px;}table, td{border-top:solid 1px black;border-left:solid 1px black;font-size:18px;}</style>";
            outXml += @"<center><h3>" + "Diary Data" + "</h3></center>";
            outXml += @"<left><h3>Document Type : " + documenttypename + "</h3></left>";
            outXml += @"<table style='width:100%;font-size:18px;margin-left: 45px;'>";
            outXml += @"<thead>";
            outXml += @"<tr>";

            outXml += @"<tr><th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'> DiaryNumber</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>To</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Subject</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>From</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Forward On</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Action On</th>";
            //outXml += "<tr><th style='margin-top:20px'></th>"; 
            outXml += @"</tr>  </thead></tbody><br>";
            //  outXml += @"</tr></tbody><br>";
            outXml += "";
            if (model.MlaDiaryList.Count > 0)
            {
                foreach (MlaDiary diary in model.MlaDiaryList)
                {

                    string frwDate = "";
                    string ActionTknDate = "";
                    // isCCItemExist = diary.DocumentCC;
                    if (diary.ForwardDate != null)
                    { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                    if (diary.ActiontakenDate != null)
                    { frwDate = Convert.ToDateTime(diary.ActiontakenDate).ToString("dd.MM.yyyy"); }
                    //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                    outXml += @"<tr><td style='text-align:center;'>" + diary.RecordNumber + "</td>";
                    outXml += @"<td  style='text-align:center;'>" + diary.DocumentTo + "</td>";
                    outXml += @"<td  style='text-align:center;'>" + diary.Subject + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + diary.RecieveFrom + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + frwDate + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + ActionTknDate + "</td>";

                    outXml += "</tr>";
                    //}
                    //else if (diary.DiaryLanguage == 2)
                    //{
                    //    outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                    //    outXml += "<center><h3>" + "" + "</h3></center>";
                    //    outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                    //    outXml += "<thead>";
                    //    outXml += "<tr>";

                    //    outXml += "</tr><thead><tbody><br>";
                    //    outXml += "";


                    //    outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                    //    outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //    outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";

                    //    outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                    //    outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //    outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                    //    outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                    //    outXml += "";
                    //}

                }
            }
            else
            {
                outXml += @"<tr><td style='text-align:center;'> No Data </td>";

                outXml += "</tr>";
            }            

            outXml += @"</tbody>";

            outXml += @"</div></div></body></html>";


            string htmlStringToConvert2 = outXml;

            HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

            AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;

            string url = "/mlaDiary/" + "PrintDiaryData/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), model.RecordNumber + ".pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            //}
            // }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

		public ActionResult SearchByType(string PaperType, int AssemId, int SessId, string ModuleName)
		{
			DiaryModel Dmdl = new DiaryModel();
			Dmdl.AssemblyID = AssemId;
			Dmdl.SessionID = SessId;
			//CurrentSession.ActionIds = ActionIds;
			// Dmdl.menMemberList = (ICollection<mMember>)Helper.ExecuteService("Notice", "DDlMemberMinisterList", Dmdl);
			Dmdl.eventList = (List<mEvent>)Helper.ExecuteService("PaperLaidMinister", "DDlEvent", null);


			//Dmdl = (DiaryModel)Helper.ExecuteService("Notice", "DDlMemberMinisterList", Dmdl);
			Dmdl.Subject = CurrentSession.SPSubject;
			if (!string.IsNullOrEmpty(CurrentSession.SPMinId))
			{
				Dmdl.MinistryId = Convert.ToInt16(CurrentSession.SPMinId);
			}
			Dmdl.FromDate = CurrentSession.SPFDate;
			Dmdl.ToDate = CurrentSession.SPTDate;
			// Dmdl.DiaryNumber = CurrentSession.SPDNum;
			Dmdl.PaperEntryType = PaperType;
			Dmdl.ModuleName = ModuleName;

			return PartialView("_SearchByData", Dmdl);
		}
		public ActionResult SearchDataByType(string PaperType, string Number, string Subject, string ModuleName, int? QNum, int? Name)
		{
			tPaperLaidV Dmdl = new tPaperLaidV();
			// DiaryModel Dmdl = new DiaryModel();
			OnlineMemberQmodel model = new OnlineMemberQmodel();
			// Dmdl.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
			Dmdl.PaperEntryType = PaperType;
			Dmdl.Subject = Subject;
			// Dmdl.MinistryId = MinId;
			Dmdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
			Dmdl.AssemblyId = Convert.ToInt16(CurrentSession.SessionId);
			Dmdl.NoticeNumber = Number;
			Dmdl.BussinessType = Number;
			Dmdl.QuestionNumber = QNum;
			Dmdl.MinID = Name;
			Dmdl.BType1 = Number;

			Dmdl.DiaryNumber = Number;
			if (ModuleName == "Starred Questions")
			{
				if (PaperType == "Pending By Department")
				{
					return PartialView("_PendingStaredQuestion", Dmdl);
				}
				if (PaperType == "Sent By Department")
				{
					return PartialView("_SubmittedStaredQuestion", Dmdl);
				}
			}
			else if (ModuleName == "UnStarred Questions")
			{
				if (PaperType == "Pending By Department")
				{

					return PartialView("_PendingUnstaredQuestion", Dmdl);
				}
				if (PaperType == "Sent By Department")
				{
					return PartialView("_SubmittedUnstaredQuestion", Dmdl);

				}

			}
			else if (ModuleName == "Notices")
			{
				if (PaperType == "Pending By Department")
				{

					return PartialView("_GetNoticeInBoxList", Dmdl);
				}
				if (PaperType == "Sent By Department")
				{
					return PartialView("_GetNoticeReplySentList", Dmdl);
				}

			}
			else if (ModuleName == "Bills")
			{
				if (PaperType == "Pending By Department")
				{
					return PartialView("_GetDraftBills", Dmdl);


				}
				else if (PaperType == "Sent By Department")
				{
					return PartialView("_SentBills", Dmdl);

				}
				else if (PaperType == "Upcoming Lob")
				{
					// return PartialView("_SentBills",Dmdl);

				}

				else if (PaperType == "Upcoming Lob")
				{
					// return PartialView("_SentBills",Dmdl);

				}

			}

			else if (ModuleName == "Other Papers")
			{
				if (PaperType == "Draft of other Papers")
				{
					return PartialView("_GetOtherPaperLaidPendings", Dmdl);

				}
				else if (PaperType == "Other Papers Sent")
				{
					return PartialView("_GetOtherPaperLaidSubmitList", Dmdl);

				}
				else if (PaperType == "Upcoming Lob")
				{
					return PartialView("_GetUpComingOtherPaperLaid", Dmdl);

				}
				else if (PaperType == "Laid In The House")
				{
					return PartialView("_OtherPaperLaidInHouse", Dmdl);

				}
				else if (PaperType == "Pending To Lay")
				{
					return PartialView("_OtherPaperPendingToLay", Dmdl);

				}

			}
			// model = (OnlineMemberQmodel)Helper.ExecuteService("PaperLaidMinister", "GetSearchDataByType", Dmdl);


			return null;
		}

		#endregion
        #region   
        public PartialViewResult MlaDiarySearchPage()
        {
            ViewBag.PageSize = 25;
            ViewBag.CurrentPage = 1;
            MlaDiary model1 = new MlaDiary();
            if (CurrentSession.MemberCode != "")
            {
                model1.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
            }
            else
            {
                model1.MlaCode = 0;
            }
            model1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            model1.DocmentType = "1"; //Development
            model1.ActionCode = 0; //Development
            model1.UserDeptId = CurrentSession.DeptID;
            model1.UserofcId = CurrentSession.OfficeId;
            model1.CreatedBy = CurrentSession.UserID;
            if (CurrentSession.SubDivisionId != "")
            {
                model1.SubDivisionId = Convert.ToInt16(CurrentSession.SubDivisionId);
            }
            else
            {
                model1.SubDivisionId = 0;
            }
            model1.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            model1.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
            model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryList", model1);
            ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCount", model1) as string;
            ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCount", model1) as string;
            ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCount", model1) as string;
            ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCount", model1) as string;
            var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
            var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
            model1.DocumentTypelist = DocumentTypelist;
            model1.ActionTypeList = ActionTypelist;
            model1.FinanCialYearList = GetFinancialYearList();

            model1.PaperEntryType = "Counsellor Diary";
            ViewBag.DocType = "1";
            ViewBag.ActionType = "0";
            ViewBag.PendencySince = "0";
            return PartialView("_MlaDiaryListForCM", model1);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveUpdateMlaDiary(MlaDiary model, HttpPostedFileBase file)
        {


            if (CurrentSession.MemberCode == "" || CurrentSession.MemberCode == null)
            {
                CurrentSession.MemberCode = "0";
            }
            model.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
            //  model.RecordNumber = Convert.ToInt32(1);

            //  model.BtnCaption = "Save";
            if (CurrentSession.AssemblyId == "" || CurrentSession.AssemblyId == null)
            {
                CurrentSession.AssemblyId = "0";
            }
            model.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);


            //if (CurrentSession.ConstituencyID == "" || CurrentSession.ConstituencyID == null)
            //{
            //    CurrentSession.ConstituencyID = "0";
            //}

            int res = 0;
            //  model.ConstituencyCode = (SBL.DomainModel.Models.Diaries.DiaryModel(Helper.ExecuteService("Diary", "GetConstByMemberId", null) as string));
            DiaryModel dobj = new DiaryModel();
            dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
            model.ConstituencyCode = dobj.ConstituencyCode;
            if (model.BtnCaption == "Save")
            {
                model.RecordNumber = Convert.ToInt32(GetRecordNumber_mlaDiary(model.DocmentType));
            }
            if (model.ForwardDateForUse != null && model.ForwardDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.ForwardDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.ForwardDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.ForwardDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                model.ForwardDate = dtDate;
            }
            if (model.EnclosureDateForUse != null && model.EnclosureDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.EnclosureDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.EnclosureDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.EnclosureDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                model.EnclosureDate = dtDate;
            }
            if (model.ActiontakenDateForUse != null && model.ActiontakenDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.ActiontakenDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.ActiontakenDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.ActiontakenDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                model.ActiontakenDate = dtDate;
            }
            model.CreatedBy = CurrentSession.UserID;

            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwardin");
            //  model.ForwardDate = DateTime.ParseExact(model.ForwardDateForUse, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwarddatget");
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            model.ForwardedFile = null;
            //if (model.IsForwardFile == "Y")
            //{
            //    model.ForwardedFile = model.ForwardedFile;
            //}
            //  else
            // {

            // Save Forward File

            //string urlforward = "/MlaDiary/ForwardFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
            //string directoryf = FileSettings.SettingValue + urlforward;

            //if (!System.IO.Directory.Exists(directoryf))
            //{
            //    System.IO.Directory.CreateDirectory(directoryf);
            //}
            //string tempurlforwrd = "~/MlaDiary/TempFile";
            //string Tempdirectoryforwrd = Server.MapPath(tempurlforwrd);


            //if (Directory.Exists(directoryf) && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
            //{

            //    string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlforwrd));
            //    if (savedFileName.Length > 0 && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
            //    {
            //        string SourceFile = savedFileName[0];
            //        foreach (string page in savedFileName)
            //        {
            //            // Guid FileName = Guid.NewGuid();
            //            string name = Path.GetFileName(page);
            //            string ext = Path.GetExtension(SourceFile);
            //            string path = System.IO.Path.Combine(directoryf, model.RecordNumber.ToString() + ext);

            //            if (!string.IsNullOrEmpty(model.RecordNumber.ToString()))
            //            {

            //                System.IO.File.Copy(SourceFile, path, true);

            //                model.ForwardedFile = urlforward + model.RecordNumber.ToString() + ext;
            //            }

            //        }


            //    }
            //    else
            //    {
            //        model.ForwardedFile = null;
            //        TempData["Msg"] = "Please select Forward File";

            //    }
            //}
            //     else
            //    {
            //        model.ForwardedFile = null;
            //        TempData["Msg"] = "Please select Forward File";

            //    }


            //if (Directory.Exists(Tempdirectoryforwrd))  //tempurlforwrd
            //{
            //    string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlforwrd));
            //    foreach (string filePath in filePaths)
            //    {
            //        System.IO.File.Delete(filePath);
            //    }
            //}

            // }

            //upload action file 
            if (model.IsFile == "Y")
            {
                model.ActiontakenFile = model.ActiontakenFile;
            }
            else
            {
                //string urlA = "/MlaDiary/ActionFile";
                string urlA = "/MlaDiary/ActionFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
                string directoryA = FileSettings.SettingValue + urlA;

                if (!System.IO.Directory.Exists(directoryA))
                {
                    System.IO.Directory.CreateDirectory(directoryA);
                }
                // int fileID = GetFileRandomNo();

                string tempurlA1 = "~/MlaDiary/ActionTempFile";
                string tempdirectoryA1 = Server.MapPath(tempurlA1);

                //Save ActionTaken File


                if (Directory.Exists(directoryA) && (model.ActiontakenDate != null))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlA1));
                    if (savedFileName.Length > 0 && (model.ActiontakenDate != null))   //&& model.ActiontakenDate != null && model.ActiontakenDate != ""
                    {
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {

                            // string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));

                            string ext = Path.GetExtension(SourceFile);
                            string name = Path.GetFileName(page);
                            string path1 = System.IO.Path.Combine(directoryA, model.RecordNumber.ToString() + ext);
                            if (!string.IsNullOrEmpty(name))
                            {
                                //  System.IO.File.Delete(System.IO.Path.Combine(directoryA, model.ActiontakenFile));
                                System.IO.File.Copy(SourceFile, path1, true);
                                model.ActiontakenFile = urlA + model.RecordNumber.ToString() + ext;

                            }
                        }


                    }
                    else
                    {
                        model.ActiontakenFile = null;
                        //   return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                        //  TempData["Msg"] = "Please select Action Taken file";
                        // return RedirectToAction("Index");
                    }
                }
                else
                {
                    model.ActiontakenFile = null;
                }
                if (Directory.Exists(tempdirectoryA1))   //tempurlA1
                {
                    string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlA1));
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

            }
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("1");
            //upload enclosure file 
            if (model.IsEnclosureFile == "Y")
            {
                model.EnclosureFile = model.EnclosureFile;
            }
            else
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("2");
                string urlEnclosure = "/MlaDiary/EnclosureFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
                //string urlEnclosure = "/MlaDiary/EnclosureFile";
                string directoryEnclosure = FileSettings.SettingValue + urlEnclosure;

                if (!System.IO.Directory.Exists(directoryEnclosure))
                {
                    System.IO.Directory.CreateDirectory(directoryEnclosure);
                }
                // int fileIDEnclosure = GetFileRandomNo();

                string TempurlEnclosure1 = "~/MlaDiary/TempEnclosureFile";
                string directoryEnclosure1 = Server.MapPath(TempurlEnclosure1);



                if (Directory.Exists(directoryEnclosure))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
                    if (savedFileName.Length > 0 && (model.EnclosureDate != null))
                    {
                        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("3");
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {
                            // Guid FileName = Guid.NewGuid();
                            string ext = Path.GetExtension(SourceFile);
                            string name = Path.GetFileName(page);
                            string path1 = System.IO.Path.Combine(directoryEnclosure, model.RecordNumber.ToString() + ext);
                            if (!string.IsNullOrEmpty(name))
                            {
                                //  System.IO.File.Delete(System.IO.Path.Combine(directoryEnclosure, model.EnclosureFile));
                                System.IO.File.Copy(SourceFile, path1, true);
                                model.EnclosureFile = urlEnclosure + model.RecordNumber.ToString() + ext;
                            }
                        }
                    }
                    else
                    {
                        model.EnclosureFile = null;
                        //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    model.EnclosureFile = null;
                    //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                }
                if (Directory.Exists(directoryEnclosure1))  //TempurlEnclosure1
                {

                    string[] filePaths = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
            }

            //Delete the temp files if exists

            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("4");
            res = (int)Helper.ExecuteService("Diary", "SaveMlaDiary", model);
            if (res == 0)
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
              //  model.en
               // return PartialView("_GetActionView",model);
                return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
            }
            else
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("6");
               // return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
               // return PartialView("_GetActionView", model);
               // return RedirectPermanent("/PaperLaidMinister/PaperLaidMinister/Index");
                return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
            }


            //if (res != 0 &&  res != -1 ||(model.BtnCaption=="Update"))
            //{
            //    if (model.BtnCaption == "Update")
            //    {
            //    }
            //    else
            //    {
            //        model.RecordNumber = res;
            //    }

            //    int reslt = (int)Helper.ExecuteService("Diary", "SaveMlaDiary", model);
            //    if (reslt == 0)
            //    {
            //        return RedirectToAction("Index");
            //    }
            //    else                
            //    {
            //        return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            //    }
            //}
            //else
            //{
            //    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            //}


            // return RedirectToAction("Index");
        }
        public int GetRecordNumber_mlaDiary(string DocmentType)
        {
            MlaDiary mdl = new MlaDiary();
            mdl.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            mdl.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
            mdl.DocmentType = DocmentType;
            mdl.RecordNumber = (int)Helper.ExecuteService("Diary", "Chk_RecordNumber", mdl);
            if (mdl.RecordNumber == 0)
            {
                mdl.RecordNumber = 1;
            }
            else
            {
                mdl.RecordNumber = mdl.RecordNumber + 1;
            }
            int re = Convert.ToInt32(mdl.RecordNumber);
            return re;
        }
        public ActionResult SearchMLADiary_DataByType(string PaperType, string DocType, int ActionType, int PendencySince, string FinancialYear)
        {
            ViewBag.PageSize = 25;
            ViewBag.CurrentPage = 1;
            MlaDiary Dmdl = new MlaDiary();

            // Dmdl.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            Dmdl.PaperEntryType = PaperType;
            Dmdl.DocmentType = DocType;
            Dmdl.ActionCode = ActionType;
            Dmdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.PendencySince = PendencySince;
            Dmdl.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
            Dmdl.FinancialYear = FinancialYear;
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("in");

            if (Dmdl.DocmentType != "3")
            {
                Dmdl.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchDataByType", Dmdl);
            }

            else if (Dmdl.DocmentType == "3")
            {
                Dmdl.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchData_ForDoctype3", Dmdl);
                //ViewBag.TotalSanctionMoney = Helper.ExecuteService("Diary", "GetMlaDiary_AmountCount_ForDoctype3", Dmdl) as string;
                ViewBag.TotalDiaryAmountCount = Helper.ExecuteService("Diary", "Get_TotalDiaryAmountCount", Dmdl) as string;
                ViewBag.pendingDiaryAmountCount = Helper.ExecuteService("Diary", "pendingDiaryAmountCount", Dmdl) as string;
                ViewBag.forwardActionPendingAmountCount = Helper.ExecuteService("Diary", "Get_forwardActionPendingAmountCount", Dmdl) as string;
                ViewBag.forwardActionDoneAmountCount = Helper.ExecuteService("Diary", "Get_forwardActionDoneAmountCount", Dmdl) as string;

            }
            ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCount", Dmdl) as string;
            if (Dmdl.ActionCode == 0)
            {
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCount", Dmdl) as string;
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCount", Dmdl) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCount", Dmdl) as string;
            }
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("Out");

            var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
            var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);

            Dmdl.DocumentTypelist = DocumentTypelist;
            Dmdl.ActionTypeList = ActionTypelist;
            Dmdl.PaperEntryType = PaperType;
            Dmdl.PendencySince = PendencySince;
            ViewBag.DocType = DocType.ToString();
            Dmdl.FinanCialYearList = GetFinancialYearList();
            ViewBag.ActionType = ActionType.ToString();
            ViewBag.PendencySince = PendencySince;

            return PartialView("_MlaDiaryListFORCM", Dmdl);
        }



        public ActionResult EditMlaDairyForm(int ID)
        {
            try
            {
                MlaDiary model = new MlaDiary();
                model = (MlaDiary)Helper.ExecuteService("Diary", "GetMlaDairyRecord", ID);
                if (model.ForwardDate != null)
                {
                    model.ForwardDateForUse = Convert.ToDateTime(model.ForwardDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.ForwardDateForUse = "";
                }
                if (model.ActiontakenDate != null)
                {
                    model.ActiontakenDateForUse = Convert.ToDateTime(model.ActiontakenDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.ActiontakenDateForUse = "";
                }
                if (model.EnclosureDate != null)
                {
                    model.EnclosureDateForUse = Convert.ToDateTime(model.EnclosureDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.EnclosureDateForUse = "";
                }
                var discd = Helper.ExecuteService("Diary", "GetDisCode", model);
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                if (discd != null || discd != "")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                {
                    model.distcd = Convert.ToInt16(discd);
                }
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                var a = CurrentSession.SPMinId;
                model.memMinList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;  //GetDepartmentByMinistery
                model.DeptList = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment_maaped", model);  //DepartmentListCol
                model.Districtist = (List<SBL.DomainModel.Models.District.DistrictModel>)Helper.ExecuteService("District", "GetAllDistrict", null);
                //model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOffice", null);
                model.DocumentTypelist = DocumentTypelist;
                model.ActionTypeList = ActionTypelist;
                model.FinanCialYearList = GetFinancialYearList();
                model.PaperEntryType = "EditEntry";
                model.BtnCaption = "Update";
                return PartialView("_MlaDiaryEntryForCM", model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DeleteMlaDairyForm(int ID)
        {
            try
            {
                OnlineMemberQmodel model = new OnlineMemberQmodel();
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                ViewBag.PageSize = 25;
                ViewBag.CurrentPage = 1;
                MlaDiary model1 = new MlaDiary();
                Helper.ExecuteService("Diary", "DeleteMlaDairyRecord", ID);
                ///  for search ui

                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);

                model1.DocmentType = "1";
                model1.ActionCode = 0;
                model1.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                model1.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
                model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryList", model1);

                ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCount", model1) as string;
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCount", model1) as string;
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCount", model1) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCount", model1) as string;
                model1.FinanCialYearList = GetFinancialYearList();
                model1.DocumentTypelist = DocumentTypelist;
                model1.ActionTypeList = ActionTypelist;
                model1.PaperEntryType = "Counsellor Diary";
                ViewBag.DocType = "1";
                ViewBag.ActionType = "0";
                ViewBag.PendencySince = "0";

                return PartialView("_MlaDiaryListForCM", model1);
                //return Content("Deleted");
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SelectListItem> GetFinancialYearList()
        {
            var methodparameter = new List<KeyValuePair<string, string>>();
            int year = 0;
            int year1 = 0;
            year = System.DateTime.Today.Year - 1;
            year1 = year + 1;
            string Year2 = year + "-" + year1;
            int i = 1;


            List<SelectListItem> YearList = new List<SelectListItem>();
            //YearList.Add(new SelectListItem { Text = "Select", Value = "0" });
            while (year < System.DateTime.Today.Year + 1)
            {
                YearList.Add(new SelectListItem { Text = Year2, Value = Year2 });

                year = year + 1;
                year1 = year + 1;
                Year2 = year + "-" + year1;
                i = i + 1;
            }
            return YearList;
        }
        public FileStreamResult PrintDiaryPdf(int DiaryId)
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();

            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            if (model.MlaDiaryList.Count > 0)
            {
                //mMember member = new mMember();
                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
              //  obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
               // if (obj != null)
               // {
                    //PdfConverter pdfConverter = new PdfConverter();
                    //  pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    int crntyear = DateTime.Now.Year;
                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);
                    //  document1.Templates.AddNewTemplate(
                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.Legal, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                    foreach (MlaDiary diary in model.MlaDiaryList)
                    {

                        string frwDate = "";
                        isCCItemExist = diary.DocumentCC;
                        if (diary.ForwardDate != null)
                        { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                        //if (diary.DiaryLanguage == 1)
                        //{
                        //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                        outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
                        outXml += @"<center><h3>" + "" + "</h3></center>";
                        outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                        outXml += @"<thead>";
                        outXml += @"<tr>";
                        // outXml += @"<th><br></th></tr>";
                        //outXml += @"<tr><th style='margin-top:30px'></th>";
                        outXml += @"</tr><thead><tbody><br><br><br><br><br><br><br><br>";
                        outXml += "";

                        outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                        //outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                        //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                        //outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                        //outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                        //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";

                        //outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";

                        //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
                        outXml += "";
                        // }
                        //else if (diary.DiaryLanguage == 2)
                        //{
                        //    outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                        //    outXml += "<center><h3>" + "" + "</h3></center>";
                        //    outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                        //    outXml += "<thead>";
                        //    outXml += "<tr>";
                        //    //outXml += "<th><br></th></tr>";
                        //    //outXml += "<tr><th style='margin-top:30px'></th>";
                        //    outXml += "</tr><thead><tbody><br><br><br><br><br><br>";
                        //    outXml += "";

                        //    //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                        //    outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                        //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                        //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";

                        //    //outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                        //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                        //    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                        //    //outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";

                        //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                        //    outXml += "";
                        //}

                    }

                    outXml += @"</tbody>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert2 = outXml;

                    HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                    AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/mlaDiary/" + "DiaryPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();
               // }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }
        #endregion

        #region PDFGenerate
        public ActionResult DisplayQuestionPDF(int id)
		{
			tPaperLaidTemp temp = new tPaperLaidTemp();
			temp.PaperLaidId = id;
			var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

			string Url = FileSettings.SettingValue;

			var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

			string FilePath = FileName.FilePath + FileName.FileName;

			string filephyacesSetting = Url + FilePath;
			temp.FilePath = filephyacesSetting;

			return View("testPDF", temp);

			//try
			//{
			//    Stream stream = null;

			//    int bytesToRead = 10000;
			//    byte[] buffer = new Byte[bytesToRead];
			//    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filephyacesSetting);
			//    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

			//    if (fileReq.ContentLength > 0)
			//        fileResp.ContentLength = fileReq.ContentLength;

			//    stream = fileResp.GetResponseStream();
			//    return File(stream, "application/pdf");
			//}
			//catch (Exception e)
			//{
			//    return View("FileNotFound");
			//}
		}

		public ActionResult DisplayQuestionDoc(int id)
		{
			tPaperLaidTemp temp = new tPaperLaidTemp();
			temp.PaperLaidId = id;
			var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

			string Url = FileSettings.SettingValue;

			var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

			string FilePath = FileName.FilePath + FileName.DocFileName;


			//string filephyacesSetting = _siteRepository.FileAccessingUrlPathLocal().SettingValue;
			//string pp = _LOBRepository.GetSQuestionFromID(Convert.ToInt16(id));
			string filephyacesSetting = Url + FilePath;
			temp.FilePath = filephyacesSetting;
			//try
			//{
			//    Stream stream = null;

			//    int bytesToRead = 10000;
			//    byte[] buffer = new Byte[bytesToRead];
			//    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filephyacesSetting);
			//    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

			//    if (fileReq.ContentLength > 0)
			//        fileResp.ContentLength = fileReq.ContentLength;

			//    stream = fileResp.GetResponseStream();
			//    return File(stream, "application/msword");
			//}
			//catch (Exception e)
			//{
			//    return View("FileNotFound");
			//}
			return View("testPDF", temp);
		}

		public ActionResult DisplayQuestionSupPDF(int id)
		{
			tPaperLaidTemp temp = new tPaperLaidTemp();
			temp.PaperLaidId = id;
			var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

			string Url = FileSettings.SettingValue;

			var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

			string FilePath = FileName.FilePath + FileName.SupFileName;


			//string filephyacesSetting = _siteRepository.FileAccessingUrlPathLocal().SettingValue;
			//string pp = _LOBRepository.GetSQuestionFromID(Convert.ToInt16(id));
			string filephyacesSetting = Url + FilePath;
			temp.FilePath = filephyacesSetting;
			//try
			//{
			//    Stream stream = null;

			//    int bytesToRead = 10000;
			//    byte[] buffer = new Byte[bytesToRead];
			//    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filephyacesSetting);
			//    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

			//    if (fileReq.ContentLength > 0)
			//        fileResp.ContentLength = fileReq.ContentLength;

			//    stream = fileResp.GetResponseStream();
			//    return File(stream, "application/pdf");
			//}
			//catch (Exception e)
			//{
			//    return View("FileNotFound");
			//}
			return View("testPDF", temp);
		}


		public ActionResult DisplayQuestionSupDoc(int id)
		{
			tPaperLaidTemp temp = new tPaperLaidTemp();
			temp.PaperLaidId = id;
			var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

			string Url = FileSettings.SettingValue;

			var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

			string FilePath = FileName.FilePath + FileName.DocFileName;


			//string filephyacesSetting = _siteRepository.FileAccessingUrlPathLocal().SettingValue;
			//string pp = _LOBRepository.GetSQuestionFromID(Convert.ToInt16(id));
			string filephyacesSetting = Url + FilePath;
			temp.FilePath = filephyacesSetting;
			//try
			//{
			//    Stream stream = null;

			//    int bytesToRead = 10000;
			//    byte[] buffer = new Byte[bytesToRead];
			//    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filephyacesSetting);
			//    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

			//    if (fileReq.ContentLength > 0)
			//        fileResp.ContentLength = fileReq.ContentLength;

			//    stream = fileResp.GetResponseStream();
			//    return File(stream, "application/msword");
			//}
			//catch (Exception e)
			//{
			//    return View("FileNotFound");
			//}
			return View("testPDF", temp);
		}


		public ActionResult DisplayQuestionSignedPDF(int id)
		{
			tPaperLaidTemp temp = new tPaperLaidTemp();
			temp.PaperLaidId = id;
			var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

			string Url = FileSettings.SettingValue;

			var FileName = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "PaperLaidFilePath", temp);

			string FilePath = FileName.SignedFilePath;


			//string filephyacesSetting = _siteRepository.FileAccessingUrlPathLocal().SettingValue;
			//string pp = _LOBRepository.GetSQuestionFromID(Convert.ToInt16(id));
			string filephyacesSetting = Url + FilePath;
			temp.FilePath = filephyacesSetting;
			//try
			//{
			//    Stream stream = null;

			//    int bytesToRead = 10000;
			//    byte[] buffer = new Byte[bytesToRead];
			//    HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(filephyacesSetting);
			//    HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

			//    if (fileReq.ContentLength > 0)
			//        fileResp.ContentLength = fileReq.ContentLength;

			//    stream = fileResp.GetResponseStream();
			//    return File(stream, "application/pdf");
			//}
			//catch (Exception e)
			//{
			//    return View("FileNotFound");
			//}

			return View("testPDF", temp);
		}



		//public ActionResult DownloadPdf(string Wid)
		//{

		//    tOTPRegistrationAuditTrial obj = new tOTPRegistrationAuditTrial();
		//    obj.MobileNumber = Wid;
		//    string path = string.Empty;
		//    var NewModel1 = (tOTPRegistrationAuditTrial)Helper.ExecuteService("PaperLaid", "GetTempDocFile", obj);

		//    var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetprintingFileSetting", null);
		//    var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
		//    var Filelocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
		//    string FileStructurePath = Acess.SettingValue;

		//    string CompletePath = NewModel1.OTPRegistrationModel.FirstOrDefault().MainReplyDocPath;
		//    string url = FileStructurePath + CompletePath;
		//    path = Filelocation.SettingValue + "\\" + CompletePath;
		//    //Response.ContentType = "application/octet-stream";
		//    //Response.AppendHeader("Content-Disposition", "attachment;filename=" + NewModel.objList.FirstOrDefault().SupDocFileName);
		//    //Response.TransmitFile(path);

		//    byte[] bytes = System.IO.File.ReadAllBytes(path);

		//    return File(bytes, "application/pdf");
		//}



		public ActionResult DownloadPdf(string Wid)
		{


			string[] Val = Wid.Split(',', ',');
			string fistId = Val[0].ToString();
			string SecondType = Val[1].ToString();

			int Id = Convert.ToInt32(fistId);

			string path = string.Empty;
			string FileName = string.Empty;

			try
			{
				tPaperLaidTemp temp = new tPaperLaidTemp();


				temp.PaperLaidTempId = Id;


				var NewModel = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetFilesVersionDetials", temp);
				//var NewModel = (tPaperLaidTemp)Helper.ExecuteService("PaperLaid", "GetFilesDetials", new tPaperLaidTemp { PaperLaidId = Id });
				var NewsSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
				var Acess = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
				var Filelocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
				string FileStructurePath = Acess.SettingValue;

				if (SecondType == "MD")
				{

					string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.FirstOrDefault().DocFileName;
					string url = FileStructurePath + CompletePath;
					path = Filelocation.SettingValue + "\\" + CompletePath;
					Response.ContentType = "application/octet-stream";
					Response.AppendHeader("Content-Disposition", "attachment;filename=" + NewModel.objList.FirstOrDefault().DocFileName);
					Response.TransmitFile(path);
					Response.End();
					return new EmptyResult();
				}

				else if (SecondType == "MP")
				{
					string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.LastOrDefault().FileName;
					string url = FileStructurePath + CompletePath;
					path = Filelocation.SettingValue + "\\" + CompletePath;

				}
				else if (SecondType == "SP")
				{
					string CompletePath = NewModel.objList.FirstOrDefault().FilePath + NewModel.objList.LastOrDefault().SupFileName;
					string url = FileStructurePath + CompletePath;
					path = Filelocation.SettingValue + "\\" + CompletePath;

				}
				else if (SecondType == "SignedP")
				{
					string CompletePath = NewModel.objList.LastOrDefault().SignedFilePath;
					string url = FileStructurePath + CompletePath;
					path = Filelocation.SettingValue + "\\" + CompletePath;

				}

				byte[] bytes = System.IO.File.ReadAllBytes(path);

				return File(bytes, "application/pdf");

			}
#pragma warning disable CS0168 // The variable 'e' is declared but never used
			catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
			{

				return RedirectToAction("FileNotFound", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });

			}
			finally
			{

			}


		}

		#endregion

		public ActionResult FileNotFound()
		{

			return View();
        }
        //PMIS Office/Institutions For CM
      #region          
        //public ActionResult GetDepartmentPostForCM()
        //{
        //    PMISReport report = new Models.PMISReport();
        //    report.List1 = (List<pmisListReport>)GetAllDepartmentPost(memberCode);
        //    ViewBag.Header = "Department List";
        //    return PartialView("_List1ForCM", report.List1);

        //}
        //public ActionResult GetOfficePostForCM(string departmentID)
        //{
        //    PMISReport report = new Models.PMISReport();
        //    //   report.List1 = (List<pmisListReport>)GetAllDepartmentPost(memberCode.ToString());
        //    report.List2 = (List<pmisListReport>)GetAllOfficePost(departmentID, memberCode.ToString());
        //    ViewBag.DeptID = departmentID;
        //    ViewBag.Header = departmentID + " >> Offlice List";
        //    return PartialView("_list2ForCM", report.List2);
        //}
        //public ActionResult GetDesignationPostForCM(string departmentID, string officeID)
        //{
        //    PMISReport report = new Models.PMISReport();
        //    report.List3 = (List<pmisListReport>)GetAllDesignationPost(departmentID, officeID);
        //    ViewBag.DeptID = departmentID;
        //    ViewBag.officeID = officeID;
        //    ViewBag.Header = departmentID + " >> " + officeID + " >> Designation List";
        //    return PartialView("_list3ForCM", report.List3);
        //}
        // public ActionResult GetOfficeEmployeePostForCM(string departmentID, string officeID, string designation)
        //{
        //    PMISReport report = new Models.PMISReport();
        //    report.List4 = (List<pmisListReport>)GetAllOfficeEmployeePost(departmentID, officeID, designation);
        //    ViewBag.Header = departmentID + " >> " + officeID + " >> " + designation + " >>  Employee List";
        //    return PartialView("_list4ForCM", report.List4);
        //}
       
        #endregion

        //Mapping In case of CM
        #region
        public ActionResult GetDisricts2ForCM(string Id, int DstCode, string SCode, string Name, string CCode)
        {
            //Helper.ExecuteService("Notice", "AddPanchayat", new { Id,DstCode,SCode});
            tConstituencyPanchayat model = new tConstituencyPanchayat();
            model.Ids = Id;
            model.DistrictCode = DstCode;
            model.StateCode = Convert.ToInt32(SCode);
            model.ConstituencyCode = Convert.ToInt32(CCode);

            Helper.ExecuteService("Notice", "AddPanchayat", model);

            int Id1 = Convert.ToInt32(CurrentSession.UserName);
           
            string[] str = new string[2];
            str[0] = Id1.ToString();
            str[1] = CurrentSession.AssemblyId;
            var GetData1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetAllPanchayat", str);
            var CName1 = (mConstituency)Helper.ExecuteService("Notice", "GetCName", str);

            PanchayatConstituencyModal model1 = new PanchayatConstituencyModal();

            model1.tCPanchayat = GetData1;
            model1.CName = CName1;

            return PartialView("_GetDisricts2ForCM", model1);
        }
        public ActionResult GetAllDisrictPanchayatForCM()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                int Id = Convert.ToInt32(CurrentSession.UserName);
                string[] str = new string[2];
                str[0] = Id.ToString();
                str[1] = CurrentSession.AssemblyId;
                var GetData1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetDistricts", str);

                var DName1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetDName", str);

                //  var model = Gallery.ToViewModel();
                // return View(model);
                // var model = GetData;
                PanchayatConstituencyModal model = new PanchayatConstituencyModal();

                model.ShowData = GetData1;
                model.DNamePanchayat = DName1;

                return PartialView("_GetAllDisrictPanchayatForCM", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Create Gallery Category");
                ViewBag.ErrorMessage = "There was an Error While Create the Gallery Categroy";
                return RedirectToAction("AdminErrorPage");
            }
        }
        public ActionResult DeletePanchayatForCM(string Id)
        {
            tConstituencyPanchayat model = new tConstituencyPanchayat();
            model.Ids = Id;

            Helper.ExecuteService("Notice", "DeletePanchayat", model);
            int Id1 = Convert.ToInt32(CurrentSession.UserName);
            string[] str = new string[2];
            str[0] = Id1.ToString();
            str[1] = CurrentSession.AssemblyId;
            var GetData1 = (List<PanchayatConstituencyModal>)Helper.ExecuteService("Notice", "GetAllPanchayat", str);
            var CName1 = (mConstituency)Helper.ExecuteService("Notice", "GetCName", str);

            PanchayatConstituencyModal model1 = new PanchayatConstituencyModal();

            model1.tCPanchayat = GetData1;
            model1.CName = CName1;

            return PartialView("_DeletePanchayatForCM", model1);
        }
        #endregion
        //Office mapping for CM

        #region Office Mapping with Department ForCM

        public ActionResult GetAllDepartmentofficeForCM()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var GetData1 = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);

                SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

                model.mDepartment = GetData1;

                return PartialView("_GetAllDepartmentOfficeForCM", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Error on Data Fetching");
                ViewBag.ErrorMessage = "Error on Data Fetching";
                return RedirectToAction("AdminErrorPage");
            }
        }

        public ActionResult GetAllOfficeListByDeptIdForCM(string DeptId)
        {
            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetOfficeByDeptId", DeptId);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model.OfficeList = GetData1;

            return PartialView("_GetOfficeByDeptForCM", model);
        }

        public ActionResult GetAllDepartmentofficeMappedForCM(string DeptId)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var GetData1 = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);

                SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

                model.mDepartment = GetData1;

                ViewBag.Deptid = DeptId;
                return PartialView("_GetAllDepartmentOfficeMappedForCM", model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Error on Data Fetching");
                ViewBag.ErrorMessage = "Error on Data Fetching";
                return RedirectToAction("AdminErrorPage");
            }
        }

        public ActionResult GetAllMappedOfficeListByDeptIdForCM(string DeptId)
        {
            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model1 = new DomainModel.Models.Office.tDepartmentOfficeMapped();
            model1.DeptId = DeptId;
            model1.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetMappedOfficeByDeptId", model1);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model.OfficeList = GetData1;
            model.DepartmentId = DeptId;
            return PartialView("_GetOfficeByDeptMappedForCM", model);
        }

        public ActionResult Getoffice2ForCM(string Id, string Deptid)
        {
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            int CConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));
            //Helper.ExecuteService("Notice", "AddPanchayat", new { Id,DstCode,SCode});
            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model = new DomainModel.Models.Office.tDepartmentOfficeMapped();
            model.Ids = Id;
            model.DeptId = Deptid;
            model.AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
            model.ConstituencyCode = CConstituencyCode;// 12;// Convert.ToInt32(CurrentSession.ConstituencyID);
            model.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
          //  model.ConstituencyCode = Convert.ToInt32(CurrentSession.ConstituencyID);
            //string[] str = new string[2];
            //str[0] = CurrentSession.AssemblyId;
            //str[1] = CurrentSession.ConstituencyID;
            //string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
            //string constituencyID = "";
            //if (strOutput != null)
            //{
            //    constituencyID = Convert.ToString(strOutput[0]);
            //    // CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
            //}
            model.ConstituencyCode = CConstituencyCode;
            Helper.ExecuteService("ContactGroups", "AddOffice", model);

            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetMappedOfficeByDeptId", model);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model1 = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model1.OfficeList = GetData1;
            model1.DepartmentId = Deptid;
            return PartialView("_GetOfficeByDeptMappedForCM", model1);
        }

        public ActionResult GetAllMappedoffice2ForCM()
        {
            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model1 = new DomainModel.Models.Office.tDepartmentOfficeMapped();

            model1.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllMappedOfficeByMemberCode", model1);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model.OfficeList = GetData1;

            return PartialView("_GetOfficeByDeptMappedForCM", model);
        }

        public ActionResult DeleteOfficeForCM(string Id, string Deptid)
        {
            mMember member = new mMember();
            member.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
            RecipientGroup recipientGroup = new RecipientGroup();
            int CConstituencyCode = Convert.ToInt32(Helper.ExecuteService("Member", "GetConstituencyCodeByMember", member));

            SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model = new DomainModel.Models.Office.tDepartmentOfficeMapped();
            model.Ids = Id;
            model.DeptId = Deptid;
            model.AssemblyCode = Convert.ToInt32(CurrentSession.AssemblyId);
            model.ConstituencyCode = CConstituencyCode;// Convert.ToInt32(CurrentSession.ConstituencyID);
            model.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

            Helper.ExecuteService("ContactGroups", "DeleteOffice", model);

            var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetMappedOfficeByDeptId", model);

            SBL.DomainModel.Models.DeptRegisterModel.DeptRegisterModel model1 = new DomainModel.Models.DeptRegisterModel.DeptRegisterModel();

            model1.OfficeList = GetData1;
            model1.DepartmentId = Deptid;
            return PartialView("_GetOfficeByDeptMappedForCM", model1);
        }
        public JsonResult AssignNodalForCM(string Starvals)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionValue = Starvals;
            objModel.Message = Helper.ExecuteService("Notice", "AssignNodalOffice", objModel) as string;
            //Update.Message = "Nodal Office created Successfully !";
            return Json(objModel.Message, JsonRequestBehavior.AllowGet);

        }

        public JsonResult DeleteNodalForCM(string Starvals)
        {
            tMemberNotice Update = new tMemberNotice();
            tQuestion objModel = new tQuestion();
            objModel.QuestionValue = Starvals;
            objModel.Message = Helper.ExecuteService("Notice", "DeleteNodalOffice", objModel) as string;
            //Update.Message = "Nodal Office created Successfully !";
            return Json(objModel.Message, JsonRequestBehavior.AllowGet);

        }
        #endregion Office Mapping with Department

        #region Public Grievance For CM
        public PartialViewResult SearchGrievance(int pageId, int pageSize, int Status, int OrderBy)
        {
            pageId = 1;
            pageSize = 20;
            OrderBy = 0;
            string MemberCode = CurrentSession.MemberCode;
            List<tMemberGrievances> GrievanceList = new List<tMemberGrievances>();
            var GrievanceData = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();
            if (OrderBy == 0)
                GrievanceList = GrievanceData.OrderByDescending(m => m.SubmittedDate).ToList();
            else
                GrievanceList = GrievanceData.OrderByDescending(m => m.StatusModifiedDate).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = GrievanceList.Count;
            return PartialView("_GrievanceListForCM", pagedRecord);
        }
        public PartialViewResult PopulateOffice(string GrvCode, int StatusId = 0)
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            ViewBag.GrvCode = GrvCode;
            ViewBag.StatusId = StatusId;
            string currentAssemby = CurrentSession.AssemblyId;
            var obj = (ForwardGrievance)Helper.ExecuteService("Grievance", "GetForwardGrievanceByGRVCode", new ForwardGrievance { MemberCode = MemberCode, GrvCode = GrvCode });

            var fillListGenric = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "memberDepartment", new string[] { CurrentSession.MemberCode, currentAssemby });

            if (obj != null)
            {
                ViewBag.DepartmentList = new SelectList(fillListGenric, "Value", "Text", obj.DeptId);
                List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetExecutiveOffice", new string[] { obj.DeptId, CurrentSession.MemberCode });
                ViewBag.OfficeList = new SelectList(_list, "Value", "Text", obj.OfficeId);
            }
            else
            {
                ViewBag.DepartmentList = new SelectList(fillListGenric, "Value", "Text");
                var result = new List<SelectListItem>();
                //  result.Add(new SelectListItem() { Text = "--Select Office--", Value = "0" });

                ViewBag.OfficeList = new SelectList(result, "Value", "Text");
            }



            return PartialView("_PopulateOfficeForCM");
        }
        public PartialViewResult PopulateAllOffice(string GrvCodes, int StatusId = 0)
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            ViewBag.GrvCodes = GrvCodes;
            ViewBag.StatusId = StatusId;
            string currentAssemby = CurrentSession.AssemblyId;
            var fillListGenric = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "memberDepartment", new string[] { CurrentSession.MemberCode, currentAssemby });


            ViewBag.DepartmentList = new SelectList(fillListGenric, "Value", "Text");
            var result = new List<SelectListItem>();
            ViewBag.OfficeList = new SelectList(result, "Value", "Text");

            return PartialView("_PopulateAllOfficeForCM");

            //return PartialView("/Areas/Grievances/Views/AssingGrievances/_PopulateAllOffice.cshtml");
        }
        public PartialViewResult DisposeGrievance(string GrvCode, int Status = 0)
        {

            tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = GrvCode });
            ViewBag.GrvCode = GrvCode;
            ViewBag.Status = Status;
            ViewBag.UserName = MemberGrievances.UserName;

            return PartialView("_DisposeGrievanceForCM", MemberGrievances);
        }
        public PartialViewResult ShowAssignOfficerList(string GrvCode)
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            string AadharId = CurrentSession.AadharId;
            tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = GrvCode });
            ViewBag.GrvCode = GrvCode;
            ViewBag.UserName = MemberGrievances.UserName;
            var OfficerInfoList = (List<ForwardGrievance>)Helper.ExecuteService("Grievance", "GetFieldOfficerByMemberCode", new ForwardGrievance { MemberCode = MemberCode, GrvCode = GrvCode });

            var GrievanceReplyList = (List<tGrievanceReply>)Helper.ExecuteService("Grievance", "GetAllGrievanceReply", GrvCode);
            GrievanceModel model = new GrievanceModel();
            model.ForwardGrievanceList = OfficerInfoList;
            model.GrievanceReplyList = GrievanceReplyList;
            model.tMemberGrievances = MemberGrievances;
            return PartialView("_ShowAssignOfficerForCM", model);
        }
        [HttpPost, ValidateInput(false)]

        public PartialViewResult SaveOffice(FormCollection collection, int pageId = 1, int pageSize = 20, int Status = 0)
        {
            string MemberCode = CurrentSession.MemberCode;
            string GrvCode = collection["hdnGrvCode"];
            // string[] RowCount = collection["OfficerRowCount"].Split(',');
            tMemberGrievances MemGrievance = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = GrvCode });
            try
            {
                if (MemGrievance != null && MemGrievance.StatusId != 3 && MemGrievance.StatusId != 4)
                {
                    MemGrievance.StatusModifiedDate = DateTime.Now;
                    MemGrievance.StatusId = SBL.DomainModel.Models.Enums.GrievanceStatus.Forwarded.GetHashCode();
                    Helper.ExecuteService("Grievance", "UpdateMemberGrievances", MemGrievance);
                }
                DataSet dataSet = new DataSet();
                var ForwardGrievanceParam = new List<KeyValuePair<string, string>>();
                ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@MemberCode", MemberCode));
                ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@GrvCode", GrvCode));
                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteForwardGrievance", ForwardGrievanceParam);


                string OfficeId = collection["OfficeId"];
                string DeptId = collection["DepartmentId"];

                ForwardGrievance obj = new ForwardGrievance();
                obj.DeptId = DeptId;
                obj.MemberCode = int.Parse(MemberCode);
                obj.GrvCode = GrvCode;
                obj.ForwardDate = DateTime.Now;
                obj.Type = "Grievance";
                obj.OfficeId = int.Parse(OfficeId);
                obj.ForwardId = (Int64)Helper.ExecuteService("Grievance", "CreateForwardGrievance", obj);

                ViewBag.Msg = "Sucessfully assign office";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! Something went wrong.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }


            var GrievanceList = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();

            ViewBag.GrievanceCount = GrievanceList.Count;
           // return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
            return PartialView("_GrievanceListForCM", pagedRecord);
        }
        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 20, int Status = 0, int OrderBy = 0)
        {
            string MemberCode = CurrentSession.MemberCode;
            List<tMemberGrievances> GrievanceList = new List<tMemberGrievances>();
            var GrievanceData = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();
            if (OrderBy == 0)
                GrievanceList = GrievanceData.OrderByDescending(m => m.SubmittedDate).ToList();
            else
                GrievanceList = GrievanceData.OrderByDescending(m => m.StatusModifiedDate).ToList();
            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = GrievanceList.Count;
           // return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
            return PartialView("_GrievanceListForCM", pagedRecord);
        }
        public PartialViewResult SaveDisposeGrievance(tGrievanceReply model, int pageId = 1, int pageSize = 20, int Status = 0)
        {
            string AadharId = CurrentSession.AadharId;
            string MemberCode = CurrentSession.MemberCode;
            try
            {
                model.OfficerCode = AadharId;
                model.ActionDate = DateTime.Now;
                model.DisposedBy = "Member";
                model.StatusId = SBL.DomainModel.Models.Enums.GrievanceStatus.Disposed.GetHashCode();
                Helper.ExecuteService("Grievance", "CreateGrievanceReply", model);

                tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = model.GrvCode, OfficerCode = AadharId });
                MemberGrievances.StatusId = SBL.DomainModel.Models.Enums.GrievanceStatus.Disposed.GetHashCode();
                MemberGrievances.DiposedUserId = AadharId;
                MemberGrievances.DisposedBy = "Member";
                MemberGrievances.StatusModifiedDate = DateTime.Now;
                Helper.ExecuteService("Grievance", "UpdateMemberGrievances", MemberGrievances);

                ViewBag.Msg = "Sucessfully disposed grievance";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";

            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }


            var GrievanceList = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).OrderByDescending(m => m.SubmittedDate).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();

            ViewBag.GrievanceCount = GrievanceList.Count;

          //  return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
            return PartialView("_GrievanceListForCM", pagedRecord);
        }
        public PartialViewResult SaveAllOffice(FormCollection collection, int pageId = 1, int pageSize = 20, int Status = 0)
        {
            string MemberCode = CurrentSession.MemberCode;
            string[] GrvCodes = collection["hdnGrvCodes"].Split(','); ;
            // string[] RowCount = collection["OfficerRowCount"].Split(',');
            foreach (var item in GrvCodes)
            {


                tMemberGrievances MemGrievance = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = item });
                try
                {
                    if (MemGrievance != null && MemGrievance.StatusId != 3 && MemGrievance.StatusId != 4)
                    {
                        MemGrievance.StatusModifiedDate = DateTime.Now;
                        MemGrievance.StatusId = SBL.DomainModel.Models.Enums.GrievanceStatus.Forwarded.GetHashCode();
                        Helper.ExecuteService("Grievance", "UpdateMemberGrievances", MemGrievance);
                    }
                    DataSet dataSet = new DataSet();
                    var ForwardGrievanceParam = new List<KeyValuePair<string, string>>();
                    ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@MemberCode", MemberCode));
                    ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@GrvCode", item));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteForwardGrievance", ForwardGrievanceParam);


                    string OfficeId = collection["OfficeIds"];
                    string DeptId = collection["DepartmentIds"];

                    ForwardGrievance obj = new ForwardGrievance();
                    obj.DeptId = DeptId;
                    obj.MemberCode = int.Parse(MemberCode);
                    obj.GrvCode = item;
                    obj.ForwardDate = DateTime.Now;
                    obj.Type = "Grievance";
                    obj.OfficeId = int.Parse(OfficeId);
                    obj.ForwardId = (Int64)Helper.ExecuteService("Grievance", "CreateForwardGrievance", obj);

                    ViewBag.Msg = "Sucessfully assign office";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                catch (Exception)
                {
                    ViewBag.Msg = "Ooops....! Something went wrong.";
                    ViewBag.Class = "alert alert-error";
                    ViewBag.Notification = "Error";
                    throw;
                }
            }

            var GrievanceList = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();

            ViewBag.GrievanceCount = GrievanceList.Count;
          //  return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
            return PartialView("_GrievanceListForCM.cshtml", pagedRecord);
        }
        [HttpGet]
        public ActionResult ShowGrievanceDetail_Pdf(string GrvCode)
        {
            int Memcode = 0;
            if (CurrentSession.SubDivisionId != "" && CurrentSession.SubUserTypeID == "40")  //sdm
            {
                Memcode = 0;
            }
            if (CurrentSession.MemberCode != "")
            {
                int MemberCode = int.Parse(CurrentSession.MemberCode);
                Memcode = MemberCode;
            }

            string AadharId = CurrentSession.AadharId;
            pdfGrievance model = new pdfGrievance();
            List<pdfGrievance> _list = new List<pdfGrievance>();
            DataSet dataSet = new DataSet();
            var _paremeter = new List<KeyValuePair<string, string>>();
            _paremeter.Add(new KeyValuePair<string, string>("@GrvCode", GrvCode));
            _paremeter.Add(new KeyValuePair<string, string>("@MemberCode", Memcode.ToString()));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Get_AllGrievanceInfo", _paremeter);
            if (dataSet != null)
            {
                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        model.PublicName = dataSet.Tables[0].Rows[0]["PublicName"].ToString();
                        model.PublicAddress = dataSet.Tables[0].Rows[0]["PublicAddress"].ToString();
                        model.PublicMobile = dataSet.Tables[0].Rows[0]["PublicMobile"].ToString();
                        model.PublicAAdharId = dataSet.Tables[0].Rows[0]["PublicAadharID"].ToString();
                        model.Public_Profile = dataSet.Tables[0].Rows[0]["ProfilePic"].ToString();
                        model.GrvCode = dataSet.Tables[0].Rows[0]["GrvCode"].ToString();
                        model.GrvDesc = dataSet.Tables[0].Rows[0]["InformationContent"].ToString();
                        model.SubmitDate = dataSet.Tables[0].Rows[0]["SubmitDate"].ToString();
                        model.PostedTo = dataSet.Tables[0].Rows[0]["MemName"].ToString();
                        model.GrvImage = dataSet.Tables[0].Rows[0]["ImageName"].ToString();
                        model.GrvStatus = dataSet.Tables[0].Rows[0]["StatusId"].ToString();
                        if (model.GrvStatus == "0")
                        {
                            model.GrvStatus = "Pending";
                        }
                        else if (model.GrvStatus == "1")
                        {
                            model.GrvStatus = "Forwarded";
                        }
                        else if (model.GrvStatus == "2")
                        {
                            model.GrvStatus = "InProgress";
                        }
                        else if (model.GrvStatus == "3")
                        {
                            model.GrvStatus = "Disposed";
                        }
                        //else
                        //{
                        //    model.GrvStatus = "Disposed";
                        //}
                    }
                    if (dataSet.Tables[1].Rows.Count > 0)
                    {
                        model.Deptname = dataSet.Tables[1].Rows[0]["deptname"].ToString();
                        model.Officename = dataSet.Tables[1].Rows[0]["officename"].ToString();
                    }
                    if (dataSet.Tables[2].Rows.Count > 0)
                    {
                        model.ActionTaken = dataSet.Tables[2].Rows[0]["Reply"].ToString();
                        model.ActionDate = dataSet.Tables[2].Rows[0]["ActionDate"].ToString();
                        model.GrvActionStatus = dataSet.Tables[2].Rows[0]["StatusId"].ToString();
                        if (model.GrvActionStatus == "0")
                        {
                            model.GrvActionStatus = "Pending";
                        }
                        else if (model.GrvActionStatus == "1")
                        {
                            model.GrvActionStatus = "Forwarded";
                        }
                        else if (model.GrvActionStatus == "2")
                        {
                            model.GrvActionStatus = "InProgress";
                        }
                        else if (model.GrvActionStatus == "3")
                        {
                            model.GrvActionStatus = "Disposed";
                        }
                        //else
                        //{
                        //    model.GrvActionStatus = "Disposed";
                        //}
                        model.ActionBy = dataSet.Tables[2].Rows[0]["ActionBy"].ToString();
                        model.Mem_MobileNo = dataSet.Tables[2].Rows[0]["MobileNo"].ToString();
                    }

                }
            }
            // _list.Add(model);
            var _pdf1 = GeneratePdf1(model);


            return Json(_pdf1, JsonRequestBehavior.AllowGet);
        }
        public Object GeneratePdf1(pdfGrievance _data)
        {
            string imagepath = "";
            string ProfilePic = "";
            var Get_NoImageFromLocal = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            string Noimagepath = Get_NoImageFromLocal.SettingValue + "/ServerUploadsPath/No_ImageAvial.png";
            string userimage = Get_NoImageFromLocal.SettingValue + "/ServerUploadsPath/nouser.png";
            var Get_GrvImageFromDataBase = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            if (_data.GrvImage == null || _data.GrvImage == "")
            {
                imagepath = Noimagepath;
                //Server.MapPath("../../Images/Common/No_ImageAvial.png");   //"http://localhost:50730/Images/Common/No_ImageAvial.png";
            }
            else
            {
                imagepath = Get_GrvImageFromDataBase.SettingValue + "/ServerUploadsPath/" + _data.GrvImage + "";
            }

            if (_data.Public_Profile == null || _data.Public_Profile == "")
            {
                ProfilePic = userimage;
                //Server.MapPath("../../Images/Common/No_ImageAvial.png");
            }
            else
            {
                ProfilePic = _data.Public_Profile;
            }

            //   D:\ProjectSVN\New folder\Project1\SBL.eLegistrator.HouseController.Web\Grievances\AssingGrievances\Images\Common\No_ImageAvial.png
            //  D:\ProjectSVN\New folder\Project1\SBL.eLegistrator.HouseController.Web\Images\Common\No_ImageAvial.png
            try
            {
#pragma warning disable CS0219 // The variable 'count' is assigned but its value is never used
                int count = 0;
#pragma warning restore CS0219 // The variable 'count' is assigned but its value is never used
                string outXml = "";

                string fileName = "";
                string savedPDF = string.Empty;

                if (_data != null)
                {

                    outXml = @"<html>                           
                                <body style='font-family:DVOT-Yogesh;color:#003366;'> <div><div style='width: 100%;'>";
                    outXml += @"<div> 


                    </div>
            
<table style='width:100%; border: solid 1px black;padding:10px;'>
<tr><td>
<table style='width:100%;border-bottom:1px solid black;padding-bottom:10px;'>
 
      <tr>
          <td style='text-align:left;font-size:28px;' >
             <b> Personal Information ( Posted By )" + @"</b>
              </td>
              </tr>
      </table>

<table style='width: 100%; padding-bottom:10px;font-size:20px;color:black;'>
           ";
                    //foreach (var item in _data.GrvList1)
                    //{
                    //  count = count + 1;
                    //  string style = (count % 2 == 0) ? "odd" : "even";
                    // //  <img  src=" + @"data:image/jpeg;base64," + imagepath + "                     height='100' width='100'/> 
                    outXml += @"

  <tr style='border-bottom:1px  solid black;'>
            <td style='text-align: left;width:100px;'><img  height='90' width='90'  src=" + "data:image/jpeg;base64," + ProfilePic + @"/> </td>
        <td style='text-align: left;'><b>Name : </b>" + _data.PublicName + @" <br />
  <b> Mobile Number:</b>" + _data.PublicMobile + @"<br />
 <b>Address:</b>" + _data.PublicAddress + @"<br /></td>
        </tr>
<tr><td colspan='2' style='text-align: center;border-bottom: 1px solid black'> <br/>
</td></tr>
<tr><td colspan='2'  style='text-align: center;padding:10px;'><b>Grievance Detail</b> <br/></td></tr>                    
<tr><td  colspan='2'>
<table style='width: 100%; border-collapse: collapse;border: solid 1px black;padding:5px;font-size:20px;color:black;'>
<tr><td style='text-align:left;width:80%;'><table style='width:100%; font-size:18px;color:black;'><tr><td>
<b>Grievance Number :</b>" + _data.GrvCode + @" <br/>   
<b>Grievance Description :</b>" + _data.GrvDesc + @" <br/> 
<b>Posted On :</b>" + _data.SubmitDate + @"<br/>
<b>Posted To :</b>" + _data.PostedTo + @"<br/> </td></tr></table></td>

 <td style='width:100px;text-align:left;'><b> </b> <br/> 
  <img height='90' width='90' src=" + @"data:image/jpeg;base64," + imagepath + @" /><br/>

</td>
</tr>
<tr><td colspan='2'><br/></td></tr>
</table>
   </td></tr>
<tr><td colspan='2' style='text-align: center;padding:10px;'><b>Action Detail</b> <br/></td></tr>
<tr><td colspan='2'> <table style='width: 100%; border-collapse: collapse;border: solid 1px black;padding:10px;font-size:20px; color:black;'>
<tr><td style='text-align: left;'><b>Department Name :</b>" + _data.Deptname + @"</td><td    style='text-align: left;'><b>Office Name :</b>" + _data.Officename + @"</td></tr>
<br/>
    <tr><td colspan='2' style='text-align: left; '><b>Action Taken : </b>" + _data.ActionTaken + @"</td></tr> 
    <tr><td  colspan='2'style='text-align: left;'><b>Action Date :</b>" + _data.ActionDate + @"</td></tr>
  <tr><td colspan='2' style='text-align: left;'><b>Action By : </b> " + _data.ActionBy + @"</td></tr>
 <tr><td colspan='2'  style='text-align: left;'><b>Status : </b> " + _data.GrvActionStatus + @"</td></tr>
</table>
</td></tr></td></tr></table>
";
                    //} src=http://164.100.88.163/FileStructure/ServerUploadsPath/" + _data.GrvImage + @" /><br/>  src='file:///C:/inetpub/e_Vidhan/FileStructure/ServerUploadsPath/' " + _data.Public_Profile + @"/> 
                }
                //// file:///C:/inetpub/e_Vidhan/FileStructure/ServerUploadsPath/353327061034732_1489474722348.jpg
                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/UserGrievanceReport/";
                // fileName = "QuestionList" + "DateFrom" + "_" + Convert.ToDateTime(model.DateFrom).ToString("dd/MM/yyyy") + "_" + "DateTo" + "_" + Convert.ToDateTime(model.DateTo).ToString("dd/MM/yyyy") + "_" + "S" + ".pdf";
                // fileName = fileName.Replace("/", "-");
                //  HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);
                fileName = "UserGrievance.pdf";
                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                //DownloadDateWiseQuestion();
                return url + fileName;


            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }
        #endregion

    }
   
}
