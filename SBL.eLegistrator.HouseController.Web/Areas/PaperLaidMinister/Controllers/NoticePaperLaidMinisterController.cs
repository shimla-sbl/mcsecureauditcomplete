﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.PaperLaid;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Notice;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.SiteSetting;
using System.Linq;
using SBL.DomainModel.ComplexModel;
using SBL.eLegistrator.HouseController.Web.Extensions;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class NoticePaperLaidMinisterController : Controller
    {
        //
        // GET: /PaperLaidDepartment/NoticePaperLaid/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllNotices(string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            tPaperLaidV model = new tPaperLaidV();
            return PartialView("_GetAllNotices", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetAllNotices(JqGridRequest request, tPaperLaidV customModel)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;
            model.PageIndex = request.PageIndex +1;

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.MinistryId = model.MinistryId;
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetAllNotices", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterNoticecustomList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<MinisterNoticecustom>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from NoticeList in resultForGrid
                                      select new JqGridRecord<MinisterNoticecustom>(Convert.ToString(NoticeList.NoticeID), new MinisterNoticecustom(NoticeList)));
            return new JqGridJsonResult() { Data = response };
        }
        //Added code venkat for dynamic 
        public ActionResult GetNoticeInBoxList(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string Noticenumber, string Subject, int? Name)
        {
            tPaperLaidV model = new tPaperLaidV();
            //Added code venkat for dynamic 
            model.NoticeNumber = Noticenumber;
            model.Subject = Subject;
            model.MinID = Name;
            //end-----------------------------
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            return PartialView("_GetNoticeInBoxList", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoticeInBoxList(JqGridRequest request, string Noticenumber, string Subject, int? Name)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;
            model.PageIndex = request.PageIndex + 1;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            //model.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.Subject = Subject;
            model.NoticeNumber = Noticenumber;
            model.MinID = Name;

            if (CurrentSession.RoleID.ToUpper() == "0FC60E41-C78E-46B0-B8AC-F6EB709BE999")
            {
                model.RequestMessage = "ChiefMinisterAdmin";
            }


            //end--------------------------------
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticeInBoxList", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterNoticecustomList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<MinisterNoticecustom>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from NoticeList in resultForGrid
                                      select new JqGridRecord<MinisterNoticecustom>(Convert.ToString(NoticeList.NoticeID), new MinisterNoticecustom(NoticeList)));
            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult ShowNoticeInboxById(int noticeId )
        {
            if (CurrentSession.UserID != null)
            {
                tMemberNotice data = new tMemberNotice();
                data.NoticeId = noticeId;
                data = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticeByNoticeID", data);
                return PartialView("_ShowNoticeInboxById", data);
            }
            return null;
        }

        public ActionResult CreateNewNoticeReply(int noticeId,int sessionId,int assemblyId)
        {
             if (CurrentSession.UserID != null)
            {
                tMemberNotice data = new tMemberNotice();
                data.NoticeId = noticeId;
                data = (tMemberNotice)Helper.ExecuteService("Notice", "GetNoticeByNoticeID", data);
                
                tPaperLaidV mdl = new tPaperLaidV();
                mdl.Title = data.Notice;
                mdl.Description = data.Subject;
                mdl.SessionId = sessionId;
                mdl.AssemblyId = assemblyId;
                mdl.NoticeID = data.NoticeId;
                //mdl.PaperLaidId = Convert.ToInt64(noticeId);
                //mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "ShowDraftBillById", mdl);

                //mDepartment departmentModel = new mDepartment();
                mEvent mEventModel = new mEvent();
                mdl.DepartmentId = CurrentSession.DeptID;
                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = mdl.DepartmentId;                

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                mdl.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", mdl);

                mdl.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", mdl);
                //mdl.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", departmentModel);

                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                mdl.mEvents = events.FindAll(a => a.EventId == 9 || a.EventId == 10);

                mdl.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
                //mdl.mBillTypes = (List<mBillType>)Helper.ExecuteService("BillPaperLaid", "GetBillTypes", "Testdata");

                ICollection<int> yearList = new List<int>();
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear - 5; i < currentYear; i++)
                {
                    yearList.Add(i);
                }
                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    yearList.Add(i);
                }
                //mdl.yearList = yearList;

                return PartialView("_CreateNewNoticeReply", mdl);
            }
            return null;
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult PaperLaidSubmitt(HttpPostedFileBase file, tPaperLaidV obj, string submitButton)
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV mdl = new tPaperLaidV();
                tPaperLaidTemp mdlTemp = new tPaperLaidTemp();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    mdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    mdl.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                }


                switch (submitButton)
                {
                    case "Send":
                        {                          
                            obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);
                            obj.ModifiedBy = CurrentSession.UserName;
                            obj.ModifiedWhen = DateTime.Now;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                            mdl.EventId = obj.EventId;                         
                        
                            if (file != null)
                            {
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                string Url = "/PaperLaid/" + mdl.AssemblyId + "/" + mdl.SessionId + "/";
                                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }
                                string ext = Path.GetExtension(file.FileName);
                                var vers = obj.FileVersion.GetValueOrDefault() + 1;
                                string fileName = file.FileName.Replace(ext, "");
                                string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                file.SaveAs(Server.MapPath(Url + actualFileName));
                                mdlTemp.FileName = actualFileName;
                                mdlTemp.FilePath = Url;
                                mdlTemp.Version = vers;
                                mdlTemp.PaperLaidId = mdl.PaperLaidId;
                                mdlTemp.DeptSubmittedDate = DateTime.Now;
                                mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                            }

                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);
                            tMemberNotice needupdatedata = new tMemberNotice();
                            needupdatedata.NoticeId = mdl.NoticeID;
                            needupdatedata.PaperLaidId = mdl.PaperLaidId;
                           
                            var data = (tMemberNotice)Helper.ExecuteService("Notice", "UpdateNoticeById", needupdatedata);                            

                            mdl.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                            mdl.ModifiedBy = CurrentSession.UserName;
                            mdl.ModifiedWhen = DateTime.Now;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", mdl);
                            break;
                        }
                    case "Update":
                        {
                            obj.tpaperLaidTempId = 0;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);
                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", mdl);
                            if (file != null)
                            {
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                               
                                string Url = mdlTemp.FilePath;
                                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }
                                string ext = Path.GetExtension(file.FileName);
                                var vers = obj.FileVersion.GetValueOrDefault();
                                string fileName = file.FileName.Replace(ext, "");
                                string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                file.SaveAs(Server.MapPath(Url + actualFileName));
                                mdlTemp.FileName = actualFileName;
                            }

                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);
                            break;
                        }
                    case "Save":
                        {                           
                            obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetDepartmentNameMinistryDetailsByMinisterID", obj);                           
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "SubmittPaperLaidEntry", obj);
                            mdl.EventId = obj.EventId;
                          
                            if (file != null)
                            {
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                                string Url = "/PaperLaid/" + mdl.AssemblyId + "/" + mdl.SessionId + "/";
                               
                                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }
                                string ext = Path.GetExtension(file.FileName);
                                var vers = obj.FileVersion.GetValueOrDefault() + 1;
                                string fileName = file.FileName.Replace(ext, "");
                                string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                file.SaveAs(Server.MapPath(Url + actualFileName));
                                mdlTemp.FileName = actualFileName;
                                mdlTemp.FilePath = Url;
                                mdlTemp.Version = vers;
                                mdlTemp.PaperLaidId = mdl.PaperLaidId;
                            }

                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "SaveDetailsInTempPaperLaid", mdlTemp);

                            tMemberNotice needupdatedata = new tMemberNotice();
                            needupdatedata.NoticeId = mdl.NoticeID;
                            needupdatedata.PaperLaidId = mdl.PaperLaidId;

                            var data = (tMemberNotice)Helper.ExecuteService("Notice", "UpdateNoticeById", needupdatedata);                            
                            break;
                        }
                    case "Update Send":
                        {
                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "GetExisitngFileDetails", obj);
                            obj.DeptActivePaperId = mdlTemp.PaperLaidTempId;
                            mdl = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "UpdatePaperLaidEntry", obj);

                            if (file != null)
                            {
                                obj = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", obj);
                            
                                string Url = mdlTemp.FilePath;
                                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                                if (!Dir.Exists)
                                {
                                    Dir.Create();
                                }
                                var vers = obj.FileVersion.GetValueOrDefault();
                                string ext = Path.GetExtension(file.FileName);
                                string fileName = file.FileName.Replace(ext, "");
                                string actualFileName = mdl.AssemblyId + "_" + mdl.SessionId + "_" + "N" + "_" + mdl.PaperLaidId + "_V" + vers + ext;
                                file.SaveAs(Server.MapPath(Url + actualFileName));
                                mdlTemp.FileName = actualFileName;
                            }

                            mdlTemp.DeptSubmittedDate = DateTime.Now;
                            mdlTemp.DeptSubmittedBy = CurrentSession.UserName;
                            mdlTemp = (tPaperLaidTemp)Helper.ExecuteService("BillPaperLaid", "UpdateDetailsInTempPaperLaid", mdlTemp);
                            break;
                        }
                }
                return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment"); 
            }
            return RedirectPermanent("/PaperLaidDepartment/PaperLaidDepartment/DepartmentDashboard");
        }

        public ActionResult GetNoticeReplyDraftList(int sessionId, int assemblyId, int PageNumber, int RowsPerPage, int loopStart, int loopEnd)
        {
            tPaperLaidV obj = new tPaperLaidV();
            obj.AssemblyId = assemblyId;
            obj.SessionId = sessionId;
            obj.loopEnd = loopEnd;
            obj.loopStart = loopStart;
            obj.RowsPerPage = 10;
            obj.PageNumber = 1;
            obj.PageIndex = PageNumber;
            obj.PAGE_SIZE = 10;
            obj.ResultCount = 1;
            obj.selectedPage = 1;
           
            obj.ListtPaperLaidV = (List<tPaperLaidV>)Helper.ExecuteService("Notice", "GetNoticeReplyDraftList", obj);
            
            return PartialView("_GetNoticeReplyDraftList", obj);
        }

        public ActionResult ShowNoticeDraftReplyById(int papperId)
        {            
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV mdl = new tPaperLaidV();
                mdl.PaperLaidId = papperId; 
                mdl = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeDraftReplyById", mdl);
                
                mEvent mEventModel = new mEvent();
                mdl.DepartmentId = CurrentSession.DeptID;
                tPaperLaidV ministerDetails = new tPaperLaidV();
                ministerDetails.DepartmentId = mdl.DepartmentId;               

                mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();
                mdl.mSessionDates = (ICollection<mSessionDate>)Helper.ExecuteService("BillPaperLaid", "GetSessionDates", mdl);

                mdl.mDocumentTypes = (ICollection<mDocumentType>)Helper.ExecuteService("BillPaperLaid", "GetDocumentType", mdl);               

                List<mEvent> events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEvents", mEventModel);
                mdl.mEvents = events.FindAll(a => a.EventId == 9 || a.EventId == 10);
                mdl.mMinisteryMinister = (ICollection<mMinisteryMinisterModel>)Helper.ExecuteService("MinistryMinister", "GetAllMinistryDetailsByDepartment", ministerDetails);
              
                ICollection<int> yearList = new List<int>();
                int currentYear = DateTime.Now.Year;
                for (int i = currentYear - 5; i < currentYear; i++)
                {
                    yearList.Add(i);
                }
                for (int i = currentYear; i < currentYear + 5; i++)
                {
                    yearList.Add(i);
                }
               // mdl.yearList = yearList;

                return PartialView("_CreateNewNoticeReply", mdl);
            }
            return null;
        }
        //Added code venkat for dynamic 
        public ActionResult GetNoticeReplySentList(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string Noticenumber, string Subject, int? Name)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;

            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = siteSettingMod.SessionCode;
            //model.AssemblyId = siteSettingMod.AssemblyCode;


            //Added code venkat for dynamic 
            model.NoticeNumber = Noticenumber;
            model.Subject = Subject;
            model.MinID = Name;
            //end---------------------------------
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }


           //// model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticeReplySentList", model);
           // //    model.AssemblyID =
           // model.RowsPerPage = 10;
           // model.PageNumber = 1;
           // model.loopEnd = Convert.ToInt16(loopEnd);
           // model.loopStart = Convert.ToInt16(loopStart);
           // model.PageIndex = Convert.ToInt16(PageNumber);
           // model.PAGE_SIZE = 10;
           // //    model.ResultCount = 1;
           // model.selectedPage = 1;
            return PartialView("_GetNoticeReplySentList", model);
        }
        //Added code venkat for dynamic 
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoticeReplySentList(JqGridRequest request, tPaperLaidV customModel, string Noticenumber, string Subject, int? Name)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex + 1;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;
            model.MinistryId = model.MinistryId;
            //Added code venkat for dynamic 
            model.NoticeNumber = Noticenumber;
            model.Subject = Subject;
            model.MinID = Name;
            if (CurrentSession.RoleID.ToUpper() == "0FC60E41-C78E-46B0-B8AC-F6EB709BE999")
            {
                model.RequestMessage = "ChiefMinisterAdmin";
            }
            //end---------------------------------
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticeReplySentList", model);
            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterNoticecustomList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<MinisterNoticecustom>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from NoticeList in resultForGrid
                                      select new JqGridRecord<MinisterNoticecustom>(Convert.ToString(NoticeList.PaperLaidId), new MinisterNoticecustom(NoticeList)));
            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult GetNoticeULOBList(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }


            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticeULOBList", model);
            //    model.AssemblyID =
            model.RowsPerPage = 10;
            model.PageNumber = 1;
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.loopStart = Convert.ToInt16(loopStart);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.PAGE_SIZE = 10;
            //    model.ResultCount = 1;
            model.selectedPage = 1;
            // obj.ListtPaperLaidV = (List<tPaperLaidV>)Helper.ExecuteService("Notice", "GetNoticeULOBList", obj);
            return PartialView("_GetNoticeULOBList", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoticeULOBList(JqGridRequest request, tPaperLaidV customModel)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;


            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }
            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;
            model.MinistryId = model.MinistryId;
            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticeULOBList", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterNoticecustomList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<MinisterNoticecustom>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from NoticeList in resultForGrid
                                      select new JqGridRecord<MinisterNoticecustom>(Convert.ToString(NoticeList.PaperLaidId), new MinisterNoticecustom(NoticeList)));
            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult GetNoticeLIHList(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;
            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;


            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticeLIHList", model);
            //    model.AssemblyID =
            model.RowsPerPage = 10;
            model.PageNumber = 1;
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.loopStart = Convert.ToInt16(loopStart);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.PAGE_SIZE = 10;
            //    model.ResultCount = 1;
            model.selectedPage = 1;
            return PartialView("_GetNoticeLIHList", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoticeLIHList(JqGridRequest request, tPaperLaidV customModel)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);

            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticeLIHList", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterNoticecustomList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<MinisterNoticecustom>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from NoticeList in resultForGrid
                                      select new JqGridRecord<MinisterNoticecustom>(Convert.ToString(NoticeList.PaperLaidId), new MinisterNoticecustom(NoticeList)));
            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult GetNoticePLIHList(string sessionId, string assemblyId, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tPaperLaidV model = new tPaperLaidV();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }
            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            foreach (var item in model.mMinisteryMinister)
            {
                model.MinisterName = item.MinisterName;
                break;
            }

            model.MinistryId = model.MinistryId;


            model.loopStart = Convert.ToInt16(loopStart);
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.PAGE_SIZE = Convert.ToInt16(RowsPerPage);
            model.PageIndex = Convert.ToInt16(PageNumber);

            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;


            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticePLIHList", model);
            //    model.AssemblyID =
            model.RowsPerPage = 10;
            model.PageNumber = 1;
            model.loopEnd = Convert.ToInt16(loopEnd);
            model.loopStart = Convert.ToInt16(loopStart);
            model.PageIndex = Convert.ToInt16(PageNumber);
            model.PAGE_SIZE = 10;
            //    model.ResultCount = 1;
            model.selectedPage = 1;

            return PartialView("_GetNoticePLIHList", model);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetNoticePLIHList(JqGridRequest request, tPaperLaidV customModel)
        {
            tPaperLaidV model = new tPaperLaidV();
            model.PAGE_SIZE = request.RecordsCount;

            model.PageIndex = request.PageIndex;
            //SiteSettings siteSettingMod = new SiteSettings();
            //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            //model.SessionId = model.SessionCode = siteSettingMod.SessionCode;
            //model.AssemblyId = model.AssemblyCode = siteSettingMod.AssemblyCode;


            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }

            model.PaperLaidId = 5;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                model.LoginId = CurrentSession.UserName;
            }

            model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.DepartmentId = CurrentSession.DeptID;

            model.MinistryId = model.MinistryId;

            var result = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetNoticePLIHList", model);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)result.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = result.ResultCount
            };

            var resultToSort = result.MinisterNoticecustomList.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<MinisterNoticecustom>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from NoticeList in resultForGrid
                                      select new JqGridRecord<MinisterNoticecustom>(Convert.ToString(NoticeList.PaperLaidId), new MinisterNoticecustom(NoticeList)));
            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult ShowNoticeSentRecordById(int paperLaidId)
        {
            tPaperLaidV mdl = new tPaperLaidV();
            mdl.PaperLaidId = Convert.ToInt64(paperLaidId);
            mdl = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeReplySentRecordById", mdl);
            return PartialView("_ShowNoticeSentRecordById", mdl);
        }

        public ActionResult GetNoticeSentForReplaceByID(string paperLaidId)
        {
            if (CurrentSession.UserID != null)
            {
                tPaperLaidV movementModel = new tPaperLaidV();
                movementModel.PaperLaidId = Convert.ToInt64(paperLaidId);
                movementModel = (tPaperLaidV)Helper.ExecuteService("Notice", "GetNoticeSubmittedPaperLaidByID", movementModel);
                return PartialView("_GetNoticeSentForReplaceByID", movementModel);
            }
            return null;
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateNoticePaperLaidFile(HttpPostedFileBase file, tPaperLaidV tPaper)
        {
            if (CurrentSession.UserID != "")
            {
                tPaperLaidTemp tPaperTemp = new tPaperLaidTemp();
                tPaper = (tPaperLaidV)Helper.ExecuteService("BillPaperLaid", "GetBillFileVersion", tPaper);
                
                if (file != null)
                {
                    //SiteSettings siteSettingMod = new SiteSettings();
                    //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);


                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                    if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                    {
                        tPaper.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                    }

                    if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                    {
                        tPaper.SessionId = Convert.ToInt16(CurrentSession.SessionId);
                    }

                    string Url = "/PaperLaid/" + tPaper.AssemblyId + "/" + tPaper.SessionId + "/";
                    DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                    if (!Dir.Exists)
                    {
                        Dir.Create();
                    }
                    var vers = tPaper.FileVersion.GetValueOrDefault() + 1;
                    string ext = Path.GetExtension(file.FileName);
                    string fileName = file.FileName.Replace(ext, "");
                    string actualFileName = tPaper.AssemblyId + "_" + tPaper.SessionId + "_" + "N" + "_" + tPaper.PaperLaidId + "_V" + vers + ext;
                    file.SaveAs(Server.MapPath(Url + actualFileName));
                    tPaperTemp.FileName = actualFileName;
                    tPaperTemp.FilePath = Url;
                    tPaperTemp.Version = tPaper.FileVersion + 1;
                    tPaperTemp.PaperLaidId = tPaper.PaperLaidId;
                    tPaperTemp.DeptSubmittedDate = DateTime.Now;
                    tPaperTemp.DeptSubmittedBy = CurrentSession.UserName;
                }

                tPaperTemp = (tPaperLaidTemp)Helper.ExecuteService("Notice", "UpdateNoticePaperLaidFileByID", tPaperTemp);

                tPaper.DeptActivePaperId = tPaperTemp.PaperLaidTempId;
                tPaper.ModifiedBy = CurrentSession.UserName;
                tPaper.ModifiedWhen = DateTime.Now;
                tPaper = (tPaperLaidV)Helper.ExecuteService("Notice", "UpdatePaperLaidEntryForNoticeReplace", tPaper);              
            }
            return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment");
        }

    }
}
