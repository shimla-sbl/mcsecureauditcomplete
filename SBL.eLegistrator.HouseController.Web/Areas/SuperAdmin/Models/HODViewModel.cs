﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SBL.DomainModel.Models.Enums;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class HODViewModel
    {
        public int HODID { get; set; }

        //[Required(ErrorMessage = "Employee Code Required")]
        //public int? EmpCode { get; set; }

        public int? EmpCode { get; set; } 

        [Required(ErrorMessage = "Department Required")]
        public string DeptID { get; set; }

        [Required(ErrorMessage = "Prefix Required")]
        public string Prefix { get; set; }

        [Required(ErrorMessage = "Name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string HODName { get; set; }

        [StringLength(60, ErrorMessage = "Maximum 60 characters allowed")]
        public string HODNameLocal { get; set; }

        [StringLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string DesignationDescription { get; set; }

        [StringLength(20)]
        public string IASRank { get; set; }

        [Required(ErrorMessage = "Email ID Required")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Email is not valid")]
        public string Email { get; set; }

        [StringLength(15, ErrorMessage = "Maximum 15 digits allowed")]
        public string PhoneResidence { get; set; }

        [Required(ErrorMessage = "Phone No.(Office) Required")]
        [StringLength(15, ErrorMessage = "Maximum 15 digits allowed")]
        public string PhoneOffice { get; set; }

        [StringLength(15, ErrorMessage = "Maximum 15 digits allowed")]
        public string PhoneOfficeAlternate { get; set; }

        [StringLength(10, ErrorMessage = "Maximum 10 digits allowed")]
        public string Mobile { get; set; }

        public bool IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public string CreatedBy { get; set; }

        [Required(ErrorMessage = "Evidhan ID Required")]
        [StringLength(12, ErrorMessage = "Enter Evidhan ID 12 digits without space.")]
        public string AadhaarId { get; set; }

        [Required(ErrorMessage = "Order ID Required")]
        public int? OrderId { get; set; }

        public string Mode { get; set; }

        public SelectList PrefixCol { get; set; }

        public SelectList DeptCol { get; set; }

        public string GetDepartmentName { get; set; }
    }
}
