﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class CutMotionDemandViewModel
    {

        public int VCMDemandId { get; set; }

        [Required(ErrorMessage = "Assembly ID Required")]
        public int VAssemblyID { get; set; }

        [Required(ErrorMessage = "Session ID Required")]
        public int VSessionID { get; set; }

        public int? VDemandNo { get; set; }

        [Required(ErrorMessage = "Demand Name Required")]
        [StringLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string VDemandName { get; set; }

        [StringLength(200, ErrorMessage = "Maximum 200 characters allowed")]
        public string VDemandName_Local { get; set; }

        
        //[Required(ErrorMessage = "Demand Amount Type Required")]
        public string VDemandAmountType { get; set; }
        
        public string VDemandAmountType_Local { get; set; }

        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string VRevenueAmount { get; set; }
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string VCapitalAmount { get; set; }


        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        public bool IsCreatNew { get; set; }

        public string Mode { get; set; }

        public string VAssemblyName { get; set; }

        public string VSessionName { get; set; }

        public List<mAssembly> AssemblyList { get; set; }

        public List<mSession> SessionList { get; set; }


    }
}