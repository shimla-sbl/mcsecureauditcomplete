﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class RulesDirectionsViewModel
    {
        public Int64 SlNo { get; set; }

        public string Title { get; set; }

        public string PdfPath { get; set; }

        public string DownloadPath { get; set; }

        public string Type { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public SelectList TypeCol { get; set; }
    }
}