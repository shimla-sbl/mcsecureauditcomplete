﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class AssemblyRemarksViewModel
    {

        
        public int AssemblyRemarksID { get; set; }

        [Required(ErrorMessage = "Assembly Remark is Required")]
        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string AssemblyRemarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool Active { get; set; }

        public string Mode { get; set; }


        [Required(ErrorMessage = "Select Assembly Name")]
        public int AssemblyID { get; set; }

        public SelectList Assembly { get; set; }


        public string GetAssemblyName { get; set; }

    }
}