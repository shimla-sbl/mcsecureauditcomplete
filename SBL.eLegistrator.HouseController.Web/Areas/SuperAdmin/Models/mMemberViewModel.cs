﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class mMemberViewModel
    {

        public int? MemberID { get; set; }
        [Required(ErrorMessage = "Member Code is Required")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "MemberId must be a number")]
        public string MemberCode { get; set; }

        public bool Active { get; set; }

        //[Required(ErrorMessage = "Select District Name")]
        public int? DistrictID { get; set; }

        public string LoginID { get; set; }
            [Required(ErrorMessage = "Select State Name")]
        public int StateNameID { get; set; }

        public string CmRefnicVipCode { get; set; }

        [Required(ErrorMessage = "Select Prefix")]
        public string Prefix { get; set; }

        [Required(ErrorMessage = " Member Name is Required")]
        [MaxLength(250, ErrorMessage = "Maximum 250 characters allowed")]
        public string Name { get; set; }

       [MaxLength(250, ErrorMessage = "Maximum 250 characters allowed")]
        public string NameLocal { get; set; }

        //[Required(ErrorMessage = "Select District Name")]
        //public int District { get; set; }

       [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string ShimlaAddress { get; set; }

       [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string PermanentAddress { get; set; }

        [Required(ErrorMessage = "Select Gender")]
        public string Sex { get; set; }

        //[Required(ErrorMessage = "Select DOB")]
        //public DateTime? memdob { get; set; }
        public string memdob { get; set; }

        public string TelOffice { get; set; }

       

        public string TelResidence { get; set; }

        //[Required(ErrorMessage = "Phone Number is Required")]
        //[RegularExpression(@"^[0-9\.\+\-\/]+$", ErrorMessage = "Invalid phone number")]
        //[MaxLength(10, ErrorMessage = "Maximum 10 characters allowed")]
        public string Mobile { get; set; }

        public string AlternateMobile { get; set; }

        //[Required(ErrorMessage = "Email Id is Required")]
        //[EmailAddress(ErrorMessage = "Invalid Email")]
        //[MaxLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string Email { get; set; }

        //[Required(ErrorMessage = "Select State Name")]
        //public int StateName { get; set; }
        [MaxLength(250, ErrorMessage = "Maximum 250 characters allowed")]
        public string FatherName { get; set; }

         [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Description { get; set; }

        //[Required(ErrorMessage = " Aadharr Number is Required")]
        public string AadhaarNo { get; set; }


        public int? HighestQualificationID { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string PreviousQualification { get; set; }
        public string DeactivationReason { get; set; }
        public string Remarks { get; set; }



        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }

        public SelectList PrefixList { get; set; }

        public SelectList GendersList { get; set; }

        public SelectList MemberCodes { get; set; }

        public SelectList Districts { get; set; }

        public SelectList StateNames { get; set; }

        public SelectList HighestQualification { get; set; }

        public string GetHighestQualificationName { get; set; }

        public string GetStateName { get; set; }
        
        public string GetDistrictName { get; set; }

        public int AssemblyId { get; set; }

        public string FullImage { get; set; }

        public string ThumbImage { get; set; }
        public string FileLocation { get; set; }
        public string ImageLocation { get; set; }

        public string ThumbName { get; set; }

        
        public int MonthlySMSQuota { get; set; }

        public bool isAlive { get; set; }

        public bool isAuthorized { get; set; }

    }
}