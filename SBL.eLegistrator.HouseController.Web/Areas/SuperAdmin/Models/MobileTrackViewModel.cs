﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class MobileTrackViewModel
    {
        public Int64 SlNo { get; set; }

        public string ZoneCode { get; set; }

        public string NetworkCode { get; set; }

        public string StateCode { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public SelectList StateCodes { get; set; }

        public SelectList NetWorkCodes { get; set; }
    }
}