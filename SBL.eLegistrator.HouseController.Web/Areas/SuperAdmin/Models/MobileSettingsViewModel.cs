﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class MobileSettingsViewModel
    {

        public Int64 SlNo { get; set; }

        public int MediaViewTime { get; set; }

        public int MemberViewTime { get; set; }

        public string MediaAfterBefore { get; set; }

        public string MemberAfterBefore { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public SelectList MediaAfterBeforeCol { get; set; }

        public SelectList MemberAfterBeforeCol { get; set; }
    }
}