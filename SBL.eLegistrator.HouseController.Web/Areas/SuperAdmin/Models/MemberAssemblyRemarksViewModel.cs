﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class MemberAssemblyRemarksViewModel
    {
        public int MemberAssemblyRemarksID { get; set; }

        [Required(ErrorMessage = "Remark is Required")]
        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string MemberAssemblyRemarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool Active { get; set; }

        public string Mode { get; set; }
    }
}