﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class MemberSMSQuotaViewModel
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Member Name Required")]
        public int MemberCode { get; set; }

        [Required(ErrorMessage = "Add on Value Required")]
        public int AddonValue { get; set; }

        [Required(ErrorMessage = "Add on Date Required")]
        //public string AddonDate { get; set; }
        public DateTime? AddonDate { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public string Mode { get; set; }

        public SelectList MemberNames { get; set; }

        public string GetMemberName { get; set; }

    }
}