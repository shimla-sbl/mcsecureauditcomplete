﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class MobileVersionsViewModel
    {
        public Int64 SlNo { get; set; }

        public string Code { get; set; }

        public int Major { get; set; }

        public int Minor { get; set; }

        public Int64 Build { get; set; }

        public string FilePath { get; set; }

        public string VersionDate { get; set; }

        public string DownloadPath { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public SelectList CodeList { get; set; }
    }
}