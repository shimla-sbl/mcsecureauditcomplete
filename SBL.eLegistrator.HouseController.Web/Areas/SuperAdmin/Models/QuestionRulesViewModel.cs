﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class QuestionRulesViewModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionRuleId { get; set; }

        [Required(ErrorMessage = "QuestionRules Required")]
        [StringLength(300, ErrorMessage = "Maximum 300 characters allowed")]
        public string Rules { get; set; }


        [StringLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string RulesH { get; set; }

        public bool Remarks { get; set; }
        public bool IsTextUse { get; set; }

        public string Mode { get; set; }
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}