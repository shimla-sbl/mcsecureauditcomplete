﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class ContituencyWorksViewModel
    {
        public long workID { get; set; }
        public long WorkTypeCode { get; set; }
        public string WorkTypeName { get; set; }
        public string WorkTypeNameLocal { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public string Mode { get; set; }

    }
}