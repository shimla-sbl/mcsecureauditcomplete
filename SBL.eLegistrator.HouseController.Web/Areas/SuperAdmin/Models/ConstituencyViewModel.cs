﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class ConstituencyViewModel
    {
        public int? ConstituencyID { get; set; }

        [Required(ErrorMessage = "Select Assembly Name")]
        public int? AssemblyID { get; set; }

        [Required(ErrorMessage = "Constituency Code is required")]
        public int ConstituencyCode { get; set; }

        [Required(ErrorMessage = "Constituency Name is Required")]
        [MaxLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string ConstituencyName { get; set; }

       
        [MaxLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string ConstituencyName_Local { get; set; }

        [Required(ErrorMessage = "Select District Name")]
        public int? DistrictCode { get; set; }

        public string ConstituencyCode_CmRefnic { get; set; }

        public string LGDistrictCode { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ConstituencyCreatedDate { get; set; }

        public bool Active { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public SelectList Assembly { get; set; }

        public SelectList District { get; set; }
       
        public int SeletedPageInList { get; set; }

        public string Mode { get; set; }

        public string ReturnMessage { get; set; }

        public string GetAssemblyName { get; set; }

        public string GetDistrictName { get; set; }

        public int? ReservedCategory { get; set; }

        public SelectList ReservedCategoryName { get; set; }

        public string GetReservedCategoryName { get; set; }

        public IEnumerable <ConstituencyViewModel> GetAllConstituency { get; set; }



    }
}