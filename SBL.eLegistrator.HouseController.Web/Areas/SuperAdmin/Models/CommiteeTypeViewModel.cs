﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class CommiteeTypeViewModel
    {
        public int CommitteeTypeId { get; set; }

        [Required(ErrorMessage = "CommitteeType Required")]
        [StringLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string CommitteeTypeName { get; set; }

        //[Required(ErrorMessage = "Is SubCommittee Is Required")]
        public bool IsSubCommittee { get; set; }

        //[Required(ErrorMessage = "Description Required")]
        [StringLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Description { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }

    }
}