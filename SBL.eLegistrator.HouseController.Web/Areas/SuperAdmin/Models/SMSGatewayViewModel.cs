﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class SMSGatewayViewModel
    {

        public int VId { get; set; }

        [Required(ErrorMessage = "Gateway ID Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string VGatewayID { get; set; }

        [Required(ErrorMessage = "Gateway URL Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string VGatewayURL { get; set; }

        [Required(ErrorMessage = "Gateway QString Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string VGatewayQString { get; set; }


        public bool VRequireCountryCode { get; set; }

        public bool VGatewayStatus { get; set; }

        [Required(ErrorMessage = "Success Response Code Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string VSuccessResponseCode { get; set; }

        [Required(ErrorMessage = "MobileNo QString Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string VMobileNoQString { get; set; }


        [Required(ErrorMessage = "SMSText QString Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string VSMSTextQString { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }
        public IEnumerable<SMSGatewayViewModel> Slist { get; set; }
    }
}