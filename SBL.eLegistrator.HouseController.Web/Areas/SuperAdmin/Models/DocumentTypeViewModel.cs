﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class DocumentTypeViewModel
    {

        public int DocumentTypeID { get; set; }

        [Required(ErrorMessage = "Document Type is Required")]
        [MaxLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string DocumentType { get; set; }

        [MaxLength(80, ErrorMessage = "Maximum 80 characters allowed")]
        public string DocumentTypeLocal { get; set; }

        public bool IsActive { get; set; }

        public string Mode { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }
    }
}