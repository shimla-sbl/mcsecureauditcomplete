﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{

    public class TelecomCirclesViewModel
    {

        public Int64 SlNo { get; set; }

        public string StateCode { get; set; }

        public string Description { get; set; }

        public string Mode { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

    }
}