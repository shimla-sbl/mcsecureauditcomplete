﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class StateViewModel
    {

        public int mStateID { get; set; }

        [Required(ErrorMessage = "State Code is Required")]
        [StringLength(5, ErrorMessage = "Maximum 5 digits allowed")]
        public string StateCode { get; set; }

        [Required(ErrorMessage = "State Abbreviation is Required")]
        [StringLength(5, ErrorMessage = "Maximum 5 characters allowed")]
        public string StateAbbreviation { get; set; }

        [Required(ErrorMessage = "State Name is Required")]
        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string StateName { get; set; }


        [StringLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string StateNameLocal { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public IEnumerable<StateViewModel> GetAllState { get; set; }

        public bool IsActive { get; set; }

    }
}