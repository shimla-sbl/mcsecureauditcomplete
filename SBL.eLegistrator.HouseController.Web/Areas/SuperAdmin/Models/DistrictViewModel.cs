﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class DistrictViewModel
    {

        public int DistrictId { get; set; }

        [Required(ErrorMessage = "State Abbreviation is Required")]
        //[MaxLength(3, ErrorMessage = "Maximum 3 characters allowed")]
        public int StateCode { get; set; }

        [Required(ErrorMessage = "District Code is Required")]
        [StringLength(10, ErrorMessage = "Maximum 10 digits allowed")]
        public string DistrictCode { get; set; }

        [Required(ErrorMessage = "District Abbreviation is Required")]
        [StringLength(10, ErrorMessage = "Maximum 10 characters allowed")]
        public string DistrictAbbreviation { get; set; }

        [Required(ErrorMessage = "District Name is Required")]
        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string DistrictName { get; set; }


        [StringLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string DistrictNameLocal { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public SelectList State { get; set; }

        public IEnumerable<StateViewModel> GetAllDistrict { get; set; }

        
        public string GetStateName { get; set; }

    }
}