﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class MinistryDepartmentViewModel
    {

        public int MinistryDepartmentsID { get; set; }

        [Required(ErrorMessage = "Select Assembly Name")]
        public int AssemblyID { get; set; }

        [Required(ErrorMessage = "Select Ministry Name")]
        public int MinistryID { get; set; }

        [Required(ErrorMessage = "Select Department Name")]
        public string DeptID { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Order ID is Required")]
        //[MaxLength(3, ErrorMessage = "Maximum 3 characters allowed")]
        public int? OrderID { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }


        public string Mode { get; set; }

        public SelectList Department { get; set; }

        public SelectList Assembly { get; set; }

        public SelectList Ministry { get; set; }

        public string GetAssemblyName { get; set; }

        public string GetDeptName { get; set; }

        public string GetMinistryName { get; set; }
    }
}