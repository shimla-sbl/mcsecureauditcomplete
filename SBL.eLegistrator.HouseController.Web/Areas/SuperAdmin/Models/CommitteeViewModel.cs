﻿using SBL.DomainModel.Models.Committee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class CommitteeViewModel
    {
        public int CommitteeId { get; set; }

        public int AssemblyID { get; set; }

        public int CommitteeTypeId { get; set; }

        public int? ParentId { get; set; }

        public string CommitteeName { get; set; }

        public string Description { get; set; }
        public SelectList ParentCommittee { get; set; }
        public SelectList Assembly { get; set; }
        public SelectList CommitteeType { get; set; }
        public DateTime? DateofCreation { get; set; }
        public bool IsPublished { get; set; }
        public bool IsActive { get; set; }

        public bool AutoSchedule { get; set; }
        [AllowHtml]
        public string Remark { get; set; }

        public string Mode { get; set; }

        public string AssemblyName { get; set; }
        public string CommitteeTypeName { get; set; }
		public string CommitteeYear { get; set; }
		public int SessionId { get; set; }
        [NotMapped]
        public string Abbreviation { get; set; }
    }

    public class CommitteeList
    {
        public List<CommitteesListView> CommitteeView { get; set; }
        public List<tCommitteeMemberList> committeeMemberList { get; set; }
    }
}