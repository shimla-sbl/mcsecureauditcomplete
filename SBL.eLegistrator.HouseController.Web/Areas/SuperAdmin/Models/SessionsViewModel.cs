﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class SessionsViewModel
    {
        [Required(ErrorMessage = "Select Assembly Name")]
        public int AssemblyID { get; set; }

        public int? SessionID { get; set; }

        [Required(ErrorMessage = "Session Name is Required")]
        [MaxLength(250, ErrorMessage = "Maximum 250 characters allowed")]
        public string SessionName { get; set; }

        [MaxLength(250, ErrorMessage = "Maximum 250 characters allowed")]
        public string SessionNameLocal { get; set; }

        [Required(ErrorMessage = "Session StartDate required")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public string StartDate { get; set; }

       
        public string EndDate { get; set; }

        //[Required(ErrorMessage = "Session Period Is required")]
        [MaxLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string SessionPeriod { get; set; }

        [MaxLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string SessionPeriodLocal { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string SessionDescription { get; set; }

        public int? StaredQuestion { get; set; }

        [Required(ErrorMessage = "Select Session Type")]
        public int? SessionType { get; set; }

        public int? UnstaredQuestions { get; set; }

        public int? BillsIntroduced { get; set; }

        public int? BillsPassed { get; set; }

        public bool? CutMotions { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }


        public int SessionCode { get; set; }

        public bool IsPublished { get; set; }

        public string SessionCalanderLocation { get; set; }

        public int? SessionStatus { get; set; }

        public string RotationMinisterLocation { get; set; }

        public string Mode { get; set; }


        public SelectList SessionTypes { get; set; }

        public SelectList Assembly { get; set; }

    
        public string GetAssemblyName { get; set; }

      
        public string GetSessionType { get; set; }


    }
}