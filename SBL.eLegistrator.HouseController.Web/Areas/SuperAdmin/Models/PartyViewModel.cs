﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class PartyViewModel
    {

        public int PartyID { get; set; }

        [Required(ErrorMessage = "Party Code is Required")]
        [StringLength(10, ErrorMessage = "Maximum 10 characters allowed")]
        public string PartyCode { get; set; }

        [Required(ErrorMessage = "Party Name is Required")]
        [StringLength(60, ErrorMessage = "Maximum 60 characters allowed")]
        public string PartyName { get; set; }


        [StringLength(80, ErrorMessage = "Maximum 80 characters allowed")]
        public string PartyName_Local { get; set; }

        [Column(TypeName = "image")]
        public byte[] Photo { get; set; }

        public bool IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }

        public IEnumerable<PartyViewModel> GetAllParty { get; set; }

    }
}