﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class SubDivisionViewModel
    {
        public int mSubDivisionId { get; set; }

        public string SubDivisionName { get; set; }
        public string SubDivisionNameLocal { get; set; }
        public int ConstituencyID { get; set; }
        public string SubDivisionAbbreviation { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedWhen { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string Mode { get; set; }

        public SelectList SubDivision { get; set; }
        public IEnumerable<SubDivisionViewModel> GetAllConstituencys { get; set; }


        public string GetConstituencyName { get; set; }
        public int AssemblyId { get; set; }

        public SelectList Constituencys { get; set; }
    }
}