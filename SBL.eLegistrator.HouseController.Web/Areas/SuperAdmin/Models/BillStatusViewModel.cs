﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class BillStatusViewModel
    {

        public int BillStatusId { get; set; }

        [Required(ErrorMessage = "Bill Status is required")]
        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string BillStatus { get; set; }

        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string BillStatusLocal { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Description { get; set; }

        public bool Active { get; set; }

        public string Mode { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

    }
}