﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class DesignationViewModel
    {

        public int? Id { get; set; }


       
        public string desigcode { get; set; }

        [Required(ErrorMessage = "Designation Name  is Required")]
        [MaxLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string designame { get; set; }

        [Required(ErrorMessage = "Select Department Name")]
        public string deptid { get; set; }

         [MaxLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string desiglocal { get; set; }

        //[Required(ErrorMessage = "Designation Abbreviation  is Required")]
        //[MaxLength(5, ErrorMessage = "Maximum 5 characters allowed")]
        public string desigabbr { get; set; }

        public string Mode { get; set; }

        public bool Active { get; set; }

        public SelectList Department { get; set; }

        public SelectList District { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public IEnumerable<DesignationViewModel> GetAllDesignations { get; set; }


        public string GetDepartmentName { get; set; }
    }
}