﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class PanchayatViewModel
    {


        public int PanchayatID { get; set; }

        [Required(ErrorMessage = "Code is Required")]
        [MaxLength(12, ErrorMessage = "Maximum 12 characters allowed")]
        public string PanchayatCode { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string PanchayatName { get; set; }

        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string PanchayatNameLocal { get; set; }

        public bool IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string Mode { get; set; }
    }
}