﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class SessionTypeViewModel
    {
       
        public int TypeID { get; set; }

        [Required(ErrorMessage = "SessionType Name is Required")]
        [StringLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string TypeName { get; set; }

         [StringLength(80, ErrorMessage = "Maximum 80 characters allowed")]
        public string TypeNameLocal { get; set; }

        public bool IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }
    }
}