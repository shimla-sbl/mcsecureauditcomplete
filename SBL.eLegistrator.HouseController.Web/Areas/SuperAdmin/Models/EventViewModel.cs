﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class EventViewModel
    {

        public int EventId { get; set; }

        [Required(ErrorMessage = "Event Name is Required")]
        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string EventName { get; set; }

        
        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string EventNameLocal { get; set; }

        [Required(ErrorMessage = " Name is Required")]
        public int? PaperCategoryTypeId { get; set; }

        public bool IsLOB { get; set; }

        public bool IsDepartment { get; set; }

        public bool IsCommittee { get; set; }

        public bool IsProceeding { get; set; }

        public bool IsMember { get; set; }
        [Required(ErrorMessage = "Order is Required")]
        public int? OrderingID { get; set; }

        public string PdfPath { get; set; }

        public DateTime? AddedDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public bool Active { get; set; }

       
        public string RuleNo { get; set; }

        public string Mode { get; set; }

        public SelectList PaperCatTypes { get; set; }

      
        public string GetPapCatName { get; set; }

    }
}