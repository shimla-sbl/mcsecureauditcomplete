﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class NoticeTypeViewModel
    {
        public int NoticeTypeID { get; set; }

        [Required(ErrorMessage = "Name Required")]
        [StringLength(200, ErrorMessage = "Maximum 200 characters allowed")]
        public string NoticeTypeName { get; set; }


        [StringLength(300, ErrorMessage = "Maximum 300 characters allowed")]
        public string NoticeTypeNameLocal { get; set; }

        public bool IsActive { get; set; }
        [StringLength(10)]
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Mode { get; set; }
    }
}