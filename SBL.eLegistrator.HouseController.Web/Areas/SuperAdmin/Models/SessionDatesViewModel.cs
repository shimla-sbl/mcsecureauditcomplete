﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class SessionDatesViewModel
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Select Session Name")]
        public int SessionId { get; set; }

        [Required(ErrorMessage = "Select Assembly Name")]
        public int AssemblyId { get; set; }

        [Required(ErrorMessage = "Session StartDate is required")]       
        public string SessionDate { get; set; }

        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string SessionDateLocal { get; set; }

        public Nullable<System.TimeSpan> SessionTime { get; set; }

        [MaxLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string SessionTimeLocal { get; set; }


        public Nullable<System.TimeSpan> SessionEndTime { get; set; }

        [MaxLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string SessionEndTimeLocal { get; set; }

        public string ModifiedBy { get; set; }

        public Nullable<System.DateTime> ModifiedWhen { get; set; }

        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string SessionDate_Local { get; set; }

        public string RotationMinister { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public Nullable<System.DateTime> CreatedDate { get; set; }

        public string DisplayDate { get; set; }

        public string StartQListPath { get; set; }

        public string UnStartQListPath { get; set; }

        public bool? IsApproved { get; set; }

        public bool? IsApprovedUnstart { get; set; }

        public bool IsSitting { get; set; }

        public string Mode { get; set; }

        //public SelectList Assembly { get; set; }

        //public SelectList Sessions { get; set; }

        public List<mAssembly> AssemblyList { get; set; }

        public List<mSession> SessionList { get; set; }

        [NotMapped]
        public string GetAssemblyName { get; set; }

        [NotMapped]
        public string GetSessionName { get; set; }

        public int SessionDateId { get; set; }

        public string SPPQListPath { get; set; }

        public string UPPQListPath { get; set; }
    }
}