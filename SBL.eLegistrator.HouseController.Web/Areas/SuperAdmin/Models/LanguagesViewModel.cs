﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class LanguagesViewModel
    {

        public int LanguageId { get; set; }
        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string LanguageText { get; set; }

        [Required(ErrorMessage = "Language Name is Required")]
        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string LanguageName { get; set; }

        public bool Defaultlang { get; set; }

        public bool Active { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }
    }
}