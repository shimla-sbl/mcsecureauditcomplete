﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class SecretoryDepartmentViewModel
    {

        public int SecretoryDepartmentID { get; set; }

        [Required(ErrorMessage = "Secretary Name is Required")]
        public int SecretoryID { get; set; }

        [Required(ErrorMessage = "Department Name is Required")]
        public string DepartmentID { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = " Order is Required")]
        public int? OrderID { get; set; }

        [Required(ErrorMessage = "Select Assembly Name")]
        public int? AssemblyID { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string Mode { get; set; }

        public SelectList SecretoryNames { get; set; }

        public SelectList Assembly { get; set; }

        public SelectList Department { get; set; }

        public string GetAssemblyName { get; set; }

        public string GetDeptName { get; set; }

        public string GetSecretoryName { get; set; }

    }
}