﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class HouseCommitteeNotificationViewModel
    {
        public Int64 SlNo { get; set; }

        public string Title { get; set; }

        public string FilePath { get; set; }

        public string DownloadPath { get; set; }

        public string HCNDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }
    }
}