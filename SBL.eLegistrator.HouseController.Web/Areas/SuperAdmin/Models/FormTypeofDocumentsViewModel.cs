﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class FormTypeofDocumentsViewModel
    {

        public int TypeofFormId { get; set; }

        [Required(ErrorMessage = "Form Name is Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string TypeofFormName { get; set; }

        [Required(ErrorMessage = "Description is Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string Description { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public IEnumerable<FormTypeofDocumentsViewModel> GetAllDoc { get; set; }

        public bool IsActive { get; set; }

    }
}