﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class PanchayatVillageViewModel
    {


        public int PanchayatVillageID { get; set; }
        [Required(ErrorMessage = "State Code is Required")]
        public int? StateCode { get; set; }
        [Required(ErrorMessage = "District Code is Required")]
        public int? DistrictCode { get; set; }
        [Required(ErrorMessage = "Panchayat Code is Required")]
        public int? PanchayatCode { get; set; }
        //[Required(ErrorMessage = "Village Code is Required")]
        public int? VillageCode { get; set; }
        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedWhen { get; set; }

        public string Mode { get; set; }

        public SelectList State { get; set; }
        public SelectList District { get; set; }
        public SelectList Panchayat { get; set; }
        public SelectList Village { get; set; }

        //public IEnumerable<StateViewModel> GetAllState { get; set; }
        //public IEnumerable<DistrictViewModel> GetAllDistrict { get; set; }
        //public IEnumerable<PanchayatViewModel> GetAllPanchayat { get; set; }
        //public IEnumerable<VillageViewModel> GetAllVillage { get; set; }

        public string GetStateName { get; set; }
        public string GetDistrictName { get; set; }
        public string GetPanchayatName { get; set; }
        public string GetVillageName { get; set; }

    }
}