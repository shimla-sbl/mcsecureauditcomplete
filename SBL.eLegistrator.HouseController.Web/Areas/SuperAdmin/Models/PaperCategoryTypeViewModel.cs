﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class PaperCategoryTypeViewModel
    {

        public int PaperCategoryTypeId { get; set; }

        [Required(ErrorMessage = "Name Required")]
        [MaxLength(41, ErrorMessage = "Maximum 40 characters allowed")]
        public string Name { get; set; }


        [MaxLength(61, ErrorMessage = "Maximum 60 characters allowed")]
        public string NameLocal { get; set; }

        public bool IsActive { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }



        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public IEnumerable<PaperCategoryTypeViewModel> GetAllPaper { get; set; }



    }
}