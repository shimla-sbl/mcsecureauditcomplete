﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class GovernerViewModel
    {

        public int mGovernerID { get; set; }
        [Required(ErrorMessage = "State is Required")]
        public int mStateID { get; set; }
        public string Prefix { get; set; }
        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }
        public string NameLocal { get; set; }
        [Required(ErrorMessage = "Member Code is Required")]
        public int MemberCode { get; set; }
        [Required(ErrorMessage = "District is Required")]
        public string District { get; set; }
        public string ShimlaAddress { get; set; }
        public string PermanentAddress { get; set; }

        [Required(ErrorMessage = "Gender is Required")]
        public string Sex { get; set; }
        public string TelOffice { get; set; }
        public string TelResidence { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string Description { get; set; }

        [Required(ErrorMessage = "Father Name is Required")]
        public string FatherName { get; set; }
        public bool Active { get; set; }
        public string LoginID { get; set; }
        [StringLength(10)]
        public string CmRefnicVipCode { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }

        public bool? IsDeleted { get; set; }

        
        public string Mode { get; set; }


        public string FullImage { get; set; }

        public string ThumbImage { get; set; }
        public string FileLocation { get; set; }
        public string ImageLocation { get; set; }

        public string ThumbName { get; set; }
       

        public SelectList StateNames { get; set; }
        public string GetStateName { get; set; }

    }
}