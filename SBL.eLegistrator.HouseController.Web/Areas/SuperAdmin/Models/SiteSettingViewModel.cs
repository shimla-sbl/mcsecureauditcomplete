﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class SiteSettingViewModel
    {

        public int SettingId { get; set; }

        [Required(ErrorMessage = "Setting Name is Required")]
        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string SettingName { get; set; }

        [Required(ErrorMessage = "SettingValue is Required")]
        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string SettingValue { get; set; }

        [MaxLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string SettingValueLocal { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public string SignaturePath { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }
    }
}