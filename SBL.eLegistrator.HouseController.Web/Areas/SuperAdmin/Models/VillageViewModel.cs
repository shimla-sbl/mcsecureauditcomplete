﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class VillageViewModel
    {

       
        public int VillageID { get; set; }


        [Required(ErrorMessage = "Village Code is Required")]
        [MaxLength(5, ErrorMessage = "Maximum 5 characters allowed")]
        public string VillageCode { get; set; }

        [Required(ErrorMessage = "Village Name is Required")]
        [MaxLength(50, ErrorMessage = "Maximum 50 characters allowed")]
        public string VillageName { get; set; }

        
        [MaxLength(80, ErrorMessage = "Maximum 80 characters allowed")]
        public string VillageNameLocal { get; set; }

        public bool IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string Mode { get; set; }

    }
}