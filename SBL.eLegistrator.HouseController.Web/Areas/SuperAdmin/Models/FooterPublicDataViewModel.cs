﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class FooterPublicDataViewModel
    {

        public int VID { get; set; }

        [Required(ErrorMessage = "Title ID is Required")]
        public string VTitleID { get; set; }

        [Required(ErrorMessage = "Title Name is Required")]
        public string VTitleName { get; set; }

        [Required(ErrorMessage = "Title Description is Required")]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string VTitleDescription { get; set; }

        public string Mode { get; set; }

        public IEnumerable<FooterPublicDataViewModel> GetAllFooterPublicData { get; set; }

    }
}