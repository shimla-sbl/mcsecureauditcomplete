﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class HighestQualificationViewModel
    {

        
        public int MemberHighestQualificationId { get; set; }

        [Required(ErrorMessage = "Qualification is Required")]
        [StringLength(20, ErrorMessage = "Maximum 20 characters allowed")]
        public string Qualification { get; set; }

        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string QualificationLocal { get; set; }

        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string Description { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public bool IsActive { get; set; }

    }
}