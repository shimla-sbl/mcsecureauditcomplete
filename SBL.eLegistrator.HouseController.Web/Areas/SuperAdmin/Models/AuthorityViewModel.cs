﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Enums;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class AuthorityViewModel
    {

        public AuthorityViewModel()
        {
            ActionsList = new List<SelectListItem>();
        }

        public IEnumerable<SelectListItem> ActionsList { get; set; }

        public int AuthorityId { get; set; }

        [Required(ErrorMessage = "Authority Name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string AuthorityName { get; set; }

        [Required(ErrorMessage = "Prefix Name Required")]
        //[StringLength(5, ErrorMessage = "Maximum 5 characters allowed")]
        public string PriFixName { get; set; }

        [Required(ErrorMessage = "Name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string Name { get; set; }

        //[Required(ErrorMessage = "Name(Hindi) Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string Name_Local { get; set; }

        [Required(ErrorMessage = "Father Name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string FatherName { get; set; }

        //[Required(ErrorMessage = "Father Name(Hindi) Required")]
        [StringLength(60, ErrorMessage = "Maximum 60 characters allowed")]
        public string FatherName_Local { get; set; }

        [Required(ErrorMessage = "Date of Birth Required")]
        public string DateOfBirth { get; set; }

        [Required(ErrorMessage = "Marital Status Required")]
        //[StringLength(8, ErrorMessage = "Maximum 8 characters allowed")]
        public string MaritalStatus { get; set; }

        //[Required(ErrorMessage = "Spouse Name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string SpouseName { get; set; }

        //[Required(ErrorMessage = "Spouse Name(Hindi) Required")]
        [StringLength(60, ErrorMessage = "Maximum 60 characters allowed")]
        public string SpouseName_Local { get; set; }

        [Required(ErrorMessage = "Qualification Required")]
        [StringLength(250, ErrorMessage = "Maximum 250 characters allowed")]
        public string EducationalQualification { get; set; }

        [Required(ErrorMessage = "Present Address Required")]
        [StringLength(300, ErrorMessage = "Maximum 300 characters allowed")]
        public string PresentAddress { get; set; }

        [Required(ErrorMessage = "Permanent Address Required")]
        [StringLength(300, ErrorMessage = "Maximum 300 characters allowed")]
        public string PermanentAddress { get; set; }

        //[Required(ErrorMessage = "Postings Required")]
        public string Postings { get; set; }

        //[Required(ErrorMessage = "Training Required")]
        public string Training { get; set; }

        //[Required(ErrorMessage = "Countries Visited Required")]
        //[StringLength(20, ErrorMessage = "Maximum 20 characters allowed")]
        public string CountriesVisited { get; set; }

        //[Required(ErrorMessage = "Special Interest Required")]
        //[StringLength(20, ErrorMessage = "Maximum 20 characters allowed")]
        public string SpecialInterest { get; set; }

        //[Required(ErrorMessage = "Meetings Attended Required")]
        [AllowHtml]
        public string MeetingsAttended { get; set; }

        //[Required(ErrorMessage = "Designation Description Required")]
        [StringLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string DesignationDescriptions { get; set; }

        //[Required(ErrorMessage = "Photo Required")]
        [Column(TypeName = "image")]
        public byte[] Photo { get; set; }

        //[Required(ErrorMessage = "Date of Marriage Required")]
        public string DateofMarriage { get; set; }

        public string Mobile { get; set; }

       
        public string Email { get; set; }


        public bool IsActive { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }



        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }
        public string Mode { get; set; }

        public IEnumerable<AuthorityViewModel> GetAllAuthority { get; set; }

        public string FullImage { get; set; }

        public string ThumbImage { get; set; }
        public string FileLocation { get; set; }
        public string ImageLocation { get; set; }

        public string ThumbName { get; set; }

        public int? SecretoryId { get; set; }

        public SelectList Secretory { get; set; }


        public string GetSecretoryName { get; set; }

    }
}