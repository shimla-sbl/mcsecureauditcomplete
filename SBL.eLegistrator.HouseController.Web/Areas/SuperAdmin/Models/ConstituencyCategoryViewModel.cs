﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class ConstituencyCategoryViewModel
    {

        public int ConstituencyCategoryId { get; set; }

        [Required(ErrorMessage = "Code is Required")]
        [StringLength(6, ErrorMessage = "Maximum 6 characters allowed")]
        public string ConstituencyCategoryCode { get; set; }

        [Required(ErrorMessage = "Name is Required")]
        [StringLength(20, ErrorMessage = "Maximum 20 characters allowed")]
        public string ConstituencyCategoryName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public IEnumerable<ConstituencyCategoryViewModel> GetAllDoc { get; set; }

        public bool IsActive { get; set; }


    }
}