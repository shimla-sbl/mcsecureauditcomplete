﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class ContituencySchemeEnumsViewModel
    {
        public int ProgramID { get; set; }
        [Required(ErrorMessage = "programmeCode  is Required")]
        public int programmeCode { get; set; }
        [Required(ErrorMessage = "programmeName  is Required")]
        public string programmeName { get; set; }

        public string CreatedBy { get; set; }

        public string ModifiedBy { get; set; }

        public string Mode { get; set; }
    }
}