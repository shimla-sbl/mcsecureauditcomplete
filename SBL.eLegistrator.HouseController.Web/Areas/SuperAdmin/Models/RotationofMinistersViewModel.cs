﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.ROM;
using SBL.DomainModel.Models.Ministery;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    [Serializable]
    public class RotationofMinistersViewModel
    {


        public int VId { get; set; }

        [Required(ErrorMessage = "Assembly  is Required")]

        public int VAssemblyId { get; set; }

        [Required(ErrorMessage = "Session  is Required")]

        public int VSessionId { get; set; }

        [Required(ErrorMessage = "Session Date  is Required")]

        public int VSessionDateId { get; set; }


        public string VMinistryId { get; set; }

        public string VAssemblyName { get; set; }

        public string VSessionName { get; set; }

        public string VSessionDate { get; set; }

        public string VMinisteryMinisterName { get; set; }



        public string Mode { get; set; }

        public List<mAssembly> AssemblyList { get; set; }

        public List<mSession> SessionList { get; set; }

        public List<mSessionDate> SessionDateList { get; set; }

        public List<mMinsitryMinister> MinisteryMinisterList { get; set; }



        public string VCreatedBy { get; set; }

        public DateTime? VCreatedDate { get; set; }


        public DateTime? VModifiedDate { get; set; }

        public string VModifiedBy { get; set; }

        public bool IsCreatNew { get; set; }

        public bool IsActive { get; set; }


        public List<string> MinistryIds { get; set; }
    }
}