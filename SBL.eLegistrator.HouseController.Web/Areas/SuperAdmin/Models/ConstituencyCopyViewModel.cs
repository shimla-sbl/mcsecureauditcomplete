﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class ConstituencyCopyViewModel
    {

        [Required(ErrorMessage = "Select Assembly Name")]
        public int? fromAssemblyID { get; set; }

        [Required(ErrorMessage = "Select Assembly Name")]
        public int? toAssemblyID { get; set; }

        public SelectList Assembly { get; set; }

        public string GetAssemblyName { get; set; }


    }
}