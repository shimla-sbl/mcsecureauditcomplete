﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class AssemblyViewModel
    {
        
        public int?  AssemblyID { get; set; }

        public int AssemblyCode { get; set; }

        [Required(ErrorMessage = " Name is required")]
        [MaxLength(250, ErrorMessage = "Maximum 250 characters allowed")]       
        public string AssemblyName { get; set; }

       [MaxLength(250, ErrorMessage = "Maximum 250 characters allowed")] 
        public string AssemblyNameLocal { get; set; }

        [Required(ErrorMessage = "Start Date is Required")]
       
        public string AssemblyStartDate { get; set; }

      
        public string AssemblyEndDate { get; set; }

        [Required(ErrorMessage = "Period is required")]
        [StringLength(12, ErrorMessage = "Maximum 12 characters allowed")] 
        public string Period { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Rem1 { get; set; }

            [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Rem2 { get; set; }

            [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Rem3 { get; set; }

          [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Rem4 { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Rem5 { get; set; }

        //[StringLength(10)]
        //public Guid ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

       // public Guid CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }

        public bool Active { get; set; }
    }
}