﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class EmployeeViewModel
    {

        public int Id { get; set; }

        //[Required]
        //[StringLength(50)]
        [Required(ErrorMessage = "Employee Code Required")]
        [StringLength(10, ErrorMessage = "Maximum 10 characters allowed")]
        public string empcd { get; set; }

        //[Required]
        //[StringLength(50)]
        [Required(ErrorMessage = "Office Id Required")]
        public string officeid { get; set; }

        //[Required]
        //[StringLength(50)]
        [Required(ErrorMessage = "Employee first name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string empfname { get; set; }

        //[StringLength(50)]

        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string empmname { get; set; }

        //[StringLength(50)]
        // [Required(ErrorMessage = "Employee last name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string emplname { get; set; }

        //[StringLength(100)]
        //[Required(ErrorMessage = "Employee Name(Hindi) Required")]
        [StringLength(60, ErrorMessage = "Maximum 60 characters allowed")]
        public string empnamelocal { get; set; }

        [Required(ErrorMessage = "Date of Birth Required")]
        public string empdob { get; set; }

        //[MaxLength(1)]
        //[Column(TypeName = "char")]
        [Required(ErrorMessage = "F/M/H Required")]
        //[StringLength(1, ErrorMessage = "Maximum 1 character allowed")]
        public string empfmh { get; set; }

        //[StringLength(100)]
        [Required(ErrorMessage = "F/M/H Name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string empfmhname { get; set; }

        //[StringLength(50)]
        //[Required(ErrorMessage = "Spouse Name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string empspouse { get; set; }

        //[MaxLength(1)]
        //[Column(TypeName = "char")]
        //[Required(ErrorMessage = "Religion Required")]
        //[StringLength(10, ErrorMessage = "Maximum 10 characters allowed")]
        public string empreligion { get; set; }

        //[Column(TypeName = "image")]
        public byte[] empphoto { get; set; }

        //[MaxLength(1)]
        //[Column(TypeName = "char")]
        [Required(ErrorMessage = "Gender Required")]
        //[StringLength(1, ErrorMessage = "Maximum 1 character allowed")]
        public string empgender { get; set; }

        //[Required]
        //[MaxLength(7)]
        //[Column(TypeName = "char")]
        [Required(ErrorMessage = "Department Id Required")]
        public string deptid { get; set; }

        public bool IsActive { get; set; }

        //[StringLength(50)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        //[StringLength(50)]
        public string CreatedBy { get; set; }

        public DateTime? CreationDate { get; set; }

        public string Mode { get; set; }

        public IEnumerable<EmployeeViewModel> GetAllEmployee { get; set; }

        public SelectList Department { get; set; }
        public string GetDepartmentName { get; set; }

        //public IEnumerable<SecretoryViewModel> GetAllSecretory { get; set; }


    }
}