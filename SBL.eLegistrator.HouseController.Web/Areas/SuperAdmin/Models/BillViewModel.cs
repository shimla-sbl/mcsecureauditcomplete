﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class BillViewModel
    {

        public int BillTypeId { get; set; }

        [Required(ErrorMessage = "Memorandum Type is Required")]
        [StringLength(150, ErrorMessage = "Maximum 150 characters allowed")]
        public string BillType { get; set; }

        //[Required(ErrorMessage = "Description Required")]
        [StringLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Description { get; set; }

        public bool IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }

        public IEnumerable<BillViewModel> GetAllBill { get; set; }


    }
}