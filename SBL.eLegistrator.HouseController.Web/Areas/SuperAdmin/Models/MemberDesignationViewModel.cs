﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class MemberDesignationViewModel
    {
        
        public int? memDesigId { get; set; }

        public int memDesigcode { get; set; }

        [Required(ErrorMessage = "Designation Name  is Required")]
        [MaxLength(300, ErrorMessage = "Maximum 300 characters allowed")]
        public string memDesigname { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string memDesiglocal { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool Active { get; set; }

        public bool? IsDeleted { get; set; }

        public string Mode { get; set; }
    }
}