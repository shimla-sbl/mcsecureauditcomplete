﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class DepartmentViewModel
    {
        //public int departmentId { get; set; }

        [Required(ErrorMessage = "Department ID Required")]
        [StringLength(7, ErrorMessage = "Maximum 7 characters allowed")]
        public string deptId { get; set; }

        [Required(ErrorMessage = "Abbreviation Required")]
        [StringLength(10, ErrorMessage = "Maximum 10 characters allowed")]
        public string deptabbr { get; set; }

        [Required(ErrorMessage = "Name Required")]
        [StringLength(40, ErrorMessage = "Maximum 40 characters allowed")]
        public string deptname { get; set; }

        //[Required(ErrorMessage = "Name(Hindi) Required")]
        [StringLength(60, ErrorMessage = "Maximum 60 characters allowed")]
        public string deptnameLocal { get; set; }

        //public int? RowNumber { get; set; }

        //public string eSamadhanNote { get; set; }

        //public string eSamadhanOnline { get; set; }

        //public string eSameekshaOnlineYN { get; set; }

        //public int? eSameekshaWorkCode { get; set; }

        //[Required(ErrorMessage = "Abbreviation(Hindi) Required")]
        [StringLength(30, ErrorMessage = "Maximum 30 characters allowed")]
        public string deptabbrLocal { get; set; }

        //public string eSameekshaWorkCodeYear { get; set; }

        public bool IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string Mode { get; set; }

        public IEnumerable<DepartmentViewModel> GetAllDepartment { get; set; }


    }
}