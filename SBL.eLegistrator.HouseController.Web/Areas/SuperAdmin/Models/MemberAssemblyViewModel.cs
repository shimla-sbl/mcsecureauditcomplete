﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models
{
    public class MemberAssemblyViewModel
    {
        public int MemberAssemblyID { get; set; }

        [Required(ErrorMessage = "Select Assembly Name")]
        public int AssemblyID { get; set; }

        [Required(ErrorMessage = "Select Member Name")]
        public int MemberID { get; set; }

        [Required(ErrorMessage = "Select Constituency Name")]
        public int ConstituencyCode { get; set; }

        

        [Required(ErrorMessage = "Member StartDate required")]
      
        public string MemberstartDate { get; set; }

       
        public string MemberEndDate { get; set; }

        public bool Active { get; set; }


        public string Remarks { get; set; }


        public string DeptAssigned { get; set; }

        public byte? LevelCode { get; set; }

        public bool? IsWomenReservation { get; set; }

        public bool? IsSCSTReservation { get; set; }


        public string Category { get; set; }

        public int? AuthorityOrder { get; set; }

        //[Required(ErrorMessage = "Select Designation ")]
        public string Designation { get; set; }

        [Required(ErrorMessage = "Select Member Designation Name")]
        public int? DesignationID { get; set; }

        [Required(ErrorMessage = "Select Party Name")]
        public int? PartyID { get; set; }


        public string PartyCode { get; set; }


        public string ProfiePicPath { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Para1 { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Para2 { get; set; }

       [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Para3 { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Para4 { get; set; }

         [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string Para5 { get; set; }


        public string sinterest { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string languagesknown { get; set; }

        [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string travels { get; set; }


        public string timepass { get; set; }


        public string sports { get; set; }


        public string Awards { get; set; }

       [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string SocialActivities { get; set; }


        public string PublishedWork { get; set; }

       [MaxLength(500, ErrorMessage = "Maximum 500 characters allowed")]
        public string ConferencesAttended { get; set; }

        [Required(ErrorMessage = "Select Location")]
        public string Location { get; set; }


        public string mMemberOldID { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string Mode { get; set; }

        public SelectList MemberNames { get; set; }

        public SelectList Locations { get; set; }

        public SelectList Assembly { get; set; }

        public SelectList ConstituencyNames { get; set; }

        public SelectList PartryNames { get; set; }

        public string GetAssemblyName { get; set; }

        public string GetMemberName { get; set; }

        public string GetConstituencyName { get; set; }

        public string GetpartyName { get; set; }

        public string GetMemberDesignationName { get; set; }

        public SelectList DesignationList { get; set; }



    }
}