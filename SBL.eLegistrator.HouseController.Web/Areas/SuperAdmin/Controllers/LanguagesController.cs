﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Language;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class LanguagesController : Controller
    {
        //
        // GET: /SuperAdmin/Languages/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Lang = (List<Languages>)Helper.ExecuteService("Language", "GetAllLanguages", null);
                var model = Lang.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Language Details";
                return View("AdminErrorPage");
                throw ex;
            }
          
        }

        public ActionResult CreateLanguages()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new LanguagesViewModel()
                {
                    Mode = "Add",
                    Active = true
                };
                return View("CreateLanguages", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Language Details";
                return View("AdminErrorPage");
                throw ex;
            }
          
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveLanguages(LanguagesViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Language = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        if (model.Defaultlang == true)
                        {
                            var data = (Languages)Helper.ExecuteService("Language", "GetAllActiveLanguages", null);
                            if (data != null)
                            {
                                bool data1 = data.Defaultlang;
                                data.Defaultlang = data1 == false;
                                Helper.ExecuteService("Language", "UpdateLanguage", data);
                            }

                            Helper.ExecuteService("Language", "CreateLanguage", Language);
                        }
                        else
                        {

                            Helper.ExecuteService("Language", "CreateLanguage", Language);
                        }

                    }
                    else
                    {
                        if (model.Defaultlang == true)
                        {
                            var data = (Languages)Helper.ExecuteService("Language", "GetAllActiveLanguages", null);
                            if (data != null)
                            {
                                bool data1 = data.Defaultlang;
                                data.Defaultlang = data1 == false;
                                Helper.ExecuteService("Language", "UpdateLanguage", data);
                            }

                            Helper.ExecuteService("Language", "UpdateLanguage", Language);
                        }
                        else
                        {
                            Helper.ExecuteService("Language", "UpdateLanguage", Language);
                        }

                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Language Details";
                return View("AdminErrorPage");
                throw ex;
            }
         

        }

        public ActionResult EditLanguages(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //Languages LangToEdit = (Languages)Helper.ExecuteService("Language", "GetLanguageBasedOnId", new Languages { LanguageId = Id });
                Languages LangToEdit = (Languages)Helper.ExecuteService("Language", "GetLanguageBasedOnId", new Languages { LanguageId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = LangToEdit.ToViewModel1("Edit");
                return View("CreateLanguages", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Language Details";
                return View("AdminErrorPage");
                throw ex;
            }
         

        }

        public ActionResult DeleteLanguages(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                Languages MobiSettToRemove = (Languages)Helper.ExecuteService("Language", "GetLanguageBasedOnId", new Languages { LanguageId = Id });
                Helper.ExecuteService("Language", "DeleteLanguage", MobiSettToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Language Details";
                return View("AdminErrorPage");
                throw ex;
            }
           

        }


    }
}
