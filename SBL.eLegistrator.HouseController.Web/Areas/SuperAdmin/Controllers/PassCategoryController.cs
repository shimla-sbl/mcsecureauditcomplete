﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Passes;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.Passes.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class PassCategoryController : Controller
    {
        //
        // GET: /SuperAdmin/PassCategory/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var PassCategory = (List<PassCategory>)Helper.ExecuteService("Pass", "GetAllPassCategories", null);
                var model = PassCategory.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Pass Category Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult Create()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new PassCategoryViewModel
           {
               Mode = "Add",
               IsActive = true
           };
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Pass Category Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult Edit(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var PassCategory = (PassCategory)Helper.ExecuteService("Pass", "GetPassCategoryById", new PassCategory { PassCategoryID = Id });
                var PassCategory = (PassCategory)Helper.ExecuteService("Pass", "GetPassCategoryById", new PassCategory { PassCategoryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                return View("Create", PassCategory.ToViewModel("Edit"));
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Pass Category Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SavePassCategory(PassCategoryViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Pass", "CreatePassCategory", model.ToDomainModel());
                }
                else

                    Helper.ExecuteService("Pass", "UpdatePassCategory", model.ToDomainModel());
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Pass Category Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult DeletePassCategory(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Organization = Helper.ExecuteService("Pass", "DeletePassCategory", new PassCategory { PassCategoryID = Id });
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Pass Category Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }


    }
}
