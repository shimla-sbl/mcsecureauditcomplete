﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.HOD;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class HODController : Controller
    {
        //
        // GET: /SuperAdmin/HOD/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mHOD>)Helper.ExecuteService("HOD", "GetAllHODDetails", null);
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Head of Department Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateHOD()
        {
            HODViewModel model = new HODViewModel();

            var Departments = (List<mDepartment>)Helper.ExecuteService("HOD", "GetAllDepartment", null);

            model.DeptCol = new SelectList(Departments, "deptId", "deptname");

            model.PrefixCol = new SelectList(Enum.GetValues(typeof(Prefix)).Cast<Prefix>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            model.Mode = "Add";

            model.IsActive = true;

            return View(model);
        }

        public ActionResult EditHOD(string Id)
        {
            var Departments = (List<mDepartment>)Helper.ExecuteService("HOD", "GetAllDepartment", null);

            //mHOD stateToEdit = (mHOD)Helper.ExecuteService("HOD", "GetHODById", new mHOD { HODID = Id });
            mHOD stateToEdit = (mHOD)Helper.ExecuteService("HOD", "GetHODById", new mHOD { HODID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            
            var model = stateToEdit.ToViewModel1(Departments, "Edit");

            model.PrefixCol = new SelectList(Enum.GetValues(typeof(Prefix)).Cast<Prefix>().Select(v => new SelectListItem
            {
                Text = v.ToString(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            model.Mode = "Edit";

            return View("CreateHOD", model);
        }

        public ActionResult DeleteHOD(int Id)
        {
            mHOD stateToDelete = (mHOD)Helper.ExecuteService("HOD", "GetHODById", new mHOD { HODID = Id });

            Helper.ExecuteService("HOD", "DeleteHOD", stateToDelete);
            
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveHOD(HODViewModel model)
        {
            if (ModelState.IsValid)
            {
                var mHOD = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("HOD", "CreateHOD", mHOD);
                }
                else
                {
                    Helper.ExecuteService("HOD", "UpdateHOD", mHOD);
                }
            }

            return RedirectToAction("Index");
        }
    }
}
