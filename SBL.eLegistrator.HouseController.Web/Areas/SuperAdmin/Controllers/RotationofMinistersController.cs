﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.ROM;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
#pragma warning disable CS0105 // The using directive for 'SBL.eLegistrator.HouseController.Web.Extensions' appeared previously in this namespace
using SBL.eLegistrator.HouseController.Web.Extensions;
#pragma warning restore CS0105 // The using directive for 'SBL.eLegistrator.HouseController.Web.Extensions' appeared previously in this namespace
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class RotationofMinistersController : Controller
    {
        //
        // GET: /SuperAdmin/RotationofMinisters/

        //public ActionResult Index()
        //{
        //    return View();
        //}


        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult GetAllRotationofMinisters()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                //FileAccessingUrlPath
                //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var tRotationMinister = (List<tRotationMinister>)Helper.ExecuteService("RotationofMinisters", "GetAllRotationofMinisters", null);

                var model = tRotationMinister.ToViewModel1();
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Getting All Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Getting the Asked From Details";
                return View("AdminErrorPage");
            }

        }



        public ActionResult CreateRotationofMinisters()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                RotationofMinistersViewModel model = new RotationofMinistersViewModel();

                //var Ministery = (List<mMinistry>)Helper.ExecuteService("RotationofMinisters", "GetAllMinistery", null);
                var MinisteryMinisterslist = (List<mMinsitryMinister>)Helper.ExecuteService("RotationofMinisters", "GetAllMinisteryMinisters", null);
              

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
                model.VAssemblyId = data.AssemblyID;
                model.VSessionId = sesdate.SessionId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;

                model.MinisteryMinisterList = MinisteryMinisterslist;
                //new SelectList(MinisteryMinisterslist, "MinsitryMinistersID", "MinistryName");

                model.Mode = "Add";
                model.IsActive = true;
                return View("CreateRotationofMinisters", model);

            }
            catch (Exception ex)
            {

                ErrorLog.WriteToLog(ex, "error");
                //RecordError(ex, "Creat Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Creating the Asked From Details";
                return View("AdminErrorPage");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveRotationofMinisters(RotationofMinistersViewModel model)
        {


            try
            {



                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                // var domainmodel = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    //string FileName = file.FileName;

                    var data = model.ToDomainModel();

                    data.CreatedBy = CurrentSession.UserID;
                    data.ModifiedBy = CurrentSession.UserID;

                    if (model.MinistryIds != null)
                    {
                        foreach (var item in model.MinistryIds)
                        {

                            string ResultString = string.Join(",", model.MinistryIds.ToArray());
                            data.MinistryId = ResultString;
                        }
                    }

                    Helper.ExecuteService("RotationofMinisters", "AddRotationofMinisters", data);

                }
                else
                {
                    var data = model.ToDomainModel();

                    data.ModifiedBy = CurrentSession.UserID;

                    if (model.MinistryIds != null)
                    {
                        foreach (var item in model.MinistryIds)
                        {

                            string ResultString = string.Join(",", model.MinistryIds.ToArray());
                            data.MinistryId = ResultString;
                        }
                    }
                    Helper.ExecuteService("RotationofMinisters", "UpdateRotationofMinisters", data);

                }
                if (model.IsCreatNew)
                {
                    //return RedirectToAction("CreateRotationofMinisters");

                    //var Ministery = (List<mMinistry>)Helper.ExecuteService("RotationofMinisters", "GetAllMinistery", null);
                    var MinisteryMinisterslist = (List<mMinsitryMinister>)Helper.ExecuteService("RotationofMinisters", "GetAllMinisteryMinisters", null);


                    //Code for AssemblyList 
                    var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                    //Code for SessionList 
                    mSession data = new mSession();
                    //data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                    data.AssemblyID = model.VAssemblyId;
                    var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                    //Code for SessionDateList 

                    mSessionDate sesdate = new mSessionDate();
                    //sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                    sesdate.SessionId = model.VSessionId;
                    sesdate.AssemblyId = data.AssemblyID;
                    
                    var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                    model.SessionDateList = sessionDateList;
                    model.AssemblyList = assmeblyList;
                    model.SessionList = sessionList;

                    model.MinisteryMinisterList = MinisteryMinisterslist;
                    //new SelectList(MinisteryMinisterslist, "MinsitryMinistersID", "MinistryName");

                    ViewBag.Success = "Data Successfully Submitted!!";

                    return View("CreateRotationofMinisters", model);



                }
                else
                {
                    return RedirectToAction("GetAllRotationofMinisters");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Saving Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Saving the Asked From Details";
                return View("AdminErrorPage");
            }


        }

        public ActionResult EditRotationofMinisters(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                tRotationMinister data = new tRotationMinister();
                //data.Id = Id;
                data.Id = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
                var serData = (tRotationMinister)Helper.ExecuteService("RotationofMinisters", "GetRotationofMinistersById", data);

                var reldata = serData.ToViewModel1("Edit");
                string s = reldata.VMinistryId;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');
                    reldata.MinistryIds = new List<string>();
                    for (int i = 0; i < values.Length; i++)
                    {
                        reldata.MinistryIds.Add(values[i]);
                    }

                }
                var MinisteryMinisterslist = (List<mMinsitryMinister>)Helper.ExecuteService("RotationofMinisters", "GetAllMinisteryMinisters", null);
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession sesdata = new mSession();
                sesdata.AssemblyID = reldata.VAssemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sesdata);

                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = reldata.VSessionId;
                sesdate.AssemblyId = reldata.VAssemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                // Code for DoumentTypeList
                //var doctypeList = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyTypeofDocumentLst", null);
                reldata.SessionDateList = sessionDateList;
                reldata.AssemblyList = assmeblyList;
                reldata.SessionList = sessionList;
                reldata.MinisteryMinisterList = MinisteryMinisterslist;
                //reldata.AssemblyDocumentTypeList = doctypeList;
                //reldata.StatusTypeList = ModelMapping.GetStatusTypes();

                return View("CreateRotationofMinisters", reldata);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Edit Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Editing Asked From Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult DeleteRotationofMinisters(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                tRotationMinister data = new tRotationMinister();
                data.Id = Id;
                // var serData = (tAssemblyFiles)Helper.ExecuteService("AssemblyFileSystem", "GetAssemblyFileById", data);
                // RemoveExistingFile(serData

                var isSucess = (bool)Helper.ExecuteService("RotationofMinisters", "DeleteRotationofMinistersById", data);

                return RedirectToAction("GetAllRotationofMinisters");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Deleting Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Deleting the Asked From Details";
                return View("AdminErrorPage");
            }



        }





        public ActionResult ChangeAssembly(int assemblyId)
        {
            try
            {


                mSession data = new mSession();
                data.AssemblyID = assemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                return Json(sessionList, JsonRequestBehavior.AllowGet);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Getting All Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Change Assembly of Asked From Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult ChangeSession(int sessionId, int assemblyId)
        {

            try
            {

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionId;
                sesdate.AssemblyId = assemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                return Json(sessionDateList, JsonRequestBehavior.AllowGet);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Change Session of Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Change Session of Asked From Details";
                return View("AdminErrorPage");
            }
        }

    }
}
