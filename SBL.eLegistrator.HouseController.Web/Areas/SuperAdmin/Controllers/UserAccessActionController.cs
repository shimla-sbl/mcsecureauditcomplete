﻿using SBL.DomainModel.Models.UserAction;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Controllers;
using SBL.eLegistrator.HouseController.Web.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class UserAccessActionController : Controller
    {
        //
        // GET: /UserManagement/UserAccessAction/


        public ActionResult Index()
        {
            return View();
        }



        public ActionResult GetUserAccessAction(int? ID)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserName))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            tUserAccessActions model = new tUserAccessActions();

            tUserAccessActionsModel objModel = new tUserAccessActionsModel();

            //For Sub User Type
            mSubUserType Submodel = new mSubUserType();
            var returnedSubUserTypeResult = Helper.ExecuteService("Module", "GetSubUserTypeDropDown", Submodel) as List<mSubUserType>;
            var returnedSubUserTypeResultNew = returnedSubUserTypeResult.ToSubUserTypeListForAccessAction();
            //For Sub User Type
            objModel = (tUserAccessActionsModel)Helper.ExecuteService("Module", "GetUserAccessAction", objModel);
            if (returnedSubUserTypeResultNew.Count() > 0 && returnedSubUserTypeResultNew!=null)
            {
                objModel.SubUserTypeList = returnedSubUserTypeResultNew;
            }
            if (ID != null && ID>0)
            {
                objModel.objModelList = objModel.objModelList.Where(x => x.SubUserTypeID ==Convert.ToInt32(ID)).ToList();
            }
            ViewBag.SelectedValue = ID;
            return PartialView("_GetUserAccessAction", objModel);
        }

        public ActionResult CreateNewUserAccessAction()
        {
            // For User Type
            mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToUserAccessActionList();

            //For Sub User Type
            mSubUserType Submodel = new mSubUserType();
            var returnedSubUserTypeResult = Helper.ExecuteService("Module", "GetSubUserTypeDropDown", Submodel) as List<mSubUserType>;
            var returnedSubUserTypeResultNew = returnedSubUserTypeResult.ToSubUserTypeListForAccessAction();

            //For User Access
            mUserModules Accessmodel = new mUserModules();
            var returnedAccessmodel = Helper.ExecuteService("Module", "GetUserAccessDropDown", model) as List<mUserModules>;
            var returnedAccessmodelNew = returnedAccessmodel.ToAccessList();

            //For User sub Access
            mUserSubModules submodule = new mUserSubModules();
            var returnedsubAccessmodel = Helper.ExecuteService("Module", "GetUserSubAccessDropDown", model) as List<mUserSubModules>;
            var returnedsubAccessmodelNew = returnedsubAccessmodel.ToSubAccessList();

            //For User Action

            mUserActions Actionmodel = new mUserActions();
            var returnedActionmodel = Helper.ExecuteService("Module", "GetUserActionDropDown", model) as List<mUserActions>;
            var returnedActionmodelNew = returnedActionmodel.ToActionList();

            var model1 = new tUserAccessActions()
            {
                Mode = "Add",
                objList = returnedResultNew,
                AccessList = returnedAccessmodelNew,
                ActionList = returnedActionmodelNew,
                SubUserTypeList = returnedSubUserTypeResultNew,
                SubAccessList = returnedsubAccessmodelNew,

            };


            return PartialView("_CreateNewUserAccessAction", model1);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveUserAccessAction(tUserAccessActions model, FormCollection coll, string SubModuleIds)
        {
            var ff = coll["SubModuleIds"];
            if (ModelState.IsValid)
            {
                model.ModifiedWhen = DateTime.Now;
                var dist = model.ToSaveUserAccessAction();
                if (model.Mode == "Add")
                {

                    foreach (var item in model.ActionIds)
                    {

                        string ResultString = string.Join(",", model.ActionIds.ToArray());

                        dist.ActionComma = ResultString;
                    }
                    foreach (var item in model.SubModuleIds)
                    {

                        string ResultString = string.Join(",", model.SubModuleIds.ToArray());

                        dist.SubModuleComma = ResultString;
                    }
                    Helper.ExecuteService("Module", "SaveUserAccessAction", dist);
                }
                else
                {
                    foreach (var item in model.ActionIds)
                    {

                        string ResultString = string.Join(",", model.ActionIds.ToArray());
                        dist.ActionComma = ResultString;
                    }
                    foreach (var item in model.AllMergeSubModuleIds)
                    {

                        string ResultString = string.Join(",", model.AllMergeSubModuleIds.ToArray());

                        dist.SubModuleComma = ResultString;
                    }
                    Helper.ExecuteService("Module", "UpdateUserAccessAction", dist);
                }

                return RedirectToAction("GetUserAccessAction");
            }
            else
            {

                return RedirectToAction("GetUserAccessAction");
            }

        }



        public ActionResult GetEditUserAccsessAction(string UserAccessActionsId)
        {

            // For User Type
            mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToUserAccessActionList();


            //For Sub User Type
            mSubUserType Submodel = new mSubUserType();
            var returnedSubUserTypeResult = Helper.ExecuteService("Module", "GetSubUserTypeDropDown", Submodel) as List<mSubUserType>;
            var returnedSubUserTypeResultNew = returnedSubUserTypeResult.ToSubUserTypeListForAccessAction();

            //For User Access
            mUserModules Accessmodel = new mUserModules();
            var returnedAccessmodel = Helper.ExecuteService("Module", "GetUserAccessDropDown", model) as List<mUserModules>;
            var returnedAccessmodelNew = returnedAccessmodel.ToAccessList();
            //For User sub Access
            mUserSubModules submodule = new mUserSubModules();
            var returnedsubAccessmodel = Helper.ExecuteService("Module", "GetUserSubAccessDropDown", model) as List<mUserSubModules>;
            var returnedsubAccessmodelNew = returnedsubAccessmodel.ToSubAccessList();

            //For User Action

            mUserActions Actionmodel = new mUserActions();
            var returnedActionmodel = Helper.ExecuteService("Module", "GetUserActionDropDown", model) as List<mUserActions>;
            var returnedActionmodelNew = returnedActionmodel.ToActionList();

            var model1 = new tUserAccessActionsModel()
            {
                //Mode = "Add",
                objList = returnedResultNew,
                AccessList = returnedAccessmodelNew,
                ActionList = returnedActionmodelNew,
                SubUserTypeList = returnedSubUserTypeResultNew,
                SubAccessList = returnedsubAccessmodelNew,
            };

            tUserAccessActionsModel UserAccessActionToEdit = (tUserAccessActionsModel)Helper.ExecuteService("Module", "GetUserAccessActionDataById", new tUserAccessActionsModel { UserAccessActionsId = Convert.ToInt32(EncryptionUtility.Decrypt(UserAccessActionsId.ToString())) });

            var model2 = UserAccessActionToEdit.ToViewUserAccessActionModel("Edit");
            model1.UserTypeID = model2.UserTypeID;
            model1.UserTypeName = model2.UserTypeName;
            model1.ActionId = model2.ActionId;
            model1.ActionName = model2.ActionName;
            model1.ModuleId = model2.ModuleId;
            model1.ModuleName = model2.ModuleName;
            model1.SubUserTypeID = model2.SubUserTypeID;
            model1.SubUserTypeName = model2.SubUserTypeName;
            model1.UserAccessActionsId = model2.UserAccessActionsId;
            model1.MergeActionId = UserAccessActionToEdit.MergeActionId;
            model1.MergeSubModuleId = model2.MergeSubModuleId;
            model1.AccessActionOrder = model2.AccessActionOrder;
            if (!string.IsNullOrEmpty(model2.MergeSubModuleId))
            {
                model1.AllMergeSubModuleIds = model2.MergeSubModuleId.Split(',').ToList();//by ashwani
            }
            string s = model1.MergeActionId;
            if (s != null && s != "")
            {
                string[] values = s.Split(',');
                model1.ActionIds = new List<string>();
                for (int i = 0; i < values.Length; i++)
                {
                    model1.ActionIds.Add(values[i]);
                }

            }
            return View("_UpdateNewUserAccessAction", model1);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUserAccessAction(tUserAccessActions model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToSaveUserAccessAction();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "UpdateUserAccessAction", dist);
                }
                else
                {

                    foreach (var item in model.ActionIds)
                    {

                        string ResultString = string.Join(",", model.ActionIds.ToArray());
                        dist.ActionComma = ResultString;
                    }

                    Helper.ExecuteService("Module", "UpdateUserAccessAction", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }


        public ActionResult DeleteUserAccessAction(string Id)
        {
            tUserAccessActions obj = new tUserAccessActions();
            tUserAccessActionsModel UserAccessActionToDelete = (tUserAccessActionsModel)Helper.ExecuteService("Module", "GetUserAccessActionDataById", new tUserAccessActionsModel { UserAccessActionsId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

            //tUserAccessActions UserAccessActionToDelete = (tUserAccessActions)Helper.ExecuteService("Module", "GetUserAccessActionDataById", new tUserAccessActions { UserAccessActionsId = UserAccessActionsId });
            obj.UserAccessActionsId = UserAccessActionToDelete.UserAccessActionsId;

            Helper.ExecuteService("Module", "DeleteUserAccessAction", obj);
            return RedirectToAction("GetUserAccessAction");

        }

        public JsonResult CheckUserAccessAction(int UserType, int UserAccess, String UserAction, int SubUserType)
        {
            tUserAccessActions model = new tUserAccessActions();

            model.UserTypeID = UserType;
            model.ModuleId = UserAccess;
            model.MergeActionId = UserAction;
            model.SubUserTypeID = SubUserType;

            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckUserAccessActionExist", model);
                //var mdl1 = (bool)Helper.ExecuteService("Module", "CheckUserAccessIdExist", UserAccess);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }

#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }

        public JsonResult CheckUserAccessIdExist(int UserAccess)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckUserAccessIdExist", UserAccess);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }


        public ActionResult ChangeUserType(int usertypeid)
        {
            try
            {
                mSubUserType data = new mSubUserType();
                data.UserTypeID = usertypeid;
                var SubUserTypeList = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserTypeByUserTypeId", data);

                return Json(SubUserTypeList, JsonRequestBehavior.AllowGet);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                return View("AdminErrorPage");
            }
        }
        public ActionResult ChangeUserModule(int moduleid)
        {
            try
            {
                mUserSubModules data = new mUserSubModules();
                data.ModuleId = moduleid;
                var SubAccessList = (List<mUserSubModules>)Helper.ExecuteService("Module", "GetSubModuleByModuleId", data);

                return Json(SubAccessList, JsonRequestBehavior.AllowGet);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                return View("AdminErrorPage");
            }
        }


        public ActionResult ChangeSubUserType(int subusertypeid)
        {
            try
            {

                mUserModules data = new mUserModules();
                data.SubUserTypeID = subusertypeid;
                var UserAccessList = (List<mUserModules>)Helper.ExecuteService("Module", "GetUseraccessBySubUserTypeId", data);

                return Json(UserAccessList, JsonRequestBehavior.AllowGet);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Getting All Assembly File");
                //ViewBag.ErrorMessage = "Their is a Error While Deleting the Gallery Details";
                return View("AdminErrorPage");
            }
        }

       
      
        public ActionResult UpdateAccessActionOrder(int Order, string AccessActionID)
        {
            try
            {
                tUserAccessActions objtUserAccessActions = new tUserAccessActions();
                objtUserAccessActions.AccessActionOrder = Order;
                objtUserAccessActions.UserAccessActionsId = Convert.ToInt32(EncryptionUtility.Decrypt(AccessActionID.ToString()));
                Helper.ExecuteService("Module", "UpdateAccessActionOrder", objtUserAccessActions);
       
                return Json("Update Successfully", JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json("Error", JsonRequestBehavior.AllowGet);
            }

        }

    }
}
