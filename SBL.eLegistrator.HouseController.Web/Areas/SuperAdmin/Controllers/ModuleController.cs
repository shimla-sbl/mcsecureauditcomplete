﻿using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.eLegistrator.HouseController.Web.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class ModuleController : Controller
    {
        //
        // GET: /UserManagement/Module/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserManagement()
        {
            return View();
        }

        public ActionResult GetUserModules()
        {
            var Modules = (List<mUserModules>)Helper.ExecuteService("Module", "GetUserModules", null);
            var model1 = Modules.ToViewUserModel();

            return PartialView("_GetUserModule", model1);
        }



        public ActionResult CreateNewModule()
        {

            //For Sub User Type
            mSubUserType Submodel = new mSubUserType();
            var returnedSubUserTypeResult = Helper.ExecuteService("Module", "GetSubUserTypeDropDown", Submodel) as List<mSubUserType>;
            var returnedSubUserTypeResultNew = returnedSubUserTypeResult.ToSubUserTypeListForAccess();

           mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToModelList();
              

            var model1 = new mUserModules()
            {
                Mode = "Add",
                objList = returnedResultNew,
                SubUserTypeList = returnedSubUserTypeResultNew,
            };
                   
            return PartialView("_CreateNewModule", model1);

        }        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveModule(mUserModules model)
        {
            model.ModuleOrder = model.ModuleOrderTemp;
            if (ModelState.IsValid)
            {
                var dist = model.ToModuleModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateModule", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntryModules", dist);
                }

                return RedirectToAction("GetUserModules");
            }
            else
            {

                return RedirectToAction("GetUserModules");
            }

        }


 

        public ActionResult EditUserModule(string Id)
        {

            mUserModules ModuleToEdit = (mUserModules)Helper.ExecuteService("Module", "GetModuleDataById", new mUserModules { ModuleId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });


            //For Sub User Type
            mSubUserType Submodel = new mSubUserType();
            var returnedSubUserTypeResult = Helper.ExecuteService("Module", "GetSubUserTypeDropDown", Submodel) as List<mSubUserType>;
            var returnedSubUserTypeResultNew = returnedSubUserTypeResult.ToSubUserTypeListForAccess();
          

            var model = ModuleToEdit.ToViewModuleModel("Edit");
            model.ModuleId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
            model.ModuleName = model.ModuleName;
            model.ModuleNameLocal = model.ModuleNameLocal;
            model.ModuleDescription = model.ModuleDescription;
            model.SubUserTypeID = model.SubUserTypeID;
            model.SubUserTypeName = model.SubUserTypeName;
            model.SubUserTypeList = returnedSubUserTypeResultNew;
            return View("_CreateNewModule", model);
        }




        public ActionResult DeleteModule(string Id)
        {
            mUserModules moduleToDelete = (mUserModules)Helper.ExecuteService("Module", "GetModuleDataById", new mUserModules { ModuleId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            Helper.ExecuteService("Module", "DeleteModule", moduleToDelete);
            return RedirectToAction("GetUserModules");

        }



        public JsonResult CheckUserAccess(string UserAccessName)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckUserAccessExist", UserAccessName);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }


        public JsonResult fillOrderNo(int UserType, string Mode)
        {
           
            List<ModuleOrderList> _list = (List<ModuleOrderList>)Helper.ExecuteService("Module", "getOrderNo", UserType);
           
            ModuleOrderList _addItem = new ModuleOrderList();

            if (_list.Count > 0)
            {
                if (Mode == "save")
                {
                    _addItem._OrderID = _list[0]._OrderID + 1;
                    _list.Insert(0, _addItem);
                }
                //else
                //{
                //int i = 1;
                //List<ModuleOrderList> _list2 = new List<ModuleOrderList>();
                //    foreach (var item in _list)
                //    {
                //        if (item._OrderID == 0)
                //        {
                //            item._OrderID = i;
                //            _list2.Add(item);
                //        }
                //        i = i + 1;
                //    }
                //    _list = new List<ModuleOrderList>();
                //    _list = _list2;
                   // _addItem._OrderID = _list[0]._OrderID + 1;
                   // _list.Insert(0, _addItem);
                //}
            }
            else
            {
                _addItem._OrderID = 1;
                _list.Insert(0, _addItem);
            }

            return Json(_list, JsonRequestBehavior.AllowGet);
        }

      
    }
}
