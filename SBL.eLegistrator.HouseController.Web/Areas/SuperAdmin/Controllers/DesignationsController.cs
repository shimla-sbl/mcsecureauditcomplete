﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.PISModules;
using SBL.DomainModel.Models.PISMolues;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Models;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class DesignationsController : Controller
    {
        //
        // GET: /SuperAdmin/Designations/

        //public ActionResult Index()
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserName))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }
        //        var Designation = (List<mPISDesignation>)Helper.ExecuteService("Designation", "GetAllDesignation", null);
        //        var model = Designation.ToViewModel();
        //        return View(model);
        //    }
        //    catch (Exception ex)
        //    {

        //        ViewBag.ErrorMessage = "There is a Error While Getting Designation Details";
        //        return View("AdminErrorPage");
        //        throw ex;
        //    }

        //}



        public ActionResult Index(int pageId = 1, int pageSize = 50)
        {

            //int pageId = 1;
            //int pageSize = 10;
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Designation = (List<mPISDesignation>)Helper.ExecuteService("Designation", "GetAllDesignation", null);
                var model = Designation.ToViewModel();
                ViewBag.PageSize = pageSize;
                List<DesignationViewModel> pagedRecord = new List<DesignationViewModel>();
                if (pageId == 1)
                {
                    pagedRecord = model.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = model.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(Designation.Count, pageId, pageSize);
                return View(pagedRecord);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }




        public PartialViewResult GetDesignationByPaging(int pageId, int pageSize)
        {
            var Designation = (List<mPISDesignation>)Helper.ExecuteService("Designation", "GetAllDesignation", null);
            ViewBag.PageSize = pageSize;
            List<mPISDesignation> pagedRecord = new List<mPISDesignation>();
            if (pageId == 1)
            {
                pagedRecord = Designation.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Designation.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Designation.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
            return PartialView("/Areas/SuperAdmin/Views/Designations/_Index.cshtml", model);
        }






        public ActionResult CreateDesignation()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Dept = (List<mDepartment>)Helper.ExecuteService("Designation", "GetAllDepartments", null);

                var model = new DesignationViewModel()
                {
                    Department = new SelectList(Dept, "deptId", "deptname"),

                    Mode = "Add",
                    Active = true

                };
                return View("CreateDesignation", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Creating Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDesignation(DesignationViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Designation = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        var DesigCode = (string)Helper.ExecuteService("Designation", "GetDesigcode", null);
                        var dd = Convert.ToInt32(DesigCode);
                        if (dd > 0)
                        {
                            int desigcode = dd;
                            Designation.desigcode = Convert.ToString(desigcode + 1);
                        }
                        else
                        {
                            int Count = 0;
                            Designation.desigcode = Convert.ToString(Count + 1);

                        }

                        Helper.ExecuteService("Designation", "CreateDesignation", Designation);
                    }
                    else
                    {
                        if (model.Id.HasValue)
                        {
                            var DesigCode = (string)Helper.ExecuteService("Designation", "GetDesigcodeById", Designation.Id);
                            var dd = Convert.ToInt32(DesigCode);
                            if (dd > 0)
                            {
                                int count = dd;
                                Designation.desigcode = Convert.ToString(count);
                            }

                        }

                        Helper.ExecuteService("Designation", "UpdateDesignation", Designation);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult EditDesignation(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Departments = (List<mDepartment>)Helper.ExecuteService("Designation", "GetAllDepartments", null);

                //mPISDesignation DesignationToEdit = (mPISDesignation)Helper.ExecuteService("Designation", "GetDesignationById", new mPISDesignation { Id = Id });
                mPISDesignation DesignationToEdit = (mPISDesignation)Helper.ExecuteService("Designation", "GetDesignationById", new mPISDesignation { Id = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = DesignationToEdit.ToViewModel1(Departments, "Edit");
                return View("CreateDesignation", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult DeleteDesignation(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mPISDesignation desigToDelete = (mPISDesignation)Helper.ExecuteService("Designation", "GetDesignationById", new mPISDesignation { Id = Id });
                Helper.ExecuteService("Designation", "DeleteDesignation", desigToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Deleting Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }




    }
}
