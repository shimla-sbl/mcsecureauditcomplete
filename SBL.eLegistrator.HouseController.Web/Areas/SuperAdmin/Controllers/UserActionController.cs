﻿using SBL.DomainModel.Models.UserAction;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class UserActionController : Controller
    {
        //
        // GET: /SuperAdmin/UserAction/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetUserAction()
        {
            var Modules = (List<mUserActions>)Helper.ExecuteService("Module", "GetUserAction", null);
            var model1 = Modules.ToViewUserAction();

            return PartialView("_GetUserAction", model1);
        }

        public ActionResult CreateNewUserAction()
        {
            mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToUserActionList();


            var model1 = new mUserActions()
            {
                Mode = "Add",
                objList = returnedResultNew
            };

            return PartialView("_CreateNewAction", model1);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAction(mUserActions model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToSaveAction();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "SaveUserAction", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntryAction", dist);
                }

                return RedirectToAction("GetUserAction");
            }
            else
            {

                return RedirectToAction("GetUserAction");
            }

        }


        public ActionResult EditUserAction(string ActionId)
        {

            mUserActions ActionToEdit = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", new mUserActions { ActionId = Convert.ToInt32(EncryptionUtility.Decrypt(ActionId.ToString())) });

            var model = ActionToEdit.ToViewActionModel("Edit");
            model.ActionId = Convert.ToInt32(EncryptionUtility.Decrypt(ActionId.ToString()));
            model.ActionName = model.ActionName;
            model.ActionNameLocal = model.ActionNameLocal;
            return View("_CreateNewAction", model);
        }

   

        public ActionResult DeleteAction(string ActionId)
        {
            mUserActions ActionToDelete = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", new mUserActions { ActionId = Convert.ToInt32(EncryptionUtility.Decrypt(ActionId.ToString())) });
            Helper.ExecuteService("Module", "DeleteAction", ActionToDelete);
            return RedirectToAction("GetUserAction");

        }

        public JsonResult CheckUserAction(string UserActionName)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckUserActionExist", UserActionName);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }

    }
}
