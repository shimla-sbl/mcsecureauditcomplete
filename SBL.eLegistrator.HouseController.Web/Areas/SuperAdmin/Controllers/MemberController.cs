﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.States;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Models;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using SBL.DomainModel.Models.Session;




namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MemberController : Controller
    {
        //
        // GET: /SuperAdmin/Member/

        //public ActionResult Index()
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserName))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }
        //        var Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);
        //        var data = Member.ToViewModel();
        //        return View(data);
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = "There is a Error While Getting Member Details";
        //        return View("AdminErrorPage");
        //        throw ex;
        //    }

        //}



        //public ActionResult Index(int pageId = 1, int pageSize = 50)
        //{

        //    //int pageId = 1;
        //    //int pageSize = 10;
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserName))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }
        //        var Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);
        //        var model = Member.ToViewModel();
        //        ViewBag.PageSize = pageSize;
        //        List<mMemberViewModel> pagedRecord = new List<mMemberViewModel>();
        //        if (pageId == 1)
        //        {
        //            pagedRecord = model.Take(pageSize).ToList();
        //        }
        //        else
        //        {
        //            int r = (pageId - 1) * pageSize;
        //            pagedRecord = model.Skip(r).Take(pageSize).ToList();
        //        }

        //        ViewBag.CurrentPage = pageId;
        //        ViewData["PagedList"] = Helper.BindPager(Member.Count, pageId, pageSize);
        //        return View(pagedRecord);
               
        //    }
        //    catch (Exception ex)
        //    {


        //        ViewBag.ErrorMessage = "There is a Error While Getting Members Details";
        //        return View("AdminErrorPage");
        //        throw ex;
        //    }

        //}

        public ActionResult Index()
        {
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog(Convert.ToString("Insert"));
            try
            {
                
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
              
               // var Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);
                //var model = Member.ToViewModel();
                //ViewBag.PageSize = pageSize;
                //List<mMemberViewModel> pagedRecord = new List<mMemberViewModel>();
                //if (pageId == 1)
                //{
                //    pagedRecord = model.Take(pageSize).ToList();
                //}
                //else
                //{
                //    int r = (pageId - 1) * pageSize;
                //    pagedRecord = model.Skip(r).Take(pageSize).ToList();
                //}

                //ViewBag.CurrentPage = pageId;
                //ViewData["PagedList"] = Helper.BindPager(Member.Count, pageId, pageSize);
               
                var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            
                ViewBag.assmeblyList = assmeblyList;
              
                //Code for SessionList 
                SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                ViewBag.sessionList = sessionList;
                //Code for SessionDateList 
             
                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;
              
                return PartialView("/Areas/SuperAdmin/Views/Member/MemberDetails.cshtml", sesdate);

            }
            catch (Exception ex)
            {

            
                ViewBag.ErrorMessage = "There is a Error While Getting Members Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult MemberDetailsWithAssembly(int assemblyid) 
        {
            try
            {
                if (assemblyid == 0)
                {
                    var Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);
                    var model = Member.ToViewModel();
                    return View("_MemberDetails", model);
                }
                else {
                    var Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersListByAssembly", assemblyid);
                    var model = Member.ToViewModel();
                    return View("_MemberDetails", model);
                }   
            }
            catch (Exception)
            {                
                throw;
            }        
        }


        public PartialViewResult GetMemberByPaging(int pageId, int pageSize)
        {
            var Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);
            ViewBag.PageSize = pageSize;
            List<mMember> pagedRecord = new List<mMember>();
            if (pageId == 1)
            {
                pagedRecord = Member.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Member.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Member.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
          //  return PartialView("/Areas/SuperAdmin/Views/Member/_Index.cshtml", model);
            return PartialView("/Areas/SuperAdmin/Views/Member/MemberDetails.cshtml", model);
        }


        public PartialViewResult GetSearchList(string MemberName, int pageId, int pageSize)
        {
            List<mMember> Member = new List<mMember>();

            if (!string.IsNullOrEmpty(MemberName))
            {
                //Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersListSearch", MemberCode);
                Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersListSearchByName", MemberName);
            }
            else
            {
                Member = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembersList", null);
                
            }

            ViewBag.PageSize = pageSize;
            List<mMember> pagedRecord = new List<mMember>();
            if (Member.Count() < 50)
            {
                pagedRecord = Member.Take(pageSize).ToList();
            }
            else
            {
            if (pageId == 1)
            {
                pagedRecord = Member.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = Member.Skip(r).Take(pageSize).ToList();
            }
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(Member.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
          //  return PartialView("/Areas/SuperAdmin/Views/Member/_Indexx.cshtml", model);
            return PartialView("/Areas/SuperAdmin/Views/Member/MemberDetails.cshtml", model);
            
        }




        public ActionResult CreateMember()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Districtcodes = (List<DistrictModel>)Helper.ExecuteService("Member", "GetAllDistrict", null);
                var StateNames = (List<mStates>)Helper.ExecuteService("Member", "GetAllStates", null);
                var HighestQualification = (List<mMemberHighestQualification>)Helper.ExecuteService("Member", "GetAllMemberHighestQualification", null);
                var model = new mMemberViewModel()
                {
                    Mode = "Add",
                    PrefixList = ExtensionOfMember.GetNameTitles(),
                    GendersList = ExtensionOfMember.GetGenderTitles(),
                    Districts = new SelectList(Districtcodes, "DistrictCode", "DistrictName"),
                    StateNames = new SelectList(StateNames, "mStateID", "StateName"),
                    HighestQualification = new SelectList(HighestQualification, "MemberHighestQualificationId", "Qualification"),
                    Active = true,
                    isAlive = true

                };
                return View("CreateMember", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Member Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMember(mMemberViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var member = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        member.FilePath = "";
                        member.FileName = "";
                        //var membercode = (int)Helper.ExecuteService("Member", "GetMemberCodeById", null);
                        //if (membercode > 0)
                        //{
                        //    int num = membercode;
                        //    member.MemberCode = num + 1;
                        //}
                        //else
                        //{
                        //    int count = 0;
                        //    member.MemberCode = count + 1;
                        //}

                        ////member.Active = true;
                        var memberId = (int)Helper.ExecuteService("Member", "CreateMember", member);



                        member.MemberID = memberId;


                        if (file != null)
                        {
                           // var filename = memberId + "_" + file.FileName;
                            var filename = memberId + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond)+ ".jpg";
                            member.FilePath = UploadPhoto(file, filename);
                            member.FileName = filename;
                            member.ThumbName = filename;
                            Helper.ExecuteService("Member", "UpdateMember", member);

                        }




                    }
                    else
                    {




                        if (file != null)
                        {
                            if (member.FileName != null)
                            {
                                RemoveExistingPhoto(member.FileName);
                            }
                            //var upfileName = member.MemberID + "_" + file.FileName;
                            var upfileName = member.MemberID + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond)  + ".jpg";
                            member.FilePath = UploadPhoto(file, upfileName);

                            member.FileName = upfileName;
                            member.ThumbName = upfileName;

                            Helper.ExecuteService("Member", "UpdateMember", member);
                        }
                        else
                        {
                            Helper.ExecuteService("Member", "UpdateMember", member);
                        }






                        //if (model.MemberID.HasValue)
                        //{

                        //    var membercode = (int)Helper.ExecuteService("Member", "GetMembeCodeById", member.MemberID);
                        //    if (membercode > 0)
                        //    {
                        //        int num = membercode;
                        //        member.MemberCode = num;
                        //    }
                        //}

                        //// member.Active = true;


                        //Helper.ExecuteService("Member", "UpdateMember", member);
                    }
                    return RedirectToAction("Index");

                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Member Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult EditMember(string Id)
        {
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog(Convert.ToString("1"));
            try
            {
              
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Districts = (List<DistrictModel>)Helper.ExecuteService("Member", "GetAllDistrict", null);
               
                var StateNames = (List<mStates>)Helper.ExecuteService("Member", "GetAllStates", null);
               
                var HighestQualification = (List<mMemberHighestQualification>)Helper.ExecuteService("Member", "GetAllMemberHighestQualification", null);
              
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
          
                //mMember memberToEdit = (mMember)Helper.ExecuteService("Member", "GetMemberById1", new mMember { MemberID = Id });
                mMember memberToEdit = (mMember)Helper.ExecuteService("Member", "GetMemberById1", new mMember { MemberID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            
                var model = memberToEdit.ToViewModel1(Districts, StateNames, HighestQualification, "Edit");
                       
                model.ImageLocation = fileAcessingSettings.SettingValue + model.FileLocation + "/" + model.FullImage;
            

               
                model.PrefixList = ExtensionOfMember.GetNameTitles();
             
                model.GendersList = ExtensionOfMember.GetGenderTitles();
                
                return View("CreateMember", model);
            }
            catch (Exception ex)
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog(Convert.ToString(ex));
                ViewBag.ErrorMessage = "There is a Error While Editing Member Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteMember(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMember desigToDelete = (mMember)Helper.ExecuteService("Member", "GetMemberById1", new mMember { MemberID = Id });


                Helper.ExecuteService("Member", "DeleteMember", desigToDelete);

                if (!string.IsNullOrEmpty(desigToDelete.FileName))
                    RemoveExistingPhoto(desigToDelete.FileName);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Member Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public JsonResult CheckMemberChildExist(int MemberID)
        {
            try
            {
                var isExist = (Boolean)Helper.ExecuteService("Member", "IsMemberIdChildExist", new mMember { MemberID = MemberID });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //public ActionResult CheckNumber(string number)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserName))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }
        //        mMember mdl = new mMember();
        //        string MemberNumber = number;
        //        mdl = (mMember)Helper.ExecuteService("Member", "CheckMemberNUmber", MemberNumber);
        //        return Json(mdl, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = "There  is a Error While Deleting Member Details";
        //        return View("AdminErrorPage");
        //        throw ex;
        //    }
        //}

        public bool CheckNumber(string number)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Member", "CheckMemberNUmber", number);
                return mdl;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }




        #region Private Methods

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {
            try
            {


                if (File != null)
                {




                    var memberSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    var memberThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberThumbFileSetting", null);

                    DirectoryInfo MemberDir = new DirectoryInfo(FileSettings.SettingValue + memberThumbSettings.SettingValue);
                    if (!MemberDir.Exists)
                    {
                        MemberDir.Create();
                    }


                    DirectoryInfo MemberTempDir = new DirectoryInfo(FileSettings.SettingValue + "\\Member\\Temp");
                    if (!MemberTempDir.Exists)
                    {
                        MemberTempDir.Create();
                    }



                    string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue, FileName);
                    string path1 = System.IO.Path.Combine(FileSettings.SettingValue + memberThumbSettings.SettingValue, FileName);
                    string basePath = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue + "\\Temp", FileName);

                    SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgex = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(750);
                    File.SaveAs(basePath);
                    imgex.Resize(basePath, path);
                    System.IO.File.Delete(basePath);
                    SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgext = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(85);
                    File.SaveAs(basePath);
                    imgext.Resize(basePath, path1);

                    System.IO.File.Delete(basePath);
                    return ("/Member");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/Member/"), FileName);
                File.SaveAs(path);
                return ("/Images/Member/");
            }
            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {

            try
            {

                var memberSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberFileSetting", null);
                var memberThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberThumbFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path1 = System.IO.Path.Combine(FileSettings.SettingValue + memberThumbSettings.SettingValue, OldPhoto);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue + "\\", OldPhoto);

                //string path = System.IO.Path.Combine(Server.MapPath("~/Images/Gallery/"), OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(path1);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "memberError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }
        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

        #endregion





    }
}
