﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class AssemblyTypeofDocumentsController : Controller
    {
        //
        // GET: /SuperAdmin/AssemblyTypeofDocuments/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var doc = (List<mAssemblyTypeofDocuments>)Helper.ExecuteService("AssemblyDoc", "GetAllDoc", null);
                var model = doc.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting AssemblyTypeofDocuments Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateDoc()
        {
            var model = new AssemblyTypeofDocumentsViewModel()
            {
                Mode = "Add",
                IsActive = true

            };

            return View("CreateDoc", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDoc(AssemblyTypeofDocumentsViewModel model)
        {

            if (ModelState.IsValid)
            {
                var doc = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("AssemblyDoc", "CreateDoc", doc);
                }
                else
                {
                    Helper.ExecuteService("AssemblyDoc", "UpdateDoc", doc);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditDoc(string Id)
        {

            //mAssemblyTypeofDocuments docToEdit = (mAssemblyTypeofDocuments)Helper.ExecuteService("AssemblyDoc", "GetDocById", new mAssemblyTypeofDocuments { TypeofDocumentId = Id });
            mAssemblyTypeofDocuments docToEdit = (mAssemblyTypeofDocuments)Helper.ExecuteService("AssemblyDoc", "GetDocById", new mAssemblyTypeofDocuments { TypeofDocumentId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = docToEdit.ToViewModel1("Edit");
            return View("CreateDoc", model);
        }

        public ActionResult DeleteDoc(int Id)
        {
            mAssemblyTypeofDocuments docToDelete = (mAssemblyTypeofDocuments)Helper.ExecuteService("AssemblyDoc", "GetDocById", new mAssemblyTypeofDocuments { TypeofDocumentId = Id });
            Helper.ExecuteService("AssemblyDoc", "DeleteDoc", docToDelete);
            return RedirectToAction("Index");

        }

    }
}
