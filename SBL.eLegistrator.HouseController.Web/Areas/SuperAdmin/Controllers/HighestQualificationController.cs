﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class HighestQualificationController : Controller
    {
        //
        // GET: /SuperAdmin/HighestQualification/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var HighestQualification = (List<mMemberHighestQualification>)Helper.ExecuteService("HighestQualification", "GetAllHighestQualification", null);
                var model = HighestQualification.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Highest Qualification Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateHighestQualification()
        {
            var model = new HighestQualificationViewModel()
            {
                Mode = "Add",
                IsActive = true

            };

            return View("CreateHighestQualification", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveHighestQualification(HighestQualificationViewModel model)
        {

            if (ModelState.IsValid)
            {
                var HighestQualification = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("HighestQualification", "CreateHighestQualification", HighestQualification);
                }
                else
                {
                    Helper.ExecuteService("HighestQualification", "UpdateHighestQualification", HighestQualification);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditHighestQualification(string Id)
        {

            //mMemberHighestQualification stateToEdit = (mMemberHighestQualification)Helper.ExecuteService("HighestQualification", "GetHighestQualificationById", new mMemberHighestQualification { MemberHighestQualificationId = Id });
            mMemberHighestQualification stateToEdit = (mMemberHighestQualification)Helper.ExecuteService("HighestQualification", "GetHighestQualificationById", new mMemberHighestQualification { MemberHighestQualificationId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            
            var model = stateToEdit.ToViewModel1("Edit");
            return View("CreateHighestQualification", model);
        }

        public ActionResult DeleteHighestQualification(int Id)
        {
            mMemberHighestQualification stateToDelete = (mMemberHighestQualification)Helper.ExecuteService("HighestQualification", "GetHighestQualificationById", new mMemberHighestQualification { MemberHighestQualificationId = Id });
            Helper.ExecuteService("HighestQualification", "DeleteHighestQualification", stateToDelete);
            return RedirectToAction("Index");

        }

    }
}
