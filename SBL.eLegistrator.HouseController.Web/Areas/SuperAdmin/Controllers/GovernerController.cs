﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using SBL.DomainModel.Models.Governor;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.States;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class GovernerController : Controller
    {
        //
        // GET: /SuperAdmin/Governer/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Governer = (List<mGoverner>)Helper.ExecuteService("Governer", "GetAllGovernerList", null);
                var data = Governer.ToViewModel();
                return View(data);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Governor Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateGoverner()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
               
                var StateNames = (List<mStates>)Helper.ExecuteService("Member", "GetAllStates", null);

                var model = new GovernerViewModel()
                {
                    Mode = "Add",
                    
                    StateNames = new SelectList(StateNames, "mStateID", "StateName"),
                    
                    Active = true
                    

                };
                return View("CreateGoverner", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Governor Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveGoverner(GovernerViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Governer = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Governer.FilePath = "";
                        Governer.FileName = "";

                        var GovernerId = (int)Helper.ExecuteService("Governer", "CreateGoverner", Governer);



                        Governer.mGovernerID = GovernerId;


                        if (file != null)
                        {

                            var filename = GovernerId + "Governor" + ".jpg";
                            Governer.FilePath = UploadPhoto(file, filename);
                            Governer.FileName = filename;
                            Governer.ThumbName = filename;
                            Helper.ExecuteService("Governer", "UpdateGoverner", Governer);

                        }




                    }
                    else
                    {




                        if (file != null)
                        {
                            if (Governer.FileName != null)
                            {
                                RemoveExistingPhoto(Governer.FileName);
                            }

                            var upfileName = Governer.mGovernerID + "Governor" + ".jpg";
                            Governer.FilePath = UploadPhoto(file, upfileName);

                            Governer.FileName = upfileName;
                            Governer.ThumbName = upfileName;

                            Helper.ExecuteService("Governer", "UpdateGoverner", Governer);
                        }
                        else
                        {
                            Helper.ExecuteService("Governer", "UpdateGoverner", Governer);
                        }


                    }
                    return RedirectToAction("Index");

                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Governor Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult EditGoverner(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
               
                var StateNames = (List<mStates>)Helper.ExecuteService("Member", "GetAllStates", null);
                
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                //mGoverner memberToEdit = (mGoverner)Helper.ExecuteService("Governer", "GetGovernerById1", new mGoverner { mGovernerID = Id });
                mGoverner memberToEdit = (mGoverner)Helper.ExecuteService("Governer", "GetGovernerById1", new mGoverner { mGovernerID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = memberToEdit.ToViewModel1(StateNames, "Edit");


                model.ImageLocation = fileAcessingSettings.SettingValue + model.FileLocation + "/" + model.FullImage;



                return View("CreateGoverner", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Governor Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteGoverner(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mGoverner desigToDelete = (mGoverner)Helper.ExecuteService("Governer", "GetGovernerById1", new mGoverner { mGovernerID = Id });


                Helper.ExecuteService("Governer", "DeleteGoverner", desigToDelete);

                if (!string.IsNullOrEmpty(desigToDelete.FileName))
                    RemoveExistingPhoto(desigToDelete.FileName);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Governor Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

       



        #region Private Methods

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {
            try
            {


                if (File != null)
                {




                    var memberSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    var memberThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberThumbFileSetting", null);

                    DirectoryInfo MemberDir = new DirectoryInfo(FileSettings.SettingValue + memberThumbSettings.SettingValue);
                    if (!MemberDir.Exists)
                    {
                        MemberDir.Create();
                    }


                    DirectoryInfo MemberTempDir = new DirectoryInfo(FileSettings.SettingValue + "\\Member\\Temp");
                    if (!MemberTempDir.Exists)
                    {
                        MemberTempDir.Create();
                    }



                    string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue, FileName);
                    string path1 = System.IO.Path.Combine(FileSettings.SettingValue + memberThumbSettings.SettingValue, FileName);
                    string basePath = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue + "\\Temp", FileName);

                    SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgex = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(750);
                    File.SaveAs(basePath);
                    imgex.Resize(basePath, path);
                    System.IO.File.Delete(basePath);
                    SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgext = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(85);
                    File.SaveAs(basePath);
                    imgext.Resize(basePath, path1);

                    System.IO.File.Delete(basePath);
                    return ("/Member");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/Member/"), FileName);
                File.SaveAs(path);
                return ("/Images/Member/");
            }
            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {

            try
            {

                var memberSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberFileSetting", null);
                var memberThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberThumbFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path1 = System.IO.Path.Combine(FileSettings.SettingValue + memberThumbSettings.SettingValue, OldPhoto);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue + "\\", OldPhoto);

                //string path = System.IO.Path.Combine(Server.MapPath("~/Images/Gallery/"), OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(path1);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "memberError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

            }
        }
        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

        #endregion

    }
}
