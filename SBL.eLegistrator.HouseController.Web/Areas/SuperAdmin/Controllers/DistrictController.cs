﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.States;
using SBL.DomainModel.Models.District;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class DistrictController : Controller
    {
        //
        // GET: /SuperAdmin/District/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<DistrictModel>)Helper.ExecuteService("District", "GetAllDistrictIDs", null);
            var model = State.ToViewModel();
            return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting District Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateDistrict()
        {
            var States = (List<mStates>)Helper.ExecuteService("District", "GetAllState", null);
            var model = new DistrictViewModel()
            {
                Mode = "Add",
                State = new SelectList(States, "mStateID", "StateAbbreviation"),
                IsActive=true
            };

            return View("CreateDistrict", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDistrict(DistrictViewModel model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("District", "CreateDistrict", dist);
                }
                else
                {
                    Helper.ExecuteService("District", "UpdateDistrict", dist);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditDistrict(string Id)
        {
            var States = (List<mStates>)Helper.ExecuteService("District", "GetAllState", null);
            //DistrictModel stateToEdit = (DistrictModel)Helper.ExecuteService("District", "GetDistrictById", new DistrictModel { DistrictId = Id });
            DistrictModel stateToEdit = (DistrictModel)Helper.ExecuteService("District", "GetDistrictById", new DistrictModel { DistrictId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1(States, "Edit");
            return View("CreateDistrict", model);
        }

        public ActionResult DeleteDistrict(int Id)
        {
            DistrictModel stateToDelete = (DistrictModel)Helper.ExecuteService("District", "GetDistrictById", new DistrictModel { DistrictId = Id });
            Helper.ExecuteService("District", "DeleteDistrict", stateToDelete);
            return RedirectToAction("Index");

        }

        public JsonResult CheckDistrictChildExist(int DistrictID)
        {
            var isExist = (Boolean)Helper.ExecuteService("District", "IsDistrictChildExist", new DistrictModel { DistrictId = DistrictID });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }


    }
}
