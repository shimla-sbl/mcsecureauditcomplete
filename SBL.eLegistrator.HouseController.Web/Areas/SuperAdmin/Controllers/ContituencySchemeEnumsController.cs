﻿using System;
using SBL.DomainModel.Models.ConstituencyVS;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class ContituencySchemeEnumsController : Controller
    {
        //
        // GET: /SuperAdmin/ContituencySchemeEnums/

        public ActionResult Index()
        {
            try
            {
                //if (string.IsNullOrEmpty(CurrentSession.UserName))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}
                var ContituencySchemeEnums = (List<mProgramme>)Helper.ExecuteService("ContituencySchemeEnums", "GetAllProgramme", null);
                var model = ContituencySchemeEnums.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Highest Qualification Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }


        public ActionResult CreateProgramme()
        {
            var model = new ContituencySchemeEnumsViewModel()
            {
                Mode = "Add",
                //IsActive = true

            };

            return View("CreateProgramme", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveProgramme(ContituencySchemeEnumsViewModel model)
        {

            if (ModelState.IsValid)
            {
                var ContituencySchemeEnums = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("ContituencySchemeEnums", "CreateProgramme", ContituencySchemeEnums);
                }
                else
                {
                    Helper.ExecuteService("ContituencySchemeEnums", "UpdateProgramme", ContituencySchemeEnums);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditProgramme(string Id)
        {

            //mMemberHighestQualification stateToEdit = (mMemberHighestQualification)Helper.ExecuteService("HighestQualification", "GetHighestQualificationById", new mMemberHighestQualification { MemberHighestQualificationId = Id });
            mProgramme stateToEdit = (mProgramme)Helper.ExecuteService("ContituencySchemeEnums", "GetProgrammeById", new mProgramme { ProgramID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

            var model = stateToEdit.ToViewModel1("Edit");
            return View("CreateProgramme", model);
        }

        public ActionResult DeleteProgramme(int Id)
        {
            mProgramme stateToDelete = (mProgramme)Helper.ExecuteService("ContituencySchemeEnums", "GetProgrammeById", new mProgramme { ProgramID = Id });
            Helper.ExecuteService("ContituencySchemeEnums", "DeleteProgramme", stateToDelete);
            return RedirectToAction("Index");

        }

    }
}
