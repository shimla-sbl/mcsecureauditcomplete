﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Enums;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MobileSettingsController : Controller
    {
        //
        // GET: /SuperAdmin/MobileSettings/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                
                var mobileSettings = (List<mMobileSettings>)Helper.ExecuteService("Mobiles", "GetAllMobileSettings", null);

                var model = mobileSettings.ToViewModel();
                
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is an Error While Getting Mobile Setting Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

     
        public ActionResult CreateMobileSettings()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new MobileSettingsViewModel()
                {
                    Mode = "Add"
                   
                };
                model.MediaAfterBeforeCol = new SelectList(Enum.GetValues(typeof(AfterBefore)).Cast<AfterBefore>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");
                model.MemberAfterBeforeCol = new SelectList(Enum.GetValues(typeof(AfterBefore)).Cast<AfterBefore>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");
                return View("CreateMobileSettings", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is an Error While Creating Mobile Setting Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        public JsonResult SaveMobileSettings()
        {
            try
            {
                MobileSettingsViewModel model = new MobileSettingsViewModel();

                model.MediaAfterBefore = Request.Form["MediaAfterBefore"].ToString();

                model.MemberAfterBefore = Request.Form["MemberAfterBefore"].ToString();

                model.MediaViewTime = (model.MediaAfterBefore == "0" ? Convert.ToInt32(Request.Form["MediaViewTime"].ToString()) :
                Convert.ToInt32(Request.Form["MediaViewTime"].ToString()) * -1);

                model.MemberViewTime = (model.MemberAfterBefore == "0" ? Convert.ToInt32(Request.Form["MemberViewTime"].ToString()) :
                    Convert.ToInt32(Request.Form["MemberViewTime"].ToString()) * -1);


                model.Mode = Request.Form["Mode"].ToString();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                var MobileSettings = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateMobileSettings", MobileSettings);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateMobileSettings", MobileSettings);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditMobileSettings(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMobileSettings LangToEdit = (mMobileSettings)Helper.ExecuteService("Mobiles", "GetMobileSettingsBasedOnId", new mMobileSettings { SlNo = Id });
                var model = LangToEdit.ToViewModel1("Edit");
                model.MediaAfterBeforeCol = new SelectList(Enum.GetValues(typeof(AfterBefore)).Cast<AfterBefore>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");
                model.MemberAfterBeforeCol = new SelectList(Enum.GetValues(typeof(AfterBefore)).Cast<AfterBefore>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");
                return View("CreateMobileSettings", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is an Error While Editing Mobile Settings Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteMobileSettings(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMobileSettings LangToRemove = (mMobileSettings)Helper.ExecuteService("Mobiles", "GetMobileSettingsBasedOnId", new mMobileSettings { SlNo = Id });
                Helper.ExecuteService("Mobiles", "DeleteMobileSettings", LangToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is an Error While Deleting Mobile Settings Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

    }
}
