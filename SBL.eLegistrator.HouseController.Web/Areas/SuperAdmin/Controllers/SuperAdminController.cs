﻿using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SuperAdminController : Controller
    {
        //
        // GET: /SuperAdmin/SuperAdmin/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeftNavigationMenu()
        {
            return PartialView("_LeftNavigationMenu");
        }

        public ActionResult GetMemberDetailsById(int memberCode)
        {
            try
            {
                mMember details = new mMember();
                details.MemberCode = memberCode;
                var member = (mMember)Helper.ExecuteService("Member", "GetMemberDetailsByMemberCode", details);
                var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);

                var model = member.ToViewModel1("Edit");
                model.ImageFile = fileAcessingSettings.SettingValue + "/thumb/" + model.MemberCode.ToString() + ".jpg";
                return View(model);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Member Edit Error");
                ViewBag.ErrorMessage = "There is a Error While Editing the Member Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult CreateMember()
        {

            try
            {

            }
            catch (Exception)
            {
                
                throw;
            }

            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveMemberDetails(MemberDetailsModel memberDetails)
        {
            try
            {
                var savedata = memberDetails.ToDomainModel();
                Helper.ExecuteService("Member", "SaveMemberData", savedata);
                return RedirectToAction("GetAllMemberList");
            }
            catch (Exception ex)
            {
                RecordError(ex, "Member Save Error");
                ViewBag.ErrorMessage = "There is a Error While Saving the Member Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult GetAllMemberList()
        {
            var memberList = (List<mMember>)Helper.ExecuteService("Member", "GetAllMembers", null);
            var memberlist = memberList.ToViewModel1();
            return View(memberlist);
        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "memberContentError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }
    }
}
