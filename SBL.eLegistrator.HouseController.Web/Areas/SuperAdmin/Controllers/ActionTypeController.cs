﻿using SBL.DomainModel.Models.Grievance;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class ActionTypeController : Controller
    {
        //
        // GET: /SuperAdmin/ActionType/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                var actionType = (List<ActionType>)Helper.ExecuteService("Mobiles", "GetAllActionType", null);
                var model = actionType.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "An Error Occured While Getting Action Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateActionType()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new ActionTypeViewModel()
                {
                    Mode = "Add"

                };
                return View("CreateActionType", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "An Error Occured While Creating Create Action Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SaveActionType()
        {
            try
            {
                ActionTypeViewModel model = new ActionTypeViewModel();

                model.StatusId = Convert.ToInt32(Request.Form["StatusId"].ToString());

                model.StatusCode = Request.Form["StatusCode"].ToString();

                model.Mode = Request.Form["Mode"].ToString();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                var mobileApps = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateActionType", mobileApps);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateActionType", mobileApps);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditActionType(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                ActionType appsToEdit = (ActionType)Helper.ExecuteService("Mobiles", "GetActionTypeBasedOnId", new ActionType { SlNo = Id });
                var model = appsToEdit.ToViewModel1("Edit");
                return View("CreateActionType", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "An Error Occured While Editing Create Action Type Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteActionType(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                ActionType appsToRemove = (ActionType)Helper.ExecuteService("Mobiles", "GetActionTypeBasedOnId", new ActionType { SlNo = Id });
                Helper.ExecuteService("Mobiles", "DeleteActionType", appsToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "An Error Occured While Deleting Action Type Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

    }
}
