﻿using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.Notices.Controllers;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.eLegistrator.HouseController.Web.Extensions;


namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class SubModuleController : Controller
    {
        //
        // GET: /UserManagement/SubModule/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserManagement()
        {
            return View();
        }

        public ActionResult GetUserSubModules()
        {
            var Modules = (List<mUserSubModules>)Helper.ExecuteService("Module", "GetUserSubModules", null);
            var model1 = Modules.ToViewUserSubModel();

            return PartialView("_GetUserSubModule", model1);
        }

        public ActionResult CreateNewSubModule()
        {
            //For User Access
            mUserModules Accessmodel = new mUserModules();
            var returnedAccessmodel = Helper.ExecuteService("Module", "GetUserAccessDropDown", Accessmodel) as List<mUserModules>;
            var returnedAccessmodelNew = returnedAccessmodel.ToSubAccessList();


            var model1 = new mUserSubModules()
            {
                Mode = "Add",
                AccessList = returnedAccessmodelNew,
            };

            return PartialView("_CreateNewSubModule", model1);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSubModule(mUserSubModules model)
        {
            model.SubModuleOrder = model.ModuleOrderTemp;
            if (ModelState.IsValid)
            {
                var dist = model.ToSubModuleModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateSubModule", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntrySubModules", dist);
                }

                return RedirectToAction("GetUserSubModules");
            }
            else
            {

                return RedirectToAction("GetUserSubModules");
            }

        }
      

        public JsonResult CheckUserAccess(int UserAccess, string UserSubAccessName)
        {
            mUserSubModules model = new mUserSubModules();


            model.ModuleId = UserAccess;
            model.SubModuleName = UserSubAccessName;

            try
            {
                var mdl = (bool)Helper.ExecuteService("Module", "CheckSubUserAccessExist", model);
                //var mdl1 = (bool)Helper.ExecuteService("Module", "CheckUserAccessIdExist", UserAccess);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }

#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }

        public ActionResult EditUserSubModule(string Id)
        {

            mUserModules Accessmodel = new mUserModules();
            var returnedAccessmodel = Helper.ExecuteService("Module", "GetUserAccessDropDown", Accessmodel) as List<mUserModules>;
            var returnedAccessmodelNew = returnedAccessmodel.ToSubAccessList();


            var model1 = new mUserSubModules()
            {
               // Mode = "Add",
                AccessList = returnedAccessmodelNew,
            };

            mUserSubModules SubModuleToEdit = (mUserSubModules)Helper.ExecuteService("Module", "GetSubModuleDataById", new mUserSubModules { SubModuleId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

            var model = SubModuleToEdit.ToSubModuleModel();


            model1.ModuleId = model.ModuleId;
            model1.SubModuleId = model.SubModuleId;
            model1.ModuleName = model.ModuleName;
            model1.SubModuleName = model.SubModuleName;
            model1.SubModuleNameLocal = model.SubModuleNameLocal;
            model1.SubModuleDescription = model.SubModuleDescription;
            model1.SubModuleOrder = model.SubModuleOrder;
            return View("_CreateNewSubModule", model1);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateSubModule(mUserSubModules model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToSubModuleModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateModule", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntrySubModules", dist);
                }

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }
            else
            {

                return RedirectToAction("UserManagement", "Role", new { @area = "UserManagement" });
            }

        }




        public ActionResult DeleteSubModule(string Id)
        {
            mUserSubModules moduleToDelete = (mUserSubModules)Helper.ExecuteService("Module", "GetSubModuleDataById", new mUserSubModules { SubModuleId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            Helper.ExecuteService("Module", "DeleteSubModule", moduleToDelete);
            return RedirectToAction("GetUserSubModules");

        }


        public JsonResult fillOrderNo(int ModuleID, string Mode)
        {

            List<SubModuleOrderList> _list = (List<SubModuleOrderList>)Helper.ExecuteService("Module", "getOrderNoForSubModule", ModuleID);

            SubModuleOrderList _addItem = new SubModuleOrderList();

            if (_list.Count > 0)
            {
                if (Mode == "save")
                {
                    _addItem._OrderID = _list[0]._OrderID + 1;
                    _list.Insert(0, _addItem);
                }
                //else
                //{
                //int i = 1;
                //List<ModuleOrderList> _list2 = new List<ModuleOrderList>();
                //    foreach (var item in _list)
                //    {
                //        if (item._OrderID == 0)
                //        {
                //            item._OrderID = i;
                //            _list2.Add(item);
                //        }
                //        i = i + 1;
                //    }
                //    _list = new List<ModuleOrderList>();
                //    _list = _list2;
                // _addItem._OrderID = _list[0]._OrderID + 1;
                // _list.Insert(0, _addItem);
                //}
            }
            else
            {
                _addItem._OrderID = 1;
                _list.Insert(0, _addItem);
            }

            return Json(_list, JsonRequestBehavior.AllowGet);
        }

    }
}
