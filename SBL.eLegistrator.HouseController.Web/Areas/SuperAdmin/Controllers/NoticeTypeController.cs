﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Notice;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class NoticeTypeController : Controller
    {
        //
        // GET: /SuperAdmin/NoticeType/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetAllNoticeType()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
            var NoticeTypelist = (List<mNoticeType>)Helper.ExecuteService("Notice", "GetAllNoticeTypes", null);
            var Getlist = NoticeTypelist.NoticeTypeViewModel();
            return View(Getlist);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Notice Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }
        public ActionResult CreateNoticeype()
        {


            var model = new NoticeTypeViewModel()
            {
                Mode = "Add",
                IsActive=true

            };
            return View(model);
        }
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveNoticeType(NoticeTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Res = model.NoticeTypeToDomainModel();


                if (model.Mode == "Add")
                {
                    var CommiteeList = Helper.ExecuteService("Notice", "CreateNoticeType", Res);
                    //Helper.ExecuteService("Notice", "CreateNoticeType", Res);
                }
                else
                {
                    Helper.ExecuteService("Notice", "UpdateNoticeType", Res);
                }
            }
            else
            {

                return RedirectToAction("CreateNoticeype");
            }



            return RedirectToAction("GetAllNoticeType");
        }
        public ActionResult EditNoticeType(string Id)
        {


            //mNoticeType committeeToEdit = (mNoticeType)Helper.ExecuteService("Notice", "EditNoticeType", Id);
            mNoticeType committeeToEdit = (mNoticeType)Helper.ExecuteService("Notice", "EditNoticeType", Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));

            var model = committeeToEdit.SingleNoticeType();
            model.Mode = "Edit";

            return View("CreateNoticeype", model);

        }
        public ActionResult DeleteNoticeType(int Id)
        {
            //mNoticeType CommitteeTypeToDelete = (mNoticeType)Helper.ExecuteService("Notice", "GetCommitteById", Id);
            Helper.ExecuteService("Notice", "DeleteNoticeType", Id);
            return RedirectToAction("GetAllNoticeType");
        }

    }
}
