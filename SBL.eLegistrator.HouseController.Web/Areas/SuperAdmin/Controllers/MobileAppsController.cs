﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MobileAppsController : Controller
    {
        //
        // GET: /SuperAdmin/MobileApps/

        public ActionResult Index()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                var mobiApps = (List<mMobileApps>)Helper.ExecuteService("Mobiles", "GetAllMobileApps", null);
                var model = mobiApps.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Mobile Apps Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateMobileApps()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new MobileAppsViewModel()
                {
                    Mode = "Add"

                };
                return View("CreateMobileApps", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Mobile Apps Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SaveMobileApps()
        {
            try
            {
                MobileAppsViewModel model = new MobileAppsViewModel();

                model.AppName = Request.Form["AppName"].ToString();

                model.Description = Request.Form["Description"].ToString();

                model.Mode = Request.Form["Mode"].ToString();

                model.Code = Request.Form["Code"].ToString();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                var mobileApps = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateMobileApps", mobileApps);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateMobileApps", mobileApps);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditMobileApps(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMobileApps appsToEdit = (mMobileApps)Helper.ExecuteService("Mobiles", "GetMobileAppsBasedOnId", new mMobileApps { SlNo = Id });
                var model = appsToEdit.ToViewModel1("Edit");
                return View("CreateMobileApps", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Mobile Apps Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteMobileApps(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMobileApps appsToRemove = (mMobileApps)Helper.ExecuteService("Mobiles", "GetMobileAppsBasedOnId", new mMobileApps { SlNo = Id });
                Helper.ExecuteService("Mobiles", "DeleteMobileApps", appsToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Mobile Apps Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

    

    }
}
