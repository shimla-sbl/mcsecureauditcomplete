﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Party;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Models;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class PartyController : Controller
    {
        //
        // GET: /SuperAdmin/Party/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
            var Party = (List<mParty>)Helper.ExecuteService("Party", "GetAllParty", null);
            var model = Party.ToViewModel();
            return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Party Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateParty()
        {
            var model = new PartyViewModel()
            {
                Mode = "Add",
                IsActive=true

            };

            return View("CreateParty", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveParty(PartyViewModel model, HttpPostedFileBase file)
        {




            if (file != null)
            {
                string DirPath = Server.MapPath("~/assets/SuperAdmin/Party/");
                if (!Directory.Exists(DirPath))
                {
                    Directory.CreateDirectory(DirPath);
                }
                var fileName = Path.GetFileName(file.FileName);

                var path = Path.Combine(Server.MapPath("~/assets/SuperAdmin/Party/"), fileName);
                file.SaveAs(path);

                ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                imgCompress.GetImage = new System.Drawing.Bitmap(path);
                imgCompress.Height = 160;
                imgCompress.Width = 120;
                imgCompress.Save("compressingImage.jpg", DirPath);

                string newPath = Path.Combine(DirPath, "compressingImage.jpg");
                byte[] image = ReadImage(newPath);


                model.Photo = image;
            }




            if (ModelState.IsValid)
            {
                var party = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Party", "CreateParty", party);
                }
                else
                {
                    Helper.ExecuteService("Party", "UpdateParty", party);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditParty(string Id)
        {

            //mParty partyToEdit = (mParty)Helper.ExecuteService("Party", "GetPartyById", new mParty { PartyID = Id });
            mParty partyToEdit = (mParty)Helper.ExecuteService("Party", "GetPartyById", new mParty { PartyID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()))});
            var model = partyToEdit.ToViewModel1("Edit");
            return View("CreateParty", model);
        }

        public ActionResult DeleteParty(int Id)
        {
            mParty partyToDelete = (mParty)Helper.ExecuteService("Party", "GetPartyById", new mParty { PartyID = Id });
            Helper.ExecuteService("Party", "DeleteParty", partyToDelete);
            return RedirectToAction("Index");

        }


        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult CheckPartyChildExist(int PartyID)
        {
            var isExist = (Boolean)Helper.ExecuteService("Party", "IsPartyIdChildExist", new mParty { PartyID = PartyID });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }

    }
}
