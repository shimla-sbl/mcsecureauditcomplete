﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Bill;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class BillStatusController : Controller
    {
        //
        // GET: /SuperAdmin/BillStatus/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var BillStatus = (List<mBillStatus>)Helper.ExecuteService("Bill", "GetAllBillStatus", null);
                var model = BillStatus.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Getting Bill Status Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
            
        }

        public ActionResult CreateBillStatus()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new BillStatusViewModel()
                {
                    Mode = "Add",
                    Active = true
                };
                return View("CreateBillStatus", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Creating Bill Status Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }
      
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveBillStatus(BillStatusViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Billstatus = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("Bill", "CreateBillStatus", Billstatus);
                    }
                    else
                    {
                        Helper.ExecuteService("Bill", "UpdateBillStatus", Billstatus);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Bill Status Details";
                return View("AdminErrorPage");
                throw ex;
            }

            

        }

        public ActionResult EditBillStatus(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //mBillStatus BstatusToEdit = (mBillStatus)Helper.ExecuteService("Bill", "GetBillStatusById", new mBillStatus { BillStatusId = Id });
                mBillStatus BstatusToEdit = (mBillStatus)Helper.ExecuteService("Bill", "GetBillStatusById", new mBillStatus { BillStatusId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = BstatusToEdit.ToViewModel1("Edit");
                return View("CreateBillStatus", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Bill Status Details";
                return View("AdminErrorPage");
                throw ex;
            }
            
        }

        public ActionResult DeleteBillStatus(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mBillStatus BstatusToDelete = (mBillStatus)Helper.ExecuteService("Bill", "GetBillStatusById", new mBillStatus { BillStatusId = Id });
                Helper.ExecuteService("Bill", "DeleteBillStatus", BstatusToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Deleting Bill Status Details";
                return View("AdminErrorPage");
                throw ex;
            }
          

        }


    }
}
