﻿using Email.API;
using EvoPdf;
using Ionic.Zip;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.salaryhead;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Accounts.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
//using SBL.eLegistrator.HouseController.Web.EmailWebService;
using SMS.API;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SalaryBillsController : Controller
    {
        //
        // GET: /SuperAdmin/SalaryBills/

        public ActionResult Index()
        {
            return View();
        }
        //#region salary bills update

        //public ActionResult UpdateSalaryBills()
        //{
        //    return View();
        //}
        //public ActionResult getNumberToWords(string number)
        //{
        //    int num = int.Parse(number);
        //    if (Request.IsAjaxRequest())
        //    {
        //        return Json(num.NumbersToWords(), JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        return Content(num.NumbersToWords());
        //    }
        //}
        //public JsonResult fillCurrentMember()
        //{
        //    List<fillCurrentMamber> list = (List<fillCurrentMamber>)Helper.ExecuteService("salaryheads", "getCurrentMemberList", null);
        //    return Json(list, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult getSalaryHeadsList(string memberid, string catId, string monthID, string yearID)
        //{
        //    salarystructure _salarystructure = null;

        //    int[] arr = new int[] { int.Parse(memberid), int.Parse(monthID), int.Parse(yearID) };
        //    _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
        //    _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();


        //    return PartialView("_salaryHeadsForMember", _salarystructure);
        //}
        //public ActionResult getSalaryHeadsListforAllMembers(string monthID, string yearID)
        //{
        //    List<mMember> _mList = (List<mMember>)Helper.ExecuteService("salaryheads", "getallmembers", null);
        //    List<salarystructure> _sList = new List<salarystructure>();
        //    foreach (var member in _mList)
        //    {
        //        int memberid = member.MemberCode;
        //        salarystructure _salarystructure = null;

        //        int[] arr = new int[] { memberid, int.Parse(monthID), int.Parse(yearID) };
        //        _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
        //        _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();

        //        _sList.Add(_salarystructure);
        //    }

        //    return PartialView("_salaryHeadsForAllMembers", _sList);
        //}
        //public ActionResult getSpeakerCurrentList(string designationID)
        //{
        //    List<mMember> _list = (List<mMember>)Helper.ExecuteService("salaryheads", "getDetailsByDesignation", designationID);

        //    List<SpeakerList> _sList = new List<SpeakerList>();
        //    foreach (var item in _list)
        //    {
        //        SpeakerList _speaker = new SpeakerList();
        //        _speaker.speakerID = item.MemberCode;
        //        _speaker.speakerName = item.Name;
        //        _sList.Add(_speaker);
        //    }
        //    return Json(_sList, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult generateBills(salarystructure _salarystructure, List<salarystructure> _salarystructureList, string Command)
        //{
        //    ZipFile zip = new ZipFile();

        //    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
        //    if (ModelState.IsValid)
        //    {
        //        if (Command == "members")
        //        {

        //            if (_salarystructure.Mode == "update")
        //            {
        //                Helper.ExecuteService("salaryheads", "updateSalaryBills", _salarystructure);
        //            }
        //        }
        //        else
        //        {
        //            if (Command == "delete")
        //            {
        //                Helper.ExecuteService("salaryheads", "deleteSalaryBills", _salarystructure);
        //            }
        //            else
        //            {

        //                try
        //                {
        //                    string[] returnResult = null;
        //                    string filepath = string.Empty;
        //                    bool pdf = true;
        //                    string SavedPdfPath = string.Empty;

        //                    if (_salarystructure.designationName == "Speaker" || _salarystructure.designationName == "Deputy Speaker")
        //                    {

        //                        returnResult = GeneratePDFforSpeaker(_salarystructure, true, filepath);

        //                    }
        //                    else
        //                    {
        //                        returnResult = GeneratePDFforMembers(_salarystructure, true, filepath);
        //                    }

        //                    SavedPdfPath = SavedPdfPath = returnResult[0] + _salarystructure.monthID + "_" + _salarystructure.yearId + "_" + _salarystructure.memberCode + ".pdf";


        //                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //                    string directory = FileSettings.SettingValue + SavedPdfPath;



        //                    zip.AddFile(directory, "SalaryBills");
        //                    Response.Clear();
        //                    Response.BufferOutput = false;
        //                    string zipName = String.Format("{0}.zip", "salaryBill");
        //                    Response.ContentType = "application/zip";
        //                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

        //                    zip.Save(Response.OutputStream);
        //                    Response.End();

        //                    //return Response;
        //                }
        //                catch (Exception ex)
        //                {

        //                }
        //            }
        //        }


        //    }
        //    else
        //    {
        //        ModelState.AddModelError("", "error in code");
        //    }

        //    return Json("", JsonRequestBehavior.AllowGet);
        //}
        //public string[] GeneratePDFforSpeaker(salarystructure qModel, bool pdf, string filePath)
        //{
        //    string msg = string.Empty;
        //    string url = string.Empty;
        //    MemoryStream output = new MemoryStream();
        //    string AllowancesText = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
        //    string Deductions = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
        //    string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

        //    outXml += "<table class=\"tg\">";
        //    outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No :Deposit/MLA/1047-48,Dated " + new DateTime(1900, Convert.ToInt32(qModel.monthID), 1).ToString("MMMM") + " , " + qModel.yearId + "</th></tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";

        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Name:   <b>" + qModel.memberName + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.designationName + "</span></b></td> </tr>";
        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += " <tr><td colspan=\"3\">TreasuryCode</td><td>SML00</td><td colspan=\"3\">Gazetted/NonGazetted</td><td colspan=\"2\">G</td></tr>";
        //    outXml += "<tr><td colspan=\"3\">DemandNumber</td><td>01</td><td colspan=\"3\">SchemaCodeNo</td><td colspan=\"2\">-</td></tr>";
        //    outXml += "<tr><td colspan=\"3\">DDOCodeNo</td><td>092</td><td colspan=\"3\">ObjectCode</td><td colspan=\"2\">01</td></tr>";
        //    outXml += "<tr><td colspan=\"3\">MajorHead</td><td>2011</td><td colspan=\"3\">VotedCharged</td><td colspan=\"2\">C</td></tr>";

        //    outXml += "<tr><td colspan=\"3\">SubMajorHead</td><td>02</td><td colspan=\"3\">Plan/Nonplan</td><td colspan=\"2\">N</td></tr>";
        //    outXml += "<tr><td colspan=\"3\">MinorHead</td><td>101</td><td colspan=\"3\"></td><td colspan=\"2\"></td></tr>";
        //    outXml += "<tr><td colspan=\"3\">subHead</td><td>01</td><td colspan=\"3\"></td><td colspan=\"2\"></td></tr><tr><td colspan=\"4\"><b>Allowances</b></td><td colspan=\"4\"><b>Deductions</b></td></tr>";
        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

        //    foreach (var item in qModel.headList)
        //    {
        //        if (item.htype == "cr")
        //        {
        //            string ctext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + ".00</td></tr>";
        //            outXml += ctext;
        //            AllowancesText += ctext;
        //        }
        //    }
        //    AllowancesText += "</table>";
        //    outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
        //    foreach (var item in qModel.headList)
        //    {
        //        if (item.htype == "dr")
        //        {
        //            string dtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + ".00</td></tr>";
        //            outXml += dtext;
        //            Deductions += dtext;
        //        }
        //    }
        //    outXml += " </table> </td> </tr>";
        //    Deductions += "</table>";

        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalAllowances + ".00</b></td><td class=\"tg-031e\"colspan=\"2\"><b>TotalDeductions</b></td>";

        //    outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalDeduction + ".00</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>NetClaim Rs. " + qModel.netSalary + ".00</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>NetClaim(Words)- " + Convert.ToInt32(qModel.netSalary).NumbersToWords() + " Only</b></th></tr>";

        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Signature</td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer</td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:</td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:</td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.</td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";

        //    outXml += "</tbody>";

        //    outXml += @"</div></div></body></html>";
        //    if (pdf)
        //    {
        //        string htmlStringToConvert = outXml;
        //        PdfConverter pdfConverter = new PdfConverter();
        //        pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

        //        pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //        pdfConverter.PdfDocumentOptions.ShowHeader = true;

        //        // set the header height in points
        //        pdfConverter.PdfHeaderOptions.HeaderHeight = 15;
        //        pdfConverter.PdfFooterOptions.FooterHeight = 30;

        //        TextElement footerTextElement = new TextElement(0, 10, "Page &p; of &P;",
        //                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //        footerTextElement.TextAlign = HorizontalTextAlign.Center;



        //        pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


        //        string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

        //        ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
        //        // imageElement1.KeepAspectRatio = true;
        //        imageElement1.Opacity = 100;
        //        imageElement1.DestHeight = 97f;
        //        imageElement1.DestWidth = 71f;
        //        pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
        //        ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
        //        imageElement2.KeepAspectRatio = false;
        //        imageElement2.Opacity = 5;
        //        imageElement2.DestHeight = 284f;
        //        imageElement2.DestWidth = 388F;

        //        EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
        //        float stampWidth = float.Parse("400");
        //        float stampHeight = float.Parse("400");

        //        // Center the stamp at the top of PDF page
        //        float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
        //        float stampYLocation = 150;

        //        RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

        //        Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
        //        stampTemplate.AddElement(imageElement2);
        //        byte[] pdfBytes = null;

        //        try
        //        {
        //            pdfBytes = pdfDocument.Save();
        //        }
        //        finally
        //        {
        //            // close the Document to realease all the resources
        //            pdfDocument.Close();
        //        }
        //        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

        //        url = "/salaryBills/12/" + qModel.yearId + "/" + qModel.monthID + "/" + qModel.memberCode + "/";
        //        string directory = FileSettings.SettingValue + url; ;

        //        if (!System.IO.Directory.Exists(directory))
        //        {
        //            System.IO.Directory.CreateDirectory(directory);
        //        }

        //        string path = Path.Combine(directory, qModel.monthID + "_" + qModel.yearId + "_" + qModel.memberCode + ".pdf");

        //        FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
        //        System.IO.FileAccess.Write);

        //        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //        // close file stream
        //        _FileStream.Close();
        //    }
        //    else
        //    {
        //        url = filePath;
        //    }
        //    return new string[] { url, AllowancesText, Deductions };
        //}

        //public string[] GeneratePDFforMembers(salarystructure qModel, bool pdf, string filePath)
        //{
        //    string msg = string.Empty;
        //    string url = string.Empty;
        //    MemoryStream output = new MemoryStream();
        //    string AllowancesText = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
        //    string Deductions = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
        //    string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

        //    outXml += "<table class=\"tg\">";
        //    outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No :Deposit/MLA/1047-48,Dated" + new DateTime(1900, Convert.ToInt32(qModel.monthID), 1).ToString("MMMM") + " , " + qModel.yearId + "</th></tr>";
        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";

        //    outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Name:   <b>" + qModel.memberName + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.designationName + "</span></b></td> </tr>";
        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";

        //    outXml += "<tr><td colspan=\"4\"><b>Allowances</b></td><td colspan=\"4\"><b>Deductions</b></td></tr>";
        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

        //    foreach (var item in qModel.headList)
        //    {
        //        if (item.htype == "cr")
        //        {
        //            string crtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + ".00</td></tr>";
        //            outXml += crtext;
        //            AllowancesText += crtext;
        //        }
        //    }
        //    AllowancesText += "</table>";
        //    outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
        //    foreach (var item in qModel.headList)
        //    {
        //        if (item.htype == "dr")
        //        {
        //            string drtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; \"><span style=\"float:left;\">Rs. </span>" + item.amount + ".00</td></tr>";
        //            outXml += drtext;
        //            Deductions += drtext;
        //        }
        //    }
        //    outXml += " </table> </td> </tr>";
        //    Deductions += "</table>";

        //    outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalAllowances + ".00</b></td><td class=\"tg-031e\"colspan=\"2\"><b>TotalDeductions</b></td>";

        //    outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalDeduction + ".00</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>NetClaim Rs. " + qModel.netSalary + ".00</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>NetClaim(Words)- " + Convert.ToInt32(qModel.netSalary).NumbersToWords() + " Only</b></th></tr>";

        //    //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Signature<br/></td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
        //    //outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
        //    //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer<br/></td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
        //    //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:<br/></td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
        //    //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:<br/></td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
        //    //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.<br/></td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
        //    //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";

        //    outXml += "</tbody>";

        //    outXml += @"</div></div></body></html>";
        //    if (pdf)
        //    {
        //        string htmlStringToConvert = outXml;
        //        PdfConverter pdfConverter = new PdfConverter();
        //        pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

        //        pdfConverter.PdfDocumentOptions.ShowFooter = true;
        //        pdfConverter.PdfDocumentOptions.ShowHeader = true;

        //        // set the header height in points
        //        pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
        //        pdfConverter.PdfFooterOptions.FooterHeight = 60;

        //        TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
        //                new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //        footerTextElement.TextAlign = HorizontalTextAlign.Center;

        //        TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
        //               new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //        footerTextElement1.TextAlign = HorizontalTextAlign.Left;
        //        TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
        //               new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
        //        footerTextElement3.TextAlign = HorizontalTextAlign.Right;

        //        pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
        //        pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
        //        pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

        //        string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

        //        ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
        //        // imageElement1.KeepAspectRatio = true;
        //        imageElement1.Opacity = 100;
        //        imageElement1.DestHeight = 97f;
        //        imageElement1.DestWidth = 71f;
        //        pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
        //        ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
        //        imageElement2.KeepAspectRatio = false;
        //        imageElement2.Opacity = 5;
        //        imageElement2.DestHeight = 284f;
        //        imageElement2.DestWidth = 388F;

        //        EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
        //        float stampWidth = float.Parse("400");
        //        float stampHeight = float.Parse("400");

        //        // Center the stamp at the top of PDF page
        //        float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
        //        float stampYLocation = 150;

        //        RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

        //        Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
        //        stampTemplate.AddElement(imageElement2);
        //        byte[] pdfBytes = null;

        //        try
        //        {
        //            pdfBytes = pdfDocument.Save();
        //        }
        //        finally
        //        {
        //            // close the Document to realease all the resources
        //            pdfDocument.Close();
        //        }

        //        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //        url = "/salaryBills/12/" + qModel.yearId + "/" + qModel.monthID + "/" + qModel.memberCode + "/";
        //        string directory = FileSettings.SettingValue + url; ;

        //        if (!System.IO.Directory.Exists(directory))
        //        {
        //            System.IO.Directory.CreateDirectory(directory);
        //        }


        //        string path = Path.Combine(directory, qModel.monthID + "_" + qModel.yearId + "_" + qModel.memberCode + ".pdf");

        //        FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
        //        System.IO.FileAccess.Write);

        //        _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //        // close file stream
        //        _FileStream.Close();
        //    }
        //    else
        //    {
        //        url = filePath;
        //    }
        //    return new string[] { url, AllowancesText, Deductions };
        //}
        //#endregion

        //#region Regenerated Bills
        //public ActionResult RegenerateSalary()
        //{
        //    return View();

        //}
        //public Object generatePdf(List<salaryBillInfo> _listsbi, string mailStatus, string smsStatus)
        //{
        //    ZipFile zip = new ZipFile();

        //    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

        //    string[] returnResult = null;
        //    foreach (var item in _listsbi)
        //    {
        //        if (item.checkStatus != null && item.status == 1)
        //        {
        //            salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", item.AutoID);
        //            try
        //            {
        //                string filepath = string.Empty;
        //                bool pdf = true;
        //                string SavedPdfPath = string.Empty;
        //                if (item.generateStatus == 1)
        //                {
        //                    pdf = false;
        //                    filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", item.AutoID) as string;

        //                }
        //                if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
        //                {

        //                    returnResult = GeneratePDFforSpeaker(s, pdf, filepath);

        //                }
        //                else
        //                {
        //                    returnResult = GeneratePDFforMembers(s, pdf, filepath);
        //                }
        //                if (pdf)
        //                {
        //                    Helper.ExecuteService("salaryheads", "updateBillFilePath", new string[] { item.AutoID.ToString(), returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf" });
        //                    SavedPdfPath = returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf";
        //                }
        //                else
        //                {
        //                    SavedPdfPath = filepath;
        //                }
        //                SalaryNotification sn = new SalaryNotification();
        //                List<string> emailAddresses = new List<string>();
        //                List<string> phoneNumbers = new List<string>();
        //                sn.mailSubject = "testing mail";

        //                phoneNumbers.Add("9045337778");
        //                phoneNumbers.Add("8395055554");
        //                sn.mobileStatus = true;
        //                sn.emailStatus = true;
        //                if (string.IsNullOrEmpty(smsStatus))
        //                {
        //                    sn.mobileStatus = false;

        //                }
        //                if (string.IsNullOrEmpty(mailStatus))
        //                {
        //                    sn.emailStatus = false;

        //                }
        //                sn.msgBody = "testing msg";
        //                sn.attachmentUrl = "";
        //                emailAddresses.Add("durgesh.net4@gmail.com");
        //                emailAddresses.Add("durgesh@sblsoftware.com");

        //                sn.mobileList = phoneNumbers;
        //                sn.emailList = emailAddresses;
        //                string mailText = System.IO.File.ReadAllText(Server.MapPath("/SalaryMailer/index.html"));
        //                mailText = mailText.Replace("__MemberName__", s.memberName);
        //                mailText = mailText.Replace("__MonthName__ ", new DateTime(1900, Convert.ToInt32(s.monthID), 1).ToString("MMMM"));
        //                mailText = mailText.Replace("__year__", s.yearId.ToString());
        //                mailText = mailText.Replace("__Allowances__", returnResult[1]);
        //                mailText = mailText.Replace("__Deductions__", returnResult[2]);
        //                mailText = mailText.Replace("__totalAllowances__", s.totalAllowances + ".00");
        //                mailText = mailText.Replace("__totalDeductions__", s.totalDeduction + ".00");
        //                mailText = mailText.Replace("__grossAmount__", s.netSalary + ".00");
        //                mailText = mailText.Replace("__amountinwords__", Convert.ToInt32(s.netSalary).NumbersToWords() + " Only");
        //                sn.mailBody = mailText;
        //                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //                string directory = FileSettings.SettingValue + SavedPdfPath;

        //                // EAttachment ea = new EAttachment { FileName = new
        //                // FileInfo(Server.MapPath(SavedPdfPath)).Name, FileContent =
        //                // System.IO.File.ReadAllBytes(Server.MapPath(SavedPdfPath)) };

        //                //SendEmailDetails(sn, Server.MapPath(SavedPdfPath));

        //                zip.AddFile(directory, "SalaryBills");
        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //        }
        //    }
        //    Response.Clear();
        //    Response.BufferOutput = false;
        //    string zipName = String.Format("{0}.zip", "salaryBill");
        //    Response.ContentType = "application/zip";
        //    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

        //    zip.Save(Response.OutputStream);
        //    Response.End();

        //    return Response;
        //}
        //public ActionResult getBillList(string monthID, String YearID, string status)
        //{
        //    int[] arr = { int.Parse(monthID), int.Parse(YearID), int.Parse(status) };
        //    List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "getSalaryBillList", arr);
        //    return PartialView("_salaryBillList", _slist);
        //}
        //public Object generatePdfByAutoID(string ID, string pdf)
        //{
        //    ZipFile zip = new ZipFile();

        //    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

        //    string[] returnResult = null;
        //    string filepath = string.Empty;
        //    string SavedPdfPath = string.Empty;

        //    salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", ID);
        //    try
        //    {
        //        if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
        //        {
        //            returnResult = GeneratePDFforSpeaker(s, true, filepath);
        //        }
        //        else
        //        {
        //            returnResult = GeneratePDFforMembers(s, true, filepath);
        //        }



        //        SavedPdfPath = returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf";



        //    }

        //    catch (Exception ex)
        //    {

        //    }


        //    return Json("http://secure.shimlamc.org/secureFileStructure/" + SavedPdfPath, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult saveBillRemark(string remark, string AutoID)
        //{
        //    string msg = "SuperAdmin**" + remark + "**" + DateTime.Now.ToShortDateString() + " , " + DateTime.Now.ToShortTimeString() + "@@";
        //    string[] arr = new string[] { msg, AutoID };
        //    string RemarkList = (string)Helper.ExecuteService("salaryheads", "saveBillRemark", arr);
        //    return Json(RemarkList, JsonRequestBehavior.AllowGet);
        //}
        //#endregion
        //#region Send Email
        //public ActionResult sendSMSBYAutoID(string autoID)
        //{


        //    string filepath = string.Empty;
        //    string SavedPdfPath = string.Empty;
        //    filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", autoID) as string;
        //    salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", autoID);
        //    try
        //    {
        //        SavedPdfPath = filepath;
        //        SalaryNotification sn = new SalaryNotification();
        //        List<string> emailAddresses = new List<string>();
        //        List<string> phoneNumbers = new List<string>();
        //        sn.mailSubject = "testing mail";
        //        phoneNumbers.Add("9045337778");
        //        phoneNumbers.Add("8395055554");
        //        sn.mobileStatus = true;
        //        sn.emailStatus = false;
        //        sn.msgBody = "testing msg";
        //        sn.attachmentUrl = "";
        //        emailAddresses.Add("durgesh.net4@gmail.com");
        //        emailAddresses.Add("durgesh@sblsoftware.com");
        //        sn.mobileList = phoneNumbers;
        //        sn.emailList = emailAddresses;
        //        sn.mailBody = "test mail";
        //        string directory = Server.MapPath(SavedPdfPath);
        //        SendEmailDetails(sn, Server.MapPath(SavedPdfPath));
        //    }

        //    catch (Exception ex)
        //    {

        //    }


        //    return Json(SavedPdfPath, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult sendMAILBYAutoID(string autoID)
        //{
        //    string[] returnResult = null;
        //    string filepath = string.Empty;
        //    string SavedPdfPath = string.Empty;


        //    filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", autoID) as string;


        //    salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", autoID);
        //    try
        //    {
        //        if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
        //        {
        //            returnResult = GeneratePDFforSpeaker(s, false, filepath);
        //        }
        //        else
        //        {
        //            returnResult = GeneratePDFforMembers(s, false, filepath);
        //        }

        //        SavedPdfPath = filepath;
        //        SalaryNotification sn = new SalaryNotification();
        //        List<string> emailAddresses = new List<string>();
        //        List<string> phoneNumbers = new List<string>();
        //        sn.mailSubject = "testing mail";
        //        phoneNumbers.Add("9045337778");
        //        phoneNumbers.Add("8395055554");

        //        sn.mobileStatus = false;
        //        sn.emailStatus = true;
        //        sn.msgBody = "testing msg";
        //        sn.attachmentUrl = "";
        //        emailAddresses.Add("durgesh.net4@gmail.com");
        //        emailAddresses.Add("durgesh@sblsoftware.com");
        //        sn.mobileList = phoneNumbers;
        //        sn.emailList = emailAddresses;
        //        string mailText = System.IO.File.ReadAllText(Server.MapPath("/SalaryMailer/index.html"));
        //        mailText = mailText.Replace("__MemberName__", s.memberName);
        //        mailText = mailText.Replace("__MonthName__ ", new DateTime(1900, Convert.ToInt32(s.monthID), 1).ToString("MMMM"));
        //        mailText = mailText.Replace("__year__", s.yearId.ToString());
        //        mailText = mailText.Replace("__Allowances__", returnResult[1]);
        //        mailText = mailText.Replace("__Deductions__", returnResult[2]);
        //        mailText = mailText.Replace("__totalAllowances__", s.totalAllowances + ".00");
        //        mailText = mailText.Replace("__totalDeductions__", s.totalDeduction + ".00");
        //        mailText = mailText.Replace("__grossAmount__", s.netSalary + ".00");
        //        mailText = mailText.Replace("__amountinwords__", Convert.ToInt32(s.netSalary).NumbersToWords() + " Only");
        //        sn.mailBody = mailText;
        //        string directory = Server.MapPath(SavedPdfPath);
        //        SendEmailDetails(sn, Server.MapPath(SavedPdfPath));

        //    }

        //    catch (Exception ex)
        //    {

        //    }


        //    return Json(SavedPdfPath, JsonRequestBehavior.AllowGet);
        //}
        //public static void SendEmailDetails(SalaryNotification EntityModel, string SavedPdfPath)
        //{
        //    #region SMS And Email

        //    List<string> emailAddresses = new List<string>();
        //    List<string> phoneNumbers = new List<string>();

        //    SMSMessageList smsMessage = new SMSMessageList();
        //    CustomMailMessage emailMessage = new CustomMailMessage();

        //    //# SMS Settings
        //    if (EntityModel != null)
        //    {
        //        emailAddresses.AddRange(EntityModel.emailList);
        //        phoneNumbers.AddRange(EntityModel.mobileList);
        //    }
        //    if (emailAddresses != null && phoneNumbers != null)
        //    {
        //        foreach (var item in phoneNumbers)
        //        {
        //            smsMessage.MobileNo.Add(item);
        //        }
        //        foreach (var item in emailAddresses)
        //        {
        //            emailMessage._toList.Add(item);
        //        }
        //    }

        //    smsMessage.SMSText = EntityModel.msgBody;
        //    smsMessage.ModuleActionID = 1;
        //    smsMessage.UniqueIdentificationID = 1;
        //    emailMessage._isBodyHtml = true;
        //    emailMessage._subject = EntityModel.mailSubject;
        //    emailMessage._body = EntityModel.mailBody;
        //    EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
        //    emailMessage._attachments = new List<EAttachment>();
        //    emailMessage._attachments.Add(ea);
        //    try
        //    {
        //        if (EntityModel.emailList != null || EntityModel.mobileList != null)
        //        {
        //            Notification.Send(EntityModel.mobileStatus, EntityModel.emailStatus, smsMessage, emailMessage);
        //        }

        //    }
        //    catch (Exception exp)
        //    {
        //        string errorMsg = exp.Message;
        //        // throw;
        //    }

        //    #endregion SMS And Email
        //}
        //public object DownloadPDF(string url)
        //{
        //    try
        //    {
        //        //string url = "/QuestionList" + "/DiaryWiseStarred/" + "12/" + "7";

        //        string directory = Server.MapPath(url);

        //        using (ZipFile zip = new ZipFile())
        //        {
        //            zip.AlternateEncodingUsage = ZipOption.AsNecessary;

        //            //zip.AddDirectoryByName("Files");

        //            if (Directory.Exists(directory))
        //            {
        //                zip.AddDirectory(directory, "salaryBills");
        //            }
        //            else
        //            {
        //            }

        //            Response.Clear();
        //            Response.BufferOutput = false;
        //            string zipName = String.Format("{0}.zip", "salaryBill");
        //            Response.ContentType = "application/zip";
        //            Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

        //            zip.Save(Response.OutputStream);
        //            Response.End();

        //            return Response;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "No File Available!";
        //    }

        //    return Response;
        //}
        //#endregion Send Email


        #region salary bills update

        public ActionResult UpdateSalaryBills()
        {
            return View();
        }
        public ActionResult getNumberToWords(string number)
        {
            int num = int.Parse(number);
            if (Request.IsAjaxRequest())
            {
                return Json(num.NumbersToWords(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content(num.NumbersToWords());
            }
        }
        public JsonResult fillCurrentMember(string assemblyid)
        {
            List<fillCurrentMamber> list = (List<fillCurrentMamber>)Helper.ExecuteService("salaryheads", "getCurrentMemberList", assemblyid);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult getSalaryHeadsList(string memberid, string catId, string monthID, string yearID, int assemblyid, int ispart)
        //{
        //    salarystructure _salarystructure = null;

        //    int[] arr = new int[] { int.Parse(memberid), int.Parse(monthID), int.Parse(yearID), assemblyid, ispart, int.Parse(catId) };

        //    _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
        //    _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();
        //    _salarystructure.ispart = ispart;
        //    _salarystructure.AssemblyId = assemblyid;

        //    return PartialView("_salaryHeadsForMember", _salarystructure);
        //}

        public ActionResult getSalaryHeadsList(string memberid, string catId, string monthID, string yearID, int assemblyid, int ispart)
        {
            salarystructure _salarystructure = null;

            int[] arr = new int[] { int.Parse(memberid), int.Parse(monthID), int.Parse(yearID), assemblyid, ispart, int.Parse(catId) };
            if (arr[0] == 129 && arr[1] == 1 && arr[2] == 2018 && arr[3] == 13 && arr[4] == 1)
            {
                arr[5] = 3;
            }
            _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
            _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();
            _salarystructure.ispart = ispart;
            _salarystructure.catID = arr[5];
            _salarystructure.AssemblyId = assemblyid;

            return PartialView("_salaryHeadsForMember", _salarystructure);
        }
        
        
        public ActionResult getSalaryHeadsListforAllMembers(string monthID, string yearID)
        {
            List<mMember> _mList = (List<mMember>)Helper.ExecuteService("salaryheads", "getallmembers", null);
            List<salarystructure> _sList = new List<salarystructure>();
            foreach (var member in _mList)
            {
                int memberid = member.MemberCode;
                salarystructure _salarystructure = null;

                int[] arr = new int[] { memberid, int.Parse(monthID), int.Parse(yearID) };
                _salarystructure = (salarystructure)Helper.ExecuteService("salaryheads", "getMonthlySalaryHeads", arr);
                _salarystructure.netSalaryInWords = Convert.ToInt32(_salarystructure.netSalary).NumbersToWords();

                _sList.Add(_salarystructure);
            }

            return PartialView("_salaryHeadsForAllMembers", _sList);
        }
        public ActionResult getSpeakerCurrentList(string designationID, int AssemblyId)
        {
            string[] arr = { designationID, Convert.ToString(AssemblyId) };
            List<mMember> _list = (List<mMember>)Helper.ExecuteService("salaryheads", "getDetailsByDesignation", arr);

            List<SpeakerList> _sList = new List<SpeakerList>();
            foreach (var item in _list)
            {
                SpeakerList _speaker = new SpeakerList();
                _speaker.speakerID = item.MemberCode;
                _speaker.speakerName = item.Name;
                _sList.Add(_speaker);
            }
            return Json(_sList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult generateBills(salarystructure _salarystructure, List<salarystructure> _salarystructureList, string Command)
        {
            ZipFile zip = new ZipFile();

            zip.AlternateEncodingUsage = ZipOption.AsNecessary;
            if (ModelState.IsValid)
            {
                if (Command == "members")
                {

                    if (_salarystructure.Mode == "update")
                    {
                        Helper.ExecuteService("salaryheads", "updateSalaryBills", _salarystructure);
                    }
                }
                else
                {
                    if (Command == "delete")
                    {
                        Helper.ExecuteService("salaryheads", "deleteSalaryBills", _salarystructure);
                    }
                    else
                    {

                        try
                        {
                            string[] returnResult = null;
                            string filepath = string.Empty;
#pragma warning disable CS0219 // The variable 'pdf' is assigned but its value is never used
                            bool pdf = true;
#pragma warning restore CS0219 // The variable 'pdf' is assigned but its value is never used
                            string SavedPdfPath = string.Empty;

                            if (_salarystructure.designationName == "Speaker" || _salarystructure.designationName == "Deputy Speaker")
                            {

                                returnResult = GeneratePDFforSpeaker(_salarystructure, true, filepath);

                            }
                            else
                            {
                                returnResult = GeneratePDFforMembers(_salarystructure, true, filepath);
                            }

                            SavedPdfPath = SavedPdfPath = returnResult[0] + _salarystructure.monthID + "_" + _salarystructure.yearId + "_" + _salarystructure.memberCode + ".pdf";


                            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                            string directory = FileSettings.SettingValue + SavedPdfPath;



                            zip.AddFile(directory, "SalaryBills");
                            Response.Clear();
                            Response.BufferOutput = false;
                            string zipName = String.Format("{0}.zip", "salaryBill");
                            Response.ContentType = "application/zip";
                            Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

                            zip.Save(Response.OutputStream);
                            Response.End();

                            //return Response;
                        }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                        catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                        {

                        }
                    }
                }


            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }
        public string[] GeneratePDFforSpeaker(salarystructure qModel, bool pdf, string filePath)
        {
            string msg = string.Empty;
            string url = string.Empty;
            MemoryStream output = new MemoryStream();
            string AllowancesText = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string Deductions = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

            outXml += "<table class=\"tg\">";
            outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No :Deposit/MLA/1047-48,Dated " + new DateTime(1900, Convert.ToInt32(qModel.monthID), 1).ToString("MMMM") + " , " + qModel.yearId + "</th></tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";

            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Name:   <b>" + qModel.memberName + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.designationName + "</span></b></td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += " <tr><td colspan=\"3\">TreasuryCode</td><td>SML00</td><td colspan=\"3\">Gazetted/NonGazetted</td><td colspan=\"2\">G</td></tr>";
            outXml += "<tr><td colspan=\"3\">DemandNumber</td><td>01</td><td colspan=\"3\">SchemaCodeNo</td><td colspan=\"2\">-</td></tr>";
            outXml += "<tr><td colspan=\"3\">DDOCodeNo</td><td>092</td><td colspan=\"3\">ObjectCode</td><td colspan=\"2\">01</td></tr>";
            outXml += "<tr><td colspan=\"3\">MajorHead</td><td>2011</td><td colspan=\"3\">VotedCharged</td><td colspan=\"2\">C</td></tr>";

            outXml += "<tr><td colspan=\"3\">SubMajorHead</td><td>02</td><td colspan=\"3\">Plan/Nonplan</td><td colspan=\"2\">N</td></tr>";
            outXml += "<tr><td colspan=\"3\">MinorHead</td><td>101</td><td colspan=\"3\"></td><td colspan=\"2\"></td></tr>";
            outXml += "<tr><td colspan=\"3\">subHead</td><td>01</td><td colspan=\"3\"></td><td colspan=\"2\"></td></tr><tr><td colspan=\"4\"><b>Allowances</b></td><td colspan=\"4\"><b>Deductions</b></td></tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

            foreach (var item in qModel.headList)
            {
                if (item.htype == "cr")
                {
                    string ctext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + ".00</td></tr>";
                    outXml += ctext;
                    AllowancesText += ctext;
                }
            }
            AllowancesText += "</table>";
            outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
            foreach (var item in qModel.headList)
            {
                if (item.htype == "dr")
                {
                    string dtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + ".00</td></tr>";
                    outXml += dtext;
                    Deductions += dtext;
                }
            }
            outXml += " </table> </td> </tr>";
            Deductions += "</table>";

            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalAllowances + ".00</b></td><td class=\"tg-031e\"colspan=\"2\"><b>TotalDeductions</b></td>";

            outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalDeduction + ".00</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>NetClaim Rs. " + qModel.netSalary + ".00</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>NetClaim(Words)- " + Convert.ToInt32(qModel.netSalary).NumbersToWords() + " Only</b></th></tr>";

            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Signature</td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer</td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:</td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:</td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.</td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";

            outXml += "</tbody>";

            outXml += @"</div></div></body></html>";
            if (pdf)
            {
                string htmlStringToConvert = outXml;
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 15;
                pdfConverter.PdfFooterOptions.FooterHeight = 30;

                TextElement footerTextElement = new TextElement(0, 10, "Page &p; of &P;",
                        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.TextAlign = HorizontalTextAlign.Center;



                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);


                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
                // imageElement1.KeepAspectRatio = true;
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 5;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388F;

                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
                float stampWidth = float.Parse("400");
                float stampHeight = float.Parse("400");

                // Center the stamp at the top of PDF page
                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
                float stampYLocation = 150;

                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
                stampTemplate.AddElement(imageElement2);
                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                url = "/salaryBills/12/" + qModel.yearId + "/" + qModel.monthID + "/" + qModel.memberCode + "/";
                string directory = FileSettings.SettingValue + url; ;

                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(directory, qModel.monthID + "_" + qModel.yearId + "_" + qModel.memberCode + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
            }
            else
            {
                url = filePath;
            }
            return new string[] { url, AllowancesText, Deductions };
        }

        public string[] GeneratePDFforMembers(salarystructure qModel, bool pdf, string filePath)
        {
            string msg = string.Empty;
            string url = string.Empty;
            MemoryStream output = new MemoryStream();
            string AllowancesText = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string Deductions = "<table style=\"width: 100%; vertical-align: top;\" class=\"itg\">";
            string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;'><div><div style='width: 100%;'>";

            outXml += "<table class=\"tg\">";
            outXml += "<tr><th class=\"tg-031e\" colspan=\"8\">A.G.H.P SHIMLA PAY SLIP No :Deposit/MLA/1047-48,Dated" + new DateTime(1900, Convert.ToInt32(qModel.monthID), 1).ToString("MMMM") + " , " + qModel.yearId + "</th></tr>";
            outXml += "<tr><td class=\"tg-031e\" colspan=\"8\">Note: Government accepts no responsibility or any fraud or misappropriation in respect of money, cheques or drafts made over to a messenger.<br/><br/></td> </tr>";

            outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Name:   <b>" + qModel.memberName + "</b></td><td class=\"tg-031e\" colspan=\"4\"><b><span id=\"Designation\">" + qModel.designationName + "</span></b></td> </tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";

            outXml += "<tr><td colspan=\"4\"><b>Allowances</b></td><td colspan=\"4\"><b>Deductions</b></td></tr>";
            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += "<tr><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\"> ";

            foreach (var item in qModel.headList)
            {
                if (item.htype == "cr")
                {
                    string crtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\">Rs. </span>" + item.amount + ".00</td></tr>";
                    outXml += crtext;
                    AllowancesText += crtext;
                }
            }
            AllowancesText += "</table>";
            outXml += "</table></td><td colspan=\"4\" valign=\"top\" style=\"padding: 0;\"><table width=\"100%\">";
            foreach (var item in qModel.headList)
            {
                if (item.htype == "dr")
                {
                    string drtext = "<tr> <td style=\"width: 60%\"> " + item.headName + " </td><td style=\"text-align: right; \"><span style=\"float:left;\">Rs. </span>" + item.amount + ".00</td></tr>";
                    outXml += drtext;
                    Deductions += drtext;
                }
            }
            outXml += " </table> </td> </tr>";
            Deductions += "</table>";

            outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            outXml += " <tr><td class=\"tg-031e\"  colspan=\"2\"><b>Gross Amount</b></td><td colspan=\"2\" style=\"text-align: right;  padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalAllowances + ".00</b></td><td class=\"tg-031e\"colspan=\"2\"><b>TotalDeductions</b></td>";

            outXml += "<td colspan=\"2\" style=\"text-align: right; padding-right: 30px;\"><span style=\"float:left;\"><b>Rs. </span>" + qModel.totalDeduction + ".00</b></td></tr><tr><th align=\"text-align:left;\" colspan=\"8\"><b>NetClaim Rs. " + qModel.netSalary + ".00</b></th></tr><tr><th align=\"text-align:left;\"colspan=\"8\"><b>NetClaim(Words)- " + Convert.ToInt32(qModel.netSalary).NumbersToWords() + " Only</b></th></tr>";

            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Signature<br/></td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
            //outXml += "<tr><th align=\"text-align:left;\" colspan=\"8\"><hr/></th></tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">For the Use of A.G. Officer<br/></td><td class=\"tg-031e\" colspan=\"4\">For the Use in Treaury</td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Admitted Rs:<br/></td><td class=\"tg-031e\" colspan=\"4\">Signature</td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Object Rs:<br/></td><td class=\"tg-031e\" colspan=\"4\"></td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Auditor    supdt.<br/></td><td class=\"tg-031e\" colspan=\"4\">Tresury Officer</td> </tr>";
            //outXml += "<tr><td class=\"tg-031e\" colspan=\"4\">Gazetted Officer </td><td class=\"tg-031e\" colspan=\"4\">Incorporated in Treasury Accountant</td> </tr>";

            outXml += "</tbody>";

            outXml += @"</div></div></body></html>";
            if (pdf)
            {
                string htmlStringToConvert = outXml;
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
                pdfConverter.PdfFooterOptions.FooterHeight = 60;

                TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement1.TextAlign = HorizontalTextAlign.Left;
                TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement3.TextAlign = HorizontalTextAlign.Right;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
                pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
                pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
                // imageElement1.KeepAspectRatio = true;
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 5;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388F;

                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(outXml);
                float stampWidth = float.Parse("400");
                float stampHeight = float.Parse("400");

                // Center the stamp at the top of PDF page
                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
                float stampYLocation = 150;

                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
                stampTemplate.AddElement(imageElement2);
                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                url = "/salaryBills/12/" + qModel.yearId + "/" + qModel.monthID + "/" + qModel.memberCode + "/";
                string directory = FileSettings.SettingValue + url; ;

                if (!System.IO.Directory.Exists(directory))
                {
                    System.IO.Directory.CreateDirectory(directory);
                }


                string path = Path.Combine(directory, qModel.monthID + "_" + qModel.yearId + "_" + qModel.memberCode + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
            }
            else
            {
                url = filePath;
            }
            return new string[] { url, AllowancesText, Deductions };
        }
        public JsonResult GetAllAssemblyReverse()
        {
            //   (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            List<SBL.DomainModel.Models.Assembly.mAssembly> list = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            //  List<fillListGenric> list = (List<fillListGenric>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Regenerated Bills
        public ActionResult RegenerateSalary()
        {
            return View();

        }
        public Object generatePdf(List<salaryBillInfo> _listsbi, string mailStatus, string smsStatus)
        {
            ZipFile zip = new ZipFile();

            zip.AlternateEncodingUsage = ZipOption.AsNecessary;

            string[] returnResult = null;
            foreach (var item in _listsbi)
            {
                if (item.checkStatus != null && item.status == 1)
                {
                    salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", item.AutoID);
                    try
                    {
                        string filepath = string.Empty;
                        bool pdf = true;
                        string SavedPdfPath = string.Empty;
                        if (item.generateStatus == 1)
                        {
                            pdf = false;
                            filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", item.AutoID) as string;

                        }
                        if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
                        {

                            returnResult = GeneratePDFforSpeaker(s, pdf, filepath);

                        }
                        else
                        {
                            returnResult = GeneratePDFforMembers(s, pdf, filepath);
                        }
                        if (pdf)
                        {
                            Helper.ExecuteService("salaryheads", "updateBillFilePath", new string[] { item.AutoID.ToString(), returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf" });
                            SavedPdfPath = returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf";
                        }
                        else
                        {
                            SavedPdfPath = filepath;
                        }
                        SalaryNotification sn = new SalaryNotification();
                        List<string> emailAddresses = new List<string>();
                        List<string> phoneNumbers = new List<string>();
                        sn.mailSubject = "testing mail";

                        phoneNumbers.Add("9045337778");
                        phoneNumbers.Add("8395055554");
                        sn.mobileStatus = true;
                        sn.emailStatus = true;
                        if (string.IsNullOrEmpty(smsStatus))
                        {
                            sn.mobileStatus = false;

                        }
                        if (string.IsNullOrEmpty(mailStatus))
                        {
                            sn.emailStatus = false;

                        }
                        sn.msgBody = "testing msg";
                        sn.attachmentUrl = "";
                        emailAddresses.Add("durgesh.net4@gmail.com");
                        emailAddresses.Add("durgesh@sblsoftware.com");

                        sn.mobileList = phoneNumbers;
                        sn.emailList = emailAddresses;
                        string mailText = System.IO.File.ReadAllText(Server.MapPath("/SalaryMailer/index.html"));
                        mailText = mailText.Replace("__MemberName__", s.memberName);
                        mailText = mailText.Replace("__MonthName__ ", new DateTime(1900, Convert.ToInt32(s.monthID), 1).ToString("MMMM"));
                        mailText = mailText.Replace("__year__", s.yearId.ToString());
                        mailText = mailText.Replace("__Allowances__", returnResult[1]);
                        mailText = mailText.Replace("__Deductions__", returnResult[2]);
                        mailText = mailText.Replace("__totalAllowances__", s.totalAllowances + ".00");
                        mailText = mailText.Replace("__totalDeductions__", s.totalDeduction + ".00");
                        mailText = mailText.Replace("__grossAmount__", s.netSalary + ".00");
                        mailText = mailText.Replace("__amountinwords__", Convert.ToInt32(s.netSalary).NumbersToWords() + " Only");
                        sn.mailBody = mailText;
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string directory = FileSettings.SettingValue + SavedPdfPath;

                        // EAttachment ea = new EAttachment { FileName = new
                        // FileInfo(Server.MapPath(SavedPdfPath)).Name, FileContent =
                        // System.IO.File.ReadAllBytes(Server.MapPath(SavedPdfPath)) };

                        //SendEmailDetails(sn, Server.MapPath(SavedPdfPath));

                        zip.AddFile(directory, "SalaryBills");
                    }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
                    catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
                    {

                    }
                }
            }
            Response.Clear();
            Response.BufferOutput = false;
            string zipName = String.Format("{0}.zip", "salaryBill");
            Response.ContentType = "application/zip";
            Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

            zip.Save(Response.OutputStream);
            Response.End();

            return Response;
        }
        public ActionResult getBillList(string monthID, String YearID, string status)
        {
            int[] arr = { int.Parse(monthID), int.Parse(YearID), int.Parse(status) };
            List<salaryBillInfo> _slist = (List<salaryBillInfo>)Helper.ExecuteService("salaryheads", "getSalaryBillList", arr);
            return PartialView("_salaryBillList", _slist);
        }
        public Object generatePdfByAutoID(string ID, string pdf)
        {
            ZipFile zip = new ZipFile();

            zip.AlternateEncodingUsage = ZipOption.AsNecessary;

            string[] returnResult = null;
            string filepath = string.Empty;
            string SavedPdfPath = string.Empty;

            salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", ID);
            try
            {
                if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
                {
                    returnResult = GeneratePDFforSpeaker(s, true, filepath);
                }
                else
                {
                    returnResult = GeneratePDFforMembers(s, true, filepath);
                }



                SavedPdfPath = returnResult[0] + s.monthID + "_" + s.yearId + "_" + s.memberCode + ".pdf";



            }

#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }


            return Json("http://secure.shimlamc.org/secureFileStructure/" + SavedPdfPath, JsonRequestBehavior.AllowGet);
        }
        public ActionResult saveBillRemark(string remark, string AutoID)
        {
            string msg = "SuperAdmin**" + remark + "**" + DateTime.Now.ToShortDateString() + " , " + DateTime.Now.ToShortTimeString() + "@@";
            string[] arr = new string[] { msg, AutoID };
            string RemarkList = (string)Helper.ExecuteService("salaryheads", "saveBillRemark", arr);
            return Json(RemarkList, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Send Email
        public ActionResult sendSMSBYAutoID(string autoID)
        {


            string filepath = string.Empty;
            string SavedPdfPath = string.Empty;
            filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", autoID) as string;
            salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", autoID);
            try
            {
                SavedPdfPath = filepath;
                SalaryNotification sn = new SalaryNotification();
                List<string> emailAddresses = new List<string>();
                List<string> phoneNumbers = new List<string>();
                sn.mailSubject = "testing mail";
                phoneNumbers.Add("9045337778");
                phoneNumbers.Add("8395055554");
                sn.mobileStatus = true;
                sn.emailStatus = false;
                sn.msgBody = "testing msg";
                sn.attachmentUrl = "";
                emailAddresses.Add("durgesh.net4@gmail.com");
                emailAddresses.Add("durgesh@sblsoftware.com");
                sn.mobileList = phoneNumbers;
                sn.emailList = emailAddresses;
                sn.mailBody = "test mail";
                string directory = Server.MapPath(SavedPdfPath);
                SendEmailDetails(sn, Server.MapPath(SavedPdfPath));
            }

#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }


            return Json(SavedPdfPath, JsonRequestBehavior.AllowGet);
        }
        public ActionResult sendMAILBYAutoID(string autoID)
        {
            string[] returnResult = null;
            string filepath = string.Empty;
            string SavedPdfPath = string.Empty;


            filepath = Helper.ExecuteService("salaryheads", "getBillFilePath", autoID) as string;


            salarystructure s = (salarystructure)Helper.ExecuteService("salaryheads", "getsalaryBillByID", autoID);
            try
            {
                if (s.designationName == "Speaker" || s.designationName == "Deputy Speaker")
                {
                    returnResult = GeneratePDFforSpeaker(s, false, filepath);
                }
                else
                {
                    returnResult = GeneratePDFforMembers(s, false, filepath);
                }

                SavedPdfPath = filepath;
                SalaryNotification sn = new SalaryNotification();
                List<string> emailAddresses = new List<string>();
                List<string> phoneNumbers = new List<string>();
                sn.mailSubject = "testing mail";
                phoneNumbers.Add("9045337778");
                phoneNumbers.Add("8395055554");

                sn.mobileStatus = false;
                sn.emailStatus = true;
                sn.msgBody = "testing msg";
                sn.attachmentUrl = "";
                emailAddresses.Add("durgesh.net4@gmail.com");
                emailAddresses.Add("durgesh@sblsoftware.com");
                sn.mobileList = phoneNumbers;
                sn.emailList = emailAddresses;
                string mailText = System.IO.File.ReadAllText(Server.MapPath("/SalaryMailer/index.html"));
                mailText = mailText.Replace("__MemberName__", s.memberName);
                mailText = mailText.Replace("__MonthName__ ", new DateTime(1900, Convert.ToInt32(s.monthID), 1).ToString("MMMM"));
                mailText = mailText.Replace("__year__", s.yearId.ToString());
                mailText = mailText.Replace("__Allowances__", returnResult[1]);
                mailText = mailText.Replace("__Deductions__", returnResult[2]);
                mailText = mailText.Replace("__totalAllowances__", s.totalAllowances + ".00");
                mailText = mailText.Replace("__totalDeductions__", s.totalDeduction + ".00");
                mailText = mailText.Replace("__grossAmount__", s.netSalary + ".00");
                mailText = mailText.Replace("__amountinwords__", Convert.ToInt32(s.netSalary).NumbersToWords() + " Only");
                sn.mailBody = mailText;
                string directory = Server.MapPath(SavedPdfPath);
                SendEmailDetails(sn, Server.MapPath(SavedPdfPath));

            }

#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

            }


            return Json(SavedPdfPath, JsonRequestBehavior.AllowGet);
        }
        public static void SendEmailDetails(SalaryNotification EntityModel, string SavedPdfPath)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (EntityModel != null)
            {
                emailAddresses.AddRange(EntityModel.emailList);
                phoneNumbers.AddRange(EntityModel.mobileList);
            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            smsMessage.SMSText = EntityModel.msgBody;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            emailMessage._isBodyHtml = true;
            emailMessage._subject = EntityModel.mailSubject;
            emailMessage._body = EntityModel.mailBody;
            EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
            emailMessage._attachments = new List<EAttachment>();
            emailMessage._attachments.Add(ea);
            try
            {
                if (EntityModel.emailList != null || EntityModel.mobileList != null)
                {
                    Notification.Send(EntityModel.mobileStatus, EntityModel.emailStatus, smsMessage, emailMessage);
                }

            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                // throw;
            }

            #endregion SMS And Email
        }
        public object DownloadPDF(string url)
        {
            try
            {
                //string url = "/QuestionList" + "/DiaryWiseStarred/" + "12/" + "7";

                string directory = Server.MapPath(url);

                using (ZipFile zip = new ZipFile())
                {
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;

                    //zip.AddDirectoryByName("Files");

                    if (Directory.Exists(directory))
                    {
                        zip.AddDirectory(directory, "salaryBills");
                    }
                    else
                    {
                    }

                    Response.Clear();
                    Response.BufferOutput = false;
                    string zipName = String.Format("{0}.zip", "salaryBill");
                    Response.ContentType = "application/zip";
                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);

                    zip.Save(Response.OutputStream);
                    Response.End();

                    return Response;
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return "No File Available!";
            }

#pragma warning disable CS0162 // Unreachable code detected
            return Response;
#pragma warning restore CS0162 // Unreachable code detected
        }
        #endregion Send Email

    }
}

