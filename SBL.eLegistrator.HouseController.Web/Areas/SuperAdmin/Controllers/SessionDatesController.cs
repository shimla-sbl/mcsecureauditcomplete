﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SessionDatesController : Controller
    {
        //
        // GET: /SuperAdmin/SessionDates/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                //Code for AssemblyList 
                var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.assmeblyList = assmeblyList;
                //Code for SessionList 
                SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                ViewBag.sessionList = sessionList;
                //Code for SessionDateList 

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                sesdate.AssemblyId = data.AssemblyID;

                //SessionDatesViewModel sesdate = new SessionDatesViewModel();
                //sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                //sesdate.AssemblyId = data.AssemblyID;

                //model.VAssemblyId = data.AssemblyID;
                //model.VSessionId = sesdate.SessionId;
                //var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

               // var sessionDates = (List<mSessionDate>)Helper.ExecuteService("Session", "GetAllSessionDates", null);
               
               // var model = sessionDates.ToViewModel();
               // var model = sessionDates;

                ViewBag.savedmsg = TempData["Saved"];
                return View(sesdate);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Session Dates Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult GetSpecificSessionDates(int assemblyId, int sessionId)
        {
            try
            {
                mSessionDate obj = new mSessionDate();
                obj.SessionId = sessionId;
                obj.AssemblyId = assemblyId;

                //var data = (List<mSessionDate>)Helper.ExecuteService("Notice", "GetOfflineMemberAttendance", obj);
                var sessionDatesList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetLatestSessionDates", obj);

                return PartialView("_SessionDateList", sessionDatesList);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        //public ActionResult CreateSessionDates()
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserName))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }
        //        var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
        //        var SessionNames = (List<mSession>)Helper.ExecuteService("Session", "GetAllSessionNames", null);
        //        var model = new SessionDatesViewModel()
        //        {
        //            Sessions = new SelectList(SessionNames, "SessionCode", "SessionName"),
        //            Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
        //            Mode = "Add",
        //            IsActive = true,
        //            IsSitting = true
        //        };
        //        return View("CreateSessionDates", model);
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = "Their is a Error While Creating Session Dates Details";
        //        return View("AdminErrorPage");
        //        throw ex;
        //    }
        //}


        public ActionResult CreateSessionDates()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                

                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                var model = new SessionDatesViewModel()
                {
                    
                    AssemblyList = assmeblyList,
                    SessionList = sessionList,
                    Mode = "Add",
                    IsActive = true,
                    IsSitting = true
                };
                return View("CreateSessionDates", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Session Dates Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSessionDates(SessionDatesViewModel model)
        {
            try
            {

                if (model.Id == 0)
                {
                    model.Id = model.SessionDateId;
                }
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var SessionsDates = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {

                        Helper.ExecuteService("Session", "CreateSessionDates", SessionsDates);
                        TempData["Saved"] = "Data Saved successfully";
                    }
                    else
                    {

                        Helper.ExecuteService("Session", "UpdateSessionDates", SessionsDates);
                        TempData["Saved"] = "Data Updated successfully";
                    }
                    
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Session Dates Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        //public ActionResult EditSessionDates(int Id)
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserName))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }
        //        var SessionNames = (List<mSession>)Helper.ExecuteService("Session", "GetAllSessionNames", null);
        //        var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);

        //        mSessionDate sessionToEdit = (mSessionDate)Helper.ExecuteService("Session", "GetSessionDatesById", new mSessionDate { Id = Id });
        //        var model = sessionToEdit.ToViewModel1(SessionNames, Assemblies, "Edit");
        //        return View("CreateSessionDates", model);
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.ErrorMessage = "Their is a Error While Getting Session Dates Details";
        //        return View("AdminErrorPage");
        //        throw ex;
        //    }
        //}



        public ActionResult EditSessionDates(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                mSessionDate sessionToEdit = (mSessionDate)Helper.ExecuteService("Session", "GetSessionDatesById", new mSessionDate { Id = Convert.ToInt32(Id) });
                //mSessionDate sessionToEdit = (mSessionDate)Helper.ExecuteService("Session", "GetSessionDatesById", new mSessionDate { Id = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                
                var model = sessionToEdit.ToViewModel1("Edit");
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession sesdata = new mSession();
                sesdata.AssemblyID = model.AssemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sesdata);

                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;

                return View("CreateSessionDates", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Session Dates Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }


        public ActionResult DeleteSessionDates(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mSessionDate sessionToDelete = (mSessionDate)Helper.ExecuteService("Session", "GetSessionDatesById", new mSessionDate { Id = Id });
                Helper.ExecuteService("Session", "DeleteSessionDates", sessionToDelete);
                TempData["Saved"] = "Data Deleted successfully";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Session Dates Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult ChangeAssembly(int assemblyId)
        {
            try
            {
                mSession data = new mSession();
                data.AssemblyID = assemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                return Json(sessionList, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Getting All Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Change Assembly of Session Dates Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult ChangeSession(int sessionId, int assemblyId)
        {
            try
            {
                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionId;
                sesdate.AssemblyId = assemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                return Json(sessionDateList, JsonRequestBehavior.AllowGet);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                //RecordError(ex, "Change Session of Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Change Session of Sesion Dates Details";
                return View("AdminErrorPage");
            }
        }
    }
}
