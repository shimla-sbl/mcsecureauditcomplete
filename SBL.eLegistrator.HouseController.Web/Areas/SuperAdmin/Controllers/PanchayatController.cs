﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using SBL.DomainModel.Models.Constituency;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PanchayatController : Controller
    {
        //
        // GET: /SuperAdmin/Panchayat/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IndexOfPanchayat(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount, string ClickCount, string TempLoop)
        {
            try
            {
                mPanchayat model = new mPanchayat();
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //    var MemAssembly = (List<mMemberAssembly>)Helper.ExecuteService("Member", "GetAllMemberAssembly", null);
                //    var model = MemAssembly.ToViewModel();
                //    return View(model);
                if ((PageNumber == null || PageNumber == "") && (RowsPerPage == null || RowsPerPage == "") && (loopStart == null || loopStart == "") && (loopEnd == null || loopEnd == ""))
                {
                    PageNumber = (1).ToString();

                    RowsPerPage = (10).ToString();
                    loopStart = (1).ToString();
                    loopEnd = (5).ToString();
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                    //SearchText = SearchText;
                }
                else
                {
                    TempLoop = Sanitizer.GetSafeHtmlFragment(TempLoop);
                    PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                    RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                    loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                    loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    ClickCount = Sanitizer.GetSafeHtmlFragment(ClickCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                }
                //model.PAGE_SIZE = 25;
                model.PageIndex = int.Parse(PageNumber);
                //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblies", null);
                model = (mPanchayat)Helper.ExecuteService("Panchayat", "GetAllPanchayat", model);
                model.ResultCount = model.ResultCount;
                model.PageNumber = Convert.ToInt32(PageNumber);
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
                // model.RowsPerPage = (25);
                model.selectedPage = Convert.ToInt32(PageNumber);
                model.loopStart = Convert.ToInt32(loopStart);
                model.loopEnd = Convert.ToInt32(loopEnd);
                //var model = Assemblies.ToViewModel();
                if (TempLoop != "")
                {
                    model.loopStart = Convert.ToInt32(TempLoop);
                }
                else
                {
                    model.loopStart = Convert.ToInt32(loopStart);
                }
                if (ClickCount != "")
                {
                    model.ClickCount = Convert.ToInt32(ClickCount);
                }
                return PartialView("_PartialIndexOfPanchayat", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Panchayat Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult CreatePanchayat()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new PanchayatViewModel()
                {
                    Mode = "Add",
                    IsActive = true

                };
               

                //return PartialView("_PartialCreateAssembly", model);
                //return PartialView("_PartialCreatePanchayat", model);
                return View("CreatePanchayat", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Panchayat Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePanchayat(PanchayatViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Panchyat = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("Panchayat", "CreatePanchayat", Panchyat);
                    }
                    else
                        Helper.ExecuteService("Panchayat", "UpdatePanchayat", Panchyat);
                    return RedirectToAction("Index");

                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Panchayat Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult EditPanchayat(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //mPanchayat panchyatToEdit = (mPanchayat)Helper.ExecuteService("Panchayat", "GetPanchayatBasedOnId", new mPanchayat { PanchayatID = Id });
                mPanchayat panchyatToEdit = (mPanchayat)Helper.ExecuteService("Panchayat", "GetPanchayatBasedOnId", new mPanchayat { PanchayatID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var data = panchyatToEdit.ToViewModel1("Edit");
                return View("CreatePanchayat", data);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Panchayat Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeletePanchayat(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mPanchayat panchyatToDelete = (mPanchayat)Helper.ExecuteService("Panchayat", "GetPanchayatBasedOnId", new mPanchayat { PanchayatID = Id });
                Helper.ExecuteService("Panchayat", "DeletePanchayat", panchyatToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Panchayat Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

    }
}
