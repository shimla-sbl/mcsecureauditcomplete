﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MinistryController : Controller
    {
        //
        // GET: /SuperAdmin/Ministry/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.assmeblyList = assmeblyList;
                //var Ministry = (List<mMinistry>)Helper.ExecuteService("Ministry", "GetAllMinistryList", null);
                //var model = Ministry.ToViewModel();
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Ministry Details";
                return View("AdminErrorPage");
                throw ex;
            }

           
        }

        public ActionResult GetMinistryWithAssembly(int assemblyId)
        {
            try
            {
                 if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                 var Ministry = (List<mMinistry>)Helper.ExecuteService("Ministry", "GetMinistersListByAssemblyID", assemblyId);
                var model = Ministry.ToViewModel();
                return PartialView("_MinisterList",model);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public ActionResult CreateMinistry()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var model = new MinistryViewModel()
                {

                    Mode = "Add",
                    //MemberNames = new SelectList(Members, "MemberCode", "Name"),
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    IsActive = true


                };
                return View("CreateMinistry", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Ministry Details";
                return View("AdminErrorPage");
                throw ex;
            }
           

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMinistry(MinistryViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var ministry = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {

                        //ministry.IsActive = true;
                        Helper.ExecuteService("Ministry", "CreateMinistry", ministry);
                    }
                    else
                    {


                        //ministry.IsActive = true;
                        Helper.ExecuteService("Ministry", "UpdateMinistry", ministry);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Ministry Details";
                return View("AdminErrorPage");
                throw ex;
            }
         

        }

        public ActionResult EditMinistry(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                //mMinistry ministryToEdit = (mMinistry)Helper.ExecuteService("Ministry", "GetMinistryById", new mMinistry { MinistryID = Id });
                mMinistry ministryToEdit = (mMinistry)Helper.ExecuteService("Ministry", "GetMinistryById", new mMinistry { MinistryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = ministryToEdit.ToViewModel1(Assemblies, "Edit");
                return View("CreateMinistry", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Ministry Details";
                return View("AdminErrorPage");
                throw ex;
            }
            
        }

        public ActionResult DeleteMinistry(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMinistry ministryToDelete = (mMinistry)Helper.ExecuteService("Ministry", "GetMinistryById", new mMinistry { MinistryID = Id });
                Helper.ExecuteService("Ministry", "DeleteMinistry", ministryToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Ministry Details";
                return View("AdminErrorPage");
                throw ex;
            }
         

        }

        public JsonResult CheckMinistryChildExist(int MinistryID)
        {
            try
            {
                var isExist = (Boolean)Helper.ExecuteService("Ministry", "IsMinistryIdChildExist", new mMinistry { MinistryID = MinistryID });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
          
        }

    }
}
