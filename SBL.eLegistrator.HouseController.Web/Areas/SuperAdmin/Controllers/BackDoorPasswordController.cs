﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class BackDoorPasswordController : Controller
    {
        //
        // GET: /SuperAdmin/BackDoorPassword/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                var backDoorPwd = (List<mBackDoorPassword>)Helper.ExecuteService("Mobiles", "GetAllBackDoorPassword", null);
                var model = backDoorPwd.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting BackDoor Password Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateBackDoor()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var AppCodes = (List<mMobileApps>)Helper.ExecuteService("Mobiles", "GetAllAppCodesforBackDoor", null);
                var model = new BackDoorViewModel()
                {
                    Mode = "Add",
                    CodeList = new SelectList(AppCodes, "Code", "Code")

                };
                return View("CreateBackDoor", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating BackDoor Password Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SaveBackDoorPassword()
        {
            try
            {
                BackDoorViewModel model = new BackDoorViewModel();

                model.BackDoorPassword = Request.Form["BackDoorPassword"].ToString();

                model.Mode = Request.Form["Mode"].ToString();

                model.Code = Request.Form["Code"].ToString();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                var mobileApps = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateBackDoorPassword", mobileApps);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateBackDoorPassword", mobileApps);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditBackDoorPassword(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var AppCodes = (List<mMobileApps>)Helper.ExecuteService("Mobiles", "GetAllAppCodesforBackDoor", null);
                mBackDoorPassword pwdToEdit = (mBackDoorPassword)Helper.ExecuteService("Mobiles", "GetBackDoorPasswordBasedOnId", new mBackDoorPassword { SlNo = Id });
                var model = pwdToEdit.ToViewModel1(AppCodes,"Edit");
                return View("CreateBackDoor", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing BackDoor Password Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteBackDoorPassword(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mBackDoorPassword pwdToRemove = (mBackDoorPassword)Helper.ExecuteService("Mobiles", "GetBackDoorPasswordBasedOnId", new mBackDoorPassword { SlNo = Id });
                Helper.ExecuteService("Mobiles", "DeleteBackDoorPassword", pwdToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting BackDoor Password Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

    }
}
