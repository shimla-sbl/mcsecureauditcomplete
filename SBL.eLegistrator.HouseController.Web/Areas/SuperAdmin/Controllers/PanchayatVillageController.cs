﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.States;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Constituency;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class PanchayatVillageController : Controller
    {
        //
        // GET: /SuperAdmin/PanchayatVillage/

        
        public ActionResult Index(int pageId = 1,int pageSize = 50)
        {
            
            //int pageId = 1;
            //int pageSize = 10;
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var PanchayatVillage = (List<tPanchayatVillage>)Helper.ExecuteService("PanchayatVillage", "GetAllPanchayatVillageIDs", null);
                var PanchayatVillage = (List<tPanchayatVillage>)Helper.ExecuteService("PanchayatVillage", "GetAllPanchayatVillage", null);
                var model = PanchayatVillage.ToViewModel();
                ViewBag.PageSize = pageSize;
                List<PanchayatVillageViewModel> pagedRecord = new List<PanchayatVillageViewModel>();
                if (pageId == 1)
                {
                    pagedRecord = model.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = model.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(PanchayatVillage.Count, pageId, pageSize);
                return View(pagedRecord);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting PanchayatVillage Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }




        public PartialViewResult GetPanchayatVillageByPaging(int pageId, int pageSize)
        {
            var PanchayatVillage = (List<tPanchayatVillage>)Helper.ExecuteService("PanchayatVillage", "GetAllPanchayatVillage", null);
            ViewBag.PageSize = pageSize;
            List<tPanchayatVillage> pagedRecord = new List<tPanchayatVillage>();
            if (pageId == 1)
            {
                pagedRecord = PanchayatVillage.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = PanchayatVillage.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(PanchayatVillage.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
            return PartialView("/Areas/SuperAdmin/Views/PanchayatVillage/_Index.cshtml", model);
        }


        public PartialViewResult GetSearchList(string Panchayat, int pageId, int pageSize)
        {
            List<tPanchayatVillage> PanchayatVillage = new List<tPanchayatVillage>();

            int PanchayatCode = (int)Helper.ExecuteService("PanchayatVillage", "SearchPanchayat", Panchayat);
            if (PanchayatCode > 0)
            {

                PanchayatVillage = (List<tPanchayatVillage>)Helper.ExecuteService("PanchayatVillage", "GetAllPanchayatVillageSearch", PanchayatCode);
               
            }
            else
            {
                PanchayatVillage = (List<tPanchayatVillage>)Helper.ExecuteService("PanchayatVillage", "GetAllPanchayatVillage", null);
               
            }

            ViewBag.PageSize = pageSize;
            List<tPanchayatVillage> pagedRecord = new List<tPanchayatVillage>();

            if (PanchayatVillage.Count() < 50)
            {
                pagedRecord = PanchayatVillage.Take(pageSize).ToList();
            }
            else
            {
            if (pageId == 1)
            {
                pagedRecord = PanchayatVillage.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = PanchayatVillage.Skip(r).Take(pageSize).ToList();
            }
            }
            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(PanchayatVillage.Count, pageId, pageSize);
            var model = pagedRecord.ToViewModel();
            return PartialView("/Areas/SuperAdmin/Views/PanchayatVillage/_Indexx.cshtml", model);
        }

        

        public ActionResult CreatePanchayatVillage()
        {
            var States = (List<mStates>)Helper.ExecuteService("PanchayatVillage", "GetAllState", null);
            var District = (List<DistrictModel>)Helper.ExecuteService("PanchayatVillage", "GetAllDistrict", null);
            var Panchayat = (List<mPanchayat>)Helper.ExecuteService("PanchayatVillage", "GetAllPanchayat", null);
            var Village = (List<mVillage>)Helper.ExecuteService("PanchayatVillage", "GetAllVillage", null);

            var model = new PanchayatVillageViewModel()
            {
                Mode = "Add",
                State = new SelectList(States, "mStateID", "StateName"),
                District = new SelectList(District, "DistrictCode", "DistrictName"),
                Panchayat = new SelectList(Panchayat, "PanchayatCode", "PanchayatName"),
                Village = new SelectList(Village, "VillageCode", "VillageName"),
                IsActive = true
            };

            return View("CreatePanchayatVillage", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePanchayatVillage(PanchayatVillageViewModel model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("PanchayatVillage", "CreatePanchayatVillage", dist);
                }
                else
                {
                    Helper.ExecuteService("PanchayatVillage", "UpdatePanchayatVillage", dist);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditPanchayatVillage(string Id)
        {
            var States = (List<mStates>)Helper.ExecuteService("PanchayatVillage", "GetAllState", null);
            var District = (List<DistrictModel>)Helper.ExecuteService("PanchayatVillage", "GetAllDistrict", null);
            var Panchayat = (List<mPanchayat>)Helper.ExecuteService("PanchayatVillage", "GetAllPanchayat", null);
            var Village = (List<mVillage>)Helper.ExecuteService("PanchayatVillage", "GetAllVillage", null);
            //tPanchayatVillage stateToEdit = (tPanchayatVillage)Helper.ExecuteService("PanchayatVillage", "GetPanchayatVillageById", new tPanchayatVillage { PanchayatVillageID = Id });
            tPanchayatVillage stateToEdit = (tPanchayatVillage)Helper.ExecuteService("PanchayatVillage", "GetPanchayatVillageById", new tPanchayatVillage { PanchayatVillageID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1(States,District,Panchayat,Village, "Edit");
            return View("CreatePanchayatVillage", model);
        }

        public ActionResult DeletePanchayatVillage(int Id)
        {
            tPanchayatVillage stateToDelete = (tPanchayatVillage)Helper.ExecuteService("PanchayatVillage", "GetPanchayatVillageById", new tPanchayatVillage { PanchayatVillageID = Id });
            Helper.ExecuteService("PanchayatVillage", "DeletePanchayatVillage", stateToDelete);
            return RedirectToAction("Index");

        }

        

    }
}
