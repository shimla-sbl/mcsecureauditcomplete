﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SessionsController : Controller
    {
        //
        // GET: /SuperAdmin/Sessions/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Sessions = (List<mSession>)Helper.ExecuteService("Session", "GetAllSession", null);
                var model = Sessions.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Session Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateSession()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var SessionTypes = (List<mSessionType>)Helper.ExecuteService("Session", "GetAllSessionTypes", null);
                var model = new SessionsViewModel()
                {
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    SessionTypes = new SelectList(SessionTypes, "TypeID", "TypeName"),
                    Mode = "Add",
                    IsPublished = true
                };
                return View("CreateSession", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Session Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSessions(SessionsViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Sessions = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        //var sessioncode = (int)Helper.ExecuteService("Session", "GetSessionCodeById", null);
                        var sessioncode = (int)Helper.ExecuteService("Session", "GetSessionCodeById", model.AssemblyID);
                        if (sessioncode > 0)
                        {
                            int num = sessioncode;
                            Sessions.SessionCode = num + 1;
                        }
                        else
                        {
                            int count = 0;
                            Sessions.SessionCode = count + 1;
                        }
                        //Sessions.IsPublished = true;
                        Helper.ExecuteService("Session", "CreateSession", Sessions);
                    }
                    else
                    {

                        if (model.SessionID.HasValue)
                        {
                            var sessioncode = (int)Helper.ExecuteService("Session", "GetSessionCodeByIdforUpdate", Sessions.SessionID);
                            //var sessioncode = (List<mSession>)Helper.ExecuteService("Session", "GetAllSession", null);
                            if (sessioncode > 0)
                            {
                                int num = sessioncode;
                                Sessions.SessionCode = num;
                            }

                        }

                        //Sessions.IsPublished = true;
                        Helper.ExecuteService("Session", "UpdateSessionData", Sessions);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Session Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult EditSessions(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var SessionTypes = (List<mSessionType>)Helper.ExecuteService("Session", "GetAllSessionTypes", null);
                //mSession sessionToEdit = (mSession)Helper.ExecuteService("Session", "GetSessionById", new mSession { SessionID = Id });
               // mSession sessionToEdit = (mSession)Helper.ExecuteService("Session", "GetSessionById", new mSession { SessionID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                mSession sessionToEdit = (mSession)Helper.ExecuteService("Session", "GetSessionById", new mSession { SessionID = Convert.ToInt16(Id) });
                var model = sessionToEdit.ToViewModel1(Assemblies, SessionTypes, "Edit");
                return View("CreateSession", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Session Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult DeleteSessions(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mSession sessionToDelete = (mSession)Helper.ExecuteService("Session", "GetSessionById", new mSession { SessionID = Id });
                Helper.ExecuteService("Session", "DeleteSession", sessionToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Session Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public JsonResult CheckSessionChildExist(int SessionID)
        {
            try
            {
                var isExist = (Boolean)Helper.ExecuteService("Session", "IsSessionIdChildExist", new mSession { SessionID = SessionID });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
