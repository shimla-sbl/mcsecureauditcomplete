﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.CutMotionDemand;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using System.IO;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.DomainModel.Models.Enums;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class CutMotionDemandController : Controller
    {
        //
        // GET: /SuperAdmin/CutMotionDemand/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult GetAllCutMotionDemand()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                //FileAccessingUrlPath
                //var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                var tCutMotionDemand = (List<tCutMotionDemand>)Helper.ExecuteService("CutMotionDemand", "GetAllCutMotionDemand", null);

                var model = tCutMotionDemand.ToViewModel1();
                return View(model);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Getting All Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Getting the Cut Motion Demand Details";
                return View("AdminErrorPage");
            }

        }



        public ActionResult CreateCutMotionDemand()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                CutMotionDemandViewModel model = new CutMotionDemandViewModel();


                //Code for AssemblyList 
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession data = new mSession();
                data.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //Code for SessionDateList 

                //mSessionDate sesdate = new mSessionDate();
                //sesdate.SessionId = sessionList.FirstOrDefault().SessionCode;
                //sesdate.AssemblyId = data.AssemblyID;
                //model.VAssemblyId = data.AssemblyID;
                //model.VSessionId = sesdate.SessionId;
                //var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                //model.SessionDateList = sessionDateList;
                model.AssemblyList = assmeblyList;
                model.SessionList = sessionList;

                // model.MinisteryMinisterList = MinisteryMinisterslist;
                //new SelectList(MinisteryMinisterslist, "MinsitryMinistersID", "MinistryName");

                model.Mode = "Add";
                model.IsActive = true;
                model.IsDeleted = false;
                return View("CreateCutMotionDemand", model);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Creat Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Creating the Cut Motion Demand Details";
                return View("AdminErrorPage");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCutMotionDemand(CutMotionDemandViewModel model)
        {


            try
            {



                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                // var domainmodel = model.ToDomainModel();

                if (model.Mode == "Add")
                {

                    var data = model.ToDomainModel();

                    data.CreatedBy = CurrentSession.UserID;
                    data.ModifiedBy = CurrentSession.UserID;

                    Helper.ExecuteService("CutMotionDemand", "AddCutMotionDemand", data);

                }
                else
                {
                    var data = model.ToDomainModel();

                    data.ModifiedBy = CurrentSession.UserID;

                    Helper.ExecuteService("CutMotionDemand", "UpdateCutMotionDemand", data);

                }
                if (model.IsCreatNew)
                {
                    return RedirectToAction("CreateCutMotionDemand");
                }
                else
                {
                    return RedirectToAction("GetAllCutMotionDemand");
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Saving Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Saving the Cut Motion Demand Details";
                return View("AdminErrorPage");
            }


        }

        public ActionResult EditCutMotionDemand(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                tCutMotionDemand data = new tCutMotionDemand();
                //data.CMDemandId = Id;
                data.CMDemandId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
                var serData = (tCutMotionDemand)Helper.ExecuteService("CutMotionDemand", "GetCutMotionDemandById", data);

                var reldata = serData.ToViewModel1("Edit");
                var assmeblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                //Code for SessionList 
                mSession sesdata = new mSession();
                sesdata.AssemblyID = reldata.VAssemblyID;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", sesdata);

                reldata.AssemblyList = assmeblyList;
                reldata.SessionList = sessionList;

                return View("CreateCutMotionDemand", reldata);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Edit Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Editing Cut Motion Demand Details";
                return View("AdminErrorPage");
            }

        }

        public ActionResult DeleteCutMotionDemand(int Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                tCutMotionDemand data = new tCutMotionDemand();
                data.CMDemandId = Id;

                var isSucess = (bool)Helper.ExecuteService("CutMotionDemand", "DeleteCutMotionDemandById", data);

                return RedirectToAction("GetAllCutMotionDemand");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Deleting Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Deleting the Cut Motion Demand Details";
                return View("AdminErrorPage");
            }



        }





        public ActionResult ChangeAssembly(int assemblyId)
        {
            try
            {


                mSession data = new mSession();
                data.AssemblyID = assemblyId;
                var sessionList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                return Json(sessionList, JsonRequestBehavior.AllowGet);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Getting All Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Change Assembly of Cut Motion Demand Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult ChangeSession(int sessionId, int assemblyId)
        {

            try
            {

                mSessionDate sesdate = new mSessionDate();
                sesdate.SessionId = sessionId;
                sesdate.AssemblyId = assemblyId;
                var sessionDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDate", sesdate);

                return Json(sessionDateList, JsonRequestBehavior.AllowGet);

            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                //RecordError(ex, "Change Session of Assembly File");
                ViewBag.ErrorMessage = "There is a Error While Change Session of Cut Motion Demand Details";
                return View("AdminErrorPage");
            }
        }





    }
}
