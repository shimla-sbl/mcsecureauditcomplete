﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Constituency;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System.Collections.Generic;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.District;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using System;
using SBL.eLegistrator.HouseController.Web.Utility;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class ConstituencyController : Controller
    {
        //
        // GET: /SuperAdmin/Constituency/

        public ActionResult ShowAllDetails()
        {
            return View();
        }

        public ActionResult IndexOfConsti(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount, string ClickCount, string TempLoop)
        {
            try
            {
                mConstituency model = new mConstituency();
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //    var MemAssembly = (List<mMemberAssembly>)Helper.ExecuteService("Member", "GetAllMemberAssembly", null);
                //    var model = MemAssembly.ToViewModel();
                //    return View(model);
                if ((PageNumber == null || PageNumber == "") && (RowsPerPage == null || RowsPerPage == "") && (loopStart == null || loopStart == "") && (loopEnd == null || loopEnd == ""))
                {
                    PageNumber = (1).ToString();

                    RowsPerPage = (10).ToString();
                    loopStart = (1).ToString();
                    loopEnd = (5).ToString();
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                    //SearchText = SearchText;
                }
                else
                {
                    TempLoop = Sanitizer.GetSafeHtmlFragment(TempLoop);
                    PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                    RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                    loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                    loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    ClickCount = Sanitizer.GetSafeHtmlFragment(ClickCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                }
                //model.PAGE_SIZE = 25;
                model.PageIndex = int.Parse(PageNumber);
                //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblies", null);
                model = (mConstituency)Helper.ExecuteService("Constituency", "GetAllAssemblyIDs", model);
                model.ResultCount = model.ResultCount;
                model.PageNumber = Convert.ToInt32(PageNumber);
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
                // model.RowsPerPage = (25);
                model.selectedPage = Convert.ToInt32(PageNumber);
                model.loopStart = Convert.ToInt32(loopStart);
                model.loopEnd = Convert.ToInt32(loopEnd);
                //var model = Assemblies.ToViewModel();
                if (TempLoop != "")
                {
                    model.loopStart = Convert.ToInt32(TempLoop);
                }
                else
                {
                    model.loopStart = Convert.ToInt32(loopStart);
                }
                if (ClickCount != "")
                {
                    model.ClickCount = Convert.ToInt32(ClickCount);
                }
                return PartialView("_PartialIndexOfConstituency", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Constituency Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }


        //public ActionResult Index()
        //{
        //    try
        //    {
        //        if (string.IsNullOrEmpty(CurrentSession.UserName))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }
        //        var Constituency = (List<mConstituency>)Helper.ExecuteService("Constituency", "GetAllAssemblyIDs", null);
        //        // var Constituency = (List<mConstituency>)Helper.ExecuteService("Constituency", "GetAllConstituency", null);       

        //        //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Constituency", "GetAllAssembly", null);
        //        var model = Constituency.ToViewModel();
        //        //ConstituencyViewModel obj = new ConstituencyViewModel();
        //        //obj.GetAllConstituency = Constituency.ToViewModel();          
        //        //List<SelectListItem> items = new List<SelectListItem>();
        //        //foreach (var item in Assemblies)
        //        //{
        //        //    SelectListItem s = new SelectListItem();
        //        //    s.Text = item.AssemblyName.ToString();
        //        //    s.Value = item.AssemblyCode.ToString();
        //        //    items.Add(s);
        //        //}
        //        //ViewBag.AssemblyName = items; 
        //        //return View(obj);
        //        return View(model);

        //    }
        //    catch (Exception ex)
        //    {

        //        ViewBag.ErrorMessage = "Their is a Error While Getting Constituency Details";
        //        return View("AdminErrorPage");
        //        throw ex;
        //    }
           
        //}

        public ActionResult CreateConstituency()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var Districts = (List<DistrictModel>)Helper.ExecuteService("Constituency", "GetAllDistrict", null);
                var Category = (List<mConstituencyReservedCategory>)Helper.ExecuteService("Constituency", "GetAllConstituencyCategory", null);
                //List<DistrictModel> newlist = new List<DistrictModel>();
                //foreach (var val in Districts)
                //{
                //    string text = "("+val.DistrictCode + ")" + val.DistrictName;
                //    val.DistrictName = text;
                //    newlist.Add(val);
                //}
                var model = new ConstituencyViewModel()
                {
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    //District = new SelectList(newlist, "DistrictId", "DistrictName"),
                    District = new SelectList(Districts, "DistrictCode", "DistrictName"),
                    ReservedCategoryName = new SelectList(Category, "ConstituencyCategoryId", "ConstituencyCategoryCode"), 
                    Mode = "Add",
                    Active = true

                };

                // var dad = (from a in Assemblies select a).LastOrDefault();
                return View("CreateConstituency", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Creating Constituency Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }


        public ActionResult CopyAssemblyData()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);

                var model = new ConstituencyCopyViewModel()
                {
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),




                };

                // var dad = (from a in Assemblies select a).LastOrDefault();
                return View("CopyAssemblyData", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Copying Previous Assembly Data.";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCopyAssemblyData(ConstituencyCopyViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var constituency = model.ToDomainnModel();

                    Helper.ExecuteService("Constituency", "CopyAssemblyData", constituency);


                    return RedirectToAction("ShowAllDetails");
                }
                else
                {
                    return RedirectToAction("ShowAllDetails");
                }

            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Previous Assembly Data.";
                return View("AdminErrorPage");
                throw ex;
            }

        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveConstituency(ConstituencyViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var constituency = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        //if (model.AssemblyID.HasValue)
                        //{
                        //    var consticode = (int)Helper.ExecuteService("Constituency", "GetConstituencyCodeById", constituency.AssemblyID);
                        //    if (consticode > 0)
                        //    {
                        //        int num = consticode;
                        //        constituency.ConstituencyCode = num + 1;
                        //    }
                        //    else
                        //    {
                        //        int count = 0;
                        //        constituency.ConstituencyCode = count + 1;
                        //    }

                        //}

                        
                        Helper.ExecuteService("Constituency", "CreateConstituency", constituency);
                    }
                    else
                    {
                        //if (model.ConstituencyID.HasValue)
                        //{


                        //    var consticode1 = (int)Helper.ExecuteService("Constituency", "GetSessionCodeByIdforUpdate", constituency.ConstituencyID);
                        //    if (consticode1 > 0)
                        //    {
                        //        //var data = consticode1;
                        //        int num = consticode1;
                        //        constituency.ConstituencyCode = num;
                        //    }


                        //}

                        
                        Helper.ExecuteService("Constituency", "UpdateConstituency", constituency);
                    }
                    return RedirectToAction("ShowAllDetails");
                }
                else
                {
                    return RedirectToAction("ShowAllDetails");
                }

            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Constituency Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }

        public ActionResult EditConstituency(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var Districts = (List<DistrictModel>)Helper.ExecuteService("Constituency", "GetAllDistrict", null);
                var Category = (List<mConstituencyReservedCategory>)Helper.ExecuteService("Constituency", "GetAllConstituencyCategory", null);
                //mConstituency constituencyToEdit = (mConstituency)Helper.ExecuteService("Constituency", "GetConstituencyById", new mConstituency { ConstituencyID = Id });
                mConstituency constituencyToEdit = (mConstituency)Helper.ExecuteService("Constituency", "GetConstituencyById", new mConstituency { ConstituencyID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = constituencyToEdit.ToViewModel1(Assemblies, Districts, Category, "Edit");
                return View("CreateConstituency", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Constituency Details";
                return View("AdminErrorPage");
                throw ex;
            }
          
        }

        public ActionResult DeleteConstituency(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mConstituency constituencyToDelete = (mConstituency)Helper.ExecuteService("Constituency", "GetConstituencyById", new mConstituency { ConstituencyID = Id });
                Helper.ExecuteService("Constituency", "DeleteConstituency", constituencyToDelete);
                return RedirectToAction("ShowAllDetails");

            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Deleting Constituency Details";
                return View("AdminErrorPage");
                throw ex;
            }
           

        }

        public PartialViewResult GetAssemblyDetails(int Id)
        {
            try
            {
                
                var Constituency = (List<mConstituency>)Helper.ExecuteService("Constituency", "GetAssemblyDataBasedOnId", Id);
                var model = Constituency.ToViewModel();
                return PartialView("_GetAssemblyDetails", model);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           
        }

        public JsonResult CheckConstiChildExist(int ConstituencyID)
        {
            try
            {
                var isExist = (Boolean)Helper.ExecuteService("Constituency", "IsConstituencyIdChildExist", new mConstituency { ConstituencyID = ConstituencyID });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }

        [HttpPost]
        public JsonResult CheckId(int ConstituencyCode, int AssemblyId)
        {

            mConstituency obj = new mConstituency();


            obj.ConstituencyCode = ConstituencyCode;
            obj.AssemblyID = AssemblyId;
            string st = Helper.ExecuteService("Constituency", "CheckId", obj) as string;

            return Json(st, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult CheckIdd(int ConstituencyCode)
        {

            mConstituency obj = new mConstituency();


            obj.AssemblyID = ConstituencyCode;

            string st = Helper.ExecuteService("Constituency", "CheckIdd", obj) as string;

            return Json(st, JsonRequestBehavior.AllowGet);
        }


    }
}
