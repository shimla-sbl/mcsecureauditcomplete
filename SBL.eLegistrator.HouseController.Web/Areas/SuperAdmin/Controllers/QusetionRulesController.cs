﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Question;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class QusetionRulesController : Controller
    {
        //
        // GET: /SuperAdmin/QusetionRules/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetQuestionRules()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
            var QuestionRuleslist = (List<mQuestionRules>)Helper.ExecuteService("Questions", "GetmQuestionRules", null);
            var Getlist = QuestionRuleslist.QuestionRulesViewModel();
            return View(Getlist);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Question Rule Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }
        public ActionResult CreateQuestionsRule()
        {


            var model = new QuestionRulesViewModel()
            {
                Mode = "Add",              
                Remarks=true

            };
            return View(model);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveQuestionRule(QuestionRulesViewModel model)
        {
            if (ModelState.IsValid)
            {
                var Res = model.QuestionRulesToDomainModel();


                if (model.Mode == "Add")
                {
                    var CommiteeList = Helper.ExecuteService("Questions", "SaveQuestionRule", Res);
                }
                else
                {
                    Helper.ExecuteService("Questions", "UpdateQuestionRule", Res);
                }
            }
            else
            {

                return RedirectToAction("CreateQuestionsRule");
            }



            return RedirectToAction("GetQuestionRules");
        }
        public ActionResult EditQuestionRule(string Id)
        {


            //mQuestionRules QuestiontoEdit = (mQuestionRules)Helper.ExecuteService("Questions", "EditQuestionRules", Id);
            mQuestionRules QuestiontoEdit = (mQuestionRules)Helper.ExecuteService("Questions", "EditQuestionRules", Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));

            var model = QuestiontoEdit.SingleQuestionRules();
            model.Mode = "Edit";

            return View("CreateQuestionsRule", model);

        }
        public ActionResult DeleteQuestionRule(int Id)
        {
            //mNoticeType CommitteeTypeToDelete = (mNoticeType)Helper.ExecuteService("Notice", "GetCommitteById", Id);
            Helper.ExecuteService("Questions", "DeleteQuestionRule", Id);
            return RedirectToAction("GetQuestionRules");
        }

    }
}

