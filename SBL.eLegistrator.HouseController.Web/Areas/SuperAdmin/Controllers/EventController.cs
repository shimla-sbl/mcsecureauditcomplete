﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.Event;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class EventController : Controller
    {
        //
        // GET: /SuperAdmin/Event/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Events = (List<mEvent>)Helper.ExecuteService("Events", "GetAllEventsList", null);
                var model = Events.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Getting Event Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }

        public ActionResult CreateEvent()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var PaperTypes = (List<mPaperCategoryType>)Helper.ExecuteService("Events", "GetAllPaperCatTypes", null);
                var model = new EventViewModel()
                {
                    PaperCatTypes = new SelectList(PaperTypes, "PaperCategoryTypeId", "Name"),
                    Mode = "Add",
                    Active = true
                };
                return View("CreateEvent", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Event Details";
                return View("AdminErrorPage");
                throw ex;
            }
          
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEvent(EventViewModel model )
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Events = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("Events", "CreateEvents", Events);

                    }
                    else
                    {
                        Helper.ExecuteService("Events", "UpdateEvents", Events);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Event Details";
                return View("AdminErrorPage");
                throw ex;
            }
          
        }

        public ActionResult EditEvent(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var PaperTypes = (List<mPaperCategoryType>)Helper.ExecuteService("Events", "GetAllPaperCatTypes", null);
                //mEvent EventToEdit = (mEvent)Helper.ExecuteService("Events", "GetEventBasedOnId", new mEvent { EventId = Id });
                mEvent EventToEdit = (mEvent)Helper.ExecuteService("Events", "GetEventBasedOnId", new mEvent { EventId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                
                var model = EventToEdit.ToViewModel1(PaperTypes, "Edit");
                return View("CreateEvent", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Event Details";
                return View("AdminErrorPage");
                throw ex;
            }
           

        }

        public ActionResult DeleteEvent(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mEvent eventToDelete = (mEvent)Helper.ExecuteService("Events", "GetEventBasedOnId", new mEvent { EventId = Id });
                Helper.ExecuteService("Events", "DeleteEvents", eventToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Event Details";
                return View("AdminErrorPage");
                throw ex;
            }
          


        }

    }
}
