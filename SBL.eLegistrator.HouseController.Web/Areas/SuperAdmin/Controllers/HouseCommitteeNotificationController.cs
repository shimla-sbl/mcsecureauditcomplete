﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using Microsoft.Security.Application;
using System.IO;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class HouseCommitteeNotificationController : Controller
    {
        //
        // GET: /SuperAdmin/HouseCommitteeNotification/

        public ActionResult Index()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                var hcn = (List<HouseCommitteeNotification>)Helper.ExecuteService("Mobiles", "GetAllHCN", null);
                var model = hcn.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting House Committee Notification Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateHouseCommitteeNotification()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var model = new HouseCommitteeNotificationViewModel()
                {
                    Mode = "Add"
                };

                return View("CreateHouseCommitteeNotification", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Create House Committee Notification Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost, ValidateInput(false)]
        public JsonResult SaveHCN(HttpPostedFileBase file)
        {
            try
            {
                HouseCommitteeNotificationViewModel model = new HouseCommitteeNotificationViewModel();

                model.Title = Request.Form["Title"].ToString();

                model.HCNDate = Request.Form["HCNDate"].ToString();

                model.Mode = Request.Form["Mode"].ToString();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                model.FilePath = Request.Form["FilePath"].ToString();

                string pdfPath = string.Empty;

                if (file != null)
                {
                    UploadFile(file, file.FileName, out pdfPath);

                    model.FilePath = pdfPath.Replace(@"\", @"/");
                }

                var committeNotification = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateHCN", committeNotification);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateHCN", committeNotification);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }


        }

        public ActionResult EditHCN(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                HouseCommitteeNotification houseCommitteeNotification = (HouseCommitteeNotification)Helper.ExecuteService("Mobiles", "GetHCNBasedOnId", new HouseCommitteeNotification { SlNo = Id });
                var model = houseCommitteeNotification.ToViewModel1("Edit");
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSettingLocal", null);
                model.DownloadPath = FileSettings.SettingValue + @"/" + houseCommitteeNotification.FilePath;
                return View("CreateHouseCommitteeNotification", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Create House Committee Notification Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }


        public ActionResult DeleteHCN(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                HouseCommitteeNotification housecommitteeNotification = (HouseCommitteeNotification)Helper.ExecuteService("Mobiles", "GetHCNBasedOnId", new HouseCommitteeNotification { SlNo = Id });

                Helper.ExecuteService("Mobiles", "DeleteHCN", housecommitteeNotification);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Committee Notification Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }


        private void UploadFile(HttpPostedFileBase File, string FileName, out string dbFileName)
        {
            try
            {
                string savedFileName = string.Empty;

                dbFileName = string.Empty;

                if (File != null)
                {
                    var fileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string dirPath = string.Empty;

                    string fileNameWithoutExt = Path.GetFileNameWithoutExtension(File.FileName).Replace(".", "_").Replace("-", "_");

                    string fileName = GetTimestamp(DateTime.Now) + "_" + fileNameWithoutExt + Path.GetExtension(File.FileName);

                        dirPath = Path.Combine(fileSettings.SettingValue + "CommitteeNotifications");

                        savedFileName = fileName;

                        dbFileName = "CommitteeNotifications" + @"\" + fileName;
                
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }

                    dirPath = dirPath + @"\" + savedFileName;

                    File.SaveAs(dirPath);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public ActionResult Download(string filePathWithName)
        {
            filePathWithName = Sanitizer.GetSafeHtmlFragment(filePathWithName);
            string filename = string.Empty;
            filename = @"C:\inetpub\e_Vidhan\FileStructure\CommitteeNotifications\" + System.IO.Path.GetFileName(filePathWithName);
            var fs = System.IO.File.OpenRead(filename);
            return File(fs, "appliaction/zip", filePathWithName.Substring(filePathWithName.LastIndexOf("/") + 1));
        }

    }
}
