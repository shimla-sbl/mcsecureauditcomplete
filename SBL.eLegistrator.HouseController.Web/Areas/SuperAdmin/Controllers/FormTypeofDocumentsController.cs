﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Forms;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class FormTypeofDocumentsController : Controller
    {
        //
        // GET: /SuperAdmin/FormTypeofDocuments/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var doc = (List<tFormsTypeofDocuments>)Helper.ExecuteService("FormDoc", "GetAllDoc", null);
                var model = doc.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting FormsTypeofDocuments Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateDoc()
        {
            var model = new FormTypeofDocumentsViewModel()
            {
                Mode = "Add",
                IsActive = true

            };

            return View("CreateDoc", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDoc(FormTypeofDocumentsViewModel model)
        {

            if (ModelState.IsValid)
            {
                var doc = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("FormDoc", "CreateDoc", doc);
                }
                else
                {
                    Helper.ExecuteService("FormDoc", "UpdateDoc", doc);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditDoc(string Id)
        {

            //tFormsTypeofDocuments docToEdit = (tFormsTypeofDocuments)Helper.ExecuteService("FormDoc", "GetDocById", new tFormsTypeofDocuments { TypeofFormId = Id });
            tFormsTypeofDocuments docToEdit = (tFormsTypeofDocuments)Helper.ExecuteService("FormDoc", "GetDocById", new tFormsTypeofDocuments { TypeofFormId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = docToEdit.ToViewModel1("Edit");
            return View("CreateDoc", model);
        }

        public ActionResult DeleteDoc(int Id)
        {
            tFormsTypeofDocuments docToDelete = (tFormsTypeofDocuments)Helper.ExecuteService("FormDoc", "GetDocById", new tFormsTypeofDocuments { TypeofFormId = Id });
            Helper.ExecuteService("FormDoc", "DeleteDoc", docToDelete);
            return RedirectToAction("Index");

        }

        


    }
}
