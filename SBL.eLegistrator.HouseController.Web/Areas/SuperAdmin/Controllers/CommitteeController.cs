﻿using SBL.DomainModel.Models.Committee;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System.Collections.Generic;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
//using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text;
using EvoPdf;
using System.Text;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.User;
using System.Text.RegularExpressions;


namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
	[Audit]
	[SBLAuthorize(Allow = "Authenticated")]
	[NoCache]
	public class CommitteeController : Controller
	{
		//
		// GET: /SuperAdmin/Committee/
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult GetAllCommitee()
		{
			try
			{
				if (string.IsNullOrEmpty(CurrentSession.UserName))
				{
					return RedirectToAction("LoginUP", "Account", new { @area = "" });
				}
				var CommiteeList = (List<mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeType", null);
				var Getlist = CommiteeList.CommiteeTypeViewModel();
				return View(Getlist);
				//return PartialView("_PartialIndexOfCommittee", Getlist);
			}
			catch (Exception ex)
			{


				ViewBag.ErrorMessage = "There is a Error While Getting Committee Details";
				return View("AdminErrorPage");
				throw ex;
			}
		}

		public ActionResult CreateCommiteeType()
		{
			var model = new CommiteeTypeViewModel()
			{
				Mode = "Add",
				IsSubCommittee = true

			};
			return View("CreateCommiteeType", model);
			// return PartialView("_PartialCreateCommittee", model);
		}

		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult SaveCommiteeType(CommiteeTypeViewModel model)
		{
			if (ModelState.IsValid)
			{
				var Res = model.CommiteeToDomainModel();
				if (model.Mode == "Add")
				{
					Helper.ExecuteService("Committee", "CreateCommiteeType", Res);
				}
				else
				{
					Helper.ExecuteService("Committee", "UpdateCommittee", Res);
				}
				return RedirectToAction("GetAllCommitee");
			}
			else
			{
				return RedirectToAction("GetAllCommitee");
			}

		}

		/// <summary>
		/// Committee will come under committee type.
		/// </summary>
		/// <param name="Id">Id</param>
		/// <returns></returns>
		[HttpGet]
		public ActionResult EditSubCommittee(int Id)
		{
			SBL.DomainModel.Models.Assembly.mAssembly Amodel = new DomainModel.Models.Assembly.mAssembly();
			tCommittee committeeToEdit = (tCommittee)Helper.ExecuteService("Committee", "GetSubCommitteById", Id);
			var AssemblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyData", Amodel);
			var CommitteTypeList = (List<SBL.DomainModel.Models.Committee.mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeType", null);
			var CommitteeList = (List<SBL.DomainModel.Models.Committee.tCommittee>)Helper.ExecuteService("Committee", "GetCommittee", committeeToEdit);
			SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.CommitteeViewModel model = new CommitteeViewModel();
			model.Assembly = new SelectList(AssemblyList, "AssemblyID", "AssemblyName", null);
			model.CommitteeType = new SelectList(CommitteTypeList, "CommitteeTypeId", "CommitteeTypeName", null);
			model.ParentCommittee = new SelectList(CommitteeList, "CommitteeId", "CommitteeName", null);
			model.CommitteeName = committeeToEdit.CommitteeName;
			//model.DateofCreation = committeeToEdit.DateOfCreation;
			model.Remark = committeeToEdit.Remark;
			model.IsActive = committeeToEdit.IsActive;
			model.AssemblyID = committeeToEdit.AssemblyID;
			model.CommitteeTypeId = committeeToEdit.CommitteeTypeId;
			model.ParentId = committeeToEdit.ParentId;
            model.Abbreviation = committeeToEdit.Abbreviation;
			//DateTime dt = Convert.ToDateTime(committeeToEdit.DateOfCreation);
			//DateTime strDate = Convert.ToDateTime(dt.ToString("dd/MM/yyyy")).Date;
			model.DateofCreation = committeeToEdit.DateOfCreation;
			model.CommitteeId = committeeToEdit.CommitteeId;
			ViewBag.CommitteeYear = GetCommitteeYear();
			model.Mode = "Edit";
			return View("CreateCommittee", model);
		}

		/// <summary>
		/// update sub committee 
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult EditSubCommittee(CommitteeViewModel model)
		{
			tCommittee committeeModel = model.ToDomainModel();
			Helper.ExecuteService("Committee", "UpdateSubCommittee", committeeModel);
			return Json(true, JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// Delete Committee
		/// </summary>
		/// <param name="CommitteeId">CommitteeId</param>
		/// <returns></returns>
		//[HttpDelete]
		public ActionResult DeleteSubCommittee(int Id)
		{
			bool count = (bool)Helper.ExecuteService("Committee", "DeleteSubCommittee", Id);
			return RedirectToAction("GetAllNewComittee");
		}

		public ActionResult EditCommiteeType(int Id)
		{
			mCommitteeType committeeToEdit = (mCommitteeType)Helper.ExecuteService("Committee", "GetCommitteById", Id);
			var model = committeeToEdit.SingleCommiteeType("Edit");
			return View("CreateCommiteeType", model);
			//return PartialView("_PartialUpdateCommittee", model);
		}

		public ActionResult DeleteCommitteeType(int Id)
		{
			mCommitteeType CommitteeTypeToDelete = (mCommitteeType)Helper.ExecuteService("Committee", "GetCommitteById", Id);
			Helper.ExecuteService("Committee", "DeleteCommitteeType", CommitteeTypeToDelete);
			return RedirectToAction("GetAllCommitee");
		}

		/// <summary> 
		/// Get all committee List
		/// </summary>
		/// <returns></returns>
		public ActionResult GetAllNewComittee()
		{
			CommitteeList comList = new CommitteeList();
			List<tCommitteeMemberList> committeMember = new List<tCommitteeMemberList>();
			try
			{
				comList.committeeMemberList = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "GetAllCommitteeMembers", null);
				comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommittee", null);
				int mode = (int)Helper.ExecuteService("Committee", "CommitteeExisit", null);
				ViewBag.mode = mode;
				//var list=committeMember
			}
			catch (Exception ex)
			{
				ViewBag.ErrorMessage = "There is a Error While Getting Committee Details";
				return View("AdminErrorPage");
				throw ex;
			}

			return View(comList);
		}


		/// <summary>
		/// Get all eFiles      
		/// </summary>
		/// <returns></returns>
		public ActionResult eFileList()
		{
			return View();
		}

		/// <summary>
		/// Get all committee members
		/// </summary>
		/// <returns></returns>
		public JsonResult GetAllMembers()
		{
			var Members = (List<SBL.DomainModel.Models.Member.mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
			return Json(Members, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ShowCommitteMemberByCommitteeId(string value)
		{
			var Members = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "ShowCommitteMemberByCommitteeId", value);
			return Json(Members, JsonRequestBehavior.AllowGet);
		}

		public ActionResult CreateCommittee()
		{
			SBL.DomainModel.Models.Assembly.mAssembly Amodel = new DomainModel.Models.Assembly.mAssembly();
			var AssemblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyData", Amodel);
			var CommitteTypeList = (List<SBL.DomainModel.Models.Committee.mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeType", null);
			var CommitteeList = (List<SBL.DomainModel.Models.Committee.CommitteeSearchModel>)Helper.ExecuteService("Committee", "GetCommittees", 2);
			SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models.CommitteeViewModel model = new CommitteeViewModel();
			model.Assembly = new SelectList(AssemblyList, "AssemblyID", "AssemblyName", null);
			model.CommitteeType = new SelectList(CommitteTypeList, "CommitteeTypeId", "CommitteeTypeName", null);
			model.ParentCommittee = new SelectList(CommitteeList, "CommitteeId", "CommitteeName", null);
			ViewBag.CommitteeYear = GetCommitteeYear();
			return View(model);
		}

		[HttpPost]
		public ActionResult CreateCommittee(CommitteeViewModel model, string Depddlmm)
		{

			tCommittee committeeModel = model.ToDomainModel();
			committeeModel.CommitteeYear = Depddlmm;
            committeeModel.Mode = model.Mode;
			Helper.ExecuteService("Committee", "CreateCommittee", committeeModel);
            return RedirectToAction("GetAllNewComittee");
		}

		public JsonResult CreateCommitteMembers(string values, string ChairmanId, string Mode, string CommitteeId)
		{
			string[] words = values.Split(',');
			tCommitteeMember model = new tCommitteeMember();
			for (int count = 0; count < words.Length; count++)
			{
				var models = new tCommitteeMember
				{
					MemberId = Convert.ToInt32(words[count]),
					CommitteeId = Convert.ToInt32(CommitteeId),
					//IsChairMan = false,
					RelievedDate = DateTime.Now,
					JoinedDate = DateTime.Now
				};

				if (ChairmanId == models.MemberId.ToString())
				{
					models.IsReportChairMan = true;
				}
				else
				{
					models.IsReportChairMan = false;
				}
				Helper.ExecuteService("Committee", "AddCommitteeMembers", models);
				//if (Mode == "0")
				//{
				//    Helper.ExecuteService("Committee", "AddCommitteeMembers", models);
				//}

				//else
				//{

				//    Helper.ExecuteService("Committee", "UpdateCommitteeMembers", models);
				//}
			}
			return Json("", JsonRequestBehavior.AllowGet);
		}

		/// <summary>
		/// GetCommitteeMembersAsPDF
		/// </summary>
		/// <returns></returns>
		public ActionResult GetCommitteeMembersAsPDF()
		{
			List<tCommitteeMemberList> membersList = new List<tCommitteeMemberList>();
			membersList = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "GetCommitteeForPDF", null);

			string outXml = "";
			string path = "";

			string fileName = "";

			MemoryStream output = new MemoryStream();
			EvoPdf.Document document1 = new EvoPdf.Document();
			document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
			document1.CompressionLevel = PdfCompressionLevel.Best;
			document1.Margins = new Margins(0, 0, 0, 0);



			EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
			PdfPageOrientation.Landscape);
			string tablestart = "";
			tablestart += @"<table style='width:100%; text-align: center;'>
                            <tr><td>
                            (हिमाचल प्रदेश सरकार के असाधारण राजपत्र में प्रकाशि त किया  जाएगा ।)
                            </td></tr>
                            <tr><td style='font-weight: bold;font-size: 150%;'>
                            हिमाचल प्रदेश बारहवीं विधान स भा
                            </td></tr>
                            <br/>
                            <br/>
                            <br/>
                            <table style='width:70%;padding-left:420px;'>
                            ";

			string members = "";
			StringBuilder builder = new StringBuilder();
			for (int count = 0; count < membersList.Count; count++)
			{
				if (membersList[count].IsChairMan)
				{
					members += "<tr><td colspan=2 style='text-align: center;font-weight: bold;'>" + membersList[count].CommitteeNameLocal + "</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr>" +
								"<tr><td style='font-weight: bold;'>" + membersList[count].CommitteMemberName + "</td><td>सभापति</td></tr>";
				}
				else
				{
					members += @"<tr><td>" + membersList[count].CommitteMemberName + "</td><td>सदस्य</td></tr><tr><td></td><td></td></tr>";
				}
			}
			//string outXml = "<html><body style='font-family:DVOT-Yogesh;margin-right:125px;margin-left: 125px;background-color:#CEF6F5;'> <div><div style='width: 100%;background-color:#CEF6F5;'><table style='width: 100%;background-color:#CEF6F5;'>";
			outXml = "<html><body>";

			outXml += tablestart + members + "</table></table></body></html>";
			string htmlStringToConvert = outXml;

			HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");
			AddElementResult addResult = page.AddElement(htmlToPdfElement);

			byte[] pdfBytes = document1.Save();

			output.Write(pdfBytes, 0, pdfBytes.Length);

			output.Position = 0;

			if (Directory.Exists(@"C:\eVidhanData"))
			{
				DirectoryInfo di = new DirectoryInfo(@"C:\eVidhanData");
				di.Delete(true);

			}
			Directory.CreateDirectory(@"C:\eVidhanData");
			fileName = "ComMembers.pdf";

			string url = @"C:\eVidhanData\";
			path = Path.Combine(url, fileName);

			FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
				  System.IO.FileAccess.Write);

			_FileStream.Write(pdfBytes, 0, pdfBytes.Length);

			// close file stream
			_FileStream.Close();

			document1.Close();

			return RedirectToAction("GetAllNewComittee");
		}


		public ActionResult SendNoticeToDepartment()
		{
			eFileAttachment documents = new eFileAttachment();
			var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Department", "GetDepartment", null);
			ViewBag.ListOfCodes = new SelectList(deptList, "deptId", "deptname", null);
			//var x=deptList.ToList().Select(c=>new SelectListItem){})
			return View(documents);
		}


		public ActionResult ShowDepartmentNotices()
		{
			SBL.DomainModel.Models.eFile.eFileAttachments eFileAttach = new eFileAttachments();
			List<eFileAttachment> attachments = new List<eFileAttachment>();
			eFileAttach.eFileAttachemts = GetDepartmentNotices();
			//var deptList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("eFile", "GetCommitteeeFile", null);
			return View(eFileAttach);
		}

		public ActionResult SendNoticeToDepartmentList()
		{
			SBL.DomainModel.Models.eFile.eFileAttachments eFileAttach = new eFileAttachments();
			List<eFileAttachment> attachments = new List<eFileAttachment>();
			eFileAttach.eFileAttachemts = GetDepartmentNotices();
			eFileAttach.departmentReplys = GetDepartmentReply();

			return View(eFileAttach);
		}

		public List<eFileAttachment> GetDepartmentNotices()
		{
			List<eFileAttachment> attachments = new List<eFileAttachment>();
			attachments = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GeteFileAttachmentonDepartment", null);
			return attachments;
		}

		public List<DepartmentReply> GetDepartmentReply()
		{
			List<DepartmentReply> DeptReply = new List<DepartmentReply>();
			DeptReply = (List<DepartmentReply>)Helper.ExecuteService("eFile", "GeteFileDepartmentReply", null);
			return DeptReply;
		}

		public ActionResult GetCommitteeReply(int ParentId)
		{
			List<DepartmentReply> DeptReply = new List<DepartmentReply>();
			DeptReply = (List<DepartmentReply>)Helper.ExecuteService("Committee", "GetCommitteeReply", ParentId);
			return View("DepartmentReplys", DeptReply);
		}

		public ActionResult PDFView(int eFileAttachID)
		{
			List<DepartmentReply> Attachment = new List<DepartmentReply>();
			Attachment = (List<DepartmentReply>)Helper.ExecuteService("eFile", "GeteFileAttachment", eFileAttachID);
			//return Json(DeptReply, JsonRequestBehavior.AllowGet);
			return View("_PopupView", Attachment);
		}

		public ActionResult GetCommitteeeFiles(string CommitteeId)
		{
			List<SBL.DomainModel.Models.eFile.eFile> eFile = (List<SBL.DomainModel.Models.eFile.eFile>)Helper.ExecuteService("eFile", "GetCommitteeeFile", CommitteeId);
			return Json(eFile, JsonRequestBehavior.AllowGet);
		}


		////////////////////////////////////////////////////////////////Added on 26-2-15

		public ActionResult GetPreviousMembers(CommitteeViewModel model)
		{

			return View(model);

		}
		//Shashi 2 May

		public static SelectList GetCommitteeYear()
		{
			var result = new List<SelectListItem>();

			result.Add(new SelectListItem() { Text = "Select Committee Year", Value = "0" });
			for (int i = 2000; i <= 2050; i++)
			{
				result.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
			}
			return new SelectList(result, "Value", "Text");
		}
		public ActionResult GetComitteeListT()
		{
			CommitteeList comList = new CommitteeList();
			List<tCommitteeMemberList> committeMember = new List<tCommitteeMemberList>();
			try
			{

				comList.committeeMemberList = (List<tCommitteeMemberList>)Helper.ExecuteService("Committee", "GetAllCommitteeMembers", null);
				comList.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommitteeByMemberPermission", CurrentSession.UserID);
				int mode = (int)Helper.ExecuteService("Committee", "CommitteeExisit", null);
				ViewBag.mode = mode;

			}
			catch (Exception ex)
			{
				ViewBag.ErrorMessage = "There is a Error While Getting Committee Details";
				return View("AdminErrorPage");
				throw ex;
			}

			return View("GetComitteeListT", comList);
		}

		//added by robin for committee type permisiion
		public ActionResult CommitteeTypeMasterPage()
		{
			try
			{
				CommitteeTypePermission model = new CommitteeTypePermission();
				model.mUsers = (List<mUsers>)Helper.ExecuteService("Committee", "GetAllUsers", null);
				model.mCommitteeType = (List<mCommitteeType>)Helper.ExecuteService("Committee", "GetAllCommiteeTypeForPermission", null);
				model.CommitteeView = (List<CommitteesListView>)Helper.ExecuteService("Committee", "GetAllCommittee", null);
				return View(model);
			}
			catch (Exception)
			{
				throw;
			}

		}

		[HttpPost, ValidateAntiForgeryToken]
		public ActionResult NewCommitteType(CommitteeTypePermission model)
		{
			try
			{
				if (ModelState.IsValid)
				{
					var check1 = Helper.ExecuteService("Committee", "CheckNewCommitteType1", model);//for checking if data already exists
					var check2 = Helper.ExecuteService("Committee", "CheckNewCommitteType2", model);//for checking if data already exists
					if (check1 != null)
					{
						return Content("dataexists");
					}
					if (check2 != null)
					{
						Helper.ExecuteService("Committee", "SaveNewCommitteTypeIsActive", model);
						return Content("saved");
					}
					if (check1 == null && check2 == null)
					{
						Helper.ExecuteService("Committee", "SaveNewCommitteType", model);
						return Content("saved");
					}
					//if (check2 == null)
					//{
					//    Helper.ExecuteService("Committee", "SaveNewCommitteTypeIsActive", model);
					//    return Content("saved");
					//}
					return Content("modelerror");
				}
				else
				{
					return Content("modelerror");
				}
			}
			catch (Exception)
			{
				return Content("error");
			}

		}

		public ActionResult CommitteReportPermissionList()
		{
			try
			{
				var data = (List<CommitteeTypePermission>)Helper.ExecuteService("Committee", "GetUserCommitteePermissionList", null);
				return PartialView("_ListCommitteTypeePermission", data);
			}
			catch (Exception)
			{

				throw;
			}

		}

		public ActionResult DeleteCommitteReportPermission(int id)
		{
			try
			{
				Helper.ExecuteService("Committee", "DeleteCommitteePermissionType", id);
				return Content("Data Deleted");
			}
			catch (Exception)
			{

				throw;
			}

		}

        public ActionResult _AuthorizeDashBoard()
        {
            try
            {
                AuthEmployee obj = new AuthEmployee();
                obj.AuthEmployeeList = (List<AuthEmployee>)Helper.ExecuteService("Committee", "GetCommitteeAuthorizeList", null);
                return View("_committeeAuthorizeDashboard",obj);
            }
            catch (Exception)
            {                
                throw;
            }
        }

        public ActionResult DeleteAuthorizedCommittee(int id)
        {
            try
            {
                Helper.ExecuteService("Committee", "DeleteCommitteeAuth", id);
                return RedirectToAction("_AuthorizeDashBoard");
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public ActionResult NewCommitteeAuthorize(AuthEmployee model)
        {
            AuthEmployee obj = new AuthEmployee();
            obj.Mode = "Add";
            obj.obUserList = (List<mUsers>)Helper.ExecuteService("Module", "GetUsersAdharDetails", null);  //GetUsersAdharDetails GetUsersAdharDetailsForCommittee
           
            return View("_NewCommiteeAuthorize",obj);
        }

        public ActionResult EditAuthorizedCommittee(int id)
        {
            AuthEmployee obj = new AuthEmployee();
            obj = (AuthEmployee)Helper.ExecuteService("Committee", "DataForEditCommAuth", id);
            obj.Mode = "Edit";
            obj.obUserList = (List<mUsers>)Helper.ExecuteService("Module", "GetUsersAdharDetails", null); //GetUsersAdharDetails GetUsersAdharDetailsForCommittee
            
            return View("_NewCommiteeAuthorize", obj);
            
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult CreateAuthorizeCommittee(AuthEmployee model)
        {
            //string AdharID;
            if (model.Mode != "Edit")
            {
                model.CreatedBy = CurrentSession.UserName;
                model.CreatedDate = DateTime.Now;
                string oldStr = model.AadharId;
                string newStr = oldStr.Trim();
                model.AadharId = newStr;
                //Regex.Replace(model.AadharId, @"\s", "");
                //model.AadharId.Replace(" ","");
               // model.AadharId.TrimStart();
            }
            else {
                model.ModifiedBy = CurrentSession.UserName;
                model.ModifiedDate = DateTime.Now;
                string oldStr = model.AadharId;
                string newStr = oldStr.Trim();
                model.AadharId = newStr;
                //Regex.Replace(model.AadharId, @"\s", "");
                //model.AadharId.Replace(" ","");
               // model.AadharId.TrimStart();
            }
            Helper.ExecuteService("Committee", "NewCommitteAuthorize", model);
            return RedirectToAction("_AuthorizeDashBoard");
        }
	}
}
