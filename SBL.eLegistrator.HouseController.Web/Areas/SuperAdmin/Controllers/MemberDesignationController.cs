﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class MemberDesignationController : Controller
    {
        //
        // GET: /SuperAdmin/MemberDesignation/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var MemDesignation = (List<mMemberDesignation>)Helper.ExecuteService("Designation", "GetAllMemDesignation", null);
                var model = MemDesignation.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There  is a Error While Getting Member Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateMemberDesignation()
        {
             try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new MemberDesignationViewModel()
                {
                   
                    Mode = "Add",
                    Active = true

                };
                return View("CreateMemberDesignation", model);
            }
             catch (Exception ex)
             {

                 ViewBag.ErrorMessage = "There  is a Error While Creating Member Designation Details";
                 return View("AdminErrorPage");
                 throw ex;
             }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMemDesignation(MemberDesignationViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Designation = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        var DesigCode = (int)Helper.ExecuteService("Designation", "GetMemDesigcode", null);
                        if (DesigCode > 0)
                        {
                            int desigcode = DesigCode;
                            Designation.memDesigcode = desigcode + 1;
                        }
                        else
                        {
                            int Count = 0;
                            Designation.memDesigcode = Count + 1;

                        }

                        Helper.ExecuteService("Designation", "CreateMemDesignation", Designation);
                    }
                    else
                    {
                        if (model.memDesigId.HasValue)
                        {
                            var DesigCode = (int)Helper.ExecuteService("Designation", "GetMemDesigcodeById", Designation.memDesigId);
                            if (DesigCode > 0)
                            {
                                int count = DesigCode;
                                Designation.memDesigcode = count;
                            }

                        }

                        Helper.ExecuteService("Designation", "UpdateMemDesignation", Designation);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Member Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult EditMemDesignation(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                //mMemberDesignation DesignationToEdit = (mMemberDesignation)Helper.ExecuteService("Designation", "GetMemDesignationById", new mMemberDesignation { memDesigId = Id });
                mMemberDesignation DesignationToEdit = (mMemberDesignation)Helper.ExecuteService("Designation", "GetMemDesignationById", new mMemberDesignation { memDesigId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = DesignationToEdit.ToViewModel1("Edit");
                return View("CreateMemberDesignation", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Member Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult DeleteMemDesignation(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMemberDesignation desigToDelete = (mMemberDesignation)Helper.ExecuteService("Designation", "GetMemDesignationById", new mMemberDesignation { memDesigId = Id });
                Helper.ExecuteService("Designation", "DeleteMemDesignation", desigToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Deleting Member Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

    }
}
