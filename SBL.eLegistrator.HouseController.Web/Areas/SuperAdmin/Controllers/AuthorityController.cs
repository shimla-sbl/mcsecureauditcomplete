﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Authority;
using SBL.DomainModel.Models.Secretory;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.DomainModel.Models.Enums;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
#pragma warning disable CS0105 // The using directive for 'System.IO' appeared previously in this namespace
using System.IO;
#pragma warning restore CS0105 // The using directive for 'System.IO' appeared previously in this namespace
using SBL.eLegistrator.HouseController.Web.Models;



namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class AuthorityController : Controller
    {
        //
        // GET: /SuperAdmin/Authority/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mAuthority>)Helper.ExecuteService("Authority", "GetAllAuthority", null);
                var model = State.ToViewModel();
                return View(model);


            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Authority Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateAuthority()
        {

            //var model = new AuthorityViewModel()
            //{

            //    Mode = "Add",
            //    IsActive=true

            //};
            var Secretery = (List<mSecretory>)Helper.ExecuteService("Authority", "GetAllSecretory", null);
            AuthorityViewModel model = new AuthorityViewModel();
            IEnumerable<Prefix> actionTypes = Enum.GetValues(typeof(Prefix))
                                                       .Cast<Prefix>();
            model.ActionsList = from action in actionTypes
                                select new SelectListItem
                                {
                                    Text = action.ToString(),
                                    Value = action.ToString()
                                };

            model.Mode = "Add";
            model.IsActive = true;
            model.Secretory = new SelectList(Secretery, "SecretoryID", "SecretoryName");
            return View("CreateAuthority", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAuthority(AuthorityViewModel model, HttpPostedFileBase file)
        {


            //if (file != null)
            //{
            //    string DirPath = Server.MapPath("~/assets/SuperAdmin/Authority/");
            //    if (!Directory.Exists(DirPath))
            //    {
            //        Directory.CreateDirectory(DirPath);
            //    }
            //    var fileName = Path.GetFileName(file.FileName);

            //    var path = Path.Combine(Server.MapPath("~/assets/SuperAdmin/Authority/"), fileName);
            //    file.SaveAs(path);

            //    ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
            //    imgCompress.GetImage = new System.Drawing.Bitmap(path);
            //    imgCompress.Height = 160;
            //    imgCompress.Width = 120;
            //    imgCompress.Save("compressingImage.jpg", DirPath);

            //    string newPath = Path.Combine(DirPath, "compressingImage.jpg");
            //    byte[] image = ReadImage(newPath);


            //    model.Photo = image;
            //}




            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    bill.FilePath = "";
                    bill.FileName = "";

                    var AuthorityID = (int)Helper.ExecuteService("Authority", "CreateAuthority", bill);

                    bill.AuthorityId = AuthorityID;


                    if (file != null)
                    {

                        var filename = AuthorityID + "Secretary" + ".jpg";
                        bill.FilePath = UploadPhoto(file, filename);
                        bill.FileName = filename;
                        bill.ThumbName = filename;
                        Helper.ExecuteService("Authority", "UpdateAuthority", bill);

                    }


                }
                else
                {

                    if (file != null)
                    {
                        if (bill.FileName != null)
                        {
                            RemoveExistingPhoto(bill.FileName);
                        }

                        var upfileName = bill.AuthorityId + "Secretary" + ".jpg";
                        bill.FilePath = UploadPhoto(file, upfileName);

                        bill.FileName = upfileName;
                        bill.ThumbName = upfileName;

                        Helper.ExecuteService("Authority", "UpdateAuthority", bill);
                    }
                    else
                    {
                        Helper.ExecuteService("Authority", "UpdateAuthority", bill);
                    }


                    //Helper.ExecuteService("Authority", "UpdateAuthority", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditAuthority(string Id)
        {
            var Secretery = (List<mSecretory>)Helper.ExecuteService("Authority", "GetAllSecretory", null);
            
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
            //mAuthority stateToEdit = (mAuthority)Helper.ExecuteService("Authority", "GetAuthorityById", new mAuthority { AuthorityId = Id });
            mAuthority stateToEdit = (mAuthority)Helper.ExecuteService("Authority", "GetAuthorityById", new mAuthority { AuthorityId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1(Secretery, "Edit");
            IEnumerable<Prefix> actionTypes = Enum.GetValues(typeof(Prefix))
                                                       .Cast<Prefix>();
            model.ActionsList = from action in actionTypes
                              select new SelectListItem
                              {
                                  Text = action.ToString(),
                                  Value = action.ToString()
                              };

            model.ImageLocation = fileAcessingSettings.SettingValue + model.FileLocation + "/" + model.FullImage;

            return View("CreateAuthority", model);
        }

        public ActionResult DeleteAuthority(int Id)
        {
            mAuthority stateToDelete = (mAuthority)Helper.ExecuteService("Authority", "GetAuthorityById", new mAuthority { AuthorityId = Id });
            Helper.ExecuteService("Authority", "DeleteAuthority", stateToDelete);

            if (!string.IsNullOrEmpty(stateToDelete.FileName))
                RemoveExistingPhoto(stateToDelete.FileName);

            return RedirectToAction("Index");

        }



        //private static byte[] ReadImage(string p_postedImageFileName)
        //{
        //    try
        //    {
        //        FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
        //        BinaryReader br = new BinaryReader(fs);
        //        byte[] image = br.ReadBytes((int)fs.Length);
        //        br.Close();
        //        fs.Close();
        //        return image;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}



        #region Private Methods

        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string UploadPhoto(HttpPostedFileBase File, string FileName)
        {
            try
            {


                if (File != null)
                {




                    var memberSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberFileSetting", null);
                    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    var memberThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberThumbFileSetting", null);

                    DirectoryInfo MemberDir = new DirectoryInfo(FileSettings.SettingValue + memberThumbSettings.SettingValue);
                    if (!MemberDir.Exists)
                    {
                        MemberDir.Create();
                    }


                    DirectoryInfo MemberTempDir = new DirectoryInfo(FileSettings.SettingValue + "\\Member\\Temp");
                    if (!MemberTempDir.Exists)
                    {
                        MemberTempDir.Create();
                    }



                    string extension = System.IO.Path.GetExtension(File.FileName);
                    string path = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue, FileName);
                    string path1 = System.IO.Path.Combine(FileSettings.SettingValue + memberThumbSettings.SettingValue, FileName);
                    string basePath = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue + "\\Temp", FileName);

                    SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgex = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(750);
                    File.SaveAs(basePath);
                    imgex.Resize(basePath, path);
                    System.IO.File.Delete(basePath);
                    SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgext = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(85);
                    File.SaveAs(basePath);
                    imgext.Resize(basePath, path1);

                    System.IO.File.Delete(basePath);
                    return ("/Member");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            if (File != null)
            {
                string extension = System.IO.Path.GetExtension(File.FileName);
                string path = System.IO.Path.Combine(Server.MapPath("~/Images/Member/"), FileName);
                File.SaveAs(path);
                return ("/Images/Member/");
            }
            return null;
        }

        private void RemoveExistingPhoto(string OldPhoto)
        {

            try
            {

                var memberSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberFileSetting", null);
                var memberThumbSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetMemberThumbFileSetting", null);
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                string path1 = System.IO.Path.Combine(FileSettings.SettingValue + memberThumbSettings.SettingValue, OldPhoto);
                string path = System.IO.Path.Combine(FileSettings.SettingValue + memberSettings.SettingValue + "\\", OldPhoto);

                //string path = System.IO.Path.Combine(Server.MapPath("~/Images/Gallery/"), OldPhoto);

                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(path1);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "memberError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

            }
        }
        private void GenerateThumbnails(double scaleFactor, Stream sourcePath, string targetPath)
        {
            using (var image = Image.FromStream(sourcePath))
            {
                var newWidth = (int)(image.Width * scaleFactor);
                var newHeight = (int)(image.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(image, imageRectangle);
                thumbnailImg.Save(targetPath, image.RawFormat);
            }
        }

        #endregion



    }
}
