﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Department;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class DepartmentController : Controller
    {
        //
        // GET: /SuperAdmin/Department/

        public ActionResult Index()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
            var State = (List<mDepartment>)Helper.ExecuteService("Department", "GetAllDepartment", null);
            var model = State.ToViewModel();
            return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Department Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateDepartment()
        {
            var model = new DepartmentViewModel()
            {
                Mode = "Add",
                IsActive=true

            };

            return View("CreateDepartment", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDepartment(DepartmentViewModel model)
        {

            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Department", "CreateDepartment", bill);
                }
                else
                {
                    Helper.ExecuteService("Department", "UpdDepartment", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditDepartment(string Id)
        {

            //mDepartment stateToEdit = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentById", new mDepartment { deptId = Id });
            mDepartment stateToEdit = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentById", new mDepartment { deptId = Convert.ToString(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1("Edit");
            return View("CreateDepartment", model);
        }

        public ActionResult DeleteDepartment(string Id)
        {
            mDepartment stateToDelete = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentById", new mDepartment { deptId = Id });
            Helper.ExecuteService("Department", "DelDepartment", stateToDelete);
            return RedirectToAction("Index");

        }


        [HttpPost]
        public JsonResult CheckId(string DepartmentId)
        {

            mDepartment obj = new mDepartment();


            obj.deptId = DepartmentId;
            string st = Helper.ExecuteService("Department", "CheckId", obj) as string;

            return Json(st, JsonRequestBehavior.AllowGet);
        }



        public JsonResult CheckDepartmentIdExist(string DepartmentID)
        {


            var isExist = (Boolean)Helper.ExecuteService("Department", "IsDepartmentIdExist", new mDepartment { deptId = DepartmentID });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteDepartmentIdExist(string DepartmentID)
        {


            var isExist = (Boolean)Helper.ExecuteService("Department", "DeleteIsDepartmentIdExist", new mDepartment { deptId = DepartmentID });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }

    }
}
