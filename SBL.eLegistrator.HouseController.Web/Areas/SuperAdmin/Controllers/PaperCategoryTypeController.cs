﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class PaperCategoryTypeController : Controller
    {
        //
        // GET: /SuperAdmin/PaperCategoryType/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
            var State = (List<mPaperCategoryType>)Helper.ExecuteService("PaperCategoryType", "GetAllPaper", null);
            var model = State.ToViewModel();
            return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Paper Category Type Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreatePaper()
        {
            var model = new PaperCategoryTypeViewModel()
            {
                Mode = "Add",
                IsActive=true

            };

            return View("CreatePaper", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePaper(PaperCategoryTypeViewModel model)
        {

            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("PaperCategoryType", "CreatePaper", bill);
                }
                else
                {
                    Helper.ExecuteService("PaperCategoryType", "UpdatePaper", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditPaper(string Id)
        {

            //mPaperCategoryType stateToEdit = (mPaperCategoryType)Helper.ExecuteService("PaperCategoryType", "GetPaperById", new mPaperCategoryType { PaperCategoryTypeId = Id });
            mPaperCategoryType stateToEdit = (mPaperCategoryType)Helper.ExecuteService("PaperCategoryType", "GetPaperById", new mPaperCategoryType { PaperCategoryTypeId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1("Edit");
            return View("CreatePaper", model);
        }

        public ActionResult DeletePaper(int Id)
        {
            mPaperCategoryType stateToDelete = (mPaperCategoryType)Helper.ExecuteService("PaperCategoryType", "GetPaperById", new mPaperCategoryType { PaperCategoryTypeId = Id });
            Helper.ExecuteService("PaperCategoryType", "DeletePaper", stateToDelete);
            return RedirectToAction("Index");

        }

        public JsonResult CheckPCTChildExist(int PCTID)
        {
            var isExist = (Boolean)Helper.ExecuteService("PaperCategoryType", "IsPCTIdChildExist", new mPaperCategoryType { PaperCategoryTypeId = PCTID });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }

    }
}
