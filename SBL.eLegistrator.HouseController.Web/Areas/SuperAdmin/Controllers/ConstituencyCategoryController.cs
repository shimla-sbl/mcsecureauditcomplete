﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Constituency;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class ConstituencyCategoryController : Controller
    {
        //
        // GET: /SuperAdmin/ConstituencyCategory/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var ConstituencyCategory = (List<mConstituencyReservedCategory>)Helper.ExecuteService("ConstituencyCategory", "GetAllConstituencyCategory", null);
                var model = ConstituencyCategory.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Constituency Category Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateConstituencyCategory()
        {
            var model = new ConstituencyCategoryViewModel()
            {
                Mode = "Add",
                IsActive = true

            };

            return View("CreateConstituencyCategory", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveConstituencyCategory(ConstituencyCategoryViewModel model)
        {

            if (ModelState.IsValid)
            {
                var ConstituencyCategory = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("ConstituencyCategory", "CreateConstituencyCategory", ConstituencyCategory);
                }
                else
                {
                    Helper.ExecuteService("ConstituencyCategory", "UpdateConstituencyCategory", ConstituencyCategory);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditConstituencyCategory(string Id)
        {

            //mConstituencyReservedCategory stateToEdit = (mConstituencyReservedCategory)Helper.ExecuteService("ConstituencyCategory", "GetConstituencyCategoryById", new mConstituencyReservedCategory { ConstituencyCategoryId = Id });
            mConstituencyReservedCategory stateToEdit = (mConstituencyReservedCategory)Helper.ExecuteService("ConstituencyCategory", "GetConstituencyCategoryById", new mConstituencyReservedCategory { ConstituencyCategoryId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1("Edit");
            return View("CreateConstituencyCategory", model);
        }

        public ActionResult DeleteConstituencyCategory(int Id)
        {
            mConstituencyReservedCategory stateToDelete = (mConstituencyReservedCategory)Helper.ExecuteService("ConstituencyCategory", "GetConstituencyCategoryById", new mConstituencyReservedCategory { ConstituencyCategoryId = Id });
            Helper.ExecuteService("ConstituencyCategory", "DeleteConstituencyCategory", stateToDelete);
            return RedirectToAction("Index");

        }

    }
}
