﻿using SBL.DomainModel.Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class SubUserTypeController : Controller
    {
        //
        // GET: /SuperAdmin/SubUserType/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserManagement()
        {
            return View();
        }

        public ActionResult GetUserSubUserType()
        {
            var UserType = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserType", null);
            var model = UserType.ToViewSubUserType();

            return PartialView("_GetSubUserType", model);
        }

        public ActionResult CreateNewSubUserType()
        {
            //For User Access
            mUserType UserTypemodel = new mUserType();
            var returnedUserTypemodel = Helper.ExecuteService("Module", "GetUserTypeDropDown", UserTypemodel) as List<mUserType>;
            var returnedUserTypemodelNew = returnedUserTypemodel.ToSubUserTypeList();


            var model1 = new mSubUserType()
            {
                Mode = "Add",
                UserTypeList = returnedUserTypemodelNew,
            };

            return PartialView("_CreateNewSubUserType", model1);

        }

         [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSubUserType(mSubUserType model)
        {

            if (ModelState.IsValid)
            {
                model.ModifiedWhen = DateTime.Now;
                var dist = model.ToSubUserTypeModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateSubUserType", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntrySubUserType", dist);
                }

                return RedirectToAction("GetUserSubUserType");
            }
            else
            {

                return RedirectToAction("GetUserSubUserType");
            }

        }


         public ActionResult EditSubUserType(string Id)
         {

             mUserType UserTypemodel = new mUserType();
             var returnedUserTypemodel = Helper.ExecuteService("Module", "GetUserTypeDropDown", UserTypemodel) as List<mUserType>;
             var returnedUserTypemodelNew = returnedUserTypemodel.ToSubUserTypeList();


             var model1 = new mSubUserType()
             {
                 // Mode = "Add",
                 UserTypeList = returnedUserTypemodelNew,
             };

             mSubUserType SubUserTypeToEdit = (mSubUserType)Helper.ExecuteService("Module", "GetSUserTypeDataById", new mSubUserType { SubUserTypeID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

             var model = SubUserTypeToEdit.ToSubUserTypeModel();


             model1.UserTypeID = model.UserTypeID;
             model1.SubUserTypeID = model.SubUserTypeID;
             model1.UserTypeName = model.UserTypeName;
             model1.SubUserTypeName = model.SubUserTypeName;
             model1.SubUserTypeNameLocal = model.SubUserTypeNameLocal;
             model1.SubUserTypeDescription = model.SubUserTypeDescription;
             return View("_CreateNewSubUserType", model1);
         }



         public ActionResult DeleteSubUserType(string Id)
         {
             mSubUserType SubUserTypeToDelete = (mSubUserType)Helper.ExecuteService("Module", "GetSUserTypeDataById", new mSubUserType { SubUserTypeID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
             Helper.ExecuteService("Module", "DeleteSubUserType", SubUserTypeToDelete);
             return RedirectToAction("GetUserSubUserType");

         }


         public JsonResult CheckSubUserType(string SubUserTypeName)
         {
             try
             {
                 var mdl = (bool)Helper.ExecuteService("Role", "CheckSubUserTypeExist", SubUserTypeName);
                 return Json(mdl, JsonRequestBehavior.AllowGet);
             }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
             catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
             {

                 throw;
             }
         }

        
    }
}
