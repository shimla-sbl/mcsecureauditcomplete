﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using System.IO;
using Microsoft.Security.Application;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MobileVersionsController : Controller
    {
        //
        // GET: /SuperAdmin/MobileVersions/

        public ActionResult Index()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                var mobiApps = (List<tMobileVersions>)Helper.ExecuteService("Mobiles", "GetAllMobileVersions", null);
                var model = mobiApps.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Mobile Version Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateMobiVersions()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var AppCodes = (List<mMobileApps>)Helper.ExecuteService("Mobiles", "GetAllAppCodes", null);
                var model = new MobileVersionsViewModel()
                {
                    Mode = "Add",
                    CodeList = new SelectList(AppCodes, "Code", "Code")

                };
                return View("CreateMobiVersions", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Mobile Versions Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SaveMobileVersions(HttpPostedFileBase file)
        {
            try
            {
                MobileVersionsViewModel model = new MobileVersionsViewModel();

                model.Code = Request.Form["Code"].ToString();

                model.Major = Convert.ToInt32(Request.Form["Major"].ToString());

                model.Minor = Convert.ToInt32(Request.Form["Minor"].ToString());

                model.Build = Convert.ToInt32(Request.Form["Build"].ToString());

                model.VersionDate = Request.Form["VersionDate"].ToString();

                model.FilePath = Request.Form["FilePath"].ToString();

                model.Mode = Request.Form["Mode"].ToString();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                string pdfPath = string.Empty;

                if (file != null)
                {
                    UploadFile(file, file.FileName, model.Code, out pdfPath);

                    model.FilePath = pdfPath.Replace(@"\", @"/");
                }

                var mobileVersions = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateMobileVersions", mobileVersions);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateMobileVersions", mobileVersions);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditMobileVersions(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var AppCodes = (List<mMobileApps>)Helper.ExecuteService("Mobiles", "GetAllAppCodes", null);
                tMobileVersions verToEdit = (tMobileVersions)Helper.ExecuteService("Mobiles", "GetMobileVersionsBasedOnId", new tMobileVersions { SlNo = Id });
                var model = verToEdit.ToViewModel1(AppCodes, "Edit");
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSettingLocal", null);
                model.DownloadPath = FileSettings.SettingValue + @"/" + verToEdit.FilePath;
                return View("CreateMobiVersions", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Mobile Versions Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteMobileVersions(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                tMobileVersions appsToRemove = (tMobileVersions)Helper.ExecuteService("Mobiles", "GetMobileVersionsBasedOnId", new tMobileVersions { SlNo = Id });
                Helper.ExecuteService("Mobiles", "DeleteMobileVersions", appsToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Mobile Apps Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        private void UploadFile(HttpPostedFileBase File, string FileName, string code, out string dbFileName)
        {
            try
            {
                string savedFileName = string.Empty;

                dbFileName = string.Empty;

                if (File != null)
                {
                    var fileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string dirPath = string.Empty;

                    //string fileNameWithoutExt = Path.GetFileNameWithoutExtension(File.FileName).Replace(".", "_").Replace("-", "_");

                    //string fileName = GetTimestamp(DateTime.Now) + "_" + fileNameWithoutExt + Path.GetExtension(File.FileName);

                    if (code == "PUB")
                    {
                        dirPath = Path.Combine(fileSettings.SettingValue + @"evapk\hpvspublic");

                        savedFileName = "Publicapp.apk";

                        dbFileName = "PUB" + @"\" + FileName;
                    }
                    else if (code == "MED")
                    {
                        dirPath = Path.Combine(fileSettings.SettingValue + @"evapk\hpvsmedia");

                        savedFileName = "hpvsmediaapp.apk";

                        dbFileName = "MED" + @"\" + FileName;
                    }
                    else if (code == "MEM")
                    {
                        dirPath = Path.Combine(fileSettings.SettingValue + @"evapk\hpvsmember");

                        savedFileName = "hpvsmemberapp.apk";

                        dbFileName = "GMEM" + @"\" + FileName;
                    }
                    else if (code == "GENCON")
                    {
                        dirPath = Path.Combine(fileSettings.SettingValue + @"evapk\hpvsconstituency");

                        savedFileName = "hpvsconstituency.apk";

                        dbFileName = "GENCON" + @"\" + FileName;
                    }
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }

                    rename(dirPath, savedFileName);
                    dirPath = dirPath + @"\" + savedFileName;

                   
                    File.SaveAs(dirPath);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void rename(string path, string savedFileName )
        {
            string oldFileName = savedFileName.Split('.')[0] +"_"+ DateTime.Now.ToString("dd_MM_yyyy") + ".apk";
            if (System.IO.File.Exists(Path.Combine(path, oldFileName)) == true)
            {
                System.IO.File.Delete(Path.Combine(path, oldFileName));
            }
            
            if (System.IO.File.Exists(Path.Combine(path, savedFileName)))
            {
                System.IO.File.Move(Path.Combine(path, savedFileName), Path.Combine(path, oldFileName));
            }
           
        }
        public ActionResult Download(string filePathWithName, string code)
        {
            filePathWithName = Sanitizer.GetSafeHtmlFragment(filePathWithName);
            string filename = string.Empty;
            if (code == "PUB")
            {
                filename = @"C:\inetpub\e_Vidhan\FileStructure\evapk\hpvsconstituency\" + System.IO.Path.GetFileName(filePathWithName);
            }
            else if (code == "MEM")
            {
                filename = @"C:\inetpub\e_Vidhan\FileStructure\evapk\hpvsmember\" + System.IO.Path.GetFileName(filePathWithName);
            }
            else if (code == "MED")
            {
                filename = @"C:\inetpub\e_Vidhan\FileStructure\evapk\hpvsmedia\" + System.IO.Path.GetFileName(filePathWithName);
            }
            var fs = System.IO.File.OpenRead(filename);
            return File(fs, "appliaction/zip", filePathWithName.Substring(filePathWithName.LastIndexOf("/") + 1));
        }

    }
}
