﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Constituency;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class VillageController : Controller
    {
        const int pageSize = 10;
        //
        // GET: /SuperAdmin/Village/

        public ActionResult ShowAllVillageDetails()
        {
            return View();
        }



        public ActionResult IndexOfVillage(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount, string ClickCount, string TempLoop)
        {
            try
            {

                mVillage model = new mVillage();
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //if (SearchText != null && SearchText != "")
                //   model.PanchayatName = SearchText;
                if ((PageNumber == null || PageNumber == "") && (RowsPerPage == null || RowsPerPage == "") && (loopStart == null || loopStart == "") && (loopEnd == null || loopEnd == ""))
                {
                    PageNumber = (1).ToString();

                    RowsPerPage = (10).ToString();
                    loopStart = (1).ToString();
                    loopEnd = (5).ToString();
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                    //SearchText = SearchText;
                }
                else
                {
                    TempLoop = Sanitizer.GetSafeHtmlFragment(TempLoop);
                    PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                    RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                    loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                    loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    ClickCount = Sanitizer.GetSafeHtmlFragment(ClickCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                }
                //model.PAGE_SIZE = 25;
                model.PageIndex = int.Parse(PageNumber);
                //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblies", null);
                model = (mVillage)Helper.ExecuteService("Village", "GetAllVillages", model);
                model.ResultCount = model.ResultCount;
                model.PageNumber = Convert.ToInt32(PageNumber);
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
                // model.RowsPerPage = (25);
                model.selectedPage = Convert.ToInt32(PageNumber);
                model.loopStart = Convert.ToInt32(loopStart);
                model.loopEnd = Convert.ToInt32(loopEnd);
                //var model = Assemblies.ToViewModel();
                if (TempLoop != "")
                {
                    model.loopStart = Convert.ToInt32(TempLoop);
                }
                else
                {
                    model.loopStart = Convert.ToInt32(loopStart);
                }
                if (ClickCount != "")
                {
                    model.ClickCount = Convert.ToInt32(ClickCount);
                }
                return PartialView("_PartialIndexOfVillage", model);
                //mVillage model = new mVillage();
                //if (string.IsNullOrEmpty(CurrentSession.UserName))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}
                //if ((PageNumber == null || PageNumber == "") && (RowsPerPage == null || RowsPerPage == "") && (loopStart == null || loopStart == "") && (loopEnd == null || loopEnd == ""))
                //{
                //    PageNumber = (1).ToString();

                //    RowsPerPage = (10).ToString();
                //    loopStart = (1).ToString();
                //    loopEnd = (5).ToString();
                //    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                //    model.PAGE_SIZE = int.Parse(RowsPerPage);
                //    //SearchText = SearchText;
                //}
                //else
                //{
                //    TempLoop = Sanitizer.GetSafeHtmlFragment(TempLoop);
                //    PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                //    RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                //    loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                //    loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                //    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                //    ClickCount = Sanitizer.GetSafeHtmlFragment(ClickCount);
                //    model.PAGE_SIZE = int.Parse(RowsPerPage);
                //}
                ////model.PAGE_SIZE = 25;
                //model.PageIndex = int.Parse(PageNumber);
                ////var Assemblies = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblies", null);
                //model = (mVillage)Helper.ExecuteService("Village", "GetAllVillages", model);
                //model.ResultCount = model.ResultCount;
                //model.PageNumber = Convert.ToInt32(PageNumber);
                //model.RowsPerPage = Convert.ToInt32(RowsPerPage);
                //// model.RowsPerPage = (25);
                //model.selectedPage = Convert.ToInt32(PageNumber);
                //model.loopStart = Convert.ToInt32(loopStart);
                //model.loopEnd = Convert.ToInt32(loopEnd);
                ////var model = Assemblies.ToViewModel();
                //if (TempLoop != "")
                //{
                //    model.loopStart = Convert.ToInt32(TempLoop);
                //}
                //else
                //{
                //    model.loopStart = Convert.ToInt32(loopStart);
                //}
                //if (ClickCount != "")
                //{
                //    model.ClickCount = Convert.ToInt32(ClickCount);
                //}
                //return PartialView("_PartialIndexOfVillage", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Village Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }


        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Village = (List<mVillage>)Helper.ExecuteService("Village", "GetAllVillages", null);
                var model = Village.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Village Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateVillage()
        {
            if (string.IsNullOrEmpty(CurrentSession.UserName))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            try
            {
                var model = new VillageViewModel()
           {
               Mode = "Add",
               IsActive = true
           };

                return View("CreateVillage", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Village Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveVillage(VillageViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Village = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("Village", "CreateVillage", Village);
                    }
                    else
                        Helper.ExecuteService("Village", "UpdateVillage", Village);
                    return RedirectToAction("ShowAllVillageDetails");

                }
                else
                {
                    return RedirectToAction("ShowAllVillageDetails");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Village Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult EditVillage(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //mVillage villageToEdit = (mVillage)Helper.ExecuteService("Village", "GetVillageBasedOnId", new mVillage { VillageID = Id });
                mVillage villageToEdit = (mVillage)Helper.ExecuteService("Village", "GetVillageBasedOnId", new mVillage { VillageID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var data = villageToEdit.ToViewModel1("Edit");
                return View("CreateVillage", data);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Village Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteVillage(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mVillage villageToDelete = (mVillage)Helper.ExecuteService("Village", "GetVillageBasedOnId", new mVillage { VillageID = Id });
                Helper.ExecuteService("Village", "DeleteVillage", villageToDelete);
                return RedirectToAction("ShowAllVillageDetails");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Village Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

    }
}
