﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Document;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class DocumentTypeController : Controller
    {
        //
        // GET: /SuperAdmin/DocumentType/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Dtype = (List<mDocumentType>)Helper.ExecuteService("Designation", "GetAllDocumentType", null);
                var model = Dtype.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Getting Document Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }

        public ActionResult CreateDocumentType()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new DocumentTypeViewModel()
                {
                    Mode = "Add",
                    IsActive = true
                };
                return View("CreateDocumentType", model);   
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Creating Document Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
                  
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveDocumentType(DocumentTypeViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var DocumentType = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("Designation", "CreateDocumentType", DocumentType);
                    }
                    else
                    {
                        Helper.ExecuteService("Designation", "UpdateDocumentType", DocumentType);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Saving Document Type Details";
                return View("AdminErrorPage");
                throw ex;
            }

          

        }

        public ActionResult EditDocumentType(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //mDocumentType DtypeToEdit = (mDocumentType)Helper.ExecuteService("Designation", "GetDocumentTypeById", new mDocumentType { DocumentTypeID = Id });
                mDocumentType DtypeToEdit = (mDocumentType)Helper.ExecuteService("Designation", "GetDocumentTypeById", new mDocumentType { DocumentTypeID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = DtypeToEdit.ToViewModel1("Edit");
                return View("CreateDocumentType", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Document Type Details";
                return View("AdminErrorPage");
                throw ex;
            }


           
        }

        public ActionResult DeleteDocumentType(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mDocumentType DoctypeToDelete = (mDocumentType)Helper.ExecuteService("Designation", "GetDocumentTypeById", new mDocumentType { DocumentTypeID = Id });
                Helper.ExecuteService("Designation", "DeleteDocumentType", DoctypeToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Deleting Document Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
          

        }



    }
}
