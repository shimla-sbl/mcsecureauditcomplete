﻿using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class SubUserTypeRolesController : Controller
    {
        //
        // GET: /SuperAdmin/SubUserTypeRoles/

        public ActionResult Index()
        {
            mSubUserType Submodel = new mSubUserType();
            var returnedSubUserTypeResult = Helper.ExecuteService("Module", "GetSubUserTypeDropDown", Submodel) as List<mSubUserType>;
            var returnedSubUserTypeResultNew = returnedSubUserTypeResult.ToSubUserTypeListForAccess();

            mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToModelList();
            mRoles rolemodel = new mRoles();
            var rolresulet = Helper.ExecuteService("Role", "GetRoleDropDown", rolemodel) as List<mRoles>;
            var roleNewRslt = rolresulet.TomroleList();

            var model1 = new mSubUserTypeRoles()
            {
                Mode = "Add",
                UserTypeList = returnedResultNew,
                SubUserTypeList = returnedSubUserTypeResultNew,
                RoleList = rolresulet
            };

            return PartialView("_CreateNewSubUserTypeRole", model1);
        }

        public ActionResult EditSubUsertypRole(string Id)
        {
            mSubUserType Submodel = new mSubUserType();
            var returnedSubUserTypeResult = Helper.ExecuteService("Module", "GetSubUserTypeDropDown", Submodel) as List<mSubUserType>;
            var returnedSubUserTypeResultNew = returnedSubUserTypeResult.ToSubUserTypeListForAccess();

            mUserType model = new mUserType();
            var returnedResult = Helper.ExecuteService("Module", "GetUserTypeDropDown", model) as List<mUserType>;
            var returnedResultNew = returnedResult.ToModelList();
            mRoles rolemodel = new mRoles();
            var rolresulet = Helper.ExecuteService("Role", "GetRoleDropDown", rolemodel) as List<mRoles>;
            var roleNewRslt = rolresulet.TomroleList();

            mSubUserTypeRoles subrolemdl = new mSubUserTypeRoles();
            subrolemdl.ID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
            subrolemdl = (mSubUserTypeRoles)Helper.ExecuteService("Module", "GetEditSubUsertypeRole", subrolemdl);
            var model1 = new mSubUserTypeRoles()
            {
                Mode = "Edit",
                UserTypeList = returnedResultNew,
                SubUserTypeList = returnedSubUserTypeResultNew,
                RoleList = rolresulet,
                UserTypeNameSelected = subrolemdl.UserTypeID,
                SubUserTypeNameSelected = subrolemdl.SubUserTypeID,
                RoleNameSelected = subrolemdl.RoleId,
                SubUserTypeID = subrolemdl.SubUserTypeID,
                RoleId = subrolemdl.RoleId,
            };

            return PartialView("_CreateNewSubUserTypeRole", model1);
        }

        public ActionResult GetSubSusertyproleList()
        {
            mSubUserTypeRoles Submodel = new mSubUserTypeRoles();
            var returnedResult = Helper.ExecuteService("Module", "GetSubUserTypeRoleList", Submodel) as List<mSubUserTypeRoles>;
            // var returnedResultNew = returnedResult.ToViewSubUserTypeRole();

            return PartialView("_GetSubUserTypeRoleList", returnedResult);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSubUserTypeRole(mSubUserTypeRoles model)
        {
            if (ModelState.IsValid)
            {
                if (model.Mode == "Add")
                {
                    model.IsActive = true;

                    if (model.AllRoleList != null)
                    {
                        foreach (var item in model.AllRoleList)
                        {
                            if (item != "0")
                            {
                                model.RoleId = Guid.Parse(item);
                                Helper.ExecuteService("Role", "CreateSubUserRole", model);
                            }
                        }
                    }
                }
                else
                {
                    if (model.AllRoleList != null)
                    {
                        foreach (var item in model.AllRoleList)
                        {
                            if (item != "0")
                            {
                                model.RoleId = Guid.Parse(item);
                                Helper.ExecuteService("Role", "UpdateSubUserRole", model);
                                return RedirectToAction("GetSubSusertyproleList");
                            }
                        }
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public JsonResult CheckSubUserRoleExist(int subuserid, string roleid)
        {
            try
            {
                mSubUserTypeRoles mdl = new mSubUserTypeRoles();
                mdl.SubUserTypeID = subuserid;
                roleid = roleid.TrimEnd(',');
                string[] idarr = roleid.Split(',');
                if (idarr.Length > 0)
                {
                    foreach (var item in idarr)
                    {
                        mdl.RoleId = new Guid(item);
                        var mdl1 = (bool)Helper.ExecuteService("Role", "CheckSubUserRoleExist", mdl);
                        return Json(mdl1, JsonRequestBehavior.AllowGet);
                    }
                }
                return null;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                throw;
            }
        }

        #region CheckForUserRoll

        public JsonResult CheckSubUserRoleExistBySubUserID(int subuserid)
        {
            try
            {
                mSubUserTypeRoles ObjmSubUserTypeRoles = new mSubUserTypeRoles();
                ObjmSubUserTypeRoles.SubUserTypeID = subuserid;
                var result = (bool)Helper.ExecuteService("Role", "CheckSubUserRoleExistBySubUserID", ObjmSubUserTypeRoles);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                throw;
            }
        }

        public List<mSubUserTypeRoles> GetAllSubUserTypeRoles()
        {
            var Roles = (List<mSubUserTypeRoles>)Helper.ExecuteService("Role", "GetUserRole", null);

            return Roles.ToList();
        }

        public JsonResult CheckUserRoleExistByUserID(Guid UserID)
        {
            try
            {
                tUserRoles ObjtUserRoles = new tUserRoles();
                ObjtUserRoles.UserID = UserID;
                var result = (bool)Helper.ExecuteService("Role", "CheckUserRoleExistByUserID", ObjtUserRoles);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                throw;
            }
        }

        #endregion CheckForUserRoll
    }
}