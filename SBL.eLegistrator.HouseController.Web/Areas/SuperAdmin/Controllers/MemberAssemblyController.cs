﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Party;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using SBL.DomainModel.Models.Session;
namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MemberAssemblyController : Controller
    {
        //
        // GET: /SuperAdmin/MemberAssembly/
        public ActionResult ShowAllAssemblies()
        {
            mMemberAssembly model = new mMemberAssembly();
            var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
            ViewBag.assmeblyList = assmeblyList;
            model.AssemblyID = assmeblyList.FirstOrDefault().AssemblyCode;
            return View(model);
        }

        public ActionResult Index(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount, string ClickCount, string TempLoop, int assemblyId)
        {
            try
            {
                mMemberAssembly model = new mMemberAssembly();
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //    var MemAssembly = (List<mMemberAssembly>)Helper.ExecuteService("Member", "GetAllMemberAssembly", null);
                //    var model = MemAssembly.ToViewModel();
                //    return View(model);
                if ((PageNumber == null || PageNumber == "") && (RowsPerPage == null || RowsPerPage == "") && (loopStart == null || loopStart == "") && (loopEnd == null || loopEnd == ""))
                {
                    PageNumber = (1).ToString();

                    RowsPerPage = (10).ToString();
                    loopStart = (1).ToString();
                    loopEnd = (5).ToString();
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                    model.AssemblyID = assemblyId;
                    //SearchText = SearchText;
                }
                else
                {
                    TempLoop = Sanitizer.GetSafeHtmlFragment(TempLoop);
                    PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                    RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                    loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                    loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    ClickCount = Sanitizer.GetSafeHtmlFragment(ClickCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                    model.AssemblyID = assemblyId;
                }
                //model.PAGE_SIZE = 25;
                model.PageIndex = int.Parse(PageNumber);
                //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblies", null);

                //model = (mMemberAssembly)Helper.ExecuteService("Member", "GetAllMemberAssembly", model);

                model = (mMemberAssembly)Helper.ExecuteService("Member", "GetAllMemberAssemblyByAssembly", model);//by robin 
                model.ResultCount = model.ResultCount;
                model.PageNumber = Convert.ToInt32(PageNumber);
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
                // model.RowsPerPage = (25);
                model.selectedPage = Convert.ToInt32(PageNumber);
                model.loopStart = Convert.ToInt32(loopStart);
                model.loopEnd = Convert.ToInt32(loopEnd);
                //var model = Assemblies.ToViewModel();
                if (TempLoop != "")
                {
                    model.loopStart = Convert.ToInt32(TempLoop);
                }
                else
                {
                    model.loopStart = Convert.ToInt32(loopStart);
                }
                if (ClickCount != "")
                {
                    model.ClickCount = Convert.ToInt32(ClickCount);
                }
                return PartialView("_PartialIndexOfMemberAssembly", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Member Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult CreateMemberAssembly()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);//   -----------------now returning empty member list in page load
                //List<mMember> Memberlist = new List<mMember>();
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var Party = (List<mParty>)Helper.ExecuteService("Member", "GetAllParty", null);
                //var Constituency = (List<mConstituency>)Helper.ExecuteService("Member", "GetAllConstituency", null);

                mConstituency data = new mConstituency();
                data.AssemblyID = Assemblies.FirstOrDefault().AssemblyCode;
                var Constituency = (List<mConstituency>)Helper.ExecuteService("Member", "GetConstituencyByAssemblyID", data);
                //var Constituency = (List<mConstituency>)Helper.ExecuteService("Constituency", "GetAllConstituency", null);// new requirement to show all consistuency done by robin

                var Locations = (List<DistrictModel>)Helper.ExecuteService("Member", "GetAllDistrict", null);
                var MemberDesignation = (List<mMemberDesignation>)Helper.ExecuteService("Member", "GetAllMemberDesignation", null);
                var model = new MemberAssemblyViewModel()
                {
                    Mode = "Add",
                    MemberNames = new SelectList(Members, "MemberCode", "Name"),
                    //MemberNames = new SelectList(Memberlist, "MemberCode", "Name"),//new change done by robin
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    //DesignationList = ExtensionsOfMemberAssembly.GetDesignation(),
                    DesignationList = new SelectList(MemberDesignation, "memDesigId", "memDesigname"),
                    PartryNames = new SelectList(Party, "PartyID", "PartyName"),
                    ConstituencyNames = new SelectList(Constituency, "ConstituencyCode", "ConstituencyName"),
                    Locations = new SelectList(Locations, "DistrictName", "DistrictName"),
                    Active = true
                };
                return View("CreateMemberAssembly", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Creating Member Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMemberAssembly(MemberAssemblyViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var ministry = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {

                        //ministry.Active = true;
                        Helper.ExecuteService("Member", "CreateMemberAssembly", ministry);
                    }
                    else
                    {


                        //ministry.Active = true;
                        Helper.ExecuteService("Member", "UpdateMemberAssembly", ministry);
                    }
                    return RedirectToAction("ShowAllAssemblies");
                }
                else
                {
                    return RedirectToAction("ShowAllAssemblies");
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Member Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult EditMemberAssembly(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                //mMemberAssembly ministryToEdit = (mMemberAssembly)Helper.ExecuteService("Member", "GetMemberAssemblyById", new mMemberAssembly { MemberAssemblyID = Id });
                mMemberAssembly ministryToEdit = (mMemberAssembly)Helper.ExecuteService("Member", "GetMemberAssemblyById", new mMemberAssembly { MemberAssemblyID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

                // var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);//

                //var MemberAssemblyID = CurrentSession.AssemblyId;
                var MemberAssemblyID = ministryToEdit.AssemblyID;
                //int MemberAssemblyID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
                var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembersByAssemblyId", MemberAssemblyID);

                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var Party = (List<mParty>)Helper.ExecuteService("Party", "GetAllParty", null);

                var Constituency = (List<mConstituency>)Helper.ExecuteService("Constituency", "GetAllConstituency", null);

                var Locations = (List<DistrictModel>)Helper.ExecuteService("District", "GetAllDistrict", null);

                var MemberDesignation = (List<mMemberDesignation>)Helper.ExecuteService("Member", "GetAllMemberDesignation", null);


                var model = ministryToEdit.ToViewModel1(Members, Assemblies, Constituency, Party, Locations, MemberDesignation, "Edit");
                //model.DesignationList = ExtensionsOfMemberAssembly.GetDesignation();

                return View("CreateMemberAssembly", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Member Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteMemberAssembly(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMemberAssembly ministryToDelete = (mMemberAssembly)Helper.ExecuteService("Member", "GetMemberAssemblyById", new mMemberAssembly { MemberAssemblyID = Id });
                Helper.ExecuteService("Member", "DeleteMemberAssembly", ministryToDelete);
                return RedirectToAction("ShowAllAssemblies");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Deleting Member Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }



        public ActionResult ChangeAssembly(int assemblyId)
        {
            try
            {


                mConstituency data = new mConstituency();
                data.AssemblyID = assemblyId;
                var constituencyList = (List<mConstituency>)Helper.ExecuteService("Member", "GetConstituencyByAssemblyID", data);

                return Json(constituencyList, JsonRequestBehavior.AllowGet);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Assembly Details";
                return View("AdminErrorPage");
            }
        }

        public ActionResult GetMembersByAssemblyId(int assemblyId)//done by robin
        {
            try
            {


                //mConstituency data = new mConstituency();
                //data.AssemblyID = assemblyId;
                var MemberList = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembersByAssemblyId", assemblyId);

                return Json(MemberList, JsonRequestBehavior.AllowGet);


            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Assembly Details";
                return View("AdminErrorPage");
            }
        }


    }
}
