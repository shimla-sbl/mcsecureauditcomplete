﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.DomainModel.Models.Department;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class EmployeeController : Controller
    {
        //
        // GET: /SuperAdmin/Employee/





        public ActionResult ShowAllEmployeeDetails()
        {
            return View();
        }

        

        [HttpGet]
        public ActionResult IndexOfEmployee(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount, string ClickCount, string TempLoop)
        {
            try
            {
                mEmployee model = new mEmployee();
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //if (SearchText != null && SearchText != "")
                //   model.PanchayatName = SearchText;
                if ((PageNumber == null || PageNumber == "") && (RowsPerPage == null || RowsPerPage == "") && (loopStart == null || loopStart == "") && (loopEnd == null || loopEnd == ""))
                {
                    PageNumber = (1).ToString();

                    RowsPerPage = (10).ToString();
                    loopStart = (1).ToString();
                    loopEnd = (5).ToString();
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                    //SearchText = SearchText;
                }
                else
                {
                    TempLoop = Sanitizer.GetSafeHtmlFragment(TempLoop);
                    PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                    RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                    loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                    loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    ClickCount = Sanitizer.GetSafeHtmlFragment(ClickCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                }
                //model.PAGE_SIZE = 25;
                model.PageIndex = int.Parse(PageNumber);
                //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblies", null);
                //model = (mPanchayat)Helper.ExecuteService("Panchayat", "GetAllPanchayat", model);
                model = (mEmployee)Helper.ExecuteService("Employee", "GetAllEmployeeIDs", model);
                model.ResultCount = model.ResultCount;
                model.PageNumber = Convert.ToInt32(PageNumber);
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
                // model.RowsPerPage = (25);
                model.selectedPage = Convert.ToInt32(PageNumber);
                model.loopStart = Convert.ToInt32(loopStart);
                model.loopEnd = Convert.ToInt32(loopEnd);
                //var model = Assemblies.ToViewModel();
                if (TempLoop != "")
                {
                    model.loopStart = Convert.ToInt32(TempLoop);
                }
                else
                {
                    model.loopStart = Convert.ToInt32(loopStart);
                }
                if (ClickCount != "")
                {
                    model.ClickCount = Convert.ToInt32(ClickCount);
                }
                return PartialView("_PartialIndexOfEmployee", model);
                //return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Employee Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }







        //public ActionResult Index()
        //{
        //    var State = (List<mEmployee>)Helper.ExecuteService("Employee", "GetAllEmployee", null);
        //    var model = State.ToViewModel();
        //    return View(model);


        //}

        public ActionResult CreateEmployee()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }


                var Departments = (List<mDepartment>)Helper.ExecuteService("Employee", "GetAllDepartment", null);
                var model = new EmployeeViewModel()
                {
                    Mode = "Add",
                    Department = new SelectList(Departments, "deptId", "deptname"),
                    IsActive = true,
                };

                return View("CreateEmployee", model);


            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Employee Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveEmployee(EmployeeViewModel model, HttpPostedFileBase file)
        {


            if (file != null)
            {
                string DirPath = Server.MapPath("~/assets/SuperAdmin/Employee/");
                if (!Directory.Exists(DirPath))
                {
                    Directory.CreateDirectory(DirPath);
                }
                var fileName = Path.GetFileName(file.FileName);

                var path = Path.Combine(Server.MapPath("~/assets/SuperAdmin/Employee/"), fileName);
                file.SaveAs(path);

                ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
                imgCompress.GetImage = new System.Drawing.Bitmap(path);
                imgCompress.Height = 160;
                imgCompress.Width = 120;
                imgCompress.Save("compressingImage.jpg", DirPath);

                string newPath = Path.Combine(DirPath, "compressingImage.jpg");
                byte[] image = ReadImage(newPath);


                model.empphoto = image;
            }


            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Employee", "CreateEmployee", bill);
                }
                else
                {
                    Helper.ExecuteService("Employee", "UpdEmployee", bill);
                }
                return RedirectToAction("ShowAllEmployeeDetails");
            }
            else
            {
                return RedirectToAction("ShowAllEmployeeDetails");
            }

        }

        public ActionResult EditEmployee(string Id)
        {
            var Departments = (List<mDepartment>)Helper.ExecuteService("Employee", "GetAllDepartment", null);
            //mEmployee stateToEdit = (mEmployee)Helper.ExecuteService("Employee", "GetEmployeeById", new mEmployee { Id = Id });
            mEmployee stateToEdit = (mEmployee)Helper.ExecuteService("Employee", "GetEmployeeById", new mEmployee { Id = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1(Departments, "Edit");
            return View("CreateEmployee", model);
        }

        public ActionResult DeleteEmployee(int Id)
        {
            mEmployee stateToDelete = (mEmployee)Helper.ExecuteService("Employee", "GetEmployeeById", new mEmployee { Id = Id });
            Helper.ExecuteService("Employee", "DelEmployee", stateToDelete);
            return RedirectToAction("ShowAllEmployeeDetails");

        }


        private static byte[] ReadImage(string p_postedImageFileName)
        {
            try
            {
                FileStream fs = new FileStream(p_postedImageFileName, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                byte[] image = br.ReadBytes((int)fs.Length);
                br.Close();
                fs.Close();
                return image;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
