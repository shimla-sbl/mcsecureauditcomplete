﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using System.IO;
using SBL.DomainModel.Models.Enums;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using Microsoft.Security.Application;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class RulesDirectionsController : Controller
    {
        //
        // GET: /SuperAdmin/RulesDirections/

        public ActionResult Index()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var MobiSetting = (List<tRulesDirections>)Helper.ExecuteService("Mobiles", "GetAllRulesDirections", null);

                var model = MobiSetting.ToViewModel();

                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Rules Directions Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateRulesDirections()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var model = new RulesDirectionsViewModel()
                {
                    Mode = "Add"
                };

                model.TypeCol = new SelectList(Enum.GetValues(typeof(StatusType)).Cast<StatusType>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                return View("CreateRulesDirections", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Create Rules Directions Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost, ValidateInput(false)]
        public JsonResult SaveRulesDirections(HttpPostedFileBase file)
        {
            try
            {
                RulesDirectionsViewModel model = new RulesDirectionsViewModel();

                model.Title = Request.Form["Title"].ToString();

                model.Type = Request.Form["Type"].ToString();

                model.Mode = Request.Form["Mode"].ToString();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                model.PdfPath = Request.Form["PdfPath"].ToString();

                string pdfPath = string.Empty;

                if (file != null)
                {
                    UploadFile(file, file.FileName, model.Type, out pdfPath);

                    model.PdfPath = pdfPath.Replace(@"\", @"/");
                }

                var rulesDirections = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateRulesDirections", rulesDirections);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateRulesDirections", rulesDirections);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditRulesDirections(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                tRulesDirections tRuleDirectionsD = (tRulesDirections)Helper.ExecuteService("Mobiles", "GetRulesDirectionsBasedOnId", new tRulesDirections { SlNo = Id });

                var model = tRuleDirectionsD.ToViewModel1("Edit");
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSettingLocal", null);
                model.DownloadPath = FileSettings.SettingValue + @"/" + tRuleDirectionsD.PdfPath;
                model.TypeCol = new SelectList(Enum.GetValues(typeof(StatusType)).Cast<StatusType>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList(), "Value", "Text");

                return View("CreateRulesDirections", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Rules Directions Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult DeleteRulesDirections(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                tRulesDirections tRuleDirectionsD = (tRulesDirections)Helper.ExecuteService("Mobiles", "GetRulesDirectionsBasedOnId", new tRulesDirections { SlNo = Id });

                Helper.ExecuteService("Mobiles", "DeleteRulesDirections", tRuleDirectionsD);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Mobile Settings Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        private void UploadFile(HttpPostedFileBase File, string FileName, string type, out string dbFileName)
        {
            try
            {
                string savedFileName = string.Empty;

                dbFileName = string.Empty;

                if (File != null)
                {
                    var fileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                    string dirPath = string.Empty;

                    string fileNameWithoutExt = Path.GetFileNameWithoutExtension(File.FileName).Replace(".", "_").Replace("-", "_");

                    string fileName = GetTimestamp(DateTime.Now) + "_" + fileNameWithoutExt + Path.GetExtension(File.FileName);

                    if (type == "0")
                    {
                        dirPath = Path.Combine(fileSettings.SettingValue + "Rules");

                        savedFileName = fileName;

                        dbFileName = "Rules" + @"\" + fileName;
                    }
                    else if (type == "1")
                    {
                        dirPath = Path.Combine(fileSettings.SettingValue + "Directions");

                        savedFileName = fileName;

                        dbFileName = "Directions" + @"\" + fileName;
                    }
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }

                    dirPath = dirPath + @"\" + savedFileName;

                    File.SaveAs(dirPath);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public ActionResult Download(string filePathWithName, string type)
        {
            filePathWithName = Sanitizer.GetSafeHtmlFragment(filePathWithName);
            string filename = string.Empty;
            if (type == "0")
            {
                filename = @"C:\inetpub\e_Vidhan\FileStructure\Rules\" + System.IO.Path.GetFileName(filePathWithName);
            }
            else
            {
                filename = @"C:\inetpub\e_Vidhan\FileStructure\Directions\" + System.IO.Path.GetFileName(filePathWithName);
            }
            var fs = System.IO.File.OpenRead(filename);
            return File(fs, "appliaction/zip", filePathWithName.Substring(filePathWithName.LastIndexOf("/") + 1));
        }
    }
}
