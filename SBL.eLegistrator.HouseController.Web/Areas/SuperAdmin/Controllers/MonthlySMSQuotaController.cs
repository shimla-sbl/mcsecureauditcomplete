﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.MemberSMSQuota;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Member;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MonthlySMSQuotaController : Controller
    {
        //
        // GET: /SuperAdmin/MonthlySMSQuota/

        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var SMSQuota = (List<mMemberSMSQuotaAddOn>)Helper.ExecuteService("MemberSMSQuota", "GetAllMemberSMSQuotaList", null);
                var model = SMSQuota.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Member SMS Quota Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult CreateMemberSMSQuota()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
                var Members = (List<mMember>)Helper.ExecuteService("MemberSMSQuota", "GetAllMembers1", null);
                
                var model = new MemberSMSQuotaViewModel()
                {

                    Mode = "Add",
                    MemberNames = new SelectList(Members, "MemberCode", "Name"),
                    //Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    IsActive = true,
                    IsDeleted = false


                };
                return View("CreateMemberSMSQuota", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Member SMS Quota Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMemberSMSQuota(MemberSMSQuotaViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var MemberSMSQuota = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {

                        //ministry.IsActive = true;
                        Helper.ExecuteService("MemberSMSQuota", "CreateMemberSMSQuota", MemberSMSQuota);
                    }
                    else
                    {


                        //ministry.IsActive = true;
                        Helper.ExecuteService("MemberSMSQuota", "UpdateMemberSMSQuota", MemberSMSQuota);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Member SMS Quota Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult EditMemberSMSQuota(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
                var Members = (List<mMember>)Helper.ExecuteService("MemberSMSQuota", "GetAllMembers1", null);
                //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                //mMemberSMSQuotaAddOn MemberSMSQuotaToEdit = (mMemberSMSQuotaAddOn)Helper.ExecuteService("MemberSMSQuota", "GetMemberSMSQuotaById", new mMemberSMSQuotaAddOn { Id = Id });
                mMemberSMSQuotaAddOn MemberSMSQuotaToEdit = (mMemberSMSQuotaAddOn)Helper.ExecuteService("MemberSMSQuota", "GetMemberSMSQuotaById", new mMemberSMSQuotaAddOn { Id = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = MemberSMSQuotaToEdit.ToViewModel1(Members, "Edit");
                return View("CreateMemberSMSQuota", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Member SMS Quota Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult DeleteMemberSMSQuota(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMemberSMSQuotaAddOn MemberSMSQuotaToDelete = (mMemberSMSQuotaAddOn)Helper.ExecuteService("MemberSMSQuota", "GetMemberSMSQuotaById", new mMemberSMSQuotaAddOn { Id = Id });
                Helper.ExecuteService("MemberSMSQuota", "DeleteMemberSMSQuota", MemberSMSQuotaToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Member SMS Quota Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        //public JsonResult CheckMinistryChildExist(int MinistryID)
        //{
        //    try
        //    {
        //        var isExist = (Boolean)Helper.ExecuteService("Ministry", "IsMinistryIdChildExist", new mMinistry { MinistryID = MinistryID });

        //        return Json(isExist, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }

        //}


    }
}
