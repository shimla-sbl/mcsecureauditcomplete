﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Resources;
using System.Collections;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models;
using SBL.DomainModel.Models.News;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Resourcefile;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Windows;


namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class ResourceController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UpdatePublicResourcePublic()
        {

            List<SelectListItem> items1 = new List<SelectListItem>();
            string resourcespath = "http://secure.shimlamc.org/" + "App_GlobalResources";


            DirectoryInfo dirInfo = new DirectoryInfo(resourcespath);
            foreach (FileInfo filInfo in dirInfo.GetFiles())
            {
                string filename = filInfo.Name;
                string currentstate_Culturecode = "";
                if (CurrentSession.LanguageCulture == "")
                {
                    currentstate_Culturecode = CurrentSession.LanguageCulture;
                    currentstate_Culturecode = "AdminDashboard.hi-IN." + currentstate_Culturecode + ".resx";
                }

                //if (filename == currentstate_Culturecode)
                //{
                items1.Add(new SelectListItem { Text = filename, Value = filename });
                //}

            }

            ViewBag.Items1 = items1;

            return View();

        }

        public ActionResult UpdatePublicResource()
        {
            
                List<SelectListItem> items1 = new List<SelectListItem>();
                string resourcespath = Request.PhysicalApplicationPath + "App_GlobalResources";


                DirectoryInfo dirInfo = new DirectoryInfo(resourcespath);
                foreach (FileInfo filInfo in dirInfo.GetFiles())
                {
                    string filename = filInfo.Name;
                    string currentstate_Culturecode = "";
                    if (CurrentSession.LanguageCulture == "")
                    {
                        currentstate_Culturecode = CurrentSession.LanguageCulture;
                        currentstate_Culturecode = "AdminDashboard.hi-IN." + currentstate_Culturecode + ".resx";
                    }

                    //if (filename == currentstate_Culturecode)
                    //{
                        items1.Add(new SelectListItem { Text = filename, Value = filename });
                    //}
                
                }
             
                ViewBag.Items1 = items1;
             
                return View();
           
        }
        public ActionResult getresourcesData(string ResourceFilename)
        {
            List<SelectListItem> list1 = new List<SelectListItem>();
            Resourcefile mdl = new Resourcefile();
            string filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + ResourceFilename;
            Stream stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);

            ResXResourceReader resxReader = new ResXResourceReader();

            System.Resources.ResXResourceReader RrX = new System.Resources.ResXResourceReader(stream);
            IDictionaryEnumerator RrEn = RrX.GetEnumerator();

            SortedList slist = new SortedList();
            while (RrEn.MoveNext())
            {
                list1.Add(new SelectListItem { Text = RrEn.Key.ToString(), Value = RrEn.Value.ToString() });
            }
            //RrX.Close();
            stream.Dispose();
            mdl.ResourceList = list1;
            mdl.ResourceFileName = ResourceFilename;
            return PartialView("_Resourcesfile", mdl);
        }
        public JsonResult UpdateResourcesValue(string ResourcesValue, string Resfilename, int Id)
        {
            string filename = Resfilename;
            int id = Id;//Convert.ToInt32(Request.QueryString["id"]);
           
            filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + filename;
            // filename = Server.MapPath("~/App_GlobalResources\\" + filename + "");
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            xmlDoc.Load(filename);
            System.Xml.XmlNodeList nlist = xmlDoc.GetElementsByTagName("data");
            System.Xml.XmlNode childnode = nlist.Item(id);
            childnode.Attributes["xml:space"].Value = "default";
            xmlDoc.Save(filename);
            System.Xml.XmlNode lastnode = childnode.SelectSingleNode("value");
            lastnode.InnerText = ResourcesValue;
            xmlDoc.Save(filename);
            return Json(Resfilename, JsonRequestBehavior.AllowGet);
         

        }


    }
}
