﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Secretory;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SecretoryDepartmentController : Controller
    {
        //
        // GET: /SuperAdmin/SecretoryDepartment/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var SecDept = (List<mSecretoryDepartment>)Helper.ExecuteService("SecretoryDepartment", "GetAllSecretoryDepartment", null);
                var model = SecDept.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Secretory Department Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateSecretoryDepartment()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var Secretories = (List<mSecretory>)Helper.ExecuteService("SecretoryDepartment", "GetAllSecretories", null);
                var Departments = (List<mDepartment>)Helper.ExecuteService("SecretoryDepartment", "GetAllDepartments", null);
                var model = new SecretoryDepartmentViewModel()
                {
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    SecretoryNames = new SelectList(Secretories, "SecretoryID", "SecretoryName"),
                    Department = new SelectList(Departments, "deptId", "deptname"),
                    Mode = "Add",
                    IsActive = true
                };
                return View("CreateSecretoryDepartment", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Secretory Department Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSecretoryDepartment(SecretoryDepartmentViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var SecDept = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("SecretoryDepartment", "CreateSecretoryDepartment", SecDept);

                    }
                    else
                    {
                        Helper.ExecuteService("SecretoryDepartment", "UpdateSecretoryDepartment", SecDept);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Secretory Department Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult EditSecretoryDepartment(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Secretories = (List<mSecretory>)Helper.ExecuteService("SecretoryDepartment", "GetAllSecretories", null);
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var Departments = (List<mDepartment>)Helper.ExecuteService("SecretoryDepartment", "GetAllDepartments", null);
                //mSecretoryDepartment secDeptToEdit = (mSecretoryDepartment)Helper.ExecuteService("SecretoryDepartment", "GetSecDeptBasedOnId", new mSecretoryDepartment { SecretoryDepartmentID = Id });
                mSecretoryDepartment secDeptToEdit = (mSecretoryDepartment)Helper.ExecuteService("SecretoryDepartment", "GetSecDeptBasedOnId", new mSecretoryDepartment { SecretoryDepartmentID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = secDeptToEdit.ToViewModel1(Secretories, Assemblies, Departments, "Edit");
                return View("CreateSecretoryDepartment", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Secretory Department Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult DeleteSecretoryDepartment(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mSecretoryDepartment secDeptToDelete = (mSecretoryDepartment)Helper.ExecuteService("SecretoryDepartment", "GetSecDeptBasedOnId", new mSecretoryDepartment { SecretoryDepartmentID = Id });
                Helper.ExecuteService("SecretoryDepartment", "DeleteSecretoryDepartment", secDeptToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Secretory Department Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }


    }
}
