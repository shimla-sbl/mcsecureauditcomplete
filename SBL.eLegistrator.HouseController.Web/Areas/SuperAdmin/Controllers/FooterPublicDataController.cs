﻿using SBL.DomainModel.Models.FooterPublicData;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class FooterPublicDataController : Controller
    {
        //
        // GET: /SuperAdmin/FooterPublicData/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var FooterPublicData = (List<FooterPublicData>)Helper.ExecuteService("FooterPublicDatas", "GetAllFooterPublicData", null);
                var model = FooterPublicData.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting FooterPublicData Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateFooterPublicData()
        {
            var model = new FooterPublicDataViewModel()
            {
                Mode = "Add"

            };

            return View("CreateFooterPublicData", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveFooterPublicData(FooterPublicDataViewModel model)
        {

            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("FooterPublicDatas", "CreateFooterPublicData", bill);
                }
                else
                {
                    Helper.ExecuteService("FooterPublicDatas", "UpdateFooterPublicData", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditFooterPublicData(int Id)
        {

            FooterPublicData FooterPublicDataToEdit = (FooterPublicData)Helper.ExecuteService("FooterPublicDatas", "GetFooterPublicDataById", new FooterPublicData { ID = Id });
            var model = FooterPublicDataToEdit.ToViewModel1("Edit");
            return View("CreateFooterPublicData", model);
        }

        public ActionResult DeleteFooterPublicData(int Id)
        {
            FooterPublicData FooterPublicDataToDelete = (FooterPublicData)Helper.ExecuteService("FooterPublicDatas", "GetFooterPublicDataById", new FooterPublicData { ID = Id });
            Helper.ExecuteService("FooterPublicData", "DeleteFooterPublicData", FooterPublicDataToDelete);
            return RedirectToAction("Index");

        }

        

    }
}
