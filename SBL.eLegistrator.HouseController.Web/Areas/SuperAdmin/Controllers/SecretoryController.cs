﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.Secretory;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Enums;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SecretoryController : Controller
    {
        //
        // GET: /SuperAdmin/Secretory/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecretoryIDs", null);
                //var State = (List<mSecretory>)Helper.ExecuteService("Secretory", "GetAllSecretory", null);
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Secretory Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateSecretory()
        {

            var Departments = (List<mDepartment>)Helper.ExecuteService("Secretory", "GetAllDepartment", null);

            //var model = new SecretoryViewModel()
            //{
            //    Mode = "Add",
            //    Department = new SelectList(Departments, "deptId", "deptname"),
            //    IsActive=true
            //};


            SecretoryViewModel model = new SecretoryViewModel();
            IEnumerable<Prefix> actionTypes = Enum.GetValues(typeof(Prefix))
                                                       .Cast<Prefix>();
            model.ActionsList = from action in actionTypes
                                select new SelectListItem
                                {
                                    Text = action.ToString(),
                                    Value = action.ToString()
                                };

            model.Mode = "Add";
            model.Department = new SelectList(Departments, "deptId", "deptname");
            model.IsActive = true;



            return View("CreateSecretory", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSecretory(SecretoryViewModel model)
        {

            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Secretory", "CreateSecretory", bill);
                }
                else
                {
                    Helper.ExecuteService("Secretory", "UpdateSecretory", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditSecretory(string Id)
        {
            var Departments = (List<mDepartment>)Helper.ExecuteService("Secretory", "GetAllDepartment", null);
            //mSecretory stateToEdit = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretoryById", new mSecretory { SecretoryID = Id });
            mSecretory stateToEdit = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretoryById", new mSecretory { SecretoryID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1(Departments, "Edit");
            IEnumerable<Prefix> actionTypes = Enum.GetValues(typeof(Prefix))
                                                       .Cast<Prefix>();
            model.ActionsList = from action in actionTypes
                                select new SelectListItem
                                {
                                    Text = action.ToString(),
                                    Value = action.ToString()
                                };
            return View("CreateSecretory", model);
        }

        public ActionResult DeleteSecretory(int Id)
        {
            mSecretory stateToDelete = (mSecretory)Helper.ExecuteService("Secretory", "GetSecretoryById", new mSecretory { SecretoryID = Id });
            Helper.ExecuteService("Secretory", "DeleteSecretory", stateToDelete);
            return RedirectToAction("Index");

        }




        public JsonResult CheckSecNameChildExist(int SecID)
        {
            var isExist = (Boolean)Helper.ExecuteService("Secretory", "IsSecIdChildExist", new mSecretory { SecretoryID = SecID });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }

    }
}
