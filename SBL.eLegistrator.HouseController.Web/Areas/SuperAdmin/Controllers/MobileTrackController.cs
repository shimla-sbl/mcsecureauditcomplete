﻿using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MobileTrackController : Controller
    {
        //
        // GET: /SuperAdmin/MobileTrack/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                var mobitrack = (List<mMobileTrack>)Helper.ExecuteService("Mobiles", "GetAllMobileTrack", null);
                var model = mobitrack.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is an Error While Getting Mobile Track Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateMobileTrack()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var tmpStateCodesList = (List<mTelecomCircles>)Helper.ExecuteService("Mobiles", "GetAllStateCodes", null);
                List<SelectListItem> stateCodes = new List<SelectListItem>();
                if (tmpStateCodesList != null)
                {
                    foreach (var val in tmpStateCodesList)
                    {
                        stateCodes.Add(new SelectListItem { Text = string.Concat(
                        val.StateCode,"(",val.Description,")"), Value = val.StateCode});
                    }
                }
                var tmpNetworkCodes = (List<mNetworkOperators>)Helper.ExecuteService("Mobiles", "GetAllNetworkCodes", null);
                List<SelectListItem> networkCodes = new List<SelectListItem>();
                if (tmpNetworkCodes != null)
                {
                    foreach (var val in tmpNetworkCodes)
                    {
                        networkCodes.Add(new SelectListItem
                        {
                            Text = string.Concat(
                                val.NetworkCode, "(", val.Description, ")"),
                            Value = val.NetworkCode
                        });
                    }
                }
                var model = new MobileTrackViewModel()
                {
                    Mode = "Add",
                    StateCodes = new SelectList(stateCodes,"Value","Text"),
                    NetWorkCodes = new SelectList(networkCodes, "Value", "Text"),
                };
                return View("CreateMobileTrack", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "An Error Occured While Creating Mobile Track Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SaveMobileTrack()
        {
            try
            {
                MobileTrackViewModel model = new MobileTrackViewModel();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                model.NetworkCode = Request.Form["NetworkCode"].ToString();

                model.StateCode = Request.Form["StateCode"].ToString();

                model.ZoneCode = Request.Form["ZoneCode"].ToString();

                model.Mode = Request.Form["Mode"].ToString();

                var netOpe = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateMobileTrack", netOpe);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateMobileTrack", netOpe);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditMobileTrack(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                
                var tmpStateCodesList = (List<mTelecomCircles>)Helper.ExecuteService("Mobiles", "GetAllStateCodes", null);
                List<SelectListItem> stateCodes = new List<SelectListItem>();
                if (tmpStateCodesList != null)
                {
                    foreach (var val in tmpStateCodesList)
                    {
                        stateCodes.Add(new SelectListItem
                        {
                            Text = string.Concat(
                                val.StateCode, "(", val.Description, ")"),
                            Value = val.StateCode
                        });
                    }
                }
                var tmpNetworkCodes = (List<mNetworkOperators>)Helper.ExecuteService("Mobiles", "GetAllNetworkCodes", null);
                List<SelectListItem> networkCodes = new List<SelectListItem>();
                if (tmpNetworkCodes != null)
                {
                    foreach (var val in tmpNetworkCodes)
                    {
                        networkCodes.Add(new SelectListItem
                        {
                            Text = string.Concat(
                                val.NetworkCode, "(", val.Description, ")"),
                            Value = val.NetworkCode
                        });
                    }
                }
              
                mMobileTrack netOpeToEdit = (mMobileTrack)Helper.ExecuteService("Mobiles", "GetMobileTrackBasedOnId", new mMobileTrack { SlNo = Id });
                var model = netOpeToEdit.ToViewModel1(stateCodes,networkCodes,"Edit");
                return View("CreateMobileTrack", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "An Error Occured While Editing Mobile Track Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteMobileTrack(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMobileTrack netOpeToRemove = (mMobileTrack)Helper.ExecuteService("Mobiles", "GetMobileTrackBasedOnId", new mMobileTrack { SlNo = Id });
                Helper.ExecuteService("Mobiles", "DeleteMobileTrack", netOpeToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "An Error Occured While Deleting Mobile Track Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

    }
}
