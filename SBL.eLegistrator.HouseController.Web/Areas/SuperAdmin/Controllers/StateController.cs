﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.States;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class StateController : Controller
    {
        //
        // GET: /SuperAdmin/State/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
            var State = (List<mStates>)Helper.ExecuteService("State", "GetAllState", null);
            var model = State.ToViewModel();
            return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting State Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateState()
        {
            var model = new StateViewModel()
            {
                Mode = "Add",
                IsActive=true

            };

            return View("CreateState", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveState(StateViewModel model)
        {

            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("State", "CreateState", bill);
                }
                else
                {
                    Helper.ExecuteService("State", "UpdateState", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditState(string Id)
        {

            //mStates stateToEdit = (mStates)Helper.ExecuteService("State", "GetStateById", new mStates { mStateID = Id });
            mStates stateToEdit = (mStates)Helper.ExecuteService("State", "GetStateById", new mStates { mStateID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1("Edit");
            return View("CreateState", model);
        }

        public ActionResult DeleteState(int Id)
        {
            mStates stateToDelete = (mStates)Helper.ExecuteService("State", "GetStateById", new mStates { mStateID = Id });
            Helper.ExecuteService("State", "DeleteState", stateToDelete);
            return RedirectToAction("Index");

        }

        public JsonResult CheckStateChildExist(int StateID)
        {
            var isExist = (Boolean)Helper.ExecuteService("State", "IsStateIdChildExist", new mStates { mStateID = StateID });

            return Json(isExist, JsonRequestBehavior.AllowGet);
        }


    }
}
