﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Member;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class SmsAndEmailController : Controller
    {
        //
        // GET: /SuperAdmin/SmsAndEmail/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult sentEmailList()
        {

            return View();
        }
        public ActionResult sentSMSList()
        {

            return View();
        }
        public ActionResult FillModuleList()
        {
            List<ModuleList> list = (List<ModuleList>)Helper.ExecuteService("SmsAndEmailReport", "getAllModuleList", null);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult FillModuleUserList(string moduleID)
        {
            List<ModuleUserList> list = (List<ModuleUserList>)Helper.ExecuteService("SmsAndEmailReport", "getModuleUserList", moduleID);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult EmailResult(string from, string to, string ModuleID, string UserID)
        {
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            DateTime date1 = new DateTime(Convert.ToInt32(from.Split('/')[2]), Convert.ToInt32(from.Split('/')[0]), Convert.ToInt32(from.Split('/')[1]));
            DateTime date2 = new DateTime(Convert.ToInt32(to.Split('/')[2]), Convert.ToInt32(to.Split('/')[0]), Convert.ToInt32(to.Split('/')[1]));
            methodParameter.Add(new KeyValuePair<string, string>("@from", date1.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@to", date2.ToString()));
            if (ModuleID == "0")
            {
                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbEMAIL", "SelectMSSql", "getAllEmail", methodParameter);
            }
            else
            {
                if (UserID == "0")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", ModuleID));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbEMAIL", "SelectMSSql", "getAllEmailByModuleID", methodParameter);
                }
                else
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", ModuleID));
                    methodParameter.Add(new KeyValuePair<string, string>("@UserID", UserID));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbEMAIL", "SelectMSSql", "getAllEmailByUserID", methodParameter);
                }
            }



            List<SMSEmailReport> reportList = new List<SMSEmailReport>();
            DataTable dt = dataSet.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SMSEmailReport report = new SMSEmailReport();
                int c = dt.Rows[i][0].ToString().IndexOf("sent to");
                string str = dt.Rows[i][0].ToString().Substring(c + 8, dt.Rows[i][0].ToString().Length - (c + 8));
                string[] arr = str.Split(',');
                List<string> lst = new List<string>();
                lst.AddRange(arr);
                report.count = arr.Length;
                report.subject = dt.Rows[i][2].ToString();
                report.Body = dt.Rows[i][3].ToString();
                report.timeSpan = Convert.ToDateTime(dt.Rows[i][1].ToString());
                report.MailorMobile = lst;
                reportList.Add(report);

            }
            return PartialView("_reportEmailList", reportList);
        }
        public ActionResult SMSResult(string from, string to, string ModuleID, string UserID)
        {
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();
            DateTime date1 = new DateTime(Convert.ToInt32(from.Split('/')[2]), Convert.ToInt32(from.Split('/')[0]), Convert.ToInt32(from.Split('/')[1]));
            DateTime date2 = new DateTime(Convert.ToInt32(to.Split('/')[2]), Convert.ToInt32(to.Split('/')[0]), Convert.ToInt32(to.Split('/')[1]));
            methodParameter.Add(new KeyValuePair<string, string>("@from", date1.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@to", date2.ToString()));
            if (ModuleID == "0")
            {
                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllSMS", methodParameter);
            }
            else
            {
                if (UserID == "0")
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", ModuleID));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllSMSByModuleID", methodParameter);
                }
                else
                {
                    methodParameter.Add(new KeyValuePair<string, string>("@ModuleID", ModuleID));
                    methodParameter.Add(new KeyValuePair<string, string>("@UserID", UserID));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "getAllSMSByUserID", methodParameter);
                }
            }



            List<SMSEmailReport> reportList = new List<SMSEmailReport>();
            DataTable dt = dataSet.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SMSEmailReport report = new SMSEmailReport();


                report.subject = dt.Rows[i][1].ToString();
                report.Body = dt.Rows[i][2].ToString();
                report.timeSpan = Convert.ToDateTime(dt.Rows[i][0].ToString());
                report.status = dt.Rows[i][3].ToString();
                reportList.Add(report);

            }
            ViewBag.MSG = "Total SMS sent :" + dt.Rows.Count;
            return PartialView("_reportSMSList", reportList);
        }

    }
}
