﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SiteSettingsController : Controller
    {
        //
        // GET: /SuperAdmin/SiteSettings/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var sessionType = (List<SiteSettings>)Helper.ExecuteService("SiteSetting", "GetAllSiteSettingsList", null);
                var model = sessionType.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Site Setting Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateSiteSettings()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new SiteSettingViewModel()
           {
               Mode = "Add",
               IsActive = true
           };
                return View("CreateSiteSettings", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Site Setting Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSiteSettings(SiteSettingViewModel model)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var SiteSetting = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {

                        Helper.ExecuteService("SiteSetting", "CreateSiteSettings", SiteSetting);
                    }
                    else
                    {

                        Helper.ExecuteService("SiteSetting", "UpdateSiteSettings", SiteSetting);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Site Setting Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult EditSiteSettings(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //SiteSettings siteToEdit = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetSiteSettingsById", new SiteSettings { SettingId = Id });
                SiteSettings siteToEdit = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetSiteSettingsById", new SiteSettings { SettingId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = siteToEdit.ToViewModel1("Edit");
                return View("CreateSiteSettings", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Site Setting Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult DeleteSiteSettings(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                SiteSettings siteToDelete = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetSiteSettingsById", new SiteSettings { SettingId = Id });
                Helper.ExecuteService("SiteSetting", "DeleteSiteSettings", siteToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Site Setting Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }






    }
}
