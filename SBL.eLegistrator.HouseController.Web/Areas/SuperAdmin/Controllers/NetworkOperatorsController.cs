﻿using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class NetworkOperatorsController : Controller
    {
        //
        // GET: /SuperAdmin/NetworkOperators/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                var networkope = (List<mNetworkOperators>)Helper.ExecuteService("Mobiles", "GetAllNetworkOperators", null);
                var model = networkope.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Network Operators Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateNetworkOperators()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new NetworkOperatorsViewModel()
                {
                    Mode = "Add"

                };
                return View("CreateNetworkOperators", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Network Operators Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SaveNetworkOperators()
        {
            try
            {
                NetworkOperatorsViewModel model = new NetworkOperatorsViewModel();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                model.NetworkCode = Request.Form["NetworkCode"].ToString();

                model.Description = Request.Form["Description"].ToString();

                model.Mode = Request.Form["Mode"].ToString();

                var netOpe = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateNetworkOperators", netOpe);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateNetworkOperators", netOpe);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditNetworkOperators(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mNetworkOperators netOpeToEdit = (mNetworkOperators)Helper.ExecuteService("Mobiles", "GetNetworkOperatorsBasedOnId", new mNetworkOperators { SlNo = Id });
                var model = netOpeToEdit.ToViewModel1("Edit");
                return View("CreateNetworkOperators", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Network Operators Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteNetworkOperators(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mNetworkOperators netOpeToRemove = (mNetworkOperators)Helper.ExecuteService("Mobiles", "GetNetworkOperatorsBasedOnId", new mNetworkOperators { SlNo = Id });
                Helper.ExecuteService("Mobiles", "DeleteNetworkOperators", netOpeToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Network Operators Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }


    }
}
