﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SessionTypeController : Controller
    {
        //
        // GET: /SuperAdmin/SessionType/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var sessionType = (List<mSessionType>)Helper.ExecuteService("SessionType", "GetAllSessionType", null);
                var model = sessionType.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Session Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }
        public ActionResult CreateSessionType()
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new SessionTypeViewModel()
            {
                Mode = "Add",
                IsActive = true
            };
                return View("CreateSessionType", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Session Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSessionType(SessionTypeViewModel model)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var Sessiontype = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        //Sessiontype.IsActive = true;
                        Helper.ExecuteService("SessionType", "CreateSessionType", Sessiontype);
                    }
                    else
                    {
                        //Sessiontype.IsActive = true;
                        Helper.ExecuteService("SessionType", "UpdateSessionType", Sessiontype);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Session Type Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult EditSessionType(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //mSessionType TypeToEdit = (mSessionType)Helper.ExecuteService("SessionType", "GetSessionTypeById", new mSessionType { TypeID = Id });
                mSessionType TypeToEdit = (mSessionType)Helper.ExecuteService("SessionType", "GetSessionTypeById", new mSessionType { TypeID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                
                var model = TypeToEdit.ToViewModel1("Edit");
                return View("CreateSessionType", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Session Type Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult DeleteSessionType(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mSessionType TypeToDelete = (mSessionType)Helper.ExecuteService("SessionType", "GetSessionTypeById", new mSessionType { TypeID = Id });
                Helper.ExecuteService("SessionType", "DeleteSessionType", TypeToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Session Type Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public JsonResult CheckSessionTypeChildExist(int SesTypeID)
        {
            try
            {
                var isExist = (Boolean)Helper.ExecuteService("SessionType", "IsSessionTypeIdChildExist", new mSessionType { TypeID = SesTypeID });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



    }
}
