﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;


namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class TelecomCirclesController : Controller
    {
        //
        // GET: /SuperAdmin/TelecomCircles/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });

                }
                var telecircles = (List<mTelecomCircles>)Helper.ExecuteService("Mobiles", "GetAllTelecomCircles", null);
                var model = telecircles.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Telecom Circles Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult CreateTelecomCircles()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new TelecomCirclesViewModel()
                {
                    Mode = "Add"

                };
                return View("CreateTelecomCircles", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Telecom Circles Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult SaveTelecomCircles()
        {
            try
            {
                TelecomCirclesViewModel model = new TelecomCirclesViewModel();

                model.SlNo = Convert.ToInt32(Request.Form["SlNo"].ToString());

                model.StateCode = Request.Form["StateCode"].ToString();

                model.Description = Request.Form["Description"].ToString();

                model.Mode = Request.Form["Mode"].ToString();
                
                var teleCircle = model.ToDomainModel();

                if (model.Mode == "Add")
                {
                    Helper.ExecuteService("Mobiles", "CreateTelecomCircles", teleCircle);
                }
                else
                {
                    Helper.ExecuteService("Mobiles", "UpdateTelecomCircles", teleCircle);
                }

                return Json("Success", JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                return Json("Failed", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EditTelecomCircles(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mTelecomCircles teleToEdit = (mTelecomCircles)Helper.ExecuteService("Mobiles", "GetTelecomCirclesBasedOnId", new mTelecomCircles { SlNo = Id });
                var model = teleToEdit.ToViewModel1("Edit");
                return View("CreateTelecomCircles", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Telecom Circles Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public ActionResult DeleteTelecomCircles(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mTelecomCircles teleToRemove = (mTelecomCircles)Helper.ExecuteService("Mobiles", "GetTelecomCirclesBasedOnId", new mTelecomCircles { SlNo = Id });
                Helper.ExecuteService("Mobiles", "DeleteTelecomCircles", teleToRemove);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Telecom Circles Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }


    }
}
