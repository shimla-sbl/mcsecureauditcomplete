﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Event;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SubEventController : Controller
    {
        //
        // GET: /SuperAdmin/SubEvent/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mSubEvents>)Helper.ExecuteService("SubEvent", "GetAllSubEvent", null);
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Sub Event Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateSubEvent()
        {
            var model = new SubEventViewModel()
            {
                Mode = "Add",
                IsActive = true,
                IsLOB = true,
                IsProceeding = true,
                IsDepartment = true,
                IsCommittee = true,
                IsVS = true,
                IsOtherNotices = true,
                IsQA = true,

            };

            return View("CreateSubEvent", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSubEvent(SubEventViewModel model)
        {

            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("SubEvent", "CreateSubEvent", bill);
                }
                else
                {
                    Helper.ExecuteService("SubEvent", "UpdateSubEvent", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditSubEvent(string Id)
        {

            //mStates stateToEdit = (mStates)Helper.ExecuteService("State", "GetStateById", new mStates { mStateID = Id });
            mSubEvents stateToEdit = (mSubEvents)Helper.ExecuteService("SubEvent", "GetSubEventById", new mSubEvents { SubEventId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = stateToEdit.ToViewModel1("Edit");
            return View("CreateSubEvent", model);
        }

        public ActionResult DeleteSubEvent(int Id)
        {
            mSubEvents stateToDelete = (mSubEvents)Helper.ExecuteService("SubEvent", "GetSubEventById", new mSubEvents { SubEventId = Id });
            Helper.ExecuteService("SubEvent", "DeleteSubEvent", stateToDelete);
            return RedirectToAction("Index");

        }

    }
}
