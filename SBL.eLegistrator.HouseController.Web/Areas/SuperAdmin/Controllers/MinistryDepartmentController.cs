﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Diaries;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MinistryDepartmentController : Controller
    {
        //
        // GET: /SuperAdmin/MinistryDepartment/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Ministry = (List<mMinistryDepartment>)Helper.ExecuteService("Ministry", "GetAllMinistry1Department", null);
                var model = Ministry.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Ministry Department Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateMinistryDepartment()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var Dept = (List<mDepartment>)Helper.ExecuteService("Designation", "GetAllDepartments", null);
                var Ministry = (List<mMinistry>)Helper.ExecuteService("Ministry", "GetAllMinistryNull", null);


                List<SelectListItem> items = new List<SelectListItem>();
                foreach (var item in Ministry)
                {
                    SelectListItem s = new SelectListItem();
                    s.Text = item.MinistryName + " " + "(" + item.MinisterName + ")";
                    s.Value = item.MinistryID.ToString();

                    items.Add(s);
                }
                //ViewBag.Ministry = items.OrderBy(a => a.Text);


                var model = new MinistryDepartmentViewModel()
                {
                    Mode = "Add",
                    //Ministry = new SelectList(Ministry, "MinistryID", "MinistryName"),
                    Ministry = new SelectList(items, "Value", "Text"),
                    Department = new SelectList(Dept, "deptId", "deptname"),
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    IsActive = true
                };
                return View("CreateMinistryDepartment", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Ministry Department Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public JsonResult GetMinistryAId(int Assemblyid)
        {
            DiaryModel DMdl = new DiaryModel();
            DMdl.AssemblyID = Assemblyid;
            DMdl.MinistryList = (List<DiaryModel>)Helper.ExecuteService("Member", "GetMinistryByAssemblyDiary", DMdl);
            return Json(DMdl.MinistryList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMinistryDepartment(MinistryDepartmentViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var ministrydept = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        //ministrydept.IsActive = true;
                        Helper.ExecuteService("Ministry", "CreateMinistryDepartment", ministrydept);
                    }
                    else
                    {
                        // ministrydept.IsActive = true;
                        Helper.ExecuteService("Ministry", "UpdateMinistryDepartment", ministrydept);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Ministry Department Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult EditMinistryDepartment(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var Dept = (List<mDepartment>)Helper.ExecuteService("Designation", "GetAllDepartments", null);
                var Ministry = (List<mMinistry>)Helper.ExecuteService("Ministry", "GetAllMinistry1", null);


                




                //mMinistryDepartment ministrydeptToEdit = (mMinistryDepartment)Helper.ExecuteService("Ministry", "GetMinistryByIdDepartment", new mMinistryDepartment { MinistryDepartmentsID = Id });
                mMinistryDepartment ministrydeptToEdit = (mMinistryDepartment)Helper.ExecuteService("Ministry", "GetMinistryByIdDepartment", new mMinistryDepartment { MinistryDepartmentsID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                //var model = ministrydeptToEdit.ToViewModel1(Dept, Ministry, Assemblies, "Edit");
                var model = ministrydeptToEdit.ToViewModel1(Dept, Ministry, Assemblies, "Edit");
                return View("CreateMinistryDepartment", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Ministry Department Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult DeleteMinistryDepartment(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMinistryDepartment ministrydeptToDelete = (mMinistryDepartment)Helper.ExecuteService("Ministry", "GetMinistryByIdDepartment", new mMinistryDepartment { MinistryDepartmentsID = Id });
                Helper.ExecuteService("Ministry", "DeleteMinistryDepartment", ministrydeptToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Ministry Department Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult DeleteSelectedMinistryDept(string arr)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if(!string.IsNullOrEmpty(arr))
                {
                string[] deptIds=arr.Split(',');
                foreach (var item in deptIds)
                {
                    int Id = Convert.ToInt32(item);
                    mMinistryDepartment ministrydeptToDelete = (mMinistryDepartment)Helper.ExecuteService("Ministry", "GetMinistryByIdDepartment", new mMinistryDepartment { MinistryDepartmentsID = Id });
                    Helper.ExecuteService("Ministry", "DeleteMinistryDepartment", ministrydeptToDelete);
                }
                return Json("deleted");
                }
                return Json("null");
                
                //return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Ministry Department Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

    }
}
