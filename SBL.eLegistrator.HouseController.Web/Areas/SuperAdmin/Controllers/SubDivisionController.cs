﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.SubDivision;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegistrator.HouseController.Web.Models;
#pragma warning disable CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace
using Microsoft.Security.Application;
#pragma warning restore CS0105 // The using directive for 'Microsoft.Security.Application' appeared previously in this namespace

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SubDivisionController : Controller
    {
        //
        // GET: /SuperAdmin/SubDivision/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var SubDivision = (List<mSubDivision>)Helper.ExecuteService("SubDivision", "GetAllSubDivision", null);
                var model = SubDivision.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Highest Qualification Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }



        public ActionResult CreateSubDivision()
        {
            try
            {
                //SubDivisionViewModel model = new SubDivisionViewModel();
                mSubDivision model1 = new mSubDivision();
                model1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var Contituency = (List<mConstituency>)Helper.ExecuteService("SubDivision", "GetAllSubDivision", null);
                var Contituency = (List<mConstituency>)Helper.ExecuteService("SubDivision", "GetConsByAid", model1);
                //var Constituencys = (List<mConstituency>)Helper.ExecuteService("SubDivision", "GetAllConstituency", null);
                var model = new SubDivisionViewModel()
                {
                    SubDivision = new SelectList(Contituency, "ConstituencyID", "ConstituencyName"),

                    Mode = "Add",


                };
                return View("CreateSubDivision", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Creating Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSubDivision(SubDivisionViewModel model)
        {

            if (ModelState.IsValid)
            {
                //mSubDivision model1 = new mSubDivision();
                //model1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                var SubDivision = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("SubDivision", "CreateSubDivision", SubDivision);
                }
                else
                {
                    Helper.ExecuteService("SubDivision", "UpdateSubDivision", SubDivision);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        public ActionResult EditSubDivision(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Constituencys = (List<mConstituency>)Helper.ExecuteService("SubDivision", "GetAllConstituency", null);

                //mPISDesignation DesignationToEdit = (mPISDesignation)Helper.ExecuteService("Designation", "GetDesignationById", new mPISDesignation { Id = Id });
                mSubDivision stateToEdit = (mSubDivision)Helper.ExecuteService("SubDivision", "GetSubDivisionById", new mSubDivision { mSubDivisionId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                //var model = stateToEdit.ToViewModel("Constituencys", "Edit");
                SubDivisionViewModel model = new SubDivisionViewModel();
                model.ConstituencyID = stateToEdit.ConstituencyID;
                model.mSubDivisionId = stateToEdit.mSubDivisionId;
                model.SubDivisionName = stateToEdit.SubDivisionName;
                model.SubDivisionNameLocal = stateToEdit.SubDivisionNameLocal;
                model.SubDivisionAbbreviation = stateToEdit.SubDivisionAbbreviation;
                model.GetConstituencyName = stateToEdit.GetConstituencyName;
                model.SubDivision = new SelectList(Constituencys, "ConstituencyID", "ConstituencyName");
                return View("CreateSubDivision", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult DeleteSubDivision(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mSubDivision desigToDelete = (mSubDivision)Helper.ExecuteService("SubDivision", "GetSubDivisionById", new mSubDivision { mSubDivisionId = Id });
                Helper.ExecuteService("SubDivision", "DeleteSubDivision", desigToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Deleting Designation Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }



    }
}