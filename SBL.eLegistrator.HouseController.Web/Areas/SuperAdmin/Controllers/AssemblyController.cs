﻿using Microsoft.Security.Application;
using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class AssemblyController : Controller
    {
        //const int pageSize = 10;
        //
        // GET: /SuperAdmin/Assembly/
        public ActionResult ShowAllAssemblyDetails()
        {
            return View();
        }

        [HttpGet]
        public ActionResult IndexOfAssembly(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount, string ClickCount, string TempLoop)
        {
            try
            {

                mAssembly model = new mAssembly();
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if ((PageNumber == null || PageNumber == "") && (RowsPerPage == null || RowsPerPage == "") && (loopStart == null || loopStart == "") && (loopEnd == null || loopEnd == ""))
                {
                    PageNumber = (1).ToString();

                    RowsPerPage = (10).ToString();
                    loopStart = (1).ToString();
                    loopEnd = (5).ToString();
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                }
                else
                {
                    TempLoop = Sanitizer.GetSafeHtmlFragment(TempLoop);
                    PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
                    RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
                    loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
                    loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
                    ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
                    ClickCount = Sanitizer.GetSafeHtmlFragment(ClickCount);
                    model.PAGE_SIZE = int.Parse(RowsPerPage);
                }
                //model.PAGE_SIZE = 25;
                model.PageIndex = int.Parse(PageNumber);
                //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblies", null);
                model = (mAssembly)Helper.ExecuteService("Assembly", "GetAllAssemblies", model);
                model.ResultCount = model.ResultCount;
                model.PageNumber = Convert.ToInt32(PageNumber);
                model.RowsPerPage = Convert.ToInt32(RowsPerPage);
                // model.RowsPerPage = (25);
                model.selectedPage = Convert.ToInt32(PageNumber);
                model.loopStart = Convert.ToInt32(loopStart);
                model.loopEnd = Convert.ToInt32(loopEnd);
                //var model = Assemblies.ToViewModel();
                if (TempLoop != "")
                {
                    model.loopStart = Convert.ToInt32(TempLoop);
                }
                else
                {
                    model.loopStart = Convert.ToInt32(loopStart);
                }
                if (ClickCount != "")
                {
                    model.ClickCount = Convert.ToInt32(ClickCount);
                }
                return PartialView("_PartialIndexOfAssembly", model);
                //return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        //public ActionResult Index(string PageNumber, string RowsPerPage, string loopStart, string loopEnd, string ResultCount)
        //{
        //    try
        //    {
        //        mAssembly model = new mAssembly();
        //        if (string.IsNullOrEmpty(CurrentSession.UserName))
        //        {
        //            return RedirectToAction("LoginUP", "Account", new { @area = "" });
        //        }
        //        if ((PageNumber == null || PageNumber == "") && (RowsPerPage == null || RowsPerPage == "") && (loopStart == null || loopStart == "") && (loopEnd == null || loopEnd == ""))
        //        {
        //            PageNumber = (1).ToString();

        //            RowsPerPage = (10).ToString();
        //            loopStart = (1).ToString();
        //            loopEnd = (5).ToString();
        //            ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
        //            model.PAGE_SIZE = int.Parse(RowsPerPage);
        //        }
        //        else
        //        {
        //            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
        //            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
        //            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
        //            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
        //            ResultCount = Sanitizer.GetSafeHtmlFragment(ResultCount);
        //            model.PAGE_SIZE = int.Parse(RowsPerPage);
        //        }
        //        //model.PAGE_SIZE = 25;
        //        model.PageIndex = int.Parse(PageNumber);
        //        //var Assemblies = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblies", null);
        //        model = (mAssembly)Helper.ExecuteService("Assembly", "GetAllAssemblies", model);
        //        model.ResultCount = model.ResultCount;
        //        model.PageNumber = Convert.ToInt32(PageNumber);
        //        model.RowsPerPage = Convert.ToInt32(RowsPerPage);
        //        // model.RowsPerPage = (25);
        //        model.selectedPage = Convert.ToInt32(PageNumber);
        //        model.loopStart = Convert.ToInt32(loopStart);
        //        model.loopEnd = Convert.ToInt32(loopEnd);
        //        //var model = Assemblies.ToViewModel();
        //       // return PartialView("_IndexOfAssembly", model);
        //       return View(model);
        //    }
        //    catch (Exception ex)
        //    {


        //        ViewBag.ErrorMessage = "Their is a Error While Getting Assembly Details";
        //        return View("AdminErrorPage");
        //        throw ex;
        //    }


        //}
        public ActionResult CreateAssembly()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new AssemblyViewModel()
                {
                    Mode = "Add"

                };
                model.Active = true;

                //return PartialView("_PartialCreateAssembly", model);
                return View("CreateAssembly", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAssembly(AssemblyViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var assembly = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        var assemblycode = (int)Helper.ExecuteService("Assembly", "GetAssemblyCodeById", null);
                        if (assemblycode > 0)
                        {
                            int num = assemblycode;
                            assembly.AssemblyCode = num + 1;
                        }
                        else
                        {
                            int count = 0;
                            assembly.AssemblyCode = count + 1;
                        }
                        Helper.ExecuteService("Assembly", "CreateAssembly", assembly);
                    }
                    else
                    {
                        if (model.AssemblyID.HasValue)
                        {
                            var assemblycode = (int)Helper.ExecuteService("Assembly", "GetAssemblyCodeById1", assembly.AssemblyID);
                            if (assemblycode > 0)
                            {
                                int num = assemblycode;
                                assembly.AssemblyCode = num;
                            }
                        }



                        Helper.ExecuteService("Assembly", "UpdateAssemblyData", assembly);
                    }
                    return RedirectToAction("ShowAllAssemblyDetails");
                }
                else
                {
                    return RedirectToAction("ShowAllAssemblyDetails");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }


        public ActionResult EditAssembly(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mAssembly assemblyToEdit = (mAssembly)Helper.ExecuteService("Assembly", "GetAssemblyDataById", new mAssembly { AssemblyID = Convert.ToInt32((Id.ToString())) });
              //  mAssembly assemblyToEdit = (mAssembly)Helper.ExecuteService("Assembly", "GetAssemblyDataById", new mAssembly { AssemblyID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = assemblyToEdit.ToViewModel1("Edit");
                //return PartialView("_PartialUpdateAssembly", model);
                return View("CreateAssembly", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult DeleteAssembly(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mAssembly assemblyToDelete = (mAssembly)Helper.ExecuteService("Assembly", "GetAssemblyDataById", new mAssembly { AssemblyID = Id });
                Helper.ExecuteService("Assembly", "DeleteAssembly", assemblyToDelete);
                return RedirectToAction("ShowAllAssemblyDetails");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Assembly Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public JsonResult CheckAssemblyChildExist(int AssemblyID)
        {
            try
            {

                var isExist = (Boolean)Helper.ExecuteService("Assembly", "IsAssemblyIdChildExist", new mAssembly { AssemblyID = AssemblyID });

                return Json(isExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

    }
}
