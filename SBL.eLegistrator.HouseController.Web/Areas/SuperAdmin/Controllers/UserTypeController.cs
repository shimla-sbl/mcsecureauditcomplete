﻿using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.DomainModel.Models.User;
using SBL.eLegistrator.HouseController.Web.Extensions;
namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    public class UserTypeController : Controller
    {
        //
        // GET: /SuperAdmin/UserType/

        public ActionResult Index()
        {
            return View();
        }


        public ActionResult GetUserType()
        {
            var Modules = (List<mUserType>)Helper.ExecuteService("Module", "GetUserType", null);
            var model = Modules.ToViewUserType();

            return PartialView("_GetUserType", model);
        }

        public ActionResult CreateNewUserType()
        {

            var model = new mUserType()
            {
                Mode = "Add",

            };
            model.Mode = "Add";

            return PartialView("_CreateNewUserType", model);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveUserType(mUserType model)
        {

            if (ModelState.IsValid)
            {
                var dist = model.ToUserTypeModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Module", "CreateUserType", dist);
                }
                else
                {
                    Helper.ExecuteService("Module", "UpdateEntryUserType", dist);
                }

                return RedirectToAction("GetUserType");
            }
            else
            {

                return RedirectToAction("GetUserType");
            }

        }


        public ActionResult EditUserType(string Id)
        {

            mUserType UserTypeToEdit = (mUserType)Helper.ExecuteService("Module", "GetUserTypeDataById", new mUserType { UserTypeID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

            var model = UserTypeToEdit.ToViewUserTypeModel("Edit");
            model.UserTypeID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
            return View("_CreateNewUserType", model);
        }



        public ActionResult DeleteUserType(string Id)
        {
            mUserType UserTypeToDelete = (mUserType)Helper.ExecuteService("Module", "GetUserTypeDataById", new mUserType { UserTypeID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            Helper.ExecuteService("Module", "DeleteUserType", UserTypeToDelete);
            return RedirectToAction("GetUserType");

        }

        public JsonResult CheckUserName(string UserTypeName)
        {
            try
            {
                var mdl = (bool)Helper.ExecuteService("Role", "CheckUserTypeExist", UserTypeName);
                return Json(mdl, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {

                throw;
            }
        }
    }
}
