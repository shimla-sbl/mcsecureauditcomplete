﻿using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using System.Data;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{

    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class SMSGatewayController : Controller
    {
        //
        // GET: /SuperAdmin/SMSGateway/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                DataSet dataSet = new DataSet();
                var methodParameter = new List<KeyValuePair<string, string>>();
                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "SelectSMSGateway", methodParameter);
                SMSGatewayViewModel model1 = new SMSGatewayViewModel();
                List<SMSGatewayViewModel> SmsList = new List<SMSGatewayViewModel>();

                if (dataSet.Tables[0].Rows.Count != 0)
                {
                    for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                    {

                        SMSGatewayViewModel model = new SMSGatewayViewModel();

                        model.VId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["Id"]);
                        model.VGatewayID = Convert.ToString(dataSet.Tables[0].Rows[i]["GatewayID"]);
                        model.VGatewayURL = Convert.ToString(dataSet.Tables[0].Rows[i]["GatewayURL"]);
                        model.VGatewayQString = Convert.ToString(dataSet.Tables[0].Rows[i]["GatewayQString"]);
                        model.VRequireCountryCode = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["RequireCountryCode"]);
                        model.VGatewayStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["GatewayStatus"]);
                        model.VSuccessResponseCode = Convert.ToString(dataSet.Tables[0].Rows[i]["SuccessResponseCode"]);
                        model.VMobileNoQString = Convert.ToString(dataSet.Tables[0].Rows[i]["MobileNoQString"]);
                        model.VSMSTextQString = Convert.ToString(dataSet.Tables[0].Rows[i]["SMSTextQString"]);
                        SmsList.Add(model);
                    }

                    model1.Slist = SmsList;
                }

                return View(model1);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting SMSGateway Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }


        public ActionResult CreateSMSGateway()
        {
            var model = new SMSGatewayViewModel()
            {
                Mode = "Add",
                VRequireCountryCode = false,
                VGatewayStatus = false


            };

            return View("CreateSMSGateway", model);
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveSMSGateway(SMSGatewayViewModel model)
        {

            if (ModelState.IsValid)
            {

                if (model.Mode == "Add")
                {

                    DataSet dataSet = new DataSet();
                    var methodParameter = new List<KeyValuePair<string, string>>();

                    methodParameter.Add(new KeyValuePair<string, string>("@GatewayID", Convert.ToString(model.VGatewayID)));
                    methodParameter.Add(new KeyValuePair<string, string>("@GatewayURL", Convert.ToString(model.VGatewayURL)));
                    methodParameter.Add(new KeyValuePair<string, string>("@GatewayQString", Convert.ToString(model.VGatewayQString)));
                    methodParameter.Add(new KeyValuePair<string, string>("@RequireCountryCode", Convert.ToString(model.VRequireCountryCode)));
                    methodParameter.Add(new KeyValuePair<string, string>("@GatewayStatus", Convert.ToString(model.VGatewayStatus)));
                    methodParameter.Add(new KeyValuePair<string, string>("@SuccessResponseCode", Convert.ToString(model.VSuccessResponseCode)));
                    methodParameter.Add(new KeyValuePair<string, string>("@MobileNoQString", Convert.ToString(model.VMobileNoQString)));
                    methodParameter.Add(new KeyValuePair<string, string>("@SMSTextQString", Convert.ToString(model.VSMSTextQString)));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "InsertSMSGateway", methodParameter);

                }
                else
                {
                    DataSet dataSet = new DataSet();
                    var methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@Id", Convert.ToString(model.VId)));
                    methodParameter.Add(new KeyValuePair<string, string>("@GatewayID", Convert.ToString(model.VGatewayID)));
                    methodParameter.Add(new KeyValuePair<string, string>("@GatewayURL", Convert.ToString(model.VGatewayURL)));
                    methodParameter.Add(new KeyValuePair<string, string>("@GatewayQString", Convert.ToString(model.VGatewayQString)));
                    methodParameter.Add(new KeyValuePair<string, string>("@RequireCountryCode", Convert.ToString(model.VRequireCountryCode)));
                    methodParameter.Add(new KeyValuePair<string, string>("@GatewayStatus", Convert.ToString(model.VGatewayStatus)));
                    methodParameter.Add(new KeyValuePair<string, string>("@SuccessResponseCode", Convert.ToString(model.VSuccessResponseCode)));
                    methodParameter.Add(new KeyValuePair<string, string>("@MobileNoQString", Convert.ToString(model.VMobileNoQString)));
                    methodParameter.Add(new KeyValuePair<string, string>("@SMSTextQString", Convert.ToString(model.VSMSTextQString)));

                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "UpdateSMSGateway", methodParameter);

                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditSMSGateway(int Id)
        {
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();

            methodParameter.Add(new KeyValuePair<string, string>("@Id", Convert.ToString(Id)));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "SelectSMSGatewayById", methodParameter);


            SMSGatewayViewModel model = new SMSGatewayViewModel();
            List<SMSGatewayViewModel> SmsList = new List<SMSGatewayViewModel>();
            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {


                model.VId = Convert.ToInt32(dataSet.Tables[0].Rows[i]["Id"]);
                model.VGatewayID = Convert.ToString(dataSet.Tables[0].Rows[i]["GatewayID"]);
                model.VGatewayURL = Convert.ToString(dataSet.Tables[0].Rows[i]["GatewayURL"]);
                model.VGatewayQString = Convert.ToString(dataSet.Tables[0].Rows[i]["GatewayQString"]);
                model.VRequireCountryCode = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["RequireCountryCode"]);
                model.VGatewayStatus = Convert.ToBoolean(dataSet.Tables[0].Rows[i]["GatewayStatus"]);
                model.VSuccessResponseCode = Convert.ToString(dataSet.Tables[0].Rows[i]["SuccessResponseCode"]);
                model.VMobileNoQString = Convert.ToString(dataSet.Tables[0].Rows[i]["MobileNoQString"]);
                model.VSMSTextQString = Convert.ToString(dataSet.Tables[0].Rows[i]["SMSTextQString"]);

            }


            return View("CreateSMSGateway", model);
        }



        public ActionResult DeleteSMSGateway(int Id)
        {
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();

            methodParameter.Add(new KeyValuePair<string, string>("@Id", Convert.ToString(Id)));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "DeleteSMSGateway", methodParameter);

            return RedirectToAction("Index");

        }





        [HttpPost]
        public JsonResult CheckId(bool checkactive)
        {
            var a = 0;
            DataSet dataSet = new DataSet();
            var methodParameter = new List<KeyValuePair<string, string>>();

            methodParameter.Add(new KeyValuePair<string, string>("@GatewayStatus", Convert.ToString(checkactive)));

            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDbSMS", "SelectMSSql", "CheckSMSGateway", methodParameter);

            a = dataSet.Tables[0].Rows.Count;

            return Json(a, JsonRequestBehavior.AllowGet);
        }




    }
}
