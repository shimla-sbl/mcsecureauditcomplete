﻿using System;
using SBL.DomainModel.Models.ConstituencyVS;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class ContituencyWorksController : Controller
    {
        //
        // GET: /SuperAdmin/ContituencyWorks/

        public ActionResult Index()
        {
            try
            {
                //if (string.IsNullOrEmpty(CurrentSession.UserName))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}
                var ContituencyWorks = (List<workType>)Helper.ExecuteService("ContituencyWorks", "GetAllWorks", null);
                var model = ContituencyWorks.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Highest Qualification Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }
        public ActionResult CreateWorks()
        {
            var model = new ContituencyWorksViewModel()
            {
                Mode = "Add",
                //IsActive = true

            };

            return View("CreateWorks", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveProgramme(ContituencyWorksViewModel model)
        {

            if (ModelState.IsValid)
            {
                var ContituencyWorks = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("ContituencyWorks", "CreateWorks", ContituencyWorks);
                }
                else
                {
                    Helper.ExecuteService("ContituencyWorks", "UpdateWorks", ContituencyWorks);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditProgramme(string Id)
        {

            //mMemberHighestQualification stateToEdit = (mMemberHighestQualification)Helper.ExecuteService("HighestQualification", "GetHighestQualificationById", new mMemberHighestQualification { MemberHighestQualificationId = Id });
            workType stateToEdit = (workType)Helper.ExecuteService("ContituencyWorks", "GetWorksById", new workType { workID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

            var model = stateToEdit.ToViewModel("Edit");
            return View("CreateWorks", model);
        }

        public ActionResult DeleteWorks(int Id)
        {
            workType stateToDelete = (workType)Helper.ExecuteService("ContituencyWorks", "GetWorksById", new workType { workID = Id });
            Helper.ExecuteService("ContituencyWorks", "DeleteWorks", stateToDelete);
            return RedirectToAction("Index");

        }

    }
}