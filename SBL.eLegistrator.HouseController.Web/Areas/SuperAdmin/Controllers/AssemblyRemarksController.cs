﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;

using SBL.DomainModel.Models.AssemblyRemarks;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Assembly;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class AssemblyRemarksController : Controller
    {
        //
        // GET: /SuperAdmin/AssemblyRemarks/

        public ActionResult Index()
        {
            try
            {
                if( string.IsNullOrEmpty(CurrentSession.UserName))
                {
                     return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var AssemblyRemarks = (List<mAssemblyRemarks>)Helper.ExecuteService("AssemblyRemarks", "GetAllAssemblyRemarks", null);
                var model = AssemblyRemarks.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }
        public ActionResult CreateAssemblyRemarks()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                var model = new AssemblyRemarksViewModel()
                {
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    Mode = "Add",
                    Active = true
                };
                return View("CreateAssemblyRemarks", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
               
            }
        
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveAssemblyRemarks(AssemblyRemarksViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var AssemblyRemarks = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("AssemblyRemarks", "CreateAssemblyRemarks", AssemblyRemarks);
                    }
                    else
                    {
                        Helper.ExecuteService("AssemblyRemarks", "UpdateAsemblyRemarks", AssemblyRemarks);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
                
            }
           

        }

        public ActionResult EditAssemblyRemarks(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                //mAssemblyRemarks assemblyToEdit = (mAssemblyRemarks)Helper.ExecuteService("AssemblyRemarks", "GetAssemblyRemarksBasedOnId", new mAssemblyRemarks { AssemblyRemarksID = Id });
                mAssemblyRemarks assemblyToEdit = (mAssemblyRemarks)Helper.ExecuteService("AssemblyRemarks", "GetAssemblyRemarksBasedOnId", new mAssemblyRemarks { AssemblyRemarksID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                
                var model = assemblyToEdit.ToViewModel1(Assemblies, "Edit");
                return View("CreateAssemblyRemarks", model);
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Editing Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
            }
          

        }

        public ActionResult DeleteAssemblyRemarks(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mAssemblyRemarks assemblyToDelete = (mAssemblyRemarks)Helper.ExecuteService("AssemblyRemarks", "GetAssemblyRemarksBasedOnId", new mAssemblyRemarks { AssemblyRemarksID = Id });
                Helper.ExecuteService("AssemblyRemarks", "DeleteAssemblyRemarks", assemblyToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                ViewBag.ErrorMessage = "There is a Error While Deleting Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }

    }
}
