﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.DomainModel.Models.MemberAssemblyRemarks;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;


namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MemberAssemblyRemarksController : Controller
    {
        //
        // GET: /SuperAdmin/MemberAssemblyRemarks/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var memberAssemblyRemarks = (List<mMemberAssemblyRemarks>)Helper.ExecuteService("MemberAssemblyRemarks", "GetAllMemberAssemblyRemarks", null);
                var model = memberAssemblyRemarks.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Member Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }

      

        public ActionResult CreateMemberAssemblyRemarks()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var model = new MemberAssemblyRemarksViewModel()
                {
                    Mode = "Add",
                    Active = true
                };
                return View("CreateMemberAssemblyRemarks", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Member Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMemberAssemblyRemarks(MemberAssemblyRemarksViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                if (ModelState.IsValid)
                {
                    var MemberAssemblyRemarks = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("MemberAssemblyRemarks", "CreateMemberAssemblyRemarks", MemberAssemblyRemarks);
                    }
                    else
                    {
                        Helper.ExecuteService("MemberAssemblyRemarks", "UpdateMemberAsemblyRemarks", MemberAssemblyRemarks);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Member Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
            }
          

        }

        public ActionResult EditMemberAssemblyRemarks(string Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //mMemberAssemblyRemarks assemblyToEdit = (mMemberAssemblyRemarks)Helper.ExecuteService("MemberAssemblyRemarks", "GetMemberAssemblyRemarksBasedOnId", new mMemberAssemblyRemarks { MemberAssemblyRemarksID = Id });
                mMemberAssemblyRemarks assemblyToEdit = (mMemberAssemblyRemarks)Helper.ExecuteService("MemberAssemblyRemarks", "GetMemberAssemblyRemarksBasedOnId", new mMemberAssemblyRemarks { MemberAssemblyRemarksID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = assemblyToEdit.ToViewModel1("Edit");
                return View("CreateMemberAssemblyRemarks", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Member Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
            }
          

        }

        public ActionResult DeleteMemberAssemblyRemarks(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMemberAssemblyRemarks assemblyToDelete = (mMemberAssemblyRemarks)Helper.ExecuteService("MemberAssemblyRemarks", "GetMemberAssemblyRemarksBasedOnId", new mMemberAssemblyRemarks { MemberAssemblyRemarksID = Id });
                Helper.ExecuteService("MemberAssemblyRemarks", "DeleteMemberAssemblyRemarks", assemblyToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Member Assembly Remarks Details";
                return View("AdminErrorPage");
                throw ex;
            }
           
        }

    }
}
