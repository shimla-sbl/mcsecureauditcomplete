﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Bill;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class BillController : Controller
    {
        //
        // GET: /SuperAdmin/Bill/

        public ActionResult IndexOfBill()
        {
            return View();
        }

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
            var Bill = (List<mBillType>)Helper.ExecuteService("Bill", "GetAllBill", null);
            var model = Bill.ToViewModel();
            return View(model);
           // return PartialView("_PartialIndexOfBill", model);
            }
            catch (Exception ex)
            {


                ViewBag.ErrorMessage = "There is a Error While Getting Memorandum Type Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateBill()
        {
            var model = new BillViewModel()
            {
                Mode = "Add",
                IsActive=true

            };

            return View("CreateBill", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveBill(BillViewModel model)
        {

            if (ModelState.IsValid)
            {
                var bill = model.ToDomainModel();
                if (model.Mode == "Add")
                {

                    Helper.ExecuteService("Bill", "CreateBill", bill);
                }
                else
                {
                    Helper.ExecuteService("Bill", "UpdateBill", bill);
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public ActionResult EditBill(string Id)
        {

            //mBillType billToEdit = (mBillType)Helper.ExecuteService("Bill", "GetBillById", new mBillType { BillTypeId = Id });
            mBillType billToEdit = (mBillType)Helper.ExecuteService("Bill", "GetBillById", new mBillType { BillTypeId = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            var model = billToEdit.ToViewModel1("Edit");
            return View("CreateBill", model);
        }

        public ActionResult DeleteBill(int Id)
        {
            mBillType billToDelete = (mBillType)Helper.ExecuteService("Bill", "GetBillById", new mBillType { BillTypeId = Id });
            Helper.ExecuteService("Bill", "DeleteBill", billToDelete);
            return RedirectToAction("Index");

        }

    }
}
