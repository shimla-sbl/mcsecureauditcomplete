﻿using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.Diaries;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Controllers
{
    [Audit]
    [SBLAuthorize(Allow = "Authenticated")]
    [NoCache]
    public class MinsitryMinisterController : Controller
    {
        //
        // GET: /SuperAdmin/MinsitryMinister/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                //var MinistryMinister = (List<mMinsitryMinister>)Helper.ExecuteService("Ministry", "GetAllMinsitryMinisterByAID", null);
                //var model = MinistryMinister.ToViewModel();

                var assmeblyList = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                ViewBag.assmeblyList = assmeblyList;
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Minsitry Minister Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult MinistryMinisterListByAID(int assemblyId)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var MinistryMinister = (List<mMinsitryMinister>)Helper.ExecuteService("Ministry", "GetAllMinsitryMinisterByAID", assemblyId);
                var model = MinistryMinister.ToViewModel();
                return PartialView("_MinistryMinisterList",model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Getting Minsitry Minister Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

        public ActionResult CreateMinsitryMinister()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
               // var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
                var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetMembersNull", null);             
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);
                //var Ministry = (List<mMinistry>)Helper.ExecuteService("Ministry", "GetAllMinistry1", null);
                var Ministry = (List<mMinistry>)Helper.ExecuteService("Ministry", "GetAllMinistryNull", null);
                
                var model = new MinsitryMinisterViewModel()
                {
                    Mode = "Add",
                    MemberNames = new SelectList(Members, "MemberCode", "Name"),
                    Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                    Ministry = new SelectList(Ministry, "MinistryID", "MinistryName"),
                    IsActive = true


                };
                return View("CreateMinsitryMinister", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Creating Minsitry Minister Details";
                return View("AdminErrorPage");
                throw ex;
            }


        }

        public JsonResult GetMemberByAId(int Assemblyid)
        {
            DiaryModel DMdl = new DiaryModel();
            DMdl.AssemblyID = Assemblyid;
            DMdl.memberList = (List<DiaryModel>)Helper.ExecuteService("Member", "GetMemByAssemblyDiary", DMdl);
            return Json(DMdl.memberList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMinistryAId(int Assemblyid)
        {
            DiaryModel DMdl = new DiaryModel();
            DMdl.AssemblyID = Assemblyid;
            DMdl.MinistryList = (List<DiaryModel>)Helper.ExecuteService("Member", "GetMinistryByAssemblyDiary", DMdl);
            return Json(DMdl.MinistryList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveMinsitryMinister(MinsitryMinisterViewModel model)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                if (ModelState.IsValid)
                {
                    var ministry = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {

                        //ministry.IsActive = true;
                        Helper.ExecuteService("Ministry", "CreateMinsitryMinister", ministry);
                    }
                    else
                    {


                        //ministry.IsActive = true;
                        Helper.ExecuteService("Ministry", "UpdateMinsitryMinister", ministry);
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }


            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Minsitry Minister Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult EditMinsitryMinister(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var Members = (List<mMember>)Helper.ExecuteService("Ministry", "GetAllMembers", null);
                var Assemblies = (List<mAssembly>)Helper.ExecuteService("Session", "GetAllAssembly", null);

                //done changes by roBIN
                DiaryModel DMdl = new DiaryModel();
                DMdl.AssemblyID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString()));
                var Ministry = (List<DiaryModel>)Helper.ExecuteService("Member", "GetMinistryByAssemblyDiary", DMdl);

                //var Ministry = (List<mMinistry>)Helper.ExecuteService("Ministry", "GetAllMinistry1", null);
                //mMinsitryMinister ministryministerToEdit = (mMinsitryMinister)Helper.ExecuteService("Ministry", "GetMinsitryMinisterById", new mMinsitryMinister { MinsitryMinistersID = Id });
                mMinsitryMinister ministryministerToEdit = (mMinsitryMinister)Helper.ExecuteService("Ministry", "GetMinsitryMinisterById", new mMinsitryMinister { MinsitryMinistersID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                var model = ministryministerToEdit.ToViewModel1(Members, Ministry, Assemblies, "Edit");
                return View("CreateMinsitryMinister", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Editing Minsitry Minister Details";
                return View("AdminErrorPage");
                throw ex;
            }
        }

        public ActionResult DeleteMinsitryMinister(int Id)
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                mMinsitryMinister ministryToDelete = (mMinsitryMinister)Helper.ExecuteService("Ministry", "GetMinsitryMinisterById", new mMinsitryMinister { MinsitryMinistersID = Id });
                Helper.ExecuteService("Ministry", "DeleteMinsitryMinister", ministryToDelete);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "There is a Error While Deleting Minsitry Minister Details";
                return View("AdminErrorPage");
                throw ex;
            }

        }

    }
}
