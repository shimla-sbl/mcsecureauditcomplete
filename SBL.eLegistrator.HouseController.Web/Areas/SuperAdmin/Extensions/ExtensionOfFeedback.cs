﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Web.Mvc;
using SBL.DomainModel.Models.ContactUs;


namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfFeedback
    {

        public static List<FeedbackViewModel> ToViewModel(this IEnumerable<ContactUs> model)
        {

            var ViewModel = model.Select(part => new FeedbackViewModel()
            {
                VId = part.Id,
                VName = part.Name,
                VEmail = part.Email,
                VMobile = part.Mobile,
                VMessage = part.Message,
                
            });
            return ViewModel.ToList();
        }


    }
}