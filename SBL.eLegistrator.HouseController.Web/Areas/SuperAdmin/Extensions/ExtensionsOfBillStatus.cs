﻿using SBL.DomainModel.Models.Bill;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfBillStatus
    {
        public static IEnumerable<BillStatusViewModel> ToViewModel(this IEnumerable<mBillStatus> model)
        {

            var ViewModel = model.Select(billstatus => new BillStatusViewModel()
            {
                BillStatusId = billstatus.BillStatusId,
                BillStatus = billstatus.BillStatus,
                BillStatusLocal = billstatus.BillStatusLocal,
                Description=billstatus.Description,
                Active = billstatus.Active ?? false

            });
            return ViewModel;
        }

        public static mBillStatus ToDomainModel(this BillStatusViewModel model)
        {
            return new mBillStatus
            {
                 BillStatusId = model.BillStatusId,
                BillStatus = model.BillStatus,
                BillStatusLocal = model.BillStatusLocal,
                Description=model.Description,
                Active = model.Active,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }


        public static BillStatusViewModel ToViewModel1(this mBillStatus model, string Mode)
        {
            return new BillStatusViewModel
            {
                BillStatusId = model.BillStatusId,
                BillStatus = model.BillStatus,
                BillStatusLocal = model.BillStatusLocal,
                Description = model.Description,
                Active = model.Active ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }
    }
}