﻿using SBL.DomainModel.Models.Constituency;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfVillage
    {
        public static IEnumerable<VillageViewModel> ToViewModel(this IEnumerable<mVillage> model)
        {

            var ViewModel = model.Select(village => new VillageViewModel()
            {
                VillageID = village.VillageID,
                VillageCode = village.VillageCode.ToString(),
                VillageName = village.VillageName,
                VillageNameLocal=village.VillageNameLocal,
                IsActive = village.IsActive??false

            });
            return ViewModel;
        }

        public static mVillage ToDomainModel(this VillageViewModel village)
        {
            return new mVillage
            {
                 VillageID = village.VillageID,
                VillageCode = Convert.ToInt32( village.VillageCode),
                VillageName = village.VillageName,
                VillageNameLocal=village.VillageNameLocal,
                IsActive = village.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static VillageViewModel ToViewModel1(this mVillage village, string Mode)
        {
            return new VillageViewModel
            {
                VillageID = village.VillageID,
                VillageCode = village.VillageCode.ToString(),
                VillageName = village.VillageName,
                VillageNameLocal = village.VillageNameLocal,
                IsActive = village.IsActive?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }
    }
}