﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Party;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfMemberAssembly
    {
        public static IEnumerable<MemberAssemblyViewModel> ToViewModel(this IEnumerable<mMemberAssembly> model)
        {

            var ViewModel = model.Select(memAssem => new MemberAssemblyViewModel()
            {
                MemberAssemblyID = memAssem.MemberAssemblyID,
                AssemblyID = memAssem.AssemblyID,
                MemberID = memAssem.MemberID,
                ConstituencyCode = memAssem.ConstituencyCode,
                Remarks = memAssem.Remarks,
                DeptAssigned = memAssem.DeptAssigned,
                LevelCode = memAssem.LevelCode,
                MemberstartDate = memAssem.MemberstartDate.ToString(),
                MemberEndDate = memAssem.MemberEndDate.ToString(),
                Active = memAssem.Active ?? false,
                IsWomenReservation = memAssem.IsWomenReservation,
                IsSCSTReservation = memAssem.IsSCSTReservation,
                Category = memAssem.Category,
                AuthorityOrder = memAssem.AuthorityOrder,
                Designation = memAssem.Designation,
                DesignationID = memAssem.DesignationID,
                PartyID = memAssem.PartyID,
                PartyCode = memAssem.PartyCode,
                ProfiePicPath = memAssem.ProfiePicPath,
                Para1 = memAssem.Para1,
                Para2 = memAssem.Para2,
                Para3 = memAssem.Para3,
                Para4 = memAssem.Para4,
                Para5 = memAssem.Para5,
                sinterest = memAssem.sinterest,
                languagesknown = memAssem.languagesknown,
                travels = memAssem.travels,
                timepass = memAssem.timepass,
                sports = memAssem.sports,
                Awards = memAssem.Awards,
                Location = memAssem.Location,
                mMemberOldID = memAssem.mMemberOldID,
                SocialActivities = memAssem.SocialActivities,
                PublishedWork = memAssem.PublishedWork,
                ConferencesAttended = memAssem.ConferencesAttended,
                GetAssemblyName = memAssem.GetAssemblyName,
                GetMemberName = memAssem.GetMemberName,
                GetConstituencyName = memAssem.GetConstituencyName,
                GetpartyName = memAssem.GetpartyName,
                GetMemberDesignationName = memAssem.GetMemberDesignationName


            });
            return ViewModel;
        }

        public static mMemberAssembly ToDomainModel(this MemberAssemblyViewModel memAssem)
        {
            return new mMemberAssembly
            {

                MemberAssemblyID = memAssem.MemberAssemblyID,
                AssemblyID = memAssem.AssemblyID,
                MemberID = memAssem.MemberID,
                ConstituencyCode = memAssem.ConstituencyCode,
                Remarks = memAssem.Remarks,
                DeptAssigned = memAssem.DeptAssigned,
                LevelCode = memAssem.LevelCode,
                MemberstartDate = string.IsNullOrEmpty(memAssem.MemberstartDate) ? DateTime.Now : DateTime.ParseExact(memAssem.MemberstartDate, "dd/MM/yyyy", null),
                MemberEndDate = string.IsNullOrEmpty(memAssem.MemberEndDate) ? DateTime.Now : DateTime.ParseExact(memAssem.MemberEndDate, "dd/MM/yyyy", null),
                Active = memAssem.Active,
                IsWomenReservation = memAssem.IsWomenReservation,
                IsSCSTReservation = memAssem.IsSCSTReservation,
                Category = memAssem.Category,
                AuthorityOrder = memAssem.AuthorityOrder,
                Designation = memAssem.Designation,
                DesignationID = memAssem.DesignationID,
                PartyID = memAssem.PartyID,
                PartyCode = memAssem.PartyCode,
                ProfiePicPath = memAssem.ProfiePicPath,
                Para1 = memAssem.Para1,
                Para2 = memAssem.Para2,
                Para3 = memAssem.Para3,
                Para4 = memAssem.Para4,
                Para5 = memAssem.Para5,
                sinterest = memAssem.sinterest,
                languagesknown = memAssem.languagesknown,
                travels = memAssem.travels,
                timepass = memAssem.timepass,
                sports = memAssem.sports,
                Awards = memAssem.Awards,
                Location = memAssem.Location,
                mMemberOldID = memAssem.mMemberOldID,
                SocialActivities = memAssem.SocialActivities,
                PublishedWork = memAssem.PublishedWork,
                ConferencesAttended = memAssem.ConferencesAttended,
                GetAssemblyName = memAssem.GetAssemblyName,
                GetMemberName = memAssem.GetMemberName,
                GetConstituencyName = memAssem.GetConstituencyName,
                GetpartyName = memAssem.GetpartyName,
                GetMemberDesignationName = memAssem.GetMemberDesignationName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        public static MemberAssemblyViewModel ToViewModel1(this mMemberAssembly memAssem, List<mMember> Members, List<mAssembly> Assemblies, List<mConstituency> Constituencies, List<mParty> Parties, List<DistrictModel> locationss, List<mMemberDesignation> memdesig, string Mode)
        {
            return new MemberAssemblyViewModel
            {
                MemberAssemblyID = memAssem.MemberAssemblyID,
                AssemblyID = memAssem.AssemblyID,
                MemberID = memAssem.MemberID,
                ConstituencyCode = memAssem.ConstituencyCode,
                Remarks = memAssem.Remarks,
                DeptAssigned = memAssem.DeptAssigned,
                LevelCode = memAssem.LevelCode,
                MemberstartDate = memAssem.MemberstartDate != null ? memAssem.MemberstartDate.Value.ToString("dd/MM/yyyy") : "",
                MemberEndDate = memAssem.MemberEndDate != null?memAssem.MemberEndDate.Value.ToString("dd/MM/yyyy"):"",
                Active = memAssem.Active ?? false,
                IsWomenReservation = memAssem.IsWomenReservation,
                IsSCSTReservation = memAssem.IsSCSTReservation,
                Category = memAssem.Category,
                AuthorityOrder = memAssem.AuthorityOrder,
                Designation = memAssem.Designation,
                DesignationID = memAssem.DesignationID,
                PartyID = memAssem.PartyID,
                PartyCode = memAssem.PartyCode,
                ProfiePicPath = memAssem.ProfiePicPath,
                Para1 = memAssem.Para1,
                Para2 = memAssem.Para2,
                Para3 = memAssem.Para3,
                Para4 = memAssem.Para4,
                Para5 = memAssem.Para5,
                sinterest = memAssem.sinterest,
                languagesknown = memAssem.languagesknown,
                travels = memAssem.travels,
                timepass = memAssem.timepass,
                sports = memAssem.sports,
                Awards = memAssem.Awards,
                Location = memAssem.Location,
                mMemberOldID = memAssem.mMemberOldID,
                SocialActivities = memAssem.SocialActivities,
                PublishedWork = memAssem.PublishedWork,
                ConferencesAttended = memAssem.ConferencesAttended,
                GetAssemblyName = memAssem.GetAssemblyName,
                GetMemberName = memAssem.GetMemberName,
                GetConstituencyName = memAssem.GetConstituencyName,
                GetpartyName = memAssem.GetpartyName,
                GetMemberDesignationName = memAssem.GetMemberDesignationName,
                MemberNames = new SelectList(Members, "MemberCode", "Name",memAssem.MemberID),//for selecting current edit member
                Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                ConstituencyNames = new SelectList(Constituencies, "ConstituencyCode", "ConstituencyName"),
                PartryNames = new SelectList(Parties, "PartyID", "PartyName"),
                Locations = new SelectList(locationss, "DistrictName", "DistrictName"),
                DesignationList = new SelectList(memdesig, "memDesigId", "memDesigname"),
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }

        public static SelectList GetDesignation()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "MLA", Value = "MLA" });
            result.Add(new SelectListItem() { Text = "Minister", Value = "Minister" });
            result.Add(new SelectListItem() { Text = "Speaker", Value = "Speaker" });

            return new SelectList(result, "Value", "Text");
        }
    }
}