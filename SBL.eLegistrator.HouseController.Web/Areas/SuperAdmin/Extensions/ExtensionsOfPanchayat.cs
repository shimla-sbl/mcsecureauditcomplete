﻿using SBL.DomainModel.Models.Constituency;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfPanchayat
    {

        public static mPanchayat ToDomainModel(this PanchayatViewModel panchyat)
        {
            try
            {
                return new mPanchayat
                {
                    PanchayatID = panchyat.PanchayatID,
                    PanchayatCode = Convert.ToInt32(panchyat.PanchayatCode),
                    PanchayatName = panchyat.PanchayatName,
                    PanchayatNameLocal = panchyat.PanchayatNameLocal,
                    IsActive = panchyat.IsActive,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName

                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static PanchayatViewModel ToViewModel1(this mPanchayat panchayat, string Mode)
        {
            try
            {
                return new PanchayatViewModel
                {
                    PanchayatID = panchayat.PanchayatID,
                    PanchayatCode = panchayat.PanchayatCode.ToString(), // panchayat.PanchayatCode != null ? panchayat.PanchayatCode.ToString() : "",
                    PanchayatName = panchayat.PanchayatName,
                    PanchayatNameLocal = panchayat.PanchayatNameLocal,
                    IsActive = panchayat.IsActive ?? false,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName,
                    Mode = Mode

                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}