﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfBackDoor
    {
        public static IEnumerable<BackDoorViewModel> ToViewModel(this IEnumerable<mBackDoorPassword> model)
        {

            var ViewModel = model.Select(backDoor => new BackDoorViewModel()
            {
                SlNo = backDoor.SlNo,
                BackDoorPassword = backDoor.BackDoorPassword,
                Code = backDoor.Code
            });
            return ViewModel;
        }

        public static mBackDoorPassword ToDomainModel(this BackDoorViewModel model)
        {
            try
            {
                return new mBackDoorPassword
                {
                    SlNo = model.SlNo,
                    BackDoorPassword = model.BackDoorPassword,
                    Code = model.Code,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static BackDoorViewModel ToViewModel1(this mBackDoorPassword model, List<mMobileApps> Codes, string Mode)
        {
            return new BackDoorViewModel
            {
                SlNo = model.SlNo,
                BackDoorPassword = model.BackDoorPassword,
                Code = model.Code,
                CodeList = new SelectList(Codes, "Code", "Code"),
                Mode = Mode
            };
        }

    }
}