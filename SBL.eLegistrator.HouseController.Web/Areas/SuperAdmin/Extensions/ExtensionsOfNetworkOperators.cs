﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfNetworkOperators
    {
        public static IEnumerable<NetworkOperatorsViewModel> ToViewModel(this IEnumerable<mNetworkOperators> model)
        {

            var ViewModel = model.Select(TeleCir => new NetworkOperatorsViewModel()
            {

                SlNo = TeleCir.SlNo,
                NetworkCode = TeleCir.NetworkCode,
                Description = TeleCir.Description

            });
            return ViewModel;
        }

        public static mNetworkOperators ToDomainModel(this NetworkOperatorsViewModel telecom)
        {
            return new mNetworkOperators
            {
                SlNo = telecom.SlNo,
                NetworkCode = telecom.NetworkCode,
                Description = telecom.Description,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static NetworkOperatorsViewModel ToViewModel1(this mNetworkOperators network, string Mode)
        {
            return new NetworkOperatorsViewModel
            {
                SlNo = network.SlNo,
                NetworkCode = network.NetworkCode,
                Description = network.Description,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }
    }
}