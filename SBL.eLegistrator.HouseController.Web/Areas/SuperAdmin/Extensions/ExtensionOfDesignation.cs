﻿using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.PISModules;
using SBL.DomainModel.Models.PISMolues;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfDesignation
    {

        public static List<DesignationViewModel> ToViewModel(this IEnumerable<mPISDesignation> model)
        {

            var ViewModel = model.Select(desig => new DesignationViewModel()
            {
                Id = desig.Id,
                designame = desig.designame,
                desiglocal = desig.desiglocal,
                desigcode = desig.desigcode,
                GetDepartmentName = desig.GetDepartmentName,
                desigabbr = desig.desigabbr,
                Active = desig.Active ?? false,
                deptid = desig.deptid

            });
            return ViewModel.ToList();
        }

        public static mPISDesignation ToDomainModel(this DesignationViewModel model)
        {
            return new mPISDesignation
            {
                Id = (Int32)(model.Id != null ? model.Id : 0),
                designame = model.designame,
                desiglocal = model.desiglocal,
                desigcode = model.desigcode,
                GetDepartmentName = model.GetDepartmentName,
                desigabbr = model.desigabbr,
                deptid = model.deptid,
                Active = model.Active,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }


        public static DesignationViewModel ToViewModel1(this mPISDesignation model, List<mDepartment> Departments, string Mode)
        {
            return new DesignationViewModel
            {
                Id = model.Id,
                designame = model.designame,
                desiglocal = model.desiglocal,
                desigcode = model.desigcode,
                desigabbr = model.desigabbr,
                GetDepartmentName = model.GetDepartmentName,
                deptid = model.deptid,
                Active = model.Active ?? false,
                Department = new SelectList(Departments, "deptId", "deptname"),
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }

    }
}