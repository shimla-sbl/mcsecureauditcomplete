﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfMinsitryMinister
    {
        public static IEnumerable<MinsitryMinisterViewModel> ToViewModel(this IEnumerable<mMinsitryMinister> model)
        {

            var ViewModel = model.Select(ministryminister => new MinsitryMinisterViewModel()
            {
                AssemblyID = ministryminister.AssemblyID,
                MinistryID = ministryminister.MinistryID,
                MinsitryMinistersID=ministryminister.MinsitryMinistersID,
                MinisterName=ministryminister.MinisterName,
                MinisterNameLocal=ministryminister.MinisterNameLocal,
                MemberCode=ministryminister.MemberCode,
                GetAssemblyName=ministryminister.GetAssemblyName,
                GetMemberName=ministryminister.GetMemberName,

                GetMinistryName=ministryminister.GetMinistryName,
                MemberStartDate=ministryminister.MemberStartDate.ToString(),
                MemberEndDate=ministryminister.MemberEndDate.ToString(),
                IsActive = ministryminister.IsActive ?? false,               
                OrderID = ministryminister.OrderID

            });
            return ViewModel;
        }

        public static mMinsitryMinister ToDomainModel(this MinsitryMinisterViewModel ministryminister)
        {
            return new mMinsitryMinister
            {
                

                AssemblyID = ministryminister.AssemblyID,
                MinistryID = ministryminister.MinistryID,
                MinsitryMinistersID=ministryminister.MinsitryMinistersID,
                MinisterName=ministryminister.MinisterName,
                MinisterNameLocal=ministryminister.MinisterNameLocal,
                MemberCode=ministryminister.MemberCode,
                GetAssemblyName = ministryminister.GetAssemblyName,
                GetMemberName = ministryminister.GetMemberName,

                GetMinistryName = ministryminister.GetMinistryName,
                MemberStartDate=string.IsNullOrEmpty( ministryminister.MemberStartDate)? DateTime.Now:DateTime.ParseExact(ministryminister.MemberStartDate,"dd/MM/yyyy",null),
                MemberEndDate= string.IsNullOrEmpty( ministryminister.MemberEndDate)?DateTime.Now:DateTime.ParseExact(ministryminister.MemberEndDate,"dd/MM/yyyy",null),
                IsActive = ministryminister.IsActive,               
                OrderID = ministryminister.OrderID,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        public static MinsitryMinisterViewModel ToViewModel1(this mMinsitryMinister ministryminister, List<mMember> Members, List<DiaryModel> Ministries, List<mAssembly> Assemblies, string Mode)
        {
            return new MinsitryMinisterViewModel
            {
                AssemblyID = ministryminister.AssemblyID,
                MinistryID = ministryminister.MinistryID,
                MinsitryMinistersID=ministryminister.MinsitryMinistersID,
                MinisterName=ministryminister.MinisterName,
                MinisterNameLocal=ministryminister.MinisterNameLocal,
                MemberCode=ministryminister.MemberCode,
                MemberStartDate=ministryminister.MemberStartDate!=null?ministryminister.MemberStartDate.Value.ToString("dd/MM/yyyy"):"",
                MemberEndDate=ministryminister.MemberEndDate!=null?ministryminister.MemberEndDate.Value.ToString("dd/MM/yyyy"):"",
                GetAssemblyName = ministryminister.GetAssemblyName,
                GetMemberName = ministryminister.GetMemberName,
                GetMinistryName = ministryminister.GetMinistryName,
                IsActive = ministryminister.IsActive ?? false,               
                OrderID = ministryminister.OrderID,                
                Ministry = new SelectList(Ministries, "MinistryID", "MinistryName"),
                MemberNames = new SelectList(Members, "MemberCode", "Name"),
                Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }

    }
}