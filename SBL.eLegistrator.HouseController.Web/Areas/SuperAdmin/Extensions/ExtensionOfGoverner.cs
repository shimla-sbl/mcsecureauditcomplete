﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using SBL.DomainModel.Models.Governor;
using SBL.DomainModel.Models.States;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfGoverner
    {


        public static IEnumerable<GovernerViewModel> ToViewModel(this IEnumerable<mGoverner> model)
        {

            var ViewModel = model.Select(memb => new GovernerViewModel()
            {
                mGovernerID = memb.mGovernerID,
                Prefix = memb.Prefix,
                mStateID = memb.mStateID,
                Name = memb.Name,
                NameLocal = memb.NameLocal,
                MemberCode = memb.MemberCode,
                District = memb.District,
                ShimlaAddress = memb.ShimlaAddress,
                Active = memb.Active ?? false,
                PermanentAddress = memb.PermanentAddress,
                Sex = memb.Sex,
                GetStateName = memb.GetStateName,

                TelOffice = memb.TelOffice,
                TelResidence = memb.TelResidence,
                Mobile = memb.Mobile,
                Email = memb.Email,
                Description = memb.Description,
                FatherName = memb.FatherName,
                LoginID = memb.LoginID,
                
                CmRefnicVipCode = memb.CmRefnicVipCode,
                
                FullImage = memb.FileName,
                ThumbImage = memb.FileName,
                FileLocation = memb.ImageAcessurl,
                ImageLocation = memb.ImageAcessurl
                

            });
            return ViewModel;
        }



        public static mGoverner ToDomainModel(this GovernerViewModel model)
        {
            return new mGoverner
            {



                mGovernerID = model.mGovernerID,
                Prefix = model.Prefix,
                mStateID = model.mStateID,
                Name = model.Name,
                NameLocal = model.NameLocal,
                MemberCode = model.MemberCode,
                District = model.District,
                ShimlaAddress = model.ShimlaAddress,

                PermanentAddress = model.PermanentAddress,
                Sex = model.Sex,
                GetStateName = model.GetStateName,

                TelOffice = model.TelOffice,
                TelResidence = model.TelResidence,
                Mobile = model.Mobile,
                Email = model.Email,
                Description = model.Description,
                FatherName = model.FatherName,
                LoginID = model.LoginID,

                CmRefnicVipCode = model.CmRefnicVipCode,
                      
                Active = model.Active,
                
                FileName = model.FullImage,
                FilePath = model.FileLocation,
                ThumbName = model.ThumbName,
                
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        public static GovernerViewModel ToViewModel1(this mGoverner model, List<mStates> StateName, string Mode)
        {
            return new GovernerViewModel
            {
                mGovernerID = model.mGovernerID,
                Prefix = model.Prefix,
                mStateID = model.mStateID,
                Name = model.Name,
                NameLocal = model.NameLocal,
                MemberCode = model.MemberCode,
                District = model.District,
                ShimlaAddress = model.ShimlaAddress,

                PermanentAddress = model.PermanentAddress,
                Sex = model.Sex,
                GetStateName = model.GetStateName,

                TelOffice = model.TelOffice,
                TelResidence = model.TelResidence,
                Mobile = model.Mobile,
                Email = model.Email,
                Description = model.Description,
                FatherName = model.FatherName,
                LoginID = model.LoginID,

                CmRefnicVipCode = model.CmRefnicVipCode,
                
                Active = model.Active ?? false,
                
                
                
                
                FullImage = model.FileName,
                ThumbImage = model.FileName,
                FileLocation = model.FilePath,
                
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                
                StateNames = new SelectList(StateName, "mStateID", "StateName"),
                
                Mode = Mode

            };
        }




    }
}