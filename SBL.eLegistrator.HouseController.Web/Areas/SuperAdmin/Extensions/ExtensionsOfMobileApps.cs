﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfMobileApps
    {
        public static IEnumerable<MobileAppsViewModel> ToViewModel(this IEnumerable<mMobileApps> model)
        {

            var ViewModel = model.Select(mobSetting => new MobileAppsViewModel()
            {
                SlNo = mobSetting.SlNo,
                AppName = mobSetting.AppName,
                Description = mobSetting.Description,
                Code=mobSetting.Code
            });
            return ViewModel;
        }

        public static mMobileApps ToDomainModel(this MobileAppsViewModel model)
        {
            try
            {
                return new mMobileApps
                {
                    SlNo = model.SlNo,
                    AppName = model.AppName,
                    Description = model.Description,
                    Code=model.Code,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static MobileAppsViewModel ToViewModel1(this mMobileApps model, string Mode)
        {
            return new MobileAppsViewModel
            {
                SlNo = model.SlNo,
                AppName = model.AppName,
                Description = model.Description,
                Code = model.Code,
                Mode = Mode
            };
        }

    }
}