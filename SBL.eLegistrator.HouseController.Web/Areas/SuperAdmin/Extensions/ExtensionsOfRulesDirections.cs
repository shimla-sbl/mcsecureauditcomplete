﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfRulesDirections
    {

        public static IEnumerable<RulesDirectionsViewModel> ToViewModel(this IEnumerable<tRulesDirections> model)
        {

            var ViewModel = model.Select(mobSetting => new RulesDirectionsViewModel()
            {

                SlNo = mobSetting.SlNo,
                Title = mobSetting.Title,
                PdfPath = mobSetting.PdfPath,
                Type=mobSetting.Type
            });
            return ViewModel;
        }

        public static tRulesDirections ToDomainModel(this RulesDirectionsViewModel model)
        {
            try
            {
                return new tRulesDirections
                {
                    SlNo = model.SlNo,
                    Title = model.Title,
                    PdfPath = model.PdfPath,
                    Type=model.Type,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static RulesDirectionsViewModel ToViewModel1(this tRulesDirections model, string Mode)
        {
            return new RulesDirectionsViewModel
            {
                SlNo = model.SlNo,
                Title = model.Title,
                PdfPath = model.PdfPath,
                DownloadPath=model.PdfPath,
                Type = model.Type,
                Mode = Mode
            };
        }
    }
}