﻿using SBL.DomainModel.Models.Grievance;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfActiontype
    {
        public static IEnumerable<ActionTypeViewModel> ToViewModel(this IEnumerable<ActionType> model)
        {

            var ViewModel = model.Select(actionType => new ActionTypeViewModel()
            {
                SlNo = actionType.SlNo,
                StatusId = actionType.StatusId,
                StatusCode = actionType.StatusCode
            });
            return ViewModel;
        }

        public static ActionType ToDomainModel(this ActionTypeViewModel model)
        {
            try
            {
                return new ActionType
                {
                    SlNo = model.SlNo,
                    StatusId = model.StatusId,
                    StatusCode = model.StatusCode,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static ActionTypeViewModel ToViewModel1(this ActionType model, string Mode)
        {
            return new ActionTypeViewModel
            {
                SlNo = model.SlNo,
                StatusId = model.StatusId,
                StatusCode = model.StatusCode,
                Mode = Mode
            };
        }
    }
}