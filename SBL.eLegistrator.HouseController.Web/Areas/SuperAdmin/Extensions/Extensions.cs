﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Forms;
using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Party;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.States;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Authority;
using SBL.DomainModel.Models.Secretory;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.UserModule;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserAction;
using SBL.DomainModel.Models.HOD;
using SBL.DomainModel.Models.ROM;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.MemberSMSQuota;
using SBL.DomainModel.Models.CutMotionDemand;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.ConstituencyVS;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;


namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
	public static class Extensions
	{
		public static tCommittee ToDomainModel(this CommitteeViewModel model)
		{
			return new tCommittee
			{
				CommitteeId = Convert.ToInt32(model.CommitteeId),
				AssemblyID = model.AssemblyID,
				CommitteeTypeId = model.CommitteeTypeId,
				ParentId = model.ParentId,
				CommitteeName = model.CommitteeName,
				DateOfCreation = model.DateofCreation,
				IsActive = model.IsActive,
				AutoSchedule = model.AutoSchedule,
				Remark = model.Remark,				 
				CommitteeYear = model.CommitteeYear,
                Abbreviation=model.Abbreviation
			};
		}

		public static mConstituency ToDomainModel(this ConstituencyViewModel model)
		{
			return new mConstituency
			{
				ConstituencyID = (Int32)(model.ConstituencyID != null ? model.ConstituencyID : 0),
				ConstituencyName_Local = model.ConstituencyName_Local,
				ConstituencyName = model.ConstituencyName,
				AssemblyID = model.AssemblyID,
				ConstituencyCode = model.ConstituencyCode,
				Active = model.Active,
                ReservedCategory = model.ReservedCategory,
				DistrictCode = model.DistrictCode,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName
			};
		}


        public static mConstituency ToDomainnModel(this ConstituencyCopyViewModel model)
        {
            return new mConstituency
            {
                toAssemblyID = model.toAssemblyID,

                AssemblyID = model.fromAssemblyID,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }



		public static IEnumerable<ConstituencyViewModel> ToViewModel(this IEnumerable<mConstituency> model)
		{

			var ViewModel = model.Select(consti => new ConstituencyViewModel()
	   {
		   ConstituencyCode = consti.ConstituencyCode,
		   ConstituencyName_Local = consti.ConstituencyName_Local,
		   ConstituencyName = consti.ConstituencyName,
		   AssemblyID = consti.AssemblyID,
		   ConstituencyID = consti.ConstituencyID,
		   ConstituencyCreatedDate = consti.ConstituencyCreatedDate,
		   DistrictCode = consti.DistrictCode,
           ReservedCategory = consti.ReservedCategory,
		   Active = consti.Active ?? false,
		   LGDistrictCode = consti.LGDistrictCode,
		   ConstituencyCode_CmRefnic = consti.ConstituencyCode_CmRefnic,
		   GetAssemblyName = consti.GetAssemblyName,
		   GetDistrictName = consti.GetDistrictName,
           GetReservedCategoryName = consti.GetReservedCategoryName,
	   });
			return ViewModel;
		}

		public static string NumbersToWords(this int inputNumber)
		{
			int inputNo = inputNumber;

			if (inputNo == 0)
				return "Zero";

			int[] numbers = new int[4];
			int first = 0;
			int u, h, t;
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			if (inputNo < 0)
			{
				sb.Append("Minus ");
				inputNo = -inputNo;
			}

			string[] words0 = {"" ,"One ", "Two ", "Three ", "Four ",
            "Five " ,"Six ", "Seven ", "Eight ", "Nine "};
			string[] words1 = {"Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ",
            "Fifteen ","Sixteen ","Seventeen ","Eighteen ", "Nineteen "};
			string[] words2 = {"Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ",
            "Seventy ","Eighty ", "Ninety "};
			string[] words3 = { "Thousand ", "Lakh ", "Crore " };

			numbers[0] = inputNo % 1000; // units
			numbers[1] = inputNo / 1000;
			numbers[2] = inputNo / 100000;
			numbers[1] = numbers[1] - 100 * numbers[2]; // thousands
			numbers[3] = inputNo / 10000000; // crores
			numbers[2] = numbers[2] - 100 * numbers[3]; // lakhs

			for (int i = 3; i > 0; i--)
			{
				if (numbers[i] != 0)
				{
					first = i;
					break;
				}
			}
			for (int i = first; i >= 0; i--)
			{
				if (numbers[i] == 0) continue;
				u = numbers[i] % 10; // ones
				t = numbers[i] / 10;
				h = numbers[i] / 100; // hundreds
				t = t - 10 * h; // tens
				if (h > 0) sb.Append(words0[h] + "Hundred ");
				if (u > 0 || t > 0)
				{
					if (h > 0 || i == 0) sb.Append("and ");
					if (t == 0)
						sb.Append(words0[u]);
					else if (t == 1)
						sb.Append(words1[u]);
					else
						sb.Append(words2[t - 2] + words0[u]);
				}
				if (i != 0) sb.Append(words3[i - 1]);
			}
			return sb.ToString().TrimEnd();
		}

        public static ConstituencyViewModel ToViewModel1(this mConstituency model, List<mAssembly> Assemblies, List<DistrictModel> Districts, List<mConstituencyReservedCategory> Category, string Mode)
		{
			return new ConstituencyViewModel
			{
				ConstituencyCode = model.ConstituencyCode,
				ConstituencyName_Local = model.ConstituencyName_Local,
				ConstituencyName = model.ConstituencyName,
				AssemblyID = model.AssemblyID,
				ConstituencyID = model.ConstituencyID,
				ConstituencyCreatedDate = model.ConstituencyCreatedDate,
				DistrictCode = model.DistrictCode,
                ReservedCategory = model.ReservedCategory,
				Active = model.Active ?? false,
				Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
				District = new SelectList(Districts, "DistrictCode", "DistrictName"),
                ReservedCategoryName = new SelectList(Category, "ConstituencyCategoryId", "ConstituencyCategoryCode"),
				Mode = Mode

			};
		}

		public static IEnumerable<CommiteeTypeViewModel> CommiteeTypeViewModel(this IEnumerable<mCommitteeType> model)
		{
			var ViewModel = model.Select(s => new CommiteeTypeViewModel()
			{
				CommitteeTypeId = s.CommitteeTypeId,
				CommitteeTypeName = s.CommitteeTypeName,
				Description = s.Description,
				IsSubCommittee = s.IsSubCommittee
			});
			return ViewModel;
		}
		public static mCommitteeType CommiteeToDomainModel(this  CommiteeTypeViewModel model)
		{
			return new mCommitteeType
			{
				CommitteeTypeId = model.CommitteeTypeId,
				CommitteeTypeName = model.CommitteeTypeName,
				Description = model.Description,
				IsSubCommittee = model.IsSubCommittee,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName


			};
		}

		public static CommiteeTypeViewModel SingleCommiteeType(this mCommitteeType model, string Mode)
		{

			var SingleCommiteeType = new CommiteeTypeViewModel()
			{
				CommitteeTypeId = model.CommitteeTypeId,
				CommitteeTypeName = model.CommitteeTypeName,
				Description = model.Description,
				IsSubCommittee = model.IsSubCommittee,
				Mode = Mode
			};
			return SingleCommiteeType;


		}


		public static mParty ToDomainModel(this PartyViewModel model)
		{
			return new mParty
			{

				PartyID = model.PartyID,
				PartyCode = model.PartyCode,
				PartyName_Local = model.PartyName_Local,
				PartyName = model.PartyName,
				Photo = model.Photo,
				IsActive = model.IsActive,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		public static List<PartyViewModel> ToViewModel(this IEnumerable<mParty> model)
		{

			var ViewModel = model.Select(part => new PartyViewModel()
			{
				PartyID = part.PartyID,
				PartyCode = part.PartyCode.ToString(),
				PartyName_Local = part.PartyName_Local,
				PartyName = part.PartyName,
				Photo = part.Photo,
				IsActive = part.IsActive ?? false,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}



		public static PartyViewModel ToViewModel1(this mParty model, string Mode)
		{
			return new PartyViewModel
			{
				PartyID = model.PartyID,
				PartyCode = model.PartyCode.ToString(),
				PartyName_Local = model.PartyName_Local,
				PartyName = model.PartyName,
				Photo = model.Photo,
				IsActive = model.IsActive ?? false,
				//CreatedBy = model.CreatedBy,
				//CreatedDate = model.CreatedDate,
				//ModifiedBy = model.ModifiedBy,
				//ModifiedWhen = model.ModifiedWhen,
				Mode = Mode

			};
		}





		public static mBillType ToDomainModel(this BillViewModel model)
		{
			return new mBillType
			{

				BillTypeId = model.BillTypeId,
				//PartyCode = model.PartyCode,
				BillType = model.BillType,
				Description = model.Description,
				IsActive = model.IsActive,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		public static List<BillViewModel> ToViewModel(this IEnumerable<mBillType> model)
		{

			var ViewModel = model.Select(part => new BillViewModel()
			{
				BillTypeId = part.BillTypeId,
				//PartyCode = part.PartyCode.ToString(),
				BillType = part.BillType,
				Description = part.Description,
				IsActive = part.IsActive ?? false,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}



		public static BillViewModel ToViewModel1(this mBillType model, string Mode)
		{
			return new BillViewModel
			{
				BillTypeId = model.BillTypeId,
				// PartyCode = model.PartyCode.ToString(),
				BillType = model.BillType,
				Description = model.Description,
				IsActive = model.IsActive ?? false,
				//CreatedBy = model.CreatedBy,
				//CreatedDate = model.CreatedDate,
				//ModifiedBy = model.ModifiedBy,
				//ModifiedWhen = model.ModifiedWhen,
				Mode = Mode

			};
		}

		public static IEnumerable<NoticeTypeViewModel> NoticeTypeViewModel(this IEnumerable<mNoticeType> model)
		{
			var ViewModel = model.Select(s => new NoticeTypeViewModel()
			{
				NoticeTypeID = s.NoticeTypeID,
				NoticeTypeName = s.NoticeTypeName,
				NoticeTypeNameLocal = s.NoticeTypeNameLocal,
				IsActive = s.IsActive
			});
			return ViewModel;
		}
		public static mNoticeType NoticeTypeToDomainModel(this  NoticeTypeViewModel model)
		{
			return new mNoticeType
			{
				NoticeTypeID = model.NoticeTypeID,
				NoticeTypeName = model.NoticeTypeName,
				NoticeTypeNameLocal = model.NoticeTypeNameLocal,
				IsActive = model.IsActive


			};
		}
		public static NoticeTypeViewModel SingleNoticeType(this mNoticeType model)
		{

			var SingleCommiteeType = new NoticeTypeViewModel()
			{
				NoticeTypeID = model.NoticeTypeID,
				NoticeTypeName = model.NoticeTypeName,
				NoticeTypeNameLocal = model.NoticeTypeNameLocal,
				IsActive = model.IsActive
			};
			return SingleCommiteeType;


		}
		public static IEnumerable<QuestionRulesViewModel> QuestionRulesViewModel(this IEnumerable<mQuestionRules> model)
		{
			var ViewModel = model.Select(s => new QuestionRulesViewModel()
			{
				QuestionRuleId = s.QuestionRuleId,
				Rules = s.Rules,
				RulesH = s.RulesH,
				Remarks = s.Remarks ?? false,
				IsTextUse = s.IsTextUse ?? false
			});
			return ViewModel;
		}
		public static mQuestionRules QuestionRulesToDomainModel(this  QuestionRulesViewModel model)
		{
			return new mQuestionRules
			{
				QuestionRuleId = model.QuestionRuleId,
				Rules = model.Rules,
				RulesH = model.RulesH,
				Remarks = model.Remarks,
				IsTextUse = model.IsTextUse,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName


			};
		}
		public static QuestionRulesViewModel SingleQuestionRules(this mQuestionRules model)
		{

			var SingleQuestionRules = new QuestionRulesViewModel()
			{
				QuestionRuleId = model.QuestionRuleId,
				Rules = model.Rules,
				RulesH = model.RulesH,
				Remarks = model.Remarks ?? false,
				IsTextUse = model.IsTextUse ?? false,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName
			};
			return SingleQuestionRules;


		}


		public static mStates ToDomainModel(this StateViewModel model)
		{
			return new mStates
			{

				mStateID = model.mStateID,
				StateAbbreviation = model.StateAbbreviation,
				StateCode = model.StateCode,
				StateName = model.StateName,
				StateNameLocal = model.StateNameLocal,
				IsActive = model.IsActive,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		public static List<StateViewModel> ToViewModel(this IEnumerable<mStates> model)
		{

			var ViewModel = model.Select(part => new StateViewModel()
			{
				mStateID = part.mStateID,
				StateAbbreviation = part.StateAbbreviation,
				StateCode = part.StateCode,
				StateName = part.StateName,
				StateNameLocal = part.StateNameLocal,
				IsActive = part.IsActive ?? false,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}



		public static StateViewModel ToViewModel1(this mStates model, string Mode)
		{
			return new StateViewModel
			{
				mStateID = model.mStateID,
				StateAbbreviation = model.StateAbbreviation,
				StateCode = model.StateCode,
				StateName = model.StateName,
				StateNameLocal = model.StateNameLocal,
				IsActive = model.IsActive ?? false,

				//CreatedBy = model.CreatedBy,
				//CreatedDate = model.CreatedDate,
				//ModifiedBy = model.ModifiedBy,
				//ModifiedWhen = model.ModifiedWhen,
				Mode = Mode

			};
		}



		public static DistrictModel ToDomainModel(this DistrictViewModel model)
		{
			return new DistrictModel
			{

				DistrictId = model.DistrictId,
				//PartyCode = part.PartyCode.ToString(),
				StateCode = model.StateCode,
				DistrictCode = Convert.ToInt32(model.DistrictCode),
				DistrictAbbreviation = model.DistrictAbbreviation,
				DistrictName = model.DistrictName,
				DistrictNameLocal = model.DistrictNameLocal,
				IsActive = model.IsActive,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		public static List<DistrictViewModel> ToViewModel(this IEnumerable<DistrictModel> model)
		{

			var ViewModel = model.Select(part => new DistrictViewModel()
			{
				DistrictId = part.DistrictId,
				//PartyCode = part.PartyCode.ToString(),
				StateCode = part.StateCode,
				DistrictCode = part.DistrictCode.ToString(),
				DistrictAbbreviation = part.DistrictAbbreviation,
				DistrictName = part.DistrictName,
				DistrictNameLocal = part.DistrictNameLocal,
				IsActive = part.IsActive,
				GetStateName = part.GetStateName,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}



		public static DistrictViewModel ToViewModel1(this DistrictModel model, List<mStates> States, string Mode)
		{
			return new DistrictViewModel
			{
				DistrictId = model.DistrictId,
				StateCode = model.StateCode,
				DistrictCode = model.DistrictCode.ToString(),
				DistrictAbbreviation = model.DistrictAbbreviation,
				DistrictName = model.DistrictName,
				DistrictNameLocal = model.DistrictNameLocal,
				State = new SelectList(States, "mStateID", "StateAbbreviation"),
				IsActive = model.IsActive,

				//CreatedBy = model.CreatedBy,
				//CreatedDate = model.CreatedDate,
				//ModifiedBy = model.ModifiedBy,
				//ModifiedWhen = model.ModifiedWhen,
				Mode = Mode

			};
		}


		public static mPaperCategoryType ToDomainModel(this PaperCategoryTypeViewModel model)
		{
			return new mPaperCategoryType
			{

				PaperCategoryTypeId = model.PaperCategoryTypeId,
				//PartyCode = part.PartyCode.ToString(),

				Name = model.Name,
				NameLocal = model.NameLocal,
				IsActive = model.IsActive,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		public static List<PaperCategoryTypeViewModel> ToViewModel(this IEnumerable<mPaperCategoryType> model)
		{

			var ViewModel = model.Select(part => new PaperCategoryTypeViewModel()
			{
				PaperCategoryTypeId = part.PaperCategoryTypeId,
				//PartyCode = part.PartyCode.ToString(),

				Name = part.Name,
				NameLocal = part.NameLocal,
				IsActive = part.IsActive ?? false,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}



		public static PaperCategoryTypeViewModel ToViewModel1(this mPaperCategoryType model, string Mode)
		{
			return new PaperCategoryTypeViewModel
			{
				PaperCategoryTypeId = model.PaperCategoryTypeId,
				//PartyCode = part.PartyCode.ToString(),

				Name = model.Name,
				NameLocal = model.NameLocal,
				IsActive = model.IsActive ?? false,
				//CreatedBy = model.CreatedBy,
				//CreatedDate = model.CreatedDate,
				//ModifiedBy = model.ModifiedBy,
				//ModifiedWhen = model.ModifiedWhen,
				Mode = Mode

			};
		}







		public static mDepartment ToDomainModel(this DepartmentViewModel model)
		{
			return new mDepartment
			{
				//departmentId = model.departmentId,
				deptId = model.deptId,
				deptname = model.deptname,
				deptnameLocal = model.deptnameLocal,
				IsActive = model.IsActive,
				deptabbr = model.deptabbr,
				//RowNumber = model.RowNumber,
				//eSamadhanNote = model.eSamadhanNote,
				//eSamadhanOnline = model.eSamadhanOnline,
				//eSameekshaOnlineYN = model.eSameekshaOnlineYN,
				//eSameekshaWorkCode = model.eSameekshaWorkCode,
				deptabbrLocal = model.deptabbrLocal,
				//eSameekshaWorkCodeYear = model.eSameekshaWorkCodeYear,

				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		public static List<DepartmentViewModel> ToViewModel(this IEnumerable<mDepartment> model)
		{

			var ViewModel = model.Select(part => new DepartmentViewModel()
			{
				//departmentId = part.departmentId,
				deptId = part.deptId,
				deptname = part.deptname,
				deptnameLocal = part.deptnameLocal,
				IsActive = part.IsActive ?? false,
				deptabbr = part.deptabbr,
				//RowNumber = part.RowNumber,
				//eSamadhanNote = part.eSamadhanNote,
				//eSamadhanOnline = part.eSamadhanOnline,
				//eSameekshaOnlineYN = part.eSameekshaOnlineYN,
				//eSameekshaWorkCode = part.eSameekshaWorkCode,
				deptabbrLocal = part.deptabbrLocal,
				//eSameekshaWorkCodeYear = part.eSameekshaWorkCodeYear,

				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}



		public static DepartmentViewModel ToViewModel1(this mDepartment model, string Mode)
		{
			return new DepartmentViewModel
			{
				//departmentId = model.departmentId,
				deptId = model.deptId,
				deptname = model.deptname,
				deptnameLocal = model.deptnameLocal,
				IsActive = model.IsActive ?? false,
				deptabbr = model.deptabbr,
				//RowNumber = model.RowNumber,
				//eSamadhanNote = model.eSamadhanNote,
				//eSamadhanOnline = model.eSamadhanOnline,
				//eSameekshaOnlineYN = model.eSameekshaOnlineYN,
				//eSameekshaWorkCode = model.eSameekshaWorkCode,
				deptabbrLocal = model.deptabbrLocal,
				//eSameekshaWorkCodeYear = model.eSameekshaWorkCodeYear,

				//CreatedBy = model.CreatedBy,
				//CreatedDate = model.CreatedDate,
				//ModifiedBy = model.ModifiedBy,
				//ModifiedWhen = model.ModifiedWhen,
				Mode = Mode

			};
		}



		public static mAuthority ToDomainModel(this AuthorityViewModel model)
		{
			return new mAuthority
			{

				AuthorityId = model.AuthorityId,

				AuthorityName = model.AuthorityName,
				PriFixName = model.PriFixName,
				Name = model.Name,
				Name_Local = model.Name_Local,
				FatherName = model.FatherName,
				FatherName_Local = model.FatherName_Local,
				DateOfBirth = model.DateOfBirth,
				MaritalStatus = model.MaritalStatus,
				SpouseName = model.SpouseName,
				SpouseName_Local = model.SpouseName_Local,
				EducationalQualification = model.EducationalQualification,
				PresentAddress = model.PresentAddress,
				PermanentAddress = model.PermanentAddress,
				Postings = model.Postings,
				Training = model.Training,
				CountriesVisited = model.CountriesVisited,
				SpecialInterest = model.SpecialInterest,
				MeetingsAttended = model.MeetingsAttended,
				DesignationDescriptions = model.DesignationDescriptions,
				Photo = model.Photo,
				DateofMarriage = model.DateofMarriage,
                Email = model.Email,
                Mobile = model.Mobile,
				IsActive = model.IsActive,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,

                FileName = model.FullImage,
                FilePath = model.FileLocation,
                ThumbName = model.ThumbName,

                SecretoryId = model.SecretoryId,
                GetSecretoryName = model.GetSecretoryName,


			};
		}


		public static List<AuthorityViewModel> ToViewModel(this IEnumerable<mAuthority> model)
		{

			var ViewModel = model.Select(part => new AuthorityViewModel()
			{
				AuthorityId = part.AuthorityId,
				AuthorityName = part.AuthorityName,
				PriFixName = part.PriFixName,
				Name = part.Name,
				Name_Local = part.Name_Local,
				FatherName = part.FatherName,
				FatherName_Local = part.FatherName_Local,
				DateOfBirth = part.DateOfBirth,
				MaritalStatus = part.MaritalStatus,
				SpouseName = part.SpouseName,
				SpouseName_Local = part.SpouseName_Local,
				EducationalQualification = part.EducationalQualification,
				PresentAddress = part.PresentAddress,
				PermanentAddress = part.PermanentAddress,
				Postings = part.Postings,
				Training = part.Training,
				CountriesVisited = part.CountriesVisited,
				SpecialInterest = part.SpecialInterest,
				MeetingsAttended = part.MeetingsAttended,
				DesignationDescriptions = part.DesignationDescriptions,
				Photo = part.Photo,
				DateofMarriage = part.DateofMarriage,
                Email = part.Email,
                Mobile = part.Mobile,
				IsActive = part.IsActive ?? false,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,

                FullImage = part.FileName,
                ThumbImage = part.FileName,
                FileLocation = part.ImageAcessurl,
                ImageLocation = part.ImageAcessurl,

                SecretoryId = part.SecretoryId,
                GetSecretoryName = part.GetSecretoryName

			});
			return ViewModel.ToList();
		}



        public static AuthorityViewModel ToViewModel1(this mAuthority model, List<mSecretory> Secretery, string Mode)
		{
			return new AuthorityViewModel
			{
				AuthorityId = model.AuthorityId,
				AuthorityName = model.AuthorityName,
				PriFixName = model.PriFixName,
				Name = model.Name,
				Name_Local = model.Name_Local,
				FatherName = model.FatherName,
				FatherName_Local = model.FatherName_Local,
				DateOfBirth = model.DateOfBirth,
				MaritalStatus = model.MaritalStatus,
				SpouseName = model.SpouseName,
				SpouseName_Local = model.SpouseName_Local,
				EducationalQualification = model.EducationalQualification,
				PresentAddress = model.PresentAddress,
				PermanentAddress = model.PermanentAddress,
				Postings = model.Postings,
				Training = model.Training,
				CountriesVisited = model.CountriesVisited,
				SpecialInterest = model.SpecialInterest,
				MeetingsAttended = model.MeetingsAttended,
				DesignationDescriptions = model.DesignationDescriptions,
				Photo = model.Photo,
				DateofMarriage = model.DateofMarriage,
                Email=model.Email,
                Mobile=model.Mobile,
				IsActive = model.IsActive ?? false,
				//CreatedBy = model.CreatedBy,
				//CreatedDate = model.CreatedDate,
				//ModifiedBy = model.ModifiedBy,
				//ModifiedWhen = model.ModifiedWhen,
				Mode = Mode,
                FullImage = model.FileName,
                ThumbImage = model.FileName,
                FileLocation = model.FilePath,

                SecretoryId = model.SecretoryId,
                GetSecretoryName = model.GetSecretoryName,
                Secretory = new SelectList(Secretery, "SecretoryID", "SecretoryName"),

			};
		}




		public static mSecretory ToDomainModel(this SecretoryViewModel model)
		{
			return new mSecretory
			{

				SecretoryID = model.SecretoryID,
				EmpCode = model.EmpCode,
				DeptID = model.DeptID,
				Prefix = model.Prefix,
				SecretoryName = model.SecretoryName,
				SecretoryNameLocal = model.SecretoryNameLocal,
				DesignationDescription = model.DesignationDescription,
				IASRank = model.IASRank,
				Email = model.Email,
				PhoneResidence = model.PhoneResidence,
				PhoneOffice = model.PhoneOffice,
				PhoneOfficeAlternate = model.PhoneOfficeAlternate,
				Mobile = model.Mobile,
				AadhaarId = model.AadhaarId,
				OrderId = model.OrderId,
				IsActive = model.IsActive,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		public static List<SecretoryViewModel> ToViewModel(this IEnumerable<mSecretory> model)
		{

			var ViewModel = model.Select(part => new SecretoryViewModel()
			{
				SecretoryID = part.SecretoryID,
				EmpCode = part.EmpCode,
				DeptID = part.DeptID,
				Prefix = part.Prefix,
				SecretoryName = part.SecretoryName,
				SecretoryNameLocal = part.SecretoryNameLocal,
				DesignationDescription = part.DesignationDescription,
				IASRank = part.IASRank,
				Email = part.Email,
				PhoneResidence = part.PhoneResidence,
				PhoneOffice = part.PhoneOffice,
				PhoneOfficeAlternate = part.PhoneOfficeAlternate,
				Mobile = part.Mobile,
				AadhaarId = part.AadhaarId,
				OrderId = part.OrderId,
				IsActive = part.IsActive ?? false,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
				GetDepartmentName = part.GetDepartmentName
			});
			return ViewModel.ToList();
		}



		public static SecretoryViewModel ToViewModel1(this mSecretory model, List<mDepartment> Departments, string Mode)
		{
			return new SecretoryViewModel
			{
				SecretoryID = model.SecretoryID,
				EmpCode = model.EmpCode,
				DeptID = model.DeptID,
				Prefix = model.Prefix,
				SecretoryName = model.SecretoryName,
				SecretoryNameLocal = model.SecretoryNameLocal,
				DesignationDescription = model.DesignationDescription,
				IASRank = model.IASRank,
				Email = model.Email,
				PhoneResidence = model.PhoneResidence,
				PhoneOffice = model.PhoneOffice,
				PhoneOfficeAlternate = model.PhoneOfficeAlternate,
				Mobile = model.Mobile,
				AadhaarId = model.AadhaarId,
				OrderId = model.OrderId,
				IsActive = model.IsActive ?? false,
				Department = new SelectList(Departments, "deptId", "deptname"),
				Mode = Mode

			};
		}



		//UserManagement

		public static List<mUserType> ToViewUserType(this IEnumerable<mUserType> model)
		{

			var ViewModel = model.Select(part => new mUserType()
			{
				UserTypeID = part.UserTypeID,
				UserTypeName = part.UserTypeName,
				UserTypeNameLocal = part.UserTypeNameLocal,
				ModifiedWhen = part.ModifiedWhen,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}


		public static List<mSubUserType> ToViewSubUserType(this IEnumerable<mSubUserType> model)
		{

			var ViewModel = model.Select(part => new mSubUserType()
			{
				SubUserTypeID = part.SubUserTypeID,
				SubUserTypeName = part.SubUserTypeName,
				SubUserTypeDescription = part.SubUserTypeDescription,
				SubUserTypeNameLocal = part.SubUserTypeNameLocal,
				ModifiedWhen = part.ModifiedWhen,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}


		public static mUserType ToViewUserTypeModel(this mUserType model, string Mode)
		{
			return new mUserType
			{
				UserTypeID = model.UserTypeID,
				UserTypeName = model.UserTypeName,
				UserTypeNameLocal = model.UserTypeNameLocal,
				IsActive = model.IsActive,
				ModifiedBy = CurrentSession.UserName,
				Mode = Mode

			};
		}

		public static mUserType ToUserTypeModel(this mUserType model)
		{
			return new mUserType
			{
				UserTypeID = model.UserTypeID,
				UserTypeName = model.UserTypeName,
				UserTypeNameLocal = model.UserTypeNameLocal,
				IsActive = model.IsActive,
				ModifiedWhen = DateTime.Now,
				ModifiedBy = CurrentSession.UserName,

			};
		}




		public static mEmployee ToDomainModel(this EmployeeViewModel model)
		{
			return new mEmployee
			{
				Id = model.Id,
				empcd = model.empcd,
				officeid = model.officeid,
				empfname = model.empfname,
				empmname = model.empmname,
				emplname = model.emplname,
				empnamelocal = model.empnamelocal,
				empdob = string.IsNullOrEmpty(model.empdob) ? DateTime.Now : DateTime.ParseExact(model.empdob, "dd/MM/yyyy", null),
				empfmh = model.empfmh,
				empfmhname = model.empfmhname,
				empspouse = model.empspouse,
				empreligion = model.empreligion,
				empphoto = model.empphoto,
				empgender = model.empgender,
				deptid = model.deptid,
				IsActive = model.IsActive,

				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		public static List<EmployeeViewModel> ToViewModel(this IEnumerable<mEmployee> model)
		{

			var ViewModel = model.Select(part => new EmployeeViewModel()
			{
				Id = part.Id,
				empcd = part.empcd,
				officeid = part.officeid,
				empfname = part.empfname,
				empmname = part.empmname,
				emplname = part.emplname,
				empnamelocal = part.empnamelocal,
				empdob = part.empdob.ToString(),
				empfmh = part.empfmh,
				empfmhname = part.empfmhname,
				empspouse = part.empspouse,
				empreligion = part.empreligion,
				empphoto = part.empphoto,
				empgender = part.empgender,
				deptid = part.deptid,
				IsActive = part.IsActive ?? false,
				GetDepartmentName = part.GetDepartmentName,

				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
			});
			return ViewModel.ToList();
		}



		public static EmployeeViewModel ToViewModel1(this mEmployee model, List<mDepartment> Departments, string Mode)
		{
			return new EmployeeViewModel
			{
				Id = model.Id,
				empcd = model.empcd,
				officeid = model.officeid,
				empfname = model.empfname,
				empmname = model.empmname,
				emplname = model.emplname,
				empnamelocal = model.empnamelocal,
				empdob = model.empdob != null ? model.empdob.Value.ToString("dd/MM/yyyy") : "",
				empfmh = model.empfmh,
				empfmhname = model.empfmhname,
				empspouse = model.empspouse,
				empreligion = model.empreligion,
				empphoto = model.empphoto,
				empgender = model.empgender,
				deptid = model.deptid,
				IsActive = model.IsActive ?? false,
				Department = new SelectList(Departments, "deptId", "deptname"),

				Mode = Mode

			};
		}

		public static List<HODViewModel> ToViewModel(this IEnumerable<mHOD> model)
		{

			var ViewModel = model.Select(part => new HODViewModel()
			{
				HODID = part.HODID,
				EmpCode = part.EmpCode,
				DeptID = part.DeptID,
				Prefix = part.Prefix,
				HODName = part.HODName,
				HODNameLocal = part.HODNameLocal,
				DesignationDescription = part.DesignationDescription,
				IASRank = part.IASRank,
				Email = part.Email,
				PhoneResidence = part.PhoneResidence,
				PhoneOffice = part.PhoneOffice,
				PhoneOfficeAlternate = part.PhoneOfficeAlternate,
				Mobile = part.Mobile,
				AadhaarId = part.AadhaarId,
				OrderId = part.OrderId,
				IsActive = part.IsActive ?? false,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
				GetDepartmentName = part.GetDepartmentName
			});
			return ViewModel.ToList();
		}

		public static HODViewModel ToViewModel1(this mHOD model, List<mDepartment> Departments, string Mode)
		{
			return new HODViewModel
			{
				HODID = model.HODID,
				EmpCode = model.EmpCode,
				DeptID = model.DeptID,
				Prefix = model.Prefix,
				HODName = model.HODName,
				HODNameLocal = model.HODNameLocal,
				DesignationDescription = model.DesignationDescription,
				IASRank = model.IASRank,
				Email = model.Email,
				PhoneResidence = model.PhoneResidence,
				PhoneOffice = model.PhoneOffice,
				PhoneOfficeAlternate = model.PhoneOfficeAlternate,
				Mobile = model.Mobile,
				AadhaarId = model.AadhaarId,
				OrderId = model.OrderId,
				IsActive = model.IsActive ?? false,
				DeptCol = new SelectList(Departments, "deptId", "deptname"),
				Mode = Mode

			};
		}

		public static mHOD ToDomainModel(this HODViewModel model)
		{
			return new mHOD
			{
				HODID = model.HODID,
				EmpCode = model.EmpCode,
				DeptID = model.DeptID,
				Prefix = model.Prefix,
				HODName = model.HODName,
				HODNameLocal = model.HODNameLocal,
				DesignationDescription = model.DesignationDescription,
				IASRank = model.IASRank,
				Email = model.Email,
				PhoneResidence = model.PhoneResidence,
				PhoneOffice = model.PhoneOffice,
				PhoneOfficeAlternate = model.PhoneOfficeAlternate,
				Mobile = model.Mobile,
				AadhaarId = model.AadhaarId,
				OrderId = model.OrderId,
				IsActive = model.IsActive,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName

			};
		}


		//Added for RotationofMinisters

		#region RotationofMinisters

		public static tRotationMinister ToDomainModel(this RotationofMinistersViewModel rotmin)
		{
			var DomainModel = new tRotationMinister()
			{
				Id = rotmin.VId,

				MinistryId = rotmin.VMinistryId,
				SessionDateId = rotmin.VSessionDateId,
				SessionId = rotmin.VSessionId,
				AssemblyId = rotmin.VAssemblyId,
				IsActive = rotmin.IsActive,
				CreatedBy = rotmin.VCreatedBy

			};
			return DomainModel;
		}


		public static IEnumerable<RotationofMinistersViewModel> ToViewModel1(this IEnumerable<tRotationMinister> tour)
		{
			var ViewModel = tour.Select(rotmin => new RotationofMinistersViewModel()
			{
				VId = rotmin.Id,
				VMinistryId = rotmin.MinistryId,
				VSessionDateId = rotmin.SessionDateId,
				VSessionId = rotmin.SessionId,
				VAssemblyId = rotmin.AssemblyId,
				IsActive = rotmin.IsActive ?? false,
				VMinisteryMinisterName = rotmin.MinisteryMinisterName,
				VSessionName = rotmin.SessionName,
				VSessionDate = rotmin.SessionDate,
				VAssemblyName = rotmin.AssemblyName,
				VCreatedBy = rotmin.CreatedBy


			});
			return ViewModel;
		}

		public static RotationofMinistersViewModel ToViewModel1(this tRotationMinister rotmin, string Mode)
		{
			var ViewModel = new RotationofMinistersViewModel()
			{
				VId = rotmin.Id,
				VMinistryId = rotmin.MinistryId,
				VSessionDateId = rotmin.SessionDateId,
				VSessionId = rotmin.SessionId,
				VAssemblyId = rotmin.AssemblyId,
				VSessionName = rotmin.SessionName,
				VSessionDate = rotmin.SessionDate,
				VAssemblyName = rotmin.AssemblyName,
				VMinisteryMinisterName = rotmin.MinisteryMinisterName,
				IsActive = rotmin.IsActive ?? false,
				VCreatedBy = rotmin.CreatedBy,
				Mode = Mode,

			};
			return ViewModel;
		}



		#endregion



		//Added for MemberSMSQuota


		public static IEnumerable<MemberSMSQuotaViewModel> ToViewModel(this IEnumerable<mMemberSMSQuotaAddOn> model)
		{

			var ViewModel = model.Select(sms => new MemberSMSQuotaViewModel()
			{

				Id = sms.Id,
				AddonValue = sms.AddonValue,
				MemberCode = sms.MemberCode,
				//AddonDate = sms.AddonDate.Value.ToString("dd/MM/yyyy"),
				//AddonDate = sms.AddonDate.Day + "/" + sms.AddonDate.Month + "/" + sms.AddonDate.Year,
				AddonDate = sms.AddonDate,
				GetMemberName = sms.GetMemberName,
				IsActive = sms.IsActive ?? false,
				IsDeleted = sms.IsDeleted ?? true

			});
			return ViewModel;
		}

		public static mMemberSMSQuotaAddOn ToDomainModel(this MemberSMSQuotaViewModel sms)
		{
			return new mMemberSMSQuotaAddOn
			{



				Id = sms.Id,
				AddonValue = sms.AddonValue,
				MemberCode = sms.MemberCode,
				//AddonDate = string.IsNullOrEmpty(sms.AddonDate) ? DateTime.Now : DateTime.ParseExact(sms.AddonDate, "dd/MM/yyyy", null),
				AddonDate = sms.AddonDate,

				IsActive = sms.IsActive,
				IsDeleted = sms.IsDeleted,
				GetMemberName = sms.GetMemberName,
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName
			};
		}

		public static MemberSMSQuotaViewModel

			ToViewModel1(this mMemberSMSQuotaAddOn sms, List<mMember> Members, string Mode)
		{
			return new MemberSMSQuotaViewModel
			{


				Id = sms.Id,
				AddonValue = sms.AddonValue,
				MemberCode = sms.MemberCode,
				GetMemberName = sms.GetMemberName,
				//AddonDate = sms.AddonDate != null ? sms.AddonDate.Value.ToString("dd/MM/yyyy") : "",
				AddonDate = sms.AddonDate,
				IsActive = sms.IsActive ?? false,
				IsDeleted = sms.IsDeleted ?? true,
				MemberNames = new SelectList(Members, "MemberCode", "Name"),
				CreatedBy = CurrentSession.UserName,
				ModifiedBy = CurrentSession.UserName,
				Mode = Mode

			};
		}





		//Added for CutMotionDemand

		#region CutMotionDemand

		public static tCutMotionDemand ToDomainModel(this CutMotionDemandViewModel cmd)
		{
			var DomainModel = new tCutMotionDemand()
			{
				CMDemandId = cmd.VCMDemandId,
				DemandName = cmd.VDemandName,
				DemandName_Local = cmd.VDemandName_Local,
				SessionID = cmd.VSessionID,
				AssemblyID = cmd.VAssemblyID,
				DemandNo = cmd.VDemandNo,
				//DemandAmountType = cmd.VDemandAmountType,
				//DemandAmountType_Local = cmd.VDemandAmountType_Local,
				RevenueAmount = cmd.VRevenueAmount,
				CapitalAmount = cmd.VCapitalAmount,
				IsActive = cmd.IsActive,
				IsDeleted = cmd.IsDeleted,
				CreatedBy = cmd.CreatedBy

			};
			return DomainModel;
		}


		public static IEnumerable<CutMotionDemandViewModel> ToViewModel1(this IEnumerable<tCutMotionDemand> tour)
		{
			var ViewModel = tour.Select(cmd => new CutMotionDemandViewModel()
			{
				VCMDemandId = cmd.CMDemandId,
				VDemandName = cmd.DemandName,
				VDemandName_Local = cmd.DemandName_Local,
				VSessionID = cmd.SessionID,
				VAssemblyID = cmd.AssemblyID,
				VDemandNo = cmd.DemandNo,
				//VDemandAmountType = cmd.DemandAmountType,
				//VDemandAmountType_Local = cmd.DemandAmountType_Local,
				VRevenueAmount = cmd.RevenueAmount,
				VCapitalAmount = cmd.CapitalAmount,
				//IsActive = cmd.IsActive ?? false,
				//IsDeleted = cmd.IsDeleted ?? true,
				IsActive = cmd.IsActive,
				IsDeleted = cmd.IsDeleted,
				VSessionName = cmd.SessionName,
				VAssemblyName = cmd.AssemblyName,
				CreatedBy = cmd.CreatedBy


			});
			return ViewModel;
		}

		public static CutMotionDemandViewModel ToViewModel1(this tCutMotionDemand cmd, string Mode)
		{
			var ViewModel = new CutMotionDemandViewModel()
			{
				VCMDemandId = cmd.CMDemandId,
				VDemandName = cmd.DemandName,
				VDemandName_Local = cmd.DemandName_Local,
				VSessionID = cmd.SessionID,
				VAssemblyID = cmd.AssemblyID,
				VSessionName = cmd.SessionName,
				VAssemblyName = cmd.AssemblyName,
				VDemandNo = cmd.DemandNo,
				//VDemandAmountType = cmd.DemandAmountType,
				//VDemandAmountType_Local = cmd.DemandAmountType_Local,
				VRevenueAmount = cmd.RevenueAmount,
				VCapitalAmount = cmd.CapitalAmount,
				//IsActive = cmd.IsActive ?? false,
				//IsDeleted = cmd.IsDeleted ?? true,
				IsActive = cmd.IsActive,
				IsDeleted = cmd.IsDeleted,
				CreatedBy = cmd.CreatedBy,
				Mode = Mode,

			};
			return ViewModel;
		}



		#endregion


        //Added for tFormsTypeofDocuments

        public static tFormsTypeofDocuments ToDomainModel(this FormTypeofDocumentsViewModel model)
        {
            return new tFormsTypeofDocuments
            {

                TypeofFormId = model.TypeofFormId,
                TypeofFormName = model.TypeofFormName,
                Description = model.Description,

                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }


        public static List<FormTypeofDocumentsViewModel> ToViewModel(this IEnumerable<tFormsTypeofDocuments> model)
        {

            var ViewModel = model.Select(part => new FormTypeofDocumentsViewModel()
            {
                TypeofFormId = part.TypeofFormId,
                TypeofFormName = part.TypeofFormName,
                Description = part.Description,

                IsActive = part.IsActive ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }



        public static FormTypeofDocumentsViewModel ToViewModel1(this tFormsTypeofDocuments model, string Mode)
        {
            return new FormTypeofDocumentsViewModel
            {
                TypeofFormId = model.TypeofFormId,
                TypeofFormName = model.TypeofFormName,
                Description = model.Description,

                IsActive = model.IsActive ?? false,

                //CreatedBy = model.CreatedBy,
                //CreatedDate = model.CreatedDate,
                //ModifiedBy = model.ModifiedBy,
                //ModifiedWhen = model.ModifiedWhen,
                Mode = Mode

            };
        }


        //Added for mAssemblyTypeofDocuments

        public static mAssemblyTypeofDocuments ToDomainModel(this AssemblyTypeofDocumentsViewModel model)
        {
            return new mAssemblyTypeofDocuments
            {

                TypeofDocumentId = model.TypeofDocumentId,
                TypeofDocumentName = model.TypeofDocumentName,
                Description = model.Description,

                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }


        public static List<AssemblyTypeofDocumentsViewModel> ToViewModel(this IEnumerable<mAssemblyTypeofDocuments> model)
        {

            var ViewModel = model.Select(part => new AssemblyTypeofDocumentsViewModel()
            {
                TypeofDocumentId = part.TypeofDocumentId,
                TypeofDocumentName = part.TypeofDocumentName,
                Description = part.Description,

                IsActive = part.IsActive ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }



        public static AssemblyTypeofDocumentsViewModel ToViewModel1(this mAssemblyTypeofDocuments model, string Mode)
        {
            return new AssemblyTypeofDocumentsViewModel
            {
                TypeofDocumentId = model.TypeofDocumentId,
                TypeofDocumentName = model.TypeofDocumentName,
                Description = model.Description,

                IsActive = model.IsActive ?? false,

                //CreatedBy = model.CreatedBy,
                //CreatedDate = model.CreatedDate,
                //ModifiedBy = model.ModifiedBy,
                //ModifiedWhen = model.ModifiedWhen,
                Mode = Mode

            };
        }


        //tpanchayatvillage

        public static tPanchayatVillage ToDomainModel(this PanchayatVillageViewModel model)
        {
            return new tPanchayatVillage
            {

                PanchayatVillageID = model.PanchayatVillageID,
                //PartyCode = part.PartyCode.ToString(),
                StateCode = model.StateCode,
                DistrictCode = model.DistrictCode,
                PanchayatCode = model.PanchayatCode,
                VillageCode = model.VillageCode,
                
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }


        public static List<PanchayatVillageViewModel> ToViewModel(this IEnumerable<tPanchayatVillage> model)
        {

            var ViewModel = model.Select(part => new PanchayatVillageViewModel()
            {
                PanchayatVillageID = part.PanchayatVillageID,
                StateCode = part.StateCode,
                DistrictCode = part.DistrictCode,
                PanchayatCode = part.PanchayatCode,
                VillageCode = part.VillageCode,

                IsActive = part.IsActive ?? false,
                GetStateName = part.GetStateName,
                GetDistrictName = part.GetDistrictName,
                GetPanchayatName = part.GetPanchayatName,
                GetVillageName = part.GetVillageName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }



        public static PanchayatVillageViewModel ToViewModel1(this tPanchayatVillage model, List<mStates> States, List<DistrictModel> District, List<mPanchayat> Panchayat, List<mVillage> Village, string Mode)
        {
            return new PanchayatVillageViewModel
            {
                PanchayatVillageID = model.PanchayatVillageID,
                StateCode = model.StateCode,
                DistrictCode = model.DistrictCode,
                PanchayatCode = model.PanchayatCode,
                VillageCode = model.VillageCode,


                State = new SelectList(States, "mStateID", "StateName"),
                District = new SelectList(District, "DistrictCode", "DistrictName"),
                Panchayat = new SelectList(Panchayat, "PanchayatCode", "PanchayatName"),
                Village = new SelectList(Village, "VillageCode", "VillageName"),
                IsActive = model.IsActive ?? false,
                Mode = Mode

            };
        }



        #region constituencyCategory



        public static mConstituencyReservedCategory ToDomainModel(this ConstituencyCategoryViewModel model)
        {
            return new mConstituencyReservedCategory
            {

                ConstituencyCategoryId = model.ConstituencyCategoryId,
                ConstituencyCategoryCode = model.ConstituencyCategoryCode,
                ConstituencyCategoryName = model.ConstituencyCategoryName,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }


        public static List<ConstituencyCategoryViewModel> ToViewModel(this IEnumerable<mConstituencyReservedCategory> model)
        {

            var ViewModel = model.Select(part => new ConstituencyCategoryViewModel()
            {
                ConstituencyCategoryId = part.ConstituencyCategoryId,
                ConstituencyCategoryCode = part.ConstituencyCategoryCode,
                ConstituencyCategoryName = part.ConstituencyCategoryName,
                IsActive = part.IsActive ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }



        public static ConstituencyCategoryViewModel ToViewModel1(this mConstituencyReservedCategory model, string Mode)
        {
            return new ConstituencyCategoryViewModel
            {
                ConstituencyCategoryId = model.ConstituencyCategoryId,
                ConstituencyCategoryCode = model.ConstituencyCategoryCode,
                ConstituencyCategoryName = model.ConstituencyCategoryName,
                IsActive = model.IsActive ?? false,
                Mode = Mode

            };
        }




        #endregion



        #region HighestQualification



        public static mMemberHighestQualification ToDomainModel(this HighestQualificationViewModel model)
        {
            return new mMemberHighestQualification
            {

                MemberHighestQualificationId = model.MemberHighestQualificationId,
                Qualification = model.Qualification,
                QualificationLocal = model.QualificationLocal,
                Description = model.Description,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }


        public static List<HighestQualificationViewModel> ToViewModel(this IEnumerable<mMemberHighestQualification> model)
        {

            var ViewModel = model.Select(part => new HighestQualificationViewModel()
            {
                MemberHighestQualificationId = part.MemberHighestQualificationId,
                Qualification = part.Qualification,
                QualificationLocal = part.QualificationLocal,
                Description = part.Description,
                IsActive = part.IsActive ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }



        public static HighestQualificationViewModel ToViewModel1(this mMemberHighestQualification model, string Mode)
        {
            return new HighestQualificationViewModel
            {
                MemberHighestQualificationId = model.MemberHighestQualificationId,
                Qualification = model.Qualification,
                QualificationLocal = model.QualificationLocal,
                Description = model.Description,
                IsActive = model.IsActive ?? false,
                Mode = Mode

            };
        }




        #endregion

        #region SubEvent

        public static mSubEvents ToDomainModel(this SubEventViewModel model)
        {
            return new mSubEvents
            {

                SubEventId = model.SubEventId,
                EventName = model.EventName,
                EventNameLocal = model.EventNameLocal,
                CategoryCode = model.CategoryCode,
                CategoryName = model.CategoryName,
                IsLOB = model.IsLOB,
                IsProceeding = model.IsProceeding,
                IsDepartment = model.IsDepartment,
                IsCommittee = model.IsCommittee,
                IsVS = model.IsVS,
                IsOtherNotices = model.IsOtherNotices,
                IsQA = model.IsQA,

                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }


        public static List<SubEventViewModel> ToViewModel(this IEnumerable<mSubEvents> model)
        {

            var ViewModel = model.Select(part => new SubEventViewModel()
            {
                SubEventId = part.SubEventId,
                EventName = part.EventName,
                EventNameLocal = part.EventNameLocal,
                CategoryCode = part.CategoryCode,
                CategoryName = part.CategoryName,
                IsLOB = part.IsLOB ?? false,
                IsProceeding = part.IsProceeding ?? false,
                IsDepartment = part.IsDepartment ?? false,
                IsCommittee = part.IsCommittee ?? false,
                IsVS = part.IsVS ?? false,
                IsOtherNotices = part.IsOtherNotices ?? false,
                IsQA = part.IsQA ?? false,

                IsActive = part.IsActive ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }



        public static SubEventViewModel ToViewModel1(this mSubEvents model, string Mode)
        {
            return new SubEventViewModel
            {
                SubEventId = model.SubEventId,
                EventName = model.EventName,
                EventNameLocal = model.EventNameLocal,
                CategoryCode = model.CategoryCode,
                CategoryName = model.CategoryName,
                IsLOB = model.IsLOB ?? false,
                IsProceeding = model.IsProceeding ?? false,
                IsDepartment = model.IsDepartment ?? false,
                IsCommittee = model.IsCommittee ?? false,
                IsVS = model.IsVS ?? false,
                IsOtherNotices = model.IsOtherNotices ?? false,
                IsQA = model.IsQA ?? false,

                IsActive = model.IsActive ?? false,

                //CreatedBy = model.CreatedBy,
                //CreatedDate = model.CreatedDate,
                //ModifiedBy = model.ModifiedBy,
                //ModifiedWhen = model.ModifiedWhen,
                Mode = Mode

            };
        }
        #endregion


        public static workType ToDomainModel(this ContituencyWorksViewModel model)
        {
            return new workType
            {

                workID = model.workID,
                WorkTypeCode = model.WorkTypeCode,
                WorkTypeName = model.WorkTypeName,
                WorkTypeNameLocal = model.WorkTypeNameLocal,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }
        public static List<ContituencyWorksViewModel> ToViewModel(this IEnumerable<workType> model)
        {


            var ViewModel = model.Select(part => new ContituencyWorksViewModel()
            {
                workID = part.workID,
                WorkTypeCode = part.WorkTypeCode,
                WorkTypeName = part.WorkTypeName,
                WorkTypeNameLocal = part.WorkTypeNameLocal,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }

        public static ContituencyWorksViewModel ToViewModel(this workType model, string Mode)
        {
            return new ContituencyWorksViewModel
            {
                workID = model.workID,
                WorkTypeCode = model.WorkTypeCode,
                WorkTypeName = model.WorkTypeName,
                WorkTypeNameLocal = model.WorkTypeNameLocal,
                Mode = Mode

            };
        }

        //----------
        public static AuthEmployee ToDomainModel(this HouseEmpCommitteeViewModel model)
        {
            return new AuthEmployee
            {

                ID = model.ID,
                Name = model.Name,
                AadharId = model.AadharId,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static List<HouseEmpCommitteeViewModel> ToViewModel(this IEnumerable<AuthEmployee> model)
        {


            var ViewModel = model.Select(part => new HouseEmpCommitteeViewModel()
            {
                ID = part.ID,
                Name = part.Name,
                AadharId = part.AadharId,

                IsActive = part.IsActive,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }

        public static HouseEmpCommitteeViewModel ToViewModel(this AuthEmployee model, string Mode, string strId)
        {
            return new HouseEmpCommitteeViewModel
            {
                ID = model.ID,
                Name = model.Name,
                AadharId = model.AadharId,
                IsActive = model.IsActive,
                Mode = Mode,
                strId = strId


            };
        }

        public static SBL.DomainModel.Models.eFile.eFilePaperSno ToDomainModel(this SerialNoViewModel model)
        {
            return new SBL.DomainModel.Models.eFile.eFilePaperSno
            {

                id = model.id,
                year = model.year,
                runno = model.runno,
                //WorkTypeNameLocal = model.WorkTypeNameLocal,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }
        public static List<SerialNoViewModel> ToViewModel(this IEnumerable<SBL.DomainModel.Models.eFile.eFilePaperSno> model)
        {


            var ViewModel = model.Select(part => new SerialNoViewModel()
            {
                id = part.id,
                year = part.year,
                runno = part.runno,
                //WorkTypeNameLocal = part.WorkTypeNameLocal,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }

        public static SerialNoViewModel ToViewModel(this SBL.DomainModel.Models.eFile.eFilePaperSno model, string Mode, string strId)
        {
            return new SerialNoViewModel
            {
                id = model.id,
                year = model.year,
                runno = model.runno,
                //WorkTypeNameLocal = model.WorkTypeNameLocal,

                Mode = Mode,
                strId = strId

            };
        }





        public static SBL.DomainModel.Models.SubDivision.mSubDivision ToDomainModel(this SubDivisionViewModel model)
        {
            return new SBL.DomainModel.Models.SubDivision.mSubDivision
            {
                ConstituencyID = model.ConstituencyID,
                mSubDivisionId = model.mSubDivisionId,
                SubDivisionName = model.SubDivisionName,
                SubDivisionNameLocal = model.SubDivisionNameLocal,
                SubDivisionAbbreviation = model.SubDivisionAbbreviation,
                //WorkTypeNameLocal = model.WorkTypeNameLocal,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }
        public static List<SubDivisionViewModel> ToViewModel(this IEnumerable<SBL.DomainModel.Models.SubDivision.mSubDivision> model)
        {


            var ViewModel = model.Select(part => new SubDivisionViewModel()
            {
                ConstituencyID = part.ConstituencyID,
                mSubDivisionId = part.mSubDivisionId,
                SubDivisionName = part.SubDivisionName,
                SubDivisionNameLocal = part.SubDivisionNameLocal,
                SubDivisionAbbreviation = part.SubDivisionAbbreviation,
                GetConstituencyName = part.GetConstituencyName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }

        public static SubDivisionViewModel ToViewModel(this SBL.DomainModel.Models.SubDivision.mSubDivision model, List<mConstituency> Constituencys, string Mode)
        {
            return new SubDivisionViewModel
            {
                ConstituencyID = model.ConstituencyID,
                mSubDivisionId = model.mSubDivisionId,
                SubDivisionName = model.SubDivisionName,
                SubDivisionNameLocal = model.SubDivisionName,
                SubDivisionAbbreviation = model.SubDivisionAbbreviation,
                GetConstituencyName = model.GetConstituencyName,
                //WorkTypeNameLocal = model.WorkTypeNameLocal,
                Constituencys = new SelectList(Constituencys, "ConstituencyID", "ConstituencyName"),
                Mode = Mode,


            };
        }

        public static mProgramme ToDomainModel(this ContituencySchemeEnumsViewModel model)
        {
            return new mProgramme
            {

                ProgramID = model.ProgramID,
                programmeName = model.programmeName,
                programmeCode = model.programmeCode,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static List<ContituencySchemeEnumsViewModel> ToViewModel(this IEnumerable<mProgramme> model)
        {

            var ViewModel = model.Select(part => new ContituencySchemeEnumsViewModel()
            {
                ProgramID = part.ProgramID,
                programmeCode = part.programmeCode,
                programmeName = part.programmeName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
            });
            return ViewModel.ToList();
        }

        public static ContituencySchemeEnumsViewModel ToViewModel1(this mProgramme model, string Mode)
        {
            return new ContituencySchemeEnumsViewModel
            {
                ProgramID = model.ProgramID,
                programmeCode = model.programmeCode,
                programmeName = model.programmeName,

                Mode = Mode

            };
        }
 
    }
}