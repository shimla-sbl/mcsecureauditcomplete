﻿using SBL.DomainModel.Models.Assembly;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfAssembly
    {
        public static IEnumerable<AssemblyViewModel> ToViewModel(this IEnumerable<mAssembly> model)
        {

            var ViewModel = model.Select(assem => new AssemblyViewModel()
            {
                AssemblyID=assem.AssemblyID,
                AssemblyCode=assem.AssemblyCode,
                AssemblyName=assem.AssemblyName,
                AssemblyNameLocal=assem.AssemblyNameLocal,
                AssemblyStartDate=assem.AssemblyStartDate.ToString(),
                AssemblyEndDate=assem.AssemblyEndDate.ToString(),
                Period=assem.Period,
                Active= assem.Active ?? true ,
                Rem1=assem.Rem1,
                Rem2= assem.Rem2,
                Rem3 = assem.Rem3,
                Rem4 = assem.Rem4,
                Rem5 = assem.Rem5
            });
            return ViewModel;
        }

        public static mAssembly ToDomainModel(this AssemblyViewModel model)
        {
            try
            {
                return new mAssembly
                {

                    AssemblyID = (Int32)(model.AssemblyID != null ? model.AssemblyID : 0),
                    //AssemblyID = model.AssemblyID,
                    AssemblyCode = model.AssemblyCode,
                    AssemblyName = model.AssemblyName,
                    AssemblyNameLocal = model.AssemblyNameLocal,
                    AssemblyStartDate = string.IsNullOrEmpty(model.AssemblyStartDate) ? DateTime.Now : DateTime.ParseExact(model.AssemblyStartDate, "dd/MM/yyyy", null),
                    AssemblyEndDate = string.IsNullOrEmpty(model.AssemblyEndDate) ? DateTime.Now : DateTime.ParseExact(model.AssemblyEndDate, "dd/MM/yyyy", null),
                    Period = model.Period,
                    Active = model.Active,
                    Rem1 = model.Rem1,
                    Rem2 = model.Rem2,
                    Rem3 = model.Rem3,
                    Rem4 = model.Rem4,
                    Rem5 = model.Rem5,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           
        }

        public static AssemblyViewModel ToViewModel1(this mAssembly model, string Mode)
        {
            return new AssemblyViewModel
            {
                AssemblyID = model.AssemblyID,
                AssemblyCode = model.AssemblyCode,
                AssemblyName = model.AssemblyName,
                AssemblyNameLocal = model.AssemblyNameLocal,
                AssemblyStartDate = model.AssemblyStartDate != null ? model.AssemblyStartDate.Value.ToString("dd/MM/yyyy") : "",
                AssemblyEndDate = model.AssemblyEndDate != null ? model.AssemblyEndDate.Value.ToString("dd/MM/yyyy") : "",
                Period = model.Period,
                Active=  model.Active ?? true,
                Rem1 = model.Rem1,
                Rem2 = model.Rem2,
                Rem3 = model.Rem3,
                Rem4 = model.Rem4,
                Rem5 = model.Rem5,                       
                Mode = Mode

            };
        }
    }
}