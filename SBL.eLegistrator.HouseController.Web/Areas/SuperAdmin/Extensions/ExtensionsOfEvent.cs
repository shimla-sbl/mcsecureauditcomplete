﻿using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfEvent
    {

        public static IEnumerable<EventViewModel> ToViewModel(this IEnumerable<mEvent> model)
        {

            var ViewModel = model.Select(Event => new EventViewModel()
            {
                EventId = Event.EventId,
                EventName = Event.EventName,
                EventNameLocal = Event.EventNameLocal,
                PaperCategoryTypeId = Event.PaperCategoryTypeId,
                IsLOB = Event.IsLOB ?? false,
                GetPapCatName=Event.GetPapCatName,
                IsDepartment = Event.IsDepartment ?? false,
                IsCommittee = Event.IsCommittee ?? false,
                IsProceeding = Event.IsProceeding ?? false,
                IsMember = Event.IsMember ?? false,
                Active = Event.Active ?? false,
                OrderingID = Event.OrderingID,
                RuleNo = Event.RuleNo

            });
            return ViewModel;
        }

        public static mEvent ToDomainModel(this EventViewModel model)
        {
            try
            {
                return new mEvent
                {
                    
                    EventId = model.EventId,
                    EventName = model.EventName,
                    EventNameLocal = model.EventNameLocal,
                    PaperCategoryTypeId = model.PaperCategoryTypeId,
                    IsLOB = model.IsLOB,
                    IsDepartment = model.IsDepartment,
                    IsCommittee = model.IsCommittee,
                    IsProceeding = model.IsProceeding,
                    GetPapCatName = model.GetPapCatName,
                    IsMember = model.IsMember,
                    Active = model.Active,
                    OrderingID = (Int32)(model.OrderingID != null ? model.OrderingID : 0),
                    RuleNo = model.RuleNo,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName

                };
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
          
        }

        public static EventViewModel ToViewModel1(this mEvent model, List<mPaperCategoryType> pCategoryType, string Mode)
        {
            return new EventViewModel
            {
                EventId = model.EventId,
                EventName = model.EventName,
                EventNameLocal = model.EventNameLocal,
                PaperCategoryTypeId = model.PaperCategoryTypeId,
                IsLOB = model.IsLOB ?? false,
                IsDepartment = model.IsDepartment ?? false,
                IsCommittee = model.IsCommittee ?? false,
                IsProceeding = model.IsProceeding ?? false,
                IsMember = model.IsMember ?? false,
                Active = model.Active ?? false,
                GetPapCatName = model.GetPapCatName,
                OrderingID = model.OrderingID,
                RuleNo = model.RuleNo,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                PaperCatTypes = new SelectList(pCategoryType, "PaperCategoryTypeId", "Name"),
                Mode = Mode

            };
        }

    }
}