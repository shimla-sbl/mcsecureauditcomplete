﻿using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfSessionType
    {
        public static IEnumerable<SessionTypeViewModel> ToViewModel(this IEnumerable<mSessionType> model)
        {

            var ViewModel = model.Select(session => new SessionTypeViewModel()
            {
                TypeID = session.TypeID,
                TypeName = session.TypeName,
                TypeNameLocal = session.TypeNameLocal,
                IsActive = session.IsActive
               
            });
            return ViewModel;
        }

        public static mSessionType ToDomainModel(this SessionTypeViewModel model)
        {
            return new mSessionType
            {
                 TypeID = model.TypeID,
                TypeName = model.TypeName,
                TypeNameLocal = model.TypeNameLocal,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static SessionTypeViewModel ToViewModel1(this mSessionType model,string Mode)
        {
            return new SessionTypeViewModel
            {
                TypeID = model.TypeID,
                TypeName = model.TypeName,
                TypeNameLocal = model.TypeNameLocal,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }
    }
}