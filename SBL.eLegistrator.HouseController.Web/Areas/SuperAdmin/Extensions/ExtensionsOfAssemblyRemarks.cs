﻿using SBL.DomainModel.Models.AssemblyRemarks;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Assembly;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfAssemblyRemarks
    {
        public static IEnumerable<AssemblyRemarksViewModel> ToViewModel(this IEnumerable<mAssemblyRemarks> model)
        {

            var ViewModel = model.Select(AssemRem => new AssemblyRemarksViewModel()
            {
                AssemblyRemarksID = AssemRem.AssemblyRemarksID,
                AssemblyRemarks = AssemRem.AssemblyRemarks,
                AssemblyID = AssemRem.AssemblyID,
                GetAssemblyName = AssemRem.GetAssemblyName,
               
                Active = AssemRem.Active ?? false

            });
            return ViewModel;
        }

        public static mAssemblyRemarks ToDomainModel(this AssemblyRemarksViewModel model)
        {
            return new mAssemblyRemarks
            {
                AssemblyRemarksID = model.AssemblyRemarksID,
                AssemblyRemarks = model.AssemblyRemarks,
                AssemblyID = model.AssemblyID,
                GetAssemblyName = model.GetAssemblyName,
          
                Active = model.Active,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static AssemblyRemarksViewModel ToViewModel1(this mAssemblyRemarks model, List<mAssembly> Assemblies, string mode)
        {
            return new AssemblyRemarksViewModel
            {
                AssemblyRemarksID = model.AssemblyRemarksID,
                AssemblyRemarks = model.AssemblyRemarks,                
                Active = model.Active ?? false,
                AssemblyID = model.AssemblyID,
                GetAssemblyName = model.GetAssemblyName,
                Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                Mode = mode
            };
        }
    }
}