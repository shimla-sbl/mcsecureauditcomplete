﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfMinistryDepartment
    {
        public static IEnumerable<MinistryDepartmentViewModel> ToViewModel(this IEnumerable<mMinistryDepartment> model)
        {

            var ViewModel = model.Select(ministryDept => new MinistryDepartmentViewModel()
            {
                AssemblyID = ministryDept.AssemblyID,
                MinistryID = ministryDept.MinistryID,
                MinistryDepartmentsID = ministryDept.MinistryDepartmentsID,
                DeptID = ministryDept.DeptID,               
                IsActive = ministryDept.IsActive ?? false,
                GetAssemblyName=ministryDept.GetAssemblyName,
                GetDeptName=ministryDept.GetDeptName,
                GetMinistryName=ministryDept.GetMinistryName,
                OrderID = ministryDept.OrderID
               
            });
            return ViewModel;
        }

        public static mMinistryDepartment ToDomainModel(this MinistryDepartmentViewModel ministryDept)
        {
            return new mMinistryDepartment
            {
                AssemblyID = ministryDept.AssemblyID,
                MinistryID = ministryDept.MinistryID,
                MinistryDepartmentsID = ministryDept.MinistryDepartmentsID,
                DeptID = ministryDept.DeptID,
                IsActive = ministryDept.IsActive,
                OrderID = ministryDept.OrderID,
                GetAssemblyName = ministryDept.GetAssemblyName,
                GetDeptName = ministryDept.GetDeptName,
                GetMinistryName = ministryDept.GetMinistryName,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        public static MinistryDepartmentViewModel ToViewModel1(this mMinistryDepartment ministryDept, List<mDepartment> Departments, List<mMinistry> Ministries, List<mAssembly> Assemblies, string Mode)
        {

            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var item in Ministries)
            {
                SelectListItem s = new SelectListItem();
                s.Text = item.MinistryName + " " + "(" + item.MinisterName + ")";
                s.Value = item.MinistryID.ToString();

                items.Add(s);
            }

            return new MinistryDepartmentViewModel
            {
                AssemblyID = ministryDept.AssemblyID,
                MinistryID = ministryDept.MinistryID,
                MinistryDepartmentsID = ministryDept.MinistryDepartmentsID,
                DeptID = ministryDept.DeptID,
                IsActive = ministryDept.IsActive ?? false,
                OrderID = ministryDept.OrderID,
                GetAssemblyName = ministryDept.GetAssemblyName,
                GetDeptName = ministryDept.GetDeptName,
                GetMinistryName = ministryDept.GetMinistryName,
                //Ministry = new SelectList(Ministries, "MinistryID", "MinistryName"),
                Ministry = new SelectList(items, "Value", "Text"),
                Department = new SelectList(Departments, "deptId", "deptname"),
                Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }
    }
}