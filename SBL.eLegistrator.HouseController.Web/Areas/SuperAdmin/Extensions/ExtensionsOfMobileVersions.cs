﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfMobileVersions
    {

        public static IEnumerable<MobileVersionsViewModel> ToViewModel(this IEnumerable<tMobileVersions> model)
        {

            var ViewModel = model.Select(mobVersion => new MobileVersionsViewModel()
            {
                SlNo = mobVersion.SlNo,
                Code = mobVersion.Code,
                Major = mobVersion.Major,
                Minor = mobVersion.Minor,
                Build = mobVersion.Build,
                FilePath = mobVersion.FilePath,
                VersionDate = mobVersion.VersionDate.ToString()

            });
            return ViewModel;
        }

        public static tMobileVersions ToDomainModel(this MobileVersionsViewModel model)
        {
            try
            {
                return new tMobileVersions
                {
                    SlNo = model.SlNo,
                    Code = model.Code,
                    Major = model.Major,
                    Minor = model.Minor,
                    Build = model.Build,
                    FilePath = model.FilePath,
                    VersionDate = string.IsNullOrEmpty(model.VersionDate) ? DateTime.Now : DateTime.ParseExact(model.VersionDate, "dd/MM/yyyy", null),
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static MobileVersionsViewModel ToViewModel1(this tMobileVersions model, List<mMobileApps> Codes, string Mode)
        {
            return new MobileVersionsViewModel
            {
                SlNo = model.SlNo,
                Code = model.Code,
                Major = model.Major,
                Minor = model.Minor,
                Build = model.Build,
                FilePath = model.FilePath,
                VersionDate = model.VersionDate != null ? model.VersionDate.Value.ToString("dd/MM/yyyy") : "",
                CodeList = new SelectList(Codes, "Code", "Code"),
                Mode = Mode
            };
        }

    }
}