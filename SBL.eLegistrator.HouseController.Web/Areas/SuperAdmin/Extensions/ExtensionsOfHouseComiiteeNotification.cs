﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfHouseComiiteeNotification
    {

        public static IEnumerable<HouseCommitteeNotificationViewModel> ToViewModel(this IEnumerable<HouseCommitteeNotification> model)
        {

            var ViewModel = model.Select(hcn => new HouseCommitteeNotificationViewModel()
            {
                SlNo = hcn.SlNo,
                Title=hcn.Title,
                FilePath=hcn.FilePath,               
                HCNDate = hcn.HCNDate.ToString()

            });
            return ViewModel;
        }

        public static HouseCommitteeNotification ToDomainModel(this HouseCommitteeNotificationViewModel model)
        {
            try
            {
                return new HouseCommitteeNotification
                {
                    SlNo = model.SlNo,
                    Title = model.Title,
                    FilePath = model.FilePath,
                    HCNDate = string.IsNullOrEmpty(model.HCNDate) ? DateTime.Now : DateTime.ParseExact(model.HCNDate, "dd/MM/yyyy", null),
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static HouseCommitteeNotificationViewModel ToViewModel1(this HouseCommitteeNotification model, string Mode)
        {
            return new HouseCommitteeNotificationViewModel
            {
                SlNo = model.SlNo,
                Title = model.Title,
                FilePath = model.FilePath,
                HCNDate = model.HCNDate != null ? model.HCNDate.Value.ToString("dd/MM/yyyy") : "",
                Mode = Mode
            };
        }


    }
}