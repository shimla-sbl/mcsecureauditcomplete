﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfMobileSettings
    {

        public static IEnumerable<MobileSettingsViewModel> ToViewModel(this IEnumerable<mMobileSettings> model)
        {

            var ViewModel = model.Select(mobSetting => new MobileSettingsViewModel()
            {
                SlNo = mobSetting.SlNo,
                MediaViewTime = (mobSetting.MediaAfterBefore  == "0" ? mobSetting.MediaViewTime : (Convert.ToInt32(mobSetting.MediaViewTime) * -1)),
                MemberViewTime = (mobSetting.MemberAfterBefore == "0" ? mobSetting.MemberViewTime : (Convert.ToInt32(mobSetting.MemberViewTime) * -1)),
                MediaAfterBefore = mobSetting.MediaAfterBefore,
                MemberAfterBefore = mobSetting.MemberAfterBefore
            });
            return ViewModel;
        }

        public static mMobileSettings ToDomainModel(this MobileSettingsViewModel model)
        {
            try
            {
                return new mMobileSettings
                {
                    SlNo = model.SlNo,
                    MediaViewTime = model.MediaViewTime,
                    MemberViewTime = model.MemberViewTime,
                    MediaAfterBefore = model.MediaAfterBefore,
                    MemberAfterBefore = model.MemberAfterBefore,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static MobileSettingsViewModel ToViewModel1(this mMobileSettings model, string Mode)
        {
            return new MobileSettingsViewModel
           {
               SlNo = model.SlNo,
               MediaViewTime = (model.MediaAfterBefore == "0" ? model.MediaViewTime : (Convert.ToInt32(model.MediaViewTime) * -1)),
               MemberViewTime = (model.MemberAfterBefore == "0" ? model.MemberViewTime : (Convert.ToInt32(model.MemberViewTime) * -1)),
               MediaAfterBefore=model.MediaAfterBefore,
                MemberAfterBefore=model.MemberAfterBefore,
               Mode = Mode
           };
        }
    }
}