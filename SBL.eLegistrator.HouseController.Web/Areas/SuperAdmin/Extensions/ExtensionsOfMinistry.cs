﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfMinistry
    {

        public static IEnumerable<MinistryViewModel> ToViewModel(this IEnumerable<mMinistry> model)
        {

            var ViewModel = model.Select(ministry => new MinistryViewModel()
            {
                AssemblyID = ministry.AssemblyID,
                MinistryID = ministry.MinistryID,
                MinistryName = ministry.MinistryName,
                MinistryNameLocal = ministry.MinistryNameLocal,
                //MinisterName = ministry.MinisterName,
                //MinisterNameLocal = ministry.MinisterNameLocal,
                //MemberCode = ministry.MemberCode,
               // MemberStartDate = ministry.MemberStartDate.ToString(),
               // MemberEndDate = ministry.MemberEndDate.ToString(),
                GetAssemblyName = ministry.GetAssemblyName,
                //GetMemberName = ministry.GetMemberName,
                IsActive = ministry.IsActive ?? false,
                AdditionalMinistry=ministry.AdditionalMinistry,
                //OrderID = ministry.OrderID,
                //MinisterPrefix = ministry.MinisterPrefix,
                //MinistryPrefix = ministry.MinistryPrefix
            });
            return ViewModel.OrderBy(m => m.OrderID);
        }

        public static mMinistry ToDomainModel(this MinistryViewModel ministry)
        {
            return new mMinistry
            {
                AssemblyID = ministry.AssemblyID,
                MinistryID = ministry.MinistryID,
                MinistryName = ministry.MinistryName,
                MinistryNameLocal = ministry.MinistryNameLocal,
                //MinisterName = ministry.MinisterName,
                //MinisterNameLocal = ministry.MinisterNameLocal,
                //MemberCode = ministry.MemberCode,
                //MemberStartDate = string.IsNullOrEmpty(ministry.MemberStartDate) ? DateTime.Now : DateTime.ParseExact(ministry.MemberStartDate, "dd/MM/yyyy", null),
                //MemberEndDate = string.IsNullOrEmpty(ministry.MemberEndDate) ? DateTime.Now : DateTime.ParseExact(ministry.MemberEndDate, "dd/MM/yyyy", null),
                IsActive = ministry.IsActive,
                GetAssemblyName = ministry.GetAssemblyName,
                //GetMemberName = ministry.GetMemberName,
                //OrderID = ministry.OrderID,
                //MinisterPrefix = ministry.MinisterPrefix,
                //MinistryPrefix = ministry.MinistryPrefix,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                AdditionalMinistry=ministry.AdditionalMinistry
            };
        }

        public static MinistryViewModel

            ToViewModel1(this mMinistry ministry, List<mAssembly> Assemblies, string Mode)
        {
            return new MinistryViewModel
            {
                AssemblyID = ministry.AssemblyID,
                MinistryID = ministry.MinistryID,
                MinistryName = ministry.MinistryName,
                MinistryNameLocal = ministry.MinistryNameLocal,
                //MinisterName = ministry.MinisterName,
                //MinisterNameLocal = ministry.MinisterNameLocal,
                //MemberCode = ministry.MemberCode,
                GetAssemblyName = ministry.GetAssemblyName,
                //GetMemberName = ministry.GetMemberName,
                //MemberStartDate = ministry.MemberStartDate!=null?ministry.MemberStartDate.Value.ToString("dd/MM/yyyy"):"",
               // MemberEndDate = ministry.MemberEndDate!=null?ministry.MemberEndDate.Value.ToString("dd/MM/yyyy"):"",
                IsActive = ministry.IsActive ?? false,
                //OrderID = ministry.OrderID,
                //MinisterPrefix = ministry.MinisterPrefix,
                //MinistryPrefix = ministry.MinistryPrefix,
                //MemberNames = new SelectList(Members, "MemberCode", "Name"),
                Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode,
                AdditionalMinistry=ministry.AdditionalMinistry

            };
        }
    }
}