﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfSessionDates
    {
        public static IEnumerable<SessionDatesViewModel> ToViewModel(this IEnumerable<mSessionDate> model)
        {

            var ViewModel = model.Select(sessiondate => new SessionDatesViewModel()
            {
                Id = sessiondate.Id,
                SessionId = sessiondate.SessionId,
                AssemblyId = sessiondate.AssemblyId,
                SessionDate = sessiondate.SessionDate.ToString(),
                SessionDateLocal = sessiondate.SessionDateLocal,
                SessionTime = sessiondate.SessionTime,         
                SessionTimeLocal = sessiondate.SessionTimeLocal,
                SessionEndTime = sessiondate.SessionEndTime,
                SessionEndTimeLocal = sessiondate.SessionEndTimeLocal,
                IsActive = sessiondate.IsActive ?? false,
                SessionDate_Local = sessiondate.SessionDate_Local,
                RotationMinister = sessiondate.RotationMinister,
                DisplayDate = sessiondate.DisplayDate,
                StartQListPath = sessiondate.StartQListPath,
                UnStartQListPath = sessiondate.UnStartQListPath,
                IsApproved = sessiondate.IsApproved,
                IsApprovedUnstart = sessiondate.IsApprovedUnstart,
                GetAssemblyName=sessiondate.GetAssemblyName,
                GetSessionName=sessiondate.GetSessionName,
                IsSitting = sessiondate.IsSitting
            });
            return ViewModel;
        }

        public static mSessionDate ToDomainModel(this SessionDatesViewModel model)
        {
            return new mSessionDate
            {
                Id = model.Id,
                SessionId = model.SessionId,
                AssemblyId = model.AssemblyId,
                SessionDate = string.IsNullOrEmpty(model.SessionDate) ? DateTime.Now : DateTime.ParseExact(model.SessionDate, "dd/MM/yyyy", null),
                
                SessionDateLocal = model.SessionDateLocal,
                SessionTime = model.SessionTime,
                SessionTimeLocal = model.SessionTimeLocal,
                SessionEndTime = model.SessionEndTime,
                SessionEndTimeLocal = model.SessionEndTimeLocal,
                IsActive = model.IsActive,
                SessionDate_Local = model.SessionDate_Local,
                RotationMinister = model.RotationMinister,
                DisplayDate = model.DisplayDate,
                StartQListPath = model.StartQListPath,
                UnStartQListPath = model.UnStartQListPath,
                IsApproved = model.IsApproved,
                IsApprovedUnstart = model.IsApprovedUnstart,
                GetAssemblyName = model.GetAssemblyName,
                GetSessionName = model.GetSessionName,
                IsSitting = model.IsSitting,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        //public static SessionDatesViewModel ToViewModel1(this mSessionDate model, List<mSession> Sessionnames, List<mAssembly> Assemblies,  string Mode)
        //{

           
        //    return new SessionDatesViewModel
        //    {
                
        //        Id = model.Id,
        //        SessionId = model.SessionId,
        //        AssemblyId = model.AssemblyId,
        //        //SessionDate =model.SessionDate.Day + "/" + model.SessionDate.Month + "/" + model.SessionDate.Year,
        //        SessionDate = model.SessionDate != null ? model.SessionDate.ToString("dd/MM/yyyy") : "",
        //        SessionDateLocal = model.SessionDateLocal,
        //        SessionTime = model.SessionTime,
        //        SessionTimeLocal = model.SessionTimeLocal,
        //        IsActive = model.IsActive ?? false,
        //        SessionDate_Local = model.SessionDate_Local,
        //        RotationMinister = model.RotationMinister,
        //        DisplayDate = model.DisplayDate,
        //        StartQListPath = model.StartQListPath,
        //        UnStartQListPath = model.UnStartQListPath,
        //        GetAssemblyName = model.GetAssemblyName,
        //        GetSessionName = model.GetSessionName,
        //        IsApproved = model.IsApproved,
        //        IsApprovedUnstart = model.IsApprovedUnstart,
        //        IsSitting = model.IsSitting,
        //        Sessions = new SelectList(Sessionnames, "SessionCode", "SessionName"),
        //        Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
        //        CreatedBy = CurrentSession.UserName,
        //        ModifiedBy = CurrentSession.UserName,
        //        Mode = Mode

        //    };
        //}


        public static SessionDatesViewModel ToViewModel1(this mSessionDate model, string Mode)
        {


            return new SessionDatesViewModel
            {
                SessionDateId=model.Id,
                Id = model.Id,
                SessionId = model.SessionId,
                AssemblyId = model.AssemblyId,
                //SessionDate =model.SessionDate.Day + "/" + model.SessionDate.Month + "/" + model.SessionDate.Year,
                SessionDate = model.SessionDate != null ? model.SessionDate.ToString("dd/MM/yyyy") : "",
                SessionDateLocal = model.SessionDateLocal,
                SessionTime = model.SessionTime,
                SessionTimeLocal = model.SessionTimeLocal,
                SessionEndTime = model.SessionEndTime,
                SessionEndTimeLocal = model.SessionEndTimeLocal,
                IsActive = model.IsActive ?? false,
                SessionDate_Local = model.SessionDate_Local,
                RotationMinister = model.RotationMinister,
                DisplayDate = model.DisplayDate,
                StartQListPath = model.StartQListPath,
                UnStartQListPath = model.UnStartQListPath,
                GetAssemblyName = model.GetAssemblyName,
                GetSessionName = model.GetSessionName,
                IsApproved = model.IsApproved,
                IsApprovedUnstart = model.IsApprovedUnstart,
                IsSitting = model.IsSitting,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                SPPQListPath = model.SPPQListPath,
                UPPQListPath = model.UPPQListPath,
                Mode = Mode

            };
        }
    }
}