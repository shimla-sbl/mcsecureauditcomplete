﻿using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfTelecomCircles
    {
        public static IEnumerable<TelecomCirclesViewModel> ToViewModel(this IEnumerable<mTelecomCircles> model)
        {

            var ViewModel = model.Select(TeleCir => new TelecomCirclesViewModel()
            {

                SlNo = TeleCir.SlNo,
                StateCode = TeleCir.StateCode,
                Description=TeleCir.Description

            });
            return ViewModel;
        }

        public static mTelecomCircles ToDomainModel(this TelecomCirclesViewModel telecom)
        {
            return new mTelecomCircles
            {
                SlNo = telecom.SlNo,
                StateCode = telecom.StateCode,
                Description=telecom.Description,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static TelecomCirclesViewModel ToViewModel1(this mTelecomCircles telecom, string Mode)
        {
            return new TelecomCirclesViewModel
            {
                SlNo = telecom.SlNo,
                StateCode = telecom.StateCode,
                Description=telecom.Description,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }
    }
}