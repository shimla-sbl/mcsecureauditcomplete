﻿using SBL.DomainModel.Models.Language;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfLanguages
    {
        public static IEnumerable<LanguagesViewModel> ToViewModel(this IEnumerable<Languages> model)
        {

            var ViewModel = model.Select(Lang => new LanguagesViewModel()
            {
                LanguageId = Lang.LanguageId,
                LanguageText = Lang.LanguageText,
                LanguageName = Lang.LanguageName,
                Defaultlang = Lang.Defaultlang,                
                Active = Lang.Active?? false
             

            });
            return ViewModel;
        }

        public static Languages ToDomainModel(this LanguagesViewModel model)
        {
            try
            {
                return new Languages
                {

                    LanguageId = model.LanguageId,
                    LanguageText = model.LanguageText,
                    LanguageName = model.LanguageName,
                    Defaultlang = model.Defaultlang,
                    Active = model.Active,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName

                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static LanguagesViewModel ToViewModel1(this Languages model, string Mode)
        {
            return new LanguagesViewModel
            {
                LanguageId = model.LanguageId,
                LanguageText = model.LanguageText,
                LanguageName = model.LanguageName,
                Defaultlang = model.Defaultlang,
                Active = model.Active ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,                
                Mode = Mode

            };
        }
    }
}