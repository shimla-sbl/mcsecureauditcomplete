﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfSessions
    {
        public static IEnumerable<SessionsViewModel> ToViewModel(this IEnumerable<mSession> model)
        {

            var ViewModel = model.Select(session => new SessionsViewModel()
            {
                SessionID = session.SessionID,
                AssemblyID = session.AssemblyID,
                SessionName = session.SessionName,
                SessionNameLocal = session.SessionNameLocal,
                StartDate = session.StartDate.ToString(),
                EndDate = session.EndDate.ToString(),
                SessionPeriod = session.SessionPeriod,
                SessionPeriodLocal = session.SessionPeriodLocal,
                SessionDescription = session.SessionDescription,
                StaredQuestion = session.StaredQuestion,
                SessionType = session.SessionType,
                UnstaredQuestions = session.UnstaredQuestions,
                BillsIntroduced = session.BillsIntroduced,
                BillsPassed = session.BillsPassed,
                CutMotions = session.CutMotions,
                GetAssemblyName = session.GetAssemblyName,
                GetSessionType=session.GetSessionType,
               
                SessionCode = session.SessionCode,
                IsPublished = session.IsPublished ?? false,
                SessionCalanderLocation = session.SessionCalanderLocation,
                SessionStatus = session.SessionStatus,
                RotationMinisterLocation = session.RotationMinisterLocation

            });
            return ViewModel;
        }

        public static mSession ToDomainModel(this SessionsViewModel model)
        {
            try
            {
                return new mSession
                {

                    SessionID = (Int32)(model.SessionID != null ? model.SessionID : 0),
                    AssemblyID = model.AssemblyID,
                    SessionName = model.SessionName,
                    SessionNameLocal = model.SessionNameLocal,
                    StartDate = string.IsNullOrEmpty(model.StartDate) ? DateTime.Now : DateTime.ParseExact(model.StartDate, "dd/MM/yyyy", null),
                    EndDate = string.IsNullOrEmpty(model.EndDate) ? DateTime.Now : DateTime.ParseExact(model.EndDate, "dd/MM/yyyy", null),
                    SessionPeriod = model.SessionPeriod,
                    SessionPeriodLocal = model.SessionPeriodLocal,
                    SessionDescription = model.SessionDescription,
                   StaredQuestion = model.StaredQuestion,
                    SessionType = model.SessionType,
                    UnstaredQuestions = model.UnstaredQuestions,
                    BillsIntroduced = model.BillsIntroduced,
                    BillsPassed = model.BillsPassed,
                    GetAssemblyName = model.GetAssemblyName,
                    GetSessionType = model.GetSessionType,
                    CutMotions = model.CutMotions,
                    SessionCode = model.SessionCode,
                    IsPublished = model.IsPublished,
                    SessionCalanderLocation = model.SessionCalanderLocation,
                    SessionStatus = model.SessionStatus,
                   RotationMinisterLocation = model.RotationMinisterLocation,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName

                };
            }
            catch (Exception)
            {
                
                throw;
            }
           
        }


        public static SessionsViewModel ToViewModel1(this mSession model, List<mAssembly> Assemblies, List<mSessionType> sessionType, string Mode)
        {
            return new SessionsViewModel
            {
                SessionID = model.SessionID,
                AssemblyID = model.AssemblyID,
                SessionName = model.SessionName,
                SessionNameLocal = model.SessionNameLocal,
                StartDate = model.StartDate != null ? model.StartDate.Value.ToString("dd/MM/yyyy") : "",
                EndDate = model.EndDate != null ? model.EndDate.Value.ToString("dd/MM/yyyy") : "",
                SessionPeriod = model.SessionPeriod,
                SessionPeriodLocal = model.SessionPeriodLocal,
                SessionDescription = model.SessionDescription,
                StaredQuestion = model.StaredQuestion,
                SessionType = model.SessionType,
                UnstaredQuestions = model.UnstaredQuestions,
                BillsIntroduced = model.BillsIntroduced,
                BillsPassed = model.BillsPassed,
                CutMotions = model.CutMotions,
                GetAssemblyName = model.GetAssemblyName,
                GetSessionType = model.GetSessionType,
                SessionCode = model.SessionCode,
                IsPublished = model.IsPublished ?? false ,
                SessionCalanderLocation = model.SessionCalanderLocation,
                SessionStatus = model.SessionStatus,
                RotationMinisterLocation = model.RotationMinisterLocation,
                SessionTypes = new SelectList(sessionType, "TypeID", "TypeName"),
                Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }
    }
}