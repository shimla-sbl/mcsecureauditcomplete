﻿using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfSiteSettings
    {

        public static IEnumerable<SiteSettingViewModel> ToViewModel(this IEnumerable<SiteSettings> model)
        {

            var ViewModel = model.Select(sett => new SiteSettingViewModel()
            {
                SettingId = sett.SettingId,
                SettingName = sett.SettingName,
                SettingValue = sett.SettingValue,
                SettingValueLocal = sett.SettingValueLocal,
                Description = sett.Description,
                SignaturePath = sett.SignaturePath,
                IsActive = sett.IsActive

            });
            return ViewModel;
        }

        public static SiteSettings ToDomainModel(this SiteSettingViewModel model)
        {
            return new SiteSettings
            {

                SettingId = model.SettingId,
                SettingName = model.SettingName,
                SettingValue = model.SettingValue,
                SettingValueLocal = model.SettingValueLocal,
                Description = model.Description,
                SignaturePath = model.SignaturePath,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static SiteSettingViewModel ToViewModel1(this SiteSettings model, string Mode)
        {
            return new SiteSettingViewModel
            {
                SettingId = model.SettingId,
                SettingName = model.SettingName,
                SettingValue = model.SettingValue,
                SettingValueLocal = model.SettingValueLocal,
                Description = model.Description,
                SignaturePath = model.SignaturePath,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }
    }
}