﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.Models.Mobiles;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfMobileTrack
    {
        public static IEnumerable<MobileTrackViewModel> ToViewModel(this IEnumerable<mMobileTrack> model)
        {

            var ViewModel = model.Select(mobiTrack => new MobileTrackViewModel()
            {

                SlNo = mobiTrack.SlNo,
                NetworkCode = mobiTrack.NetworkCode,
                ZoneCode = mobiTrack.ZoneCode,
                StateCode = mobiTrack.StateCode

            });
            return ViewModel;
        }

        public static mMobileTrack ToDomainModel(this MobileTrackViewModel mobitrack)
        {
            return new mMobileTrack
            {
                SlNo = mobitrack.SlNo,
                NetworkCode = mobitrack.NetworkCode,
                ZoneCode = mobitrack.ZoneCode,
                StateCode = mobitrack.StateCode,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static MobileTrackViewModel ToViewModel1(this mMobileTrack mobitrack, List<SelectListItem> stateCodes, List<SelectListItem> networkCodes, string Mode)
        {
            return new MobileTrackViewModel
            {
                SlNo = mobitrack.SlNo,
                NetworkCode = mobitrack.NetworkCode,
                ZoneCode = mobitrack.ZoneCode,
                StateCode = mobitrack.StateCode,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                StateCodes = new SelectList(stateCodes, "Value", "Text"),
                NetWorkCodes = new SelectList(networkCodes, "Value", "Text"),
                Mode = Mode

            };
        }
    }
}