﻿using SBL.DomainModel.Models.Document;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfDocumentType
    {
        public static IEnumerable<DocumentTypeViewModel> ToViewModel(this IEnumerable<mDocumentType> model)
        {

            var ViewModel = model.Select(dtype => new DocumentTypeViewModel()
            {
                DocumentTypeID = dtype.DocumentTypeID,
                DocumentType = dtype.DocumentType,
                DocumentTypeLocal = dtype.DocumentTypeLocal,
                IsActive = dtype.IsActive ?? false,

            });
            return ViewModel;
        }

        public static mDocumentType ToDomainModel(this DocumentTypeViewModel model)
        {
            return new mDocumentType
            {
                DocumentTypeID = model.DocumentTypeID,
                DocumentType = model.DocumentType,
                DocumentTypeLocal = model.DocumentTypeLocal,
                IsActive = model.IsActive,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static DocumentTypeViewModel ToViewModel1(this mDocumentType model, string Mode)
        {
            return new DocumentTypeViewModel
            {
                DocumentTypeID = model.DocumentTypeID,
                DocumentType = model.DocumentType,
                DocumentTypeLocal = model.DocumentTypeLocal,
                IsActive = model.IsActive ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode

            };
        }

    }
}