﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Secretory;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfSecretoryDepartment
    {
        public static IEnumerable<SecretoryDepartmentViewModel> ToViewModel(this IEnumerable<mSecretoryDepartment> model)
        {

            var ViewModel = model.Select(Sdept => new SecretoryDepartmentViewModel()
            {
                SecretoryDepartmentID = Sdept.SecretoryDepartmentID,
                SecretoryID = Sdept.SecretoryID,
                DepartmentID = Sdept.DepartmentID,
                AssemblyID = Sdept.AssemblyID,
                IsActive = Sdept.IsActive ?? false,
                GetAssemblyName = Sdept.GetAssemblyName,
                GetDeptName = Sdept.GetDeptName,
                GetSecretoryName = Sdept.GetSecretoryName,
                OrderID = Sdept.OrderID


            });
            return ViewModel;
        }

        public static mSecretoryDepartment ToDomainModel(this SecretoryDepartmentViewModel model)
        {
            try
            {
                return new mSecretoryDepartment
                {

                    SecretoryDepartmentID = model.SecretoryDepartmentID,
                    SecretoryID = model.SecretoryID,
                    DepartmentID = model.DepartmentID,
                    AssemblyID = model.AssemblyID,
                    IsActive = model.IsActive,
                    OrderID = model.OrderID,
                    GetAssemblyName = model.GetAssemblyName,
                    GetDeptName = model.GetDeptName,
                    GetSecretoryName = model.GetSecretoryName,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName

                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static SecretoryDepartmentViewModel ToViewModel1(this mSecretoryDepartment model, List<mSecretory> SecretoryName, List<mAssembly> Assemblies, List<mDepartment> Departments, string Mode)
        {
            return new SecretoryDepartmentViewModel
            {
                SecretoryDepartmentID = model.SecretoryDepartmentID,
                SecretoryID = model.SecretoryID,
                DepartmentID = model.DepartmentID,
                AssemblyID = model.AssemblyID,
                IsActive = model.IsActive ?? false,
                OrderID = model.OrderID,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                GetAssemblyName = model.GetAssemblyName,
                GetDeptName = model.GetDeptName,
                GetSecretoryName = model.GetSecretoryName,
                SecretoryNames = new SelectList(SecretoryName, "SecretoryID", "SecretoryName"),
                Assembly = new SelectList(Assemblies, "AssemblyCode", "AssemblyName"),
                Department = new SelectList(Departments, "deptId", "deptname"),
                Mode = Mode

            };
        }
    }
}