﻿using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.States;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfMember
    {

        public static List<mMemberViewModel> ToViewModel(this IEnumerable<mMember> model)
        {

            var ViewModel = model.Select(memb => new mMemberViewModel()
            {
                MemberID = memb.MemberID,
                MemberCode = memb.MemberCode.ToString(),
                StateNameID = memb.StateNameID,
                Email = memb.Email,
                LoginID = memb.LoginID,
                CmRefnicVipCode = memb.CmRefnicVipCode,
                Prefix = memb.Prefix,
                Name = memb.Name,
                Active = memb.Active ?? false,
                isAlive = memb.isAlive,
                NameLocal = memb.NameLocal,
                GetStateName=memb.GetStateName,
                GetDistrictName=memb.GetDistrictName,
                HighestQualificationID = memb.HighestQualificationID,
                GetHighestQualificationName = memb.GetHighestQualificationName,
                PreviousQualification = memb.PreviousQualification,
                DeactivationReason = memb.DeactivationReason,
                Remarks = memb.Remarks,
                DistrictID = memb.DistrictID,
                ShimlaAddress = memb.ShimlaAddress,
                PermanentAddress = memb.PermanentAddress,
                Sex = memb.Sex,
                memdob = memb.memdob != null ? memb.memdob.Value.ToString("dd/MM/yyyy") : "",
                TelOffice = memb.TelOffice,
                TelResidence = memb.TelResidence,
                Mobile = memb.Mobile,
                AlternateMobile=memb.AlternateMobile,
                FatherName = memb.FatherName,
                Description = memb.Description,
                FullImage = memb.FileName,
                ThumbImage = memb.FileName,
                FileLocation = memb.ImageAcessurl,
                ImageLocation = memb.ImageAcessurl,
                MonthlySMSQuota = memb.MonthlySMSQuota,
                AadhaarNo = memb.AadhaarNo

            });
            return ViewModel.ToList();
        }

        public static SelectList GetNameTitles()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Mr.", Value = "Mr." });
            result.Add(new SelectListItem() { Text = "Mrs.", Value = "Mrs." });
            result.Add(new SelectListItem() { Text = "Miss.", Value = "Miss." });
            result.Add(new SelectListItem() { Text = "Sh.", Value = "Sh." });
            result.Add(new SelectListItem() { Text = "Smt.", Value = "Smt." });
            result.Add(new SelectListItem() { Text = "Dr.", Value = "Dr." });
            result.Add(new SelectListItem() { Text = "Prof.", Value = "Prof." });
            result.Add(new SelectListItem() { Text = "Col.", Value = "Col." });
            result.Add(new SelectListItem() { Text = "Er.", Value = "Er." });
            result.Add(new SelectListItem() { Text = "Dr.(Col.)", Value = "Dr.(Col.)" });
        
            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetGenderTitles()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "MALE", Value = "MALE" });
            result.Add(new SelectListItem() { Text = "FEMALE", Value = "FEMALE" });

            return new SelectList(result, "Value", "Text");
        }

        public static mMember ToDomainModel(this mMemberViewModel model)
        {
            return new mMember
            {
                MemberID = (Int32)(model.MemberID != null ? model.MemberID : 0),
                MemberCode = Convert.ToInt32( model.MemberCode),
                StateNameID = model.StateNameID,
                Email = model.Email,
                //LoginID = model.LoginID,
                //CmRefnicVipCode = model.CmRefnicVipCode,
                Prefix = model.Prefix,
                Name = model.Name,
                Active = model.Active,
                isAlive = model.isAlive,
                NameLocal = model.NameLocal,
                DistrictID = model.DistrictID,
                ShimlaAddress = model.ShimlaAddress,
                PermanentAddress = model.PermanentAddress,
                Sex = model.Sex,
                memdob = string.IsNullOrEmpty(model.memdob) ? DateTime.Now : DateTime.ParseExact(model.memdob, "dd/MM/yyyy", null),
                HighestQualificationID = model.HighestQualificationID,
                GetHighestQualificationName = model.GetHighestQualificationName,
                PreviousQualification = model.PreviousQualification,
                DeactivationReason = model.DeactivationReason,
                Remarks = model.Remarks,
                GetStateName = model.GetStateName,
                GetDistrictName = model.GetDistrictName,
                TelOffice = model.TelOffice,
                TelResidence = model.TelResidence,
                Mobile = model.Mobile,
                AlternateMobile=model.AlternateMobile,
                FatherName = model.FatherName,
                Description = model.Description,
                AadhaarNo = model.AadhaarNo,
                FileName = model.FullImage,
                FilePath = model.FileLocation,
                ThumbName = model.ThumbName,
                MonthlySMSQuota = model.MonthlySMSQuota,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                IsAuthorty = model.isAuthorized
            };
        }

        public static mMemberViewModel ToViewModel1(this mMember model, List<DistrictModel> Districtcodes, List<mStates> StateName, List<mMemberHighestQualification> HighestQualification, string Mode)
        {
            return new mMemberViewModel
            {
                MemberID = model.MemberID,
                MemberCode = model.MemberCode.ToString(),
                StateNameID = model.StateNameID,
                //LoginID = model.LoginID,
                //CmRefnicVipCode = model.CmRefnicVipCode,
                Email = model.Email,
                Prefix = model.Prefix,
                Name = model.Name,
                NameLocal = model.NameLocal,
                Active = model.Active ?? false,
                isAlive = model.isAlive,
                DistrictID = model.DistrictID,
                ShimlaAddress = model.ShimlaAddress,
                PermanentAddress = model.PermanentAddress,
                Sex = model.Sex,
                memdob = model.memdob != null ? model.memdob.Value.ToString("dd/MM/yyyy") : "",
                HighestQualificationID = model.HighestQualificationID,
                GetHighestQualificationName = model.GetHighestQualificationName,
                PreviousQualification = model.PreviousQualification,
                DeactivationReason = model.DeactivationReason,
                Remarks = model.Remarks,

                GetStateName = model.GetStateName,
                GetDistrictName = model.GetDistrictName,
                TelOffice = model.TelOffice,
                TelResidence = model.TelResidence,
                Mobile = model.Mobile,
                AlternateMobile=model.AlternateMobile,
                FatherName = model.FatherName,
                Description = model.Description,
                AadhaarNo = model.AadhaarNo,
                FullImage = model.FileName,
                ThumbImage = model.FileName,
                FileLocation = model.FilePath,
                MonthlySMSQuota = model.MonthlySMSQuota,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Districts = new SelectList(Districtcodes, "DistrictCode", "DistrictName"),
                StateNames = new SelectList(StateName, "mStateID", "StateName"),
                HighestQualification = new SelectList(HighestQualification, "MemberHighestQualificationId", "Qualification"),
                Mode = Mode,
                isAuthorized = Convert.ToBoolean(model.IsAuthorty)
            };
        }

    }
}