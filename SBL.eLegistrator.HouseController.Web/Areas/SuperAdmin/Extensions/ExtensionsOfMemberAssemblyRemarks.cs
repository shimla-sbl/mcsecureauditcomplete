﻿using SBL.DomainModel.Models.MemberAssemblyRemarks;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionsOfMemberAssemblyRemarks
    {

        public static IEnumerable<MemberAssemblyRemarksViewModel> ToViewModel(this IEnumerable<mMemberAssemblyRemarks> model)
        {

            var ViewModel = model.Select(AssemRem => new MemberAssemblyRemarksViewModel()
            {
                MemberAssemblyRemarksID = AssemRem.MemberAssemblyRemarksID,
                MemberAssemblyRemarks = AssemRem.MemberAssemblyRemarks,
                Active = AssemRem.Active ?? false

            });
            return ViewModel;
        }

        public static mMemberAssemblyRemarks ToDomainModel(this MemberAssemblyRemarksViewModel model)
        {
            return new mMemberAssemblyRemarks
            {
                MemberAssemblyRemarksID = model.MemberAssemblyRemarksID,
                MemberAssemblyRemarks = model.MemberAssemblyRemarks,
                Active = model.Active,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName

            };
        }

        public static MemberAssemblyRemarksViewModel ToViewModel1(this mMemberAssemblyRemarks model, string mode)
        {
            return new MemberAssemblyRemarksViewModel
            {
                MemberAssemblyRemarksID = model.MemberAssemblyRemarksID,
                MemberAssemblyRemarks = model.MemberAssemblyRemarks,
                Active = model.Active ?? false,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = mode
            };
        }
    }
}