﻿using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfMemberDesignation
    {

        public static IEnumerable<MemberDesignationViewModel> ToViewModel(this IEnumerable<mMemberDesignation> model)
        {

            var ViewModel = model.Select(memDesig => new MemberDesignationViewModel()
            {
                memDesigId = memDesig.memDesigId,
                memDesigcode = memDesig.memDesigcode,
                memDesigname = memDesig.memDesigname,
                memDesiglocal = memDesig.memDesiglocal,
                Active = memDesig.Active ?? true

            });
            return ViewModel;
        }

        public static mMemberDesignation ToDomainModel(this MemberDesignationViewModel model)
        {
            try
            {
                return new mMemberDesignation
                {
                    memDesigId = (Int32)(model.memDesigId != null ? model.memDesigId : 0),
                    //memDesigId = model.memDesigId,
                    memDesigcode = model.memDesigcode,
                    memDesigname = model.memDesigname,
                    memDesiglocal = model.memDesiglocal,
                    Active = model.Active,
                    CreatedBy = CurrentSession.UserName,
                    ModifiedBy = CurrentSession.UserName
                };
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static MemberDesignationViewModel ToViewModel1(this mMemberDesignation model, string Mode)
        {
            return new MemberDesignationViewModel
            {
                memDesigId = model.memDesigId,
                memDesigcode = model.memDesigcode,
                memDesigname = model.memDesigname,
                memDesiglocal = model.memDesiglocal,
                Active = model.Active ?? true,
                Mode = Mode

            };
        }


    }
}