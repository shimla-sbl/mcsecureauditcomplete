﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using SBL.DomainModel.Models.FooterPublicData;
using SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.SuperAdmin.Extensions
{
    public static class ExtensionOfFooterPublicData
    {



        public static FooterPublicData ToDomainModel(this FooterPublicDataViewModel model)
        {
            return new FooterPublicData
            {

                ID = model.VID,
                TitleID = model.VTitleID,
                TitleName = model.VTitleName,
                TitleDescription = model.VTitleDescription
                

            };
        }


        public static List<FooterPublicDataViewModel> ToViewModel(this IEnumerable<FooterPublicData> model)
        {

            var ViewModel = model.Select(part => new FooterPublicDataViewModel()
            {
                VID = part.ID,
                VTitleID = part.TitleID,
                VTitleName = part.TitleName,
                VTitleDescription = part.TitleDescription
                
            });
            return ViewModel.ToList();
        }



        public static FooterPublicDataViewModel ToViewModel1(this FooterPublicData model, string Mode)
        {
            return new FooterPublicDataViewModel
            {
                VID = model.ID,
                VTitleID = model.TitleID,
                VTitleName = model.TitleName,
                VTitleDescription = model.TitleDescription,
                
                Mode = Mode

            };
        }





    }
}