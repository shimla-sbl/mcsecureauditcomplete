﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses
{
    public class VehiclePassesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "VehiclePasses";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "VehiclePasses_default",
                "VehiclePasses/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
