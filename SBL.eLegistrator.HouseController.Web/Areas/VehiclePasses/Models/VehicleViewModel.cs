﻿namespace SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Models
{
    #region Namespace Reffrences
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Officers;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Session;
    using System.ComponentModel.DataAnnotations.Schema;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;
    #endregion

    public class VehicleViewModel : PhotoCaptureGenericModel
    {
        public VehicleViewModel()
        {
            this.ListPassCategory = new List<PassCategory>();
            this.VehiclePassList = new List<VehicleViewModel>();
        }

        public int PassID { get; set; }

        public Int32? PassCode { get; set; }

        [Required(ErrorMessage = "Pass Type Is Required")]
        public int PassCategoryID { get; set; }

        public int AssemblyCode { get; set; }

        public int SessionCode { get; set; }

        public string Prefix { get; set; }
        
        public string AadharID { get; set; }

        [Required(ErrorMessage = "Please Enter Name")]
        [StringLength(50, ErrorMessage = "Maximum 30 characters allowed")]
        [RegularExpression(@"^[a-zA-Z''-'\s]{1,40}$", ErrorMessage =
            "Numbers and special characters are not allowed")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Gender is required")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "Age is required")]
        public int? Age { get; set; }

        //[Required(ErrorMessage = "Father Name is required")]
        public string FatherName { get; set; }

        //[Required(ErrorMessage = "No Of Persion is required")]
        public int? NoOfPersions { get; set; }

        //[Required(ErrorMessage = "Recommendation Type is required")]
        public string RecommendationType { get; set; }

        //[Required(ErrorMessage = "Recommendation By is required")]
        public string RecommendationBy { get; set; }

        public string RecommendationDescription { get; set; }

        [StringLength(15, ErrorMessage = "Maximum 15 characters allowed")]
        [RegularExpression(@"^[0-9\.\+\-\/]+$", ErrorMessage = "Invalid phone number")]
        [Required(ErrorMessage = "Mobile Number is required")]
        public string MobileNo { get; set; }

        public string Email { get; set; }

        public string Address { get; set; }

        public string OrganizationName { get; set; }

        [Required(ErrorMessage = "Designation is required")]
        public string Designation { get; set; }

        //[Required(ErrorMessage = "From and To dates are required")]
        public string DayTime { get; set; }

        public TimeSpan? Time { get; set; }

        [Required(ErrorMessage = "Department is required")]
        public string DepartmentID { get; set; }

        [Required(ErrorMessage = "From date is required")]
        public string SessionDateFrom { get; set; }

        [Required(ErrorMessage = "To date is required")]
        public string SessionDateTo { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public string ApprovedBy { get; set; }

        [Required(ErrorMessage = "Vehicle Number Is Required")]
        public string VehicleNumber { get; set; }

        public string GateNumber { get; set; }

        public bool IsRequested { get; set; }

        public DateTime? RequestedDate { get; set; }

        public int? Status { get; set; }

        public string RequestedBy { get; set; }

        public bool IsActive { get; set; }

        //Newly Created.
        public string RecommendationByName { get; set; }

        public string Mode { get; set; }

        public string RejectedBy { get; set; }

        public DateTime? RejectionDate { get; set; }

        public string RejectionReason { get; set; }

        public string PassCategoryName { get; set; }

        public string PassCategoryTemplate { get; set; }

        public bool DeptApprovalNeeded { get; set; }

        public bool IsApproved { get; set; }

        #region #region Lists
        public SelectList PassTypeList { get; set; }

        public SelectList GenderList { get; set; }

        public List<PassCategory> ListPassCategory { get; set; }

        public SelectList RecommendationTypeList { get; set; }

        public virtual ICollection<mOfficerDetails> RecomendedByListOfficer { get; set; }

        public SelectList TimeTypeList { get; set; }

        public virtual ICollection<mSessionDate> mSessionDateList { get; set; }
        public string RequestdID { get; set; }
        public List<mDepartment> mDepartment { get; set; }
        public List<mDepartment> mDepartmentList { get; set; }
        public List<VehicleViewModel> VehiclePassList { get; set; }

        [NotMapped]
        public int ValidateSessionID { get; set; }
        public string IsRequestUserID { get; set; }
        #endregion
    }
}