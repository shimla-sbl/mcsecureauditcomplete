﻿namespace SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Controllers
{
    #region Namespace Reffrences
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Models;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Extensions;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using SBL.DomainModel.Models.Session;
    using System.Linq;
    using SBL.DomainModel.Models.PaperLaid;
    using System.Web;
    using SBL.DomainModel.Models.Diaries;
    using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
    using SBL.eLegistrator.HouseController.Filters;
    using SBL.eLegistrator.HouseController.Web.Filters;
    using SBL.DomainModel.Models.Adhaar;
    #endregion

    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class VehiclePassMinisterController : Controller
    {
        //
        // GET: /VehiclePasses/VehiclePassMinister/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PartialVehiclePassList(int Status = 0, int PassCategoryID = 0, string DepartmentCatID = "")
        {
            VehicleViewModel model = new VehicleViewModel();
            List<mDepartmentPasses> ListdeprtmentPasses = new List<mDepartmentPasses>();
            mDepartmentPasses model1 = new mDepartmentPasses();
            if (Utility.CurrentSession.UserID != null && Utility.CurrentSession.UserID != "")
            {
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                //model1.AssemblyCode = Convert.ToInt16(siteSettingMod.AssemblyCode);
                //model1.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model1.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model1.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
                }

                tPaperLaidV objPaperLaid = new tPaperLaidV();

                if (CurrentSession.UserName != null && CurrentSession.UserName != "")
                {
                    objPaperLaid.LoginId = CurrentSession.UserName;
                    objPaperLaid.UserID = new Guid(CurrentSession.UserID);
                    model1.IsRequestUserID = CurrentSession.UserID;
                }

                objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);

                DiaryModel DMdl = new DiaryModel();
                DMdl.MinistryId = objPaperLaid.MinistryId;
                DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

                if (DMdl.DiaryList != null)
                {
                    List<mDepartment> objDept = new List<mDepartment>();
                    foreach (var item in DMdl.DiaryList.Distinct())
                    {
                        model1.DepartmentID += item.DepartmentId + ",";
                    }
                    model1.DepartmentID = model1.DepartmentID.Substring(0, model1.DepartmentID.LastIndexOf(","));
                }
                else
                {
                    model1.DepartmentID = string.Empty;
                }

                DMdl.MinistryId = objPaperLaid.MinistryId;
                DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

                if (DMdl.DiaryList != null)
                {
                    List<SBL.DomainModel.Models.Department.mDepartment> objDept = new List<SBL.DomainModel.Models.Department.mDepartment>();
                    foreach (var item in DMdl.DiaryList.Distinct())
                    {
                        SBL.DomainModel.Models.Department.mDepartment obj = new SBL.DomainModel.Models.Department.mDepartment();
                        obj.deptId = item.DepartmentId;
                        obj.deptname = item.DepartmentName;
                        objDept.Add(obj);
                    }

                    if (objDept != null)
                    {
                        model.mDepartmentList = objDept;
                    }
                }


                //var DepartmentCategoryList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Passes", "GetDepartmentCategorys", null);
                model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
                ListdeprtmentPasses = (List<mDepartmentPasses>)Helper.ExecuteService("Passes", "GetDepartmentVehiclePassesByDeptId", model1);
                var ListViewModel = ListdeprtmentPasses.ToModelList();
                // model.mDepartmentList = DepartmentCategoryList;
                if (DepartmentCatID != "")
                {
                    ListViewModel = ListViewModel.Where(m => m.DepartmentID == DepartmentCatID).ToList();

                }

                if (PassCategoryID != 0)
                {
                    ListViewModel = ListViewModel.Where(m => m.PassCategoryID == PassCategoryID).ToList();

                }

                if (Status != 0)
                {
                    if (Status == 10)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 1 && m.IsRequested == false).ToList();
                    }
                    else if (Status == 1)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 1 && m.IsRequested == true && m.IsApproved == false).ToList();
                    }
                    else if (Status == 2)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 2 && m.IsRequested == true).ToList();
                    }

                }

                //model1.DepartmentID = Utility.CurrentSession.DeptID;
                model.DepartmentID = DepartmentCatID;
                model.PassCategoryID = PassCategoryID;


                model.VehiclePassList = ListViewModel;
                model.SessionCode = model1.SessionCode;
                model.ValidateSessionID = siteSettingMod.SessionCode;
                return PartialView("_VehiclePassesGridPending", model);
            }
            return PartialView("_VehiclePassesGridPending", model);
        }

        [HttpGet]
        public ActionResult VehiclePassRequest()
        {
            VehicleViewModel model = new VehicleViewModel();
            tPaperLaidV objPaperLaid = new tPaperLaidV();
            model.GenderList = ExtensionMethods.GetGender();
            model.TimeTypeList = ExtensionMethods.GetTimeTypeList();

            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            session.AssemblyID = model.AssemblyCode = Convert.ToInt16(siteSettingMod.AssemblyCode);
            session.SessionCode = model.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);

            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesList", new PublicPass { SessionCode = model.SessionCode, AssemblyCode = model.AssemblyCode }) as List<mSessionDate>;
            model.mSessionDateList = sessionDates;
            if (string.IsNullOrEmpty(model.SessionDateTo))
            {
                var sessionDate = sessionDates.OrderByDescending(m => m.DisplayDate).Take(1).FirstOrDefault() as mSessionDate;
                model.SessionDateTo = sessionDate.DisplayDate.ToString();
            }

            model.Mode = "Add";
            string DepartmentId = Utility.CurrentSession.DeptID;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                objPaperLaid.UserID = new Guid(CurrentSession.UserID);
            }



            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                objPaperLaid.LoginId = CurrentSession.UserName;
            }

            objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);

            DiaryModel DMdl = new DiaryModel();
            DMdl.MinistryId = objPaperLaid.MinistryId;
            DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

            if (DMdl.DiaryList != null)
            {
                List<SBL.DomainModel.Models.Department.mDepartment> objDept = new List<SBL.DomainModel.Models.Department.mDepartment>();
                foreach (var item in DMdl.DiaryList.Distinct())
                {
                    SBL.DomainModel.Models.Department.mDepartment obj = new SBL.DomainModel.Models.Department.mDepartment();
                    obj.deptId = item.DepartmentId;
                    obj.deptname = item.DepartmentName;
                    objDept.Add(obj);
                }

                if (objDept != null)
                {
                    model.mDepartment = objDept;
                }
            }

            AdhaarDetails mUserDetails = new AdhaarDetails();
            string AadharID = CurrentSession.AadharId;
            if (AadharID != null)
            {
                mUserDetails.AdhaarID = AadharID;
                mUserDetails = (AdhaarDetails)Helper.ExecuteService("User", "GetUser_DetailsByAdhaarID", mUserDetails);
                if (mUserDetails != null)
                {
                    model.RecommendationBy = mUserDetails.Name;
                }
            }
          //  model.RecommendationBy = Utility.CurrentSession.Name;

            //mDepartmentPasses department = new mDepartmentPasses();
            //department.DepartmentID = model.DepartmentID;
            //model.mDepartment = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Passes", "GetDepartmentByids", department);

            model.RecommendationType = "Minister";
            model.AssemblyCode = session.AssemblyID;
            model.SessionCode = session.SessionCode;

            model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategoriesForVehicles", null);

            return PartialView("_NewVehiclePassRequest", model);
        }

        [HttpPost]
        public ActionResult VehiclePassRequest(VehicleViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SBL.DomainModel.Models.Department.mDepartment department = new DomainModel.Models.Department.mDepartment();
                    department.deptId = model.DepartmentID;
                    department = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", department);
                    model.OrganizationName = department.deptname.Trim();
                    model.IsActive = true;

                    var EntityModel = model.ToEntity();
                    EntityModel.IsVehiclaPass = true;
                    if (model.Mode == "Add")
                    {
                        if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                        {
                            EntityModel.IsRequestUserID = CurrentSession.UserID;
                        }
                        Helper.ExecuteService("Passes", "AddPassRequest", EntityModel);
                        TempData["Msg"] = "Save";
                        TempData["Dept"] = "Mins";
                        TempData["ReqMesg"] = EntityModel.RequestdID;
                      //  Notification.SendEmailDetails(EntityModel);
                    }
                    else
                    {
                        Helper.ExecuteService("Passes", "UpdatePassRequest", EntityModel);
                        TempData["Msg"] = "Update";
                    }
                }
                string Value = "";
                if ((string)TempData["Msg"] == "Save")
                {
                    Value = "Save" + "," + TempData["ReqMesg"];
                    return Json(Value, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Value = "Update";
                    return Json(Value, JsonRequestBehavior.AllowGet);

                }
                // return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
            }
            catch (Exception)
            {
                return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                throw;
            }
        }

        public ActionResult FullPassDetails(int Id)
        {
            var PassRequest = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", new mDepartmentPasses { PassID = Id });

            var model = PassRequest.ToModel();

            return View(model);
        }

        public object SubmitSelectedDepartmentPass(string PassIDs)
        {
            string[] PassIds = PassIDs.Split(',');

            foreach (var PassID in PassIds)
            {
                mDepartmentPasses model = new mDepartmentPasses();
                model.PassID = Convert.ToInt32(PassID);
                Helper.ExecuteService("Passes", "SubmitDepartmentPassRequest", model);
            }

            return null;
        }

        public object SubmitDepartmentPass(string PassID)
        {
            mDepartmentPasses model = new mDepartmentPasses();
            model.PassID = Convert.ToInt16(PassID);
            Helper.ExecuteService("Passes", "SubmitDepartmentPassRequest", model);
            return null;
        }

        public ActionResult EditPassRequest(int Id)
        {
            var PassRequest = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", new mDepartmentPasses { PassID = Id });

            var model = PassRequest.ToModel();

            tPaperLaidV objPaperLaid = new tPaperLaidV();
            string DepartmentId = Utility.CurrentSession.DeptID;

            if (CurrentSession.UserName != null && CurrentSession.UserName != "")
            {
                objPaperLaid.LoginId = CurrentSession.UserName;
            }

            objPaperLaid = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", objPaperLaid);

            DiaryModel DMdl = new DiaryModel();
            DMdl.MinistryId = objPaperLaid.MinistryId;
            DMdl.DiaryList = (List<DiaryModel>)Helper.ExecuteService("LegislationFixation", "GetDepartmentByMinister", DMdl);

            if (DMdl.DiaryList != null)
            {
                List<SBL.DomainModel.Models.Department.mDepartment> objDept = new List<SBL.DomainModel.Models.Department.mDepartment>();
                foreach (var item in DMdl.DiaryList.Distinct())
                {
                    SBL.DomainModel.Models.Department.mDepartment obj = new SBL.DomainModel.Models.Department.mDepartment();
                    obj.deptId = item.DepartmentId;
                    obj.deptname = item.DepartmentName;
                    objDept.Add(obj);
                }

                if (objDept != null)
                {
                    model.mDepartment = objDept;
                }
            }


            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            session.AssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
            session.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);
            List<mSessionDate> ListSessionDates = new List<mSessionDate>();


            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesList", new PublicPass { SessionCode = session.SessionCode, AssemblyCode = session.AssemblyID }) as List<mSessionDate>;
            model.mSessionDateList = sessionDates;
            if (string.IsNullOrEmpty(model.SessionDateTo))
            {
                var sessionDate = sessionDates.OrderByDescending(m => m.DisplayDate).Take(1).FirstOrDefault() as mSessionDate;
                model.SessionDateTo = sessionDate.DisplayDate.ToString();
            }


            //var sessionDates = Helper.ExecuteService("Session", "GetSessionDateBySessionCode", session) as List<mSessionDate>;
            //model.mSessionDateList = sessionDates;
            //if (string.IsNullOrEmpty(model.SessionDateTo))
            //{
            //    var sessionDate = sessionDates.OrderByDescending(m => m.DisplayDate).Take(1).FirstOrDefault() as mSessionDate;
            //    model.SessionDateTo = sessionDate.DisplayDate.ToString();
            //}

            model.GenderList = ExtensionMethods.GetGender();
            model.TimeTypeList = ExtensionMethods.GetTimeTypeList();
            model.Mode = "Update";
            model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);

            return View("_NewVehiclePassRequest", model);
        }

        public object TransferSelectedDepartmentPass(string PassIDs)
        {
            string[] PassIds = PassIDs.Split(',');

            foreach (var PassID in PassIds)
            {
                mDepartmentPasses model = new mDepartmentPasses();
                mDepartmentPasses model1 = new mDepartmentPasses();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.PassID = Convert.ToInt16(PassID);
                model = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", model);
                model.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);
                model.PassCode = null;
                model.ApprovedDate = null;
                model.ApprovedBy = null;
                model.IsRequested = false;
                model.RejectionDate = null;
                model.RejectionDate = DateTime.Now;
                model.IsApproved = false;
                model.Status = 1;
                model.RequestdID = SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions.ExtensionMethods.GenerateValue(model.RecommendationType.Substring(0, 1) + "V", siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                model.SessionDateFrom = SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions.ExtensionMethods.GetSessionDateFrom(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                model.SessionDateTo = SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions.ExtensionMethods.GetSessionDateTo(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    model.IsRequestUserID = CurrentSession.UserID;
                }

                //model.SessionDateFrom = "05/12/2014";
                //model.SessionDateTo = "12/12/2014";
                ///
                /// Insert New Record To Next Section
                ///
                Helper.ExecuteService("Passes", "AddPassRequest", model);

                ///
                /// Update Is TransferID Coressponding PassID
                ///
                model1 = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", model);
                model1.PassID = Convert.ToInt16(PassID);
                model1.IsSessionTransferID = true;

                Helper.ExecuteService("Passes", "UpdatePassRequest", model1);
               // Notification.SendEmailDetails(model);
            }

            return null;
        }
        public string GetSession()
        {
            string value = "";
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            int SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);
            value = Convert.ToString(SessionCode);
            return value;
        }
    }
}
