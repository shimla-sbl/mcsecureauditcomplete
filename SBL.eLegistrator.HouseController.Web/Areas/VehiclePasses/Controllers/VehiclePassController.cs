﻿namespace SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Controllers
{
    #region Namespace Reffrences
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Models;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using System;
    using System.Collections.Generic;
    using System.Web.Mvc;
    using SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Extensions;
    using SBL.eLegistrator.HouseController.Web.Utility;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.Employee;
    using System.Linq;
    using SBL.DomainModel.Models.PaperLaid;
    using System.Web;
    using SBL.eLegistrator.HouseController.Filters;
    using SBL.eLegistrator.HouseController.Web.Filters;
    using SBL.DomainModel.Models.Adhaar;
    #endregion

    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class VehiclePassController : Controller
    {
        //
        // GET: /VehiclePasses/VehiclePass/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult PartialVehiclePassList(int Status = 0, int PassCategoryID = 0, string DepartmentCatID = "")  
        {
            VehicleViewModel model = new VehicleViewModel();
            List<mDepartmentPasses> ListdeprtmentPasses = new List<mDepartmentPasses>();
            mDepartmentPasses model1 = new mDepartmentPasses();
            if (Utility.CurrentSession.DeptID != null && Utility.CurrentSession.DeptID != "")
            {
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                model1.IsCurrentAssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
                model1.IsCurrentSessionID = Convert.ToInt16(siteSettingMod.SessionCode);

                if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    model1.AssemblyCode = Convert.ToInt16(CurrentSession.AssemblyId);
                }

                if (!string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    model1.SessionCode = Convert.ToInt16(CurrentSession.SessionId);
                }

                //New changes according  employee authorizations
                /*Start*/
                tPaperLaidV modelPaperLaid = new tPaperLaidV();
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    modelPaperLaid.UserID = new Guid(CurrentSession.UserID);
                    model1.IsRequestUserID = CurrentSession.UserID;
                }

                List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", modelPaperLaid);

                if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                {
                    foreach (var item in AuthorisedEmp)
                    {
                        model1.DepartmentID = item.AssociatedDepts;

                    }
                    if (model1.DepartmentID == null)
                    {
                        model1.DepartmentID = CurrentSession.DeptID;
                    }
                }
                else
                {
                    model1.DepartmentID = CurrentSession.DeptID;
                }

                //model1.DepartmentID = model.DepartmentID;
                model.mDepartmentList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Passes", "GetDepartmentByids", model1);
                // var DepartmentCategoryList = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Passes", "GetDepartmentCategorys", null);
                model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);
                ListdeprtmentPasses = (List<mDepartmentPasses>)Helper.ExecuteService("Passes", "GetDepartmentVehiclePassesByDeptId", model1);
                // model.mDepartmentList = DepartmentCategoryList;
                var ListViewModel = ListdeprtmentPasses.ToModelList();
                /*End*/
                if (DepartmentCatID != "")
                {
                    ListViewModel = ListViewModel.Where(m => m.DepartmentID == DepartmentCatID).ToList();

                }

                if (PassCategoryID != 0)
                {
                    ListViewModel = ListViewModel.Where(m => m.PassCategoryID == PassCategoryID).ToList();

                }

                if (Status != 0)
                {
                    if (Status == 10)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 1 && m.IsRequested == false).ToList();
                    }
                    else if (Status == 1)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 1 && m.IsRequested == true && m.IsApproved == false).ToList();
                    }
                    else if (Status == 2)
                    {
                        ListViewModel = ListViewModel.Where(m => m.Status == 2 && m.IsRequested == true).ToList();
                    }

                }

                //model1.DepartmentID = Utility.CurrentSession.DeptID;
                model.DepartmentID = DepartmentCatID;
                model.PassCategoryID = PassCategoryID;
                model.VehiclePassList = ListViewModel;
                model.SessionCode = model1.SessionCode;
                model.ValidateSessionID = siteSettingMod.SessionCode;
                return PartialView("_VehiclePassesGridPending", model);
            }
            return PartialView("_VehiclePassesGridPending", model);
        }

        [HttpGet]
        public ActionResult VehiclePassRequest()
        {

            VehicleViewModel model = new VehicleViewModel();
            tPaperLaidV objPaperLaid = new tPaperLaidV();
            model.GenderList = ExtensionMethods.GetGender();
            model.TimeTypeList = ExtensionMethods.GetTimeTypeList();

            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            session.AssemblyID = model.AssemblyCode = Convert.ToInt16(siteSettingMod.AssemblyCode);
            session.SessionCode = model.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);

            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesList", new PublicPass { SessionCode = model.SessionCode, AssemblyCode = model.AssemblyCode }) as List<mSessionDate>;
            model.mSessionDateList = sessionDates;
            if (sessionDates.Count > 0)
            {
                if (string.IsNullOrEmpty(model.SessionDateTo))
                {
                    var sessionDate = sessionDates.OrderByDescending(m => m.DisplayDate).Take(1).FirstOrDefault() as mSessionDate;
                    model.SessionDateTo = sessionDate.DisplayDate.ToString();
                }
            }
            model.Mode = "Add";
            string DepartmentId = Utility.CurrentSession.DeptID;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                objPaperLaid.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", objPaperLaid);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp) { model.DepartmentID = item.AssociatedDepts; }
            }
            else
            { model.DepartmentID = CurrentSession.DeptID; }
            /*End*/
            AdhaarDetails mUserDetails = new AdhaarDetails();
            string AadharID = CurrentSession.AadharId;
            if (AadharID != null)
            {
                mUserDetails.AdhaarID = AadharID;
                mUserDetails = (AdhaarDetails)Helper.ExecuteService("User", "GetUser_DetailsByAdhaarID", mUserDetails);
                if (mUserDetails != null)
                {
                    model.RecommendationBy = mUserDetails.Name;
                }
            }
           // model.RecommendationBy = Utility.CurrentSession.Name;
            mDepartmentPasses department = new mDepartmentPasses();
            department.DepartmentID = model.DepartmentID;
            model.mDepartment = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Passes", "GetDepartmentByids", department);

            model.RecommendationType = "Department";
            model.AssemblyCode = session.AssemblyID;
            model.SessionCode = session.SessionCode;

            model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategoriesForVehicles", null);

            return PartialView("_NewVehiclePassRequest", model);
        }

        [HttpPost]
        public ActionResult VehiclePassRequest(VehicleViewModel model, HttpPostedFileBase file)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SBL.DomainModel.Models.Department.mDepartment department = new DomainModel.Models.Department.mDepartment();
                    department.deptId = model.DepartmentID;
                    department = (SBL.DomainModel.Models.Department.mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", department);
                    model.OrganizationName = department.deptname.Trim();
                    model.IsActive = true;

                    var EntityModel = model.ToEntity();
                    EntityModel.IsVehiclaPass = true;
                    if (model.Mode == "Add")
                    {
                        if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                        {
                            EntityModel.IsRequestUserID = CurrentSession.UserID;
                        }
                        Helper.ExecuteService("Passes", "AddPassRequest", EntityModel);
                        TempData["Msg"] = "Save";
                        TempData["Dept"] = "Dept";
                        TempData["ReqMesg"] = EntityModel.RequestdID;
                       // Notification.SendEmailDetails(EntityModel);
                    }
                    else
                    {
                        Helper.ExecuteService("Passes", "UpdatePassRequest", EntityModel);
                        TempData["Msg"] = "Update";
                    }
                }
                string Value = "";
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                if (TempData["Msg"] == "Save")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                {
                    Value = "Save" + "," + TempData["ReqMesg"];
                    return Json(Value, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Value = "Update";
                    return Json(Value, JsonRequestBehavior.AllowGet);

                }
               // return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
            }
            catch (Exception)
            {
                return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                throw;
            }
        }

        public ActionResult FullPassDetails(int Id)
        {
            var PassRequest = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", new mDepartmentPasses { PassID = Id });

            var model = PassRequest.ToModel();

            return View(model);
        }

        public object SubmitSelectedDepartmentPass(string PassIDs)
        {
            string[] PassIds = PassIDs.Split(',');

            foreach (var PassID in PassIds)
            {
                mDepartmentPasses model = new mDepartmentPasses();
                model.PassID = Convert.ToInt32(PassID);
                Helper.ExecuteService("Passes", "SubmitDepartmentPassRequest", model);
            }

            return null;
        }

        public object SubmitDepartmentPass(string PassID)
        {
            mDepartmentPasses model = new mDepartmentPasses();
            model.PassID = Convert.ToInt16(PassID);
            Helper.ExecuteService("Passes", "SubmitDepartmentPassRequest", model);
            return null;
        }

        public ActionResult EditPassRequest(int Id)
        {
            var PassRequest = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", new mDepartmentPasses { PassID = Id });

            var model = PassRequest.ToModel();

            tPaperLaidV objPaperLaid = new tPaperLaidV();
            string DepartmentId = Utility.CurrentSession.DeptID;

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            { objPaperLaid.UserID = new Guid(CurrentSession.UserID); }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", objPaperLaid);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                { objPaperLaid.DepartmentId = item.AssociatedDepts; }
            }
            /*End*/

            mDepartmentPasses department = new mDepartmentPasses();
            if (objPaperLaid.DepartmentId != null)
            { department.DepartmentID = objPaperLaid.DepartmentId; }
            else
            { department.DepartmentID = model.DepartmentID; }
            model.mDepartment = (List<SBL.DomainModel.Models.Department.mDepartment>)Helper.ExecuteService("Passes", "GetDepartmentByids", department);

            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            session.AssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
            session.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);
            List<mSessionDate> ListSessionDates = new List<mSessionDate>();


            var sessionDates = Helper.ExecuteService("PublicPass", "GetSessionDatesList", new PublicPass { SessionCode = session.SessionCode, AssemblyCode = session.AssemblyID }) as List<mSessionDate>;
            model.mSessionDateList = sessionDates;
            if (string.IsNullOrEmpty(model.SessionDateTo))
            {
                var sessionDate = sessionDates.OrderByDescending(m => m.DisplayDate).Take(1).FirstOrDefault() as mSessionDate;
                model.SessionDateTo = sessionDate.DisplayDate.ToString();
            }


            //var sessionDates = Helper.ExecuteService("Session", "GetSessionDateBySessionCode", session) as List<mSessionDate>;
            //model.mSessionDateList = sessionDates;
            //if (string.IsNullOrEmpty(model.SessionDateTo))
            //{
            //    var sessionDate = sessionDates.OrderByDescending(m => m.DisplayDate).Take(1).FirstOrDefault() as mSessionDate;
            //    model.SessionDateTo = sessionDate.DisplayDate.ToString();
            //}

            model.GenderList = ExtensionMethods.GetGender();
            model.TimeTypeList = ExtensionMethods.GetTimeTypeList();
            model.Mode = "Update";
            model.ListPassCategory = (List<PassCategory>)Helper.ExecuteService("Passes", "GetPassCategories", null);

            return View("_NewVehiclePassRequest", model);
        }

        public object TransferSelectedDepartmentPass(string PassIDs)
        {
            string[] PassIds = PassIDs.Split(',');

            foreach (var PassID in PassIds)
            {
                mDepartmentPasses model = new mDepartmentPasses();
                mDepartmentPasses model1 = new mDepartmentPasses();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                model.PassID = Convert.ToInt16(PassID);
                model = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", model);
                model.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);
                model.PassCode = null;
                model.ApprovedDate = null;
                model.ApprovedBy = null;
                model.IsRequested = false;
                model.RejectionDate = null;
                model.RejectionDate = DateTime.Now;
                model.IsApproved = false;
                model.Status = 1;
                model.RequestdID = SBL.eLegistrator.HouseController.Web.Areas.PaperLaidDepartment.Extensions.ExtensionMethods.GenerateValue(model.RecommendationType.Substring(0, 1) + "V", siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                model.SessionDateFrom = SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions.ExtensionMethods.GetSessionDateFrom(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                model.SessionDateTo = SBL.eLegistrator.HouseController.Web.Areas.AdministrationBranch.Extensions.ExtensionMethods.GetSessionDateTo(siteSettingMod.AssemblyCode, siteSettingMod.SessionCode);
                if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                {
                    model.IsRequestUserID = CurrentSession.UserID;
                }

                //model.SessionDateFrom = "05/12/2014";
                //model.SessionDateTo = "12/12/2014";
                ///
                /// Insert New Record To Next Section
                ///
                Helper.ExecuteService("Passes", "AddPassRequest", model);
               
                ///
                /// Update Is TransferID Coressponding PassID
                ///
                model1 = (mDepartmentPasses)Helper.ExecuteService("Passes", "GetDepartmentPassesById", model);
                model1.PassID = Convert.ToInt16(PassID);
                model1.IsSessionTransferID = true;

                Helper.ExecuteService("Passes", "UpdatePassRequest", model1);
               // Notification.SendEmailDetails(model);
            }

            return null;
        }
        public string GetSession()
        {
            string value = "";
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            int SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);
            value = Convert.ToString(SessionCode);
            return value;
        }
    }
}
