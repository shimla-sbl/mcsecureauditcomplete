﻿namespace SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Extensions
{
    #region Namespace Reffrences
    using SBL.DomainModel.Models.Passes;
    using SBL.eLegistrator.HouseController.Web.Areas.VehiclePasses.Models;
    using SBL.eLegistrator.HouseController.Web.Helpers;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    #endregion

    public static class ExtensionMethods
    {
        public static SelectList GetGender()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Male", Value = "Male" });
            result.Add(new SelectListItem() { Text = "Female", Value = "Female" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetTimeTypeList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Full Day", Value = "Full Day" });
            result.Add(new SelectListItem() { Text = "AN", Value = "AN" });
            result.Add(new SelectListItem() { Text = "FN", Value = "FN" });
            result.Add(new SelectListItem() { Text = "After Session", Value = "After Session" });
            result.Add(new SelectListItem() { Text = "Before Session", Value = "Before Session" });

            return new SelectList(result, "Value", "Text");
        }

        public static mDepartmentPasses ToEntity(this VehicleViewModel Model)
        {
            mDepartmentPasses entity = new mDepartmentPasses()
            {
                PassID = Model.PassID,
                PassCode = Model.PassCode,
                PassCategoryID = Model.PassCategoryID,
                AssemblyCode = Model.AssemblyCode,
                SessionCode = Model.SessionCode,
                AadharID = Model.AadharID,
                Prefix = Model.Prefix,
                Name = Model.Name,
                Gender = Model.Gender,
                Age = Model.Age,
                FatherName = Model.FatherName,
                NoOfPersions = Model.NoOfPersions,
                RecommendationType = Model.RecommendationType,
                RecommendationBy = Model.RecommendationBy,
                RecommendationDescription = Model.RecommendationDescription,
                MobileNo = Model.MobileNo,
                Email = Model.Email,
                Address = Model.Address,
                Photo = Model.Photo,
                OrganizationName = Model.OrganizationName,
                Designation = Model.Designation,
                DayTime = Model.DayTime,
                Time = Model.Time,
                DepartmentID = Model.DepartmentID,
                SessionDateFrom = Model.SessionDateFrom,
                SessionDateTo = Model.SessionDateTo,
                ApprovedDate = Model.ApprovedDate,
                ApprovedBy = Model.ApprovedBy,
                VehicleNumber = Model.VehicleNumber,
                GateNumber = Model.GateNumber,
                IsRequested = Model.IsRequested,
                RequestedDate = Model.RequestedDate,
                Status = Model.Status,
                RequestedBy = Model.RequestedBy,
                IsActive = Model.IsActive,
                IsRequestUserID = Model.IsRequestUserID,
                RequestdID = GenerateValue(Model.RecommendationType.Substring(0, 1) + "V", Model.AssemblyCode, Model.SessionCode)

            };
            return entity;
        }

        public static VehicleViewModel ToModel(this mDepartmentPasses entity)
        {
            var Model = new VehicleViewModel()
            {
                PassID = entity.PassID,
                PassCode = entity.PassCode,
                PassCategoryID = entity.PassCategoryID,
                AssemblyCode = entity.AssemblyCode,
                SessionCode = entity.SessionCode,
                AadharID = entity.AadharID,
                Prefix = entity.Prefix,
                Name = entity.Name,
                Gender = entity.Gender,
                Age = entity.Age,
                FatherName = entity.FatherName,
                NoOfPersions = entity.NoOfPersions,
                RecommendationType = entity.RecommendationType,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                MobileNo = entity.MobileNo,
                Email = entity.Email,
                Address = entity.Address,
                Photo = entity.Photo,
                OrganizationName = entity.OrganizationName,
                Designation = entity.Designation,
                DayTime = entity.DayTime,
                Time = entity.Time,
                DepartmentID = entity.DepartmentID,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                ApprovedDate = entity.ApprovedDate,
                ApprovedBy = entity.ApprovedBy,
                VehicleNumber = entity.VehicleNumber,
                GateNumber = entity.GateNumber,
                IsRequested = entity.IsRequested,
                RequestedDate = entity.RequestedDate,
                Status = entity.Status,
                RequestedBy = entity.RequestedBy,
                IsActive = entity.IsActive,
                IsApproved = entity.IsApproved,
                IsRequestUserID = entity.IsRequestUserID,
                PassCategoryName = GetPassCategoryNameByID(entity.PassCategoryID)
            };
            return Model;
        }

        public static List<VehicleViewModel> ToModelList(this List<mDepartmentPasses> entityList)
        {
            var ModelList = entityList.Select(entity => new VehicleViewModel()
            {
                PassID = entity.PassID,
                PassCode = entity.PassCode,
                PassCategoryID = entity.PassCategoryID,
                AssemblyCode = entity.AssemblyCode,
                SessionCode = entity.SessionCode,
                AadharID = entity.AadharID,
                Prefix = entity.Prefix,
                Name = entity.Name,
                Gender = entity.Gender,
                Age = entity.Age,
                FatherName = entity.FatherName,
                NoOfPersions = entity.NoOfPersions,
                RecommendationType = entity.RecommendationType,
                RecommendationBy = entity.RecommendationBy,
                RecommendationDescription = entity.RecommendationDescription,
                MobileNo = entity.MobileNo,
                Email = entity.Email,
                Address = entity.Address,
                Photo = entity.Photo,
                OrganizationName = entity.OrganizationName,
                Designation = entity.Designation,
                DayTime = entity.DayTime,
                Time = entity.Time,
                DepartmentID = entity.DepartmentID,
                SessionDateFrom = entity.SessionDateFrom,
                SessionDateTo = entity.SessionDateTo,
                ApprovedDate = entity.ApprovedDate,
                ApprovedBy = entity.ApprovedBy,
                VehicleNumber = entity.VehicleNumber,
                GateNumber = entity.GateNumber,
                IsRequested = entity.IsRequested,
                RequestedDate = entity.RequestedDate,
                Status = entity.Status,
                RequestedBy = entity.RequestedBy,
                IsActive = entity.IsActive,
                IsApproved = entity.IsApproved,
                RequestdID = entity.RequestdID

            });
            return ModelList.ToList();
        }

        public static string GetPassCategoryNameByID(int passCategoryID)
        {
            var passCategoryModel = (PassCategory)Helper.ExecuteService("AdministrationBranch", "GetPassCategoryById", new PassCategory { PassCategoryID = passCategoryID });

            if (passCategoryModel != null)
            {
                return passCategoryModel.Name;
            }
            return "";
        }

        public static string GenerateValue(string Prefix, int AssemblyCode, int SessionCode)
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 6)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            var ResultVal = string.Format("{0}{1}{2}{3}", Prefix, AssemblyCode, SessionCode, result);
            return ResultVal;
        }
    }
}