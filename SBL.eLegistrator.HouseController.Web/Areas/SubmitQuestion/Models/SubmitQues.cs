﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.Web.Core.Areas.SubmitQuestion.Models
{
    public class SubmitQues
    {
        public string add { get; set; }
        public List<SubmitQues> AllQuestions { get; set; }
    }
}