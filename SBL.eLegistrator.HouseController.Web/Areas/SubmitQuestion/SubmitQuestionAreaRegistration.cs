﻿using System.Web.Mvc;

namespace SBL.Web.Core.Areas.SubmitQuestion
{
    public class SubmitQuestionAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "SubmitQuestion";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "SubmitQuestion_default",
                "SubmitQuestion/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
