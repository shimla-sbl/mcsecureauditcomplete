﻿
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.Web.Core.Areas.SubmitQuestion.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Bill;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Filters;

namespace SBL.Web.Core.Areas.SubmitQuestion.Controllers
{
    [SBLAuthorize(Allow = "Authenticated")]
    [Audit]
    [NoCache]
    public class SubmitQuestionController : Controller
    {

        public ActionResult Index(string Msg)
        {
            tQuestionModel model = new tQuestionModel();
            model = Helper.ExecuteService("Questions", "GetQuestionSearchData", model) as tQuestionModel;

            List<SiteSettings> ListSiteSettingsVS = new List<SiteSettings>();
            SiteSettings SiteSettingsAssem = new SiteSettings();
            SiteSettings SiteSettingsSessn = new SiteSettings();
            SiteSettings SiteSettingsSessnCal = new SiteSettings();

            SiteSettings model1 = new SiteSettings();

            model1 = Helper.ExecuteService("SiteSetting", "GetQASettings", SiteSettingsAssem) as SiteSettings;
            model.SessionId = model1.SessionCode;
            model.AssemblyId = model1.AssemblyCode;
            //model.SessionCalendarId = model1.Count;

            tBillRegister obj = new tBillRegister();
            mSession mSession = new mSession();
            if (model.AssemblyId != 0)
            {
                mSession.AssemblyID = Convert.ToInt16(model.AssemblyId);
            }

            obj = Helper.ExecuteService("Session", "GetAssemblySession", mSession) as tBillRegister;
            model.mSession = obj.mAssemblySession;   

            if (Msg != null && Msg != "")
            {
                model.Message = Msg;
            }
            return View(model);
        }

        public ActionResult PartialCreateQuestionEntry()
        {

            tQuestionModel model = new tQuestionModel();
          
            model = Helper.ExecuteService("Questions", "GetQuestionSearchData", model) as tQuestionModel;

            List<SiteSettings> ListSiteSettingsVS = new List<SiteSettings>();
            SiteSettings model1 = new SiteSettings();

            model1 = Helper.ExecuteService("SiteSetting", "GetQASettings", model1) as SiteSettings;
            model.SessionId = model1.SessionCode;
            model.AssemblyId = model1.AssemblyCode;

            //model.SessionCalendarId = model1.Count;

            tBillRegister objSetting = new tBillRegister();

            mSession mSession = new mSession();
            if (model.AssemblyId != 0)
            {
                mSession.AssemblyID = Convert.ToInt16(model.AssemblyId);
            }

            model.mMemberModel = Helper.ExecuteService("Member", "GetMemberByAssemblyCode", model.AssemblyId) as List<mMember>;
            mMember obj = new mMember();
            obj.MemberID = 0;
            obj.Name = "Select Member";
            model.mMemberModel.Add(obj);

            mAssembly model4 = new mAssembly();
            model4.AssemblyID = model.AssemblyId;
            model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinisterMinistry", null) as List<mMinisteryMinisterModel>;
            mMinisteryMinisterModel obj2 = new mMinisteryMinisterModel();
            obj2.MinistryID = 0;
            obj2.MinisterMinistryName = "Select Minister";
            model.ministerList.Add(obj2);
        
            mSessionDate obj4 = new mSessionDate();
            obj4.SessionId = model.SessionId;
            model.SessionDateList = Helper.ExecuteService("Session", "GetSessionDate", obj4) as List<mSessionDate>;
            obj4.Id = 0;
            obj4.DisplayDate = "Select Date";
            model.SessionDateList.Add(obj4);
            model.BtnCaption = "SaveQuestion";

            List<tMemberNotice> ListtMemberNotice = new List<tMemberNotice>();
            tMemberNotice noticeModel = new tMemberNotice();
            model.MinisterId = 0;
            noticeModel.DepartmentId = Convert.ToString(0);
            noticeModel.DepartmentName = "Select Department";
            ListtMemberNotice.Add(noticeModel);
            model.DepartmentList = ListtMemberNotice;

            return PartialView("_CreateQuestionEntry", model);
        }

        public ActionResult PartialPendingList(string QType, string Date, string QuestionNumber, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            string Month = "";
            string Day = "";
            string Year = "";
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            tQuestionModel obj = new tQuestionModel();
            tQuestion obj1 = new tQuestion();
            if (PageNumber != "" && RowsPerPage != "")
            {
                obj.PAGE_SIZE = int.Parse(RowsPerPage);
                obj.PageIndex = int.Parse(PageNumber);
               
            }
            else
            {
                obj.PAGE_SIZE = 10;
                obj.PageIndex = 1;
            }
            if (Date != "" && Date != null && Date != "Select Date")
            {
                obj.IsFixedDate = ConvertUIDate(Date);
            }

            if (QType != "" && QType != null && QType != "0")
            {
                obj.QuestionType = QType;
            }
            if (QuestionNumber != "" && QuestionNumber != null)
            {
                obj.QuestionNumber = Convert.ToInt32(QuestionNumber);
            }
            obj.objQuestList = Helper.ExecuteService("Questions", "GetPendingQuestion", obj) as List<tQuestionModel>;
            string st = Helper.ExecuteService("Questions", "GetPendingQuestionCount", obj) as string;
            obj.ResultCount = Convert.ToInt32(st);


            foreach (var item in obj.objQuestList)
            {
                obj.TableName = item.TableName;
                obj.View = item.View;
                string SDate = Convert.ToString(item.QuestionFixDate);

                SDate = Convert.ToString(item.QuestionFixDate);
                if (SDate != "")
                {
                    if (SDate.IndexOf("/") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("/"));
                        if (Month.Length < 2)
                        {
                            Month = "0" + Month;
                        }
                        Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        item.StringReplyDate = SDate;
                    }
                    if (SDate.IndexOf("-") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("-"));
                        Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        item.StringReplyDate = SDate;
                    }
                }
            }
            if (PageNumber != null && PageNumber != "")
            {
                obj.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                obj.PageNumber = Convert.ToInt32("1");
            }

            if (RowsPerPage != null && RowsPerPage != "")
            {
                obj.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                obj.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                obj.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                obj.selectedPage = Convert.ToInt32("1");
            }

            if (loopStart != null && loopStart != "")
            {
                obj.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                obj.loopStart = Convert.ToInt32("1");
            }

            if (loopEnd != null && loopEnd != "")
            {
                obj.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                obj.loopEnd = Convert.ToInt32("5");
            }
            obj.listAction = "MemberEntrysubmit";
            obj = Helper.ExecuteService("Questions", "GetQuestionSearchData", obj) as tQuestionModel;
            List<SiteSettings> ListSiteSettingsVS = new List<SiteSettings>();
            SiteSettings model1 = new SiteSettings();
            model1 = Helper.ExecuteService("SiteSetting", "GetQASettings", model1) as SiteSettings;
            obj.SessionId = model1.SessionCode;
            mSessionDate obj4 = new mSessionDate();
            obj4.SessionId = obj.SessionId;
            obj.SessionDateList = Helper.ExecuteService("Session", "GetSessionDate", obj4) as List<mSessionDate>;
            obj4.Id = 0;
            obj4.DisplayDate = "Select Date";
            obj.SessionDateList.Add(obj4);
            return PartialView("_PendingListData", obj);
        }
        
        public JsonResult SubmitQues(int QuestionID)
        {
            tQuestionModel obj = new tQuestionModel();
            obj.QuestionID = QuestionID;
            obj = Helper.ExecuteService("Questions", "SubmitById", obj) as tQuestionModel;
          
            return Json(obj.Message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteQues(int QuestionID)
        {
            tQuestionModel obj = new tQuestionModel();
            obj.QuestionID = QuestionID;
            obj = Helper.ExecuteService("Questions", "DeleteQues", obj) as tQuestionModel;         

            return Json(obj.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PartialSubmittedList(string QType, string Date, string QuestionNumber, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)       
        {
            string Month = "";
            string Day = "";
            string Year = "";
            QType = Sanitizer.GetSafeHtmlFragment(QType);
            Date = Sanitizer.GetSafeHtmlFragment(Date);
            QuestionNumber = Sanitizer.GetSafeHtmlFragment(QuestionNumber);
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);
        
            tQuestionModel obj = new tQuestionModel();

            if (PageNumber != "" && RowsPerPage != "")
            {
                obj.PAGE_SIZE = int.Parse(RowsPerPage);
                obj.PageIndex = int.Parse(PageNumber);
              
            }
            else
            {
                obj.PAGE_SIZE = 10;
                obj.PageIndex = 1;
            }
            if (Date != "" && Date != null  && Date != "Select Date"  )
            {
               obj.IsFixedDate = ConvertUIDate(Date);
            }

            if (QType != "" && QType != null && QType != "0")
            {
                obj.QuestionType = QType;
            }
            if (QuestionNumber != "" && QuestionNumber != null)
            {
                obj.QuestionNumber = Convert.ToInt32(QuestionNumber);
            }
            obj.objQuestList = Helper.ExecuteService("Questions", "GetSubmitQuestion", obj) as List<tQuestionModel>;
            string st = Helper.ExecuteService("Questions", "GetSubmitQuestionCount", obj) as string;
            obj.ResultCount = Convert.ToInt32(st);
           
            foreach (var item in obj.objQuestList)
            {
                obj.TableName = item.TableName;
                obj.View = item.View;
                string SDate = Convert.ToString(item.QuestionFixDate);

                SDate = Convert.ToString(item.QuestionFixDate);
                if (SDate != "")
                {
                    if (SDate.IndexOf("/") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("/"));
                        if (Month.Length < 2)
                        {
                            Month = "0" + Month;
                        }
                        Day = SDate.Substring(SDate.IndexOf("/") + 1, (SDate.LastIndexOf("/") - SDate.IndexOf("/") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("/") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        item.StringReplyDate = SDate;
                    }
                    if (SDate.IndexOf("-") > 0)
                    {
                        Month = SDate.Substring(0, SDate.IndexOf("-"));
                        Day = SDate.Substring(SDate.IndexOf("-") + 1, (SDate.LastIndexOf("-") - SDate.IndexOf("-") - 1));
                        Year = SDate.Substring(SDate.LastIndexOf("-") + 1, 4);
                        SDate = Day + "/" + Month + "/" + Year;
                        item.StringReplyDate = SDate;
                    }
                }
            }
           
            if (PageNumber != null && PageNumber != "")
            {
                obj.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                obj.PageNumber = Convert.ToInt32("1");
            }

            if (RowsPerPage != null && RowsPerPage != "")
            {
                obj.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                obj.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                obj.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                obj.selectedPage = Convert.ToInt32("1");
            }

            if (loopStart != null && loopStart != "")
            {
                obj.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                obj.loopStart = Convert.ToInt32("1");
            }

            if (loopEnd != null && loopEnd != "")
            {
                obj.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                obj.loopEnd = Convert.ToInt32("5");
            }
           
            obj = Helper.ExecuteService("Questions", "GetQuestionSearchData", obj) as tQuestionModel;
            List<SiteSettings> ListSiteSettingsVS = new List<SiteSettings>();
            SiteSettings model1 = new SiteSettings();
            model1 = Helper.ExecuteService("SiteSetting", "GetQASettings", model1) as SiteSettings;
            obj.SessionId = model1.SessionCode;
            mSessionDate obj4 = new mSessionDate();
            obj4.SessionId = obj.SessionId;
            obj.SessionDateList = Helper.ExecuteService("Session", "GetSessionDate", obj4) as List<mSessionDate>;
            obj4.Id = 0;
            obj4.DisplayDate = "Select Date";
            obj.SessionDateList.Add(obj4);
            return PartialView("_SubmittedListData", obj);
        }
       
        static List<SubmitQues> ExitingData = new List<SubmitQues>();

        private List<SubmitQues> AddQuestions(string Qname)
        {
            SubmitQues data = new SubmitQues();
            if (Convert.ToInt16(ExitingData.Count) == 0)
            {
                data.add = Qname;
                ExitingData.Add(data);
            }            
            data.AllQuestions = ExitingData;
            return ExitingData;

        }
      
        [HttpGet]
        public JsonResult GetDepartmentByMinistery(int MinistryId)
        {
            tMemberNotice noticeModel = new tMemberNotice();
            mMinistry model = new mMinistry();
            model.MinistryID = MinistryId;

            List<tMemberNotice> DepartmentList = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model) as List<tMemberNotice>;
            noticeModel.DepartmentId = Convert.ToString(0);
            noticeModel.DepartmentName = "Select Department";
            DepartmentList.Add(noticeModel);
            return Json(DepartmentList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetConstByMemberId(int AssemblyId, int MemberId)
        {
            mMemberAssembly ObjMem = new mMemberAssembly();
            tQuestion obj = new tQuestion();
            ObjMem.MemberID = MemberId;
            ObjMem.AssemblyID = AssemblyId;
            obj.mConstituency = Helper.ExecuteService("Questions", "GetConstByMemberId", ObjMem) as List<mConstituency>;

            return Json(obj.mConstituency, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetQuestionId(int AssemblyId, int Qtype)
        {
            tQuestionModel obj = new tQuestionModel();
            obj.AssemblyId = AssemblyId;
            obj.QtypeId = Qtype;
            string st = Helper.ExecuteService("Questions", "GetQuestionID", obj) as string;
           
            return Json(st, JsonRequestBehavior.AllowGet);

        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveQuestionEntry(tQuestionModel model, string submit)
        {
#pragma warning disable CS0219 // The variable 'Message' is assigned but its value is never used
            string Message = "";
#pragma warning restore CS0219 // The variable 'Message' is assigned but its value is never used

            try
            {
                if (submit == "SaveQuestion")
                {
                    int count = ExitingData.Count;
                    tQuestion objSubQuestionModel = new tQuestion();

                    objSubQuestionModel.AssemblyID = model.AssemblyId;
                    objSubQuestionModel.SessionID = model.SessionId;
                    objSubQuestionModel.QuestionNumber = model.QuestionNumber;
                    objSubQuestionModel.QuestionType = model.QuestionTypeId;
                    objSubQuestionModel.MemberID = model.MemberId;
                    objSubQuestionModel.MinisterID = model.MinisterId;
                    objSubQuestionModel.DepartmentID = model.DepartmentId;

                    objSubQuestionModel.IsFixedDate = ConvertUIDate(model.SessionDate); ;
                    objSubQuestionModel.Subject = model.Subject;

                    objSubQuestionModel.MainQuestion = model.MainQuestion;
                    objSubQuestionModel.ConstituencyName = model.ConstituencyName;
                    objSubQuestionModel.ConstituencyNo = model.ConstituencyNumber;
                    objSubQuestionModel.IsMerge = model.IsMerge;
                    foreach (var item in model.MemberIds)
                    {

                        string ResultString = string.Join(",", model.MemberIds.ToArray());
                        objSubQuestionModel.ReferenceMemberCode = ResultString;
                    }
                    objSubQuestionModel = Helper.ExecuteService("Questions", "InsertNGetQid", objSubQuestionModel) as tQuestion;
                    Message = "Question Added to Pending List";


                }
                if (submit == "UpdateQuestion")
                {
                    tSubQuestion objSubQuestion = new tSubQuestion();

                    tQuestion objSubQuestionModel = new tQuestion();
                    objSubQuestionModel.QuestionID = model.QuestionID;
                    objSubQuestionModel.AssemblyID = model.AssemblyId;
                    objSubQuestionModel.SessionID = model.SessionId;
                    objSubQuestionModel.QuestionNumber = model.QuestionNumber;
                    objSubQuestionModel.QuestionType = model.QuestionTypeId;
                    objSubQuestionModel.MemberID = model.MemberId;
                    objSubQuestionModel.MinisterID = model.MinisterId;
                    objSubQuestionModel.DepartmentID = model.DepartmentId;
                    objSubQuestionModel.IsFixedDate = ConvertUIDate(model.SessionDate);
                    objSubQuestionModel.Subject = model.Subject;
                    objSubQuestionModel.MainQuestion = model.MainQuestion;
                    objSubQuestionModel.ConstituencyName = model.ConstituencyName;
                    objSubQuestionModel.ConstituencyNo = model.ConstituencyNumber;
                    objSubQuestionModel.IsMerge = model.IsMerge;

                    foreach (var item in model.MemberIds)
                    {
                        string ResultString = string.Join(",", model.MemberIds.ToArray());
                        objSubQuestionModel.ReferenceMemberCode = ResultString;
                    }

                    objSubQuestionModel = Helper.ExecuteService("Questions", "UpdateQuestionsbyVS", objSubQuestionModel) as tQuestion;
                    objSubQuestionModel.Message = "Update Sucessfully";
                }
            }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
            catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
            {

            }
            ViewBag.Message = "test";
            return RedirectToAction("Index", "SubmitQuestion");
        }

        public DateTime ConvertUIDate(string dt)
        {
            dt = Sanitizer.GetSafeHtmlFragment(dt);
            string Day = "";
            string Month = "";
            string Year = "";
            DateTime ConvtDate = new DateTime();
            if (dt.IndexOf("-") > 0)
            {
                Day = dt.Substring(0, dt.IndexOf("-"));
                Month = dt.Substring(dt.IndexOf("-") + 1, (dt.LastIndexOf("-") - dt.IndexOf("-") - 1));
                Year = dt.Substring(dt.LastIndexOf("-") + 1, 4);
                ConvtDate = new DateTime(Convert.ToInt16(Year), Convert.ToInt16(Month), Convert.ToInt16(Day));
            }
            else
            {

                Day = dt.Substring(0, dt.IndexOf("/"));
                Month = dt.Substring(dt.IndexOf("/") + 1, (dt.LastIndexOf("/") - dt.IndexOf("/") - 1));
                Year = dt.Substring(dt.LastIndexOf("/") + 1, 4);
                ConvtDate = new DateTime(Convert.ToInt16(Year), Convert.ToInt16(Month), Convert.ToInt16(Day));
            }

            return ConvtDate;
        }
        
        [HttpPost]
        public JsonResult CheckQuestionNo(string QuestionNumber)
        {
            tQuestionModel obj = new tQuestionModel();

            QuestionNumber = Sanitizer.GetSafeHtmlFragment(QuestionNumber);
            obj.QuestionNumber = Convert.ToInt32(QuestionNumber);
            string st = Helper.ExecuteService("Questions", "CheckQuestionNo", obj) as string;
       
            return Json(st, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult EditQues(int QuestionID)
        {

            tQuestionModel model = new tQuestionModel();
            model.QuestionID = QuestionID;
            model = Helper.ExecuteService("Questions", "GetQuesForEditById", model) as tQuestionModel;           
            mSessionDate obj4 = new mSessionDate();           
          
            foreach (var item in model.objQuestList)
            {
                obj4.SessionId = item.SessionId;
                model.SessionDateList = Helper.ExecuteService("Session", "GetSessionDate", obj4) as List<mSessionDate>;
                //model.ConstituencyNo = item.ConstituencyName + "," + "(" + item.ConstituencyNo + ")";      
                model.ConstituencyNo = item.ConstituencyName;      
                model.ConstituencyName = item.ConstituencyName;
                model.ConstituencyNumber = item.ConstituencyNo;
                model.Subject = item.Subject;
                model.MainQuestion = item.MainQuestion;
                model.AssemblyId = item.AssemblyId;
                model.SessionId = item.SessionId;
                model.SessionN = item.SessionN;
                model.MemberId = item.MemberId;
                model.MinisterId = item.MinisterId;
                model.DepartmentId = item.DepartmentId;
                model.SubmittedBy = item.SubmittedBy;
                model.QuestionTypeId = item.QtypeId;
                model.DiaryNumber = item.DiaryNumber;
                model.FileNumber = item.FileNumber;
                model.TOReceipt = item.TOReceipt;
                model.TableName = item.TableName;
                model.QuestionPriority = item.QuestionPriority;
                model.QuestionID = item.QuestionID;
                model.IsHindi = item.IsHindi;
                model.QuestionNumber = item.QuestionNumber;
                model.IsSubmitted = item.IsSubmitted;
                model.BtnCaption = "update";
                model.IsMerge = item.IsMerge;
                string s = item.gggg;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');
                    model.MemberIds = new List<string>();
                    for (int i = 0; i < values.Length; i++)
                    {
                        model.MemberIds.Add(values[i]);
                    }

                }
              
                model.SessionDate = Convert.ToDateTime(item.Date).ToString("dd-MM-yyyy");
                mAssembly model4 = new mAssembly();
                model4.AssemblyID = model.AssemblyId;
                model.ministerList = Helper.ExecuteService("MinistryMinister", "GetMinisterMinistry", null) as List<mMinisteryMinisterModel>;
                mMinisteryMinisterModel obj2 = new mMinisteryMinisterModel();
                obj2.MinistryID = 0;
                obj2.MinisterMinistryName = "Select Minister";
                model.ministerList.Add(obj2);
                tMemberNotice noticeModel = new tMemberNotice();
                mMinistry model7 = new mMinistry();
                model7.MinistryID = item.MinisterId;               
                model.DepartmentList = Helper.ExecuteService("MinistryMinister", "GetDepartmentByMinistery", model7) as List<tMemberNotice>;
                noticeModel.DepartmentId = Convert.ToString(0);
                noticeModel.DepartmentName = "Select Department";
                model.DepartmentList.Add(noticeModel);

            }

            return PartialView("_CreateQuestionEntry", model);


        }

        public JsonResult GetSessionDates(int sessionId)
        {
            tQuestionModel model = new tQuestionModel();
            mSessionDate obj = new mSessionDate();
            obj.SessionId = sessionId;
            List<mSessionDate> SessionDateList = Helper.ExecuteService("Session", "GetSessionDate", obj) as List<mSessionDate>;
            obj.Id = 0;
            obj.DisplayDate = "Select Date";
            SessionDateList.Add(obj);

            return Json(SessionDateList, JsonRequestBehavior.AllowGet);
        }
        }
    }

          

              
