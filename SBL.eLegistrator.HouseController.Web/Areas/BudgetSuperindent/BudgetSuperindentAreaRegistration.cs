﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.BudgetSuperindent
{
    public class BudgetSuperindentAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BudgetSuperindent";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BudgetSuperindent_default",
                "BudgetSuperindent/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
