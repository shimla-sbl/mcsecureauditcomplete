﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.CommitteePaperLiadChairperson
{
    public class CommitteePaperLiadChairpersonAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CommitteePaperLiadChairperson";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CommitteePaperLiadChairperson_default",
                "CommitteePaperLiadChairperson/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
