﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Ministery;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Helpers;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Utility;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.PaperLaid;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.DomainModel.Models.Employee;
using SBL.eLegistrator.HouseController.Web.Areas.PaperLaidMinister.Models;
using SBL.eLegistrator.HouseController.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.CommitteePaperLiadChairperson.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class CommitteePaperLiadChairpersonController : Controller
    {
        //
        // GET: /CommitteePaperLiadChairperson/CommitteePaperLiadChairperson/

        #region "CommitteePaper By Uday"

        public ActionResult GetAllList()
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tCommitteeModel Obj = new tCommitteeModel();
                //Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetPendings", Obj);
                return PartialView("_NewGetAllList", Obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewGetAllList(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;
            //this is add for require in new Design

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetAllChairList", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,
            };

            var resultToSort = Obj.ComModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult GetPendings()
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tCommitteeModel Obj = new tCommitteeModel();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                Obj.SessionId = siteSettingMod.SessionCode;
                Obj.AssemblyId = siteSettingMod.AssemblyCode;
                //Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetPendings", Obj);
                return PartialView("_NewGetPendingList", Obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewGetPendings(JqGridRequest request)
        {
            if (CurrentSession.UserID == null) { return RedirectToAction("Login", "Account"); }

            tPaperLaidV model = new tPaperLaidV();

            //Get Current Session and Assembly Information
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
            model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;

            tCommitteeSendModel Obj = new tCommitteeSendModel();

            //New changes according  employee authorizations
            /*Start*/
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
            AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

            if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
            {
                foreach (var item in AuthorisedEmp)
                {
                    Obj.DepartmentId = item.AssociatedDepts;
                }
            }
            else
            {
                Obj.DepartmentId = CurrentSession.DeptID;
            }

            Obj.AssemblyId = model.AssemblyCode;
            Obj.SessionId = model.SessionCode;
            Obj.paperLaidId = model.PaperLaidId;
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;
            //this is add for require in new Design
            Obj.ActionType = "Pending";




            Obj = (tCommitteeSendModel)Helper.ExecuteService("Committee", "SendingPendingForSubCommitteeChairByType", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,

            };


            var resultToSort = Obj.SendComModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<tCommitteeSendModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeSendModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeSendModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult GetSubmitted()
        {
            tCommitteeModel Obj = new tCommitteeModel();
            return PartialView("_NewGetSubmittedList", Obj);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewGetSubmitted(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;

            //this is add for require in new Design
            Obj.ActionType = "Submitted";

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "SearchChairByCategory", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,
            };

            var resultToSort = Obj.ComModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult ViewDetails(int Id)
        {
            PaperMovementContainerModel Obj = new PaperMovementContainerModel();
            Obj.paperLaidId = Id;
            Obj = (PaperMovementContainerModel)Helper.ExecuteService("Committee", "CommitteeChairPendingViewDetails", Obj);
            return PartialView("_DraftViewDetails", Obj);
        }

        public ActionResult SubmittedCommitteeChairViewDetails(int Id)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.paperLaidId = Id;
            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "SubmittedCommitteeChairViewDetails", Obj);
            return PartialView("_ViewDetails", Obj);
        }

        public ActionResult SearchByCategory(int CategId, string Tabtype, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.EventId = CategId;
            Obj.ActionType = Tabtype;
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            Obj.loopStart = Convert.ToInt16(loopStart);
            Obj.loopEnd = Convert.ToInt16(loopEnd);
            Obj.PageSize = Convert.ToInt16(RowsPerPage);
            Obj.PageIndex = Convert.ToInt16(PageNumber);

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "SearchChairByCategory", Obj);
            if (PageNumber != null && PageNumber != "")
            {
                Obj.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                Obj.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                Obj.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                Obj.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                Obj.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                Obj.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                Obj.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                Obj.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                Obj.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                Obj.loopEnd = Convert.ToInt32("5");
            }
            if (Tabtype == "Pending")
            {
                return PartialView("_GetPendingSearchList", Obj);
            }
            else
            {
                return PartialView("_GetSubmittedListSearch", Obj);
            }
        }
        //if (MemberId == CurrentSession.UserID || cm.IsChairMan == true)
        //        {

        public ActionResult CommitteeDashboard()
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tCommitteeMember cm = new tCommitteeMember();

                var MemberId = CurrentSession.UserID;
                tCommitteeModel Obj = new tCommitteeModel();


                    tCommitteeSendModel obj1 = new tCommitteeSendModel();

                    List<mSiteSettingsVS> ListSiteSettingsVS = new List<mSiteSettingsVS>();
                    mSiteSettingsVS SiteSettingsAssem = new mSiteSettingsVS();
                    mSiteSettingsVS SiteSettingsSessn = new mSiteSettingsVS();

                    Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetInitialChairCount", Obj);


                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                    Obj.SessionCode = Obj.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
                    Obj.AssemblyCode = Obj.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

                    mSession Mdl = new mSession();
                    Mdl.SessionCode = Obj.SessionId;
                    Mdl.AssemblyID = Obj.AssemblyCode;
                    mAssembly assmblyMdl = new mAssembly();
                    assmblyMdl.AssemblyID = Obj.AssemblyCode;

                    if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
                    {
                        Obj.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                        Obj.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

                    }
                    else
                    {
                        Obj.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameLocalBySessionCode", Mdl);
                        Obj.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameLocalByAssemblyCode", assmblyMdl);
                    }

                    if (Convert.ToString(TempData["lSM"]) != "")
                    {
                        Obj.lSM = Convert.ToString(TempData["lSM"]);

                    }
                    else
                    {
                        Obj.lSM = "";
                    }


                    //ListSiteSettingsVS = Helper.ExecuteService("User", "GetSettings", SiteSettingsAssem) as List<mSiteSettingsVS>;


                    //SiteSettingsAssem = ListSiteSettingsVS.First(x => x.SettingName == "Assembly");
                    //if (SiteSettingsAssem != null)
                    //{
                    //    Obj.AssemblyId = Convert.ToInt16(SiteSettingsAssem.SettingValue);
                    //    obj1.AssemblyId = Convert.ToInt16(SiteSettingsAssem.SettingValue);

                    //    SiteSettingsSessn = ListSiteSettingsVS.First(x => x.SettingName == "Session");
                    //    if (SiteSettingsSessn != null)
                    //    {
                    //        Obj.SessionId = Convert.ToInt16(SiteSettingsSessn.SettingValue);
                    //        obj1.SessionId = Convert.ToInt16(SiteSettingsSessn.SettingValue);
                    //    }
                    //}

                    //uday

                    //mDepartment deptInfo = new mDepartment();
                    //deptInfo.deptId = CurrentSession.DeptID;
                    //deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
                    //Obj.DepartmentName = deptInfo.deptname;

                    Obj.CurrentUserName = CurrentSession.UserName;
                    Obj.UserDesignation = CurrentSession.MemberDesignation;

                    //obj1.DepartmentName = deptInfo.deptname;

                    obj1.CurrentUserName = CurrentSession.UserName;
                    obj1.UserDesignation = CurrentSession.MemberDesignation;



                    if (Convert.ToString(TempData["Msg"]) != "")
                    {
                        if (Convert.ToString(TempData["Msg"]) == "Save")
                        {
                            Obj.CustomMessage = "Data Save Successfully !!";
                        }
                        if (Convert.ToString(TempData["Msg"]) == "Update")
                        {
                            Obj.CustomMessage = "Data Update Successfully !!";
                        }
                        if (Convert.ToString(TempData["Msg"]) == "File")
                        {
                            Obj.CustomMessage = "Paper Replacement Successfully !!";
                        }
                        if (Convert.ToString(TempData["Msg"]) == "Sign")
                        {
                            Obj.CustomMessage = "Paper Signed Successfully !!";
                        }

                    }
                    else
                    {
                        Obj.CustomMessage = "";
                    }

                
                return View(Obj);
            }

            else
            {
                return RedirectToAction("LoginUP", "Account");

            }
        }

        public ActionResult UpcomingLob()
        {
            tCommitteeModel Obj = new tCommitteeModel();

            // Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetSubmitted", Obj);

            return PartialView("_NewUpcomingLobList", Obj);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpcomingLobGrid(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;
            //this is add for require in new Design
            //Obj.ActionType = "Submitted";
            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "CommitteeUpcomingLOB", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,
            };

            var resultToSort = Obj.ComModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult LaidInHouse()
        {
            tCommitteeModel Obj = new tCommitteeModel();
            return PartialView("_NewLaidInHouseList", Obj);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LaidInHouseGrid(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;

            //this is add for require in new Design
            //  Obj.ActionType = "Submitted";

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "CommitteeLaidinHouse", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,
            };


            var resultToSort = Obj.ComModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };

        }


        public ActionResult PendingToLay()
        {
            tCommitteeModel Obj = new tCommitteeModel();
            return PartialView("_NewPendingToLayList", Obj);
        }

        public ActionResult PendingToLayGrid(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;

            //this is add for require in new Design
            //Obj.ActionType = "Submitted";

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "CommitteePendingToLay", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,
            };

            var resultToSort = Obj.ComModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        #region "DSC Sign"

        public ActionResult SubmitSelectedPaper(string Ids, string lSMCode)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            tCommitteeModel mMinisteryMinisterModel = new tCommitteeModel();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }
            }

            if (Ids != null && Ids != "")
            {
                PaperLaidAttachmentVS.tCommitteeModel = (ICollection<tCommitteeModel>)Helpers.Helper.ExecuteService("Committee", "GetCommitteePaperAttachmentByIds", Ids);

                string path = "";
                string Files = "";
                string Prifix = "";
                foreach (var item in PaperLaidAttachmentVS.tCommitteeModel)
                {
                    path = item.FilePath + item.FileName;
                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }
                }

                mMinisteryMinisterModel.FilePath = Files;
                mMinisteryMinisterModel.HashKey = CurrentSession.UserHashKey;
                mMinisteryMinisterModel.ForSave = Prifix + "/CommitteePaperLiadChairperson/CommitteePaperLiadChairperson/SubmitedSignIntention?PaperLaidId=" + Ids;

                mMinisteryMinisterModel.Message = Prifix + "/CommitteePaperLiadChairperson/CommitteePaperLiadChairperson/UpdateInfoSignPDF?PaperLaidId=" + Ids + "&Uid=" + CurrentSession.UserName;

                string session = Convert.ToString(Session["Ids"]);
            }
            return PartialView("_SignCommitteeFile", mMinisteryMinisterModel);
        }

        [SBLAuthorize(Allow = "ALL")]
        public ActionResult SubmitedSignIntention(HttpPostedFileBase file, string PaperLaidId)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);
            tPaperLaidV model = new tPaperLaidV();
            if (file != null)
            {


                //string filename = System.IO.Path.GetFileName(file.FileName);
                //bool folderExists = Directory.Exists(Server.MapPath(file.FileName));

                string AssemblyId = "";
                string SessionId = "";

                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
                AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);




                string Url = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/" + "/Signed/";
                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                string ext = Path.GetExtension(file.FileName);
                string fileName = file.FileName.Replace(ext, "");


                file.SaveAs(Server.MapPath(Url + fileName + ext));


                //string path = System.IO.Path.Combine(Server.MapPath("~/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/"), filename);

                //int indexof = path.LastIndexOf("\\");

                //string directory = path.Substring(0, indexof);
                ////  string directory = Server.MapPath(path);
                //if (!System.IO.Directory.Exists(directory))
                //{
                //    System.IO.Directory.CreateDirectory(directory);
                //}

                //file.SaveAs(path);


                return null;
            }

            return RedirectToAction("CommitteeDashboard", "CommitteePaperLiadChairperson", new { area = "CommitteePaperLiadChairperson", message = "Sign completed" });
        }

        [SBLAuthorize(Allow = "ALL")]
        public ActionResult UpdateInfoSignPDF(string PaperLaidId, string Uid)
        {
            PaperLaidId = Sanitizer.GetSafeHtmlFragment(PaperLaidId);

            Uid = Sanitizer.GetSafeHtmlFragment(Uid);
            tPaperLaidV model = new tPaperLaidV();
            tPaperLaidTemp pT = new tPaperLaidTemp();
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                model.UserID = new Guid(CurrentSession.UserID);
            }

            pT.FilePath = PaperLaidId;
            pT.MinisterSubmittedDate = DateTime.Now;
            pT.DeptSubmittedDate = DateTime.Now;
            pT.DeptSubmittedBy = Convert.ToString(Uid);

            string AssemblyId = "";
            string SessionId = "";

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
            AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);
            pT.AssemblyId = AssemblyId;
            pT.SessionId = SessionId;
            pT.CommtSubmittedDate = DateTime.Now;
            pT.CommtSubmittedBy = CurrentSession.UserName;

            var modelReturned = Helper.ExecuteService("Committee", "InsertSignPathAttachmentCommittee", pT) as tPaperLaidTemp;

            var modelReturned1 = Helper.ExecuteService("Committee", "UpdateDepartmentMinisterActivePaperIdCommittee", PaperLaidId) as tPaperLaidTemp;

            TempData["Msg"] = "Sign";

            return RedirectToAction("CommitteeDashboard", "CommitteePaperLiadChairperson", new { area = "CommitteePaperLiadChairperson", message = "Sign completed" });
        }

        private string alert(string p)
        {
            throw new NotImplementedException();
        }

        #endregion

        public ActionResult SubmitHandWrittenSignatureSelectedPaperWithoutSignImage(string Ids, string lSMCode)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            tPaperLaidTemp pT = new tPaperLaidTemp();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }
            }

            if (Ids != null && Ids != "")
            {
                PaperLaidAttachmentVS.tCommitteeModel = (ICollection<tCommitteeModel>)Helpers.Helper.ExecuteService("Committee", "GetCommitteePaperAttachmentByIds", Ids);

                string path = "";
                string Files = "";
                string Prifix = "";
                string OldFileName = "";

                foreach (var item in PaperLaidAttachmentVS.tCommitteeModel)
                {
                    path = item.FilePath + item.FileName;
                    OldFileName = item.FileName;
                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }

                    string AssemblyId = "";
                    string SessionId = "";

                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
                    AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);

                    pT.FilePath = Ids;
                    pT.MinisterSubmittedDate = DateTime.Now;
                    pT.MinisterSubmittedBy = Convert.ToInt32(CurrentSession.UserName);
                    pT.SessionId = SessionId;
                    pT.AssemblyId = AssemblyId;
                    pT.CommtSubmittedDate = DateTime.Now;
                    pT.CommtSubmittedBy = CurrentSession.UserName;

#pragma warning disable CS0219 // The variable 'imageFileLoc' is assigned but its value is never used
                    string imageFileLoc = "";
#pragma warning restore CS0219 // The variable 'imageFileLoc' is assigned but its value is never used

                    if (CurrentSession.SignaturePath == "")
                    {
                        imageFileLoc = "";
                        string[] Afile = OldFileName.Split('.');
                        OldFileName = Afile[0];

                        string newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/";
                        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(newFileLocation));
                        if (!Dir.Exists)
                        {
                            Dir.Create();
                        }
                        string PasteLoc = Server.MapPath(newFileLocation + OldFileName + "_Signed.pdf");
                        string CopyLoc = Server.MapPath(path);

                        System.IO.File.Copy(CopyLoc, PasteLoc, true);
                    }

                }

                mMinisteryMinisterModel.FilePath = Files;

                var modelReturned = Helper.ExecuteService("Committee", "InsertSignPathAttachmentCommittee", pT) as tPaperLaidTemp;

                Helper.ExecuteService("Committee", "UpdateDepartmentMinisterActivePaperIdCommittee", Ids);

                ///For LOB File Replace
                string[] obja = Ids.Split(',');

            }

            TempData["Msg"] = "Sign";
            return RedirectToAction("CommitteeDashboard", "CommitteePaperLiadChairperson", new { area = "CommitteePaperLiadChairperson", message = "Sign completed" });
        }

        public ActionResult CommitteeChairpersonHandWrittenSignDocument(string SIds)
        {
            HandwrittenSignatureModel model = new HandwrittenSignatureModel();
            model.PagesToApplySignatureList = StaticControlBinder.GetPagesToApplySignature();
            model.PositionToApplySignatureList = StaticControlBinder.GetPositionToApplySignature();
            return View("_CommitteeChairpersonHandWrittenSignDocument", model);
        }

        public ActionResult SubmitHandWrittenSignatureSelectedPaperWithoutSign(string Ids, string lSMCode, int SignatureType, int PagesToApplySignatureID, int PositionToApplySignatureID)
        {
            tPaperLaidV PaperLaidAttachmentVS = new tPaperLaidV();
            tPaperLaidTemp pT = new tPaperLaidTemp();
            mMinisteryMinisterModel mMinisteryMinisterModel = new mMinisteryMinisterModel();

            if (lSMCode != null && lSMCode != "")
            {
                if (lSMCode == "SQ")
                {
                    TempData["lSM"] = "SQ";
                }
                else if (lSMCode == "USQ")
                {
                    TempData["lSM"] = "USQ";
                }
                else if (lSMCode == "NTC")
                {
                    TempData["lSM"] = "NTC";
                }
                else if (lSMCode == "BLL")
                {
                    TempData["lSM"] = "BLL";
                }
                else if (lSMCode == "OPT")
                {
                    TempData["lSM"] = "OPT";
                }
            }

            if (Ids != null && Ids != "")
            {
                PaperLaidAttachmentVS.tCommitteeModel = (ICollection<tCommitteeModel>)Helpers.Helper.ExecuteService("Committee", "GetCommitteePaperAttachmentByIds", Ids);

                string path = "";
                string Files = "";
                string Prifix = "";
                string OldFileName = "";
                string newFileLocation = "";
                foreach (var item in PaperLaidAttachmentVS.tCommitteeModel)
                {
                    path = item.FilePath + item.FileName;
                    OldFileName = item.FileName;
                    path = path.Replace(" ", "%20");
                    var test = Request.Url.AbsoluteUri;
                    string[] abc = test.Split('/');
                    Prifix = abc[0] + "//" + abc[2];
                    if (Files == "")
                    {
                        Files = Prifix + path;
                    }
                    else
                    {
                        Files = Files + "," + Prifix + path;
                    }


                    string AssemblyId = "";
                    string SessionId = "";

                    SiteSettings siteSettingMod = new SiteSettings();
                    siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                    SessionId = Convert.ToString(siteSettingMod.SessionCode); //Convert.ToInt32(siteSettingMod.SessionCode);
                    AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);// Convert.ToInt32(siteSettingMod.AssemblyCode);

                    pT.FilePath = Ids;
                    pT.MinisterSubmittedDate = DateTime.Now;
                    pT.MinisterSubmittedBy = Convert.ToInt32(CurrentSession.UserName);
                    pT.SessionId = SessionId;
                    pT.AssemblyId = AssemblyId;

                    pT.CommtSubmittedDate = DateTime.Now;
                    pT.CommtSubmittedBy = CurrentSession.UserName;

                    string imageFileLoc = Server.MapPath(CurrentSession.SignaturePath);

                    string[] Afile = OldFileName.Split('.');
                    OldFileName = Afile[0];
                    newFileLocation = "/PaperLaid/" + AssemblyId + "/" + SessionId + "/Signed/" + OldFileName + "_signed.pdf";
                    // string newFileLocation = item.FilePath + "HWSigned/" + Path.GetFileNameWithoutExtension(path) + "_HWSigned" + Path.GetExtension(path);

                    string textLine1 = "";
                    string textLine2 = "";

                    if (CurrentSession.UserName != "")
                    {
                        if (CurrentSession.IsMember != null)
                        {
                            if (CurrentSession.IsMember.ToUpper() == "FALSE")
                            {

                                mEmployee empMod = new mEmployee();
                                if (CurrentSession.DeptID != "")
                                {
                                    empMod.deptid = CurrentSession.DeptID;
                                }
                                empMod.empcd = CurrentSession.UserName;
                                empMod = (mEmployee)Helper.ExecuteService("Employee", "GetEmployeeDetialsByEmpID", siteSettingMod);
                                if (empMod != null)
                                {
                                    textLine1 = empMod.empfname + "" + empMod.emplname;
                                }
                                else
                                {
                                    textLine1 = "";
                                }
                            }
                            else
                            {

                                if (CurrentSession.UserName != null && CurrentSession.UserName != "")
                                {
                                    PaperLaidAttachmentVS.LoginId = CurrentSession.UserName;
                                }

                                PaperLaidAttachmentVS = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", PaperLaidAttachmentVS);
                                foreach (var item1 in PaperLaidAttachmentVS.mMinisteryMinister)
                                {
                                    PaperLaidAttachmentVS.MinisterName = item1.MinisterName;
                                    PaperLaidAttachmentVS.MinistryId = item1.MinistryID;
                                }

                                textLine1 = PaperLaidAttachmentVS.MinisterName;

                            }
                        }

                    }
                    else
                    {
                        textLine1 = "";
                    }
                    if (CurrentSession.Designation != "")
                    {
                        textLine2 = CurrentSession.MemberDesignation;
                    }
                    else
                    {
                        textLine2 = "";
                    }


                    PDFExtensions.InsertImageToPdf(Server.MapPath(path), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);

                }


                mMinisteryMinisterModel.FilePath = Files;


                var modelReturned = Helper.ExecuteService("Committee", "InsertSignPathAttachmentCommittee", pT) as tPaperLaidTemp;

                Helper.ExecuteService("Committee", "UpdateDepartmentMinisterActivePaperIdCommittee", Ids);


                //var modelReturned = Helper.ExecuteService("PaperLaidMinister", "InsertSignPathAttachment", pT) as tPaperLaidTemp;

                //Helper.ExecuteService("PaperLaid", "UpdateMinisterActivePaperId", Ids);

                ///For LOB File Replace
                string[] obja = Ids.Split(',');


            }

            TempData["Msg"] = "Sign";
            return RedirectToAction("CommitteeDashboard", "CommitteePaperLiadChairperson", new { area = "CommitteePaperLiadChairperson", message = "Sign completed" });
        }
        
        #endregion
    }
}
