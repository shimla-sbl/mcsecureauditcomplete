﻿using Email.API;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Grievances.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SMS.API;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EvoPdf;

namespace SBL.eLegistrator.HouseController.Web.Areas.Grievances.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class AssingGrievancesController : Controller
    {
        //
        // GET: /Grievances/AssingGrievances/


        public ActionResult Index(int pageId = 1, int pageSize = 20)
        {
            try
            {

                string MemberCode = CurrentSession.MemberCode;
                var GrievanceList = (List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode);

                ViewBag.PageSize = pageSize;
                List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
                if (pageId == 1)
                {
                    pagedRecord = GrievanceList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                ViewBag.GrievanceCount = GrievanceList.Count;

                return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Grievances List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GrievanceIndex(int pageId = 1, int pageSize = 20)
        {
            try
            {
                int Status = SBL.DomainModel.Models.Enums.GrievanceStatus.Pending.GetHashCode();
                string MemberCode = CurrentSession.MemberCode;
                var GrievanceList = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).OrderByDescending(m => m.SubmittedDate).ToList();

                ViewBag.PageSize = pageSize;
                List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
                if (pageId == 1)
                {
                    pagedRecord = GrievanceList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                ViewBag.GrievanceCount = GrievanceList.Count;

                return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Grievances List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 20, int Status = 0, int OrderBy = 0)
        {
            string MemberCode = CurrentSession.MemberCode;
            List<tMemberGrievances> GrievanceList = new List<tMemberGrievances>();
            var GrievanceData = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();
            if (OrderBy == 0)
                GrievanceList = GrievanceData.OrderByDescending(m => m.SubmittedDate).ToList();
            else
                GrievanceList = GrievanceData.OrderByDescending(m => m.StatusModifiedDate).ToList();
            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = GrievanceList.Count;
            return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
        }

        //public PartialViewResult GetGrievanceByStatus(int pageId = 1, int pageSize = 20, int Status = 0, int OrderBy = 0)
        //{
        //    string MemberCode = CurrentSession.MemberCode;
        //    List<tMemberGrievances> GrievanceList = new List<tMemberGrievances>();
        //    var GrievanceData = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();
        //    if (OrderBy == 0)
        //        GrievanceList = GrievanceData.OrderByDescending(m => m.SubmittedDate).ToList();
        //    else
        //        GrievanceList = GrievanceData.OrderByDescending(m => m.StatusModifiedDate).ToList();
        //    ViewBag.PageSize = pageSize;
        //    List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
        //    if (pageId == 1)
        //    {
        //        pagedRecord = GrievanceList.Take(pageSize).ToList();
        //    }
        //    else
        //    {
        //        int r = (pageId - 1) * pageSize;
        //        pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
        //    }

        //    ViewBag.CurrentPage = pageId;
        //    ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
        //    ViewBag.GrievanceCount = GrievanceList.Count;
        //    return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
        //}
        //public PartialViewResult GetGrievanceByOrderBy(int pageId = 1, int pageSize = 20, int Status = 0, int OrderBy = 0)
        //{
        //    string MemberCode = CurrentSession.MemberCode;
        //    List<tMemberGrievances> GrievanceList = new List<tMemberGrievances>();
        //    var GrievanceData = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();
        //    if (OrderBy == 0)
        //        GrievanceList = GrievanceData.OrderByDescending(m => m.SubmittedDate).ToList();
        //    else
        //        GrievanceList = GrievanceData.OrderByDescending(m => m.StatusModifiedDate).ToList();

        //    ViewBag.PageSize = pageSize;
        //    List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
        //    if (pageId == 1)
        //    {
        //        pagedRecord = GrievanceList.Take(pageSize).ToList();
        //    }
        //    else
        //    {
        //        int r = (pageId - 1) * pageSize;
        //        pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
        //    }

        //    ViewBag.CurrentPage = pageId;
        //    ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
        //    ViewBag.GrievanceCount = GrievanceList.Count;
        //    return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
        //}

        public PartialViewResult SearchGrievance(int pageId , int pageSize, int Status , int OrderBy)
        {
            pageId = 1;
            pageSize = 20;
            OrderBy = 0;
            string MemberCode = CurrentSession.MemberCode;
            List<tMemberGrievances> GrievanceList = new List<tMemberGrievances>();
            var GrievanceData = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();
            if (OrderBy == 0)
                GrievanceList = GrievanceData.OrderByDescending(m => m.SubmittedDate).ToList();
            else
                GrievanceList = GrievanceData.OrderByDescending(m => m.StatusModifiedDate).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = GrievanceList.Count;
            return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
        }


        public PartialViewResult PopulateOffice(string GrvCode, int StatusId = 0)
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            ViewBag.GrvCode = GrvCode;
            ViewBag.StatusId = StatusId;
            string currentAssemby = CurrentSession.AssemblyId;
            var obj = (ForwardGrievance)Helper.ExecuteService("Grievance", "GetForwardGrievanceByGRVCode", new ForwardGrievance { MemberCode = MemberCode, GrvCode = GrvCode });

            var fillListGenric = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "memberDepartment", new string[] { CurrentSession.MemberCode, currentAssemby });

            if (obj != null)
            {
                ViewBag.DepartmentList = new SelectList(fillListGenric, "Value", "Text", obj.DeptId);
                List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetExecutiveOffice", new string[] { obj.DeptId, CurrentSession.MemberCode });
                ViewBag.OfficeList = new SelectList(_list, "Value", "Text", obj.OfficeId);
            }
            else
            {
                ViewBag.DepartmentList = new SelectList(fillListGenric, "Value", "Text");
                var result = new List<SelectListItem>();
                //  result.Add(new SelectListItem() { Text = "--Select Office--", Value = "0" });

                ViewBag.OfficeList = new SelectList(result, "Value", "Text");
            }



            return PartialView("/Areas/Grievances/Views/AssingGrievances/_PopulateOffice.cshtml");
        }

        public PartialViewResult PopulateAllOffice(string GrvCodes, int StatusId = 0)
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            ViewBag.GrvCodes = GrvCodes;
            ViewBag.StatusId = StatusId;
            string currentAssemby = CurrentSession.AssemblyId;
            var fillListGenric = (List<fillListGenric>)Helper.ExecuteService("ConstituencyVS", "memberDepartment", new string[] { CurrentSession.MemberCode, currentAssemby });


            ViewBag.DepartmentList = new SelectList(fillListGenric, "Value", "Text");
            var result = new List<SelectListItem>();
            ViewBag.OfficeList = new SelectList(result, "Value", "Text");



            return PartialView("/Areas/Grievances/Views/AssingGrievances/_PopulateAllOffice.cshtml");
        }

        public JsonResult GetOffice(string ExecutiveDepartment)
        {

            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "GetExecutiveOffice", new string[] { ExecutiveDepartment, CurrentSession.MemberCode });
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult DisposeGrievance(string GrvCode, int Status = 0)
        {

            tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = GrvCode });
            ViewBag.GrvCode = GrvCode;
            ViewBag.Status = Status;
            ViewBag.UserName = MemberGrievances.UserName;

            return PartialView("/Areas/Grievances/Views/AssingGrievances/_DisposeGrievance.cshtml", MemberGrievances);
        }

        public PartialViewResult SaveDisposeGrievance(tGrievanceReply model, int pageId = 1, int pageSize = 20, int Status = 0)
        {
            string AadharId = CurrentSession.AadharId;
            string MemberCode = CurrentSession.MemberCode;
            try
            {
                model.OfficerCode = AadharId;
                model.ActionDate = DateTime.Now;
                model.DisposedBy = "Member";
                model.StatusId = SBL.DomainModel.Models.Enums.GrievanceStatus.Disposed.GetHashCode();
                Helper.ExecuteService("Grievance", "CreateGrievanceReply", model);

                tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = model.GrvCode, OfficerCode = AadharId });
                MemberGrievances.StatusId = SBL.DomainModel.Models.Enums.GrievanceStatus.Disposed.GetHashCode();
                MemberGrievances.DiposedUserId = AadharId;
                MemberGrievances.DisposedBy = "Member";
                MemberGrievances.StatusModifiedDate = DateTime.Now;
                Helper.ExecuteService("Grievance", "UpdateMemberGrievances", MemberGrievances);

                ViewBag.Msg = "Sucessfully disposed grievance";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";

            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }


            var GrievanceList = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).OrderByDescending(m => m.SubmittedDate).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();

            ViewBag.GrievanceCount = GrievanceList.Count;

            return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
        }

        [HttpPost, ValidateInput(false)]

        public PartialViewResult SaveOffice(FormCollection collection, int pageId = 1, int pageSize = 20, int Status = 0)
        {
            string MemberCode = CurrentSession.MemberCode;
            string GrvCode = collection["hdnGrvCode"];
            // string[] RowCount = collection["OfficerRowCount"].Split(',');
            tMemberGrievances MemGrievance = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = GrvCode });
            try
            {
                if (MemGrievance != null && MemGrievance.StatusId != 3 && MemGrievance.StatusId != 4)
                {
                    MemGrievance.StatusModifiedDate = DateTime.Now;
                    MemGrievance.StatusId = SBL.DomainModel.Models.Enums.GrievanceStatus.Forwarded.GetHashCode();
                    Helper.ExecuteService("Grievance", "UpdateMemberGrievances", MemGrievance);
                }
                DataSet dataSet = new DataSet();
                var ForwardGrievanceParam = new List<KeyValuePair<string, string>>();
                ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@MemberCode", MemberCode));
                ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@GrvCode", GrvCode));
                dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteForwardGrievance", ForwardGrievanceParam);


                string OfficeId = collection["OfficeId"];
                string DeptId = collection["DepartmentId"];

                ForwardGrievance obj = new ForwardGrievance();
                obj.DeptId = DeptId;
                obj.MemberCode = int.Parse(MemberCode);
                obj.GrvCode = GrvCode;
                obj.ForwardDate = DateTime.Now;
                obj.Type = "Grievance";
                obj.OfficeId = int.Parse(OfficeId);
                obj.ForwardId = (Int64)Helper.ExecuteService("Grievance", "CreateForwardGrievance", obj);

                ViewBag.Msg = "Sucessfully assign office";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! Something went wrong.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }


            var GrievanceList = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();

            ViewBag.GrievanceCount = GrievanceList.Count;
            return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
        }

        public PartialViewResult SaveAllOffice(FormCollection collection, int pageId = 1, int pageSize = 20, int Status = 0)
        {
            string MemberCode = CurrentSession.MemberCode;
            string[] GrvCodes = collection["hdnGrvCodes"].Split(','); ;
            // string[] RowCount = collection["OfficerRowCount"].Split(',');
            foreach (var item in GrvCodes)
            {


                tMemberGrievances MemGrievance = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = item });
                try
                {
                    if (MemGrievance != null && MemGrievance.StatusId != 3 && MemGrievance.StatusId != 4)
                    {
                        MemGrievance.StatusModifiedDate = DateTime.Now;
                        MemGrievance.StatusId = SBL.DomainModel.Models.Enums.GrievanceStatus.Forwarded.GetHashCode();
                        Helper.ExecuteService("Grievance", "UpdateMemberGrievances", MemGrievance);
                    }
                    DataSet dataSet = new DataSet();
                    var ForwardGrievanceParam = new List<KeyValuePair<string, string>>();
                    ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@MemberCode", MemberCode));
                    ForwardGrievanceParam.Add(new KeyValuePair<string, string>("@GrvCode", item));
                    dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "DeleteForwardGrievance", ForwardGrievanceParam);


                    string OfficeId = collection["OfficeIds"];
                    string DeptId = collection["DepartmentIds"];

                    ForwardGrievance obj = new ForwardGrievance();
                    obj.DeptId = DeptId;
                    obj.MemberCode = int.Parse(MemberCode);
                    obj.GrvCode = item;
                    obj.ForwardDate = DateTime.Now;
                    obj.Type = "Grievance";
                    obj.OfficeId = int.Parse(OfficeId);
                    obj.ForwardId = (Int64)Helper.ExecuteService("Grievance", "CreateForwardGrievance", obj);

                    ViewBag.Msg = "Sucessfully assign office";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                catch (Exception)
                {
                    ViewBag.Msg = "Ooops....! Something went wrong.";
                    ViewBag.Class = "alert alert-error";
                    ViewBag.Notification = "Error";
                    throw;
                }
            }

            var GrievanceList = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


            if (TempData["Msg"] != null)
                ViewBag.Msg = TempData["Msg"].ToString();

            ViewBag.GrievanceCount = GrievanceList.Count;
            return PartialView("/Areas/Grievances/Views/AssingGrievances/_GrievanceList.cshtml", pagedRecord);
        }


        public PartialViewResult ShowAssignOfficerList(string GrvCode)
        {
            int MemberCode = int.Parse(CurrentSession.MemberCode);
            string AadharId = CurrentSession.AadharId;
            tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = GrvCode });
            ViewBag.GrvCode = GrvCode;
            ViewBag.UserName = MemberGrievances.UserName;
            var OfficerInfoList = (List<ForwardGrievance>)Helper.ExecuteService("Grievance", "GetFieldOfficerByMemberCode", new ForwardGrievance { MemberCode = MemberCode, GrvCode = GrvCode });

            var GrievanceReplyList = (List<tGrievanceReply>)Helper.ExecuteService("Grievance", "GetAllGrievanceReply", GrvCode);
            GrievanceModel model = new GrievanceModel();
            model.ForwardGrievanceList = OfficerInfoList;
            model.GrievanceReplyList = GrievanceReplyList;
            model.tMemberGrievances = MemberGrievances;
            return PartialView("/Areas/Grievances/Views/AssingGrievances/_ShowAssignOfficer.cshtml", model);
        }
        [HttpGet]
        public ActionResult ShowGrievanceDetail_Pdf(string GrvCode)
        {
            int Memcode = 0;
            if (CurrentSession.SubDivisionId != ""  && CurrentSession.SubUserTypeID=="40")  //sdm
            {
                Memcode = 0;
            }
            if (CurrentSession.MemberCode != "")
            {
                int MemberCode = int.Parse(CurrentSession.MemberCode);
                Memcode = MemberCode;
            }
          
            string AadharId = CurrentSession.AadharId;
            pdfGrievance model = new pdfGrievance();
            List<pdfGrievance> _list = new List<pdfGrievance>();
            DataSet dataSet = new DataSet();
            var _paremeter = new List<KeyValuePair<string, string>>();
            _paremeter.Add(new KeyValuePair<string, string>("@GrvCode", GrvCode));
            _paremeter.Add(new KeyValuePair<string, string>("@MemberCode", Memcode.ToString()));
            dataSet = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "Get_AllGrievanceInfo", _paremeter);
            if (dataSet != null)
            {
                if (dataSet.Tables.Count > 0)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {
                        model.PublicName = dataSet.Tables[0].Rows[0]["PublicName"].ToString();
                        model.PublicAddress = dataSet.Tables[0].Rows[0]["PublicAddress"].ToString();
                        model.PublicMobile = dataSet.Tables[0].Rows[0]["PublicMobile"].ToString();
                        model.PublicAAdharId = dataSet.Tables[0].Rows[0]["PublicAadharID"].ToString();
                        model.Public_Profile = dataSet.Tables[0].Rows[0]["ProfilePic"].ToString();
                        model.GrvCode = dataSet.Tables[0].Rows[0]["GrvCode"].ToString();
                        model.GrvDesc = dataSet.Tables[0].Rows[0]["InformationContent"].ToString();
                        model.SubmitDate = dataSet.Tables[0].Rows[0]["SubmitDate"].ToString();
                        model.PostedTo = dataSet.Tables[0].Rows[0]["MemName"].ToString();
                        model.GrvImage = dataSet.Tables[0].Rows[0]["ImageName"].ToString();
                        model.GrvStatus = dataSet.Tables[0].Rows[0]["StatusId"].ToString();
                        if (model.GrvStatus == "0")
                        {
                            model.GrvStatus = "Pending";
                        }
                        else if (model.GrvStatus == "1")
                        {
                            model.GrvStatus = "Forwarded";
                        }
                        else if (model.GrvStatus == "2")
                        {
                            model.GrvStatus = "InProgress";
                        }
                        else if (model.GrvStatus == "3")
                        {
                            model.GrvStatus = "Disposed";
                        }
                        //else
                        //{
                        //    model.GrvStatus = "Disposed";
                        //}
                    }
                    if (dataSet.Tables[1].Rows.Count > 0)
                    {
                        model.Deptname = dataSet.Tables[1].Rows[0]["deptname"].ToString();
                        model.Officename = dataSet.Tables[1].Rows[0]["officename"].ToString();
                    }
                    if (dataSet.Tables[2].Rows.Count > 0)
                    {
                        model.ActionTaken = dataSet.Tables[2].Rows[0]["Reply"].ToString();
                        model.ActionDate = dataSet.Tables[2].Rows[0]["ActionDate"].ToString();
                        model.GrvActionStatus = dataSet.Tables[2].Rows[0]["StatusId"].ToString();
                        if (model.GrvActionStatus == "0")
                        {
                            model.GrvActionStatus = "Pending";
                        }
                        else if (model.GrvActionStatus == "1")
                        {
                            model.GrvActionStatus = "Forwarded";
                        }
                        else if (model.GrvActionStatus == "2")
                        {
                            model.GrvActionStatus = "InProgress";
                        }
                        else if (model.GrvActionStatus == "3")
                        {
                            model.GrvActionStatus = "Disposed";
                        }
                        //else
                        //{
                        //    model.GrvActionStatus = "Disposed";
                        //}
                        model.ActionBy = dataSet.Tables[2].Rows[0]["ActionBy"].ToString();
                        model.Mem_MobileNo = dataSet.Tables[2].Rows[0]["MobileNo"].ToString();
                    }

                }
            }
            // _list.Add(model);
            var _pdf1 = GeneratePdf1(model);

            
            return Json(_pdf1, JsonRequestBehavior.AllowGet);
        }
        public Object GeneratePdf1(pdfGrievance _data)
        {
            string imagepath = "";
            string ProfilePic = "";
            var Get_NoImageFromLocal = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            string Noimagepath = Get_NoImageFromLocal.SettingValue + "/ServerUploadsPath/No_ImageAvial.png";
            string userimage = Get_NoImageFromLocal.SettingValue + "/ServerUploadsPath/nouser.png";
            var Get_GrvImageFromDataBase = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
            if(_data.GrvImage==null || _data.GrvImage=="")
            {
                imagepath = Noimagepath;
                //Server.MapPath("../../Images/Common/No_ImageAvial.png");   //"http://localhost:50730/Images/Common/No_ImageAvial.png";
             }
            else 
            {
                imagepath = Get_GrvImageFromDataBase.SettingValue + "/ServerUploadsPath/" + _data.GrvImage + "";
            }
              
              if (_data.Public_Profile == null || _data.Public_Profile == "")
              {
                  ProfilePic = userimage;
                      //Server.MapPath("../../Images/Common/No_ImageAvial.png");
              }
              else
              {
                   ProfilePic = _data.Public_Profile;
              }
           
      //   D:\ProjectSVN\New folder\Project1\SBL.eLegistrator.HouseController.Web\Grievances\AssingGrievances\Images\Common\No_ImageAvial.png
       //  D:\ProjectSVN\New folder\Project1\SBL.eLegistrator.HouseController.Web\Images\Common\No_ImageAvial.png
            try
            {
#pragma warning disable CS0219 // The variable 'count' is assigned but its value is never used
                int count = 0;
#pragma warning restore CS0219 // The variable 'count' is assigned but its value is never used
                string outXml = "";

                string fileName = "";
                string savedPDF = string.Empty;

                if (_data != null)
                {

                    outXml = @"<html>                           
                                <body style='font-family:DVOT-Yogesh;color:#003366;'> <div><div style='width: 100%;'>";
                    outXml += @"<div> 


                    </div>
            
<table style='width:100%; border: solid 1px black;padding:10px;'>
<tr><td>
<table style='width:100%;border-bottom:1px solid black;padding-bottom:10px;'>
 
      <tr>
          <td style='text-align:left;font-size:28px;' >
             <b> Personal Information ( Posted By )" + @"</b>
              </td>
              </tr>
      </table>

<table style='width: 100%; padding-bottom:10px;font-size:20px;color:black;'>
           ";
                    //foreach (var item in _data.GrvList1)
                    //{
                    //  count = count + 1;
                    //  string style = (count % 2 == 0) ? "odd" : "even";
                    // //  <img  src=" + @"data:image/jpeg;base64," + imagepath + "                     height='100' width='100'/> 
                    outXml += @"

  <tr style='border-bottom:1px  solid black;'>
            <td style='text-align: left;width:100px;'><img  height='90' width='90'  src=" + "data:image/jpeg;base64," + ProfilePic + @"/> </td>
        <td style='text-align: left;'><b>Name : </b>" + _data.PublicName + @" <br />
  <b> Mobile Number:</b>" + _data.PublicMobile + @"<br />
 <b>Address:</b>" + _data.PublicAddress + @"<br /></td>
        </tr>
<tr><td colspan='2' style='text-align: center;border-bottom: 1px solid black'> <br/>
</td></tr>
<tr><td colspan='2'  style='text-align: center;padding:10px;'><b>Grievance Detail</b> <br/></td></tr>                    
<tr><td  colspan='2'>
<table style='width: 100%; border-collapse: collapse;border: solid 1px black;padding:5px;font-size:20px;color:black;'>
<tr><td style='text-align:left;width:80%;'><table style='width:100%; font-size:18px;color:black;'><tr><td>
<b>Grievance Number :</b>" + _data.GrvCode + @" <br/>   
<b>Grievance Description :</b>" + _data.GrvDesc + @" <br/> 
<b>Posted On :</b>" + _data.SubmitDate + @"<br/>
<b>Posted To :</b>" + _data.PostedTo + @"<br/> </td></tr></table></td>

 <td style='width:100px;text-align:left;'><b> </b> <br/> 
  <img height='90' width='90' src=" + @"data:image/jpeg;base64," + imagepath + @" /><br/>

</td>
</tr>
<tr><td colspan='2'><br/></td></tr>
</table>
   </td></tr>
<tr><td colspan='2' style='text-align: center;padding:10px;'><b>Action Detail</b> <br/></td></tr>
<tr><td colspan='2'> <table style='width: 100%; border-collapse: collapse;border: solid 1px black;padding:10px;font-size:20px; color:black;'>
<tr><td style='text-align: left;'><b>Department Name :</b>" + _data.Deptname + @"</td><td    style='text-align: left;'><b>Office Name :</b>" + _data.Officename + @"</td></tr>
<br/>
    <tr><td colspan='2' style='text-align: left; '><b>Action Taken : </b>" + _data.ActionTaken + @"</td></tr> 
    <tr><td  colspan='2'style='text-align: left;'><b>Action Date :</b>" + _data.ActionDate + @"</td></tr>
  <tr><td colspan='2' style='text-align: left;'><b>Action By : </b> " + _data.ActionBy + @"</td></tr>
 <tr><td colspan='2'  style='text-align: left;'><b>Status : </b> " + _data.GrvActionStatus + @"</td></tr>
</table>
</td></tr></td></tr></table>
";
                    //} src=http://164.100.88.163/FileStructure/ServerUploadsPath/" + _data.GrvImage + @" /><br/>  src='file:///C:/inetpub/e_Vidhan/FileStructure/ServerUploadsPath/' " + _data.Public_Profile + @"/> 
                }
           //// file:///C:/inetpub/e_Vidhan/FileStructure/ServerUploadsPath/353327061034732_1489474722348.jpg
                MemoryStream output = new MemoryStream();

                EvoPdf.Document document1 = new EvoPdf.Document();

                // set the license key
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40), PdfPageOrientation.Portrait);

                AddElementResult addResult;

                HtmlToPdfElement htmlToPdfElement;
                string htmlStringToConvert = outXml;
                string baseURL = "";
                htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, baseURL);

                addResult = page.AddElement(htmlToPdfElement);
                byte[] pdfBytes = document1.Save();

                try
                {
                    output.Write(pdfBytes, 0, pdfBytes.Length);
                    output.Position = 0;

                }
                finally
                {
                    // close the PDF document to release the resources
                    document1.Close();
                }
                string url = "/UserGrievanceReport/";
                // fileName = "QuestionList" + "DateFrom" + "_" + Convert.ToDateTime(model.DateFrom).ToString("dd/MM/yyyy") + "_" + "DateTo" + "_" + Convert.ToDateTime(model.DateTo).ToString("dd/MM/yyyy") + "_" + "S" + ".pdf";
                // fileName = fileName.Replace("/", "-");
                //  HttpContext.Response.AddHeader("content-disposition", "attachment; filename=QuestionsList" + fileName);
                fileName = "UserGrievance.pdf";
                string directory = Server.MapPath(url);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                var path = Path.Combine(directory, fileName);
                System.IO.FileStream _FileStream = new System.IO.FileStream(path, System.IO.FileMode.Create, System.IO.FileAccess.Write);
                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                //DownloadDateWiseQuestion();
                return url + fileName;


            }

            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }


        }

        #region Send Email
        public static void SendEmailDetails(List<string> emailList, List<string> mobileList, string msgBody, string mailSubject, string mailBody, bool mobileStatus, bool emailStatus, string SavedPdfPath)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (emailList != null)
            {
                emailAddresses.AddRange(emailList);

            }
            if (mobileList != null)
            {
                phoneNumbers.AddRange(mobileList);
            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            smsMessage.SMSText = msgBody;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            emailMessage._isBodyHtml = true;
            emailMessage._subject = mailSubject;
            emailMessage._body = mailBody;
            if (!string.IsNullOrEmpty(SavedPdfPath))
            {
                EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
                emailMessage._attachments = new List<EAttachment>();
                emailMessage._attachments.Add(ea);
            }
            try
            {
                if (emailList != null || mobileList != null)
                {
                    Notification.Send(mobileStatus, emailStatus, smsMessage, emailMessage);
                }

            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                // throw;
            }

            #endregion SMS And Email
        }

        #endregion
    }
}
