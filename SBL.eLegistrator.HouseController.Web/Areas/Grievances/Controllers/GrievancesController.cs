﻿using EvoPdf;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Grievances.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Ministery;
using System.Web;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.Office;
using System.Globalization;
using SBL.eLegistrator.HouseController.Web.Areas.Admin.Models;
using SBL.eLegistrator.HouseController.Web.Extensions;

namespace SBL.eLegistrator.HouseController.Web.Areas.Grievances.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class GrievancesController : Controller
    {
        //
        // GET: /Grievances/Grievances/

        public ActionResult Index()
        {
            //CurrentSession.UserID = "64727E89-ABD5-45FA-8DF1-F1E045C33DEE";
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }

            tPaperLaidV model = new tPaperLaidV();

            mSession mSessionModel = new mSession();
            mAssembly mAssemblymodel = new mAssembly();
            model = (tPaperLaidV)Helper.ExecuteService("SiteSetting", "GetSettings", model);
            model.mSession = (ICollection<mSession>)Helper.ExecuteService("Session", "GetAllSessionData", mSessionModel);
            model.mAssembly = (ICollection<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssembly", mAssemblymodel);

            //Added by Ashwani 
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            if (siteSettingMod != null)
            {
                if (string.IsNullOrEmpty(CurrentSession.AssemblyId))
                {
                    CurrentSession.AssemblyId = siteSettingMod.AssemblyCode.ToString();
                }

                if (string.IsNullOrEmpty(CurrentSession.SessionId))
                {
                    CurrentSession.SessionId = siteSettingMod.SessionCode.ToString();
                }
            }



            //Get the Total count of All Type of question.
            //added code venkat for dynamic
            model.UserID = new Guid(CurrentSession.UserID);
            //end--------------------------------------------------
            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }
            if (!string.IsNullOrEmpty(CurrentSession.AssemblyId))
            {
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);

            }

            if (!string.IsNullOrEmpty(CurrentSession.SessionId))
            {
                model.SessionId = Convert.ToInt16(CurrentSession.SessionId);
            }


            model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDashBoardValue", model);

            if (!string.IsNullOrEmpty(CurrentSession.DeptID))
            {
                tPaperLaidV model1 = new tPaperLaidV();
                List<mDepartment> deptInfo = new List<mDepartment>();
                model1.DepartmentId = CurrentSession.DeptID;
                deptInfo = (List<mDepartment>)Helper.ExecuteService("Department", "GetDepartmentByIDs", model1);
                foreach (var item in deptInfo)
                {
                    if (item != null)
                    {
                        model.DeparmentNameByids += item.deptname + ", ";
                    }
                }
                if (model.DeparmentNameByids != null && model.DeparmentNameByids.LastIndexOf(",") > 0)
                {
                    model.DeparmentNameByids = model.DeparmentNameByids.Substring(0, model.DeparmentNameByids.Length - 2);
                }
                if (CurrentSession.OfficeLevel == "4" && CurrentSession.SubUserTypeID == "37")
                {
                    string StOfficeName = (string)Helper.ExecuteService("Office", "GetOfficeNameByOfficeIdNew", CurrentSession.OfficeId);
                    ViewBag.OfficeName = StOfficeName;
                }
            }
            return View(model);
        }

        public PartialViewResult GrievanceIndex(int pageId = 1, int pageSize = 20)
        {
            try
            {
                string AadharId = CurrentSession.AadharId;
                string DeptId = CurrentSession.DeptID;
                var User = (mUsers)Helper.ExecuteService("Grievance", "GetUserByAadharId", AadharId);
                int Status = SBL.DomainModel.Models.Enums.GrievanceStatus.Forwarded.GetHashCode();
                var GrievanceList = (List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllOfficeGrievances", new ForwardGrievance { DeptId = DeptId, OfficeId = User.OfficeId, status = Status });

                ViewBag.PageSize = pageSize;
                List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
                if (pageId == 1)
                {
                    pagedRecord = GrievanceList.Take(pageSize).ToList();
                }
                else
                {
                    int r = (pageId - 1) * pageSize;
                    pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
                }

                ViewBag.CurrentPage = pageId;
                ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);


                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();

                ViewBag.GrievanceCount = GrievanceList.Count;

                return PartialView("/Areas/Grievances/Views/Grievances/_PartialIndex.cshtml", pagedRecord);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Grievances List";
                return PartialView("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetBillByPaging(int pageId = 1, int pageSize = 20)
        {
            string AadharId = CurrentSession.AadharId;
            string DeptId = CurrentSession.DeptID;
            int Status = SBL.DomainModel.Models.Enums.GrievanceStatus.Forwarded.GetHashCode();
            var User = (mUsers)Helper.ExecuteService("Grievance", "GetUserByAadharId", AadharId);
            var GrievanceList = (List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllOfficeGrievances", new ForwardGrievance { DeptId = DeptId, OfficeId = User.OfficeId, status = Status });
            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = GrievanceList.Count;
            return PartialView("/Areas/Grievances/Views/Grievances/_GrievanceList.cshtml", pagedRecord);
        }

        public PartialViewResult PopulateGrievances(string GrvCode, int pageId = 1, int pageSize = 20)
        {
            tGrievanceReply model = new tGrievanceReply();
            model.GrvCode = GrvCode;
            ViewBag.Add = "Add";
            ViewBag.PageSize = pageSize;
            ViewBag.CurrentPage = pageId;
            return PartialView("/Areas/Grievances/Views/Grievances/_PopulateReply.cshtml", model);
        }

        [HttpPost, ValidateInput(false)]

        public PartialViewResult SaveGrievances(tGrievanceReply model, int pageId = 1, int pageSize = 20)
        {
            string AadharId = CurrentSession.AadharId;
            try
            {
                if (model.StatusId == 4)
                {
                    model.DisposedBy = "Officer";
                }
                model.OfficerCode = AadharId;
                model.ActionDate = DateTime.Now;
                Helper.ExecuteService("Grievance", "CreateGrievanceReply", model);

                tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = model.GrvCode, OfficerCode = AadharId });
                MemberGrievances.StatusId = model.StatusId;
                MemberGrievances.StatusModifiedDate = DateTime.Now;
                if (model.StatusId == 4)
                {
                    MemberGrievances.DiposedUserId = AadharId;
                    MemberGrievances.DisposedBy = "Officer";

                }
                Helper.ExecuteService("Grievance", "UpdateMemberGrievances", MemberGrievances);

                ViewBag.Msg = "Sucessfully posted reply";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";

            }
            catch (Exception)
            {
                ViewBag.Msg = "Ooops....! any error occured.";
                ViewBag.Class = "alert alert-error";
                ViewBag.Notification = "Error";
                throw;
            }


        
            string DeptId = CurrentSession.DeptID;
            var User = (mUsers)Helper.ExecuteService("Grievance", "GetUserByAadharId", AadharId);
            int Status = SBL.DomainModel.Models.Enums.GrievanceStatus.Forwarded.GetHashCode();
            var GrievanceList = (List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllOfficeGrievances", new ForwardGrievance { DeptId = DeptId, OfficeId = User.OfficeId, status = Status });


            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);

            return PartialView("/Areas/Grievances/Views/Grievances/_GrievanceList.cshtml", pagedRecord);
        }

        public PartialViewResult GetReplyList(string GrvCode)
        {


            string AadharId = CurrentSession.AadharId;
            tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesByGrvCode", new tMemberGrievances { GrvCode = GrvCode });
            ViewBag.GrvCode = GrvCode;
            ViewBag.UserName = MemberGrievances.UserName;
            ViewBag.MemberName = MemberGrievances.MemberName;
            var OfficerInfoList = (List<ForwardGrievance>)Helper.ExecuteService("Grievance", "GetFieldOfficerByGrvCode", new ForwardGrievance {  GrvCode = GrvCode });

            var GrievanceReplyList = (List<tGrievanceReply>)Helper.ExecuteService("Grievance", "GetAllGrievanceReply", GrvCode);
            GrievanceModel model = new GrievanceModel();
            model.ForwardGrievanceList = OfficerInfoList;
            model.GrievanceReplyList = GrievanceReplyList;
            model.tMemberGrievances = MemberGrievances;

            return PartialView("/Areas/Grievances/Views/Grievances/_ReplyList.cshtml", model);
        }

        public string GetReplyListData(List<tGrievanceReply> model, tMemberGrievances GrivencesObj)
        {

            StringBuilder ReplyList = new StringBuilder();

            if (model != null && model.Count() > 0)
            {
                ReplyList.Append("<div style='text-align:center;'><h2>Grievance Reply List</h2></div>");
                ReplyList.Append("<div style='width:1000px' >");
                ReplyList.Append(string.Format("<div style='background-color: #9abc32;width:274px ; border-color: #9abc32;margin: 1px 1px 0 0;  border-color: transparent !important;  border-width: 0;  color: #FFF;  padding: 4px;font-size: 20px;float:left;'>Grievance Code: {0}</div>", GrivencesObj.GrvCode));
                ReplyList.Append(string.Format("<div style='background-color: #9abc32;width:298px ; border-color: #9abc32;margin: 1px 1px 0 0;  border-color: transparent !important;  border-width: 0;  color: #FFF;  padding: 4px;font-size: 20px;float:right;'>Username: {0}</div>", GrivencesObj.UserName));
                ReplyList.Append(string.Format("<div style='background-color: #9abc32;width:400px ; border-color: #9abc32;margin: 1px 1px 0 0;  border-color: transparent !important;  border-width: 0;  color: #FFF;  padding: 4px;font-size: 20px;float:right;'>Member Name: {0}</div>", GrivencesObj.MemberName));
                ReplyList.Append("</div>");
                ReplyList.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                ReplyList.Append("<thead class='header' ><tr style='background-color: #428bca ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:30px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px;'>Sr. No.</th>");
                ReplyList.Append("<th style='width:150px;text-align:left;padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;font-size:18px; ' >Reply</th>");
                ReplyList.Append("</tr></thead>");


                ReplyList.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    ReplyList.Append("<tr>");
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;padding:5px;'>{0}</td>", count));
                    ReplyList.Append(string.Format("<td style='border: 1px dotted #808080;'>"));
                    ReplyList.Append(string.Format("<div style='padding:5px;font-family:Helvetica Neue',Helvetica,Arial,sans-serif;'>{0}</div>", obj.Reply));
                    ReplyList.Append(string.Format("<div style=' text-align:right; float:right;margin:5px;padding:5px;font-size: 14px;display: inline-block;  line-height: 1.15;  height: 20px;  background-color: #f89406 !important;  border-radius: 0;  text-shadow: none;  font-weight: normal;'><b>Reply Via:{0}</div>", obj.OfficerName));
                    ReplyList.Append(string.Format("</td>"));
                    ReplyList.Append("</tr>");
                }
                ReplyList.Append("</tbody></table></div> ");
            }
            else
            {
                ReplyList.Append("No Reply Found");
            }
            return ReplyList.ToString();
        }

        public string CreteGrievanceReplyPdf(string Report, string Url)
        {
            string Result = Report;
            try
            {
                PdfConverter pdfConverter = new PdfConverter();
                pdfConverter.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";

                pdfConverter.PdfDocumentOptions.ShowFooter = true;
                pdfConverter.PdfDocumentOptions.ShowHeader = true;

                // set the header height in points
                pdfConverter.PdfHeaderOptions.HeaderHeight = 75;
                pdfConverter.PdfFooterOptions.FooterHeight = 60;

                TextElement footerTextElement = new TextElement(0, 30, "&p; of &P;",
                        new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement.TextAlign = HorizontalTextAlign.Center;

                TextElement footerTextElement1 = new TextElement(10, 30, "eVidhan v1.0.0",
                       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement1.TextAlign = HorizontalTextAlign.Left;
                TextElement footerTextElement3 = new TextElement(10, 30, "Himachal Pradesh",
                       new System.Drawing.Font(new FontFamily("Times New Roman"), 10, GraphicsUnit.Point));
                footerTextElement3.TextAlign = HorizontalTextAlign.Right;

                pdfConverter.PdfFooterOptions.AddElement(footerTextElement);
                pdfConverter.PdfFooterOptions.AddElement(footerTextElement1);
                pdfConverter.PdfFooterOptions.AddElement(footerTextElement3);

                string imagesPath = System.IO.Path.Combine(Server.MapPath("~"), "Images");

                ImageElement imageElement1 = new ImageElement(250, 10, System.IO.Path.Combine(imagesPath, "logo.png"));
                // imageElement1.KeepAspectRatio = true;
                imageElement1.Opacity = 100;
                imageElement1.DestHeight = 97f;
                imageElement1.DestWidth = 71f;
                pdfConverter.PdfHeaderOptions.AddElement(imageElement1);
                ImageElement imageElement2 = new ImageElement(0, 0, System.IO.Path.Combine(imagesPath, "logo.png"));
                imageElement2.KeepAspectRatio = false;
                imageElement2.Opacity = 5;
                imageElement2.DestHeight = 284f;
                imageElement2.DestWidth = 388F;

                EvoPdf.Document pdfDocument = pdfConverter.GetPdfDocumentObjectFromHtmlString(Result);
                float stampWidth = float.Parse("400");
                float stampHeight = float.Parse("400");

                // Center the stamp at the top of PDF page
                float stampXLocation = (pdfDocument.Pages[0].ClientRectangle.Width - stampWidth) / 2;
                float stampYLocation = 150;

                RectangleF stampRectangle = new RectangleF(stampXLocation, stampYLocation, stampWidth, stampHeight);

                Template stampTemplate = pdfDocument.AddTemplate(stampRectangle);
                stampTemplate.AddElement(imageElement2);
                byte[] pdfBytes = null;

                try
                {
                    pdfBytes = pdfDocument.Save();
                }
                finally
                {
                    // close the Document to realease all the resources
                    pdfDocument.Close();
                }




                Guid FId = Guid.NewGuid();
                string fileName = FId + "_ReplyList.pdf";


                string directory = Server.MapPath(Url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + Url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();



                return fileName;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {


            }
        }

        public ActionResult DownloadGrievanceReplyPdf(string GrvCode)
        {
            string url = "/GrievanceReplyList/";
            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }


            string path = "";
            string FileName = string.Empty;
            string Result = string.Empty;

            string AadharId = CurrentSession.AadharId;
            tMemberGrievances MemberGrievances = (tMemberGrievances)Helper.ExecuteService("Grievance", "GetMemberGrievancesWithInfo", new tMemberGrievances { GrvCode = GrvCode, OfficerCode = AadharId });
            var GrievanceReplyList = (List<tGrievanceReply>)Helper.ExecuteService("Grievance", "GetAllGrievanceReply", GrvCode);


            DateTime GenerateDate = DateTime.Now;
            StringBuilder sb = new StringBuilder();
            Result = this.GetReplyListData(GrievanceReplyList, MemberGrievances);
            FileName = CreteGrievanceReplyPdf(Result, url);
            try
            {

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                path = Path.Combine(Server.MapPath("~" + url), FileName);

#pragma warning disable CS0219 // The variable 'contentType' is assigned but its value is never used
                string contentType = "application/octet-stream";
#pragma warning restore CS0219 // The variable 'contentType' is assigned but its value is never used
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

            byte[] bytes = System.IO.File.ReadAllBytes(path);
            return File(bytes, "application/pdf");
        }

        public ActionResult GetAllDistrict()
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("ConstituencyVS", "getAllDistrict", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        #region Menu
        public ActionResult GetSubMenu(int ModuleId, string ActIds, string Assigned_SubAccess)
        {
            mUserSubModules SM = new mUserSubModules();
            List<mUserSubModules> lstmUserSubModules = new List<mUserSubModules>();
            SM.ModuleId = ModuleId;
          
            
            SM = (mUserSubModules)Helper.ExecuteService("Notice", "GetSubMenuByMenuId", SM);
            //SM.MemberId = MemberId;
            SM.ActionIds = ActIds;
            //////////////////////////////////////This code For Manage SubMenu By User//////////////////////////////////////
            if (SM != null && SM.AccessList.Count > 0)
            {
                if (!string.IsNullOrEmpty(Assigned_SubAccess))
                {
                    string[] SubAccessArry = Assigned_SubAccess.Split(',');
                    foreach (var item in SM.AccessList)
                    {
                        if (item != null)
                        {
                            if (SubAccessArry.Contains(item.SubModuleId.ToString()))
                            {
                                lstmUserSubModules.Add(item);
                            }
                        }
                    }
                    if (lstmUserSubModules.Count > 0)
                    {
                        SM.AccessList = lstmUserSubModules;
                    }
                }
            }
            //////////////////////////////////////End This code For Manage SubMenu By User////////////////////////////////////
            return PartialView("_SubMenu", SM);
        }


        public PartialViewResult GetActionView(int SubMenuID, int AssemId, int SessId, int MainMenuID, int Count, int pageId = 1, int pageSize = 20)
        {

            if (MainMenuID == 51)
            {
                if (SubMenuID == 215)
                {
                    string AadharId = CurrentSession.AadharId;
                    string DeptId = CurrentSession.DeptID;
                    var User = (mUsers)Helper.ExecuteService("Grievance", "GetUserByAadharId", AadharId);
                    int Status = SBL.DomainModel.Models.Enums.GrievanceStatus.Forwarded.GetHashCode();
                    string SubDivisionID = CurrentSession.SubDivisionId;
                    var GrievanceList = (List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllOfficeGrievances", new ForwardGrievance { DeptId = DeptId, OfficeId = User.OfficeId, status = Status, SubDivisionId = SubDivisionID });

                    ViewBag.PageSize = pageSize;
                    List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
                    if (pageId == 1)
                    {
                        pagedRecord = GrievanceList.Take(pageSize).ToList();
                    }
                    else
                    {
                        int r = (pageId - 1) * pageSize;
                        pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
                    }
                    ViewBag.CurrentPage = pageId;
                    ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
                    if (TempData["Msg"] != null)
                        ViewBag.Msg = TempData["Msg"].ToString();
                    ViewBag.GrievanceCount = GrievanceList.Count;
                    return PartialView("/Areas/Grievances/Views/Grievances/_PartialIndex.cshtml", pagedRecord);
                }
            }
            else if (MainMenuID == 52)
            {
                if (SubMenuID == 216)
                {
                    return PartialView("/Areas/Grievances/Views/Grievances/_Works.cshtml");
                }
                if (SubMenuID == 1310)
                {
                    return PartialView("/Areas/Grievances/Views/Grievances/_ViewWorks.cshtml");
                }
                if (SubMenuID == 1313)
                {
                    //   return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_ActionView.cshtml",model);
                    return PartialView("/Areas/Grievances/Views/Grievances/_ViewMLADiary.cshtml");
                }
            }
            #region Offices/Institutes
            else if (MainMenuID == 60)
            {

                if (SubMenuID == 237)
                {
                    return PartialView("/Areas/PublishDocument/Views/PublishDocument/_DocumentList.cshtml");

                }
                if (SubMenuID == 235)
                {
                    return PartialView("/Areas/PISModule/Views/SanctionedPost/_TransferMetodPage.cshtml");

                }
                if (SubMenuID == 234)
                {
                    return PartialView("/Areas/PISModule/Views/PISDesignation/_DesignationMethodPage.cshtml");

                }
                if (SubMenuID == 236)
                {
                    return PartialView("/Areas/PISModule/Views/PISEmployee/_EmpMethodPage.cshtml");

                }
                if (SubMenuID == 1305)
                {
                    return PartialView("/Areas/PISModule/Views/PISDesignation/SancFillPostPage.cshtml");

                }
            }
            else if (MainMenuID == 69)
            {
                if (SubMenuID == 255)
                {
                  //  return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_pmisReports.cshtml");
                    return PartialView("_PMISReportSDM");
               //   Areas\Notices\Views\OnlineMemberQuestions

                }
            }

            #endregion
            #region Corspondance
            else if (MainMenuID == 64)
            {

                if (SubMenuID == 243)  //efile
                {
                    return PartialView("/Areas/eFile/Views/eFile/_MethodeFile.cshtml");

                }
                if (SubMenuID == 244) //receive
                {

                    return PartialView("/Areas/eFile/Views/eFile/_MethodReceive.cshtml");

                }
                if (SubMenuID == 245) //send
                {

                    return PartialView("/Areas/eFile/Views/eFile/_MethodSend.cshtml");

                }

            }
            else if (MainMenuID == 83)
            {
                if (SubMenuID == 1304)
                {

                    //   return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_ActionView.cshtml",model);
                    return PartialView("/Areas/Grievances/Views/Grievances/_MlaDiaryForConstituency.cshtml");
                }
                if (SubMenuID == 1313)
                {

                    //   return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_ActionView.cshtml",model);
                    return PartialView("/Areas/Grievances/Views/Grievances/_ViewMLADiary.cshtml");
                }
            }

            else if (MainMenuID == 88)
            {
               

                    //   return PartialView("/Areas/Notices/Views/OnlineMemberQuestions/_ActionView.cshtml",model);
                    return PartialView("/Areas/Grievances/Views/Grievances/_TenderList.cshtml");
               
            }
            #endregion
            return null;
        }
        #endregion

        #region
        public PartialViewResult MlaDiarySearchPage()
        {
            //DiaryModel model = new DiaryModel();
            //model.PaperEntryType = "MLA Diary";
            //model.SessionID = Convert.ToInt32(CurrentSession.SessionId);// SID ?? 0;
            //model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);// ?? 0;
            //model.ResultCount = 0;// ?? 0;
            ////model.RIds = ActionId;
            //CurrentSession.SPSubject = "";
            //CurrentSession.SPMinId = "";
            //CurrentSession.SPFDate = "";
            //CurrentSession.SPTDate = "";
            //CurrentSession.SPDNum = "";
           // return PartialView("/Areas/Grievances/Views/Grievances/_MlaDiaryForConstituency.cshtml");
            ViewBag.PageSize = 25;
            ViewBag.CurrentPage = 1;
            MlaDiary model1 = new MlaDiary();            
            //if (CurrentSession.MemberCode != "" )
            //{
            //    model1.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
            //}
            //else
            //{

            //   // model1.MlaCode = 0;
            //}
            model1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            model1.DocmentType = "1"; //Development
            model1.ActionCode = 0; //Development
            model1.UserDeptId = CurrentSession.DeptID;
            model1.UserofcId = CurrentSession.OfficeId;
            model1.CreatedBy = CurrentSession.UserID;
            if (CurrentSession.SubDivisionId != "")
            {
                model1.SubDivisionId = Convert.ToInt16(CurrentSession.SubDivisionId);
            }
            else
            {
                model1.SubDivisionId = 0;
            }
            if (CurrentSession.SubUserTypeID == "37" || CurrentSession.SubUserTypeID == "40")  //NODAL OFFICER AND SDM
            {
                if (model1.MlaCode == 0)
                {
                    model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryListForConUSer", model1);
                }
                else
                {
                    model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryListForConUSer", model1);
                }

                ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCountForConUser", model1) as string;
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCountForConUser", model1) as string;
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCountForConUser", model1) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCountForConUser", model1) as string;
                 //model1.MappedMemberList = (List<SBL.DomainModel.Models.Member.mMember>)Helper.ExecuteService("Diary", "Get_MappedMemberList", model1); ;
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", model1);
                var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);
                var officeListDepartment = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllOfficeListForDepartment", model1);
                var MlaList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetConsMlaList", model1);
                var DepartmentList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllDepartmentListForUser", model1);
                model1.OfficeDeptList = officeListDepartment;
                model1.DocumentTypelist = DocumentTypelist;
                model1.OfficeSubList = Officelist;
                model1.MappedMemberList = MlaList;
                model1.DepartmentList = DepartmentList;
                model1.ActionTypeList = ActionTypelist;
                model1.FinanCialYearList = GetFinancialYearList();
                model1.PaperEntryType = "Counsellor Diary";
                ViewBag.DocType = "1";
                ViewBag.ActionType = "0";
                ViewBag.PendencySince = "0";
                ////////////////////////////////////////////
                RecipientGroup Rmdl1 = new RecipientGroup();
                if (CurrentSession.SubDivisionId != "")
                {
                    Rmdl1.ConstituencyCodeForSubdivision = Convert.ToString(CurrentSession.SubDivisionId);
                }
                else
                {
                    Rmdl1.ConstituencyCodeForSubdivision = "0";
                }
                string[] str = new string[3];
                str[0] = CurrentSession.AadharId;
                str[1] = Convert.ToString(model1.SubDivisionId);
               // str[2] = CurrentSession.MemberCode;
               // string[] strOutput = (string[])Helper.ExecuteService("Diary", "GetDistrict_ByAAdharID", str);
                //if (strOutput != null)
                //{
                //    Rmdl1.DistId = strOutput[0].ToString();
                //}
                Rmdl1.DeptId = CurrentSession.DeptID;
                Rmdl1.OfficeId = Convert.ToInt16(CurrentSession.OfficeId);
                Rmdl1.AssId = Convert.ToInt32(CurrentSession.AssemblyId);
                Rmdl1.OfficeList_search = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOfficemappedForSearchDepartment", Rmdl1);
                model1.OfficeList_search = Rmdl1.OfficeList_search;
            }
          
           
            return PartialView("_MlaDiaryListForConstituency", model1);
           // return PartialView("_MLADiaryDetails", model1);
        }

        public ActionResult NewMlaEntryForm()
        {
            MlaDiary model = new MlaDiary();
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
               
                model.UserofcId = CurrentSession.OfficeId;
              //  var Districtlist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetConsMlaList", model);
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);

                var a = CurrentSession.SPMinId;
              //  model.memMinList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;  //GetDepartmentByMinistery
              //  model.DeptList = (List<mDepartment>)Helper.ExecuteService("ContactGroups", "GetAllDepartment", null);
              //  model.Districtist = (List<SBL.DomainModel.Models.District.DistrictModel>)Helper.ExecuteService("District", "GetAllDistrict", null);
                //model.SubDivisionList = (List<SBL.DomainModel.Models.SubDivision.mSubDivision>)Helper.ExecuteService("Diary", "Get_Subdivision_ByDistrictndCon", null);
                // model.DeptId = CurrentSession.DeptID;
                //model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOffice", null);
                model.AdhrId = CurrentSession.AadharId;
                model.UserDeptId = CurrentSession.DeptID;
             //   model.UserofcId = CurrentSession.OfficeId;
                model.OfficeId = Convert.ToInt16(CurrentSession.OfficeId);
                var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model);
                var officeListDepartment = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllOfficeListForDepartment", model);

             


                model.OfficeDeptList = officeListDepartment;

                model.OfficeSubList = Officelist;


                if (CurrentSession.SubDivisionId != "")
                {
                    model.SubDivisionId = Convert.ToInt16(CurrentSession.SubDivisionId);                    
                }
                else
                {
                    model.SubDivisionId = 0;                  
                }
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
               
                model.MappedMemberList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetConsMlaList", model);
              //  model.DistrictList = Districtlist; 
                model.DocumentTypelist = DocumentTypelist;
                model.ActionTypeList = ActionTypelist;
                model.FinanCialYearList = GetFinancialYearList();                               
                model.PaperEntryType = "NewEntry";
                model.BtnCaption = "Save";
                string[] str = new string[3];
                str[0] = CurrentSession.AadharId;
                str[1] = Convert.ToString(model.SubDivisionId);
                //str[2] = CurrentSession.MemberCode;
                //string[] strOutput = (string[])Helper.ExecuteService("Diary", "GetDistrict_ByAAdharID", str);
                //if (strOutput != null)
                //{
                //    model.distcd = Convert.ToInt16(strOutput[0]);
                 
                //}
                return PartialView("_MlaDiaryEntryForConstit", model);
          
        }

        public ActionResult GetList_MlaForConstituency()
        {
          
                ViewBag.PageSize = 25;
                ViewBag.CurrentPage = 1;
                MlaDiary model1 = new MlaDiary();
                // model.AssemblyId = CurrentSession.AssemblyId;
                // model.SessionId = CurrentSession.SessionId;
                //var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                //var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                //////ViewBag.sessiondates = new SelectList(SessionDate, "SessionDates", "SessionDates", null);
                //model1.DocumentTypelist = DocumentTypelist;
                //model1.ActionTypeList = ActionTypelist;
                //model1.BtnCaption = "Save";
                //model1.PaperEntryType = "NewEntry";
                if (CurrentSession.MemberCode != "")
                {
                    model1.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
                }
                else
                {

                    model1.MlaCode = 0;
                }
                model1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
                model1.DocmentType = "1"; //Development
                model1.ActionCode = 0; //Development
                model1.UserDeptId = CurrentSession.DeptID;
                model1.UserofcId = CurrentSession.OfficeId;
                model1.CreatedBy = CurrentSession.UserID;
                if (CurrentSession.SubDivisionId != "")
                {
                    model1.SubDivisionId = Convert.ToInt16(CurrentSession.SubDivisionId);
                }
                else
                {
                    model1.SubDivisionId = 0;
                }
                if (CurrentSession.SubUserTypeID == "37" || CurrentSession.SubUserTypeID == "40")
                {
                    model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryListForConUSer", model1);

                }
              //  model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryList", model1);

                // search
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCountForConUser", model1) as string;
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCountForConUser", model1) as string;  //Priyanka
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCountForConUser", model1) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCountForConUser", model1) as string;
                model1.DocumentTypelist = DocumentTypelist;
                model1.ActionTypeList = ActionTypelist;
                model1.FinanCialYearList = GetFinancialYearList();
                model1.PaperEntryType = "Counsellor Diary";
                ViewBag.DocType = "1";
                ViewBag.ActionType = "0";
                ViewBag.PendencySince = "0";
                return PartialView("_MlaDiaryListForConstituency", model1);
            }

        public ActionResult SearchMLADiary_DataByTypeForCon(string PaperType, string DocType, int ActionType, int PendencySince,string OfficeId, string FinancialYear,int? MemberCode)
        {
            ViewBag.PageSize = 25;
            ViewBag.CurrentPage = 1;
            MlaDiary Dmdl = new MlaDiary();

            if (MemberCode != 0 && MemberCode != null)
            {
                //Dmdl.MemberCode = MemberCode;
                Dmdl.MlaCode = MemberCode;
            }

            else
            {
                Dmdl.MlaCode = null;
            }
         

            Dmdl.PaperEntryType = PaperType;
            Dmdl.DocmentType = DocType;
            Dmdl.ActionCode = ActionType;
            Dmdl.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            Dmdl.PendencySince = PendencySince;
          
            Dmdl.FinancialYear = FinancialYear;
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("in");
            Dmdl.UserDeptId = CurrentSession.DeptID;
            Dmdl.UserofcId = OfficeId;
            Dmdl.CreatedBy = OfficeId;
            if (CurrentSession.SubDivisionId != "")
            {
                Dmdl.SubDivisionId = Convert.ToInt16(CurrentSession.SubDivisionId);
                Dmdl.MlaCode = 0;
            }
            else
            {
                Dmdl.SubDivisionId = 0;
              //  Dmdl.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
            }
            if (Dmdl.DocmentType != "3")
            {
                if (MemberCode != 0 && MemberCode != null)
                {
                    Dmdl.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchDataByTypeForConUser", Dmdl);
                }
                else
                {
                    Dmdl.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryListForConUSer", Dmdl);
                }
            }

            else if (Dmdl.DocmentType == "3")
            {
                Dmdl.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchData_ForDoctype3ForConUSer", Dmdl);
                //ViewBag.TotalSanctionMoney = Helper.ExecuteService("Diary", "GetMlaDiary_AmountCount_ForDoctype3", Dmdl) as string;
                ViewBag.TotalDiaryAmountCount = Helper.ExecuteService("Diary", "Get_TotalDiaryAmountCountForConUser", Dmdl) as string;
                ViewBag.pendingDiaryAmountCount = Helper.ExecuteService("Diary", "pendingDiaryAmountCountForConUser", Dmdl) as string;
                ViewBag.forwardActionPendingAmountCount = Helper.ExecuteService("Diary", "Get_forwardActionPendingAmountCountForConUser", Dmdl) as string;
                ViewBag.forwardActionDoneAmountCount = Helper.ExecuteService("Diary", "Get_forwardActionDoneAmountCountForUser", Dmdl) as string;

            }
            ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCountForConUser", Dmdl) as string;
            if (Dmdl.ActionCode == 0)
            {
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCountForConUser", Dmdl) as string;
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCountForConUser", Dmdl) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCountForConUser", Dmdl) as string;
            }
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("Out");
            Dmdl.UserofcId = CurrentSession.OfficeId;
            var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
            var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
            var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", Dmdl);
            var officeListDepartment = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllOfficeListForDepartment", Dmdl);
            var MlaList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetConsMlaList", Dmdl);
            var DepartmentList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllDepartmentListForUser", Dmdl);
            Dmdl.OfficeDeptList = officeListDepartment;
           // model1.DocumentTypelist = DocumentTypelist;
            Dmdl.OfficeSubList = Officelist;
            Dmdl.MappedMemberList = MlaList;
            
            Dmdl.DepartmentList = DepartmentList;
            Dmdl.DocumentTypelist = DocumentTypelist;
            Dmdl.ActionTypeList = ActionTypelist;
            Dmdl.PaperEntryType = PaperType;
            Dmdl.PendencySince = PendencySince;
            ViewBag.DocType = DocType.ToString();
            Dmdl.FinanCialYearList = GetFinancialYearList();
            ViewBag.ActionType = ActionType.ToString();
            ViewBag.PendencySince = PendencySince;

            return PartialView("_MlaDiaryListForConstituency", Dmdl);
        }
       


        //public ActionResult SaveUpdateMlaDiary(MlaDiary model, HttpPostedFileBase file)
        //{
        //    if (model.officenameCC != null && model.officenameCC != "")
        //    {
        //        string s= model.officenameCC;
        //        string OfficeInCc = s.TrimEnd(',');
        //        model.OfficeId_CC = OfficeInCc;
        //    }
        //    model.MlaCode = Convert.ToInt32(model.MemberCode);
         
        //    if (CurrentSession.AssemblyId == "" || CurrentSession.AssemblyId == null)
        //    {
        //        CurrentSession.AssemblyId = "0";
        //    }
        //    model.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);

        //    string res = "0";
       
        //    DiaryModel dobj = new DiaryModel();
        //    dobj.MemberId = Convert.ToInt32(model.MemberCode);
        //    //dobj.MemberId = model.MlaCode;
        //    dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
        //    dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
        //    model.ConstituencyCode = dobj.ConstituencyCode;

          
        //   if (model.MultipleOfcId == null) 
        //    { model.MultipleOfcId = ""; }
           
        //    if (model.MultipleOfcId != "")
        //    {
        //        if (model.MultipleOfcId.Contains(','))
        //        {
        //            //  List<long> ID = Mdl.MultipleOfcId.Split(',').Select(Int64.Parse).ToList();
        //            string[] ids = model.MultipleOfcId.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);   //Mdl.MultipleOfcId.Split(',');
        //            for (int i = 0; i <= ids.Length - 1; i++)
        //            {
        //                // string ofcname = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", Convert.ToInt16(ids[i]));
        //                string Department = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", Convert.ToInt16(ids[i]));
        //                model.DeptId += Department + ",";
        //            }
        //        }
        //    }

        //    else
        //    {
        //        if (model.OfficeId == null || model.OfficeId == 0)
        //        {
        //            model.OfficeId = Convert.ToInt16(CurrentSession.OfficeId); // only for constituency user
        //        }
        //        string Departmentid = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", model.OfficeId);
        //        model.DeptId = Departmentid;
        //    }

        //    if (model.BtnCaption == "Save")
        //    {
        //        model.RecordNumber = Convert.ToInt32(GetRecordNumber_mlaDiary(model));
        //        DateTime Date2 = DateTime.Now;
        //        string span = Convert.ToString(DateTime.Now.ToFileTime());
        //        model.DiaryRefId = model.RecordNumber + "_" + Convert.ToInt32(model.MemberCode) + "_" + model.DocmentType + "_" + span;
        //    }
        //    if (model.BtnCaption == "Update")
        //    {
        //        model.ActionOfficeId = Convert.ToInt16(CurrentSession.OfficeId);
        //    }
        //    if (model.ForwardDateForUse != null && model.ForwardDateForUse != "")
        //    {
        //        string Date = model.ForwardDateForUse;
        //        Int16 dd = Convert.ToInt16(model.ForwardDateForUse.Substring(0, 2));
        //        Int16 mm = Convert.ToInt16(model.ForwardDateForUse.Substring(3, 2));
        //        Int16 yyyy = Convert.ToInt16(model.ForwardDateForUse.Substring(6, 4));
        //        // DateTime dtDate = new DateTime(2018, 2, 12);
        //        DateTime dtDate = new DateTime(yyyy, mm, dd);
        //        model.ForwardDate = dtDate;
        //    }
        //    if (model.EnclosureDateForUse != null && model.EnclosureDateForUse != "")
        //    {
        //        string Date = model.ForwardDateForUse;
        //        Int16 dd = Convert.ToInt16(model.EnclosureDateForUse.Substring(0, 2));
        //        Int16 mm = Convert.ToInt16(model.EnclosureDateForUse.Substring(3, 2));
        //        Int16 yyyy = Convert.ToInt16(model.EnclosureDateForUse.Substring(6, 4));
        //        // DateTime dtDate = new DateTime(2018, 2, 12);
        //        DateTime dtDate = new DateTime(yyyy, mm, dd);
        //        model.EnclosureDate = dtDate;
        //    }
        //    if (model.ActiontakenDateForUse != null && model.ActiontakenDateForUse != "")
        //    {
        //        string Date = model.ForwardDateForUse;
        //        Int16 dd = Convert.ToInt16(model.ActiontakenDateForUse.Substring(0, 2));
        //        Int16 mm = Convert.ToInt16(model.ActiontakenDateForUse.Substring(3, 2));
        //        Int16 yyyy = Convert.ToInt16(model.ActiontakenDateForUse.Substring(6, 4));
        //        // DateTime dtDate = new DateTime(2018, 2, 12);
        //        TimeSpan d = DateTime.Now.TimeOfDay;
        //       // TimeSpan time;
        //       // time = TimeSpan.Parse(d);
        //        DateTime dtDate = new DateTime(yyyy, mm, dd);
        //        dtDate += d;
        //       // IFormatProvider culture = new CultureInfo("en-US", true);
        //       // DateTime dateVal = DateTime.ParseExact(Date, "dd/MM/yyyy HH:mm:ss.fff", culture);

        //        model.ActiontakenDate = dtDate;
        //    }
        //    model.CreatedBy = CurrentSession.UserID;

        //    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwardin");
        //    //  model.ForwardDate = DateTime.ParseExact(model.ForwardDateForUse, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
        //    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwarddatget");
        //    var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
        //    model.ForwardedFile = null;
        //    //if (model.IsForwardFile == "Y")
        //    //{
        //    //    model.ForwardedFile = model.ForwardedFile;
        //    //}
        //    //  else
        //    // {

        //    // Save Forward File

        //    //string urlforward = "/MlaDiary/ForwardFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
        //    //string directoryf = FileSettings.SettingValue + urlforward;

        //    //if (!System.IO.Directory.Exists(directoryf))
        //    //{
        //    //    System.IO.Directory.CreateDirectory(directoryf);
        //    //}
        //    //string tempurlforwrd = "~/MlaDiary/TempFile";
        //    //string Tempdirectoryforwrd = Server.MapPath(tempurlforwrd);


        //    //if (Directory.Exists(directoryf) && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
        //    //{

        //    //    string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlforwrd));
        //    //    if (savedFileName.Length > 0 && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
        //    //    {
        //    //        string SourceFile = savedFileName[0];
        //    //        foreach (string page in savedFileName)
        //    //        {
        //    //            // Guid FileName = Guid.NewGuid();
        //    //            string name = Path.GetFileName(page);
        //    //            string ext = Path.GetExtension(SourceFile);
        //    //            string path = System.IO.Path.Combine(directoryf, model.RecordNumber.ToString() + ext);

        //    //            if (!string.IsNullOrEmpty(model.RecordNumber.ToString()))
        //    //            {

        //    //                System.IO.File.Copy(SourceFile, path, true);

        //    //                model.ForwardedFile = urlforward + model.RecordNumber.ToString() + ext;
        //    //            }

        //    //        }


        //    //    }
        //    //    else
        //    //    {
        //    //        model.ForwardedFile = null;
        //    //        TempData["Msg"] = "Please select Forward File";

        //    //    }
        //    //}
        //    //     else
        //    //    {
        //    //        model.ForwardedFile = null;
        //    //        TempData["Msg"] = "Please select Forward File";

        //    //    }


        //    //if (Directory.Exists(Tempdirectoryforwrd))  //tempurlforwrd
        //    //{
        //    //    string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlforwrd));
        //    //    foreach (string filePath in filePaths)
        //    //    {
        //    //        System.IO.File.Delete(filePath);
        //    //    }
        //    //}

        //    // }

        //    //upload action file 
        //    if (model.IsFile == "Y")
        //    {
        //        model.ActiontakenFile = model.ActiontakenFile;
        //    }
        //    else
        //    {
        //        //string urlA = "/MlaDiary/ActionFile";
        //        string urlA = "/MlaDiary/ActionFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
        //        string directoryA = FileSettings.SettingValue + urlA;

        //        if (!System.IO.Directory.Exists(directoryA))
        //        {
        //            System.IO.Directory.CreateDirectory(directoryA);
        //        }
        //        // int fileID = GetFileRandomNo();

        //        string tempurlA1 = "~/MlaDiary/ActionTempFile";
        //        string tempdirectoryA1 = Server.MapPath(tempurlA1);
        //        //Save ActionTaken File
        //        if (!System.IO.Directory.Exists(tempdirectoryA1))
        //        {
        //            System.IO.Directory.CreateDirectory(tempdirectoryA1);
        //        }
        //        if (Directory.Exists(directoryA))// && (model.ActiontakenDate != null ))
        //        {
        //            string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlA1));
        //            if (savedFileName.Length > 0)// && (model.ActiontakenDate != null ))   //&& model.ActiontakenDate != null && model.ActiontakenDate != ""
        //            {
        //                string SourceFile = savedFileName[0];
        //                foreach (string page in savedFileName)
        //                {

        //                    // string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));

        //                    string ext = Path.GetExtension(SourceFile);
        //                    string name = Path.GetFileName(page);
        //                   // string path1 = System.IO.Path.Combine(directoryA, model.RecordNumber.ToString() + ext);
        //                    var fname = Guid.NewGuid().ToString();
        //                    string path1 = System.IO.Path.Combine(directoryA, fname + ext);
        //                    if (!string.IsNullOrEmpty(name))
        //                    {
        //                        //  System.IO.File.Delete(System.IO.Path.Combine(directoryA, model.ActiontakenFile));
        //                        System.IO.File.Copy(SourceFile, path1, true);
        //                        model.ActiontakenFile = urlA + fname + ext;

        //                    }
        //                }


        //            }
        //            else
        //            {
        //                model.ActiontakenFile = null;
        //                //   return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
        //                //  TempData["Msg"] = "Please select Action Taken file";
        //                // return RedirectToAction("Index");
        //            }
        //        }
        //        else
        //        {
        //            model.ActiontakenFile = null;
        //        }
        //        if (Directory.Exists(tempdirectoryA1))   //tempurlA1
        //        {
        //            string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlA1));
        //            foreach (string filePath in filePaths)
        //            {
        //                System.IO.File.Delete(filePath);
        //            }
        //        }

        //    }
        //    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("1");
        //    //upload enclosure file 
        //    if (model.IsEnclosureFile == "Y")
        //    {
        //        model.EnclosureFile = model.EnclosureFile;
        //    }
        //    else
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("2");
        //        string urlEnclosure = "/MlaDiary/EnclosureFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
        //        //string urlEnclosure = "/MlaDiary/EnclosureFile";
        //        string directoryEnclosure = FileSettings.SettingValue + urlEnclosure;

        //        if (!System.IO.Directory.Exists(directoryEnclosure))
        //        {
        //            System.IO.Directory.CreateDirectory(directoryEnclosure);
        //        }
        //        // int fileIDEnclosure = GetFileRandomNo();

        //        string TempurlEnclosure1 = "~/MlaDiary/TempEnclosureFile";
        //        string directoryEnclosure1 = Server.MapPath(TempurlEnclosure1);

        //        if (!System.IO.Directory.Exists(directoryEnclosure1))
        //        {
        //            System.IO.Directory.CreateDirectory(directoryEnclosure1);
        //        }

        //        if (Directory.Exists(directoryEnclosure))
        //        {
        //            string[] savedFileName = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
        //            if (savedFileName.Length > 0 && (model.EnclosureDate != null))
        //            {
        //                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("3");
        //                string SourceFile = savedFileName[0];
        //                foreach (string page in savedFileName)
        //                {
        //                    // Guid FileName = Guid.NewGuid();
        //                    string ext = Path.GetExtension(SourceFile);
        //                    string name = Path.GetFileName(page);
        //                    string path1 = System.IO.Path.Combine(directoryEnclosure, model.RecordNumber.ToString() + ext);
        //                    if (!string.IsNullOrEmpty(name))
        //                    {
        //                        //  System.IO.File.Delete(System.IO.Path.Combine(directoryEnclosure, model.EnclosureFile));
        //                        System.IO.File.Copy(SourceFile, path1, true);
        //                        model.EnclosureFile = urlEnclosure + model.RecordNumber.ToString() + ext;
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                model.EnclosureFile = null;
        //                //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
        //            }
        //        }
        //        else
        //        {
        //            model.EnclosureFile = null;
        //            //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
        //        }
        //        if (Directory.Exists(directoryEnclosure1))  //TempurlEnclosure1
        //        {

        //            string[] filePaths = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
        //            foreach (string filePath in filePaths)
        //            {
        //                System.IO.File.Delete(filePath);
        //            }
        //        }
        //    }

        //    //Delete the temp files if exists

        //    SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("4");
        //    res = (string)Helper.ExecuteService("Diary", "SaveMlaDiary", model);

        //    if (res != "0" && res != "-1")
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
        //        return Json(res, JsonRequestBehavior.AllowGet);
        //        //  return RedirectToAction("Index");
        //    }
        //    else if (res == "0")
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
        //        return Json("U", JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("6");
        //        return Json("E", JsonRequestBehavior.AllowGet);
        //    }


        //    //if (res != 0 &&  res != -1 ||(model.BtnCaption=="Update"))
        //    //{
        //    //    if (model.BtnCaption == "Update")
        //    //    {
        //    //    }
        //    //    else
        //    //    {
        //    //        model.RecordNumber = res;
        //    //    }

        //    //    int reslt = (int)Helper.ExecuteService("Diary", "SaveMlaDiary", model);
        //    //    if (reslt == 0)
        //    //    {
        //    //        return RedirectToAction("Index");
        //    //    }
        //    //    else                
        //    //    {
        //    //        return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
        //    //}


        //    // return RedirectToAction("Index");

        //}


        public ActionResult SaveUpdateMlaDiary(MlaDiary model, HttpPostedFileBase file)
        {
            if (model.officenameCC != null && model.officenameCC != "")
            {
                string s = model.officenameCC;
                string OfficeInCc = s.TrimEnd(',');
                model.OfficeId_CC = OfficeInCc;
            }
            model.MlaCode = Convert.ToInt32(model.MemberCode);

            if (CurrentSession.AssemblyId == "" || CurrentSession.AssemblyId == null)
            {
                CurrentSession.AssemblyId = "0";
            }
            model.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);

            string res = "0";

            DiaryModel dobj = new DiaryModel();
            dobj.MemberId = Convert.ToInt32(model.MemberCode);
            //dobj.MemberId = model.MlaCode;
            dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
            model.ConstituencyCode = dobj.ConstituencyCode;


            if (model.MultipleOfcId == null)
            { model.MultipleOfcId = ""; }

            if (model.MultipleOfcId != "")
            {
                if (model.MultipleOfcId.Contains(','))
                {
                    //  List<long> ID = Mdl.MultipleOfcId.Split(',').Select(Int64.Parse).ToList();
                    string[] ids = model.MultipleOfcId.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);   //Mdl.MultipleOfcId.Split(',');
                    for (int i = 0; i <= ids.Length - 1; i++)
                    {
                        // string ofcname = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", Convert.ToInt16(ids[i]));
                        string Department = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", Convert.ToInt16(ids[i]));
                        model.DeptId += Department + ",";
                    }
                }
            }

            else
            {
                if (model.OfficeId == null || model.OfficeId == 0)
                {
                    model.OfficeId = Convert.ToInt16(CurrentSession.OfficeId); // only for constituency user
                }
                string Departmentid = (string)Helper.ExecuteService("Diary", "GetDeptID_FromOfficeId", model.OfficeId);
                model.DeptId = Departmentid;
            }

            if (model.BtnCaption == "Save")
            {
                model.RecordNumber = Convert.ToInt32(GetRecordNumber_mlaDiary(model));
                DateTime Date2 = DateTime.Now;
                string span = Convert.ToString(DateTime.Now.ToFileTime());
                model.DiaryRefId = model.RecordNumber + "_" + Convert.ToInt32(model.MemberCode) + "_" + model.DocmentType + "_" + span;
            }
            if (model.BtnCaption == "Update")
            {
                model.ActionOfficeId = Convert.ToInt16(CurrentSession.OfficeId);
            }
            if (model.ForwardDateForUse != null && model.ForwardDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.ForwardDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.ForwardDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.ForwardDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                model.ForwardDate = dtDate;
            }
            if (model.EnclosureDateForUse != null && model.EnclosureDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.EnclosureDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.EnclosureDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.EnclosureDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                model.EnclosureDate = dtDate;
            }
            if (model.ActiontakenDateForUse != null && model.ActiontakenDateForUse != "")
            {
                string Date = model.ForwardDateForUse;
                Int16 dd = Convert.ToInt16(model.ActiontakenDateForUse.Substring(0, 2));
                Int16 mm = Convert.ToInt16(model.ActiontakenDateForUse.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(model.ActiontakenDateForUse.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                TimeSpan d = DateTime.Now.TimeOfDay;
                // TimeSpan time;
                // time = TimeSpan.Parse(d);
                DateTime dtDate = new DateTime(yyyy, mm, dd);
                dtDate += d;
                // IFormatProvider culture = new CultureInfo("en-US", true);
                // DateTime dateVal = DateTime.ParseExact(Date, "dd/MM/yyyy HH:mm:ss.fff", culture);

                model.ActiontakenDate = dtDate;
            }
            model.CreatedBy = CurrentSession.UserID;

            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwardin");
            //  model.ForwardDate = DateTime.ParseExact(model.ForwardDateForUse, "dd/MM/yyyy", CultureInfo.GetCultureInfo("hi-IN").DateTimeFormat);
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("forwarddatget");
            var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            model.ForwardedFile = null;
            //if (model.IsForwardFile == "Y")
            //{
            //    model.ForwardedFile = model.ForwardedFile;
            //}
            //  else
            // {

            // Save Forward File

            //string urlforward = "/MlaDiary/ForwardFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
            //string directoryf = FileSettings.SettingValue + urlforward;

            //if (!System.IO.Directory.Exists(directoryf))
            //{
            //    System.IO.Directory.CreateDirectory(directoryf);
            //}
            //string tempurlforwrd = "~/MlaDiary/TempFile";
            //string Tempdirectoryforwrd = Server.MapPath(tempurlforwrd);


            //if (Directory.Exists(directoryf) && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
            //{

            //    string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlforwrd));
            //    if (savedFileName.Length > 0 && (model.ForwardDateForUse != null || model.ForwardDateForUse != ""))
            //    {
            //        string SourceFile = savedFileName[0];
            //        foreach (string page in savedFileName)
            //        {
            //            // Guid FileName = Guid.NewGuid();
            //            string name = Path.GetFileName(page);
            //            string ext = Path.GetExtension(SourceFile);
            //            string path = System.IO.Path.Combine(directoryf, model.RecordNumber.ToString() + ext);

            //            if (!string.IsNullOrEmpty(model.RecordNumber.ToString()))
            //            {

            //                System.IO.File.Copy(SourceFile, path, true);

            //                model.ForwardedFile = urlforward + model.RecordNumber.ToString() + ext;
            //            }

            //        }


            //    }
            //    else
            //    {
            //        model.ForwardedFile = null;
            //        TempData["Msg"] = "Please select Forward File";

            //    }
            //}
            //     else
            //    {
            //        model.ForwardedFile = null;
            //        TempData["Msg"] = "Please select Forward File";

            //    }


            //if (Directory.Exists(Tempdirectoryforwrd))  //tempurlforwrd
            //{
            //    string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlforwrd));
            //    foreach (string filePath in filePaths)
            //    {
            //        System.IO.File.Delete(filePath);
            //    }
            //}

            // }

            //upload action file 
            if (model.IsFile == "Y")
            {
                model.ActiontakenFile = model.ActiontakenFile;
            }
            else
            {
                //string urlA = "/MlaDiary/ActionFile";
                string urlA = "/MlaDiary/ActionFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
                string directoryA = FileSettings.SettingValue + urlA;

                if (!System.IO.Directory.Exists(directoryA))
                {
                    System.IO.Directory.CreateDirectory(directoryA);
                }
                // int fileID = GetFileRandomNo();

                string tempurlA1 = "~/MlaDiary/ActionTempFile";
                string tempdirectoryA1 = Server.MapPath(tempurlA1);
                //Save ActionTaken File
                if (!System.IO.Directory.Exists(tempdirectoryA1))
                {
                    System.IO.Directory.CreateDirectory(tempdirectoryA1);
                }
                if (Directory.Exists(directoryA))// && (model.ActiontakenDate != null ))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(tempurlA1));
                    if (savedFileName.Length > 0)// && (model.ActiontakenDate != null ))   //&& model.ActiontakenDate != null && model.ActiontakenDate != ""
                    {
                        string SourceFile = savedFileName[0];
                        foreach (string page in savedFileName)
                        {

                            // string path1 = System.IO.Path.Combine(directory, FileName1 + "_" + nametwo.Replace(" ", ""));

                            string ext = Path.GetExtension(SourceFile);
                            string name = Path.GetFileName(page);
                            // string path1 = System.IO.Path.Combine(directoryA, model.RecordNumber.ToString() + ext);
                            var fname = Guid.NewGuid().ToString();
                            string path1 = System.IO.Path.Combine(directoryA, fname + ext);
                            if (!string.IsNullOrEmpty(name))
                            {
                                //  System.IO.File.Delete(System.IO.Path.Combine(directoryA, model.ActiontakenFile));
                                System.IO.File.Copy(SourceFile, path1, true);
                                model.ActiontakenFile = urlA + fname + ext;

                            }
                        }


                    }
                    else
                    {
                        model.ActiontakenFile = null;
                        //   return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                        //  TempData["Msg"] = "Please select Action Taken file";
                        // return RedirectToAction("Index");
                    }
                }
                else
                {
                    model.ActiontakenFile = null;
                }
                if (Directory.Exists(tempdirectoryA1))   //tempurlA1
                {
                    string[] filePaths = Directory.GetFiles(Server.MapPath(tempurlA1));
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }

            }
            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("1");
            //upload enclosure file 
            if (model.IsEnclosureFile == "Y")
            {
                model.EnclosureFile = model.EnclosureFile;
            }
            else
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("2");
                string urlEnclosure = "/MlaDiary/EnclosureFile/" + CurrentSession.AssemblyId + "/" + CurrentSession.MemberCode + "/" + model.DocmentType + "/";
                //string urlEnclosure = "/MlaDiary/EnclosureFile";
                string directoryEnclosure = FileSettings.SettingValue + urlEnclosure;

                if (!System.IO.Directory.Exists(directoryEnclosure))
                {
                    System.IO.Directory.CreateDirectory(directoryEnclosure);
                }
                // int fileIDEnclosure = GetFileRandomNo();

                string TempurlEnclosure1 = "~/MlaDiary/TempEnclosureFile";
                string directoryEnclosure1 = Server.MapPath(TempurlEnclosure1);

                if (!System.IO.Directory.Exists(directoryEnclosure1))
                {
                    System.IO.Directory.CreateDirectory(directoryEnclosure1);
                }

                if (Directory.Exists(directoryEnclosure))
                {
                    string[] savedFileName = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
                    if (savedFileName.Length > 0 && (model.EnclosureDate != null))
                    {
                        SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("3");
                        string SourceFileold = savedFileName[0];
                        string SourceFile = System.IO.Path.Combine(directoryEnclosure1, model.EnclosureFile);
                        foreach (string page in savedFileName)
                        {
                            // Guid FileName = Guid.NewGuid();
                            string ext = Path.GetExtension(SourceFile);
                            string name = Path.GetFileName(page);

                            if (name == model.EnclosureFile)
                            {

                                var NFileName = Guid.NewGuid().ToString() + model.RecordNumber.ToString() + ext;
                                string path1 = System.IO.Path.Combine(directoryEnclosure, NFileName);
                                if (!string.IsNullOrEmpty(name))
                                {
                                    //  System.IO.File.Delete(System.IO.Path.Combine(directoryEnclosure, model.EnclosureFile));
                                    System.IO.File.Copy(SourceFile, path1, true);
                                    model.EnclosureFile = urlEnclosure + NFileName;
                                    System.IO.File.Delete(SourceFile);
                                }
                            }
                        }
                    }
                    else
                    {
                        model.EnclosureFile = null;
                        //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    model.EnclosureFile = null;
                    //  return Json("Please select Action Taken file", JsonRequestBehavior.AllowGet);
                }
                //    if (Directory.Exists(directoryEnclosure1))  //TempurlEnclosure1
                //    {

                //        string[] filePaths = Directory.GetFiles(Server.MapPath(TempurlEnclosure1));
                //        foreach (string filePath in filePaths)
                //        {
                //            System.IO.File.Delete(filePath);
                //        }
                //    }
            }

            //Delete the temp files if exists

            SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("4");
            res = (string)Helper.ExecuteService("Diary", "SaveMlaDiary", model);

            if (res != "0" && res != "-1")
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
                return Json(res, JsonRequestBehavior.AllowGet);
                //  return RedirectToAction("Index");
            }
            else if (res == "0")
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("5");
                return Json("U", JsonRequestBehavior.AllowGet);
            }
            else
            {
                SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("6");
                return Json("E", JsonRequestBehavior.AllowGet);
            }


            //if (res != 0 &&  res != -1 ||(model.BtnCaption=="Update"))
            //{
            //    if (model.BtnCaption == "Update")
            //    {
            //    }
            //    else
            //    {
            //        model.RecordNumber = res;
            //    }

            //    int reslt = (int)Helper.ExecuteService("Diary", "SaveMlaDiary", model);
            //    if (reslt == 0)
            //    {
            //        return RedirectToAction("Index");
            //    }
            //    else                
            //    {
            //        return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            //    }
            //}
            //else
            //{
            //    return Json("Error Occured. Try Again", JsonRequestBehavior.AllowGet);
            //}


            // return RedirectToAction("Index");

        }

        public int GetRecordNumber_mlaDiary(MlaDiary mdl)
        {
            MlaDiary mdl1 = new MlaDiary();
            mdl1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            mdl1.MlaCode = Convert.ToInt32(mdl.MlaCode);
            mdl1.DocmentType = mdl.DocmentType;
            mdl.RecordNumber = (int)Helper.ExecuteService("Diary", "Chk_RecordNumber", mdl);
            if (mdl.RecordNumber == 0)
            {
                mdl.RecordNumber = 1;
            }
            else
            {
                mdl.RecordNumber = mdl.RecordNumber + 1;
            }
            int re = Convert.ToInt32(mdl.RecordNumber);
            return re;
        }

        public int GetRecordNumber_mlaDiaryDepartment(MlaDiaryDepartment mdl)
        {
            MlaDiaryDepartment mdl1 = new MlaDiaryDepartment();
            mdl1.AssemblyId = Convert.ToInt32(CurrentSession.AssemblyId);
            mdl1.MlaCode = Convert.ToInt32(mdl.MlaCode);
            mdl1.DocmentType = mdl.DocmentType;
            mdl.RecordNumber = (int)Helper.ExecuteService("Diary", "Chk_RecordNumberDepartment", mdl);
            if (mdl.RecordNumber == 0)
            {
                mdl.RecordNumber = 1;
            }
            else
            {
                mdl.RecordNumber = mdl.RecordNumber + 1;
            }
            int re = Convert.ToInt32(mdl.RecordNumber);
            return re;
        }

        public ActionResult EditMlaDairyForm(int ID)
        {
            try
            {
                MlaDiary model = new MlaDiary();
                model = (MlaDiary)Helper.ExecuteService("Diary", "GetMlaDairyRecord", ID);

                var ActionDetails = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionDetails", model);

                if (model.ForwardDate != null)
                {
                    model.ForwardDateForUse = Convert.ToDateTime(model.ForwardDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.ForwardDateForUse = "";
                }
                if (model.ActiontakenDate != null)
                {
                    model.ActiontakenDateForUse = Convert.ToDateTime(model.ActiontakenDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.ActiontakenDateForUse = "";
                }
                if (model.EnclosureDate != null)
                {
                    model.EnclosureDateForUse = Convert.ToDateTime(model.EnclosureDate).ToString("dd/MM/yyyy");
                }
                else
                {
                    model.EnclosureDateForUse = "";
                }
                var discd = Helper.ExecuteService("Diary", "GetDisCode", model);
#pragma warning disable CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                if (discd != null || discd != "")
#pragma warning restore CS0252 // Possible unintended reference comparison; to get a value comparison, cast the left hand side to type 'string'
                {
                    model.distcd = Convert.ToInt16(discd);
                }
                
                model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                model.MappedMemberCode = model.MlaCode.ToString();
                model.MemberCode = Convert.ToInt16(model.MlaCode);
                model.AdhrId = CurrentSession.AadharId;
                model.UserDeptId = model.DeptId;
                model.UserofcId = model.OfficeId.ToString();
                model.OfficeId = model.OfficeId;
                model.DocumentTo = model.DocumentTo;
                model.OfficeId_CC = model.OfficeId_CC;
               
                if (model.OfficeId_CC != null)
                {
                    model.officenameCC = model.OfficeId_CC;
                    model.AllOfficeIds = model.OfficeId_CC.Split(',').ToList();
                    string DeptName = "";
                    DeptName = (string)Helper.ExecuteService("Diary", "GetOfficeNameById", model);
                    if (DeptName != null && DeptName != "")
                    {
                        DeptName = DeptName.TrimEnd(',');
                        model.officenameCCforUse = DeptName;
                    }

                }



              
                model.RawItemDescription = model.ItemDescription;
                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);
                var a = CurrentSession.SPMinId;
                model.memMinList = Helper.ExecuteService("MinistryMinister", "GetMinistersonly", null) as List<mMinisteryMinisterModel>;  //GetDepartmentByMinistery

                var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model);
                var officeListDepartment = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllOfficeListForDepartment", model);

                var MlaList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllMlaList", model);
                var DepartmentList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllDepartmentListForUser", model);
                model.OfficeDeptList = officeListDepartment;
                model.OfficeSubList = Officelist;
                model.MappedMemberList = MlaList;
                model.DepartmentList = DepartmentList;
                model.ActionDetailsList = ActionDetails;
                //for (var i = 0; i < 1; i++)
                //{
                //    model.ActionCode = model.ActionDetailsList[0].ActionCode;
                //}
               
                model.DocumentTypelist = DocumentTypelist;
                model.ActionTypeList = ActionTypelist;
                model.FinanCialYearList = GetFinancialYearList();
                model.PaperEntryType = "EditEntry";
                model.BtnCaption = "Update";
                return PartialView("_MlaDiaryEntryForConstit", model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DeleteMlaDairyForm(int ID)
        {
            try
            {
                OnlineMemberQmodel model = new OnlineMemberQmodel();
               // model.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                model.AssemblyID = Convert.ToInt32(CurrentSession.AssemblyId);
                model.SessionID = Convert.ToInt32(CurrentSession.SessionId);
                ViewBag.PageSize = 25;
                ViewBag.CurrentPage = 1;
                MlaDiary model1 = new MlaDiary();
                Helper.ExecuteService("Diary", "DeleteMlaDairyRecord", ID);
                ///  for search ui

                var DocumentTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetDucumentTypeList", null);
                var ActionTypelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetActionTypeList", null);

                model1.DocmentType = "1";
                model1.ActionCode = 0;
                model1.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
               // model1.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
                if (CurrentSession.MemberCode != "")
                {
                    model1.MlaCode = Convert.ToInt32(CurrentSession.MemberCode);
                }
                else
                {

                    model1.MlaCode = 0;
                }
                model1.UserDeptId = CurrentSession.DeptID;
                model1.UserofcId = CurrentSession.OfficeId;
               // if (CurrentSession.SubUserTypeID == "37")
              //  {
                    model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryListForConUSer", model1);

              //  }
              //  model1.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "Get_MlaDiaryList", model1);

                ViewBag.TotalDiary = Helper.ExecuteService("Diary", "Get_TotalDiaryCountForConUser", model1) as string;
                ViewBag.pendingDiaryCount = Helper.ExecuteService("Diary", "Get_PendingDiaryCountForConUser", model1) as string;
                ViewBag.forwardActionPendingCount = Helper.ExecuteService("Diary", "Get_ActionPendingDiaryCountForConUser", model1) as string;
                ViewBag.forwardActionDoneCount = Helper.ExecuteService("Diary", "Get_ActionDoneDiaryCountForConUser", model1) as string;
                model1.FinanCialYearList = GetFinancialYearList();
                model1.DocumentTypelist = DocumentTypelist;
                model1.ActionTypeList = ActionTypelist;
                model1.PaperEntryType = "Counsellor Diary";
                ViewBag.DocType = "1";
                ViewBag.ActionType = "0";
                ViewBag.PendencySince = "0";

               return PartialView("_MlaDiaryListForConstituency", model1);
             
                //return Content("Deleted");
            }
            catch (Exception)
            {

                throw;
            }
        }

        // [HttpPost]
        public FileStreamResult GenerateDirayPdf(int DiaryId)  //GeneratePublishedTourPDF
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();

            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            if (model.MlaDiaryList.Count > 0)
            {
                //mMember member = new mMember();
                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                if (obj != null)
                {
                    int crntyear = DateTime.Now.Year;
                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);

                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                    foreach (MlaDiary diary in model.MlaDiaryList)
                    {

                        string frwDate = "";
                        isCCItemExist = diary.DocumentCC;
                        if (diary.ForwardDate != null)
                        { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                        if (diary.DiaryLanguage == 1)
                        {
                            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                            outXml += @"<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                            outXml += @"<center><h3>" + "" + "</h3></center>";
                            outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                            outXml += @"<thead>";
                            outXml += @"<tr>";
                            //outXml += @"<th style='text-align:center;font-size:28px;color:#307ecc;font-weight: bold;'>" + obj.HeaderText1 + "</th></tr>";
                            //outXml += @"<th style='text-align:center;font-size:28px;color:#307ecc;font-weight: bold;'>" + obj.HeaderText2 + "</th></tr>";
                            //outXml += "<tr><th style='margin-top:20px'></th>";
                            outXml += @"</tr><thead><tbody><br>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                            outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                            outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";
                            //outXml += @"<tr><td style='text-align:left;' >UO No. SPS(Spk)/" + crntyear + "-" + diary.RecordNumber + "</td><td><div style='width:100%;'>Dated:" + Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy") + "</div><br></td></tr>";
                            //outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                            //outXml += @"<tr><td colspan='2' style='text-align:right;' ><strong>" + "Speaker, <br> H.P.Vidhan Sabha, <br>Shimla-171004. <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + " <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
                            outXml += "";
                        }
                        else if (diary.DiaryLanguage == 2)
                        {
                            outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                            outXml += "<center><h3>" + "" + "</h3></center>";
                            outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                            outXml += "<thead>";
                            outXml += "<tr>";
                            //outXml += @"<th style='text-align:center;font-size:28px;color:#307ecc;font-weight: bold;'>" + obj.HeaderText1 + "</th></tr>";
                            //outXml += @"<th style='text-align:center;font-size:28px;color:#307ecc;font-weight: bold;'>" + obj.HeaderText2 + "</th></tr>";
                            //outXml += "<tr><th style='margin-top:20px'></th>";
                            outXml += "</tr><thead><tbody><br>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;' ><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                            outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                            //outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;'><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "<strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            outXml += "";
                        }

                    }


                    outXml += @"</tbody>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert2 = outXml;

                    HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                    AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/mlaDiary/" + "DiaryPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();
                }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

        //public FileStreamResult PrintDiaryPdf(int DiaryId)  
        //{
        //    string msg = string.Empty;

        //    MlaDiary model = new MlaDiary();

        //    model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
        //    MemoryStream output = new MemoryStream();
        //    if (model.MlaDiaryList.Count > 0)
        //    {
        //        //mMember member = new mMember();
        //        string isCCItemExist = "";
        //        //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
        //        mMlaSignature obj = new mMlaSignature();
        //        obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
        //        obj.MlaCode = CurrentSession.MemberCode;
        //       // obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
        //       // if (obj != null)
        //      //  {
        //            //PdfConverter pdfConverter = new PdfConverter();
        //            //  pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
        //            int crntyear = DateTime.Now.Year;
        //            EvoPdf.Document document1 = new EvoPdf.Document();
        //            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
        //            document1.CompressionLevel = PdfCompressionLevel.Best;
        //            document1.Margins = new Margins(0, 0, 0, 0);
        //            //  document1.Templates.AddNewTemplate(
        //            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.Legal, new Margins(0, 0, 40, 40),
        //            PdfPageOrientation.Portrait);

        //            string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
        //            foreach (MlaDiary diary in model.MlaDiaryList)
        //            {

        //                string frwDate = "";
        //                isCCItemExist = diary.DocumentCC;
        //                if (diary.ForwardDate != null)
        //                { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

        //                //if (diary.DiaryLanguage == 1)
        //                //{
        //                    //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
        //                    outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
        //                    outXml += @"<center><h3>" + "" + "</h3></center>";
        //                    outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
        //                    outXml += @"<thead>";
        //                    outXml += @"<tr>";
        //                    // outXml += @"<th><br></th></tr>";
        //                    //outXml += @"<tr><th style='margin-top:30px'></th>";
        //                    outXml += @"</tr><thead><tbody><br><br><br><br><br><br><br><br>";
        //                    outXml += "";
                                                     
        //                    outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
        //                    //outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
        //                    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
        //                    //outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
        //                    //outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
        //                    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";
                           
        //                    //outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                          
        //                    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
        //                    outXml += "";
        //               // }
        //                //else if (diary.DiaryLanguage == 2)
        //                //{
        //                //    outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
        //                //    outXml += "<center><h3>" + "" + "</h3></center>";
        //                //    outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
        //                //    outXml += "<thead>";
        //                //    outXml += "<tr>";
        //                //    //outXml += "<th><br></th></tr>";
        //                //    //outXml += "<tr><th style='margin-top:30px'></th>";
        //                //    outXml += "</tr><thead><tbody><br><br><br><br><br><br>";
        //                //    outXml += "";

        //                //    //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
        //                //    outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
        //                //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
        //                //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            
        //                //    //outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
        //                //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
        //                //    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

        //                //    //outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                           
        //                //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
        //                //    outXml += "";
        //                //}

        //            }

        //            outXml += @"</tbody>";

        //            outXml += @"</div></div></body></html>";

        //            string htmlStringToConvert2 = outXml;

        //            HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

        //            AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

        //            byte[] pdfBytes = document1.Save();

        //            output.Write(pdfBytes, 0, pdfBytes.Length);

        //            output.Position = 0;

        //            string url = "/mlaDiary/" + "DiaryPdf/";

        //            string directory = Server.MapPath(url);

        //            if (!Directory.Exists(directory))
        //            {
        //                Directory.CreateDirectory(directory);
        //            }

        //            string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

        //            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
        //            System.IO.FileAccess.Write);

        //            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

        //            // close file stream
        //            _FileStream.Close();
        //       // }
        //    }
        //    // }

        //    return new FileStreamResult(output, "application/pdf");
        //}

        public FileStreamResult PrintDiaryPdf(int DiaryId)
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();
            mdiaryno mdl1 = new mdiaryno();
            var resDiaryNo = (List<MlaDiary>)Helper.ExecuteService("Diary", "get_CommonDiaryNo", DiaryId);
            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            string DiariesRefNo = "";
            string documentCC = "";
            if (resDiaryNo.ToList() != null)
            {


                foreach (var item in resDiaryNo.ToList())
                {
                    // for (int i = 0; i < resDiaryNo.ToList().Count; i++)
                    // {
                    DiariesRefNo += item.RecordNumber + "/" + Convert.ToDateTime(item.DiaryDate).Year + " & ";
                    documentCC += item.DocumentTo + " & "; ;
                    // }
                }

            }
            if (model.MlaDiaryList.Count > 0)
            {

                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                //  obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                // if (obj != null)
                // {
                //PdfConverter pdfConverter = new PdfConverter();
                //  pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                int crntyear = DateTime.Now.Year;
                EvoPdf.Document document1 = new EvoPdf.Document();
                document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                document1.CompressionLevel = PdfCompressionLevel.Best;
                document1.Margins = new Margins(0, 0, 0, 0);
                //  document1.Templates.AddNewTemplate(
                PdfPage page = document1.Pages.AddNewPage(PdfPageSize.Legal, new Margins(0, 0, 40, 40),
                PdfPageOrientation.Portrait);

//                string outXml = @"<html>";
//                outXml += @"<script>
//                      $(function () {
//                      $('div #Scroll').each(function () {
//                     var $this = $(this);
//                     $this.html($this.html().replace(/&nbsp;/g, ''));
//                         });
//                     });     </script>";


//                outXml += @"<body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";



                string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                foreach (MlaDiary diary in model.MlaDiaryList)
                {

                    string frwDate = "";
                    isCCItemExist = diary.DocumentCC;
                    if (diary.ForwardDate != null)
                    { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                    //if (diary.DiaryLanguage == 1)
                    //{
                    //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                    outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
                    outXml += @"<center><h3>" + "" + "</h3></center>";
                    outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                    outXml += @"<thead>";
                    outXml += @"<tr>";
                    // outXml += @"<th><br></th></tr>";
                    //outXml += @"<tr><th style='margin-top:30px'></th>";
                    outXml += @"</tr><thead><tbody><br><br><br><br><br><br><br><br>";
                    outXml += "";

                    outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr><br>";
                    if (DiariesRefNo != "")
                    {
                        string diarysNo = DiariesRefNo.Trim().Substring(0, DiariesRefNo.Length - 2);
                        outXml += @"<tr><td colspan='2' style='text-align:justify;' ><hr style='border-width: 1px; border-color:grey;' /></td></tr><br>";
                        outXml += @"<tr><td colspan='2' style='text-align:left;'><span style='color:blue'>Copy To : </span> " + documentCC.Substring(0, documentCC.Length - 2) + " - Vide Refrence Number : <strong>" + diarysNo + "</strong></td></tr> ";
                        outXml += @"<tr><td colspan='2' style='text-align:right;'><span style='color:blue'>Dated : </span<strong>" + Convert.ToDateTime(diary.DiaryDate).ToString("dd/MM/yyyy") + " <br>" + "</strong></td></tr><br>";

                    }
                    //outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                    //outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";

                    //outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";

                    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
                    outXml += "";
                    // }
                    //else if (diary.DiaryLanguage == 2)
                    //{
                    //    outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                    //    outXml += "<center><h3>" + "" + "</h3></center>";
                    //    outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                    //    outXml += "<thead>";
                    //    outXml += "<tr>";
                    //    //outXml += "<th><br></th></tr>";
                    //    //outXml += "<tr><th style='margin-top:30px'></th>";
                    //    outXml += "</tr><thead><tbody><br><br><br><br><br><br>";
                    //    outXml += "";

                    //    //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                    //    outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                    //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";

                    //    //outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                    //    //outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //    //outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                    //    //outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";

                    //    //outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                    //    outXml += "";
                    //}

                }

                outXml += @"</tbody>";

                outXml += @"</div></div></body></html>";

                string htmlStringToConvert2 = outXml;

                HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/mlaDiary/" + "DiaryPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }

                string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();
                // }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

        public FileStreamResult GenerateOfficeCopyDirayPdf(int DiaryId)  //GeneratePublishedTourPDF
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();

            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            if (model.MlaDiaryList.Count > 0)
            {
                //mMember member = new mMember();
                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                if (obj != null)
                {
                    //PdfConverter pdfConverter = new PdfConverter();
                    //  pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
                    int crntyear = DateTime.Now.Year;
                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);
                    //  document1.Templates.AddNewTemplate(
                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.Legal, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                    foreach (MlaDiary diary in model.MlaDiaryList)
                    {

                        string frwDate = "";
                        isCCItemExist = diary.DocumentCC;
                        if (diary.ForwardDate != null)
                        { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                        if (diary.DiaryLanguage == 1)
                        {
                            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                            outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
                            outXml += @"<center><h3>" + "" + "</h3></center>";
                            outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                            outXml += @"<thead>";
                            outXml += @"<tr>";
                            // outXml += @"<th><br></th></tr>";
                            //outXml += @"<tr><th style='margin-top:30px'></th>";
                            outXml += @"</tr><thead><tbody><br><br><br><br><br><br><br><br>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                            outXml += @"<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                            outXml += @"<tr><td style='text-align:left;' >" + obj.Text1 + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate + ":" + frwDate + "<br></td></tr>";
                            //outXml += @"<tr><td style='text-align:left;' >UO No. SPS(Spk)/" + crntyear + "-" + diary.RecordNumber + "</td><td><div style='width:100%;'>Dated:" + Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy") + "</div><br></td></tr>";
                            outXml += @"<tr><td colspan='2 style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                            //outXml += @"<tr><td colspan='2' style='text-align:right;' ><strong>" + "Speaker, <br> H.P.Vidhan Sabha, <br>Shimla-171004. <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + "<br><br>" + "</strong></td></tr>";
                            outXml += "";
                        }
                        else if (diary.DiaryLanguage == 2)
                        {
                            outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                            outXml += "<center><h3>" + "" + "</h3></center>";
                            outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                            outXml += "<thead>";
                            outXml += "<tr>";
                            //outXml += "<th><br></th></tr>";
                            //outXml += "<tr><th style='margin-top:30px'></th>";
                            outXml += "</tr><thead><tbody><br><br><br><br><br><br>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;' ><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "</strong></td></tr>";
                            outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                            outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                            outXml += "<tr><td colspan='2' style='text-align:left;'>" + diary.DocumentCC + "<br><br></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;'><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "<strong></td></tr>";
                            outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            outXml += "";
                        }

                    }

                    outXml += @"</tbody>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert2 = outXml;

                    HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                    AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/mlaDiary/" + "DiaryPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();
                }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

        public FileStreamResult GenerateLetterCopyDairyPdf(int DiaryId)  //GeneratePublishedTourPDF
        {
            string msg = string.Empty;

            MlaDiary model = new MlaDiary();

            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDairyListForPdf", DiaryId);
            MemoryStream output = new MemoryStream();
            if (model.MlaDiaryList.Count > 0)
            {
                //mMember member = new mMember();
                string isCCItemExist = "";
                //  member.ase = Convert.ToInt32(CurrentSession.MemberCode);
                mMlaSignature obj = new mMlaSignature();
                obj.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
                obj.MlaCode = CurrentSession.MemberCode;
                obj = (mMlaSignature)Helper.ExecuteService("Diary", "GetDiarySignatureData", obj);
                if (obj != null)
                {
                    int crntyear = DateTime.Now.Year;
                    EvoPdf.Document document1 = new EvoPdf.Document();
                    document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
                    document1.CompressionLevel = PdfCompressionLevel.Best;
                    document1.Margins = new Margins(0, 0, 0, 0);

                    PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                    PdfPageOrientation.Portrait);

                    string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";
                    foreach (MlaDiary diary in model.MlaDiaryList)
                    {

                        string frwDate = "";
                        isCCItemExist = diary.DocumentCC;
                        if (diary.ForwardDate != null)
                        { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                        if (diary.DiaryLanguage == 1)
                        {
                            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
                            outXml += @"<style>table { }table, th{border:0px solid white;font-size:21px;}table, td{}</style>";
                            outXml += @"<center><h3>" + "" + "</h3></center>";
                            outXml += @"<table cellpadding='4' style='width:100%;font-size:18px;margin-left: 55px;'>";
                            outXml += @"<thead>";
                            outXml += @"<tr>";
                            //outXml += @"<th><br></th></tr>";
                            // outXml += @"<tr><th style='margin-top:30px'></th>";
                            outXml += @"</tr><thead><tbody>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += @"<tr><td colspan='2' style='text-align:justify;'>" + diary.letterDescription + "</td></tr>";
                            outXml += @"<tr><td colspan='2 style='text-align:right;' ><br></td></tr>";
                            //outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation + ", <br>" + obj.SignaturePlace1 + ", <br>" + obj.SignaturePin1 + " <br><br>" + "</strong></td></tr>";
                            //outXml += @"<tr><td colspan='2' style='text-align:left;' ><br><br><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2 + ",<br>" + obj.SignaturePin2 + "<br></strong></td></tr>";
                            outXml += "";
                        }
                        else if (diary.DiaryLanguage == 2)
                        {
                            outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                            outXml += "<center><h3>" + "" + "</h3></center>";
                            outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                            outXml += "<thead>";
                            outXml += "<tr>";
                            // outXml += "<th><br></th></tr>";
                            // outXml += "<tr><th style='margin-top:30px'></th>";
                            outXml += "</tr><thead><tbody>";
                            outXml += "";

                            //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                            outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.letterDescription + "</td></tr>";
                            outXml += "<tr><td colspan='2 style='text-align:right;' ><br></td></tr>";
                            // outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                            //outXml += "<tr><td colspan='2' style='text-align:right;' ><strong>" + "???????, <br> ??0???0 ????????, <br>?????-171004. <br><br>" + "</strong></td></tr>";
                            // outXml += @"<tr><td colspan='2'style='text-align:left;' ><br><br><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                            outXml += "";
                        }

                    }

                    outXml += @"</tbody>";

                    outXml += @"</div></div></body></html>";

                    string htmlStringToConvert2 = outXml;

                    HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

                    AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

                    byte[] pdfBytes = document1.Save();

                    output.Write(pdfBytes, 0, pdfBytes.Length);

                    output.Position = 0;

                    string url = "/mlaDiary/" + "DiaryPdf/";

                    string directory = Server.MapPath(url);

                    if (!Directory.Exists(directory))
                    {
                        Directory.CreateDirectory(directory);
                    }

                    string path = Path.Combine(Server.MapPath("~" + url), model.MlaCode + "_" + model.RecordNumber + ".pdf");

                    FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                    System.IO.FileAccess.Write);

                    _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                    // close file stream
                    _FileStream.Close();
                }
            }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

        [HttpGet]
        public ActionResult GetOffices_ByDptDistrictWise(string DistrictId, string Deptid,string MapedMLacode)
        {
            //  RecipientGroupViewModel model1 = new RecipientGroupViewModel();
            RecipientGroup model = new RecipientGroup();
            model.DistId = DistrictId;
            model.DeptId = Deptid;
            if (MapedMLacode == "null")
            {
                MapedMLacode = "0";
            }
            model.MemberCode = Convert.ToInt16(MapedMLacode);

            model.AssId = Convert.ToInt16(CurrentSession.AssemblyId);
            if (CurrentSession.SubDivisionId == "")
            {
                string constituencyID = CurrentSession.ConstituencyID;
                string[] str = new string[2];
                str[0] = CurrentSession.AssemblyId;
                str[1] = constituencyID;
                string[] strOutput = (string[])Helper.ExecuteService("ConstituencyVS", "GetConstituencyCode_ByConID", str);
                if (strOutput != null)
                {
                    constituencyID = Convert.ToString(strOutput[0]);
                    //CurrentSession.ConstituencyName = Convert.ToString(strOutput[1]);
                }
                model.ConstituencyCode = Convert.ToInt16(constituencyID);
                model.ConstituencyCodeForSubdivision = "0";
            }
            else
            {
                //model.ConstituencyCode = Convert.ToInt16(CurrentSession.ConstituencyID);
                model.ConstituencyCodeForSubdivision = CurrentSession.ConstituencyID;
            }
            model.OfficeList = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllOfficemapped_ByDeptDistrict", model);  //GetAllOffice_ByDeptDistrict
            return Json(model.OfficeList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult getSubdivision_DistrictWise(string DistrictId)
        {
            //  RecipientGroupViewModel model1 = new RecipientGroupViewModel();
            MlaDiary model = new MlaDiary();
            model.distcd = Convert.ToInt16(DistrictId);
            //DiaryModel dobj = new DiaryModel();
            //dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            //dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
            //dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
            //model.ConstituencyCode = dobj.ConstituencyCode;
            int Assid = Convert.ToInt16(CurrentSession.AssemblyId);
          //  model.AssemblyId = Assid;
            //if(CurrentSession.ConstituencyID!="")
            //{
            //    model.ConstituencyCode = Convert.ToInt16(CurrentSession.ConstituencyID);
            //}
            model.SubDivisionList = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "Get_Subdivision_ByDistrictndCon", model);

            return Json(model.SubDivisionList, JsonRequestBehavior.AllowGet);
        }

        public List<SelectListItem> GetFinancialYearList()
        {
            var methodparameter = new List<KeyValuePair<string, string>>();
            int year = 0;
            int year1 = 0;
            year = System.DateTime.Today.Year - 1;
            year1 = year + 1;
            string Year2 = year + "-" + year1;
            int i = 1;


            List<SelectListItem> YearList = new List<SelectListItem>();
            //YearList.Add(new SelectListItem { Text = "Select", Value = "0" });
            while (year < System.DateTime.Today.Year + 1)
            {
                YearList.Add(new SelectListItem { Text = Year2, Value = Year2 });

                year = year + 1;
                year1 = year + 1;
                Year2 = year + "-" + year1;
                i = i + 1;
            }
            return YearList;
        }
        #endregion

        private void RecordError(Exception errormessage, string placeofOrigin)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

            var path = fileAcessingSettings.SettingValue + "\\ServerErrors\\" + "ContentError.txt";

            if (!System.IO.File.Exists(path))
            {
                using (System.IO.File.Create(path))
                {

                }


                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);
                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Stack Trace" + errormessage.StackTrace);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine("Error Message: " + errormessage.Message);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
            else if (System.IO.File.Exists(path))
            {
                List<string> errordata = new List<string>();
                errordata.Add(DateTime.Now.ToString());
                errordata.Add(placeofOrigin);
                errordata.Add("Stack Trace" + errormessage.StackTrace);
                errordata.Add("Error Message: " + errormessage.Message);
                errordata.Add(" ");
                System.IO.File.AppendAllLines(path, errordata, System.Text.ASCIIEncoding.ASCII);

                //TextWriter tw = new StreamWriter(path);
                //tw.WriteLine(DateTime.Now.ToString());
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(placeofOrigin);
                //tw.WriteLine(tw.NewLine);
                //tw.WriteLine(errormessage);
                //tw.WriteLine(tw.NewLine);
                //tw.Close();
            }
        }
        public ActionResult TenderList()
        {
            try
            {



                return PartialView("/Areas/Grievances/Views/Grievances/_TenderByDept.cshtml",DocumentViewModel.GetDeptDocumentListByDept(CurrentSession.DeptID));
            }
            catch (Exception ex)
            {
                RecordError(ex, "Getting All DepartMent Document List");
                ViewBag.ErrorMessage = "Their is a Error While Getting All DepartMent Document List";
                return View("AdminErrorPage");

            }
        }
        public ActionResult CreateTender()
        {
            try
            {

                DocumentViewModel model1 = new DocumentViewModel();
                model1.Mode = "Add";
                model1.StatusTypeList = ModelMapping.GetStatusTypes();
                model1.IsActive = 1;
                model1.IsDeleted = false;

                model1.CategoryList = DepartmnetDetailsViewModel.GetTenderCategory();
                model1.ProjectList = DepartmnetDetailsViewModel.GetDepartmentById(CurrentSession.DeptID);
                ViewBag.FileAcessingPath = "/DepartmentDetails/Doc/;";





                return PartialView("/Areas/Grievances/Views/Grievances/_CreateTender.cshtml", model1);
            }
            catch (Exception ex)
            {
                RecordError(ex, "Getting All DepartMent Document List");
                ViewBag.ErrorMessage = "Their is a Error While Getting All DepartMent Document List";
                return View("AdminErrorPage");

            }
        }




        public ActionResult EditTenderById(string Id)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return base.RedirectToAction("LoginUP", "Account", new { area = "" });
            }
            DocumentViewModel deptDocumentListById = DocumentViewModel.GetDeptDocumentListById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString(), "qwerty12345")));
            deptDocumentListById.Mode = "Edit";
            deptDocumentListById.StatusTypeList = ModelMapping.GetStatusTypes();
            deptDocumentListById.ProjectList = DepartmnetDetailsViewModel.GetAllDepartmentDetails(deptDocumentListById.catId,CurrentSession.DeptID);
            deptDocumentListById.CategoryList = DepartmnetDetailsViewModel.GetTenderCategory();
            return PartialView("/Areas/Grievances/Views/Grievances/_CreateTender.cshtml", deptDocumentListById);
        }

        [HttpPost, ValidateAntiForgeryToken]

        public ActionResult SaveTender(DocumentViewModel model)
        {
            var fileAcessingSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
            string path = fileAcessingSettings.SettingValue + "\\DepartmentDetails\\Doc";


            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

              
              

                if (model.Mode == "Add")
                {
                    // model.IsActive = true;
                    model.CreatedBy = Guid.Parse(CurrentSession.UserID);
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);



                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }



                    byte[] bytes1 = null;
                    string type = "";

                    string[] filenamearr = model.DocumentFileName.Split('.');
                    string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                    ConvertBase64StringToBytes(model.DocumentLocation, ref bytes1, ref type);
                    System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                    model.DocumentLocation = "DepartmentDetails/Doc/" + FileName;
                    DocumentViewModel.SaveDocumentDepartment(model);
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.DocumentFileName))
                    {
                        // model.CreatedBy = Guid.Parse(CurrentSession.UserID);




                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        byte[] bytes1 = null;
                        string type = "";

                        string[] filenamearr = model.DocumentFileName.Split('.');
                        string FileName = filenamearr[0] + "_" + DateTime.Now.Month + "_" + DateTime.Now.Day + "_" + DateTime.Now.Year + "_" + DateTime.Now.Second + "_" + Convert.ToString(DateTime.Now.Millisecond) + "." + filenamearr[1];
                        ConvertBase64StringToBytes(model.DocumentLocation, ref bytes1, ref type);
                        System.IO.File.WriteAllBytes(path + "/" + FileName, bytes1);
                        model.DocumentLocation = "DepartmentDetails\\Doc\\" + FileName;
                    }
                    model.ModifiedBy = Guid.Parse(CurrentSession.UserID);

                    DocumentViewModel.SaveDocumentDepartment(model);
                    //var result = model.ToDomainModel();
                    //result.GalleryId = model.GalleryImageId.HasValue ? model.GalleryImageId.Value : 0;
                    //Helper.ExecuteService("Gallery", "UpdateGalleryCategory", result);
                }

                //if (model.IsCreatNew)
                //{
                //    return RedirectToAction("CreateGallery");
                //}
                //else
                //{
                //    return RedirectToAction("GalleryCategoryIndex");
                //}
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                RecordError(ex, "Saving Tender Document");
                ViewBag.ErrorMessage = "Their is a Error While saving the Tender Document";
                return View("AdminErrorPage");
            }

            // return RedirectToAction("GalleryCategoryIndex");
        }

        public ActionResult DeleteTender(string Id)
        {

            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { area = "" });
                }
                else
                {
                    DepartmnetDetailsViewModel.DeleteDocumentById(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())));
                    return PartialView("/Areas/Grievances/Views/Grievances/_TenderList.cshtml");
                }
            }
            catch (Exception ex)
            {
                RecordError(ex, "Deleting DepartMent Document Deails");
                ViewBag.ErrorMessage = "Their is a Error While Deleting DepartMent Document Deails";
                return View("AdminErrorPage");
            }


        }

        private static void ConvertBase64StringToBytes(string base64String, ref byte[] bytes1, ref string Type)
        {


            string[] split = base64String.Split(',');


            bytes1 = System.Convert.FromBase64String(base64String.Substring(base64String.IndexOf(',') + 1));

            if (base64String != "")
            {
                split = base64String.Split(',');
                Type = split[0];

            }


            //new SqlParameter("@filescannedBankGurnthdn",bytes1),

            //   new SqlParameter("@Type",Type),
        }

        public ActionResult GetDivData()
        {
            return PartialView("_DivData");
        }

        public PartialViewResult SearchGrievance(int pageId , int pageSize, int Status, int OrderBy )
        {
            pageId = 1;
            pageSize = 20;
            OrderBy = 0;
            string AadharId = CurrentSession.AadharId;
            string DeptId = CurrentSession.DeptID;
            var User = (mUsers)Helper.ExecuteService("Grievance", "GetUserByAadharId", AadharId);
            string SubDivisionID = CurrentSession.SubDivisionId;
            var GrievanceList = (List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllOfficeGrievances", new ForwardGrievance { DeptId = DeptId, OfficeId = User.OfficeId, status = Status, SubDivisionId = SubDivisionID });

            //string MemberCode = CurrentSession.MemberCode;
            //List<tMemberGrievances> GrievanceList = new List<tMemberGrievances>();
            //var GrievanceData = ((List<tMemberGrievances>)Helper.ExecuteService("Grievance", "GetAllMemberGrievances", MemberCode)).Where(m => m.StatusId == Status).ToList();
          
            
            if (OrderBy == 0)
                GrievanceList = GrievanceList.OrderByDescending(m => m.SubmittedDate).ToList();
            else
                GrievanceList = GrievanceList.OrderByDescending(m => m.StatusModifiedDate).ToList();

            ViewBag.PageSize = pageSize;
            List<tMemberGrievances> pagedRecord = new List<tMemberGrievances>();
            if (pageId == 1)
            {
                pagedRecord = GrievanceList.Take(pageSize).ToList();
            }
            else
            {
                int r = (pageId - 1) * pageSize;
                pagedRecord = GrievanceList.Skip(r).Take(pageSize).ToList();
            }

            ViewBag.CurrentPage = pageId;
            ViewData["PagedList"] = Helper.BindPager(GrievanceList.Count, pageId, pageSize);
            ViewBag.GrievanceCount = GrievanceList.Count;
            return PartialView("/Areas/Grievances/Views/Grievances/_GrievanceList.cshtml", pagedRecord);
        }

        public FileStreamResult GetDiaryPrint(string DocType, int ActionType, int PendencySince)  //GenerateDiaryPrint
        {
            string msg = string.Empty;
            MlaDiary model = new MlaDiary();
            model.PaperEntryType = "Counsellor Diary";
            model.DocmentType = DocType;
            model.ActionCode = ActionType;
            model.AssemblyId = Convert.ToInt16(CurrentSession.AssemblyId);
            model.PendencySince = PendencySince;
           // model.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);

            // SBL.eLegistrator.HouseController.Web.Extensions.ErrorLog.WriteToLog("in");
            mTypeOfDocument md1 = new mTypeOfDocument();
            md1.DocumentTypeId = Convert.ToInt16(DocType);
            model.UserDeptId = CurrentSession.DeptID;
            model.UserofcId = CurrentSession.OfficeId;
            model.CreatedBy = CurrentSession.UserID;
            if (CurrentSession.SubDivisionId != "")
            {
                model.SubDivisionId = Convert.ToInt16(CurrentSession.SubDivisionId);
                model.MlaCode = 0;
            }
            else
            {
                model.SubDivisionId = 0;
                model.MlaCode = Convert.ToInt16(CurrentSession.MemberCode);
            }
            model.MlaDiaryList = (List<MlaDiary>)Helper.ExecuteService("Diary", "GetMlaDiary_SearchDataByTypeForConUser", model);
            //   var documenttypename = Helper.ExecuteService("Diary", "Get_DocumentTypeName", md1.DocumentTypeId);
            md1.DocumentTypeName = (string)Helper.ExecuteService("Diary", "Get_DocumentTypeName", md1.DocumentTypeId);
            var documenttypename = md1.DocumentTypeName;
            MemoryStream output = new MemoryStream();


            int crntyear = DateTime.Now.Year;
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);

            PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
            PdfPageOrientation.Portrait);

            string outXml = @"<html><body style='font-family:DVOT-Yogesh;font-size:18px;margin-right:100px;margin-left: 120px;'><div><div style='width: 100%;'>";


            //if (diary.DiaryLanguage == 1)
            // {
            //  string HeadingPdf ="";// "Tour Programme of " + CurrentSession.MemberPrefix + " " + member.Name + ", Hon'ble " + CurrentSession.MemberDesignation + ", Himanchal Pradesh Vidhan Sabha Shimla w.e.f. " + Startdate.ToString("dd MMMMMM, yyyy") + " to " + endate.ToString("dd MMMMMM, yyyy");
            outXml += @"<style>table {border: solid 1px black; }table, th{border-left:1px solid black;font-size:18px;}table, td{border-top:solid 1px black;border-left:solid 1px black;font-size:18px;}</style>";
            outXml += @"<center><h3>" + "Diary Data" + "</h3></center>";
            outXml += @"<left><h3>Document Type : " + documenttypename + "</h3></left>";
            outXml += @"<table style='width:100%;font-size:18px;margin-left: 55px;'>";
            outXml += @"<thead>";
            outXml += @"<tr>";

            outXml += @"<tr><th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'> DiaryNumber</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>To</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Subject</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>From</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Forward On</th>";
            outXml += @"<th style='text-align:center;font-size:20px;color:#307ecc;font-weight: bold;'>Action On</th>";
            //outXml += "<tr><th style='margin-top:20px'></th>"; 
            outXml += @"</tr>  </thead></tbody><br>";
            //  outXml += @"</tr></tbody><br>";
            outXml += "";
            if (model.MlaDiaryList.Count > 0)
            {
                foreach (MlaDiary diary in model.MlaDiaryList)
                {

                    string frwDate = "";
                    string ActionTknDate = "";
                    // isCCItemExist = diary.DocumentCC;
                    if (diary.ForwardDate != null)
                    { frwDate = Convert.ToDateTime(diary.ForwardDate).ToString("dd.MM.yyyy"); }

                    if (diary.ActiontakenDate != null)
                    { frwDate = Convert.ToDateTime(diary.ActiontakenDate).ToString("dd.MM.yyyy"); }
                    //outXml += "<td align=\"text-align:left;\" >" + tour.Purpose + "</td>";
                    outXml += @"<tr><td style='text-align:center;'>" + diary.RecordNumber + "</td>";
                    outXml += @"<td  style='text-align:center;'>" + diary.DocumentTo + "</td>";
                    outXml += @"<td  style='text-align:center;'>" + diary.Subject + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + diary.RecieveFrom + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + frwDate + "</td>";
                    outXml += @"<td  style='text-align:center;' >" + ActionTknDate + "</td>";

                    outXml += "</tr>";
                    //}
                    //else if (diary.DiaryLanguage == 2)
                    //{
                    //    outXml += "<style>table { }table, th{border:0px solid white;font-size:18px;}table, td{}</style>";
                    //    outXml += "<center><h3>" + "" + "</h3></center>";
                    //    outXml += "<table cellpadding='4' style='width:100%; font-size:18px;margin-left: 55px;'>";
                    //    outXml += "<thead>";
                    //    outXml += "<tr>";

                    //    outXml += "</tr><thead><tbody><br>";
                    //    outXml += "";


                    //    outXml += "<tr><td colspan='2' style='text-align:justify;'>" + diary.ItemDescription + "</td></tr>";
                    //    outXml += "<tr><td colspan='2 style='text-align:right;' ><br><br><br></td></tr>";
                    //    outXml += @"<tr><td colspan='2' style='text-align:right;'><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + ", <br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";

                    //    outXml += @"<tr><td colspan='2'style='text-align:left;' ><strong>" + diary.DocumentTo + "<br>" + obj.SignaturePlace2_Local + ",<br>" + obj.SignaturePin2_Local + "<br></strong></td></tr>";
                    //    outXml += "<tr><td colspan='2 style='text-align:right;' ><hr style='border-width: 2px;' /></td></tr>";
                    //    outXml += @"<tr><td style='text-align:left;' >" + obj.Text1_local + crntyear + "-" + diary.RecordNumber + " (" + (string)Helper.ExecuteService("Department", "GetDepartmentNameById", diary.DeptId) + " )</td><td>" + obj.SignatureDate_local + ":" + frwDate + "<br></td></tr>";

                    //    outXml += @"<tr><td colspan='2' style='text-align:right;'><br><br><strong>" + obj.SignatureDesignation_Local + ", <br>" + obj.SignaturePlace1_Local + "<br>" + obj.SignaturePin1_Local + "<br><br>" + "</strong></td></tr>";
                    //    outXml += "";
                    //}

                }
            }
            else
            {
                outXml += @"<tr><td style='text-align:center;'> No Data </td>";

                outXml += "</tr>";
            }



            outXml += @"</tbody>";

            outXml += @"</div></div></body></html>";


            string htmlStringToConvert2 = outXml;

            HtmlToPdfElement htmlToPdfElement2 = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert2, "");

            AddElementResult addResult2 = page.AddElement(htmlToPdfElement2);

            byte[] pdfBytes = document1.Save();

            output.Write(pdfBytes, 0, pdfBytes.Length);

            output.Position = 0;

            string url = "/mlaDiary/" + "PrintDiaryData/";

            string directory = Server.MapPath(url);

            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            string path = Path.Combine(Server.MapPath("~" + url), model.RecordNumber + ".pdf");

            FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
            System.IO.FileAccess.Write);

            _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

            // close file stream
            _FileStream.Close();
            //}
            // }
            // }

            return new FileStreamResult(output, "application/pdf");
        }

        public ActionResult MLADiaryReport()
         {
               MlaDiary _Diary = new MlaDiary();
            //schemeMapping _scheme = new schemeMapping();
            var santioned1 = _Diary.myDiaryList;
           

            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId!="" && SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId!=null
               && SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId != "0")
            {
                if (CurrentSession.DeptID != "" || CurrentSession.DeptID != null)
                {
                    //ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                   // model1.UserofcId = CurrentSession.OfficeId;
                   // var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);
                    SBL.DomainModel.Models.Office.tDepartmentOfficeMapped tmodel = new DomainModel.Models.Office.tDepartmentOfficeMapped();
                    tmodel.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                    //var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllMappedOfficeByMemberCode", tmodel);
                    //_Diary.OfficeList = GetData1;

                    var distt = CurrentSession.DistrictCode;

                    DiaryModel dobj = new DiaryModel();
                    dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                    dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                    dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
                    var Consttituency = Convert.ToString(dobj.ConstituencyCode);
                    var MlaCode = Convert.ToString(tmodel.MemberCode);
                    CurrentSession.ConstituencyID = Consttituency;
                    _Diary = (MlaDiary)Helper.ExecuteService("Diary", "getMLADiaryListByTypeCount_MLA", new string[] { Consttituency, MlaCode });
                    santioned1 = _Diary.myDiaryList.ToList();
                }
                else
                {
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                    // model1.UserofcId = CurrentSession.OfficeId;
                    // var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);
                    SBL.DomainModel.Models.Office.tDepartmentOfficeMapped tmodel = new DomainModel.Models.Office.tDepartmentOfficeMapped();
                    tmodel.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);
                    //var GetData1 = (List<SBL.DomainModel.Models.Office.mOffice>)Helper.ExecuteService("ContactGroups", "GetAllMappedOfficeByMemberCode", tmodel);
                    //_Diary.OfficeList = GetData1;

                    var distt = CurrentSession.DistrictCode;

                    DiaryModel dobj = new DiaryModel();
                    dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                    dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                    dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
                    var Consttituency = Convert.ToString(dobj.ConstituencyCode);
                    var MlaCode = Convert.ToString(tmodel.MemberCode);
                    CurrentSession.ConstituencyID = Consttituency;
                    _Diary = (MlaDiary)Helper.ExecuteService("Diary", "getMLADiaryListByTypeCount_MLA", new string[] { Consttituency, MlaCode });
                    santioned1 = _Diary.myDiaryList.ToList();

                }
            }
            else
            {

                if (CurrentSession.DeptID != "" && CurrentSession.DeptID != null)
                {
                    ViewBag.DeptID = CurrentSession.DeptID;
                    if (string.IsNullOrEmpty(CurrentSession.UserID))
                    {
                        return RedirectToAction("LoginUP", "Account", new { @area = "" });
                    }
                    MlaDiary model1 = new MlaDiary();
                    model1.UserofcId = CurrentSession.OfficeId;

                    var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteService("Diary", "GetAllSubOfficeList", model1);

                    var Agency = CurrentSession.OfficeId;
                    _Diary = (MlaDiary)Helper.ExecuteService("Diary", "getMLADiaryListByTypeCount", new string[] { Agency });
                    santioned1 = _Diary.myDiaryList.ToList();
                }
                else

                {

                    _Diary = (MlaDiary)Helper.ExecuteService("Diary", "getAllMLADiaryList",null);
                    santioned1 = _Diary.myDiaryList.ToList();
                }
            }

          
            var Development = santioned1.Where(x => x.DocmentType == Convert.ToString(1)).Count();
            var Transfer = santioned1.Where(x => x.DocmentType == Convert.ToString(2)).Count();
            var DiscretionaryGrant = santioned1.Where(x => x.DocmentType == Convert.ToString(3)).Count();
            var Advisory = santioned1.Where(x => x.DocmentType == Convert.ToString(4)).Count();
            var Grievance = santioned1.Where(x => x.DocmentType == Convert.ToString(5)).Count();
            var Direction = santioned1.Where(x => x.DocmentType == Convert.ToString(6)).Count();
            var UserTypeId = CurrentSession.UserTypeConstituencyId;
            var MemberCode = CurrentSession.MemberCode;
            var ConstituencyID = CurrentSession.ConstituencyID;
            var AssemblyId = CurrentSession.AssemblyId;
            var SessionId = CurrentSession.SessionId;

            return PartialView("_MLADiaryDetails", new string[] { CurrentSession.OfficeId, "0", Convert.ToString(Development), Convert.ToString(Transfer), Convert.ToString(DiscretionaryGrant), Convert.ToString(Advisory), Convert.ToString(Grievance), Convert.ToString(Direction), Convert.ToString(UserTypeId), Convert.ToString(MemberCode), Convert.ToString(ConstituencyID), Convert.ToString(AssemblyId), Convert.ToString(SessionId) });
        }

        public ActionResult FilteredMLADiaryListForAgency(string Agency, string Status, string DocumentType, string Office, string Pendency, string District, string Constituency)
        {
            MlaDiary _Diary = new MlaDiary();
            var santioned1 = _Diary.myDiaryList;
            
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId != "" && SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId != "0")
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                SBL.DomainModel.Models.Office.tDepartmentOfficeMapped tmodel = new DomainModel.Models.Office.tDepartmentOfficeMapped();
                tmodel.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

                DiaryModel dobj = new DiaryModel();
                dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
                var Consttituency = Convert.ToString(dobj.ConstituencyCode);
                var MlaCode = Convert.ToString(tmodel.MemberCode);
                CurrentSession.ConstituencyID = Consttituency;
                _Diary = (MlaDiary)Helper.ExecuteService("Diary", "getFilteredMyDiaryByType_MLA", new string[] { Consttituency, MlaCode, DocumentType, Office, Pendency, Status, District, Agency });
                santioned1 = _Diary.myDiaryList.ToList();

                //if (Status == "0" || Status == "1" || Status == "-1")
                //{
                //    _Diary.myDiaryList = _Diary.myDiaryList.Where(x => x.ActionCode == Convert.ToInt16(Status)).ToList();
                //}

              //  return PartialView("_dairySearchList", _Diary);

            }
            else
            {
                if (Constituency != Convert.ToString(0))
                {
                    _Diary = (MlaDiary)Helper.ExecuteService("Diary", "getFilteredMyDiaryByTypeConstcheck", new string[] { Agency, DocumentType, Constituency, Office, Pendency, Status, District, });
                    _Diary.myDiaryList = _Diary.myDiaryList.ToList();
                }
                else
                {
                    _Diary = (MlaDiary)Helper.ExecuteService("Diary", "getFilteredMyDiaryByType", new string[] { Agency, DocumentType, Constituency, Office, Pendency, Status, District, });
                    _Diary.myDiaryList = _Diary.myDiaryList.ToList();
                }


                //if (Status == "0" || Status == "1" || Status == "-1")
                //{
                //    _Diary.myDiaryList = _Diary.myDiaryList.Where(x => x.ActionCode == Convert.ToInt16(Status)).ToList();
                //}

            }

            var Total = _Diary.myDiaryList;
            var Devlopement = Total.Where(x => x.DocmentType == "1").Count();
            var Schemes = Total.Where(x => x.DocmentType == "2").Count();
            var DG = Total.Where(x => x.DocmentType == "3").Count();
            var Advice = Total.Where(x => x.DocmentType == "4").Count();
            var grieves = Total.Where(x => x.DocmentType == "5").Count();
            var direction = Total.Where(x => x.DocmentType == "6").Count();

            ViewBag.Devlopement = Devlopement;
            ViewBag.Schemes = Schemes;
            ViewBag.DG = DG;
            ViewBag.Advice = Advice;
            ViewBag.grieves = grieves;
            ViewBag.direction = direction;
            
            return PartialView("_dairySearchList", _Diary);
        }

        public ActionResult GetDistrictForOffice(int OfficeId)
        {
            List<fillDistrict> _list = (List<fillDistrict>)Helper.ExecuteService("Diary", "GetDistrictForOffice", OfficeId);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Department_ForAllOffices(int OfficeId)
        {
            List<fillDepartment> _list = (List<fillDepartment>)Helper.ExecuteService("Diary", "GetDepartmentForOffice", OfficeId);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Filtered_MLADiaryListForAll(string Status, string Office)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            
          
            MlaDiary model1 = new MlaDiary();
            model1.UserofcId = Office;
            var santioned1 = model1.myDiaryList;
            if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UserTypeConstituencyId == "1")
            {

                if (string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }

                SBL.DomainModel.Models.Office.tDepartmentOfficeMapped tmodel = new DomainModel.Models.Office.tDepartmentOfficeMapped();
                tmodel.MemberCode = Convert.ToInt32(CurrentSession.MemberCode);

                DiaryModel dobj = new DiaryModel();
                dobj.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                dobj.AssemblyID = Convert.ToInt16(CurrentSession.AssemblyId);
                dobj = (DiaryModel)Helper.ExecuteService("Diary", "GetConstByMemberId", dobj);
                var Consttituency = Convert.ToString(dobj.ConstituencyCode);
                var MlaCode = Convert.ToString(tmodel.MemberCode);
                CurrentSession.ConstituencyID = Consttituency;
                model1 = (MlaDiary)Helper.ExecuteService("Diary", "GetFiltered_MLADiaryList_ForMLA", new string[] { Consttituency, MlaCode, Status, Office });
                santioned1 = model1.myDiaryList.ToList();

                //if (Status == "0" || Status == "1" || Status == "-1")
                //{
                //    model1.myDiaryList = model1.myDiaryList.Where(x => x.ActionCode == Convert.ToInt16(Status)).ToList();
                //}

            }

            else
            {
                
                // var Officelist = (List<SBL.DomainModel.Models.Diaries.MlaDiary>)Helper.ExecuteServiceFilteredMLADiaryListForAgency("Diary", "GetAllSubOfficeList", model1);
                // model1 = (MlaDiary)Helper.ExecuteService("Diary", "GetFiltered_MLADiaryList", new string[] { Status, Office });
                model1 = (MlaDiary)Helper.ExecuteService("Diary", "GetFiltered_MLADiaryList", new string[] { Status, Office });
                //if (model1.UserofcId != "0")
                //{
                //    model1.myDiaryList = model1.myDiaryList.ToList();
                //}

                //if (Status == "0" || Status == "1" || Status == "-1")
                //{
                //    model1.myDiaryList = model1.myDiaryList.Where(x => x.ActionCode == Convert.ToInt16(Status)).ToList();
                //}
            }

           
            var Total = model1.myDiaryList;
            var Devlopement = Total.Where(x => x.DocmentType == "1").Count();
            var Schemes = Total.Where(x => x.DocmentType == "2").Count();
            var DG = Total.Where(x => x.DocmentType == "3").Count();
            var Advice = Total.Where(x => x.DocmentType == "4").Count();
            var grieves = Total.Where(x => x.DocmentType == "5").Count();
            var direction = Total.Where(x => x.DocmentType == "6").Count();
            //var PendingClearDPR = Total.Where(x => x.workStatusCode == 7).Count();

            ViewBag.Devlopement = Devlopement;
            ViewBag.Schemes = Schemes;
            ViewBag.DG = DG;
            ViewBag.Advice = Advice;
            ViewBag.grieves = grieves;
            ViewBag.direction = direction;
          
            return PartialView("_dairySearchList", model1);
           
             }

        public ActionResult SearchDiaryListForNodal(string District, string Constituency, string Agency, string Office)
        {
            if (string.IsNullOrEmpty(CurrentSession.UserID))
            {
                return RedirectToAction("LoginUP", "Account", new { @area = "" });
            }
            MlaDiary _Diary = new MlaDiary();
            var santioned1 = _Diary.myDiaryList;
            // string SubDivisionCode = "0";

            _Diary = (MlaDiary)Helper.ExecuteService("Diary", "GetMLADiaryList_Search", new string[] { District, Constituency, Agency, Office });
            _Diary.myDiaryList = _Diary.myDiaryList.ToList();

            var Total = _Diary.myDiaryList;
            var Devlopement = Total.Where(x => x.DocmentType == "1").Count();
            var Schemes = Total.Where(x => x.DocmentType == "2").Count();
            var DG = Total.Where(x => x.DocmentType == "3").Count();
            var Advice = Total.Where(x => x.DocmentType == "4").Count();
            var grieves = Total.Where(x => x.DocmentType == "5").Count();
            var direction = Total.Where(x => x.DocmentType == "6").Count();
            //var PendingClearDPR = Total.Where(x => x.workStatusCode == 7).Count();

            ViewBag.Devlopement = Devlopement;
            ViewBag.Schemes = Schemes;
            ViewBag.DG = DG;
            ViewBag.Advice = Advice;
            ViewBag.grieves = grieves;
            ViewBag.direction = direction;

           return PartialView("_dairySearchList", _Diary);
           
        }

        public ActionResult GetMappedConstituencyforOfficer_MLADiary(int OfficeId)
        {
            List<fillListGenricInt> _list = (List<fillListGenricInt>)Helper.ExecuteService("Diary", "GetMappedConstituencyforOfficer_MLADiary", OfficeId);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult GetOfficeDrpList(string officeId_CC)
        //{
        //    MlaDiary user = new MlaDiary();
           
        //    if (officeId_CC != null)
        //    {
        //        user.AllOfficeIds = officeId_CC.Split(',').ToList();
        //    }
        //    mOffice depart = new mOffice();

        //    user.mDepartment = (ICollection<mDepartment>)Helper.ExecuteService("Department", "GetDepartment", user);
        //    return PartialView("_SecretoryDepartment", user);
        //}
    }

}
