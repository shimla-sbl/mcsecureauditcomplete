﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Grievances
{
    public class GrievancesAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Grievances";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Grievances_default",
                "Grievances/{controller}/{action}/{id}",
                new { action = "GrievanceIndex", Controller = "Grievances", id = UrlParameter.Optional }
            );
        }
    }
}
