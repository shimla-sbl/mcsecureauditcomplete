﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.DisbursementOfficer
{
    public class DisbursementOfficerAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "DisbursementOfficer";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "DisbursementOfficer_default",
                "DisbursementOfficer/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
