﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.BlankRed
{
    public class BlankRedAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "BlankRed";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "BlankRed_default",
                "BlankRed/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
