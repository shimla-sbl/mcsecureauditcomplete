﻿using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.CommitteePaperLaid
{
    public class CommitteePaperLaidAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "CommitteePaperLaid";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "CommitteePaperLaid_default",
                "CommitteePaperLaid/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
