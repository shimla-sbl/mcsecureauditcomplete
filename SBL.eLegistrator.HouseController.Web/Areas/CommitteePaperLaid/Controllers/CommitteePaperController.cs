﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.DomainModel.Models.Committee;
using SBL.Service.Common;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Department;
using SBL.eLegistrator.HouseController.Web.HouseControllerService;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Ministery;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Helpers;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Utility;
using Lib.Web.Mvc.JQuery.JqGrid;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using System.Data;
using SBL.eLegislator.HPMS.ServiceAdaptor;

namespace SBL.eLegistrator.HouseController.Web.Areas.CommitteePaperLaid.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class CommitteePaperController : Controller
    {
        //
        // GET: /CommitteePaperLaid/CommitteePaper/
        #region "CommitteePaper By Sunil"

        public ActionResult CommitteePaperLaid()
        {
            tCommitteeModel Obj = new tCommitteeModel();
            mAssembly Assem = new mAssembly();
            Obj.AssemList = Helper.ExecuteService("Assembly", "GetAllAssembly", Assem) as List<mAssembly>;

            List<mSiteSettingsVS> ListSiteSettingsVS = new List<mSiteSettingsVS>();
            mSiteSettingsVS SiteSettingsAssem = new mSiteSettingsVS();
            mSiteSettingsVS SiteSettingsSessn = new mSiteSettingsVS();
            mSiteSettingsVS SiteSettingsSessnCal = new mSiteSettingsVS();

            ListSiteSettingsVS = Helper.ExecuteService("User", "GetSettings", SiteSettingsAssem) as List<mSiteSettingsVS>;


            SiteSettingsAssem = ListSiteSettingsVS.First(x => x.SettingName == "Assembly");
            if (SiteSettingsAssem != null)
            {
                Obj.AssemblyId = Convert.ToInt16(SiteSettingsAssem.SettingValue);

                SiteSettingsSessn = ListSiteSettingsVS.First(x => x.SettingName == "Session");
                if (SiteSettingsSessn != null)
                {
                    Obj.SessionId = Convert.ToInt16(SiteSettingsSessn.SettingValue);
                }
            }


            mSession SessObj = new mSession();
            if (Obj.AssemblyId != 0)
            {
                SessObj.AssemblyID = Obj.AssemblyId;
            }
            Obj.SessList = (List<mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", SessObj);

            return View(Obj);
        }

       

        public ActionResult GetPaperEntry(int assemblyId, int sessionId)
        {
            tCommitteeModel Obj = new tCommitteeModel();
           // Obj.AssemblyId = assemblyId;
           // Obj.SessionId = sessionId;
            Obj.CommList = (List<tCommittee>)Helper.ExecuteService("Committee", "GetCommittee", Obj);
            //Obj.MemList = (List<mMember>)Helper.ExecuteService("Committee", "GetChairPerson", Obj);
            Obj.DocList = (List<mDocumentType>)Helper.ExecuteService("Committee", "GetDocumentType", Obj);
            Obj.EventList = (List<mEvent>)Helper.ExecuteService("Committee", "GetCategory", Obj);
            Obj.memMinList = (List<mMinisteryMinisterModel>)Helper.ExecuteService("Committee", "GetMinisterMinistry", Obj);
            Obj.ComModel = (List<tCommitteeModel>)Helper.ExecuteService("Committee", "GetAllDepartment", Obj);

            List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

            DataSet dataSetsetting = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectSiteSettings", methodParameter);
            string CurrentAssembly = "";
            string Currentsession = "";


            for (int i = 0; i < dataSetsetting.Tables[0].Rows.Count; i++)
            {
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Assembly")
                {
                    CurrentAssembly = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
                if (Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingName"]) == "Session")
                {
                    Currentsession = Convert.ToString(dataSetsetting.Tables[0].Rows[i]["SettingValue"]);
                }
            }
            Obj.AssemblyId = Convert.ToInt16(CurrentAssembly);
            Obj.SessionId = Convert.ToInt16(Currentsession);
            Obj.SessDateList = (List<mSessionDate>)Helper.ExecuteService("Session", "GetSessionDateCurrentSessionID", Obj);
           

            /*Check Committee and CommitteeChairperson*/
            //Obj.CommitteeName = "PAC-Test";
            //Obj.ChairPersonName = "Test";
            //Obj.CommitteeId = 1;
            //Obj.MemberId = 129;

            Obj.ActionType = "Get";
            return PartialView("_GetPaperEntry", Obj);
        }

        public JsonResult GetDepartmentByMinister(int MinistryId)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.MinistryId = MinistryId;
            Obj.ComModel = (List<tCommitteeModel>)Helper.ExecuteService("Committee", "GetDepartmentByMinister", Obj);
            return Json(Obj.ComModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveCommittee(HttpPostedFileBase file, tCommitteeModel Obj)
        {
            string M = "";
            string Url = "";
            if (Utility.CurrentSession.UserName != null)
            {
                Obj.SubmittedBy = Utility.CurrentSession.UserName;
            }
            Url = "/PaperLaid/" + Obj.AssemblyId + "/" + Obj.SessionId + "/";
            string newFileLocation = "";
            if (file != null)
            {
                Obj.FilePath = Url;                
            }          

            //string imageFileLoc = Server.MapPath("~/SignaturePath/blank.gif");

#pragma warning disable CS0219 // The variable 'PositionToApplySignatureID' is assigned but its value is never used
            int PositionToApplySignatureID = 3;
#pragma warning restore CS0219 // The variable 'PositionToApplySignatureID' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'PagesToApplySignatureID' is assigned but its value is never used
            int PagesToApplySignatureID = 4;
#pragma warning restore CS0219 // The variable 'PagesToApplySignatureID' is assigned but its value is never used
            string textLine1 = "eVidhan Time Stamp: " + System.DateTime.Now;
#pragma warning disable CS0219 // The variable 'textLine2' is assigned but its value is never used
            string textLine2 = "";
#pragma warning restore CS0219 // The variable 'textLine2' is assigned but its value is never used

            M = (String)Helper.ExecuteService("Committee", "SaveCommittee", Obj);

            //Save file in filesystem
            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~" + Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }

            if (Obj.BtnCaption == "Update")
            {
                if (file != null)
                {
                    string fileName = file.FileName;
                    newFileLocation = "/PaperLaid/" + Obj.AssemblyId + "/" + Obj.SessionId + "/Signed/" + file.FileName;
                    Obj.FileName = (string)Helper.ExecuteService("Committee", "GetOldFile", Obj);
                    //string Ver = Obj.FileName.Substring(0, Obj.FileName.LastIndexOf("_") + 1);
                    if (System.IO.File.Exists(Server.MapPath("~" + Url + Obj.FileName)))
                    {
                        System.IO.File.Delete(Server.MapPath("~" + Url + Obj.FileName));
                    }

                    file.SaveAs(Server.MapPath("~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + Obj.paperLaidId + "_V1.pdf"));

                    string NewPath = "~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + Obj.paperLaidId + "_V1.pdf";
                   // PDFExtensions.InsertImageToPdf(Server.MapPath(NewPath), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);
                }
                TempData["Message"] = M;
            }
            else
            {
                string fileName = file.FileName;
                newFileLocation = "/PaperLaid/" + Obj.AssemblyId + "/" + Obj.SessionId + "/Signed/" + file.FileName;
                var newPaperLaidId = M.Substring(0, M.IndexOf("_"));
                file.SaveAs(Server.MapPath("~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + newPaperLaidId + "_V1.pdf"));

                string NewPath = "~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + newPaperLaidId + "_V1.pdf";
                //PDFExtensions.InsertImageToPdf(Server.MapPath(NewPath), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);
                
                TempData["Message"] = M.Substring(M.IndexOf("_") + 1);
            }
            return View("Index");
        }

        //[HttpPost, ValidateAntiForgeryToken]
        //public ActionResult SaveCommittee(HttpPostedFileBase file, tCommitteeModel Obj)
        //{

           
        //        string M = "";
        //        string Url = "";
        //        if (Utility.CurrentSession.UserName != null)
        //        {
        //            Obj.SubmittedBy = Utility.CurrentSession.UserName;
        //        }

        //        Url = "/PaperLaid/" + Obj.AssemblyId + "/" + Obj.SessionId + "/";
                


        //        if (file != null)
        //        {

        //            Obj.FilePath = Url;
        //            string newFileLocation = "/PaperLaid/" + Obj.AssemblyId + "/" + Obj.SessionId + "/Signed/" + file.FileName;
        //            string fileName = file.FileName;

        //            string imageFileLoc = Server.MapPath("~/SignaturePath/blank.gif");

        //            int PositionToApplySignatureID = 3;
        //            int PagesToApplySignatureID = 4;
        //            string textLine1 = "eVidhan Time Stamp: " + System.DateTime.Now;
        //            string textLine2 = "";
        //            var newPaperLaidId = M.Substring(0, M.IndexOf("_"));
        //            file.SaveAs(Server.MapPath("~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + newPaperLaidId + "_V1.pdf"));

        //            string NewPath = "~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + newPaperLaidId + "_V1.pdf";
        //            PDFExtensions.InsertImageToPdf(Server.MapPath(NewPath), imageFileLoc, Server.MapPath(newFileLocation), PositionToApplySignatureID, PagesToApplySignatureID, textLine1, textLine2);



        //            TempData["Message"] = M.Substring(M.IndexOf("_") + 1);


        //        }

           
        //        M = (String)Helper.ExecuteService("Committee", "SaveCommittee", Obj);


        //        //Save file in filesystem
        //        DirectoryInfo Dir = new DirectoryInfo(Server.MapPath("~" + Url));
        //        if (!Dir.Exists)
        //        {
        //            Dir.Create();
        //        }
            

        //    if (Obj.BtnCaption == "Update")
        //    {
        //        if (file != null)
        //        {

        //            Obj.FileName = (string)Helper.ExecuteService("Committee", "GetOldFile", Obj);
        //            //string Ver = Obj.FileName.Substring(0, Obj.FileName.LastIndexOf("_") + 1);
                   
        //            var ver = Obj.version.GetValueOrDefault();
        //            string ext = Path.GetExtension(file.FileName);
        //            string fileNamee = file.FileName.Replace(ext, "");
        //            string actualFileName = Obj.AssemblyId + "_" + Obj.SessionId + "_" + "C" + "_" + Obj.paperLaidId + "_V" + ver + ext;
        //            file.SaveAs(Server.MapPath(Url + actualFileName));
        //            Obj.FileName = actualFileName;

        //            if (System.IO.File.Exists(Server.MapPath("~" + Url + Obj.FileName)))
        //            {
        //                System.IO.File.Delete(Server.MapPath("~" + Url + Obj.FileName));
        //            }

        //            //file.SaveAs(Server.MapPath("~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + Obj.paperLaidId + "_V1.pdf"));

        //        }
        //        TempData["Message"] = M;
        //    }
        //    else
        //    {
                
        //        var newPaperLaidId = M.Substring(0, M.IndexOf("_"));
        //        file.SaveAs(Server.MapPath("~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + newPaperLaidId + "_V1.pdf"));

        //        string NewPath = "~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + newPaperLaidId + "_V1.pdf";



        //        TempData["Message"] = M.Substring(M.IndexOf("_") + 1);

        //    }

        //    return RedirectToAction("CommitteeDashboard", "CommitteePaper");

        //}

        //New Desing code
        public ActionResult GetAllList()
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tCommitteeModel Obj = new tCommitteeModel();
                //Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetPendings", Obj);
                return PartialView("_NewGetAllList", Obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewGetAllList(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;
            //this is add for require in new Design

            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            Obj.SessionCode = Obj.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
            Obj.AssemblyCode = Obj.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

            mSession Mdl = new mSession();
            Mdl.SessionCode = Obj.SessionId;
            Mdl.AssemblyID = Obj.AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = Obj.AssemblyCode;

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetAllList", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,
            };

            var resultToSort = Obj.ComModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());
            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        public ActionResult GetPendings()
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {
                tCommitteeModel Obj = new tCommitteeModel();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetPendings", Obj);
                Obj.SessionId = siteSettingMod.SessionCode;
                Obj.AssemblyId = siteSettingMod.AssemblyCode;
                return PartialView("_NewGetPendingList", Obj);
            }
            else
            {
                return RedirectToAction("LoginUP", "Account");
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewGetPendings(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;
            //this is add for require in new Design
            Obj.ActionType = "Pending";
            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "SearchByCategory", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,
            };

            var resultToSort = Obj.ComModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }
        public ActionResult GetSubmitted()
        {
            tCommitteeModel Obj = new tCommitteeModel();
            //Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetSubmitted", Obj);
            return PartialView("_NewGetSubmittedList", Obj);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult NewGetSubmitted(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;
            //this is add for require in new Design
            Obj.ActionType = "Submitted";
            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "SearchByCategory", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,
            };

            var resultToSort = Obj.ComModel.AsQueryable();
            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult EditPending(int Id)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetRecordForEdit", Id);

            //Obj.CommitteeName = "PAC-Test";
            //Obj.ChairPersonName = "Test";
            //Obj.CommitteeId = 1;
            //Obj.MemberId = 129;

            Obj.ActionType = "Edit";
            return PartialView("_GetPaperEntry", Obj);
        }

        public JsonResult SubmitPending(string Ids)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            if (Ids != null && Ids != "")
            {
                string[] Cids = Ids.Split(',');
                if (Utility.CurrentSession.UserName != null)
                {
                    Obj.SubmittedBy = Utility.CurrentSession.UserName;
                }
                for (int i = 0; i < Cids.Length; i++)
                {
                    Obj.PaperLaidIdTemp = Convert.ToInt16(Cids[0]);
                    Obj.Message = Helper.ExecuteService("Committee", "SubmitPending", Obj) as string;
                }
            }
            return Json(Obj.Message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeletePending(int Id)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PaperLaidIdTemp = Id;
            Obj.Message = Helper.ExecuteService("Committee", "DeletePending", Id) as string;
            return Json(Obj.Message, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewDetails(int Id)
        {
            PaperMovementContainerModel Obj = new PaperMovementContainerModel();
            Obj.paperLaidId = Id;
            Obj = (PaperMovementContainerModel)Helper.ExecuteService("Committee", "ViewDetails", Obj);
            return PartialView("_ViewDetails", Obj);
        }

        public ActionResult SendViewDetails(int Id)
        {
            PaperMovementContainerModel Obj = new PaperMovementContainerModel();
            Obj.paperLaidId = Id;
            Obj = (PaperMovementContainerModel)Helper.ExecuteService("Committee", "SendViewDetails", Obj);
            return PartialView("_ViewDetails", Obj);
        }
        
        public ActionResult ViewDetailsForReplacePaper(int Id)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.paperLaidId = Id;
            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "ViewDetailsForReplacePaper", Obj);

            PaperMovementContainerModel movementModel = new PaperMovementContainerModel();
            movementModel.paperLaidId = Id;
            movementModel = (PaperMovementContainerModel)Helper.ExecuteService("Committee", "SendViewDetails", movementModel);
            Obj.paperMovementModel = movementModel;
            return PartialView("_ViewDetailsForReplacePaper", Obj);
        }

        public ActionResult FileUpload(int Id)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.paperLaidId = Id;
            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "FileUpload", Obj);
            return PartialView("_AddFile", Obj);
        }

        public ActionResult SearchByCategory(int CategId, string Tabtype, string PageNumber, string RowsPerPage, string loopStart, string loopEnd)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.EventId = CategId;
            Obj.ActionType = Tabtype;
            PageNumber = Sanitizer.GetSafeHtmlFragment(PageNumber);
            RowsPerPage = Sanitizer.GetSafeHtmlFragment(RowsPerPage);
            loopStart = Sanitizer.GetSafeHtmlFragment(loopStart);
            loopEnd = Sanitizer.GetSafeHtmlFragment(loopEnd);

            Obj.loopStart = Convert.ToInt16(loopStart);
            Obj.loopEnd = Convert.ToInt16(loopEnd);
            Obj.PageSize = Convert.ToInt16(RowsPerPage);
            Obj.PageIndex = Convert.ToInt16(PageNumber);

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "SearchByCategory", Obj);
            if (PageNumber != null && PageNumber != "")
            {
                Obj.PageNumber = Convert.ToInt32(PageNumber);
            }
            else
            {
                Obj.PageNumber = Convert.ToInt32("1");
            }
            if (RowsPerPage != null && RowsPerPage != "")
            {
                Obj.RowsPerPage = Convert.ToInt32(RowsPerPage);
            }
            else
            {
                Obj.RowsPerPage = Convert.ToInt32("10");
            }
            if (PageNumber != null && PageNumber != "")
            {
                Obj.selectedPage = Convert.ToInt32(PageNumber);
            }
            else
            {
                Obj.selectedPage = Convert.ToInt32("1");
            }
            if (loopStart != null && loopStart != "")
            {
                Obj.loopStart = Convert.ToInt32(loopStart);
            }
            else
            {
                Obj.loopStart = Convert.ToInt32("1");
            }
            if (loopEnd != null && loopEnd != "")
            {
                Obj.loopEnd = Convert.ToInt32(loopEnd);
            }
            else
            {
                Obj.loopEnd = Convert.ToInt32("5");
            }
            if (Tabtype == "Pending")
            {
                return PartialView("_GetPendingSearchList", Obj);
            }
            else
            {
                return PartialView("_GetSubmittedListSearch", Obj);
            }
        }
        
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddLatestFile(HttpPostedFileBase file, tCommitteeModel Obj)
        {
            string M = "";
            if (file != null)
            {
                if (Utility.CurrentSession.UserName != null)
                {
                    Obj.SubmittedBy = Utility.CurrentSession.UserName;
                }
                string Url = "/PaperLaid/" + Obj.AssemblyId + "/" + Obj.SessionId + "/";

                Obj.version = (int)Helper.ExecuteService("Committee", "GetLatestVersion", Obj);

                //Obj.FileName = file.FileName;

                Obj.FilePath = Url;

                M = (String)Helper.ExecuteService("Committee", "AddLatestFile", Obj);

                file.SaveAs(Server.MapPath("~" + Url + Obj.AssemblyId + "_" + Obj.SessionId + "_C_" + Obj.paperLaidId + "_V" + Obj.version + ".pdf"));
                TempData["Message"] = M;
            }
            return RedirectToAction("CommitteeDashboard", "CommitteePaper");
        }

        //public ActionResult CommitteeDashboard()
        //{
        //    if (!String.IsNullOrEmpty(CurrentSession.UserID))
        //    {

        //        tCommitteeModel Obj = new tCommitteeModel();

        //        List<mSiteSettingsVS> ListSiteSettingsVS = new List<mSiteSettingsVS>();
        //        mSiteSettingsVS SiteSettingsAssem = new mSiteSettingsVS();
        //        mSiteSettingsVS SiteSettingsSessn = new mSiteSettingsVS();

        //        Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetInitialCount", Obj);

        //        ListSiteSettingsVS = Helper.ExecuteService("User", "GetSettings", SiteSettingsAssem) as List<mSiteSettingsVS>;


        //        SiteSettingsAssem = ListSiteSettingsVS.First(x => x.SettingName == "Assembly");
        //        if (SiteSettingsAssem != null)
        //        {
        //            Obj.AssemblyId = Convert.ToInt16(SiteSettingsAssem.SettingValue);

        //            SiteSettingsSessn = ListSiteSettingsVS.First(x => x.SettingName == "Session");
        //            if (SiteSettingsSessn != null)
        //            {
        //                Obj.SessionId = Convert.ToInt16(SiteSettingsSessn.SettingValue);
        //            }
        //        }
        //        mDepartment deptInfo = new mDepartment();
        //        deptInfo.deptId = CurrentSession.DeptID;
        //        deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
        //        Obj.DepartmentName = deptInfo.deptname;

        //        Obj.CurrentUserName = CurrentSession.UserName;
        //        Obj.UserDesignation = CurrentSession.MemberDesignation;


        //        return View(Obj);
        //    }

        //    else
        //    {
        //        return RedirectToAction("LoginUP", "Account");

        //    }
        //}


        public ActionResult CommitteeDashboard()
        {
            if (!String.IsNullOrEmpty(CurrentSession.UserID))
            {

                tCommitteeModel Obj = new tCommitteeModel();

                tCommitteeSendModel obj1 = new tCommitteeSendModel();

                List<mSiteSettingsVS> ListSiteSettingsVS = new List<mSiteSettingsVS>();
                mSiteSettingsVS SiteSettingsAssem = new mSiteSettingsVS();
                mSiteSettingsVS SiteSettingsSessn = new mSiteSettingsVS();

                Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetInitialCount", Obj);


                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                Obj.SessionCode = Obj.SessionId = siteSettingMod.SessionCode; //Convert.ToInt32(siteSettingMod.SessionCode);
                Obj.AssemblyCode = Obj.AssemblyId = siteSettingMod.AssemblyCode;// Convert.ToInt32(siteSettingMod.AssemblyCode);

                mSession Mdl = new mSession();
                Mdl.SessionCode = Obj.SessionId;
                Mdl.AssemblyID = Obj.AssemblyCode;
                mAssembly assmblyMdl = new mAssembly();
                assmblyMdl.AssemblyID = Obj.AssemblyCode;

                if (SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID == "7C4F28F6-02FC-4627-993D-E255037531AD")
                {
                    Obj.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameBySessionCode", Mdl);
                    Obj.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameByAssemblyCode", assmblyMdl);

                }
                else
                {
                    Obj.SessionName = (string)Helper.ExecuteService("Session", "GetSessionNameLocalBySessionCode", Mdl);
                    Obj.AssesmblyName = (string)Helper.ExecuteService("Assembly", "GetAssemblyNameLocalByAssemblyCode", assmblyMdl);
                }

                if (Convert.ToString(TempData["lSM"]) != "")
                {
                    Obj.lSM = Convert.ToString(TempData["lSM"]);

                }
                else
                {
                    Obj.lSM = "";
                }


                //ListSiteSettingsVS = Helper.ExecuteService("User", "GetSettings", SiteSettingsAssem) as List<mSiteSettingsVS>;


                //SiteSettingsAssem = ListSiteSettingsVS.First(x => x.SettingName == "Assembly");
                //if (SiteSettingsAssem != null)
                //{
                //    Obj.AssemblyId = Convert.ToInt16(SiteSettingsAssem.SettingValue);
                //    obj1.AssemblyId = Convert.ToInt16(SiteSettingsAssem.SettingValue);

                //    SiteSettingsSessn = ListSiteSettingsVS.First(x => x.SettingName == "Session");
                //    if (SiteSettingsSessn != null)
                //    {
                //        Obj.SessionId = Convert.ToInt16(SiteSettingsSessn.SettingValue);
                //        obj1.SessionId = Convert.ToInt16(SiteSettingsSessn.SettingValue);
                //    }
                //}
                mDepartment deptInfo = new mDepartment();
                deptInfo.deptId = CurrentSession.DeptID;
                deptInfo = (mDepartment)Helper.ExecuteService("Department", "GetDepartmentByID", deptInfo) as mDepartment;
                Obj.DepartmentName = deptInfo.deptname;

                Obj.CurrentUserName = CurrentSession.UserName;
                Obj.UserDesignation = CurrentSession.MemberDesignation;

                obj1.DepartmentName = deptInfo.deptname;

                obj1.CurrentUserName = CurrentSession.UserName;
                obj1.UserDesignation = CurrentSession.MemberDesignation;



                if (Convert.ToString(TempData["Msg"]) != "")
                {
                    if (Convert.ToString(TempData["Msg"]) == "Save")
                    {
                        Obj.CustomMessage = "Data Save Successfully !!";
                    }
                    if (Convert.ToString(TempData["Msg"]) == "Update")
                    {
                        Obj.CustomMessage = "Data Update Successfully !!";
                    }
                    if (Convert.ToString(TempData["Msg"]) == "File")
                    {
                        Obj.CustomMessage = "Paper Replacement Successfully !!";
                    }
                    if (Convert.ToString(TempData["Msg"]) == "Sign")
                    {
                        Obj.CustomMessage = "Paper Signed Successfully !!";
                    }

                }
                else
                {
                    Obj.CustomMessage = "";
                }


                return View(Obj);
            }

            else
            {
                return RedirectToAction("LoginUP", "Account");

            }
        }

        public JsonResult GetChairmanByCommId(int CommId)
        {

            tCommitteeModel Obj = new tCommitteeModel();
            Obj.CommitteeId = CommId;
            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetChairmanByCommId", Obj);
            return Json(Obj, JsonRequestBehavior.AllowGet);

        }

        public ActionResult UpcomingLob()
        {
            tCommitteeModel Obj = new tCommitteeModel();

            // Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetSubmitted", Obj);

            return PartialView("_NewUpcomingLobList", Obj);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UpcomingLobGrid(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;

            //this is add for require in new Design
            //Obj.ActionType = "Submitted";

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "CommitteeUpcomingLOB", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,

            };


            var resultToSort = Obj.ComModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        public ActionResult LaidInHouse()
        {
            tCommitteeModel Obj = new tCommitteeModel();
            // Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetSubmitted", Obj);
            return PartialView("_NewLaidInHouseList", Obj);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LaidInHouseGrid(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;

            //this is add for require in new Design
            //  Obj.ActionType = "Submitted";

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "CommitteeLaidinHouse", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,

            };


            var resultToSort = Obj.ComModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };

        }

        public ActionResult PendingToLay()
        {
            tCommitteeModel Obj = new tCommitteeModel();
            // Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "GetSubmitted", Obj);
            return PartialView("_NewPendingToLayList", Obj);
        }

        public ActionResult PendingToLayGrid(JqGridRequest request)
        {
            tCommitteeModel Obj = new tCommitteeModel();
            Obj.PageSize = request.RecordsCount;
            Obj.PageIndex = request.PageIndex + 1;

            //this is add for require in new Design
            //Obj.ActionType = "Submitted";

            Obj = (tCommitteeModel)Helper.ExecuteService("Committee", "CommitteePendingToLay", Obj);

            JqGridResponse response = new JqGridResponse()
            {
                TotalPagesCount = (int)Math.Ceiling((float)Obj.ResultCount / (float)request.RecordsCount),
                PageIndex = request.PageIndex,
                TotalRecordsCount = Obj.ResultCount,

            };


            var resultToSort = Obj.ComModel.AsQueryable();

            var resultForGrid = resultToSort.OrderBy<tCommitteeModel>(request.SortingName, request.SortingOrder.ToString());

            response.Records.AddRange(from questionList in resultForGrid
                                      select new JqGridRecord<tCommitteeModel>(Convert.ToString(questionList.paperLaidId), new tCommitteeModel(questionList)));

            return new JqGridJsonResult() { Data = response };
        }

        #endregion
    }
}
