﻿using SBL.DomainModel.Models.ItemSubscription;
using SBL.DomainModel.Models.PressClipping;
using SBL.DomainModel.Models.UploadImages;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.DomainModel.Models.User;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PressClippingController : Controller
    {
        //
        // GET: /Library/PressClipping/
        #region Old pressclippings
        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mPressClipping>)Helper.ExecuteService("PressClipping", "GetAllPressClipDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        //added by robin to show published press clipings to public or members
        public ActionResult GetOldPublishedPressClippingsDetails()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mPressClipping>)Helper.ExecuteService("PressClipping", "GetOldPublishedPressClippingsDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                var model = State.ToViewModel();
                return View("_OldPressClippingsListForMembers", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult GetTodayPublishedPressClippingsDetails()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mPressClipping>)Helper.ExecuteService("PressClipping", "GetTodayPublishedPressClippingsDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                var model = State.ToViewModel();
                return View("_PressClippingListForMembers", model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult SetAvailability()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mPressClipping>)Helper.ExecuteService("PressClipping", "GetAllPressClipDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                var model = State.ToViewModel();
                var ISList = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "GetAllNewspaperSubscriptionDetails", null);
                ViewBag.NewsPapaerList = new SelectList(ISList, "ItemSubscriptionID", "ItemName");

                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult CreatePressClipping()
        {
            PressClipViewModel model = new PressClipViewModel();
            model.CategoryList = ModelMapping.GetCategoryList();
            var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "GetAllNewspaperSubscriptionDetails", null);
            model.NewsPapaerList = new SelectList(State, "ItemSubscriptionID", "ItemName");
            model.Mode = "Add";
            return View(model);
        }

        public ActionResult EditPressClip(string Id)
        {

            mPressClipping stateToEdit = (mPressClipping)Helper.ExecuteService("PressClipping", "GetPressClipById", new mPressClipping { PressClipID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

            var model = stateToEdit.ToViewModel1("Edit");
            model.CategoryList = ModelMapping.GetCategoryList();
            var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "GetAllNewspaperSubscriptionDetails", null);
            model.NewsPapaerList = new SelectList(State, "ItemSubscriptionID", "ItemName");

            return View("CreatePressClipping", model);
        }

        public ActionResult DeletePressClip(string Id)
        {
            RemoveExistingPhoto(Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())), "PressClipping");
            mPressClipping stateToDelete = (mPressClipping)Helper.ExecuteService("PressClipping", "GetPressClipById", new mPressClipping { PressClipID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
            //if (stateToDelete != null && stateToDelete.ScannedCopyImg != null && stateToDelete.ScannedCopyImg.Length > 0)
            //    RemoveExistingPhoto(stateToDelete.ScannedCopyImg);
            Helper.ExecuteService("PressClipping", "DeletePressClip", stateToDelete);
            TempData["Msg"] = "Sucessfully Deleted";
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePressClip(PressClipViewModel model, HttpPostedFileBase[] files)
        {
            if (ModelState.IsValid)
            {
                Guid FileName = Guid.NewGuid();
                var mPressClipping = model.ToDomainModel();
                int PressClipId = 0;
                if (model.Mode == "Add")
                {
                    //   mPressClipping.ScannedCopyImg = UploadPhoto(file, FileName);
                    mPressClipping.Status = false;
                    PressClipId = (int)Helper.ExecuteService("PressClipping", "CreatePressClip", mPressClipping);
                }
                else
                {
                    PressClipId = model.PressClipID;
                    if (files != null && files.Count() > 0 && files.FirstOrDefault() != null)
                        RemoveExistingPhoto(PressClipId, "PressClipping");

                    //if (file != null && file.ContentLength > 0)
                    //{
                    //    mPressClipping.ScannedCopyImg = UploadPhoto(file, FileName);
                    //    RemoveExistingPhoto(OldImageUrl);
                    //}

                    Helper.ExecuteService("PressClipping", "UpdatePressClip", mPressClipping);
                }
                if (files != null && files.Count() > 0 && files.FirstOrDefault() != null && PressClipId > 0)
                    UploadImages(files, PressClipId, "PressClipping");
            }

            return RedirectToAction("Index");
        }

        public JsonResult UpdatePressCliping(FormCollection collection)
        {
            string Result = string.Empty;

            string PCRow = collection["hdnPCRow"];

            // Add QUalification
            if (PCRow != null && PCRow.Length > 0)
            {
                string[] RowCount = PCRow.Split(',');

                foreach (var val in RowCount)
                {

                    try
                    {
                        int Id = 0;
                        int.TryParse(collection["hdnPCId-" + val], out Id);
                        mPressClipping stateToEdit = (mPressClipping)Helper.ExecuteService("PressClipping", "GetPressClipById", new mPressClipping { PressClipID = Id });

                        if (collection["chbStatus-" + val] != null && collection["chbStatus-" + val] == "on")
                            stateToEdit.Status = true;
                        else
                            stateToEdit.Status = false;

                        Helper.ExecuteService("PressClipping", "UpdatePressClip", stateToEdit);
                        Result = "Availability saved sucessfully ";
                    }
                    catch (Exception ex)
                    {

                        Result = ex.ToString();
                    }
                }
            }
            else
            {
                Result = "Unable to recived data";

            }
            return Json(Result, JsonRequestBehavior.AllowGet);


        }

        public PartialViewResult SearchPressClip(string NewsPaperName, string Subject, string ContKey, string NewsDateFrom, string NewsDateTo, string Category)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(NewsPaperName))
                sb.Append(NewsPaperName + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Subject))
                sb.Append(Subject + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(ContKey))
                sb.Append(ContKey + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(NewsDateFrom))
                sb.Append(NewsDateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(NewsDateTo))
                sb.Append(NewsDateTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Category))
                sb.Append(Category + ",");
            else
                sb.Append(" " + ",");


            // Call Split method
            var State = (List<mPressClipping>)Helper.ExecuteService("PressClipping", "SearchPressClipping", sb.ToString());
            var model = State.ToViewModel();

            return PartialView("/Areas/Library/Views/PressClipping/_PressClippingList.cshtml", model);


        }

        #region Private Methods

        public void UploadImages(HttpPostedFileBase[] files, int ParentId, string Module)
        {
            foreach (HttpPostedFileBase file in files)
            {
                if (file != null)
                {
                    Guid FileName = Guid.NewGuid();

                    mUploadImages obj = new mUploadImages();
                    obj.Module = Module;
                    obj.ParentId = ParentId;
                    obj.FileName = UploadPhoto(file, FileName);
                    obj.CreationDate = DateTime.Now;
                    obj.CreatedBy = CurrentSession.UserName;
                    Helper.ExecuteService("UploadImages", "AddImages", obj);
                }
                //System.IO.Path.Combine(Server.MapPath("~/App_Data"), System.IO.Path.GetFileName(file.FileName));
            }
        }
        private void RemoveExistingPhoto(int ParentId, string Module)
        {
            if (ParentId > 0)
            {
                string url = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(ParentId + ",");
                sb.Append(Module);
                var State = (List<mUploadImages>)Helper.ExecuteService("UploadImages", "GetAllUploadImages", sb.ToString());

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                url = "/Images/PressClipping";
                string directory = FileSettings.SettingValue + url;

                if (State != null && State.Count() > 0)
                {
                    foreach (var obj in State)
                    {
                        if (obj != null && obj.FileName != null)
                        {
                            //string path = System.IO.Path.Combine(Server.MapPath("~/Images/PressClipping"), obj.FileName);
                            string path = System.IO.Path.Combine(directory, obj.FileName);
                            if (System.IO.File.Exists(path))
                            {
                                System.IO.File.Delete(path);
                                Helper.ExecuteService("UploadImages", "DeleteImages", new mUploadImages { UploadImagesID = obj.UploadImagesID });

                            }
                        }
                    }
                }


            }

        }
        private string UploadPhoto(HttpPostedFileBase File, Guid FileName)
        {
            if (File != null)
            {
                string url = string.Empty;
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                url = "/Images/PressClipping";
                string url1 = "Images/Temp";
                string directory = FileSettings.SettingValue + url;
                string directory1 = FileSettings.SettingValue + url1;


                // string directory = Server.MapPath("~/Images/PressClipping");
                string path = System.IO.Path.Combine(directory, FileName + "_" + File.FileName);
                string path1 = System.IO.Path.Combine(directory1, FileName + "_" + File.FileName);
                try
                {
                    if (!System.IO.Directory.Exists(directory))
                    {
                        System.IO.Directory.CreateDirectory(directory);
                    }
                    if (!System.IO.Directory.Exists(directory1))
                    {
                        System.IO.Directory.CreateDirectory(directory1);
                    }

                    SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions imgex = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(750);
                    File.SaveAs(path1);
                    imgex.Resize(path1, path);
                    System.IO.File.Delete(path1);

                }
                catch (Exception)
                {

                    throw;
                }

                return (FileName + "_" + File.FileName);
            }
            return null;
        }

        //private string UploadPhoto(HttpPostedFileBase File, Guid FileName)
        //{
        //    if (File != null)
        //    {

        //        string directory = Server.MapPath("~/Images/PressClip");
        //        string path = System.IO.Path.Combine(directory, FileName + "_" + File.FileName);
        //        try
        //        {
        //            if (!System.IO.Directory.Exists(directory))
        //            {
        //                System.IO.Directory.CreateDirectory(directory);
        //            }
        //            File.SaveAs(path);
        //        }
        //        catch (Exception)
        //        {

        //            throw;
        //        }

        //        return (FileName + "_" + File.FileName);
        //    }
        //    return null;
        //}

        //private void RemoveExistingPhoto(string OldPhoto)
        //{
        //    if (!string.IsNullOrEmpty(OldPhoto))
        //    {
        //        string path = System.IO.Path.Combine(Server.MapPath("~/Images/PressClip"), OldPhoto);

        //        if (System.IO.File.Exists(path))
        //        {
        //            System.IO.File.Delete(path);
        //        }
        //    }

        //}



        #endregion
        #endregion

        #region New Pressclippings for as PDF

        public ActionResult PressClippingIndex()
        {
            try
            {
                //if (string.IsNullOrEmpty(CurrentSession.UserName))
                //{
                //    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                //}

                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetFileAcessSetting", null);
                ViewBag.FileLocation = FileSettings.SettingValue;
                var model = (List<mPressClippingsPDF>)Helper.ExecuteService("PressClipping", "GetAllPressClipDetailsPDF", null);
                ViewBag.saved = TempData["saved"];
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult CreatePressClippingPDF(int Id, string Action)
        {
            try
            {
                mPressClippingsPDF model = new mPressClippingsPDF();
                if (Action == "Add")
                {
                    model.Mode = "Add";
                }
                else if (Action == "Edit")
                {
                    model.Mode = "Edit";
                }
                return PartialView("~/Areas/Library/Views/PressClipping/_NewPressClippings.cshtml", model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePressClipingsPDFRecord(mPressClippingsPDF model, HttpPostedFileBase files)
        {
            try
            {

                var checkdate = (mPressClippingsPDF)Helper.ExecuteService("PressClipping", "CheckPressclipingPDFDate", model);
                if (checkdate == null)
                {
                    if (model.Mode == "Add")
                    {
                        model.ModifiedBy = CurrentSession.UserName;
                        model.ModifiedWhen = DateTime.Now;
                        model.CreationDate = DateTime.Now;
                        model.CreatedBy = CurrentSession.UserName;
                        model.Category = "Vidhan Sabha";


                        string url = string.Empty;
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        url = "/PDF/PressClipping";

                        Guid FileName = Guid.NewGuid();

                        //string directory = FileSettings.SettingValue + url;
                        string directory = FileSettings.SettingValue + url;

                        if (!System.IO.Directory.Exists(directory))
                        {
                            System.IO.Directory.CreateDirectory(directory);
                        }
                        string path = System.IO.Path.Combine(directory, FileName + "_PressCliping.pdf" );

                        files.SaveAs(path);

                        model.PressClippingPdfPath = FileName + "_PressCliping.pdf";

                        Helper.ExecuteService("PressClipping", "CreatePressClipPDFInfo", model);
                        TempData["saved"] = "Date saved successfully";
                    }
                    else if (model.Mode == "Edit")
                    {
                        //delete section before new file
                        string url = string.Empty;
                        var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                        string localdirectory = FileSettings.SettingValue;
                        url = "/PDF/PressClipping/";
                        string path = model.PressClippingPdfPath;                        
                        System.IO.File.Delete(localdirectory + url + path);


                        Guid FileName = Guid.NewGuid();

                        string directory = FileSettings.SettingValue + url;

                        if (!System.IO.Directory.Exists(directory))
                        {
                            System.IO.Directory.CreateDirectory(directory);
                        }
                        string path1 = System.IO.Path.Combine(directory, FileName + "_PressCliping.pdf");

                        files.SaveAs(path1);

                        model.PressClippingPdfPath = FileName + "_PressCliping.pdf";
                        model.ModifiedBy = CurrentSession.UserName;
                        model.ModifiedWhen = DateTime.Now;
                        model.Category = "Vidhan Sabha";
                        Helper.ExecuteService("PressClipping", "EditPressClipPDFInfo", model);
                        TempData["saved"] = "Date Edited successfully";
                    }
                }
                else
                {
                    TempData["saved"] = "pdf for selected date already exist";
                }

                return RedirectToAction("PressClippingIndex");
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult PressClipPDFInfo(string Id)
        {
            try
            {
                string url = string.Empty;
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                mPressClippingsPDF model = (mPressClippingsPDF)Helper.ExecuteService("PressClipping", "GetPressClipPDFById", new mPressClippingsPDF { PressClipPDFID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

                string localdirectory = FileSettings.SettingValue;

                url = "/PDF/PressClipping/";
                string path = model.PressClippingPdfPath;

                string path1 = System.IO.Path.Combine(localdirectory, url, path);
                System.IO.Directory.GetFiles(path1);

                return new EmptyResult();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult DeletePressClipPDF(string Id)
        {
            try
            {
                string url = string.Empty;
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                mPressClippingsPDF model = (mPressClippingsPDF)Helper.ExecuteService("PressClipping", "GetPressClipPDFById", new mPressClippingsPDF { PressClipPDFID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

                string localdirectory = FileSettings.SettingValue;

                url = "/PDF/PressClipping/";

                string path = model.PressClippingPdfPath;

                //string pathfordelete = localdirectory + url + path; 
                System.IO.File.Delete(localdirectory+url+path);

                Helper.ExecuteService("PressClipping", "DeletePressClipPDF", new mPressClippingsPDF { PressClipPDFID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                TempData["saved"] = "File deleted successfully";
               return RedirectToAction("PressClippingIndex");
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ActionResult EditPressClipPDF(string Id) 
        {
            try
            {
                string url = string.Empty;
                var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);

                mPressClippingsPDF model = (mPressClippingsPDF)Helper.ExecuteService("PressClipping", "GetPressClipPDFById", new mPressClippingsPDF { PressClipPDFID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });

                string localdirectory = FileSettings.SettingValue;
                model.Mode = "Edit";
                url = "/PDF/PressClipping/";

                string path = model.PressClippingPdfPath;

                //string pathfordelete = localdirectory + url + path; 
               // System.IO.File.Delete(localdirectory + url + path);

               // Helper.ExecuteService("PressClipping", "DeletePressClipPDF", new mPressClippingsPDF { PressClipPDFID = Convert.ToInt32(EncryptionUtility.Decrypt(Id.ToString())) });
                //TempData["saved"] = "File deleted successfully";
                return RedirectToAction("_NewPressClippings",model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion
    }
}
