﻿using SBL.DomainModel.Models.Library;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using System.Text;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class WhoiswhoController : Controller
    {
        //
        // GET: /Library/Whoiswho/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<tWhoiswho>)Helper.ExecuteService("Whoiswho", "GetAllWhoiswhoDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                ViewBag.Page = "Home";
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Who's who Information Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }



        public PartialViewResult CreatetWhoiswho(int Id, string Action)
        {
            WhoiswhoViewModel model = new WhoiswhoViewModel();
            tWhoiswho stateToEdit = (tWhoiswho)Helper.ExecuteService("Whoiswho", "GetWhoiswhoById", new tWhoiswho { WhoiswhoId = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit.ToViewModel1(Action);
            model.Mode = Action;

            return PartialView("/Areas/Library/Views/Whoiswho/_CreateWhoiswho.cshtml", model);
        }




        [ValidateAntiForgeryToken]
        public PartialViewResult SavetWhoiswho(WhoiswhoViewModel model)
        {
            List<WhoiswhoViewModel> ModelList = new List<WhoiswhoViewModel>();
            ViewBag.Page = "Home";
            try
            {

                if (ModelState.IsValid)
                {

                    var tWhoiswho = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("Whoiswho", "CreateWhoiswho", tWhoiswho);
                    }
                    else
                    {
                        Helper.ExecuteService("Whoiswho", "UpdateWhoiswho", tWhoiswho);
                    }
                    var State = (List<tWhoiswho>)Helper.ExecuteService("Whoiswho", "GetAllWhoiswhoDetails", null);
                    ViewBag.Msg = "Who's who Information saved sucessfully";
                    ModelList = State.ToViewModel();
                }
                else
                {
                    ViewBag.Msg = "Unable to add Who's who Information";
                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException.ToString();
            }


            return PartialView("/Areas/Library/Views/Whoiswho/_WhoiswhoList.cshtml", ModelList);
        }

        public PartialViewResult DeleteWhoiswho(int Id = 0)
        {
            List<WhoiswhoViewModel> ModelList = new List<WhoiswhoViewModel>();
            ViewBag.Page = "Home";
            if (Id > 0)
            {
                tWhoiswho stateToDelete = (tWhoiswho)Helper.ExecuteService("Whoiswho", "GetWhoiswhoById", new tWhoiswho { WhoiswhoId = Id });

                Helper.ExecuteService("Whoiswho", "DeleteWhoiswho", stateToDelete);
                ViewBag.Msg = "Who's who Information deleted sucessfully";
            }
            else
            {
                ViewBag.Msg = "Unable to delete";
            }
            var State = (List<tWhoiswho>)Helper.ExecuteService("Whoiswho", "GetAllWhoiswhoDetails", null);

            ModelList = State.ToViewModel();
            return PartialView("/Areas/Library/Views/Whoiswho/_WhoiswhoList.cshtml", ModelList);
        }






    }
}
