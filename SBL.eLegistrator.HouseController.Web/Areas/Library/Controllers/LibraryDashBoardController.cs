﻿using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class LibraryDashBoardController : Controller
    {
        //
        // GET: /Library/LibraryDashBoard/

        public ActionResult Index()
        {
            return View();
        }

    }
}
