﻿using SBL.DomainModel.Models.CommitteeReport;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using System.Text;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class CommitteeReportController : Controller
    {



        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<tCommitteeReportLib>)Helper.ExecuteService("CommitteeReport", "GetAllCommitteeReportDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                ViewBag.Page = "Home";
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Committee Report Information Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }



        public PartialViewResult CreatetCommitteeReport(int Id, string Action)
        {
            CommitteeReportViewModel model = new CommitteeReportViewModel();
            tCommitteeReportLib stateToEdit = (tCommitteeReportLib)Helper.ExecuteService("CommitteeReport", "GetCommitteeReportById", new tCommitteeReportLib { CommitteeReportId = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit.ToViewModel1(Action);
            model.Mode = Action;

            return PartialView("/Areas/Library/Views/CommitteeReport/_CreateCommitteeReport.cshtml", model);
        }




        [ValidateAntiForgeryToken]
        public PartialViewResult SavetCommitteeReport(CommitteeReportViewModel model)
        {
            List<CommitteeReportViewModel> ModelList = new List<CommitteeReportViewModel>();
            ViewBag.Page = "Home";
            try
            {

                if (ModelState.IsValid)
                {

                    var tCommitteeReportLib = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("CommitteeReport", "CreateCommitteeReport", tCommitteeReportLib);
                    }
                    else
                    {
                        Helper.ExecuteService("CommitteeReport", "UpdateCommitteeReport", tCommitteeReportLib);
                    }
                    var State = (List<tCommitteeReportLib>)Helper.ExecuteService("CommitteeReport", "GetAllCommitteeReportDetails", null);
                    ViewBag.Msg = "Committee Report Information saved sucessfully";
                    ModelList = State.ToViewModel();
                }
                else
                {
                    ViewBag.Msg = "Unable to add Committee Report Information";
                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException.ToString();
            }


            return PartialView("/Areas/Library/Views/CommitteeReport/_CommitteeReportList.cshtml", ModelList);
        }

        public PartialViewResult DeleteCommitteeReport(int Id = 0)
        {
            List<CommitteeReportViewModel> ModelList = new List<CommitteeReportViewModel>();
            ViewBag.Page = "Home";
            if (Id > 0)
            {
                tCommitteeReportLib stateToDelete = (tCommitteeReportLib)Helper.ExecuteService("CommitteeReport", "GetCommitteeReportById", new tCommitteeReportLib { CommitteeReportId = Id });

                Helper.ExecuteService("CommitteeReport", "DeleteCommitteeReport", stateToDelete);
                ViewBag.Msg = "Committee Report Information deleted sucessfully";
            }
            else
            {
                ViewBag.Msg = "Unable to delete";
            }
            var State = (List<tCommitteeReportLib>)Helper.ExecuteService("CommitteeReport", "GetAllCommitteeReportDetails", null);

            ModelList = State.ToViewModel();
            return PartialView("/Areas/Library/Views/CommitteeReport/_CommitteeReportList.cshtml", ModelList);
        }



    }
}
