﻿using Email.API;
using EvoPdf;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Library;
using SBL.DomainModel.Models.LibraryVS;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.PaidMemeberSubscription;
using SBL.DomainModel.Models.StaffManagement;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using SMS.API;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class IssueBookController : Controller
    {
        //
        // GET: /Library/IssueBook/

        public ActionResult Index()
        {
            ViewBag.Page = "Home";
            var model = (List<mIssueBook>)Helper.ExecuteService("IssueBook", "GetAllIssueBookDetails", null);//List for members
            return View(model);
        }

        public ActionResult IssueBookListForMembers()
        {
            ViewBag.Page = "Home";
            var model = (List<mIssueBook>)Helper.ExecuteService("IssueBook", "GetAllIssueBookDetails", null);//List for members
            return PartialView("~/Areas/Library/Views/IssueBook/_IssueBookList.cshtml", model);
        }

        public ActionResult IssueBookListForStaff()
        {
            ViewBag.Page = "Home";
            var model = (List<mIssueBook>)Helper.ExecuteService("IssueBook", "GetAllIssueBookDetailsForStaff", null);
            return PartialView("~/Areas/Library/Views/IssueBook/_IssueBookList.cshtml", model);
        }

        public ActionResult IssueBookListForPaid()
        {
            ViewBag.Page = "Home";
            var model = (List<mIssueBook>)Helper.ExecuteService("IssueBook", "GetAllIssueBookDetailsForPaid", null);
            return PartialView("~/Areas/Library/Views/IssueBook/_IssueBookList.cshtml", model);
        }

        public JsonResult GetPaidMemberDetails(int MemberId)
        {
            mIssueBook Resultpopulate = new mIssueBook();
            Resultpopulate.PaidMemberDetailWithId = (PaidMemeberSubs)Helper.ExecuteService("IssueBook", "GetPaidMemberDetails", MemberId);
            return Json(Resultpopulate, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult PopulateIssueBook(int Id, string Action)
        {
            mIssueBook model = new mIssueBook();

            mIssueBook stateToEdit = (mIssueBook)Helper.ExecuteService("IssueBook", "GetIssueBookById", new mIssueBook { IssueBookID = Id });
            if (stateToEdit != null && Id > 0)
            {
                model = stateToEdit;
                ViewBag.SubscriberDesignationId = stateToEdit.SubscriberDesignationId;
                if (stateToEdit.IsPaidMember == true) {
                    ViewBag.IsPaidMember = "true";
                    ViewBag.SubscriberId = stateToEdit.SubscriberId;
                }
                ViewBag.SubscriberId = stateToEdit.SubscriberId;
                ViewBag.SubscriptionType = model.SubscriptionType;
            }
            model.Mode = Action;
           

            model.PaidMemeberSubs = (List<PaidMemeberSubs>)Helper.ExecuteService("IssueBook", "PaidMemberSubscriptionList", null);
            if (Action == "Edit")
            {
                if (stateToEdit.IsPaidMember == false)
                {
                    if (stateToEdit.IsMember == true)
                    {
                        ViewBag.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("RoadPermit", "GetAllDesignations", null);
                        ViewBag.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", stateToEdit.SubscriberDesignationId);
                    }
                    else {
                        ViewBag.DesignationCol = (List<mStaff>)Helper.ExecuteService("VsIssueDocument", "GetAllDesignationsForStaff", null);
                        ViewBag.MemberCol = (List<mStaff>)Helper.ExecuteService("VsIssueDocument", "GetMemberListByDesignationForStaff", "0");
                        ViewBag.staff = true;
                    }
                }
                else
                {
                    ViewBag.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", "0");
                }
            }
            else
            {
                ViewBag.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("RoadPermit", "GetAllDesignations", null);
                ViewBag.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", "0");
            }
            return PartialView("/Areas/Library/Views/IssueBook/_PopulatingIssueBook.cshtml", model);
        }

        public JsonResult GetMemberListByDesignation(string deptId, string typeid)
        {
            if (typeid == "0")
            {
                //int selectedDeptId = deptId ?? 0;
                var MemberCol = (List<mMember>)Helper.ExecuteService("IssueBook", "SubsOfMemberWhomBookIssued", deptId);
                return Json(MemberCol, JsonRequestBehavior.AllowGet);
            }
            if (typeid == "1") {
                //int selectedDeptId = deptId ?? 0;
                var MemberCol = (List<mStaff>)Helper.ExecuteService("IssueBook", "SubsOfstaffWhomBookIssued", deptId);
                return Json(MemberCol, JsonRequestBehavior.AllowGet);
            }
            if(typeid =="2"){
                //int selectedDeptId = deptId ?? 0;
                var MemberCol = (List<PaidMemeberSubs>)Helper.ExecuteService("IssueBook", "SubsOfPaidWhomBookIssued", deptId);
                return Json(MemberCol, JsonRequestBehavior.AllowGet);     
            }
            return null;
        }

        public ActionResult ReturnIssuedBook()
        {
            mIssueBook obj = new mIssueBook();
            //ViewBag.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("RoadPermit", "GetAllDesignations", null);
            ViewBag.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("IssueBook", "DesignationOfMemberWhomBookIssued", null);
            ViewBag.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", "0");                        
            return View();
        }

        public ActionResult ReturnIssuedBookForStaff() {
            mStaff obj = new mStaff();
            obj.mStaffList = (List<mStaff>)Helper.ExecuteService("IssueBook", "DesignationOfstaffWhomBookIssued", null);
            //obj.MemberCol = (List<mMember>)Helper.ExecuteService("IssueBook", "SubsOfstaffWhomBookIssued", null);
            return Json(obj,JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReturnIssuedBookForMember()
        {
            mMemberDesignation obj = new mMemberDesignation();
            obj.MemberDesignationList = (List<mMemberDesignation>)Helper.ExecuteService("IssueBook", "DesignationOfMemberWhomBookIssued", null);
            //obj.MemberCol = (List<mMember>)Helper.ExecuteService("IssueBook", "SubsOfMemberWhomBookIssued",null);
            return Json(obj,JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReturnIssuedBookForPaid()
        {
            PaidMemeberSubs obj = new PaidMemeberSubs();
            obj.PaidMemeberList = (List<PaidMemeberSubs>)Helper.ExecuteService("IssueBook", "SubsOfPaidWhomBookIssued", null);
            return Json(obj,JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// this is for getting accession number list for updateing return date
        /// </summary>
        /// <param name="SubscriberDesignation"></param>
        /// <param name="MemberCode"></param>
        /// <returns></returns>
        public PartialViewResult GetAccesionNumberList(string SubscriberDesignation, string MemberCode)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append(SubscriberDesignation + ",");
            sb.Append(MemberCode + ",");
            ViewBag.SubscriberDesignation = SubscriberDesignation;
            ViewBag.MemberCode = MemberCode;
            var MemberCol = (List<mIssueBookAccessionNum>)Helper.ExecuteService("IssueBook", "SearchAccesionNum", sb.ToString());
            return PartialView("/Areas/Library/Views/IssueBook/_AccesionNumberList.cshtml", MemberCol);
        }


        public PartialViewResult DeleteIssueBook(int Id)
        {
            try
            {
                Helper.ExecuteService("IssueBook", "DeleteIssueBookAccessionNum", new mIssueBook { IssueBookID = Id });
                Helper.ExecuteService("IssueBook", "DeleteIssueBook", new mIssueBook { IssueBookID = Id });
                ViewBag.Msg = "Sucessfully deleted issue book";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            catch (Exception)
            {
                
                throw;
            }


            ViewBag.Page = "Home";
            var modelList = (List<mIssueBook>)Helper.ExecuteService("IssueBook", "GetAllIssueBookDetails", null);
            return PartialView("/Areas/Library/Views/IssueBook/_IssueBookList.cshtml", modelList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveIssueBook(FormCollection collection)
        {
            try
            {
                int FinalIssueBookId = 0;
                int IssueBookID = 0;
                int.TryParse(collection["IssueBookID"], out IssueBookID);

                string paidMemberDesignationLabel = collection["paidMemberDesignationLabel"];                

                int PaidMemberId = 0;
                int.TryParse(collection["PaidMemberId"], out PaidMemberId);

                int PaidMemberIdSelected = 0;
                int.TryParse(collection["PaidMemberIdSelected"], out PaidMemberIdSelected);

                string memDesigcode = collection["SubscriberDesignation"];
                string DesignationType = collection["designationType"];

                int SubscriberId = 0;
                int.TryParse(collection["SubscriberId"], out SubscriberId);

                int PaidMemberIdname = 0;
                int.TryParse(collection["PaidMemberIdname"], out PaidMemberIdname);

                DateTime DateOfIssue = new DateTime();
                DateTime.TryParse(collection["DateOfIssue"], out DateOfIssue);

                //int a = Convert.ToInt16(collection["SubscriberId"]);

                mIssueBook model = new mIssueBook();
                model.DateOfIssue = DateOfIssue;
                model.SubscriptionType = collection["SubscriptionType"];
                if (collection["SubscriptionType"] == "Paid")
                {                    
                    model.IsPaidMember = true;
                    model.SubscriberDesignationId = paidMemberDesignationLabel;
                    model.SubscriberId = PaidMemberIdSelected;
                    //model.PaidMemberId = PaidMemberId;
                    model.IsMember = false;
                }
                else
                {                    
                    model.IsPaidMember = false; 
                }
                
                model.Mode = collection["Mode"];

                if (DesignationType == "0")
                {
                    model.IsMember = true;
                    model.SubscriberDesignationId = memDesigcode;
                    model.SubscriberId = SubscriberId;
                }
                else if (DesignationType == "1")
                {
                    model.IsMember = false;
                    model.SubscriberDesignationId = memDesigcode;
                    model.SubscriberId = SubscriberId;
                }

                if (model.Mode == "Add")
                {
                    model.CreatedBy = CurrentSession.UserName;
                    model.ModifiedBy = CurrentSession.UserName;
                    FinalIssueBookId = (int)Helper.ExecuteService("IssueBook", "CreateIssueBook", model);
                    ViewBag.Msg = "Sucessfully Issue Book";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                else
                {
                    FinalIssueBookId = model.IssueBookID = IssueBookID;
                    model.ModifiedBy = CurrentSession.UserName;
                    Helper.ExecuteService("IssueBook", "UpdateIssueBook", model);
                    ViewBag.Msg = "Sucessfully updated Issue Book";
                    ViewBag.Class = "alert alert-info";
                    ViewBag.Notification = "Success";
                }
                if (FinalIssueBookId > 0)
                {
                    string PCRow = collection["AccesionNumRow"];
                    Helper.ExecuteService("IssueBook", "DeleteIssueBookAccessionNum", new mIssueBook { IssueBookID = FinalIssueBookId });
                    if (PCRow != null && PCRow.Length > 0)
                    {
                        string[] RowCount = PCRow.Split(',');
                        StringBuilder AccessNumberSB= new StringBuilder();
                        foreach (var val in RowCount)
                        {
                            if (!string.IsNullOrEmpty(collection["AccessNumber-" + val]))
                            {
                                mIssueBookAccessionNum obj = new mIssueBookAccessionNum();
                                obj.AccessNumber = collection["AccessNumber-" + val];
                                obj.IssueBookId = FinalIssueBookId;
                                obj.DateOfReturn = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                                Helper.ExecuteService("IssueBook", "CreateAccessionNumber", obj);
                                
                                AccessNumberSB.Append(obj.AccessNumber+",");
                            }
                        }
                        mIssueBook IssueModel = (mIssueBook)Helper.ExecuteService("IssueBook", "GetIssueBookById", new mIssueBook { IssueBookID = FinalIssueBookId });
                        List<string> mobileList = new List<string>();
                        mobileList.Add(IssueModel.Mobile);
                        string msgBody = AccessNumberSB.ToString();
                        SendEmailDetails(null,   mobileList,   msgBody,string.Empty,string.Empty, true, false, string.Empty);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            ViewBag.Page = "Home";
            var modelList = (List<mIssueBook>)Helper.ExecuteService("IssueBook", "GetAllIssueBookDetails", null);
            return PartialView("/Areas/Library/Views/IssueBook/_IssueBookList.cshtml", modelList);
        }


        [HttpPost]
        public PartialViewResult SaveReturnBook(FormCollection collection)
        {
            string SubscriberDesignation = collection["SubscriberDesignation"];
            string MemberCode = collection["MemberCode"];
            try
            {
                string PCRow = collection["RowCount"];
                if (PCRow != null && PCRow.Length > 0)
                {
                    string[] RowCount = PCRow.Split(',');
                    int AccessionNumID = 0;
                    DateTime DateOfReturn = new DateTime();

                    foreach (var val in RowCount)
                    {
                        int.TryParse(collection["AccessionNumID-" + val], out AccessionNumID);
                        DateTime.TryParse(collection["DateOfReturn-" + val], out DateOfReturn);
                        var obj = (mIssueBookAccessionNum)Helper.ExecuteService("IssueBook", "GetAccessionNumById", new mIssueBookAccessionNum { IssueBookAccessionNumID = AccessionNumID });
                        if (obj != null)
                        {
                            if (DateOfReturn.ToShortDateString() == "01/01/0001")
                                obj.DateOfReturn = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                            else
                                obj.DateOfReturn = DateOfReturn;
                            Helper.ExecuteService("IssueBook", "UpdateAccessionNum", obj);
                            ViewBag.Msg = "Sucessfully updated return Book";
                            ViewBag.Class = "alert alert-info";
                            ViewBag.Notification = "Success";
                        }
                        else
                        {
                            ViewBag.Msg = "Oops... something wrong happened.";
                            ViewBag.Class = "alert alert-danger";
                            ViewBag.Notification = "Warning";
                        }
                    }
                }


            }
            catch (Exception)
            {

                throw;
            }

            ViewBag.SubscriberDesignation = SubscriberDesignation;
            ViewBag.MemberCode = MemberCode;
            StringBuilder sb = new StringBuilder();
            sb.Append(SubscriberDesignation + ",");
            sb.Append(MemberCode + ",");
            var MemberCol = (List<mIssueBookAccessionNum>)Helper.ExecuteService("IssueBook", "SearchAccesionNum", sb.ToString());
            return PartialView("/Areas/Library/Views/IssueBook/_AccesionNumberList.cshtml", MemberCol);
        }

        public ActionResult IssueBookReport()
        {
            string url = "/LibraryPdf/" + "IssueReturnBookPdf/";

            string directory = Server.MapPath(url);
            if (Directory.Exists(directory))
            {
                string[] filePaths = Directory.GetFiles(directory);
                foreach (string filePath in filePaths)
                {
                    System.IO.File.Delete(filePath);
                }
            }
            ViewBag.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("IssueBook", "DesignationOfMemberWhomBookIssued", null);
            ViewBag.MemberCol = (List<mMember>)Helper.ExecuteService("RoadPermit", "GetMemberListByDesignation", "0");
            return View();
        }

        public PartialViewResult SearchIssueBookResult(string MemberCode, string IssueDateFrom, string IssueDateTo, string AccessNumber, string ReturnDateFrom, string ReturnDateTo, string SubscriberDesignation, string designationType)
        {
            ViewBag.Page = "Report";

            Session["IssueBookList"] = null;
            Session["IssuBookHtml"] = null;
            var modelList = Session["IssueBookList"] = GetIssueBookList(MemberCode, IssueDateFrom, IssueDateTo, AccessNumber, ReturnDateFrom, ReturnDateTo, SubscriberDesignation, designationType);
            return PartialView("/Areas/Library/Views/IssueBook/_IssueBookList.cshtml", modelList);
        }


        [HttpPost]
        public ActionResult DownloadExcel(string MemberCode, string IssueDateFrom, string IssueDateTo, string AccessNumber, string ReturnDateFrom, string ReturnDateTo, string SubscriberDesignation, string designationType)
        {
            string Result = string.Empty;

            if (Session["IssuBookHtml"] != null)
                Result = Session["IssuBookHtml"].ToString();
            else
            {
                List<mIssueBook> model = new List<mIssueBook>();

                if (Session["IssueBookList"] != null)
                    model = (List<mIssueBook>)Session["IssueBookList"];
                else
                    Session["IssueBookList"] = model = GetIssueBookList(MemberCode, IssueDateFrom, IssueDateTo, AccessNumber, ReturnDateFrom, ReturnDateTo, SubscriberDesignation,designationType);

                Result = GetIssueBookHtml(model);
                Session["IssuBookHtml"] = Result;
            }
            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=IssueReturnBookList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }
        public ActionResult DownloadPdf(string MemberCode, string IssueDateFrom, string IssueDateTo, string AccessNumber, string ReturnDateFrom, string ReturnDateTo, string SubscriberDesignation, string designationType)
        {
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);
            string path = "";
            try
            {
                string Result = string.Empty;



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_IssueReturnBookList.pdf";


                if (Session["IssuBookHtml"] != null)
                    Result = Session["IssuBookHtml"].ToString();
                else
                {
                    List<mIssueBook> model = new List<mIssueBook>();

                    if (Session["IssueBookList"] != null)
                        model = (List<mIssueBook>)Session["IssueBookList"];
                    else
                        Session["IssueBookList"] = model = GetIssueBookList(MemberCode, IssueDateFrom, IssueDateTo, AccessNumber, ReturnDateFrom, ReturnDateTo, SubscriberDesignation, designationType);

                    Result = GetIssueBookHtml(model);
                    Session["IssuBookHtml"] = Result;
                }
                MemoryStream output = new MemoryStream();



                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                           PdfPageOrientation.Portrait);

                string htmlStringToConvert = Result;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);


                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/LibraryPdf/" + "IssueReturnBookPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "IssueReturnBookList.pdf";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                document1.Close();
                // System.IO.File.Delete(path);
            }

          
#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }

        /// <summary>
        /// this is for getting html issue book list
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string GetIssueBookHtml(List<mIssueBook> model)
        {

            StringBuilder IssueBookResport = new StringBuilder();


            if (model != null && model.Count() > 0)
            {
                IssueBookResport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'><h2 style='text-align:center'>Issue Return Report for the Books</h2>");
                IssueBookResport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Issue Sr. No.</th>");
                IssueBookResport.Append("<th style='width:150px;text-align:left; ' >Subscription Type</th><th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Subscription's Designation</th><th style='width:150px;text-align:left;'>Subscription's Name</th>");
                IssueBookResport.Append("<th style='width:150px;text-align:left;' >Date of Issue</th><th style='width:150px;text-align:left;'>Accession Number</th><th style='width:150px;text-align:left;'>Date of Return</th>");

                IssueBookResport.Append("</tr></thead>");


                IssueBookResport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    IssueBookResport.Append("<tr>");
                    IssueBookResport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.IssueBookID));
                    IssueBookResport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.SubscriptionType));
                    IssueBookResport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.SubscriberDesignation));
                    IssueBookResport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}. {1}</td>", obj.Prefix, obj.SubscriberName));
                    IssueBookResport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.DateOfIssue.ToString("dd/MM/yyyy")));
                    var AccessionNumList = (List<mIssueBookAccessionNum>)Helper.ExecuteService("IssueBook", "GetAllAccessNumDetails", obj.IssueBookID.ToString());
                    if (AccessionNumList != null && AccessionNumList.Count() > 0)
                    {
                        IssueBookResport.Append("<td style='border: 1px dotted #808080;'><table>");
                        foreach (var item in AccessionNumList)
                        {
                            IssueBookResport.Append(string.Format("<tr><td >{0}</td></tr>", item.AccessNumber));
                        }
                        IssueBookResport.Append("</table></td>");

                        IssueBookResport.Append("<td style='border: 1px dotted #808080;'><table>");
                        foreach (var item in AccessionNumList)
                        {
                            if (item.DateOfReturn.ToString("dd/MM/yyyy").Equals("01/01/1753"))
                            {
                                IssueBookResport.Append("<tr><td >NA</td></tr>");
                            }
                            else
                            {
                                IssueBookResport.Append(string.Format("<tr><td >{0}</td></tr>", item.DateOfReturn.ToString("dd/MM/yyyy")));
                            }

                        }
                        IssueBookResport.Append("</table></td>");
                    }
                    else
                    {
                        IssueBookResport.Append("  <td style='width: 10%'>NA</td>  <td style='width: 10%'>NA</td>");
                    }

                    IssueBookResport.Append("</tr>");
                }
                IssueBookResport.Append("</tbody></table></div> ");
            }
            else
            {
                IssueBookResport.Append("No records available");
            }
            return IssueBookResport.ToString();

        }


        public static List<mIssueBook> GetIssueBookList(string MemberCode, string IssueDateFrom, string IssueDateTo, string AccessNumber, string ReturnDateFrom, string ReturnDateTo, string SubscriberDesignation, string designationType)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(MemberCode))
                sb.Append(MemberCode + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(IssueDateFrom))
                sb.Append(IssueDateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(IssueDateTo))
                sb.Append(IssueDateTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(AccessNumber))
                sb.Append(AccessNumber + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(ReturnDateFrom))
                sb.Append(ReturnDateFrom + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(ReturnDateTo))
                sb.Append(ReturnDateTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(SubscriberDesignation))
                sb.Append(SubscriberDesignation + ",");
            else
                sb.Append(" " + ",");
            if (designationType == "0")
            {
                // Call Split method
                return (List<mIssueBook>)Helper.ExecuteService("IssueBook", "SearchIssueBook", sb.ToString());
            }
            if (designationType == "1") {
                return (List<mIssueBook>)Helper.ExecuteService("IssueBook", "SearchIssueBookForStaff", sb.ToString());
            
            }
            if (designationType == "2") {
                return (List<mIssueBook>)Helper.ExecuteService("IssueBook", "SearchIssueBookForPaid", sb.ToString());
            
            }
            return null;
        }
        
        #region Send Email
        public static void SendEmailDetails(List<string> emailList, List<string> mobileList, string msgBody, string mailSubject, string mailBody, bool mobileStatus, bool emailStatus, string SavedPdfPath)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();

            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (emailList != null)
            {
                emailAddresses.AddRange(emailList);

            }
            if (mobileList != null)
            {
                phoneNumbers.AddRange(mobileList);
            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            smsMessage.SMSText = msgBody;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            emailMessage._isBodyHtml = true;
            emailMessage._subject = mailSubject;
            emailMessage._body = mailBody;
            if (!string.IsNullOrEmpty(SavedPdfPath))
            {
                EAttachment ea = new EAttachment { FileName = new FileInfo(SavedPdfPath).Name, FileContent = System.IO.File.ReadAllBytes((SavedPdfPath)) };
                emailMessage._attachments = new List<EAttachment>();
                emailMessage._attachments.Add(ea);
            }
            try
            {
                if (emailList != null || mobileList != null)
                {
                    Notification.Send(mobileStatus, emailStatus, smsMessage, emailMessage);
                }

            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                // throw;
            }

            #endregion SMS And Email
        }

        #endregion

        public ActionResult CheckAccessionNoOfBookForIssue(string AccsnNO)
        {
            var CheckAccessionNo = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "AccessionNumberCheck", AccsnNO);
            if (CheckAccessionNo != null)
            {
                var value = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "GetBookInfoByIdForUpdate", AccsnNO);
                return Content(value.TitleOfBook);
            }
            else {
                return Content("nodatafound");
            }            
        }
    }
}
