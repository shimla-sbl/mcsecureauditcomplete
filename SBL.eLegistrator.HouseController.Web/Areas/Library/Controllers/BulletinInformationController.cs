﻿using SBL.DomainModel.Models.LibraryVS;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class BulletinInformationController : Controller
    {
        //
        // GET: /Library/BulletinInformation/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddBulletinInfoPage()
        {
            try
            {
                VsLibraryBulletin model = new VsLibraryBulletin();
                //mSession mSessionModel = new mSession();
                //mAssembly mAssemblymodel = new mAssembly();
                //model.mSession = (ICollection<mSession>)Helper.ExecuteService("Session", "GetAllSessionData", mSessionModel);
                //model.mAssembly = (ICollection<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssembly", mAssemblymodel);
                //var Content1 = (VsLibraryBulletin)Helper.ExecuteService("VsLibraryBulletin", "AssemblyAndSession", null);

                //Code for AssemblyList 
                model.assemblylist = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
                data.AssemblyID = model.assemblylist.FirstOrDefault().AssemblyCode;
                
                model.sessionlist = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                var a = model.sessionlist.FirstOrDefault().SessionCode;
                data.SessionCode = a;
                DateTime? strtdate =(DateTime?)Helper.ExecuteService("Session", "sessionstartdate", data);
                DateTime? enddate = (DateTime?)Helper.ExecuteService("Session", "sessionEnddate", data);                
               
                model.StartDate = strtdate.Value.ToShortDateString();
                model.EndDate = enddate.Value.ToShortDateString();
                return View(model);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bulletin form page";
                return View("_LibraryErrorPage");
            }

        }

        [HttpPost]
        public ActionResult SaveBulletinInfo(VsLibraryBulletin model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var CheckSessionNO = (VsLibraryBulletin)Helper.ExecuteService("VsLibraryBulletin", "SessionCheck", model);
                    if (CheckSessionNO == null)
                    {
                        var Content1 = (VsLibraryBulletin)Helper.ExecuteService("VsLibraryBulletin", "SaveBulletinInfo", model);

                        return Content("1");
                    }
                    else
                    {
                        return Content("0");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "error");
                    return Content("2");//Failure
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Bulletin information";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult UpdateBulletinInfoPage()
        {
            try
            {
                VsLibraryBulletin model = new VsLibraryBulletin();
                //Code for AssemblyList 
                model.assemblylist = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
                data.AssemblyID = model.assemblylist.FirstOrDefault().AssemblyCode;

                model.sessionlist = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                var a = model.sessionlist.FirstOrDefault().SessionCode;
                data.SessionCode = a;
                DateTime? strtdate = (DateTime?)Helper.ExecuteService("Session", "sessionstartdate", data);
                DateTime? enddate = (DateTime?)Helper.ExecuteService("Session", "sessionEnddate", data);

                model.StartDate = strtdate.Value.ToShortDateString();
                model.EndDate = enddate.Value.ToShortDateString();
                return View(model);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bulletin form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult UpdateWithSerialNo(int id)
        {
            try
            {
                VsLibraryBulletin model = new VsLibraryBulletin();
                SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
                model = (VsLibraryBulletin)Helper.ExecuteService("VsLibraryBulletin", "UpdateWithSerialNo", id);

                model.assemblylist = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);
                data.AssemblyID = model.assemblylist.FirstOrDefault().AssemblyCode;

                model.sessionlist = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);

                //var a = model.sessionlist.FirstOrDefault().SessionCode;
                //data.SessionCode = a;
                return PartialView("_UpdateBulletinPage", model);
            }
            catch (Exception)
            {
                
                throw;
            }
            
        }

        public ActionResult SearchBulletinForUpdate(VsLibraryBulletin model, string ButtonType)
        {
            try
            {                
                if (ButtonType == "Search"||ButtonType==null)
                {
                    var UpdationList = (List<VsLibraryBulletin>)Helper.ExecuteService("VsLibraryBulletin", "SearchBulletinForUpdate", model);
                    if (UpdationList.Count == 0)
                    {
                        if (ButtonType == null) {
                            return PartialView("_BulletinListForUpdate",UpdationList);
                        }
                        return Content("3null");
                    }
                    return PartialView("_BulletinListForUpdate", UpdationList);
                }
                if (ModelState.IsValid)
                {
                    if (ButtonType == "update")
                    {
                        var chksession = (VsLibraryBulletin)Helper.ExecuteService("VsLibraryBulletin", "SessionCheck", model);
                        if (chksession == null)
                        {
                            var CheckSessionNO = (VsLibraryBulletin)Helper.ExecuteService("VsLibraryBulletin", "SaveBulletinInfo", model);
                            return Content("3UpdateSuccess");
                        }
                        else {
                            return Content("SessionExists");
                        }
                    }
                }
                else
                {
                    return Content("2null");
                }

                return RedirectToAction("UpdateBulletinInfoPage", "BulletinInformation");

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bulletin form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult SearchBulletin(VsLibraryBulletin model)
        {
            try
            {
                var data = (List<VsLibraryBulletin>)Helper.ExecuteService("VsLibraryBulletin", "SearchBulletin", model);
                if (data.Count == 0)
                {
                    return Content("3null");
                }
                return PartialView("_BulletinListForSearch", data);

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bulletin form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult SearchLocation(VsLibraryBulletin model)
        {
            try
            {
                var data = (List<VsLibraryBulletin>)Helper.ExecuteService("VsLibraryBulletin", "SearchLocation", model);
                if (data.Count == 0)
                {
                    return Content("3null");
                }
                return PartialView("_BulletinListOfLocation",data);

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bulletin form page";
                return View("_LibraryErrorPage");
            }
        }


        [HttpPost]
        public ActionResult SaveUpdatedBulletinInfo(VsLibraryBulletin model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //int SessionNO = model.Session;
                    var CheckSessionNO = (VsLibraryBulletin)Helper.ExecuteService("VsLibraryBulletin", "SessionCheck", model);
                    if (CheckSessionNO == null)
                    {
                        var Content1 = (VsLibraryBulletin)Helper.ExecuteService("VsLibraryBulletin", "SaveBulletinInfo", model);

                        return Content("1");
                    }
                    else
                    {
                        //TempData["Message"] = "Accession no already exit please change the accession number";
                        return Content("0");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "error");
                    return Content("2");//Failure
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Bulletin information";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult SearchBulletinInfoPage()
        {
            try
            {
                VsLibraryBulletin model = new VsLibraryBulletin();
                model.assemblylist = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
                data.AssemblyID = model.assemblylist.FirstOrDefault().AssemblyCode;

                //model.sessionlist = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                //var a = model.sessionlist.FirstOrDefault().SessionCode;
                //data.SessionCode = a;
               // DateTime? strtdate = (DateTime?)Helper.ExecuteService("Session", "sessionstartdate", data);
                //DateTime? enddate = (DateTime?)Helper.ExecuteService("Session", "sessionEnddate", data);

                //model.StartDate = strtdate.Value.ToShortDateString();
                //model.EndDate = enddate.Value.ToShortDateString();
               
                return View(model);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bulletin form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult LocateBulletinInfoPage()
        {
            try
            {
                VsLibraryBulletin model = new VsLibraryBulletin();
                //Code for AssemblyList 
                model.assemblylist = (List<SBL.DomainModel.Models.Assembly.mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssemblyReverse", null);

                SBL.DomainModel.Models.Session.mSession data = new SBL.DomainModel.Models.Session.mSession();
                data.AssemblyID = model.assemblylist.FirstOrDefault().AssemblyCode;

                model.sessionlist = (List<SBL.DomainModel.Models.Session.mSession>)Helper.ExecuteService("Session", "GetSessionsByAssemblyID", data);
                var a = model.sessionlist.FirstOrDefault().SessionCode;
                data.SessionCode = a;
                DateTime? strtdate = (DateTime?)Helper.ExecuteService("Session", "sessionstartdate", data);
                DateTime? enddate = (DateTime?)Helper.ExecuteService("Session", "sessionEnddate", data);

                model.StartDate = strtdate.Value.ToShortDateString();
                model.EndDate = enddate.Value.ToShortDateString();
                return View(model);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bulletin form page";
                return View("_LibraryErrorPage");
            }
        }
    }
}
