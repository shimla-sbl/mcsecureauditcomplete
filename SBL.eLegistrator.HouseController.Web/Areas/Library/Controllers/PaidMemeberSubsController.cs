﻿using SBL.DomainModel.Models.PaidMemeberSubscription;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Text;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Models;
using System.IO;
using EvoPdf;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PaidMemeberSubsController : Controller
    {
        //
        // GET: /Library/PaidMemeberSubs/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                
                   

                var State = (List<PaidMemeberSubs>)Helper.ExecuteService("PaidMemeberSubs", "GetAllPaidMemeberSubsDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult SubscriptionReport()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                string url = "/LibraryPdf/" + "PaidMemberSubsPdf/";

                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                var State = (List<PaidMemeberSubs>)Helper.ExecuteService("PaidMemeberSubs", "GetAllPaidMemeberSubsDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult CreatePaidMemeberSubs()
        {
            PaidMemeberSubsViewModel model = new PaidMemeberSubsViewModel();
            model.Mode = "Add";
            return View(model);
        }


        //[HttpPost]
        //public ActionResult ConvertAboutPageToPdf(string htmlToConvert)
        //{

        //    // the base URL to resolve relative images and css
        //    String thisPageUrl = this.ControllerContext.HttpContext.Request.Url.AbsoluteUri;
        //    String baseUrl = thisPageUrl.Substring(0, thisPageUrl.Length - "Home/ConvertAboutPageToPdf".Length);

        //    // instantiate the HiQPdf HTML to PDF converter
        //    HtmlToPdf htmlToPdfConverter = new HtmlToPdf();

        //    // render the HTML code as PDF in memory
        //    byte[] pdfBuffer = htmlToPdfConverter.ConvertHtmlToMemory(htmlToConvert, baseUrl);

        //    // send the PDF file to browser
        //    FileResult fileResult = new FileContentResult(pdfBuffer, "application/pdf");
        //    fileResult.FileDownloadName = "AboutMvcViewToPdf.pdf";

        //    return fileResult;
        //}



        [HttpPost]
        public ActionResult DownloadExcel(string SubscriberName, string Designation, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            string Result = string.Empty;


            Result = GetPaidMemberSubsList(SubscriberName, Designation, StartDateFrom, StartDateTo, EndDateFrom, EndDateTo);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=PaidMemberSubs.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }
        public ActionResult DownloadPdf(string SubscriberName, string Designation, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);
            string path = "";
            try
            {
                string Result = string.Empty;



                Guid FId = Guid.NewGuid();
                string fileName =FId+"_PaidMemberSubs.pdf";


                Result = GetPaidMemberSubsList(SubscriberName, Designation, StartDateFrom, StartDateTo, EndDateFrom, EndDateTo);

                MemoryStream output = new MemoryStream();

              

                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                           PdfPageOrientation.Portrait);

                string htmlStringToConvert = Result;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);


                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/LibraryPdf/" + "PaidMemberSubsPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                 string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "PaidMemberSubs.pdf";
                   
                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                document1.Close();
               // System.IO.File.Delete(path);
            }
          
          
#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }


        public static string GetPaidMemberSubsList(string SubscriberName, string Designation, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            List<PaidMemeberSubsViewModel> model = new List<PaidMemeberSubsViewModel>();
            StringBuilder PaidMemberReport = new StringBuilder();

            if (string.IsNullOrEmpty(SubscriberName) && string.IsNullOrEmpty(Designation) && string.IsNullOrEmpty(StartDateFrom) && string.IsNullOrEmpty(StartDateTo) &&
                string.IsNullOrEmpty(EndDateFrom) && string.IsNullOrEmpty(EndDateTo))
            {
                var State = (List<PaidMemeberSubs>)Helper.ExecuteService("PaidMemeberSubs", "GetAllPaidMemeberSubsDetails", null);
                model = State.ToViewModel();
            }
            else
                model = GetPaidMemberList(SubscriberName, Designation, StartDateFrom, StartDateTo, EndDateFrom, EndDateTo);




            if (model != null && model.Count() > 0)
            {
                PaidMemberReport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                PaidMemberReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th>");
                PaidMemberReport.Append("<th style='width:150px;text-align:left; ' >Name of subscriber</th><th style='width:150px;text-align:left;'>Address</th><th style='width:150px;text-align:left;'>Mobile Number</th>");
                PaidMemberReport.Append("<th style='width:150px;text-align:left;' >Designation</th><th style='width:150px;text-align:left;'>Subscription Start Date</th><th style='width:150px;text-align:left;' >Subscription End Date</th><th >Remarks</th>");

                PaidMemberReport.Append("</tr></thead>");


                PaidMemberReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    PaidMemberReport.Append("<tr>");
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.NameOfSubscriber));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Address));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.MobileNumber));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Designation));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.SubsStartDate.ToString("dd/MM/yyyy")));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.SubsEndDate.ToString("dd/MM/yyyy")));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Remarks));
                    PaidMemberReport.Append("</tr>");
                }
                PaidMemberReport.Append("</tbody></table></div> ");
            }
            else
            {
                PaidMemberReport.Append("No records available");
            }
            return PaidMemberReport.ToString();

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SavePaidMemeberSubs(PaidMemeberSubsViewModel model)
        {
            if (ModelState.IsValid)
            {
                Guid FileName = Guid.NewGuid();
                var PaidMemeberSubs = model.ToDomainModel();
                Helper.ExecuteService("PaidMemeberSubs", "CreatePaidMemeberSubs", PaidMemeberSubs);
            }

            return RedirectToAction("Index");
        }


        public PartialViewResult SearchPaidMemeberSubs(string SubscriberName, string Designation, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            //  PaidMemeberSubsController obj = new PaidMemeberSubsController();
            var model = GetPaidMemberList(SubscriberName, Designation, StartDateFrom, StartDateTo, EndDateFrom, EndDateTo);
            return PartialView("/Areas/Library/Views/PaidMemeberSubs/_PaidMemberSubList.cshtml", model);


        }

        public static List<PaidMemeberSubsViewModel> GetPaidMemberList(string SubscriberName, string Designation, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(SubscriberName))
                sb.Append(SubscriberName + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Designation))
                sb.Append(Designation + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(StartDateFrom))
                sb.Append(StartDateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(StartDateTo))
                sb.Append(StartDateTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(EndDateFrom))
                sb.Append(EndDateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(EndDateTo))
                sb.Append(EndDateTo + ",");
            else
                sb.Append(" " + ",");


            // Call Split method
            var State = (List<PaidMemeberSubs>)Helper.ExecuteService("PaidMemeberSubs", "SearchPaidMemeberSubs", sb.ToString());
            var model = State.ToViewModel();
            return model;

        }

    }
}
