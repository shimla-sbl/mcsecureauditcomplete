﻿using SBL.DomainModel.Models.ItemSubscription;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Models;
using System.Text;
using SBL.eLegistrator.HouseController.Web.Utility;
using EvoPdf;
using System.IO;
namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class ItemSubsDeliveryController : Controller
    {
        //
        // GET: /Library/ItemSubsDelivery/

        public ActionResult Index()
        {
            try
            {

                var State = (List<mItemSubsDelivery>)Helper.ExecuteService("ItemSubscription", "GetAllItemSubsDeliveryDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                ViewBag.Page = "Home";

                ViewBag.ItemTypeList = ModelMapping.GetItemTypeList();
                // model.FrequencyList = ModelMapping.GetFrequencyList();
                ViewBag.LanguageList = ModelMapping.GetLanguageList();
                return View(State);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult GetItemSubsList(string ItemType, string Language)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(ItemType + ",");
            sb.Append(Language);
            var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "SearchItemSubsForDelivery", sb.ToString());
            ViewBag.ItemSubscriptionList = new SelectList(State, "ItemSubscriptionID", "ItemName");
            return PartialView("/Areas/Library/Views/ItemSubsDelivery/_AddItemDelivery.cshtml");
        }

        public PartialViewResult EditItemDelievery(int Id)
        {
            var State = (mItemSubsDelivery)Helper.ExecuteService("ItemSubscription", "GetItemSubsDeliveryById", new mItemSubsDelivery { ItemSubsDeliveryID = Id });
            return PartialView("/Areas/Library/Views/ItemSubsDelivery/_EditItemDelivery.cshtml", State);
        }

        public JsonResult GetItemSubscriptionDetail(int Id)
        {
            mItemSubscription model = (mItemSubscription)Helper.ExecuteService("ItemSubscription", "GetItemSubscriptionById", new mItemSubscription { ItemSubscriptionID = Id });
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public PartialViewResult SaveItemDelievery(int ItemName, int QuantityRecieved, string Year, DateTime RecivedDate, string Building, string Floor, string Almirah, string Rack, string Mode, int ItemSubsDeliveryID,string Volume,string Number,int month)
        {
            mItemSubsDelivery model = new mItemSubsDelivery();
            ViewBag.Page = "Home";
            if (ModelState.IsValid)
            {
                try
                {

                    if (Mode == "Add")
                    {
                        model.ItemSubscriptionId = ItemName;
                        model.QuantityReceived = QuantityRecieved;
                        model.Year = Year;
                        model.DateOfReceiving = RecivedDate;
                        model.Building = Building;
                        model.Floor = Floor;
                        model.Almirah = Almirah;
                        model.Rack = Rack;
                        model.CreatedBy = CurrentSession.UserName;
                        model.ModifiedBy = CurrentSession.UserName;
                        model.Volume = Volume;
                        model.Month = month;
                        model.Number = Number;
                        Helper.ExecuteService("ItemSubscription", "CreateItemSubsDelivery", model);
                        ViewBag.Msg = "Sucessfully saved delivery and placement details";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                    else
                    {

                        var State = (mItemSubsDelivery)Helper.ExecuteService("ItemSubscription", "GetItemSubsDeliveryById", new mItemSubsDelivery { ItemSubsDeliveryID = ItemSubsDeliveryID });
                        // State.ItemSubscriptionId = ItemName;
                        State.QuantityReceived = QuantityRecieved;
                        State.Year = Year;
                        State.DateOfReceiving = RecivedDate;
                        State.Building = Building;
                        State.Floor = Floor;
                        State.Almirah = Almirah;
                        State.Rack = Rack;
                        State.Volume = Volume;
                        State.Month = month;
                        State.Number = Number;
                        State.ModifiedBy = CurrentSession.UserName;
                        Helper.ExecuteService("ItemSubscription", "UpdateItemSubsDelivery", State);
                        ViewBag.Msg = "Sucessfully updated delivery and placement details";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                }
                catch (Exception ex)
                {

                    ViewBag.Msg = ex.InnerException.ToString();
                    ViewBag.Class = "alert alert-danger";
                    ViewBag.Notification = "Error";
                }

            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }

            var modelList = (List<mItemSubsDelivery>)Helper.ExecuteService("ItemSubscription", "GetAllItemSubsDeliveryDetails", null);

            return PartialView("/Areas/Library/Views/ItemSubsDelivery/_ItemDeliveryList.cshtml", modelList);
        }

        public PartialViewResult DeleteItemSubsDelivery(int Id)
        {
            ViewBag.Page = "Home";
            if (Id > 0)
            {
                var State = (mItemSubsDelivery)Helper.ExecuteService("ItemSubscription", "GetItemSubsDeliveryById", new mItemSubsDelivery { ItemSubsDeliveryID = Id });

                Helper.ExecuteService("ItemSubscription", "DeleteItemSubsDelivery", State);
                ViewBag.Msg = "Delivery and placement details of the item Subscribed deleted sucessfully";
                ViewBag.Class = "alert alert-info";
                ViewBag.Notification = "Success";
            }
            else
            {
                ViewBag.Msg = "Unable to delete";
                ViewBag.Class = "alert alert-danger";
                ViewBag.Notification = "Error";
            }
            var modelList = (List<mItemSubsDelivery>)Helper.ExecuteService("ItemSubscription", "GetAllItemSubsDeliveryDetails", null);

            return PartialView("/Areas/Library/Views/ItemSubsDelivery/_ItemDeliveryList.cshtml", modelList);
        }


        #region Report Purpose


        public ActionResult ItemSubsDeliveryReport()
        {
            try
            {
                Session["SearchedData"] = null;
                string url = "/LibraryPdf/" + "ItemSubsDelieveryPdf/";

                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                ViewBag.Page = "Report";

                ViewBag.ItemTypeList = ModelMapping.GetItemTypeList();
                //ViewBag.FrequencyList = ModelMapping.GetFrequencyList();
                ViewBag.LanguageList = ModelMapping.GetLanguageList();
                return View();
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }


        [HttpPost]
        public ActionResult DownloadExcel(string NameofItem, string ItemType, string Language, string StartDateFrom, string StartDateTo)
        {
            string Result = string.Empty;

            if (Session["SearchedData"] != null)
                Result = Session["SearchedData"].ToString();
            else
                Session["SearchedData"] = Result = GetItemSubsString(NameofItem, ItemType, Language, StartDateFrom, StartDateTo);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ItemSubsDeliveryList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }
        public ActionResult DownloadPdf(string NameofItem, string ItemType, string Language, string StartDateFrom, string StartDateTo)
        {
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);
            string path = "";
            try
            {
                string Result = string.Empty;



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_ItemSubscDeliveryList.pdf";

                if (Session["SearchedData"] != null)
                    Result = Session["SearchedData"].ToString();
                else
                    Session["SearchedData"] = Result = GetItemSubsString(NameofItem, ItemType, Language, StartDateFrom, StartDateTo);
              
                MemoryStream output = new MemoryStream();



                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                           PdfPageOrientation.Portrait);

                string htmlStringToConvert = Result;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);


                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/LibraryPdf/" + "ItemSubsDelieveryPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "ItemSubsDeliveryList.pdf";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                document1.Close();
                // System.IO.File.Delete(path);
            }


#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }


        public static string GetItemSubsString(string NameofItem, string ItemType, string Language, string StartDateFrom, string StartDateTo)
        {
            List<mItemSubsDelivery> model = new List<mItemSubsDelivery>();
            StringBuilder PaidMemberReport = new StringBuilder();

            model = GetItemSubsDeliveryList(NameofItem, ItemType, Language, StartDateFrom, StartDateTo);




            if (model != null && model.Count() > 0)
            {
                PaidMemberReport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><h2 style='text-align:left;'>Delivery Report of items Subscribed by the Library</h2><table class='table table-condensed table-bordered'  id='ResultTable'>");
                PaidMemberReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th>");
                PaidMemberReport.Append("<th style='width:150px;text-align:left; ' >Name of Item</th><th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Item Type</th><th style='width:150px;text-align:left;'>Language</th>");
                PaidMemberReport.Append("<th style='width:150px;text-align:left;' >Quantity/Frequency</th><th style='width:150px;text-align:left;'>Quantity Recerved</th>");
                PaidMemberReport.Append("<th style='width:150px;text-align:left;'>Year</th><th style='width:150px;text-align:left;'>Date of Receiving</th>");
                PaidMemberReport.Append("<th style='width:150px;text-align:left;'>Building</th><th style='width:150px;text-align:left;' >Floor</th><th style='width:150px;text-align:left;' >Almirah</th><th style='width:150px;text-align:left;'>Rack</th>");

                PaidMemberReport.Append("</tr></thead>");


                PaidMemberReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    PaidMemberReport.Append("<tr>");
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ItemName));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ItemType));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Language));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}/{1}</td>",obj.Quantity, obj.Frequancy));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.QuantityReceived));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Year));
                    
                    if (Convert.ToDateTime(obj.DateOfReceiving).ToString("dd/MM/yyyy").Equals("01/01/1753"))
                    {
                        PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "NA"));
                    }
                    else
                    {
                        PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.DateOfReceiving).ToString("dd/MM/yyyy")));
                    }
                   
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Building));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Floor));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Almirah));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Rack));
                    PaidMemberReport.Append("</tr>");
                }
                PaidMemberReport.Append("</tbody></table></div> ");
            }
            else
            {
                PaidMemberReport.Append("No records available");
            }
            return PaidMemberReport.ToString();

        }

        public PartialViewResult SearchmItemSubsDelievery(string NameofItem, string ItemType, string Language, string StartDateFrom, string StartDateTo)
        {
            //  mItemSubscriptionController obj = new mItemSubscriptionController();
            ViewBag.Page = "Report";
            Session["SearchedData"] = null;
            var model = GetItemSubsDeliveryList(NameofItem, ItemType, Language, StartDateFrom, StartDateTo);

            return PartialView("/Areas/Library/Views/ItemSubsDelivery/_ItemDeliveryList.cshtml", model);
        }

        public static List<mItemSubsDelivery> GetItemSubsDeliveryList(string NameofItem, string ItemType, string Language, string StartDateFrom, string StartDateTo)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(NameofItem))
                sb.Append(NameofItem + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(ItemType))
                sb.Append(ItemType + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Language))
                sb.Append(Language + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(StartDateFrom))
                sb.Append(StartDateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(StartDateTo))
                sb.Append(StartDateTo + ",");
            else
                sb.Append(" " + ",");



            // Call Split method
            return (List<mItemSubsDelivery>)Helper.ExecuteService("ItemSubscription", "SearchItemSubsDelivery", sb.ToString());



        }

        #endregion
    }
}
