﻿using EvoPdf;
using SBL.DomainModel.Models.ItemSubscription;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class ItemSubscriptionController : Controller
    {
        //
        // GET: /Library/ItemSubscription/

        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "GetAllItemSubscriptionDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                ViewBag.Page = "Home";
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public ActionResult ItemSubscriptionReport()
        {
            try
            {

                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                string url = "/LibraryPdf/" + "ItemSubscriptioPdf/";

                string directory = Server.MapPath(url);
                if (Directory.Exists(directory))
                {
                    string[] filePaths = Directory.GetFiles(directory);
                    foreach (string filePath in filePaths)
                    {
                        System.IO.File.Delete(filePath);
                    }
                }
                ViewBag.Page = "Report";
                var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "GetAllItemSubscriptionDetails", null);
                var model = State.ToViewModel();
                ViewBag.ItemTypeList = ModelMapping.GetItemTypeList();
                ViewBag.FrequencyList = ModelMapping.GetFrequencyList();
                ViewBag.LanguageList = ModelMapping.GetLanguageList();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult CreatemItemSubscription(int Id, string Action)
        {
            ItemSubscriptionViewModel model = new ItemSubscriptionViewModel();
            mItemSubscription stateToEdit = (mItemSubscription)Helper.ExecuteService("ItemSubscription", "GetItemSubscriptionById", new mItemSubscription { ItemSubscriptionID = Id });
            if (stateToEdit != null && Id>0)
                model = stateToEdit.ToViewModel1(Action);
            model.Mode = Action;
            model.ItemTypeList = ModelMapping.GetItemTypeList();
            model.FrequencyList = ModelMapping.GetFrequencyList();
            model.LanguageList = ModelMapping.GetLanguageList();

            return PartialView("/Areas/Library/Views/ItemSubscription/_CreateItemSubscription.cshtml", model);
        }


        [HttpPost]
        public ActionResult DownloadExcel(string NameofItem, string ItemType, string Language, string Frequency, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            string Result = string.Empty;


            Result = GetItemSubsString(NameofItem, ItemType, Language, Frequency, StartDateFrom, StartDateTo, EndDateFrom, EndDateTo);

            Result = HttpUtility.UrlDecode(Result);
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=ItemSubscriptionList.xls");
            Response.Charset = "";
            Response.ContentType = "application/excel";
            Response.Write(Result);
            Response.Flush();
            Response.End();
            return new EmptyResult();
        }
        public ActionResult DownloadPdf(string NameofItem, string ItemType, string Language, string Frequency, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            EvoPdf.Document document1 = new EvoPdf.Document();
            document1.LicenseKey = "vjAjMSQhMSAoMSQ/ITEiID8gIz8oKCgo";
            document1.CompressionLevel = PdfCompressionLevel.Best;
            document1.Margins = new Margins(0, 0, 0, 0);
            string path = "";
            try
            {
                string Result = string.Empty;



                Guid FId = Guid.NewGuid();
                string fileName = FId + "_ItemSubscriptionList.pdf";


                Result = GetItemSubsString(NameofItem, ItemType, Language, Frequency, StartDateFrom, StartDateTo, EndDateFrom, EndDateTo);

                MemoryStream output = new MemoryStream();



                EvoPdf.PdfPage page = document1.Pages.AddNewPage(PdfPageSize.A4, new Margins(0, 0, 40, 40),
                           PdfPageOrientation.Portrait);

                string htmlStringToConvert = Result;

                HtmlToPdfElement htmlToPdfElement = new HtmlToPdfElement(0, 0, 0, 0, htmlStringToConvert, "");

                AddElementResult addResult = page.AddElement(htmlToPdfElement);


                byte[] pdfBytes = document1.Save();

                output.Write(pdfBytes, 0, pdfBytes.Length);

                output.Position = 0;

                string url = "/LibraryPdf/" + "ItemSubscriptioPdf/";

                string directory = Server.MapPath(url);

                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }


                path = Path.Combine(Server.MapPath("~" + url), fileName);

                FileStream _FileStream = new FileStream(path, System.IO.FileMode.Create,
                System.IO.FileAccess.Write);

                _FileStream.Write(pdfBytes, 0, pdfBytes.Length);

                // close file stream
                _FileStream.Close();

                string contentType = "application/octet-stream";
                FilePathResult pathRes = null;
                if (System.IO.File.Exists(path))
                {
                    pathRes = new FilePathResult(path, contentType);
                    pathRes.FileDownloadName = "ItemSubscriptionList.pdf";

                }

                return pathRes;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                document1.Close();
                // System.IO.File.Delete(path);
            }


#pragma warning disable CS0162 // Unreachable code detected
            return new EmptyResult();
#pragma warning restore CS0162 // Unreachable code detected
        }


        public static string GetItemSubsString(string NameofItem, string ItemType, string Language, string Frequency, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            List<ItemSubscriptionViewModel> model = new List<ItemSubscriptionViewModel>();
            StringBuilder PaidMemberReport = new StringBuilder();

            if (string.IsNullOrEmpty(NameofItem) && string.IsNullOrEmpty(ItemType) && string.IsNullOrEmpty(Language) && string.IsNullOrEmpty(Frequency) &&
                string.IsNullOrEmpty(StartDateFrom) && string.IsNullOrEmpty(StartDateTo) &&
                string.IsNullOrEmpty(EndDateFrom) && string.IsNullOrEmpty(EndDateTo))
            {
                var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "GetAllItemSubscriptionDetails", null);
                model = State.ToViewModel();
            }
            else
                model = GetItemSubsList(NameofItem, ItemType,Language,Frequency, StartDateFrom, StartDateTo, EndDateFrom, EndDateTo);




            if (model != null && model.Count() > 0)
            {
                PaidMemberReport.Append("<div class='panel panel-default' style='width:1000px;font-size:22px;'><table class='table table-condensed table-bordered'  id='ResultTable'>");
                PaidMemberReport.Append("<thead class='header' ><tr style='background-color: #6fb3e0 ;  border: 1px dotted #808080; color: #FFFFFF;'><th style='width:80px;text-align:left;'>Sr. No.</th>");
                PaidMemberReport.Append("<th style='width:150px;text-align:left; ' >Name of Item</th><th style='width:150px;text-align:left;border-left: 1px solid #fff; ' >Item Type</th><th style='width:150px;text-align:left;'>Language</th>");
                PaidMemberReport.Append("<th style='width:150px;text-align:left;' >Frequency</th><th style='width:150px;text-align:left;'>Quantity</th><th style='width:150px;text-align:left;'>Subscription Start Date</th><th style='width:150px;text-align:left;' >Subscription End Date</th>");

                PaidMemberReport.Append("</tr></thead>");


                PaidMemberReport.Append("<tbody  class='body'>");
                int count = 0;
                foreach (var obj in model)
                {
                    count++;
                    PaidMemberReport.Append("<tr>");
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", count));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ItemName));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.ItemType));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Language));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Frequancy));
                    PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", obj.Quantity));
                    if (Convert.ToDateTime(obj.SubsStartDate).ToString("dd/MM/yyyy").Equals("01/01/1753"))
                    {
                        PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "NA"));
                    }
                    else {
                        PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.SubsStartDate).ToString("dd/MM/yyyy")));
                    }

                    if (Convert.ToDateTime(obj.SubsEndDate).ToString("dd/MM/yyyy").Equals("01/01/1753"))
                    {
                        PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", "NA"));
                    }
                    else
                    {
                        PaidMemberReport.Append(string.Format("<td style='border: 1px dotted #808080;'>{0}</td>", Convert.ToDateTime(obj.SubsEndDate).ToString("dd/MM/yyyy")));
                    }
                   
                    PaidMemberReport.Append("</tr>");
                }
                PaidMemberReport.Append("</tbody></table></div> ");
            }
            else
            {
                PaidMemberReport.Append("No records available");
            }
            return PaidMemberReport.ToString();

        }

      
        [ValidateAntiForgeryToken]
        public PartialViewResult SavemItemSubscription(ItemSubscriptionViewModel model)
        {
            List<ItemSubscriptionViewModel> ModelList = new List<ItemSubscriptionViewModel>();
            ViewBag.Page = "Home";
            try
            {
                if (model.SubsStartDate == null )
                    model.SubsStartDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                if (model.SubsEndDate == null )
                    model.SubsEndDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue;
                
                if (ModelState.IsValid)
                {
                    
                    var mItemSubscription = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("ItemSubscription", "CreateItemSubscription", mItemSubscription);
                    }
                    else
                    {
                        Helper.ExecuteService("ItemSubscription", "UpdateItemSubscription", mItemSubscription);
                    }
                    var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "GetAllItemSubscriptionDetails", null);
                    ViewBag.Msg = "Item Subscription saved sucessfully";
                    ModelList = State.ToViewModel();
                }
                else
                {
                    ViewBag.Msg = "Unable to add item subscription";
                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException.ToString();
            }


            return PartialView("/Areas/Library/Views/ItemSubscription/_ItemSubscriptionList.cshtml", ModelList);
        }

        public PartialViewResult DeleteItemSubscription(int Id=0)
        {
            List<ItemSubscriptionViewModel> ModelList = new List<ItemSubscriptionViewModel>();
            ViewBag.Page = "Home";
            if (Id > 0)
            {
                mItemSubscription stateToDelete = (mItemSubscription)Helper.ExecuteService("ItemSubscription", "GetItemSubscriptionById", new mItemSubscription { ItemSubscriptionID = Id });

                Helper.ExecuteService("ItemSubscription", "DeleteItemSubscription", stateToDelete);
                ViewBag.Msg = "Item Subscription deleted sucessfully";
            }
            else
            {
                ViewBag.Msg = "Unable to delete";
            }
            var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "GetAllItemSubscriptionDetails", null);
           
            ModelList = State.ToViewModel();
            return PartialView("/Areas/Library/Views/ItemSubscription/_ItemSubscriptionList.cshtml", ModelList);
        }

        public PartialViewResult SearchmItemSubscription(string NameofItem, string ItemType, string Language, string Frequency, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            //  mItemSubscriptionController obj = new mItemSubscriptionController();
            ViewBag.Page = "Report";
            var model = GetItemSubsList(NameofItem, ItemType,Language,Frequency, StartDateFrom, StartDateTo, EndDateFrom, EndDateTo);
            return PartialView("/Areas/Library/Views/ItemSubscription/_ItemSubscriptionList.cshtml", model);
        }

        public static List<ItemSubscriptionViewModel> GetItemSubsList(string NameofItem, string ItemType,string Language,string Frequency, string StartDateFrom, string StartDateTo, string EndDateFrom, string EndDateTo)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(NameofItem))
                sb.Append(NameofItem + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(ItemType))
                sb.Append(ItemType + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Language))
                sb.Append(Language + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Frequency))
                sb.Append(Frequency + ",");
            else
                sb.Append(" " + ",");

          
            if (!string.IsNullOrEmpty(StartDateFrom))
                sb.Append(StartDateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(StartDateTo))
                sb.Append(StartDateTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(EndDateFrom))
                sb.Append(EndDateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(EndDateTo))
                sb.Append(EndDateTo + ",");
            else
                sb.Append(" " + ",");


            // Call Split method
            var State = (List<mItemSubscription>)Helper.ExecuteService("ItemSubscription", "SearchItemSubscription", sb.ToString());
            var model = State.ToViewModel();
            return model;

        }

    }
}
