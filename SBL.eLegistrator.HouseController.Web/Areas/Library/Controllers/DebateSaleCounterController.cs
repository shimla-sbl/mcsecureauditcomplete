﻿using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Library;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class DebateSaleCounterController : Controller
    {
        //
        // GET: /Library/DebateSaleCounter/

        public ActionResult Index()
        {
            try
            {
                ViewBag.Page = "Home";
                var model = (List<mDebateSaleCounter>)Helper.ExecuteService("SaleCounter", "GetAllDebateSaleCounterDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();


                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult PopulateDebateSaleCounter(int Id, string Action)
        {
            mDebateSaleCounter model = new mDebateSaleCounter();


            var stateToEdit = (mDebateSaleCounter)Helper.ExecuteService("SaleCounter", "GetDebateSaleCounterById", new mDebateSaleCounter { DebateSaleCounterID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit;

            model.Mode = Action;


            ViewBag.SetAvailability = model.Availability;

            ViewBag.Availability = new SelectList(Enum.GetValues(typeof(Availability)).Cast<Availability>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            return PartialView("/Areas/Library/Views/DebateSaleCounter/_PopulateDebateSale.cshtml", model);
        }

        public PartialViewResult DeleteDebateSaleCounter(int Id)
        {
            ViewBag.Page = "Home";
            Helper.ExecuteService("SaleCounter", "DeleteDebateSaleCounter", new mDebateSaleCounter { DebateSaleCounterID = Id });
            ViewBag.Msg = "Sucessfully deleted Debate for Sale Counter";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var modelList = (List<mDebateSaleCounter>)Helper.ExecuteService("SaleCounter", "GetAllDebateSaleCounterDetails", null);
            return PartialView("/Areas/Library/Views/DebateSaleCounter/_DebateSaleCounterList.cshtml", modelList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveDebateSaleCounter(mDebateSaleCounter model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ViewBag.Page = "Home";
                    if (model.Mode == "Add")
                    {
                        model.CreatedBy = CurrentSession.UserName;
                        model.ModifiedBy = CurrentSession.UserName;
                        Helper.ExecuteService("SaleCounter", "CreateDebateSaleCounter", model);
                        ViewBag.Msg = "Sucessfully added Debate for  sale counter";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                    else
                    {
                        model.ModifiedBy = CurrentSession.UserName;
                        Helper.ExecuteService("SaleCounter", "UpdateDebateSaleCounter", model);
                        ViewBag.Msg = "Sucessfully updated Debate for  sale counter";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                }
                catch (Exception ex)
                {

                    ViewBag.Msg = ex.InnerException.ToString();
                    ViewBag.Class = "alert alert-danger";
                    ViewBag.Notification = "Error";
                }

            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }


            var modelList = (List<mDebateSaleCounter>)Helper.ExecuteService("SaleCounter", "GetAllDebateSaleCounterDetails", null);
            return PartialView("/Areas/Library/Views/DebateSaleCounter/_DebateSaleCounterList.cshtml", modelList);
        }


        public ActionResult SearchDebateSaleCounter()
        {
            ViewBag.Availability = new SelectList(Enum.GetValues(typeof(Availability)).Cast<Availability>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            return View();
        }

        public PartialViewResult SearchDebateSaleCounterResult(string Khand, string Ank, string DateOfDebateFrom, string DateOfDebateTo, string HindiDate, string HindiMonth, string Pages, string Assembly, string Session, string PriceFrom, string PriceTo, string Availability)
        {
            ViewBag.Page = "Report";
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(Khand))
                sb.Append(Khand + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Ank))
                sb.Append(Ank + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(DateOfDebateFrom))
                sb.Append(DateOfDebateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(DateOfDebateTo))
                sb.Append(DateOfDebateTo + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(HindiDate))
                sb.Append(HindiDate + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(HindiMonth))
                sb.Append(HindiMonth + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Pages))
                sb.Append(Pages + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Assembly))
                sb.Append(Assembly + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Session))
                sb.Append(Session + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(PriceFrom))
                sb.Append(PriceFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PriceTo))
                sb.Append(PriceTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Availability))
                sb.Append(Availability + ",");
            else
                sb.Append(" " + ",");


          
            var modelList = (List<mDebateSaleCounter>)Helper.ExecuteService("SaleCounter", "SearchDebateSaleCounter", sb.ToString());
            return PartialView("/Areas/Library/Views/DebateSaleCounter/_DebateSaleCounterList.cshtml", modelList);
        }


        public ActionResult LocateDebateSaleCounter()
        {
            ViewBag.Availability = new SelectList(Enum.GetValues(typeof(Availability)).Cast<Availability>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            return View();
        }


        public PartialViewResult LocateDebateSaleCounterResult(string Khand, string Ank, string DateOfDebateFrom, string DateOfDebateTo, string HindiDate, string HindiMonth, string Pages, string Assembly, string Session, string PriceFrom, string PriceTo, string Availability, string Building, string Floor, string Almirah)
        {
            ViewBag.Page = "Report";
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(Khand))
                sb.Append(Khand + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Ank))
                sb.Append(Ank + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(DateOfDebateFrom))
                sb.Append(DateOfDebateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(DateOfDebateTo))
                sb.Append(DateOfDebateTo + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(HindiDate))
                sb.Append(HindiDate + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(HindiMonth))
                sb.Append(HindiMonth + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Pages))
                sb.Append(Pages + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Assembly))
                sb.Append(Assembly + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Session))
                sb.Append(Session + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(PriceFrom))
                sb.Append(PriceFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PriceTo))
                sb.Append(PriceTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Availability))
                sb.Append(Availability + ",");
            else
                sb.Append(" " + ",");

           
            if (!string.IsNullOrEmpty(Building))
                sb.Append(Building + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Floor))
                sb.Append(Floor + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Almirah))
                sb.Append(Almirah + ",");
            else
                sb.Append(" " + ",");


            var modelList = (List<mDebateSaleCounter>)Helper.ExecuteService("SaleCounter", "LocateDebateSaleCounter", sb.ToString());
            return PartialView("/Areas/Library/Views/DebateSaleCounter/_DebateSaleCounterList.cshtml", modelList);
        }

    }
}
