﻿using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Library;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class SaleCounterController : Controller
    {
        //
        // GET: /Library/SaleCounter/

        public ActionResult Index()
        {
            try
            {
                ViewBag.Page = "Home";
                var model = (List<mSaleCounter>)Helper.ExecuteService("SaleCounter", "GetAllSaleCounterDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();


                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }


        public PartialViewResult PopulateSaleCounter(int Id, string Action)
        {
            mSaleCounter model = new mSaleCounter();


            var stateToEdit = (mSaleCounter)Helper.ExecuteService("SaleCounter", "GetSaleCounterById", new mSaleCounter { SaleCounterID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit;

            model.Mode = Action;


            ViewBag.SetAvailability = model.Availability;

            ViewBag.Availability = new SelectList(Enum.GetValues(typeof(Availability)).Cast<Availability>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");

            return PartialView("/Areas/Library/Views/SaleCounter/_PopulateSaleCounter.cshtml", model);
        }

        public PartialViewResult DeleteSaleCounter(int Id)
        {
            ViewBag.Page = "Home";
            Helper.ExecuteService("SaleCounter", "DeleteSaleCounter", new mSaleCounter { SaleCounterID = Id });
            ViewBag.Msg = "Sucessfully deleted Sale Counter";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var modelList = (List<mSaleCounter>)Helper.ExecuteService("SaleCounter", "GetAllSaleCounterDetails", null);
            return PartialView("/Areas/Library/Views/SaleCounter/_SaleCounterList.cshtml", modelList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveSaleCounter(mSaleCounter model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    ViewBag.Page = "Home";
                    if (model.Mode == "Add")
                    {
                        model.CreatedBy = CurrentSession.UserName;
                        model.ModifiedBy = CurrentSession.UserName;
                        Helper.ExecuteService("SaleCounter", "CreateSaleCounter", model);
                        ViewBag.Msg = "Sucessfully added sale counter";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                    else
                    {
                        model.ModifiedBy = CurrentSession.UserName;
                        Helper.ExecuteService("SaleCounter", "UpdateSaleCounter", model);
                        ViewBag.Msg = "Sucessfully updated sale counter";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                }
                catch (Exception ex)
                {

                    ViewBag.Msg = ex.InnerException.ToString();
                    ViewBag.Class = "alert alert-danger";
                    ViewBag.Notification = "Error";
                }

            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }


            var modelList = (List<mSaleCounter>)Helper.ExecuteService("SaleCounter", "GetAllSaleCounterDetails", null);
            return PartialView("/Areas/Library/Views/SaleCounter/_SaleCounterList.cshtml", modelList);
        }

        public ActionResult SearchSaleCounter()
        {
            ViewBag.Availability = new SelectList(Enum.GetValues(typeof(Availability)).Cast<Availability>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            return View();

        }

        public PartialViewResult SearchSaleCounterResult(string AuthorName, string Title, string Edition, string Volume, string PublishingYear, string PriceFrom, string PriceTo, string Source, string Availability)
        {
            //  mItemSubscriptionController obj = new mItemSubscriptionController();
            ViewBag.Page = "Report";
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(AuthorName))
                sb.Append(AuthorName + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Title))
                sb.Append(Title + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Edition))
                sb.Append(Edition + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Volume))
                sb.Append(Volume + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(PublishingYear))
                sb.Append(PublishingYear + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PriceFrom))
                sb.Append(PriceFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PriceTo))
                sb.Append(PriceTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Source))
                sb.Append(Source + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Availability))
                sb.Append(Availability + ",");
            else
                sb.Append(" " + ",");

            // Call Split method
            var modelList = (List<mSaleCounter>)Helper.ExecuteService("SaleCounter", "SearchSaleCounter", sb.ToString());
            return PartialView("/Areas/Library/Views/SaleCounter/_SaleCounterList.cshtml", modelList);
        }


        public ActionResult LocateSaleCounter()
        {
            ViewBag.Availability = new SelectList(Enum.GetValues(typeof(Availability)).Cast<Availability>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            return View();
          

        }

        public PartialViewResult LocateSaleCounterResult(string AuthorName, string Title, string Edition, string Volume, string PublishingYear, string PriceFrom, string PriceTo, string Source, string Availability, string Building, string Floor, string Almirah)
        {
            //  mItemSubscriptionController obj = new mItemSubscriptionController();
            ViewBag.Page = "Report";
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(AuthorName))
                sb.Append(AuthorName + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Title))
                sb.Append(Title + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Edition))
                sb.Append(Edition + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Volume))
                sb.Append(Volume + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(PublishingYear))
                sb.Append(PublishingYear + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PriceFrom))
                sb.Append(PriceFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PriceTo))
                sb.Append(PriceTo + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Source))
                sb.Append(Source + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Availability))
                sb.Append(Availability + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Building))
                sb.Append(Building + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Floor))
                sb.Append(Floor + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Almirah))
                sb.Append(Almirah + ",");
            else
                sb.Append(" " + ",");


            // Call Split method
            var modelList = (List<mSaleCounter>)Helper.ExecuteService("SaleCounter", "LocateSaleCounter", sb.ToString());
            return PartialView("/Areas/Library/Views/SaleCounter/_SaleCounterList.cshtml", modelList);
        }


    }
}
