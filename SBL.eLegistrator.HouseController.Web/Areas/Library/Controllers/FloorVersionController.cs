﻿using SBL.DomainModel.Models.Library;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using System.Text;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class FloorVersionController : Controller
    {
        //
        // GET: /Library/FloorVersion/

        //public ActionResult Index()
        //{
        //    return View();
        //}



        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<tFloorVersion>)Helper.ExecuteService("FloorVersion", "GetAllFloorVersionDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                ViewBag.Page = "Home";
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Floor Version Information Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }



        public PartialViewResult CreatetFloorVersion(int Id, string Action)
        {
            FloorVersionViewModel model = new FloorVersionViewModel();
            tFloorVersion stateToEdit = (tFloorVersion)Helper.ExecuteService("FloorVersion", "GetFloorVersionById", new tFloorVersion { FloorVersionId = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit.ToViewModel1(Action);
            model.Mode = Action;

            return PartialView("/Areas/Library/Views/FloorVersion/_CreateFloorVersion.cshtml", model);
        }




        [ValidateAntiForgeryToken]
        public PartialViewResult SavetFloorVersion(FloorVersionViewModel model)
        {
            List<FloorVersionViewModel> ModelList = new List<FloorVersionViewModel>();
            ViewBag.Page = "Home";
            try
            {

                if (ModelState.IsValid)
                {

                    var tFloorVersion = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("FloorVersion", "CreateFloorVersion", tFloorVersion);
                    }
                    else
                    {
                        Helper.ExecuteService("FloorVersion", "UpdateFloorVersion", tFloorVersion);
                    }
                    var State = (List<tFloorVersion>)Helper.ExecuteService("FloorVersion", "GetAllFloorVersionDetails", null);
                    ViewBag.Msg = "Floor Version Information saved sucessfully";
                    ModelList = State.ToViewModel();
                }
                else
                {
                    ViewBag.Msg = "Unable to add Floor Version Information";
                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException.ToString();
            }


            return PartialView("/Areas/Library/Views/FloorVersion/_FloorVersionList.cshtml", ModelList);
        }

        public PartialViewResult DeleteFloorVersion(int Id = 0)
        {
            List<FloorVersionViewModel> ModelList = new List<FloorVersionViewModel>();
            ViewBag.Page = "Home";
            if (Id > 0)
            {
                tFloorVersion stateToDelete = (tFloorVersion)Helper.ExecuteService("FloorVersion", "GetFloorVersionById", new tFloorVersion { FloorVersionId = Id });

                Helper.ExecuteService("FloorVersion", "DeleteFloorVersion", stateToDelete);
                ViewBag.Msg = "Floor Version Information deleted sucessfully";
            }
            else
            {
                ViewBag.Msg = "Unable to delete";
            }
            var State = (List<tFloorVersion>)Helper.ExecuteService("FloorVersion", "GetAllFloorVersionDetails", null);

            ModelList = State.ToViewModel();
            return PartialView("/Areas/Library/Views/FloorVersion/_FloorVersionList.cshtml", ModelList);
        }





    }
}
