﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.LibraryVS;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class BillInformationController : Controller
    {
        //
        // GET: /Library/BillInformation/

        public ActionResult Index()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bill form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult AddBillPage()
        {
            try
            {
                VsLibraryBillInfo obj = new VsLibraryBillInfo();
                obj.BillNoList = (List<fillListGenric>)Helper.ExecuteService("VsLibraryBillInfo", "GetBillNoInfo", null);

                return View(obj);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bill form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult findtitleofbill(string id)
        {
            var title = Helper.ExecuteService("VsLibraryBillInfo", "GetBillNoInfoTitle", id);
            return Json(title,JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveBillInfo(VsLibraryBillInfo model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var datacheck = (VsLibraryBillInfo)Helper.ExecuteService("VsLibraryBillInfo", "BillCheck", model);
                    if (datacheck == null)
                    {
                        var Content1 = (VsLibraryBillInfo)Helper.ExecuteService("VsLibraryBillInfo", "SaveBillInfo", model);
                        return Content("1");
                    }
                    else
                    {
                        return Content("5");
                    }
                }
                else
                {
                    // ModelState.AddModelError("", "error");
                    return Content("2");//Failure
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Saving Bill information";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult UpdateBillPage()
        {
            try
            {
                VsLibraryBillInfo obj = new VsLibraryBillInfo();
                obj.BillNoList = (List<fillListGenric>)Helper.ExecuteService("VsLibraryBillInfo", "GetBillNoInfo", null);

                return View(obj);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bill form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult UpdateBillPageWithId(int id)
        {
            try
            {
                VsLibraryBillInfo obj = new VsLibraryBillInfo();               

                //var UpdationList = (VsLibraryBillInfo)Helper.ExecuteService("VsLibraryBillInfo", "UpdateBillInfoWithSerialNo", id);
                //return PartialView("_UpdateBillPage", UpdationList);
                obj = (VsLibraryBillInfo)Helper.ExecuteService("VsLibraryBillInfo", "UpdateBillInfoWithSerialNo", id);
                obj.BillNoList = (List<fillListGenric>)Helper.ExecuteService("VsLibraryBillInfo", "GetBillNoInfo", null);
                return PartialView("_UpdateBillPage", obj);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult SaveUpdateBillInfo(VsLibraryBillInfo model, string ButtonType)
        {
            try
            {
                if (ButtonType == "Search" || ButtonType == null)
                {
                    var SearchBillInfo = (List<VsLibraryBillInfo>)Helper.ExecuteService("VsLibraryBillInfo", "BillCheckForUpdation", model);
                    if (SearchBillInfo.Count == 0)
                    {
                        return Content("3null");
                    }
                    return PartialView("_BillListForUpdate", SearchBillInfo);

                }
                else if (ButtonType == "update")
                {
                    if (ModelState.IsValid)
                    {
                        var Content1 = (VsLibraryBillInfo)Helper.ExecuteService("VsLibraryBillInfo", "UpdateBillInfo", model);
                        return Content("3UpdateSuccess");
                    }
                    else
                    {
                        //ModelState.AddModelError("", "error");
                        return Content("2null");//Failure
                    }
                }
                return null;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Updating Bill information";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult SearchBillPage()
        {
            try
            {
                VsLibraryBillInfo obj = new VsLibraryBillInfo();
                obj.BillNoList = (List<fillListGenric>)Helper.ExecuteService("VsLibraryBillInfo", "GetBillNoInfo", null);
                return View(obj);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bill form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult SearchBillInfoForAll(VsLibraryBillInfo model)
        {
            try
            {
                var CheckBillInfo = (List<VsLibraryBillInfo>)Helper.ExecuteService("VsLibraryBillInfo", "SearchBillInfoForAll", model);
                if (CheckBillInfo.Count == 0)
                {
                    return Content("3null");
                }
                return PartialView("_BillListForSearch", CheckBillInfo);

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bill Info page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult LocateBillPage()
        {
            try
            {
                VsLibraryBillInfo obj = new VsLibraryBillInfo();
                obj.BillNoList = (List<fillListGenric>)Helper.ExecuteService("VsLibraryBillInfo", "GetBillNoInfo", null);
                return View(obj);
            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bill form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult LocateBillInfo(VsLibraryBillInfo model)
        {
            try
            {
                var CheckBillInfo = (List<VsLibraryBillInfo>)Helper.ExecuteService("VsLibraryBillInfo", "SearchBillLocation", model);
                if (CheckBillInfo.Count == 0)
                {
                    return Content("3null");
                }
                return PartialView("_BillListForLocate", CheckBillInfo);

            }
            catch (Exception)
            {
                ViewBag.ErrorMessage = "There is a Error In Bill location form page";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult LibraryBillInfo()
        {
            try
            {
                VsLibraryBillInfo obj = new VsLibraryBillInfo();


                return View();
            }
            catch (Exception)
            {
                
                throw;
            }
        }

    }
}
