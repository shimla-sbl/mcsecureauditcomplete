﻿using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Library;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class DebateInfoController : Controller
    {
        //
        // GET: /Library/DebateInfo/

        public ActionResult Index()
        {
            try
            {
                ViewBag.Page = "Home";
                var model = (List<mDebateInfo>)Helper.ExecuteService("SaleCounter", "GetAllDebateInfoDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"].ToString();


                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Head of Press Clip Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }

        public PartialViewResult PopulateDebateInfo(int Id, string Action)
        {
            mDebateInfo model = new mDebateInfo();


            var stateToEdit = (mDebateInfo)Helper.ExecuteService("SaleCounter", "GetDebateInfoById", new mDebateInfo { DebateInfoID = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit;

            model.Mode = Action;


            ViewBag.TitleType = new SelectList(Enum.GetValues(typeof(TitleType)).Cast<TitleType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            return PartialView("/Areas/Library/Views/DebateInfo/_PopulateDebateInfo.cshtml", model);
        }

        public PartialViewResult DeleteDebateInfo(int Id)
        {
            ViewBag.Page = "Home";
            Helper.ExecuteService("SaleCounter", "DeleteDebateInfo", new mDebateInfo { DebateInfoID = Id });
            ViewBag.Msg = "Sucessfully deleted Debate Information";
            ViewBag.Class = "alert alert-info";
            ViewBag.Notification = "Success";

            var modelList = (List<mDebateInfo>)Helper.ExecuteService("SaleCounter", "GetAllDebateInfoDetails", null);
            return PartialView("/Areas/Library/Views/DebateInfo/_DebateInfoList.cshtml", modelList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public PartialViewResult SaveDebateInfo(mDebateInfo model)
        {
            ViewBag.Page = "Home";
            if (model.Rack == 0)
            {
               
                ModelState["Rack"].Errors.Clear();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    ViewBag.Page = "Home";
                    if (model.Mode == "Add")
                    {
                        model.CreatedBy = CurrentSession.UserName;
                        model.ModifiedBy = CurrentSession.UserName;
                        Helper.ExecuteService("SaleCounter", "CreateDebateInfo", model);
                        ViewBag.Msg = "Sucessfully added Debate Information";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                    else
                    {
                        model.ModifiedBy = CurrentSession.UserName;
                        Helper.ExecuteService("SaleCounter", "UpdateDebateInfo", model);
                        ViewBag.Msg = "Sucessfully updated Debate Information";
                        ViewBag.Class = "alert alert-info";
                        ViewBag.Notification = "Success";
                    }
                }
                catch (Exception ex)
                {

                    ViewBag.Msg = ex.InnerException.ToString();
                    ViewBag.Class = "alert alert-danger";
                    ViewBag.Notification = "Error";
                }

            }
            else
            {
                ModelState.AddModelError("", "error in code");
            }


            var modelList = (List<mDebateInfo>)Helper.ExecuteService("SaleCounter", "GetAllDebateInfoDetails", null);
            return PartialView("/Areas/Library/Views/DebateInfo/_DebateInfoList.cshtml", modelList);
        }


        public ActionResult SearchDebateInfo()
        {
            ViewBag.TitleType = new SelectList(Enum.GetValues(typeof(TitleType)).Cast<TitleType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            return View();
        }

        public PartialViewResult SearchDebateInfoResult(string Title, string Khand, string Ank, string DateOfDebateFrom, string DateOfDebateTo, string HindiDate, string HindiMonth, string HindiYear,  string Assembly, string Session)
        {
            ViewBag.Page = "Report";
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(Title))
                sb.Append(Title + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Khand))
                sb.Append(Khand + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Ank))
                sb.Append(Ank + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(DateOfDebateFrom))
                sb.Append(DateOfDebateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(DateOfDebateTo))
                sb.Append(DateOfDebateTo + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(HindiDate))
                sb.Append(HindiDate + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(HindiMonth))
                sb.Append(HindiMonth + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(HindiYear))
                sb.Append(HindiYear + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Assembly))
                sb.Append(Assembly + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Session))
                sb.Append(Session + ",");
            else
                sb.Append(" " + ",");


          
            var modelList = (List<mDebateInfo>)Helper.ExecuteService("SaleCounter", "SearchDebateInfo", sb.ToString());
            return PartialView("/Areas/Library/Views/DebateInfo/_DebateInfoList.cshtml", modelList);
        }


        public ActionResult LocateDebateInfo()
        {
            ViewBag.TitleType = new SelectList(Enum.GetValues(typeof(TitleType)).Cast<TitleType>().Select(v => new SelectListItem
            {
                Text = v.GetDescription(),
                Value = ((int)v).ToString()
            }).ToList(), "Value", "Text");
            return View();
        }


        public PartialViewResult LocateDebateInfoResult(string Title, string Khand, string Ank, string DateOfDebateFrom, string DateOfDebateTo, string HindiDate, string HindiMonth, string HindiYear,  string Assembly, string Session, string Building, string Floor, string Almirah)
        {
            ViewBag.Page = "Report";
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(Title))
                sb.Append(Title + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Khand))
                sb.Append(Khand + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Ank))
                sb.Append(Ank + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(DateOfDebateFrom))
                sb.Append(DateOfDebateFrom + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(DateOfDebateTo))
                sb.Append(DateOfDebateTo + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(HindiDate))
                sb.Append(HindiDate + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(HindiMonth))
                sb.Append(HindiMonth + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(HindiYear))
                sb.Append(HindiYear + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Assembly))
                sb.Append(Assembly + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Session))
                sb.Append(Session + ",");
            else
                sb.Append(" " + ",");


            if (!string.IsNullOrEmpty(Building))
                sb.Append(Building + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Floor))
                sb.Append(Floor + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Almirah))
                sb.Append(Almirah + ",");
            else
                sb.Append(" " + ",");


            var modelList = (List<mDebateInfo>)Helper.ExecuteService("SaleCounter", "LocateDebateInfo", sb.ToString());
            return PartialView("/Areas/Library/Views/DebateInfo/_DebateInfoList.cshtml", modelList);
        }

    }
}
