﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.LibraryVS;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.StaffManagement;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Extensions;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class IssueDocumentController : Controller
    {
        //
        // GET: /Library/IssueDocument/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult IssueDocumentDashBoard()
        {
            try
            {
                DocumentIssueViewModel model = new DocumentIssueViewModel();
                model.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("VsIssueDocument", "GetAllDesignations", null);
                model.MemberCol = (List<mMember>)Helper.ExecuteService("VsIssueDocument", "GetMemberListByDesignation", "0");

                return View(model);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public JsonResult StaffOrMember(int type)
        {
            try
            {
                if (type == 1)
                {
                    DocumentIssueViewModel model = new DocumentIssueViewModel();
                    model.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("VsIssueDocument", "GetAllDesignations", null);
                    model.MemberCol = (List<mMember>)Helper.ExecuteService("VsIssueDocument", "GetMemberListByDesignation", "0");
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                else if (type == 2)
                {
                    DocumentIssueViewModel model = new DocumentIssueViewModel();
                    model.mStaffdesignation = (List<mStaff>)Helper.ExecuteService("VsIssueDocument", "GetAllDesignationsForStaff", null);
                    model.mStaffMembers = (List<mStaff>)Helper.ExecuteService("VsIssueDocument", "GetMemberListByDesignationForStaff", "0");
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                return null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public ActionResult ReturnDocumentDashBoard()
        {
            try
            {
                DocumentIssueViewModel model = new DocumentIssueViewModel();
                //By default populate list with members
                model.MemberCol = (List<mMember>)Helper.ExecuteService("VsIssueDocument", "GetMembersWithUnReturnedDocs", "0");

                return View(model);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult ListOfSubscriberWithUnReturnedDocs(int type)
        {
            try
            {
                DocumentIssueViewModel model = new DocumentIssueViewModel();
                if (type == 1)
                {
                    model.MemberCol = (List<mMember>)Helper.ExecuteService("VsIssueDocument", "GetMembersWithUnReturnedDocs", "0");
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.mStaffMembers = (List<mStaff>)Helper.ExecuteService("VsIssueDocument", "GetStaffWithUnReturnedDocs", "0");
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult SaveReturnDocumentIssue(FormCollection _saveData)
        {
            try
            {

                var data = _saveData["SelectedIndexID"];
                var isMember = _saveData["isMemberOrStaff"];
                if (data != null)
                {
                    if (isMember == "0")
                    {
                        string[] IssuID = data.Split(',');
                        for (int i = 0; i < IssuID.Length; i++)
                        {
                            Int32 IssuIDData = Convert.ToInt32(IssuID[i]);
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                            if (IssuIDData != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                            {
                                Helper.ExecuteService("VsIssueDocument", "UpdateReturnedDocumentsForMembers", IssuIDData);
                            }
                        }
                        return Content("saved");
                    }
                    else
                    {
                        string[] IssuID = data.Split(',');
                        for (int i = 0; i < IssuID.Length; i++)
                        {
                            Int32 IssuIDData = Convert.ToInt32(IssuID[i]);
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                            if (IssuIDData != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'int' is never equal to 'null' of type 'int?'
                            {
                                Helper.ExecuteService("VsIssueDocument", "UpdateReturnedDocumentsForStaff", IssuIDData);
                            }
                        }
                    } return Content("saved");
                }
                else
                { return Content("cH_error"); }//Server side validation
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult GetUnReturnedDocsOfSubscribers(int? SubId, string type)
        {
            try
            {
                if (type == "1")
                {
                    //DocumentIssueViewModel model = new DocumentIssueViewModel();
                    int selectedMembId = SubId ?? 0;
                    //model.MemberList = (List<VsDocumentIssue>)Helper.ExecuteService("VsIssueDocument", "GetUnReturnedDocumentsForMember", selectedMembId);
                    var MemberList = (List<VsDocumentIssue>)Helper.ExecuteService("VsIssueDocument", "GetUnReturnedDocumentsForMember", selectedMembId);

                    return PartialView("_SubscriberUnReturnedDocuments", MemberList);

                }
                else
                {
                    //DocumentIssueViewModel model = new DocumentIssueViewModel();
                    int selectedMembId = SubId ?? 0;
                    //model.MemberList = (List<VsDocumentIssue>)Helper.ExecuteService("VsIssueDocument", "GetUnReturnedDocumentsForStaff", selectedMembId);
                    var MemberList = (List<VsDocumentIssue>)Helper.ExecuteService("VsIssueDocument", "GetUnReturnedDocumentsForStaff", selectedMembId);

                    return PartialView("_SubscriberUnReturnedDocuments", MemberList);

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult GetMemberWithDesignationCode(string deptId, int? membType)
        {
            try
            {
                if (membType == 0)
                {
                    DocumentIssueViewModel model = new DocumentIssueViewModel();
                    model.MemberCol = (List<mMember>)Helper.ExecuteService("VsIssueDocument", "GetMemberListByDesignation", deptId);
                    if (model.MemberCol.Count == 0)
                    {
                        return Json("data:false", JsonRequestBehavior.AllowGet);
                        // return Content("NOCONTENT");
                    }
                    else
                    {
                        return Json(model, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    DocumentIssueViewModel model = new DocumentIssueViewModel();
                    model.mStaffMembers = (List<mStaff>)Helper.ExecuteService("VsIssueDocument", "GetMemberListByDesignationForStaff", deptId);
                    if (model.mStaffMembers.Count == 0)
                    {
                        return Json("data:false", JsonRequestBehavior.AllowGet);
                    }
                    return Json(model, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult NewDocumentIssue(FormCollection _frmCollection, DocumentIssueViewModel _DocumentIssueViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var designationType = _frmCollection["designationType"];
                    int count = int.Parse(_frmCollection["tcount"]);
                    for (int i = 1; i < count; i++)
                    {
                        VsDocumentIssue _docIssue = new VsDocumentIssue();
                        _docIssue.SubscriberDesignationId = _frmCollection["searchcriteria1"];
                        _docIssue.SubscriberId = _DocumentIssueViewModel.SubscriberId;
                        _docIssue.DateOfIssue = DateTime.ParseExact(_DocumentIssueViewModel.DisplayedDOI, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture); //Convert.ToDateTime(_DocumentIssueViewModel.DisplayedDOI);
                        _docIssue.TimeOfIssue = DateTime.Now.ToShortTimeString();
                        if (designationType == "0")
                        {
                            _docIssue.IsMember = true;
                        }
                        else
                        {
                            _docIssue.IsMember = false;
                        }
                        _docIssue.IssueDocumentName = _frmCollection["span_1_" + i] + " " + _frmCollection["span_2_" + i];
                        Helper.ExecuteService("VsIssueDocument", "CreateDocumentIssue", _docIssue);
                    }
                    return Content("saved");
                }
                return Content("modelerror");
            }
            catch (Exception)
            {
                return Content("error");
                throw;
            }

        }

        public ActionResult IssueReturnReport()
        {
            try
            {
                DocumentIssueViewModel model = new DocumentIssueViewModel();
                model.DesignationCol = (List<mMemberDesignation>)Helper.ExecuteService("VsIssueDocument", "GetAllDesignations", null);
                model.MemberCol = (List<mMember>)Helper.ExecuteService("VsIssueDocument", "GetMemberListByDesignation", "0");
                return View(model);
            }
            catch (Exception)
            {
                
                throw;
            }        
        }

        public ActionResult DocumentIssueSearchReport(DocumentIssueViewModel model,FormCollection formdata)
        {
            try
            {
                VsDocumentIssue vsDoc = new VsDocumentIssue();
                var a = formdata["IssueStartRange"];
                var DesignationID = formdata["searchcriteria1"];
                if (a != "")
                {
                    vsDoc.IssueStartRange = DateTime.ParseExact(formdata["IssueStartRange"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    vsDoc.IssueEndRange = DateTime.ParseExact(formdata["IssueEndRange"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                   
                }
                if (!string.IsNullOrEmpty(formdata["ReturnStartRange"]) )
                {
                    vsDoc.ReturnStartRange = DateTime.ParseExact(formdata["ReturnStartRange"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    vsDoc.ReturnEndRange = DateTime.ParseExact(formdata["ReturnEndRange"], "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                }
                vsDoc.SubscriberDesignationId = DesignationID;
                vsDoc.SubscriberId = model.SubscriberId;
                var result = (List<VsDocumentIssue>)Helper.ExecuteService("VsIssueDocument", "GetReportWIthCriteria", vsDoc);
                var returnResult = result.ToViewModel();//Extension method For Model mapping
                return PartialView("_IssueReturnReportList",returnResult);

            }
            catch (Exception)
            {
                
                throw;
            }
        }


    }
}
