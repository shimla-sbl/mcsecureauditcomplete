﻿using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.LibraryVS;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class BookManagementController : Controller
    {
        //
        // GET: /Library/BookManagement/

        public ActionResult Index()
        {
            return View();
        }

        #region for demo----code for loader

        //[HttpPost]
        //public ActionResult PostMethod()
        //{
        //    System.Threading.Thread.Sleep(5000);

        //    return Json("Message from Post action method.");
        //}

        #endregion for demo----code for loader

        #region View Page to add or updating book info

        public ActionResult AddNewBookView()
        {
            try
            {
               // ViewBag.SaveData = TempData["Message"];//to show successful message in case of saving
                return View("AddNewBook");
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error In book form page";
                return View("_LibraryErrorPage");
            }
        }

        #endregion View Page to add or updating book info

        #region Action for updating Form for updating book info

        [HttpGet]
        public ActionResult AddNewBook(string model)
        {
            try
            {
                if (model == null || model == "")
                {
                    return Content("2");
                }
                else
                {
                    var Content1 = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "GetBookInfoByIdForUpdate", model);
                    if (Content1 == null)
                    {
                        //TempData["Message"] = "Please check the Accession number";//to show successful message in case of saving
                        return Content("3");
                    }
                    else
                    {
                        //ViewBag.SaveData = TempData["Message"];//to show successful message in case of saving
                        ViewBag.updatecheck = "updatecheck";
                        return PartialView("_UpdateBookInfo", Content1);
                    }
                    
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error In book form page";
                return View("_LibraryErrorPage");
            }
        }

        #endregion Action for updating Form for updating book info

        #region Saving updated info
        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult UpdateNewBookInfo(VsLibraryBooks model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var Content1 = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "AddBookInfo", model);
                    
                    return Content("1");
                }
                else {
                    return Content("4");
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Action For Saving book info

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult SaveNewBookInfo(VsLibraryBooks model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    string AccsnNO = model.AccessionNumber;
                    var CheckAccessionNo = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "AccessionNumberCheck", AccsnNO);
                    if (CheckAccessionNo == null)
                    {
                        var Content1 = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "AddBookInfo", model);
                        //TempData["Message"] = "Book Added SuccessFully";//to show successful message in case of saving
                        return Content("1");
                        //return RedirectToAction("AddNewBook");
                    }
                    else
                    {
                        //TempData["Message"] = "Accession no already exit please change the accession number";
                        return Content("0");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "error");
                    return Content("2");//Failure
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Saving book information";
                return View("_LibraryErrorPage");
            }
        }

        #endregion Action For Saving book info

        #region Update Book info according to Accession no

        public ActionResult UpdateBookWithAccessionNo()
        {
            try
            {
                //ViewBag.Message = TempData["Message"];
                return View();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a some Error , pls contact admin";
                return View("_LibraryErrorPage");
            }
        }

        public ActionResult updateWriteOff(VsLibraryBooks _book)
        {

            Helper.ExecuteService("VsLibraryBooks", "updateWriteOff", _book);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        #endregion Update Book info according to Accession no

        #region Getting info of book by accession no for update

        public ActionResult GetBookInfoByIdForUpdate(string model)
        {
            try
            {
                var Content = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "GetBookInfoByIdForUpdate", model);
                return View(Content);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While getting info of book information";
                return View("_LibraryErrorPage");
            }
        }
        public ActionResult getBookDetailsByAccsNO(string accessionNumber)
        {

            List<VsLibraryBooks> CheckAccessionNo = (List<VsLibraryBooks>)Helper.ExecuteService("VsLibraryBooks", "SearchBooksWithAccessionNo", accessionNumber);
            return PartialView("_BooksVerificationList", CheckAccessionNo);
        }
        public ActionResult getBookDetailsBylocation(string Building, string Floor, string Almirah, string Rack)
        {

            List<VsLibraryBooks> CheckAccessionNo = (List<VsLibraryBooks>)Helper.ExecuteService("VsLibraryBooks", "SearchBookWithLocation", new string[] { Building, Floor, Almirah, Rack });
            return PartialView("_BooksVerificationList", CheckAccessionNo);
        }
        public ActionResult getAccessionNumberList()
        {
            List<AccessionNumberList> _list = (List<AccessionNumberList>)Helper.ExecuteService("VsLibraryBooks", "getAccessionNumberList", null);
            return Json(_list, JsonRequestBehavior.AllowGet);
        }
        public ActionResult verifiedBook(string id)
        {
            Helper.ExecuteService("VsLibraryBooks", "verifiedBook", id);
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult verifyAll(List<VsLibraryBooks> _list)
        {
            foreach (var item in _list)
            {
                if (item.checkStatus)
                {
                    Helper.ExecuteService("VsLibraryBooks", "verifiedBook", item.AccessionNumber);
                }
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }
        #endregion Getting info of book by accession no for update

        #region View Page for Search Book According to location or accession no

        public ActionResult SearchBookInfo()
        {
            try
            {
                return View();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Searching book";
                return View("_LibraryErrorPage");
            }
        }

        #endregion View Page for Search Book According to location or accession no

        #region Search Book by Accession number

        [HttpPost]
        public ActionResult SearchBooksWithAccessionNo(VsLibraryBooks model)
        {
            try
            {
                var Content = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "SearchBooksWithAccessionNo", model);
                return View(Content);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Searching book";
                return View("_LibraryErrorPage");
            }
        }

        #endregion Search Book by Accession number

        #region Search Book by location

        [HttpPost]
        public ActionResult SearchBookWithLocation(VsLibraryBooks model)
        {
            try
            {
                var Content = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "SearchBookWithLocation", model);
                return View(Content);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error While Searching book";
                return View("_LibraryErrorPage");
            }
        }

        #endregion Search Book by location

        #region Page to Generate Reports

        public ActionResult GenerateReports()
        {
            try
            {
                return View();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error in reports page";
                return View("_LibraryErrorPage");
            }
        }

        #endregion Page to Generate Reports

        #region Viewpage of writeoff about the book i.e adding book info

        public ActionResult WriteOffBook()
        {
            try
            {
                return View();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error in reports page";
                return View("_LibraryErrorPage");
            }
        }

        #endregion Viewpage of writeoff about the book i.e adding book info

        #region View Page for verfication

        public ActionResult BooksVerification()
        {
            try
            {
                return View();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error in reports page";
                return View("_LibraryErrorPage");
            }
        }

        #endregion View Page for verfication

        #region View Page for WriteoffBook

        public ActionResult SaveWriteOffInfo()
        {
            try
            {
                return View();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error in reports page";
                return View("_LibraryErrorPage");
            }
        }

        #endregion View Page for WriteoffBook

        #region View Page for ReportOfBook

        public ActionResult ReportView()
        {
            try
            {
                return View();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error in reports page";
                return View("_LibraryErrorPage");
            }
        }

        #endregion View Page for ReportOfBook

        #region View Page for Searching books

        public ActionResult SearchAvailableBooks()
        {
            try
            {
                return View();
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                ViewBag.ErrorMessage = "There is a Error in reports page";
                return View("_LibraryErrorPage");
            }
        }

        #endregion View Page for Searching books

        #region Search according to Different creteria

        [HttpPost]
        public PartialViewResult SearchAvailableBooks(string TitleOfBook, string AuthorName, string EditorName, string Edition, string Volume, string PublisherName, string PublicationPlace, string PublicationYear, string PublicationStatus)
        {
            ViewBag.Page = "Search";
            var model = GetItemSubsList(TitleOfBook, AuthorName, EditorName, Edition, Volume, PublisherName, PublicationPlace, PublicationYear, PublicationStatus);
            return PartialView("~/Areas/Library/Views/BookManagement/_AvailableBooksList.cshtml", model);
        }

        public static List<VsLibraryBooks> GetItemSubsList(string TitleOfBook, string AuthorName, string EditorName, string Edition, string Volume, string PublisherName, string PublicationPlace, string PublicationYear, string PublicationStatus)
        {
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(TitleOfBook))
                sb.Append(TitleOfBook + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(AuthorName))
                sb.Append(AuthorName + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(EditorName))
                sb.Append(EditorName + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Edition))
                sb.Append(Edition + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(Volume))
                sb.Append(Volume + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PublisherName))
                sb.Append(PublisherName + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PublicationPlace))
                sb.Append(PublicationPlace + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PublicationYear))
                sb.Append(PublicationYear + ",");
            else
                sb.Append(" " + ",");

            if (!string.IsNullOrEmpty(PublicationStatus))
                sb.Append(PublicationStatus + ",");
            else
                sb.Append(" " + ",");
            // Call Split method
            var BooksSearched = (List<VsLibraryBooks>)Helper.ExecuteService("VsLibraryBooks", "SearchBookForPublicUser", sb.ToString());
            return BooksSearched;
        }

        #endregion Search according to Different creteria

        #region To check accession no for Write off

        #region Search books for report generating
        [HttpPost]
        public ActionResult SearchBookForReport(VsLibraryBooks model)
        {
            try
            {
                var reportlist = (List<VsLibraryBooks>)Helper.ExecuteService("VsLibraryBooks", "SearchBookForReport", model);
                return PartialView("_BookListForReport",reportlist);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        #endregion

        public ActionResult CheckAccessionNoForWriteOff(string AccessionNumber)
        {
            try
            {
                string AccsnNO = AccessionNumber;
                var CheckAccessionNo = (VsLibraryBooks)Helper.ExecuteService("VsLibraryBooks", "AccessionNumberCheck", AccsnNO);
                return PartialView("_WriteOffInfo", CheckAccessionNo);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion To check accession no for Write off
       
    }
}