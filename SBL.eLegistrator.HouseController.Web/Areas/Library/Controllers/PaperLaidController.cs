﻿using SBL.DomainModel.Models.Library;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Areas.Library.Models;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.IO;
using System.Text;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Controllers
{
    [Audit]
    [NoCache]
    [SBLAuthorize(Allow = "Authenticated")]
    public class PaperLaidController : Controller
    {
        //
        // GET: /Library/PaperLaid/

        //public ActionResult Index()
        //{
        //    return View();
        //}



        public ActionResult Index()
        {
            try
            {
                if (string.IsNullOrEmpty(CurrentSession.UserName))
                {
                    return RedirectToAction("LoginUP", "Account", new { @area = "" });
                }
                var State = (List<tPaperLaid>)Helper.ExecuteService("PaperLaidLibrary", "GetAllPaperLaidDetails", null);
                if (TempData["Msg"] != null)
                    ViewBag.Msg = TempData["Msg"];
                ViewBag.Page = "Home";
                var model = State.ToViewModel();
                return View(model);
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMessage = "Their is a Error While Getting Paper Laid Information Details";
                return View("/Areas/SuperAdmin/Views/Shared/AdminErrorPage.cshtml");
                throw ex;
            }
        }



        public PartialViewResult CreatetPaperLaid(int Id, string Action)
        {
            PaperLaidViewModel model = new PaperLaidViewModel();
            tPaperLaid stateToEdit = (tPaperLaid)Helper.ExecuteService("PaperLaidLibrary", "GetPaperLaidById", new tPaperLaid { PaperLaidId = Id });
            if (stateToEdit != null && Id > 0)
                model = stateToEdit.ToViewModel1(Action);
            model.Mode = Action;

            return PartialView("/Areas/Library/Views/PaperLaid/_CreatePaperLaid.cshtml", model);
        }




        [ValidateAntiForgeryToken]
        public PartialViewResult SavetPaperLaid(PaperLaidViewModel model)
        {
            List<PaperLaidViewModel> ModelList = new List<PaperLaidViewModel>();
            ViewBag.Page = "Home";
            try
            {

                if (ModelState.IsValid)
                {

                    var tPaperLaid = model.ToDomainModel();
                    if (model.Mode == "Add")
                    {
                        Helper.ExecuteService("PaperLaidLibrary", "CreatePaperLaid", tPaperLaid);
                    }
                    else
                    {
                        Helper.ExecuteService("PaperLaidLibrary", "UpdatePaperLaid", tPaperLaid);
                    }
                    var State = (List<tPaperLaid>)Helper.ExecuteService("PaperLaidLibrary", "GetAllPaperLaidDetails", null);
                    ViewBag.Msg = "Paper Laid Information saved sucessfully";
                    ModelList = State.ToViewModel();
                }
                else
                {
                    ViewBag.Msg = "Unable to add Paper Laid Information";
                }
            }
            catch (Exception ex)
            {

                ViewBag.Msg = ex.InnerException.ToString();
            }


            return PartialView("/Areas/Library/Views/PaperLaid/_PaperLaidList.cshtml", ModelList);
        }

        public PartialViewResult DeletePaperLaid(int Id = 0)
        {
            List<PaperLaidViewModel> ModelList = new List<PaperLaidViewModel>();
            ViewBag.Page = "Home";
            if (Id > 0)
            {
                tPaperLaid stateToDelete = (tPaperLaid)Helper.ExecuteService("PaperLaidLibrary", "GetPaperLaidById", new tPaperLaid { PaperLaidId = Id });

                Helper.ExecuteService("PaperLaidLibrary", "DeletePaperLaid", stateToDelete);
                ViewBag.Msg = "Paper Laid Information deleted sucessfully";
            }
            else
            {
                ViewBag.Msg = "Unable to delete";
            }
            var State = (List<tPaperLaid>)Helper.ExecuteService("PaperLaidLibrary", "GetAllPaperLaidDetails", null);

            ModelList = State.ToViewModel();
            return PartialView("/Areas/Library/Views/PaperLaid/_PaperLaidList.cshtml", ModelList);
        }





    }
}
