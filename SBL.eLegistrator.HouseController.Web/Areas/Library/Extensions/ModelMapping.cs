﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.LibraryVS;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Extensions
{
    public static class ModelMapping
    {
        //mapping of all former members
        public static IEnumerable<DocumentIssueViewModel> ToViewModel(this IEnumerable<VsDocumentIssue> content)
        {
            var ViewModel = content.Select(contentItem => new DocumentIssueViewModel()
            {
                DateOfIssue = contentItem.DateOfIssue,
                DisplayedDOI = Convert.ToString(contentItem.DateOfIssue.ToShortDateString()),
                DateOfReturned = contentItem.DateOfReturned,
                DisplayedDOR =  contentItem.DateOfReturned.HasValue ? contentItem.DateOfReturned.Value.ToString("dd/MM/yyyy") : string.Empty,
                DesignationCol = contentItem.DesignationCol,                
                IsMember = contentItem.IsMember,
                IsReturned = contentItem.IsReturned,
                IssueDocumentName = contentItem.IssueDocumentName,
                IssueID = contentItem.IssueID,
                MemberCol = contentItem.MemberCol,
                TimeOfIssue = contentItem.TimeOfIssue,
                TimeOfReturned = contentItem.TimeOfReturn,
                SubscriberDesignationId = contentItem.SubscriberDesignationId,
                SubscriberId = contentItem.SubscriberId
            });
            return ViewModel;
        }
    }
}