﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Models
{
    public class PressClipViewModel
    {
        public int PressClipID { get; set; }


        [Required(ErrorMessage = "News Paper Name Required")]
        public int NewsPaperId { get; set; }

        public string NewsPaperName { get; set; }

        [Required(ErrorMessage = "News Date Required")]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime NewsDate { get; set; }

        //[Required(ErrorMessage = "Page Number Required")]
        public string PageNumber { get; set; }

        [Required(ErrorMessage = "Subject Required")]
        public string Subject { get; set; }
        //[Required(ErrorMessage = "Content Keywords Required")]
        public string ContentKeywords { get; set; }

        public string ScannedCopyImg { get; set; }

        public string AliasNameOfImg { get; set; }

        public string ContentText { get; set; }

        public string Category { get; set; }

        public bool? Status { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string Mode { get; set; }
        public string Msg { get; set; }
        public SelectList CategoryList { get; set; }
        public SelectList NewsPapaerList { get; set; }
       
    }
}