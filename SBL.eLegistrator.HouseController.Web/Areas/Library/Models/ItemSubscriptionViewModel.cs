﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Models
{
    public class ItemSubscriptionViewModel
    {


        public int ItemSubscriptionID { get; set; }

        [Required(ErrorMessage = "Item Name Required")]
        public string ItemName { get; set; }

        [Required(ErrorMessage = "Item Required")]
        public string ItemType { get; set; }

        [Required(ErrorMessage = "Language Required")]
        public string Language { get; set; }

        [Required(ErrorMessage = "Frequency Required")]
        public string Frequancy { get; set; }

        [Required(ErrorMessage = "Quantity Required")]
        public string Quantity { get; set; }

       // [Required(ErrorMessage = "Subscription Start Date Required")]
        public DateTime?  SubsStartDate { get; set; }

      //  [Required(ErrorMessage = "Subscription End Date Required")]
        public DateTime?  SubsEndDate { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }


        public string Mode { get; set; }
        public string Msg { get; set; }
        public SelectList ItemTypeList { get; set; }
        public SelectList LanguageList { get; set; }
        public SelectList FrequencyList { get; set; }
      
    }
}