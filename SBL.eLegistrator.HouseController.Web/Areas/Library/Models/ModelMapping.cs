﻿using SBL.DomainModel.Models.ItemSubscription;
using SBL.DomainModel.Models.PaidMemeberSubscription;
using SBL.DomainModel.Models.PressClipping;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.Library;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Models
{
    public static class ModelMapping
    {
        // Extenstion Method for PressClipping
        public static List<PressClipViewModel> ToViewModel(this IEnumerable<mPressClipping> model)
        {

            var ViewModel = model.Select(part => new PressClipViewModel()
            {
                PressClipID = part.PressClipID,
                NewsPaperId = part.NewsPaperId,
                NewsPaperName = part.NewsPaperName,
                NewsDate = part.NewsDate,
                PageNumber = part.PageNumber,
                Subject = part.Subject,
                ContentKeywords = part.ContentKeywords,
                ScannedCopyImg = part.ScannedCopyImg,
                AliasNameOfImg = part.AliasNameOfImg,
                ContentText = part.ContentText,
                Category = part.Category,
                Status = part.Status,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static PressClipViewModel ToViewModel1(this mPressClipping model, string Mode)
        {
            return new PressClipViewModel
            {
                PressClipID = model.PressClipID,
                NewsPaperId = model.NewsPaperId,
                NewsPaperName = model.NewsPaperName,
                NewsDate = model.NewsDate,
                PageNumber = model.PageNumber,
                Subject = model.Subject,
                ContentKeywords = model.ContentKeywords,
                ScannedCopyImg = model.ScannedCopyImg,
                AliasNameOfImg = model.AliasNameOfImg,
                ContentText = model.ContentText,
                Category = model.Category,
                Status = model.Status,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static mPressClipping ToDomainModel(this PressClipViewModel model)
        {
            return new mPressClipping
            {
                PressClipID = model.PressClipID,
                NewsPaperId = model.NewsPaperId,
             //   NewsPaperName = model.NewsPaperName,
                NewsDate = model.NewsDate,
                PageNumber = model.PageNumber,
                Subject = model.Subject,
                ContentKeywords = model.ContentKeywords,
                ScannedCopyImg = model.ScannedCopyImg,
                AliasNameOfImg = model.AliasNameOfImg,
                ContentText = model.ContentText,
                Category = model.Category,
                Status = model.Status,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }


        // Extenstion Method for PaidMemeberSubs
        public static List<PaidMemeberSubsViewModel> ToViewModel(this IEnumerable<PaidMemeberSubs> model)
        {

            var ViewModel = model.Select(part => new PaidMemeberSubsViewModel()
            {
                PaidMemeberSubsID = part.PaidMemeberSubsID,
                NameOfSubscriber = part.NameOfSubscriber,
                Address = part.Address,
                MobileNumber = part.MobileNumber,
                Designation = part.Designation,
                SubsStartDate = part.SubsStartDate,
                SubsEndDate = part.SubsEndDate,
                Remarks = part.Remarks,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                AmountPaid = part.AmountPaid
            });
            return ViewModel.ToList();
        }

        public static PaidMemeberSubsViewModel ToViewModel1(this PaidMemeberSubs model, string Mode)
        {
            return new PaidMemeberSubsViewModel
            {
                PaidMemeberSubsID = model.PaidMemeberSubsID,
                NameOfSubscriber = model.NameOfSubscriber,
                Address = model.Address,
                MobileNumber = model.MobileNumber,
                Designation = model.Designation,
                SubsStartDate = model.SubsStartDate,
                SubsEndDate = model.SubsEndDate,
                Remarks = model.Remarks,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode,
                AmountPaid = model.AmountPaid
            };
        }

        public static PaidMemeberSubs ToDomainModel(this PaidMemeberSubsViewModel model)
        {
            return new PaidMemeberSubs
            {
                PaidMemeberSubsID = model.PaidMemeberSubsID,
                NameOfSubscriber = model.NameOfSubscriber,
                Address = model.Address,
                MobileNumber = model.MobileNumber,
                Designation = model.Designation,
                SubsStartDate = model.SubsStartDate,
                SubsEndDate = model.SubsEndDate,
                Remarks = model.Remarks,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                AmountPaid = model.AmountPaid
            };
        }

        // Extenstion Method for PaidMemeberSubs
        public static List<ItemSubscriptionViewModel> ToViewModel(this IEnumerable<mItemSubscription> model)
        {

            var ViewModel = model.Select(part => new ItemSubscriptionViewModel()
            {
                ItemSubscriptionID = part.ItemSubscriptionID,
                ItemName = part.ItemName,
                ItemType = part.ItemType,
                Language = part.Language,
                Frequancy = part.Frequancy,
                Quantity = part.Quantity,
                SubsStartDate = part.SubsStartDate,
                SubsEndDate = part.SubsEndDate,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,                
            });
            return ViewModel.ToList();
        }

        public static ItemSubscriptionViewModel ToViewModel1(this mItemSubscription model, string Mode)
        {
            return new ItemSubscriptionViewModel
            {
               ItemSubscriptionID = model.ItemSubscriptionID,
                ItemName = model.ItemName,
                ItemType = model.ItemType,
                Language = model.Language,
                Frequancy = model.Frequancy,
               Quantity = model.Quantity,
                SubsStartDate = model.SubsStartDate,
                SubsEndDate = model.SubsEndDate,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static mItemSubscription ToDomainModel(this ItemSubscriptionViewModel model)
        {
            return new mItemSubscription
            {
                ItemSubscriptionID = model.ItemSubscriptionID,
                ItemName = model.ItemName,
                ItemType = model.ItemType,
                Language = model.Language,
                Frequancy = model.Frequancy,
                Quantity = model.Quantity,
                SubsStartDate = model.SubsStartDate,
                SubsEndDate = model.SubsEndDate,
                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }





        // Extension Method for CommitteeReport
        public static List<CommitteeReportViewModel> ToViewModel(this IEnumerable<tCommitteeReportLib> model)
        {

            var ViewModel = model.Select(part => new CommitteeReportViewModel()
            {
                CommitteeReportId = part.CommitteeReportId,
                ReportSource = part.ReportSource,
                NameofCommittee = part.NameofCommittee,
                Year = part.Year,
                ReportNo = part.ReportNo,
                LocationDetails = part.LocationDetails,
                Building = part.Building,
                Floor = part.Floor,
                Almirah = part.Almirah,
                Reck = part.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static CommitteeReportViewModel ToViewModel1(this tCommitteeReportLib model, string Mode)
        {
            return new CommitteeReportViewModel
            {
                CommitteeReportId = model.CommitteeReportId,
                ReportSource = model.ReportSource,
                NameofCommittee = model.NameofCommittee,
                Year = model.Year,
                ReportNo = model.ReportNo,
                LocationDetails = model.LocationDetails,
                Building = model.Building,
                Floor = model.Floor,
                Almirah = model.Almirah,
                Reck = model.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static tCommitteeReportLib ToDomainModel(this CommitteeReportViewModel model)
        {
            return new tCommitteeReportLib
            {
                CommitteeReportId = model.CommitteeReportId,
                ReportSource = model.ReportSource,
                NameofCommittee = model.NameofCommittee,
                Year = model.Year,
                ReportNo = model.ReportNo,
                LocationDetails = model.LocationDetails,
                Building = model.Building,
                Floor = model.Floor,
                Almirah = model.Almirah,
                Reck = model.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }



        // Extension Method for PaperLaid
        public static List<PaperLaidViewModel> ToViewModel(this IEnumerable<tPaperLaid> model)
        {

            var ViewModel = model.Select(part => new PaperLaidViewModel()
            {
                PaperLaidId = part.PaperLaidId,
                Date = part.Date,
                Year = part.Year,
                LibraryNo = part.LibraryNo,
                LocationDetails = part.LocationDetails,
                Building = part.Building,
                Floor = part.Floor,
                Almirah = part.Almirah,
                Reck = part.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static PaperLaidViewModel ToViewModel1(this tPaperLaid model, string Mode)
        {
            return new PaperLaidViewModel
            {
                PaperLaidId = model.PaperLaidId,
                Date = model.Date,
                Year = model.Year,
                LibraryNo = model.LibraryNo,
                LocationDetails = model.LocationDetails,
                Building = model.Building,
                Floor = model.Floor,
                Almirah = model.Almirah,
                Reck = model.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static tPaperLaid ToDomainModel(this PaperLaidViewModel model)
        {
            return new tPaperLaid
            {
                PaperLaidId = model.PaperLaidId,
                Date = model.Date,
                Year = model.Year,
                LibraryNo = model.LibraryNo,
                LocationDetails = model.LocationDetails,
                Building = model.Building,
                Floor = model.Floor,
                Almirah = model.Almirah,
                Reck = model.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }




        // Extension Method for FloorVersion
        public static List<FloorVersionViewModel> ToViewModel(this IEnumerable<tFloorVersion> model)
        {

            var ViewModel = model.Select(part => new FloorVersionViewModel()
            {
                FloorVersionId = part.FloorVersionId,
                Title = part.Title,
                Date = part.Date,
                Year = part.Year,
                CopyNo = part.CopyNo,
                LocationDetails = part.LocationDetails,
                Building = part.Building,
                Floor = part.Floor,
                Almirah = part.Almirah,
                Reck = part.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static FloorVersionViewModel ToViewModel1(this tFloorVersion model, string Mode)
        {
            return new FloorVersionViewModel
            {
                FloorVersionId = model.FloorVersionId,
                Title = model.Title,
                Date = model.Date,
                Year = model.Year,
                CopyNo = model.CopyNo,
                LocationDetails = model.LocationDetails,
                Building = model.Building,
                Floor = model.Floor,
                Almirah = model.Almirah,
                Reck = model.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static tFloorVersion ToDomainModel(this FloorVersionViewModel model)
        {
            return new tFloorVersion
            {
                FloorVersionId = model.FloorVersionId,
                Title = model.Title,
                Date = model.Date,
                Year = model.Year,
                CopyNo = model.CopyNo,
                LocationDetails = model.LocationDetails,
                Building = model.Building,
                Floor = model.Floor,
                Almirah = model.Almirah,
                Reck = model.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }

        // Extension Method for Whoiswho
        public static List<WhoiswhoViewModel> ToViewModel(this IEnumerable<tWhoiswho> model)
        {

            var ViewModel = model.Select(part => new WhoiswhoViewModel()
            {
                WhoiswhoId = part.WhoiswhoId,
                Source = part.Source,
                Date = part.Date,
                Year = part.Year,
                Num = part.Num,
                LocationDetails = part.LocationDetails,
                Building = part.Building,
                Floor = part.Floor,
                Almirah = part.Almirah,
                Reck = part.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            });
            return ViewModel.ToList();
        }

        public static WhoiswhoViewModel ToViewModel1(this tWhoiswho model, string Mode)
        {
            return new WhoiswhoViewModel
            {
                WhoiswhoId = model.WhoiswhoId,
                Source = model.Source,
                Date = model.Date,
                Year = model.Year,
                Num = model.Num,
                LocationDetails = model.LocationDetails,
                Building = model.Building,
                Floor = model.Floor,
                Almirah = model.Almirah,
                Reck = model.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName,
                Mode = Mode
            };
        }

        public static tWhoiswho ToDomainModel(this WhoiswhoViewModel model)
        {
            return new tWhoiswho
            {
                WhoiswhoId = model.WhoiswhoId,
                Source = model.Source,
                Date = model.Date,
                Year = model.Year,
                Num = model.Num,
                LocationDetails = model.LocationDetails,
                Building = model.Building,
                Floor = model.Floor,
                Almirah = model.Almirah,
                Reck = model.Reck,

                CreatedBy = CurrentSession.UserName,
                ModifiedBy = CurrentSession.UserName
            };
        }







        #region Private

        public static SelectList GetCategoryList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "General", Value = "General" });
            result.Add(new SelectListItem() { Text = "Vidhan Sabha", Value = "VidhanSabha" });

            return new SelectList(result, "Value", "Text");
        }

        public static SelectList GetItemTypeList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Magazine", Value = "Magazine" });
            result.Add(new SelectListItem() { Text = "Newspaper", Value = "Newspaper" });
            result.Add(new SelectListItem() { Text = "Journals", Value = "Journals" });
            result.Add(new SelectListItem() { Text = "Subscription", Value = "Subscription" });
            result.Add(new SelectListItem() { Text = "Budget", Value = "Budget" });
            result.Add(new SelectListItem() { Text = "Speech", Value = "Speech" });

            return new SelectList(result, "Value", "Text");
        }
        public static SelectList GetLanguageList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Hindi", Value = "Hindi" });
            result.Add(new SelectListItem() { Text = "English", Value = "English" });

            return new SelectList(result, "Value", "Text");
        }
        public static SelectList GetFrequencyList()
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem() { Text = "Daily", Value = "Daily" });
            result.Add(new SelectListItem() { Text = "Weekly", Value = "Weekly" });
            result.Add(new SelectListItem() { Text = "Fortnightly", Value = "Fortnightly" });
            result.Add(new SelectListItem() { Text = "Monthly", Value = "Monthly" });
            result.Add(new SelectListItem() { Text = "Quarterly", Value = "Quarterly" });

            return new SelectList(result, "Value", "Text");
        }
        #endregion
    }
}