﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Models
{
    public class PaidMemeberSubsViewModel
    {

         
        public int PaidMemeberSubsID { get; set; }

        [Required(ErrorMessage = "Name of Subscriber Required")]
        public string NameOfSubscriber { get; set; }


        [Required(ErrorMessage = "Address Required")]
        public string Address { get; set; }

         [Required(ErrorMessage = "Mobile Number Required")]
        public string MobileNumber { get; set; }

        [Required(ErrorMessage = "Designation Required")]
        public string Designation { get; set; }
        [Required(ErrorMessage = "Subscription Start Date Required")]
        public DateTime SubsStartDate { get; set; }
         [Required(ErrorMessage = "Subscription End Date Required")]
        public DateTime SubsEndDate { get; set; }
         [Required(ErrorMessage = "Remarks Required")]
        public string Remarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public int AmountPaid { get; set; }

        public string Mode { get; set; }
        public string Msg { get; set; }


         
    }
}