﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Areas.Library.Models
{
    public class FloorVersionViewModel
    {

        public int FloorVersionId { get; set; }

        [Required(ErrorMessage = "Title Required")]
        [StringLength(100, ErrorMessage = "Maximum 100 characters allowed")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Date Required")]
        public string Date { get; set; }


        [Required(ErrorMessage = "Year Required")]
        [StringLength(4, ErrorMessage = "Maximum 4 digits allowed")]
        public string Year { get; set; }

        [Required(ErrorMessage = "Copy No. Required")]
        [StringLength(2, ErrorMessage = "Maximum 2 digits allowed")]
        public string CopyNo { get; set; }

        //[Required(ErrorMessage = "Location Details Required")]
        public string LocationDetails { get; set; }

        [Required(ErrorMessage = "Building Details Required")]
        [StringLength(25, ErrorMessage = "Maximum 25 characters allowed")]
        public string Building { get; set; }

        [Required(ErrorMessage = "Floor Details Required")]
        [StringLength(25, ErrorMessage = "Maximum 25 characters allowed")]
        public string Floor { get; set; }

        [Required(ErrorMessage = "Almirah Details Required")]
        [StringLength(25, ErrorMessage = "Maximum 25 characters allowed")]
        public string Almirah { get; set; }

        //[Required(ErrorMessage = "Reck Details Required")]
        // [StringLength(1, ErrorMessage = "Maximum 1 digits allowed")]
        public string Reck { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }


        public string Mode { get; set; }
        public string Msg { get; set; }


    }
}