﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace SBL.eLegistrator.HouseController.Web.Utility
{
    public class DataUtility
    {
        public DataUtility()
        {
            sqlcon = new SqlConnection();
            sqlcon.ConnectionString = getConnectionString();
            sqlcon.Open();

        }
        public static string getConnectionString()
        {
            string txtpath = System.Web.HttpContext.Current.Server.MapPath("/ConnectionFile/connectionString.txt");
            try
            {
                if (File.Exists(txtpath))
                {
                    using (StreamReader sr = new StreamReader(txtpath))
                    {
                        while (sr.Peek() >= 0)
                        {
                            string ss = sr.ReadLine();
                            string[] txtsplit = ss.Split(';');
                            string server = txtsplit[0].ToString();
                            string userid = txtsplit[1].ToString();
                            string password = txtsplit[2].ToString();
                            StringBuilder str = new StringBuilder();
                            str.Append("Server =").Append(server).Append("; Database = ").Append("Email").Append("; User Id = ").Append(userid).Append("; Password = ").Append(password);
                            return str.ToString();
#pragma warning disable CS0162 // Unreachable code detected
                            break;
#pragma warning restore CS0162 // Unreachable code detected
                        }

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: {0}", e.ToString());
            }
            return string.Empty;

        }
        # region All Module level variables
#pragma warning disable CS0169 // The field 'DataUtility.sqlda' is never used
        SqlDataAdapter sqlda;
#pragma warning restore CS0169 // The field 'DataUtility.sqlda' is never used
        SqlConnection sqlcon;
#pragma warning disable CS0169 // The field 'DataUtility.ds' is never used
        DataSet ds;
#pragma warning restore CS0169 // The field 'DataUtility.ds' is never used
#pragma warning disable CS0169 // The field 'DataUtility.sqldr' is never used
        SqlDataReader sqldr;
#pragma warning restore CS0169 // The field 'DataUtility.sqldr' is never used
#pragma warning disable CS0169 // The field 'DataUtility.sqltransaction' is never used
        SqlTransaction sqltransaction;
#pragma warning restore CS0169 // The field 'DataUtility.sqltransaction' is never used
#pragma warning disable CS0169 // The field 'DataUtility.sqlcmd' is never used
        SqlCommand sqlcmd;
#pragma warning restore CS0169 // The field 'DataUtility.sqlcmd' is never used

#pragma warning disable CS0169 // The field 'DataUtility.query' is never used
        private string query;
#pragma warning restore CS0169 // The field 'DataUtility.query' is never used

        # endregion
    }
}