﻿using Newtonsoft.Json;
using SBL.eLegistrator.HouseController.Web.App_Start;
using SBL.eLegistrator.HouseController.Web.Controllers;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Filters.Security;
using SBL.eLegistrator.HouseController.Web.Utility;
using StackExchange.Profiling;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;


namespace SBL.eLegistrator.HouseController.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new NoCacheAttribute());
            //\\\\\\filters.Add(new ElmahHTTPErrorAttribute());

        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
       "BillWorks" // Route name
       , "SecureFiles/{Wid}" // URL with parameters
       , new { area = "BillWorks", controller = "BillWorks", action = "DownloadPdf", id = UrlParameter.Optional } // Parameter defaults               
   );

            routes.MapRoute(
       "PrintingPress" // Route name
       , "SecureFile/{Wid}" // URL with parameters
       , new { area = "PrintingPress", controller = "PrintingPress", action = "DownloadPdf", id = UrlParameter.Optional } // Parameter defaults               
   );

            routes.MapRoute(
    "PaperLaidDepartment" // Route name
    , "SecuredFiles/{Wid}" // URL with parameters
    , new { area = "PaperLaidDepartment", controller = "PaperLaidDepartment", action = "DownloadPdf", id = UrlParameter.Optional } // Parameter defaults               
);

            routes.MapRoute(
"PaperLaidMinister" // Route name
, "SecuredTempFiles/{Wid}" // URL with parameters
, new { area = "PaperLaidMinister", controller = "PaperLaidMinister", action = "DownloadPdf", id = UrlParameter.Optional } // Parameter defaults               
);

            routes.MapRoute(
                "Diaries", // Route name
                "SecureFiles/{id}", // URL with parameters
                new { area = "Notices", controller = "Diaries", action = "DisplayQuestionPDFByID", id = UrlParameter.Optional } // Parameter defaults
            );



            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Account", action = "LoginUP", id = UrlParameter.Optional } // Parameter defaults
            );



        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            // Use LocalDB for Entity Framework by default
#pragma warning disable CS0618 // 'Database.DefaultConnectionFactory' is obsolete: 'The default connection factory should be set in the config file or using the DbConfiguration class. (See http://go.microsoft.com/fwlink/?LinkId=260883)'
            Database.DefaultConnectionFactory = new SqlConnectionFactory(@"Data Source=(localdb)\v11.0; Integrated Security=True; MultipleActiveResultSets=True");
#pragma warning restore CS0618 // 'Database.DefaultConnectionFactory' is obsolete: 'The default connection factory should be set in the config file or using the DbConfiguration class. (See http://go.microsoft.com/fwlink/?LinkId=260883)'

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //TODO : For testing purpose we will be setting AntiForgeryConfig.RequireSsl value to false
            //Otherwise it should be true for the Server.
            //if (Request.IsLocal)
            //{
           AntiForgeryConfig.RequireSsl = false;
           AntiForgeryConfig.SuppressIdentityHeuristicChecks = false;
            MvcHandler.DisableMvcResponseHeader = true;
            //}
            //else
            //{
            //    AntiForgeryConfig.RequireSsl = true;

            //}

        }
        //protected void Session_Start(Object sender, EventArgs e)
        //{
        //    if (Request.IsSecureConnection)
        //    {
        //        Response.Cookies["ASP.NET_SessionId"].Secure = true;
        //    }
        //}
        protected void Session_End(Object sender, EventArgs e)
        {
           
            Session.Abandon(); //ashwani
           // CurrentSession.ClearSessions();
        }
        public void Session_OnStart()
        {
            Application.Lock();
            CurrentSession.InitializeValues();
            Session.Timeout = 20;
            
            Application.UnLock();
        }

        //protected void Application_AcquireRequestState(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        /// To Prevent Session Hijacking
        //        //if (Request.UrlReferrer == null && Request.Path != "/" && Request.Path != "/Account/Login")
        //        //{
        //        //    Response.Redirect("~/Account/Login", true);
        //        //    //Response.Redirect("~/Error/Error404", true);
        //        //}

        //        if (HttpContext.Current.Session != null)
        //        {
        //            if (Session["encryptedSession"] != null)
        //            {
        //                string _sessionIPAddress = string.Empty;
        //                string _sessionBrowserInfo = string.Empty;
        //                if (HttpContext.Current.Session != null)
        //                {

        //                    string authcookie = Request.Cookies["AuthToken"].Value;
        //                    string _encryptedString = Convert.ToString(Session["encryptedSession"]);
        //                    byte[] _encodedAsBytes = System.Convert.FromBase64String(_encryptedString);
        //                    string _decryptedString = System.Text.ASCIIEncoding.ASCII.GetString(_encodedAsBytes);

        //                    char[] _separator = new char[] { '^' };
        //                    if (_decryptedString != string.Empty && _decryptedString != "" && _decryptedString != null)
        //                    {
        //                        string[] _splitStrings = _decryptedString.Split(_separator);
        //                        if (_splitStrings.Count() > 0)
        //                        {
        //                            if (_splitStrings[2].Count() > 0)
        //                            {
        //                                string[] _userBrowserInfo = _splitStrings[2].Split('~');
        //                                if (_userBrowserInfo.Count() > 0)
        //                                {
        //                                    _sessionBrowserInfo = _userBrowserInfo[0];
        //                                    _sessionIPAddress = _userBrowserInfo[1];
        //                                }
        //                            }
        //                        }
        //                    }

        //                    string _currentUseripAddress;
        //                    if (string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
        //                    {
        //                        _currentUseripAddress = Request.ServerVariables["REMOTE_ADDR"];
        //                    }
        //                    else
        //                    {
        //                        _currentUseripAddress =
        //                        Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
        //                    }

        //                    System.Net.IPAddress result;
        //                    if (!System.Net.IPAddress.TryParse(_currentUseripAddress, out result))
        //                    {
        //                        result = System.Net.IPAddress.None;
        //                    }

        //                    if (_sessionIPAddress != "" && _sessionIPAddress != string.Empty)
        //                    {
        //                        //Same way we can validate browser info also...
        //                        string _currentBrowserInfo = Request.Browser.Browser + Request.Browser.Version + Request.UserAgent;
        //                        if (_sessionIPAddress != _currentUseripAddress || _sessionBrowserInfo != _currentBrowserInfo)  // || authcookie != Convert.ToString(Session["encryptedSession"]))
        //                        {
        //                            Response.Redirect("~/Account/Login", true);
        //                            //Response.Redirect("~/Error/Error404", true);
        //                        }

        //                        if (_decryptedString != string.Empty && _decryptedString != "" && _decryptedString != null)
        //                        {
        //                            string _browserInfo = Request.Browser.Browser
        //                            + Request.Browser.Version
        //                            + Request.UserAgent + "~"
        //                            + Request.ServerVariables["REMOTE_ADDR"];

        //                            string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
        //                                                    + DateTime.Now.Ticks + "^"
        //                                                    + _browserInfo + "^"
        //                                                    + System.Guid.NewGuid();

        //                            byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
        //                            string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

        //                            //if (Request.Cookies["AuthToken"] != null)
        //                            //{
        //                            //    var c = new HttpCookie("AuthToken");
        //                            //    c.Value = _newEncryptedString;
        //                            //    Response.SetCookie(c);
        //                            //}
        //                            //else
        //                            //{
        //                            //    var c = new HttpCookie("AuthToken");
        //                            //    c.Value = _newEncryptedString;
        //                            //    Response.Cookies.Add(c);
        //                            //}

        //                            Session["encryptedSession"] = _newEncryptedString;

        //                        }

        //                    }
        //                }
        //            }
        //        }









        //        string langCulture = SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture;
        //        var langCookie = new HttpCookie("lang", langCulture)
        //        {
        //            HttpOnly = true,
        //            Secure = true,

        //        };
        //        langCookie.Expires =DateTime.Now.AddDays(-1);

        //        Response.Cookies.Add(langCookie);
        //        if (langCookie != null)
        //        {
        //            var ci = new CultureInfo(langCookie.Value);
        //            //Checking first if there is no value in session 
        //            //and set default language 
        //            //this can happen for first user's request
        //            if (ci == null)
        //            {
        //                //Sets default culture to english invariant
        //                string langName = "en";

        //                //Try to get values from Accept lang HTTP header
        //                if (HttpContext.Current.Request.UserLanguages != null && HttpContext.Current.Request.UserLanguages.Length != 0)
        //                {
        //                    //Gets accepted list 
        //                    langName = HttpContext.Current.Request.UserLanguages[0].Substring(0, 2);
        //                }

        //                langCookie = new HttpCookie("lang", langName)
        //                {
        //                    HttpOnly = true,
        //                    Secure = true
        //                };
        //                langCookie.Expires =DateTime.Now.AddDays(-1);
        //                HttpContext.Current.Response.AppendCookie(langCookie);
        //            }

        //            //Finally setting culture for each request
        //            Thread.CurrentThread.CurrentUICulture = ci;
        //            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(ci.Name);
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        //Response.Redirect("~/Account/Login", true);
        //    }

        //}

        public void FormsAuthentication_OnAuthenticate(object sender, FormsAuthenticationEventArgs args)
        {
            if (FormsAuthentication.CookiesSupported)
            {
                if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    try
                    {
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(
                          Request.Cookies[FormsAuthentication.FormsCookieName].Value);
                        args.User = new System.Security.Principal.GenericPrincipal(
                          new System.Security.Principal.GenericIdentity(ticket.Name),
                          new string[0]);
                    }
#pragma warning disable CS0168 // The variable 'e' is declared but never used
                    catch (Exception e)
#pragma warning restore CS0168 // The variable 'e' is declared but never used
                    {
                        // Decrypt method failed.
                    }
                }
            }
            else
            {
                throw new HttpException("Cookieless Forms Authentication is not " +
                                        "supported for this application.");
            }
        }

        protected void Application_BeginRequest()
        {
            //var loadbalancerReceivedSslRequest = string.Equals(Request.Headers["X-Forwarded-Proto"], "https");
            //var serverReceivedSslRequest = Request.IsSecureConnection;

            //if (loadbalancerReceivedSslRequest || serverReceivedSslRequest) return;

            //UriBuilder uri = new UriBuilder(Context.Request.Url);
            //if (!uri.Host.Equals("localhost"))
            //{
            //    uri.Port = 443;
            //    uri.Scheme = "https";
            //    Response.Redirect(uri.ToString());
            //}
        }
        //protected void Application_EndRequest(object sender, EventArgs e)
        //{

        //    MiniProfiler.Stop();
        //    if (Response.Cookies.Count > 0)
        //    {
        //        foreach (string s in Response.Cookies.AllKeys)
        //        {
        //            if (s == FormsAuthentication.FormsCookieName || s.ToLower() == "asp.net_sessionid")
        //            {
        //                Response.Cookies[s].Secure = true;
        //            }
        //        }
        //    }
        //}

        // ashwani
        protected void Application_PreSendRequestHeaders()
        {
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Response.Headers.Remove("Server");
            }
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {

                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
               

                CustomPrincipalSerializeModel serializeModel = JsonConvert.DeserializeObject<CustomPrincipalSerializeModel>(authTicket.UserData);
                CustomPrincipal newUser = new CustomPrincipal(authTicket.Name);
                if (newUser != null && serializeModel != null)
                {
                    newUser.UserId = serializeModel.UserId;
                    newUser.FirstName = serializeModel.FirstName;
                    newUser.LastName = serializeModel.LastName;
                    newUser.roles = serializeModel.roles;
                    newUser.modules = serializeModel.modules;
                    newUser.UserType = serializeModel.UserType;

                    HttpContext.Current.User = newUser;
                }
            }

        }
        protected void Application_Error(object sender, EventArgs e)
        {

            Exception exception = Server.GetLastError();
            Server.ClearError();


            //Exception exception = Server.GetLastError();
            // Server.ClearError();

            // var error = Server.GetLastError();

            //  if ((error as HttpException).GetHttpCode() == 404)
            // {
            //     Server.ClearError();
            //     Response.StatusCode = 404;
            // }


            // if ((error as HttpException).GetHttpCode() == 500)
            // {
            //     Session.Clear();
            //     Session.Abandon();
            // }
            // if (exception.Message.Contains("The controller for path"))
            // {
            //     Session.Clear();
            //     Session.Abandon();
            //     Server.ClearError();
            //     RouteData routeData = new RouteData();
            //     routeData.Values.Add("controller", "Error");
            //     routeData.Values.Add("action", "Index");
            //     routeData.Values.Add("exception", exception);
            //     if (exception.GetType() == typeof(HttpException))
            //     {
            //         routeData.Values.Add("statusCode", ((HttpException)exception).GetHttpCode());
            //     }
            //     else
            //     {
            //         routeData.Values.Add("statusCode", 500);
            //     }
            //     string url = HttpContext.Current.Request.Url.AbsoluteUri;
            //     routeData.Values.Add("returnurl", url);

            //     IController controller = new ErrorController();
            //     controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData)); Response.End();
            // }



        }
       
    }
}