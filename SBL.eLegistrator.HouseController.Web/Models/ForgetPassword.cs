﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Models
{
    public class ForgetPassword
    {
       //[Required(ErrorMessage = "Please Select Valid Date Of Birth")]
       public string DOB { get; set; }

        [Required(ErrorMessage = "Please Enter valid Aadhar ID")]
        public string AadharID { get; set; }

        [Required(ErrorMessage = "Please Enter valid OTP")]
        public string OTP { get; set; }

        public string CaptchaMessage { get; set; }
    }
}