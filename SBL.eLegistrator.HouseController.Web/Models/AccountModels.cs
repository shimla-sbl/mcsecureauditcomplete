﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace SBL.eLegistrator.HouseController.Web.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Username required")]
        
        [Display(Name = "User name")]
     
        [RegularExpression(@"^[\w_]+$", ErrorMessage = "Please Enter Valid User Name")]
        public string UserName { get; set; }
       
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        public string returnUrl { get; set; }

        public string LoginDepartmentName { get; set; }
  
        public string CaptchaMessage { get; set; }

        [Required(ErrorMessage = "Password required")]
        public string ActualPassword { get; set; }

        public string ErrorMessage { get; set; }
        public int UserType { get; set; }

    }
}