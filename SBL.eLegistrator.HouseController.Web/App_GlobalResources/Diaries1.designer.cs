//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Diaries {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Diaries() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Diaries", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Signature.
        /// </summary>
        internal static string AddSign {
            get {
                return ResourceManager.GetString("AddSign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All.
        /// </summary>
        internal static string All {
            get {
                return ResourceManager.GetString("All", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approve.
        /// </summary>
        internal static string Approve {
            get {
                return ResourceManager.GetString("Approve", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Approved Date.
        /// </summary>
        internal static string ApprovedDate {
            get {
                return ResourceManager.GetString("ApprovedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Asked By.
        /// </summary>
        internal static string AskedBy {
            get {
                return ResourceManager.GetString("AskedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Assigned On.
        /// </summary>
        internal static string AssignedOn {
            get {
                return ResourceManager.GetString("AssignedOn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Online Send By Member.
        /// </summary>
        internal static string AttachedPaperByMember {
            get {
                return ResourceManager.GetString("AttachedPaperByMember", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back To Previous Level.
        /// </summary>
        internal static string BackToPreviousLevel {
            get {
                return ResourceManager.GetString("BackToPreviousLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bracket.
        /// </summary>
        internal static string Bracket {
            get {
                return ResourceManager.GetString("Bracket", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bracketed / Clubbed.
        /// </summary>
        internal static string BracketedClubbed {
            get {
                return ResourceManager.GetString("BracketedClubbed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bracketed.
        /// </summary>
        internal static string BracketedQuestions {
            get {
                return ResourceManager.GetString("BracketedQuestions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bracketed With.
        /// </summary>
        internal static string BracketedWith {
            get {
                return ResourceManager.GetString("BracketedWith", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Business Type.
        /// </summary>
        internal static string BusinessType {
            get {
                return ResourceManager.GetString("BusinessType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Content.
        /// </summary>
        internal static string ChangeContent {
            get {
                return ResourceManager.GetString("ChangeContent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Changed Notices.
        /// </summary>
        internal static string ChangedNotice {
            get {
                return ResourceManager.GetString("ChangedNotice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Paper.
        /// </summary>
        internal static string ChangePaper {
            get {
                return ResourceManager.GetString("ChangePaper", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Signature.
        /// </summary>
        internal static string ChangeSign {
            get {
                return ResourceManager.GetString("ChangeSign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Type.
        /// </summary>
        internal static string ChangeType {
            get {
                return ResourceManager.GetString("ChangeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clubbed / Revised.
        /// </summary>
        internal static string Clubbed {
            get {
                return ResourceManager.GetString("Clubbed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ward.
        /// </summary>
        internal static string Constituency {
            get {
                return ResourceManager.GetString("Constituency", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ward Name.
        /// </summary>
        internal static string ConstituencyName {
            get {
                return ResourceManager.GetString("ConstituencyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Department.
        /// </summary>
        internal static string Department {
            get {
                return ResourceManager.GetString("Department", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        internal static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Details.
        /// </summary>
        internal static string Details {
            get {
                return ResourceManager.GetString("Details", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Notice Office.
        /// </summary>
        internal static string DiariesScreen {
            get {
                return ResourceManager.GetString("DiariesScreen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diary.
        /// </summary>
        internal static string Diary {
            get {
                return ResourceManager.GetString("Diary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diary No..
        /// </summary>
        internal static string DiaryNumber {
            get {
                return ResourceManager.GetString("DiaryNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Done.
        /// </summary>
        internal static string Done {
            get {
                return ResourceManager.GetString("Done", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        internal static string DType {
            get {
                return ResourceManager.GetString("DType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        internal static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fixed Date.
        /// </summary>
        internal static string FixedDate {
            get {
                return ResourceManager.GetString("FixedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fixed Questions.
        /// </summary>
        internal static string FixedQuestion {
            get {
                return ResourceManager.GetString("FixedQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fixing Questions.
        /// </summary>
        internal static string Fixing {
            get {
                return ResourceManager.GetString("Fixing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to For Bracketing.
        /// </summary>
        internal static string ForBrackting {
            get {
                return ResourceManager.GetString("ForBrackting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to For Clubbing / Revise.
        /// </summary>
        internal static string ForClubbing {
            get {
                return ResourceManager.GetString("ForClubbing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Freeze.
        /// </summary>
        internal static string Freeze {
            get {
                return ResourceManager.GetString("Freeze", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Freezed By.
        /// </summary>
        internal static string FreezedBy {
            get {
                return ResourceManager.GetString("FreezedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Freezed On.
        /// </summary>
        internal static string FreezedDate {
            get {
                return ResourceManager.GetString("FreezedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inbox.
        /// </summary>
        internal static string Inbox {
            get {
                return ResourceManager.GetString("Inbox", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submitted Date.
        /// </summary>
        internal static string MinisterSubmittedDate {
            get {
                return ResourceManager.GetString("MinisterSubmittedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Asked From.
        /// </summary>
        internal static string Ministry {
            get {
                return ResourceManager.GetString("Ministry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Unstarred.
        /// </summary>
        internal static string MovetoUnstarred {
            get {
                return ResourceManager.GetString("MovetoUnstarred", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Dashboard.
        /// </summary>
        internal static string MyDashboard {
            get {
                return ResourceManager.GetString("MyDashboard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New.
        /// </summary>
        internal static string New {
            get {
                return ResourceManager.GetString("New", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next.
        /// </summary>
        internal static string Next {
            get {
                return ResourceManager.GetString("Next", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Record Found.
        /// </summary>
        internal static string NoRecords {
            get {
                return ResourceManager.GetString("NoRecords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resolution Detail.
        /// </summary>
        internal static string NoticeDetail {
            get {
                return ResourceManager.GetString("NoticeDetail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resolution Number.
        /// </summary>
        internal static string NoticeNumber {
            get {
                return ResourceManager.GetString("NoticeNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resolutions.
        /// </summary>
        internal static string Notices {
            get {
                return ResourceManager.GetString("Notices", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number.
        /// </summary>
        internal static string Number {
            get {
                return ResourceManager.GetString("Number", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old Type.
        /// </summary>
        internal static string OldType {
            get {
                return ResourceManager.GetString("OldType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Original Diary Paper.
        /// </summary>
        internal static string OriginalDiaryPaper {
            get {
                return ResourceManager.GetString("OriginalDiaryPaper", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Original Notices.
        /// </summary>
        internal static string OriginalNotice {
            get {
                return ResourceManager.GetString("OriginalNotice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pending.
        /// </summary>
        internal static string Pending {
            get {
                return ResourceManager.GetString("Pending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postpone On.
        /// </summary>
        internal static string PostponeDate {
            get {
                return ResourceManager.GetString("PostponeDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Postponing Questions.
        /// </summary>
        internal static string Postponing {
            get {
                return ResourceManager.GetString("Postponing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Print For Correction.
        /// </summary>
        internal static string PrintForCorrection {
            get {
                return ResourceManager.GetString("PrintForCorrection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Priority.
        /// </summary>
        internal static string Priority {
            get {
                return ResourceManager.GetString("Priority", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proof Reading On.
        /// </summary>
        internal static string ProofDate {
            get {
                return ResourceManager.GetString("ProofDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Proof Reading By.
        /// </summary>
        internal static string ProofReadingBy {
            get {
                return ResourceManager.GetString("ProofReadingBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Question Detail.
        /// </summary>
        internal static string Question {
            get {
                return ResourceManager.GetString("Question", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Question Number.
        /// </summary>
        internal static string QuestionNumber {
            get {
                return ResourceManager.GetString("QuestionNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Is Question In Part.
        /// </summary>
        internal static string QuestionPart {
            get {
                return ResourceManager.GetString("QuestionPart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Questions.
        /// </summary>
        internal static string Questions {
            get {
                return ResourceManager.GetString("Questions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Questions For Clubbing / Revise.
        /// </summary>
        internal static string QuestionsForClubbingRevise {
            get {
                return ResourceManager.GetString("QuestionsForClubbingRevise", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reason.
        /// </summary>
        internal static string Reason {
            get {
                return ResourceManager.GetString("Reason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Received Date.
        /// </summary>
        internal static string ReceivedDate {
            get {
                return ResourceManager.GetString("ReceivedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Received Time.
        /// </summary>
        internal static string ReceivedTime {
            get {
                return ResourceManager.GetString("ReceivedTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reject.
        /// </summary>
        internal static string Reject {
            get {
                return ResourceManager.GetString("Reject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reject Description.
        /// </summary>
        internal static string RejectDescription {
            get {
                return ResourceManager.GetString("RejectDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rejected By.
        /// </summary>
        internal static string RejectedBy {
            get {
                return ResourceManager.GetString("RejectedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rejected Date.
        /// </summary>
        internal static string RejectedDate {
            get {
                return ResourceManager.GetString("RejectedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rejected Questions.
        /// </summary>
        internal static string RejectedQuestion {
            get {
                return ResourceManager.GetString("RejectedQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rejection Rule.
        /// </summary>
        internal static string RejectionRule {
            get {
                return ResourceManager.GetString("RejectionRule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reject Reason.
        /// </summary>
        internal static string RejectReason {
            get {
                return ResourceManager.GetString("RejectReason", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remark.
        /// </summary>
        internal static string Remark {
            get {
                return ResourceManager.GetString("Remark", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remove Bracketing.
        /// </summary>
        internal static string RemoveBracketing {
            get {
                return ResourceManager.GetString("RemoveBracketing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reply Received On.
        /// </summary>
        internal static string ReplyReceivedDate {
            get {
                return ResourceManager.GetString("ReplyReceivedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset.
        /// </summary>
        internal static string Reset {
            get {
                return ResourceManager.GetString("Reset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rule.
        /// </summary>
        internal static string Rule {
            get {
                return ResourceManager.GetString("Rule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        internal static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Freeze.
        /// </summary>
        internal static string SaveFreeze {
            get {
                return ResourceManager.GetString("SaveFreeze", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stage.
        /// </summary>
        internal static string Stage {
            get {
                return ResourceManager.GetString("Stage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Questions.
        /// </summary>
        internal static string Starred {
            get {
                return ResourceManager.GetString("Starred", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Questions.
        /// </summary>
        internal static string StarredQuestion {
            get {
                return ResourceManager.GetString("StarredQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Question Details.
        /// </summary>
        internal static string StarredQuestionDetails {
            get {
                return ResourceManager.GetString("StarredQuestionDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to To Starred .
        /// </summary>
        internal static string StarredToUnstarred {
            get {
                return ResourceManager.GetString("StarredToUnstarred", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        internal static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subject (in Catch Word).
        /// </summary>
        internal static string SubinCatchword {
            get {
                return ResourceManager.GetString("SubinCatchword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Subject.
        /// </summary>
        internal static string Subject {
            get {
                return ResourceManager.GetString("Subject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sent.
        /// </summary>
        internal static string Submit {
            get {
                return ResourceManager.GetString("Submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Question and Notice.
        /// </summary>
        internal static string SubmitDepartment {
            get {
                return ResourceManager.GetString("SubmitDepartment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pending.
        /// </summary>
        internal static string SubmitPending {
            get {
                return ResourceManager.GetString("SubmitPending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submitted Date.
        /// </summary>
        internal static string SubmittedDate {
            get {
                return ResourceManager.GetString("SubmittedDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type Change.
        /// </summary>
        internal static string TypeChange {
            get {
                return ResourceManager.GetString("TypeChange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type Change Date.
        /// </summary>
        internal static string TypeChangeDate {
            get {
                return ResourceManager.GetString("TypeChangeDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Typing By.
        /// </summary>
        internal static string TypingBy {
            get {
                return ResourceManager.GetString("TypingBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UnFixed Questions.
        /// </summary>
        internal static string UnFixedQuestion {
            get {
                return ResourceManager.GetString("UnFixedQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Un Freeze.
        /// </summary>
        internal static string UnFreeze {
            get {
                return ResourceManager.GetString("UnFreeze", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unstarred.
        /// </summary>
        internal static string Unstarred {
            get {
                return ResourceManager.GetString("Unstarred", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unstarred Questions.
        /// </summary>
        internal static string UnstarredQuestion {
            get {
                return ResourceManager.GetString("UnstarredQuestion", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unstarred Question Details.
        /// </summary>
        internal static string UnstarredQuestionDetails {
            get {
                return ResourceManager.GetString("UnstarredQuestionDetails", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View.
        /// </summary>
        internal static string View {
            get {
                return ResourceManager.GetString("View", resourceCulture);
            }
        }
    }
}
