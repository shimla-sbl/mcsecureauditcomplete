//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class UserRegistration {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UserRegistration() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.UserRegistration", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Administrative Head.
        /// </summary>
        internal static string AdministrativeHead {
            get {
                return ResourceManager.GetString("AdministrativeHead", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Administrative Secretary.
        /// </summary>
        internal static string AdministrativeSecretary {
            get {
                return ResourceManager.GetString("AdministrativeSecretary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date of Birth.
        /// </summary>
        internal static string Age {
            get {
                return ResourceManager.GetString("Age", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Applet failed to run.  No Java plug-in was found..
        /// </summary>
        internal static string AppletError {
            get {
                return ResourceManager.GetString("AppletError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Back.
        /// </summary>
        internal static string Back {
            get {
                return ResourceManager.GetString("Back", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string BtnCancel {
            get {
                return ResourceManager.GetString("BtnCancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset.
        /// </summary>
        internal static string BtnReset {
            get {
                return ResourceManager.GetString("BtnReset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        internal static string BtnSubmit {
            get {
                return ResourceManager.GetString("BtnSubmit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update.
        /// </summary>
        internal static string BtnUpdate {
            get {
                return ResourceManager.GetString("BtnUpdate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Captcha.
        /// </summary>
        internal static string Captcha {
            get {
                return ResourceManager.GetString("Captcha", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Change Password.
        /// </summary>
        internal static string ChangePassword {
            get {
                return ResourceManager.GetString("ChangePassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirmation Password.
        /// </summary>
        internal static string ConfirmationPassword {
            get {
                return ResourceManager.GetString("ConfirmationPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Department.
        /// </summary>
        internal static string Department {
            get {
                return ResourceManager.GetString("Department", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Department of employee.
        /// </summary>
        internal static string DepartmentList {
            get {
                return ResourceManager.GetString("DepartmentList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Designation.
        /// </summary>
        internal static string Designation {
            get {
                return ResourceManager.GetString("Designation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active.
        /// </summary>
        internal static string DSCActivate {
            get {
                return ResourceManager.GetString("DSCActivate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DeActivate.
        /// </summary>
        internal static string DSCDeActivate {
            get {
                return ResourceManager.GetString("DSCDeActivate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        internal static string DSCDelete {
            get {
                return ResourceManager.GetString("DSCDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enroll. Date.
        /// </summary>
        internal static string DSCEnroll {
            get {
                return ResourceManager.GetString("DSCEnroll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        internal static string DSCName {
            get {
                return ResourceManager.GetString("DSCName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Note: Please plug-in USB token first to select the DSC.
        /// </summary>
        internal static string DSCNote {
            get {
                return ResourceManager.GetString("DSCNote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select DSC for Registration.
        /// </summary>
        internal static string DSCSelection {
            get {
                return ResourceManager.GetString("DSCSelection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        internal static string DSCStatus {
            get {
                return ResourceManager.GetString("DSCStatus", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        internal static string DSCType {
            get {
                return ResourceManager.GetString("DSCType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validity Date.
        /// </summary>
        internal static string DSCValidity {
            get {
                return ResourceManager.GetString("DSCValidity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Employee Code.
        /// </summary>
        internal static string EmployeeCode {
            get {
                return ResourceManager.GetString("EmployeeCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Father&apos;s Name.
        /// </summary>
        internal static string FatherName {
            get {
                return ResourceManager.GetString("FatherName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Gender.
        /// </summary>
        internal static string Gender {
            get {
                return ResourceManager.GetString("Gender", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Get OTP.
        /// </summary>
        internal static string GetOTP {
            get {
                return ResourceManager.GetString("GetOTP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Head Of Department.
        /// </summary>
        internal static string HeadOfDepartment {
            get {
                return ResourceManager.GetString("HeadOfDepartment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hon&apos;ble Member.
        /// </summary>
        internal static string Honable {
            get {
                return ResourceManager.GetString("Honable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to H.P Secretariat Employee.
        /// </summary>
        internal static string HPSecretariatEmployee {
            get {
                return ResourceManager.GetString("HPSecretariatEmployee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Media.
        /// </summary>
        internal static string Media {
            get {
                return ResourceManager.GetString("Media", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Journalist Code.
        /// </summary>
        internal static string MediaCode {
            get {
                return ResourceManager.GetString("MediaCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Member.
        /// </summary>
        internal static string Member {
            get {
                return ResourceManager.GetString("Member", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Member Administrator.
        /// </summary>
        internal static string MemberAdministrator {
            get {
                return ResourceManager.GetString("MemberAdministrator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Member Code.
        /// </summary>
        internal static string MemberCode {
            get {
                return ResourceManager.GetString("MemberCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New Password.
        /// </summary>
        internal static string NewPassword {
            get {
                return ResourceManager.GetString("NewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Old Password.
        /// </summary>
        internal static string OldPassword {
            get {
                return ResourceManager.GetString("OldPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Other.
        /// </summary>
        internal static string Other {
            get {
                return ResourceManager.GetString("Other", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Others.
        /// </summary>
        internal static string Others {
            get {
                return ResourceManager.GetString("Others", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter OTP.
        /// </summary>
        internal static string OTP {
            get {
                return ResourceManager.GetString("OTP", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        internal static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Department Associated.
        /// </summary>
        internal static string SecretaryDepartment {
            get {
                return ResourceManager.GetString("SecretaryDepartment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Secretary of employee.
        /// </summary>
        internal static string SecretoryList {
            get {
                return ResourceManager.GetString("SecretoryList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select.
        /// </summary>
        internal static string SelectListItem {
            get {
                return ResourceManager.GetString("SelectListItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Send Request.
        /// </summary>
        internal static string SendRequest {
            get {
                return ResourceManager.GetString("SendRequest", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your Registration has been done..
        /// </summary>
        internal static string SuccessMessage {
            get {
                return ResourceManager.GetString("SuccessMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Profile.
        /// </summary>
        internal static string UpdateMemberProfile {
            get {
                return ResourceManager.GetString("UpdateMemberProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Your Profile Updated Successfully.
        /// </summary>
        internal static string UpdateMessage {
            get {
                return ResourceManager.GetString("UpdateMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update User Profile.
        /// </summary>
        internal static string UpDateUserProfile {
            get {
                return ResourceManager.GetString("UpDateUserProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address.
        /// </summary>
        internal static string UserAddress {
            get {
                return ResourceManager.GetString("UserAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Evidhan ID.
        /// </summary>
        internal static string UserAdhaarCard {
            get {
                return ResourceManager.GetString("UserAdhaarCard", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Employee Code Or Member code.
        /// </summary>
        internal static string UserCode {
            get {
                return ResourceManager.GetString("UserCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Designation.
        /// </summary>
        internal static string UserDesignation {
            get {
                return ResourceManager.GetString("UserDesignation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DSC.
        /// </summary>
        internal static string UserDSC {
            get {
                return ResourceManager.GetString("UserDSC", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email.
        /// </summary>
        internal static string UserEmail {
            get {
                return ResourceManager.GetString("UserEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mobile.
        /// </summary>
        internal static string UserMobile {
            get {
                return ResourceManager.GetString("UserMobile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        internal static string UserName {
            get {
                return ResourceManager.GetString("UserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Name.
        /// </summary>
        internal static string UserNameChanged {
            get {
                return ResourceManager.GetString("UserNameChanged", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Registration.
        /// </summary>
        internal static string UserRegistrationHeading {
            get {
                return ResourceManager.GetString("UserRegistrationHeading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Upload Signature.
        /// </summary>
        internal static string UserSignature {
            get {
                return ResourceManager.GetString("UserSignature", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Type.
        /// </summary>
        internal static string UserType {
            get {
                return ResourceManager.GetString("UserType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Vidhan Sabha Employee.
        /// </summary>
        internal static string VidhanSabhaEmployee {
            get {
                return ResourceManager.GetString("VidhanSabhaEmployee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Commissioner (Department).
        /// </summary>
        internal static string VidhanSabhaSecretary {
            get {
                return ResourceManager.GetString("VidhanSabhaSecretary", resourceCulture);
            }
        }
    }
}
