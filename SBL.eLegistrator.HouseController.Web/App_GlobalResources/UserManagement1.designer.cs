//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class UserManagement {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal UserManagement() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.UserManagement", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are You Sure Want to Delete?.
        /// </summary>
        internal static string AreYouSure {
            get {
                return ResourceManager.GetString("AreYouSure", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create User Access.
        /// </summary>
        internal static string CreateModule {
            get {
                return ResourceManager.GetString("CreateModule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create Role.
        /// </summary>
        internal static string CreateRole {
            get {
                return ResourceManager.GetString("CreateRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create User Access Action.
        /// </summary>
        internal static string CreateUserAccessAction {
            get {
                return ResourceManager.GetString("CreateUserAccessAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        internal static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete successfully.
        /// </summary>
        internal static string Deletesuccessfully {
            get {
                return ResourceManager.GetString("Deletesuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Active.
        /// </summary>
        internal static string IsActive {
            get {
                return ResourceManager.GetString("IsActive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Modified When.
        /// </summary>
        internal static string ModifiedWhen {
            get {
                return ResourceManager.GetString("ModifiedWhen", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Access.
        /// </summary>
        internal static string Module {
            get {
                return ResourceManager.GetString("Module", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Access Description.
        /// </summary>
        internal static string ModuleDescription {
            get {
                return ResourceManager.GetString("ModuleDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Access Id.
        /// </summary>
        internal static string ModuleId {
            get {
                return ResourceManager.GetString("ModuleId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Access Name.
        /// </summary>
        internal static string ModuleName {
            get {
                return ResourceManager.GetString("ModuleName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Module Name Local.
        /// </summary>
        internal static string ModuleNameLocal {
            get {
                return ResourceManager.GetString("ModuleNameLocal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to New.
        /// </summary>
        internal static string New {
            get {
                return ResourceManager.GetString("New", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role.
        /// </summary>
        internal static string Role {
            get {
                return ResourceManager.GetString("Role", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role Description.
        /// </summary>
        internal static string RoleDescription {
            get {
                return ResourceManager.GetString("RoleDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role Id.
        /// </summary>
        internal static string RoleId {
            get {
                return ResourceManager.GetString("RoleId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Role Name.
        /// </summary>
        internal static string RoleName {
            get {
                return ResourceManager.GetString("RoleName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        internal static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Successfully.
        /// </summary>
        internal static string Savesuccessfully {
            get {
                return ResourceManager.GetString("Savesuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to S.No.
        /// </summary>
        internal static string SNo {
            get {
                return ResourceManager.GetString("SNo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update.
        /// </summary>
        internal static string Update {
            get {
                return ResourceManager.GetString("Update", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update User Access.
        /// </summary>
        internal static string UpdateModule {
            get {
                return ResourceManager.GetString("UpdateModule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Role.
        /// </summary>
        internal static string UpdateRole {
            get {
                return ResourceManager.GetString("UpdateRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update Successfully.
        /// </summary>
        internal static string Updatesuccessfully {
            get {
                return ResourceManager.GetString("Updatesuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Update User Access Action.
        /// </summary>
        internal static string UpdateUserAccessAction {
            get {
                return ResourceManager.GetString("UpdateUserAccessAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Access.
        /// </summary>
        internal static string UserAccess {
            get {
                return ResourceManager.GetString("UserAccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Access Action.
        /// </summary>
        internal static string UserAccessAction {
            get {
                return ResourceManager.GetString("UserAccessAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Action.
        /// </summary>
        internal static string UserAction {
            get {
                return ResourceManager.GetString("UserAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Action Name.
        /// </summary>
        internal static string UserActionName {
            get {
                return ResourceManager.GetString("UserActionName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Managements.
        /// </summary>
        internal static string UserManagements {
            get {
                return ResourceManager.GetString("UserManagements", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Roles.
        /// </summary>
        internal static string UserRoles {
            get {
                return ResourceManager.GetString("UserRoles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Type.
        /// </summary>
        internal static string UserType {
            get {
                return ResourceManager.GetString("UserType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User Type Name.
        /// </summary>
        internal static string UserTypeName {
            get {
                return ResourceManager.GetString("UserTypeName", resourceCulture);
            }
        }
    }
}
