﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SBL.eLegistrator.HouseController.Web.App_GlobalResources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Session {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Session() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("SBL.eLegistrator.HouseController.Web.App_GlobalResources.Session", typeof(Session).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create New Session.
        /// </summary>
        public static string AccHeadCreateSession {
            get {
                return ResourceManager.GetString("AccHeadCreateSession", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sessions List.
        /// </summary>
        public static string AccHeadSessionsList {
            get {
                return ResourceManager.GetString("AccHeadSessionsList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to delete this session?.
        /// </summary>
        public static string ConfirmDelete {
            get {
                return ResourceManager.GetString("ConfirmDelete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure you want to publish this session?.
        /// </summary>
        public static string ConfirmPublish {
            get {
                return ResourceManager.GetString("ConfirmPublish", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Assembly Name.
        /// </summary>
        public static string DisplayAssemblyName {
            get {
                return ResourceManager.GetString("DisplayAssemblyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session End Date.
        /// </summary>
        public static string DisplayEndDate {
            get {
                return ResourceManager.GetString("DisplayEndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Local Name.
        /// </summary>
        public static string DisplayLocalName {
            get {
                return ResourceManager.GetString("DisplayLocalName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string DisplayName {
            get {
                return ResourceManager.GetString("DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session Start Date.
        /// </summary>
        public static string DisplayStartDate {
            get {
                return ResourceManager.GetString("DisplayStartDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First Page.
        /// </summary>
        public static string FirstPage {
            get {
                return ResourceManager.GetString("FirstPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go to the first page.
        /// </summary>
        public static string GoToTheFirstPage {
            get {
                return ResourceManager.GetString("GoToTheFirstPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go to the last page.
        /// </summary>
        public static string GoToTheLastPage {
            get {
                return ResourceManager.GetString("GoToTheLastPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go to the next page.
        /// </summary>
        public static string GoToTheNextPage {
            get {
                return ResourceManager.GetString("GoToTheNextPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Go to the previous page.
        /// </summary>
        public static string GoToThePreviousPage {
            get {
                return ResourceManager.GetString("GoToThePreviousPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Last Page.
        /// </summary>
        public static string LastPage {
            get {
                return ResourceManager.GetString("LastPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Next Page.
        /// </summary>
        public static string NextPage {
            get {
                return ResourceManager.GetString("NextPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No.
        /// </summary>
        public static string No {
            get {
                return ResourceManager.GetString("No", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Records Found.
        /// </summary>
        public static string NoRecords {
            get {
                return ResourceManager.GetString("NoRecords", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Of.
        /// </summary>
        public static string Of {
            get {
                return ResourceManager.GetString("Of", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Pages.
        /// </summary>
        public static string Pages {
            get {
                return ResourceManager.GetString("Pages", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Previous Page.
        /// </summary>
        public static string PreviousPage {
            get {
                return ResourceManager.GetString("PreviousPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Publish.
        /// </summary>
        public static string Publish {
            get {
                return ResourceManager.GetString("Publish", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Published.
        /// </summary>
        public static string Published {
            get {
                return ResourceManager.GetString("Published", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Something went wrong, Please contact administrator or try later..
        /// </summary>
        public static string SaveError {
            get {
                return ResourceManager.GetString("SaveError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contents have been saved successfully..
        /// </summary>
        public static string SaveSuccess {
            get {
                return ResourceManager.GetString("SaveSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to End Date.
        /// </summary>
        public static string SessionEndDate {
            get {
                return ResourceManager.GetString("SessionEndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string SessionName {
            get {
                return ResourceManager.GetString("SessionName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Local Name.
        /// </summary>
        public static string SessionNameLocal {
            get {
                return ResourceManager.GetString("SessionNameLocal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Start Date.
        /// </summary>
        public static string SessionStartDate {
            get {
                return ResourceManager.GetString("SessionStartDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status.
        /// </summary>
        public static string Status {
            get {
                return ResourceManager.GetString("Status", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Submit.
        /// </summary>
        public static string Submit {
            get {
                return ResourceManager.GetString("Submit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session end date is required.
        /// </summary>
        public static string ValidationEndDate {
            get {
                return ResourceManager.GetString("ValidationEndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Local name is required.
        /// </summary>
        public static string ValidationLocalName {
            get {
                return ResourceManager.GetString("ValidationLocalName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session name is required.
        /// </summary>
        public static string ValidationName {
            get {
                return ResourceManager.GetString("ValidationName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session start date is required.
        /// </summary>
        public static string ValidationStartDate {
            get {
                return ResourceManager.GetString("ValidationStartDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Session End Date can not be less than Session start date.
        /// </summary>
        public static string ValidationStartEndDate {
            get {
                return ResourceManager.GetString("ValidationStartEndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The diffrence between Session Start and End dates is more than expected, Please check..
        /// </summary>
        public static string ValidationStartEndYear {
            get {
                return ResourceManager.GetString("ValidationStartEndYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yes.
        /// </summary>
        public static string Yes {
            get {
                return ResourceManager.GetString("Yes", resourceCulture);
            }
        }
    }
}
