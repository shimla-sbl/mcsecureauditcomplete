﻿
function ShowErrorMessage(message) {
    showNotification({
        message: message,
        type: "error",
        //autoClose: true,
        duration: 50
    });
}

function ShowInformationMessage(message) {
    showNotification({
        message: message,
        type: "information",
        //autoClose: true,
        duration: 50
    });
}