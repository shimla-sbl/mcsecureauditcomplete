﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SMSMessage", Namespace="http://schemas.datacontract.org/2004/07/SMS.API")]
    [System.SerializableAttribute()]
    public partial class SMSMessage : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int MessageIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MobileNoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int QueueStateIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SMSTextField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<bool> isLocaleField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int MessageID {
            get {
                return this.MessageIDField;
            }
            set {
                if ((this.MessageIDField.Equals(value) != true)) {
                    this.MessageIDField = value;
                    this.RaisePropertyChanged("MessageID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string MobileNo {
            get {
                return this.MobileNoField;
            }
            set {
                if ((object.ReferenceEquals(this.MobileNoField, value) != true)) {
                    this.MobileNoField = value;
                    this.RaisePropertyChanged("MobileNo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int QueueStateID {
            get {
                return this.QueueStateIDField;
            }
            set {
                if ((this.QueueStateIDField.Equals(value) != true)) {
                    this.QueueStateIDField = value;
                    this.RaisePropertyChanged("QueueStateID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SMSText {
            get {
                return this.SMSTextField;
            }
            set {
                if ((object.ReferenceEquals(this.SMSTextField, value) != true)) {
                    this.SMSTextField = value;
                    this.RaisePropertyChanged("SMSText");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<bool> isLocale {
            get {
                return this.isLocaleField;
            }
            set {
                if ((this.isLocaleField.Equals(value) != true)) {
                    this.isLocaleField = value;
                    this.RaisePropertyChanged("isLocale");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SMSMessageList", Namespace="http://schemas.datacontract.org/2004/07/SMS.API")]
    [System.SerializableAttribute()]
    public partial class SMSMessageList : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string[] MobileNoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ModuleActionIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.DateTime ProccessDateField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string SMSTextField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private long UniqueIdentificationIDField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Nullable<bool> isLocaleField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string[] MobileNo {
            get {
                return this.MobileNoField;
            }
            set {
                if ((object.ReferenceEquals(this.MobileNoField, value) != true)) {
                    this.MobileNoField = value;
                    this.RaisePropertyChanged("MobileNo");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ModuleActionID {
            get {
                return this.ModuleActionIDField;
            }
            set {
                if ((this.ModuleActionIDField.Equals(value) != true)) {
                    this.ModuleActionIDField = value;
                    this.RaisePropertyChanged("ModuleActionID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime ProccessDate {
            get {
                return this.ProccessDateField;
            }
            set {
                if ((this.ProccessDateField.Equals(value) != true)) {
                    this.ProccessDateField = value;
                    this.RaisePropertyChanged("ProccessDate");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string SMSText {
            get {
                return this.SMSTextField;
            }
            set {
                if ((object.ReferenceEquals(this.SMSTextField, value) != true)) {
                    this.SMSTextField = value;
                    this.RaisePropertyChanged("SMSText");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public long UniqueIdentificationID {
            get {
                return this.UniqueIdentificationIDField;
            }
            set {
                if ((this.UniqueIdentificationIDField.Equals(value) != true)) {
                    this.UniqueIdentificationIDField = value;
                    this.RaisePropertyChanged("UniqueIdentificationID");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Nullable<bool> isLocale {
            get {
                return this.isLocaleField;
            }
            set {
                if ((this.isLocaleField.Equals(value) != true)) {
                    this.isLocaleField = value;
                    this.RaisePropertyChanged("isLocale");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="SMSWebServiceLatest.IService1")]
    public interface IService1 {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/SMSListToQueue", ReplyAction="http://tempuri.org/IService1/SMSListToQueueResponse")]
        void SMSListToQueue(SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.SMSMessage Msgs);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/AddSMSToQueue", ReplyAction="http://tempuri.org/IService1/AddSMSToQueueResponse")]
        void AddSMSToQueue(SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.SMSMessageList messageList);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/GetSMSStatusByUIModuleID", ReplyAction="http://tempuri.org/IService1/GetSMSStatusByUIModuleIDResponse")]
        byte[] GetSMSStatusByUIModuleID(SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.SMSMessageList smsMessage);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IService1Channel : SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.IService1, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class Service1Client : System.ServiceModel.ClientBase<SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.IService1>, SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.IService1 {
        
        public Service1Client() {
        }
        
        public Service1Client(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public Service1Client(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Service1Client(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public Service1Client(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public void SMSListToQueue(SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.SMSMessage Msgs) {
            base.Channel.SMSListToQueue(Msgs);
        }
        
        public void AddSMSToQueue(SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.SMSMessageList messageList) {
            base.Channel.AddSMSToQueue(messageList);
        }
        
        public byte[] GetSMSStatusByUIModuleID(SBL.eLegistrator.HouseController.Web.SMSWebServiceLatest.SMSMessageList smsMessage) {
            return base.Channel.GetSMSStatusByUIModuleID(smsMessage);
        }
    }
}
