
function ConfirmBox(titleTextToShow) {   
        $("#dialog-confirmBox").removeClass('hide').dialog({
            resizable: false,
            modal: true,
            title: titleTextToShow,
            title_html: true,
            buttons: [
                {
                    html: "&nbsp; Yes",
                    "class": "btn btn-primary",
                    click: function () {
                        $(this).dialog("close");
                        return true;
                    }
                }
                ,
                {
                    html: "&nbsp; Cancel",
                    "class": "btn btn-primary",
                    click: function () {
                        $(this).dialog("close");
                        return false;
                    }
                }
            ]
        });
    }