﻿function fillFilterProgramme() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/GetAllProgramme';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Program").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Program").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#Program").trigger("chosen:updated");


            fillFilterWorkStatus();
        }
    });
}

function fillFilterWorkStatus() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllWorkStatus';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Status").html("");
            $("#Status").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Status").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#workStatusID").trigger("chosen:updated");

            fillFilterWorkType();
        }
    });
}

function fillFilterWorkType() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllWorkType';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#workType").html("");
            $("#workType").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#workType").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#workType").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterOwnerDepartment(memberCode) {
    $("#loader-wrapper").show();
    if (memberCode === undefined) {
        memberCode = '';
    }

    var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { memberCode: memberCode },
        success: function (data) {
            $("#OwnerDepartment").html("");
            $("#OwnerDepartment").append($("<option></option>").val('0').html('--Select--'))

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#OwnerDepartment").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#OwnerDepartment").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }

    });

    //if (memberCode == '' || memberCode == null) {
    //    var pathurl = '/Constituency/Constituency/getAllDepartment';
    //    $.ajax({
    //        url: pathurl,
    //        type: 'GET',
    //        success: function (data) {
    //            $("#OwnerDepartment").html("");
    //            $("#OwnerDepartment").append($("<option></option>").val('0').html('--Select--'))

    //            for (var i = 0; i < data.length; i++) {
    //                var item = data[i];

    //                $("#OwnerDepartment").append($("<option></option>").val(item.value).html(item.Text))
    //            }
    //            $("#OwnerDepartment").trigger("chosen:updated");
    //            $("#loader-wrapper").hide();
    //        }
    //    });
    //} else
    //{
    //  //  alert('memberCode1');
    //    var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
    //    $.ajax({
    //        url: pathurl,
    //        type: 'GET',
    //        data: { memberCode: memberCode },
    //        success: function (data) {
    //            $("#OwnerDepartment").html("");
    //            $("#OwnerDepartment").append($("<option></option>").val('0').html('--Select--'))

    //            for (var i = 0; i < data.length; i++) {
    //                var item = data[i];

    //                $("#OwnerDepartment").append($("<option></option>").val(item.value).html(item.Text))
    //            }
    //            $("#OwnerDepartment").trigger("chosen:updated");
    //            $("#loader-wrapper").hide();
    //        }

    //    });


    //}
}

function fillFilterFinancialYear1() {
    $("#loader-wrapper").show();
 
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#FinancialYear").html("");
            $("#FinancialYear").append($("<option></option>").val('0').html('-- select --'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#FinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterFinancialYear() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#FinancialYear").html("");
            $("#FinancialYear").append($("<option></option>").val('0').html('-- select --'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#FinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
            $("#loader-wrapper").hide();

            fillFilterAllDemand();
        }
    });
}

function fillFilterAllDemand() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllDemand';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Demand").html("");
            $("#Demand").append($("<option></option>").val('0').html('--Select--'))

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#Demand").append($("<option></option>").val(item.value).html(item.value + "-" + item.Text))
            }

            $("#Demand").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterControllingAuthority(districtID) {
    debugger;
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllControllingAuthority';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { DistrictID: districtID },
        success: function (data) {
            $("#ControllingAuthority").html("");
            $("#ControllingAuthority").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#ControllingAuthority").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#ControllingAuthority").trigger("chosen:updated");
          
            fillFilterProgramme();

        }
    });
}

function fillFilterExecutingDepartment1(office, department, usertype) {
    $("#loader-wrapper").show();
    //if (usertype == 8) {
    //    fillOffice1("0");
    //}
    if (usertype == 7 || usertype == 2) {
        if (usertype == 7) {
            fillSubOffice(office);
        }
        else if (usertype == 2){
            fillOffice1(department)
        }
        else {
            fillOffice();
        }
        
        var pathurl = '/Constituency/Constituency/getAllDepartmentBase';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { DepartMent: department },
            success: function (data) {
                debugger;
                    $("#Agency").html("");
                    //$("#Agency").append($("<option></option>").val('0').html('--Select--'))
                    for (var i = 0; i < data.length; i++) {
                        var item = data[i];
                        $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
                    }

                    $("#Agency").trigger("chosen:updated");

                    $("#loader-wrapper").hide();
                }
          });
       }
    
    else {
        var pathurl = '/Constituency/Constituency/getWorkDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',

            success: function (data) {
                debugger;
                $("#Agency").html("");
                $("#Agency").append($("<option></option>").val('0').html('--Select--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#Agency").trigger("chosen:updated");

                $("#loader-wrapper").hide();
            }
        });
    }
    }
   
function fillFilterExecutingDepartment(memberCode) {
    debugger;
    $("#loader-wrapper").show();
    if (memberCode == '') {
        var pathurl = '/Constituency/Constituency/getAllDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',

            success: function (data) {
                debugger;
                $("#Agency").html("");
                $("#Agency").append($("<option></option>").val('0').html('--Select--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#Agency").trigger("chosen:updated");

                $("#loader-wrapper").hide();
            }
        });
    }
    else {
        var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { memberCode: memberCode },
            success: function (data) {
                debugger;
                $("#Agency").html("");
                $("#Agency").append($("<option></option>").val('0').html('--Select--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#Agency").trigger("chosen:updated");

                $("#loader-wrapper").hide();
            }
        });
        // $("#Agency").val(currntdeptid);

    }
}

//function fillFilterFinancialYear() {
//    var pathurl = '/Constituency/Constituency/getFinancialYearList';
//    $.ajax({
//        url: pathurl,
//        type: 'GET',
//        success: function (data) {
//            $("#FinancialYear").html("");
//            $("#FinancialYear").append($("<option></option>").val('0').html('--Select--'))
//            for (var i = 0; i < data.length; i++) {
//                var item = data[i];

//                $("#FinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
//            }
//        }
//    });
//}

function fillFilterExecutingDepartment_current(CurrentDept, subDivisionId) {
    $("#loader-wrapper").show();
    // alert(subDivisionId);
    var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {},
        success: function (data) {
            debugger;
            $("#Agency").html("");
            if (subDivisionId == "" || subDivisionId == null) {
                $("#Agency").append($("<option></option>").val('0').html('--Select--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
                }
                if (CurrentDept != '') {
                    $("#Agency").val(CurrentDept);
                    $('#Agency option:not(:selected)').attr('disabled', true);
                }
                //$("#Agency").prop("disabled", true);

            } else {
                $("#Agency").append($("<option></option>").val('0').html('--Select--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
                }
            }

            $("#Agency").trigger("chosen:updated");
            // $("#Agency").append(CurrentDept);
            //  $("#loader-wrapper").hide();
        }

    }).done(function () {
        $("#Agency").val(CurrentDept);
        $("#Agency").trigger("chosen:updated");
    });
}

function fillstatusFinancialYear() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#statusFinancialYear").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#statusFinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterPanchayatByConstituency(constituencyID) {
    $("#loader-wrapper").show();
    // alert(constituencyID);
    var pathurl = '/Constituency/Constituency/getConstituencyPanchayat';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { constituencyID: constituencyID },
        success: function (data) {
            $("#Panchayat").html("");
            $("#Panchayat").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Panchayat").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#Panchayat").trigger("chosen:updated");
         
            fillFilterFinancialYear();
        }
    });
}

function fillFilterConstituency(constituencyID) {
    $("#loader-wrapper").show();
    // alert(constituencyID);
    var pathurl = '/Constituency/Constituency/getConstituencyForMLA_SDM';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { constituencyID: constituencyID },
        success: function (data) {
           
            $("#Constituency").html("");
            if (constituencyID == "" || constituencyID == "0") {
                $("#Constituency").append($("<option></option>").val("0").html("--All Ward---"));
            }
           // $("#Constituency").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Constituency").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#Constituency").trigger("chosen:updated");
            $("#loader-wrapper").hide();
            fillstatusFinancialYear();
        }
    });
}

function fillFilterConstituencyForMLA(constituencyID) {
    $("#loader-wrapper").show();
    // alert(constituencyID);
    var pathurl = '/Constituency/Constituency/getConstituencyForMLA_SDM';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { constituencyID: constituencyID },
        success: function (data) {
            $("#Constituency").html("");
            debugger;
           // $("#Constituency").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Constituency").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#Constituency").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterSubdivision(Subdivision) {
    $("#loader-wrapper").show();
    // alert(constituencyID);
    var pathurl = '/Constituency/Constituency/getSubdivisionForSDM';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { Subdivision: Subdivision },
        success: function (data) {
            $("#SubDivision").html("");
          //  $("#SubDivision").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#SubDivision").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#SubDivision").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterSubdivisionMLA(Subdivision) {
    $("#loader-wrapper").show();
    // alert(constituencyID);
    var pathurl = '/Constituency/Constituency/getSubdivisionForSDM';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { Subdivision: Subdivision },
        success: function (data) {
            $("#SubDivision").html("");
             $("#SubDivision").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#SubDivision").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#SubDivision").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterSubdivisionByDistrict(District) {
    $("#loader-wrapper").show();
    // alert(constituencyID);
    var pathurl = '/Constituency/Constituency/getSubdivisionForUser';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { DisttId: District },
        success: function (data) {
            $("#SubDivision").html("");
            $("#SubDivision").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#SubDivision").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#SubDivision").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function FilterDistrictList1(UserType, Distt) {
  if (UserType == 8 || UserType == 9 || UserType==1) {
       
        $("#loader-wrapper").show();
        var pathurl = '/Constituency/Constituency/GetDistrictForDC';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { DisttCode: Distt },
            success: function (data) {
                $("#District").html("");
              //  $("#District").append($("<option></option>").val('0').html('--Select--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];

                    $("#District").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#District").trigger("chosen:updated");
                //fillFilterConstituencyByDistrict($("#District :selected").val());
                $("#loader-wrapper").hide();
            }
        });
        if (UserType == 8) {
            fillFilterConstituencyByDistrict($("#District :selected").val());
            fillFilterSubdivisionMLA(0);
        }
        if (UserType == 9) {
            fillFilterConstituency(0);
            fillFilterSubdivision(0);
        }
        if (UserType == 1) {
            fillFilterConstituencyForMLA(0);
            fillFilterSubdivisionMLA(0);
        }
    }
    else {
        fillFilterSubdivisionByDistrict(Distt)
        $("#loader-wrapper").show();
        var pathurl = '/Constituency/Constituency/GetAllDistrict';
        $.ajax({
            url: pathurl,
            type: 'GET',
            success: function (data) {
                $("#District").html("");
                $("#District").append($("<option></option>").val('0').html('--Select--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];

                    $("#District").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#District").trigger("chosen:updated");
                fillFilterConstituencyByDistrict($("#District :selected").val());
                $("#loader-wrapper").hide();
            }
        });
    }
    //if (usertype == 8) {
    //    //$("#District option[value=" + Distt + "]").attr("selected", "selected");
    //    //fillFilterConstituencyByDistrict(Distt);
    //}
}

function FilterDistrictList() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/GetAllDistrict';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#District").html("");
            $("#District").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#District").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#District").trigger("chosen:updated");
            fillFilterConstituencyByDistrict($("#District :selected").val());
            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterConstituencyByDistrict(DistrictID) {

    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getDistrictConstituency';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { DistrictID: DistrictID },
        success: function (data) {
            debugger;
            $("#Constituency").html("");
            $("#Constituency").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Constituency").append($("<option></option>").val(item.value).html(item.Text))
            }
         // fillFilterPanchayatByConstituency($("#Constituency :selected").val());
            $("#Constituency").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterMLA() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/GetAllMlaListforWorks';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            
            $("#MLA").html("");
            $("#MLA").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#MLA").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#MLA").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}

function fillFilterSectionYear() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#statusSectionFinancialYear").html("");
            $("#statusSectionFinancialYear").append($("<option></option>").val('0').html('-- select --'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#statusSectionFinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
            $("#loader-wrapper").hide();
        }
    });
}

function fillSubOffice(ExecutiveDepartment) {
    $("#loader-wrapper").show();
    //alert(ExecutiveDepartment);
    var pathurl = '/Constituency/Constituency/DepartmentAllSubOffices';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { ExecutiveDepartment: ExecutiveDepartment },
        success: function (data) {
            $("#Office").html("");
            //$("#Office").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Office").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#Office").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}

function fillOffice1(ExecutiveDepartment) {
    $("#loader-wrapper").show();
 //   alert(ExecutiveDepartment)
    var pathurl = '/Constituency/Constituency/DepartmentAllOffices';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { ExecutiveDepartment: ExecutiveDepartment },
        success: function (data) {
            $("#Office").html("");
            $("#Office").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Office").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#Office").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}

function fillOffice() {
    $("#loader-wrapper").show();
        // alert('for officer');
        var pathurl = '/Constituency/Constituency/GetAllSubOfficeList';
        $.ajax({
            url: pathurl,
            type: 'GET',
            success: function (data) {
                $("#Office").html("");
                if (data.length > 1) {
                    $("#Office").append($("<option></option>").val('0').html('--Select--'));
                }
                 //
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#Office").append($("<option></option>").val(item.value).html(item.Text))
                }

                $("#Office").trigger("chosen:updated");

                $("#loader-wrapper").hide();
            }
        });

}
//Madhur (for View MLA Diary)

//function FilterDistrictList(office) {

//    //  $("#loader-wrapper").show();
//    var pathurl = '/Grievances/Grievances/GetDistrictForOffice';
//    $.ajax({
//        url: pathurl,
//        type: 'GET',
//        data: { OfficeId: office },
//        success: function (data) {
//            $("#District").html("");
//            // $("#District").append($("<option></option>").val('0').html('--Select--'))
//            for (var i = 0; i < data.length; i++) {
//                var item = data[i];
//                //alert(item.DistrictCode);
//                $("#District").append($("<option></option>").val(item.DistrictCode).html(item.DistrictName))
//            }
//            $("#District").trigger("chosen:updated");
//            //  fillFilterConstituencyByDistrict($("#District :selected").val());
//            $("#loader-wrapper").hide();
//        }
//    });
//}

//function fillDepartment_MLADiary(office) {

//    var pathurl = '/Grievances/Grievances/Department_ForAllOffices';
//    $.ajax({
//        url: pathurl,
//        type: 'GET',
//        data: { OfficeId: office },
//        success: function (data) {
//            $("#Agency").html("");

//            //$("#Office").append($("<option></option>").val('0').html('--Select--'))
//            for (var i = 0; i < data.length; i++) {
//                var item = data[i];

//                $("#Agency").append($("<option></option>").val(item.DepartrmentID).html(item.DepartmentName))
//                filterCount();
//            }

//            $("#Agency").trigger("chosen:updated");

//            $("#loader-wrapper").hide();
//        }
//    });

//}

//function fillOffice() {
//    // alert('for officer');
//    var pathurl = '/Constituency/Constituency/GetAllSubOfficeList';
//    $.ajax({
//        url: pathurl,
//        type: 'GET',
//        success: function (data) {
//            $("#Office").html("");
//            // $("#Office").append($("<option></option>").val('0').html('--Select--'))
//            for (var i = 0; i < data.length; i++) {
//                var item = data[i];
//                $("#Office").append($("<option></option>").val(item.value).html(item.Text))
//            }

//            $("#Office").trigger("chosen:updated");

//            $("#loader-wrapper").hide();
//        }
//    });


//}

//function fillOffice_ForMLA(MlaCode) {
//    // alert('for mla');
//    var pathurl = '/Constituency/Constituency/GetAllOfficemappedByMemberCode';
//    $.ajax({
//        url: pathurl,
//        type: 'GET',
//        data: { MemberCode: MlaCode },
//        success: function (data) {
//            $("#Office").html("");
//            $("#Office").append($("<option></option>").val('0').html('--Select--'))
//            for (var i = 0; i < data.length; i++) {
//                var item = data[i];
//                // alert(item.officename);
//                $("#Office").append($("<option></option>").val(item.OfficeId).html(item.officename))

//            }

//            $("#Office").trigger("chosen:updated");

//            $("#loader-wrapper").hide();
//        }
//    });


//}

//function fillFilterConstituencyForOfficer_MLADiary(OfficeID) {
//    //alert(OfficeID);
//    var pathurl = '/Grievances/Grievances/GetMappedConstituencyforOfficer_MLADiary';
//    $.ajax({
//        url: pathurl,
//        type: 'GET',
//        data: { OfficeID: OfficeID },
//        success: function (data) {
//            $("#Constituency").html("");
//            // $("#Constituency").append($("<option></option>").val('0').html('--Select--'))
//            for (var i = 0; i < data.length; i++) {
//                var item = data[i];
//                $("#Constituency").append($("<option></option>").val(item.value).html(item.Text))
//            }
//            $("#Constituency").trigger("chosen:updated");
//            $("#loader-wrapper").hide();
//        }
//    });
//}