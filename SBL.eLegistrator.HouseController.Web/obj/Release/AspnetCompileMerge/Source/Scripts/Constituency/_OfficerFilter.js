﻿function fillFilterProgramme() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/GetAllProgramme';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Program").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Program").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#Program").trigger("chosen:updated");

            //$("#loader-wrapper").hide();
        }
    });
}
function fillFilterWorkStatus() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllWorkStatus';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
          ;
            $("#Status").html("");
            $("#Status").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Status").append($("<option></option>").val(item.value).html(item.Text))
            }
            //$("#Status").trigger("chosen:updated");

            //$("#loader-wrapper").hide();
        }
    });
}
function fillFilterWorkType() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllWorkType';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#workType").html("");
            $("#workType").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#workType").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#workType").trigger("chosen:updated");

            //$("#loader-wrapper").hide();
        }
    });
}
function fillFilterOwnerDepartment() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#OwnerDepartment").html("");
            $("#OwnerDepartment").append($("<option></option>").val('0').html('--All--'))

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#OwnerDepartment").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#OwnerDepartment").trigger("chosen:updated");
            //$("#loader-wrapper").hide();
        }
    });
}
function fillFilterPanchayatByConstituency(constituencyID) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getConstituencyPanchayat';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { constituencyID: constituencyID },
        success: function (data) {
            $("#PanchayatID").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#PanchayatID").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#PanchayatID").trigger("chosen:updated");
            //$("#loader-wrapper").hide();
        }
    });
}
function fillFilterFinancialYear() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#FinancialYear").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#FinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
            //$("#loader-wrapper").hide();
        }
    });
}
function fillFilterAllDemand() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllDemand';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Demand").html("");
            $("#Demand").append($("<option></option>").val('0').html('--All--'))

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#Demand").append($("<option></option>").val(item.value).html(item.value + ' - ' + item.Text))
            }

            $("#Demand").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterControllingAuthority(ControllingAuthorityID) {
    debugger;
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllControllingAuthority';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { DistrictID: '11' },
        success: function (data) {
            $("#ControllingAuthority").html("");
            $("#programID").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#ControllingAuthority").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#ControllingAuthority").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterExecutingDepartment() {
    $("#loader-wrapper").show();
    
    var pathurl = '/Constituency/Constituency/getAllDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Agency").html("");
            $("#Agency").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#Agency").trigger("chosen:updated");

            //$("#loader-wrapper").hide();
        }
    });
}
function fillFilterFinancialYear() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#FinancialYear").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#FinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
        }
    });
}