﻿
//-----MLA Diary

function FilterDistrictList(office) {

    //  $("#loader-wrapper").show();
    var pathurl = '/Grievances/Grievances/GetDistrictForOffice';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { OfficeId: office },
        success: function (data) {
            $("#District").html("");
            // $("#District").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                //alert(item.DistrictCode);
                $("#District").append($("<option></option>").val(item.DistrictCode).html(item.DistrictName))
            }
            $("#District").trigger("chosen:updated");
            //  fillFilterConstituencyByDistrict($("#District :selected").val());
            $("#loader-wrapper").hide();
        }
    });
}

function fillDepartment_MLADiary(office) {

    var pathurl = '/Grievances/Grievances/Department_ForAllOffices';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { OfficeId: office },
        success: function (data) {
            $("#Agency").html("");

            //$("#Office").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#Agency").append($("<option></option>").val(item.DepartrmentID).html(item.DepartmentName))
                filterCount();
            }

            $("#Agency").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });

}

function fillOffice() {
    debugger;
    // alert('for officer');
    var pathurl = '/Constituency/Constituency/GetAllSubOfficeList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            debugger;
            $("#Office").html("");
            // $("#Office").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Office").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#Office").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });


}

function fillOffice_ForMLA(MlaCode) {
    // alert('for mla');
    var pathurl = '/Constituency/Constituency/GetAllOfficemappedByMemberCode';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { MemberCode: MlaCode },
        success: function (data) {
            $("#Office").html("");
            $("#Office").append($("<option></option>").val('0').html('--Select--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                // alert(item.officename);
                $("#Office").append($("<option></option>").val(item.OfficeId).html(item.officename))

            }

            $("#Office").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });


}

function fillFilterConstituencyForOfficer_MLADiary(OfficeID) {
    //alert(OfficeID);
    var pathurl = '/Grievances/Grievances/GetMappedConstituencyforOfficer_MLADiary';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { OfficeID: OfficeID },
        success: function (data) {
            $("#Constituency").html("");
             $("#Constituency").append($("<option></option>").val('0').html('--All of the below--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Constituency").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#Constituency").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}