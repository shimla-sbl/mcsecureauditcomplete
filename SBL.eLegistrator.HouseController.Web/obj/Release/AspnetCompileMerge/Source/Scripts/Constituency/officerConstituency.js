﻿
/// <reference path="../jquery-1.9.1.min.js" />

/// <reference path="../../assets/js/bootstrap.min.js" />



function GetFilterWorkSchemeList() {
    //alert("nodal officer");
    if ($("#Checklatest").is(':checked')) {
        if ($("#latestDate").val() == '') {
            var dlg = bootbox.alert('Please select a Date .', function (e) {
                $(dlg).modal('hide');
            });
        }
        else {
            getData();
        }
    } else {
        getData();
    }
}





//function getData() {

//    //alert("nodal officer get data");

//    $("#RightSidebar .widget-box").hide();
//    $("#loader-wrapper").show();


//    var FinancialYearID = $("#FinancialYear option:selected").val();
//    var ControllingAuthorityID = $("#ControllingAuthority option:selected").val();
//    var DemandID = $("#Demand option:selected").val();
//    var ProgramID = $("#Program option:selected").val();
//    var AgencyID = $("#Agency  option:selected").val();
//    var OwnerDepartmentID = $("#OwnerDepartment option:selected").val();
//    var PanchayatID = $("#Panchayat option:selected").val();
//    var WorkTypeID = $("#workType option:selected").val();
//    var StatusID = $("#Status  option:selected").val();
//    var DistrictID = $("#District  option:selected").val();
//    var ConstituencyID = $("#Constituency  option:selected").val();
//    var cFinancialYearID = $("#statusFinancialYear option:selected").val();
//    var sanctionedDateID = $("#latestDate").val();
//    var summaryTypeID = $("#summaryTypeDropDown").val();

//    var ReportType = $("input[name=form-field-radio]:checked").val();
//    //alert(summaryTypeID);

//    if (ReportType == "DetailReport") {
//        var pathurl = '/Constituency/Constituency/FilteredconstituencyListForOfficer';
//        $.ajax({
//            url: pathurl,
//            type: 'GET',
//            data: { FinancialYear: FinancialYearID, ControllingAuthority: ControllingAuthorityID, Demand: DemandID, Program: ProgramID, Agency: AgencyID, OwnerDepartment: OwnerDepartmentID, Panchayat: PanchayatID, WorkType: WorkTypeID, Status: StatusID, DistrictID: DistrictID, Constituency: ConstituencyID, cFinancialYear: cFinancialYearID, sanctionedDate: sanctionedDateID },
//            success: function (data) {
//                $('#inneridebar').show();
//                $("#innerLeftSidebar").html(data);
//                //document.getElementById("MainDiveThreeTire").style.display = "none";
//                //document.getElementById("MainDiveTwoTire").style.display = "none";
//                $("#loader-wrapper").hide();
//            }
//        });
//    } else if (ReportType == "SummaryReport") {
//       // alert("report type hit");
//        $("#innerLeftSidebar").html("");
//        var pathurl = '/Constituency/Constituency/SearchWorkListForNodal';
//        $.ajax({
//            url: pathurl,
//            type: 'GET',
//            data: { FinancialYear: FinancialYearID, ControllingAuthority: ControllingAuthorityID, Demand: DemandID, Program: ProgramID, Agency: AgencyID, OwnerDepartment: OwnerDepartmentID, Panchayat: PanchayatID, WorkType: WorkTypeID, Status: StatusID, DistrictID: DistrictID, Constituency: ConstituencyID, cFinancialYear: cFinancialYearID, sanctionedDate: sanctionedDateID, summaryTypeID: summaryTypeID },
//            success: function (data) {
//                $("#NviewWorksSummary").html("");
//                $("#NviewWorksSummary").html(data);
//                $("#NviewWorksSummary").modal({
//                    "backdrop": "static",
//                    "keyboard": true,
//                    "show": true,
//                });

//                $("#loader-wrapper").hide();
//            }
//        });
//    }
//}


//deptRowClickNodal


function UpdateProgress(schemeID) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/updateProgress';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { schemeID: schemeID },
        success: function (data) {

            $("#UpdateProgress").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            $("#UpdateProgress").html("");
            $("#UpdateProgress").html(data);
            $("#loader-wrapper").hide();
           $(".modal-backdrop.in").hide();
            
        }
    });

}
function UpdateImages(schemeID) {
    
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/updateProgressImages';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { schemeID: schemeID },
        success: function (data) {
            
            
            $("#UpdateImagesDIv").html("");
            $("#UpdateImagesDIv").html(data)
            $("#UpdateImages").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            
            $(".modal-backdrop").hide();
            $("#loader-wrapper").hide();
        }
    });
}
function OnUpdateImagesSuccess() {
    $("#UpdateImages").modal('hide');
    ShowNotification("success", "Image Has been updated.");
    // GetFilterWorkSchemeList();
}
function OnUpdateprogressSuccess(rowId) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/findConstituencyWorkbyCode';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { schemeID: rowId },
        success: function (data) {
            $('#resize-left').animate({ 'width': '530px' });
            $("#innerRightSidebar").html("");
            $("#innerRightSidebar").html(data)
            $("#UpdateProgress").modal('hide');
            ShowNotification("success", "Progress Has been updated.");
            $("#loader-wrapper").hide();
        }
    });
}
try {


    $("input[numberonly]").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

}
catch (error) {
    alert(error.message);
}