﻿function AuthoriseEmployees() {
    var pathurl = '/PaperLaidDepartment/Miscellaneous/AuthorizeEmployees';//'@Url.Action("AuthorizeEmployees", "Miscellaneous", new {@area="PaperLaidDepartment" })';
    var deptid = $('#SecDeptLst :selected').val();
    var userlst = $('#unauthorizedEmpList').val();
    
    if (userlst.length != 0||userlst!="") {

        $.ajax({
            url: pathurl,
            type: 'GET',
            data: {
                userIds: userlst,
                deptId: deptid
            },
            success: function (data) {
                $("#listofEmployeestables").empty();
                $("#listofEmployeestables").html(data);

            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText);
                alert(thrownError);
            }
        });
    }
    else {

        //alert("Please Select atlest one UnAuthorised Employee");
        alert("Please Select atlest one UnAuthorised Employee");
    }
}


function UnAuthoriseEmployees() {
    var pathurl = "/PaperLaidDepartment/Miscellaneous/UnAuthorizeEmployees";
    var deptid = $('#SecDeptLst :selected').val();
    
    var userlst = $('#authorizedEmpList').val();
   

    if (userlst.length != 0 || userlst != "") {
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: {
                userIds: userlst,
                deptId: deptid
            },
            success: function (data) {
                $("#listofEmployeestables").empty();
                $("#listofEmployeestables").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText);
                alert(thrownError);
            }
        });
    }
    else {
        //alert("Please Select atlest one Authorised Employee");
        alert("Please Select atlest one Authorised Employee");
    }
}

$("#SecDeptLst").change(function () {
    var deptid = $(this).val();
    
    if (deptid != '0000' && deptid != 'Na') {
       

        $.ajax({
            url: "/PaperLaidDepartment/Miscellaneous/GetDepartmentEmpList",
            type: 'GET',
            data: {
                deptId: deptid
            },
            success: function (data) {
               
                $("#listofEmployeestables").html(data);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText);
                alert(thrownError);
            }
        });
    }
    else {
        $("#listofEmployeestables").empty();
    }
});



function NextButtonPendingBills(rPage, pageNo, lStart, lEnd) {

    var deptid = $('#SecDeptLst :selected').val();

    $.ajaxSetup({ cache: false });
    var pageNumber = pageNo;
    var rowsPerPage = rPage;
    var loopStart = lStart;
    var loopEnd = lEnd;
    var pathurl = '@Url.Action("GetAuthorisedList", "Miscellaneous",new{@area="PaperLaidDepartment"})';

    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {

            PageNumber: pageNo,
            RowsPerPage: rPage,
            loopStart: lStart,
            loopEnd: lEnd,
            deptId: deptid
        },
        success: function (data) {
           
            $("#authorisedList").html(data);
            
        }
    });
}

function NextAndPreviousButtonPendingBills(rPage, pageNo, lStart, lEnd) {
    
    var deptid = $('#SecDeptLst :selected').val();

    $.ajaxSetup({ cache: false });
    var pageNumber = pageNo;
    var rowsPerPage = rPage;
    var loopStart = lStart;
    var loopEnd = lEnd;
    var pathurl = '@Url.Action("GetAuthorisedList", "Miscellaneous",new{@area="PaperLaidDepartment"}))';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {

            PageNumber: pageNo,
            RowsPerPage: rPage,
            loopStart: lStart,
            loopEnd: lEnd,
            deptId: deptid
        },
        success: function (data) {
          


            $("#authorisedList").html(data);
            
        }
    });
}

function NextButtonPendingPLIHBills(rPage, pageNo, lStart, lEnd) {
   
    var deptid = $('#SecDeptLst :selected').val();
    $.ajaxSetup({ cache: false });
    var pageNumber = pageNo;
    var rowsPerPage = rPage;
    var loopStart = lStart;
    var loopEnd = lEnd;
    var pathurl = '@Url.Action("GetUnAuthorisedList", "Miscellaneous",new{@area="PaperLaidDepartment"}))';

    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {

            PageNumber: pageNo,
            RowsPerPage: rPage,
            loopStart: lStart,
            loopEnd: lEnd,
            deptId: deptid
        },
        success: function (data) {
            //ClearControls();
            $("#unauthorisedList").html(data);
            // cl.hide();
        }
    });
}

function NextAndPreviousButtonPendingPLIHBills(rPage, pageNo, lStart, lEnd) {
    //var cl = new CanvasLoader('canvasloader-container1');
    //cl.show();
    var deptid = $('#SecDeptLst :selected').val();
    $.ajaxSetup({ cache: false });
    var pageNumber = pageNo;
    var rowsPerPage = rPage;
    var loopStart = lStart;
    var loopEnd = lEnd;
    var pathurl = '@Url.Action("GetUnAuthorisedList", "Miscellaneous",new{@area="PaperLaidDepartment"}))';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {

            PageNumber: pageNo,
            RowsPerPage: rPage,
            loopStart: lStart,
            loopEnd: lEnd,
            deptId: deptid
        },
        success: function (data) {
            //ClearControls();
            $("#unauthorisedList").html(data);
            // cl.hide();
        }
    });
}
