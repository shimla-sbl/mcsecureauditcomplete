﻿function DeActivateDsc(data) {
    //alert("deactive" + data);
    var id = data;
    var dscststus = $("#dscstatus").val();
    var dscarray = dscststus.split(',');

    var dscId = dscarray[1];
    // if (dscarray[0] == "deactivate") {

    var sdata = "deactivate," + id;
    $("#dscstatus").val(sdata);

    var tdid = "Radiotd" + id;
    var linkid = id;
    $("td#" + tdid).empty();

    $("td#" + tdid).html("<a id='" + linkid + "' class='activeCls' onclick='ActivateDsc(this.id)'>Activate</a>");

    // }
    //$.ajax({
    //    url: "/UserRegistration/UserRegistration/DeactivateUserDsc",
    //    type: 'GET',
    //    data: {
    //        DscId: id

    //    },
    //    success: function (data) {
    //        $("#dsclist").empty();
    //        $("#dsclist").html(data);
    //    },
    //    error: function (xhr, ajaxOptions, thrownError) {

    //        alert(xhr.responseText);
    //        alert(thrownError);
    //        location.reload(true);
    //    }
    //});

}

function ActivateDsc(data) {

    // alert("active" + data);
    var id = data;

    var dscststus = $("#dscstatus").val();
    var dscarray = dscststus.split(',');
    //   alert(dscststus);
    //   alert(dscarray[0]);
    var dscId = dscarray[1];
    if (dscarray[0] == "deactivate") {
        var sdata = "activate," + id;
        //   alert(sdata);
        $("#dscstatus").val(sdata);

        var tdid = "Radiotd" + id;
        var linkid = id;
        $("td#" + tdid).empty();

        $("td#" + tdid).html("<a id='" + linkid + "' class='activeCls' onclick='DeActivateDsc(this.id)'>DeActivate</a>");
    }
    else {
        var sdata = "activate," + id;
        $("#dscstatus").val(sdata);
        //var sdfsd = "sdfsd,"
        var newtdid = "Radiotd" + id;
        var newlinkid = id;
        $("td#" + newtdid).empty();
        $("td#" + newtdid).html("<a id='" + newlinkid + "' class='activeCls' onclick='DeActivateDsc(this.id)'>DeActivate</a>");

        var oldtdid = "Radiotd" + dscId;
        //var oldlinkid = "Radiobtn" + dscId;
        $("td#" + oldtdid).empty();
        $("td#" + oldtdid).html("<a id='" + dscId + "' class='activeCls' onclick='ActivateDsc(this.id)'>Activate</a>");


    }


    //$.ajax({
    //    url: "/UserRegistration/UserRegistration/ActivateUserDsc",
    //    type: 'GET',
    //    data: {
    //        DscId: id

    //    },
    //    success: function (data) {
    //        $("#dsclist").empty();
    //        $("#dsclist").html(data);
    //    },
    //    error: function (xhr, ajaxOptions, thrownError) {

    //        alert(xhr.responseText);
    //        alert(thrownError);
    //        location.reload(true);
    //    }
    //});
}
function GetOTP() {

    var empId = document.getElementById("UserName").value
    var Mobile = document.getElementById("Mobile").value

    if (empId == "" || Mobile == "") {
        alert("Make Sure you have entered emp id and Mobile Number.")
        //jAlert("", "Okay");
        return false;
    }
    $.ajax({
        url: "/UserRegistration/UserRegistration/GetOTP",
        type: 'GET',
        data: {
            empId: empId,
            mobile: Mobile
        },
        success: function (data) {

            if (data) {
                var OTP;
                $("#ShowOTP").html(data);
                $('#otpTable tr').each(function () {
                    OTP = $(this).find("#otpvalue").html();
                });
                //$("#ValidateMobile").val(Mobile);
                $('#hdnOTPValues').val(OTP);

                //var dialog = $("#PopModel").removeClass('hide').dialog({
                //    modal: true,
                //    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i> OTP Number</h4></div>",
                //    title_html: true,
                //    width: 600,
                //    buttons: [
                //        {
                //            text: "OK",
                //            "class": "btn btn-primary btn-xs",
                //            click: function () {
                //                $(this).dialog("close");
                //            }
                //        }
                //    ]
                //});
            }
            else {
                alert("Unable to Found the Emp or Member Id");
            }
           
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
            alert(thrownError);
            location.reload(true);
        }
    });
}

//$('#DdlSecretory').change(function () {
function SecretoyLstChange() {

    var secretoryId = $('#DdlSecretory option:selected').val();
    //alert(secretoryId);

    $.ajax({
        url: "/UserRegistration/UserRegistration/GetSelectedDeptBySecID",
        type: 'GET',
        data: {
            secratoryId: secretoryId
        },
        success: function (data) {
            if (data) {
                $("#departmentChkBoxList").empty();
                $("#departmentChkBoxList").html(data);
            }
            else {

                alert("Unable to Found the Emp or Member Id");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
            alert(thrownError);
            location.reload(true);
        }
    });
}

function CheckOTPByID() {

    // alert("checkopt");
    var response = false;
    var empId = document.getElementById("UserName").value
    var Mobile = document.getElementById("Mobile").value
    var OTPValue = document.getElementById("OTPValue").value

    var mobileno = document.getElementById("MobileNo").value;
    // alert(Mobile + "," + mobileno);
    if (Mobile != mobileno) {
        $.ajax({
            url: "/UserRegistration/UserRegistration/CheckOTPByIDForUserUpdate",
            type: 'GET',
            data: {
                empId: empId,
                Mobile: Mobile,
                OTPValue: OTPValue
            },
            success: function (data) {
                //alert(data);
                if (data) {
                    response = true;
                }
                else {
                    alert("mismatch OTP.");
                    response = false;
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                alert(xhr.responseText);
                alert(thrownError);
                location.reload(true);
            }
        });
    }
    else {
        // alert("not match");
        response = true;
    }
    return response;
}

$(document).ready(function () {

    //$('#captchacontainer').find('br').remove();
    //$('#CaptchaInputText').attr('placeholder', 'Enter the text shown above');

    //$("#1").prop("checked", true);
    //$("input[name$='SelectType']").click(function () {
    //    $("#Name").html("");
    //    $("#Address").html("");
    //    $("#EmpOrMemberCode").val("");
    //});
    //$("#deptList").hide();
    //$("#secretoryField").hide();
    //$("#Name").html("");
    //$("#Address").html("");
    //$("#DdlSecDept").multipleSelect({ selectAll: false, placeHolder: "Select Member" });

});

function CancelForm() {

    $("#RightSidebar").empty();
    $("#ToBeHideDiv").show("slow", function () {
    });
    $("#aPrev").show();
    $("#aNext").hide();
    $("#btnBackHideDiv").hide();
    $("#ExpandDiv").animate({ width: '43%' }, "slow");

}


function RemoveDSC() {
    $("#applateResultDiv").addClass('hide');
    $("#DSCNameValue").text("");
    $("#DSCName").val("");
    $("#DSCTypeValue").text("");
    $("#DSCType").val("");
    $("#DSCValidity").val("");
    $("#DSCHash").val("");
}

function getHash() {
    var Id = document.getElementById("UserName").value;
    if (Id == "") {

        alert("Maker sure you have entered employee or member id");
        return false;
    }


    var dialog = $("#HashModel").removeClass('hide').dialog({
        modal: true,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i>Select DSC Certificate</h4></div>",
        width: 880,
        title_html: true,

        buttons: [

            {
                text: "OK",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    var dschash = document.getElementById("DSCHashUniqueID").getCertHash();
                    //alert(dschash);

                    if (dschash != "") {
                        getHashModel();

                        $.ajax({
                            url: "/UserRegistration/UserRegistration/CheckDSCHasExist",
                            type: 'GET',
                            data: {
                                Dschashcode: dschash

                            },
                            success: function (data) {
                                // alert(data);
                                if (data) {

                                    //alert("true income");
                                    $("#applateResultDiv").addClass('hide');
                                    $("#DSCNameValue").text("");
                                    $("#DSCName").val("");
                                    $("#DSCTypeValue").text("");
                                    $("#DSCType").val("");
                                    $("#DSCValidity").val("");
                                    $("#DSCHash").val("");

                                    alert("DSC Already Exist");
                                }
                                else {

                                    $(this).dialog("close");
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                alert(xhr.responseText);
                                alert(thrownError);
                                location.reload(true);
                            }
                        });

                    }
                    else {
                        $(this).dialog("close");
                    }



                }
            }
        ]
    });

}

function getHashModel() {

    var DSCName = document.getElementById("DSCHashUniqueID").getCertName();
    var DSCType = document.getElementById("DSCHashUniqueID").getCertType();
    var DSCValidity = document.getElementById("DSCHashUniqueID").getCertValidity();
    $("#applateResultDiv").removeClass('hide');
    $("#DSCNameValue").text(DSCName);
    $("#DSCName").val(DSCName);
    $("#DSCTypeValue").text(DSCType);
    $("#DSCType").val(DSCType);
    $("#DSCValidity").val(DSCValidity);
    $("#DSCHash").val(document.getElementById("DSCHashUniqueID").getCertHash());
    //var data = false;

    //var dschash = document.getElementById("DSCHashUniqueID").getCertHash();

    //if ($('#userDscTable').length > 0) {
    //    var dscnamesoftable = [];
    //    $("td.dscName").each(function (i) {
    //        dscnamesoftable[i] = $(this).text();
    //    });

    //    if (dscnamesoftable.length > 0) {


    //        for (var i = 0; i < dscnamesoftable.length ; i++) {


    //            if (dscnamesoftable[i].trim() == DSCName.trim()) {

    //                data = true;
    //                break;
    //            }


    //        }

    //        if (!data) {
    //            $("#applateResultDiv").removeClass('hide');
    //            $("#DSCNameValue").text(DSCName);
    //            $("#DSCName").val(DSCName);
    //            $("#DSCTypeValue").text(DSCType);
    //            $("#DSCType").val(DSCType);
    //            $("#DSCValidity").val(DSCValidity);
    //            $("#DSCHash").val(document.getElementById("DSCHashUniqueID").getCertHash());

    //        }
    //    }
    //    else {
    //        $("#applateResultDiv").removeClass('hide');
    //        $("#DSCNameValue").text(DSCName);
    //        $("#DSCName").val(DSCName);
    //        $("#DSCTypeValue").text(DSCType);
    //        $("#DSCType").val(DSCType);
    //        $("#DSCValidity").val(DSCValidity);
    //        $("#DSCHash").val(document.getElementById("DSCHashUniqueID").getCertHash());

    //    }

    //}
    //else {
    //    $("#applateResultDiv").removeClass('hide');
    //    $("#DSCNameValue").text(DSCName);
    //    $("#DSCName").val(DSCName);
    //    $("#DSCTypeValue").text(DSCType);
    //    $("#DSCType").val(DSCType);
    //    $("#DSCValidity").val(DSCValidity);
    //    $("#DSCHash").val(document.getElementById("DSCHashUniqueID").getCertHash());

    //}

    //return data;


}


var otpvalue = "";
var flag = false;

function checkValidation() {
    var mobileno = document.getElementById("MobileNo").value;
    var moible = document.getElementById("Mobile").value;

    otpvalue = document.getElementById("hdnOTPValues").value;
    var otpId = document.getElementById("OTPValue").value;
    

    if (mobileno != moible) {
        if (otpvalue != otpId) {
            alert("otpvalue:" + otpvalue + "otpId:" + otpId);
            alert("OTP Mismatch");
            return false;
        }
        else {
            return true;
        }
    }

}


function ChangeAdhar() {

    // var onkeyup = "this.value=this.value.replace(/[^\\d]/,'')";
    var AadarId = $('#AadarId').val();
    if (AadarId.length == 12) {

        if (/^[0-9\-]+$/i.test(AadarId)) {

            return true;

        }
        else {

            alert("Evidhan ID should be in digits.");
            $('#AadarId').val("");
            $('#AadarId').focus();
            return false;

        }
    }
    else {

        alert("Evidhan ID Should be in 12 numbers.");
        $('#AadarId').val("");
        $('#AadarId').focus();
        return false;
    }
}

function ChangeMobile() {
   
    var MobilNumber = $('#Mobile').val();
    var mobileno = document.getElementById("MobileNo").value;

    if (MobilNumber != mobileno) {

        if (MobilNumber.length == 10) {
            //MobilNumber = MobilNumber.replace("(", "");
            //MobilNumber = MobilNumber.replace(")", "");
            //MobilNumber = MobilNumber.replace("-", "");
            //MobilNumber = MobilNumber.replace(" ", "");

            if (/^[0-9\-]+$/i.test(MobilNumber)) {
                //Show button
                return true;

            }
            else {

                alert("Make Sure you entered valid Mobile number.");
                $('#Mobile').val('');

                $('#Mobile').focus();

                //Hide button
                return false;
            }

        }
        else {


            $('#Mobile').val('');
            alert("Make sure Mobile Number should be in 10 digits.");
            //Hide button
            return false;

        }
    }
    else {
    }
}