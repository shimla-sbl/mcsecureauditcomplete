﻿$(document).ready(function () {
    frameResize();

    /*    // move this part to the tab html page 
        $('#tabs .tabcontent div').hide();
        $('#tabs .tabcontent div:first').show();
        $('#tabs ul li:first').addClass('active');
    
        $('#tabs ul li a').click(function () {
            $('#tabs ul li').removeClass('active');
            $(this).parent().addClass('active');
            var currentTab = $(this).attr('href');
            $('#tabs .tabcontent div').hide();
            $(currentTab).show();
            return false;
        });
                              * 
                              */

    $('.eachmember').tooltipster({
        animation: 'grow',
        content: 'Loading....',
        position: 'left',
        fixedWidth: 500,
        maxWidth: 500,
        interactive: true,
        theme: '.tooltipster-light',
        trigger: 'hover',
        functionBefore: function (origin, continueTooltip) {
            continueTooltip();

            if (origin.data('ajax') !== 'cached') {

                setTimeout(function () {
                    //console.log(1);
                    var htmcont = $('#' + origin.context.id).html();
                    var memId = origin.context.id;
                    if (memId == 'memb2' || memId == 'memb6') {
                        origin.data('tooltipsterContent', '<div style="width:300px;"">' + htmcont + '<br><a href="/RightSideBar/RightSideBar/ReadMore?memId=' + memId + '">Read More...</a></div>').data('ajax', 'cached');
                    }
                    else if (memId == 'memb1') {
                        origin.data('tooltipsterContent', '<div style="width:300px;"">' + htmcont + '<br><a target="_blank" href="http://himachalrajbhavan.nic.in/Governor.html">Read More...</a></div>').data('ajax', 'cached');
                    }
                    else {
                        origin.data('tooltipsterContent', '<div style="width:300px;"">' + htmcont + '<br><a href="/RightSideBar/RightSideBar/ReadMore?memId=' + memId + '"></a></div>').data('ajax', 'cached');
                    }
                }, 300);
            }
        }
    });
    /*
        //      left side bar scrool
        $('.sidebarlistscroll').jScrollPane({
            horizontalGutter: 5,
            verticalGutter: 5,
            'showArrows': false
        });
    
        $('#left-sidebar .jspDrag').hide();
        $('#left-sidebar .jspScrollable').mouseenter(function () {
            $('#left-sidebar .jspDrag').stop(true, true).fadeIn('slow');
        });
        $('#left-sidebar .jspScrollable').mouseleave(function () {
            $('#left-sidebar .jspDrag').stop(true, true).fadeOut('slow');
        });
             */


    //      right side bar scrool
    $('.noticeboard').jScrollPane({
        horizontalGutter: 5,
        verticalGutter: 5,
        'showArrows': false
    });

    $('#right-sidebar .jspDrag').hide();
    $('#right-sidebar .jspScrollable').mouseenter(function () {
        $('#right-sidebar .jspDrag').stop(true, true).fadeIn('slow');
    });
    $('#right-sidebar .jspScrollable').mouseleave(function () {
        $('#right-sidebar .jspDrag').stop(true, true).fadeOut('slow');
    });

    //    //set slide show interval
    //    $(function() {
    //        setInterval( "slideSwitch()", 5000 );
    //    });
    //
    //$(".page_prev").onclick = function() {
    //    alert(1);
    //    //$('.news_container').fadeOut('slow').fadeIn('slow');
    //    $('.news_container').hide('slow').show('slow');
    //}
    //$(".page_next").onclick = function() {
    //$('.news_container').fadeOut('slow').fadeIn('slow');
    //    $('.news_container').hide('slow').show('slow');
    //}


  //   ------------   Popup div  functionality ---------------------
  $('a.login-window').click(function() {
    
            //Getting the variable's value from a link 
    var loginBox = $(this).attr('href');

    //Fade in the Popup
    $(loginBox).fadeIn(300);
    
    //Set the center alignment padding + border see css style
    var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    
    $(loginBox).css({ 
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    
    // Add the mask to body
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    
    return false;
});

    // When clicking on the button close or the mask layer the popup closed
    $('a.close, #mask').live('click', function() { 
        $('#mask , .login-popup').fadeOut(300 , function() {
            $('#mask').remove();  
            
        }); 
        return false;
    });
  //   ------------ EOD  Popup div  functionality --------------------- 

});

//function slideSwitch()
//{
//alert('test');
//}
//$(function () {
// Simplest jQuery slideshow by Jonathan Snook
//$('#slideshow img:gt(0)').hide();
//    setInterval(function () {
//        $('#slideshow :first-child').fadeOut(1000)
//        .next('img').fadeIn(1000).end().appendTo('#slideshow');
//        }, 4000);
//});
function noticescrool() {
    $('.notice_container').hide('slow').show('slow')
}
function pagescrool() {
    $('.news_container').hide('slow').show('slow')
}
$(window).resize(function () {
    frameResize();
});

function frameResize() {
    //var doc_width = $(document).width();
    var doc_width = $(window).width();
    var doc_height = $(window).height();
    var padright, padleft;
    debug(doc_width);
    debug(doc_height);
    // set min height of main div , so that the footer lies on bottom of the page
    $(".main").css('minHeight', ($(window).height() - ($('header').outerHeight() + $('footer').outerHeight())));

    if (doc_width < 1260) {
        $('#right-sidebar').hide();
        $('#left-sidebar').hide();
        pad = 0;        // default pad
    } else {
        $('#right-sidebar').show();
        $('#left-sidebar').show();
        padleft = 175;        // default pad
        padright = 175;        // default pad
    }
    $('#right-sidebar').width(padright);
    $('#left-sidebar').width(padleft);



    if (!($('#right-sidebar').html())) {
        padright = 0;
    }

    if (!($('#left-sidebar').html())) {
        padleft = 0;
    }




    $('body').css({
        paddingLeft: padleft + 'px',
        paddingRight: padright + 'px'
    });


   

    $('.noticeboard').height(doc_height - ($('.memberlist').height() + 30));

}
$(function () {

    setTimeout(function () {   //calls click event after a certain time
        frameResize();
    }, 1500);
});
function debug(data) {
    //console.debug(data);  // only for firebug enabled FF. show error on other browser, Remove it after development
}