﻿/*****************************************************************************************************************/
//This transliteration service is hosted to be used by MMPs only.                                                //
//Author - CDAC GIST                                                                                       //                                                                                             //
//Version : 1.0.0.9                                                                                             //
/*****************************************************************************************************************/

/***************************GLOBAL VARIABLES STARTS***********************/
///////////////////////////////////////////////////////////////////////////
/******Fixed Variables******/
var glocale = "hi_in";
var radioBtn = "input:radio[name=Language]";
var url = 'http://10.146.26.202:86/';
var transJSPath = url+"js";
var mainUrl = "";
var maxTypingLength = 15;
var topMargin = 27;
var leftMargin = 15;
var lineHeight = 25;
var noOfLinesInTextArea = 11;
var typingLayout = "typewriter";
/******Fixed Variables******/

var element_Id = "";  //To store id of input element
var languageMn = ""; //To store language mnemonic
var targetId = "";   //To store target element id for displaying output
var content_Id = ""; //To store id of Popup element
var popbox_Id = ""; //To store Pop Box Id
var suggestion_Id = "" //To store suggestion box id
var container_Id = "" //To store clonediv id
var caretPos = 0;
var shift = false;
var capsCounter = 0;
var tempLength = 0;
var suggestionsCount = 0;
var arrSugg = new Array();  //array for storing the suggestions of any word.
var arrWords = new Array(); //array for storing the keycode of each key pressed.
var upDownArrowCounter = 0;
var nextLineChecker = 0;
var originalText = "";
var beforeText = "";
var afterText = "";
var text = "";  //Input english word.
var result = "";
var browserName = "";
var totalTopPos = 0;
var totalLeftPos = 0;
var storePrevKeyCode = 0;
var suggestions = "";
var hashtableHI_IN = new Hashtable();
var hashtableGJ_IN = new Hashtable();
var hashtableMR_IN = new Hashtable();
var hashtablePN_IN = new Hashtable();
var hashtableML_IN = new Hashtable();
var hashtableBN_IN = new Hashtable();

var hashTableName = hashtableHI_IN;

var selLang = "";
var bsCount = 0;
var indS = 0;

/*****************************GLOBAL VARIABLES ENDS***********************/
///////////////////////////////////////////////////////////////////////////
/***************************BODY ONLOAD EVENTS STARTS*********************/

function SetTypingLayout(layout)
{
	typingLayout = layout;
}

function SetText()
{
	if ($(suggestion_Id).text() != "")
	{
		var divID = '#' + arrSugg[upDownArrowCounter];
		var selectedTranslation = $(divID).text();
		$(element_Id).val($(element_Id).val()+" "+selectedTranslation);
		resetValues();
	}
}

function EnableTyping(controlIds) {
//("Got in");
    //typingLayout = layout;
    executeOnLoad(controlIds);
}

//$(window).load(executeOnLoad);

function executeOnLoad(controlIds) {//To Hide the div id="cloneBox" Using it to calsulate the position//
    try {
        
		element_Id = "#"+controlIds;//"#InputTextBox";
        popbox_Id = "#popupBox_"+controlIds;
        content_Id = "#PopUpInputBox_"+controlIds;
        suggestion_Id = "#Suggestions_"+controlIds;
        container_Id = "#democontainer_"+controlIds;
        languageMn = 'hi_in';
		
		//var offset = $(element_Id).offset();
		
		if(document.getElementById("popupBox_"+controlIds) == null)
		{
			$(container_Id).append("<div class='popup' id='popupBox_"+controlIds+"'><div id='PopUpInputBox_"+controlIds+"' class='PopupTypingBox'></div><div id='Suggestions_"+controlIds+"' class='suggestnBox'></div></div>");
			//$(popbox_Id).css("top",offset.top+"px");
			//$(popbox_Id).css("right",offset.left+"px");
		}
        resetValues();

        hashTableName.clear();

        browserName = getBrowserName();
        $(element_Id).attr("autocomplete", "off");
        $(element_Id).focus();
    } catch (ex) {
		alert(ex.Message);
    }
}

function Expand(obj) {
    try {
        if (!obj.savesize) obj.savesize = obj.size;
        obj.size = Math.max(obj.savesize, obj.value.length);
    } catch (ex) {
        alert(ex.Message);
    }
}

if (window.onresize) {
    window.onresize = function () { resetValues(); };
}
/***************************BODY ONLOAD EVENTS ENDS************************/
////////////////////////////////////////////////////////////////////////////
/************************DOCUMENT READY FUNCTION STARTS********************/



$(document).ready(function () {

    
	SetTypingLayout("typewriter");
	 
	$("input, textarea").click(function()
	{
		var tempObj = this;
		$(this).replaceWith("<elem id='democontainer_"+$(tempObj).attr("id")+"' class='democontainer_dynamic'></elem>");
		$("#democontainer_"+$(tempObj).attr("id")).append(tempObj);
		EnableTyping($(tempObj).attr("id")); 
	});
	
	//Handling Body KEYDOWN EVENTS STARTS//
    $("body").keydown(function (e) {                                                        //To Detect Caps Lock//
        try {
            if (e.keyCode == 20) {
                capsCounter++;
            }
        } catch (ex) {
            alert(ex.Message);
        }
    });
    //Handling Body KEYDOWN EVENTS ENDS//

    //Handling Double Click Event for InputTextBox STARTS//
    $(element_Id).dblclick(function () {
        try {
            funShowSuggOfSelWord(element_Id, 'False');
        } catch (ex) {
            //alert(ex.Message);
        }
    });
    //Handling Double Click Event for InputTextBox ENDS//

    //Handling KeyDown Event for InputTextBox STARTS//
    $(document).on('keydown', element_Id, function (e) 
	{
        try 
		{
            var tmp = String.fromCharCode(e.keyCode);          //Getting key value in upper case by default//

            if (e.shiftKey) {   //To Detect Shift Key Press//
                shift = true;
            }

            var flag = false; //For Key Combos of ctrl+a, ctrl+x, ctrl+v, ctrl+u
            if (e.ctrlKey) 
			{
                var flag = true;
                if ((e.keyCode == 65 || e.keyCode == 67 || e.keyCode == 85 || e.keyCode == 86 || e.keyCode == 88) && flag) 
				{
                    resetValues();
                    $(popbox_Id).hide();
                    $(content_Id).hide();
                    flag = false;
                    return;
                }
            }

            //For Alphabets typing on main text box//
            if (e.keyCode > 64 && e.keyCode < 91 && !flag) {//If the key pressed is an alphabet eg: from A to Z and not combo key//
                e.preventDefault();
                if (shift || capsCounter % 2 != 0) {//Checking for shift key and caps lock to manage upper and lower cases starts//
                    tmp = tmp.toUpperCase();
                    shift = false;
                } else
                    if (shift != true || capsCounter % 2 == 0) {
                        tmp = tmp.toLowerCase();
                    }                               //Checking for shift key and caps lock to manage upper and lower cases ends//

                if ($(content_Id).text().length < maxTypingLength)
                    $(content_Id).html($(content_Id).text() + tmp);
                else
                    return;

                tempLength = $(content_Id).text().length;
            }

            if ($(popbox_Id).is(':visible')) {
                if ($(content_Id).text() == "" && e.keyCode == 8) {//For Backspace + no value in popup
                    e.preventDefault();
                    resetValues();
                }
                else if (e.keyCode == 37 || e.keyCode == 39) { // Right Left Arrow Key
                    e.preventDefault();
                    return;
                }
                else if (e.keyCode == 8) {//For backspace key only if popup is visible
                    e.preventDefault();
                    var tempText = $(content_Id).text();
                    var len = tempText.length;
                    tempText = tempText.substr(0, len - 1);
                    $(content_Id).html(tempText);
                }
                else if (e.keyCode == 27) {//Esc Key
                    var str = $(element_Id).text().replace(text, "");
                    $(element_Id).text(str);
                    resetValues();
                }
                else if ((e.keyCode == 32 || e.keyCode == 13) && $(suggestion_Id).html() != "") { //space key //Enter Key
                    try {
                        e.preventDefault();
                        //SetText();
						setSuggestionToTextbox();
                    } catch (ex) {
                        //alert(ex.Message);
                    }
                }
                else if (e.keyCode == 38) { //Up Arrow Key
                    e.preventDefault();
                    if (upDownArrowCounter - 1 >= 0) {
                        unSelectSuggestion(upDownArrowCounter);
                        --upDownArrowCounter;
                        selectSuggestion(upDownArrowCounter);
                    }
                }
                else if (e.keyCode == 40) { //Down Arrow Key
                    e.preventDefault();
                    if (upDownArrowCounter + 1 < suggestionsCount) {
                        unSelectSuggestion(upDownArrowCounter);
                        ++upDownArrowCounter;
                        selectSuggestion(upDownArrowCounter);
                    }
                }
                else if ((e.keyCode < 64 || e.keyCode > 91) && e.keyCode != 16) { //For Keys Except all the above and shift key
                    resetValues();
                    $(popbox_Id).hide();
                    return;
                }
            }

            if (e.keyCode == 13 && $(popbox_Id).is(':visible') == false && browserName == 'msie') { //For Enter key in IE
                e.preventDefault();
                // var cursorPos = getCaretPosInIE();
                // var tempBfrText = tempWholeText.substr(0, cursorPos);
                // var tempAftText = tempWholeText.substr(cursorPos, wholeLength);
                // $(element_Id).innerHTML(tempBfrText + "\n\r" + tempAftText);
                // $(element_Id).setCursorPosition(cursorPos);
				alert("Currently there is Limited Support for Internet Explorer\nTry using another browser");
            }

            //if ((e.keyCode < 64 || e.keyCode > 91) && !$(popbox_Id).is(':visible')) { //For Keys Except all the above
            //    //alert("other key presed");
            //}
        }
        catch (ex) {
            alert(ex.Message);
        }
    });
    //Handling KeyDown Event for InputTextBox ENDS//

    //Handling KeyUp Event for InputTextBox STARTS//
    $(document).on('keyup', element_Id, function (e) {
        try {
            var flag = false; //For Key Combos of ctrl+a, ctrl+x, ctrl+v, ctrl+u
            if (e.ctrlKey) {
                var flag = true;
                if ((e.keyCode == 65 || e.keyCode == 67 || e.keyCode == 85 || e.keyCode == 86 || e.keyCode == 88) && flag) {
                    resetValues();
                    $(popbox_Id).hide();
                    $(content_Id).hide();
                    flag = false;
                    return;
                }
            }
	
             if (e.keyCode == 13 && $(popbox_Id).is(':visible') == false && browserName == 'msie') { //For Enter key in IE
                 e.preventDefault();
                // var cursorPos = getCaretPosInIE();
                // var tempBfrText = tempWholeText.substr(0, cursorPos);
                // var tempAftText = tempWholeText.substr(cursorPos, wholeLength);
                // $(element_Id).innerHTML(tempBfrText + "\n\r" + tempAftText);
                // $(element_Id).setCursorPosition(cursorPos);
				alert("Currently there is Limited Support for Internet Explorer\nTry using another browser");
            }

            if (e.keyCode > 64 && e.keyCode < 91) {//For Alphabets typed on main text box A to Z
                $(suggestion_Id).html(""); //Important Otherwise suggestion will repeat itself
                setPopupBox();
                SuggestOnNewChar(e, "FORWARD", glocale, content_Id);
                if (tempLength < maxTypingLength) { //to check the typing limit
                    $(popbox_Id).show();
                    $(content_Id).show();
                    tempLength = 0;
                }
                else { //if user types more then the limit 1st suggestion automatically gets selected 
                    return;
                }
            }
            else if (e.keyCode == 8 && $(popbox_Id).is(':visible')) { //for backspace if popup is visible
                if ($(content_Id).text() == "") {
                    resetValues();
                    return;
                }
                setPopupBox();
                $(suggestion_Id).html("");
                SuggestOnNewChar(e, "REVERSE", glocale, content_Id);
                $(popbox_Id).show();
            }

            //if ((e.keyCode < 64 || e.keyCode > 91) && !$(popbox_Id).is(':visible')) { //For Keys Except all the above
            //    //alert("other key presed");
            //    $(element_Id).append(e.keyText);
            //}
        }
        catch (ex) {
            //alert(ex.Message);
        }
    });
    //Handling KeyUp Event for InputTextBox ENDS//
	
    //Handling Radio Button Events
    $("input:radio[name=Language]").click(function () {
        try {
            glocale = $(this).val();
            setHashTableName();
            $(element_Id).focus();
        } catch (ex) {
            //alert(ex.Message);
        }
    });
    //Handling KeyUp Event for InputTextBox ENDS//

    //Handling Radio Button Events Starts//
    $(radioBtn).click(function () {
        try {
            glocale = $(this).val();
            $(element_Id).focus();
        } catch (ex) {
            //alert(ex.Message);
        }
    });
    //Handling Radio Button Events Ends//

    //Handling MouseEnter Leave events for suggestion box Starts//
    $(document).on('mouseenter', '.suggestnBox', function () {
        unSelectSuggestion(upDownArrowCounter);
    }).on('mouseleave', '.suggestnBox', function () {
        selectSuggestion(upDownArrowCounter);
    });
    //Handling MouseEnter Leave events for suggestion box Ends//

    //Handling Click Event for PopupInput box Starts//
    $(document).on('click', '.PopupTypingBox', function () {
        resetValues();
        $(element_Id).setCursorPosition(caretPos);
    });
    //Handling Click Event for PopupInput box Ends//

    //Handling Click Event for TextArea Starts//
    $(document).on('click', '.editor', function () {
        resetValues();
        //$(element_Id).setCursorPosition(caretPos);
    });
    //Handling Click Event for TextArea Ends//
});

/************************DOCUMENT READY FUNCTION ENDS**********************/
////////////////////////////////////////////////////////////////////////////
/***************************OTHER FUNCTIONS STARTS*************************/
$.fn.getRange = function () {
    try {
        var sel, Range;
        if (window.getSelection) {
            sel = window.getSelection();
            if (sel.rangeCount) {
                Range = sel.getRangeAt(0);
            }
            else if (document.selection && document.selection.createRange) {
                Range = document.selection.createRange();
            }
        }
        return Range;
    }
    catch (ex) {
        //alert(ex.Message);
    }
}

function getCaretPosInIE() {
    //$(element_Id).focus();
    try {
        var sel = document.selection.createRange();
        sel.moveStart('character', -document.activeElement.innerHTML.length);

        var tmp = $(sel.htmlText);
        if (tmp.length > 0)
            var tmpText = tmp[tmp.length - 1].innerText;
        else
            var tmpText = tmp.selector;
        var cursorPos = tmpText.length;
        return cursorPos
    } catch (ex) {
        //alert(ex.Message);
    }
}

function setPopupBox() {
    try {
        upDownArrowCounter = 0;
        var TPos = 0;
        var LPos = 0;
        var totalTextLength = 0;
        var range = "";

        originalText = $(element_Id).val() || $(element_Id).text();
        totalTextLength = originalText.length; //Calculating total text length

        if (browserName == 'msie' || browserName == 'mozilla') { // For IE
            var textBoxId = $(element_Id).attr('id');
            var textBox = document.getElementById(textBoxId);
            caretPos = getCaretPosInIE();   //Calculating caret position
            beforeText = originalText.substring(0, caretPos); //Calculating text before cursor position
            afterText = originalText.substring(caretPos, totalTextLength); //Calculating text after cursor position
            var beforeLinesArray = beforeText.split('\n');
            var currentLineCount = beforeLinesArray.length - 1; //Lines Before Caret Position

            TPos = 7;
            LPos = textWidth(beforeLinesArray[currentLineCount]);
            resetSpan();

            //for top position
            if (currentLineCount == 0)
                totalTopPos = TPos + topMargin;
            else if (currentLineCount > noOfLinesInTextArea)
                totalTopPos = topMargin + (topMargin * noOfLinesInTextArea);
            else
                totalTopPos = textBox.clientHeight;

            //for left position
            if (LPos < textBox.clientWidth)
                totalLeftPos = LPos + leftMargin; //Final left position
            else
                totalLeftPos = textBox.clientWidth;


            //if ((totalLeftPos - LPos) > $(element_Id).clientWidth) {
            //    totalLeftPos = totalLeftPos - $(element_Id).clientWidth;
            //}
            //if ((totalTopPos - LPos) > $(element_Id).clientHeight) {
            //    totalLeftPos = totalLeftPos - $(element_Id).clientWidth;
            //}
        }
        else { //FOR OTHER BROWSER
            range = $(element_Id).getRange();

            if (range.commonAncestorContainer.firstChild.nodeName == "TEXTAREA") {
                caretPos = range.commonAncestorContainer.firstChild.selectionEnd; //Calculating caret position          
                beforeText = originalText.substring(0, caretPos); //Calculating text before cursor position
                afterText = originalText.substring(caretPos, totalTextLength); //Calculating text after cursor position

                var beforeLinesArray = beforeText.split('\n');
                var currentLineCount = beforeLinesArray.length - 1; //Lines Before Caret Position

                var afterLinesArray = afterText.split('\n');
                var afterlinecount = afterLinesArray.length - 1;

                TPos = range.commonAncestorContainer.parentNode.offsetTop; //Calculating temp top position
                LPos = textWidth(beforeLinesArray[currentLineCount]); //Calculating temp left position
                resetSpan();

                //for top position
                if (currentLineCount == 0)
                    totalTopPos = topMargin + TPos;//40 + (topMargin * currentLineCount);
                else if (currentLineCount < noOfLinesInTextArea)
                    totalTopPos = TPos + topMargin + (topMargin * currentLineCount);
                else
                    totalTopPos = range.commonAncestorContainer.firstChild.clientHeight;

                //for left position
                if (LPos < range.commonAncestorContainer.firstChild.clientWidth)
                    totalLeftPos = LPos + leftMargin;
                else
                    totalLeftPos = range.commonAncestorContainer.firstChild.clientWidth;

                //if (LPos < range.commonAncestorContainer.firstChild.clientWidth && has_scrollbar("HORIZONTAL")) {
                //    //var len = range.commonAncestorContainer.firstChild.scrolLength;
                //    totalLeftPos = LPos;
                //}

                //if (LPos > range.commonAncestorContainer.firstChild.clientWidth && afterLinesArray[0] != "") {
                //    var len = LPos - $(element_Id).clientWidth;
                //    totalLeftPos = $(element_Id).clientWidth - LPos; //LPos - $(element_Id).clientWidth;
                //    $(element_Id).scrollLeft();
                //}
            }
        }

        $(".popup").css('left', totalLeftPos + 'px');
        $(".popup").css('top', totalTopPos + 'px');
    }
    catch (ex) {
        //alert(ex.Message);
    }
}

function textWidth(textToCalculate) {
    try {
        $(container_Id).append($("<span id='widthcalc' style='font-size:15px; font-family:mangal; visibility:hidden; white-space:pre;'></span>"));
        $("#widthcalc").text(textToCalculate);
        return $("#widthcalc").width();
    }
    catch (ex) {
        //alert(ex.Message);
    }
}

function resetSpan() {
    try {
        $("#widthcalc").html("");
        $("#widthcalc").remove();
    } catch (ex) {
        //alert(ex.Message);
    }
};

function setSuggestionToTextbox() {
    try {
        if ($(suggestion_Id).text() != "") {
            var divID = '#' + arrSugg[upDownArrowCounter];
            var selectedTranslation = $(divID).text();
            putWord(selectedTranslation);
            resetValues();
        }
		var tempBeforeText = $(element_Id).val().substr(0, caretPos);
        nextLineChecker = textWidth(tempBeforeText);
        resetSpan();
    }
    catch (ex) {
        //alert(ex.Message);
    }
}

function putWord(word) {
    try {
        if (word != "") {
            var tempWidth = 0;
            if (beforeText == "") {
                $(element_Id).val(beforeText + word + " ");
                caretPos = caretPos + word.length + 1;
            }
            else if (afterText == "" && beforeText != "") {
                $(element_Id).val(beforeText + word + " ");
                caretPos = caretPos + word.length + 1;
            }
            else if (afterText.indexOf(" ") == 0) {
                $(element_Id).val(beforeText + " " + word + " " + afterText);
                caretPos = caretPos + word.length + 1;
            }
            else if (beforeText.lastIndexOf(" ") != -1) {
                $(element_Id).val(beforeText + word + " " + afterText);
                caretPos = caretPos + word.length + 1;
            }
            else {
                $(element_Id).val(beforeText + word + " " + afterText);
                caretPos = caretPos + word.length + 1;
            }
            $(element_Id).setCursorPosition(caretPos);
        }
    }
    catch (ex) {
        //alert(ex.Message);
    }
}

function selectSuggestion(indx) {
    try {
        var divID = '#' + arrSugg[indx];
        $(divID).attr('class', 'selected');
    } catch (ex) {
        //alert(ex.Message);
    }
}

function unSelectSuggestion(indx) {
    try {
        var divID = '#' + arrSugg[indx];
        $(divID).attr('class', 'unselected')
    } catch (ex) {
        //alert(ex.Message);
    }
}

function getBrowserName() {//This function detects the browser and returns the browser name.
    try {
        var browserName = "";
        var ua = navigator.userAgent.toLowerCase();
        if (ua.indexOf("opera") != -1) {
            browserName = "opera";
        }
        else if (ua.indexOf("msie") != -1) {
            browserName = "msie";
        }
        else if (ua.indexOf("safari") != -1) {
            browserName = "safari";
        }
        else if (ua.indexOf("mozilla") != -1) {
            if (ua.indexOf("firefox") != -1) {
                browserName = "firefox";
            }
            else {
                browserName = "mozilla";
            }
        }
        return browserName;
    }
    catch (ex) {
        //alert(ex.Message);
    }
}

function SuggestOnNewChar(e, transDirection, locale, itemId) {
    try {
        if (glocale != locale) {
            glocale = locale;
            hashTableName.clear();
        }

        dWord = "";
        dSuggestions = "";
        selLang = languageMn;

        //for before IE9 only,because IE doesn't pass event as parameter to function.Instead,it makes Event object available
        //as the event property of the global window object.
        if (!e) {
            e = window.event;
        }
        //Storing keyCode of each key press in an array.
        arrWords[indS] = e.keyCode;
        indS++;

        //Variable for storing the keycode of  second last key pressed.
        var prevKeyCode = arrWords[indS - 2];
        storePrevKeyCode = prevKeyCode;
        //Variable for storing the keycode of last key pressed.
        var currKeyCode = arrWords[indS - 1];
        bsCount = bsCount + 1;
        var str = $(content_Id).text();
        str = str.replace(/^\s+|\s+$/g, '')

        if (str == "") {
            $(popbox_Id).hide();
            return;
        }

        if (suggestions != "" && bsCount == 1) {
            showSuggestions(suggestions);
            return;
        }

        flagDblClk = "0";
        //In case of very first character & character after space,ind=1 and prevKeyCode becomes arrWords[-1],so explicitly assigning it value as 0.
        arrWords[-1] = 0;

        if ((currKeyCode >= 65 && currKeyCode <= 122) || transDirection == "REVERSE") {
            //This method will set the global position of the cursor. 
            transDi = transDirection;

            text = $.trim($(content_Id).text());

            if (prevKeyCode == 32 && currKeyCode == 32) {
                lenInputWord = text.length;
                var isUni = false;
                for (var i = 0; i < lenInputWord; i++) {
                    if (!(text.charCodeAt(i) >= 0 && text.charCodeAt(i) <= 255)) {
                        isUni = true;
                        break;
                    }
                    else {
                        isUni = false;
                    }
                }
                if (isUni || firstPart == "") {
                    return false;
                }
            }

            if (currKeyCode == 13) {
                return false;
            }

            if (browserName == "msie" || browserName == "opera") {
                var indx = text.indexOf("\r");
                if (indx != -1) {
                    text = text.substring(0, indx);
                }
            }

            if (selLang == languageMn) {
                //result = hashtableMR2.get2(text);
                result = hashTableName.get(text);
            }

            if (typeof (result) != 'undefined' && result != "" && result != null) {
                showSuggestions(result);
            }
            else {
               // mainUrl = "";
               // mainUrl = url + "Transliteration.aspx?itext=" + text + "&transliteration=NAME&locale=" + locale;

                if (typingLayout === "transliteration") 
                    mainUrl = url + "Transliteration.aspx?itext=" + text + "&transliteration=NAME&locale=" + locale;
                else if (typingLayout === "typewriter") 
                    mainUrl = url + "TypeWriter.aspx?iText=" + text;
               
                //if (browserName == "msie" || browserName == "mozilla") {

                //Implemented AJAX.
                var httpRequest;

                if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                else {// code for IE6, IE5
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (!httpRequest) {
                    alert('Cannot create an XMLHTTP instance');
                    return;
                }
                try {
                    //$.ajax
                    //    ({
                    //        type: "POST",
                    //        url: mainUrl,
                    //        contentType: "application/json; charset=UTF-8",
                    //        crossDomain: true,
                    //        dataType: "json",
                    //        data: JSON.stringify({ iText: text, locale: locale }), // converting javaScript value to json value
                    //        success: function (resData) {
                    //            alert(resData.d);
                    //        },
                    //        complete: function () {

                    //        },
                    //        error: function (xhr, errorType, exception) {
                    //            //var errorDetails = "ERROR MSG : " + xhr.statusText + " , ERROR CODE : " + xhr.status;
                    //            //alert("Error Details : \n" + errorDetails);
                    //            alert('Currently there is no support for Internet Explorer!\nPlease try using different browser.');
                    //        }
                    //    });

                    //httpRequest.open('POST', mainUrl, true);
                    //httpRequest.setRequestHeader("Content-type", "application/json")
                    //httpRequest.send(JSON.stringify({ iText: text, locale: locale }));
                    //httpRequest.onreadystatechange = function () {

                    //    alert(httpRequest.status);
                    //    if (httpRequest.readyState == 4 && httpRequest.status == 200) {
                    //        //Variable for storing result coming from servlet.
                    //        result = httpRequest.responseText;
                    //        if (typeof (result) != 'undefined' && result != "") {
                    //            result = result + text + "^";
                    //            hashtableAdd.put2(text, result);
                    //            hashtableMR2.put2(text, result);
                    //        }
                    //        else {
                    //            result = text + "^";
                    //        }
                    //        $(suggestion_Id).html("");
                    //        showSuggestions(result);
                    //    }
                    //}

                    //Callin service on aspx page
                    httpRequest.onreadystatechange = function () 
					{
                        if (httpRequest.readyState == 4 && httpRequest.status == 200)
						{
                            var result = httpRequest.responseText;
                            if (result != "") {
                                if (selLang == languageMn) {
                                    text = decodeURIComponent(text);
                                    text = $.trim(text);
                                    result = result +"^"+ text + "^";
                                    hashTableName.put(text, result);
                                }
                            }
							else
							{
								result = result +"^"+ text + "^";
								hashTableName.put(text, result);
								//console.log("Empty response was detected for text : '"+text+"'");
							}
                            $(suggestion_Id).html("");
                            showSuggestions(result);
                        }
                    };
                    httpRequest.open("GET", mainUrl, true);
                    httpRequest.send();
                }
                catch (err) {
                    if (XDomainRequest) {
                        // IE8
                        httpRequest = new XDomainRequest();
                        //httpRequest.open('POST', mainUrl, true);
                        //httpRequest.setRequestHeader('Content-type', 'application/json')
                        //httpRequest.send(JSON.stringify(parameters));
                        //httpRequest.onload = function () {
                        //    result = httpRequest.responseText;
                        //    if (typeof (result) != 'undefined' && result != "") {
                        //        result = result + text + "^";
                        //        hashtableAdd.put2(text, result);
                        //        hashtableMR2.put2(text, result);
                        //    }
                        //    else {
                        //        result = text + "^";
                        //    }
                        //    $(suggestion_Id).html("");
                        //    showSuggestions(result);
                        //}

                        httpRequest.onload = function () {
                            var result = httpRequest.responseText;
                            if (result != "") {
                                if (selLang == languageMn) {
                                    text = decodeURIComponent(text);
                                    text = $.trim(text);
                                    result = result + text + "^";
                                    hashTableName.put(text, result);
                                }
                            }
                            $(suggestion_Id).html("");
                            showSuggestions(result);
                        }
                        httpRequest.open("GET", mainUrl);
                        httpRequest.send();
                    }
                }
                //}

                //else { // For Other Browser
                //    //to remove \r from text because IE adds \r with text on typing text with Enter key
                //    var indx = text.indexOf("\r");
                //    if (indx != -1) {
                //        text = text.substring(0, indx);
                //    }
                //    mainUrl = "";
                //    mainUrl = url + "/Suggest";

                //    $.ajax({
                //        type: "POST",
                //        url: mainUrl,
                //        data: JSON.stringify({ iText: text, locale: locale }),
                //        contentType: "application/json;charset=utf-8",
                //        dataType: "json",
                //        crossDomain: true,
                //        success: function (response) {
                //            result = response.d;
                //            if (typeof (result) != 'undefined' && result != "") {
                //                text = $.trim(text);
                //                result = result + text + "^";
                //                hashtableAdd.put2(text, result);
                //                hashtableMR2.put2(text, result);
                //            }
                //            $(suggestion_Id).html("");
                //            showSuggestions(result);
                //        },
                //        error: function (XMLHttpRequest, textStatus, errorThrown) {
                //            //alert(textStatus + '\n' + errorThrown);
                //            return;
                //        }
                //    });
                //}
            }
        }
    }
    catch (ex) {
        //alert(ex.Message);
    }
}

function showSuggestions(suggs) {//This function is added to tokennize the suggestion list and add to div popup ,then display the popup
    try {
        var index = 0;
        suggestionsCount = 0;
        if (suggs) {
            arrSugg = suggs.split('^');
            suggestionsCount = arrSugg.length - 1; //-1 is because there is one more ^ at the last so one index is extra
        }

        //Code for dynamically creating the div tag and corresponding anchor tags in it for displaying suggestion box.//
        var divIdName;
        //Creating anchor tag dynamically & setting in div until suggestions contain comma ie for each suggestion except the last one.//
        if (suggs == "" || suggs == null || suggestionsCount == 0) {
            $(suggestion_Id).hide();
            resetValues();
        }
        else {
            while (index < suggestionsCount) {
                divIdName = arrSugg[index];
                var newdiv = $("<div id='" + divIdName + "' class='unselected'></div>");

                $(newdiv).html("<a style='color:black;' onClick=funClick('" + divIdName + "');>" + divIdName + "</a></center>");
                $(suggestion_Id).append(newdiv);

                index++;
                if (index == 9)
                    break;
            }
            //To Add CDAC-GIST Image//
            var imagediv = $("<div id='imgcg' class='cdacImg'></div>");
            $(imagediv).html("<center><img src='" + transJSPath + "/images/CDAC-GIST.bmp' alt='CDAC Gist' class='imageCDAC'></img></center>");
            $(suggestion_Id).append(imagediv);

            selectSuggestion(0);
            $(suggestion_Id).show();
        }
    }
    catch (ex) {
        //alert(ex.Message);
    }
}

$.fn.setCursorPosition = function (pos) {
    try {
        if ($(this).get(0).setSelectionRange) {
            $(this).get(0).setSelectionRange(pos, pos);
        } else if ($(this).get(0).createTextRange) {
            var range = $(this).get(0).createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    } catch (ex) {
        //alert(ex.Message);
    }
};

function funClick(txt) {
    
	SetText();
	/*try {
        $(element_Id).focus();
        putWord(txt);
        resetValues();
    } catch (ex) {
        //alert(ex.Message);
    }*/
}

function resetValues() {
    try {
        $(suggestion_Id).html("");
        $(suggestion_Id).hide();
        $(content_Id).html("");
        $(popbox_Id).hide();
        suggestions = "";
        result = "";
    } catch (ex) {
        //alert(ex.Message);
    }
}

function setHashTableName() {
    try {
        if (glocale == "hi_in") {
            hashTableName = hashtableHI_IN;
        }
        else if (glocale == "gj_in") {
            hashTableName = hashtableGJ_IN;
        }
        else if (glocale == "mr_in") {
            hashTableName = hashtableMR_IN;
        }
        else if (glocale == "pn_in") {
            hashTableName = hashtablePN_IN;
        }
        else if (glocale == "ml_in") {
            hashTableName = hashtableML_IN;
        }
        else if (glocale == "bn_in") {
            hashTableName = hashtableBN_IN;
        }
    } catch (ex) {
        //alert(ex.Message);
    }
}

/***************************OTHER FUNCTIONS ENDS**************************/
///////////////////////////////////////////////////////////////////////////
/*******************Private methods for internal use only*****************/
//This Portion is coded by Debnarayan Das//
function Hashtable() {
    this.clear = hashtable_clear;
    this.containsKey = hashtable_containsKey;
    this.containsValue = hashtable_containsValue;
    this.get = hashtable_get;
    this.isEmpty = hashtable_isEmpty;
    this.keys = hashtable_keys;
    this.put = hashtable_put;
    this.remove = hashtable_remove;
    this.size = hashtable_size;
    this.toString = hashtable_toString;
    this.values = hashtable_values;
    this.hashtable = new Array();
}
function hashtable_clear() {
    this.hashtable = new Array();
    this.hashtable2 = new Array();
}
function hashtable_containsKey(key) {
    var exists = false;
    for (var i in this.hashtable) {
        if (i == key && this.hashtable[i] != null) {
            exists = true;
            break;
        }
    }
    //alert("exists:" + exists);
    return exists;
}
function hashtable_containsValue(value) {
    var contains = false;
    if (value != null) {
        for (var i in this.hashtable) {
            if (this.hashtable[i] == value) {
                contains = true;
                break;
            }
        }
    }
    return contains;
}
function hashtable_get(key) {
    return this.hashtable[key];
}
function hashtable_isEmpty() {
    return (parseInt(this.size()) == 0) ? true : false;
}
function hashtable_keys() {
    var keys = new Array();
    for (var i in this.hashtable) {
        if (this.hashtable[i] != null)
            keys.push(i);
    }
    return keys;
}
function hashtable_put(key, value) {
    if (key == null || value == null) {
        this.hashtable[key] = value;
    } else {
        this.hashtable[key] = value;
    }
}
function hashtable_size() {
    var size = 0;
    for (var i in this.hashtable) {
        if (this.hashtable[i] != null)
            size++;
    }
    return size;
}
function hashtable_toString() {
    var result = "";
    for (var i in this.hashtable) {
        if (this.hashtable[i] != null)
            result += "{" + i + "},{" + this.hashtable[i] + "}\n";
    }
    return result;
}
function hashtable_values() {
    var values = new Array();
    for (var i in this.hashtable) {
        if (this.hashtable[i] != null)
            values.push(this.hashtable[i]);
    }
    return values;
}
function hashtable_remove(key) {
    var rtn = this.hashtable[key];
    this.hashtable[key] = null;
    return rtn;
}

/*============================Private methods for internal use only=============================*/
//////////////////////////////////////////////////////////////////////////////////////////////////
//Licensed to Government of Maharashtra under CoE. This tool/dll/file is for non-commercial use. For commercial license contact info.gist@cdac.in”. 