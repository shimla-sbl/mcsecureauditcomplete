﻿	//Version 1.0.0.14
	/*===============================================================================================================
	Change path of files as per current relative location.
	===============================================================================================================*/
	//document.write("<link rel='stylesheet' type='text/css' href='CSS/FloatingKeyboard.css' />");
	document.write("<script language='javascript' type='text/javascript' src='"+transJSPath+"/LanguagesArray.js'></script>");
    //document.write("<style type='text/css'>@font-face{font-family: 'Sakal Marathi';font-style: normal;font-weight: normal;src: local('Sakal Marathi'),url('./Fonts/Sakal Marathi_Normal_Ship.ttf')format('truetype');}	</style>");
	//document.write("<style type='text/css'>@font-face{font-family:'Sakal Marathi';font-style:normal;font-weight:normal;src:url('./Fonts/SAKALMA0.eot');}@font-face{font-family:'sakal Marathi';font-style:normal;font-weight:normal;src:url('./Fonts/SAKALMA0.eot?')format('eot'),url('./Fonts/Sakal Marathi Normal_Ship.ttf')format( 'truetype' );}</style>");//corrected font inclusion.
	//document.write("<style type='text/css'>@font-face{font-family:'Sakal Marathi';font-style:normal;font-weight:normal;src:url('./Fonts/SAKALMA0.eot');}@font-face{font-family:'sakal Marathi';font-style:normal;font-weight:normal;src:url('./Fonts/Sakal Marathi Normal_Ship.ttf')format( 'truetype' );}</style>");

	//document.write("<style type='text/css'>@font-face{font-family:'Sakal Marathi';font-style:normal;font-weight:normal;src:url('./Fonts/SAKALMA0.eot?')format('embedded-opentype'),url('./Fonts/Sakal Marathi Normal_Ship.ttf')format( 'truetype' );}</style>");
	

	/*===============================================================================================================*/
	//Direct typing
	
    var g_EnableGlobalTyping = true; 
    var g_CheckEventAttached = null;
    var g_ActiveObj = null;
    var g_CurrentBrowser = getBrowserName();
	var g_EnableTypingOnlyOnKBDOpen = 1;/*0:- ignore,1: enable only if Flt kbd open*/
    var g_CurrentLanguage = "";

    //var g_Debug = null;
	if(window)
    {
        if(window.addEventListener)
        {
            window.addEventListener('focus', eventfocus, true);
            window.addEventListener('load', eventload, true);
            window.addEventListener('click', eventclick, true);
			window.addEventListener('keyup', eventKeyUp,true);
        }
        else if(window.attachEvent)
        {
            // for IE8
            window.attachEvent('onfocus', eventfocus);
            window.attachEvent('onload', eventload);
            window.document.attachEvent('onclick', eventclick);
            window.document.attachEvent('onfocus', eventfocus);
            window.document.attachEvent('onkeyup', eventKeyUp);
            window.document.attachEvent('onmouseover', eventMouseOver);
        }
	}
	function eventMouseOver() {
	    eventclick();
	}
	function eventKeyUp(event) {
	    var e = event ? event : window.event; 
		var num = null;
		if(window.event) // IE
		{
			num = e.keyCode;			
		}
		else if(e.which)// Netscape/Firefox/Opera
		    num = parseInt(e.which);
		if((num == 112)/*F1*/&&(e.altKey))
	    {
            openKeyboard();			
		}

		if((num == 89)/*Y*/&&(e.ctrlKey))
		{
		    if (g_EnableGlobalTyping == true) {
		        g_EnableGlobalTyping = false;
		    }
		    else {
		        g_EnableGlobalTyping = true;
		        eventclick();
		    }
		//	openKeyboard();			
		}
		//if((num == 113)/*F2*/&&(e.altKey))
		/*{
            if (g_EnableGlobalTyping == true) {
                 g_EnableGlobalTyping = false;
            }
            else {
                 g_EnableGlobalTyping = true;
            }
		}*/
	    if (g_CurrentBrowser == "msie") {
	        if (event.keyCode == 9) {
	            eventclick();
	            display(g_ActiveObj);
	        }
	    }
	}
    function eventBlur(evt)
    {
        g_CheckEventAttached = null;
        if(window.addEventListener)
        {
            languageEnabledCtrl.removeEventListener("blur",eventBlur,false);
            languageEnabledCtrl.removeEventListener("keypress",keypressListenerFKBD,false);
            languageEnabledCtrl.removeEventListener("focus",focusListenerFKBD,false);
            languageEnabledCtrl.removeEventListener("keyup",keyupListenerFKBD,false);
            languageEnabledCtrl.removeEventListener("mouseup", mouseupListenerFKBD, false);
            if (g_CurrentBrowser != "opera")
                languageEnabledCtrl.removeEventListener("mouseout", mouseoutListenerFKBD, false);
        }
        else if(window.attachEvent)
        {        
            languageEnabledCtrl.detachEvent("onblur",eventBlur);
            languageEnabledCtrl.detachEvent("onkeypress",keypressListenerFKBD);
            languageEnabledCtrl.detachEvent("onfocus",focusListenerFKBD);												
            languageEnabledCtrl.detachEvent("onkeyup",keyupListenerFKBD);
            languageEnabledCtrl.detachEvent("onmouseup", mouseupListenerFKBD);
            if (g_CurrentBrowser != "opera")
                languageEnabledCtrl.detachEvent("onmouseout", mouseoutListenerFKBD);
        }
    }
    function eventload(evt)
    {
		var g_CurrentLanguage = "";
        setUpdatedLanguageArrays("");
        eventclick();
    }
    function eventfocus()
    {
        eventclick();
    }
       
    function eventclick(ev)
    {
        g_ActiveObj = document.activeElement;
        if (g_ActiveObj && (((g_ActiveObj.tagName == "INPUT") && ((g_ActiveObj.getAttribute("type") == "text") || (g_ActiveObj.getAttribute("type") == "email") || (g_ActiveObj.getAttribute("type") == "search"))) || (g_ActiveObj.tagName == "TEXTAREA")) && (g_ActiveObj.getAttribute("readonly") != "readonly") && /*For IE*/(g_ActiveObj.getAttribute("readonly") != true)) {
            eventAttachment(g_ActiveObj.id);
		}
	}
    function eventAttachment(CtlId)
    {
        try
        {
            if(!g_EnableGlobalTyping)
                return;
            languageEnabledCtrl = g_ActiveObj;
            //languageEnabledCtrl = document.getElementById(CtlId);
            if(/*CtlId && */languageEnabledCtrl )
            {
                if(languageEnabledCtrl.addEventListener)
                {
                    if((g_CheckEventAttached != null) && (g_CheckEventAttached.id == CtlId))
                        return;
                    g_CheckEventAttached = languageEnabledCtrl;
                    languageEnabledCtrl.addEventListener("blur", eventBlur, false);
                    languageEnabledCtrl.addEventListener("keypress",keypressListenerFKBD,false);
                    languageEnabledCtrl.addEventListener("focus",focusListenerFKBD,false);											
                    languageEnabledCtrl.addEventListener("keyup",keyupListenerFKBD,false);
                    languageEnabledCtrl.addEventListener("mouseup",mouseupListenerFKBD,false);
                    if(g_CurrentBrowser!= "opera") 
                        languageEnabledCtrl.addEventListener("mouseout",mouseoutListenerFKBD,false);
                }
                else if(languageEnabledCtrl.attachEvent)
                {
                    if((g_CheckEventAttached != null) && (g_CheckEventAttached.id == CtlId))
                        return;
                    g_CheckEventAttached = languageEnabledCtrl;
                    
                    // for IE8
                    languageEnabledCtrl.attachEvent("onblur",eventBlur);
                    languageEnabledCtrl.attachEvent("onkeypress",keypressListenerFKBD);
                    languageEnabledCtrl.attachEvent("onfocus",focusListenerFKBD);												
                    languageEnabledCtrl.attachEvent("onkeyup",keyupListenerFKBD);
                    languageEnabledCtrl.attachEvent("onmouseup",mouseupListenerFKBD);
                    if(g_CurrentBrowser!= "opera") 
                        languageEnabledCtrl.attachEvent("onmouseout",mouseoutListenerFKBD);
                }
                else
                {
                    languageEnabledCtrl.onkeypress =  fnKeyPress ;
                    languageEnabledCtrl.onfocus = function(event) {languageEnabledCtrl=this; };						
                    languageEnabledCtrl.onkeyup =  function(event){positionOfCursor=caretPos(this);display(this);};
                    languageEnabledCtrl.onmouseup = function(event){positionOfCursor=caretPos(this);display(this);};
                    languageEnabledCtrl.onmouseout = function(event) {positionOfCursor=caretPos(this);display(this);};
                }                
            }
        }
        catch(ex)
        {
            //alert("Event attachment Error:\n"+ex);
        }
        
    }
    
	//End	
    //Global Variables
	var mapArray = "";
	var mapExtArray = "";	
	var mapShiftArray = "";
	var mapShiftExtArray = "";
	var isKBDOpen = false,isShiftBtn = false,isChecked = false;  
    var languageEnabledCtrl = "";
	var btnVal = "";
	var ids;
	var langs;
	var wstr = "";
	var positionOfCursor = 0;
	var selectedText = "";
	var g_start;
    var g_end;    
    var isMozilla;
    var objDiv = null;
    var originalDivHTML = "";
    var DivID = "";
    var over = false;
    var distFlt2BrowserBorder = 20;
    document.onkeydown = KBDShiftDown;
    document.onkeyup = KBDShiftUp;
    init();
	function Launch()
    {
        initializeControls();
        openKeyboard();
    }
    
    function initializeControls()
	{
	    if(document.getElementById('iPluginControlsIdToEnable'))
	    {
            var string = document.getElementById('iPluginControlsIdToEnable').innerHTML; 
            var splitString= string.split('|');
            var txtIds=splitString[0];
            var txtLangs=splitString[1];			
            ids=txtIds.split(',');
            langs=txtLangs.split(',');
		    setUpdatedLanguageArrays(langs,true);
            var termArrayCnt=ids.length;
            for(var i=0; i<termArrayCnt; i++)
            {
                
                if(ids[i] && document.getElementById(ids[i]))
                {
                    if(document.getElementById(ids[i]).addEventListener)
                    {
                        document.getElementById(ids[i]).addEventListener("keypress",keypressListenerFKBD,false);
                        document.getElementById(ids[i]).addEventListener("focus",focusListenerFKBD,false);											
                        document.getElementById(ids[i]).addEventListener("keyup",keyupListenerFKBD,false);
                        document.getElementById(ids[i]).addEventListener("mouseup",mouseupListenerFKBD,false);
                        if(g_CurrentBrowser!= "opera") 
                            document.getElementById(ids[i]).addEventListener("mouseout",mouseoutListenerFKBD,false);
                    }
                    else if(document.getElementById(ids[i]).attachEvent)
                    {
                        // for IE8
                        document.getElementById(ids[i]).attachEvent("onkeypress",keypressListenerFKBD);
                        document.getElementById(ids[i]).attachEvent("onfocus",focusListenerFKBD);												
                        document.getElementById(ids[i]).attachEvent("onkeyup",keyupListenerFKBD);
                        document.getElementById(ids[i]).attachEvent("onmouseup",mouseupListenerFKBD);
                        if(g_CurrentBrowser!= "opera") 
                            document.getElementById(ids[i]).attachEvent("onmouseout",mouseoutListenerFKBD);
                    }
                    else
                    {
                        document.getElementById(ids[i]).onkeypress =  fnKeyPress ;
                        document.getElementById(ids[i]).onfocus = function(event) {languageEnabledCtrl=this; };						
                        document.getElementById(ids[i]).onkeyup =  function(event){positionOfCursor=caretPos(this);display(this);};
                        document.getElementById(ids[i]).onmouseup = function(event){positionOfCursor=caretPos(this);display(this);};
                        document.getElementById(ids[i]).onmouseout = function(event) {positionOfCursor=caretPos(this);display(this);};
                    }                
                }
                else
                {
                    alert("Control with id: "+ids[i]+" is not present on page.");
                }
            }
            languageEnabledCtrl = document.getElementById(ids[0]);
        }
        else
        {
            alert("Unable to find div tag with id 'iPluginControlsIdToEnable'. ");
        }	
	}
	function mouseoutListenerFKBD()//for handling events if selection of text is made beyond text box area.
    {
	    if (g_CurrentBrowser == "msie")
	    //if(document.documentMode<=8)
        {
            if(document.activeElement == languageEnabledCtrl )//Should work for only valid object of control
            {
                positionOfCursor=caretPos(document.activeElement);
                display(document.activeElement);
            }
        }
	    else
	    {
	        if(this == languageEnabledCtrl )//Should work for only valid object of control
	        {
	           positionOfCursor=caretPos(this);
                display(this);
            }
	   }	 
	}
	function focusListenerFKBD()
	{
	    if (g_CurrentBrowser == "msie")
        //if(document.documentMode<=8)
        {
            languageEnabledCtrl=document.activeElement;
        }
        else
        {
            languageEnabledCtrl=this;
        }
	 }
	function keyupListenerFKBD()
	{
	    if (g_CurrentBrowser == "msie")
        //if(document.documentMode<=8)
        {
            positionOfCursor=caretPos(document.activeElement);
            display(document.activeElement);
        }
	    else
	    {
            positionOfCursor=caretPos(this);
            display(this);
	   }	 
	}
	function keypressListenerFKBD(event)
	{   if(isMozilla || (g_CurrentBrowser== "opera") )//Preventing English typing in case of browser other than IE
	    {
			if(!(((document.getElementById('disableIndianLangTyping')!= null) && (document.getElementById('disableIndianLangTyping').checked)) || ((g_EnableTypingOnlyOnKBDOpen == 1 )&& (isKBDOpen == false))||(g_EnableGlobalTyping == false)))//if IL typing has to be disabled or typing only when keyboard is open is activated then to enable English typing, avoid calling below function.
			{
                var e = event ? event : window.event;
                var num;
                num = parseInt(e.which);
                if(!((num == 9)||(num == 0)||(e.ctrlKey)||(num==32)||(num==8)||(num==13)))
				    event.preventDefault();
            }
	    }
	    else if (event.preventDefault) {
	        if (!(((document.getElementById('disableIndianLangTyping') != null) && (document.getElementById('disableIndianLangTyping').checked)) || ((g_EnableTypingOnlyOnKBDOpen == 1) && (isKBDOpen == false)) || (g_EnableGlobalTyping == false)))//if IL typing has to be disabled or typing only when keyboard is open is activated then to enable English typing, avoid calling below function.
	        {
	            var e = event ? event : window.event;
	            var num;
	            num = parseInt(e.which);
	            if (!((num == 9) || (num == 0) || (e.ctrlKey) || (num == 32) || (num == 8) || (num == 13)))
	                event.preventDefault();
	        }
	    }
        return fnKeyPress(event);
	}
	function mouseupListenerFKBD()
	{
	    if (g_CurrentBrowser == "msie")
        //if(document.documentMode<=8)
        {
            positionOfCursor=caretPos(document.activeElement);
            display(document.activeElement);
        }
        else
        {
            positionOfCursor=caretPos(this);
            display(this);
        }	  
	}
	/*
	Function Name      :- openKeyboard()
	File Name          :-Support.js
	Arguments          :- None
	Returns            :- None
	Calls			   :-None
	Called By 		   :- Client
	Description	       :- Delete value from text box.
	If user has made any selection then 
		only that part is deleted,
	else
		Charater at last position is removed from string.		
	*/
	function openKeyboard(currentLang)
    {
		//alert("open"+currentLang);
		if(!document.getElementById("keyBrd"))
	    {
	        alert("Specify launch position of keyboard by including div tag with id 'keyBrd'.");
	        return;
	    }
	    if (currentLang != null) {
	        if (document.getElementById("kbd")) {
	            closeKeyboard();
	        }
	    }
		var elements = document.getElementsByTagName("input");
		var e=0;
		//languageEnabledCtrl = elements[0];
		if(!document.getElementById("kbd"))
		{		
			var ref=document.getElementById("keyBrd");            
			 
			wstr="<div id='kbd' class='keyboard' style='font-family:arial'>"+
			"<div id='id1_content'  align='center'>"+
			"<div id='wdrag' style=' height: 18pt; background-color: #2B4D70;color:#ffffff; border-bottom: 1pt solid #9A998A; margin-left: 6px;margin-right: 6px;margin-bottom: 2px;margin-top: 4px;'>"+
			"<table align='center' id='d' style='font-family: arial; color:#ffffff;font-size:10pt;margin-top:2px' width='100%' cellpadding='0' cellspacing='0' border='0px' >"+
			"<tr><td id='title' width='95%' align='left' style='padding-left: 5pt;cursor:move;height:18px' onmouseover='over=true;' onmouseout='over=false;' >INSCRIPT keyboard"+
			//"<div width='95%' id='FltKbdLang'></div>"+
			"<\/td><td align='right' width='3%' style='padding-right: 5pt;'><a href='javascript:closeKeyboard()' style='text-decoration: none;' title='Close keyboard'>"+
			"<b><img alt = 'Close' src = '"+transJSPath+"/images/close.png'><\/b><\/a>"+
			"<\/td><\/tr><\/table><\/div>"+
			"<table border='0pt' cellpadding='0pt' cellspacing='0pt' width='100%' ><tr><td align='center'>"+
			"<input type='button' id='b1' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b2' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b3' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b4' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b5' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b6' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b7' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b8' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b9' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' >"+
			"<input type='button' id='b10' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b11' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b12' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b13' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='Bksp' value='Bksp' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);' style='WIDTH: 43pt;'>"+
			"<\/td><\/tr><tr><td align='center'>"+
			"<input type='button' id='Tab' value='Tab' class='kbdBtnDisable' style='width: 43pt;' disabled = 'disabled'>"+
			"<input type='button' id='b14' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b15' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b16' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b17' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b18' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b19' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b20' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b21' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b22' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b23' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b24' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b25' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b26' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<\/td><\/tr><tr><td align='center'>"+
			"<input type='button' id='Lock' value='Caps' class='kbdBtnDisable' style='width: 50pt;' disabled = 'disabled'>"+
			"<input type='button' id='b27' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b28' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b29' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b30' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b31' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b32' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b33' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b34' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b35' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b36' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b37' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='Enter' value='Enter' class='kbdEnterBtn' onclick='mapKey(this);'>"+			
			"<\/td><\/tr><tr><td align='center'>"+
			"<input type='button' id='btnShift1' value='Shift' class='kbdBtn' onclick='shiftbtnClicked();' style='width: 59pt;'>"+
			"<input type='button' id='b38' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b39' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b40' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b41' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b42' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b43' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b44' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b45' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b46' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='b47' value='' class='kbdBtn' onclick='mapKey(this);' onmouseover='btnMouseOver(this);' onmouseout='btnMouseOut(this);'>"+
			"<input type='button' id='btnShift2' value='Shift' class='kbdBtn' onclick='shiftbtnClicked();' style='width: 58pt;'>"+
			"<\/td><\/tr><\/table>"+
			"<table border=0><tr><td>"+
			"<a href='http://www.cdac.in' target='_blank'><img class = 'companyLogo' border='0pt' title='C-DAC Logo' alt='C-DAC Logo' src='"+transJSPath+"/images/c-dacLogo.png'><\/a>"+
			"<\/td><td>"+			
			"&nbsp;&nbsp;<input type='button' id='SpaceBar' value='Space Bar' class='kbdSpaceBarBtn' onclick='mapKey(this);'>&nbsp;&nbsp;"+
			"<\/td><td>"+
			"AltGr:<input type='checkbox' id='lchange' onclick='changeLayout();'>" +
			"<\/td><td>"+
			"<a href='http://pune.cdac.in/html/gist/gistidx.aspx' target='_blank'><img class = 'companyLogo' border='0pt' title='C-DAC GIST Logo'  alt='C-DAC GIST Logo' src='"+transJSPath+"/images/gistlogo.png'><\/a>"+
			
			"<\/td><\/tr><\/table>"+
			"<\/div><\/div>";			
			
			if (currentLang != null) {
			    displayFloatingDivAtCurrentObj('keyBrd',this);
			} else {
			    displayFloatingDiv('keyBrd');
			}
			/*var cnt = 0;			
			var string = document.getElementById('iPluginControlsIdToEnable').innerHTML; 
            var splitString= string.split('|');
		    var txtIds=splitString[0];
			var txtLangs=splitString[1];			
			ids=txtIds.split(',');
		    langs=txtLangs.split(',');*/
			
			/*var termArrayCnt=ids.length;
			for(var i=0; i<termArrayCnt; i++)
			{
				if(ids[i])
				{
					//document.getElementById(ids[i]).onkeypress =  fnKeyPress ;
					//document.getElementById(ids[i]).onfocus = function(event) {languageEnabledCtrl=this; };	
                    if(document.getElementById(ids[i]).addEventListener)
                    {
	                    document.getElementById(ids[i]).addEventListener("keypress",keypressListenerFKBD,false);
	                    document.getElementById(ids[i]).addEventListener("focus",focusListenerFKBD,false);											
	                    document.getElementById(ids[i]).addEventListener("keyup",keyupListenerFKBD,false);
	                    document.getElementById(ids[i]).addEventListener("mouseup",mouseupListenerFKBD,false);
                    }
                    else if(document.getElementById(ids[i]).attachEvent)
                    {
	                    // for IE8
	                    document.getElementById(ids[i]).attachEvent("onkeypress",keypressListenerFKBD);
	                    document.getElementById(ids[i]).attachEvent("onfocus",focusListenerFKBD);												
	                    document.getElementById(ids[i]).attachEvent("onkeyup",keyupListenerFKBD);
	                    document.getElementById(ids[i]).attachEvent("onmouseup",mouseupListenerFKBD);
                    }
                    else
                    {
	                    document.getElementById(ids[i]).onkeypress =  fnKeyPress ;
	                    document.getElementById(ids[i]).onfocus = function(event) {languageEnabledCtrl=this; };						
	                    document.getElementById(ids[i]).onkeyup =  function(event){positionOfCursor=caretPos(this);display(this);};
	                    document.getElementById(ids[i]).onmouseup = function(event){positionOfCursor=caretPos(this);display(this);};				
                    }	
				}
			}*/	
			
		    /*var  mapArray = langs[0];*/
			if (currentLang != null) {
			    g_CurrentLanguage = currentLang;
			    setUpdatedLanguageArrays(currentLang, true);
			}
			else {
			    setUpdatedLanguageArrays(g_CurrentLanguage, true);
			}
			//document.getElementById('FltKbdLang').innerHTML = mapArray;//For display of language
		}
		else
		{
			
		    	closeKeyboard();
		}
    }
	
	/*
	Function Name      :- deleteValue()
	File Name          :-Support.js
	Arguments          :- None
	Returns            :- None
	Calls	:-None
	Called By 	:- mapKey()
	Description	:- Delete value from text box.
	If user has made any selection then 
		only that part is deleted,
	else
		Charater at last position is removed from string.		
	*/
	function deleteValue()
	{	    
	    var languageStringLength = languageEnabledCtrl.value.length;
	    if(selectedText != "")
		{
		   	if(languageEnabledCtrl.createTextRange)
			{
				var start = getSelectionStart(languageEnabledCtrl);
				var end = getSelectionEnd(languageEnabledCtrl);
				languageEnabledCtrl.value = languageEnabledCtrl.value.substr(0, start) + languageEnabledCtrl.value.substr(end, languageStringLength);
				//if()
				positionOfCursor = start;
				//console.log("deleteValue() called2:start:" + start + "|end:" + end + "|First Part:" + languageEnabledCtrl.value.substr(0, start) + "| Rest Part:" + languageEnabledCtrl.value.substr(end, languageStringLength) + "\n||languageEnabledCtrl.value :" + languageEnabledCtrl.value);
				if (((languageEnabledCtrl.tagName == "TEXTAREA") && (g_CurrentBrowser == "msie") && (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g)))) {
				    start = start - languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g).length;

				    setCursorPosition(languageEnabledCtrl, start);
				    //console.log("deleteValue() called3:start" + start + "||positionOfCursor:" + positionOfCursor + "|\n|First part:" + languageEnabledCtrl.value.substr(0, start)+ "|Rest part" + languageEnabledCtrl.value.substr(end, languageStringLength));

				}
                else
				    setCursorPosition(languageEnabledCtrl , positionOfCursor);
				positionOfCursor = caretPos(languageEnabledCtrl);
				selectedText = "";        
			}
			else if(languageEnabledCtrl.selectionStart || (languageEnabledCtrl.selectionStart =='0'))
			{
				languageEnabledCtrl.value = languageEnabledCtrl.value.substr(0,g_start) + languageEnabledCtrl.value.substr(g_end,languageStringLength);
				setCursorPosition(languageEnabledCtrl,positionOfCursor);
				positionOfCursor = caretPos(languageEnabledCtrl);
				selectedText = "";
			}    		
		}
		else
		{   
		    if(positionOfCursor != 0)
		    {
		        var firstPart = languageEnabledCtrl.value.slice(0, positionOfCursor - 1);
		        var restPart = languageEnabledCtrl.value.slice(positionOfCursor, languageStringLength);
		        if (((languageEnabledCtrl.tagName == "TEXTAREA") && (g_CurrentBrowser == "msie") && (firstPart.charAt(firstPart.length-1)=='\r'))) {
		            firstPart = firstPart.slice(0, firstPart.length - 1);
		            positionOfCursor = positionOfCursor - 1;
		            //console.log("deleteValue() called:New line handling:" + firstPart + "|restPart" + restPart + "|positionOfCursor:" + positionOfCursor);

		        }
		        else if (((languageEnabledCtrl.tagName == "TEXTAREA") && (g_CurrentBrowser == "msie") && (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g)))) {
		            //console.log("deleteValue() called: Count Other 1:" + (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g)).length);

		            positionOfCursor = positionOfCursor - languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g).length;
		            languageEnabledCtrl.value = firstPart + restPart;
		            setCursorPosition(languageEnabledCtrl, positionOfCursor-1);
		            //positionOfCursor = positionOfCursor - 1;

		            //console.log("deleteValue() called: Count Other 2:" + firstPart + "|restPart" + restPart + "|positionOfCursor:" + positionOfCursor + "|start" + start + "");
		            return;

		        }

		        //console.log("deleteValue() called:firstPart:" + firstPart + "|restPart" + restPart);

		        languageEnabledCtrl.value = firstPart + restPart;
		        setCursorPosition(languageEnabledCtrl, positionOfCursor - 1);
		        positionOfCursor = positionOfCursor - 1;
            }
            else
            {
                setCursorPosition(languageEnabledCtrl ,positionOfCursor);
            }		   
		}
	}
	/*
	Function Name      :- insertValue()
	File Name          :-Support.js
	Arguments          :- None
	Returns            :- None
	Calls	:-None
	Called By 	:- mapKey()
	Description	:- Insert value into text box.
	If user has made any selection then 
		only that part is deleted and charater at last position is inserted into string.
	else
		Charater at last position is inserted into string.		
	*/
	function insertValue(KbdCheckFlag/*Flag will be set to true when data comes from HTML keyboard*/)
	{
	      
        if(((document.getElementById('disableIndianLangTyping')!= null) && (document.getElementById('disableIndianLangTyping').checked))||(g_EnableGlobalTyping == false))
        {
            alert("Indian Language inputting is disabled.")
            return ;
        }
        if ((btnVal != "‏")&&(btnVal != "‎"))
		{
		    if(btnVal == '\uE002')
			{
				btnVal='\u200C';
			}
			else if(btnVal == '\uE003')
			{
				btnVal='\u200D';
			}
			if(btnVal=='Space Bar')
			{
				btnVal=' ';
			}
			if ((btnVal == 'Enter') && (g_CurrentBrowser == "msie") && (!isBrowserAboveIE9())) {
			    btnVal = '\r\n';
			}
			else if(btnVal=='Enter')
			{
				btnVal='\n';
			}
			if(languageEnabledCtrl == "")
			{
			    alert("Click on field where you want to type.");
			    /*var elements = document.getElementsByTagName("input");
		        languageEnabledCtrl = elements[0];
		        languageEnabledCtrl.focus();*/
			    return ; 	
            }
			var languageStringLength = languageEnabledCtrl.value.length;
		    var cursorPosition = getSelectionStart(languageEnabledCtrl);
		    if ((KbdCheckFlag != true) && ((languageEnabledCtrl.tagName == "TEXTAREA") && (g_CurrentBrowser == "msie")))
		    {
				positionOfCursor = cursorPosition;
			}			
		    if(selectedText != "")
			{
			    if(languageEnabledCtrl.createTextRange)
				{
					var start = getSelectionStart(languageEnabledCtrl);
					var end = getSelectionEnd(languageEnabledCtrl);
					languageEnabledCtrl.value = languageEnabledCtrl.value.substr(0,start) + btnVal + languageEnabledCtrl.value.substr(end,languageStringLength);
			        //setCursorPosition(languageEnabledCtrl, positionOfCursor + btnVal.length);//Commented by AnupK on 23-11-2012 to correct the typing support in case of TextArea.
					if (((languageEnabledCtrl.tagName == "TEXTAREA") && (g_CurrentBrowser == "msie") && (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g)) && (KbdCheckFlag != true))) {
					    start = start - languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g).length;;
					}
					
					if (((languageEnabledCtrl.tagName == "TEXTAREA") && (g_CurrentBrowser == "msie") && (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g)) && (KbdCheckFlag == true))) {
					    setCursorPosition(languageEnabledCtrl, start + btnVal.length - languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g).length);
					    positionOfCursor = caretPos(languageEnabledCtrl) ;

					    //console.log("insertValue()With selection 0 called:start:" + start + "|End" + end + "|positionOfCursor :" + positionOfCursor);
					}
					else {
					    setCursorPosition(languageEnabledCtrl, start + btnVal.length);
					    positionOfCursor = caretPos(languageEnabledCtrl);
					}
                    //console.log("insertValue()With selection called:start:" + start + "|End" + end + "|positionOfCursor :" + positionOfCursor);
                    selectedText = "";
				}
				else if(languageEnabledCtrl.selectionStart || (languageEnabledCtrl.selectionStart =='0'))
				{
					languageEnabledCtrl.value = languageEnabledCtrl.value.substr(0,g_start) + btnVal + languageEnabledCtrl.value.substr(g_end,languageStringLength);
					setCursorPosition(languageEnabledCtrl,positionOfCursor + 1);
					positionOfCursor = caretPos(languageEnabledCtrl);
					selectedText = "";
				}
			}
			else
		    {
		        //console.log("insertValue() called:Prev:" + languageEnabledCtrl.value);
		        languageEnabledCtrl.value = languageEnabledCtrl.value.replace(new RegExp('ab', 'g'), '\r\n');
		        //languageEnabledCtrl.value = languageEnabledCtrl.value.replace("ab", "\r\n");
		        //console.log("insertValue() called:After:" + languageEnabledCtrl.value);
                //AK
		        languageEnabledCtrl.value = languageEnabledCtrl.value.substr(0, positionOfCursor) + btnVal + languageEnabledCtrl.value.substr(positionOfCursor, languageStringLength);
		        if (((languageEnabledCtrl.tagName == "TEXTAREA") && (g_CurrentBrowser == "msie") && (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g)) && (KbdCheckFlag == true))) {
		            //positionOfCursor = positionOfCursor + (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g).length - 1 );
		            positionOfCursor = positionOfCursor + btnVal.length;
		            setCursorPosition(languageEnabledCtrl, positionOfCursor - (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g).length) /*+ btnVal.length*/);
		            //console.log("insertValue() called:KBD typing:" + positionOfCursor);

		        }
                else if (((languageEnabledCtrl.tagName == "TEXTAREA") && (g_CurrentBrowser == "msie") && (languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g)))) {
		            //console.log("insertValue() called:count:" + languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g).length);

		            positionOfCursor = positionOfCursor - languageEnabledCtrl.value.substr(0, positionOfCursor).match(/\r\n/g).length;
		            setCursorPosition(languageEnabledCtrl, positionOfCursor + btnVal.length);
		            positionOfCursor = positionOfCursor + btnVal.length;
		        }
		        else {
		            setCursorPosition(languageEnabledCtrl, positionOfCursor + btnVal.length);
		            positionOfCursor = positionOfCursor + btnVal.length;
		        }
            }
		}
	}
	
	/*
	Function Name      :- fnKeyPress(keyevnt)
	File Name          :-Support.js
	Arguments          :- event
	Returns            :- true or false
	Calls	:-btnMouseOver(),shiftbtnClicked(),mapKey(),btnMouseOut()
	Called By 	:-onKeyPress event 
	Description	:- according to ASCII value of key is compared with AsciiArray to get position of character,then it is maped into 
		mapArray[] or mapShiftArray[],After that processing is done to sent character to text box.
	*/
	function fnKeyPress(keyevnt)
    {
             
	   if(((document.getElementById('disableIndianLangTyping')!= null) && (document.getElementById('disableIndianLangTyping').checked)) || ((g_EnableTypingOnlyOnKBDOpen == 1 )&& (isKBDOpen == false))||(g_EnableGlobalTyping == false))
           return true;
	 	var e = keyevnt ? keyevnt : window.event;
		var num;
		
		if(window.event) // IE
		{
			num = e.keyCode;			
		}
		else if(e.which)// Netscape/Firefox/Opera
			num=parseInt(e.which);
		if(e.ctrlKey)//For handling Ctrl key + events
			return true;
		if(num==32)//For handling SpaceBar
			return true;
		var asciiArrayLength=asciiArray.length;
		
		/*var string =document.getElementById('iPluginControlsIdToEnable').innerHTML; 
		var splitString= string.split('|');
		var txtIds=splitString[0];
		var txtLangs=splitString[1];			
		ids=txtIds.split(',');
		langs=txtLangs.split(',');
		var  mapArray1= langs[0];
			
		if( mapArray == 'Hindi')
		{
			mapArray=mapHindiArray;
			mapExtArray=mapHindiExtArray;
			
			mapShiftArray=mapHindiShiftArray;
			mapShiftExtArray=mapHindiShiftExtArray;
		}*/
		for (var cnt = 0; cnt <asciiArrayLength;  cnt++)
		{
			if(String.fromCharCode(num) == asciiArray[cnt])
	        {
			   				  
				if((cnt > 47) && (e.shiftKey==true))
				{ 
					if(isChecked)
					{
					   btnVal = mapShiftExtArray[cnt-47];						   
					   insertValue();
					   isShiftBtn = true;
					}
					else
					{
						btnVal=mapShiftArray[cnt-47];	                        						
					    insertValue();
						isShiftBtn = true;
					}	
				}
				else if((cnt < 47) && (e.shiftKey==false))
				{
				    if(isChecked)
					{
					    btnVal=mapExtArray[cnt];					   
						insertValue();
						isShiftBtn = false;
					}
					else
					{
						btnVal=mapArray[cnt];					   
						insertValue();
						isShiftBtn = false;
					}
				}
				else//FOR HANDLING CAPS LOCK
				{
					if((cnt > 47))
					{
						if(isChecked)
						{
						    btnVal=mapExtArray[cnt-47];					   
							insertValue();
							isShiftBtn = false;
						}
						else
						{
							btnVal=mapArray[cnt-47];					   
							insertValue();
							isShiftBtn = false;
						}
					}
					else if((cnt < 47))
					{
						if(isChecked)
						{
						    btnVal=mapShiftExtArray[cnt];					
					        insertValue();
					        isShiftBtn = true;
						}
						else
						{
						    btnVal=mapShiftArray[cnt];
					        insertValue();
					        isShiftBtn = true;
						}
					}
				}
				return false;
			}
		}
	}

// Added For Typing Using Mouse 
	function btnMouseOver(ref)
    {
        ref.className = "btnMouseOver"; 
    }
    function btnMouseOut(ref)
    {
        ref.className = "btnMouseOut";
	}
    function changeLayout()
    {	
	    /*var string =document.getElementById('iPluginControlsIdToEnable').innerHTML; 
		var splitString= string.split('|');
		var txtIds=splitString[0];
		var txtLangs=splitString[1];			
		ids=txtIds.split(',');
		langs=txtLangs.split(',');
		var  mapArray1= langs[0];			
		//setUpdatedLanguageArrays(mapArray1);
		if( mapArray == 'Hindi')
		{
			mapArray=mapHindiArray;
			mapExtArray=mapHindiExtArray;
			
			mapShiftArray=mapHindiShiftArray;
			mapShiftExtArray=mapHindiShiftExtArray;
		}*/
		if(isKBDOpen)
     	{
			isChecked= document.getElementById("lchange").checked;
			
			if(isChecked)
			{
				for(cnt=0;cnt<mapExtArray.length;cnt++)
				{
					document.getElementById("b"+(cnt+1)).value=mapExtArray[cnt];				
				}	  
			}	
			if(isShiftBtn && isChecked)
			{
			
			 	 for(cnt=0;cnt<mapShiftExtArray.length;cnt++)
				{
					document.getElementById("b"+(cnt+1)).value=mapShiftExtArray[cnt];
					
				}
				document.getElementById("btnShift1").className = "shiftBtnDown";
				document.getElementById("btnShift2").className = "shiftBtnDown";
				isShiftBtn = true;
			}
			else if(!isShiftBtn && !isChecked)
			{
				 
				for(cnt=0;cnt<mapArray.length;cnt++)
				{
					document.getElementById("b"+(cnt+1)).value=mapArray[cnt];
				}
				document.getElementById("btnShift1").className = "shiftBtnUp";
				document.getElementById("btnShift2").className = "shiftBtnUp";
				isShiftBtn = false;			
			}	    
			else if(isShiftBtn && !isChecked)
			{
				
				for(cnt=0;cnt<mapShiftArray.length;cnt++)
				{
					document.getElementById("b"+(cnt+1)).value=mapShiftArray[cnt];
				}
				document.getElementById("btnShift1").className = "shiftBtnDown";
				document.getElementById("btnShift2").className = "shiftBtnDown";
				isShiftBtn = true;			
			}
		}
		 
	 }
	function shiftbtnClicked()
    {
	    isChecked= document.getElementById("lchange").checked;
		//document.getElementById("SpaceBar").value =(mapArray);
        /*var string =document.getElementById('iPluginControlsIdToEnable').innerHTML; 
		var splitString= string.split('|');
		var txtIds=splitString[0];
		var txtLangs=splitString[1];			
		ids=txtIds.split(',');
		langs=txtLangs.split(',');
		var  mapArray1= langs[0];			
		//setUpdatedLanguageArrays(mapArray1);
		if( mapArray == 'Hindi')
		{
			mapArray=mapHindiArray;
			mapExtArray=mapHindiExtArray;
			
			mapShiftArray=mapHindiShiftArray;
			mapShiftExtArray=mapHindiShiftExtArray
		}*/
		if(isKBDOpen && !isShiftBtn && isChecked==true)
		{
			for(cnt=0;cnt<mapShiftExtArray.length;cnt++)
			{
				document.getElementById("b"+(cnt+1)).value=mapShiftExtArray[cnt];
				
			}
			document.getElementById("btnShift1").className = "shiftBtnDown";
			document.getElementById("btnShift2").className = "shiftBtnDown";
			isShiftBtn = true;			  
		}
		else if(isKBDOpen && isShiftBtn && isChecked==true)
		{
			for(cnt=0;cnt<mapExtArray.length;cnt++)
			{
				document.getElementById("b"+(cnt+1)).value=mapExtArray[cnt];
				
			}
			document.getElementById("btnShift1").className = "shiftBtnUp";
			document.getElementById("btnShift2").className = "shiftBtnUp";
			isShiftBtn = false;			  
		}
       	
		else if(isKBDOpen && !isShiftBtn)
		{
			for(cnt=0;cnt<mapShiftArray.length;cnt++)
			{
				document.getElementById("b"+(cnt+1)).value=mapShiftArray[cnt];
			}
			document.getElementById("btnShift1").className = "shiftBtnDown";
			document.getElementById("btnShift2").className = "shiftBtnDown";
			isShiftBtn = true;			
		}
		else if(isKBDOpen && isShiftBtn)
		{
			for(cnt=0;cnt<mapArray.length;cnt++)
			{
				document.getElementById("b"+(cnt+1)).value=mapArray[cnt];
			}
			document.getElementById("btnShift1").className = "shiftBtnUp";
			document.getElementById("btnShift2").className = "shiftBtnUp";
			isShiftBtn = false;			
		}
    }
	
    function closeKeyboard()
    {

	KeyBrdOpenFlag = false;
		var keyBoard = document.getElementById("keyBrd");
		var kbd = document.getElementById("kbd");
		
		/*var string =document.getElementById('iPluginControlsIdToEnable').innerHTML; 
        var splitString= string.split('|');
        var txtIds=splitString[0];
        var txtLangs=splitString[1];			
        ids=txtIds.split(',');
        langs=txtLangs.split(',');
        var termArrayCnt=ids.length;*/
        /*for(var i=0; i<termArrayCnt; i++)
        {
            if(ids[i])
            {	
				document.getElementById(ids[i]).onkeypress =  "" ;
				document.getElementById(ids[i]).onfocus ="";						
				document.getElementById(ids[i]).onkeyup =enableAjax;
				document.getElementById(ids[i]).onmouseup = "";
			}
		}*/
		keyBoard.removeChild(kbd); 
		isKBDOpen = false;
		isShiftBtn = false;
		hiddenFloatingDiv("keyBrd");
		isChecked = false;
		if(languageEnabledCtrl != "")
		    languageEnabledCtrl.focus();
    }
	    
	function KBDShiftDown(keyevnt)
    { 
	    if(isKBDOpen)
		isChecked= document.getElementById("lchange").checked; 
		
		/*var string =document.getElementById('iPluginControlsIdToEnable').innerHTML; 
		var splitString= string.split('|');
		var txtIds=splitString[0];
		var txtLangs=splitString[1];			
		ids=txtIds.split(',');
		langs=txtLangs.split(',');
		var  mapArray1= langs[0];
		//setUpdatedLanguageArrays(mapArray1);
		if( mapArray == 'Hindi')
		{
			mapArray=mapHindiArray;
			mapExtArray=mapHindiExtArray;
			
			mapShiftArray=mapHindiShiftArray;
			mapShiftExtArray=mapHindiShiftExtArray;
		}*/		
		var e = keyevnt ? keyevnt : window.event;
		
		
		if(e.shiftKey && isKBDOpen)
		{
		   for(cnt=0;cnt<mapShiftArray.length;cnt++)
			{
			     document.getElementById("b"+(cnt+1)).value=mapShiftArray[cnt];				
			}
			document.getElementById("btnShift1").className = "shiftBtnDown";
			document.getElementById("btnShift2").className = "shiftBtnDown";
			isShiftBtn = true;
		}
		if(e.shiftKey && isKBDOpen && isChecked)
		{
			for(cnt=0;cnt<mapShiftExtArray.length;cnt++)
			{
				document.getElementById("b"+(cnt+1)).value=mapShiftExtArray[cnt];				
			}
			document.getElementById("btnShift1").className = "shiftBtnDown";
			document.getElementById("btnShift2").className = "shiftBtnDown";
			isShiftBtn = true;
		}		
    }
    
    function KBDShiftUp(keyevnt)
    {
	    if(isKBDOpen)
		    isChecked= document.getElementById("lchange").checked; 
        
		/*var string =document.getElementById('iPluginControlsIdToEnable').innerHTML; 
		var splitString= string.split('|');
		var txtIds=splitString[0];
		var txtLangs=splitString[1];			
		ids=txtIds.split(',');
		langs=txtLangs.split(',');
		var  mapArray1= langs[0];			
		//setUpdatedLanguageArrays(mapArray1);
		if( mapArray == 'Hindi')
		{
			mapArray=mapHindiArray;
			mapExtArray=mapHindiExtArray;
			
			mapShiftArray=mapHindiShiftArray;
			mapShiftExtArray=mapHindiShiftExtArray;
		}*/		
		var e = keyevnt ? keyevnt : window.event;

		if(!e.shiftKey && isKBDOpen)
		{
			for(cnt=0;cnt<mapArray.length;cnt++)
			{
				document.getElementById("b"+(cnt+1)).value=mapArray[cnt];
			}
			document.getElementById("btnShift1").className = "shiftBtnUp";
			document.getElementById("btnShift2").className = "shiftBtnUp";
			isShiftBtn = false;
		}
		if(isKBDOpen && isChecked && !e.shiftKey)
		{
		    for(cnt=0;cnt<mapExtArray.length;cnt++)
			{
				document.getElementById("b"+(cnt+1)).value=mapExtArray[cnt];
			}
			document.getElementById("btnShift1").className = "shiftBtnUp";
			document.getElementById("btnShift2").className = "shiftBtnUp";
			isShiftBtn = false;
		}
		
    } 
    function mapKey(btnRef)
    {   
		btnVal = btnRef.value;
		switch (btnVal)
		{
			case "Bksp":
				deleteValue();
				break;  
			default:
				insertValue(true);
				break;
		}						
    }
    ///////////////////////////////////////Floating KBD/////////////////////////////////////////////////
    function displayFloatingDiv(divId) 
    {
		var divPos = findAbsolutePosition(document.getElementById("keyBrd"));
        DivID = divId;
        if (originalDivHTML == "")
            originalDivHTML = document.getElementById(divId).innerHTML;
	    document.getElementById(divId).innerHTML = wstr;
	    document.getElementById(divId).className = 'dimming';
	    document.getElementById(divId).style.visibility = "visible";
	    if(divPos[0]< distFlt2BrowserBorder)
	        divPos[0] = distFlt2BrowserBorder;
		document.getElementById(divId).style.left = divPos[0]+ 'px';
        document.getElementById(divId).style.top = divPos[1] + 'px';		
    }
	
	function displayFloatingDivAtCurrentObj(divId,currentObj) 
    {
		var divPos = findAbsolutePosition(currentObj);
        DivID = divId;
        if (originalDivHTML == "")
            originalDivHTML = document.getElementById(divId).innerHTML;
	    document.getElementById(divId).innerHTML = wstr;
	    document.getElementById(divId).className = 'dimming';
	    document.getElementById(divId).style.visibility = "visible";
	    if(divPos[0]< distFlt2BrowserBorder)
	        divPos[0] = distFlt2BrowserBorder;
		document.getElementById(divId).style.left = divPos[0]+ 'px';
        document.getElementById(divId).style.top = divPos[1] + 'px';		
    }
	function findAbsolutePosition(obj) 
	{
	    var curleft = curtop = 0;
	    if (obj.offsetParent) {
		    do {
			    curleft += obj.offsetLeft;
			    curtop += obj.offsetTop;
		    } while (obj = obj.offsetParent);
	    }
        return [curleft,curtop];
        //returns an array
    }
    function hiddenFloatingDiv(divId) 
    {
	    document.getElementById(divId).innerHTML = originalDivHTML;
	    document.getElementById(divId).style.visibility='hidden';
	    DivID = "";	
    }

    function MouseDown(e) 
    {
        if (over)
        {
            if (isMozilla) {
                objDiv = document.getElementById(DivID);
                X = e.layerX;
                Y = e.layerY;
                return false;
            }
            else {
                objDiv = document.getElementById(DivID);
                objDiv = objDiv.style;
                X = event.offsetX;
                Y = event.offsetY;
            }
        }
    }

    function MouseMove(e)
    {
    var t_top = 0;
    var t_left = 0;
    if (objDiv){
       var scrOfX = 0, scrOfY = 0;
        if( typeof( window.pageYOffset ) == 'number' ) {
            //Netscape compliant
            scrOfY = window.pageYOffset;
            scrOfX = window.pageXOffset;
        } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
            //DOM compliant
            scrOfY = document.body.scrollTop;
            scrOfX = document.body.scrollLeft;
        } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
            //IE6 standards compliant mode
            scrOfY = document.documentElement.scrollTop;
            scrOfX = document.documentElement.scrollLeft;
        }
        var winW = 0, winH = 0;
        if (document.body && document.body.offsetWidth) {
            //Internet Explorer (backward-compatibility mode)
            winW = document.body.offsetWidth;
            winH = document.body.offsetHeight;
        }
        if (document.compatMode=='CSS1Compat' && document.documentElement && document.documentElement.offsetWidth ) {
            //Internet Explorer (standards mode, document.compatMode=='CSS1Compat')
            winW = document.documentElement.offsetWidth;
            winH = document.documentElement.offsetHeight;
        }
        if (window.innerWidth && window.innerHeight) {
            //Other browsers . as well as IE9 (standards mode)
            winW = window.innerWidth;
            winH = window.innerHeight;
        }

        winW = winW + scrOfX;
        winH = winH + scrOfY;

        var DivHeight = document.getElementById('keyBrd').offsetHeight + distFlt2BrowserBorder ;
        var DivWidth = document.getElementById('keyBrd').offsetWidth + distFlt2BrowserBorder;

        if (isMozilla){
            t_top = (e.pageY-Y) ;
            t_left = (e.pageX-X) ;
            if(t_top < ( 0 + distFlt2BrowserBorder ) )
                t_top = 0 + distFlt2BrowserBorder;
            if( t_left < ( 0 + distFlt2BrowserBorder ) )
                t_left = 0 + distFlt2BrowserBorder;
            if(t_top > (winH - DivHeight) )
                t_top = winH - DivHeight ;
            if( t_left > (winW - DivWidth) )
                t_left = winW - DivWidth;
            objDiv.style.top = t_top + 'px';
            objDiv.style.left = t_left + 'px';
            return false;
        }
        else
        {
            //var debug = document.getElementById('TextArea1');
            //debug.value += document.body.scrollLeft+ "||"+document.documentElement.scrollTop+"\n";
            //t_left = (event.clientX-X + document.body.scrollLeft);
            //t_top = ( event.clientY-Y + document.body.scrollTop);
            t_left = (event.clientX-X + document.documentElement.scrollLeft);
            t_top = ( event.clientY-Y + document.documentElement.scrollTop);
            if(t_top < ( 0 + distFlt2BrowserBorder ) )
                t_top = 0 + distFlt2BrowserBorder;
            if( t_left < ( 0 + distFlt2BrowserBorder ) )
                t_left = 0 + distFlt2BrowserBorder;
            if(t_top > (winH - DivHeight) )
                t_top = winH - DivHeight ;
            if( t_left > (winW - DivWidth) )
                t_left = winW - DivWidth;
            objDiv.pixelLeft = t_left;
            objDiv.pixelTop = t_top;
            return false;
        }
    }
}
function MouseMove1(e) 
    {
        var t_top = 0;
        var t_left = 0;
        
        if (objDiv)
        {
            if (isMozilla) 
            {
                t_top = (e.pageY-Y) ;
                t_left = (e.pageX-X) ;
                if(t_top < 0 )
                    t_top = 0 ;
                if( t_left < 0 )
                    t_left = 0;
                objDiv.style.top = t_top + 'px';
                objDiv.style.left = t_left + 'px';
                return false;
            }
            else 
            {
                t_left = (event.clientX-X + document.body.scrollLeft);
                t_top = ( event.clientY-Y + document.body.scrollTop);
                if(t_top < 0 )
                    t_top = 0 ;
                if( t_left < 0 )
                    t_left = 0;
                objDiv.pixelLeft = t_left;
                objDiv.pixelTop = t_top;
                return false;
            }
        }
    }
    function MouseUp() 
    {
        objDiv = null;
    }
    function init()
    {
        // check browser
        if(g_CurrentBrowser== "opera")//Setting for handling of mouse move in case of Opera
        {
            isMozilla = 0;
        }
        else
        {
            isMozilla = (document.all) ? 0 : 1;
        }
        if (isMozilla) 
        {
            document.captureEvents(Event.MOUSEDOWN | Event.MOUSEMOVE | Event.MOUSEUP);
        }
        document.onmousedown = MouseDown;
        document.onmousemove = MouseMove;
        document.onmouseup = MouseUp;
    }

    /////////////////////////////////////Selection mechanism /////////////////////

    function caretPos(txtRef)
    {
	try{
        var i = txtRef.value.length;
            if (txtRef.createTextRange && !isBrowserAboveIE9()) {
                if (txtRef.tagName == "TEXTAREA") {//Complete if condition added by AnupK on 23-11-2012 For handling typing support in TextArea as caret position retrieved is incorrect.
                    //create a range object and save off it's text
                    var objRange = document.selection.createRange();
                    var sOldRange = objRange.text;
                    if (sOldRange == "") {
                        //set this string to a small string that will not normally be encountered
                        var sWeirdString = '#%~';
                    
                        //insert the weirdstring where the cursor is at
                        objRange.text = sOldRange + sWeirdString;
                        objRange.moveStart('character', (0 - sOldRange.length - sWeirdString.length));

                        //save off the new string with the weirdstring in it
                        var sNewText = txtRef.value;
                        //set the actual text value back to how it was
                        objRange.text = sOldRange;

                        //look through the new string we saved off and find the location of
                        //the weirdstring that was inserted and return that value
                        for (i = 0; i <= sNewText.length; i++) {
                            var sTemp = sNewText.substring(i, i + sWeirdString.length);
                            if (sTemp == sWeirdString) {
                                var cursorPos = (i - sOldRange.length);
                                return cursorPos;
                            }
                        }
                    }
                }
                else {
                    theCaret = document.selection.createRange().duplicate();
                    while (theCaret.parentElement() == txtRef
                    && theCaret.move("character", 1) == 1)--i;
                    return i == txtRef.value.length + 1 ? -1 : i;
                }
            }
            else if (txtRef.selectionStart || (txtRef.selectionStart == '0')) {
                return txtRef.selectionStart;
            }  
		}
		catch(e)
		{
			//alert("caretPos Error:"+e)
		}
    }
    /*****************************SetCursorPosition*******************************************/
    function setCursorPosition(field, pos) 
    {
       if (field.createTextRange) {
          var range = field.createTextRange();
          range.collapse(true);
          range.moveEnd('character', pos);
          range.moveStart('character', pos);
          range.select();
       } else if (field.selectionStart || field.selectionStart=='0') {	
          field.setSelectionRange(pos,pos);
          field.focus();//Setting focus of cursor in case of FF and Opera
       }
    } 
    function getSelectionStart(o)
    {
        if (o.createTextRange && !isBrowserAboveIE9()) {
            if (o.tagName == "TEXTAREA") {//Complete if condition added by AnupK on 23-11-2012 For handling typing support in TextArea and to return currect value of start of selection.
                if ((selectedText != "")) {
                        //console.log("Legth getSelectionStart() called:Caret Pos:" + caretPos(o));

                    var bm = document.selection.createRange().duplicate().getBookmark();
                    //o.value = o.value.replace(/\r\n/g, "ab");
                    var sel = o.createTextRange().duplicate();
                    sel.moveToBookmark(bm);
                    var sleft = o.createTextRange().duplicate();
                    sleft.collapse(true);
                    sleft.setEndPoint("EndToStart", sel);
                    o.selectionStart = sleft.text.length;
                    return o.selectionStart;
                }
                else if ((caretPos(o) != o.value.length))
                {

                    var start = 0, end = 0, normalizedValue, range, textInputRange, len, endRange;

                    //if (typeof o.selectionStart == "number" && typeof o.selectionEnd == "number") {

                    //    console.log("Ruchi 0 getSelectionStart() called:TYPE:" + document.selection.type + "||typeDetail:" + document.selection.typeDetail);
                    //    start = o.selectionStart;
                    //    end = o.selectionEnd;
                    //} else 
                    {
                        // document.selection.empty();
                        //console.log("Ruchi 1 getSelectionStart() called:TYPE:" + document.selection.type + "||typeDetail:" + document.selection.typeDetail);

                        range = document.selection.createRange();

                        if (range && range.parentElement() == o) {
                            len = o.value.length;
                            normalizedValue = o.value.replace(/\r\n/g, "\n");

                            // Create a working TextRange that lives only in the input
                            textInputRange = o.createTextRange();
                            textInputRange.moveToBookmark(range.getBookmark());

                            // Check if the start and end of the selection are at the very end
                            // of the input, since moveStart/moveEnd doesn't return what we want
                            // in those cases
                            endRange = o.createTextRange();
                            endRange.collapse(false);

                            if (textInputRange.compareEndPoints("StartToEnd", endRange) > -1) {
                                start = end = len;
                            } else {
                                start = -textInputRange.moveStart("character", -len);
                                start += normalizedValue.slice(0, start).split("\n").length - 1;

                                if (textInputRange.compareEndPoints("EndToEnd", endRange) > -1) {
                                    end = len;
                                } else {
                                    end = -textInputRange.moveEnd("character", -len);
                                    end += normalizedValue.slice(0, end).split("\n").length - 1;
                                }
                            }
                        }
                    }
                    //console.log("Ruchi getSelectionStart() called:start:" + start + "|end" + end);
                    if (start != end)
                        return start + 1;
                    return start;


                //    var start = 0,
                //    range = o.createTextRange(),
                //    range2 = document.selection.createRange().duplicate(),
                //    // get the opaque string
                //    range2Bookmark = range2.getBookmark();

                //    // move the current range to the duplicate range
                //    range.moveToBookmark(range2Bookmark);

                //    // counts how many units moved (range calculated as before char and after char, loop count is the position)
                //    while (range.moveStart('character', -1) !== 0) {
                //        start++;
                //    }

                //    console.log("mIDDLE getSelectionStart() called:len:" + start + "||Caret Pos:");
                //    //return caretPos(o);
                //    return start;

                }
                else {
                    return caretPos(o);
                  
                    
                    //{
                    //    start: start,
                    //    end: end
                    //};

                    //// To get cursor position, get empty selection range
                    //var oSel = document.selection.createRange();

                    //// Move selection start to 0 position
                    //oSel.moveStart('character', -o.value.length);

                    //// The caret position is selection length
                    //var iCaretPos = oSel.text.length;

                    //console.log("New MIDDLE getSelectionStart() called:len:" + iCaretPos);
                    //return iCaretPos;

                    //o.value = o.value.replace(/\r\n/g, "ab");
                    //var start = 0,
                    //range = o.createTextRange(),
                    //range2 = document.selection.createRange().duplicate(),
                    //// get the opaque string
                    //range2Bookmark = range2.getBookmark();

                    //// move the current range to the duplicate range
                    //range.moveToBookmark(range2Bookmark);

                    //// counts how many units moved (range calculated as before char and after char, loop count is the position)
                    //while (range.moveStart('character', -1) !== 0) {
                    //    start++;
                    //}

                    ////console.log("3nd getSelectionStart() called:len:" + start + "||Caret Pos:" + caretPos(o));
                    //return start;
                }
            }
            else {
                var r = document.selection.createRange().duplicate()
                r.moveEnd('character', o.value.length)
                if (r.text == '') return o.value.length
                return o.value.lastIndexOf(r.text)
            }
        } else return o.selectionStart
    }

    function getSelectionEnd(o)
    {
        if (o.createTextRange && !isBrowserAboveIE9()) {
            if (o.tagName == "TEXTAREA") {//Complete if condition added by AnupK on 23-11-2012 For handling typing support in TextArea and to return currect value of end of selection.
                var bm = document.selection.createRange().duplicate().getBookmark();
                var sel = o.createTextRange().duplicate();
                sel.moveToBookmark(bm);
                var sleft = o.createTextRange().duplicate();
                sleft.collapse(true);
                sleft.setEndPoint("EndToStart", sel);
                o.selectionEnd = sleft.text.length + sel.text.length;
                return o.selectionEnd;
            }
            else {
                var r = document.selection.createRange().duplicate()
                r.moveStart('character', -o.value.length)
                return r.text.length
            }
        } else return o.selectionEnd
    }

    /*****************************OnSelection****************************/
    function display(txtRef)
    {
        if (txtRef == null)
            return;
      if (document.getSelection) 
      {
        if(txtRef.selectionStart || txtRef.selectionStart=='0')
        {
	        g_start = txtRef.selectionStart;
	        g_end = txtRef.selectionEnd;
	        str = txtRef.value.substr(g_start,g_end-g_start);
        }
        else
        {
            var str = document.getSelection();
            if (window.RegExp) 
            {
                var regstr = unescape("%20%20%20%20%20");	
                var regexp = new RegExp(regstr, "g");
                str = str.replace(regexp, "");
	        }
        }
      } 
      else if (document.selection && document.selection.createRange) 
      {
        var range = document.selection.createRange();
        var str = range.text;
      } 
      /*else 
      {
        var str = "Sorry, this is not possible with your browser.";
      }*/
      selectedText = str;
    }
	function setNewLanguage(newLang)
	{
	    /*var string = document.getElementById('iPluginControlsIdToEnable').innerHTML; 
        var splitString= string.split('|');
		document.getElementById('iPluginControlsIdToEnable').innerHTML = splitString[0]+"|"+newLang;*/
		setUpdatedLanguageArrays(newLang,true);
	}
	function setUpdatedLanguageArrays(mapLang,bNewLang)
	{
		//alert(mapLang);
		bNewLang = (typeof(bNewLang) != 'undefined') ? bNewLang : false;
		//document.getElementById('FltKbdLang').innerHTML = mapLang;//For display of language
		mapLang = (new String(mapLang)).toLowerCase();
		loadcssfile(mapLang);
		if( mapLang == 'hi_in')
		{
			mapArray=mapHindiArray;
			mapExtArray=mapHindiExtArray;
			
			mapShiftArray=mapHindiShiftArray;
			mapShiftExtArray=mapHindiShiftExtArray;
		}
		else if(mapLang == 'gj_in')
		{
			mapArray=mapGujratiArray;
			mapExtArray=mapGujratiExtArray;
			
			mapShiftArray=mapGujratiShiftArray;
			mapShiftExtArray=mapGujratiShiftExtArray;
		}
		else if(mapLang == 'pn_in')
		{
			mapArray=mapPunjabiArray;
			mapExtArray=mapPunjabiExtArray;
			
			mapShiftArray=mapPunjabiShiftArray;
			mapShiftExtArray=mapPunjabiShiftExtArray;
		}
		else if(mapLang == 'tl_in')
		{
			mapArray=mapTeluguArray;
			mapExtArray=mapTeluguExtArray;
			
			mapShiftArray=mapTeluguShiftArray;
			mapShiftExtArray=mapTeluguShiftExtArray;
		}
		else if(mapLang == 'bn_in')
		{
			mapArray=mapBengaliArray;
			mapExtArray=mapBengaliExtArray;
			
			mapShiftArray=mapBengaliShiftArray;
			mapShiftExtArray=mapBengaliShiftExtArray;
		}
		else if(mapLang == 'tm_in')
		{
			mapArray=mapTamilArray;
			mapExtArray=mapTamilExtArray;
			
			mapShiftArray=mapTamilShiftArray;
			mapShiftExtArray=mapTamilShiftExtArray;
		}
		else if(mapLang == 'mr_in')
		{
			mapArray = mapMarathiArray;
			mapExtArray = mapMarathiExtArray;
			
			mapShiftArray = mapMarathiShiftArray;
			mapShiftExtArray = mapMarathiShiftExtArray;
		}
		else if(mapLang == 'nepali')
		{
			mapArray = mapNepaliArray;
			mapExtArray = mapNepaliExtArray;
			
			mapShiftArray = mapNepaliShiftArray;
			mapShiftExtArray = mapNepaliShiftExtArray;
		}
		else if (mapLang == 'kn_in') {
		
		
			//var e=document.getElementById('keyBrd');
			//e.className='KannadaFontSupport';
			
			//e.style.fontFamily = "KNOT-Uma";

			mapArray = mapKannadaArray;
		    mapExtArray = mapKannadaExtArray;

		    mapShiftArray = mapKannadaShiftArray;
		    mapShiftExtArray = mapKannadaShiftExtArray;
			
		}
		if(document.getElementById("kbd") && bNewLang)
		{
			var len = mapArray.length;
			
			for(cnt=0;cnt<len;cnt++)
			{				
				document.getElementById("b"+(cnt+1)).value=mapArray[cnt];	
			}
			isKBDOpen = true;
			document.getElementById("btnShift1").className = "shiftBtnUp";
			document.getElementById("btnShift2").className = "shiftBtnUp";
			isShiftBtn = false;
			document.getElementById("lchange").checked = false;
			
		}
	}
	
	function loadcssfile(lang){
//alert(lang);
	 if (lang=="hi_in"){ //if filename is an external CSS file
	var fileref6=document.createElement('link');
	  fileref6.setAttribute('rel', 'stylesheet');
	  fileref6.setAttribute('type', 'text/css');
	  fileref6.setAttribute('href', 'http://196.1.113.73/TransliterationTest/js/css/Hi.css');
	 
	 var fileref7=document.createElement('link');
	  fileref7.setAttribute('rel', 'stylesheet');
	  fileref7.setAttribute('type', 'text/css');
	  fileref7.setAttribute('href', 'http://196.1.113.73/TransliterationTest/js/css/FloatingKeyboardHi.css');
	 if (typeof fileref6!="undefined")
  		document.getElementsByTagName("head")[0].appendChild(fileref6);
	if (typeof fileref7!="undefined")
  		document.getElementsByTagName("head")[0].appendChild(fileref7);

	 
	 }

	
	 if (lang=="bn_in"){ //if filename is an external CSS file
	var fileref=document.createElement('link');
	  fileref.setAttribute('rel', 'stylesheet');
	  fileref.setAttribute('type', 'text/css');
	  fileref.setAttribute('href', 'http://196.1.113.73/TransliterationTest/js/css/Bn.css');
	 
	 var fileref1=document.createElement('link');
	  fileref1.setAttribute('rel', 'stylesheet');
	  fileref1.setAttribute('type', 'text/css');
	  fileref1.setAttribute('href', 'http://196.1.113.73/TransliterationTest/js/css/FloatingKeyboardBn.css');
	 if (typeof fileref!="undefined")
  		document.getElementsByTagName("head")[0].appendChild(fileref);
	if (typeof fileref1!="undefined")
  		document.getElementsByTagName("head")[0].appendChild(fileref1);

	 
	 }

	if (lang=='kn_in'){ 
	var fileref2=document.createElement('link');
	  fileref2.setAttribute('rel', 'stylesheet');
	  fileref2.setAttribute('type', 'text/css');
	  fileref2.setAttribute('href', '/TransliterationTest/js/css/Kn.css');
	 
	var fileref3=document.createElement('link');
	  fileref3.setAttribute('rel', 'stylesheet');
	  fileref3.setAttribute('type', 'text/css');
	  fileref3.setAttribute('href', '/TransliterationTest/js/css/FloatingKeyboardKn.css');
	
	if (typeof fileref2!="undefined")
  		document.getElementsByTagName("head")[0].appendChild(fileref2);
	if (typeof fileref3!="undefined")
  		document.getElementsByTagName("head")[0].appendChild(fileref3);

	 }

	 

	 
	 if (lang=='tl_in'){ 
	 var fileref4=document.createElement('link');
	  fileref4.setAttribute('rel', 'stylesheet');
	  fileref4.setAttribute('type', 'text/css');
	  fileref4.setAttribute('href', '/TransliterationTest/js/css/Tl.css');
	 
	 var fileref5=document.createElement('link');
	  fileref5.setAttribute('rel', 'stylesheet');
	  fileref5.setAttribute('type', 'text/css');
	  fileref5.setAttribute('href', '/TransliterationTest/js/css/FloatingKeyboardTl.css');
	
	 if (typeof fileref4!="undefined")
  		document.getElementsByTagName("head")[0].appendChild(fileref4);
	if (typeof fileref5!="undefined")
  		document.getElementsByTagName("head")[0].appendChild(fileref5);
	 }


	 
	 
}
	
	function getBrowserName()
	{ 
		var browserName = ""; 
		var ua = navigator.userAgent.toLowerCase(); 
		if ( ua.indexOf( "opera" ) != -1 )
		{ 
			browserName = "opera"; 
		} 
		else if ( ua.indexOf( "msie" ) != -1 )
		{ 
			browserName = "msie"; 
		} 
		else if ( ua.indexOf( "safari" ) != -1 )
		{ 
			browserName = "safari"; 
		}
		else if ( ua.indexOf( "mozilla" ) != -1 )
		{ 
			if ( ua.indexOf( "firefox" ) != -1 )
			{ 
				browserName = "firefox"; 
			}
			else
			{ 
				browserName = "mozilla"; 
			}	 
		} 
		return browserName; 
	}
	function isBrowserAboveIE9()
	{	
		var navAppVer = navigator.appVersion;
		var versionInfo = parseInt(navigator.appVersion);
		if(versionInfo <= 4)
		{
			return false;
		}
		return true;
	}