
  $('.login-window').click(function() {
   
    var loginBox = $(this).attr('href');


    $(loginBox).fadeIn(300);

    var popMargTop = ($(loginBox).height() + 24) / 2; 
    var popMargLeft = ($(loginBox).width() + 24) / 2; 
    
    $(loginBox).css({ 
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
 
    $('body').append('<div id="mask"></div>');
    $('#mask').fadeIn(300);
    
    return false;
});


$('a.close, #mask').live('click', function() { 
    $('#mask , .login-popup').fadeOut(300 , function() {
        $('#mask').remove();  
            
    }); 
    return false;
});

$(window).resize(function () {
  
    frameResize();
});

function frameResize() {
    var doc_width = $(window).width();
    var doc_height = $(window).height();
    var padright = 0, padleft = 0;
   

    $(".main").css('minHeight', ($(window).height() - ($('header').outerHeight() + $('footer').outerHeight())));       
    $('body').css({
        paddingLeft: padleft + 'px',
        paddingRight: padright + 'px'
    });
}

$(function () {
    
    frameResize();
  
});

