﻿function fillAllDepartment() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Department").html("");
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Department").append($("<option></option>").val(item.value).html(item.value + ' - ' + item.Text))
            }
            $("#Department").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function fillDepartmentOffice(Department) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/GetExecutiveOffice';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { ExecutiveDepartment: Department },
        success: function (data) {
            $("#Office").html("");

            $("#Office").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#Office").append($("<option></option>").val(item.value).html(item.value + ' - ' + item.Text))
            }
            $("#Office").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}

function GetFilterPMISReport() {
    
    $("#loader-wrapper").show();
   
    var pathurl = '/Constituency/PMIS/GetPMISReport';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { departmentID: Department, OfficeID: Office },
        success: function (data) {
            $('#maincontainer').show();
            $("#maincontainer").html(data);
           

            document.getElementById("MainDiveThreeTire").style.display = "none";
            document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
        }
    });
}

function GetDepartmentPost() {
    
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/PMIS/GetDepartmentPost';
    $("#currentSlide").val("1");
    var x = parseInt($("#currentSlide").val());
    if (x <= 1) {
        $("#reportbackButton").disabled = false;
    }
    else {
        $("#reportbackButton").disabled = true;
    }
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $('#DivList1').show();
            $("#DivList1").html(data);

            document.getElementById("MainDiveThreeTire").style.display = "none";
            document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
        }
    });
}
function DepartmentListTRCLick(deptID, DeptName)
{
    

  
    if ($('#NavigationHeader li').length == 1) {
        $("#NavigationHeader").append("<li><span href='#'   class='white disabled'>" + DeptName + "</span></li>");
    }
    else
    {
        $('#NavigationHeader li:last-child').remove();
        $("#NavigationHeader").append("<li><span href='#'   class='white disabled'>" + DeptName + "</span></li>");
    }
    $("#loader-wrapper").show();

    var pathurl = '/Constituency/PMIS/GetOfficePost';
    $("#currentSlide").val("2");
    var x = parseInt($("#currentSlide").val());
    if (x <= 1) {
        $("#reportbackButton").disabled = false;
    }
    else {
        $("#reportbackButton").disabled = true;
    }
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { departmentID: deptID },
        success: function (data) {
            $('#DivList2').show();
            $("#DivList2").html(data);
            $('#DepartmentNameHead').html(DeptName);
            $("#loader-wrapper").hide();
        }
    });
}
function OfficeListTRCLick(deptID, officeid, officeName) {
    
   

    if ($('#NavigationHeader li').length == 2) {
        $("#NavigationHeader").append("<li><span href='#'   class='white disabled'>" + officeName + "</span></li>");
    }
    else {
        $('#NavigationHeader li:last-child').remove();
        $("#NavigationHeader").append("<li><span href='#'  class='white disabled'>" + officeName + "</span></li>");
    }
    $("#loader-wrapper").show();
    $("#currentSlide").val("3");
    var x = parseInt($("#currentSlide").val());
    if (x <= 1) {
        $("#reportbackButton").disabled = false;
    }
    else {
        $("#reportbackButton").disabled = true;
    }
    var pathurl = '/Constituency/PMIS/GetDesignationPost';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { departmentID: deptID, officeID: officeid },
        success: function (data) {
            $("#DivList1").fadeOut('slow');
            $('#DivList3').fadeIn('slow').delay(800);
            $("#DivList3").html(data);
            $('#OfficeName').html(officeName);
           
       
            $("#loader-wrapper").hide();
        }
    });
}
function DesignationListTRCLick(deptID, officeid, designation, designationName)
{
    if ($('#NavigationHeader li').length == 3)
    {
        $("#NavigationHeader").append("<li><span  class='white disabled'>" + designationName + "</span></li>");

    }
    else
    {
        $('#NavigationHeader li:last-child').remove();
        $("#NavigationHeader").append("<li><span  class='white disabled'>" + designationName + "</span></li>");

    }
    $("#loader-wrapper").show();
    $("#currentSlide").val("4");
    var x = parseInt($("#currentSlide").val());
    if (x <= 1) {
        $("#reportbackButton").disabled = false;
    }
    else {
        $("#reportbackButton").disabled = true;
    }
    var pathurl = '/Constituency/PMIS/GetOfficeEmployeePost';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { departmentID: deptID, officeID: officeid, designation: designation },
        success: function (data) {
            $("#DivList2").fadeOut('slow');
            $('#DivList4').fadeIn('slow').delay(800);
            $('#DivList4').show();
            $("#DivList4").html(data);
            $('#DesignationName').html(designationName);
         
            $("#loader-wrapper").hide();
        }
    });
}

$("#reportbackButton").on("click", function () {
    
    try {
        var cslide = $("#currentSlide").val();
        var x = parseInt(cslide) - 2;
        if (x+2 > 1) {
            $('#DivList' + x).fadeIn('slow');
            $('#DivList' + cslide).fadeOut('slow').delay(800);
            $("#currentSlide").val(x + 1);
            $('#NavigationHeader li:last-child').remove();
        }
    }
    catch (error) {
        console.log(error.message);
    }
});

function goBack(cslide) {
    
    try {
      //  var cslide = $("#currentSlide").val();
        var x = parseInt(cslide) - 2;
        if (x + 2 > 1) {
            $('#DivList' + x).fadeIn('slow');
            $('#DivList' + cslide).fadeOut('slow').delay(800);
            $("#currentSlide").val(x + 1);
            $('#NavigationHeader li:last-child').remove();
        }
    }
    catch (error) {
        console.log(error.message);
    }
}

function getEmpDetails(AdhaarID)
{
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/PMIS/getEmpDetails';
 
    $.ajax({
        url: pathurl,
        type: 'GET',
        data:{AdhaarID:AdhaarID},
        success: function (data) {
            $("#EmpDetailsDiv").html("");
            $("#EmpDetailsDiv").html(data);
            $("#EmpDetailsModal").modal({                    // wire up the actual modal functionality and show the dialog
                "backdrop": "static",
                "keyboard": true,
                "show": true                     // ensure the modal is shown immediately
            });

            $("#loader-wrapper").hide();
        }
    });
}