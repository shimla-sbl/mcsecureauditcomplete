﻿

//Path : ~/ScriptigFunctions/UserRegistrationForm/UpdateMemberUserDetailsScriptingFunction.js


$(document).ready(function () {
    //$('#Mobile').numeric();
    $('#captchacontainer').find('br').remove();
    $('#CaptchaInputText').attr('placeholder', 'Enter the text shown above');
    $("#HashModel").hide();
    $("#btnmultichkbox").text($('#DepartmentIDs'));
    $("#DSCHash").val('');
    $("#DdlSecDept button").css('width', '263px');

})
$('#Age').change(function () {
    var Age = $('#Age').val();
    if (isNaN(Age)) {
        alert("Make Sure you entered valid Age.");
        $('#Age').val('');
        $('#Age').focus();
        return false;

    }
    if (Age > 150) {
        alert("Onely less then 150 age is permitted.");
        $('#Age').val('');
        return false;
    }
})

$('#DdlSecretory').change(function () {
    var secretoryId = $('#DdlSecretory').val();
    $("#DDLSecretaryValidation").text("");
    if (secretoryId != 0) {
        $.ajaxSetup({ cache: false });
        $.ajax({
            url: "/UserRegistration/UserRegistration/GetAllDeptList",
            type: 'GET',
            data: {
                //secratoryId: secretoryId
            },
            success: function (data) {
                if (data) {
                    $("#ddldepartment").hide();
                    $("#departmentChkBoxList").empty();
                    $("#departmentChkBoxList").html(data);
                    $("#SecretaryDepartmentList").show();
                }
                else {
                    $("#ddldepartment").show();
                    alert("Unable to Found the Code");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.responseText);
                alert(thrownError);
            }
        });
    }
    else {
        $('#DDLSecretaryValidation').text("Please Select Secretary");
        //$("#SecretaryDepartmentList").hide();
    }
});
var otpvalue = "";
var flag = false;

$('#Mobile').change(function () {

    var MobilNumber = $('#Mobile').val();

    if (MobilNumber.length > 0) {
        MobilNumber = MobilNumber.replace("(", "");
        MobilNumber = MobilNumber.replace(")", "");
        MobilNumber = MobilNumber.replace("-", "");
        MobilNumber = MobilNumber.replace(" ", "");

        if (isNaN(MobilNumber)) {
            alert("Make Sure you entered valid Mobile number.");
            $('#Mobile').val('');
            $('#Mobile').focus();
            return false;

        }
        if (MobilNumber.length != 10) {
            alert("Make sure Mobile Number should be in 10 digits.");
            $('#Mobile').val('');
            $('#Mobile').focus();
            return false;
        }
    }

});

function getHash() {
    var Id = $("#EmpOrMemberCode").text();
    if (Id == "") {
        alert("Maker sure you have entered Correct Code");
        return false;
    }
    var dialog = $("#HashModel").removeClass('hide').dialog({
        modal: true,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i>Select DSC Certificate</h4></div>",
        width: 880,
        title_html: true,

        buttons: [

            {
                text: "OK",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    getHashModel();
                    $(this).dialog("close");
                }
            }
        ]
    });


    //$("#HashModel").modal("show");
}

function getHashModel() {

    var key = document.getElementById("DSCHashUniqueID").getCertHash();
    var DSCName = document.getElementById("DSCHashUniqueID").getCertName();
    var DSCType = document.getElementById("DSCHashUniqueID").getCertType();
    var DSCValidity = document.getElementById("DSCHashUniqueID").getCertValidity();
    var currentDate = new Date();
    $("#DSCHash").val(key);
    $.ajaxSetup({ cache: false });
    $.ajax({
        url: "/UserRegistration/UserRegistration/CheckDSCHaskKey",
        type: 'GET',
        data: {
            key: key
        },
        success: function (data) {

            if (data != null && data != "") {
                alert("Selected DSC Already Registered.");
                $("#DSCHash").val('');
                return false;
            }
            else {
                $("#DSCResultView").removeClass('hide');
                if ($("#applateResultDiv").hasClass("hide")) {
                    $("#applateResultDiv").removeClass('hide');
                }
                $("#DSCNameValue").text(DSCName);
                $("#DSCName").val(DSCName);
                $("#DSCTypeValue").text(DSCType);
                $("#DSCEnrollValue").text(currentDate.toDateString());
                $("#DSCValidityValue").text(DSCValidity);
                $("#DSCType").val(DSCType);
                $("#DSCValidity").val(DSCValidity);
                $("#DSCResultView").show();
                return true;


            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            alert(xhr.responseText);
            alert(thrownError);
        }
    });

}

function checkValidation() {
  
    var PreMobile = $("#ValidateMobile").val();
    var NewMobile = $("#Mobile").val();
    var MailID = $("#MailID").val();
    var DdlUserType = $("#DdlUserType option:selected").val();
    var DdlSecretory = $("#DdlSecretory option:selected").val();
    var ddlHOD = $("#ddlHOD option:selected").val();
    var ddlSubUserType = $("#ddlSubUserType option:selected").val();
    var ddlMembers = $("#ddlMembers option:selected").val();
    var ddlOffece = $("#ddlOffice option:selected").val();
    var OtpRequiredInRegistration = $("#hdnOtpRequiredInRegistration").val();
    if (MailID == "")
    {
        alert("Please enter email.");
        $("#MailID").focus();
        return false;
    }
    if (NewMobile == "") {
        alert("Please enter mobile number.");
        $("#Mobile").focus();
        return false;
    }
   else if (DdlUserType == "" || DdlUserType == "0") {

        alert("Please select user type.");
        $("#DdlUserType").focus();
        return false;
    }
    else {
       
        if (DdlUserType == "2" || DdlUserType == "3" || DdlUserType == "4" || DdlUserType == "5") {
            if (ddlSubUserType == "0") {
                alert("Please select are you.");
                $("#ddlSubUserType").focus();
                return false;
            }
            else {
                if (ddlSubUserType == "3" || ddlSubUserType == "5") {
                    if (DdlSecretory == "0" || DdlSecretory == "") {
                        alert("Please select secretary");
                        $("#DdlSecretory").focus();
                        return false;
                    }
                                    
                }
               else if (ddlSubUserType == "16") {
                    if (ddlHOD == "0" || ddlHOD == "") {
                        alert("Please select Hod");
                        $("#ddlHOD").focus();
                        return false;
                    }

               }
               else if (ddlSubUserType == "22") {
                   if (ddlMembers == "0" || ddlMembers == "") {
                       alert("Please select Member");
                       $("#ddlMembers").focus();
                       return false;
                   }

               }
               else if (ddlSubUserType == "37") {
                   if (ddlOffece == "0" || ddlOffece == "") {
                       alert("Please select Office");
                       $("#ddlOffece").focus();
                       return false;
                   }

               }
            }
        }
   }
    if (OtpRequiredInRegistration == "1") {
        var otpvalue = document.getElementById("hdnOTPValue").value;
        var otpId = document.getElementById("OTPValue").value;

        if (PreMobile == NewMobile && otpvalue == otpId) {
            return true;
        }
        else {
            if (PreMobile != NewMobile) {
                alert("Please Generate OTP to validate mobile number.");
                return false;
            }
            else {
                if (otpvalue != otpId) {
                    alert("OTP didn't match.");
                    return false;
                }
                else {

                    return true;
                }
            }
        }
    }
}



function CheckOTPByID() {
    var response = false;
    var empId = $("#EmpOrMemberCode").text();
  
    var Mobile = document.getElementById("Mobile").value
    var OTPValue = document.getElementById("OTPValue").value
    $.ajaxSetup({ cache: false });
    $.ajax({
        url: "/UserRegistration/UserRegistration/CheckOTPByID",
        type: 'GET',
        data: {
            empId: empId,
            Mobile: Mobile,
            OTPValue: OTPValue
        },
        success: function (data) {
            if (data.OTPid != "") {
                if (data.OTP != OTPValue) {
                   // alert("OTP didn't match.");
                    return false;
                }
                response = true;
            }
            else {
                //alert("OTP didn't match.");
                response = false;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            alert(xhr.responseText);
            alert(thrownError);
        }
    });
    return response;
}

function GetOTP() {
    var empId = $("#Name").val(); 
    var Mobile = document.getElementById("Mobile").value;
  
    Mobile = Mobile.replace("(", "");
    Mobile = Mobile.replace(")", "");
    Mobile = Mobile.replace("-", "");
    Mobile = Mobile.replace(" ", "");
    var OTP = "";
    if (empId == "" || Mobile == "") {
        alert("Make Sure you have entered emp id and Mobile Number.");
        return false;
    }
    $.ajaxSetup({ cache: false });
    $.ajax({
        url: "/UserRegistration/UserRegistration/GetOTP",
        type: 'GET',
        data: {
            empId: empId,
            Mobile: Mobile
        },
        success: function (data) {
            if (data) {
                $("#ShowOTP").html(data);
                $('#otpTable tr').each(function () {
                    OTP = $(this).find("#otpvalue").html();
                });
              
                $("#ValidateMobile").val(Mobile);
                $('#hdnOTPValue').val(OTP);
               
            }
            else { alert("Unable to Found the Code"); }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
            alert(thrownError);
        }
    });
}




//function DeActivateDsc(data) {
//    // var userid = $("#UserId").val();

//    var id = data.split('+')[1];

//    //alert(id);
//    $.ajaxSetup({ cache: false });
//    $.ajax({
//        url: "/UserRegistration/UserRegistration/DeactivateUserDsc",
//        type: 'GET',
//        data: {
//            DscId: id

//        },
//        success: function (data) {
//            $("#dsclist").empty();
//            $("#dsclist").html(data);
//        },
//        error: function (xhr, ajaxOptions, thrownError) {

//            alert(xhr.responseText);
//            alert(thrownError); ActivateUserDsc
//        }
//    });

//}

//function ActivateDsc(data) {
//    var id = data.split('+')[1];
//    //alert(id);
//    $.ajaxSetup({ cache: false });
//    $.ajax({
//        url: "/UserRegistration/UserRegistration/ActivateUserDsc",
//        type: 'GET',
//        data: {
//            DscId: id

//        },
//        success: function (data) {
//            $("#dsclist").empty();
//            $("#dsclist").html(data);
//        },
//        error: function (xhr, ajaxOptions, thrownError) {

//            alert(xhr.responseText);
//            alert(thrownError);
//        }
//    });
//}



function DeActivateDsc(data) {

    var id = data;
    var dscststus = $("#dscstatus").val();
    var dscarray = dscststus.split(',');

    var dscId = dscarray[1];
    // if (dscarray[0] == "deactivate") {

    var sdata = "deactivate," + id;
    $("#dscstatus").val(sdata);

    var tdid = "Radiotd" + id;
    var linkid = id;
    $("td#" + tdid).empty();

    $("td#" + tdid).html("<a id='" + linkid + "' class='activeCls' onclick='ActivateDsc(this.id)'>Activate</a>");

    // }
    //$.ajax({
    //    url: "/UserRegistration/UserRegistration/DeactivateUserDsc",
    //    type: 'GET',
    //    data: {
    //        DscId: id

    //    },
    //    success: function (data) {
    //        $("#dsclist").empty();
    //        $("#dsclist").html(data);
    //    },
    //    error: function (xhr, ajaxOptions, thrownError) {

    //        alert(xhr.responseText);
    //        alert(thrownError);
    //        location.reload(true);
    //    }
    //});

}

function ActivateDsc(data) {


    var id = data;

    var dscststus = $("#dscstatus").val();
    var dscarray = dscststus.split(',');

    var dscId = dscarray[1];
    if (dscarray[0] == "deactivate") {
        var sdata = "activate," + id;

        $("#dscstatus").val(sdata);

        var tdid = "Radiotd" + id;
        var linkid = id;
        $("td#" + tdid).empty();

        $("td#" + tdid).html("<a id='" + linkid + "' class='activeCls' onclick='DeActivateDsc(this.id)'>DeActivate</a>");
    }
    else {
        var sdata = "activate," + id;
        $("#dscstatus").val(sdata);
        //var sdfsd = "sdfsd,"
        var newtdid = "Radiotd" + id;
        var newlinkid = id;
        $("td#" + newtdid).empty();
        $("td#" + newtdid).html("<a id='" + newlinkid + "' class='activeCls' onclick='DeActivateDsc(this.id)'>DeActivate</a>");

        var oldtdid = "Radiotd" + dscId;
        //var oldlinkid = "Radiobtn" + dscId;
        $("td#" + oldtdid).empty();
        $("td#" + oldtdid).html("<a id='" + dscId + "' class='activeCls' onclick='ActivateDsc(this.id)'>Activate</a>");


    }


    //$.ajax({
    //    url: "/UserRegistration/UserRegistration/ActivateUserDsc",
    //    type: 'GET',
    //    data: {
    //        DscId: id

    //    },
    //    success: function (data) {
    //        $("#dsclist").empty();
    //        $("#dsclist").html(data);
    //    },
    //    error: function (xhr, ajaxOptions, thrownError) {

    //        alert(xhr.responseText);
    //        alert(thrownError);
    //        location.reload(true);
    //    }
    //});
}



function DeleteDsc() {

    $("#DSCHash").val('');
    $("#DSCResultView").hide();
    $("#DSCName").val('');
    $("#DSCType").val('');
    $("#DSCValidity").text('');


}

//jQuery(function ($) {

//    $("#Mobile").mask("(999) 999-9999");
//});