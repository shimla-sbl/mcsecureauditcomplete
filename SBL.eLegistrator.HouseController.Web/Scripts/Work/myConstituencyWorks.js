﻿
﻿//function GetFilterMySchemeList() {
//  alert('user');
//    if ($("#Checklatest").is(':checked')) {
//        if ($("#latestDate").val() == '') {
//            var dlg = bootbox.alert('Please select a Date .', function (e) {
//                $(dlg).modal('hide');
//            });
//        }
//        else {
          
//            getData();
//        }
//    } else {
//       // alert('user2');
//        getData();
//    }
//}


//﻿function GetFilterMySchemeList() {
//    //alert("member");
//    if ($("#Checklatest").is(':checked')) {
//        if ($("#latestDate").val() == '') {
//            var dlg = bootbox.alert('Please select a Date .', function (e) {
//                $(dlg).modal('hide');
//            });
//        }
//        else {
//            getData();
//        }
//    } else {
//        getData();
//    }
//}


function checkAll(ele, cssclass) {
    var checkboxes = document.getElementsByClassName(cssclass);
    if (ele.checked) {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox' && (!$(checkboxes[i].id).is(':disabled'))) {
                checkboxes[i].checked = true;
            }
        }
    }
    else {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox' && ($(checkboxes[i].id).prop({ disabled: false }))) {
                checkboxes[i].checked = false;
            }
        }
    }
}

function ActivateDetailDiv()
{
    $('#innerLeftSidebar').empty();
    $('#innerLeftSidebar').show();
    $('#summaryTypeDIV').hide();
}

function ActivateSummaryDiv()
{
    $('#innerLeftSidebar').empty();
    $('#innerLeftSidebar').show();
    $('#summaryTypeDIV').show();
}


//function getData() {
//   // alert("membrt");
//    $("#loader-wrapper").show();


//    var ReportType = $("input[name=form-field-radio]:checked").val();
//    var FinancialYear = $("#FinancialYear option:selected").val();
//    var ControllingAuthority = $("#ControllingAuthority option:selected").val();
//    var Demand = $("#Demand option:selected").val();
//    var Program = $("#Program option:selected").val();
//    var Agency = $("#Agency  option:selected").val();
//    var OwnerDepartment = $("#OwnerDepartment option:selected").val();
//    var Panchayat = $("#Panchayat option:selected").val();
//    var WorkType = $("#workType option:selected").val();
//    var Status = $("#Status  option:selected").val();
//    var cFinancialYear = $("#statusFinancialYear option:selected").val();
//    var summaryTypeDropDown = $("#summaryTypeDropDown option:selected").val();
//    var sanctionedDate = $("#latestDate").val();

//    if (ReportType == "DetailReport") {
//        var pathurl = '/Constituency/Constituency/FilteredMyConstituencyWorks';
//        $.ajax({
//            url: pathurl,
//            type: 'GET',
//            data: {
//                FinancialYear: FinancialYear,
//                ControllingAuthority: ControllingAuthority,
//                Demand: Demand, Program: Program,
//                Agency: Agency, OwnerDepartment: OwnerDepartment,
//                Panchayat: Panchayat, WorkType: WorkType, Status: Status,
//                cFinancialYear: cFinancialYear,
//                sanctionedDate: sanctionedDate
//            },
//            success: function (data) {
//                $('#innerLeftSidebar').empty();
//                $('#innerLeftSidebar').show();
//                $("#innerLeftSidebar").html(data);
//                //document.getElementById("MainDiveThreeTire").style.display = "none";
//                //document.getElementById("MainDiveTwoTire").style.display = "none";
//                $("#loader-wrapper").hide();
//            }
//        });
//    } else if (ReportType == "SummaryReport") {

//        //alert(cFinancialYear);
//        $('#innerLeftSidebar').empty();
//        var pathurl = '/Constituency/Constituency/SearchWorkList';
//        $.ajax({
//            url: pathurl,
//            type: 'GET',
//            data: {
//                financialYear: FinancialYear,
//                controllingAuthorityId: ControllingAuthority,
//                Demand: Demand,
//                programmeName: Program,
//                executingAgencyCode: Agency,
//                OwnerDepartment: OwnerDepartment,
//                panchayatId: Panchayat,
//                WorkType: WorkType,
//                workStatusId: Status,
//                cFinancialYear: cFinancialYear,
//                sanctionedDate: sanctionedDate,
//                SummaryTypeDD: summaryTypeDropDown
//            },
//            success: function (data) {
                
//                $("#viewWorksSummary").html("");
//                $("#viewWorksSummary").html(data);
//                $("#viewWorksSummary").modal({
//                    "backdrop": "static",
//                    "keyboard": true,
//                    "show": true,
//                });



//                //$('#viewWorksSummaryDiv').empty();
//                //$('#viewWorksSummaryDiv').append(data);
//                //$('#viewWorksSummary').show('modal');
                
//               // $('#innerLeftSidebar').show();
//                //$("#innerLeftSidebar").html(data);
                
//                $("#loader-wrapper").hide();
//            }
//        });
//    }
//}

function fillExecutingDepartment() {
    
    var pathurl = '/Constituency/Constituency/getAllDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#ExecutingDepartment").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#ExecutingDepartment").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#ExecutingDepartment").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}

function fillControllingAuthority(ControllingAuthorityID, DistrictID) {
    
    //if (DistrictID != null && DistrictID != null) {
    var pathurl = '/Constituency/Constituency/getAllControllingAuthority'
   // '/Constituency/Constituency/getAllControllingAuthority';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { DistrictID: DistrictID },
            success: function (data) {
                $("#ControllingAuthorityID").html("");
                $("#ControllingAuthorityID").append($("<option></option>").val('0').html('--Select--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];

                    $("#ControllingAuthorityID").append($("<option></option>").val(item.value).html(item.Text))
                }

                if (ControllingAuthorityID != '') {
                    $("#ControllingAuthorityID").val(ControllingAuthorityID);
                }

                $("#ControllingAuthorityID").trigger("chosen:updated");
                $("#loader-wrapper").hide();
            }
        });
   // }
}
function fillProgramme(selected) {
    var pathurl = '/Constituency/Constituency/GetAllProgramme';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#programID").html("");
            $("#programID").append($("<option></option>").val('0').html('--Select a Project Name--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#programID").append($("<option></option>").val(item.value).html(item.Text))
            }
            if (selected != '') {
                $("#programID").val(selected);
            }
            $("#programID").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillWorkNature(selected) {
    var pathurl = '/Constituency/Constituency/getAllWorkNature';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#workNatureID").html("");
            $("#workNatureID").append($("<option></option>").val('0').html('--Select a Work Nature--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#workNatureID").append($("<option></option>").val(item.value).html(item.Text))
            }
            if (selected != '') {
                $("#workNatureID").val(selected);
            }
            $("#workNatureID").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillWorkStatus(selected) {
    var pathurl = '/Constituency/Constituency/getAllWorkStatus';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#workStatusID").html("");
            //$("#workStatusID").html("");
            $("#workStatusID").append($("<option></option>").val('0').html('Select a Work Status'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#workStatusID").append($("<option></option>").val(item.value).html(item.Text))
            }

            if (selected != '') {
                $("#workStatusID").val(selected);
            }
            if (selected == 3) {
                $("#DivcompletionDate").show('fast');
            }
            else {
                $("#DivcompletionDate").hide('fast');
            }
            $("#loader-wrapper").hide();
        }
    });
}
function fillWorkType(selected) {
    var pathurl = '/Constituency/Constituency/getAllWorkType';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#workTypeID").html("");
            $("#workTypeID").append($("<option></option>").val('0').html('--Select a  Work Type--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#workTypeID").append($("<option></option>").val(item.value).html(item.Text))
            }
            if (selected != '') {
                $("#workTypeID").val(selected);
            }
            $("#workTypeID").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillOwnerDepartment(selected) {
    //if (selected != null && selected != '') {
    //alert("myconstituencyworks fill woner department");
        var pathurl = '/Constituency/Constituency/getAllDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',
            success: function (data) {
                $("#newOwnerDepartmentID").html("");
                $("#newOwnerDepartmentID").append($("<option></option>").val('0').html('select a Owner Department'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#newOwnerDepartmentID").append($("<option></option>").val(item.value).html(item.Text))
                }

                if (selected != '') {
                    $("#newOwnerDepartmentID").val(selected);
                }
                $("#newOwnerDepartmentID").trigger("chosen:updated");
                $("#loader-wrapper").hide();
            }
        });
    //}
}
function fillExecutingAgency(memberCode, selected, officeID) {
   // if (selected != '' && selected != null) {
        var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { memberCode: memberCode },
            success: function (data) {
                $("#ExecutingAgencyID").html("");
                $("#ExecutingAgencyID").append($("<option></option>").val('0').html('Select a Executing Department'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#ExecutingAgencyID").append($("<option></option>").val(item.value).html(item.Text))
                }

                if (selected != '') {
                    $("#ExecutingAgencyID").val(selected);
                    $('#ExecutingAgencyID option:not(:selected)').attr('disabled', true);
                }
                fillExecutiveOffice($("#ExecutingAgencyID :selected").val(), memberCode, officeID);
                $("#ExecutingAgencyID").trigger("chosen:updated");
                $("#loader-wrapper").hide();
            }
        });
    //}
}
function fillExecutingAgency1(memberCode, selected, currentofficeID) {   // For Current dept nd ofc on load page 
    //alert('memberCode')// if (selected != '' && selected != null) {
    var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { memberCode: memberCode },
        success: function (data) {
            $("#ExecutingAgencyID").html("");
            $("#ExecutingAgencyID").append($("<option></option>").val('0').html('Select a Executing Department'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#ExecutingAgencyID").append($("<option></option>").val(item.value).html(item.Text))
            }

            if (selected != '') {
                $("#ExecutingAgencyID").val(selected);
                $('#ExecutingAgencyID option:not(:selected)').attr('disabled', true);
            }
            fillExecutiveOffice1($("#ExecutingAgencyID :selected").val(), memberCode, currentofficeID);
            $("#ExecutingAgencyID").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
    //}
}
function fillPanchayat(selected) {
    var pathurl = '/Constituency/Constituency/getAllPanchayat';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#newPanchayat").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#newPanchayat").append($("<option></option>").val(item.value).html(item.Text))
            }

            if (selected != '') {
                $("#newPanchayat").val(selected);
            }
            $("#newPanchayat").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}
function OnUpdateSuccess() {
  //  alert('success'); // memeber 
    $('#resize-right .widget-box').hide();
    var w = $('#resize-left').parent().width();
    $('#resize-left').animate({ "width": w });
    ShowNotification("success", "Work Has been update");
    GetFilterMySchemeList();
}


function OnAddSuccess() {
  //  alert('addsuccess'); // officer
    $('#resize-right .widget-box').hide();
    var w = $('#resize-left').parent().width();
    $('#resize-left').animate({ "width": w });
    ShowNotification("success", "Work Has been update");
    //getDataAddd();
    getDataOfficer();
}



function getDataOfficer() {
    
    $("#RightSidebar .widget-box").hide();
    $("#loader-wrapper").show();
    var FinancialYear = $("#FinancialYear option:selected").val();
    var ControllingAuthority = $("#ControllingAuthority option:selected").val();
    var Demand = $("#Demand option:selected").val();
    var Program = $("#Program option:selected").val();
    var Agency = $("#Agency  option:selected").val();
    var OwnerDepartment = $("#OwnerDepartment option:selected").val();
    var Panchayat = $("#Panchayat option:selected").val();
    var WorkType = $("#workType option:selected").val();
    var Status = $("#Status  option:selected").val();
    var DistrictID = $("#District  option:selected").val();
    var Constituency = $("#Constituency  option:selected").val();
    var cFinancialYear = $("#statusFinancialYear option:selected").val();
    var sanctionedDate = $("#latestDate").val();
    var pathurl = '/Constituency/Constituency/FilteredconstituencyListForOfficer';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { FinancialYear: FinancialYear, ControllingAuthority: ControllingAuthority, Demand: Demand, Program: Program, Agency: Agency, OwnerDepartment: OwnerDepartment, Panchayat: Panchayat, WorkType: WorkType, Status: Status, DistrictID: DistrictID, Constituency: Constituency, cFinancialYear: cFinancialYear, sanctionedDate: sanctionedDate },
        success: function (data) {
            $('#inneridebar').show();
            $("#innerLeftSidebar").html(data);
            //document.getElementById("MainDiveThreeTire").style.display = "none";
            //document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
        }
    });
}
function newWork(type) {
   
    $("#divNew").show();
    $("#divUpdate").hide();

    $("#loader-wrapper").show();
    $('.widget-box').show();
    var pathurl = '/Constituency/Constituency/NewConstituencyWork';
    $.ajax({
        url: pathurl,
        type: 'GET',

        success: function (data) {
            //$('#resize-left').animate({ 'width': '530px' });
            //$("#innerRightSidebar").html("");
            //$("#innerRightSidebar").html(data)
            $("#loginModal").modal('show');
            $('.modal-body').html(data);
            $('#myModalContent').html(data);
            //$('#loginModal').modal(options);

            $("#loader-wrapper").hide();
        }
    });


}


function viewProgressImages(schemeID) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/viewProgressImages';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { schemeID: schemeID },
        success: function (data) {
            $("#viewProgressImages").html("");
            $("#viewProgressImages").html(data);
            $("#viewProgressImages").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            $("#loader-wrapper").hide();
        }
    });
}
function viewFinancialProgressbyMember(schemeID) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/FinancialviewProgressbyMember';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { schemeID: schemeID },
        success: function (data) {
            $("#viewFinancialProgress").html("");
            $("#viewFinancialProgress").html(data);
            $("#viewFinancialProgress").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            $("#loader-wrapper").hide();
        }
    });
}
function viewProgressbyMember(schemeID) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/viewProgressbyMember';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { schemeID: schemeID },
        success: function (data) {
            $("#viewphysicalProgress").html("");
            $("#viewphysicalProgress").html(data);
            $("#viewphysicalProgress").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            $("#loader-wrapper").hide();
        }
    });
}
function fillExecutiveOffice(ExecutiveDepartment, memberCode, selected) {
    
    $("#loader-wrapper").show();
   // alert('fill from here');
    var pathurl = '/Constituency/Constituency/GetExecutiveOffice';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { ExecutiveDepartment: ExecutiveDepartment, memberCode: memberCode },
        success: function (data) {
            $("#ExecutingOfficeID").html("");
            
            //$("#ExecutingOfficeID").append($("<option></option>").val('0').html('Select a Executing Office'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
               // alert(item.Text);
                $("#ExecutingOfficeID").append($("<option></option>").val(item.value).html(item.Text))
               
            }
            if (selected != '') {
                $("#ExecutingOfficeID").val(selected);
                //$('#ExecutingOfficeID option:not(:selected)').attr('disabled', true);
            }
            $("#ExecutingOfficeID").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillExecutiveOffice1(ExecutiveDepartment, memberCode, crntofcid) {
    //alert(crntofcid);
    //alert(memberCode);
    //alert(ExecutiveDepartment);
    
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/GetExecutiveOffice';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { ExecutiveDepartment: ExecutiveDepartment, memberCode: memberCode },
        success: function (data) {
            $("#ExecutingOfficeID").html("");
            
            //$("#ExecutingOfficeID").append($("<option></option>").val('0').html('Select a Executing Office'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#ExecutingOfficeID").append($("<option></option>").val(item.value).html(item.Text))
               // alert(item.Text);
            }
            if (crntofcid != '') {
                $("#ExecutingOfficeID").val(crntofcid);
                //$('#ExecutingOfficeID option:not(:selected)').attr('disabled', true);
            }
            $("#ExecutingOfficeID").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillDistrict(constituencyID) {
    //alert(constituencyID);
    //alert('file distt');
        var pathurl = '/Constituency/Constituency/getConstituencyDistrict';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { constituencyID: constituencyID },
            success: function (data) {
                $("#DistrictID").html("");
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#DistrictID").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#DistrictID").trigger("chosen:updated");
                $("#loader-wrapper").hide();
            }
        });
    
}


function fillPanchayatByConstituency(constituencyID, selected) {
    debugger;
   // if (constituencyID != null && constituencyID != '') {
       // alert("fill panhayat by const -- constituee id " + constituencyID);
        var pathurl = '/Constituency/Constituency/getConstituencyPanchayat';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { constituencyID: constituencyID },
            success: function (data) {
                $("#newPanchayatID").html("");

                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#newPanchayatID").append($("<option></option>").val(item.value).html(item.Text))
                }
               
                $("#newPanchayatID").trigger("chosen:updated");
                $("#loader-wrapper").hide();
            }
        });
    //}
}

//code by priyanka//    mSchemeId
function fillSubdevisionByConstituency(constituencyID, selected) {
    debugger;
   // if (constituencyID != null && constituencyID != '') {
        //alert("on change coid" + constituencyID);
        var pathurl = '/Constituency/Constituency/get_subdevision';

        $.ajax({
            url: pathurl,
            type: 'GET',

            data: { constituencyID: constituencyID },
            success: function (data) {

                $("#subdevisionId").html("");
                //$("#subdevisionId").append($("<option></option>").val('0').html('--Select a Subdivision--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#subdevisionId").append($("<option></option>").val(item.value).html(item.Text))
                }
                if (selected != '') {
                    $("#subdevisionId").val(selected);
                }
                $("#subdevisionId").trigger("chosen:updated");
                $("#loader-wrapper").hide();
            }
        });
    //}
}
function fillSchemeType(selected) {
    var pathurl = '/Constituency/Constituency/fillSchemeType';
  
    $.ajax({
        url: pathurl,
        type: 'GET',

        data: {},
        success: function (data) {
            $("#schemeTypeId").html("");
            //$("#schemeTypeId").append($("<option></option>").val('0').html('--Select Scheme Type--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
              
                $("#schemeTypeId").append($("<option></option>").val(item.value).html(item.Text))
            }
            if (selected != '') {
                $("#schemeTypeId").val(selected);
            }
            $("#schemeTypeId").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}
/////////
function AssigntoOfficer(schemeID) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/AssigntoOfficer';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { schemeCode: schemeID },
        success: function (data) {
            $("#AssigntoOfficer").html("");
            $("#AssigntoOfficer").html(data);
            $("#AssigntoOfficer").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            $("#loader-wrapper").hide();
        }
    });
}
function fillOfficerIDList(OfficeID) {
    $("#loader-wrapper").show();
    //console.log('coming method');
    var pathurl = '/Constituency/Constituency/getAllOfficerDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { OfficeID: OfficeID },
        success: function (data) {
            $("#OfficerIDList").html("");

            //$("#programID").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#OfficerIDList").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#OfficerIDList").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillFinancialYear(select) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#FinancialYearID").html("");
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#FinancialYearID").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
            if (select != '') {
                $("#FinancialYearID").val(select);
            }
            $("#loader-wrapper").hide();
        }
    });
}
function fillAllDemand(selected) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllDemand';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#DemandID").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#DemandID").append($("<option></option>").val(item.value).html(item.Text))
            }
            if (selected != '') {
                $("#DemandID").val(selected);
            }
            $("#DemandID").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}

try {
    $("input[numberonly]").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}
catch (error) {
    alert(error.message);
}
$("#Checklatest").change(function () {
    console.log('coming');
    if ($(this).is(':checked')) {
        $("#latestDate").attr("disabled", false);
    }
    else {
        $("#latestDate").attr("disabled", true);
    }
});
// update excuting agency UpdateExecutingAgencyModal
function updateselectedexcutingagency() {
    var checkedSScheme = getAllCheckedmyConstituency();
    if (checkedSScheme != '') {
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                $("#loader-wrapper").show();
                var pathurl = '/Constituency/Constituency/updateexcutingagency';
                $.ajax({
                    url: pathurl,
                    type: 'GET',
                    data: { schemeID: checkedSScheme, Mode: "all" },
                    success: function (data) {
                        $("#UpdateExecutingAgencyModal").html("");
                        $("#UpdateExecutingAgencyModal").html(data);
                        $("#UpdateExecutingAgencyModal").modal({
                            "backdrop": "static",
                            "keyboard": true,
                            "show": true
                        });
                        $("#loader-wrapper").hide();
                    }
                });
            }
        });
    } else {
        var dlg = bootbox.alert('Please select at least one Check Box.', function (e) {
            $(dlg).modal('hide');
        });
    }
}
function getAllCheckedmyConstituency() {
    var mycheckedID = '';
    $(".myconstituency").each(function (event, obj) {
        if ($(obj).is(':checked')) {
            if (mycheckedID != '') {
                mycheckedID = mycheckedID + ',' + obj.id;
            } else {
                mycheckedID = obj.id;
            }
        }
    });
    return mycheckedID;
}
function updateexcutingagency(schemeID) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/updateexcutingagency';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { schemeID: schemeID, Mode: "Single" },
        success: function (data) {
            $("#UpdateExecutingAgencyModal").html("");
            $("#UpdateExecutingAgencyModal").html(data);
            $("#UpdateExecutingAgencyModal").modal({
                "backdrop": "static",
                "keyboard": true,
                "show": true
            });
            $("#loader-wrapper").hide();
        }
    });
}

function fillExecutingDepartmentForUpdate(memberCode, selected, office) {
    var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { memberCode: memberCode },
        success: function (data) {
            $("#ExecutingDepartmentForUpdate").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#ExecutingDepartmentForUpdate").append($("<option></option>").val(item.value).html(item.Text))
            }

            if (selected != '') {
                $("#ExecutingDepartmentForUpdate").val(selected);
            }
            fillExecutiveOfficeForUpdate($("#ExecutingDepartmentForUpdate option:selected").val(), memberCode, office);
            $("#ExecutingDepartmentForUpdate").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillExecutiveOfficeForUpdate(ExecutiveDepartment, memberCode, selected) {
    $("#loader-wrapper").show();
   // alert('yes')
    var pathurl = '/Constituency/Constituency/GetExecutiveOffice';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { ExecutiveDepartment: ExecutiveDepartment, memberCode: memberCode },
        success: function (data) {
            $("#ExecutingOfficeIDForUpdate").html("");

            $("#ExecutingOfficeIDForUpdate").append($("<option></option>").val('0').html('select an Office'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#ExecutingOfficeIDForUpdate").append($("<option></option>").val(item.value).html(item.Text))
            }
            if (selected != '') {
                $("#ExecutingOfficeIDForUpdate").val(selected);
            }
            $("#ExecutingOfficeIDForUpdate").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}

function removeexcutingagency(schemeID) {
    var msg = confirm("Are You Sure")
    if (msg == true) {
       
     //   bootbox.confirm("Are you sure?", function (result) {
           // if (result) {
                $("#loader-wrapper").show();

                var pathurl = '/Constituency/Constituency/removeExecutingDetails';
                $.ajax({
                    url: pathurl,
                    type: 'GET',
                    data: { schemeID: schemeID },
                    success: function (data) {
                        $("#loader-wrapper").hide();
                        ShowNotification("success", "Executing Department and Executing Office has been removed.");
                        GetFilterMySchemeList();
                    }
                });
          //  }
      // });
    }
    else {

        return false;
    }
    
}
function fillConstituency(selectedconId) {
   // alert("Fill consitituency" + selectedconId);
    var pathurl = '/Constituency/Constituency/getConstituencyNameByID';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {},// constituencyID: conId
        success: function (data) {
            $("#ConcID").html("");

            $("#ConcID").append($("<option></option>").val('0').html('Select a Ward Name'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#ConcID").append($("<option></option>").val(item.value).html(item.Text))
            }
            if (selectedconId != '') {
                $("#ConcID").val(selectedconId);
            }
            $("#ConcID").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function fillConstituencyByDistrict(selectedDistrictID) {
    debugger;
    var s;
    var pathurl = '/Constituency/Constituency/getConstituencyByDistrictId';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { DistrictID: selectedDistrictID },// constituencyID: conId
        success: function (data) {
            $("#ConcID").html("");
            $("#ConcID").append($("<option></option>").val('0').html('Select a Ward Name'))
            for (var i = 0; i < data.length; i++) {
                
                var item = data[i];
                if (i == 0) {
                   fillPanchayatByConstituency(item.value);
                }
                $("#ConcID").append($("<option></option>").val(item.value).html(item.Text))
            }
            //if (selectedconId != '') {
            //    $("#ConcID").val(selectedconId);
            //}
            $("#ConcID").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
    //var s = $("#ConcID").val();
    //alert(s);
}

function Validation() {
    //alert();
    var ControllingAuthorityID = $("#ControllingAuthorityID option:selected").text();
    if (ControllingAuthorityID = "--All--") {
        alert("Please select controlling authority");
        return false;
    }
}