﻿$(document).ready(function () {
    try
    {
      

        $("input[numberonly]").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

    }
    catch (error)
    {
        alert(error.message);
    }
    //delete html dis rows
    try {
        $(document).on("click", ".deleteButton", function (e) {

            var target = e.target;
            $(target).closest('tr').remove();
            var table = $("#disbursementTable");
            $("#spancount").text('0');
            table.find('tr').each(function (i, el)
            {
                
                var trId = $(this).attr('id');
                var $tds = $(this).find('td');
              
                $tds.eq(0).text(i+1);
              
                $("input[name =span_1_" + trId + "]").attr('name', 'span_1_' + i);
                $("input[name =span_2_" + trId + "]").attr('name', 'span_2_' + i);
                $(this).attr('id', i);
                $("#tcount").val(i+1);
                $("#spancount").text(i+1);
            });
        });
    }
    catch (error) {
        alert(error.message);
    }
});
function showLoanAmount() {
    try {
        var loanAmount = $("#loanAmount").val();
        var EMI = $("#EMI").val();
        var intRate = $("#intRate").val();
        var sanctionDate = $("#sanctionDate").val();
        $(".loanAmount").text(loanAmount+'.00');
        $(".EMI").text(EMI);
        $(".intRate").text(intRate+ '%');
        $(".sanctionDate").text(sanctionDate);
        $(".loanAccountNumber").text($("#loanAccountNumber").val());
        $(".loanNumber").text($("#loanNumber").val());

    } catch (error) {
        alert(error.message);
    }
}
function addDisbursement() {
   
    if ($("#tenuresDetailsWidget").hasClass("collapsed")) {
        $("#tenuresDetailsWidget").removeClass("collapsed");
    }

        var table = document.getElementById("disbursementTable");
        var rowCount = table.rows.length;
        var row = table.insertRow(rowCount);
        row.id = rowCount;
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        cell1.innerHTML = rowCount + 1;
        cell2.innerHTML = cell2.innerHTML + '<span class="input-icon"> <input type="text"  name="span_1_' + rowCount + '"  class="Disbursement col-xs-12 form-control date-picker" data-date-format="dd-mm-yyyy" numberonly  /><i class="ace-icon fa fa-calendar blue"></i></span>';
        cell3.innerHTML = cell3.innerHTML + '<span class="input-icon"> <input type="text"  name="span_2_' + rowCount + '" value="" class="Disbursement col-xs-12 disadd"  onkeypress="return onlyNos(event,this);"  /> <i class="ace-icon fa fa-rupee blue"></i></span>';
        cell4.innerHTML = cell4.innerHTML + '<div  class="hidden-sm hidden-xs btn-group"> <button type="button" class="btn btn-xs btn-danger deleteButton"  ><i class="ace-icon fa fa-trash-o bigger-120"></i>Delete </button></div>';
        $("#tcount").val(rowCount + 1);
        $("#spancount").text(rowCount + 1);
        inItDate();
}
function step2() {
    var IsValid = true;

    if ($('#sanctionDate').val() == '') {
        var dlg = bootbox.alert('Sanction Date is Required', function (e) {
            $(dlg).modal('hide');
        });

        IsValid = false;
    }
    else if ($('#loanAmount').val() == '') {
        var dlg = bootbox.alert('Loan Amount is  Required', function (e) {
            $(dlg).modal('hide');
        });

        IsValid = false;
    }
    else if ($('#loanAccountNumber').val() == '') {
        var dlg = bootbox.alert('Loan Account Number is Required', function (e) {
            $(dlg).modal('hide');
        });

        IsValid = false;
    }
    else if ($('#EMI').val() == '') {
        var dlg = bootbox.alert('EMI is required', function (e) {
            $(dlg).modal('hide');
        });

        IsValid = false;
    }
    else if ($('#intRate').val() == '') {
        //$('#OrderIdValidation').html("Order Id is Required");

        var dlg = bootbox.alert('Int Rate is Required', function (e) {
            $(dlg).modal('hide');
        });

        IsValid = false;
    }
    else {
        IsValid = true;
    }

    return IsValid;
    //else if ($('#EmpCode').val() == '') {
    //    $('#EmpCodeValidation').html("Employee Code is Required");

    //    return false;
    //}
}
function step3() {

    var IsValid = true;

    if ( $("#tcount").val()==0) {
        var dlg = bootbox.alert('Please enter atleast one Disbursement Details.', function (e) {
            $(dlg).modal('hide');
        });

        IsValid = false;
    }
    if ($(".Disbursement").val() == '') {
        var dlg = bootbox.alert('Please Fill Disbursement Details.', function (e) {
            $(dlg).modal('hide');
        });

        IsValid = false;
    }
    var sum = 0;
    $(".disadd").each(function () {
        sum += +$(this).val();
    });
   
    if (sum > $("#loanAmount").val()) {
        var dlg = bootbox.alert('Total Disbursement Amount can not be greater than loan Amount.', function (e) {
            $(dlg).modal('hide');
        });

        IsValid = false;
    }
    return IsValid;
}
function step4() {
    alert('COMING');
    $("#saveLoanForm").submit(function (e) {
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        alert(formURL);
        $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (data, textStatus, jqXHR) {
                //data: return data from server
            },
            error: function (jqXHR, textStatus, errorThrown) {
                //if fails      
            }
        });
     
    });
}
function inItDate() {
    $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true
    })
           //show datepicker when clicking on the icon
           .next().on(ace.click_event, function () {
               $(this).prev().focus();
           });
}
function resetFields() {
    $(".field-validation-error").empty();
    $('#NewsPaperName').html('');
    $('#NewsDate').html('');
    $('#PageNumber').html('');
    $('#Subject').html('');
    $('#ContentKeywords').html('');
    $('#ContentText').html('');
    $('#NewsPaperName').focus();
}
function onlyNos(e, t) {
    try {
        if (window.event) {
            var charCode = window.event.keyCode;
        }
        else if (e) {
            var charCode = e.which;
        }
        else { return true; }
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    catch (err) {
        alert(err.Description);
    }
}