﻿$(document).ready(function () {
    $("input[numberonly]").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $(".salaryheadscheckbox").click(function (event) {
        var id = event.target.id;
        if (document.getElementById(id).checked) {
            var aid = id.split("_");
            var textid = "amount_" + aid[1];
            var buttonid = "updatebutton_" + aid[1];
            var FixedCheckBox = "fixedcheck_" + aid[1];
            document.getElementById(textid).removeAttribute('readonly', true);
            document.getElementById(FixedCheckBox).removeAttribute('disabled', true);
            document.getElementById(buttonid).disabled = false
        }
        else {
            var aid = id.split("_");

            var textid = "amount_" + aid[1];
            var buttonid = "updatebutton_" + aid[1];
            var FixedCheckBox = "fixedcheck_" + aid[1];
            document.getElementById(textid).setAttribute('readonly', true);
            document.getElementById(FixedCheckBox).setAttribute('disabled', false);
            document.getElementById(buttonid).disabled = true;
        }
    });

    //check or uncheck check box on fixed click
    $(".Fixedcheckbox").click(function (event) {
        var id = event.target.id;
        if (document.getElementById(id).checked) {
            var aid = id.split("_");
            var textid = "amount_" + aid[1];
            document.getElementById(textid).removeAttribute('readonly', true);
        }
        else {
            var aid = id.split("_");
            var textid = "amount_" + aid[1];
            document.getElementById(textid).setAttribute('readonly', true);
        }
    });

    $("input[class='Fixedcheckbox']").each(function (i, obj) {
        var aid = obj.id.split("_");
        var textid = "amount_" + aid[1];
        document.getElementById(textid).setAttribute('readonly', true);
    });

    $("input[class='Fixedcheckbox']").each(function (i, obj) {
       
        var id = obj.id;
       
        if (document.getElementById(id).checked) {
            var aid = id.split("_");
            var textid = "amount_" + aid[1];
            document.getElementById(textid).removeAttribute('readonly', true);
        }
        else {
            var aid = id.split("_");
            var textid = "amount_" + aid[1];
            document.getElementById(textid).setAttribute('readonly', true);
        }
    });

    try {
        $("[input[valueChanged=True]").keyup(function (event) {
            // alert(event.target.id + ' changed clicked');
            var amount = $("#" + event.target.id).val();
            var sHeadId = $("#" + event.target.id).attr('cheadid');

            

            // alert(getComponentList(sHeadId));
            var pathurl = "/accounts/salary/getAssociateComponents";

            $.ajax({
                url: pathurl,
                type: 'POST',
                data: JSON.stringify({ sHeadID: sHeadId }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var lst = data;
                    for (var i = 0; i < lst.length; i++) {
                        var perc = lst[i].perc;
                        var result = (amount * perc) / 100;
                        $("input[cheadID=" + lst[i].componentID + "]").val(result);
                    }
                }
            });
        });
    }
    catch (error) {
        alert(error.message);
    }
});

function updateheads(catID, sHeadID, rowID) {

    var textid = "amount_" + rowID;
    var checkbox = "headcheck_" + rowID;
    var buttonid = "updatebutton_" + rowID;
    //alert(document.getElementById(textid).value);
    // alert(document.getElementById(checkbox).value);
    try {

        $("#" + textid).addClass('textboxwait');

        document.getElementById(buttonid).disabled = true;
    } catch (error) {
        alert(error.message);
    }
    var pathurl = "/accounts/salary/updateHead";
    var request = $.ajax({

        url: pathurl,
        data: { catId: catID, sHeadID: sHeadID, amount: document.getElementById(textid).value, status: document.getElementById(checkbox).value },
        success: function (result) {
            $("#" + textid).removeClass('textboxwait');           
            $("#" + textid).addClass('textboxsuccess');
            $("#" + textid).delay(1000);           
            document.getElementById(buttonid).disabled = false;
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("Status: " + textStatus); alert("Error: " + errorThrown);
            $("#" + textid).removeClass('textboxwait');
            document.getElementById(buttonid).disabled = false;
        }
    });
}