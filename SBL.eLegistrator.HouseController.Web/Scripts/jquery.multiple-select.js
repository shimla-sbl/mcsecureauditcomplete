
$.fn.multipleSelect = function(settings) {
	var config = {};
	if(settings)
	{
		$.extend(config, settings);
	}
	return this.each(function() {
		var select = $(this);
		var values = new Array();
		select.bind("click", function() {
			select.val(values);
		});
		select.find("option").bind("mousedown", function() {
			var option = $(this);
			values = select.val();
			if(option.attr("selected"))
			{
				var newValues = new Array();
				for(var i = 0; i < values.length; i++)
				{
					if(values[i] != option.attr("value"))
					{
						newValues.push(values[i]);
					}
				}
				values = newValues;
			}
			else
			{
				if(!values)
				{
					values = new Array();
				}
				values.push(option.attr("value"));
			}
			select.val(values);
			return false;
		});
	});
};