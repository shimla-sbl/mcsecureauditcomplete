﻿function ShowNotification(type, msg) {
    if (type == "success") {
        $('.notification').removeClass('alert alert-danger');
        $('.notification').addClass('notification alert alert-info');
        $('#notificationmsg').text(msg);
        $('.notification').slideDown('fast');
        window.setTimeout(closeNotification, 5000);
    }
    else if (type == "error") {
        $('.notification').removeClass('alert alert-info');
        $('.notification').addClass('notification alert alert-danger');
        $('#notificationmsg').text(msg);
        $('.notification').slideDown('fast');
        window.setTimeout(closeNotification, 5000);
    }
}

function closeNotification() {         
    $('.notification').slideToggle('fast');         
    $('.notification').hide();         
} 