﻿using System.Web;
using System.Web.Optimization;
using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Resolvers;
using BundleTransformer.Core.Transformers;
namespace SBL.eLegistrator.HouseController.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {

            // model validation Bundle
            bundles.Add(new ScriptBundle("~/bundles/validations").Include(
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/jquery.alerts.js",
                "~/Scripts/knockout-2.2.0.debug.js",
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/jquery.numeric.js"
));

            // microsoft Ajax Bundle
            bundles.Add(new ScriptBundle("~/bundles/Ajax").Include(
                "~/Scripts/jquery-te-1.4.0.min.js",
                "~/Scripts/MicrosoftAjax.js",
                "~/Scripts/MicrosoftMvcAjax.js"
                ));
            // jquery css
            bundles.Add(new ScriptBundle("~/bundles/jQueryCss").Include(
                "~/Content/jquery-te-1.4.0.css",
                "~/Content/jquery-ui.css"
                ));
            // Canvas loader script
            bundles.Add(new ScriptBundle("~/bundles/canvasloader").Include(
                "~/Scripts/canvasloader-min.js"
                ));

            //multiple selectiondropdown
            bundles.Add(new ScriptBundle("~/bundles/multipleCheckDropDown").Include(
                "~/Scripts/MultiSelectBox/jquery.multiple.select.js"));

            //multiple selectiondropdown css
            bundles.Add(new ScriptBundle("~/bundles/multipleCheckDropDownCSS").Include(
                "~/Content/MultiSelectList/multiple-select.css"));

            //Scripting Function for UserRegistrationForm Page
            bundles.Add(new ScriptBundle("~/Scripting/UserRegistrationForm").Include(
                "~/ScriptigFunctions/UserRegistrationForm/UserRegistrationScriptingFunctions.js"));

            //Scripting Function for UpdateUserMemberDetails Page
            bundles.Add(new ScriptBundle("~/Scripting/UpdateUserMemberDetails").Include(
                "~/ScriptigFunctions/UserRegistrationForm/UpdateMemberUserDetailsScriptingFunction.js"));

            //Scripting Funtion for UpdateProfile page
            bundles.Add(new ScriptBundle("~/Scripting/UserProfileUpdate").Include(
                 "~/ScriptigFunctions/UserRegistrationForm/UserProfileUpdate.js"));

            //Scripting Funtion for EmployeDsc Authorization page
            bundles.Add(new ScriptBundle("~/Scripting/EmployeDSCAuthorize").Include(
                 "~/ScriptigFunctions/EmployeDscAuthorize/EmployeDSCAuthorize.js"));

            //Mask Input Jquery
            bundles.Add(new ScriptBundle("~/bundles/maskinput").Include(
                "~/assets/js/jquery.maskedinput.min.js",
                "~/assets/js/uncompressed/jquery.js"
                ));

          

            var nullBuilder = new NullBuilder();
            //StyleTransformer and ScriptTransformer classes produce processing of stylesheets and scripts.
            var styleTransformer = new StyleTransformer();

            var scriptTransformer = new ScriptTransformer();
            //NullOrderer class disables the built-in sorting mechanism and save assets sorted in the order they are declared.
            var nullOrderer = new NullOrderer();

            var scriptbundleToObfuscateLogin = new Bundle("~/bundles/LoginJs");
            scriptbundleToObfuscateLogin.Include("~/assets/js/ace-extra.min.js",
                  "~/Scripts/CryptoJSv3.1.2/rollups/pbkdf2.js",
                  "~/Scripts/jquery-1.8.2.min.js",
                  "~/Scripts/jquery.validate.min.js",
                  "~/Scripts/jquery.validate.unobtrusive.js",
                  "~/Scripts/CryptoJSv3.1.2/components/enc-base64-min.js",
                  "~/assets/js/bootstrap.min.js",
                  "~/assets/js/jquery-ui.custom.min.js",
                  "~/assets/js/jquery.ui.touch-punch.min.js",
                  "~/assets/js/bootbox.min.js",
                  "~/assets/js/jquery.easypiechart.min.js",
                  "~/assets/js/jquery.gritter.min.js",
                  "~/assets/js/spin.min.js",
                  "~/assets/js/bootbox.min.js",
                  "~/Scripts/Security.js",
                    "~/Scripts/Login.js"



                  );
            scriptbundleToObfuscateLogin.Builder = nullBuilder;
            scriptbundleToObfuscateLogin.Transforms.Add(scriptTransformer);
            scriptbundleToObfuscateLogin.Orderer = nullOrderer;
            bundles.Add(scriptbundleToObfuscateLogin);
            var scriptbundleToObfuscateResponsiveLayout = new Bundle("~/bundles/ResponsiveLayoutJs");
            scriptbundleToObfuscateResponsiveLayout.Include(
                 "~/assets/js/ace-extra.min.js",
                 "~/Scripts/jquery-1.7.min.js",
                "~/Scripts/Security.js",
                 "~/assets/js/bootstrap.min.js",
                  "~/assets/js/ace-elements.min.js",
                  "~/assets/js/ace.min.js",
                  "~/assets/js/ace/ace.onpage-help.js",
                  "~/assets/js/spin.min.js",
                  "~/assets/docs/assets/js/rainbow.js",
                  "~/assets/docs/assets/js/language/generic.js",
                  "~/assets/docs/assets/js/language/html.js",
                  "~/assets/docs/assets/js/language/css.js",
                  "~/assets/docs/assets/js/language/javascript.js",
                  "~/Scripts/jquery.unobtrusive-ajax.js"             
            );
            scriptbundleToObfuscateResponsiveLayout.Builder = nullBuilder;
            scriptbundleToObfuscateResponsiveLayout.Transforms.Add(scriptTransformer);
            scriptbundleToObfuscateResponsiveLayout.Orderer = nullOrderer;
            bundles.Add(scriptbundleToObfuscateResponsiveLayout);


            var scriptbundleToObfuscateDepartmentDashboard = new Bundle("~/bundles/DepartmentDashboard");
            scriptbundleToObfuscateDepartmentDashboard.Include(
                 "~/Scripts/jquery-1.7.min.js",
                "~/Scripts/jquery-ui-1.8.20.min.js",
                 "~/Scripts/canvasloader-min.js",
                  "~/assets/js/jqGrid/jquery.jqGrid.min.js",
                  "~/assets/js/jqGrid/i18n/grid.locale-en.js",
                  "~/assets/js/CommonNotify.js",
                  "~/Scripts/canvasloader-min.js",
                  "~/assets/js/CommonNotify.js",
                  "~/assets/js/bootbox.min.js",
                  "~/Content/ScriptsUp/jquery.ui.widget.js",
                  "~/Content/ScriptsUp/jquery.fileupload.js",
                  "~/assets/js/jquery-ui.min.js",
                  "~/assets/js/jquery.ui.touch-punch.min.js",
                  "~/Scripts/canvasloader-min.js",
                   "~/assets/js/jqGrid/jquery.jqGrid.min.js",
                    "~/assets/js/jqGrid/i18n/grid.locale-en.js",
                     "~/assets/js/jquery.dataTables.min.js",
                      "~/assets/js/jquery.dataTables.bootstrap.js"
            );
            scriptbundleToObfuscateDepartmentDashboard.Builder = nullBuilder;
            scriptbundleToObfuscateDepartmentDashboard.Transforms.Add(scriptTransformer);
            scriptbundleToObfuscateDepartmentDashboard.Orderer = nullOrderer;
            bundles.Add(scriptbundleToObfuscateDepartmentDashboard);

            var scriptbundleToObfuscateMainDashboard = new Bundle("~/bundles/MainDashboard");
            scriptbundleToObfuscateMainDashboard.Include("~/assets/js/flot/jquery.flot.min.js",
                "~/assets/js/flot/jquery.flot.pie.min.js",
                 "~/assets/js/flot/jquery.flot.resize.min.js"
                  );
            scriptbundleToObfuscateMainDashboard.Builder = nullBuilder;
            scriptbundleToObfuscateMainDashboard.Transforms.Add(scriptTransformer);
            scriptbundleToObfuscateMainDashboard.Orderer = nullOrderer;
            bundles.Add(scriptbundleToObfuscateMainDashboard);






               BundleTable.EnableOptimizations = true;


        }
    }
}