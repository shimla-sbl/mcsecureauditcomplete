﻿using CaptchaMvc.Attributes;
using Microsoft.Security.Application;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.OTP;
using SBL.DomainModel.Models.User;
using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegistrator.HouseController.Filters;
using SBL.eLegistrator.HouseController.Web.Extensions;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using SMS.API;
using SMSServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SRVTextToImage;
using System.Drawing.Imaging;
using System.Drawing;
using SBL.DomainModel.Models.SiteSetting;
using System.Web.Routing;

namespace SBL.Web.Core.Controllers
{
    // [Authorize]
    [Audit]
    [NoCache]
    [ElmahHTTPErrorAttribute]
    public class AccountController : Controller
    {
        //
        // GET: /Account/Login
        [SBLAuthorize(Allow = "All")]
        [NoCache]
        [AllowAnonymous]
        [CheckSession]
        public ActionResult Login(string returnUrl, string message)
        {
            LoginModel objLoginModel = new LoginModel();
            returnUrl = Sanitizer.GetSafeHtmlFragment(returnUrl);
            message = Sanitizer.GetSafeHtmlFragment(message);

            ViewBag.ReturnUrl = returnUrl;
            if (!string.IsNullOrEmpty(message))
            {
                if (message == "session")
                    message = "Session Expired! Please Login.";
            }
            ViewBag.Message = message;
            objLoginModel.CaptchaMessage = "";
            HttpCookie reqCookies = Request.Cookies["Login"];
            if (reqCookies != null)
            {
                var  User_name = reqCookies["Password"].ToString();
                var Password = reqCookies["Name"].ToString();
            }  
            return View(objLoginModel);
        }

        [SBLAuthorize(Allow = "All")]
        public ActionResult RedirectedLogin(string returnUrl)
        {
            returnUrl = Sanitizer.GetSafeHtmlFragment(returnUrl);
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
       
        [SBLAuthorize(Allow = "All")]
        [NoCache]
        [AllowAnonymous]
        [CheckSession]
        public ActionResult LoginUP(string returnUrl)
        {
            


            //added by durgesh for error handling and logging testing only
            //long i = Convert.ToInt64("e");

            Session.Abandon();//by ashwani
            LoginModel objLoginModel = new LoginModel();

            //var assembly = new mAssembly();
            //var assemblyList = (List<mAssembly>)Helper.ExecuteService("Assembly", "GetAllAssembly", assembly);

            //object model = new { Name = "World" };
            //string moduleName = ConfigurationManager.AppSettings["moduleName"];
            //string moduleActionName = ConfigurationManager.AppSettings["moduleActionName"];
            //Notification.Send(moduleName, moduleActionName, model, DateTime.Now);


            returnUrl = Sanitizer.GetSafeHtmlFragment(returnUrl);
            returnUrl = Sanitizer.GetSafeHtmlFragment(returnUrl);
            if (returnUrl == null)
            {
                ViewBag.ReturnUrl = string.Empty;
            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
            }
            if (HttpContext.Request.Cookies["userAadharID"] != null)
            {
                objLoginModel.RememberMe = true;
                try
                {
                    string str = Decrypt(HttpContext.Request.Cookies["userAadharID"].Value);
                    objLoginModel.UserName = str.Substring(0, str.Length - 5);
                }
                catch { }
            }
            else
            {
                objLoginModel.RememberMe = false;
            }
            HttpCookie reqCookies = Request.Cookies["Login"];
            if (reqCookies != null)
            {
                LoginModel model = new LoginModel();
                var User_name = reqCookies["Password"].ToString();
                var Password = reqCookies["Name"].ToString();
                Session["CaptchaImageText"] = "CookieCaptcha";
#pragma warning disable CS0219 // The variable 'CaptchaText' is assigned but its value is never used
                string CaptchaText = "CookieCaptcha";
#pragma warning restore CS0219 // The variable 'CaptchaText' is assigned but its value is never used
                model.UserName = User_name;
                model.ActualPassword = Password;
                //Login(model, false, CaptchaText);
                return View("LoginPersist", model);
            }
            else
            {
                return View("Login", objLoginModel);
            }
            //return PartialView("Login");
          
        }


        public ActionResult SignInChooser()
        {
            return View();
        }

        internal void SetSessions()
        {
            //Session["UserName"] = CurrentSession.UserName;
            //Session.Timeout = 20;
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.UserID));


            DataSet dataSetUser = new DataSet();
            dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "SelectUserbyUserId", methodParameter);


            if (dataSetUser != null && dataSetUser.Tables.Count > 0 && dataSetUser.Tables[0].Rows.Count > 0)
            {

                //  Remove this as this method is not require as now 
                //if (!string.IsNullOrEmpty(CurrentSession.UserID))
                //{
                //    mUserDSHDetails details = new mUserDSHDetails();
                //    details.UserId = Guid.Parse(CurrentSession.UserID);
                //    var userDscLst = (List<mUserDSHDetails>)Helper.ExecuteService("User", "getDscDetailsByUserId", details);
                //    if (userDscLst != null)
                //    {
                //        foreach (var item in userDscLst)
                //        {
                //            CurrentSession.UserHashKey = item.HashKey;
                //        }
                //    }

                //}


                bool IsMember = false;
                //remove this if condition 
                //if ( CurrentSession.SubUserTypeID != "22")
                //{
                if (dataSetUser.Tables[0].Rows[0]["Roleid"] != DBNull.Value)
                {
                    CurrentSession.RoleID = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Roleid"]);
                    CurrentSession.RoleName = Convert.ToString(dataSetUser.Tables[0].Rows[0]["RoleName"]);
                }

                if (dataSetUser.Tables[0].Rows[0]["UserTypeConstituencyId"] != DBNull.Value)
                {
                    CurrentSession.UserTypeConstituencyId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["UserTypeConstituencyId"]);
                }
                if (dataSetUser.Tables[0].Rows[0]["DistrictCode"] != DBNull.Value)
                {
                    CurrentSession.DistrictCode = Convert.ToString(dataSetUser.Tables[0].Rows[0]["DistrictCode"]);
                }

                //}
                /////added by dharmendra,add department in session, if additional department exist
                if (dataSetUser.Tables[0].Rows[0]["DeptId"] != DBNull.Value)
                {
                    if (dataSetUser.Tables[0].Rows[0]["ApprovedAdditionalDept"] != DBNull.Value)
                    {
                        string Department = Convert.ToString(dataSetUser.Tables[0].Rows[0]["DeptId"]);
                        string AdditionalDept = Convert.ToString(dataSetUser.Tables[0].Rows[0]["ApprovedAdditionalDept"]);
                        string alldept = Department + "," + AdditionalDept;
                        List<string> deptlist = alldept.Split(',').Distinct().ToList();
                        string DistDept = deptlist.Aggregate((a, x) => a + "," + x);
                        CurrentSession.DeptID = DistDept;
                    }
                    else
                    {
                        CurrentSession.DeptID = Convert.ToString(dataSetUser.Tables[0].Rows[0]["DeptId"]);
                    }

                }

                if (dataSetUser.Tables[0].Rows[0]["SignaturePath"] != DBNull.Value)
                {
                    CurrentSession.SignaturePath = Convert.ToString(dataSetUser.Tables[0].Rows[0]["SignaturePath"]);

                }

                if (dataSetUser.Tables[1] != null )
                {
                    if(dataSetUser.Tables[1].Rows .Count >0)
                    {
                        CurrentSession.UserId_House = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_House"]);



                        CurrentSession.UserId_DeptReply = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_DeptReply"]);



                        CurrentSession.UserId_eConstituency = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_eConstituency"]);
                    }
                    else
                    {
                        CurrentSession.UserId_House = "";



                        CurrentSession.UserId_DeptReply = "";


                        CurrentSession.UserId_eConstituency = "";

                    }

                   

                }
                else
                {
                    CurrentSession.UserId_House = "";



                    CurrentSession.UserId_DeptReply = "";


                    CurrentSession.UserId_eConstituency = "";

                }





                if (dataSetUser.Tables[0].Rows[0]["Designation"] != DBNull.Value)
                {
                    CurrentSession.Designation = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Designation"]);

                }


                if (dataSetUser.Tables[0].Rows[0]["SecretoryId"] != DBNull.Value)
                {
                    CurrentSession.SecretaryId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["SecretoryId"]);

                }

                if (dataSetUser.Tables[0].Rows[0]["IsHOD"] != DBNull.Value)
                {
                    CurrentSession.IsHOD = Convert.ToString(dataSetUser.Tables[0].Rows[0]["IsHOD"]);

                }

                if (dataSetUser.Tables[0].Rows[0]["IsSecretoryId"] != DBNull.Value)
                {
                    CurrentSession.IsSecretoryId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["IsSecretoryId"]);

                }

                if (dataSetUser.Tables[0].Rows[0]["IsMember"] != DBNull.Value)
                {
                    CurrentSession.MemberDesignation = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Designation"]);
                    IsMember = Convert.ToBoolean(dataSetUser.Tables[0].Rows[0]["IsMember"]);
                }

                if (dataSetUser.Tables[0].Rows[0]["OfficeId"] != DBNull.Value)
                {
                    CurrentSession.OfficeId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["OfficeId"]);

                }
                if (dataSetUser.Tables[0].Rows[0]["OfficeId"] != DBNull.Value)
                {
                    CurrentSession.OfficeId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["OfficeId"]);

                }

                if (dataSetUser.Tables[0].Rows[0]["SubdivisionCode"] != DBNull.Value)
                {
                    CurrentSession.SubDivisionId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["SubdivisionCode"]);

                }
                if (dataSetUser.Tables[0].Rows[0]["ConstituencyID"] != DBNull.Value)
                {
                    CurrentSession.ConstituencyID = Convert.ToString(dataSetUser.Tables[0].Rows[0]["ConstituencyID"]);

                }
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

                if (siteSettingMod != null)
                {
                    CurrentSession.AssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);
                    CurrentSession.SessionId = Convert.ToString(siteSettingMod.SessionCode);

                    //added by robin to compare current session and assembly 
                    CurrentSession.SiteSettingsCurrentAssemblyId = Convert.ToString(siteSettingMod.AssemblyCode);
                    CurrentSession.SiteSettingsCurrentSessionId = Convert.ToString(siteSettingMod.SessionCode);
                }

              // set session alive = 
               // CurrentSession.SetSessionAlive = "1";
                    methodParameter = new List<KeyValuePair<string, string>>();
                    //put condition for member emp and member itself
                    methodParameter.Add(new KeyValuePair<string, string>("@MemberId", CurrentSession.UserName));
                DataSet dataSet = null;
                //ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "SelectMamberIdByLoginId", methodParameter);
                CurrentSession.MemberCode = CurrentSession.UserName;
                if (!IsMember && CurrentSession.SubUserTypeID == "22")//add suser subtype id for members Employee
                    {
                        methodParameter = new List<KeyValuePair<string, string>>();
                        methodParameter.Add(new KeyValuePair<string, string>("@empcd", CurrentSession.UserName));
                        methodParameter.Add(new KeyValuePair<string, string>("@dept", CurrentSession.DeptID));
                    DataSet dataSetEmp = null;
                        //ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEmployeeById", methodParameter);
                        if (dataSetEmp != null)
                        {
                            if (dataSetEmp.Tables[0].Rows.Count > 0)
                            {
                                CurrentSession.Name = Convert.ToString(dataSetEmp.Tables[0].Rows[0]["empfname"]) + " " + Convert.ToString(dataSetEmp.Tables[0].Rows[0]["empmname"]) + " " + Convert.ToString(dataSetEmp.Tables[0].Rows[0]["emplname"]);
                            }
                        }

                    }


                    if (dataSet != null)
                    {
                        if (dataSet.Tables[0].Rows.Count > 0)
                        {

                            CurrentSession.MemberID = Convert.ToString(dataSet.Tables[0].Rows[0][0]);
                            if (CurrentSession.IsMember == "False" && CurrentSession.SubUserTypeID == "22")//add subtype id for OSD Login
                            {
                                CurrentSession.MemberCode = CurrentSession.UserName;
                                CurrentSession.MemberUserID = (string)Helper.ExecuteService("User", "GetMemberDetailsForOSD", CurrentSession.MemberCode);
                                CurrentSession.AadharId = Convert.ToString(dataSet.Tables[0].Rows[0]["AadhaarNo"]);
                            }
                            else
                            {
                                CurrentSession.MemberCode = Convert.ToString(dataSet.Tables[0].Rows[0][1]);
                            }
                            CurrentSession.Name = Convert.ToString(dataSet.Tables[0].Rows[0]["Name"]);
                            CurrentSession.Name_Hindi = Convert.ToString(dataSet.Tables[0].Rows[0]["NameLocal"]);
                            CurrentSession.MemberLocation = Convert.ToString(dataSet.Tables[0].Rows[0]["Location"]);
                            CurrentSession.OldUserName = Convert.ToString(dataSet.Tables[0].Rows[0]["mMemberOldID"]);
                            CurrentSession.MemberPrefix = Convert.ToString(dataSet.Tables[0].Rows[0]["Prefix"]);
                            CurrentSession.MemberEmail = Convert.ToString(dataSet.Tables[0].Rows[0]["Email"]);
                            CurrentSession.MemberMobile = Convert.ToString(dataSet.Tables[0].Rows[0]["Mobile"]);
                            methodParameter = new List<KeyValuePair<string, string>>();
                            methodParameter.Add(new KeyValuePair<string, string>("@MemberID", Convert.ToString(dataSet.Tables[0].Rows[0][1])));
                            methodParameter.Add(new KeyValuePair<string, string>("@Match", null));
                            CurrentSession.MemberDesignation = Convert.ToString(dataSet.Tables[0].Rows[0]["memDesigname"]);
                            DataSet dataSetconst = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "eVidhan_SelectMemberConstinuency", methodParameter);
                            if (dataSetconst.Tables[0].Rows.Count > 0)
                            {
                                CurrentSession.ConstituencyID = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyID"]);
                                CurrentSession.ConstituencyName = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyName"]);
                                CurrentSession.ConstituencyName_Local = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyName_Local"]);
                            }
                        }
                    }
                
            }

        }

        internal void SetId()
        {
            var methodParameter = new List<KeyValuePair<string, string>>();
            string val = "71132CD2-2110-49B5-922D-0092AE59C490";
            //methodParameter.Add(new KeyValuePair<string, string>("@UserID", CurrentSession.UserID));
            methodParameter.Add(new KeyValuePair<string, string>("@UserID", val));
            DataSet dataSetUser = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "SelectUserbyUserId", methodParameter);



            if (dataSetUser != null && dataSetUser.Tables.Count > 0 && dataSetUser.Tables[0].Rows.Count > 0)
            {

                if (dataSetUser.Tables[1] != null)
                {


                    if (dataSetUser.Tables[1].Rows.Count > 0)
                    {
                        CurrentSession.UserId_House = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_House"]);



                        CurrentSession.UserId_DeptReply = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_DeptReply"]);



                        CurrentSession.UserId_eConstituency = Convert.ToString(dataSetUser.Tables[1].Rows[0]["UserId_eConstituency"]);
                    }
                    else
                    {
                        CurrentSession.UserId_House = "";



                        CurrentSession.UserId_DeptReply = "";


                        CurrentSession.UserId_eConstituency = "";

                    }

                }
                else
                {
                    CurrentSession.UserId_House = "";



                    CurrentSession.UserId_DeptReply = "";


                    CurrentSession.UserId_eConstituency = "";

                }



                if (!string.IsNullOrEmpty(CurrentSession.UserID))
                {
                    mUserDSHDetails details = new mUserDSHDetails();
                    details.UserId = Guid.Parse(CurrentSession.UserID);
                    var userDscLst = (List<mUserDSHDetails>)Helper.ExecuteService("User", "getDscDetailsByUserId", details);
                    if (userDscLst != null)
                    {
                        foreach (var item in userDscLst)
                        {
                            CurrentSession.UserHashKey = item.HashKey;
                        }
                    }

                }


                bool IsMember = false;
                if (dataSetUser.Tables[0].Rows[0]["Roleid"] != DBNull.Value)
                {
                    CurrentSession.RoleID = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Roleid"]);
                    CurrentSession.RoleName = Convert.ToString(dataSetUser.Tables[0].Rows[0]["RoleName"]);
                }


                if (dataSetUser.Tables[0].Rows[0]["DeptId"] != DBNull.Value)
                {
                    CurrentSession.DeptID = Convert.ToString(dataSetUser.Tables[0].Rows[0]["DeptId"]);

                }



                if (dataSetUser.Tables[0].Rows[0]["SignaturePath"] != DBNull.Value)
                {
                    CurrentSession.SignaturePath = Convert.ToString(dataSetUser.Tables[0].Rows[0]["SignaturePath"]);

                }
                if (dataSetUser.Tables[0].Rows[0]["Designation"] != DBNull.Value)
                {
                    CurrentSession.Designation = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Designation"]);

                }


                if (dataSetUser.Tables[0].Rows[0]["SecretoryId"] != DBNull.Value)
                {
                    CurrentSession.SecretaryId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["SecretoryId"]);

                }

                if (dataSetUser.Tables[0].Rows[0]["IsHOD"] != DBNull.Value)
                {
                    CurrentSession.IsHOD = Convert.ToString(dataSetUser.Tables[0].Rows[0]["IsHOD"]);

                }

                if (dataSetUser.Tables[0].Rows[0]["IsSecretoryId"] != DBNull.Value)
                {
                    CurrentSession.IsSecretoryId = Convert.ToString(dataSetUser.Tables[0].Rows[0]["IsSecretoryId"]);

                }

                if (dataSetUser.Tables[0].Rows[0]["IsMember"] != DBNull.Value)
                {
                    CurrentSession.MemberDesignation = Convert.ToString(dataSetUser.Tables[0].Rows[0]["Designation"]);
                    IsMember = Convert.ToBoolean(dataSetUser.Tables[0].Rows[0]["IsMember"]);
                }

                methodParameter = new List<KeyValuePair<string, string>>();

                methodParameter.Add(new KeyValuePair<string, string>("@MemberId", CurrentSession.UserName));
                DataSet dataSet = null;
                    //ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "SelectMamberIdByLoginId", methodParameter);

                if (!IsMember)
                {
                    methodParameter = new List<KeyValuePair<string, string>>();
                    methodParameter.Add(new KeyValuePair<string, string>("@empcd", CurrentSession.UserName));
                    methodParameter.Add(new KeyValuePair<string, string>("@dept", CurrentSession.DeptID));
                    DataSet dataSetEmp = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "HPMS_SelectEmployeeById", methodParameter);
                    if (dataSetEmp != null)
                    {
                        if (dataSetEmp.Tables[0].Rows.Count > 0)
                        {
                            CurrentSession.Name = Convert.ToString(dataSetEmp.Tables[0].Rows[0]["empfname"]) + " " + Convert.ToString(dataSetEmp.Tables[0].Rows[0]["empmname"]) + " " + Convert.ToString(dataSetEmp.Tables[0].Rows[0]["emplname"]);
                        }
                    }
                    CurrentSession.IsMember = "false";
                    //DataSet dataSetEmp = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "PMISSelectPersonalInfo", methodParameter);
                    //CurrentSession.Name = Convert.ToString(dataSetEmp.Tables[0].Rows[0]["empfname"]) + " " + Convert.ToString(dataSetEmp.Tables[0].Rows[0]["empmname"]) + " " + Convert.ToString(dataSetEmp.Tables[0].Rows[0]["emplname"]);
                }
                else
                {
                    CurrentSession.IsMember = "true";
                }

                if (dataSet != null)
                {
                    if (dataSet.Tables[0].Rows.Count > 0)
                    {

                        CurrentSession.MemberID = Convert.ToString(dataSet.Tables[0].Rows[0][0]);
                        CurrentSession.MemberCode = Convert.ToString(dataSet.Tables[0].Rows[0][1]);
                        CurrentSession.Name = Convert.ToString(dataSet.Tables[0].Rows[0]["Name"]);
                        CurrentSession.Name_Hindi = Convert.ToString(dataSet.Tables[0].Rows[0]["NameLocal"]);
                        CurrentSession.MemberLocation = Convert.ToString(dataSet.Tables[0].Rows[0]["Location"]);
                        CurrentSession.OldUserName = Convert.ToString(dataSet.Tables[0].Rows[0]["mMemberOldID"]);


                        methodParameter = new List<KeyValuePair<string, string>>();
                        methodParameter.Add(new KeyValuePair<string, string>("@MemberID", Convert.ToString(dataSet.Tables[0].Rows[0][0])));
                        methodParameter.Add(new KeyValuePair<string, string>("@Match", null));
                        CurrentSession.MemberDesignation = Convert.ToString(dataSet.Tables[0].Rows[0]["memDesigname"]);
                        DataSet dataSetconst = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "eVidhan_SelectMemberConstinuency", methodParameter);
                        if (dataSetconst.Tables[0].Rows.Count > 0)
                        {
                            CurrentSession.ConstituencyID = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyID"]);
                            CurrentSession.ConstituencyName = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyName"]);
                            CurrentSession.ConstituencyName_Local = Convert.ToString(dataSetconst.Tables[0].Rows[0]["ConstituencyName_Local"]);
                        }
                    }
                }
            }
            else
            {
                mUsers user = new mUsers();
                CurrentSession.Name = "Test";
                //  user.UserId = new Guid(CurrentSession.UserID);
                // user.UserId = new Guid(val);
                //  user = (mUsers)Helper.ExecuteService("User", "GetIsMemberDetails", user);
                //if (user != null)
                //{
                //    if (user.IsMember == "true")
                //    {
                //        CurrentSession.IsMember = "true";
                //    }
                //    else
                //    {
                //        CurrentSession.IsMember = "false";
                //    }

                //}
            }

        }

        [HttpPost]
        [SBLAuthorize(Allow = "All")]
        [ValidateAntiForgeryToken]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        [AllowAnonymous]//this is for Antiforgery token
        public ActionResult Login(LoginModel model, bool saveUserName, string CaptchaText)
        {
            
            
            string returnUrl = model.returnUrl;
            CurrentSession.ClearSessions();
            var data = false;
            //bool IsPasswordAthenticated = false;
            Int64 n;
            bool isNumeric = Int64.TryParse(model.UserName, out n);
            try
            {
                if (Session["CaptchaImageText"] != null)
                {
                    if (this.Session["CaptchaImageText"].ToString() != CaptchaText)
                    {
                        model.ErrorMessage = "Captcha Validation Failed!";
                        return View("Login", model);
                    }
                }
                string usrreg1 = ConfigurationManager.AppSettings["usrreg5"];
                string pwdreg1 = ConfigurationManager.AppSettings["pwdreg5"];
                string usrreg = ConfigurationManager.AppSettings["usrreg4"];
                string pwdreg = ConfigurationManager.AppSettings["pwdreg4"];
                string passUsr = ConfigurationManager.AppSettings["verUser"];
                string passPwd = ConfigurationManager.AppSettings["verPwd"];





                string actualPass = RemoveSalt(model.ActualPassword);
                string input = model.UserName;



                string sub = input.Substring(0, 1);
                 string subUser="";
                if (input.Length >= 5)
                {
                    subUser = input.Substring(0, 5);
                }
                else
                {
                    subUser = input;
                }
                string subUsercon = "";
                if (input.Length >= 3)
                {
                    subUsercon = input.Substring(0, 3);
                }
                else
                {
                    subUsercon = input;
                }
                if (usrreg1 != null && pwdreg1 != null && usrreg1 == model.UserName && actualPass == pwdreg1)
                {
                    #region login through PrintingPress
                    CurrentSession.Printing = "true";
                    return RedirectToAction("Index", "PrintingPress", new { area = "PrintingPress" });
                    #endregion
                }
                else if (passUsr != null && passPwd != null && passUsr == model.UserName && actualPass == passPwd)
                {
                    #region login through pass
                    return RedirectToAction("Index", "PassVerification", new { area = "PassVerification" });
                    #endregion
                }
                else if (usrreg != null && pwdreg != null && usrreg == model.UserName && actualPass == pwdreg)
                {
                    #region login for user registration
                    return RedirectToAction("UserRegistraitonForm", "UserRegistration", new { area = "UserRegistration" });
                    #endregion
                }
                else if (sub == "T")
                {

                    #region login through OTP Password

                    ServiceAdaptor adapter = new ServiceAdaptor();
                    string correctPassword = RemoveSalt(model.ActualPassword);
                    string ePassword = ServiceAdaptor.ComputeHash(correctPassword, "SHA1", null);
                    mUsers user = new mUsers();
                    user.OTPId = model.UserName;
                    user.OneTimeRegDetails = (ICollection<tOneTimeUserRegistration>)Helper.ExecuteService("User", "GetDetailsOTPID", user);
                    if (user.OneTimeRegDetails != null && user.OneTimeRegDetails.Count > 0)
                    {
                        foreach (var list in user.OneTimeRegDetails)
                        {
                            if (ServiceAdaptor.VerifyHash(correctPassword, "SHA1", list.OTPPassword) == true)
                            {

                                // set status session var as successful
                                CurrentSession.LoginStatus = "Successful";
                                CurrentSession.MbNo = list.MobileNumber;
                                // return RedirectToAction("Index", "OneTimeUserRegistration", new { area = "OneTimeUserRegistration" });
                                //   Avoid Session Hijacking

                                string _browserInfo = Request.Browser.Browser
                                + Request.Browser.Version
                                + Request.UserAgent + "~"
                                + Request.ServerVariables["REMOTE_ADDR"];

                                string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                        + DateTime.Now.Ticks + "^"
                                                        + _browserInfo + "^"
                                                        + System.Guid.NewGuid();

                                byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                                string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                                if (Request.Cookies["AuthToken"] != null)
                                {
                                    var c = new HttpCookie("AuthToken");
                                    c.Value = _newEncryptedString;
                                    if (!Request.IsLocal)
                                    {
                                        c.Secure = true;
                                    }
                                   
                                    Response.SetCookie(c);
                                }
                                else
                                {
                                    var c = new HttpCookie("AuthToken");
                                    c.Value = _newEncryptedString;
                                    if (!Request.IsLocal)
                                    {
                                        c.Secure = true;
                                    }
                                  
                                    Response.Cookies.Add(c);
                                }
                                Session["encryptedSession"] = _newEncryptedString;





                                model.UserName = list.UserId;
                                FormsAuthentication.SetAuthCookie(list.UserId, false);
                                CurrentSession.UserID = list.UserId.ToString();
                                CurrentSession.UserName = list.UserId;
                                // CurrentSession.MbNo = list.MobileNumber;
                                //   SetSessions();


                                SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                                if (!string.IsNullOrEmpty(returnUrl))
                                {
                                    return Redirect(returnUrl);
                                }
                                else
                                {
                                    data = true;
                                    var res = Json(data, JsonRequestBehavior.AllowGet);
                                    //return res;
                                    return RedirectToAction("Index", "OneTimeUserRegistration", new { area = "OneTimeUserRegistration" });
                                }

                            }
                            else
                            {

                                //Wrong Attempt
                                model.ErrorMessage = "Login Failed,Invalid UserId or Password"; //(string)Helper.ExecuteService("User", "WrongAttemptLoginData", list);
                                FormsAuthentication.SignOut();
                                CurrentSession.ClearSessions();
                                return View("Login", model);


                            }
                        }
                    }
                    else
                    {
                        model.ErrorMessage = "Login Failed,Invalid UserId or Password"; //(string)Helper.ExecuteService("User", "WrongAttemptLoginData", null);
                        FormsAuthentication.SignOut();
                        CurrentSession.ClearSessions();
                        return View("Login", model);
                    }
                    #endregion

                }
                else if (isNumeric)
                {
                    #region login through Adhaar ID

                    ServiceAdaptor adapter = new ServiceAdaptor();

                    // string ePassword = ServiceAdaptor.ComputeHash(correctPassword, "SHA1", null);
                    string correctPassword = RemoveSalt(model.ActualPassword);
                    mUsers user = new mUsers();
                    user.AadarId = model.UserName;
                    CurrentSession.AadharId = user.AadarId;
                    user.mUserList = (ICollection<mUsers>)Helper.ExecuteService("User", "GetDetailsByadhaarID", user);

                    if (user.mUserList != null && user.mUserList.Count > 0)
                    {
                        foreach (var list in user.mUserList)
                        {
                            if (ServiceAdaptor.VerifyHash(correctPassword, "SHA1", list.Password) == true)
                            {
                                var result = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = true });
                                if (result.IsValid)
                                {
                                    if (saveUserName)
                                    {
                                        HttpCookie cookie = new HttpCookie("userAadharID");
                                        cookie.Value = Encrypt(user.AadarId + Guid.NewGuid().ToString().Substring(0, 5));
                                        cookie.HttpOnly = true;
                                        cookie.Expires = DateTime.Now.AddMinutes(20);
                                        // cookie.Secure = true;
                                        //cookie.Expires = DateTime.Now.AddDays(10);
                                        HttpContext.Response.Cookies.Remove("userAadharID");
                                        HttpContext.Response.SetCookie(cookie);
                                    }
                                    else
                                    {
                                        if (Request.Cookies["userAadharID"] != null)
                                        {
                                            var c = new HttpCookie("userAadharID");
                                            c.Expires = DateTime.Now.AddMinutes(20);
                                            c.HttpOnly = true;

                                            Response.Cookies.Add(c);
                                        }


                                    }
                                    // set status session var as successful
                                    CurrentSession.LoginStatus = "Successful";

                                    // Avoid Session Hijacking

                                    string _browserInfo = Request.Browser.Browser
                                    + Request.Browser.Version
                                    + Request.UserAgent + "~"
                                    + Request.ServerVariables["REMOTE_ADDR"];

                                    string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                            + DateTime.Now.Ticks + "^"
                                                            + _browserInfo + "^"
                                                            + System.Guid.NewGuid();

                                    byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                                    string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                                    if (Request.Cookies["AuthToken"] != null)
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        c.Expires = DateTime.Now.AddMinutes(20);
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        Response.SetCookie(c);
                                    }
                                    else
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        c.Expires = DateTime.Now.AddMinutes(20);
                                        Response.Cookies.Add(c);

                                    }
                                    Session["encryptedSession"] = _newEncryptedString;





                                    model.UserName = list.UserName;
                                    FormsAuthentication.SetAuthCookie(list.UserName, false);
                                    CurrentSession.UserID = list.UserId.ToString();
                                    CurrentSession.UserName = list.UserName;
                                    CurrentSession.SubUserTypeID = list.UserType.ToString();
                                    CurrentSession.IsMember = list.IsMember;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();
                                   
                                    CurrentSession.OfficeLevel = list.OfficeLevel;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();
                                    if (list.DeptId != null && list.DeptId != "")
                                    {
                                        CurrentSession.DeptID = Convert.ToString(list.DeptId);
                                    }
                                    if (list.SubdivisionCode != null && list.SubdivisionCode != "")
                                    {
                                        CurrentSession.SubDivisionId = Convert.ToString(list.SubdivisionCode);
                                    } 
                                    SetSessions();


                                    SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                                    if (!string.IsNullOrEmpty(returnUrl))
                                    {
                                        return Redirect(returnUrl);
                                    }
                                    else
                                    {
                                        data = true;
                                        var res = Json(data, JsonRequestBehavior.AllowGet);
                                        //return res;
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                                else
                                {
                                    model.ErrorMessage = result.Message;
                                    FormsAuthentication.SignOut();
                                    CurrentSession.ClearSessions();
                                }
                            }
                            else
                            {
                                var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                                model.ErrorMessage = result1.Message;
                                FormsAuthentication.SignOut();
                                CurrentSession.ClearSessions();
                            }
                        }
                    }
                    else
                    {
                        var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                        model.ErrorMessage = result1.Message;
                        FormsAuthentication.SignOut();
                        CurrentSession.ClearSessions();
                    }
                    #endregion

                }
                else if (subUser == "CHIEF")
                {
                    #region login through Adhaar ID

                    ServiceAdaptor adapter = new ServiceAdaptor();

                    // string ePassword = ServiceAdaptor.ComputeHash(correctPassword, "SHA1", null);
                    string correctPassword = RemoveSalt(model.ActualPassword);
                    mUsers user = new mUsers();
                    user.AadarId = model.UserName;
                    CurrentSession.AadharId = user.AadarId;
                    user.mUserList = (ICollection<mUsers>)Helper.ExecuteService("User", "GetDetailsByadhaarID", user);

                    if (user.mUserList != null && user.mUserList.Count > 0)
                    {
                        foreach (var list in user.mUserList)
                        {
                            if (ServiceAdaptor.VerifyHash(correctPassword, "SHA1", list.Password) == true)
                            {
                                var result = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = true });
                                if (result.IsValid)
                                {
                                    if (saveUserName)
                                    {
                                        HttpCookie cookie = new HttpCookie("userAadharID");
                                        cookie.Value = Encrypt(user.AadarId + Guid.NewGuid().ToString().Substring(0, 5));
                                        cookie.HttpOnly = true;
                                       
                                        cookie.Expires = DateTime.Now.AddDays(-1);
                                        // cookie.Secure = true;
                                        //cookie.Expires = DateTime.Now.AddDays(10);
                                        HttpContext.Response.Cookies.Remove("userAadharID");
                                        HttpContext.Response.SetCookie(cookie);
                                    }
                                    else
                                    {
                                        if (Request.Cookies["userAadharID"] != null)
                                        {
                                            var c = new HttpCookie("userAadharID");
                                           
                                            c.Expires = DateTime.Now.AddDays(-1);
                                            c.HttpOnly = true;
                                          
                                            Response.Cookies.Add(c);
                                        }


                                    }
                                    // set status session var as successful
                                    CurrentSession.LoginStatus = "Successful";

                                    // Avoid Session Hijacking

                                    string _browserInfo = Request.Browser.Browser
                                    + Request.Browser.Version
                                    + Request.UserAgent + "~"
                                    + Request.ServerVariables["REMOTE_ADDR"];

                                    string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                            + DateTime.Now.Ticks + "^"
                                                            + _browserInfo + "^"
                                                            + System.Guid.NewGuid();

                                    byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                                    string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                                    if (Request.Cookies["AuthToken"] != null)
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        c.Expires = DateTime.Now.AddDays(-1);
                                        Response.SetCookie(c);
                                    }
                                    else
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        c.Expires = DateTime.Now.AddDays(-1);
                                        Response.Cookies.Add(c);
                                    }
                                    Session["encryptedSession"] = _newEncryptedString;





                                    model.UserName = list.UserName;
                                    FormsAuthentication.SetAuthCookie(list.UserName, false);
                                    CurrentSession.UserID = list.UserId.ToString();
                                    CurrentSession.UserName = list.UserName;
                                    CurrentSession.SubUserTypeID = list.UserType.ToString();
                                    CurrentSession.IsMember = list.IsMember;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();

                                    CurrentSession.OfficeLevel = list.OfficeLevel;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();
                                    if (list.DeptId != null && list.DeptId != "")
                                    {
                                        CurrentSession.DeptID = Convert.ToString(list.DeptId);
                                    }
                                    if (list.SubdivisionCode != null && list.SubdivisionCode != "")
                                    {
                                        CurrentSession.SubDivisionId = Convert.ToString(list.SubdivisionCode);
                                    }
                                    SetSessions();


                                    SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                                    if (!string.IsNullOrEmpty(returnUrl))
                                    {
                                        return Redirect(returnUrl);
                                    }
                                    else
                                    {
                                        data = true;
                                        var res = Json(data, JsonRequestBehavior.AllowGet);
                                        //return res;
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                                else
                                {
                                    model.ErrorMessage = result.Message;
                                    FormsAuthentication.SignOut();
                                    CurrentSession.ClearSessions();
                                }
                            }
                            else
                            {
                                var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                                model.ErrorMessage = result1.Message;
                                FormsAuthentication.SignOut();
                                CurrentSession.ClearSessions();
                            }
                        }
                    }
                    else
                    {
                        var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                        model.ErrorMessage = result1.Message;
                        FormsAuthentication.SignOut();
                        CurrentSession.ClearSessions();
                    }
                    #endregion

                }
                else if (subUsercon == "HPD" || subUsercon =="MMC")
                {
                    #region login through Adhaar ID

                    ServiceAdaptor adapter = new ServiceAdaptor();

                    // string ePassword = ServiceAdaptor.ComputeHash(correctPassword, "SHA1", null);
                    string correctPassword = RemoveSalt(model.ActualPassword);
                    mUsers user = new mUsers();
                    user.AadarId = model.UserName;
                    CurrentSession.AadharId = user.AadarId;
                   
                    user.mUserList = (ICollection<mUsers>)Helper.ExecuteService("User", "GetDetailsByadhaarID", user);

                    if (user.mUserList != null && user.mUserList.Count > 0)
                    {
                        //if (CaptchaText != "CookieCaptcha")
                        //{
                        //    HttpCookie loginCookie = new HttpCookie("Login");
                        //    //Set the Cookie value.
                        //    loginCookie.Values["Name"] = model.ActualPassword;
                        //    loginCookie.Values["Password"] = model.UserName;
                        //    loginCookie.Path = Request.ApplicationPath;
                        //    //Set the Expiry date.
                        //    loginCookie.Expires = DateTime.Now.AddDays(365);
                        //    //Add the Cookie to Browser.
                        //    Response.Cookies.Add(loginCookie);
                        //}
                       

                        foreach (var list in user.mUserList)
                        {
                            if (ServiceAdaptor.VerifyHash(correctPassword, "SHA1", list.Password) == true)
                            {
                                var result = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = true });
                                if (result.IsValid)
                                {
                                    if (saveUserName)
                                    {
                                        HttpCookie cookie = new HttpCookie("userAadharID");
                                        cookie.Value = Encrypt(user.AadarId + Guid.NewGuid().ToString().Substring(0, 5));
                                        cookie.HttpOnly = true;
                                        cookie.Expires = DateTime.Now.AddDays(-1);
                                        // cookie.Secure = true;
                                        //cookie.Expires = DateTime.Now.AddDays(10);
                                        HttpContext.Response.Cookies.Remove("userAadharID");
                                        HttpContext.Response.SetCookie(cookie);
                                    }
                                    else
                                    {
                                        if (Request.Cookies["userAadharID"] != null)
                                        {
                                            var c = new HttpCookie("userAadharID");
                                            c.Expires = DateTime.Now.AddDays(-1);
                                            c.HttpOnly = true;

                                            Response.Cookies.Add(c);
                                        }


                                    }
                                    // set status session var as successful
                                    CurrentSession.LoginStatus = "Successful";

                                    // Avoid Session Hijacking

                                    string _browserInfo = Request.Browser.Browser
                                    + Request.Browser.Version
                                    + Request.UserAgent + "~"
                                    + Request.ServerVariables["REMOTE_ADDR"];

                                    string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                            + DateTime.Now.Ticks + "^"
                                                            + _browserInfo + "^"
                                                            + System.Guid.NewGuid();

                                    byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                                    string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                                    if (Request.Cookies["AuthToken"] != null)
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        c.Expires = DateTime.Now.AddDays(-1);
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        Response.SetCookie(c);
                                    }
                                    else
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        c.Expires = DateTime.Now.AddDays(-1);
                                        Response.Cookies.Add(c);
                                    }
                                    Session["encryptedSession"] = _newEncryptedString;





                                    model.UserName = list.UserName;
                                    FormsAuthentication.SetAuthCookie(list.UserName, false);
                                    CurrentSession.UserID = list.UserId.ToString();
                                    CurrentSession.UserName = list.UserName;
                                    CurrentSession.SubUserTypeID = list.UserType.ToString();
                                    CurrentSession.IsMember = list.IsMember;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();
                                   // CurrentSession.MemberCode = list.UserName; 
                                    CurrentSession.OfficeLevel = list.OfficeLevel;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();
                                    if (list.DeptId != null && list.DeptId != "")
                                    {
                                        CurrentSession.DeptID = Convert.ToString(list.DeptId);
                                    }
                                    if (list.SubdivisionCode != null && list.SubdivisionCode != "")
                                    {
                                        CurrentSession.SubDivisionId = Convert.ToString(list.SubdivisionCode);
                                    }
                                   
                                    SetSessions();
                                    if (list.AadarId.Contains("MMC"))
                                    {

                                    }
                                    else
                                    {
                                        string Memcode = Helper.ExecuteService("User", "GetMemberCode_ForConUser", user) as string;
                                        if (Memcode != "" || Memcode != null)
                                        {
                                            string[] str = new string[3];
                                            str[0] = CurrentSession.OfficeId;
                                            str[1] = Memcode;
                                            str[2] = CurrentSession.AssemblyId;
                                            string UserCondId = Helper.ExecuteService("User", "GetConId_ForConUser", str) as string;
                                            CurrentSession.ConstituencyID = UserCondId;
                                        }
                                    }
                                    SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                                    if (!string.IsNullOrEmpty(returnUrl))
                                    {
                                        return Redirect(returnUrl);
                                    }
                                    else
                                    {
                                        data = true;
                                        var res = Json(data, JsonRequestBehavior.AllowGet);
                                        //return res;
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                                else
                                {
                                    model.ErrorMessage = result.Message;
                                    FormsAuthentication.SignOut();
                                    CurrentSession.ClearSessions();
                                }
                            }
                            else
                            {
                                var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                                model.ErrorMessage = result1.Message;
                                FormsAuthentication.SignOut();
                                CurrentSession.ClearSessions();
                            }
                        }
                    }
                    else
                    {
                        var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                        model.ErrorMessage = result1.Message;
                        FormsAuthentication.SignOut();
                        CurrentSession.ClearSessions();
                    }
                    #endregion

                }
                else if (subUsercon == "SDM")
                {
                    #region login through Adhaar ID

                    ServiceAdaptor adapter = new ServiceAdaptor();

                    // string ePassword = ServiceAdaptor.ComputeHash(correctPassword, "SHA1", null);
                    string correctPassword = RemoveSalt(model.ActualPassword);
                    mUsers user = new mUsers();
                    user.AadarId = model.UserName;
                    CurrentSession.AadharId = user.AadarId;

                    user.mUserList = (ICollection<mUsers>)Helper.ExecuteService("User", "GetDetailsByadhaarID", user);

                    if (user.mUserList != null && user.mUserList.Count > 0)
                    {
                        //if (CaptchaText != "CookieCaptcha")
                        //{
                        //    HttpCookie loginCookie = new HttpCookie("Login");
                        //    //Set the Cookie value.
                        //    loginCookie.Values["Name"] = model.ActualPassword;
                        //    loginCookie.Values["Password"] = model.UserName;
                        //    loginCookie.Path = Request.ApplicationPath;
                        //    //Set the Expiry date.
                        //    loginCookie.Expires = DateTime.Now.AddDays(365);
                        //    //Add the Cookie to Browser.
                        //    Response.Cookies.Add(loginCookie);
                        //}
                       

                        foreach (var list in user.mUserList)
                        {
                            if (ServiceAdaptor.VerifyHash(correctPassword, "SHA1", list.Password) == true)
                            {
                                var result = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = true });
                                if (result.IsValid)
                                {
                                    if (saveUserName)
                                    {
                                        HttpCookie cookie = new HttpCookie("userAadharID");
                                        cookie.Value = Encrypt(user.AadarId + Guid.NewGuid().ToString().Substring(0, 5));
                                        cookie.HttpOnly = true;
                                        // cookie.Secure = true;
                                        cookie.Expires = DateTime.Now.AddDays(-1);
                                        HttpContext.Response.Cookies.Remove("userAadharID");
                                        HttpContext.Response.SetCookie(cookie);
                                    }
                                    else
                                    {
                                        if (Request.Cookies["userAadharID"] != null)
                                        {
                                            var c = new HttpCookie("userAadharID");
                                            c.Expires = DateTime.Now.AddDays(-1);
                                            c.HttpOnly = true;

                                            Response.Cookies.Add(c);
                                        }


                                    }
                                    // set status session var as successful
                                    CurrentSession.LoginStatus = "Successful";

                                    // Avoid Session Hijacking

                                    string _browserInfo = Request.Browser.Browser
                                    + Request.Browser.Version
                                    + Request.UserAgent + "~"
                                    + Request.ServerVariables["REMOTE_ADDR"];

                                    string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                            + DateTime.Now.Ticks + "^"
                                                            + _browserInfo + "^"
                                                            + System.Guid.NewGuid();

                                    byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                                    string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                                    if (Request.Cookies["AuthToken"] != null)
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        c.Expires = DateTime.Now.AddDays(-1);
                                        Response.SetCookie(c);
                                    }
                                    else
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        c.Expires =DateTime.Now.AddDays(-1);
                                        Response.Cookies.Add(c);
                                    }
                                    Session["encryptedSession"] = _newEncryptedString;





                                    model.UserName = list.UserName;
                                    FormsAuthentication.SetAuthCookie(list.UserName, false);
                                    CurrentSession.UserID = list.UserId.ToString();
                                    CurrentSession.UserName = list.UserName;
                                    CurrentSession.SubUserTypeID = list.UserType.ToString();
                                    CurrentSession.IsMember = list.IsMember;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();

                                    CurrentSession.OfficeLevel = list.OfficeLevel;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();
                                    if (list.DeptId != null && list.DeptId != "")
                                    {
                                        CurrentSession.DeptID = Convert.ToString(list.DeptId);
                                    }
                                    if (list.SubdivisionCode != null && list.SubdivisionCode != "")
                                    {
                                        CurrentSession.SubDivisionId = Convert.ToString(list.SubdivisionCode);
                                    }

                                    SetSessions();
                                    //string Memcode = Helper.ExecuteService("User", "GetMemberCode_ForConUser", user) as string;
                                    //if (Memcode != "" || Memcode != null)
                                    //{
                                    //    string[] str = new string[3];
                                    //    str[0] = CurrentSession.OfficeId;
                                    //    str[1] = Memcode;
                                    //    str[2] = CurrentSession.AssemblyId;
                                    //    string UserCondId = Helper.ExecuteService("User", "GetConId_ForConUser", str) as string;
                                    //    CurrentSession.ConstituencyID = UserCondId;
                                    //}
                                    SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                                    if (!string.IsNullOrEmpty(returnUrl))
                                    {
                                        return Redirect(returnUrl);
                                    }
                                    else
                                    {
                                        data = true;
                                        var res = Json(data, JsonRequestBehavior.AllowGet);
                                        //return res;
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                                else
                                {
                                    model.ErrorMessage = result.Message;
                                    FormsAuthentication.SignOut();
                                    CurrentSession.ClearSessions();
                                }
                            }
                            else
                            {
                                var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                                model.ErrorMessage = result1.Message;
                                FormsAuthentication.SignOut();
                                CurrentSession.ClearSessions();
                            }
                        }
                    }
                    else
                    {
                        var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                        model.ErrorMessage = result1.Message;
                        FormsAuthentication.SignOut();
                        CurrentSession.ClearSessions();
                    }
                    #endregion
                }
                else if (subUsercon == "MIN")
                {
                    #region login through Adhaar ID

                    ServiceAdaptor adapter = new ServiceAdaptor();

                    // string ePassword = ServiceAdaptor.ComputeHash(correctPassword, "SHA1", null);
                    string correctPassword = RemoveSalt(model.ActualPassword);
                    mUsers user = new mUsers();
                    user.AadarId = model.UserName;
                    CurrentSession.AadharId = user.AadarId;
                    user.mUserList = (ICollection<mUsers>)Helper.ExecuteService("User", "GetDetailsByadhaarID", user);

                    if (user.mUserList != null && user.mUserList.Count > 0)
                    {
                        foreach (var list in user.mUserList)
                        {
                            if (ServiceAdaptor.VerifyHash(correctPassword, "SHA1", list.Password) == true)
                            {
                                var result = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = true });
                                if (result.IsValid)
                                {
                                    if (saveUserName)
                                    {
                                        HttpCookie cookie = new HttpCookie("userAadharID");
                                        cookie.Value = Encrypt(user.AadarId + Guid.NewGuid().ToString().Substring(0, 5));
                                        cookie.HttpOnly = true;
                                        cookie.Expires =DateTime.Now.AddDays(-1);
                                        // cookie.Secure = true;
                                        //cookie.Expires = DateTime.Now.AddDays(10);
                                        HttpContext.Response.Cookies.Remove("userAadharID");
                                        HttpContext.Response.SetCookie(cookie);
                                    }
                                    else
                                    {
                                        if (Request.Cookies["userAadharID"] != null)
                                        {
                                            var c = new HttpCookie("userAadharID");
                                            c.Expires =DateTime.Now.AddDays(-1);
                                            c.HttpOnly = true;

                                            Response.Cookies.Add(c);
                                        }


                                    }
                                    // set status session var as successful
                                    CurrentSession.LoginStatus = "Successful";

                                    // Avoid Session Hijacking

                                    string _browserInfo = Request.Browser.Browser
                                    + Request.Browser.Version
                                    + Request.UserAgent + "~"
                                    + Request.ServerVariables["REMOTE_ADDR"];

                                    string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                            + DateTime.Now.Ticks + "^"
                                                            + _browserInfo + "^"
                                                            + System.Guid.NewGuid();

                                    byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                                    string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                                    if (Request.Cookies["AuthToken"] != null)
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        c.Expires =DateTime.Now.AddDays(-1);
                                        Response.SetCookie(c);
                                    }
                                    else
                                    {
                                        var c = new HttpCookie("AuthToken");
                                        c.Value = _newEncryptedString;
                                        if (!Request.IsLocal)
                                        {
                                            c.Secure = true;
                                        }
                                        c.Expires =DateTime.Now.AddDays(-1);
                                        Response.Cookies.Add(c);
                                    }
                                    Session["encryptedSession"] = _newEncryptedString;





                                    model.UserName = list.UserName;
                                    FormsAuthentication.SetAuthCookie(list.UserName, false);
                                    CurrentSession.UserID = list.UserId.ToString();
                                    CurrentSession.UserName = list.UserName;
                                    CurrentSession.SubUserTypeID = list.UserType.ToString();
                                    CurrentSession.IsMember = list.IsMember;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();

                                    CurrentSession.OfficeLevel = list.OfficeLevel;
                                    CurrentSession.OfficeId = list.OfficeId.ToString();
                                    if (list.DeptId != null && list.DeptId != "")
                                    {
                                        CurrentSession.DeptID = Convert.ToString(list.DeptId);
                                    }
                                    if (list.SubdivisionCode != null && list.SubdivisionCode != "")
                                    {
                                        CurrentSession.SubDivisionId = Convert.ToString(list.SubdivisionCode);
                                    }
                                    SetSessions();


                                    SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                                    if (!string.IsNullOrEmpty(returnUrl))
                                    {
                                        return Redirect(returnUrl);
                                    }
                                    else
                                    {
                                        data = true;
                                        var res = Json(data, JsonRequestBehavior.AllowGet);
                                        //return res;
                                        return RedirectToAction("Index", "Home");
                                    }
                                }
                                else
                                {
                                    model.ErrorMessage = result.Message;
                                    FormsAuthentication.SignOut();
                                    CurrentSession.ClearSessions();
                                }
                            }
                            else
                            {
                                var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                                model.ErrorMessage = result1.Message;
                                FormsAuthentication.SignOut();
                                CurrentSession.ClearSessions();
                            }
                        }
                    }
                    else
                    {
                        var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                        model.ErrorMessage = result1.Message;
                        FormsAuthentication.SignOut();
                        CurrentSession.ClearSessions();
                    }
                    #endregion
                }

                else
                {

                    #region  login through admin, super admin

                    ServiceAdaptor login = new ServiceAdaptor();
                    CurrentSession.LoginTime = DateTime.Now;
                    string correctPassword = RemoveSalt(model.ActualPassword);
                    string usrid = login.GetUserLoginData(model.UserName, correctPassword);
                    Guid usrIdd = new Guid();
                    if ((!string.IsNullOrWhiteSpace(usrid)) && (0 != string.Compare(usrid.Trim(), Guid.Empty.ToString(), true)) && (Guid.TryParse(usrid, out usrIdd)))
                    {

                        var result = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptAdminLoginData", new mUsers { UserName = model.UserName, IsAuthorized = true });
                        if (result.IsValid)
                        {
                            // set status session var as successful
                            CurrentSession.LoginStatus = "Successful";

                            // Avoid Session Hijacking

                            string _browserInfo = Request.Browser.Browser
                            + Request.Browser.Version
                            + Request.UserAgent + "~"
                            + Request.ServerVariables["REMOTE_ADDR"];

                            string _sessionValue = Convert.ToString(Session["UserId"]) + "^"
                                                    + DateTime.Now.Ticks + "^"
                                                    + _browserInfo + "^"
                                                    + System.Guid.NewGuid();

                            byte[] _encodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(_sessionValue);
                            string _newEncryptedString = System.Convert.ToBase64String(_encodeAsBytes);

                            if (Request.Cookies["AuthToken"] != null)
                            {
                                var c = new HttpCookie("AuthToken");
                                c.Value = _newEncryptedString;
                                if (!Request.IsLocal)
                                {
                                    c.Secure = true;
                                }
                                c.Expires =DateTime.Now.AddDays(-1);
                                Response.SetCookie(c);
                            }
                            else
                            {
                                var c = new HttpCookie("AuthToken");
                                c.Value = _newEncryptedString;
                                if (!Request.IsLocal)
                                {
                                    c.Secure = true;
                                }
                                c.Expires =DateTime.Now.AddDays(-1);
                                Response.Cookies.Add(c);
                            }
                            Session["encryptedSession"] = _newEncryptedString;
                            FormsAuthentication.SetAuthCookie(model.UserName, false);
                            CurrentSession.UserID = usrid;
                            CurrentSession.UserName = model.UserName;
                            if (CurrentSession.UserName == "admin")
                            {
                                CurrentSession.AadharId = "999999999999";
                            }
                            CurrentSession.LoginDepartmentName = model.LoginDepartmentName;
                            CurrentSession.SubUserTypeID = "1";
                            // CurrentSession.AadharId=model.ad
                            SetSessions();
                            SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                            if (!string.IsNullOrEmpty(returnUrl))
                            {
                                return Redirect(returnUrl);
                            }
                            else
                            {
                                data = true;
                                var res = Json(data, JsonRequestBehavior.AllowGet);
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            model.ErrorMessage = result.Message;
                            FormsAuthentication.SignOut();
                            CurrentSession.ClearSessions();
                        }
                    }
                    else
                    {
                        var result1 = (LoginMessage)Helper.ExecuteService("User", "WrongAttemptAdminLoginData", new mUsers { UserName = model.UserName, IsAuthorized = false });
                        model.ErrorMessage = result1.Message;
                        FormsAuthentication.SignOut();
                        CurrentSession.ClearSessions();
                    }
                    #endregion

                }
                FormsAuthentication.SignOut();
                CurrentSession.ClearSessions();
                return View("Login", model);

            }

#pragma warning disable CS0168 // The variable 'exception' is declared but never used
            catch (Exception exception)
#pragma warning restore CS0168 // The variable 'exception' is declared but never used
            {
                ViewBag.HasError = true;
            }
            SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();
            data = false;
            var res2 = Json(data, JsonRequestBehavior.AllowGet);
            return res2;
        }

        public string abc(string str)
        {
            var aa = GetGeneratedSalt();
            StringBuilder sb = new StringBuilder();
            for (int i = 1; i < str.Length; i += 2)
            {
                sb.Append(str[i]);
            }

            string strNew = sb.ToString();

            StringBuilder sb1 = new StringBuilder();
            for (int i = 0; i < strNew.Length; i += 2)
            {
                int value = Convert.ToInt32(strNew.Substring(i, 2), 16);
                sb1.Append(Char.ConvertFromUtf32(value));
            }

            string str64 = sb1.ToString();
            byte[] hash = Convert.FromBase64String(str64);
            string getHash = System.Text.Encoding.Default.GetString(hash);
            return getHash;
        }
        public static string RemoveSalt(string str)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 1; i < str.Length; i += 2)
                {
                    sb.Append(str[i]);
                }

                string strNew = sb.ToString();

                StringBuilder sb1 = new StringBuilder();
                for (int i = 0; i < strNew.Length; i += 2)
                {
                    int value = Convert.ToInt32(strNew.Substring(i, 2), 16);
                    sb1.Append(Char.ConvertFromUtf32(value));
                }

                string str64 = sb1.ToString();
                byte[] hash = Convert.FromBase64String(str64);
                string getHash = System.Text.Encoding.Default.GetString(hash);
                return getHash;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
            }
            return "";
        }

        [HttpPost]
        [SBLAuthorize(Allow = "All")]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon(); //ashwani
            CurrentSession.ClearSessions();
            SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();
                         HttpCookie reqCookies = Request.Cookies["Login"];
                         if (reqCookies != null)
                         {
                             HttpCookie nameCookie = Request.Cookies["Login"];
                             nameCookie.Expires =DateTime.Now.AddDays(-1);
                             Response.Cookies.Add(nameCookie);
                         }
        
            return RedirectToAction("LoginUP", "Account");
        }

        [SBLAuthorize(Allow = "All")]
        public ActionResult DigitalSignatureLogin(string UserName, string uid)
        {
            UserName = Sanitizer.GetSafeHtmlFragment(UserName);
            uid = Sanitizer.GetSafeHtmlFragment(uid);

            CurrentSession.ClearSessions();
            string returnUrl = "";
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@transtype", "DSC"));
            methodParameter.Add(new KeyValuePair<string, string>("@uid", UserName));
            methodParameter.Add(new KeyValuePair<string, string>("@errorCode", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@status", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@timeStamp", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@transactionId", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@code", ""));
            methodParameter.Add(new KeyValuePair<string, string>("@info", uid));
            DataSet returnval = ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[InsertUserAuthLog]", methodParameter);
            if (returnval.Tables[0].Rows.Count > 0)
            {
                CurrentSession.UserID = returnval.Tables[0].Rows[0][0].ToString();
                CurrentSession.UserName = UserName;

                FormsAuthentication.SetAuthCookie(CurrentSession.UserName, false);
                SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                                && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    return RedirectToAction("Index", "Home", new { area = "" });
                }
            }

            SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();
            ModelState.AddModelError("", "Login credentials are not Correct");
            return RedirectToAction("Index", "Home", new { area = "" });
        }

        [SBLAuthorize(Allow = "Authenticated")]
        public ActionResult RegisterDSC()
        {
            ViewBag.Message = "DSC ";
            return View();
        }

        [HttpPost]
        [SBLAuthorize(Allow = "Authenticated")]
        [ValidateAntiForgeryToken]
        public ActionResult RegisterDSC(string uid)
        {
            //insert to db with userid
            uid = Sanitizer.GetSafeHtmlFragment(uid);
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@userid", CurrentSession.UserID));
            methodParameter.Add(new KeyValuePair<string, string>("@dschash", uid));
            byte[] returnval = ServiceAdaptor.GetbyteFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "[dbo].[UpdateDscHash]", methodParameter);
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        [SBLAuthorize(Allow = "All")]
        public ActionResult LoginDSC()
        {
            return PartialView();
        }


        [SBLAuthorize(Allow = "All")]
        public ActionResult LoginBiometric()
        {
            return PartialView();
        }

        #region Helpers

        [SBLAuthorize(Allow = "All")]
        private ActionResult RedirectToLocal(string returnUrl)
        {
            var data = "returnUrl";
            var res2 = Json(data, JsonRequestBehavior.AllowGet);
            return res2;
        }



        #endregion

        public ActionResult LogOn()
        {
            return PartialView("_RedirectToLogin");
        }

        #region ForgetPassword

        public ActionResult ForgetPassword()
        {
            ForgetPassword fp = new ForgetPassword();

            if (TempData["Failed"] != null)
            {
                fp.CaptchaMessage = TempData["Failed"].ToString();
            }
            else
            {
                fp.CaptchaMessage = "";
            }
            return View(fp);
        }

        public ActionResult GetOTP(string aadharID)
        {
            tOTPDetails otp = new tOTPDetails();
            mUsers user = new mUsers();
            user.AadarId = aadharID;
            user = (mUsers)Helper.ExecuteService("User", "CheckAdhaarID", user);



            if (user != null)
            {
                string _AadharNo = user.AadarId;
                var _ObjmMember = (mMember)Helper.ExecuteService("Member", "GetMemberByAadharID", _AadharNo);

                string OTP = GenerateOTPNmuber();
                otp.OTP = OTP;
                if (user.IsMember == "true" || user.IsMember == "True")
                {
                    if (_ObjmMember != null)
                    {
                        otp.MobileNo = _ObjmMember.Mobile;
                    }
                    else
                    {
                        otp.MobileNo = user.MobileNo;
                    }
                }
                else
                {
                    otp.MobileNo = user.MobileNo;
                }
                otp.UserName = user.UserName;
                otp = (tOTPDetails)Helper.ExecuteService("OTPDetails", "AddOTP", otp);


                try
                {
                    SMSService Services = new SMSService();
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = "Your eVidhan OTP is " + otp.OTP + " .";
                    smsMessage.ModuleActionID = 2;
                    smsMessage.UniqueIdentificationID = 2;
                    if (user.IsMember == "true" || user.IsMember == "True")
                    {
                        if (_ObjmMember != null)
                        {
                            smsMessage.MobileNo.Add(_ObjmMember.Mobile);
                        }

                        else
                        {
                            smsMessage.MobileNo.Add(user.MobileNo);
                        }
                    }

                    else
                    {
                        smsMessage.MobileNo.Add(user.MobileNo);
                    }

                    Notification.Send(true, false, smsMessage, null);

                    //Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", user.MobileNo, "Your eVidhan OTP is " + otp.OTP + " .");

                }
                catch
                {

                }
            }
            return PartialView("_OTP", otp);
        }

        public string GenerateOTPNmuber()
        {


            string OTP = GetUniqueKey();


            return OTP;


        }

        public static string GetUniqueKey()
        {
            int maxSize = 6; // whatever length you want
            // char[] chars = new char[62];
            string a;
            a = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            char[] chars = new char[a.Length];
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

        public static string GeneratePassword()
        {
            int maxSize = 8; // whatever length you want
            // char[] chars = new char[62];
            string a;
            a = "1qaz2wsx3edc4rfv5tgb6yhn7ujm8ik9ol";
            char[] chars = new char[a.Length];
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - 1)]);
            }
            return result.ToString();
        }

        public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        {
            // If salt is not specified, generate it on the fly.
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                int minSaltSize = 4;
                int maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                Random random = new Random();
                int saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];


            HashAlgorithm hash;
            // Make sure hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hash = new SHA1Managed();
                    break;

                case "SHA256":
                    hash = new SHA256Managed();
                    break;

                case "SHA384":
                    hash = new SHA384Managed();
                    break;

                case "SHA512":
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }
        [HttpPost]
        [CaptchaVerify("Captcha Result is not valid")]
        public ActionResult GetForgetPassword(ForgetPassword model, string CaptchaText)
        {
            string ss = Request.Form["CaptchaText1"];
            if (ModelState.ContainsKey("OTP"))
                ModelState["OTP"].Errors.Clear();

            if (Session["CaptchaImageText"] != null)
            {
                if (this.Session["CaptchaImageText"].ToString() != CaptchaText)
                {
                    model.CaptchaMessage = "Captcha Validation Failed!";
                    model.DOB = null;
                    model.OTP = null;
                    model.AadharID = null;
                    return View("ForgetPassword", model);
                }
            }
            mUsers user = new mUsers();
            user.AadarId = model.AadharID;
            user.DOB = model.DOB;
            user = (mUsers)Helper.ExecuteService("User", "GetForgetPassword", user);

            if (user != null)
            {
                string mobileNo = "";
                if (user.IsMember == "true" || user.IsMember == "True")
                {
                    string _AadharNo = user.AadarId;
                    var _ObjmMember = (mMember)Helper.ExecuteService("mMember", "GetMemberByAadharID", _AadharNo);

                    if (_ObjmMember != null)
                    {
                        mobileNo = _ObjmMember.Mobile;
                    }
                    else
                    {
                        mobileNo = user.MobileNo;
                    }
                }
                else
                {
                    mobileNo = user.MobileNo;
                }
                //string password = GeneratePassword();
                // string password = Membership.GeneratePassword(8,0);
                TempUser tempuser = new TempUser();
                tempuser = (TempUser)Helper.ExecuteService("User", "CheckPasswordIntemp", user);
                if (tempuser == null || tempuser.password == null || tempuser.password == "")
                {
                    string password = GeneratePassword();
                    string ePassword = ComputeHash(password, "SHA1", null);
                    user.Password = ePassword;
                    user.PasswordString = password;
                    bool flag = false;
                    flag = (bool)Helper.ExecuteService("User", "ChangePassword", user);
                    SMSService Services = new SMSService();
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = "Your new Password for eVidhan is " + password + " .";
                    //Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", mobileNo, "Your new Password for eVidhan is " + password + "");
                    smsMessage.ModuleActionID = 2;
                    smsMessage.UniqueIdentificationID = 2;
                    smsMessage.MobileNo.Add(mobileNo);
                    Notification.Send(true, false, smsMessage, null);
                }
                else
                {
                    SMSService Services = new SMSService();
                    SMSMessageList smsMessage = new SMSMessageList();
                    smsMessage.SMSText = "Your Password for eVidhan is " + tempuser.password + " .";
                    //Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", mobileNo, "Your new Password for eVidhan is " + tempuser.password + "");
                    smsMessage.ModuleActionID = 2;
                    smsMessage.UniqueIdentificationID = 2;
                    smsMessage.MobileNo.Add(mobileNo);
                    Notification.Send(true, false, smsMessage, null);
                }



                // Services.sendSingleSMS("hpgovt", "hpdit@1234", "hpgovt", mobileNo, "Your new Password for eVidhan is " + password + " .");
                TempData["Success"] = "Success";
                return RedirectToAction("LoginUP");
            }
            else
            {
                TempData["Failed"] = "Login Failed,Invalid UserId or Password";
                return RedirectToAction("ForgetPassword");
            }
        }

        #endregion
        [HttpPost]
   
        public ActionResult GetGeneratedSalt()
        {
            try
            {
                //var Salt = (string)Helper.ExecuteService("User", "GetGeneratedSalt", null);
                var Salt = ExtensionMethods.GetGeneratedSalt();
                return Json(Salt, JsonRequestBehavior.AllowGet);
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                throw;
            }

        }

        public string Encrypt(string text, string pwd = "qwerty12345")
        {
            byte[] originalBytes = Encoding.UTF8.GetBytes(text);
            byte[] encryptedBytes = null;
            byte[] passwordBytes = Encoding.UTF8.GetBytes(pwd);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            // Generating salt bytes
            byte[] saltBytes = GetRandomBytes();

            // Appending salt bytes to original bytes
            byte[] bytesToBeEncrypted = new byte[saltBytes.Length + originalBytes.Length];
            for (int i = 0; i < saltBytes.Length; i++)
            {
                bytesToBeEncrypted[i] = saltBytes[i];
            }
            for (int i = 0; i < originalBytes.Length; i++)
            {
                bytesToBeEncrypted[i + saltBytes.Length] = originalBytes[i];
            }

            encryptedBytes = AES_Encrypt(bytesToBeEncrypted, passwordBytes);

            return Convert.ToBase64String(encryptedBytes);
        }

        public byte[] GetRandomBytes()
        {
            int _saltSize = 4;
            byte[] ba = new byte[_saltSize];
            RNGCryptoServiceProvider.Create().GetBytes(ba);
            return ba;
        }

        public byte[] AES_Decrypt(byte[] bytesToBeDecrypted, byte[] passwordBytes)
        {
            byte[] decryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeDecrypted, 0, bytesToBeDecrypted.Length);
                        cs.Close();
                    }
                    decryptedBytes = ms.ToArray();
                }
            }

            return decryptedBytes;
        }

        public byte[] AES_Encrypt(byte[] bytesToBeEncrypted, byte[] passwordBytes)
        {
            byte[] encryptedBytes = null;

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8 };

            using (MemoryStream ms = new MemoryStream())
            {
                using (RijndaelManaged AES = new RijndaelManaged())
                {
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(passwordBytes, saltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(bytesToBeEncrypted, 0, bytesToBeEncrypted.Length);
                        cs.Close();
                    }
                    encryptedBytes = ms.ToArray();
                }
            }

            return encryptedBytes;
        }

        public string Decrypt(string decryptedText, string pwd = "qwerty12345")
        {
            byte[] bytesToBeDecrypted = Convert.FromBase64String(decryptedText);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(pwd);

            // Hash the password with SHA256
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);

            byte[] decryptedBytes = AES_Decrypt(bytesToBeDecrypted, passwordBytes);

            // Getting the size of salt
            int _saltSize = 4;

            // Removing salt bytes, retrieving original bytes
            byte[] originalBytes = new byte[decryptedBytes.Length - _saltSize];
            for (int i = _saltSize; i < decryptedBytes.Length; i++)
            {
                originalBytes[i - _saltSize] = decryptedBytes[i];
            }

            return Encoding.UTF8.GetString(originalBytes);
        }
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")] // This is for output cache false
        public FileResult GetCaptchaImage()
        {
            CaptchaRandomImage CI = new CaptchaRandomImage();
            this.Session["CaptchaImageText"] = GetCaptchaText(); // here 5 means I want to get 5 char long captcha
            //CI.GenerateImage(this.Session["CaptchaImageText"].ToString(), 300, 75);
            // Or We can use another one for get custom color Captcha Image 
            CI.GenerateImage(this.Session["CaptchaImageText"].ToString(), 250, 55, Color.DarkGray, Color.White);
            MemoryStream stream = new MemoryStream();
            CI.Image.Save(stream, ImageFormat.Png);
            stream.Seek(0, SeekOrigin.Begin);
            return new FileStreamResult(stream, "image/png");
        }
        public string GetCaptchaText()
        {
            StringBuilder builder = new StringBuilder();
            // builder.Append(RandomString(5, true));
            builder.Append(RandomNumber(10000, 99999));
            // builder.Append(RandomString(2, false));
            return builder.ToString();
        }

        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }


    }

}
