﻿using SBL.eLegislator.HPMS.ServiceAdaptor;
using SBL.eLegislator.HPMS.ServiceAdaptor.Menu;
using SBL.eLegistrator.HouseController.Web.Utility;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/
        public ActionResult MainMenu(string userId)
        {
            MenuItem menu = ServiceAdaptor.GetMenuList(CurrentSession.UserID, CurrentSession.LanguageID);
            return PartialView(menu);
        }
    }
}
