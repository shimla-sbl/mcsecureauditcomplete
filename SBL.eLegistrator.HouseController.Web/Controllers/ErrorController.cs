﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/
        public void Clearsession()
        {

            //Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            //Response.Cache.SetValidUntilExpires(false);
            //Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.Cache.SetNoStore();
            //Response.Cache.AppendCacheExtension("no-cache");
            //Response.Expires = 0;
            //Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
            //System.Web.Security.FormsAuthentication.SignOut();
            //Session.Clear();
            //Session.Abandon();
            //Session.RemoveAll();
            //HttpCookie aCookie;
            //string cookieName;
            //int limit = Request.Cookies.Count;
            //for (int i = 0; i < limit; i++)
            //{
            //    cookieName = Request.Cookies[i].Name;
            //    aCookie = new HttpCookie(cookieName);
            //    aCookie.Expires = DateTime.Now.AddDays(-1);
            //    Response.Cookies.Add(aCookie);
            //}

            ////if (Request.Cookies["god"] != null)
            ////{
            ////    Response.Cookies["god"].Value = string.Empty;
            ////    Response.Cookies["god"].Expires = DateTime.Now.AddMonths(-20);
            ////}

            //if (Request.Cookies["__AntiXsrfToken"] != null)
            //{
            //    Response.Cookies["__AntiXsrfToken"].Value = string.Empty;
            //    Response.Cookies["__AntiXsrfToken"].Expires = DateTime.Now.AddMonths(-20);
            //}
            //if (Request.Cookies["AuthToken"] != null)
            //{
            //    Response.Cookies["AuthToken"].Value = string.Empty;
            //    Response.Cookies["AuthToken"].Expires = DateTime.Now.AddMonths(-20);
            //}
        }
        public ActionResult AccessDenied()
        {
            Clearsession();
            return View("AccessDenied");
        }
        //public ActionResult Index(int statusCode, Exception exception, string returnurl)
        //{
        //    Response.StatusCode = statusCode;

        //    ViewBag.returnurl = returnurl;
        //    return View();
        //}

        public ActionResult Index()
        {
            Clearsession();
            ViewBag.ReturnUrl= GetSite123Url() + "/" + Request.QueryString["aspxerrorpath"];
            return View();
        }

        public ActionResult NotFound()
        {
            Clearsession();
            return View();
        }
        public ActionResult BadRequest()
        {
            Clearsession();
            return View();
        }
        public ActionResult SessionTimedOut()
        {
            Clearsession();
            return View();
        }
        public string GetSite123Url()
        {
            string protocol;

            if (HttpContext.Request.IsSecureConnection)
                protocol = "https";
            else
                protocol = "http";

            StringBuilder uri = new StringBuilder(protocol + "://");

            string hostname = HttpContext.Request.Url.Host;

            uri.Append(hostname);

            int port = HttpContext.Request.Url.Port;

            if (port != 80 && port != 443)
            {
                uri.Append(":");
                uri.Append(port.ToString());
            }

            return uri.ToString();
        }
    }
}
