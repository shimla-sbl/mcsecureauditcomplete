﻿using SBL.DomainModel.Models.SiteSetting;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SBL.eLegistrator.HouseController.Web.Controllers
{
    public class WebCamLiveController : Controller
    {
        static string CapturedImage = "";

        public string returnPicLocation()
        {
            string picLoc = CapturedImage;
            return picLoc;
        }



        public void Capture(string filelocation)
        {
            var stream = Request.InputStream;
            string dump;

            using (var reader = new StreamReader(stream))
                dump = reader.ReadToEnd();

            string picName = GetPicNameFromPath(filelocation);


            Guid PicName;
            if (string.IsNullOrEmpty(picName))
            {
                PicName = Guid.NewGuid();
            }
            else
            {
                PicName = new Guid(picName);
            }

            SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
            session.AssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
            session.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);

            string Url = "/Images/Pass/Photo/" + session.AssemblyID + "/" + session.SessionCode + "/";
            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }
            var path = Server.MapPath(Url) + PicName + ".jpg";

            string TempUrl = "/Images/Pass/Photo/" + session.AssemblyID + "/" + session.SessionCode + "/" + "Temp/";
            DirectoryInfo TempDir = new DirectoryInfo(Server.MapPath(TempUrl));
            if (!TempDir.Exists)
            {
                TempDir.Create();
            }
            var TempPath = Server.MapPath(TempUrl) + PicName + ".jpg";

            System.IO.File.WriteAllBytes(TempPath, String_To_Bytes(dump));
            CropImage(path, TempPath, 70, 10, 180, 230);

            CapturedImage = Url + PicName + ".jpg";
        }

        public static void CropImage(string Path, string TempPath, int x, int y, int width, int height)
        {
            Image source = Image.FromFile(TempPath);

            Rectangle crop = new Rectangle(x, y, width, height);

            var bmp = new Bitmap(crop.Width, crop.Height);
            using (var gr = Graphics.FromImage(bmp))
            {
                gr.DrawImage(source, new Rectangle(0, 0, bmp.Width, bmp.Height), crop, GraphicsUnit.Pixel);
            }

            bmp.Save(Path);
        }

        private byte[] String_To_Bytes(string strInput)
        {
            int numBytes = (strInput.Length) / 2;
            byte[] bytes = new byte[numBytes];

            for (int x = 0; x < numBytes; ++x)
            {
                bytes[x] = Convert.ToByte(strInput.Substring(x * 2, 2), 16);
            }

            return bytes;
        }

        public string SavePicToDirectory(Stream stream, string pictureName)
        {
            string dump;
            using (var reader = new StreamReader(stream))
                dump = reader.ReadToEnd();

            Guid PicName;
            if (string.IsNullOrEmpty(pictureName))
            {
                PicName = Guid.NewGuid();
            }
            else
            {
                PicName = new Guid(pictureName);
            }
            SiteSettings siteSettingMod = new SiteSettings();
            siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);

            string Url = "/Images/Pass/Photo/" + Convert.ToInt16(siteSettingMod.AssemblyCode) + "/" + Convert.ToInt16(siteSettingMod.SessionCode) + "/";

            DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
            if (!Dir.Exists)
            {
                Dir.Create();
            }

            var path = Server.MapPath(Url) + PicName + ".jpg";
            System.IO.File.WriteAllBytes(path, String_To_Bytes(dump));

            //this.CompressPhoto(path);

            return Url + PicName + ".jpg";
        }

        public string CompressPhoto(string path)
        {
            ImageCompress imgCompress = ImageCompress.GetImageCompressObject;
            imgCompress.GetImage = new System.Drawing.Bitmap(path);
            imgCompress.Height = 160;
            imgCompress.Width = 120;

            //compress image according to the Pasport photograph.
            string imageName = Path.GetFileName(path);
            imgCompress.Save(imageName, path);

            return path;
        }

        public string GetPicNameFromPath(string filePath)
        {
            if (!string.IsNullOrEmpty(filePath))
            {
                string result = string.Empty;
                result = Path.GetFileNameWithoutExtension(Server.MapPath(filePath));
                return result;
            }
            else
            {
                return "";
            }
        }

        [HttpPost]
        public string UploadPhoto(HttpPostedFileBase file, string filelocation)
        {
            if (file != null)
            {
                var stream = file.InputStream;
                string picName = GetPicNameFromPath(filelocation);

                //var picLocation = SavePicToDirectory(stream, picName);

                string extension = System.IO.Path.GetExtension(file.FileName);

                SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                session.AssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
                session.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);

                // var FileSettings = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetDISFileSetting", null);
                // var PassFileLocalServerPath = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "PassFileLocalPath", null);
                // var PassFileLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "PassFileLocation", null);
                // var GetSecureFileSettingLocation = (SBL.DomainModel.Models.SiteSetting.SiteSettings)Helper.ExecuteService("SiteSetting", "GetSecureFileSettingLocation", null);
                string urlPath = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImageLocation(session.AssemblyID, session.SessionCode);
                string Url = urlPath;
                string baseTemp = urlPath + "Temp" + "/";
                DirectoryInfo Dir = new DirectoryInfo(Url);
                DirectoryInfo Dir1 = new DirectoryInfo(baseTemp);
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                if (!Dir1.Exists)
                {
                    Dir1.Create();
                }
                Guid PicName;
                PicName = Guid.NewGuid();
                string path = System.IO.Path.Combine(Url, PicName + ".jpg");
                string Temp = System.IO.Path.Combine(baseTemp, PicName + ".jpg");

                SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions sdf = new SBL.eLegistrator.HouseController.Web.Extensions.ImageResizerExtensions(180);
                file.SaveAs(Temp);
                sdf.Resize(Temp, path);
                System.IO.File.Delete(Temp);

                string showUrl = SBL.eLegistrator.HouseController.Web.Areas.PublicPasses.Extensions.ExtensionMethods.GetImagePath(session.AssemblyID, session.SessionCode, PicName + ".jpg");
                // var picLocation = Url + PicName + ".jpg";
                var picLocation = showUrl + "," + PicName + ".jpg";
                return picLocation;
            }
            return null;
        }
        [HttpPost]
        public string UploadPhotos(HttpPostedFileBase file, string filelocation)
        {
            if (file != null)
            {
                var stream = file.InputStream;
                string picName = GetPicNameFromPath(filelocation);

                //var picLocation = SavePicToDirectory(stream, picName);


                string extension = System.IO.Path.GetExtension(file.FileName);

                SBL.DomainModel.Models.Session.mSession session = new SBL.DomainModel.Models.Session.mSession();
                SiteSettings siteSettingMod = new SiteSettings();
                siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                session.AssemblyID = Convert.ToInt16(siteSettingMod.AssemblyCode);
                session.SessionCode = Convert.ToInt16(siteSettingMod.SessionCode);

                string Url = "/Images/Pass/Photo/" + session.AssemblyID + "/" + session.SessionCode + "/";
                DirectoryInfo Dir = new DirectoryInfo(Server.MapPath(Url));
                if (!Dir.Exists)
                {
                    Dir.Create();
                }
                Guid PicName;
                PicName = Guid.NewGuid();
                string path = System.IO.Path.Combine(Server.MapPath("~" + Url), PicName + ".jpg");
                file.SaveAs(path);

                var picLocation = Url + PicName + ".jpg";
                return picLocation;
            }
            return null;
        }
    }
}
