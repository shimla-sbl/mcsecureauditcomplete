﻿using Newtonsoft.Json;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Filters;
using SBL.eLegistrator.HouseController.Web.Filters.Security;
using SBL.eLegistrator.HouseController.Web.Helpers;
using SBL.eLegistrator.HouseController.Web.Models;
using SBL.eLegistrator.HouseController.Web.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Question;

namespace SBL.eLegistrator.HouseController.Web.Controllers
{
    //[Audit]
    // [Authorize]
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [NoCache]
        public ActionResult Index()
        {
            //added by durgesh for error handling and logging testing only
            //long i = Convert.ToInt64("e");
            //if (!string.IsNullOrEmpty(CurrentSession.UserUpdateFlag) || CurrentSession.UserID.ToString() == "e7e83032-1cb0-41ea-bca8-ffdedd859173")
            //{
            if (CurrentSession.UserName != "")
            {
                string UserName = CurrentSession.UserName;
                string RoleName = CurrentSession.RoleName;
                string RoleId = CurrentSession.RoleID;
                string UserID = CurrentSession.UserID.ToString();
                string SubUserTypeID = Convert.ToString(CurrentSession.SubUserTypeID);
                string officeId = Convert.ToString(CurrentSession.OfficeId);
                #region OldAuth

                switch (RoleId)
                {
                    case "1fca5265-162b-4bf3-8926-70a9c1548095":
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });

                    case "d457778d-b3e2-4a1f-a522-203ec7b09608":
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be997":
                        return RedirectToAction("Index", "Reporters", new { area = "Reporters" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be998":
                        return RedirectToAction("Index", "Media", new { area = "Media" });
                    /// Translator for LegislationFixation
                    case "c4c92e7c-3d31-4cc7-bdce-8ded55e77b99":
                        return RedirectToAction("Index", "Changer", new { area = "Notices" });
                    /// Secretary for LegislationFixation
                    case "f601de42-22f5-4b77-92b2-cd506564da25":
                        return RedirectToAction("PaperLaidSummary", "LegislationFixation", new { area = "Notices" });
                    /// Legislation Emp for LegislationFixation
                    case "e5144dad-7a5d-4308-976a-8a5d2a462bd1":
                        return RedirectToAction("PaperLaidSummary", "LegislationFixation", new { area = "Notices" });
                    /// For Committee
                    case "c4c92e7c-3d31-4cc7-bdce-8ded55e77b94":
                        return RedirectToAction("CommitteeDashboard", "CommitteePaper", new { area = "CommitteePaperLaid" });
                    /// For Diary
                    case "5e023679-e59e-407d-b487-9df5c0faae47":
                        return RedirectToAction("DiariesDashboard", "Diaries", new { area = "Notices" });
                    /// For Create CommitteeXML
                    case "0d22a8f9-2bfc-4f7b-8963-4d8ce230d173":
                        return RedirectToAction("CreateLOB", "CommitteeXML", new { area = "Committee" });
                    /// For Create LOB
                    case "42b80446-fbbb-4ed1-a9e9-8d43c8cafb80":
                        return RedirectToAction("CreateLOB", "ListOfBusiness", new { area = "ListOfBusiness" });
                    /// for approval of LOB
                    case "139dd5da-1bee-4bac-af7d-02cd53c82632":
                        return RedirectToAction("Approval", "ListOfBusiness", new { area = "ListOfBusiness" });
                    ///for create Speaker Pad
                    case "ea86388d-43f7-46ea-9201-47929da4733e":
                        return RedirectToAction("SpeakerPad", "Speaker", new { area = "ListOfBusiness" });
                    ///For approval of Speaker Pad
                    case "00ed74e6-1ea4-41ed-ad78-90049d4508ff":
                        return RedirectToAction("ApprovalSpeakerPad", "Speaker", new { area = "ListOfBusiness" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be999":
                        return RedirectToAction("Index", "AdministrationBranch", new { area = "AdministrationBranch" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be994":
                        return RedirectToAction("Index", "PublicPass", new { area = "PublicPasses" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be100":
                        return RedirectToAction("Index", "SuperAdmin", new { area = "SuperAdmin" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba111":
                        return RedirectToAction("Index", "DashBoardAccountsAdmin", new { area = "AccountsAdmin" });//Module done by Robin--AccountsAdmin
                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba112":
                        return RedirectToAction("Index", "DashBoardDisbursementOfficer", new { area = "DisbursementOfficer" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba113":
                        return RedirectToAction("Index", "DashBoardBudgetSuperindent", new { area = "BudgetSuperindent" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba114":
                        return RedirectToAction("Index", "AccountDashBoard", new { area = "Accounts" });//Module done by durgesh----BillClerk
                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba115":
                        return RedirectToAction("AllMembersAccountDetails", "MemberAccountDetails", new { area = "SalaryClerks" });//Module - by robin as Salary clerk
                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba116":
                        return RedirectToAction("Index", "DashBoardCashier", new { area = "Cashier" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba117":
                        return RedirectToAction("Index", "DashBoardLoanDealingOfficer", new { area = "LoanDealingOfficer" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba118":
                        return RedirectToAction("Index", "DashBoardPensionClerk", new { area = "PensionClerk" });

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708ba119":
                        return RedirectToAction("Index", "PublicPass", new { area = "PublicPasses" });//for public- Admin

                    //Part by robin 
                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be150":
                        Session["LibraryRole"] = SBL.DomainModel.Models.Enums.LibraryRoleType.DocumentationOfficer.GetHashCode();
                        return RedirectToAction("Index", "LibraryDashBoard", new { area = "Library" });//Library Part ----Documentation Officer

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be151":
                        Session["LibraryRole"] = SBL.DomainModel.Models.Enums.LibraryRoleType.Librarion.GetHashCode();
                        return RedirectToAction("Index", "LibraryDashBoard", new { area = "Library" });//Library Part ----Librarion

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be152":
                        Session["LibraryRole"] = SBL.DomainModel.Models.Enums.LibraryRoleType.LibrarionClerk.GetHashCode();
                        return RedirectToAction("Index", "LibraryDashBoard", new { area = "Library" });//Library Part ----Librarion Clerk

                    case "0fc60e41-c78e-46b0-b8ac-f6eb708be153":
                        Session["LibraryRole"] = SBL.DomainModel.Models.Enums.LibraryRoleType.SaleCounterClerk.GetHashCode();
                        return RedirectToAction("Index", "LibraryDashBoard", new { area = "Library" });//Library Part -----Sale Counter CLerk

                    case "0fc60e41-c78e-46b0-b8ac-f6eb709be999":
                        return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });//for Minister- Admin
                    case "0fc50e41-c78f-78b0-b8ac-f6eb709be999":  // Con User 0fc50e41-c78f-78b0-b8ac-f6eb709be999
                        var OId = (int)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        var MId = (int)Helper.ExecuteService("Grievance", "GetMemberByOfficeUser", CurrentSession.AadharId);
                        if (MId > 0)
                        {
                            CurrentSession.MemberCode = Convert.ToString(MId);
                        }
                        if (OId == Convert.ToInt32(officeId))
                        {

                            return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                        }
                        else
                        {

                            return RedirectToAction("Index", "Blank", new { area = "BlankRed" });
                        }

                    case "0fc60e41-c78e-49b0-b8ac-f6eb708be151":  // SDM-MLA Diary USer
                        return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                    //var OIdd = (int)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                    //var MIdd = (int)Helper.ExecuteService("Grievance", "GetMemberByOfficeUser", CurrentSession.AadharId);
                    //if (MIdd > 0)
                    //{
                    //    CurrentSession.MemberCode = Convert.ToString(MIdd);
                    //}
                    //if (OIdd == Convert.ToInt32(officeId))
                    //{

                    //    return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                    //}
                    //else
                    //{

                    //    return RedirectToAction("Index", "Blank", new { area = "BlankRed" });
                    //}
                    case "f8e0ef76-afe7-4e94-88f4-f399f967e37e":
                        //CommitteeChairpersonUserId();


                        tCommitteeMember cm = new tCommitteeMember();
                        tCommitteeModel Obj = new tCommitteeModel();

                        List<tCommitteeMember> AuthorisedMem = new List<tCommitteeMember>();

                        mUsers musr = new mUsers();

                        string userid = CurrentSession.UserName;
                        AuthorisedMem = (List<tCommitteeMember>)Helper.ExecuteService("Committee", "ShowCommitteeMemberByID", userid);

                        if (AuthorisedMem.Count == 0)
                        {
                            //return RedirectToAction("LoginUP", "Account", new { area = "" });
                            return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                        }
                        else
                        {
                            //return RedirectToAction("CommitteeDashboard", "CommitteePaperLiadChairperson", new { area = "CommitteePaperLiadChairperson" });
                            return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                        }

                    default:
                        break;
                }

                switch (RoleName)
                {
                    //case "Member":
                    //    return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    case "Vidhan Sabha Typist":
                        return RedirectToAction("Index", "NoticeDetails", new { area = "Notices" });

                    case "Vidhan Sabha Proof Reader":
                        return RedirectToAction("Index", "NoticeDetails", new { area = "Notices" });

                    case "PaperLaidDepartment":
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });

                    case "PaperLaidMinister":
                        return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });

                    default:
                        break;
                }

                switch (UserName)
                {
                    //case "10023":
                    //    return RedirectToAction("PaperLaidSummary", "LegislationFixation", new { area = "Notices" });
                    //case "10103":
                    //    return RedirectToAction("CommitteeDashboard", "CommitteePaper", new { area = "CommitteePaperLaid" });
                    //case "61":
                    //    return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                    //case "407":
                    //    return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                    //case "10014":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10031":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    case "10032":
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10033":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10034":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10035":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10036":
                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    //case "10041":
                    //case "10016":
                    //    return RedirectToAction("DiariesDashboard", "Diaries", new { area = "Notices" });
                    //case "10029":
                    //    return RedirectToAction("Index", "Reporters", new { area = "Reporters" });
                    //case "10011":
                    //    return RedirectToAction("PaperLaidSummary", "LegislationFixation", new { area = "Notices" });
                    //case "10090":
                    //    return RedirectToAction("CreateLOB", "ListOfBusiness", new { area = "ListOfBusiness" });

                    //case "10091":
                    //    return RedirectToAction("Approval", "ListOfBusiness", new { area = "ListOfBusiness" });
                    //case "10092":
                    //    return RedirectToAction("SpeakerPad", "Speaker", new { area = "ListOfBusiness" });
                    //case "10094":
                    //    return RedirectToAction("ApprovalSpeakerPad", "Speaker", new { area = "ListOfBusiness" });
                    case "admin":
                        return RedirectToAction("Index", "Dashboard", new { area = "Admin" });
                    case "Vivek":
                        return RedirectToAction("Index", "MetaData", new { area = "Admin" });
                    case "lobadmin":
                        return RedirectToAction("GetAssebmlySessionDetails", "BusinessWork", new { area = "BusinessWork" });

                    //    return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    case "user":
                        return View("UserDashboard");

                    default:
                        break;
                }

                #endregion OldAuth

                #region CustumAuth
                //User=>ForUserType
                //role=>ForModule
                string ModuleIds = "";
                tUserAccessRequest objtUserAccessRequest = new tUserAccessRequest();
                objtUserAccessRequest.UserID = Guid.Parse(UserID);
                var UserModules = (List<tUserAccessActions>)Helper.ExecuteService("Module", "GetUseModulesByUserID", objtUserAccessRequest);

                var usres = (List<mUsers>)Helper.ExecuteService("Module", "GetUsers", null);
                var user = usres.Where(u => u.UserName == UserName && u.UserId.ToString() == UserID).FirstOrDefault();
                if (user != null)
                {
                    mRoles iObjRole = new mRoles();
                    var ilstRole = (List<mSubUserType>)Helper.ExecuteService("Module", "GetSubUserType", null);
                    var _UserType = ilstRole.Select(m => m.iSubUserTypeID = SubUserTypeID).Take(1).ToArray();
                    // var roles = user.Roles.Select(m => m.iRoleId).ToArray();
                    CustomPrincipalSerializeModel serializeModel = new CustomPrincipalSerializeModel();
                    serializeModel.UserId = user.UserId.ToString();
                    serializeModel.FirstName = user.UserName;
                    serializeModel.LastName = user.UserName;
                    serializeModel.roles = _UserType;
                    if (UserModules != null && UserModules.Count() > 0)
                    {
                        foreach (var item in UserModules)
                        {
                            ModuleIds += item.ModuleId + ",";
                        }
                        bool flag = ModuleIds.EndsWith(",");
                        if (flag)
                        {
                            ModuleIds = ModuleIds.Substring(0, ModuleIds.Length - 1);
                        }
                        serializeModel.modules = ModuleIds.Split(',');
                    }

                    string userData = JsonConvert.SerializeObject(serializeModel);
                    FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(
                             1,
                            user.UserName,
                             DateTime.Now,
                             DateTime.Now.AddMinutes(20),
                             false,
                             userData);

                    string encTicket = FormsAuthentication.Encrypt(authTicket);
                    HttpCookie faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);

                    #region ByRoleID
                    // redirect for Secretary,other than secreatary, HOD, Other than Hod,
                    if (_UserType.Contains("2"))
                    {
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    }

                    else if (_UserType.Contains("3"))
                    {
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    }

                    else if (_UserType.Contains("15"))
                    {
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    }
                    else if (_UserType.Contains("16"))
                    {
                        return RedirectToAction("DepartmentDashboard", "PaperLaidDepartment", new { area = "PaperLaidDepartment" });
                    }
                    else if (_UserType.Contains("37"))
                    {
                        //  var OId = (SBL.DomainModel.Models.Grievance.tGrievanceOfficerDeleted)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        var OId = (int)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        var MId = (int)Helper.ExecuteService("Grievance", "GetMemberByOfficeUser", CurrentSession.AadharId);
                        if (MId > 0)
                        {
                            CurrentSession.MemberCode = Convert.ToString(MId);
                        }
                        if (OId == Convert.ToInt32(officeId))
                        {

                            return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                        }
                        else
                        {

                            return RedirectToAction("Index", "Blank", new { area = "BlankRed" });
                        }

                    }
                    else if (_UserType.Contains("40"))
                    {
                        //  var OId = (SBL.DomainModel.Models.Grievance.tGrievanceOfficerDeleted)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        //var OId = (int)Helper.ExecuteService("Grievance", "GetOfficeUser", CurrentSession.AadharId);
                        //  var MId = (int)Helper.ExecuteService("Grievance", "GetMemberByOfficeUser", CurrentSession.AadharId);
                        // if (MId > 0)
                        //  {
                        //     CurrentSession.MemberCode = Convert.ToString(MId);
                        // }
                        //if (OId == Convert.ToInt32(officeId))
                        //{

                        return RedirectToAction("Index", "Grievances", new { area = "Grievances" });
                        //}
                        //else
                        //{

                        //    return RedirectToAction("Index", "Blank", new { area = "BlankRed" });
                        //}

                    }
                    //------------------------------------------------------------------------------------------------------------------------------------------
                    //redirect for Vidhan Shabha Secretary,Other than Vidhan Shabha Secretary
                    else if (_UserType.Contains("4"))
                    {
                        return RedirectToAction("Index", "VidhanSabhaDept", new { area = "VidhanSabhaDepartment" });
                    }
                    else if (_UserType.Contains("5"))
                    {
                        return RedirectToAction("Index", "VidhanSabhaDept", new { area = "VidhanSabhaDepartment" });
                    }

                    //------------------------------------------------------------------------------------------------------------------------------------------
                    //redirect for member, spaker, dupty speaker,personal staf
                    else if (_UserType.Contains("22"))
                    {
                        return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    }
                    else if (_UserType.Contains("17"))
                    {
                        return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    }
                    else if (_UserType.Contains("18"))
                    {
                        return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    }
                    else if (_UserType.Contains("36"))
                    {
                        return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                    }
                    else if (_UserType.Contains("38"))
                    {
                        if (!string.IsNullOrEmpty(CurrentSession.UserName))
                        {
                            string[] arr = CurrentSession.UserName.Split(',').ToArray();
                            CurrentSession.MemberCode = arr[0];
                            return RedirectToAction("Index", "OnlineMemberQuestions", new { area = "Notices" });
                        }
                    }
                    //------------------------------------------------------------------------------------------------------------------------------------------
                    //redirect for miniser, CPS
                    else if (_UserType.Contains("19"))
                    {
                        return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                    }
                    else if (_UserType.Contains("20"))
                    {
                        return RedirectToAction("MinisterDashboard", "PaperLaidMinister", new { area = "PaperLaidMinister" });
                    }
                    #endregion ByRoleID
                }

                #endregion CustumAuth

            }
            else
            {
                return RedirectToAction("Login", "Account", new { area = "" });
            }
            return View();
            //}
            //else
            //{
            //    return View();
            //}

        }

        //[HttpPost]
        //public JsonResult KeepSessionAlive()
        //{
        //    return new JsonResult { Data = "Success" };
        //}

        public JsonResult KeepSessionAlive()
        {
            //  tQuestion objModel = new tQuestion();
            //string DepartmentId = CurrentSession.DeptID;
            //string Officecode = CurrentSession.OfficeId;
            //string AadharId = CurrentSession.AadharId;
            //string currtBId = CurrentSession.BranchId;
            //string Year = "5";
            //string papers = "1";
            //string[] strngBIDN = new string[2];
            //strngBIDN[0] = CurrentSession.BranchId;
            //var value123 = (string[])Helper.ExecuteService("eFileCommiteePaperLaid", "GetCommiteeIDAndNameByBranchID", strngBIDN);
            //string[] str = new string[6];
            //str[0] = DepartmentId;
            //str[1] = Officecode;
            //str[2] = AadharId;
            //str[3] = value123[0];
            //str[4] = Year;
            //str[5] = papers;
            //List<eFileAttachment> ListModel = new List<eFileAttachment>();
            ////for draftlistCOunt
            //List<eFileAttachment> ListModelDraft = new List<eFileAttachment>();
            //string[] strD = new string[6];
            //strD[0] = DepartmentId;
            //strD[1] = Officecode;
            //strD[2] = AadharId;
            //strD[3] = currtBId;
            //strD[4] = CurrentSession.Designation;
            //strD[5] = "1";
            //List<ReadTableData> tableList = new List<ReadTableData>();
            //ListModel = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetReceivePaperDetailsByDeptIdHC", str);
            //ListModelDraft = (List<eFileAttachment>)Helper.ExecuteService("eFile", "GetDraftList", strD);
            //tableList = (List<ReadTableData>)Helper.ExecuteService("eFile", "GetReadDataTableList", str);
            //objModel.CountInbox = ListModel.Count - tableList.Count;
            //objModel.CountDraft = ListModelDraft.Count;
            //objModel.DataType = "Success";
            //   return new JsonResult { Data = "Success" };
            //   return Json(objModel, JsonRequestBehavior.AllowGet);
            return null;
        }
        public ActionResult KeepSessionAliveTest()
        {


            return Content("Session Hit");

        }

        public object CommitteeChairpersonUserId()
        {
            tCommitteeMember cm = new tCommitteeMember();
            tCommitteeModel Obj = new tCommitteeModel();
            var MemberId = CurrentSession.UserID;

            Obj.comtmembrList = (List<tCommitteeMember>)Helper.ExecuteService("Committee", "ShowCommitteeMemberByID", cm);

            if (MemberId == CurrentSession.UserID)
            {
                return RedirectToAction("CommitteeDashboard", "CommitteePaperLiadChairperson", new { area = "CommitteePaperLiadChairperson" });
            }
            else
            {
                return false;
            }
        }

        public object RenderUserDetails()
        {
            if (CurrentSession.UserID == "")
            {
                return null;
            }
            else
            {
                PhotoModel mdl = new PhotoModel();
                string input = CurrentSession.UserID;
                string sub = input.Substring(0, 1);
                if (sub == "T")
                {
                    mUsers user = new mUsers();
                    mdl.Name = CurrentSession.MbNo; ;

                    return PartialView("_GetUserPhoto", mdl);
                }
                else
                {
                    if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                    {
                        try
                        {
                            mUsers user = new mUsers();
                            user.UserId = new Guid(CurrentSession.UserID);
                            user.IsMember = CurrentSession.IsMember;

                            user = (mUsers)Helper.ExecuteService("User", "GetIsMemberNameAndPhotoDetails", user);
                            if (user != null)
                            {
                                mdl.Name = user.Name;
                                mdl.Photo = user.Photo;
                            }
                        }
                        catch
                        {
                            return RedirectToAction("LoginUP", "Account", new { area = "" });
                        }
                        return PartialView("_GetUserPhoto", mdl);
                    }
                }
            }
            return null;
        }
        [HttpPost]
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            string langCulture = "en";
            if (lang == "43177cd4-b51d-497a-a3bc-e81f23b07450")
            {
                langCulture = "hi-IN";
            }
            var langCookie = new HttpCookie("lang", langCulture)
            {
                HttpOnly = true
            };
            langCookie.Expires =DateTime.Now.AddDays(-1);

            Response.Cookies.Add(langCookie);
            SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageCulture = langCulture;
            if (lang != null)
            {
                SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID = lang;
            }
            else
            {
                SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.LanguageID = "7C4F28F6-02FC-4627-993D-E255037531AD";
            }
            SBL.eLegistrator.HouseController.Web.Utility.CurrentSession.UpdateMenuandModules();

            return Redirect(Request.UrlReferrer.ToString());
        }

        public ActionResult WorkProgress()
        {
            if (CurrentSession.UserID != null && CurrentSession.UserID != "")
            {
                tPaperLaidV model = new tPaperLaidV();
                //SiteSettings siteSettingMod = new SiteSettings();
                //siteSettingMod = (SiteSettings)Helper.ExecuteService("SiteSetting", "GetAllSiteSettings", siteSettingMod);
                //model.SessionCode = model.SessionId = siteSettingMod.SessionCode;
                //model.AssemblyCode = model.AssemblyId = siteSettingMod.AssemblyCode;

                //if (CurrentSession.RoleID == "1fca5265-162b-4bf3-8926-70a9c1548095" || CurrentSession.RoleID == "d457778d-b3e2-4a1f-a522-203ec7b09608")
                //{
                //    if (CurrentSession.UserID != null && CurrentSession.UserID != "")
                //    {
                //        model.UserID = new Guid(CurrentSession.UserID);
                //    }

                //    List<AuthorisedEmployee> AuthorisedEmp = new List<AuthorisedEmployee>();
                //    AuthorisedEmp = (List<AuthorisedEmployee>)Helper.ExecuteService("PaperLaid", "GetAuthorizedEmployees", model);

                //    if (AuthorisedEmp != null && AuthorisedEmp.Count() != 0)
                //    {
                //        foreach (var item in AuthorisedEmp)
                //        {
                //            model.DepartmentId = item.AssociatedDepts;
                //            model.IsPermissions = item.IsPermissions; ;
                //        }
                //    }
                //    else
                //    {
                //        if (model.DepartmentId == null)
                //        {
                //            model.DepartmentId = CurrentSession.DeptID;
                //        }
                //    }
                //    model = (tPaperLaidV)Helper.ExecuteService("PaperLaid", "GetDepartmentProgressCounter", model);
                //}
                //if (CurrentSession.RoleID == "e32dc9c9-02c2-447f-9072-89291a9a9a15")
                //{
                //    if (CurrentSession.UserName != null && CurrentSession.UserName != "")
                //    {
                //        model.LoginId = CurrentSession.UserName;
                //    }
                //    model = (tPaperLaidV)Helper.ExecuteService("Minister", "GetMinisterNameByMinistryID", model);
                //    model.MinistryId = model.MinistryId;
                //    model = (tPaperLaidV)Helper.ExecuteService("PaperLaidMinister", "GetMinisterProgressCounter", model);
                //}

                return PartialView("_WorkProgress", model);
            }
            return null;
        }

        //Set dynamic AssemblyId and SessionId in CurrentSession

        public JsonResult GetDataByAssemblySession(string AssemblyId, string SessionId)
        {
            CurrentSession.AssemblyId = AssemblyId;
            CurrentSession.SessionId = SessionId;

            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult BranchInSession(string BranchId, string BranchName)
        {
            CurrentSession.BranchId = BranchId;
            CurrentSession.BranchName = BranchName;
            return Json("Success", JsonRequestBehavior.AllowGet);
        }

        //Capture ModuleId For Dynamically Active the module or Menu in LeftNavigation for onlineMember

        public JsonResult CaptureModuleIdActionId(string ModuleId, string ActionIds)
        {
            CurrentSession.MenuId = ModuleId;
            CurrentSession.ActionIds = ActionIds;
            return Json("", JsonRequestBehavior.AllowGet);
        }
    }
}