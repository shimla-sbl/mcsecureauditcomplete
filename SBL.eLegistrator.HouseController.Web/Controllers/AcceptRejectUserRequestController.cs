﻿using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.eLegistrator.HouseController.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBL.eLegistrator.HouseController.Web.Areas.UserManagement.Extensions;
using Microsoft.Security.Application;
using SBL.eLegistrator.HouseController.Web.Utility;
using SBL.DomainModel.Models.UserAction;
using SBL.eLegistrator.HouseController.Web.Filters.Security;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Role;
using System.Text;
using SBL.DomainModel.Models.RecipientGroups;
//using SBL.eLegistrator.HouseController.Web.Areas.ListOfBusiness.Models;

namespace SBL.eLegistrator.HouseController.Web.Controllers
{
    // [CustomAuthorize(Roles = "0fc60e41-c78e-46b0-b8ac-f6eb708be100,d457778d-b3e2-4a1f-a522-203ec7b09608,f8e0ef76-afe7-4e94-88f4-f399f967e37e")]
    //[CustomAuthorize(Users = "1,2,3")]
    public class AcceptRejectUserRequestController : BaseController
    {
        //
        // GET: /AcceptRejectUserRequest/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserManagement()
        {
            return View();
        }

        public ActionResult LeftNavigationMenu()
        {
            return PartialView("_LeftNavigationMenu");
        }


        public ActionResult WebAdminAcceptRejectUser()
        {
            CurrentSession.SetSessionAlive = null;
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();

            objmodel.UserID = new Guid(CurrentSession.UserID);
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);

            mUsers usermdl = new mUsers();
            usermdl.UserId = new Guid(CurrentSession.UserID);
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            if (usermdl.IsSecretoryId == true)
            {
                objmodel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
            }
            else if (usermdl.IsMember == "True")
            {
                objmodel.MemberId = Convert.ToInt16(usermdl.UserName);
            }
            else if (usermdl.IsHOD == true)
            {
                objmodel.HODId = Convert.ToInt16(usermdl.SecretoryId);
            }

            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAccessRequestrGrid", objmodel);
            //List<tUserAccessRequest> newlist = new List<tUserAccessRequest>();
            //foreach (var val in model.objAccRejList)
            //{
            //    //string text = "(" + val.AadarId + ")" + val.UserName;
            //    if (val.Adharid != null)
            //    {
            //        string text = val.name + "--" + val.Adharid;
            //        val.name = text;
            //        newlist.Add(val);
            //    }
            //}
            //var DistinctItems = newlist.GroupBy(x => x.Adharid).Select(y => y.First());
            //model.FullName = new SelectList(DistinctItems, "UserId", "Name");
            model.objAccRejList = model.objAccRejList.OrderBy(z => z.name).GroupBy(x => x.UserID).Select(y => y.First()).ToList();
            return PartialView("_WebAdminAcceptRejectUser", model);

        }

        public ActionResult GetWebAdminAcceptRejectGridByUserId(Guid UserID)
        {

            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();
            mRoles usertypemodel = new mRoles();
            //objmodel.UserID = new Guid(CurrentSession.UserID);
            //objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);

            mUsers usermdl = new mUsers();
            usermdl.UserId = UserID;
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            usertypemodel.SubUserTypeID = Convert.ToInt32(usermdl.UserType);
            model.RoleListByType = (List<mRoles>)Helper.ExecuteService("Role", "GetUserTypeRoleList", usertypemodel);
            // model.RoleKeyList = getkeyRolelist(model);
            //model.RoleKeyList = ids;

            objmodel.UserID = UserID;
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                objmodel.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }
            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAccessRequestrGridByUserId", objmodel);

            List<tUserAccessRequest> newlist = new List<tUserAccessRequest>();
#pragma warning disable CS0219 // The variable 'i' is assigned but its value is never used
            int i = 0;
#pragma warning restore CS0219 // The variable 'i' is assigned but its value is never used




            foreach (var val in model.objAccRejList)
            {
                //string text = "(" + val.AadarId + ")" + val.UserName;
                if (val.Adharid != null)
                {
                    string text = val.name + "--" + val.Adharid;
                    val.name = text;
                    newlist.Add(val);
                }
            }


            var DistinctItems = newlist.OrderBy(z => z.name).GroupBy(x => x.Adharid).Select(y => y.First());
            model.FullName = new SelectList(DistinctItems, "UserId", "Name");

            string deptlist = usermdl.DepartmentIDs;
            string Additionaldeptlist = usermdl.RequestedAdditionalDept;
            model.Deptid = usermdl.DeptId;
            model.DeptList = getAssocitedDepartmentlist(deptlist);
            model.AdditionalDeptList = getAssocitedDepartmentlist(Additionaldeptlist);//
            model.RequestedDeptList = getRequestDepartmentlist(usermdl.DepartmentIDs);
            model.ApprovedDeptList = getRequestDepartmentlist(usermdl.DeptId);
            model.RequestedAdditionalDeptList = getRequestDepartmentlist(usermdl.RequestedAdditionalDept);
            model.ApprovedAdditionalDeptList = getRequestDepartmentlist(usermdl.ApprovedAdditionalDept);//
            return PartialView("_SearchByUserId", model);
        }


        public ActionResult GetWebAdminRejectedGridByUserId(Guid UserID)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();

            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();

            objmodel.UserID = UserID;
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                objmodel.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }


            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminRejectedGridByUSerId", objmodel);

            return PartialView("_SearchWebAdminRejectedByUserId", model);
        }

        public ActionResult GetWebAdminAcceptedGridByUserId(Guid UserID)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();

            objmodel.UserID = UserID;
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);

            mUsers usermdl = new mUsers();
            usermdl.UserId = UserID;
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            //usertypemodel.SubUserTypeID = Convert.ToInt32(usermdl.UserType);
            string deptlist = usermdl.DepartmentIDs;
            model.Deptid = usermdl.DeptId;
            model.DeptList = getAssocitedDepartmentlist(deptlist);
            model.RequestedDeptList = getRequestDepartmentlist(usermdl.DepartmentIDs);
            model.ApprovedDeptList = getRequestDepartmentlist(usermdl.DeptId);
            model.ApprovedAdditionalDeptList = getRequestDepartmentlist(usermdl.ApprovedAdditionalDept);
            if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
            {
                objmodel.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
            }

            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAcceptedGridByUSerId", objmodel);

            return PartialView("_SearchWebAdminAcceptedByUserId", model);
        }

        public ActionResult UpdateWebAcceptUser(string SIds, string roleids, string deptids)
        {
            tUserAccessRequest model = new tUserAccessRequest();


            Helper.ExecuteService("Module", "UpdateAcceptRequest", SIds);

            #region CheckForUserRoll
            string[] ids = SIds.Split(',');
            if (ids.Length > 0)
            {
                foreach (var item in ids)
                {
                    //model.ID = Convert.ToInt16(item);
                    //model = (tUserAccessRequest)Helper.ExecuteService("Module", "GetUserRequestByID", model);

                    //SubUserTypeRolesController ObjSubUserTypeRolesController = new SubUserTypeRolesController();
                    //var result = ObjSubUserTypeRolesController.CheckSubUserRoleExistBySubUserID(Convert.ToInt32(model.TypedDomainId));
                    //if (Convert.ToBoolean(result.Data))
                    //{
                    //    var result1 = ObjSubUserTypeRolesController.CheckUserRoleExistByUserID(model.UserID);
                    //    if (!Convert.ToBoolean(result1.Data))
                    //    {
                    //        mSubUserTypeRoles objmSubUserTypeRoles = (mSubUserTypeRoles)Helper.ExecuteService("Role", "GetSubUserTypeRoleById", new mSubUserTypeRoles { SubUserTypeID = Convert.ToInt32(model.TypedDomainId) });
                    //        if (objmSubUserTypeRoles != null)
                    //        {
                    //            tUserRoles objtUserRoles = new tUserRoles();
                    //            objtUserRoles.RoleDetailsID = Guid.NewGuid();
                    //            objtUserRoles.Roleid = objmSubUserTypeRoles.RoleId;
                    //            objtUserRoles.UserID = model.UserID;
                    //            var result2 = (tUserRoles)Helper.ExecuteService("Role", "MapRollToUsers", objtUserRoles);
                    //        }
                    //    }
                    //}
                }
            }
            #endregion
            return RedirectToAction("WebAdminAcceptRejectUser");


        }







        public bool UpdateWebAcceptUserByUser(string UserIdsList)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();
            mUsers usermdl = new mUsers();
            string[] strArray = UserIdsList.Split(',');
            foreach (var UserID in strArray)
            {
                ///////////////////////Get User Details By ID//////////////////////////////////////
                usermdl.UserId = Guid.Parse(UserID);
                usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
                ///////////////////////Get User Details By ID//////////////////////////////////////


                /////////////////////Get User Request Details/////////////////////////////
                objmodel.UserID = Guid.Parse(UserID);
                objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
                if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
                {
                    objmodel.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                }
                model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAccessRequestrGridByUserId", objmodel);
                /////////////////////Get User Request Details/////////////////////////////



                //////////////////////Manage Department/////////////////////////
                string deptlist = usermdl.DepartmentIDs;
                model.Deptid = usermdl.DeptId;
                model.DeptList = getAssocitedDepartmentlist(deptlist);//
                model.RequestedDeptList = getRequestDepartmentlist(usermdl.DepartmentIDs);
                model.ApprovedDeptList = getRequestDepartmentlist(usermdl.DeptId);//
                //////////////////////Manage Department/////////////////////////

                string UserIddToUpdate = "";
                if (model.objAccRejList.Count() > 0)
                {
                    foreach (var item in model.objAccRejList)
                    {
                        UserIddToUpdate += item.ID + ",";
                    }
                    bool flag = UserIddToUpdate.EndsWith(",");
                    if (flag)
                    {
                        UserIddToUpdate = UserIddToUpdate.Substring(0, UserIddToUpdate.Length - 1);
                    }
                    Helper.ExecuteService("Module", "UpdateAcceptRequest", UserIddToUpdate);
                }

                string reqdeptids = "";
                string oldDeptid = "";
                if (model.DeptList.Count() > 0)
                {
                    foreach (var reqlist in model.DeptList)
                    {
                        reqdeptids += reqlist.Key + ',';
                    }
                    if (model.Deptid == null)
                    {
                        oldDeptid = "";
                    }
                    UpdateRoleandDepartment(UserID, reqdeptids, oldDeptid);
                }



            }
            return true;
        }


        public bool UpdateWebRejectUserByUser(string UserIdsList)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();
            mUsers usermdl = new mUsers();
            string[] strArray = UserIdsList.Split(',');
            foreach (var UserID in strArray)
            {
                ///////////////////////Get User Details By ID//////////////////////////////////////
                usermdl.UserId = Guid.Parse(UserID);
                usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
                ///////////////////////Get User Details By ID//////////////////////////////////////


                /////////////////////Get User Request Details/////////////////////////////
                objmodel.UserID = Guid.Parse(UserID);
                objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
                if (!string.IsNullOrEmpty(CurrentSession.MemberCode))
                {
                    objmodel.MemberId = Convert.ToInt16(CurrentSession.MemberCode);
                }
                model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAccessRequestrGridByUserId", objmodel);
                /////////////////////Get User Request Details/////////////////////////////



                //////////////////////Manage Department/////////////////////////
                string deptlist = usermdl.DepartmentIDs;
                model.Deptid = usermdl.DeptId;
                model.DeptList = getAssocitedDepartmentlist(deptlist);//
                model.RequestedDeptList = getRequestDepartmentlist(usermdl.DepartmentIDs);
                model.ApprovedDeptList = getRequestDepartmentlist(usermdl.DeptId);//
                //////////////////////Manage Department/////////////////////////

                string UserIddToUpdate = "";
                if (model.objAccRejList.Count() > 0)
                {
                    foreach (var item in model.objAccRejList)
                    {
                        UserIddToUpdate += item.ID + ",";
                    }
                    bool flag = UserIddToUpdate.EndsWith(",");
                    if (flag)
                    {
                        UserIddToUpdate = UserIddToUpdate.Substring(0, UserIddToUpdate.Length - 1);
                    }
                    Helper.ExecuteService("Module", "UpdateRejectRequest", UserIddToUpdate);
                }

                string reqdeptids = "";
                string oldDeptid = "";
                if (model.DeptList.Count() > 0)
                {
                    foreach (var reqlist in model.DeptList)
                    {
                        reqdeptids += reqlist.Key + ',';
                    }
                    if (model.Deptid == null)
                    {
                        oldDeptid = "";
                    }
                    UpdateRoleandDepartment(UserID, reqdeptids, oldDeptid);
                }
            }
            return true;
        }








        public ActionResult UpdateRoleandDepartment(string userid, string reqdeptids, string oldDeptid)
        {
            if (reqdeptids != null && reqdeptids != "")
            {
                if (!reqdeptids.Contains(","))
                {
                    reqdeptids = reqdeptids + ",";
                }
            }
            if (oldDeptid != null && oldDeptid != "")
            {
                if (!oldDeptid.Contains(","))
                {
                    oldDeptid = oldDeptid + ",";
                }
            }


            if (reqdeptids != null)
            {
                bool flag = reqdeptids.EndsWith(",");
                if (flag)
                {
                    reqdeptids = reqdeptids.Substring(0, reqdeptids.Length - 1);
                }
            }


            mUsers objmUsers = new mUsers();

            string[] PendingDepIDs = reqdeptids.Split(',');

            List<mUsers> lstmUsers = new List<mUsers>();

            if (PendingDepIDs.Length > 0)
            {
                //mUsers ObjmUsers = new mUsers();
                //var returnedResult = Helper.ExecuteService("User", "GetAllUser", null) as List<mUsers>;

                //if (returnedResult.Count() > 0)
                //{
                //    foreach (var item in PendingDepIDs)
                //    {
                //        if (item != "")
                //        {
                //            foreach (var item2 in returnedResult)
                //            {
                //                string[] temp;
                //                if (item2.DeptId.Contains(","))
                //                {
                //                    temp = item2.DeptId.Split(',');
                //                }

                //                else
                //                {
                //                    item2.DeptId = item2.DeptId + ",";
                //                    temp = item2.DeptId.Split(',');
                //                }
                //                if (temp.Contains(item))
                //                {
                //                    lstmUsers.Add(item2);
                //                }

                //            }
                //        }
                //    }

                //}

                //foreach (var item in lstmUsers)
                //{
                //    Helper.ExecuteService("User", "UpdateDeptUser", item);

                //}
                objmUsers.UserId = new Guid(userid);

                //mdl.DepartmentIDs = null;
                //if (oldDeptid != null && oldDeptid != "")
                //{

                //    if (oldDeptid.IndexOf(",") != -1)
                //    {
                //        string[] arr = oldDeptid.Split(',');
                //        string[] arr1 = reqdeptids.Split(',');
                //        foreach (var item in arr1)
                //        {
                //            bool flag = oldDeptid.EndsWith(",");
                //            if (flag)
                //            {
                //                oldDeptid = oldDeptid.Substring(0, oldDeptid.Length - 1);
                //            }

                //            if (arr.Contains(item))
                //            {
                //            }
                //            else
                //            {
                //                oldDeptid += "," + item;
                //            }

                //        }
                //        if (oldDeptid != null)
                //        {
                //            bool flag = oldDeptid.EndsWith(",");
                //            if (flag)
                //            {
                //                oldDeptid = oldDeptid.Substring(0, oldDeptid.Length - 1);
                //            }
                //        }
                //        objmUsers.DeptId = oldDeptid;
                //    }
                //}
                //else
                //{

                //    string[] arr = reqdeptids.Split(',');

                //    foreach (var dept in arr)
                //    {
                //        objmUsers.DeptId += dept + ",";
                //    }
                //    bool flag = objmUsers.DeptId.EndsWith(",");
                //    if (flag)
                //    {
                //        objmUsers.DeptId = objmUsers.DeptId.Substring(0, objmUsers.DeptId.Length - 1);
                //    }

                //}
                objmUsers.DeptId = reqdeptids;
                objmUsers.DepartmentIDs = null;

                Helper.ExecuteService("User", "UpdatedMemberDepartment", objmUsers);
                #region UpdateSecDept

                if (objmUsers != null)
                {
                    //mSecretoryDepartment ObjmSecretoryDepartment = new mSecretoryDepartment();
                    //ObjmSecretoryDepartment.SecretoryID = Convert.ToInt32(objmUsers.SecretoryId);
                    //var lstmSecretoryDepartment = Helper.ExecuteService("SecretoryDepartment", "GetmSecretoryDepartmentBySecID", ObjmSecretoryDepartment) as List<mSecretoryDepartment>;
                    //if (lstmSecretoryDepartment.Count() > 0)
                    //{
                    //    foreach (var item in lstmSecretoryDepartment)
                    //    {
                    //        var Result = Helper.ExecuteService("SecretoryDepartment", "DelSecretoryDepartment", item);
                    //    }

                    //    if (objmUsers.DeptId != null)
                    //    {
                    //        //string temp12 = "HPD0011,HPD0018,HPD0047";
                    //        string[] SecDeptTOUpdate = objmUsers.DeptId.Split(',');
                    //        foreach (var item2 in SecDeptTOUpdate)
                    //        {
                    //            ObjmSecretoryDepartment.DepartmentID = item2;
                    //            ObjmSecretoryDepartment.IsActive = true;
                    //            var Result2 = Helper.ExecuteService("SecretoryDepartment", "CreateSecretoryDepartment", ObjmSecretoryDepartment);
                    //        }
                    //    }
                    //}
                }

                #endregion UpdateSecDept

            }


            return RedirectToAction("WebAdminAcceptRejectUser");




        }
        public ActionResult DeleteWebAcceptUser(string SIds)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();

            //  var dist = model.ToAcceptReject();
            Helper.ExecuteService("Module", "DeleteUserAccepted", SIds);
            return RedirectToAction("WebAdminAccepted");

        }


        public ActionResult DeleteWebRejectedUser(string SIds)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();

            //  var dist = model.ToAcceptReject();
            Helper.ExecuteService("Module", "DeleteUserRejected", SIds);
            return RedirectToAction("WebAdminAcceptRejectUser");

        }

        public ActionResult UpdateWebRejectUser(string SIds)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();

            var dist = model.ToAcceptReject();

            Helper.ExecuteService("Module", "UpdateRejectRequest", SIds);

            return RedirectToAction("WebAdminAcceptRejectUser");

        }



        public ActionResult WebAdminAccepted()
        {
            CurrentSession.SetSessionAlive = null;
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();

            objmodel.UserID = new Guid(CurrentSession.UserID);
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);

            mUsers usermdl = new mUsers();
            usermdl.UserId = new Guid(CurrentSession.UserID);
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            if (usermdl.IsSecretoryId == true)
            {
                objmodel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
            }
            else if (usermdl.IsMember == "True")
            {
                objmodel.MemberId = Convert.ToInt16(usermdl.UserName);
            }
            else if (usermdl.IsHOD == true)
            {
                objmodel.HODId = Convert.ToInt16(usermdl.SecretoryId);
            }

            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAcceptedGrid", objmodel);
            //List<tUserAccessRequest> newlist = new List<tUserAccessRequest>();
            //foreach (var val in model.objAccRejList)
            //{
            //    //string text = "(" + val.AadarId + ")" + val.UserName;
            //    if (val.Adharid != null)
            //    {
            //        string text = val.name + "--" + val.Adharid;
            //        val.name = text;
            //        newlist.Add(val);
            //    }
            //}
            //var DistinctItems = newlist.GroupBy(x => x.Adharid).Select(y => y.First());
            //model.FullName = new SelectList(DistinctItems, "UserId", "Name");
            model.objAccRejList = model.objAccRejList.OrderBy(z => z.name).GroupBy(x => x.UserID).Select(y => y.First()).ToList();
            return PartialView("_WebAdminAcceptedUser", model);
        }

        public ActionResult WebAdminRejected()
        {
            CurrentSession.SetSessionAlive = null;

            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();

            objmodel.UserID = new Guid(CurrentSession.UserID);
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);

            mUsers usermdl = new mUsers();
            usermdl.UserId = new Guid(CurrentSession.UserID);
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            if (usermdl.IsSecretoryId == true)
            {
                objmodel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
            }
            else if (usermdl.IsMember == "True")
            {
                objmodel.MemberId = Convert.ToInt16(usermdl.UserName);
            }
            else if (usermdl.IsHOD == true)
            {
                objmodel.HODId = Convert.ToInt16(usermdl.SecretoryId);
            }

            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminRejectedGrid", objmodel);
            //List<tUserAccessRequest> newlist = new List<tUserAccessRequest>();
            //foreach (var val in model.objAccRejList)
            //{
            //    //string text = "(" + val.AadarId + ")" + val.UserName;
            //    if (val.Adharid != null)
            //    {
            //        string text = val.name + "--" + val.Adharid;
            //        val.name = text;
            //        newlist.Add(val);
            //    }
            //}
            //var DistinctItems = newlist.GroupBy(x => x.Adharid).Select(y => y.First());
            //model.FullName = new SelectList(DistinctItems, "UserId", "Name");
            model.objAccRejList = model.objAccRejList.OrderBy(z => z.name).GroupBy(x => x.UserID).Select(y => y.First()).ToList();
            return PartialView("_WebAdminRejectedUser", model);
        }

        public ActionResult EditWebAdminAccepted(int Id)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            tUserAccessRequest objmodel = new tUserAccessRequest();
            tUserAccessActions obj = new tUserAccessActions();

            objmodel.UserID = new Guid(CurrentSession.UserID);
            objmodel.ID = Id;
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAcceptedGridForEdit", objmodel);


            foreach (var itm in model.objAccRejList)
            {
                //mUserModules obj = new mUserModules();
                string csv = itm.ActionControlId;
                itm.ActionListKey = getactionlist(csv);

                string MergeActionId = itm.MergeActionId;
                itm.ActionListControlId = getactionlist(MergeActionId);

                string AssocitedDeaprtmnetId = itm.AssociateDepartmentId;
                itm.AssociatedDepartmentListKey = getAssocitedDepartmentlist(AssocitedDeaprtmnetId);

                string SelectedAssocitedDeaprtmnetId = itm.SelectedAssociateDepartmentId;
                itm.selectedAssociatedDepartmentListKey = getAssocitedDepartmentlist(SelectedAssocitedDeaprtmnetId);


            }

            return PartialView("_EditWebAdminAccepted", model);
        }


        public ActionResult EditWebAdminRejected(int Id)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            tUserAccessRequest objmodel = new tUserAccessRequest();
            objmodel.UserID = new Guid(CurrentSession.UserID);
            objmodel.ID = Id;
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminRejectedGridForEdit", objmodel);



            foreach (var itm in model.objAccRejList)
            {
                //mUserModules obj = new mUserModules();
                string csv = itm.ActionControlId;
                itm.ActionListKey = getactionlist(csv);

                string MergeActionId = itm.MergeActionId;
                itm.ActionListControlId = getactionlist(MergeActionId);

                string AssocitedDeaprtmnetId = itm.AssociateDepartmentId;
                itm.AssociatedDepartmentListKey = getAssocitedDepartmentlist(AssocitedDeaprtmnetId);

                string SelectedAssocitedDeaprtmnetId = itm.SelectedAssociateDepartmentId;
                itm.selectedAssociatedDepartmentListKey = getAssocitedDepartmentlist(SelectedAssocitedDeaprtmnetId);

            }



            return PartialView("_EditWebAdminRejected", model);
        }
        public ActionResult EditWebAdminAcceptReject(int Id)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            tUserAccessRequest objmodel = new tUserAccessRequest();
            tUserAccessActions obj = new tUserAccessActions();

            objmodel.UserID = new Guid(CurrentSession.UserID);
            objmodel.ID = Id;
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);

            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAcceptRejectGridForEdit", objmodel);


            foreach (var itm in model.objAccRejList)
            {
                //mUserModules obj = new mUserModules();
                string csv = itm.ActionControlId;
                itm.ActionListKey = getactionlist(csv);

                string MergeActionId = itm.MergeActionId;
                itm.ActionListControlId = getactionlist(MergeActionId);

                string AssocitedDeaprtmnetId = itm.AssociateDepartmentId;
                itm.AssociatedDepartmentListKey = getAssocitedDepartmentlist(AssocitedDeaprtmnetId);

                string SelectedAssocitedDeaprtmnetId = itm.SelectedAssociateDepartmentId;
                itm.selectedAssociatedDepartmentListKey = getAssocitedDepartmentlist(SelectedAssocitedDeaprtmnetId);


            }

            return PartialView("_EditWebAdminAcceptReject", model);
        }
        public List<KeyValuePair<int, string>> getactionlist(string ids)
        {
            var radioButtonList = new List<KeyValuePair<int, string>>();
            if (ids != null && ids.Length > 0)
            {
                IEnumerable<string> idss = ids.Split(',').Select(str => str);
                mUserActions user = new mUserActions();

                foreach (var itm2 in idss)
                {
                    user.ActionId = Convert.ToInt32(itm2);
                    user = (mUserActions)Helper.ExecuteService("Module", "GetActionDataById", user);
                    radioButtonList.Insert(0, new KeyValuePair<Int32, string>(user.ActionId, user.ActionName));
                }
            }
            return radioButtonList;
        }


        public List<KeyValuePair<string, string>> getAssocitedDepartmentlist(string ids)
        {
            var radioButtonList = new List<KeyValuePair<string, string>>();
            if (ids != null && ids.Length > 0)
            {
                IEnumerable<string> idss = ids.Split(',').Select(str => str);
                mDepartment user = new mDepartment();

                foreach (var itm2 in idss)
                {
                    user.deptId = itm2;

                    user = (mDepartment)Helper.ExecuteService("Module", "GetAssiciatedDepartmentDataById", user);
                    radioButtonList.Insert(0, new KeyValuePair<string, string>(user.deptId, user.deptname));
                    //radioButtonList.Insert(0, new KeyValuePair<Int32, string>(i, user.deptname));
                }
            }
            return radioButtonList;
        }
        public List<KeyValuePair<string, string>> getkeyRolelist(tUserAccessRequestModel ids)
        {
            var radioButtonList = new List<KeyValuePair<string, string>>();
            if (ids != null)
            {
                //IEnumerable<string> idss = ids.Split(',').Select(str => str);
                // mDepartment user = new mDepartment();

                foreach (var itm2 in ids.RoleListByType)
                {
                    //user.deptId = itm2;

                    //user = (mDepartment)Helper.ExecuteService("Module", "GetAssiciatedDepartmentDataById", user);
                    radioButtonList.Insert(0, new KeyValuePair<string, string>(itm2.RoleId.ToString(), itm2.RoleName.ToString()));
                    //radioButtonList.Insert(0, new KeyValuePair<Int32, string>(i, user.deptname));
                }
            }
            return radioButtonList;
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendUserAcccessRequest(mUsers model, string hdnUserAccessAccess, int ID, string DepartmentIDs, string redirect)
        {
            // if (ModelState.IsValid)
            //{

            bool isSubmitted = false;
            tUserAccessRequest mdl = new tUserAccessRequest();
            mdl.SecreataryId = Convert.ToInt32(model.SecretoryId);
            mdl.SelectedAssociateDepartmentId = model.DepartmentIDs;
            mdl.UserID = new Guid(CurrentSession.UserID);
            mdl.ID = ID;
            mdl.MemberId = 0;
            mdl.DesignationId = 0;
            if (hdnUserAccessAccess.Length > 0)
            {
                hdnUserAccessAccess = hdnUserAccessAccess.Substring(2, hdnUserAccessAccess.Length - 2);
                string[] arr = hdnUserAccessAccess.Split('#');
                for (int i = 0; arr.Length > i; i++)
                {
                    string value = arr[i].ToString();
                    if (value.Length > 0)
                    {
                        value = value.Substring(1, value.Length - 1);
                        string[] tparr = value.Split(',');

                        string moduleid = tparr[0]; //value.Substring(0, 1);
                        int indx = value.IndexOf(",");
                        string actionids = value.Substring(indx, value.Length - indx);
                        actionids = actionids.Substring(1, actionids.Length - 1);
                        mdl.AccessID = Convert.ToInt32(moduleid);
                        mdl.MergeActionId = actionids;



                        //string moduleid = value.Substring(0, 1);
                        //string actionids = value.Substring(2, value.Length - 2);
                        mdl.AccessID = Convert.ToInt32(moduleid);
                        mdl.MergeActionId = actionids;

                        Helper.ExecuteService("Module", "UpdateUserAccessRequest", mdl);
                        isSubmitted = true;




                    }
                    //string[] arr1 = value.Split(',');
                }
            }
            if (isSubmitted)
            {
                TempData["IsSubmitted"] = "Submitted";
            }
            else
            {
                TempData["IsSubmitted"] = "";
            }
            if (redirect == "Accepted")
            {
                return RedirectToAction("WebAdminAccepted");
            }
            else
            {
                return RedirectToAction("WebAdminAcceptRejectUser");
            }
        }


        [HttpGet]
        //[ValidateAntiForgeryToken]
        public ActionResult SendUserAcccessRequestforPending(string hdnUserAccessAccess, int ID, string redirect)
        {
            // if (ModelState.IsValid)
            //{

            bool isSubmitted = false;
            tUserAccessRequest mdl = new tUserAccessRequest();
            // mdl.SecreataryId = SecretoryId ?? 0;
            //mdl.SelectedAssociateDepartmentId = DepartmentIDs;
            mdl.UserID = new Guid(CurrentSession.UserID);
            mdl.ID = ID;
            mdl.MemberId = 0;
            mdl.DesignationId = 0;
            if (hdnUserAccessAccess.Length > 0)
            {
                hdnUserAccessAccess = hdnUserAccessAccess.Substring(2, hdnUserAccessAccess.Length - 2);
                string[] arr = hdnUserAccessAccess.Split('#');
                for (int i = 0; arr.Length > i; i++)
                {
                    string value = arr[i].ToString();
                    if (value.Length > 0)
                    {

                        //string[] tparr = value.Split(',');

                        //string moduleid = tparr[0]; //value.Substring(0, 1);
                        // int indx = value.IndexOf(",");
                        //string actionids = value.Substring(indx, value.Length - indx);
                        //actionids = actionids.Substring(1, actionids.Length - 1);
                        // mdl.AccessID = Convert.ToInt32(moduleid);
                        // mdl.MergeActionId = actionids;

                        // mdl.AccessID = Convert.ToInt32(moduleid);
                        mdl.MergeActionId = value;

                        Helper.ExecuteService("Module", "UpdateUserAccessRequest", mdl);
                        isSubmitted = true;
                    }
                    //string[] arr1 = value.Split(',');
                }
            }
            if (isSubmitted)
            {
                TempData["IsSubmitted"] = "Submitted";
            }
            else
            {
                TempData["IsSubmitted"] = "";
            }
            if (redirect == "Accepted")
            {
                return RedirectToAction("WebAdminAccepted");
            }
            else
            {
                return RedirectToAction("WebAdminAcceptRejectUser");
            }

            // return RedirectToAction("WebAdminAcceptRejectUser");


        }



        public ActionResult DeleteWebAcceptRejectUser(string SIds)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();

            //  var dist = model.ToAcceptReject();
            Helper.ExecuteService("Module", "DeleteWebAcceptRejectUser", SIds);
            return RedirectToAction("WebAdminAcceptRejectUser");

        }
        #region new role updated

        //public ActionResult WebAdminPendingRequestWithRole(Guid UserID)
        //{
        //    tUserAccessRequestModel model = new tUserAccessRequestModel();
        //    List<mUsers> ListSiteSettingsVS = new List<mUsers>();
        //    tUserAccessRequest objmodel = new tUserAccessRequest();
        //    mRoles usertypemodel = new mRoles();
        //    //objmodel.UserID = new Guid(CurrentSession.UserID);
        //    //objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);

        //    mUsers usermdl = new mUsers();
        //    usermdl.UserId = UserID;
        //    usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
        //    usertypemodel.SubUserTypeID = Convert.ToInt32(usermdl.UserType);
        //    model.RoleListByType = (List<mRoles>)Helper.ExecuteService("Role", "GetUserTypeRoleList", usertypemodel);
        //    // model.RoleKeyList = getkeyRolelist(model);
        //    //model.RoleKeyList = ids;

        //    objmodel.UserID = UserID;
        //    objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
        //    model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAccessRequestrGridByUserId", objmodel);

        //    List<tUserAccessRequest> newlist = new List<tUserAccessRequest>();
        //    int i = 0;




        //    foreach (var val in model.objAccRejList)
        //    {
        //        //string text = "(" + val.AadarId + ")" + val.UserName;
        //        if (val.Adharid != null)
        //        {
        //            string text = val.name + "--" + val.Adharid;
        //            val.name = text;
        //            newlist.Add(val);
        //        }
        //    }


        //    var DistinctItems = newlist.OrderBy(z => z.name).GroupBy(x => x.Adharid).Select(y => y.First());
        //    model.FullName = new SelectList(DistinctItems, "UserId", "Name");

        //    string deptlist = usermdl.DepartmentIDs;
        //    model.Deptid = usermdl.DeptId;
        //    model.DeptList = getAssocitedDepartmentlist(deptlist);
        //    model.RequestedDeptList = getRequestDepartmentlist(usermdl.DepartmentIDs);
        //    model.ApprovedDeptList = getRequestDepartmentlist(usermdl.DeptId);
        //    return PartialView("_WebAdminPendingRequestWithRole", model);

        //}
        public string getRequestDepartmentlist(string id)
        {

            string stringlist = "";
            if (id != null && id.Length > 0)
            {
                string ids = id.TrimEnd(',');
                IEnumerable<string> idss = ids.Split(',').Select(str => str);
                mDepartment user = new mDepartment();

                foreach (var itm2 in idss)
                {
                    user.deptId = itm2;

                    user = (mDepartment)Helper.ExecuteService("Module", "GetAssiciatedDepartmentDataById", user);
                    stringlist += user.deptname + ",";

                }
            }
            if (stringlist != "")
            {
                bool flag = stringlist.EndsWith(",");
                if (flag)
                {
                    stringlist = stringlist.Substring(0, stringlist.Length - 1);
                }
            }
            return stringlist;
        }
        #endregion


        public ActionResult AssignRole()
        {
            CurrentSession.SetSessionAlive = null;

            tUserAccessRequestModel model = new tUserAccessRequestModel();
            model.obUserList = (List<mUsers>)Helper.ExecuteService("Module", "GetUsersAdharDetails", null);
            model.FullName = new SelectList(model.obUserList, "UserId", "Name");

            mRoles objmRoles = new mRoles();
            objmRoles.SubUserTypeID = 3;
            model.AssignedRollListByUserType = Helper.ExecuteService("Role", "GetUserRole", null) as List<mRoles>;
            //Guid dd = new Guid();
            //Guid.TryParse("C4C92E7C-3D31-4CC7-BDCE-8DED55E77B99", out dd);
            //model.AssignedRollListByUserType = model.AssignedRollListByUserType.Where(x => x.RoleId == Guid.Parse("C4C92E7C-3D31-4CC7-BDCE-8DED55E77B99") || x.RoleId == Guid.Parse("0FC60E41-C78E-46B0-B8AC-F6EB708BE995") || x.RoleId == Guid.Parse("0FC60E41-C78E-46B0-B8AC-F6EB708BE996") || x.RoleId == Guid.Parse("E5144DAD-7A5D-4308-976A-8A5D2A462BD1") || x.RoleId == Guid.Parse("0FC60E41-C78E-46B0-B8AC-F6EB708BE150") || x.RoleId == Guid.Parse("0FC60E41-C78E-46B0-B8AC-F6EB708BE151") || x.RoleId == Guid.Parse("0FC60E41-C78E-46B0-B8AC-F6EB708BE152") || x.RoleId == Guid.Parse("0FC60E41-C78E-46B0-B8AC-F6EB708BE153")).ToList();
            return PartialView("_AssignRole", model);

        }

        public ActionResult GetUserRoleByUserID(Guid UserID)
        {
            tUserRoles ObjtUserRoles = new tUserRoles();
            ObjtUserRoles.UserID = UserID;
            var result = (mRoles)Helper.ExecuteService("Role", "GetUserRoleByUserID", ObjtUserRoles);
            if (result != null)
            {
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No Role", JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult AssignRoleToUser(string UserID, string Role, string Staus)
        {



            tUserRoles objtUserRoles = new tUserRoles();
            objtUserRoles.RoleDetailsID = Guid.NewGuid();
            objtUserRoles.Roleid = Guid.Parse(Role);
            objtUserRoles.UserID = Guid.Parse(UserID);
            if (Staus == "true")
            {
                var result = (tUserRoles)Helper.ExecuteService("Role", "MapRollToUsers", objtUserRoles);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = (tUserRoles)Helper.ExecuteService("Role", "RemoveUserRole", objtUserRoles);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSubAccesByUser(int ID, Guid UserID, int AccessID)
        {
            tUserAccessActions _objtUserAccessActions = new tUserAccessActions();
            _objtUserAccessActions.UserAccessActionsId = AccessID;

            var _lstmUserSubModules = (List<mUserSubModules>)Helper.ExecuteService("Module", "GetUserAccessActionByUserAccessActionsId", _objtUserAccessActions);
            if (_lstmUserSubModules.Count() > 0)
            {
                StringBuilder obj = new StringBuilder();
                foreach (var item in _lstmUserSubModules)
                {
                    obj.Append("<li class='dd-item dd2-item dd-colored' data-id='17'>");
                    obj.Append("<div class='dd-handle dd2-handle btn-info'>");
                    obj.Append("<label class='position-relative'>");
                    obj.Append(string.Format("<input type='checkbox' id='chkSubAccess' value='{0}' checked='checked'   class='ace'> <span class='lbl'></span>", item.SubModuleId));
                    obj.Append("</label> <i class='drag-icon ace-icon fa fa-arrows bigger-125'></i>");
                    obj.Append("</div>");
                    obj.Append(string.Format("<div class='dd2-content btn-info no-hover'>{0}</div>", item.SubModuleName));
                    obj.Append("</li>");
                }
                return Json(obj.ToString(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("No Data Available", JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult GetSubAccesByUser2(int ID, Guid UserID, int AccessID)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();

            objmodel.UserID = new Guid(CurrentSession.UserID);
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);

            mUsers usermdl = new mUsers();
            usermdl.UserId = new Guid(CurrentSession.UserID);
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            if (usermdl.IsSecretoryId == true)
            {
                objmodel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
            }
            else if (usermdl.IsMember == "True")
            {
                objmodel.MemberId = Convert.ToInt16(usermdl.UserName);
            }
            else if (usermdl.IsHOD == true)
            {
                objmodel.HODId = Convert.ToInt16(usermdl.SecretoryId);
            }

            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAccessRequestrGrid", objmodel);
            model.objAccRejList = model.objAccRejList.Where(x => x.ID == ID).ToList();
            string[] str = new string[50]; ;
            if (!string.IsNullOrEmpty(model.objAccRejList.ToList()[0].SubAccessByUser))
            {
                str = model.objAccRejList.ToList()[0].SubAccessByUser.ToString().Split(',');
            }
            tUserAccessActions _objtUserAccessActions = new tUserAccessActions();
            _objtUserAccessActions.UserAccessActionsId = AccessID;
            var _lstmUserSubModules = (List<mUserSubModules>)Helper.ExecuteService("Module", "GetUserAccessActionByUserAccessActionsId", _objtUserAccessActions);


            List<mUserSubModules> lstmUserSubModules = new List<mUserSubModules>();
            mUserSubModules objmUserSubModules = new mUserSubModules();
            foreach (var item in str)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    objmUserSubModules = _lstmUserSubModules.Where(x => x.SubModuleId == Convert.ToInt32(item)).FirstOrDefault();
                    if (objmUserSubModules != null)
                    {
                        lstmUserSubModules.Add(objmUserSubModules);
                    }
                }
            }
            if (lstmUserSubModules.Count() > 0)
            {
                StringBuilder obj = new StringBuilder();
                foreach (var item in lstmUserSubModules)
                {
                    if (item != null)
                    {
                        obj.Append("<li class='dd-item dd2-item dd-colored' data-id='17'>");
                        obj.Append("<div class='dd-handle dd2-handle btn-info'>");
                        obj.Append("<label class='position-relative'>");
                        obj.Append(string.Format("<input type='checkbox' id='chkAssignSubAccess' value='{0}' checked='checked' disabled='disabled'   class='ace1'> <span class='lbl'></span>", item.SubModuleName));
                        obj.Append("</label> <i class='drag-icon ace-icon fa fa-arrows bigger-125'></i>");
                        obj.Append("</div>");
                        obj.Append(string.Format("<div class='dd2-content no-hover'>{0}</div>", item.SubModuleName));
                        obj.Append("<span class='sticker'>");
                        obj.Append("</li>");
                    }
                }
                return Json(obj.ToString(), JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json("No Data Available", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSubAccesByUser2ForAccepted(int ID, Guid UserID, int AccessID)
        {
            tUserAccessRequestModel model = new tUserAccessRequestModel();
            List<mUsers> ListSiteSettingsVS = new List<mUsers>();
            tUserAccessRequest objmodel = new tUserAccessRequest();

            objmodel.UserID = new Guid(CurrentSession.UserID);
            objmodel.DomainAdminstratorId = Convert.ToInt32(CurrentSession.SubUserTypeID);
            mUsers usermdl = new mUsers();
            usermdl.UserId = new Guid(CurrentSession.UserID);
            usermdl = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", usermdl);
            if (usermdl.IsSecretoryId == true)
            {
                objmodel.SecreataryId = Convert.ToInt16(usermdl.SecretoryId);
            }
            else if (usermdl.IsMember == "True")
            {
                objmodel.MemberId = Convert.ToInt16(usermdl.UserName);
            }
            else if (usermdl.IsHOD == true)
            {
                objmodel.HODId = Convert.ToInt16(usermdl.SecretoryId);
            }
            model.objAccRejList = (ICollection<tUserAccessRequest>)Helper.ExecuteService("Module", "GetWebAdminAcceptedGrid", objmodel);
            model.objAccRejList = model.objAccRejList.Where(x => x.ID == ID).ToList();
            string[] str = new string[50]; ;
            if (!string.IsNullOrEmpty(model.objAccRejList.ToList()[0].SubAccessByUser))
            {
                str = model.objAccRejList.ToList()[0].SubAccessByUser.ToString().Split(',');
            }
            tUserAccessActions _objtUserAccessActions = new tUserAccessActions();
            _objtUserAccessActions.UserAccessActionsId = AccessID;
            var _lstmUserSubModules = (List<mUserSubModules>)Helper.ExecuteService("Module", "GetUserAccessActionByUserAccessActionsId", _objtUserAccessActions);


            List<mUserSubModules> lstmUserSubModules = new List<mUserSubModules>();
            mUserSubModules objmUserSubModules = new mUserSubModules();
            foreach (var item in str)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    objmUserSubModules = _lstmUserSubModules.Where(x => x.SubModuleId == Convert.ToInt32(item)).FirstOrDefault();
                    if (objmUserSubModules != null)
                    {
                        lstmUserSubModules.Add(objmUserSubModules);
                    }
                }
            }
            if (lstmUserSubModules.Count() > 0)
            {
                StringBuilder obj = new StringBuilder();
                foreach (var item in lstmUserSubModules)
                {
                    if (item != null)
                    {
                        obj.Append("<li class='dd-item dd2-item dd-colored' data-id='17'>");
                        obj.Append("<div class='dd-handle dd2-handle btn-info'>");
                        obj.Append("<label class='position-relative'>");
                        obj.Append(string.Format("<input type='checkbox' id='chkAssignSubAccess' value='{0}' checked='checked' disabled='disabled'   class='ace1'> <span class='lbl'></span>", item.SubModuleName));
                        obj.Append("</label> <i class='drag-icon ace-icon fa fa-arrows bigger-125'></i>");
                        obj.Append("</div>");
                        obj.Append(string.Format("<div class='dd2-content no-hover'>{0}</div>", item.SubModuleName));
                        obj.Append("<span class='sticker'>");
                        obj.Append("</li>");
                    }
                }
                return Json(obj.ToString(), JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json("No Data Available", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateSubAccess(int ID, string SubAccessID)
        {
            //if (SubAccessID.LastIndexOf(",")>0)
            //{
            //    SubAccessID = SubAccessID.Substring(0, SubAccessID.Length - 2);
            //}
            if (SubAccessID != null && SubAccessID != "" && ID > 0)
            {
                bool flag = SubAccessID.EndsWith(",");
                if (flag)
                {
                    SubAccessID = SubAccessID.Substring(0, SubAccessID.Length - 1);
                }
            }
            tUserAccessRequest _ObjtUserAccessRequest = new tUserAccessRequest();
            _ObjtUserAccessRequest.ID = ID;
            _ObjtUserAccessRequest.SubAccessByUser = SubAccessID;
            Helper.ExecuteService("Module", "UpdateSubAccessByAccess", _ObjtUserAccessRequest);
            return Json("Update Successfully", JsonRequestBehavior.AllowGet);

        }

        public ActionResult UpdateUserNodalOfficer(Guid UserId, bool NodalOffice)
        {
            string Msg = string.Empty;
            try
            {

                mUsers model = (mUsers)Helper.ExecuteService("User", "GetUserDetailsByUserID", new mUsers { UserId = UserId });
                model.IsNodalOfficer = NodalOffice;

                var IsUpdate = (bool)Helper.ExecuteService("User", "UpdateUserNodalOffice", model);
                if (IsUpdate)
                {
                    if (NodalOffice)
                    {
                        RecipientGroupMember obj = new RecipientGroupMember();
                        obj.GroupID = 163;
                        obj.AadharId = model.AadarId;
                        obj.MobileNo = model.MobileNo;
                        var DepartmentNames = (string)Helper.ExecuteService("ContactGroups", "GetAllDepartmentByDepartmentCode", model.DeptId);
                        obj.Name = DepartmentNames;
                        obj.Gender = model.Gender;
                        obj.Designation = model.Designation;
                        obj.Email = model.EmailId;
                        obj.Address = model.Address;
                        obj.PinCode = "";
                        obj.IsActive = true;
                        obj.DepartmentCode = model.DeptId;
                        obj.CreatedDate = DateTime.Now;                       
                        Helper.ExecuteService("ContactGroups", "CreateNewContactGroupMember", obj);

                    }
                    else
                    {

                        RecipientGroupMember obj = (RecipientGroupMember)Helper.ExecuteService("ContactGroups", "GetContactGroupMembersByAadharId", new RecipientGroupMember { AadharId = model.AadarId });
                        Helper.ExecuteService("ContactGroups", "DeleteContactGroupMember", obj);
                    }
                    Msg = "Update Successfully";
                }
                else
                    Msg = "Something went wrong...";
            }
            catch (Exception ex)
            {

                Msg = ex.InnerException.ToString();
            }


            return Json(Msg, JsonRequestBehavior.AllowGet);

        }
    }
}
