﻿using Email.API;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;
using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.SystemModule;
using SBL.eLegistrator.HouseController.Web.Areas.RecipientGroups.Models;
using SBL.Service.Common;
using SMS.API;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SBL.eLegistrator.HouseController.Web.Helpers
{
    public static class Helper
    {
        public static object ExecuteService(string ModuleName, string MethodName, object Parameter)
        {
            HouseControllerService.HouseControllerServiceClient scMainClient = new HouseControllerService.HouseControllerServiceClient();
            //SBL.eLegislator.HPMS.ServiceAdaptor.CFService.Service1Client scMainClient = new SBL.eLegislator.HPMS.ServiceAdaptor.CFService.Service1Client();
            ServiceParameter serParam = ServiceParameter.Create(ModuleName, MethodName, Parameter);
            if (null != serParam)
            {
                 Byte[] returnResult = scMainClient.Execute(serParam.ToByteArray());
                if (null != returnResult)
                {

                    object ObjectReturn = SBL.Service.Common.Common.ObjectFromByteArray(returnResult);
                    return ObjectReturn;
                }
            }

            return null;
        }


        // this is for create paging

        public static List<ListItem> BindPager(int recordCount, int currentPage, int pageSize)
        {
            double dblPageCount = (double)((decimal)recordCount / pageSize);
            int pageCount = (int)Math.Ceiling(dblPageCount);
            List<ListItem> pages = new List<ListItem>();
            if (pageCount > 1)
            {
                pages.Add(new ListItem("<<", "1", true));
                if (currentPage > 4)
                {
                    int counnt = 0;
                    int diffrence = pageCount - currentPage;
                    if (diffrence >= 0 && diffrence < 5)
                    {
                        pages.Add(new ListItem("<", (pageCount - 5).ToString(), currentPage > 1));
                        for (int i = pageCount - 4; i <= pageCount; i++)
                        {

                            pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPage));
                            counnt++;
                            if (counnt == 5)
                                break;
                        }
                        pages.Add(new ListItem(">", (currentPage + 1).ToString(), currentPage < pageCount));
                    }
                    else
                    {
                        pages.Add(new ListItem("<", (currentPage - 1).ToString(), currentPage > 1));
                        for (int i = currentPage; i <= pageCount; i++)
                        {
                            pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPage));
                            counnt++;
                            if (counnt == 5)
                                break;
                        }
                        pages.Add(new ListItem(">", (currentPage + 1).ToString(), currentPage < pageCount));

                    }


                }
                else
                {

                    pages.Add(new ListItem("<", (currentPage - 1).ToString(), currentPage > 1));
                    for (int i = 1; i <= pageCount; i++)
                    {
                        pages.Add(new ListItem(i.ToString(), i.ToString(), i != currentPage));
                        if (i == 5)
                            break;
                    }
                    pages.Add(new ListItem(">", (currentPage + 1).ToString(), currentPage < pageCount));

                }

                pages.Add(new ListItem(">>", pageCount.ToString(), true));

            }
            return pages;
        }

    }

    public static class HtmlExtension
    {
        #region Paging

        public static MvcHtmlString Pager(this HtmlHelper helper, int NoOfPages, int loopStart, int loopEnd, int currentPage)
        {
            if (NoOfPages < 1) return MvcHtmlString.Create(string.Empty);
            string GoToTheFirstPage = "GoToTheFirstPage";
            string GotoNextPage = "GoToTheNextPage";
            string GotoPrevPage = "GoToThePreviousPage";
            string of = "Of";

            string Pages = "Pages";
            int loopDifference = loopStart - 5;
            int loopStartSum = loopStart + 5;
            int loopEndSum = loopEnd + 5;

            StringBuilder html = new StringBuilder();
            html.Append("<div class=\"grid-ex-footer\">");
            html.Append("<div class=\"grid-pagination\">");
            if (NoOfPages > 0)
            {
                if (NoOfPages < loopEnd) loopEnd = NoOfPages;
                html.Append("<a href=\"javascript:void(0);\" onclick=\"NextAndPreviousButton(1,1,5)\" title=\"" + GoToTheFirstPage + "\" class=\"grid-page-link grp-state-disabled\"><span class=\"gridpag-icon\" id=\"gridpag-firstpage\">first page</span></a>");
                if (loopStart > 5)
                {
                    html.Append("<a href=\"javascript:void(0);\" onclick=\"NextAndPreviousButton(" + loopDifference + "," + loopDifference + "," + loopDifference + ")\" title=\"" + GotoPrevPage + "\" class=\"grid-page-link grp-state-disabled\">");
                    html.Append("<span class=\"gridpag-icon\" id=\"gridpag-prevpage\">previous page</span></a>");
                }
                html.Append("<ul class=\"gridpage-numbers\">");
                for (int i = loopStart; i <= loopEnd; i++)
                {
                    if (i == currentPage)
                    {
                        html.Append("<li><span class=\"grid-page-selected\">" + i + "</span></li>");
                    }
                    else
                    {
                        html.Append("<li><a href=\"javascript:void(0);\" onclick=\"NextAndPreviousButton(" + i + "," + loopStart + "," + loopEnd + ")\" class=\"grid-num-link\" data-page=\"" + i + "\">" + i + "</a></li>");
                    }
                }
                html.Append("</ul>");
                if (loopStart + 5 < NoOfPages)
                {
                    html.Append("<a href=\"javascript:void(0);\" onclick=\"NextAndPreviousButton(" + loopStartSum + "," + loopStartSum + "," + loopEndSum + ")\" title=\"" + GotoNextPage + "\" class=\"grid-page-link\"><span class=\"gridpag-icon\" id=\"gridpag-nextpage\">next page</span></a>");
                }
                int value = NoOfPages - loopEnd + 1;
                html.Append("<a href=\"javascript:void(0);\" onclick=\"NextAndPreviousButton(" + NoOfPages + "," + value + "," + NoOfPages + ")\" title=\"" + GotoNextPage + "\" class=\"grid-page-link\"><span class=\"gridpag-icon\" id=\"gridpag-lastpage\">last page</span></a>");
                html.Append("<span class=\"grid-page-info\">" + loopStart + " - " + loopEnd + " " + of + " " + NoOfPages + " " + Pages + ".</span>");
            }

            html.Append("</div></div>");
            if (NoOfPages > 0)
            {
                //html.Append("<div class=\"grid-scroller\">");
                //html.Append("<span class=\"grid-scroller-header\">" + GoToPage + "</span>");
                //html.Append("<div class=\"sliderctr\">");
                //html.Append("<span class=\"slidertooltip\"></span>");
                //html.Append("<div id=\"slider\"></div>");
                //html.Append("</div>");
                //html.Append("</div> </div>");
            }
            return MvcHtmlString.Create(html.ToString());
        }

        #endregion Paging
    }

    #region Notification Helper

    public static class Notification
    {
        private static SMSWebServiceLatest.Service1Client SMSServiceClient = new SMSWebServiceLatest.Service1Client();
        private static EmailWebService.Service1Client EmailWebServiceClient = new EmailWebService.Service1Client();

        /// <summary>
        ///
        /// </summary>
        /// <param name="moduleName"></param>
        /// <param name="functionName"></param>
        /// <param name="parameters"></param>
        /// <param name="processingDate"></param>
        public static void Send(string moduleName, string functionName, object model, DateTime? processingDate)
        {
            try
            {
                List<RecipientGroupMember> contactGroupMembers = new List<RecipientGroupMember>();
                //Get the Record from the SystemModules on the basis of the moduleName.
                SystemModuleModel systemMoule = new SystemModuleModel();
                systemMoule.Name = moduleName;
                var moduleObj = Helper.ExecuteService("SystemModule", "GetSystemModuleByName", systemMoule) as SystemModuleModel;

                SystemModuleModel actionObj = new SystemModuleModel();

                if (moduleObj != null)
                {
                    //Get the record from the SystemFunctions on the basis of moduleName and actionName.
                    SystemModuleModel moduleAction = new SystemModuleModel();
                    moduleAction.ActionName = functionName;
                    actionObj = Helper.ExecuteService("SystemModule", "GetModuleActionByName", moduleAction) as SystemModuleModel;
                }

                if (actionObj != null)
                {
                    #region Get All The Contacts Associated to the action.

                    if (!string.IsNullOrEmpty(actionObj.AssociatedContactGroups))
                    {
                        //pass the Comma seperated Associated Contact Groups to a Function and get all the contact details at a time.
                        contactGroupMembers = Helper.ExecuteService("ContactGroups", "GetContactMembersByGroupID", new RecipientGroupMember { AssociatedContactGroups = actionObj.AssociatedContactGroups }) as List<RecipientGroupMember>;
                    }

                    #endregion Get All The Contacts Associated to the action.

                    #region Send SMS Message if SMS Sending is enabled for Module Action.

                    //Get the Templates parse them with Razor Engine.
                    if (actionObj.SendSMS)
                    {
                        //Get the SMS Templates and parse it with Razor Engine (We will be passing the KeyValuePair or model).
                        string SMSTemplateText = actionObj.SMSTemplate;
                        string SMSBody = Razor.Parse(SMSTemplateText, model);

                        //Send the SMS to associated Contact Members.
                        SMSMessageList smslist = new SMSMessageList();
                        smslist.SMSText = SMSBody;

                        List<string> mobileNumbers = new List<string>();
                        foreach (var item in contactGroupMembers)
                        {
                            mobileNumbers.Add(item.MobileNo);
                        }
                        smslist.MobileNo = mobileNumbers;
                        //SMSServiceClient.AddSMSToQueue(smslist);


                    }

                    #endregion Send SMS Message if SMS Sending is enabled for Module Action.

                    #region Send Email Message if Email Sending is enabled for Module Action.

                    if (actionObj.SendEmail)
                    {
                        var config = new TemplateServiceConfiguration
                        {
                            BaseTemplateType = typeof(HtmlTemplateBase<>)
                        };

                        using (var service = new TemplateService(config))
                        {
                            //Get the Email Templates and parse it with Razor Engine.
                            string emailTemplateText = actionObj.EmailTemplate;

                            var modelNew = new SystemModuleModel { Name = "World", EmailTemplate = "someone@somewhere.com" };
                            string resultNew = Razor.Parse(emailTemplateText, model);

                            string emailBoby = Razor.Parse(emailTemplateText, new { Name = "World" });
                        }

                        //send the Email Message to the Associated Groups.
                        CustomMailMessage EmailMessage = new CustomMailMessage();
                        //  EmailWebServiceClient.AddEmailToQueue(EmailMessage);
                    }

                    #endregion Send Email Message if Email Sending is enabled for Module Action.
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                throw;
            }
            //string template = "Hello @Model.Name! Welcome to Razor!";
            //string result = Razor.Parse(template, new { Name = "World" });
        }
        #region Send Email
        public static void SendEmailDetails(mDepartmentPasses EntityModel)
        {
            #region SMS And Email

            List<string> emailAddresses = new List<string>();
            List<string> phoneNumbers = new List<string>();
            SendNotificationsModel model = new SendNotificationsModel();
            SMSMessageList smsMessage = new SMSMessageList();
            CustomMailMessage emailMessage = new CustomMailMessage();

            //# SMS Settings
            if (EntityModel != null)
            {

                emailAddresses.Add(EntityModel.Email);
                phoneNumbers.Add(EntityModel.MobileNo);

            }
            if (emailAddresses != null && phoneNumbers != null)
            {
                foreach (var item in phoneNumbers)
                {
                    smsMessage.MobileNo.Add(item);
                }
                foreach (var item in emailAddresses)
                {
                    emailMessage._toList.Add(item);
                }
            }

            if (EntityModel.Status == 2)
            {
                model.SMSTemplate = "Your entry pass request with request ID :- " + EntityModel.RequestdID + " has been approved by admin, please collect it from Vidhan Sabha";
                model.EmailTemplate = "Your entry pass request with request ID :- " + EntityModel.RequestdID + " has been approved by admin, please collect it from Vidhan Sabha";

            }
            else if (EntityModel.VehicleNumber != null)
            {
                model.SMSTemplate = "Your vehicle pass request with request ID :- " + EntityModel.RequestdID + " has been generated, please keep this for future reference";
                model.EmailTemplate = "Your vehicle pass request with request ID :- " + EntityModel.RequestdID + " has been generated, please keep this for future reference";

            }
            else
            {
                model.SMSTemplate = "Your entry pass request with request ID :- " + EntityModel.RequestdID + " has been generated, please keep this for future reference";
                model.EmailTemplate = "Your entry pass request with request ID :- " + EntityModel.RequestdID + " has been generated, please keep this for future reference";
            }
            string message = model.SMSTemplate;
            smsMessage.SMSText = message;
            smsMessage.ModuleActionID = 1;
            smsMessage.UniqueIdentificationID = 1;
            bool Mobile = false;
            bool Email = false;
            if (EntityModel.MobileNo != null)
            {
                Mobile = EntityModel.MobileNo.Equals(EntityModel.MobileNo) ? true : false;
            }
            if (EntityModel.Email != null)
            {
                Email = EntityModel.Email.Equals(EntityModel.Email) ? true : false;
            }
            emailMessage._isBodyHtml = true;
            emailMessage._subject = EntityModel.AssemblyCode + "th Legislative Assembly Pass Request";

            // model.EmailTemplate = "Your pass request with request ID :- " + EntityModel.RequestdID + " has been generated, please keep this for future reference";

            emailMessage._body = model.EmailTemplate;

            try
            {
                if (EntityModel.MobileNo != null || EntityModel.Email != null)
                {
                    Notification.Send(Mobile, Email, smsMessage, emailMessage);
                }

            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                throw;
            }

            #endregion
        }
        #endregion

        #region Send SMS
        public static void Send(bool sendSMS, bool sendEmail, SMSMessageList smsMessage, CustomMailMessage emailMessage)
        {
            try
            {
                if (sendSMS && smsMessage != null)
                {
                    smsMessage.isLocale = smsMessage.isLocale ?? false;
                    //SMSServiceClient.AddSMSToQueue(smsMessage);
                }
                if (sendEmail && emailMessage != null)
                {
                    //  EmailWebServiceClient.AddEmailToQueue(emailMessage);
                }
            }
            catch (Exception exp)
            {
                string errorMsg = exp.Message;
                throw;
            }
        }

        #endregion Send SMS

        //public static void SendSMSNotification(SMSWebService.SMSMessage SMSMessage)
        //{
        //    SMSServiceClient.SMSToQueue(SMSMessage);
        //}

        //public static void SendSMSNotification(List<SMSWebService.SMSMessage> SMSMessageList)
        //{
        //    SMSServiceClient.SMSListToQueue(SMSMessageList.ToArray());
        //}

        //public static void SendEmailNotification(EmailWebService.CustomMailMessage EmailMessage)
        //{
        //    EmailWebServiceClient.AddEmailToQueue(EmailMessage);
        //}
        public static List<SMSMessagesModel> GetNotificationStatus(int modeulActionID, int UIID)
        {
            //var returned = SMSServiceClient.GetSMSStatusByUIModuleID(new SMSMessageList { UniqueIdentificationID = UIID, ModuleActionID = modeulActionID }) as byte[];
            //var returnedList = returned.ObjectFromByteArray() as List<SMSMessagesModel>;
            var returnedList = new List<SMSMessagesModel>();
            return returnedList;
        }
    }

    #endregion Notification Helper

    #region Validation at Server Side

    public static class ValidationAtServer
    {

        public static bool CheckMobileNumber(string MobileNo)
        {
            if (MobileNo.Trim().Length == 10)
            {
                string MatchPhoneNumberPattern = @"\d{10}";
                if (MobileNo != null) return Regex.IsMatch(MobileNo, MatchPhoneNumberPattern);
                else return false;
            }
            else
            {
                return false;
            }
        }

    }
    #endregion

    #region Pagination at Server Side

    public class Pagination
    {
        public int PageId { get; set; }
        public int PageSize { get; set; }
        public string SortCoulmnName { get; set; }
        public string SortOrder { get; set; }
        public string SortOrderingClass { get; set; }
        public string SearchWord { get; set; }
        public string TableId { get; set; }
        public int TotalRow { get; set; }
        public string FunctionType { get; set; }
    }
    #endregion
}