﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegistrator.HouseController.Web.Helpers
{
    public class PagedList<T> where T : class
    {
        public IEnumerable<T> List { get; set; }
        public int CurrentPage { get; set; }
        public int RowsPerPage { get; set; }
        public int TotalRecordCount { get; set; }
        public int LoopStart { get; set; }
        public int LoopEnd { get; set; }

        //For Search if needs
        public string SearchText { get; set; }

        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}