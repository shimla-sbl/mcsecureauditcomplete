﻿function fillFilterProgramme() {
    var pathurl = '/Constituency/Constituency/GetAllProgramme';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Program").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Program").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#Program").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterWorkStatus() {
    var pathurl = '/Constituency/Constituency/getAllWorkStatus';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Status").html("");
            $("#Status").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Status").append($("<option></option>").val(item.value).html(item.Text))
            }
             $("#workStatusID").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterWorkType() {
    var pathurl = '/Constituency/Constituency/getAllWorkType';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#workType").html("");
            $("#workType").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#workType").append($("<option></option>").val(item.value).html(item.Text))
            }

            $("#workType").trigger("chosen:updated");

            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterOwnerDepartment(memberCode) {
   
    if (memberCode === undefined)
    {
        memberCode = '';
    }
   
    if (memberCode == '' || memberCode == null) {
        var pathurl = '/Constituency/Constituency/getAllDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',
            success: function (data) {
                $("#OwnerDepartment").html("");
                $("#OwnerDepartment").append($("<option></option>").val('0').html('--All--'))

                for (var i = 0; i < data.length; i++) {
                    var item = data[i];

                    $("#OwnerDepartment").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#OwnerDepartment").trigger("chosen:updated");
                $("#loader-wrapper").hide();
            }
        });
    } else
    {
      //  alert('memberCode1');
        var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { memberCode: memberCode },
            success: function (data) {
                $("#OwnerDepartment").html("");
                $("#OwnerDepartment").append($("<option></option>").val('0').html('--All--'))

                for (var i = 0; i < data.length; i++) {
                    var item = data[i];

                    $("#OwnerDepartment").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#OwnerDepartment").trigger("chosen:updated");
                $("#loader-wrapper").hide();
            }

        });
        
        
    }
}
function fillFilterFinancialYear() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#FinancialYear").html("");
            $("#FinancialYear").append($("<option></option>").val('0').html('-- All (Current Legislative Assembly) --'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#FinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterAllDemand() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllDemand';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#Demand").html("");
            $("#Demand").append($("<option></option>").val('0').html('--All--'))

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#Demand").append($("<option></option>").val(item.value).html(item.value+ "-" + item.Text))
            }

            $("#Demand").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterControllingAuthority(districtID) {
    var pathurl = '/Constituency/Constituency/getAllControllingAuthority';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data:{districtID:districtID},
        success: function (data) {
            $("#ControllingAuthority").html("");
            $("#ControllingAuthority").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#ControllingAuthority").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#ControllingAuthority").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterExecutingDepartment(memberCode) {
    
    if (memberCode == '') {
        var pathurl = '/Constituency/Constituency/getAllDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',

            success: function (data) {
                $("#Agency").html("");
                $("#Agency").append($("<option></option>").val('0').html('--All--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#Agency").trigger("chosen:updated");

                $("#loader-wrapper").hide();
            }
        });
    }
    else
    {
        var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
        $.ajax({
            url: pathurl,
            type: 'GET',
            data: { memberCode: memberCode },
            success: function (data) {
                $("#Agency").html("");
               $("#Agency").append($("<option></option>").val('0').html('--All--'))
                for (var i = 0; i < data.length; i++) {
                    var item = data[i];
                    $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
                }
                $("#Agency").trigger("chosen:updated");

                $("#loader-wrapper").hide();
            }
        });
       // $("#Agency").val(currntdeptid);
        
    }
}
//function fillFilterFinancialYear() {
//    var pathurl = '/Constituency/Constituency/getFinancialYearList';
//    $.ajax({
//        url: pathurl,
//        type: 'GET',
//        success: function (data) {
//            $("#FinancialYear").html("");
//            $("#FinancialYear").append($("<option></option>").val('0').html('--All--'))
//            for (var i = 0; i < data.length; i++) {
//                var item = data[i];

//                $("#FinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
//            }
//        }
//    });
//}

function fillFilterExecutingDepartment_current(CurrentDept) {
  
    var pathurl = '/Constituency/Constituency/getAllmemberDepartment';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {},
        success: function (data) {
            $("#Agency").html("");
            //  $("#Agency").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Agency").append($("<option></option>").val(item.value).html(item.Text))
            }
           $("#Agency").trigger("chosen:updated");
           // $("#Agency").append(CurrentDept);
          //  $("#loader-wrapper").hide();
        }
         
    }).done(function () { 
        $("#Agency").val(CurrentDept);
        $("#Agency").trigger("chosen:updated");
    });
}

function fillstatusFinancialYear() {
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#statusFinancialYear").html("");

            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#statusFinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
        }
    });
}

//
function fillFilterPanchayatByConstituency(constituencyID) {
   // alert(constituencyID);
    var pathurl = '/Constituency/Constituency/getConstituencyPanchayat';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { constituencyID: constituencyID },
        success: function (data) {
            $("#Panchayat").html("");
            $("#Panchayat").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Panchayat").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#Panchayat").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}
function FilterDistrictList()
{
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/GetAllDistrict';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#District").html("");
            $("#District").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];

                $("#District").append($("<option></option>").val(item.value).html(item.Text))
            }
            $("#District").trigger("chosen:updated");
            fillFilterConstituencyByDistrict($("#District :selected").val());
            $("#loader-wrapper").hide();
        }
    });
}
function fillFilterConstituencyByDistrict(DistrictID) {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getDistrictConstituency';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { DistrictID: DistrictID },
        success: function (data) {
            $("#Constituency").html("");
            $("#Constituency").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#Constituency").append($("<option></option>").val(item.value).html(item.Text))
            }
         
            fillFilterPanchayatByConstituency($("#Constituency :selected").val());
            $("#Constituency").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}