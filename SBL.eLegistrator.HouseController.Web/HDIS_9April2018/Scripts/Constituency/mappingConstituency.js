﻿function fillFinancialYear() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getFinancialYearList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#FinancialYear").html("");
        

            for (var i = 0; i < data.length; i++) {
                var item = data[i];
              
                $("#FinancialYear").append($("<option></option>").val(item.yearValue).html(item.yearText))
            }
            $("#loader-wrapper").hide();
        }
    });
}

function fillAllDemand() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllDemand';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
         
            $("#DemandID").html("");
            $("#DemandID").append($("<option></option>").val("0").html("--All--"))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#DemandID").append($("<option></option>").val(item.value).html(item.value + '  -  ' + item.Text))
              
            }
            $("#DemandID").trigger("chosen:updated");
      
            $("#loader-wrapper").hide();
        }
    });
}
function fillAllDistrict() {
    $("#loader-wrapper").show();
    var pathurl = '/Constituency/Constituency/getAllDistrict';
    $.ajax({
        url: pathurl,
        type: 'GET',
        success: function (data) {
            $("#District").html("");
            $("#District").append($("<option></option>").val('0').html('--All--'))
            for (var i = 0; i < data.length; i++) {
                var item = data[i];
                $("#District").append($("<option></option>").val(item.value).html( item.Text))
            }
            $("#District").trigger("chosen:updated");
            $("#loader-wrapper").hide();
        }
    });
}

function GetFilterSchemeList() {
    $("#loader-wrapper").show();
    var FinancialYear = $("#FinancialYear option:selected").val();
    var ControllingAuthority = $("#ControllingAuthority option:selected").val();
    var Demand = $("#DemandID option:selected").val();
    var District = $("#District option:selected").val();
    var pathurl = '/Constituency/Constituency/GetFilterSchemeList';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: { FinancialYear: FinancialYear, ControllingAuthority: ControllingAuthority, Demand: Demand, District: District },
        success: function (data) {
          //  $('#outerDiv').show();
            //$("#outerDiv").html(data);
            //divDataContainer
              $('#divDataContainer').show();
            $("#divDataContainer").html(data);
           // document.getElementById("MainDiveThreeTire").style.display = "none";
            //document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
        }
    });
}
$(".allconstituency").click(function (obj) {
    if ($(obj.target.id).checked) {
        $(obj.target.id).val('true');
    }
    else {
        $(obj.target.id).val('false');
    }
});

function checkAll(ele, cssclass) {
    var checkboxes = document.getElementsByClassName(cssclass);
    if (ele.checked) {
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox' && (!$(checkboxes[i].id).is(':disabled'))) {
                checkboxes[i].checked = true;
            }
        }
    }
    else {
        debugger;
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].type == 'checkbox' && ($(checkboxes[i].id).prop({ disabled: false }))) {
                checkboxes[i].checked = false;
            }
        }
    }
}
function getAllCheckedAllConstituency() {
    var checkedID = '';
    $(".allconstituency").each(function (event, obj) {
        if ($(obj).is(':checked')) {
            if (checkedID != '') {
                checkedID = checkedID + ',' + obj.id;
            } else {
                checkedID = obj.id;
            }
        }
    });
    return checkedID;
}
function getAllCheckedmyConstituency() {
    var mycheckedID = '';
    $(".myconstituency").each(function (event, obj) {
        if ($(obj).is(':checked')) {
            if (mycheckedID != '') {
                mycheckedID = mycheckedID + ',' + obj.id;
            } else {
                mycheckedID = obj.id;
            }
        }
    });
    return mycheckedID;
}
function sendWorkToConstituency() {
    var checkedSScheme = getAllCheckedAllConstituency();
    if (checkedSScheme != '') {
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                $("#loader-wrapper").show();
                var pathurl = '/Constituency/Constituency/saveConstituencytoMyList';
                $.ajax({
                    url: pathurl,
                    type: 'GET',
                    data: { checkedList: checkedSScheme },
                    success: function (data) {
                        ShowNotification("success", "Work Has been Added in my work List.");
                        var checkboxes = document.getElementsByTagName('input');

                        for (var i = 0; i < checkboxes.length; i++) {
                            if (checkboxes[i].type == 'checkbox') {
                                checkboxes[i].checked = false;
                            }
                        }
                        GetFilterSchemeList();
                    }
                });
            }
        });
    }
    else {
        alert('please select a work before send');
    }
}
function deletemyConstituencyWorks() {
    var mycheckedSScheme = getAllCheckedmyConstituency();
    if (mycheckedSScheme != '') {
        bootbox.confirm("Are you sure?", function (result) {
            if (result) {
                $("#loader-wrapper").show();
                var pathurl = '/Constituency/Constituency/deleteConstituencyfromMyList';
                $.ajax({
                    url: pathurl,
                    type: 'GET',
                    data: { checkedList: mycheckedSScheme },
                    success: function (data) {
                        var checkboxes = document.getElementsByTagName('input');
                        ShowNotification("success", "Work Has been deleted from my work List.");
                        for (var i = 0; i < checkboxes.length; i++) {
                            if (checkboxes[i].type == 'checkbox') {
                                checkboxes[i].checked = false;
                            }
                        }
                        GetFilterSchemeList();
                    }
                });
            }
        });
    }
    else {
        var dlg = bootbox.alert('please select a work before send .', function (e) {
            $(dlg).modal('hide');
            //alert('please select a work before send');
        });
    }
}


