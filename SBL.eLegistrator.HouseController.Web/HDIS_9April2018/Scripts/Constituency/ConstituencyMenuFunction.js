﻿function getDashboard() {
    $("#loader-wrapper").show();
    var cusMsg = $("#CustomMessage").val();

    $.ajaxSetup({ cache: false });
    var pathurl = '/Constituency/Mapping/mainaccountadmindashboard';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {
        },
        success: function (data) {

            $('#outerDiv').show();
            $("#outerDiv").html(data);
            document.getElementById("MainDiveThreeTire").style.display = "none";
            document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
        }
    });

}
function ChooseMyConstituencyWorks() {

    $("#loader-wrapper").show();
    $.ajaxSetup({ cache: false });
    var pathurl = '/Constituency/Constituency/constituecyMapping';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {
        },
        success: function (data) {

            $('#outerDiv').show();
            $("#outerDiv").html(data);
            document.getElementById("MainDiveThreeTire").style.display = "none";
            document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
        }
    });
}
function MyConstituencyWorks() {
    $("#loader-wrapper").show();
    $.ajaxSetup({ cache: false });
    var pathurl = '/Constituency/Constituency/MyConstituencyWorks';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {
        },
        success: function (data) {

            $('#outerDiv').show();
            $("#outerDiv").html(data);
            document.getElementById("MainDiveThreeTire").style.display = "none";
            document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
        }
    });
}
function constituencyListForOfficer() {
    $("#loader-wrapper").show();
    $.ajaxSetup({ cache: false });
    var pathurl = '/Constituency/Constituency/constituencyListForOfficer';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {
        },
        success: function (data) {

            $('#outerDiv').show();
            $("#outerDiv").html(data);
            document.getElementById("MainDiveThreeTire").style.display = "none";
            document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
        }
    });
}

function getPMISReport()
{
   console.log('coming');
    $("#loader-wrapper").show();
    $.ajaxSetup({ cache: false });
    var pathurl = '/Constituency/PMIS/PMISReport';
    $.ajax({
        url: pathurl,
        type: 'GET',
        data: {
        },
        success: function (data) {
          
            $('#outerDiv').show();
            $("#outerDiv").html(data);
            document.getElementById("MainDiveThreeTire").style.display = "none";
            document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
            $(".page-header h1").text("PMIS Reports");
        }
    });
}


function AssignOfficer() {
    debugger;
    $("#loader-wrapper").show();
    $.ajaxSetup({ cache: false });
    var pathurl = '/Constituency/Constituency/AssignOfficer';
    $.ajax({
        url: pathurl,
        type: 'GET',
       
        success: function (data) {
          
            $('#outerDiv').show();
            $("#outerDiv").html(data);
            document.getElementById("MainDiveThreeTire").style.display = "none";
            document.getElementById("MainDiveTwoTire").style.display = "none";
            $("#loader-wrapper").hide();
           // $(".page-header h1").text("PMIS Reports");
        }
    });
}
