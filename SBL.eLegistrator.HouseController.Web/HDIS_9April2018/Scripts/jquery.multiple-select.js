/**
 * jQuery Multiple Select Plugin
 * @author Sebastian Noll <http://basti.4ym.org/>
 * @date September 22, 2010
 * @copyright (c) 2010 Sebastian Noll
 * @license GNU/GPL <http://www.gnu.org/licenses/gpl-3.0.html>
 * @version 0.9
 */
$.fn.multipleSelect = function(settings) {
	var config = {};
	if(settings)
	{
		$.extend(config, settings);
	}
	return this.each(function() {
		var select = $(this);
		var values = new Array();
		select.bind("click", function() {
			select.val(values);
		});
		select.find("option").bind("mousedown", function() {
			var option = $(this);
			values = select.val();
			if(option.attr("selected"))
			{
				var newValues = new Array();
				for(var i = 0; i < values.length; i++)
				{
					if(values[i] != option.attr("value"))
					{
						newValues.push(values[i]);
					}
				}
				values = newValues;
			}
			else
			{
				if(!values)
				{
					values = new Array();
				}
				values.push(option.attr("value"));
			}
			select.val(values);
			return false;
		});
	});
};