﻿(function( $ ) {

    String.prototype.endsWith = function (str) {

        try {
            return (this.match(str + "$") == str);
        }
        catch (err) {
            return this;
        }
    }

    String.prototype.startsWith = function (str) {

        try {
            return (this.match("^" + str) == str);
        }
        catch (err) {
            return this;
        }
    }

    String.prototype.trim = function (str) {

        try {
            return this.replace(/^\s+|\s+$/, "");
        }
        catch (err) {
            return this;
        }
    }

    String.prototype.trimFullStops = function () {

        try {
            return this.replace(/^\.+|\.+$/, "");
        }
        catch (err) {
            return this;
        }

    };

    String.prototype.replaceAll = function (oldChar, newChar) {

        try {
            return this.split(oldChar).join(newChar);
        }
        catch (err) {
            return this;
        }
    }


    String.prototype.short = function (nLen) {

        try {

            var nFSPos = this.indexOf('.');

            return (this.length > nLen) ? ((nFSPos > -1) && (nFSPos < nLen + 1) && (nFSPos > 3)) ? this.split('.')[0].trim() + '…' : this.substring(0, nLen).trim() + '…' : this;
        }
        catch (err) {
            return this;
        }
    };

    String.prototype.replaceNewLine = function () {

        try {
            return this.replace(/(\r\n|\r|\n)/g, "<br />");
        }
        catch (err) {
            return this;
        }
    }

    String.prototype.replaceBreaks = function () {

        try {

            return this.replace(/<br \/>|<br\/>/g, "\n");
        }
        catch (err) {
            return this;
        }
    }

    String.prototype.encode = function () {

        try {

            return (this.length > 0) ? encodeURIComponent(this) : this;
        }
        catch (err) {
            return this;
        }

    };

    String.prototype.replaceQuotes = function () {

        try {

            return this.replace(/"/g, "\\\"");
        }
        catch (err) {
            return this;
        }
    }

    String.prototype.stripTags = function () {

        try {

            return this.replace(/<\S[^>]*>/g, "");
        }
        catch (err) {
            return this;
        }
    }

    String.prototype.tidyNumeric = function () {

        try {

            return Math.abs(this.replace(/[^0-9.]/ig, '').trimFullStops());
        }
        catch (err) {
            return this;
        }

    };


    String.prototype.left = function (n) {

        try {

            return this.substr(0, n);
        }
        catch (err) {
            return this;
        }

    };

    String.prototype.right = function (n) {

        try {

            return this.substr((this.length - n), this.length);
        }
        catch (err) {
            return this;
        }

    };

    String.prototype.clearPunc = function () {

        try {

            return this.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g, "").replace(/\s{2,}/g, " ");
        }
        catch (err) {
            return this;
        }
    }

    String.prototype.highlight = function (vWords) {

        try {

            var oWords = vWords.clearPunc().stripTags().split(' '), vNewPhrase = this;

            oWords.each(function (o) {

                vNewPhrase = vNewPhrase.replace(new RegExp("(" + o + ")", "ig"), '<span class="highlight">$1</span>');
            });

            return vNewPhrase;
        }
        catch (err) {
            return this;
        }

    }

    String.prototype.splice = function (i, n, str) {

        try {

            var characterArray = this.split("");

            Array.prototype.splice.apply(
                characterArray,
                arguments
            );

            return (
                characterArray.join("")
            );
        }
        catch (err) {
            return this;
        }
    }

    Array.prototype.remove = function (from, to) {

        try {

            var rest = this.slice((to || from) + 1 || this.length);

            this.length = from < 0 ? this.length + from : from;

            return this.push.apply(this, rest);
        }
        catch (err) {
            return this;
        }

    };

}(jQuery));