﻿

//path: ~/ScriptigFunctions/UserRegistrationForm/UserRegistrationScriptingFunctions.js

$(document).ready(function () {

    //$("#EmpOrMemberCode").numeric();
    $('#captchacontainer').find('br').remove();
    $('#CaptchaInputText').attr('placeholder', 'Enter the text Shown');
    $("#AdhaarID").val("");
    $("#EmpOrMemberCode").val("");
    $("#OTPValue").val("");
    $("#otpId").text("");
    $("#addressSpan").hide();
    $("#getPhotoFromAdhaar").hide();
    $(".field-validation-error").val("");
    $("#EmpOrMemberCode").attr("placeholder", "Enter Member code");
    $("#1").prop("checked", true);
    $("#SelectType").val("1");
    $("input[name$='SelectType']").click(function () {
        $("#Name").html("");
        $("#Address").html("");
        $("#EmpOrMemberCode").val("");
        $("#FatherName").html("");
        $("#DSCHash").val('');
        $("#DSCResultView").hide();
        $("#DSCName").val('');
        $("#DSCType").val('');
        $("#DSCValidity").text('');
        $("#AdhaarID").val("");
        $("#FatherName").html("");
        $("#Gender").text("");
    });
    $("#deptList").hide();
    $("#HashModel").hide();
    $("#secretoryField").hide();
    $("#Name").html("");
    $("#Address").html("");
    $("#FatherName").text("");
    $("#Gender").text("");
    $("#DSCNameValue").html("");
    $("#DSCTypeValue").html("");
    $("#AdhaarID").focus();
    $("#DdlSecDept").multipleSelect({ selectAll: false, placeHolder: "Select Department" });
});
var flag = false;
var otpvalue = "";

function GetDetailsByAadharID() {

    var AdhaarID = $('#AdhaarID').val();
    alert('addd' + AdhaarID);
    if (AdhaarID.length == 12) {

        if (isNaN(AdhaarID)) {

            alert("Aadhar Id should be in digits.");
            $('#AdhaarID').val("");
            return false;
        }
        else {
            $.ajaxSetup({ cache: false });
            $.ajax({
                url: "/UserRegistration/UserRegistration/CheckAdhaarID",
                type: 'GET',
                dataType: "text",
                data: {
                    AdhaarID: AdhaarID,
                    IsModel: "abc"
                },
                success: function (data) {
                    if (data != "" && data != null) {

                        alert("This Adhaar ID is already registered");
                        $('#AdhaarID').val('');

                        return false;
                    }
                    else {
                        var cl = new CanvasLoader('canvasloader-container1');
                        cl.show();
                        $.ajaxSetup({ cache: false });
                        $.ajax({
                            url: "/UserRegistration/UserRegistration/GetAdhaarDetails",
                            type: 'GET',
                            data: {
                                AdhaarID: AdhaarID
                            },
                            success: function (result) {
                                //    alert(result);
                                var adhaarMobile, adhaaremail, dob, photo, address, adharname, adhargender, adharFathername;
                                if (result != "False" && result != null && result != "") {

                                    $("#AdhaarIDDetails").html(result);
                                    $('#adhaarDetailsbyID tr').each(function () {
                                        adhaarMobile = $('#adhaarDetailsbyID').find("#adhaarMobile").html();
                                        adhaaremail = $('#adhaarDetailsbyID').find("#adhaaremail").html();
                                        dob = $('#adhaarDetailsbyID').find('#adhaarDOB').html();
                                        address = $('#adhaarDetailsbyID').find("#aadharAddress").html();
                                        adharname = $('#adhaarDetailsbyID').find("#adharName").html();
                                        adhargender = $('#adhaarDetailsbyID').find("#adharGender").html();
                                        adharFathername = $('#adhaarDetailsbyID').find("#adharFathername").html();

                                    });

                                    $('#Mobile').val($.trim(adhaarMobile));
                                    $('#MailID').val($.trim(adhaaremail));
                                    $('#DOB').val($.trim(dob));
                                    $('#Name').val($.trim(adharname));
                                    $('#Gender').val($.trim(adhargender));
                                    $('#FatherName').val($.trim(adharFathername));
                                    cl.hide();
                                    $("#addressSpan").show();
                                    photo = $('img[id="image"]').attr('src');

                                    if (photo.length > 50) {
                                        $("#getPhotoFromAdhaar").show();
                                    }
                                    if (address != "") {
                                        $("#addressSpan").show();
                                    }
                                }
                                else {
                                    alert("Either Aadhar Details Not found or Network not found!");
                                    // $('#AdhaarID').val('');
                                    cl.hide();
                                    return false;
                                }

                            },

                            error: function (xhr, ajaxOptions, thrownError) {

                                alert(xhr.responseText);
                                alert(thrownError);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    alert(xhr.responseText);
                    alert(thrownError);
                }
            });
        }
    }
    else {
        alert("Aadhar Id Should be in 12 numbers.");
        $('#AdhaarID').val('');
        return false;
    }
}


$('#AdhaarID').change(function () {

    var AdhaarID = $('#AdhaarID').val();
    if (AdhaarID.length == 12) {

        if (isNaN(AdhaarID)) {

            alert("Aadhar Id should be in digits.");
            $('#AdhaarID').val("");
            return false;
        }
        else {
            $.ajaxSetup({ cache: false });
            $.ajax({
                url: "/UserRegistration/UserRegistration/CheckAdhaarID",
                type: 'GET',
                dataType: "text",
                data: {
                    AdhaarID: AdhaarID,
                    IsModel: "abc"
                },
                success: function (data) {
                    if (data != "" && data != null) {

                        alert("This Adhaar ID is already registered");
                        $('#AdhaarID').val('');

                        return false;
                    }
                    else {
                        var cl = new CanvasLoader('canvasloader-container1');
                        cl.show();
                        $.ajaxSetup({ cache: false });
                        $.ajax({
                            url: "/UserRegistration/UserRegistration/GetAdhaarDetails",
                            type: 'GET',
                            data: {
                                AdhaarID: AdhaarID
                            },
                            success: function (result) {
                                //    alert(result);
                                var adhaarMobile, adhaaremail, dob, photo, addressadharname, adhargender, adharFathername;
                                if (result != "False" && result != null && result != "") {

                                    $("#AdhaarIDDetails").html(result);
                                    $('#adhaarDetailsbyID tr').each(function () {
                                        adhaarMobile = $('#adhaarDetailsbyID').find("#adhaarMobile").html();
                                        adhaaremail = $('#adhaarDetailsbyID').find("#adhaaremail").html();
                                        dob = $('#adhaarDetailsbyID').find('#adhaarDOB').html();
                                        address = $('#adhaarDetailsbyID').find("#aadharAddress").html();
                                        adharname = $('#adhaarDetailsbyID').find("#adharName").html();
                                        adhargender = $('#adhaarDetailsbyID').find("#adharGender").html();
                                        adharFathername = $('#adhaarDetailsbyID').find("#adharFathername").html();
                                    });

                                    $('#Mobile').val($.trim(adhaarMobile));
                                    $('#MailID').val($.trim(adhaaremail));
                                    $('#tDOB').val($.trim(dob));
                                    $('#Name').val($.trim(adharname));
                                    $('#Gender').val($.trim(adhargender));
                                    $('#FatherName').val($.trim(adharFathername));
                                    cl.hide();

                                    photo = $('img[id="image"]').attr('src');

                                    if (photo.length > 50) {
                                        $("#getPhotoFromAdhaar").show();
                                    }
                                    if (address != "") {
                                        $("#addressSpan").show();
                                    }

                                }
                                else {
                                    alert("Either Aadhar Details Not found or Network not found!");
                                    //$('#AdhaarID').val('');
                                    cl.hide();
                                    return false;
                                }

                            },

                            error: function (xhr, ajaxOptions, thrownError) {

                                alert(xhr.responseText);
                                alert(thrownError);
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    alert(xhr.responseText);
                    alert(thrownError);
                }
            });
        }
    }
    else {
        alert("Aadhar Id Should be in 12 numbers.");
        $('#AdhaarID').val('');
        return false;
    }

});

$('#Mobile').change(function () {

    var MobilNumber = $('#Mobile').val();
    if (MobilNumber.length > 0) {
        MobilNumber = MobilNumber.replace("(", "");
        MobilNumber = MobilNumber.replace(")", "");
        MobilNumber = MobilNumber.replace("-", "");
        MobilNumber = MobilNumber.replace(" ", "");
        if (isNaN(MobilNumber)) {
            alert("Make Sure you entered valid Mobile number.");
            $('#Mobile').val('');
            $('#Mobile').focus();
            return false;

        }
        if (MobilNumber.length != 10) {
            alert("Make sure Mobile Number should be in 10 digits.");
            $('#Mobile').val('');
            return false;
        }
    }

});
//$('#Age').change(function () {
//    var Age = $('#Age').val();
//    if (isNaN(Age)) {
//        alert("Make Sure you entered valid Age.");
//        $('#Age').val('');
//        $('#Age').focus();
//        return false;

//    }
//    if (Age > 150) {
//        alert("Onely less then 150 age is permitted.");
//        $('#Age').val('');
//        return false;
//    }
//})


function getHash() {
    var Id = document.getElementById("EmpOrMemberCode").value;
    if (Id == "") {
        alert("Maker sure you have entered employee or member id");
        return false;
    }
    var dialog = $("#HashModel").removeClass('hide').dialog({
        resizable: false,
        modal: true,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i>Select DSC Certificate</h4></div>",
        width: 880,
        title_html: true,

        buttons: [

            {
                text: "OK",
                "class": "btn btn-primary btn-xs",
                click: function () {
                    getHashModel();
                    $(this).dialog("close");
                }
            }
        ]
    });

}

//jQuery(function ($) {

//    $("#getHash").on('click', function (e) {
//        e.preventDefault();

//        var Id = document.getElementById("EmpOrMemberCode").value;
//        alert(Id);
//        if (Id == "") {
//            alert("Maker sure you have entered employee or member Code");
//            return false;
//        }
//        $("#HashModel").removeClass('hide').dialog({
//            resizable: false,
//            modal: true,
//            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i>Select DSC Certificate</h4></div>",
//            width: 880,
//            title_html: true,
//            buttons: [
//                {
//                    html: "&nbsp; Okay",
//                    "class": "btn btn-primary",
//                    click: function () {
//                        getHashModel();
//                        $(this).dialog("close");
//                    }
//                }
//            ]
//        });
//    });
//});

function getHashModel() {

    var key = document.getElementById("DSCHashUniqueID").getCertHash();
    var DSCName = document.getElementById("DSCHashUniqueID").getCertName();
    var DSCType = document.getElementById("DSCHashUniqueID").getCertType();
    var DSCValidity = document.getElementById("DSCHashUniqueID").getCertValidity();
    var currentDate = new Date();
    $("#DSCHash").val(key);

    $.ajaxSetup({ cache: false });
    $.ajax({
        url: "/UserRegistration/UserRegistration/CheckDSCHaskKey",
        type: 'GET',
        data: {
            key: key
        },
        success: function (data) {
            if (data != null && data != "") {
                alert("Selected DSC Already Registered.");
                $("#DSCHash").val('');
                return false;
            }
            else {
                if (key != "") {
                    $("#DSCResultView").removeClass('hide');
                    if ($("#applateResultDiv").hasClass("hide")) {
                        $("#applateResultDiv").removeClass('hide');
                    }
                    $("#DSCNameValue").text(DSCName);
                    $("#DSCName").val(DSCName);
                    $("#DSCTypeValue").text(DSCType);
                    $("#DSCEnrollValue").text(currentDate.toDateString());
                    $("#DSCValidityValue").text(DSCValidity);
                    $("#DSCType").val(DSCType);
                    $("#DSCValidity").val(DSCValidity);
                    $("#DSCResultView").show();
                    return true;
                }
                return false;

            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            alert(xhr.responseText);
            alert(thrownError);
        }
    });

}

function checkValidation() {
    
    var PreMobile = $("#ValidateMobile").val();
    var NewMobile = $("#Mobile").val();
    otpvalue = $("#hdnOTPValue").val();// document.getElementById("hdnOTPValue").value;
    var otpId = $("#OTPValue").val();// document.getElementById("OTPValue").value;
    var OtpRequiredInRegistration = $("#hdnOtpRequiredInRegistration").val();// document.getElementById("OtpRequiredInRegistration").value;
    
    var secretoryId = $('#DdlSecretory').val();
    var selectType = $('#SelectType').val();
    if (secretoryId == 0 && selectType == "2") {
        $("#DDLSecretaryValidation").text("Please Select Secretary of employee");
        return false;
    } 
    if (secretoryId != 0 && selectType == "2") {

        if ($("#btnmultichkbox").text() == "") {
            $("#DdlSecDeptValidation").text("Please Select Department Associate");
            return false;
        }
    }
    if (OtpRequiredInRegistration == "1") {
        if (PreMobile == NewMobile && otpvalue == otpId) {
            $.validator.addMethod('accept', function () { return true; });
            return true;
        }
        else {
            if (PreMobile != NewMobile) {
                alert("Please Generate OTP to validate mobile number.");
                return false;
            }
            else {

                if (otpvalue != otpId) {
                    alert("OTP didn't match");
                    return false;
                }
                else {
                    $.validator.addMethod('accept', function () { return true; });
                    return true;
                }
            }
        }
    }
    
}

$("#btnmultichkbox").click(function () {
    // alert("sdssadsd");
    $("#DdlSecDeptValidation").text("");
});

function ResetForm() {

    window.location.href = 'UserRegistration';
    //@Url.Action("Index", "Home", new { Area = "" })';
    //@Url.Action("UserRegistraitonForm", "UserRegistration", new { Area = "UserRegistration" })';
    //$.ajaxSetup({ cache: false });
    //$.ajax({
    //    url: "/UserRegistration/UserRegistration/UserRegistraitonForm",
    //    type: 'GET',
    //    data: {

    //    },
    //    success: function (data) {

    //    },
    //    error: function (xhr, ajaxOptions, thrownError) {
    //        alert(xhr.responseText);
    //        alert(thrownError);
    //    }
    //});
}

//$('#DdlSecretory').change(function () {
//    var secretoryId = $('#DdlSecretory').val();
//    $("#DDLSecretaryValidation").text("");
//    if (secretoryId != 0) {
//        $.ajaxSetup({ cache: false });
//        $.ajax({
//            url: "/UserRegistration/UserRegistration/GetSelectedDeptBySecID",
//            type: 'GET',
//            data: {
//                secratoryId: secretoryId
//            },
//            success: function (data) {
//                if (data) {
//                    $("#ddldepartment").hide();
//                    $("#departmentChkBoxList").html(data);
//                    $("#SecretaryDepartmentList").show();
//                }
//                else {
//                    $("#ddldepartment").show();
//                    alert("Unable to Found the Emp or Member Id");
//                }
//            },
//            error: function (xhr, ajaxOptions, thrownError) {
//                alert(xhr.responseText);
//                alert(thrownError);
//            }
//        });
//    }
//    else {
//        $('#DDLSecretaryValidation').text("Please Select Secretary");
//        $("#SecretaryDepartmentList").hide();
//    }
//});

function CheckOTPByID() {
    var response = false;
    var empId = document.getElementById("EmpOrMemberCode").value;
    var Mobile = document.getElementById("Mobile").value;
    var OTPValue = document.getElementById("OTPValue").value;
    $.ajaxSetup({ cache: false });
    $.ajax({
        url: "/UserRegistration/UserRegistration/CheckOTPByID",
        type: 'GET',
        data: {
            empId: empId,
            Mobile: Mobile,
            OTPValue: OTPValue
        },
        success: function (data) {
            if (data.OTPid != "") {

                if (data.OTP != OTPValue) {
                    alert("OTP didn't match.");
                    $('#OTPValue').val('');
                    return false;
                }
                response = true;
            }
            else {
                alert("OTP didn't match.");
                $('#OTPValue').val('');
                response = false;
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

            alert(xhr.responseText);
            alert(thrownError);
        }
    });
    return response;
}

$('#EmpOrMemberCode').change(function () {
    var value = $('#EmpOrMemberCode').val();

    var selectType = $("#SelectType").val();;
    if (isNaN(value)) {
        if (selectType == "1") {
            alert("Member Code must be in digit.");
            $("#EmpOrMemberCode").val("");
            return false;
        }
        else if (selectType == "2") {
            alert("Employee Code must be in digit.");
            $("#EmpOrMemberCode").val("");
            return false;
        } else if (selectType == "3") {
            alert("Media Code must be in digit.");
            $("#EmpOrMemberCode").val("");
            return false;
        }
    }
    var deptId = $('#DdlDepartment option:selected').attr("value");
    var selected = $("input[type='radio']:checked");

    if (value == "")
    { return false; }
    var cl = new CanvasLoader('canvasloader-container1');
    cl.show();

    if (selected.length > 0) {
        selectType = selected.attr('id');

    }
    $.ajaxSetup({ cache: false });
    $.ajax({
        url: "/UserRegistration/UserRegistration/GetRegisteredDetails",
        type: 'POST',
        data: {
            Id: value,
            deptId: deptId,
            selectType: selectType
        },
        success: function (data) {
            if (data != null || data != "") {

                if (data.UserName != "Exists") {
                    if (selectType == "2")
                    { alert("Employee Code is already registered."); }
                    else if (selectType == "3")
                    { alert("Media Code is already registered."); }
                    else {
                        alert("Member Code is already registered.");
                    }
                    $("#EmpOrMemberCode").val("");
                    $('#Name').text('');
                    $('#Address').text('');
                    $('#FatherName').text('');
                    $('#Gender').text('');
                    //$("#Mobile").val('');
                    //$("#MailID").val('');
                    //$('#Age').val('');
                    cl.hide();
                    return false;
                }
                else {
                    $.ajaxSetup({ cache: false });
                    $.ajax({
                        url: "/UserRegistration/UserRegistration/GetDetailsByEmpID",
                        type: 'GET',
                        data: {
                            Id: value,
                            selectType: selectType,
                            deptId: deptId
                        },
                        success: function (result) {
                            if (result != null) {

                                if (result.Name != null && result.Address != null) {
                                    $('#Name').html("");
                                    $('#Address').html("");
                                    $("#Name").html(result.Name);
                                    $("#PMISName").val(result.Name);
                                    $('#Address').html(result.Address);
                                    $("#UserName").val(result.Name);
                                    $("#FatherName").html(result.FatherName);
                                    $("#OfficeId").val(result.OfficeId);
                                    if ($("#Mobile").val() == "") {
                                        $("#Mobile").val(result.Mobile);
                                    }
                                    if ($("#MailID").val() == "")
                                    { $("#MailID").val(result.MailID); }
                                    $('#Gender').text(result.Gender);
                                    $('#Age').val('');
                                    flag = true;
                                    cl.hide();
                                } else {
                                    cl.hide();
                                    if (selectType == "1") {
                                        alert("Unable to found this Member code");
                                    }
                                    else if (selectType == "2") {
                                        alert("Unable to found this Employee code");
                                    } else if (selectType == "3") {
                                        alert("Unable to found this Media code");
                                    }
                                    $("#EmpOrMemberCode").val("");
                                    $('#Name').html("");
                                    $('#Address').html("");
                                    $("#FatherName").html("");
                                    $('#Gender').text('');
                                    $('#Age').val('');
                                    flag = false;

                                }
                            }
                            else {
                                cl2.hide();
                                if (selectType == "1") {
                                    alert("Unable to found this Member code");
                                }
                                else if (selectType == "2") {
                                    alert("Unable to found this Employee code");
                                } else if (selectType == "3") {
                                    alert("Unable to found this Media code");
                                }
                                $("#EmpOrMemberCode").val("");
                                $('#Name').html("");
                                $('#Address').html("");
                                $("#FatherName").html("");
                                flag = false;
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert(xhr.responseText);
                            alert(thrownError);
                            return false;
                        }
                    });
                }
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {

            return false;
            alert(xhr.responseText);
            alert(thrownError);

        }
    });
});

//$('#DdlDepartment').change(function () {
//    var DeptId = $('#DdlDepartment').val();
//    $("#AdhaarID").focus();
//    if (DeptId == "HPD0005") {
//        $("#secretoryField").hide();
//    }
//    else {
//        $("#secretoryField").show();
//    }
//});

function check(id) {
    if (id == 2) {
        $("#SelectType").val("2");
        $("#deptList").show();
        $("#secretoryField").show();
        $("#lblEmpCode").show();
        $("#EmpOrMemberCode").attr("placeholder", "Enter employee code");
        $("#lblMemberCode").hide();
        $("#lblMediaCode").hide();
        $("#DdlDepartment").focus();
        $("#AdhaarIDDetails").empty();
        $("#Mobile").val("");
        $("#MailID").val("");
    }
    else if (id == 3) {
        $("#SelectType").val("3");
        $("#EmpOrMemberCode").focus();
        $("#deptList").hide();
        $("#secretoryField").hide();
        $("#lblEmpCode").hide();
        $("#lblMemberCode").hide();
        $("#lblMediaCode").show();
        $("#EmpOrMemberCode").attr("placeholder", "Enter Journalist code");
        $("#AdhaarIDDetails").empty();
        $("#Mobile").val("");
        $("#MailID").val("");
    }
    else {

        $("#SelectType").val("1");
        $("#EmpOrMemberCode").focus();
        $("#deptList").hide();
        $("#secretoryField").hide();
        $("#lblEmpCode").hide();
        $("#lblMemberCode").show();
        $("#lblMediaCode").hide();
        $("#EmpOrMemberCode").attr("placeholder", "Enter Member code");
        $("#AdhaarIDDetails").empty();
        $("#Mobile").val("");
        $("#MailID").val("");
    }
}

function GetOTP() {

    var empId = document.getElementById("EmpOrMemberCode").value;
    var Mobile = document.getElementById("Mobile").value;
    Mobile = Mobile.replace("(", "");
    Mobile = Mobile.replace(")", "");
    Mobile = Mobile.replace("-", "");
    Mobile = Mobile.replace(" ", "");
    if (empId == "" || Mobile == "") {
        alert("Make Sure you have entered emp id and Mobile Number.");
        return false;
    }
    $.ajax({
        url: "/UserRegistration/UserRegistration/GetOTP",
        type: 'GET',
        data: {
            empId: empId,
            Mobile: Mobile
        },
        success: function (data) {
            if (data) {
                var OTP;
                $("#ShowOTP").html(data);
                $('#otpTable tr').each(function () {
                    OTP = $(this).find("#otpvalue").html();
                });
                $("#ValidateMobile").val(Mobile);
                $('#hdnOTPValue').val(OTP);

                //var dialog = $("#PopModel").removeClass('hide').dialog({
                //    modal: true,
                //    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='ace-icon fa fa-check'></i> OTP Number</h4></div>",
                //    title_html: true,
                //    width: 600,
                //    buttons: [
                //        {
                //            text: "OK",
                //            "class": "btn btn-primary btn-xs",
                //            click: function () {
                //                $(this).dialog("close");
                //            }
                //        }
                //    ]
                //});
            }
            else {
                alert("Unable to Found the Emp or Member Id");
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
            alert(thrownError);
        }
    });
}

