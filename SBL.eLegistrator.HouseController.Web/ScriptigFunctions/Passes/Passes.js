﻿function ChangeTimeValTimePicker(value) {
    if (value == 'Full Day') {
        $('#Time').timepicker('setTime', '10:00');
    }
    else if (value == 'AN') {
        $('#Time').timepicker('setTime', '14:00');
    }
    else if (value == 'FN') {
        $('#Time').timepicker('setTime', '10:00');
    }
    else if (value == 'After Session') {
        $('#Time').timepicker('setTime', '16:00');
    }
    else if (value == 'Before Session') {
        $('#Time').timepicker('setTime', '09:00');
    }
    else {
        $('#Time').timepicker('setTime', '10:00');
    }
}

function DateCheck() {
    var StartDate = document.getElementById('SessionDateFrom').value;
    var EndDate = document.getElementById('SessionDateTo').value;

    var eDate = new Date(EndDate);
    var sDate = new Date(StartDate);

    var from = StartDate.split("/");
    var to = EndDate.split("/");

    var startNew = new Date(from[2], from[1] - 1, from[0]);
    var endNew = new Date(to[2], to[1] - 1, to[0]);

    if (startNew != '' && endNew != '' && startNew > endNew) {
        alert("Please ensure that the To Date is greater than or equal to the From Date.");
        return false;
    }
    return true;
}

jQuery(function ($) {
    $("#sizeDown").click(function () {
        if ($("body").hasClass('sizeLarge')) {
            $("body").css("font-size", "13px");
            $("body").removeClass('sizeLarge');
        } else {
            $("body").css("font-size", "10px");
            $("body").addClass('sizeSmall');
        }
    });
    $("#sizeUp").click(function () {
        if ($("body").hasClass('sizeSmall')) {
            $("body").css("font-size", "13px");
            $("body").removeClass('sizeSmall');
        } else {
            $("body").css("font-size", "18px");
            $("body").addClass('sizeLarge');
        }
    });
});

