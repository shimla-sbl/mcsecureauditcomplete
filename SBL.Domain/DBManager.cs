﻿namespace SBL.Domain
{
    #region Namespace Reffrences

    using SBL.DAL;
    using SBL.Domain.Migrations;
    using SBL.DomainModel;
    using SBL.DomainModel.Models.ActionButtons;
    using SBL.DomainModel.Models.Adhaar;
    using SBL.DomainModel.Models.AlbumCategory;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.AssemblyFileSystem;
    using SBL.DomainModel.Models.AssemblyRemarks;
    using SBL.DomainModel.Models.AuditTrail;
    using SBL.DomainModel.Models.Authority;
    using SBL.DomainModel.Models.Bill;
    using SBL.DomainModel.Models.Budget;
    using SBL.DomainModel.Models.Category;
    using SBL.DomainModel.Models.Committee;
    using SBL.DomainModel.Models.CommitteeReport;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.ConstituencyVS;
    using SBL.DomainModel.Models.ContactDetails;
    using SBL.DomainModel.Models.ContactUs;
    using SBL.DomainModel.Models.Content;
    using SBL.DomainModel.Models.CutMotionDemand;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Departmentpdfpath;
    using SBL.DomainModel.Models.Diaries;
    using SBL.DomainModel.Models.District;
    using SBL.DomainModel.Models.Districtconsistency;
    using SBL.DomainModel.Models.Document;
    using SBL.DomainModel.Models.eFile;
    using SBL.DomainModel.Models.Employee;
    using SBL.DomainModel.Models.Event;
    using SBL.DomainModel.Models.EvSecretories;
    using SBL.DomainModel.Models.FooterPublicData;
    using SBL.DomainModel.Models.Forms;
    using SBL.DomainModel.Models.FormTR2;
    using SBL.DomainModel.Models.Gallery;
    using SBL.DomainModel.Models.GalleryThumb;
    using SBL.DomainModel.Models.Governor;
    using SBL.DomainModel.Models.Grievance;
    using SBL.DomainModel.Models.HOD;
    using SBL.DomainModel.Models.House;
    using SBL.DomainModel.Models.ItemSubscription;
    using SBL.DomainModel.Models.Language;
    using SBL.DomainModel.Models.Library;
    using SBL.DomainModel.Models.LibraryVS;
    using SBL.DomainModel.Models.Loan;
    using SBL.DomainModel.Models.LOB;
    using SBL.DomainModel.Models.Media;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.MemberAssemblyRemarks;
    using SBL.DomainModel.Models.MemberSMSQuota;
    using SBL.DomainModel.Models.Menu;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Mobiles;
    using SBL.DomainModel.Models.News;
    using SBL.DomainModel.Models.Notice;
    using SBL.DomainModel.Models.Office;
    using SBL.DomainModel.Models.Officers;
    using SBL.DomainModel.Models.Others;
    using SBL.DomainModel.Models.OTP;
    using SBL.DomainModel.Models.PaidMemeberSubscription;
    using SBL.DomainModel.Models.PaperLaid;
    using SBL.DomainModel.Models.PaperMovement;
    using SBL.DomainModel.Models.Party;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Pension;
    using SBL.DomainModel.Models.PISModules;
    using SBL.DomainModel.Models.PISMolues;
    using SBL.DomainModel.Models.PressClipping;
    using SBL.DomainModel.Models.PrintingPress;
    using SBL.DomainModel.Models.Promotion;
    using SBL.DomainModel.Models.PublishDocument;
    using SBL.DomainModel.Models.Question;
    using SBL.DomainModel.Models.RecipientGroups;
    using SBL.DomainModel.Models.RoadPermit;
    using SBL.DomainModel.Models.Role;
    using SBL.DomainModel.Models.ROM;
    using SBL.DomainModel.Models.Rule;
    using SBL.DomainModel.Models.salaryhead;
    using SBL.DomainModel.Models.Secretory;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.SessionDateSignature;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.DomainModel.Models.SmsGateway;
    using SBL.DomainModel.Models.Speaker;
    using SBL.DomainModel.Models.Speech;
    using SBL.DomainModel.Models.StaffManagement;
    using SBL.DomainModel.Models.States;
    using SBL.DomainModel.Models.SystemModule;
    using SBL.DomainModel.Models.Tour;
    using SBL.DomainModel.Models.UploadImages;
    using SBL.DomainModel.Models.UploadMetaFile;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.UserAction;
    using SBL.DomainModel.Models.UserModule;
    using SBL.DomainModel.Models.WorksProposal;
    using SBL.DomainModel.Models.SmsGateway;
    using System.Data.Entity;
    using SBL.DomainModel.Models.JobsPosting;
    using SBL.DomainModel.Models.OldSalary;
    using SBL.Domain.Context.Budget;
    using SBL.DomainModel.Models.SubDivision;
    using SBL.DomainModel.Models.mSchemeType;
    using SBL.DomainModel.Models.pushgreviance;
    using SBL.DomainModel.Models.VideoArkive;
    using SBL.DomainModel.Models;
    using SBL.DomainModel.Models.tWebCasting;
    using SBL.DomainModel.Models.Activity;
    using SBL.DomainModel.Models.References;

    #endregion Namespace Reffrences

    public class DBManager : DBManagerBase<DBManager>
    {
        public DBManager() : base("eVidhan")
        {
            this.Database.CommandTimeout = 60;
        }
        public static void InitializeDB()
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DBManager, Configuration>());
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DBManager>());
            using (DBManager dbMan = new DBManager())
            {
                dbMan.Close();
            }
        }

        // Income Tax of Member and Salary
        public DbSet<mSalaryTypeDetailForTax> mSalaryTypeDetailForTax { get; set; }

        // Income Tax of Member and Salary
        public DbSet<DbSyncVersion> DbSyncVersion { get; set; }

        public DbSet<mSpeakerIncomeTax> mSpeakerIncomeTax { get; set; }

        public DbSet<mMemberIncomeTax> mMemberIncomeTax { get; set; }

        //TreeView
        public DbSet<TreeViewStructure> meTreeViewStructure { get; set; }

        //House
        public DbSet<tOtherPapers> tOtherPapers { get; set; }

        public DbSet<tHouseProceedingsTimeSlots> tHouseProceedingsTimeSlots { get; set; }

        public DbSet<tHouseProceedings> tHouseProceedings { get; set; }

        //SMSQuota
        public DbSet<mMemberSMSQuotaAddOn> mMemberSMSQuotaAddOn { get; set; }

        public DbSet<FooterPublicData> FooterPublicData { get; set; }

        public DbSet<mSubEvents> mSubEvents { get; set; }

        //SessionDateSignature
        public virtual DbSet<tSessionDateSignature> tSessionDateSignature { get; set; }

        //Mobiles
        public DbSet<mMobileApps> mMobileApps { get; set; }

        public DbSet<mMobileSettings> mMobileSettings { get; set; }

        public DbSet<tMobileVersions> tMobileVersions { get; set; }

        public DbSet<tRulesDirections> tRulesDirections { get; set; }

        public DbSet<HouseCommitteeNotification> HouseCommitteeNotification { get; set; }

        public DbSet<mErrorLog> mErrorLog { get; set; }

        public DbSet<mTelecomCircles> mTelecomCircles { get; set; }

        public DbSet<mNetworkOperators> mNetworkOperators { get; set; }

        public DbSet<mMobileTrack> mMobileTrack { get; set; }

        public DbSet<mGrievanceMember> mGrievanceMember { get; set; }

        public DbSet<mBackDoorPassword> mBackDoorPassword { get; set; }

        //Grievance
        public DbSet<tGrievanceReply> tGrievanceReply { get; set; }

        public DbSet<ForwardGrievance> ForwardGrievance { get; set; }

        public DbSet<tMemberGrievances> tMemberGrievances { get; set; }

        public DbSet<mUserInformation> mUserInformation { get; set; }

        public DbSet<tGrievanceOfficerDeleted> tGrievanceOfficerDeleted { get; set; }

        public DbSet<ActionType> ActionType { get; set; }
        public DbSet<BudgetMapWithDesig> BudgetMapWithDesig { get; set; }

        // Budget/Bill
        public DbSet<tBudget> tBudget { get; set; }

        public DbSet<mSessionForms> mSessionForms { get; set; }

        public DbSet<tForms> tForms { get; set; }

        public DbSet<tFormsTypeofDocuments> tFormsTypeofDocuments { get; set; }

        public DbSet<DemandType> DemandType { get; set; }

        public DbSet<mBudget> mBudget { get; set; }

        public DbSet<AllocateBudget> AllocateBudget { get; set; }

        public DbSet<AdditionFunds> AdditionFunds { get; set; }

        public DbSet<mReimbursementBill> mReimbursementBill { get; set; }

        public DbSet<mSubVoucher> mSubVoucher { get; set; }

        public DbSet<mUploadImages> mUploadImages { get; set; }

        public DbSet<mAuthorityLetter> mAuthorityLetter { get; set; }

        public DbSet<mDebitReceiptExp> mDebitReceiptExp { get; set; }

        public DbSet<mEstablishBill> mEstablishBill { get; set; }

        public DbSet<mComments> mComments { get; set; }

        public DbSet<mRtgsSoft> mRtgsSoft { get; set; }

        public DbSet<mOtherFirm> mOtherFirm { get; set; }

        public DbSet<mMedicine> mMedicine { get; set; }

        public DbSet<mBudgetType> mBudgetType { get; set; }

        public DbSet<mBudgetBillTypes> mBudgetBillTypes { get; set; }

        public DbSet<mMedicalTestCharge> mMedicalTestCharge { get; set; }

        public DbSet<mApprovedHospital> mApprovedHospital { get; set; }

        //Assembly
        public virtual DbSet<mAssembly> mAssemblies { get; set; }

        public virtual DbSet<mAssemblyRemarks> mAssemblyRemarks { get; set; }

        // Library
        public DbSet<mPressClipping> mPressClipping { get; set; }

        public DbSet<mPressClippingsPDF> mPressClippingsPDF { get; set; }

        public DbSet<PaidMemeberSubs> PaidMemeberSubs { get; set; }

        public DbSet<mItemSubscription> mItemSubscription { get; set; }

        public DbSet<mSaleCounter> mSaleCounter { get; set; }

        public DbSet<mDebateSaleCounter> mDebateSaleCounter { get; set; }

        public DbSet<mDebateInfo> mDebateInfo { get; set; }

        public DbSet<mItemSubsDelivery> mItemSubsDelivery { get; set; }

        public DbSet<mIssueBook> mIssueBook { get; set; }

        public DbSet<mIssueBookAccessionNum> mIssueBookAccessionNum { get; set; }

        public DbSet<tCommitteeReportLib> tCommitteeReportLib { get; set; }

        public DbSet<tCommitteeReport> tCommitteeReport { get; set; }

        public DbSet<tCommitteReplyPendency> tCommitteReplyPendency { get; set; }

        public DbSet<mCommitteeReplyItemType> mCommitteeReplyItemType { get; set; }

        public DbSet<mCommitteeReplyStatus> mCommitteeReplyStatus { get; set; }

        public DbSet<tPaperLaid> tPaperLaid { get; set; }

        public DbSet<tAuditTrial> tAuditTrial { get; set; }

        public DbSet<tFloorVersion> tFloorVersion { get; set; }

        public DbSet<tWhoiswho> tWhoiswho { get; set; }

        //Bill
        public virtual DbSet<mBillRule> mBillRules { get; set; }

        public virtual DbSet<mBillStatus> mBillStatuss { get; set; }

        public virtual DbSet<mBillType> mBillTypes { get; set; }

        public virtual DbSet<tBillAttachment> tBillAttachments { get; set; }

        public virtual DbSet<tBillMotion> tBillMotions { get; set; }

        public virtual DbSet<tBillMotionOpinion> tBillMotionOpinions { get; set; }

        public virtual DbSet<tBillRegister> tBillRegisters { get; set; }

        public virtual DbSet<tBillRuleRegister> tBillRuleRegisters { get; set; }

        //Committee
        public virtual DbSet<tCommittee> tCommittees { get; set; }

        public virtual DbSet<mCommitteeType> mCommitteeType { get; set; }

        public virtual DbSet<tCommitteeCalendar> tCommitteeCalendars { get; set; }

        public virtual DbSet<tCommitteeMember> tCommitteeMembers { get; set; }

        public virtual DbSet<tCommitteeReportType> tCommitteeReportTypes { get; set; }

        public virtual DbSet<tCommitteeScheduledDay> tCommitteeScheduledDays { get; set; }

        public virtual DbSet<tCommitteeTransaction> tCommitteeTransactions { get; set; }

        public DbSet<eFile> meFile { get; set; }

        public DbSet<eFileAttachment> teFileAttachment { get; set; }

        public DbSet<eFileNoting> teFileNoting { get; set; }

        //ePaper
        public DbSet<eFilePaperSno> mePaperSno { get; set; }

        public DbSet<eFilePaperNature> meFilePaperNature { get; set; }

        public DbSet<eFilePaperType> meFilePaperType { get; set; }

        public DbSet<CommitteeMembersUpload> meCommitteeMembersUpload { get; set; }

        public DbSet<COmmitteeProceeding> tCommitteeProceeding { get; set; }

        public DbSet<CommitteeTypePermission> CommitteeTypePermission { get; set; }

        public DbSet<eFileType> meFileType { get; set; }

        public DbSet<tPromotions> tPromotions { get; set; }

        public DbSet<tLeaveDetail> tLeaveDetail { get; set; }

        public DbSet<tPensiondetail> tPensiondetail { get; set; }


        //Constituency
        public virtual DbSet<mConstituency> mConstituency { get; set; }



        public virtual DbSet<mConstituencyReservedCategory> mConstituencyReservedCategory { get; set; }

        public virtual DbSet<mPanchayat> mPanchayat { get; set; }

        public virtual DbSet<mVillage> mVillage { get; set; }

        public virtual DbSet<mTown> mTown { get; set; }

        public virtual DbSet<tPanchayatVillage> tPanchayatVillage { get; set; }

        public virtual DbSet<tConstituencyPanchayat> tConstituencyPanchayat { get; set; }

        //Department
        public virtual DbSet<mDepartment> mDepartments { get; set; }

        public virtual DbSet<mchangeDepartmentAuditTrail> mchangeDepartmentAuditTrail { get; set; }

        //Document
        public virtual DbSet<mDocumentType> mDocumentTypes { get; set; }

        //Event
        public virtual DbSet<mEvent> mEvents { get; set; }

        //Language
        public virtual DbSet<Languages> Languages { get; set; }

        public virtual DbSet<LanguageText> LanguageText { get; set; }

        //LOB
        public virtual DbSet<AdminLOB> AdminLOB { get; set; }

        public virtual DbSet<Audit_PublishedLOB> Audit_PublishedLOB { get; set; }

        public virtual DbSet<DraftLOB> DraftLOB { get; set; }

        public virtual DbSet<tCommitteeDraft> tCommitteeDraft { get; set; }

        public virtual DbSet<CurrentLOB> CurrentLOB { get; set; }

        public virtual DbSet<CorrigendumDetails> CorrigendumDetails { get; set; }

        public virtual DbSet<CorrigendumLOB> CorrigendumLOB { get; set; }

        //Member
        public virtual DbSet<mMember> mMembers { get; set; }

        public virtual DbSet<mMemberHighestQualification> mMemberHighestQualification { get; set; }

        public virtual DbSet<mMemberAssembly> mMemberAssembly { get; set; }

        public virtual DbSet<mMemberAssemblyRemarks> mMemberAssemblyRemarks { get; set; }

        public DbSet<tLokSabhaandRajyaSabhaMember> tLokSabhaandRajyaSabhaMembers { get; set; }

        //Menu

        public virtual DbSet<mMenu> mMenu { get; set; }

        public virtual DbSet<tTempMobileUser> tTempMobileUser { get; set; }

        //Ministery
        public virtual DbSet<mMinistry> mMinistries { get; set; }

        public virtual DbSet<mMinistryDepartment> mMinistryDepartments { get; set; }

        public virtual DbSet<mMinsitryMinister> mMinsitryMinisters { get; set; }

        //Notice
        public virtual DbSet<tMemberNotice> tMemberNotices { get; set; }

        public virtual DbSet<mNoticeType> mNoticeTypes { get; set; }

        public virtual DbSet<OnlineNotices> OnlineNotices { get; set; }

        //PaperLaid
        public virtual DbSet<mPaperCategoryType> mPaperCategoryTypes { get; set; }

        public virtual DbSet<tPaperLaidTemp> tPaperLaidTemps { get; set; }

        public virtual DbSet<tPaperLaidV> tPaperLaidVS { get; set; }

        public virtual DbSet<tPaperLaidDepartmentAuditTrial> tPaperLaidDepartmentAuditTrials { get; set; }

        public virtual DbSet<tPaperLaidMinisterAuditTrial> tPaperLaidMinisterAuditTrials { get; set; }

        //Questions
        public virtual DbSet<tCorrigendumQuestion> tCorrigendumQuestions { get; set; }

        public virtual DbSet<tQuestion> tQuestions { get; set; }

        public virtual DbSet<tQuestionsTemp> tQuestionsTemps { get; set; }

        public virtual DbSet<tSubQuestion> tSubQuestions { get; set; }

        public virtual DbSet<tSubQuestionTemp> tSubQuestionTemps { get; set; }

        public virtual DbSet<tQuestionType> tQuestionTypes { get; set; }

        public virtual DbSet<OnlineQuestions> OnlineQuestions { get; set; }

        public virtual DbSet<tCancelQuestionAuditTrial> tCancelQuestionAuditTrial { get; set; }

        //Role
        public virtual DbSet<mRoles> mRoles { get; set; }

        public virtual DbSet<tRoleDetails> tRoleDetails { get; set; }

        public virtual DbSet<tUserRoles> tUserRoles { get; set; }

        public virtual DbSet<tMenuRoleDetails> tMenuRoleDetails { get; set; }

        public virtual DbSet<mSubUserTypeRoles> mSubUserTypeRoles { get; set; }

        //UserModule
        public virtual DbSet<mUserModules> mUserModules { get; set; }

        public virtual DbSet<tUserAccessRequest> tUserAccessRequest { get; set; }

        public virtual DbSet<tUserAccessRequestLog> tUserAccessRequestLog { get; set; }

        public virtual DbSet<mUserSubModules> mUserSubModules { get; set; }

        //public virtual DbSet<mUserSubType> mUserSubType { get; set; }
        public virtual DbSet<mSubUserType> mSubUserType { get; set; }

        //Rule
        public virtual DbSet<mRule> mRules { get; set; }

        public virtual DbSet<tUserAccessActions> tUserAccessActions { get; set; }

        public virtual DbSet<mHOD> mHOD { get; set; }

        //Rule
        public virtual DbSet<mUserActions> mUserActions { get; set; }

        //Session
        public virtual DbSet<mSession> mSessions { get; set; }

        public virtual DbSet<mSessionDate> mSessionDates { get; set; }

        public virtual DbSet<mSessionType> mSessionTypes { get; set; }

        //SiteSetting
        public virtual DbSet<SiteSettings> SiteSettings { get; set; }

        //SystemModule
        public virtual DbSet<SystemModule> SystemModule { get; set; }

        public virtual DbSet<ModuleAction> ModuleAction { get; set; }

        public DbSet<tFAQ> tFAQ { get; set; }

        public DbSet<tUserMannuals> tUserMannuals { get; set; }

        public DbSet<tSiteMap> tSiteMap { get; set; }

        public DbSet<SanFillPostPdf> SanFillPostPdf { get; set; }

        public DbSet<tMailStatus> tMailStatus { get; set; }

        #region User Registration

        public virtual DbSet<mSecretory> mSecretory { get; set; }

        public virtual DbSet<mSecretoryDepartment> mSecretoryDepartment { get; set; }

        public virtual DbSet<mUserType> mUserType { get; set; }

        public virtual DbSet<mUserDSHDetails> mUserDSHDetails { get; set; }

        public virtual DbSet<TempUser> TempUser { get; set; }

        public DbSet<mUsers> mUser { get; set; }

        public DbSet<mUsersAuditTrail> mUsersAuditTrail { get; set; }

        #endregion User Registration

        #region Employee

        public virtual DbSet<mDesignation> mDesignation { get; set; }

        public virtual DbSet<mMemberDesignation> mMemDesignation { get; set; }

        public virtual DbSet<mEmployee> mEmployee { get; set; }

        #endregion Employee

        #region tOTPDetails

        public virtual DbSet<tOTPDetails> tOTPDetails { get; set; }

        #endregion tOTPDetails

        #region Pass

        public virtual DbSet<Pass> Passes { get; set; }

        public virtual DbSet<PassCategory> PassCategories { get; set; }

        public virtual DbSet<PublicPass> PublicPasses { get; set; }

        public virtual DbSet<mPasses> masterPasses { get; set; }

        public virtual DbSet<mPassesDeleted> masterPassesDeleted { get; set; }

        public virtual DbSet<tVerifiedPasses> VerifiedPasses { get; set; }

        public virtual DbSet<VisitorData> visitorData { get; set; }

        #endregion Pass

        #region Pass

        public DbSet<Journalist> Journalists { get; set; }

        public DbSet<Organization> Organizations { get; set; }

        #endregion Pass

        //tUserOneTimeUserRegistration Sameer
        public virtual DbSet<tOneTimeUserRegistration> tOneTimeUserRegistration { get; set; }

        public virtual DbSet<tUserRegistrationDetails> tUserRegistrationDetails { get; set; }

        public virtual DbSet<tOTPRegistrationAuditTrial> tOTPRegistrationAuditTrial { get; set; }

        //ActionButton
        public virtual DbSet<ActionButtonControl> ActionButtonControl { get; set; }

        public virtual DbSet<TQuestionAuditTrial> TQuestionAuditTrial { get; set; }

        //Sunil
        public virtual DbSet<mQuestionRules> mQuestionRules { get; set; }

        public virtual DbSet<tQuestionRuleRegister> tQuestionRuleRegister { get; set; }

        public virtual DbSet<tCutMotionDemand> tCutMotionDemand { get; set; }

        //Adhaar ID
        public virtual DbSet<AdhaarDetails> AdhaarDetails { get; set; }

        public DbSet<mAuthority> mAuthority { get; set; }

        public DbSet<tMediaYears> tMediaYears { get; set; }

        public DbSet<mDepartmentPasses> mPasses { get; set; }

        public DbSet<JournalistsForPass> JournalistsForPass { get; set; }

        //Paper Movements Module
        public DbSet<PaperMovementDetail> mPaperMovementDetail { get; set; }

        public DbSet<AuthorisedEmployee> tAuthorizedEmployees { get; set; }

        //RotationMinister
        public DbSet<tRotationMinister> tRotationMinister { get; set; }

        //AlbumCategory
        public virtual DbSet<AlbumCategory> tAlbumcategory { get; set; }

        //DistrictConsistency
        public virtual DbSet<Districtconsistency> tDistrictconsistency { get; set; }

        //mDepartmentPdfPath
        public virtual DbSet<mDepartmentPdfPath> mDepartmentPdfPath { get; set; }

        //member account details---By Robin
        public virtual DbSet<mMemberAccountDetails> mMemberAccountDetails { get; set; }//with foreign key problem...now not using this table

        public virtual DbSet<mMemberAccountsDetails> mMemberAccountsDetails { get; set; }//Newtable with no foreign key issue

        public virtual DbSet<mMemberNominee> mMemberNominee { get; set; }

        //Vidhan sabha library-----By Robin
        public virtual DbSet<VsLibraryBooks> VsLibraryBooks { get; set; }

        public virtual DbSet<VsLibraryBillInfo> VsLibraryBillInfo { get; set; }

        public virtual DbSet<VsLibraryBulletin> VsLibraryBulletin { get; set; }

        public virtual DbSet<VsDocumentIssue> VsDocumentIssue { get; set; }

        //Vidhan sabha EPWBILLS-----By Robin
        public virtual DbSet<EVidhanEPWBill> EVidhanEPWBill { get; set; }

        public virtual DbSet<EVidhanEPWBillTransaction> EVidhanEPWBillTransaction { get; set; }

        public virtual DbSet<EvidhanMasterPhoneBill> EvidhanMasterPhoneBill { get; set; }

        //Printing Press ----Sameer

        public virtual DbSet<tPrintingTemp> tPrintingTemp { get; set; }

        public virtual DbSet<tPrintingPress> tPrintingPress { get; set; }

        public virtual DbSet<tStatusPrintingPress> tStatusPrintingPress { get; set; }

        //House committee ----Sameer

        public virtual DbSet<mBranches> mBranches { get; set; }
        public virtual DbSet<tBranchesCommittee> tBranchesCommittee { get; set; }
        public virtual DbSet<tMovementFile> tMovementFile { get; set; }
        public virtual DbSet<tDrafteFile> tDrafteFile { get; set; }
        public virtual DbSet<FileNotingMovement> FileNotingMovement { get; set; }


        //Vidhan sabha EPWBILLS-----By Robin
        public virtual DbSet<EvidhanPost> EvidhanPost { get; set; }

        //By Lakshay
        //By Lakshay
        public DbSet<pHouseCommitteeFiles> pHouseCommitteeFiles { get; set; }
        public DbSet<pHouseCommitteeSubFiles> pHouseCommitteeSubFiles { get; set; }

        public DbSet<pHouseCommitteeItemPendency> pHouseCommitteeItemPendency { get; set; }
        public DbSet<pHouseCommitteeSubItemPendency> pHouseCommitteeSubItemPendency { get; set; }

        public DbSet<mCommitteeReplySubItemType> mCommitteeReplySubItemType { get; set; }

        #region Officers

        public DbSet<mOfficerDetails> mOfficerDetails { get; set; }

        public DbSet<mOffice> mOffice { get; set; }

        public DbSet<tDepartmentOfficeMapped> tDepartmentOfficeMapped { get; set; }

        // --------- By Madhur
        public DbSet<tOfficeHierarchy> tOfficeHierarchy { get; set; }

        public DbSet<tDiaryAction> tDiaryAction { get; set; }

        public DbSet<MlaDiaryDepartment> MlaDiaryDepartment { get; set; }

        #endregion Officers

        #region Admin Section

        public DbSet<tGallery> tGallery { get; set; }

        public DbSet<Content> tContent { get; set; }

        public DbSet<tMemberTour> tMemberTour { get; set; }

        public DbSet<tSpeech> Speeches { get; set; }

        public DbSet<DistrictModel> Districts { get; set; }

        public DbSet<mNotice> tNoticeBoard { get; set; }

        public DbSet<mNews> tLatestUpdate { get; set; }

        public DbSet<Category> Category { get; set; }

        public DbSet<tGalleryThumb> tGallerythumb { get; set; }

        public DbSet<tAssemblyFiles> tAssemblyFiles { get; set; }

        public DbSet<mAssemblyTypeofDocuments> mAssemblyTypeofDocuments { get; set; }

        public DbSet<mParty> mParty { get; set; }

        public DbSet<mBills> mBills { get; set; }

        public DbSet<eVSecretories> eVSecretories { get; set; }

        public DbSet<mVSpeaker> mVSpeaker { get; set; }

        public DbSet<mLibraryMaterial> mLibraryMaterial { get; set; }

        public DbSet<mLibraryBooksSubject> mLibraryBooksSubject { get; set; }

        public DbSet<mLibraryBooksLanguage> mLibraryBooksLanguage { get; set; }

        public DbSet<mLibraryBooks> mLibraryBooks { get; set; }

        public DbSet<mGoverner> mGoverner { get; set; }

        public DbSet<mStates> mStates { get; set; }

        public DbSet<mCountry> mCountry { get; set; }

        public DbSet<mBlock> mBlock { get; set; }

        public DbSet<tBeneficiarySchemes> tBeneficiarySchemes { get; set; }

        public DbSet<tBeneficearyFeedback> tBeneficearyFeedback { get; set; }

        public DbSet<tMemberHelpline> tMemberHelpline { get; set; }

        public DbSet<mUnitsType> mUnitType { get; set; }

        public DbSet<mUnits> mUnits { get; set; }

        public DbSet<mUnitContituency> mUnitContituency { get; set; }


        public DbSet<mContact> mContact { get; set; }

        public DbSet<ContactUs> ContactUs { get; set; }

        public DbSet<tTrainingDetails> tTrainingDetails { get; set; }

        #endregion Admin Section

        #region Audit Trails

        public DbSet<Audit> Audits { get; set; }

        #endregion Audit Trails

        #region RecipientGroups

        public DbSet<RecipientGroup> RecipientGroups { get; set; }

        public DbSet<RecipientGroupMember> RecipientGroupMembers { get; set; }

        #endregion RecipientGroups

        #region "House"

        public virtual DbSet<EbookLoginDetails> EbookLoginDetails { get; set; }

        public virtual DbSet<EventsAllotedTime> EventsAllotedTime { get; set; }

        public virtual DbSet<EvotingResultLog> EvotingResultLog { get; set; }

        public virtual DbSet<tLOBHistory> LOBHistory { get; set; }

        public virtual DbSet<MainEventLog> MainEventLog { get; set; }

        public virtual DbSet<MemberAttendance> MemberAttendance { get; set; }

        public virtual DbSet<MemberEventLog> MemberEventLog { get; set; }

        public virtual DbSet<MemberSeat> MemberSeat { get; set; }

        public virtual DbSet<ReporterDescriptionsLog> ReporterDescriptionsLog { get; set; }

        public virtual DbSet<Seat> Seat { get; set; }

        public virtual DbSet<tSpeakerpadHistory> SpeakerpadHistory { get; set; }

        public virtual DbSet<tStarredQuestionsHistory> StarredQuestionsHistory { get; set; }

        public virtual DbSet<SubEventLog> SubEventLog { get; set; }

        public virtual DbSet<TableOfficerBriefofProceedings> TableOfficerBriefofProceedings { get; set; }

        public virtual DbSet<tUnStarredQuestionsHistory> UnStarredQuestionsHistory { get; set; }

        public virtual DbSet<TemplatesDescriptions> TemplatesDescriptions { get; set; }

        public virtual DbSet<ProceedingsHistory> ProceedingsHistory { get; set; }

        #endregion "House"

        #region accountssalary

        public DbSet<salaryheads> salaryheads { get; set; }

        public DbSet<membersHead> membersHead { get; set; }

        public DbSet<membersSalary> membersSalary { get; set; }

        public DbSet<membersMonthlySalaryDetails> membersMonthlySalaryDetails { get; set; }

        public DbSet<memberCategory> memberCategory { get; set; }

        #endregion accountssalary

        #region loans

        public DbSet<loanTransDetails> loanTransDetails { get; set; }

        public DbSet<loanDetails> loanDetails { get; set; }

        public DbSet<loaneeDetails> loaneeDetails { get; set; }

        public DbSet<LoanType> loanType { get; set; }

        public DbSet<disbursementDetails> disbursementDetails { get; set; }

        #endregion loans

        #region pension

        public DbSet<PensionerDetails> PensionerDetails { get; set; }

        public DbSet<pensionerTenuresDetails> pensionerTenuresDetails { get; set; }

        #endregion pension

        #region RoadPermit

        public virtual DbSet<mPermit> mPermit { get; set; }

        public virtual DbSet<mSealedRoads> mSealedRoads { get; set; }

        public virtual DbSet<mRestrictedRoads> mRestrictedRoads { get; set; }

        public virtual DbSet<tPermitSealedRoads> tPermitSealedRoads { get; set; }

        public virtual DbSet<tPermitRestrictedRoads> tPermitRestrictedRoads { get; set; }

        #endregion RoadPermit

        #region

        public virtual DbSet<mStaff> mStaff { get; set; }

        public virtual DbSet<mStaffNominee> mStaffNominee { get; set; }

        public virtual DbSet<mStaffDesignation> mStaffDesignation { get; set; }

        #endregion

        #region Scheme Constituency

        public virtual DbSet<SchemeMastersConstituency> SchemeMastersConstituency { get; set; }

        public virtual DbSet<SchemeMastersVS> SchemeMastersVS { get; set; }

        public virtual DbSet<SchemeMastersDC> SchemeMastersDC { get; set; }

        public virtual DbSet<ConstituencyOffice> ConstituencyOffice { get; set; }

        public virtual DbSet<ConstituencyWorkProgressImages> ConstituencyWorkProgressImages { get; set; }

        public virtual DbSet<ConstituencyWorkProgress> ConstituencyWorkProgress { get; set; }

        public virtual DbSet<ConstituencyMapping> ConstituencyMapping { get; set; }

        public virtual DbSet<DepartmentMapping> DepartmentMapping { get; set; }

        public virtual DbSet<OfficeMapping> OfficeMapping { get; set; }

        public virtual DbSet<mOfficerMapping> mOfficerMapping { get; set; }

        public virtual DbSet<workType> workType { get; set; }

        public virtual DbSet<mworkStatus> mworkStatus { get; set; }

        public virtual DbSet<workNature> workNature { get; set; }

        public virtual DbSet<DistrictsMapping> DistrictsMapping { get; set; }

        public virtual DbSet<Demands> Demands { get; set; }

        public virtual DbSet<ConstituencySchemesBasicDetails> ConstituencySchemesBasicDetails { get; set; }

        public DbSet<mProgramme> mProgramme { get; set; }

        public DbSet<workTrans> workTrans { get; set; }

        public DbSet<mControllingAuthority> mControllingAuthority { get; set; }

        //Contituency Mobile App Dynamic labels and Menu Tables -----By Joginder
        public DbSet<tMenuMasterForContituencyApp> tMenuMasterForContituencyApp { get; set; }
        public DbSet<tCopyRights> tCopyRights { get; set; }
        public DbSet<tUnitLanguage> tUnitLanguage { get; set; }
        public DbSet<tLoginLabels> tLoginLabels { get; set; }
        public DbSet<tDiaryLabels> tDiaryLabels { get; set; }
        public DbSet<tWorksLabels> tWorksLabels { get; set; }
        public DbSet<tVacancyLabels> tVacancyLabels { get; set; }
        public DbSet<tMenuMasterForAssemblyApp> tMenuMasterForAssemblyApp { get; set; }
        public DbSet<tInnerMenuMasterForContituencyApp> tInnerMenuMasterForContituencyApp { get; set; }
        public DbSet<tNewsLabels> tNewsLabels { get; set; }

        #region Proposal
        public DbSet<mWorkProposal> mWorkProposal { get; set; }
        #endregion

        // public virtual DbSet<ConstituencySchemesFinancialDetails> ConstituencySchemesFinancialDetails { get; set; }
        #endregion
        #region PISModule

        public DbSet<tPISEmpSanctionpost> tPISEmpSanctionpost { get; set; }

        public DbSet<mPISDesignation> mPISDesignation { get; set; }

        public DbSet<tPISEmployeePersonal> tPISEmployeePersonal { get; set; }

        #endregion
        #region Publish Document

        public virtual DbSet<PublishDocumentDetails> PublishDocumentDetails { get; set; }
        public virtual DbSet<AuditPublishDocument> AuditPublishDocument { get; set; }

        #endregion


        //form TR2 ----Sameer

        public virtual DbSet<mAccountFields> mAccountFields { get; set; }
        public virtual DbSet<tEmployeeAccountDetails> tEmployeeAccountDetails { get; set; }
        public virtual DbSet<tRebate> tRebate { get; set; }
        public virtual DbSet<TR2LoanDetail> TR2LoanDetail { get; set; }
        public virtual DbSet<Form16> Form16 { get; set; }
        //meta file upload
        public DbSet<mUploadmetaDataFile> metaDataFile { get; set; }
        public DbSet<SalaryVoucherNumbers> mSalaryVoucherNumbers { get; set; }

        //SMS
        public DbSet<tSmsGateway> tSmsGateway { get; set; }

        //jobs Portal
        public DbSet<ExperiencesDetails> ExperiencesDetails { get; set; }
        public DbSet<ExamDetails> ExamDetails { get; set; }
        public DbSet<Candidates> Candidates { get; set; }
        public DbSet<tCandidateAdmitDetails> tCandidateAdmitDetails { get; set; }
        public DbSet<Job> Job { get; set; }
        public DbSet<JobWithPostDetail> JobWithPostDetail { get; set; }

        //old Salary Bills
        public DbSet<MlaDir_old> MlaDir_old { get; set; }
        public DbSet<MlaAdvances_old> MlaAdvances_old { get; set; }
        public DbSet<MlaAllowances_old> MlaAllowances_old { get; set; }


        //Budget Files for Public
        public DbSet<BudgetFile> mBudgetFiles { get; set; }
        //Rplicate table for Starred Questions History for House
        public DbSet<StarredQuestionsHistoryHouse> StarredQuestionsHistoryHouse { get; set; }
        //New Table relating to House
        public DbSet<SessionTimingDetails> SessionTimingDetails { get; set; }
        public DbSet<ReportersTimingDetails> ReportersTimingDetails { get; set; }
        public DbSet<HouseProceedingsEditedHistory> HouseProceedingsEditedHistory { get; set; }
        public DbSet<HouseProceedingsDocumentType> HouseProceedingsDocumentType { get; set; }
        public DbSet<Reporterusers> Reporterusers { get; set; }

        //New Tables regarding new committee works
        public DbSet<tCommitteMeetingPapers> tCommitteMeetingPapers { get; set; }
        public DbSet<tCommitteeReplyPendencyDetail> tCommitteeReplyPendencyDetail { get; set; } // by robin
        public DbSet<tCommitteReplyByMaster> tCommitteReplyByMaster { get; set; } // by robin
        public DbSet<Emppersonal_Log> Emppersonal_Log { get; set; }
        public DbSet<AuthEmployee> AuthEmployee { get; set; }
        public DbSet<mSubDivision> mSubDivision { get; set; }
        public DbSet<mSchemeType> mSchemeType { get; set; }
        public DbSet<tLogSessionSSettings> tLogSessionSSettings { get; set; }
        public DbSet<MessagePushGreviance> MessagePushGreviance { get; set; }

        public DbSet<tDrafteFile_Data> tDrafteFile_Data { get; set; }
        public DbSet<ReadTableData> ReadTableData { get; set; }
        public DbSet<MobileTokens> MobileTokens { get; set; }

        public DbSet<mMediaInfo> mMediaInfo { get; set; }
        public DbSet<mspeakerPadPdf> mspeakerPadPdf { get; set; }
        public DbSet<MlaDiary> MlaDiary { get; set; }
        public DbSet<mTypeOfAction> mTypeOfAction { get; set; }
        public DbSet<mTypeOfDocument> mTypeOfDocument { get; set; }
        public DbSet<mMlaSignature> mMlaSignature { get; set; }

        public DbSet<tReferenceMaterial> tReferenceMaterial { get; set; }
        public DbSet<mReferenceMaterialType> mReferenceMaterialType { get; set; }
        public virtual DbSet<tWebCasting> tWebCasting { get; set; }
        public virtual DbSet<NevaVisitors> NevaVisitors { get; set; }
        public DbSet<mVideoArkive> mVideoArkive { get; set; }
        public virtual DbSet<mHomeGallery> mHomeGallery { get; set; }
        public virtual DbSet<ActivityCategory> ActivityCategory { get; set; }
        public virtual DbSet<ActivityLog> ActivityLog { get; set; }
        public virtual DbSet<ActivityImages> ActivityImages { get; set; }
        public virtual DbSet<tCollectionType> CollectionType { get; set; }
        public virtual DbSet<tSectors> Sectors { get; set; }
        public virtual DbSet<tKnowldgeBankRef> KnowldgeBankReferences { get; set; }
    }
}