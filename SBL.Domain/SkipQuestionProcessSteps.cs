﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain
{
    public abstract class SkipQuestionProcessSteps
    {
        public static Int32 SkippingQuesProcessingSteps(Int32 QuestionID)
        {
            try
            {
                SqlConnection con =clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_InsSkipQuesProcessStep2Legi", con);
                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int32 SkippingQuesProcessingStep3Typist(Int32 QuestionID)
        {
            try
            {
                SqlConnection con = clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_InsSkipQuesProcessStep3Typist", con);
                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int32 SkippingQuesProcessingStep4Proof(Int32 QuestionID)
        {
            try
            {
                SqlConnection con = clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_InsSkipQuesProcessStep4Proof", con);
                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int32 SkippingQuesProcessingStep5LegiFreeze(Int32 QuestionID)
        {
            try
            {
                SqlConnection con = clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_InsSkipQuesProcessStep5LegiFreeze", con);
                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int32 SkippingQuesProcessingStepsAfterFreezings(Int32 QuestionID)
        {
            try
            {
                SqlConnection con = clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_InsSkipQuesProcessStep6InitialAproval", con);
                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int32 SkipQuesProcessingStep7LegiFixing(Int32 QuestionID,Int32 ? SessionDateId)
        {
            try
            {
                SqlConnection con = clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_InsSkipQuesProcessStep7LegiFix", con);
                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);
                cmd.Parameters.AddWithValue("@SessionDateId", SessionDateId);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int32 SkippingQuesProcessingStepsAfterFixing(Int32 QuestionID)
        {
            try
            {
                SqlConnection con = clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_InsSkipQuesProcessStep8", con);
                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int32 SkipApprovePDFStepOfTranslator(Int32 SessionDateID)
        {
            try
            {
                SqlConnection con = clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_SkipApprovePDFStepOfTranslator", con);
                cmd.Parameters.AddWithValue("@SessionDateID", SessionDateID);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Int32 SkipQuesProcessStepDiary(Int32 QuestionID)
        {
            try
            {
                SqlConnection con = clsConnection.GetConnection();
                if (con.State != System.Data.ConnectionState.Open)
                    con.Open();
                SqlCommand cmd = new SqlCommand("sp_InsSkipQuesProcessStepDiary", con);
                cmd.Parameters.AddWithValue("@QuestionID", QuestionID);
                cmd.CommandType = CommandType.StoredProcedure;
                Int32 result = cmd.ExecuteNonQuery();
                cmd.Dispose();
                con.Dispose();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
