﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Member
{
    [Serializable]
    [Table("mMemberNominee")]
    public class mMemberNominee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NomineeId { get; set; }

        public int MemberCode { get; set; }

        public string NomineeName { get; set; }

        public string NomineeRelationship { get; set; }
        [DefaultValue(false)]
        public bool NomineeMaritalStatus { get; set; }
        [DefaultValue(false)]
        public bool IsDependent { get; set; }

        [DefaultValue(false)]
        public bool IsHandicaped { get; set; }
        [DefaultValue(false)]
        public bool Isactive { get; set; }

        public string NomineeBankName { get; set; }

        //[Required(ErrorMessage = "Please Enter Nominee's Account No")]
        public long? NomineeAccountNo { get; set; }
        public string NomineeAccountNoS { get; set; }
        //[Required(ErrorMessage = "Please Enter Nominee's Bank IFSC Code")]
        public string NomineeIFSCCode { get; set; }
        [NotMapped]
        public string Mode { get; set; }
    }
}
