﻿namespace SBL.Domain.Context.Member
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SBL.DAL;
    using System.Data.Entity;
    using SBL.DomainModel.Models.Member;
    using SBL.Service.Common;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.Employee;
    using SBL.DomainModel.ComplexModel;
    using System.Data;
    using SBL.DomainModel.Models.District;
    using SBL.DomainModel.Models.States;
    using SBL.DomainModel.Models.Party;
    using SBL.DomainModel.Models.Ministery;
    using System.Data.SqlClient;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.Diaries;
    using SBL.DomainModel.Models.Activity;
    using SBL.DomainModel.Models.References;
    using SBL.DomainModel.ComplexModel.LinqKitSource;

    #endregion

    public class MemberContext : DBBase<MemberContext>
    {
        public MemberContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        //public DbSet<mAssembly> mAssembly { get; set; }
        public virtual DbSet<mMember> mMembers { get; set; }
        public virtual DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public virtual DbSet<SiteSettings> SiteSettings { get; set; }
        public virtual DbSet<mConstituency> mConstituency { get; set; }
        public virtual DbSet<DistrictModel> mDistricts { get; set; }
        public virtual DbSet<mStates> mStates { get; set; }
        public virtual DbSet<mMinistry> mMinistry { get; set; }
        public virtual DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }
        public virtual DbSet<mUsers> mUsers { get; set; }
        public virtual DbSet<mMemberDesignation> mMemDesignation { get; set; }
        public virtual DbSet<mMemberHighestQualification> mMemberHighestQualification { get; set; }
        public virtual DbSet<ActivityLog> ActivityLog { get; set; }
        public virtual DbSet<ActivityCategory> ActivityCategory { get; set; }
        public virtual DbSet<ActivityImages> ActivityImages { get; set; }
        public virtual DbSet<tSectors> Sectors { get; set; }
        public virtual DbSet<tKnowldgeBankRef> KnowldgeBankRef { get; set; }
        public virtual DbSet<tCollectionType> CollectionType { get; set; }

        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {
                //Newly added for member data
                case "GetAllMembers": { return GetAllMembers(); }
                case "GetAllMembersList": { return GetAllMembersList(); }
                case "GetAllMembersListSearch": { return GetAllMembersListSearch(param.Parameter); }
                case "GetAllMembersListSearchByName": { return GetAllMembersListSearchByName(param.Parameter); }
                case "GetAllMembersListByAssembly": { return GetAllMembersListByAssembly(param.Parameter); }
                case "GetMemberByAadharID": { return GetMemberByAadharID(param.Parameter); }
                case "CreateMember": { return CreateMember(param.Parameter); }
                case "UpdateMember": { return UpdateMember(param.Parameter); }
                case "DeleteMember": { return DeleteMember(param.Parameter); }
                case "GetMemberById": { return GetMemberById(param.Parameter); }
                case "GetMemberById1": { return GetMemberById1(param.Parameter); }
                case "GetMemberCodeById": { return GetMemberCodeById(); }
                case "GetAllDistrict": { return GetAllDistrict(); }
                case "GetAllStates": { return GetAllStates(); }
                case "GetAllMemberHighestQualification": { return GetAllMemberHighestQualification(); }
                case "GetAllMemberCodes": { return GetAllMemberCodes(); }
                case "GetMembeCodeById": { return GetMembeCodeById(param.Parameter); }
                case "IsMemberIdChildExist": { return IsMemberIdChildExist(param.Parameter); }
                case "CheckMemberNUmber": { return CheckMemberNUmber(param.Parameter); }

                //*****************************************


                //Newly added for memberAssembly data
                case "GetAllMemberAssembly": { return GetAllMemberAssembly(param.Parameter); }
                case "GetAllMemberAssemblyByAssembly": { return GetAllMemberAssemblyByAssembly(param.Parameter); }
                case "CreateMemberAssembly": { CreateMemberAssembly(param.Parameter); break; }
                case "UpdateMemberAssembly": { return UpdateMemberAssembly(param.Parameter); }
                case "DeleteMemberAssembly": { return DeleteMemberAssembly(param.Parameter); }
                case "GetMemberAssemblyById": { return GetMemberAssemblyById(param.Parameter); }
                case "GetAllParty": { return GetAllParty(); }
                case "GetAllConstituency": { return GetAllConstituency(); }
                case "GetAllMemberDesignation":
                    {
                        return GetAllMemberDesignation();
                    }
                case "GetConstituencyByAssemblyID":
                    {
                        return GetConstituencyByAssemblyID(param.Parameter);
                    }


                //*****************************************
                case "GetMemberByAssemblyID":
                    {
                        return GetMemberByAssemblyID(param.Parameter);
                    }
                case "GetMemberByAssemblyCode":
                    {
                        return GetMemberByAssemblyCode(param.Parameter);
                    }
                case "GetMemberDetailsByMemberCode":
                    {
                        return GetMemberDetailsByMemberCode(param.Parameter);
                    }
                case "GetMemberInfoByMemberCode":
                    {
                        return GetMemberInfoByMemberCode(param.Parameter);
                    }

                case "GetMemberDetailsById":
                    {
                        return GetMemberDetailsById(param.Parameter);
                    }

                case "SaveMemberData":
                    {
                        return SaveMemberData(param.Parameter);
                    }
                case "GetConstituencyCodeByMember":
                    {
                        return GetConstituencyCodeByMember(param.Parameter);
                    }
                case "GetMemberByUserSubType":
                    {
                        return GetMemberByUserSubType(param.Parameter);
                    }
                case "GetMobileNoByMembercode":
                    {
                        return GetMobileNoByMembercode(param.Parameter);
                    }
                case "GetMemberNameByIds":
                    {
                        return GetMemberNameByIds(param.Parameter);
                    }
                case "GetMemByAssemblyDiary":
                    {
                        return GetMemByAssemblyDiary(param.Parameter);
                    }
                case "GetMinistryByAssemblyDiary":
                    {
                        return GetMinistryByAssemblyDiary(param.Parameter);
                    }
                case "GetMemberList_ByAssembly":
                    {
                        return GetMemberList_ByAssembly(param.Parameter);
                    }
                case "getMemberActivityList":
                    {
                        return getMemberActivityList(param.Parameter);
                    }
                case "GetDropDownData":
                    {
                        return GetDropDownData(param.Parameter);
                    }
                case "getMemberActivityListByCategory":
                    {
                        return getMemberActivityListByCategory(param.Parameter);
                    }
                case "getMemberActivityListByCategoryAll":
                    {
                        return getMemberActivityListByCategoryAll(param.Parameter);
                    }
                case "getActivityImagesList":
                    {
                        return getActivityImagesList(param.Parameter);
                    }
                case "getActivityImagesListById":
                    {
                        return getActivityImagesListById(param.Parameter);
                    }

                case "GetCollectionDataForCollectionType":
                    {
                        return GetCollectionDataForCollectionType(param.Parameter);
                    }
                case "GetCollectionData":
                    {
                        return GetCollectionData(param.Parameter);
                    }
                case "getReferencesListUpdated":
                    {
                        return getReferencesListUpdated(param.Parameter);
                    }
                case "getReferencesList":
                    {
                        return getReferencesList(param.Parameter);
                    }

                case "AddSector":
                    {
                        return AddSector(param.Parameter);
                    }
                case "DeleteSector":
                    {
                        return DeleteSector(param.Parameter);
                    }

                case "GetSectorById":
                    {
                        return GetSectorById(param.Parameter);
                    }

                case "GetAllSectors":
                    {
                        return GetAllSectors();
                    }

                case "UpdateSector":
                    {
                        return UpdateSector(param.Parameter);
                    }

                case "AddKnowledgeBankListEntry":
                    {
                        return AddKnowledgeBankListEntry(param.Parameter);
                    }
                case "UpdateKnowledgeBankList":
                    {
                        return UpdateKnowledgeBankList(param.Parameter);
                    }
                case "GetListEntryById":
                    {
                        return GetListEntryById(param.Parameter);
                    }
                case "DeleteKnowledgeListEntry":
                    {
                        return DeleteKnowledgeListEntry(param.Parameter);
                    }
                case "getReferencesCount":
                    {
                        return getReferencesCount(param.Parameter);
                    }
                case "getLastUpdationDate":
                    {
                        return getLastUpdationDate(param.Parameter);
                    }

                case "AddCollectionType":
                    {
                        return AddCollectionType(param.Parameter);
                    }
                case "UpdateCollectionType":
                    {
                        return UpdateCollectionType(param.Parameter);
                    }
                case "GetCollectionTypeById":
                    {
                        return GetCollectionTypeById(param.Parameter);
                    }
                case "DeleteCollectionType":
                    {
                        return DeleteCollectionType(param.Parameter);
                    }


            }

            return null;
        }

        //Newly added for memberAssembly data

        private static object IsMemberIdChildExist(object param)
        {
            try
            {
                using (MemberContext db = new MemberContext())
                {
                    mMember model = (mMember)param;
                    var Res = (from e in db.mMemberAssembly
                               where (e.MemberID == model.MemberCode)
                               select e).Count();

                    if (Res == 0)
                    {
                        var minis = (from e in db.mMinistry
                                     where (e.MemberCode == model.MemberCode)
                                     select e).Count();

                        if (minis == 0)
                        {
                            var minisMin = (from e in db.mMinsitryMinister
                                            where (e.MemberCode == model.MemberCode)
                                            select e).Count();
                            if (minisMin == 0)
                            {
                                return false;
                            }

                        }


                    }

                    else
                    {
                        return true;

                    }

                }
                return true;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }
        static int GetMembeCodeById(object param)
        {
            try
            {
                MemberContext db = new MemberContext();
                int id = Convert.ToInt32(param);
                var query1 = db.mMembers.SingleOrDefault(a => a.MemberID == id);
                return query1.MemberCode;

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
        static object GetMemberByUserSubType(object param)
        {
            mUsers parameter = param as mUsers;
            MemberContext db = new MemberContext();
            var query = new mUsers();
            if (parameter.IsMember.ToUpper() == "TRUE")
            {
                query = db.mUsers.SingleOrDefault(a => a.UserName == parameter.UserName && a.IsMember.ToUpper() == "TRUE");
            }
            else
            {
                query = db.mUsers.SingleOrDefault(a => a.UserName == parameter.UserName);
            }
            return query;



        }

        static object GetAllMemberAssembly(object param)
        {

            try
            {
                DBManager db = new DBManager();
                mMemberAssembly model = param as mMemberAssembly;
                var data = (

                    from memAssembly in db.mMemberAssembly

                    join assembly in db.mAssemblies on memAssembly.AssemblyID equals assembly.AssemblyCode into joinAssemblyName
                    from assem in joinAssemblyName.DefaultIfEmpty()
                    //join member in db.mMembers on memAssembly.MemberID equals member.MemberCode into joinmemberName
                    //from mem in joinmemberName.DefaultIfEmpty()
                    join desig in db.mMemDesignation on memAssembly.DesignationID equals desig.memDesigId into joinmemDesigname
                    from des in joinmemDesigname.DefaultIfEmpty()


                    // SqlFunctions.StringConvert((double)t1.Pk_Id).Trim()
                    join party in db.mParty on memAssembly.PartyID equals party.PartyID into joinpartyName

                    from partycode in joinpartyName.DefaultIfEmpty()
                    where memAssembly.IsDeleted == null
                    //orderby memAssembly.MemberAssemblyID
                    select new
                    {
                        memAssembly,
                        assem.AssemblyName,
                        //mem.Name,
                        partycode.PartyName,
                        des.memDesigname,
                        // cont.ConstituencyName,
                        MemberName = (from mc in db.mMembers where mc.MemberCode == memAssembly.MemberID select mc.Name).FirstOrDefault(),
                        ConstituencyName = (from t in db.mConstituency
                                            where t.ConstituencyCode == memAssembly.ConstituencyCode && t.AssemblyID == memAssembly.AssemblyID
                                            select t.ConstituencyName).FirstOrDefault(),

                    }).ToList();

                List<mMemberAssembly> list = new List<mMemberAssembly>();
                foreach (var item in data)
                {
                    item.memAssembly.GetAssemblyName = item.AssemblyName;
                    item.memAssembly.GetMemberName = item.MemberName;
                    item.memAssembly.GetpartyName = item.PartyName;
                    item.memAssembly.GetMemberDesignationName = item.memDesigname;
                    item.memAssembly.GetConstituencyName = item.ConstituencyName;
                    list.Add(item.memAssembly);

                }
                var data1 = (from a in db.mMemberAssembly

                             where a.IsDeleted == null
                             orderby a.MemberAssemblyID descending
                             select a).Count();

                int totalRecords = data1;
                model.ResultCount = totalRecords;
                var results = list.OrderByDescending(m => m.GetAssemblyName).ThenBy(m => m.GetMemberName).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                model.MemberAssemblyList = results;
                return model;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetAllMemberAssemblyByAssembly(object param)
        {

            try
            {
                DBManager db = new DBManager();
                mMemberAssembly model = param as mMemberAssembly;
                var data = (

                    from memAssembly in db.mMemberAssembly

                    join assembly in db.mAssemblies on memAssembly.AssemblyID equals assembly.AssemblyCode into joinAssemblyName
                    from assem in joinAssemblyName.DefaultIfEmpty()
                    //join member in db.mMembers on memAssembly.MemberID equals member.MemberCode into joinmemberName
                    //from mem in joinmemberName.DefaultIfEmpty()
                    join desig in db.mMemDesignation on memAssembly.DesignationID equals desig.memDesigId into joinmemDesigname
                    from des in joinmemDesigname.DefaultIfEmpty()


                    // SqlFunctions.StringConvert((double)t1.Pk_Id).Trim()
                    join party in db.mParty on memAssembly.PartyID equals party.PartyID into joinpartyName

                    from partycode in joinpartyName.DefaultIfEmpty()
                    where memAssembly.IsDeleted == null
                        //&& (!model.AssemblyID.HasValue || a.CommitteeId == CommitteeId)
                    && memAssembly.AssemblyID == model.AssemblyID
                    //orderby memAssembly.MemberAssemblyID
                    select new
                    {
                        memAssembly,
                        assem.AssemblyName,
                        //mem.Name,
                        partycode.PartyName,
                        des.memDesigname,
                        // cont.ConstituencyName,
                        MemberName = (from mc in db.mMembers where mc.MemberCode == memAssembly.MemberID select mc.Name).FirstOrDefault(),
                        ConstituencyName = (from t in db.mConstituency
                                            where t.ConstituencyCode == memAssembly.ConstituencyCode && t.AssemblyID == memAssembly.AssemblyID
                                            select t.ConstituencyName).FirstOrDefault(),

                    }).ToList();

                List<mMemberAssembly> list = new List<mMemberAssembly>();
                foreach (var item in data)
                {
                    item.memAssembly.GetAssemblyName = item.AssemblyName;
                    item.memAssembly.GetMemberName = item.MemberName;
                    item.memAssembly.GetpartyName = item.PartyName;
                    item.memAssembly.GetMemberDesignationName = item.memDesigname;
                    item.memAssembly.GetConstituencyName = item.ConstituencyName;
                    list.Add(item.memAssembly);

                }
                var data1 = (from a in db.mMemberAssembly

                             where a.IsDeleted == null
                             orderby a.MemberAssemblyID descending
                             select a).Count();

                int totalRecords = data1;
                model.ResultCount = totalRecords;
                var results = list.OrderByDescending(m => m.GetAssemblyName).ThenBy(m => m.GetMemberName).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                model.MemberAssemblyList = results;
                return model;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mParty> GetAllParty()
        {
            DBManager db = new DBManager();
            var data = (from p in db.mParty
                        orderby p.PartyName ascending
                        where p.IsDeleted == null
                        select p).ToList();
            return data;

        }
        static List<mConstituency> GetAllConstituency()
        {
            DBManager db = new DBManager();
            var data = (from p in db.mConstituency
                        orderby p.ConstituencyName ascending
                        where p.IsDeleted == null
                        select p).ToList();
            return data;

        }

        //public virtual DbSet<mMemberDesignation> mMemDesignation { get; set; }
        static List<mMemberDesignation> GetAllMemberDesignation()
        {
            DBManager db = new DBManager();
            var data = (from p in db.mMemDesignation
                        orderby p.memDesigname ascending
                        where p.IsDeleted == null
                        select p).ToList();
            return data;

        }



        static List<mConstituency> GetConstituencyByAssemblyID(object param)
        {
            MemberContext db = new MemberContext();
            mConstituency model = param as mConstituency;
            var query = (from dist in db.mConstituency
                         where dist.AssemblyID == model.AssemblyID
                         && dist.IsDeleted != true
                         orderby dist.ConstituencyName ascending
                         select dist);

            return query.ToList();
        }



        static void CreateMemberAssembly(object param)
        {
            try
            {
                using (MemberContext db = new MemberContext())
                {
                    mMemberAssembly model = param as mMemberAssembly;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMemberAssembly.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateMemberAssembly(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {
                mMemberAssembly model = param as mMemberAssembly;
                model.CreatedDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mMemberAssembly.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllMemberAssembly(param);
        }

        static object DeleteMemberAssembly(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {
                mMemberAssembly parameter = param as mMemberAssembly;
                mMemberAssembly memberToRemove = db.mMemberAssembly.SingleOrDefault(a => a.MemberAssemblyID == parameter.MemberAssemblyID);
                if (memberToRemove != null)
                {
                    memberToRemove.IsDeleted = true;
                }
                //db.mMemberAssembly.Remove(memberToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllMemberAssembly(param);
        }

        static mMemberAssembly GetMemberAssemblyById(object param)
        {
            mMemberAssembly parameter = param as mMemberAssembly;
            MemberContext db = new MemberContext();
            var query = db.mMemberAssembly.SingleOrDefault(a => a.MemberAssemblyID == parameter.MemberAssemblyID);
            return query;
        }

        //*****************************************


        static List<mMember> GetAllMemberCodes()
        {
            MemberContext db = new MemberContext();
            //var query = (from p in db.mMembers
            //             orderby p.MemberCode 
            //             select p);
            //return query.ToList();
            var data = db.mMembers.ToList();
            return data;
        }

        static List<DistrictModel> GetAllDistrict()
        {
            DBManager db = new DBManager();
            var query = (from p in db.Districts
                         orderby p.DistrictCode ascending
                         where p.IsDeleted == null
                         select p);
            return query.ToList();
        }
        static List<mStates> GetAllStates()
        {
            DBManager db = new DBManager();
            var query = (from p in db.mStates
                         orderby p.StateName ascending
                         where p.IsDeleted == null
                         select p);
            return query.ToList();
        }

        static List<mMemberHighestQualification> GetAllMemberHighestQualification()
        {
            DBManager db = new DBManager();
            var query = (from p in db.mMemberHighestQualification
                         orderby p.MemberHighestQualificationId ascending
                         where p.IsDeleted == null
                         select p);
            return query.ToList();
        }

        static int GetMemberCodeById()
        {
            // mConstituency parameter = param as mConstituency;
            try
            {
                MemberContext db = new MemberContext();
                //int id = Convert.ToInt32(param);
                mMember model = new mMember();
                var query1 = db.mMembers.ToList();

                if (query1.Count > 0)
                {

                    var query = query1.Max(x => x.MemberCode);
                    return query;

                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
        static mMember GetMemberById(object param)
        {
            mMember parameter = param as mMember;
            MemberContext db = new MemberContext();
            //var query = db.mMembers.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
            var query = db.mMembers.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
            return query;
        }
        static object GetMemberNameByIds(object param)
        {
            mUsers parameter = param as mUsers;
            string userNames = "";
            List<int> TagIds = parameter.UserName.Split(',').Select(int.Parse).ToList();
            MemberContext db = new MemberContext();
            //var query = db.mMembers.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
            var data = db.mMembers.Where(t => TagIds.Contains(t.MemberCode)).Select(t => t.Name).ToList();
            foreach (var n in data)
            {
                userNames += n + ",";
            }
            bool flag = userNames.EndsWith(",");
            if (flag)
            {
                userNames = userNames.Substring(0, userNames.Length - 1);
            }
            return userNames;
        }

        static mMember GetMemberById1(object param)
        {
            mMember parameter = param as mMember;
            MemberContext db = new MemberContext();
            //var query = db.mMembers.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
            var query = db.mMembers.SingleOrDefault(a => a.MemberID == parameter.MemberID);
            return query;
        }

        static object DeleteMember(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {
                mMember parameter = param as mMember;
                mMember memberToRemove = db.mMembers.SingleOrDefault(a => a.MemberID == parameter.MemberID);
                if (memberToRemove != null)
                {
                    memberToRemove.IsDeleted = true;
                }
                // db.mMembers.Remove(memberToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllMembers();
        }
        static object UpdateMember(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {
                mMember model = param as mMember;
                model.CreatedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                db.mMembers.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllMembers();
        }
        //static void CreateMember(object param)
        static object CreateMember(object param)
        {
            try
            {
                using (MemberContext db = new MemberContext())
                {
                    mMember model = param as mMember;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mMembers.Add(model);
                    db.SaveChanges();
                    db.Close();
                    return model.MemberID;
                }

            }
            catch
            {
                throw;
            }
        }

        private static object SaveMemberData(object p)
        {
            var memcode = p as mMember;
            using (MemberContext MCtx = new MemberContext())
            {
                var memberDetails = (from a in MCtx.mMembers where a.MemberCode == memcode.MemberCode select a).FirstOrDefault();

                memberDetails.Email = memcode.Email;
                memberDetails.Description = memcode.Description;
                memberDetails.NameLocal = memcode.NameLocal;
                memberDetails.Name = memcode.Name;
                memberDetails.PermanentAddress = memcode.PermanentAddress;
                memberDetails.ShimlaAddress = memcode.ShimlaAddress;
                memberDetails.TelOffice = memcode.TelOffice;
                memberDetails.TelResidence = memcode.TelResidence;
                memberDetails.Mobile = memcode.Mobile;
                memberDetails.FatherName = memcode.FatherName;
                memberDetails.District = memcode.District;
                MCtx.mMembers.Attach(memberDetails);
                MCtx.Entry(memberDetails).State = EntityState.Modified;
                MCtx.SaveChanges();

                return memberDetails;
            }
        }

        private static object GetMemberDetailsById(object p)
        {
            try
            {
                var memcode = p as mMember;

                using (MemberContext MCtx = new MemberContext())
                {
                    var memberDetails = (from a in MCtx.mMembers where a.MemberCode == memcode.MemberCode select a).FirstOrDefault();

                    return memberDetails;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        #region GetMemberDetailByCode

        static object GetMemberDetailsByMemberCode(object param)
        {
            MemberContext MCtx = new MemberContext();

            mMember model = param as mMember;

            var M = (from mem in MCtx.mMembers
                     where mem.MemberCode == model.MemberCode
                     select mem).SingleOrDefault();
            return M;

        }

        #endregion
        static object GetMemberByAssemblyCode(object param)
        {
            MemberContext MCtx = new MemberContext();

            int AssemblyID = (int)(param);

            var M = (from mem in MCtx.mMembers
                     join MemAssembly in MCtx.mMemberAssembly on mem.MemberCode equals MemAssembly.MemberID
                     where MemAssembly.AssemblyID == AssemblyID
                     select mem).ToList();
            return M;

        }

        static object GetMemberByAssemblyID(object param)
        {
            MemberContext MCtx = new MemberContext();

            mAssembly m = param as mAssembly;

            var M = (from mem in MCtx.mMembers
                     join MemAssembly in MCtx.mMemberAssembly on mem.MemberCode equals MemAssembly.MemberID
                     where MemAssembly.AssemblyID == m.AssemblyID
                     select mem).ToList();
            return M;

        }

        static object GetMemByAssemblyDiary(object param)
        {
            MemberContext MCtx = new MemberContext();

            DiaryModel m = param as DiaryModel;

            var M = (from mem in MCtx.mMembers
                     join MemAssembly in MCtx.mMemberAssembly on mem.MemberCode equals MemAssembly.MemberID
                     where MemAssembly.AssemblyID == m.AssemblyID
                     && MemAssembly.IsDeleted != true
                     select new DiaryModel
                            {
                                MemberCode = mem.MemberCode,
                                MemberName = mem.Name,

                            }).ToList();

            return M;

        }

        static object GetMinistryByAssemblyDiary(object param)
        {
            MemberContext MCtx = new MemberContext();

            DiaryModel m = param as DiaryModel;
            //change by robin added isactive and isdeleted condition
            var M = (from mem in MCtx.mMinistry
                     where mem.AssemblyID == m.AssemblyID
                     && mem.IsActive == true
                     && mem.IsDeleted != true
                     select new DiaryModel
                     {
                         MinistryId = mem.MinistryID,
                         MinistryName = mem.MinistryName,

                     }).ToList();

            return M;

        }

        static List<mMember> GetAllMembersList()
        {

            MemberContext db = new MemberContext();
            string AssemblyCode = (from mdl in db.SiteSettings
                                   where mdl.SettingName == "Assembly"
                                   select mdl.SettingValue).SingleOrDefault();
            Int32 asscode = 0;
            Int32.TryParse(AssemblyCode, out asscode);
            var data = (from Memb in db.mMembers
                        //join ma in db.mMemberAssembly on Memb.MemberCode equals ma.MemberID
                        join state in db.mStates on Memb.StateNameID equals state.mStateID into joinStateName
                        from Stat in joinStateName.DefaultIfEmpty()
                        join district in db.mDistricts on Memb.DistrictID equals district.DistrictCode into joinDistrictName
                        from distr in joinDistrictName.DefaultIfEmpty()
                        join high in db.mMemberHighestQualification on Memb.HighestQualificationID equals high.MemberHighestQualificationId into joinhighName
                        from highq in joinhighName.DefaultIfEmpty()
                        where Memb.IsDeleted == null //&& ma.AssemblyID == asscode
                        select new
                        {
                            Memb,
                            Stat.StateName,
                            distr.DistrictName,
                            highq.Qualification

                        }).ToList().Distinct();
            //var data1=
            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            List<mMember> list = new List<mMember>();
            foreach (var item in data)
            {
                item.Memb.GetStateName = item.StateName;
                item.Memb.GetDistrictName = item.DistrictName;
                item.Memb.GetHighestQualificationName = item.Qualification;

                item.Memb.ImageAcessurl = siteSettingData.SettingValue + item.Memb.FilePath + "/Thumb/" + item.Memb.FileName;


                list.Add(item.Memb);

            }
            return list.OrderByDescending(c => c.MemberID).ToList();
            //var query = (from p in db.mMembers
            //             orderby p.MemberID descending
            //             select p);
            //return query.ToList();
        }

        static List<mMember> GetAllMembersListByAssembly(object obj)
        {
            int assemblyid = Convert.ToInt16(obj);
            MemberContext db = new MemberContext();
            //string AssemblyCode = (from mdl in db.SiteSettings
            //                       where mdl.SettingName == "Assembly"
            //                       select mdl.SettingValue).SingleOrDefault();
            // Int32 asscode = 0;
            // Int32.TryParse(AssemblyCode, out asscode);
            var data = (from Memb in db.mMembers
                        join ma in db.mMemberAssembly on Memb.MemberCode equals ma.MemberID
                        join state in db.mStates on Memb.StateNameID equals state.mStateID into joinStateName
                        from Stat in joinStateName.DefaultIfEmpty()
                        join district in db.mDistricts on Memb.DistrictID equals district.DistrictCode into joinDistrictName
                        from distr in joinDistrictName.DefaultIfEmpty()
                        join high in db.mMemberHighestQualification on Memb.HighestQualificationID equals high.MemberHighestQualificationId into joinhighName
                        from highq in joinhighName.DefaultIfEmpty()
                        where Memb.IsDeleted == null && ma.AssemblyID == assemblyid
                        select new
                        {
                            Memb,
                            Stat.StateName,
                            distr.DistrictName,
                            highq.Qualification

                        }).ToList();
            //var data1=
            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            List<mMember> list = new List<mMember>();
            foreach (var item in data)
            {
                item.Memb.GetStateName = item.StateName;
                item.Memb.GetDistrictName = item.DistrictName;
                item.Memb.GetHighestQualificationName = item.Qualification;

                item.Memb.ImageAcessurl = siteSettingData.SettingValue + item.Memb.FilePath + "/Thumb/" + item.Memb.FileName;


                list.Add(item.Memb);

            }
            return list.OrderByDescending(c => c.MemberID).ToList();
            //var query = (from p in db.mMembers
            //             orderby p.MemberID descending
            //             select p);
            //return query.ToList();
        }



        static List<mMember> GetAllMembersListSearch(object param)
        {

            MemberContext db = new MemberContext();
            int id = Convert.ToInt32(param);
            var data = (from Memb in db.mMembers
                        join state in db.mStates on Memb.StateNameID equals state.mStateID into joinStateName
                        from Stat in joinStateName.DefaultIfEmpty()
                        join district in db.mDistricts on Memb.DistrictID equals district.DistrictCode into joinDistrictName
                        from distr in joinDistrictName.DefaultIfEmpty()
                        join high in db.mMemberHighestQualification on Memb.HighestQualificationID equals high.MemberHighestQualificationId into joinhighName
                        from highq in joinhighName.DefaultIfEmpty()
                        where Memb.IsDeleted == null && Memb.MemberCode == id
                        select new
                        {
                            Memb,
                            Stat.StateName,
                            distr.DistrictName,
                            highq.Qualification

                        }).ToList();
            //var data1=
            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            List<mMember> list = new List<mMember>();
            foreach (var item in data)
            {
                item.Memb.GetStateName = item.StateName;
                item.Memb.GetDistrictName = item.DistrictName;
                item.Memb.GetHighestQualificationName = item.Qualification;

                item.Memb.ImageAcessurl = siteSettingData.SettingValue + item.Memb.FilePath + "/Thumb/" + item.Memb.FileName;


                list.Add(item.Memb);

            }
            return list.OrderByDescending(c => c.MemberID).ToList();
            //var query = (from p in db.mMembers
            //             orderby p.MemberID descending
            //             select p);
            //return query.ToList();
        }


        static List<mMember> GetAllMembersListSearchByName(object param)
        {

            MemberContext db = new MemberContext();
            string name = Convert.ToString(param);
            var data = (from Memb in db.mMembers
                        join state in db.mStates on Memb.StateNameID equals state.mStateID into joinStateName
                        from Stat in joinStateName.DefaultIfEmpty()
                        join district in db.mDistricts on Memb.DistrictID equals district.DistrictCode into joinDistrictName
                        from distr in joinDistrictName.DefaultIfEmpty()
                        join high in db.mMemberHighestQualification on Memb.HighestQualificationID equals high.MemberHighestQualificationId into joinhighName
                        from highq in joinhighName.DefaultIfEmpty()
                        //where Memb.IsDeleted == null 
                        where Memb.IsDeleted.Equals(null) && Memb.Name.Contains(name)
                        select new
                        {
                            Memb,
                            Stat.StateName,
                            distr.DistrictName,
                            highq.Qualification

                        }).ToList();
            //var data1=
            // data = data.Where(q => q.Memb.Name.Contains(name));

            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            List<mMember> list = new List<mMember>();
            foreach (var item in data)
            {
                item.Memb.GetStateName = item.StateName;
                item.Memb.GetDistrictName = item.DistrictName;
                item.Memb.GetHighestQualificationName = item.Qualification;

                item.Memb.ImageAcessurl = siteSettingData.SettingValue + item.Memb.FilePath + "/Thumb/" + item.Memb.FileName;


                list.Add(item.Memb);

            }
            return list.OrderByDescending(c => c.MemberID).ToList();
            //var query = (from p in db.mMembers
            //             orderby p.MemberID descending
            //             select p);
            //return query.ToList();
        }

        static List<mMember> GetAllMembers()
        {

            MemberContext db = new MemberContext();
            var query = (from p in db.mMembers
                         orderby p.Name
                         select p);
            return query.ToList();
        }

        static mMember GetMemberByAadharID(object param)
        {
            string obj = param as string;
            MemberContext db = new MemberContext();
            var query = db.mMembers.SingleOrDefault(a => a.AadhaarNo == obj);
            return query;
        }

        static object GetMemberInfoByMemberCode(object param)
        {
            MemberContext db = new MemberContext();
            mMember Member = (mMember)param;

            string AssemblyCode = (from mdl in db.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();

            int AssemblyCode1 = Convert.ToInt16(AssemblyCode);
            var LOB = (from Members in db.mMembers
                       join MemberAssembly in db.mMemberAssembly on new { MemberID = Members.MemberCode, AssemblyID = AssemblyCode1 } equals new { MemberID = MemberAssembly.MemberID, AssemblyID = MemberAssembly.AssemblyID }
                       join Constituency in db.mConstituency on MemberAssembly.ConstituencyCode equals Constituency.ConstituencyCode
                       where Members.MemberCode == Member.MemberCode
                       select new QuestionModelCustom
                       {
                           MemberName = Members.Name,
                           MemberNameLocal = Members.NameLocal,
                           ConstituencyName = Constituency.ConstituencyName,
                           ConstituencyName_Local = Constituency.ConstituencyName_Local
                       }).FirstOrDefault();


            return LOB;
        }
        static object GetMemberList_ByAssembly(object param)
        {
            MemberContext db = new MemberContext();
            // mMember Member = (mMember)param;

            string AssemblyCode = (from mdl in db.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();

            int AssemblyCode1 = Convert.ToInt16(AssemblyCode);
            var memList = (from Members in db.mMembers
                           join MemberAssembly in db.mMemberAssembly on Members.MemberCode equals MemberAssembly.MemberID
                           //  join Assembly in db.si


                           //  join MemberAssembly in db.mMemberAssembly on new { MemberID = Members.MemberCode, AssemblyID = AssemblyCode1 } equals new { MemberID = MemberAssembly.MemberID, AssemblyID = MemberAssembly.AssemblyID }
                           //join MemberAssembly in db.mMemberAssembly on Members.MemberCode equals MemberAssembly.MemberID //&& MemberAssembly.AssemblyID =  

                           where Members.Active == true && MemberAssembly.AssemblyID == AssemblyCode1
                           select new QuestionModelCustom
                           {
                               MemberName = Members.Name,
                               MemberId = Members.MemberCode,
                           }).ToList();


            return memList;
        }

        private static object GetConstituencyCodeByMember(object param)
        {
            MemberContext _context = new MemberContext();

            mMember Member = (mMember)param;

            string AssemblyCode = (from mdl in _context.SiteSettings
                                   where mdl.SettingName == "Assembly"
                                   select mdl.SettingValue).SingleOrDefault();

            int CurrentAssembly = Convert.ToInt32(AssemblyCode);

            var ConstituencyCode = (from Members in _context.mMemberAssembly
                                    where Members.MemberID == Member.MemberCode
                                    && Members.AssemblyID == CurrentAssembly
                                    select Members.ConstituencyCode).SingleOrDefault();

            int CurrentConstituencyCode = Convert.ToInt32(ConstituencyCode);

            return CurrentConstituencyCode;
        }

        //static object CheckMemberNUmber(object param)
        //{
        //    string memberNumS = param as string;
        //    int memberNumber = 0;
        //    int.TryParse(memberNumS, out memberNumber);
        //    MemberContext pCtxt = new MemberContext();
        //    mMember model = new mMember();
        //    model = (from mdl in pCtxt.mMembers where mdl.MemberCode == memberNumber select mdl).FirstOrDefault();
        //    return model;
        //}

        private static object CheckMemberNUmber(object p)
        {
            string MobileNo = p as string;
            int memberNumber = 0;
            int.TryParse(MobileNo, out memberNumber);
            //var MobileNo = (string)p;
            using (var ctx = new MemberContext())
            {

                var isexist = (from OTPReg in ctx.mMembers where OTPReg.MemberCode == memberNumber select OTPReg).Count() > 0;

                return isexist;
            }
        }
        private static object GetMobileNoByMembercode(object param)
        {
            MemberContext _context = new MemberContext();

            mMember Member = (mMember)param;

            string MobileNumber = (from mdl in _context.mMembers
                                   where mdl.MemberCode == Member.MemberCode
                                   select mdl.Mobile).SingleOrDefault();

            return MobileNumber;
        }

        public static object getMemberActivityList(object param)
        {
            using (MemberContext context = new MemberContext())
            {
                string Date = DateTime.Now.ToString("dd/MM/yyyy");
                Int16 dd = Convert.ToInt16(Date.Substring(0, 2));
                Int16 mm = Convert.ToInt16(Date.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(Date.Substring(6, 4));
                DateTime myDate = new DateTime(yyyy, mm, dd);

                string NDate = myDate.AddYears(-1).ToString("dd/MM/yyyy");
                Int16 dd1 = Convert.ToInt16(NDate.Substring(0, 2));
                Int16 mm1 = Convert.ToInt16(NDate.Substring(3, 2));
                Int16 yyyy1 = Convert.ToInt16(NDate.Substring(6, 4));
                DateTime newDate = new DateTime(yyyy1, mm1, dd1);



                int MemberCode = Convert.ToInt16(param);
                var result = (from ALog in context.ActivityLog
                              join _ACat in context.ActivityCategory
                              on ALog.ActivityCategoryId equals _ACat.Id
                              where ALog.Date >= newDate && ALog.Date <= myDate && ALog.MemberCode == MemberCode
                              orderby ALog.Date descending
                              select new ActivitySearchzResult
                              {
                                  ActivityId = ALog.Id,
                                  Date = ALog.Date,
                                  ActivityDescription = ALog.ActivityDescription,
                                  ActivityCatCode = ALog.ActivityCode
                              }).ToList();

                return result;
            }
        }


        public static object getMemberActivityListByCategory(object param)
        {
            using (MemberContext context = new MemberContext())
            {
                string[] str = param as string[];
                int ID = Convert.ToInt16(str[2]);
               // DateTime From = Convert.ToDateTime(str[0]);
                //DateTime To = Convert.ToDateTime(str[1]);
                int MemberCode = Convert.ToInt16(str[3]);

                Int16 dd = Convert.ToInt16(str[0].Substring(0, 2));
                Int16 mm = Convert.ToInt16(str[0].Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(str[0].Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime From = new DateTime(yyyy, mm, dd);

                Int16 dd1 = Convert.ToInt16(str[1].Substring(0, 2));
                Int16 mm1 = Convert.ToInt16(str[1].Substring(3, 2));
                Int16 yyyy1 = Convert.ToInt16(str[1].Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime To = new DateTime(yyyy1, mm1, dd1);
               
                var result = (from ALog in context.ActivityLog
                              join _ACat in context.ActivityCategory
                              on ALog.ActivityCategoryId equals _ACat.Id
                              //join _AImage in context.ActivityImages on ALog.ActivityCode equals _AImage.ActivityId
                              where ALog.ActivityCategoryId==ID &&
                              ALog.Date >= From && ALog.Date <= To && ALog.MemberCode == MemberCode
                              orderby ALog.Date descending
                              select new ActivitySearchzResult
                              {
                                  ActivityId = ALog.Id,
                                  Date = ALog.Date,
                                 // FilePath=_AImage.FilePath,
                                  ActivityDescription = ALog.ActivityDescription,
                                  ActivityCatCode=ALog.ActivityCode
                              }).ToList();

                return result;
            }
        }

        public static object getActivityImagesListById(object param)
        {
            //string ID = param as string;
            int ImageCode = Convert.ToInt16(param);
           
            using (MemberContext context = new MemberContext())
            {

              var result = (from ALog in context.ActivityLog
                              join _AImage in context.ActivityImages on ALog.ActivityCode equals _AImage.ActivityId
                              where _AImage.ActivityId == ImageCode
                              select new ActivitySearchzResult
                              {
                                  Date=ALog.Date,
                                  ActivityDescription=ALog.ActivityDescription,
                                  ActivityId = _AImage.ActivityId,
                                  FilePath = _AImage.FilePath
                              }).ToList();

                return result;
            }
        }

        public static object getActivityImagesList(object param)
        {
           
            using (MemberContext context = new MemberContext())
            {
                var result = (from _AImage in context.ActivityImages
                             // where _AImage.ActivityId == ImageCode

                              select new ActivitySearchzResult
                              {
                                  ActivityId = _AImage.ActivityId,
                                  FilePath = _AImage.FilePath
                              }).ToList();

                return result;
            }
        }



        public static object getMemberActivityListByCategoryAll(object param)
        {
            using (MemberContext context = new MemberContext())
            {
                string[] str = param as string[];
                int ID = Convert.ToInt16(str[2]);
               // DateTime From = Convert.ToDateTime(str[0]);
               // DateTime To = Convert.ToDateTime(str[1]);
                int MemberCode = Convert.ToInt16(str[3]);

                Int16 dd = Convert.ToInt16(str[0].Substring(0, 2));
                Int16 mm = Convert.ToInt16(str[0].Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(str[0].Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime From = new DateTime(yyyy, mm, dd);

                Int16 dd1 = Convert.ToInt16(str[1].Substring(0, 2));
                Int16 mm1 = Convert.ToInt16(str[1].Substring(3, 2));
                Int16 yyyy1 = Convert.ToInt16(str[1].Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                DateTime To = new DateTime(yyyy1, mm1, dd1);



                var result = (from ALog in context.ActivityLog
                              join _ACat in context.ActivityCategory
                               on ALog.ActivityCategoryId equals _ACat.Id
                              //join _AImage in context.ActivityImages on ALog.ActivityCode equals _AImage.ActivityId
                              where ALog.Date >= From && ALog.Date <= To && ALog.MemberCode == MemberCode
                              orderby ALog.Date descending
                              select new ActivitySearchzResult
                              {
                                  ActivityId = ALog.Id,
                                  Date = ALog.Date,
                                  ActivityDescription = ALog.ActivityDescription,
                                  ActivityCatCode=ALog.ActivityCode
                                 // FilePath=_AImage.FilePath
                              }).ToList();

                return result;
            }
        }

        public static object GetDropDownData(object param)
        {
            MemberContext db = new MemberContext();
            var Res = (from e in db.ActivityCategory
                       select new ActivityResultDrp
                       {
                           Id = e.Id,
                           ActivityName_Local = e.ActivityName_Local
                           //  ActivityDescription = ALog.ActivityDescription,
                       }).ToList();


            return Res;

        }


        public static object getReferencesList(object param)
        {
            using (MemberContext context = new MemberContext())
            {
                //string Date = DateTime.Now.ToString("dd/MM/yyyy");
                //Int16 dd = Convert.ToInt16(Date.Substring(0, 2));
                //Int16 mm = Convert.ToInt16(Date.Substring(3, 2));
                //Int16 yyyy = Convert.ToInt16(Date.Substring(6, 4));
                //DateTime myDate = new DateTime(yyyy, mm, dd);

                //string NDate = myDate.AddYears(-1).ToString("dd/MM/yyyy");
                //Int16 dd1 = Convert.ToInt16(NDate.Substring(0, 2));
                //Int16 mm1 = Convert.ToInt16(NDate.Substring(3, 2));
                //Int16 yyyy1 = Convert.ToInt16(NDate.Substring(6, 4));
                //DateTime newDate = new DateTime(yyyy1, mm1, dd1);


                var result = (from ALog in context.KnowldgeBankRef
                              join _ACat in context.Sectors on ALog.SectorId equals _ACat.Id
                              join _AColl in context.CollectionType on ALog.CollectionTypeId equals _AColl.Id
                              where _ACat.Active == true
                              // where ALog.SubmittedDate >= newDate && ALog.SubmittedDate <= myDate
                             // orderby ALog.Id descending
                              orderby ALog.SubmittedDate descending
                              select new ReferenceSearchResult
                              {
                                  Id = ALog.Id,
                                  Date = ALog.SubmittedDate,
                                  SectorName = _ACat.Title,
                                  CollectionName = _AColl.Title,
                                  Brief = ALog.Description,
                                  FileLocation = ALog.FileLocation.Replace("\r\n", string.Empty),
                                  RegionId = ALog.RegionId

                              }).ToList();

                return result;
            }
        }

        public static object getReferencesListUpdated(object param)
        {
            ReferenceSearchResult model = new ReferenceSearchResult();
            using (MemberContext context = new MemberContext())
            {
                string[] str = param as string[];
                //int ID = Convert.ToInt16(str[2]);

                model.SectorId = Convert.ToInt16(str[0]);
                model.CollectionTypeId = Convert.ToInt16(str[1]);
                model.RegionId = Convert.ToInt16(str[2]);


                //int TypeId = Convert.ToInt16(str[3]);
                //int RegionId = Convert.ToInt16(str[4]);

                //Int16 dd = Convert.ToInt16(str[0].Substring(0, 2));
                //Int16 mm = Convert.ToInt16(str[0].Substring(3, 2));
                //Int16 yyyy = Convert.ToInt16(str[0].Substring(6, 4));
                //DateTime From = new DateTime(yyyy, mm, dd);

                //Int16 dd1 = Convert.ToInt16(str[1].Substring(0, 2));
                //Int16 mm1 = Convert.ToInt16(str[1].Substring(3, 2));
                //Int16 yyyy1 = Convert.ToInt16(str[1].Substring(6, 4));


                // DateTime To = new DateTime(yyyy1, mm1, dd1);
                var predicate = PredicateBuilder.True<ReferenceSearchResult>();

                if (model.SectorId != 0 && model.SectorId != null)
                {
                    predicate = predicate.And(q => q.SectorId == model.SectorId);

                }
                if (model.CollectionTypeId != 0 && model.CollectionTypeId != null)
                {
                    predicate = predicate.And(q => q.CollectionTypeId == model.CollectionTypeId);

                }
                if (model.RegionId != 0 && model.RegionId != null)
                {
                    predicate = predicate.And(q => q.RegionId == model.RegionId);

                }

                var result = (from ALog in context.KnowldgeBankRef
                              join _ACat in context.Sectors on ALog.SectorId equals _ACat.Id
                              join _AColl in context.CollectionType on ALog.CollectionTypeId equals _AColl.Id
                              where _ACat.Active == true
                              //where ALog.SubmittedDate >= From && ALog.SubmittedDate <= To
                              // && ALog.SectorId == SectorId && ALog.CollectionTypeId == TypeId && ALog.RegionId == RegionId
                              //orderby ALog.Id descending
                              orderby ALog.SubmittedDate descending
                              select new ReferenceSearchResult
                              {
                                  SectorId = ALog.SectorId,
                                  CollectionTypeId = ALog.CollectionTypeId,
                                  RegionId = ALog.RegionId,
                                  Id = ALog.Id,
                                  Date = ALog.SubmittedDate,
                                  SectorName = _ACat.Title,
                                  CollectionName = _AColl.Title,
                                  Brief = ALog.Description,
                                 // FileLocation = ALog.FileLocation
                                  FileLocation = ALog.FileLocation.Replace("\r\n", string.Empty),
                              }).AsExpandable().Where(predicate).ToList();

                return result;
            }
        }


        public static int getReferencesCount(object param)
        {
            ReferenceSearchResult model = new ReferenceSearchResult();
            using (MemberContext context = new MemberContext())
            {
                DateTime[] str = param as DateTime[];
                var startdate = str[0];
                var enddate = str[1];
                int count = (from x in context.KnowldgeBankRef where x.SubmittedDate <= startdate && x.SubmittedDate >= enddate select x).Count();
                return count;
            }
        }

        public static DateTime getLastUpdationDate(object param)
        {
            ReferenceSearchResult model = new ReferenceSearchResult();
            using (MemberContext context = new MemberContext())
            {
                var date = (from x in context.KnowldgeBankRef
                            orderby x.SubmittedDate descending
                            select x.SubmittedDate).FirstOrDefault();

                return Convert.ToDateTime(date);
            }
        }



        public static object GetCollectionData(object param)
        {
            MemberContext db = new MemberContext();
            var Res = (from e in db.Sectors
                       where e.Active == true 
                       orderby e.Title
                       select new SectorResultDrp
                       {
                           Id = e.Id,
                           SectorName = e.Title
                           //  ActivityDescription = ALog.ActivityDescription,
                       }).ToList();


            return Res;

        }


        public static object GetCollectionDataForCollectionType(object param)
        {
            MemberContext db = new MemberContext();
            var Res = (from e in db.CollectionType
                       orderby e.Title
                       select new CollectionResultDrp
                       {
                           Id = e.Id,
                           Title = e.Title
                           //  ActivityDescription = ALog.ActivityDescription,
                       }).ToList();


            return Res;

        }


        

        //------- Madhur--------------------------------------------------------

        static object AddSector(object param)
        {
            try
            {
                using (MemberContext newsContext = new MemberContext())
                {
                    tSectors news = param as tSectors;
                    newsContext.Sectors.Add(news);

                    //  newsContext.tContent.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object DeleteSectorss(object param)
        {
            try
            {
                if (null == param)
                {
                    return null;
                }
                using (MemberContext db = new MemberContext())
                {
                    tSectors parameter = param as tSectors;
                    // mMember memberToRemove = db.mMembers.SingleOrDefault(a => a.MemberID == parameter.MemberID);

                    // tSectors DataTodelete = param as tSectors;
                    // int SectId = DataTodelete.Id;

                    tSectors tSectors = db.Sectors.SingleOrDefault(a => a.Id == parameter.Id);
                    db.Sectors.Remove(tSectors);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static List<tSectors> GetAllSectors()
        {
            MemberContext db = new MemberContext();

            // var query = db.mParty.ToList();
            var query = (from a in db.Sectors
                         orderby a.Id descending

                         select a).ToList();

            return query;
        }

        static tSectors GetSectorById(object param)
        {
            tSectors Mdl = param as tSectors;
            MemberContext db = new MemberContext();
            var query = db.Sectors.SingleOrDefault(a => a.Id == Mdl.Id);
            return query;
        }

        static object DeleteSector(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {
                tSectors parameter = param as tSectors;
                tSectors sessionToRemove = db.Sectors.SingleOrDefault(a => a.Id == parameter.Id);

                db.Sectors.Remove(sessionToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllSectors();
        }

        static object UpdateSector(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {

                tSectors parameter = param as tSectors;
                db.Sectors.Attach(parameter);
                db.Entry(parameter).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllSectors();
        }

        static tKnowldgeBankRef GetListEntryById(object param)
        {
            tKnowldgeBankRef Mdl = param as tKnowldgeBankRef;
            MemberContext db = new MemberContext();
            var query = db.KnowldgeBankRef.SingleOrDefault(a => a.Id == Mdl.Id);
            return query;
        }
      
        static object AddKnowledgeBankListEntry(object param)
        {
            try
            {
                using (MemberContext newsContext = new MemberContext())
                {
                    tKnowldgeBankRef news = param as tKnowldgeBankRef;

                    newsContext.KnowldgeBankRef.Add(news);

                    // newsContext.Sectors.Add(news);
                    //  newsContext.tContent.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            // return null;
            return GetKnowledgeBankList();
        }

        static List<tKnowldgeBankRef> GetKnowledgeBankList()
        {
            MemberContext db = new MemberContext();

            // var query = db.mParty.ToList();
            var query = (from a in db.KnowldgeBankRef
                         orderby a.Id descending

                         select a).ToList();

            return query;
        }

        static object UpdateKnowledgeBankList(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {

                tKnowldgeBankRef parameter = param as tKnowldgeBankRef;

                db.KnowldgeBankRef.Attach(parameter);
                db.Entry(parameter).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetKnowledgeBankList();
        }

        static object DeleteKnowledgeListEntry(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {
                tKnowldgeBankRef parameter = param as tKnowldgeBankRef;
                tKnowldgeBankRef sessionToRemove = db.KnowldgeBankRef.SingleOrDefault(a => a.Id == parameter.Id);

                db.KnowldgeBankRef.Remove(sessionToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetKnowledgeBankList();
        }

        static object AddCollectionType(object param)
        {
            try
            {
                using (MemberContext newsContext = new MemberContext())
                {
                    tCollectionType news = param as tCollectionType;
                    newsContext.CollectionType.Add(news);

                    //  newsContext.tContent.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object UpdateCollectionType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {

                tCollectionType parameter = param as tCollectionType;
                db.CollectionType.Attach(parameter);
                db.Entry(parameter).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllCollectionType();
        }

        static List<tCollectionType> GetAllCollectionType()
        {
            MemberContext db = new MemberContext();

            // var query = db.mParty.ToList();
            var query = (from a in db.CollectionType
                         orderby a.Id descending

                         select a).ToList();

            return query;
        }

        static tCollectionType GetCollectionTypeById(object param)
        {
            tCollectionType Mdl = param as tCollectionType;
            MemberContext db = new MemberContext();
            var query = db.CollectionType.SingleOrDefault(a => a.Id == Mdl.Id);
            return query;
        }

        static object DeleteCollectionType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberContext db = new MemberContext())
            {
                tCollectionType parameter = param as tCollectionType;
                tCollectionType sessionToRemove = db.CollectionType.SingleOrDefault(a => a.Id == parameter.Id);

                db.CollectionType.Remove(sessionToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllCollectionType();
        }
       
    }
}
