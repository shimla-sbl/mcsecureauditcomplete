﻿using SBL.DAL;
using SBL.DomainModel.Models.Member;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Member
{
    
        public class HighestQualificationContext : DBBase<HighestQualificationContext>
    {

        public HighestQualificationContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mMemberHighestQualification> mMemberHighestQualification { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateHighestQualification": { CreateHighestQualification(param.Parameter); break; }

                case "UpdateHighestQualification": { return UpdateHighestQualification(param.Parameter); }
                case "DeleteHighestQualification": { return DeleteHighestQualification(param.Parameter); }
                case "GetAllHighestQualification": { return GetAllHighestQualification(); }
                case "GetHighestQualificationById": { return GetHighestQualificationById(param.Parameter); }
                
            }
            return null;
        }

        static void CreateHighestQualification(object param)
        {
            try
            {
                using (HighestQualificationContext db = new HighestQualificationContext())
                {
                    mMemberHighestQualification model = param as mMemberHighestQualification;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMemberHighestQualification.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateHighestQualification(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (HighestQualificationContext db = new HighestQualificationContext())
            {
                mMemberHighestQualification model = param as mMemberHighestQualification;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mMemberHighestQualification.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllHighestQualification();
        }

        static object DeleteHighestQualification(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (HighestQualificationContext db = new HighestQualificationContext())
            {
                mMemberHighestQualification parameter = param as mMemberHighestQualification;
                mMemberHighestQualification stateToRemove = db.mMemberHighestQualification.SingleOrDefault(a => a.MemberHighestQualificationId == parameter.MemberHighestQualificationId);
                if (stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllHighestQualification();
        }

        static List<mMemberHighestQualification> GetAllHighestQualification()
        {
            HighestQualificationContext db = new HighestQualificationContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.mMemberHighestQualification
                         orderby a.MemberHighestQualificationId descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mMemberHighestQualification GetHighestQualificationById(object param)
        {
            mMemberHighestQualification parameter = param as mMemberHighestQualification;
            HighestQualificationContext db = new HighestQualificationContext();
            var query = db.mMemberHighestQualification.SingleOrDefault(a => a.MemberHighestQualificationId == parameter.MemberHighestQualificationId);
            return query;
        }

    }
}
