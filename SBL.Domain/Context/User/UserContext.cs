﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SBL.DAL;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Secretory;
using SBL.DomainModel.Models.UserModule;
//using SBL.DomainModel.Models.Employee;
using System.Data.Entity;
using SBL.Service.Common;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Role;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Media;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.UserComplexModel;
using SBL.DomainModel.Models.Question;
using System.Security.Cryptography;
using System.Data;
using SBL.DomainModel.Models.Notice;
using System.Data.Entity.Validation;
using SBL.Domain.Context.Constituency;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.SubDivision;



namespace SBL.Domain.Context.User
{
    class UserContext : DBBase<UserContext>
    {
        public UserContext() : base("eVidhan") { }//this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mSession> mSessionVS { get; set; }
        public DbSet<mSiteSettingsVS> mSiteSettingsVS { get; set; }
        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<mUserDSHDetails> mUserDSHDetails { get; set; }
        public DbSet<TempUser> TempUser { get; set; }
        public DbSet<mEmployee> mEmployee { get; set; }
        public DbSet<mSecretoryDepartment> mSecretoryDepartment { get; set; }
        public DbSet<mSecretory> mSecretory { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<mMember> mMembers { get; set; }
        public DbSet<AdhaarDetails> AdhaarDetails { get; set; }
        public DbSet<Journalist> mJournalist { get; set; }
        public DbSet<tQuestion> tQuestions { get; set; }
        //Sammer-----and latest robin
        public DbSet<tUserRoles> tUserRoles { get; set; }
        public DbSet<mRoles> mRoles { get; set; }
        public DbSet<tGrievanceOfficerDeleted> tGrievanceOfficerDeleted { get; set; }
        public DbSet<tOneTimeUserRegistration> tOneTimeUserRegistration { get; set; }
        public DbSet<tUserAccessRequest> tUserAccessRequest { get; set; }
        public virtual DbSet<tMemberNotice> tMemberNotice { get; set; }
        public DbSet<tDepartmentOfficeMapped> tDepartmentOfficeMapped { get; set; }
        public DbSet<mConstituency> mConstituency { get; set; }
        public DbSet<mOffice> moffice { get; set; }
        #region AuthEmployee

        public DbSet<AuthorisedEmployee> AuthorisedEmployee { get; set; }
        public DbSet<mSubUserType> mSubUserType { get; set; }
        #endregion
        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetSettings":
                    {
                        return GetSettings(param.Parameter);
                    }
                case "SetSettings":
                    {
                        return SetSettings(param.Parameter);
                    }
                case "AddUserDetails":
                    {
                        return AddUserDetails(param.Parameter);
                    }
                case "GetRegisteredDetails":
                    {
                        return GetRegisteredDetails(param.Parameter);
                    }
                case "ADDUserDSCDetails":
                    {
                        return ADDUserDSCDetails(param.Parameter);
                    }
                case "AddTempUSerData":
                    {
                        return AddTempUSerData(param.Parameter);
                    }
                case "GetIsMemberDetails":
                    {
                        return GetIsMemberDetails(param.Parameter);
                    }
                case "GetUserDetails":
                    {
                        return GetUserDetails(param.Parameter);
                    }

                case "UpdateUserDetails":
                    {
                        return UpdateUserDetails(param.Parameter);
                    }
                case "UpdateUserNodalOffice":
                    {
                        return UpdateUserNodalOffice(param.Parameter);
                    }

                case "UpdatedMemberDetailsReporters":
                    {
                        return UpdatedMemberDetailsReporters(param.Parameter);
                    }


                case "DeactivateUserDsc":
                    {
                        return DeactivateUserDsc(param.Parameter);
                    }

                case "ActivateUserDsc":
                    {
                        return ActivateUserDsc(param.Parameter);
                    }
                case "UpdatedMemberDetails":
                    {
                        return UpdatedMemberDetails(param.Parameter);
                    }
                case "getDscDetailsByUserId":
                    {
                        return getDscDetailsByUserId(param.Parameter);
                    }
                //Nitin User Registration
                case "GetDSCDetailsByHashKey":
                    {
                        return GetDSCDetailsByHashKey(param.Parameter);
                    }
                case "CheckAdhaarID":
                    {
                        return CheckAdhaarID(param.Parameter);
                    }
                case "GetDetailsByadhaarID":
                    {
                        return GetDetailsByadhaarID(param.Parameter);
                    }
                case "AddAadharDetails":
                    {
                        return AddAadharDetails(param.Parameter);
                    }
                case "ChangePassword":
                    {
                        return ChangePassword(param.Parameter);
                    }
                case "GetUserDetailsByUserTypeAndOfficeLevel":
                    {
                        return GetUserDetailsByUserTypeAndOfficeLevel(param.Parameter);
                    }

                case "CheckOldPassword":
                    {
                        return CheckOldPassword(param.Parameter);
                    }
                case "GetForgetPassword":
                    {
                        return GetForgetPassword(param.Parameter);
                    }
                case "GetUserDetailsBySecID":
                    {
                        return GetUserDetailsBySecID(param.Parameter);
                    }
                case "GetAuthorizedUserDetailsBySecID":
                    {
                        return GetAuthorizedUserDetailsBySecID(param.Parameter);
                    }
                case "AuthrorizedEmployee":
                    {
                        return AuthrorizedEmployee(param.Parameter);
                    }
                case "SetUserPermission":
                    {
                        return SetUserPermission(param.Parameter);
                    }
                case "AssignRoles":
                    {
                        return AssignRoles(param.Parameter);
                    }
                case "UnAuthrorizedEmployee":
                    {
                        return UnAuthrorizedEmployee(param.Parameter);
                    }
                case "RemoveEmployeeByUSerID":
                    {
                        return RemoveEmployeeByUSerID(param.Parameter);
                    }
                case "GetUserDetailsByUserID":
                    {
                        return GetUserDetailsByUserID(param.Parameter);
                    }
                case "GetUserDetailsByUserIDSMS":
                    {
                        return GetUserDetailsByUserIDSMS(param.Parameter);
                    }
                case "GetUserDetailsBySecretoryID":
                    {
                        return GetUserDetailsBySecretoryID(param.Parameter);
                    }

                case "DeleteUserByUserID":
                    {
                        return DeleteUserByUserID(param.Parameter);
                    }

                case "GetDetailsOTPID":
                    {
                        return GetDetailsOTPID(param.Parameter);
                    }

                case "GetGeneratedSalt": { return GetGeneratedSalt(); }
                case "WrongAttemptLoginData": { return WrongAttemptLoginData(param.Parameter); }

                //By Robin ----users of account type in musers table accounts type code - 12
                case "GetAllUsersOfAccountType": { return GetAllUsersOfAccountType(); }

                case "GetUserOfAccountTypeById": { return GetUserOfAccountTypeById(param.Parameter); }

                case "AvailableAccountRoles": { return AvailableAccountRoles(); }

                case "UpdateAccountRoles": { return UpdateAccountRoles(param.Parameter); }

                case "GetAllUser":
                    {
                        return GetAllUser(param.Parameter);
                    }
                case "UpdateDeptUser":
                    {
                        UpdateDeptUser(param.Parameter); break;
                    }
                case "UpdatedMemberDepartment":
                    {
                        return UpdatedMemberDepartment(param.Parameter);
                    }
                case "UpdateDepatmentAndSecretary":
                    {
                        return UpdateDepatmentAndSecretary(param.Parameter);
                    }
                case "ExistUserSubTypeId":
                    {
                        return ExistUserSubTypeId(param.Parameter);
                    }
                case "GetUserDeptDetailsByUserID":
                    {
                        return GetUserDeptDetailsByUserID(param.Parameter);
                    }
                case "UpdateAssemblyPassword":
                    {
                        return UpdateAssemblyPassword(param.Parameter);
                    }
                case "GetSettingSettingName":
                    {
                        return GetSettingSettingName(param.Parameter);
                    }
                case "UpdateSiteSettings":
                    {
                        return UpdateSiteSettings(param.Parameter);
                    }
                case "GetUser_DetailsByAdhaarID":
                    {
                        return GetUser_DetailsByAdhaarID(param.Parameter);
                    }
                case "UpdateIdsInRequest":
                    {
                        return UpdateIdsInRequest(param.Parameter);
                    }
                case "CheckPasswordIntemp":
                    {
                        return CheckPasswordIntemp(param.Parameter);
                    }
                case "GetMemberDetailsForOSD":
                    {
                        return GetMemberDetailsForOSD(param.Parameter);
                    }
                case "UpdateAdditionalDepatment":
                    {
                        return UpdateAdditionalDepatment(param.Parameter);
                    }
                case "ApproveAdditionalDepatment":
                    {
                        return ApproveAdditionalDepatment(param.Parameter);
                    }
                case "WrongAttemptAdminLoginData":
                    {
                        return WrongAttemptAdminLoginData(param.Parameter);
                    }
                case "GetIsMemberNameAndPhotoDetails":
                    {
                        return GetIsMemberNameAndPhotoDetails(param.Parameter);
                    }
                case "GetMemberCode_ForConUser":
                    {
                        return GetMemberCode_ForConUser(param.Parameter);
                    }

                case "GetConId_ForConUser":
                    {
                        return GetConId_ForConUser(param.Parameter);
                    }

                case "GetNodalOfficerList":
                    {
                        return GetNodalOfficerList(param.Parameter);
                    }
                case "GetSDMList":
                    {
                        return GetSDMList(param.Parameter);
                    }
                case "CheckAadhaarID":
                    {
                        return CheckAadhaarID(param.Parameter);
                    }
                case "get_AllSubdivision":
                    {
                        return get_AllSubdivision(param.Parameter);
                    }
            }
            return null;
        }


        static object GetDetailsOTPID(object param)
        {
            UserContext ctxt = new UserContext();
            mUsers model = param as mUsers;

            var query = (from mdl in ctxt.tOneTimeUserRegistration where mdl.UserId == model.OTPId select mdl).ToList();
            if (query != null)
            {
                return query;
            }
            return query;
        }

        static object WrongAttemptLoginData(object param)
        {
            mUsers model = param as mUsers;
            bool IsPasswordAthenticated = false;
            bool IsNotBlocked = false;
            string Message = "Login Failed,Invalid UserId or Password";
            DateTime CurrentTime = DateTime.Now;
            DateTime BlockUntilTime = DateTime.Now.AddMinutes(10);
            using (DBManager db = new DBManager())
            {

                var userByUsername = db.mUser.SingleOrDefault(a => a.AadarId == model.UserName);
                IsPasswordAthenticated = Convert.ToBoolean(model.IsAuthorized);

                if (userByUsername != null)
                {
                    Guid UserId = userByUsername.UserId;
                    userByUsername.MailID = "test13@gmail.com";
                    userByUsername.EmpOrMemberCode = "tet123";
                    userByUsername.OTPValue = "VTRF";
                    if (userByUsername.Mobile == null)
                    {
                        userByUsername.Mobile = "9999999999";
                    }

                    IsNotBlocked = (userByUsername.BlockUntil == null || CurrentTime > userByUsername.BlockUntil.Value) ? true : false;

                    if (!IsPasswordAthenticated && !IsNotBlocked)
                    {

                        return new LoginMessage() { IsValid = false, Message = "Login Failed.. You may try after 10 minutes" }; //Failed
                    }

                    if (!IsPasswordAthenticated)
                    {
                        //Need to update the wrong attempt fields
                        if (userByUsername.WrongAttemptCount == 0)
                        {
                            userByUsername.FirstWrongAttemptTime = CurrentTime;
                            userByUsername.WrongAttemptCount = 1;
                        }
                        else if (userByUsername.WrongAttemptCount == 1)
                        {
                            userByUsername.WrongAttemptCount = userByUsername.WrongAttemptCount + 1;
                        }
                        else if (userByUsername.WrongAttemptCount == 2)
                        {
                            TimeSpan TimeDifference = CurrentTime - userByUsername.FirstWrongAttemptTime.Value;
                            if (TimeDifference.Minutes <= 5)
                            {
                                Message = "Account is locked for 10 minutes..";
                                userByUsername.BlockUntil = BlockUntilTime;
                            }
                        }
                        db.mUser.Attach(userByUsername);
                        db.Entry(userByUsername).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();

                        return new LoginMessage() { IsValid = false, Message = Message }; //Failed
                    }
                    else
                    {
                        if (IsNotBlocked)
                        {
                            //For clear all 3 fields
                            userByUsername.FirstWrongAttemptTime = null;
                            userByUsername.WrongAttemptCount = 0;
                            userByUsername.BlockUntil = null;
                            db.mUser.Attach(userByUsername);
                            db.Entry(userByUsername).State = EntityState.Modified;
                            db.SaveChanges();
                            db.Close();

                            return new LoginMessage() { IsValid = true, Message = "Login success" }; //Success
                        }
                        else if (!IsNotBlocked)
                        {

                            return new LoginMessage()
                            {
                                IsValid = false,
                                Message = "Login Failed.. You may try after 10 minutes",
                            }; //Failed
                        }
                    }
                }

                return new LoginMessage() { IsValid = false, Message = "Login Failed,Invalid UserId or Password" }; //Failed
            }
        }
        static object WrongAttemptAdminLoginData(object param)
        {
            mUsers model = param as mUsers;
            bool IsPasswordAthenticated = false;
            bool IsNotBlocked = false;
            string Message = "Login Failed,Invalid UserId or Password";
            DateTime CurrentTime = DateTime.Now;
            DateTime BlockUntilTime = DateTime.Now.AddMinutes(10);
            using (DBManager db = new DBManager())
            {

                var userByUsername = db.mUser.SingleOrDefault(a => a.UserName == model.UserName);
                IsPasswordAthenticated = Convert.ToBoolean(model.IsAuthorized);

                if (userByUsername != null)
                {
                    Guid UserId = userByUsername.UserId;
                    userByUsername.MailID = "test13@gmail.com";
                    userByUsername.EmpOrMemberCode = "tet123";
                    userByUsername.OTPValue = "VTRF";
                    if (userByUsername.Mobile == null)
                    {
                        userByUsername.Mobile = "9999999999";
                    }

                    IsNotBlocked = (userByUsername.BlockUntil == null || CurrentTime > userByUsername.BlockUntil.Value) ? true : false;

                    if (!IsPasswordAthenticated && !IsNotBlocked)
                    {

                        return new LoginMessage() { IsValid = false, Message = "Login Failed.. You may try after 10 minutes" }; //Failed
                    }

                    if (!IsPasswordAthenticated)
                    {
                        //Need to update the wrong attempt fields
                        if (userByUsername.WrongAttemptCount == 0)
                        {
                            userByUsername.FirstWrongAttemptTime = CurrentTime;
                            userByUsername.WrongAttemptCount = 1;
                        }
                        else if (userByUsername.WrongAttemptCount == 1)
                        {
                            userByUsername.WrongAttemptCount = userByUsername.WrongAttemptCount + 1;
                        }
                        else if (userByUsername.WrongAttemptCount == 2)
                        {
                            TimeSpan TimeDifference = CurrentTime - userByUsername.FirstWrongAttemptTime.Value;
                            if (TimeDifference.Minutes <= 5)
                            {
                                Message = "Account is locked for 10 minutes..";
                                userByUsername.BlockUntil = BlockUntilTime;
                            }
                        }
                        db.mUser.Attach(userByUsername);
                        db.Entry(userByUsername).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();

                        return new LoginMessage() { IsValid = false, Message = Message }; //Failed
                    }
                    else
                    {
                        if (IsNotBlocked)
                        {
                            //For clear all 3 fields
                            userByUsername.FirstWrongAttemptTime = null;
                            userByUsername.WrongAttemptCount = 0;
                            userByUsername.BlockUntil = null;
                            db.mUser.Attach(userByUsername);
                            db.Entry(userByUsername).State = EntityState.Modified;
                            db.SaveChanges();
                            db.Close();

                            return new LoginMessage() { IsValid = true, Message = "Login success" }; //Success
                        }
                        else if (!IsNotBlocked)
                        {

                            return new LoginMessage()
                            {
                                IsValid = false,
                                Message = "Login Failed.. You may try after 10 minutes"
                            }; //Failed
                        }
                    }
                }

                return new LoginMessage() { IsValid = false, Message = "Login Failed,Invalid UserId or Password" }; //Failed
            }
        }

        private static object CheckOldPassword(object p)
        {
            mUsers userdetails = (mUsers)p;
            using (var ctx = new UserContext())
            {
                var result = (from userdet in ctx.mUsers where userdet.UserId == userdetails.UserId && userdet.Password == userdetails.Password select userdet).Count() > 0;

                return result;
            }
        }
        private static object CheckPasswordIntemp(object p)
        {
            mUsers userdetails = (mUsers)p;
            using (var ctx = new UserContext())
            {
                var result = (from userdet in ctx.mUsers
                              join temp in ctx.TempUser on userdet.UserId equals temp.userGuid
                              where userdet.AadarId == userdetails.AadarId
                              orderby temp.IdKey descending
                              select temp).FirstOrDefault();
                return result;
            }
        }
        private static object ChangePassword(object p)
        {
            mUsers userdetails = (mUsers)p;
            using (var ctx = new UserContext())
            {
                var result = (from userdet in ctx.mUsers where userdet.UserId == userdetails.UserId select userdet).FirstOrDefault();

                ctx.mUsers.Attach(result);

                result.Password = userdetails.Password;
                ctx.Configuration.ValidateOnSaveEnabled = false;
                ctx.SaveChanges();
            }

            using (var ctx = new UserContext())
            {

                var userTempdata = (from a in ctx.TempUser where a.userGuid == userdetails.UserId orderby a.IdKey descending select a).FirstOrDefault();

                if (userTempdata != null)
                {

                    ctx.TempUser.Attach(userTempdata);
                    userTempdata.password = userdetails.PasswordString;
                    ctx.Configuration.ValidateOnSaveEnabled = false;
                    ctx.SaveChanges();
                }
                else
                {

                    TempUser tempuserdata = new TempUser();
                    tempuserdata.UserId = userdetails.UserName;
                    tempuserdata.userGuid = userdetails.UserId;
                    tempuserdata.password = userdetails.PasswordString;
                    ctx.TempUser.Add(tempuserdata);
                    ctx.SaveChanges();
                }

            }

            return true;
        }

        private static object getDscDetailsByUserId(object p)
        {
            mUserDSHDetails data = (mUserDSHDetails)p;
            List<mUserDSHDetails> userdscList = new List<mUserDSHDetails>();
            using (var ctx = new UserContext())
            {

                userdscList = (from userdsclist in ctx.mUserDSHDetails where userdsclist.UserId == data.UserId && userdsclist.IsActive == true select userdsclist).ToList();
            }
            return userdscList;
        }

        private static object DeactivateUserDsc(object p)
        {
            mUserDSHDetails data = (mUserDSHDetails)p;

            List<mUserDSHDetails> userdscList2 = new List<mUserDSHDetails>();
            using (var ctx = new UserContext())
            {


                var userdscList = (from userdsclist in ctx.mUserDSHDetails where userdsclist.UserId == data.UserId select userdsclist).ToList();
                foreach (var item in userdscList)
                {

                    ctx.mUserDSHDetails.Attach(item);


                    item.IsActive = false;

                    ctx.SaveChanges();

                }
                // ctx.mUserDSHDetails.Attach(result);

                // result.IsActive = false;

                ctx.SaveChanges();
                // userdscList2 = (from userdsclist in ctx.mUserDSHDetails where userdsclist.UserId == data.UserId select userdsclist).ToList();

            }

            return userdscList2;
        }

        private static object ActivateUserDsc(object p)
        {
            mUserDSHDetails data = (mUserDSHDetails)p;

            List<mUserDSHDetails> userdscList2 = new List<mUserDSHDetails>();
            using (var ctx = new UserContext())
            {

                var userdscList = (from userdsclist in ctx.mUserDSHDetails where userdsclist.UserId == data.UserId select userdsclist).ToList();

                foreach (var item in userdscList)
                {

                    ctx.mUserDSHDetails.Attach(item);

                    if (item.IdKey == data.IdKey)
                    {
                        item.IsActive = true;
                    }
                    else
                    {
                        item.IsActive = false;
                    }
                    ctx.SaveChanges();

                }


                //userdscList2 = (from userdsclist in ctx.mUserDSHDetails where userdsclist.UserId == data.UserId select userdsclist).ToList();


            }

            return userdscList2;
        }

        private static object UpdateUserDetails(object p)
        {
            mUsers userdata = (mUsers)p;

            using (var ctx = new UserContext())
            {
                var data = (from user in ctx.mUsers where user.UserId == userdata.UserId select user).FirstOrDefault();
                ctx.mUsers.Attach(data);
                //var validationdata = ctx.GetValidationErrors();
                data.MobileNo = userdata.MobileNo;
                data.EmailId = userdata.EmailId;
                data.AadarId = userdata.AadarId;
                if (userdata.Photo != "")
                {
                    data.Photo = userdata.Photo;
                }
                if (userdata.DeptId != "HPD0005")
                {
                    data.DepartmentIDs = userdata.DepartmentIDs;
                    data.SecretoryId = userdata.SecretoryId;
                }
                ctx.Configuration.ValidateOnSaveEnabled = false;
                ctx.SaveChanges();
            }

            if (userdata.DSCHash != null && userdata.DSCHash != "")
            {
                using (var ctx = new UserContext())
                {
                    mUserDSHDetails dSCdetails = new mUserDSHDetails();

                    dSCdetails.EnrollDate = DateTime.Now;
                    dSCdetails.HashKey = userdata.DSCHash;
                    dSCdetails.Name = userdata.DSCName;
                    dSCdetails.Type = userdata.DSCType;
                    dSCdetails.UserId = userdata.UserId;
                    dSCdetails.Validity = DateTime.ParseExact(userdata.DSCValidity, "ddd MMM dd HH:mm:ss IST yyyy", null);
                    dSCdetails.IsActive = false;
                    ctx.mUserDSHDetails.Add(dSCdetails);
                    ctx.SaveChanges();
                }

            }

            var dscstatus = userdata.DscActiveStatus.Split(',');
            if (dscstatus[1] != "")
            {
                mUserDSHDetails data = new mUserDSHDetails();
                data.IdKey = Int32.Parse(dscstatus[1]);
                data.UserId = userdata.UserId;
                if (dscstatus[0] == "deactivate")
                {
                    DeactivateUserDsc(data);
                }
                else if (dscstatus[0] == "activate")
                {
                    ActivateUserDsc(data);
                }

                // ActivateUserDsc(object sd);
            }

            return true;

        }

        private static object UpdateUserNodalOffice(object p)
        {
            mUsers userdata = (mUsers)p;
            try
            {
                using (UserContext db = new UserContext())
                {
                    //var data = (from user in db.mUsers where user.UserId == userdata.UserId select user).FirstOrDefault();
                    //data.IsNodalOfficer = userdata.IsNodalOfficer;
                    if (string.IsNullOrEmpty(userdata.MailID))
                        userdata.MailID = userdata.EmailId;
                    if (string.IsNullOrEmpty(userdata.Mobile))
                        userdata.Mobile = userdata.MobileNo;
                    if (string.IsNullOrEmpty(userdata.OTPValue))
                        userdata.OTPValue = "123456";

                    db.mUsers.Attach(userdata);
                    db.Entry(userdata).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return true;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                //   throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);

                return false;
            }



        }


        private static object GetUserDetails(object parameter)
        {
            mUsers userdata = (mUsers)parameter;


            using (var ctx = new UserContext())
            {

                var userdetails = (from user in ctx.mUsers
                                   join emp in ctx.mEmployee on new { c = user.UserName, d = user.DeptId } equals new { c = emp.empcd, d = emp.deptid }
                                   join sec in ctx.mSecretory on user.SecretoryId equals sec.SecretoryID
                                   where
                                       user.UserName == userdata.UserName && user.DeptId == userdata.DeptId
                                   select new
                                   {

                                       EmpCode = user.UserName,
                                       Name = emp.empfname + emp.emplname + emp.empmname,
                                       Address = emp.emphmtown,
                                       SectoryName = sec.SecretoryName,
                                       Departments = user.DepartmentIDs,
                                       Mobile = user.MobileNo,
                                       Email = user.EmailId,
                                       AdharId = user.AadarId,
                                       FatherName = emp.empfmhname,
                                       Photo = user.Photo
                                   }).FirstOrDefault();
                //var userdetailers = (from user in ctx.mUsers //where user.UserId==userdata.UserId
                //                     select user
                //                   ).ToList();

                //var userdatasd = userdetails.Where(a => a.UserName == userdata.UserName && a.DeptId == userdata.DeptId).FirstOrDefault();

                //var empdata = (from emp in ctx.mEmployee
                //               where
                //                   emp.empcd == userdata.UserName && emp.deptid == userdata.DeptId
                //               select emp).FirstOrDefault();

                //var secdata = (from sec in ctx.mSecretory select sec).ToList();
                List<SecretoryDepartmentModel> secdepts = new List<SecretoryDepartmentModel>();
                List<mSecretory> userSeclst = new List<mSecretory>();

                var sectry = new mSecretory { SecretoryID = 0, SecretoryName = "Select Secretory" };

                userSeclst.Add(sectry);

                var department = new SecretoryDepartmentModel { DepartmentID = "0", DepartmentName = "Select Department", SecretoryId = 0 };

                secdepts.Add(department);

                if (userdata.DeptId != "HPD0005")
                {
                    secdepts = (from secDepts in ctx.mSecretoryDepartment
                                join dept in ctx.mDepartment on secDepts.DepartmentID equals dept.deptId
                                where
                                     secDepts.SecretoryID == userdata.SecretoryId
                                select new SecretoryDepartmentModel { DepartmentID = dept.deptId, DepartmentName = dept.deptname, SecretoryId = secDepts.SecretoryID }).ToList();

                    userSeclst = (from usersecs in ctx.mSecretory where usersecs.IsActive.Value select usersecs).ToList();

                    userdata.SecretoryDepartmentModel = secdepts;
                    userdata.mSecretory = userSeclst;
                    if (userdetails != null)
                    {
                        userdata.AllDepartmentIds = userdetails.Departments != null ? userdetails.Departments.Split(',').ToList() : new List<string>();
                    }
                }
                else
                {
                    userdata.SecretoryDepartmentModel = secdepts;
                    userdata.mSecretory = userSeclst;
                    userdata.AllDepartmentIds = new List<string>();
                }


                var userDSCLst = (from userDsc in ctx.mUserDSHDetails
                                  where userDsc.UserId == userdata.UserId
                                  select userDsc).ToList();

                //userdata.UserName = userdetails.EmpCode;
                if (userdetails != null)
                {
                    userdata.Name = userdetails.Name;
                    userdata.Address = userdetails.Address;
                    userdata.SecretoryName = userdetails.SectoryName;
                    userdata.DepartmentIDs = userdetails.Departments;
                    userdata.MobileNo = userdetails.Mobile;
                    userdata.EmailId = userdetails.Email;
                    userdata.AadarId = userdetails.AdharId;
                    userdata.SecretoryDepartmentModel = secdepts;
                    userdata.UserDscList = userDSCLst;
                    userdata.FatherName = userdetails.FatherName;
                    userdata.Photo = userdetails.Photo;
                    if (userDSCLst != null)
                    {
                        userdata.DscActiveStatus = userDSCLst.Where(a => a.IsActive).Count() > 0 ? "activate," + userDSCLst.Where(a => a.IsActive).Select(a => a.IdKey).First() : "deactivate,";
                    }
                    else
                    {
                        userdata.DscActiveStatus = "";
                    }
                }
                else
                {
                    userdata = null;
                }
                return userdata;
            }
        }

        private static object GetUserDetailsByUserTypeAndOfficeLevel(object parameter)
        {
            mUsers userdata = (mUsers)parameter;
            List<mUsers> result = new List<mUsers>();

            try
            {
                using (var ctx = new UserContext())
                {

                    var userdetails = (from user in ctx.mUsers
                                       //  join emp in ctx.mEmployee on new { c = user.UserName, d = user.DeptId } equals new { c = emp.empcd, d = emp.deptid }
                                       join sec in ctx.mSecretory on user.SecretoryId equals sec.SecretoryID
                                       where user.UserType == userdata.UserType && user.OfficeLevel == userdata.OfficeLevel
                                       select new
                                       {
                                           UserId = user.UserId,
                                           UserName = user.UserName,
                                           Address = user.Address,
                                           SecretoryName = sec.SecretoryName,
                                           DeptId = user.DeptId,
                                           MobileNo = user.MobileNo,
                                           EmailId = user.EmailId,
                                           AadarId = user.AadarId,


                                       }).ToList();

                    foreach (var item in userdetails)
                    {
                        mUsers obj = new mUsers();
                        obj.UserId = item.UserId;
                        obj.UserName = item.UserName;
                        obj.Address = item.Address;
                        obj.SecretoryName = item.SecretoryName;
                        obj.DeptId = item.DeptId;
                        obj.MobileNo = item.MobileNo;
                        obj.EmailId = item.EmailId;
                        obj.AadarId = item.AadarId;
                        string[] deprtIds = (!string.IsNullOrEmpty(item.DeptId) ? item.DeptId.Split(',') : new string[0]);
                        var DeprtName = (from Dept in ctx.mDepartment
                                         where (deprtIds).Contains(Dept.deptId)
                                         select new { Dept.deptname }).ToList().ToArray();

                        foreach (var s in DeprtName)
                        {
                            obj.DepartmentName = obj.DepartmentName + s.deptname + ",";
                        }
                        result.Add(obj);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }



            return result;

        }

        static object UpdatedMemberDetails(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            ConstituencyContext context = new ConstituencyContext();
            var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            //var queryaadhar = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            var queryaadhar = (from adhar in uCtxt.AdhaarDetails where adhar.AdhaarID == model.AadarId select adhar).SingleOrDefault();

            if (query != null)
            {
                query.IsMember = model.IsMember;
                query.MobileNo = model.MobileNo;
                query.EmailId = model.EmailId;
                query.Photo = model.Photo;
                query.UserName = model.UserName;
                query.DOB = model.DOB;
                query.OfficeLevel = model.OfficeLevel;
                query.DeptId = model.DeptId;
                query.DepartmentIDs = model.DepartmentIDs;
                query.OfficeId = model.OfficeId;
                query.IsSecretoryId = model.IsSecretoryId;
                query.IsOther = model.IsOther;
                query.IsHOD = model.IsHOD;
                query.FatherName = model.FatherName;
                query.Gender = model.Gender;
                query.Name = model.UserName;
                query.IsMedia = model.IsMedia;
                query.UserType = model.UserType;
                query.SecretoryId = model.SecretoryId;
                query.IsNodalOfficer = model.IsNodalOfficer;
                if (model.SignaturePath != null)
                {
                    query.SignaturePath = model.SignaturePath;
                }
                query.Designation = model.Designation;
                query.Address = model.Address;
                query.IsNotification = model.IsNotification;
                if (model.AllDepartmentIds.Count > 0)
                {
                    query.SecretoryId = model.SecretoryId;
                    query.DepartmentIDs = model.DepartmentIDs;
                }
                if (model.DSCHash != null)
                { query.IsDSCAuthorized = true; }
                else if (query.IsDSCAuthorized == true)
                { }
                else
                {
                    query.IsDSCAuthorized = false;
                }
                query.DistrictCode = model.DistrictCode;
                query.ConstituencyId = model.ConstituencyId;
                query.SubdivisionCode = model.SubdivisionCode;
                query.PanchyatCode = model.PanchyatCode;
                var currentstate = (from site in context.SiteSettings
                                    where site.SettingName == "StateCode"
                                    select site.SettingValue).FirstOrDefault();
                int statecode = Convert.ToInt32(currentstate);
                query.stateCode = statecode;
            }
            if (queryaadhar != null)
            {
                if (model.IsMember == "False")
                {
                    queryaadhar.Name = model.UserName;
                }
                else
                {
                    queryaadhar.Name = model.Name;
                }
                queryaadhar.FatherName = model.FatherName;
                queryaadhar.MobileNo = model.MobileNo;
                queryaadhar.Address = model.Address;
                queryaadhar.Email = model.EmailId;
                queryaadhar.Gender = model.Gender;
                queryaadhar.DOB = model.DOB;

            }
            uCtxt.Configuration.ValidateOnSaveEnabled = false;
            uCtxt.SaveChanges();

            if (model.DSCHash != null)
            {
                var dsc = (from ds in uCtxt.mUserDSHDetails where ds.UserId == model.UserId && ds.IsActive == true select ds).SingleOrDefault();
                if (dsc != null)
                {
                    dsc.IsActive = false;
                }
                uCtxt.SaveChanges();
                mUserDSHDetails dscDetials = new mUserDSHDetails();
                dscDetials.UserId = model.UserId;
                dscDetials.Name = model.DSCName;
                dscDetials.Type = model.DSCType;
                dscDetials.HashKey = model.DSCHash;
                dscDetials.IsActive = true;
                if (model.DSCValidity != null)
                {
                    dscDetials.Validity = DateTime.ParseExact(model.DSCValidity, "ddd MMM dd HH:mm:ss IST yyyy", null);
                }
                dscDetials.EnrollDate = DateTime.Now;
                uCtxt.mUserDSHDetails.Add(dscDetials);
                uCtxt.SaveChanges();
            }
            uCtxt.Close();
            if (model.DscActiveStatus != null)
            {
                var dscstatus = model.DscActiveStatus.Split(',');
                if (dscstatus[1] != "")
                {
                    mUserDSHDetails data = new mUserDSHDetails();
                    data.IdKey = Int32.Parse(dscstatus[1]);
                    data.UserId = model.UserId;
                    if (dscstatus[0] == "deactivate")
                    {
                        DeactivateUserDsc(data);
                    }
                    else if (dscstatus[0] == "activate")
                    {
                        ActivateUserDsc(data);
                    }
                }
            }
            return null;
        }

        //static object UpdatedMemberDetails(object param)
        //{
        //    mUsers model = param as mUsers;
        //    UserContext uCtxt = new UserContext();
        //    var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
        //    if (query != null)
        //    {
        //        query.IsMember = model.IsMember;
        //        query.MobileNo = model.MobileNo;
        //        query.EmailId = model.EmailId;
        //        query.Photo = model.Photo;
        //        query.UserName = model.UserName;
        //        query.OfficeLevel = model.OfficeLevel;
        //        query.DeptId = model.DeptId;
        //        query.DepartmentIDs = model.DepartmentIDs;
        //        query.OfficeId = model.OfficeId;
        //        query.IsSecretoryId = model.IsSecretoryId;
        //        query.IsOther = model.IsOther;
        //        query.IsHOD = model.IsHOD;
        //        query.FatherName = model.FatherName;
        //        query.Gender = model.Gender;
        //        query.Name = model.UserName;
        //        query.IsMedia = model.IsMedia;
        //        query.UserType = model.UserType;
        //        query.SecretoryId = model.SecretoryId;
        //        query.IsNodalOfficer = model.IsNodalOfficer;
        //        if (model.SignaturePath != null)
        //        {
        //            query.SignaturePath = model.SignaturePath;
        //        }
        //        query.Designation = model.Designation;
        //        query.Address = model.Address;
        //        query.IsNotification = model.IsNotification;
        //        if (model.AllDepartmentIds.Count > 0)
        //        {
        //            query.SecretoryId = model.SecretoryId;
        //            query.DepartmentIDs = model.DepartmentIDs;
        //        }
        //        if (model.DSCHash != null)
        //        { query.IsDSCAuthorized = true; }
        //        else if (query.IsDSCAuthorized == true)
        //        { }
        //        else
        //        {
        //            query.IsDSCAuthorized = false;
        //        }
        //    }

        //    uCtxt.Configuration.ValidateOnSaveEnabled = false;
        //    uCtxt.SaveChanges();

        //    if (model.DSCHash != null)
        //    {
        //        var dsc = (from ds in uCtxt.mUserDSHDetails where ds.UserId == model.UserId && ds.IsActive == true select ds).SingleOrDefault();
        //        if (dsc != null)
        //        {
        //            dsc.IsActive = false;
        //        }
        //        uCtxt.SaveChanges();
        //        mUserDSHDetails dscDetials = new mUserDSHDetails();
        //        dscDetials.UserId = model.UserId;
        //        dscDetials.Name = model.DSCName;
        //        dscDetials.Type = model.DSCType;
        //        dscDetials.HashKey = model.DSCHash;
        //        dscDetials.IsActive = true;
        //        if (model.DSCValidity != null)
        //        {
        //            dscDetials.Validity = DateTime.ParseExact(model.DSCValidity, "ddd MMM dd HH:mm:ss IST yyyy", null);
        //        }
        //        dscDetials.EnrollDate = DateTime.Now;
        //        uCtxt.mUserDSHDetails.Add(dscDetials);
        //        uCtxt.SaveChanges();
        //    }
        //    uCtxt.Close();
        //    if (model.DscActiveStatus != null)
        //    {
        //        var dscstatus = model.DscActiveStatus.Split(',');
        //        if (dscstatus[1] != "")
        //        {
        //            mUserDSHDetails data = new mUserDSHDetails();
        //            data.IdKey = Int32.Parse(dscstatus[1]);
        //            data.UserId = model.UserId;
        //            if (dscstatus[0] == "deactivate")
        //            {
        //                DeactivateUserDsc(data);
        //            }
        //            else if (dscstatus[0] == "activate")
        //            {
        //                ActivateUserDsc(data);
        //            }
        //        }
        //    }
        //    return null;
        //}

        static object UpdatedMemberDetailsReporters(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            if (query != null)
            {
                query.MobileNo = model.MobileNo;
                query.EmailId = model.EmailId;
                query.Photo = model.Photo;
                if (model.SignaturePath != null)
                {
                    query.SignaturePath = model.SignaturePath;
                }
                query.Designation = model.Designation;
                query.Address = model.Address;
                query.IsNotification = model.IsNotification;
                if (model.AllDepartmentIds.Count > 0)
                {
                    query.SecretoryId = model.SecretoryId;
                    query.DepartmentIDs = model.DepartmentIDs;
                }
                if (model.DSCHash != null)
                { query.IsDSCAuthorized = true; }
                else if (query.IsDSCAuthorized == true)
                { }
                else
                {
                    query.IsDSCAuthorized = false;
                }
            }

            uCtxt.Configuration.ValidateOnSaveEnabled = false;
            uCtxt.SaveChanges();


            var journalistUpdate = (from mdl in uCtxt.mJournalist where mdl.UserID == model.UserId select mdl).SingleOrDefault();
            if (journalistUpdate != null)
            {
                journalistUpdate.Phone = model.MobileNo;
                journalistUpdate.Email = model.EmailId;
                journalistUpdate.Photo = model.Photo;

                journalistUpdate.Designation = model.Designation;
                journalistUpdate.Address = model.Address;


                if (model.DSCHash != null)
                { query.IsDSCAuthorized = true; }
                else if (query.IsDSCAuthorized == true)
                { }
                else
                {
                    query.IsDSCAuthorized = false;
                }
            }

            uCtxt.Configuration.ValidateOnSaveEnabled = false;
            uCtxt.SaveChanges();

            uCtxt.Close();

            return null;
        }

        static object AddTempUSerData(object param)
        {
            TempUser user = param as TempUser;
            using (var model = new UserContext())
            {
                model.TempUser.Add(user);
                model.SaveChanges();
                model.Close();
            }
            return user;
        }

        static object AddUserDetails(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            using (var obj = new UserContext())
            {
                obj.mUsers.Add(model);
                obj.SaveChanges();
                obj.Close();
                model.UserId = model.UserId;
            }

            return model;
        }

        static object ADDUserDSCDetails(object param)
        {
            mUserDSHDetails mdl = param as mUserDSHDetails;

            using (var model = new UserContext())
            {
                model.mUserDSHDetails.Add(mdl);
                model.SaveChanges();
                model.Close();
                mdl.IdKey = mdl.IdKey;
            }
            return mdl;
        }

        static object GetRegisteredDetails(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            if (model.OfficeLevel == "2")
            {
                model = (from mdl in uCtxt.mUsers where mdl.UserName == model.UserName && mdl.DeptId == model.DeptId select mdl).FirstOrDefault();
            }
            else
            {
                model = (from mdl in uCtxt.mUsers where mdl.UserName == model.UserName select mdl).FirstOrDefault();

            }

            return model;
        }

        static object GetSettings(object param)
        {

            UserContext SCtx = new UserContext();
            mSiteSettingsVS s = param as mSiteSettingsVS;
            var session = (from m in SCtx.mSiteSettingsVS
                           select m).ToList();


            return session;
        }

        static object GetSettingSettingName(object param)
        {

            UserContext SCtx = new UserContext();
            mSiteSettingsVS SiteSetting = param as mSiteSettingsVS;
            var session = (from m in SCtx.mSiteSettingsVS
                           where m.SettingName == SiteSetting.SettingName
                           select m).FirstOrDefault();


            return session;
        }
        public static object UpdateSiteSettings(object param)
        {
            try
            {
                using (UserContext db = new UserContext())
                {
                    mSiteSettingsVS model = param as mSiteSettingsVS;
                    db.mSiteSettingsVS.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        static object GetIsMemberNameAndPhotoDetails(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            var QueryResult = (from mdl in uCtxt.mUsers
                               where mdl.UserId == model.UserId
                               select new
                               {
                                   UserName = mdl.UserName,
                                   Photo = mdl.Photo,
                                   IsMember = mdl.IsMember,
                                   IsMedia = mdl.IsMedia,
                                   DeptId = mdl.DeptId
                               }).FirstOrDefault();

            if (QueryResult != null)
            {

                if (QueryResult.IsMember.ToUpper() == "TRUE")
                {
                    int MemberID = Convert.ToInt16(QueryResult.UserName);
                    var InnnerResult = (from m in uCtxt.mMembers where m.MemberCode == MemberID select m.Name).FirstOrDefault();
                    if (InnnerResult != null && InnnerResult != "")
                    {
                        model.Name = InnnerResult;
                    }
                }
                else if (model.IsMedia == "true")
                {
                    Journalist query = new Journalist();
                    if (model.UserName != "1001")
                    {
                        query = (from mdl1 in uCtxt.mJournalist where mdl1.UID == model.UserName select mdl1).SingleOrDefault();
                    }
                    else
                    {
                        int JCode = Convert.ToInt32(model.UserName);
                        query = (from mdl1 in uCtxt.mJournalist where mdl1.JournalistCode == JCode select mdl1).SingleOrDefault();
                    }
                    if (query != null)
                    {
                        model.Name = query.Name;
                        if (query.FatherName != null)
                        {
                            model.FatherName = query.FatherName;
                        }
                        else
                        { model.FatherName = "Not Available"; }
                        if (query.Gender != null)
                        {
                            model.Gender = query.Gender;
                        }
                        else
                        {
                            model.Gender = "Not Available";

                        }
                    }
                }
                else
                {
                    int num;
                    bool isNum = Int32.TryParse(QueryResult.UserName.Trim(), out num);
                    if (isNum)
                    {
                        var query = (from mdl1 in uCtxt.mEmployee where mdl1.empcd == QueryResult.UserName && mdl1.deptid == QueryResult.DeptId select mdl1).FirstOrDefault();
                        if (query != null)
                        {
                            if (query.empgender == "F")
                            {
                                model.Name = "Smt. " + query.empfname + " " + query.empmname + " " + query.emplname;
                                model.Gender = "Female";
                            }
                            else if (query.empgender == "M")
                            {
                                model.Name = "Shri. " + query.empfname + " " + query.empmname + " " + query.emplname;
                                model.Gender = "Male";
                            }

                            model.FatherName = query.empfmhname;
                        }
                    }
                    else
                    {
                        model.Name = QueryResult.UserName;
                    }

                }
                model.Photo = QueryResult.Photo;



            }
            return model;
        }

        static object GetIsMemberDetails(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            model = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).FirstOrDefault();
            if (model != null)
            {
                if (model.IsMember.ToUpper() == "TRUE")
                {

                    //int memberCode = Convert.ToInt32(model.UserName);
                    // var query = (from mdl1 in uCtxt.mMembers where mdl1.MemberCode == memberCode select mdl1).SingleOrDefault();
                    var getDSC = (from dsc in uCtxt.mUserDSHDetails where dsc.UserId == model.UserId select dsc).ToList();
                    //if (query != null)
                    //{
                    //    model.Name = query.Name;
                    //    if (query.Sex != null)
                    //    {
                    //        model.Gender = query.Sex;
                    //    }
                    //    else
                    //    {
                    //        model.Gender = "Not Available";
                    //    }
                    //    if (query.FatherName != null)
                    //    {
                    //        model.FatherName = query.FatherName;
                    //    }
                    //    else
                    //    { model.FatherName = "Not Available"; }
                    //}
                    if (getDSC != null)
                    {
                        if (getDSC.Count > 0)
                        {
                            model.UserDscList = getDSC;
                        }
                    }
                }
                else if (model.IsMedia == "true")
                {
                    Journalist query = new Journalist();
                    if (model.UserName != "1001")
                    {
                        query = (from mdl1 in uCtxt.mJournalist where mdl1.UID == model.UserName select mdl1).SingleOrDefault();
                    }
                    else
                    {
                        int JCode = Convert.ToInt32(model.UserName);
                        query = (from mdl1 in uCtxt.mJournalist where mdl1.JournalistCode == JCode select mdl1).SingleOrDefault();
                    }
                    var getDSC = (from dsc in uCtxt.mUserDSHDetails where dsc.UserId == model.UserId select dsc).ToList();
                    if (query != null)
                    {
                        model.Name = query.Name;
                        if (query.FatherName != null)
                        {
                            model.FatherName = query.FatherName;
                        }
                        else
                        { model.FatherName = "Not Available"; }
                        if (query.Gender != null)
                        {
                            model.Gender = query.Gender;
                        }
                        else
                        {
                            model.Gender = "Not Available";

                        }
                    }
                    if (getDSC != null)
                    {
                        if (getDSC.Count > 0)
                        {
                            model.UserDscList = getDSC;
                        }
                    }
                }
                else
                {
                    var mUser = (from user in uCtxt.mUsers where user.UserId == model.UserId select user).SingleOrDefault();
                    var query = (from mdl1 in uCtxt.mEmployee where mdl1.empcd == model.UserName && mdl1.deptid == mUser.DeptId select mdl1).SingleOrDefault();
                    var getDSC = (from dsc in uCtxt.mUserDSHDetails where dsc.UserId == model.UserId select dsc).ToList();
                    if (query != null)
                    {
                        if (query.empgender == "F")
                        {
                            model.Name = "Smt. " + query.empfname + " " + query.empmname + " " + query.emplname;
                            model.Gender = "Female";
                        }
                        else if (query.empgender == "M")
                        {
                            model.Name = "Shri. " + query.empfname + " " + query.empmname + " " + query.emplname;
                            model.Gender = "Male";
                        }

                        model.FatherName = query.empfmhname;
                    }
                    if (getDSC != null)
                    {
                        if (getDSC.Count > 0)
                        {
                            model.UserDscList = getDSC;
                        }
                    }
                }
                var adq = (from adhar in uCtxt.AdhaarDetails where adhar.AdhaarID == model.AadarId select adhar).SingleOrDefault();
                if (adq != null)
                {
                    model.Name = adq.Name;
                    model.FatherName = adq.FatherName;
                }
            }




            return model;

        }

        static object SetSettings(object param)
        {
            SiteSettingsModel model = param as SiteSettingsModel;
            var vals = 0;
            if (model.AssemblyId != null && model.AssemblyId != "")
            {
                using (var obj = new UserContext())
                {
                    var Obj = obj.mSiteSettingsVS.Where(m => m.SettingName == "Assembly").FirstOrDefault();
                    if (Obj != null)
                    {
                        Obj.SettingValue = model.AssemblyId;
                        vals = obj.SaveChanges();
                        obj.Close();
                    }
                    else
                    {
                        mSiteSettingsVS SiteSettingsVS = new mSiteSettingsVS();
                        SiteSettingsVS.SettingName = "Assembly";
                        SiteSettingsVS.SettingValue = model.AssemblyId;
                        SiteSettingsVS.IsActive = true;
                        obj.mSiteSettingsVS.Add(SiteSettingsVS);
                        vals = obj.SaveChanges();
                        obj.Close();
                    }
                }
            }
            if (model.SessionId != null && model.SessionId != "")
            {
                using (var obj = new UserContext())
                {
                    var Obj = obj.mSiteSettingsVS.Where(m => m.SettingName == "Session").FirstOrDefault();
                    if (Obj != null)
                    {
                        Obj.SettingValue = model.SessionId;
                        vals = obj.SaveChanges();
                        obj.Close();
                    }
                    else
                    {
                        mSiteSettingsVS SiteSettingsVS = new mSiteSettingsVS();
                        SiteSettingsVS.SettingName = "Session";
                        SiteSettingsVS.SettingValue = model.SessionId;
                        SiteSettingsVS.IsActive = true;
                        obj.mSiteSettingsVS.Add(SiteSettingsVS);
                        vals = obj.SaveChanges();
                        obj.Close();
                    }
                }
            }
            return vals;
        }

        //Nitin Code
        public static mUserDSHDetails GetDSCDetailsByHashKey(object param)
        {
            UserContext uCtxt = new UserContext();
            mUserDSHDetails model = param as mUserDSHDetails;
            var query = (from mdl in uCtxt.mUserDSHDetails where mdl.HashKey == model.HashKey select mdl).FirstOrDefault();
            if (query != null)
            {
                return query;
            }
            return null;
        }

        public static mUsers CheckAdhaarID(object param)
        {
            UserContext uCtxt = new UserContext();
            mUsers model = param as mUsers;
            var query = (from mdl in uCtxt.mUsers where mdl.AadarId == model.AadarId select mdl).FirstOrDefault();
            if (query != null)
            {
                return query;
            }
            return query;
        }

        static object GetDetailsByadhaarID(object param)
        {
            UserContext ctxt = new UserContext();
            mUsers model = param as mUsers;

            var query = (from mdl in ctxt.mUsers where mdl.AadarId == model.AadarId select mdl).ToList();
            if (query != null)
            {
                return query;
            }
            return query;
        }
        static string GetMemberCode_ForConUser(object param)
        {
            UserContext ctxt = new UserContext();

            mUsers model = param as mUsers;

            var query = (from mdl in ctxt.tGrievanceOfficerDeleted
                         where mdl.AdharId == model.AadarId
                         select mdl.MemberCode).FirstOrDefault();
            if (query != null)
            {
                return query.ToString();
            }
            return query.ToString();
        }

        static string GetConId_ForConUser(object param)
        {
            UserContext ctxt = new UserContext();
            string[] str = param as string[];
            // string[] strOutput = new string[2];
            int Officeid = Convert.ToInt32(str[0]);
            int Memcode = Convert.ToInt32(str[1]);
            var currentAssembly = (from site in ctxt.mSiteSettingsVS
                                   where site.SettingName == "Assembly"
                                select site.SettingValue).FirstOrDefault();
            int asID = Convert.ToInt32(currentAssembly);
            //int asID = Convert.ToInt32(str[2]);
            string ConID = "";
            var query = (from mdl in ctxt.tDepartmentOfficeMapped
                         where mdl.MemberCode == Memcode && mdl.OfficeId == Officeid
                         select mdl.ConstituencyCode).FirstOrDefault();
            if (query != null)
            {
                int concode = Convert.ToInt16(query);
                var query1 = (from mdl1 in ctxt.mConstituency
                              where mdl1.AssemblyID == asID && mdl1.ConstituencyCode == concode
                              select mdl1.ConstituencyID).FirstOrDefault();
                ConID = query1.ToString();
            }
            return ConID;
        }



        static object GetUser_DetailsByAdhaarID(object param)
        {
            UserContext ctxt = new UserContext();
            AdhaarDetails model = param as AdhaarDetails;

            var query = (from adhr in ctxt.AdhaarDetails where adhr.AdhaarID == model.AdhaarID select adhr).FirstOrDefault();

            if (query != null)
            {
                return query;
            }
            return query;
        }

        static object AddAadharDetails(object param)
        {
            try
            {
                AdhaarDetails model = param as AdhaarDetails;
                UserContext uCtxt = new UserContext();
                using (var obj = new UserContext())
                {
                    obj.AdhaarDetails.Add(model);
                    obj.SaveChanges();
                    obj.Close();
                }
                return model;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }



        }

        #region Forget password

        static object GetForgetPassword(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var query = (from mdl in ctxt.mUsers where mdl.AadarId == model.AadarId select mdl).FirstOrDefault();
            if (query != null)
            {
                return query;
            }
            return query;
        }

        #endregion

        #region Employee Authorisation

        static object GetUserDetailsBySecID(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();

            var query = (from users in ctxt.mUsers
                         join mDepartments in ctxt.mDepartment on users.DeptId equals mDepartments.deptId
                         // join authEmployee in ctxt.AuthorisedEmployee on users.UserId 
                         where (users.SecretoryId == model.SecretoryId)
                         && (users.IsSecretoryId != true)
                         && (users.IsAuthorized == false)
                         && !(from authEmp in ctxt.AuthorisedEmployee where authEmp.UserId == users.UserId && authEmp.isAuthorized == true select authEmp.UserId).Contains(users.UserId)
                         select new mUserModelClass
                         {
                             //Name = "",
                             EmpOrMemberCode = users.UserName,
                             DepartmentIDs = mDepartments.deptId,
                             DeptId = mDepartments.deptname,
                             UserId = users.UserId,
                             IsAuthorized = users.IsAuthorized,
                             Permission = users.Permission,
                             Photo = users.Photo,
                             AdhaarID = users.AadarId
                         }).ToList();

            foreach (var item in query)
            {
                item.Name = (from adhar in ctxt.AdhaarDetails where adhar.AdhaarID == item.AdhaarID select adhar.Name).FirstOrDefault();
            }

            return query;
        }
        static object GetAuthorizedUserDetailsBySecID(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var query = (from users in ctxt.mUsers
                         join mDepartments in ctxt.mDepartment on users.DeptId equals mDepartments.deptId
                         join authEmployee in ctxt.AuthorisedEmployee on users.UserId equals authEmployee.UserId
                         where users.SecretoryId == model.SecretoryId && authEmployee.isAuthorized == true
                         && (users.IsSecretoryId != true)
                         select new mUserModelClass
                         {
                             //Name = "",
                             EmpOrMemberCode = users.UserName,
                             DepartmentIDs = mDepartments.deptId,
                             DeptId = mDepartments.deptname,
                             UserId = users.UserId,
                             IsAuthorized = authEmployee.isAuthorized,
                             Permission = users.Permission,
                             Photo = users.Photo,
                             AdhaarID = users.AadarId
                         }).ToList();
            foreach (var item in query)
            {
                item.Name = (from adhar in ctxt.AdhaarDetails where adhar.AdhaarID == item.AdhaarID select adhar.Name).FirstOrDefault();
            }
            return query;
        }
        static object AuthrorizedEmployee(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var query = (from users in ctxt.mUsers
                         where users.UserId == model.UserId
                         select users).SingleOrDefault();
            if (query != null)
            {
                query.IsAuthorized = true;
            }
            ctxt.Configuration.ValidateOnSaveEnabled = false;
            ctxt.SaveChanges();
            ctxt.Close();
            return null;
        }

        static object UnAuthrorizedEmployee(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var query = (from authEmployee in ctxt.AuthorisedEmployee
                         where authEmployee.UserId == model.UserId
                         select authEmployee).SingleOrDefault();
            if (query != null)
            {
                ctxt.AuthorisedEmployee.Remove(query);
            }
            //ctxt.Configuration.ValidateOnSaveEnabled = false;
            ctxt.SaveChanges();


            var queryRole = (from authEmployee in ctxt.tUserRoles
                             where authEmployee.UserID == model.UserId
                             select authEmployee).SingleOrDefault();

            if (queryRole != null)
            {
                ctxt.tUserRoles.Remove(queryRole);
            }
            ctxt.SaveChanges();

            ctxt.Close();

            return null;
        }

        static object SetUserPermission(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var query = (from users in ctxt.mUsers
                         where users.UserId == model.UserId
                         select users).SingleOrDefault();
            if (query != null)
            {
                query.Permission = model.Permission;
            }
            ctxt.Configuration.ValidateOnSaveEnabled = false;
            ctxt.SaveChanges();
            ctxt.Close();
            return null;
        }
        static object AssignRoles(object param)
        {
            tUserRoles model = param as tUserRoles;
            UserContext ctxt = new UserContext();
            var query = (from usersRoles in ctxt.tUserRoles
                         where usersRoles.UserID == model.UserID && usersRoles.Roleid == model.Roleid
                         select usersRoles).SingleOrDefault();
            if (query != null)
            {
                return null;
            }
            else
            {

                ctxt.tUserRoles.Add(model);
                ctxt.SaveChanges();
                ctxt.Close();
            }

            return null;
        }
        static object RemoveEmployeeByUSerID(object param)
        {
            AuthorisedEmployee model = param as AuthorisedEmployee;
            UserContext ctxt = new UserContext();
            var EmpQuery = (from authEmployee in ctxt.AuthorisedEmployee
                            where authEmployee.UserId == model.UserId
                            select authEmployee).SingleOrDefault();
            if (EmpQuery != null)
            {
                ctxt.AuthorisedEmployee.Remove(EmpQuery);

            }
            var UserQuery = (from users in ctxt.mUsers
                             where users.UserId == model.UserId
                             select users).SingleOrDefault();
            if (UserQuery != null)
            {
                UserQuery.SecretoryId = null;
                UserQuery.DepartmentIDs = null;
            }
            ctxt.Configuration.ValidateOnSaveEnabled = false;
            ctxt.SaveChanges();
            ctxt.Close();
            return null;
        }
        static object GetUserDetailsByUserID(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var adharid = (from users in ctxt.mUsers where users.UserId == model.UserId select users).SingleOrDefault();
            var adq = (from adhar in ctxt.AdhaarDetails where adhar.AdhaarID == adharid.AadarId select adhar).SingleOrDefault();

            var query = (from users in ctxt.mUsers
                         where users.UserId == model.UserId
                         select users).SingleOrDefault();
            if(query.UserType ==null)
            {
                query.UserType = 0;

            }
            if (adq != null)
            {
                query.Name = adq.Name;
                query.FatherName = adq.FatherName;
            }
            return query;
        }

        static object GetUserDetailsByUserIDSMS(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var adharid = (from users in ctxt.mUsers where users.UserId == model.UserId select users).SingleOrDefault();
            var adq = (from adhar in ctxt.AdhaarDetails where adhar.AdhaarID == adharid.AadarId select adhar).SingleOrDefault();

            var query = (from users in ctxt.mUsers
                         where users.UserId == model.UserId
                         select users).SingleOrDefault();
            if (adq != null)
            {
                query.Name = adq.Name;
                query.FatherName = adq.FatherName;
            }
            return query;
        }

        static object GetUserDetailsBySecretoryID(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var adharid = (from users in ctxt.mUsers where users.SecretoryId == model.SecretoryId && users.IsSecretoryId == true select users).FirstOrDefault();

            return adharid;
        }

        static object GetUserDeptDetailsByUserID(object param)
        {
            mUsers model = param as mUsers;
            UserContext ctxt = new UserContext();
            var adharid = (from users in ctxt.mUsers where users.UserId == model.UserId select users).SingleOrDefault();

            return adharid;
        }
        #endregion

        //By Ram
        static object DeleteUserByUserID(object param)
        {
            mUsers Parameter = param as mUsers;
            try
            {
                using (UserContext db = new UserContext())
                {
                    var UserToDelete = db.mUsers.SingleOrDefault(a => a.UserId == Parameter.UserId);
                    db.mUsers.Remove(UserToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
            return null;
        }
        #region Hashing Password

        //static bool VerifyHash(string plainText, string hashAlgorithem, string hashValue)
        //{
        //    byte[] hashWithSaltBytes = Convert.FromBase64String(hashValue);

        //    int hashSizeInBits, hashSizeInBytes;

        //    if (hashAlgorithem == null)
        //        hashAlgorithem = "";

        //    switch (hashAlgorithem.ToUpper())
        //    {
        //        case "SHA384":
        //            hashSizeInBits = 384;
        //            break;

        //        case "SHA512":
        //            hashSizeInBits = 512;
        //            break;

        //        default: // Must be MD5
        //            hashSizeInBits = 128;
        //            break;
        //    }

        //    hashSizeInBytes = hashSizeInBits / 8;

        //    if (hashWithSaltBytes.Length < hashSizeInBytes)
        //    {
        //        return false;
        //    }

        //    byte[] saltBytes = new byte[hashWithSaltBytes.Length - hashSizeInBytes];

        //    for (int i = 0; i < saltBytes.Length; i++)
        //    {
        //        saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];
        //    }

        //    string expectedHashString = ComputeHash(plainText, hashAlgorithem, saltBytes);

        //    return (hashValue == expectedHashString);
        //}

        //static string ComputeHash(string plainText, string hashAlgorithem, byte[] saltBytes)
        //{
        //    //if Salt is not specified, Generate it
        //    if (saltBytes == null)
        //    {
        //        int minSaltSize = 4;
        //        int maxSaltSize = 8;

        //        Random random = new Random();
        //        int saltSize = random.Next(minSaltSize, maxSaltSize);

        //        saltBytes = new byte[saltSize];

        //        RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
        //        rng.GetNonZeroBytes(saltBytes);
        //    }

        //    byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        //    byte[] plainTextWithSaltBytes = new byte[plainTextBytes.Length + saltBytes.Length];

        //    for (int i = 0; i < plainTextBytes.Length; i++)
        //    {
        //        plainTextWithSaltBytes[i] = plainTextBytes[i];
        //    }

        //    for (int i = 0; i < saltBytes.Length; i++)
        //    {
        //        plainTextWithSaltBytes[plainTextBytes.Length + 1] = saltBytes[i];
        //    }

        //    HashAlgorithm hash;

        //    if (hashAlgorithem == null)
        //        hashAlgorithem = "";

        //    switch (hashAlgorithem.ToUpper())
        //    {
        //        case "SHA384":
        //            hash = new SHA384Managed();
        //            break;

        //        case "SHA512":
        //            hash = new SHA512Managed();
        //            break;

        //        default:
        //            hash = new MD5CryptoServiceProvider();
        //            break;
        //    }

        //    byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

        //    byte[] hashWithSaltBytes = new byte[hashBytes.Length + saltBytes.Length];

        //    for (int i = 0; i < hashBytes.Length; i++)
        //    {
        //        hashWithSaltBytes[i] = hashBytes[i];
        //    }

        //    for (int i = 0; i < saltBytes.Length; i++)
        //    {
        //        hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];
        //    }

        //    string hashValue = Convert.ToBase64String(hashWithSaltBytes);

        //    return hashValue;
        //}

        // The following constants may be changed without breaking existing hashes.
        public const int SALT_BYTE_SIZE = 24;
        public const int HASH_BYTE_SIZE = 24;
        public const int PBKDF2_ITERATIONS = 1000;

        public const int ITERATION_INDEX = 0;
        public const int SALT_INDEX = 1;
        public const int PBKDF2_INDEX = 2;

        /// <summary>
        /// Creates a salted PBKDF2 hash of the password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <returns>The hash of the password.</returns>
        public static string CreateHash(string password)
        {
            // Generate a random salt
            RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[SALT_BYTE_SIZE];
            csprng.GetBytes(salt);

            // Hash the password and encode the parameters
            byte[] hash = PBKDF2(password, salt, PBKDF2_ITERATIONS, HASH_BYTE_SIZE);
            return PBKDF2_ITERATIONS + ":" +
                Convert.ToBase64String(salt) + ":" +
                Convert.ToBase64String(hash);
        }

        public static string GetGeneratedSalt()
        {
            // Generate a random salt
            RNGCryptoServiceProvider csprng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[SALT_BYTE_SIZE];
            csprng.GetBytes(salt);
            string getSalt = System.Text.Encoding.Default.GetString(salt);
            return getSalt;
        }

        //public static string  RemoveSalt(string correctHash)
        //{
        //    try
        //    {
        //        //correctHash = System.Text.Encoding.Default.GetString(Convert.FromBase64String(correctHash));
        //        // Extract the parameters from the hash
        //        //char[] delimiter = { ':' };
        //        //string[] split = correctHash.Split(delimiter);
        //        //int iterations = Int32.Parse(split[ITERATION_INDEX]);
        //        //byte[] salt = Convert.FromBase64String(split[SALT_INDEX]);
        //        //byte[] hash = Convert.FromBase64String(split[PBKDF2_INDEX]);
        //        //byte[] hash = Convert.FromBase64String(correctHash);
        //        //string getHash = System.Text.Encoding.Default.GetString(hash);
        //        //return getHash;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return "";
        //}



        public static string RemoveSalt(string str)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 1; i < str.Length; i += 2)
                {
                    sb.Append(str[i]);
                }

                string strNew = sb.ToString();

                StringBuilder sb1 = new StringBuilder();
                for (int i = 0; i < strNew.Length; i += 2)
                {
                    int value = Convert.ToInt32(strNew.Substring(i, 2), 16);
                    sb1.Append(Char.ConvertFromUtf32(value));
                }

                string str64 = sb1.ToString();
                byte[] hash = Convert.FromBase64String(str64);
                string getHash = System.Text.Encoding.Default.GetString(hash);
                return getHash;
            }
            catch (Exception ex)
            {
            }
            return "";
        }




        /// <summary>
        /// Validates a password given a hash of the correct one.
        /// </summary>
        /// <param name="password">The password to check.</param>
        /// <param name="correctHash">A hash of the correct password.</param>
        /// <returns>True if the password is correct. False otherwise.</returns>
        public static bool ValidatePassword(string password, string correctHash)
        {
            try
            {
                // Extract the parameters from the hash
                char[] delimiter = { ':' };
                string[] split = correctHash.Split(delimiter);
                int iterations = Int32.Parse(split[ITERATION_INDEX]);
                byte[] salt = Convert.FromBase64String(split[SALT_INDEX]);
                byte[] hash = Convert.FromBase64String(split[PBKDF2_INDEX]);

                byte[] testHash = PBKDF2(password, salt, iterations, hash.Length);
                return SlowEquals(hash, testHash);

            }
            catch (Exception ex)
            {
            }
            return false;
        }

        /// <summary>
        /// Compares two byte arrays in length-constant time. This comparison
        /// method is used so that password hashes cannot be extracted from
        /// on-line systems using a timing attack and then attacked off-line.
        /// </summary>
        /// <param name="a">The first byte array.</param>
        /// <param name="b">The second byte array.</param>
        /// <returns>True if both byte arrays are equal. False otherwise.</returns>
        private static bool SlowEquals(byte[] a, byte[] b)
        {
            uint diff = (uint)a.Length ^ (uint)b.Length;
            for (int i = 0; i < a.Length && i < b.Length; i++)
                diff |= (uint)(a[i] ^ b[i]);
            return diff == 0;
        }

        /// <summary>
        /// Computes the PBKDF2-SHA1 hash of a password.
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <param name="salt">The salt.</param>
        /// <param name="iterations">The PBKDF2 iteration count.</param>
        /// <param name="outputBytes">The length of the hash to generate, in bytes.</param>
        /// <returns>A hash of the password.</returns>
        private static byte[] PBKDF2(string password, byte[] salt, int iterations, int outputBytes)
        {
            Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(password, salt);
            pbkdf2.IterationCount = iterations;
            return pbkdf2.GetBytes(outputBytes);
        }


        #endregion

        //By Robin ---For getting list of all users of accounts type and their roles i.e 12
        #region Getting Users of account type and their roles
        static object GetAllUsersOfAccountType()
        {
            try
            {
                using (UserContext context = new UserContext())
                {
                    var users = (from accUser in context.mUsers
                                 join mSubUser in context.mSubUserType on
                                 accUser.UserType equals mSubUser.SubUserTypeID
                                 where (mSubUser.UserTypeID == 12)
                                 select new mUserAccountRoles
                                 {
                                     userName = accUser.UserName,
                                     userID = accUser.UserId,
                                     roleName = mSubUser.SubUserTypeName
                                 }
                                 ).ToList();

                    return users;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion


        #region Getting user of accout type by ID
        static object GetUserOfAccountTypeById(object param)
        {
            try
            {
                var value = param as mUserAccountRoles;
                using (UserContext context = new UserContext())
                {
                    var users = (from accUser in context.mUsers

                                 join mSubUser in context.mSubUserType on
                                 accUser.UserType equals mSubUser.SubUserTypeID
                                 where (mSubUser.UserTypeID == 12) && (accUser.UserId == value.userID)
                                 select new mUserAccountRoles
                                 {
                                     userName = accUser.UserName,
                                     userID = accUser.UserId,
                                     roleName = mSubUser.SubUserTypeName
                                     //SubUserTypeID = accUser.UserType
                                 }
                                 ).FirstOrDefault();
                    return users;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region To get available Accounts Roles
        static object AvailableAccountRoles()
        {
            try
            {
                using (UserContext context = new UserContext())
                {
                    var AccountRoles = (from _subUserType in context.mSubUserType
                                        where _subUserType.UserTypeID == 12
                                        select new RoleList
                                        {
                                            SubUserTypeName = _subUserType.SubUserTypeName,
                                            SubUserTypeID = _subUserType.SubUserTypeID
                                        }
                                            ).ToList();
                    return AccountRoles;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
        #region Saving new roles
        public static object UpdateAccountRoles(object p)
        {
            try
            {
                mUserAccountRoles userdata = (mUserAccountRoles)p;

                using (var ctx = new UserContext())
                {
                    var data = (from user in ctx.mUsers
                                where user.UserId == userdata.userID
                                select user).FirstOrDefault();
                    if (data.MailID == "" || data.MailID == null)
                    {
                        data.MailID = "AccountsAdmin@sbl.com";
                    }
                    if (data.Mobile == "" || data.Mobile == null)
                    {
                        data.Mobile = "AccountsAdmin";
                    }
                    if (data.OTPValue == "" || data.OTPValue == null)
                    {
                        data.OTPValue = "AccountsAdmin";
                    }
                    data.ModifiedWhen = DateTime.Today;
                    data.ModifiedBy = "Accounts Admin";
                    data.UserType = userdata.SubUserTypeID;
                    ctx.mUsers.Attach(data);
                    ctx.Entry(data).State = EntityState.Modified;
                    ctx.SaveChanges();
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ValidateDeptUser
        static List<mUsers> GetAllUser(object param)
        {
            mUsers Parameter = param as mUsers;
            UserContext objUserContext = new UserContext();
            List<string> deptarr = Parameter.DeptId.Split(',').ToList();
            var query = (from User in objUserContext.mUsers
                         where (User.IsSecretoryId == false || User.IsSecretoryId == null)
                             && (User.IsMember.ToUpper() == "FALSE" || User.IsMember == null)
                             && (User.IsHOD == false || User.IsHOD == null)
                             && (deptarr.Contains(User.DeptId))
                             && User.DeptId != null
                         select User).ToList();
            var results = query.ToList();
            return results;
        }

        static void UpdateDeptUser(object param)
        {
            UserContext objUserContext = new UserContext();
            mUsers objUpdatemUsers = param as mUsers;
            mUsers obj = objUserContext.mUsers.Single(m => m.UserId == objUpdatemUsers.UserId);
            obj.IsSecChange = true;
            objUserContext.Configuration.ValidateOnSaveEnabled = false;
            objUserContext.SaveChanges();

        }
        static object UpdatedMemberDepartment(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            if (query != null)
            {

                query.DeptId = model.DeptId;
                query.DepartmentIDs = model.DepartmentIDs;
                query.IsSecChange = false;
                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                uCtxt.SaveChanges();
            }

            return null;
        }
        #endregion
        static object UpdateDepatmentAndSecretary(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            if (query != null)
            {
                if (model.UserType == 3)
                {

                    query.SecretoryId = model.SecretoryId;

                }
                if (model.UserType == 16)
                {

                    if (model.HODID == 0 || model.HODID == null)
                    {
                        query.SecretoryId = model.SecretoryId;
                    }
                    else
                    {
                        query.SecretoryId = model.HODID;
                    }
                }

                query.IsSecChange = model.IsSecChange;
                query.DepartmentIDs = model.DepartmentIDs;
                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                uCtxt.SaveChanges();
            }
            if (model.UserType == 3 || model.UserType == 16)
            {
                var query2 = (from mdl in uCtxt.tUserAccessRequest where mdl.UserID == model.UserId select mdl).ToList();
                if (query != null)
                {
                    if (query2.Count > 0 && query2 != null)
                    {
                        foreach (var val in query2)
                        {
                            if (val.SecreataryId != null && val.SecreataryId != 0)
                            {
                                val.SecreataryId = Convert.ToInt32(query.SecretoryId);
                                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                            }
                            else
                            {
                                val.HODId = Convert.ToInt32(query.SecretoryId);
                                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                            }
                            uCtxt.SaveChanges();
                        }
                    }


                }
            }
            return null;
        }
        static object UpdateAdditionalDepatment(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            if (query != null)
            {
                query.RequestedAdditionalDept = model.RequestedAdditionalDept;
                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                uCtxt.SaveChanges();
            }

            return null;
        }
        static object ApproveAdditionalDepatment(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            if (query != null)
            {
                query.ApprovedAdditionalDept = model.ApprovedAdditionalDept;
                query.RequestedAdditionalDept = null;
                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                uCtxt.SaveChanges();
            }

            return null;
        }
        static object UpdateIdsInRequest(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            if (query != null)
            {
                if (model.UserType == 2 || model.UserType == 3)
                {

                    query.SecretoryId = model.SecretoryId;

                }
                if (model.UserType == 15 || model.UserType == 16)
                {

                    if (model.HODID == 0 || model.HODID == null)
                    {
                        query.SecretoryId = model.SecretoryId;
                    }
                    else
                    {
                        query.SecretoryId = model.HODID;
                    }
                }
                if (model.UserType == 37)
                {

                    if (model.OfficeId != null && model.OfficeId != 0)
                    {
                        query.SecretoryId = Convert.ToInt32(model.OfficeId);
                    }

                }
                if (model.UserType == 17 || model.UserType == 18 || model.UserType == 19 || model.UserType == 20 || model.UserType == 21 || model.UserType == 22)
                {
                    int num;

                    bool isNum = Int32.TryParse(model.UserName, out num);

                    if (isNum)
                    {
                        query.SecretoryId = Convert.ToInt32(model.UserName);
                    }
                }


            }
            if (model.UserType == 2 || model.UserType == 3 || model.UserType == 15 || model.UserType == 16 || model.UserType == 17 || model.UserType == 18 || model.UserType == 19 || model.UserType == 20 || model.UserType == 21 || model.UserType == 22 || model.UserType == 37)
            {
                var query2 = (from mdl in uCtxt.tUserAccessRequest where mdl.UserID == model.UserId select mdl).ToList();
                if (query != null)
                {
                    if (query2.Count > 0 && query2 != null)
                    {
                        foreach (var val in query2)
                        {

                            if (val.SecreataryId != null && val.SecreataryId != 0)
                            {
                                val.SecreataryId = Convert.ToInt32(query.SecretoryId);
                                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                            }
                            else
                            {
                                val.SecreataryId = 0;
                                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                            }
                            if (val.HODId != null && val.HODId != 0)
                            {
                                val.HODId = Convert.ToInt32(query.SecretoryId);
                                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                            }
                            if (val.MemberId != null && val.MemberId != 0)
                            {
                                val.MemberId = Convert.ToInt32(query.SecretoryId);
                                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                            }
                            else
                            {
                                val.MemberId = 0;
                                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                            }
                            if (val.OfficeID != null && val.OfficeID != 0)
                            {
                                val.OfficeID = Convert.ToInt32(query.SecretoryId);
                                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                            }
                            uCtxt.SaveChanges();
                        }
                    }


                }
            }
            return null;
        }
        private static object ExistUserSubTypeId(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            bool boolvalue = false;
            var isexist = (from usrdschash in uCtxt.mUsers where usrdschash.UserId == model.UserId select usrdschash).SingleOrDefault();
            if (isexist.UserType == 0 || isexist.UserType == null)
            {
                boolvalue = false;
            }
            else
            {
                boolvalue = true;
            }
            return boolvalue;
        }

        static object UpdateAssemblyPassword(object param)
        {
            mUsers model = param as mUsers;
            UserContext uCtxt = new UserContext();
            var query = (from mdl in uCtxt.mUsers where mdl.UserId == model.UserId select mdl).SingleOrDefault();
            if (query != null)
            {
                query.AssemblyPassword = model.AssemblyPassword;
                uCtxt.Configuration.ValidateOnSaveEnabled = false;
                uCtxt.SaveChanges();
            }
            return null;
        }

        static object GetMemberDetailsForOSD(object param)
        {
            UserContext ctxt = new UserContext();
            string MemberCode = param as string;
            int usertype = 17;

            var query = (from adhr in ctxt.mUsers where adhr.UserName == MemberCode && adhr.UserType == usertype select adhr.AadarId).FirstOrDefault();

            return query;
        }

        static object GetNodalOfficerList(object param)
        {
            using (UserContext ctxt = new UserContext())
            {
                string MemberCode = param as string;

                var query = (from data in ctxt.mUsers
                             join dept in ctxt.mDepartment on
                                 data.DeptId equals dept.deptId into dt
                             from dept in dt.DefaultIfEmpty()
                             join office in ctxt.moffice on
                                 data.OfficeId equals office.OfficeId into of
                             from office in of.DefaultIfEmpty()
                             where data.UserType == 37 && data.OfficeLevel == "4"
                             orderby data.ModifiedWhen
                             select new
                             {
                                 aAdharId = data.AadarId,
                                 creationDate = data.ModifiedWhen,
                                 deptId = data.DeptId,
                                 deptName = dept.deptname,
                                 gender = data.Gender,
                                 mobile = data.MobileNo,
                                 officeid = data.OfficeId,
                                 officeName = office.officename,
                                 username = data.UserName
                             }
                             ).ToList();

                List<UserCreation> listObj = new List<UserCreation>();

                foreach (var item in query)
                {
                    UserCreation obj = new UserCreation();
                    obj.AadhaarId = item.aAdharId;
                    obj.CreationDate = Convert.ToString(item.creationDate);
                    obj.DepartmentId = item.deptId;
                    obj.DepartmentName = item.deptName;
                    obj.Gender = item.gender;
                    obj.Mobile = item.mobile;
                    obj.OfficeId = item.officeid;
                    obj.OfficeName = item.officeName;
                    obj.UserName = item.username;
                    listObj.Add(obj);
                }

                return listObj;
            }
        }

        static object GetSDMList(object param)
        {
            using (UserContext ctxt = new UserContext())
            {
                string MemberCode = param as string;

                var query = (from data in ctxt.mUsers
                             join dept in ctxt.mDepartment on
                                 data.DeptId equals dept.deptId into dt
                             from dept in dt.DefaultIfEmpty()
                             join office in ctxt.moffice on
                                 data.OfficeId equals office.OfficeId into of
                             from office in of.DefaultIfEmpty()
                             where data.UserType == 40 && data.OfficeLevel == "4"
                             orderby data.ModifiedWhen
                             select new
                             {
                                 aAdharId = data.AadarId,
                                 creationDate = data.ModifiedWhen,
                                 deptId = data.DeptId,
                                 deptName = dept.deptname,
                                 gender = data.Gender,
                                 mobile = data.MobileNo,
                                 officeid = data.OfficeId,
                                 officeName = office.officename,
                                 username = data.UserName
                             }
                             ).ToList();

                List<UserCreation> listObj = new List<UserCreation>();

                foreach (var item in query)
                {
                    UserCreation obj = new UserCreation();
                    obj.AadhaarId = item.aAdharId;
                    obj.CreationDate = Convert.ToString(item.creationDate);
                    obj.DepartmentId = item.deptId;
                    obj.DepartmentName = item.deptName;
                    obj.Gender = item.gender;
                    obj.Mobile = item.mobile;
                    obj.OfficeId = item.officeid;
                    obj.OfficeName = item.officeName;
                    obj.UserName = item.username;
                    listObj.Add(obj);
                }

                return listObj;
            }
        }

        public static string CheckAadhaarID(object param)
        {
            UserContext ctxt = new UserContext();
            string AadhaarCode = param as string;
            var query = (from adhr in ctxt.mUsers
                         where adhr.AadarId == AadhaarCode
                         select adhr.AadarId).FirstOrDefault();

            if (query == null || query == "")
            {
                query = "N";
            }
            else
            {
                query = "Y";
            }
            return query;
        }

        public static object get_AllSubdivision(object param)
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var query = (from list in context.mSubDivision
                             select new
                             {
                                 mSubDivisionId = list.mSubDivisionId,
                                 SubDivisionName = list.SubDivisionName
                             }
                             ).ToList();

                List<mSubDivision> listObj = new List<mSubDivision>();

                foreach (var item in query)
                {
                    mSubDivision obj = new mSubDivision();
                    obj.mSubDivisionId = item.mSubDivisionId;
                    obj.SubDivisionName = item.SubDivisionName;
                    listObj.Add(obj);
                }

                return listObj;

            }
        }

    }
}
