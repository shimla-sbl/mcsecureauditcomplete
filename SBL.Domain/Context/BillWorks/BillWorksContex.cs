﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.Domain.Context.Assembly;
using SBL.Domain.Context.Session;
using SBL.Domain.Context.SiteSetting;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Session;
using SBL.Service.Common;

namespace SBL.Domain.Context.BillWorks
{
    //class BillWorksContex
    //{
    //}
    public class BillWorksContex : DBBase<BillWorksContex>
    {
        public BillWorksContex() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public virtual DbSet<mBills> mBills { get; set; }
        public virtual DbSet<mAssembly> mAssemblies { get; set; }
        public virtual DbSet<mSession> mSessions { get; set; }


        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "GetBills":
                    {
                        return GetBills(param.Parameter);
                    }
                case "GetBillsDetails":
                    {
                        return GetBillsDetails(param.Parameter);
                    }
                case "GetAssemblySessionList":
                    {
                        return GetAssemblySessionList(param.Parameter);
                    }
                case "GetSessionsByAssemblyID":
                    {
                        return GetSessionsByAssemblyID(param.Parameter);
                    }
                case "UpdateMBills":
                    {
                        return UpdateMBills(param.Parameter);
                    }


            }
            return null;
        }
        public static object GetBills(object param)
        {

            mBills model = param as mBills;
            BillWorksContex pCtxt = new BillWorksContex();
            var query = (from Bills in pCtxt.mBills
                         select new BillWorkModel
                         {
                             ID = Bills.ID,
                             BillTitle = Bills.BillTitle,
                             BillNo = Bills.BillNo

                         }).ToList();
            int totalRecords = query.Count();
            var results = query.ToList();
            model.BillWorkModel = results;
            return model;

        }
        static mBills GetBillsDetails(object param)
        {

            mBills model = param as mBills;

            BillWorksContex pCtxt = new BillWorksContex();

            var query = (from Bills in pCtxt.mBills
                         where (Bills.ID == model.ID)
                         select new BillWorkModel
                         {
                             ID = Bills.ID,
                             BillTitle = Bills.BillTitle,
                             BillNo = Bills.BillNo,
                             IntroductionDate = Bills.IntroductionDate,
                             PassingDate = Bills.PassingDate,
                             AssesntDate = Bills.AssesntDate,
                             ActNo = Bills.ActNo,
                             IntroductionFilePath = Bills.IntroductionFilePath,
                             AccentedFilePath = Bills.AccentedFilePath,
                             PassingFilePath = Bills.PassingFilePath,
                             ActFilePath = Bills.ActFilePath,
                             AssemblyId = Bills.AssemblyId,
                             SessionId = Bills.SessionId,
                             PublishedDate =Bills.PublishedDate,
                             AssentedBy=Bills.AssentedBy,
                             IsFreeze=Bills.IsFreeze
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.BillWorkModel = results;

            return model;
        }
        public static object GetAssemblySessionList(object param)
        {
            mBills model = param as mBills;
            BillWorksContex pCtxt = new BillWorksContex();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();
            //if (model.AssemblyID == 0 && model.SessionID == 0)
            //{
            //    // Get SiteSettings 
            //    var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            //    var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

            //    model.SessionID = Convert.ToInt16(SessionCode);
            //    model.AssemblyID = Convert.ToInt16(AssemblyCode);
            //}

            //string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionID && m.AssemblyID == model.AssemblyID select m.SessionName).SingleOrDefault();
            //string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyID select m.AssemblyName).SingleOrDefault();

            //model.SessionName = SessionName;
            //model.AssesmblyName = AssemblyName;

            //model.mAssemblyList = (from A in pCtxt.mAssemblies
            //                       select A).ToList();
            //model.sessionList = (from S in pCtxt.mSessions
            //                     where S.AssemblyID == model.AssemblyID
            //                     select S).ToList();
            var AssemblyList = (from Min in pCtxt.mAssemblies
                                select new BillWorkModel
                                {
                                    AssemblyId = Min.AssemblyCode,
                                    AssesmblyName = Min.AssemblyName
                                }).ToList();

            return AssemblyList;
            // return model;
        }

        public static object GetSessionsByAssemblyID(object param)
        {
            SessionContext db = new SessionContext();
            mBills model = param as mBills;
            //mSession model = param as mSession;
            var SessionLt = (from dist in db.mSession
                             where dist.AssemblyID == model.AssemblyId
                             select new BillWorkModel
                             {
                                 SessionId = dist.SessionCode,
                                 SessionName = dist.SessionName
                             }).ToList();

            return SessionLt;
        }


        static object UpdateMBills(object param)
        {

            mBills model = param as mBills;
            BillWorksContex pCtxt = new BillWorksContex();

            mBills obj = pCtxt.mBills.Single(m => m.ID == model.ID);
            obj.AssemblyId = model.AssemblyId;
            obj.SessionId = model.SessionId;
            obj.BillTitle = model.BillTitle;
            obj.ActNo = model.ActNo;
            obj.IntroductionDate = model.IntroductionDate;
            obj.PassingDate = model.PassingDate;
            obj.AssesntDate = model.AssesntDate;
            obj.IntroductionFilePath = model.IntroductionFilePath;
            obj.PassingFilePath = model.PassingFilePath;
            obj.AccentedFilePath = model.AccentedFilePath;
            obj.ActFilePath = model.ActFilePath;
            obj.AssentedBy = model.AssentedBy;
            obj.PublishedDate = model.PublishedDate;
            obj.IsFreeze = model.IsFreeze;
            pCtxt.SaveChanges();
            string msg = "";
            return msg;
        }


    }
}
