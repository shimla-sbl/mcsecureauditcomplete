﻿using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.UploadMetaFile;
using SBL.Service.Common;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.House;
using SBL.DomainModel;
using SBL.DomainModel.Models.Passes;

namespace SBL.Domain.Context.UploadMetaFile
{
    public class UploadMetaFileContext : DBBase<UploadMetaFileContext>
    {
        public UploadMetaFileContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mUploadmetaDataFile> metaDataFile { get; set; }
        public DbSet<tCommittee> tCommittee { get; set; }
        public DbSet<tStarredQuestionsHistory> tStarredQuestionsHistory { get; set; }
        public DbSet<tUnStarredQuestionsHistory> tUnStarredQuestionsHistory { get; set; }
        public DbSet<ReporterDescriptionsLog> ReporterDescriptionsLog { get; set; }
        public DbSet<VisitorData> VisitorData { get; set; }
        public DbSet<mPasses> mPasses { get; set; }
        public DbSet<PassCategory> PassCategory { get; set; }
        
        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetAllMetaDataFile":
                    {
                        return GetAllMetaDataFile(param.Parameter);
                    }
                case "AddUploadmetaData":
                    {
                        return AddUploadmetaData(param.Parameter);
                    }
                case "GetAllCommittee":
                    {
                        return GetAllCommittee();
                    }
                case "GetFilteredSQuestionList":
                    {
                        return GetFilteredSQuestionList(param.Parameter);
                    }
                case "GetFilteredUSQuestionList":
                    {
                        return GetFilteredUSQuestionList(param.Parameter);
                    }
                case "GetFilteredDebateList":
                    {
                        return GetFilteredDebateList(param.Parameter);
                    }
                case "DeleteSQuestionList":
                    {
                        return DeleteSQuestionList(param.Parameter);
                    }
                case "DeleteUSQuestionList":
                    {
                        return DeleteUSQuestionList(param.Parameter);
                    }
                case "DeleteDebateList":
                    {
                        return DeleteDebateList(param.Parameter);
                    }
                case "GetPassCountList":
                    {
                        return GetPassCountList(param.Parameter);
                    }
                    
            }
            return null;
        }
        static object GetAllMetaDataFile(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile model = param as mUploadmetaDataFile;
                    // if (model.IsUserType == "5")
                    // {
                    var result = (from MetaData in _context.metaDataFile
                                  orderby MetaData.Id descending
                                  select MetaData).ToList();

                    return result;

                }

            }
            catch (Exception ee)
            {
                throw ee;
            }
            return null;
        }

        static object AddUploadmetaData(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile DocumentCreate = param as mUploadmetaDataFile;
                    _context.metaDataFile.Add(DocumentCreate);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
        static object GetAllCommittee()
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                  
                    var result = (from committee in _context.tCommittee
                                  orderby committee.CommitteeName ascending
                                  select committee).ToList();

                    return result;

                }

            }
            catch (Exception ee)
            {
                throw ee;
            }
            return null;
        }
        static object GetFilteredSQuestionList(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile model = param as mUploadmetaDataFile;
                    // if (model.IsUserType == "5")
                    // {
                    var result = (from MetaData in _context.tStarredQuestionsHistory where MetaData.AssemblyID==model.AssemblyId && MetaData.SessionID==model.SessionId && MetaData.SessionDate==model.SessionDate
                                  orderby MetaData.id ascending
                                  select MetaData).ToList();

                    return result;

                }

            }
            catch (Exception ee)
            {
                throw ee;
            }
           
        }

        static object GetPassCountList(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile model = param as mUploadmetaDataFile;
                    // if (model.IsUserType == "5")
                    // {
                    var result = (from vd in _context.VisitorData
                                  join mp in _context.mPasses on vd.PassCode equals mp.PassCode
                                  join pc in _context.PassCategory on mp.ApprovedPassCategoryID equals pc.PassCategoryID
                                  where vd.AssemblyCode == model.AssemblyId && vd.SessionCode == model.SessionId && vd.SessionDate == model.SessionDate
                                  orderby vd.VerifiedPassID ascending
                                  select  vd).ToList();

                    return result;

                }

            }
            catch (Exception ee)
            {
                throw ee;
            }

        }


       
        static object GetFilteredUSQuestionList(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile model = param as mUploadmetaDataFile;
                    // if (model.IsUserType == "5")
                    // {
                    var result = (from MetaData in _context.tUnStarredQuestionsHistory
                                  where MetaData.AssemblyID == model.AssemblyId && MetaData.SessionID == model.SessionId && MetaData.SessionDate == model.SessionDate
                                  orderby MetaData.id ascending
                                  select MetaData).ToList();

                    return result;

                }

            }
            catch (Exception ee)
            {
                throw ee;
            }

        }
        static object GetFilteredDebateList(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile model = param as mUploadmetaDataFile;
                    // if (model.IsUserType == "5")
                    // {
                    var result = (from MetaData in _context.ReporterDescriptionsLog
                                  where MetaData.AssemblyId == model.AssemblyId && MetaData.SessionId == model.SessionId && MetaData.LOBDate == model.SessionDate
                                  orderby MetaData.Id ascending
                                  select MetaData).ToList();

                    return result;

                }

            }
            catch (Exception ee)
            {
                throw ee;
            }

        }
        static object DeleteSQuestionList(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile model = param as mUploadmetaDataFile;
                 string[] idsarr=model.DeletedIdsList.Split(',').ToArray();
                 foreach (var id in idsarr)
                 {
                     int i = Convert.ToInt32(id);
                     tStarredQuestionsHistory ModuleToRemove = _context.tStarredQuestionsHistory.SingleOrDefault(a => a.id == i);
                     _context.tStarredQuestionsHistory.Remove(ModuleToRemove);
                     _context.SaveChanges();
                 }

                 var result = (from MetaData in _context.tStarredQuestionsHistory
                                  where MetaData.AssemblyID == model.AssemblyId && MetaData.SessionID == model.SessionId && MetaData.SessionDate == model.SessionDate
                                  orderby MetaData.id ascending
                                  select MetaData).ToList();

                    return result;
                   
                }
              

            }
            catch (Exception ee)
            {
                throw ee;
            }

        }
        static object DeleteUSQuestionList(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile model = param as mUploadmetaDataFile;
                    string[] idsarr = model.DeletedIdsList.Split(',').ToArray();
                    foreach (var id in idsarr)
                    {
                        int i = Convert.ToInt32(id);
                        tUnStarredQuestionsHistory ModuleToRemove = _context.tUnStarredQuestionsHistory.SingleOrDefault(a => a.id == i);
                        _context.tUnStarredQuestionsHistory.Remove(ModuleToRemove);
                        _context.SaveChanges();
                    }

                    var result = (from MetaData in _context.tUnStarredQuestionsHistory
                                  where MetaData.AssemblyID == model.AssemblyId && MetaData.SessionID == model.SessionId && MetaData.SessionDate == model.SessionDate
                                  orderby MetaData.id ascending
                                  select MetaData).ToList();

                    return result;

                }


            }
            catch (Exception ee)
            {
                throw ee;
            }

        }
        static object DeleteDebateList(object param)
        {
            try
            {
                using (UploadMetaFileContext _context = new UploadMetaFileContext())
                {
                    mUploadmetaDataFile model = param as mUploadmetaDataFile;
                    string[] idsarr = model.DeletedIdsList.Split(',').ToArray();
                    foreach (var id in idsarr)
                    {
                        int i = Convert.ToInt32(id);
                        ReporterDescriptionsLog ModuleToRemove = _context.ReporterDescriptionsLog.SingleOrDefault(a => a.Id == i);
                        _context.ReporterDescriptionsLog.Remove(ModuleToRemove);
                        _context.SaveChanges();
                    }

                    var result = (from MetaData in _context.ReporterDescriptionsLog
                                  where MetaData.AssemblyId == model.AssemblyId && MetaData.SessionId == model.SessionId && MetaData.LOBDate == model.SessionDate
                                  orderby MetaData.Id ascending
                                  select MetaData).ToList();

                    return result;

                }


            }
            catch (Exception ee)
            {
                throw ee;
            }

        }
    }

    

}
