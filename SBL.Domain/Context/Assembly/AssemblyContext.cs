﻿namespace SBL.Domain.Context.Assembly
{
    #region Namespace Reffrences
    using SBL.DAL;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Secretory;
    using SBL.DomainModel.Models.Session;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;


    #endregion

    public class AssemblyContext : DBBase<AssemblyContext>
    {
        public AssemblyContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public virtual DbSet<mAssembly> mAssemblies { get; set; }
        public virtual DbSet<mSession> mSessions { get; set; }
        public virtual DbSet<mConstituency> mConstituency { get; set; }
        public virtual DbSet<mSessionDate> mSessionDate { get; set; }
        public virtual DbSet<mDepartment> mDepartment { get; set; }
        public virtual DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public virtual DbSet<mMinistry> mMinistry { get; set; }
        public virtual DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }
        public virtual DbSet<mMinistryDepartment> mMinistryDepartment { get; set; }
        public virtual DbSet<mSecretoryDepartment> mSecretoryDepartment { get; set; }


        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
               
                case "CreateAssembly": { return CreateAssembly(param.Parameter); }
                case "GetAllAssemblyData": { return GetAllAssemblyData(param.Parameter); }
                case "GetAllAssemblies": { return GetAllAssemblies(param.Parameter); }
                case "GetAssemblyDataById": { return GetAssemblyDataById(param.Parameter); }
                case "UpdateAssemblyData": { return UpdateAssemblyData(param.Parameter); }
                case "DeleteAssembly": { return DeleteAssembly(param.Parameter); }
                case "GetAssemblyCodeById": { return GetAssemblyCodeById(); }
                case "GetAssemblyCodeById1": { return GetAssemblyCodeById1(param.Parameter); }
                case "IsAssemblyIdChildExist": { return IsAssemblyIdChildExist(param.Parameter); }
                case "ChangeAssemblyStatus":
                    {
                        return ChangeAssemblyStatus(param.Parameter);
                    }
                case "GetAssemblyNameByAssemblyCode":
                    {
                        return GetAssemblyNameByAssemblyCode(param.Parameter);
                    }
                case "GetAllAssemblyWithSessions":
                    {
                        return GetAllAssemblyWithSessions(param.Parameter);
                    }
                case "GetAllAssemblyReverse":
                    {
                        return GetAllAssemblyReverse(param.Parameter);
                    }
                case "GetAllDepartments":
                    {
                        return GetAllDepartments();
                    }
                case "GetAssemblyNameLocalByAssemblyCode":
                    {
                        return GetAssemblyNameLocalByAssemblyCode(param.Parameter);
                    }
                case "GetAllPeriods":
                    {
                        return GetAllPeriods();
                    }
                case "GetAssemblyPeriodByAssemblyCode":
                    {
                        return GetAssemblyPeriodByAssemblyCode(param.Parameter);
                    }

            }
            return null;
        }



        private static object IsAssemblyIdChildExist(object param)
        {
            try
            {
                using (AssemblyContext db = new AssemblyContext())
                {
                    mAssembly model = (mAssembly)param;
                    var Res = (from e in db.mSessions
                               where (e.AssemblyID == model.AssemblyID)
                               select e).Count();

                    if (Res == 0)
                    {
                        var mDate = (from e in db.mSessionDate
                                     where (e.AssemblyId == model.AssemblyID)
                                     select e).Count();

                        if (mDate == 0)
                        {
                            var mConst = (from e in db.mConstituency
                                          where (e.AssemblyID == model.AssemblyID)
                                          select e).Count();
                            if (mConst == 0)
                            {
                                var memAssem = (from e in db.mMemberAssembly
                                                where (e.AssemblyID == model.AssemblyID)
                                                select e).Count();
                                if (memAssem == 0)
                                {
                                    var mini = (from e in db.mMinistry
                                                where (e.AssemblyID == model.AssemblyID)
                                                select e).Count();

                                    if (mini == 0)
                                    {
                                        var minmini = (from e in db.mMinsitryMinister
                                                       where (e.AssemblyID == model.AssemblyID)
                                                       select e).Count();
                                        if (minmini == 0)
                                        {
                                            var minDept = (from e in db.mMinistryDepartment
                                                           where (e.AssemblyID == model.AssemblyID)
                                                           select e).Count();
                                            if (minDept == 0)
                                            {
                                                var secDept = (from e in db.mSecretoryDepartment
                                                               where (e.AssemblyID == model.AssemblyID)
                                                               select e).Count();

                                                if (secDept == 0)
                                                {
                                                    return false;


                                                }

                                            }

                                        }
                                    }
                                }
                            }
                        }
                    }

                    else
                    {
                        return true;

                    }

                }
                return true;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }


        static object GetAllPeriods()
        {
            AssemblyContext db = new AssemblyContext();


            var PerdList = (from cmt in db.mAssemblies
                            orderby cmt.AssemblyID descending
                            select cmt).ToList();
            db.Close();
            return PerdList;

        }

        static int GetAssemblyCodeById1(object param)
        {
            try
            {
                AssemblyContext db = new AssemblyContext();
                int id = Convert.ToInt32(param);
                var query1 = db.mAssemblies.SingleOrDefault(a => a.AssemblyID == id);
                return query1.AssemblyCode;

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        static int GetAssemblyCodeByIdforUpdate()
        {
            try
            {
                AssemblyContext db = new AssemblyContext();
                mAssembly model = new mAssembly();
                //int id = Convert.ToInt32(param);
                var query1 = db.mAssemblies.SingleOrDefault(a => a.AssemblyID == model.AssemblyID);
                return query1.AssemblyCode;

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        static int GetAssemblyCodeById()
        {
            try
            {
                AssemblyContext db = new AssemblyContext();

                var query1 = db.mAssemblies.ToList();

                if (query1.Count > 0)
                {
                    var query = query1.LastOrDefault();

                    return query.AssemblyCode;

                }
                else
                {
                    return 0;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        public static object GetAllAssemblies(object param)
        {
            try
            {
                AssemblyContext db = new AssemblyContext();
                mAssembly model = param as mAssembly;
                var data = (from a in db.mAssemblies
                            orderby a.AssemblyID descending
                            where a.IsDeleted == null
                            select a).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                var data1 = (from a in db.mAssemblies
                             orderby a.AssemblyID descending
                             where a.IsDeleted==null
                             select a).Count();
                int totalRecords = data1;
                model.ResultCount = totalRecords;
                var results = data.ToList();
                model.AssemblyList = results;
                return model;

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           
        }

        static object GetAssemblyNameLocalByAssemblyCode(object param)
        {
            AssemblyContext AssemblyContext = new AssemblyContext();
            mAssembly assmblyMdl = param as mAssembly;
            string AssemblyNameLocal = (from m in AssemblyContext.mAssemblies where m.AssemblyCode == assmblyMdl.AssemblyID select m.AssemblyNameLocal).SingleOrDefault();
            return AssemblyNameLocal;

        }

        static object GetAllDepartments()
        {
            AssemblyContext db = new AssemblyContext();
            var GetAllData = db.mDepartment.ToList();
            return GetAllData;
        }

        public static object GetAssemblyNameByAssemblyCode(object param)
        {
            AssemblyContext AssemblyContext = new AssemblyContext();
            mAssembly assmblyMdl = param as mAssembly;
            string AssemblyName = (from m in AssemblyContext.mAssemblies where m.AssemblyCode == assmblyMdl.AssemblyID select m.AssemblyName).SingleOrDefault();
            return AssemblyName;

        }
        public static object GetAssemblyPeriodByAssemblyCode(object param)
        {
            AssemblyContext AssemblyContext = new AssemblyContext();
            String assmblyMdl = param as String;
            int assemblyId = Convert.ToInt16(assmblyMdl);
            string AssemblyPeriod = (from m in AssemblyContext.mAssemblies where m.AssemblyCode == assemblyId select m.Period).FirstOrDefault();
            return AssemblyPeriod;

        }
        static object CreateAssembly(object param)
        {
            if (null == param)
            {
                return null;
            }
            mAssembly assmblyMdl = param as mAssembly;
            using (AssemblyContext db = new AssemblyContext())
            {

                if (assmblyMdl != null)
                {
                    assmblyMdl.CreatedDate = DateTime.Now;
                    assmblyMdl.ModifiedDate = DateTime.Now;
                    db.mAssemblies.Add(assmblyMdl);
                    db.SaveChanges();
                }
                else
                {
                    return null; //null means already exists
                }
                db.Close();

            }
            return assmblyMdl;
        }

        static List<mAssembly> GetAllAssemblyData(object param)
        {
            AssemblyContext db = new AssemblyContext();
            mAssembly model = param as mAssembly;
            var query = (from dist in db.mAssemblies
                         select dist);
            string searchText = (model.AssemblyName != null && model.AssemblyName != "") ? model.AssemblyName.ToLower() : "";
            if (searchText != "")
            {
                query = query.Where(a =>
                    a.AssemblyName != null && a.AssemblyName.ToLower().Contains(searchText));

            }

            return query.ToList();
        }

        static mAssembly GetAssemblyDataById(object param)
        {
            mAssembly assmblyMdl = param as mAssembly;
            AssemblyContext db = new AssemblyContext();
            var query = db.mAssemblies.SingleOrDefault(a => a.AssemblyID == assmblyMdl.AssemblyID);
            return query;
        }

        static object UpdateAssemblyData(object param)
        {
            mAssembly model = param as mAssembly;
            if (null == param)
            {
                return null;
            }
            AssemblyContext ctxt = new AssemblyContext();
            var existingDis = ctxt.mAssemblies.SingleOrDefault(a => a.AssemblyID == model.AssemblyID);
            ctxt.Close();

            using (AssemblyContext db = new AssemblyContext())
            {
                if (db.mAssemblies.Where(a => a.AssemblyName == model.AssemblyName && a.AssemblyName != existingDis.AssemblyName).Count() == 0)
                {
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mAssemblies.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                else
                {
                    return null;
                }
            }
            return GetAllAssemblyData(param);
        }

        static object DeleteAssembly(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (AssemblyContext db = new AssemblyContext())
            {
                mAssembly parameter = param as mAssembly;
                mAssembly asmblyToRemove = db.mAssemblies.SingleOrDefault(a => a.AssemblyID == parameter.AssemblyID);
                //if (asmblyToRemove != null)
                //{
                //    asmblyToRemove.IsDeleted = true;
                //}
                db.mAssemblies.Remove(asmblyToRemove);
                db.SaveChanges();
                db.Close();
            }
            return param;
        }

        static object ChangeAssemblyStatus(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (AssemblyContext db = new AssemblyContext())
            {
                mAssembly parameter = param as mAssembly;
                mAssembly asmblyToRemove = db.mAssemblies.SingleOrDefault(a => a.AssemblyID == parameter.AssemblyID);
                db.mAssemblies.Attach(asmblyToRemove);
                db.Entry(asmblyToRemove).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return param;
        }

        static object GetAllAssemblyWithSessions(object param)
        {
            mAssembly parameter = param as mAssembly;
            using (AssemblyContext db = new AssemblyContext())
            {
                parameter.ListmAssembly = (from Assembly in db.mAssemblies
                                           orderby Assembly.AssemblyID descending
                                           select Assembly).ToList();

                for (int i = 0; i < parameter.ListmAssembly.Count; i++)
                {
                    int AssemblyCode = parameter.ListmAssembly[i].AssemblyCode;

                    parameter.ListmAssembly[i].ListmSession = (from Session in db.mSessions
                                                               orderby Session.SessionCode descending
                                                               where Session.AssemblyID == AssemblyCode
                                                               select Session).ToList();

                    for (int j = 0; j < parameter.ListmAssembly[i].ListmSession.Count; j++)
                    {
                        int indexof1 = parameter.ListmAssembly[i].ListmSession[j].SessionName.IndexOf("(");
                        parameter.ListmAssembly[i].ListmSession[j].SessionName = parameter.ListmAssembly[i].ListmSession[j].SessionName.Substring(0, indexof1);
                    }


                    int indexof = parameter.ListmAssembly[i].AssemblyName.IndexOf(" ");
                    parameter.ListmAssembly[i].AssemblyName = parameter.ListmAssembly[i].AssemblyName.Substring(0, indexof);
                }

            }
            return parameter.ListmAssembly;
        }

        static List<mAssembly> GetAllAssemblyReverse(object param)
        {
            AssemblyContext db = new AssemblyContext();
            // mAssembly model = param as mAssembly;
            var query = (from dist in db.mAssemblies
                         orderby dist.AssemblyID descending
                         select dist).ToList();


            return query.ToList();
        }


    }
}
