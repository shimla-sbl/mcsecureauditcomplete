﻿namespace SBL.Domain.Context.Session
{
    #region Namespace Reffrences
    using SBL.DAL;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Bill;
    using SBL.DomainModel.Models.Committee;
    using SBL.DomainModel.Models.Session;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using SBL.DomainModel.Models.Speaker;


    #endregion

    public class SessionContext : DBBase<SessionContext>
    {
        public SessionContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<mSession> mSession { get; set; }
        public DbSet<mSessionDate> mSessDate { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mSessionType> mSessionTypes { get; set; }
        public DbSet<mspeakerPadPdf> SpeakerPadPdf { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "UpdateSessionData":
                    {
                        return UpdateSessionData(param.Parameter);
                    }
                case "DeleteSession":
                    {
                        return DeleteSession(param.Parameter);
                    }
                case "GetSessionCodeById":
                    {
                        return GetSessionCodeById(param.Parameter);
                    }

                case "GetSessionCodeByIdforUpdate":
                    {
                        return GetSessionCodeByIdforUpdate(param.Parameter);
                    }
                case "GetAllSessionTypes":
                    {
                        return GetAllSessionTypes();
                    }
                case "GetAllSession":
                    {
                        return GetAllSession();
                    }
                case "GetAllSessionData":
                    {
                        return GetAllSessionData(param.Parameter);
                    }
                case "CreateSession":
                    {
                        return CreateSession(param.Parameter);
                    }
                case "EditSession":
                    {
                        return "";//EditSession(param.Parameter);
                    }
                case "GetSessionDataById":
                    {
                        return GetSessionDataById(param.Parameter);
                    }
                case "GetSessionById":
                    {
                        return GetSessionById(param.Parameter);
                    }
                case "GetSessionsByAssemblyID":
                    {
                        return GetSessionsByAssemblyID(param.Parameter);
                    }
                case "GetAssemblySession":
                    {
                        return GetAssemblySession(param.Parameter);
                    }
                case "GetSessionsDateBySessionID":
                    {
                        return GetSessionDateBySessionID(param.Parameter);
                    }

                case "GetSessionDateCurrentSessionID":
                    {
                        return GetSessionDateCurrentSessionID(param.Parameter);
                    }
                case "GetSessionDate":
                    {
                        return GetSessionDate(param.Parameter);
                    }
                case "GetSessionDateAssembly":
                    {
                        return GetSessionDateAssembly(param.Parameter);
                    }
                case "GetSessionNameBySessionCode":
                    {
                        return GetSessionNameBySessionCode(param.Parameter);
                    }
                case "GetSessionDateBySessionCode":
                    {
                        return GetSessionDateBySessionCode(param.Parameter);
                    }
                case "GetSessionsByAssemblyIDReverse":
                    {
                        return GetSessionsByAssemblyIDReverse(param.Parameter);
                    }
                case "GetSessionNameLocalBySessionCode":
                    {
                        return GetSessionNameLocalBySessionCode(param.Parameter);
                    }
                case "GetSessionNameBySessionCodeHindi":
                    {
                        return GetSessionNameBySessionCodeHindi(param.Parameter);
                    }
                #region ForSessionDates
                case "CreateSessionDates": { CreateSessionDates(param.Parameter); break; }
                case "UpdateSessionDates": { return UpdateSessionDates(param.Parameter); }
                case "DeleteSessionDates": { return DeleteSessionDates(param.Parameter); }
                case "GetAllSessionDates": { return GetAllSessionDates(); }
                case "GetSessionDatesById": { return GetSessionDatesById(param.Parameter); }
                case "GetAllSessionNames": { return GetAllSessionNames(); }
                case "GetAllAssembly": { return GetAllAssembly(); }
                case "IsSessionIdChildExist": { return IsSessionIdChildExist(param.Parameter); }
                #endregion

#region by robin for bulletin
                case "sessionstartdate": { return sessionstartdate(param.Parameter); }
                case "sessionEnddate": { return sessionEnddate(param.Parameter); }
#endregion
//by robin
                case "GetLatestSessionDates": { return GetLatestSessionDates(param.Parameter); }

                case "Get_SpeakerPadPdfList": { return Get_SpeakerPadPdfList(param.Parameter); }
                case "GetSessionDateForPdf": { return GetSessionDateForPdf(param.Parameter); }
                case "NewSpeakerPad_Pdf": { return NewSpeakerPad_Pdf(param.Parameter); }
                case "Chk_PdfVersion": { return Chk_PdfVersion(param.Parameter); }
                case "GetSpeakrPdfData_ID": { return GetSpeakrPdfData_ID(param.Parameter); }
            }
            return null;
        }
        static List<mAssembly> GetAllAssembly()
        {
            try
            {
                using (SessionContext db = new SessionContext())
                {
                    var data = (from a in db.mAssembly
                                orderby a.AssemblyCode
                                where a.IsDeleted == null
                                select a).ToList();
                    return data;

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #region ForSessionDates

        private static object IsSessionIdChildExist(object param)
        {
            try
            {
                using (SessionContext db = new SessionContext())
                {
                    mSession model = (mSession)param;
                    var Res = (from e in db.mSessDate
                               where (e.SessionId == model.SessionID)
                               select e).Count();

                    if (Res == 0)
                    {

                        return false;


                    }

                    else
                    {
                        return true;

                    }

                }
                //return null;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateSessionDates(object param)
        {
            try
            {
                using (SessionContext db = new SessionContext())
                {
                    mSessionDate model = param as mSessionDate;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mSessDate.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        //static object UpdateSessionDates(object param)
        //{
        //    if (null == param)
        //    {
        //        return null;
        //    }
        //    using (SessionContext db = new SessionContext())
        //    {
        //        mSessionDate model = param as mSessionDate;
        //        model.CreatedDate = DateTime.Now;
        //        model.ModifiedWhen = DateTime.Now;
        //        db.mSessDate.Attach(model);
        //        db.Entry(model).State = EntityState.Modified;
        //        db.SaveChanges();
        //        db.Close();
        //    }
        //    return GetAllSessionDates();
        //}
        static object UpdateSessionDates(object param)
        {
            if (null == param)
            {
                return null;
            }
            //using (SessionContext db = new SessionContext())
            //{
            //    mSessionDate model = param as mSessionDate;
            //    model.CreatedDate = DateTime.Now;
            //    model.ModifiedWhen = DateTime.Now;
            //    db.mSessDate.Attach(model);
            //    db.Entry(model).State = EntityState.Modified;
            //    db.SaveChanges();
            //    db.Close();
            //}
            mSessionDate parameter = param as mSessionDate;
            using (SessionContext ctx = new SessionContext())
            {
                var query = (from q in ctx.mSessDate
                             where q.Id == parameter.Id
                             select q).First();
                query.AssemblyId = parameter.AssemblyId;
                query.SessionId = parameter.SessionId;
                query.SessionDate = parameter.SessionDate;
                query.SessionDateLocal = parameter.SessionDateLocal;
                query.SessionDate_Local = parameter.SessionDate_Local;
                query.SessionTime = parameter.SessionTime;
                query.SessionTimeLocal = parameter.SessionTimeLocal;
                query.SessionEndTime = parameter.SessionEndTime;
                query.SessionEndTimeLocal = parameter.SessionEndTimeLocal;

                query.IsSitting = parameter.IsSitting;
                query.ModifiedWhen = System.DateTime.Now;
                ctx.SaveChanges();
                //ctx.Close();
                // int result = ctx.SaveChanges();
                // return result;
            }
            return GetAllSessionDates();
        }

        static object DeleteSessionDates(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SessionContext db = new SessionContext())
            {
                mSessionDate parameter = param as mSessionDate;
                mSessionDate typeToRemove = db.mSessDate.SingleOrDefault(a => a.Id == parameter.Id);
                //if (typeToRemove != null)
                //{
                //    typeToRemove.IsDeleted = true;
                //}
                db.mSessDate.Remove(typeToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllSessionDates();
        }

        static List<mSessionDate> GetAllSessionDates()
        {
            SessionContext db = new SessionContext();

            //var data = (from SessDate in db.mSessDate
            //            join Assem in db.mAssembly on SessDate.AssemblyId equals Assem.AssemblyCode into JoinAssemblyName
            //            from assemb in JoinAssemblyName.DefaultIfEmpty()
            //            join Session in db.mSession on SessDate.SessionId equals Session.SessionCode into JoinSessionName

            //            from session in JoinSessionName.DefaultIfEmpty()
            //            where SessDate.IsDeleted == null
            //            select new
            //            {
            //                SessDate,
            //                assemb.AssemblyName,
            //                session.SessionName
            //            }).ToList();

            var data1=(from SessDate in db.mSessDate                       
                        where SessDate.IsDeleted==null
                        select new {
                             SessDate,
                            AssemblyName=(from m in db.mAssembly where m.AssemblyCode==SessDate.AssemblyId select m.AssemblyName).FirstOrDefault(),
                            SessionName=(from t in db.mSession where t.SessionCode== SessDate.SessionId && t.AssemblyID==SessDate.AssemblyId select t.SessionName).FirstOrDefault()

                        }).ToList();



            //var data = (from SessDate in db.mSessDate
            //            join Session in db.mSession on

            //            new 
            //            { SessionCode = SessDate.SessionId, assemblyId = SessDate.AssemblyId } 
            //            equals new { SessionCode = Session.SessionCode, assemblyId = Session.AssemblyID }

            //            join Assem in db.mAssembly 
            //            on SessDate.AssemblyId equals Assem.AssemblyCode into JoinAssemblyName
            //            from assemb in JoinAssemblyName.DefaultIfEmpty()
            //            where SessDate.IsDeleted == null
            //            select new
            //            {
            //                SessDate,
            //                assemb.AssemblyName,
            //                Session.SessionName
            //            }).ToList();




            List<mSessionDate> list = new List<mSessionDate>();
            foreach (var item in data1)
            {
                item.SessDate.GetAssemblyName = item.AssemblyName;
                item.SessDate.GetSessionName = item.SessionName;
                list.Add(item.SessDate);
            }

            return list.OrderByDescending(q => q.Id).ToList();
        }

        public static List<mSessionDate> GetLatestSessionDates(object param)
        {
            SessionContext db = new SessionContext();
           mSessionDate returntype = param as mSessionDate;
            
            var data1 = (from SessDate in db.mSessDate
                         where SessDate.IsDeleted == null && SessDate.AssemblyId == returntype.AssemblyId && SessDate.SessionId == returntype.SessionId
                         select new
                         {
                             SessDate,
                             AssemblyName = (from m in db.mAssembly where m.AssemblyCode == SessDate.AssemblyId select m.AssemblyName).FirstOrDefault(),
                             SessionName = (from t in db.mSession where t.SessionCode == SessDate.SessionId && t.AssemblyID == SessDate.AssemblyId select t.SessionName).FirstOrDefault()

                         }).ToList();

            List<mSessionDate> list = new List<mSessionDate>();
            foreach (var item in data1)
            {
                item.SessDate.GetAssemblyName = item.AssemblyName;
                item.SessDate.GetSessionName = item.SessionName;
                list.Add(item.SessDate);
            }

            return list.OrderByDescending(q => q.Id).ToList();
        }

        static List<mSession> GetAllSessionNames()
        {
            SessionContext db = new SessionContext();
            var data = (from sd in db.mSession
                        orderby sd.SessionCode ascending
                        where sd.IsDeleted == null
                        select sd).ToList();
            return data;
        }

        static mSessionDate GetSessionDatesById(object param)
        {
            mSessionDate parameter = param as mSessionDate;
            SessionContext db = new SessionContext();
            var query = db.mSessDate.SingleOrDefault(a => a.Id == parameter.Id);
            return query;
        }

        #endregion

        static object UpdateSessionData(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SessionContext db = new SessionContext())
            {
                mSession model = param as mSession;
                model.CreatedDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;
                db.mSession.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllSession();

        }

        static object DeleteSession(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SessionContext db = new SessionContext())
            {
                mSession parameter = param as mSession;
                mSession sessionToRemove = db.mSession.SingleOrDefault(a => a.SessionID == parameter.SessionID);
                //if(sessionToRemove!=null)
                //{
                //    sessionToRemove.IsDeleted = true;
                //}
                db.mSession.Remove(sessionToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllSession();
        }

        static int GetSessionCodeById(object param)
        {
            try
            {
                SessionContext db = new SessionContext();
                //var data = db.mSession.ToList();
                int id = Convert.ToInt32(param);
                var data = (from s in db.mSession

                            where s.AssemblyID == id
                            select s).ToList();

                if (data.Count > 0)
                {
                    //var data1 = data.LastOrDefault();
                    //return data1.SessionCode;
                    var query = data.Max(x => x.SessionCode);
                    return query;

                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static List<mSessionType> GetAllSessionTypes()
        {
            SessionContext db = new SessionContext();
            var data = (from s in db.mSessionTypes
                        orderby s.TypeName ascending
                        where s.IsDeleted == null
                        select s).ToList();
            return data;
            //var data = db.mSessionTypes.ToList();
            //return data;
        }

        static object GetSessionNameLocalBySessionCode(object param)
        {
            SessionContext sessionContext = new SessionContext();
            mSession Mdl = param as mSession;

            string SessionNameLocal = (from m in sessionContext.mSession where m.SessionCode == Mdl.SessionCode && m.AssemblyID == Mdl.AssemblyID select m.SessionNameLocal).SingleOrDefault();
            return SessionNameLocal;

        }

        public static object GetSessionNameBySessionCode(object param)
        {
            SessionContext sessionContext = new SessionContext();
            mSession Mdl = param as mSession;

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == Mdl.SessionCode && m.AssemblyID == Mdl.AssemblyID select m.SessionName).SingleOrDefault();
            return SessionName;

        }
        static object GetSessionDateAssembly(object param)
        {

            SessionContext sessionContext = new SessionContext();
            mSessionDate s = param as mSessionDate;
            var SessionDateList = (from m in sessionContext.mSessDate
                                   where (m.AssemblyId == s.AssemblyId)
                                   orderby m.SessionDate ascending
                                   //where (m.SessionId == 2)
                                   select m).ToList();
            List<mSessionDate> ListSession = new List<mSessionDate>();


            foreach (var item in SessionDateList)
            {
                mSessionDate test = new mSessionDate();
                test.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");
                test.SessionDate = item.SessionDate;
                test.Id = item.Id;
                ListSession.Add(test);
            }


            return ListSession;
        }
        static object GetSessionDate(object param)
        {

            SessionContext sessionContext = new SessionContext();
            mSessionDate s = param as mSessionDate;
            var SessionDateList = (from m in sessionContext.mSessDate
                                   where (m.SessionId == s.SessionId && m.AssemblyId == s.AssemblyId)
                                   //where (m.SessionId == 2)
                                   orderby m.SessionDate ascending
                                   select m).ToList();
            List<mSessionDate> ListSession = new List<mSessionDate>();


            foreach (var item in SessionDateList)
            {
                mSessionDate test = new mSessionDate();

                string Month = item.SessionDate.Month.ToString();
                string Day = item.SessionDate.Day.ToString();
                string Year = item.SessionDate.Year.ToString();

                var DateString = Day + "-" + Month + "-" + Year;
                test.DisplayDate = DateString;
                test.SessionDate = item.SessionDate;
                test.Id = item.Id;
                ListSession.Add(test);
            }


            return ListSession;
        }

        #region GetdSessionData

        static object GetAssemblySession(object param)
        {

            SessionContext sessionContext = new SessionContext();
            mSession s = param as mSession;
            var session = (from m in sessionContext.mSession
                           where (!(s.AssemblyID != 0) || m.AssemblyID == s.AssemblyID)
                           orderby m.SessionID descending
                           select m).ToList();
            tBillRegister obj = new tBillRegister();
            obj.mAssemblySession = session;
            return obj;
        }

        static List<mSession> GetSessionsByAssemblyID(object param)
        {
            SessionContext db = new SessionContext();
            mSession model = param as mSession;
            var query = (from dist in db.mSession
                         where dist.AssemblyID == model.AssemblyID
                         orderby dist.SessionID descending
                         select dist);

            return query.ToList();
        }

        static List<mSession> GetAllSession()
        {
            //SessionContext db = new SessionContext();           
            //var query = (from dist in db.mSession 
            //             orderby dist.SessionCode ascending
            //             select dist);
            //var query = db.mSession.OrderByDescending(a => a.SessionID);
            //return query.ToList();
            try
            {
                using (SessionContext db = new SessionContext())
                {
                    var data = (from Session in db.mSession
                                join Assem in db.mAssembly on Session.AssemblyID equals Assem.AssemblyCode into JoinAssemblyName
                                from assemb in JoinAssemblyName.DefaultIfEmpty()
                                join SessionType in db.mSessionTypes on Session.SessionType equals SessionType.TypeID into JoinSessionName
                                from sessionty in JoinSessionName.DefaultIfEmpty()
                                where Session.IsDeleted == null
                                select new
                                {
                                    Session,
                                    assemb.AssemblyName,
                                    sessionty.TypeName

                                });
                    List<mSession> list = new List<mSession>();
                    foreach (var item in data)
                    {
                        item.Session.GetAssemblyName = item.AssemblyName;
                        item.Session.GetSessionType = item.TypeName;
                        list.Add(item.Session);
                    }
                    return list.OrderByDescending(s => s.SessionID).ToList();

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static List<mSession> GetAllSessionData(object param)
        {
            SessionContext db = new SessionContext();
            mSession model = param as mSession;
            var query = (from dist in db.mSession
                         select dist);
            string searchText = (model.SessionName != null && model.SessionName != "") ? model.SessionName.ToLower() : "";
            if (searchText != "")
            {
                query = query.Where(a =>
                    a.SessionName != null && a.SessionName.ToLower().Contains(searchText));

            }

            return query.ToList();
        }

        static mSession GetSessionDataById(object param)
        {
            mSession Mdl = param as mSession;
            SessionContext db = new SessionContext();
            var query = db.mSession.SingleOrDefault(a => a.SessionCode == Mdl.SessionCode);
            return query;
        }

        static mSession GetSessionById(object param)
        {
            mSession Mdl = param as mSession;
            SessionContext db = new SessionContext();
            var query = db.mSession.SingleOrDefault(a => a.SessionID == Mdl.SessionID);
            return query;
        }

        static int GetSessionCodeByIdforUpdate(object param)
        {
            try
            {
                SessionContext db = new SessionContext();
                int id = Convert.ToInt32(param);
                var query1 = db.mSession.SingleOrDefault(a => a.SessionID == id);
                return query1.SessionCode;

            }
            catch (Exception ex)
            {

                throw ex;
            }


        }
        #endregion

        static object CreateSession(object param)
        {
            if (null == param)
            {
                return null;
            }
            mSession Mdl = param as mSession;
            //var sId = GetLatestSessionId(Mdl.AssemblyID);
            //Mdl.SessionID = sId + 1;
            using (SessionContext db = new SessionContext())
            {

                if (Mdl != null)
                {
                    Mdl.CreatedDate = DateTime.Now;
                    Mdl.ModifiedWhen = DateTime.Now;
                    db.mSession.Add(Mdl);
                    db.SaveChanges();
                }
                else
                {
                    return null; //null means already exists
                }
                db.Close();

            }
            return Mdl;
        }

        static int GetLatestSessionId(int AssemblyId)
        {
            SessionContext db = new SessionContext();
            var query = db.mSession.OrderByDescending(a => a.AssemblyID == AssemblyId).FirstOrDefault();
            return query.SessionID;
        }

        static object GetSessionDateBySessionID(object param)
        {
            SessionContext db = new SessionContext();
            int SessionId = (int)param;
            var SessDate = (from dist in db.mSessDate
                            where dist.SessionId == SessionId
                            select dist).ToList();

            foreach (var item in SessDate)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }

            return SessDate;


        }

        static object GetSessionDateCurrentSessionID(object param)
        {
            SessionContext db = new SessionContext();
            tCommitteeModel s = param as tCommitteeModel;
           
            var SessDate = (from dist in db.mSessDate
                            where dist.SessionId == s.SessionId
                            && dist.AssemblyId==s.AssemblyId
                            select dist).ToList();

            foreach (var item in SessDate)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }

            return SessDate;


        }


        static List<mSessionDate> GetSessionDateBySessionCode(object param)
        {
            SessionContext db = new SessionContext();
            mSession Session = (mSession)param;
            var SessDates = (from SessDate in db.mSessDate
                             where SessDate.SessionId == Session.SessionCode && SessDate.AssemblyId == Session.AssemblyID && (SessDate.IsDeleted == null || SessDate.IsDeleted == false)
                             orderby (SessDate.SessionDate)
                             select SessDate).ToList();


            return SessDates;
        }

        static List<mSession> GetSessionsByAssemblyIDReverse(object param)
        {
            SessionContext db = new SessionContext();
            mSession model = param as mSession;
            var query = (from dist in db.mSession
                         orderby dist.SessionCode descending
                         where dist.AssemblyID == model.AssemblyID
                         select dist);

            return query.ToList();
        }

        static object GetSessionNameBySessionCodeHindi(object param)
        {
            SessionContext sessionContext = new SessionContext();
            mSession Mdl = param as mSession;

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == Mdl.SessionCode && m.AssemblyID == Mdl.AssemblyID select m.SessionNameLocal).SingleOrDefault();
            return SessionName.Split('(')[0];

        }


        public static object sessionstartdate(object param)
        {
            SessionContext db = new SessionContext();
            mSession obj = param as mSession;
            //int SessionId = (int)param;
            var SessDate = (from dist in db.mSession
                            where dist.SessionCode == obj.SessionCode && dist.AssemblyID == obj.AssemblyID
                            select dist.StartDate).FirstOrDefault();

            //SessDate.StartDate = SessDate.StartDate.ToString("dd/MM/yyyy");

            return SessDate;
        }

        public static object sessionEnddate(object param)
        {
            SessionContext db = new SessionContext();
            mSession obj = param as mSession;
            //int SessionId = (int)param;
            var SessDate = (from dist in db.mSession
                            where dist.SessionCode == obj.SessionCode && dist.AssemblyID == obj.AssemblyID
                            select dist.EndDate).FirstOrDefault();

            //SessDate.EndDate = SessDate.SessionDate.ToString("dd/MM/yyyy");

            return SessDate;
        }


        public static object Get_SpeakerPadPdfList(object param)
        {
            mspeakerPadPdf val = param as mspeakerPadPdf;
            using (SessionContext ctx = new SessionContext())
            {
                var query = (from a in ctx.SpeakerPadPdf
                             //  join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                             //from ps in temp.DefaultIfEmpty()

                             // where a.AssemblyId == val.AssemblyId && a.SessionId == val.SessionId
                             select new
                             {
                                 Id = a.Id,
                                 Pdfvesion = a.Pdfvesion,
                                 // Department = ps.deptname ?? "OTHER",
                                 Pdfpath = a.Pdfpath,
                                 PdfDateTime = a.PdfDateTime,
                                 SessionDates = a.SessionDates
                                 // EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                 // AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),

                             }).ToList().OrderByDescending(a => a.Id); //  (a => a.DraftId;.eFileAttachmentId)
                List<mspeakerPadPdf> lst = new List<mspeakerPadPdf>();
                foreach (var item in query)
                {
                    mspeakerPadPdf mdl = new mspeakerPadPdf();
                    mdl.Id = item.Id;
                    mdl.Pdfvesion = item.Pdfvesion;
                    mdl.Pdfpath = item.Pdfpath;
                    mdl.PdfDateTime = item.PdfDateTime;
                    mdl.SessionDates = item.SessionDates;
                    lst.Add(mdl);
                }
                return lst;

            }
        }

        public static object GetSessionDateForPdf(object param)
        {
            mspeakerPadPdf val = param as mspeakerPadPdf;
            Int32 assId = Convert.ToInt32(val.AssemblyId);
            Int32 SessionID = Convert.ToInt32(val.SessionId);
            using (SessionContext ctx = new SessionContext())
            {
                var query = (from a in ctx.mSessDate
                             //  join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                             //from ps in temp.DefaultIfEmpty()
                             where a.AssemblyId == assId && a.SessionId == SessionID
                             select new
                             {
                                 Id = a.Id,
                                 SessionDate = a.SessionDate,

                             }).ToList().OrderByDescending(a => a.Id); //  (a => a.DraftId;.eFileAttachmentId)
                List<mspeakerPadPdf> lst = new List<mspeakerPadPdf>();
                foreach (var item in query)
                {
                    mspeakerPadPdf mdl = new mspeakerPadPdf();
                    mdl.Id = item.Id;
                    mdl.SessionDates = item.SessionDate.ToString("dd-MM-yyyy");
                    lst.Add(mdl);
                }
                return lst;

            }
        }
        static object NewSpeakerPad_Pdf(object param)
        {
            if (null == param)
            {
                return 0;
            }
            mspeakerPadPdf Mdl = param as mspeakerPadPdf;
            //var sId = GetLatestSessionId(Mdl.AssemblyID);
            //Mdl.SessionID = sId + 1;
            using (SessionContext db = new SessionContext())
            {
                if (Mdl != null)
                {
                    if (Mdl.Id == 0)
                    {
                        Mdl.PdfDateTime = DateTime.Now;
                        db.SpeakerPadPdf.Add(Mdl);
                        db.SaveChanges(); //pp
                        return 1;
                    }
                    else
                    {
                        SBL.DomainModel.Models.Speaker.mspeakerPadPdf obj = db.SpeakerPadPdf.Single(m => m.Id == Mdl.Id);
                        obj.PdfDateTime = DateTime.Now;
                        obj.Pdfpath = Mdl.Pdfpath;
                        db.SaveChanges();
                        return 1;
                    }
                }
                else
                {
                    return 0;
                }
                // db.Close();
            }
            // return Mdl;
        }
        static int Chk_PdfVersion(object param)
        {
            try
            {
                SessionContext db = new SessionContext();
                //var data = db.mSession.ToList();
                mspeakerPadPdf Mdl = param as mspeakerPadPdf;

                var data = (from s in db.SpeakerPadPdf

                            where s.SessionDates == Mdl.SessionDates
                            select s).ToList();

                if (data.Count > 0)
                {
                    //var data1 = data.LastOrDefault();
                    //return data1.SessionCode;
                    var query = data.Max(x => x.Pdfvesion);
                    return query;

                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public static object GetSpeakrPdfData_ID(object param)
        {
            mspeakerPadPdf val = param as mspeakerPadPdf;

            using (SessionContext ctx = new SessionContext())
            {
                var query = (from a in ctx.SpeakerPadPdf

                             where a.Id == val.Id
                             select new
                             {
                                 Id = a.Id,
                                 SessionDates = a.SessionDates,
                                 Pdfpath = a.Pdfpath,
                                 Pdfvesion = a.Pdfvesion,
                             }).ToList().OrderByDescending(a => a.Id); //  (a => a.DraftId;.eFileAttachmentId)
                List<mspeakerPadPdf> lst = new List<mspeakerPadPdf>();
                foreach (var item in query)
                {
                    mspeakerPadPdf mdl = new mspeakerPadPdf();
                    mdl.Id = item.Id;
                    mdl.SessionDates = item.SessionDates;
                    mdl.Pdfpath = item.Pdfpath;
                    mdl.IsPdf = "Y";
                    mdl.Pdfvesion = item.Pdfvesion;
                    lst.Add(mdl);
                }
                return lst;

            }
        }
    }
}
