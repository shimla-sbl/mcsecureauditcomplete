﻿using SBL.Service.Common;
using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SBL.DomainModel.Models.SiteSetting;
using SBL.Domain.Context.Department;
using SBL.Domain.Context.Session;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Notice;
using System.Text.RegularExpressions;
using SBL.DomainModel.ComplexModel;
using System.Data;
using SBL.DomainModel.Models.Enums;
using SBL.Domain.Context.BillPaperLaid;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.User;
using SBL.Domain.Context.SiteSetting;
using SBL.Domain.Context.Assembly;
using SBL.Domain.Context.UserManagement;
using SBL.DomainModel.Models.UserModule;
using SBL.DomainModel.Models.Departmentpdfpath;
using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.DomainModel.Models.LOB;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.SmsGateway;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.ROM;
using SBL.Domain.Context.Notice;
namespace SBL.Domain.Context.paperLaid
{
    public class PaperLaidContext : DBBase<PaperLaidContext>
    {
        public PaperLaidContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mMember> mMember { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<mDepartmentPdfPath> mDepartmentPdfPath { get; set; }
        public DbSet<mSession> mSession { get; set; }
        public virtual DbSet<SiteSettings> mSiteSettings { get; set; }

        //Pending List
        public DbSet<mPaperCategoryType> mPaperCategoryType { get; set; }
        public DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }
        public DbSet<tOneTimeUserRegistration> tOneTimeUserRegistration { get; set; }
        public DbSet<tUserRegistrationDetails> tUserRegistrationDetails { get; set; }
        public DbSet<tTempMobileUser> tTempMobileUser { get; set; }
        public DbSet<AdhaarDetails> AdhaarDetails { get; set; }


        public DbSet<mEvent> mEvent { get; set; }
        public DbSet<tPaperLaidV> tPaperLaidV { get; set; }

        //PaperLaidEntry
        public DbSet<mDocumentType> mDocumentTypes { get; set; }
        public DbSet<mSessionDate> mSessionDates { get; set; }
        public DbSet<mBillType> mBillTypes { get; set; }

        public DbSet<tQuestion> tQuestions { get; set; }
        public DbSet<tOTPRegistrationAuditTrial> tOTPRegistrationAuditTrial { get; set; }

        public DbSet<mchangeDepartmentAuditTrail> mchangeDepartmentAuditTrails { get; set; }
        public DbSet<tQuestionType> tQuestionTypes { get; set; }
        public DbSet<mNoticeType> mNoticeTypes { get; set; }
        public DbSet<tMemberNotice> tMemberNotice { get; set; }
        public DbSet<mMinistry> mMinistry { get; set; }
        public DbSet<mMinistryDepartment> mMInistryDepartment { get; set; }
        public DbSet<tBillRegister> tBillRegister { get; set; }
        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public DbSet<mBills> tBills { get; set; }

        public DbSet<tSmsGateway> tSmsGateway { get; set; }

        //New Changes
        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<AuthorisedEmployee> AuthorisedEmployee { get; set; }
        public DbSet<tAssemblyFiles> tAssemblyFiles { get; set; }
        public virtual DbSet<AdminLOB> AdminLOB { get; set; }
        public virtual DbSet<tAuditTrial> tAuditTrial { get; set; }

        //For LOB New
        public virtual DbSet<tRotationMinister> tRotationMinister { get; set; }
        public virtual DbSet<tCommitteeReport> tCommitteeReport { get; set; }
        public DbSet<OnlineNotices> OnlineNotices { get; set; }
        public virtual DbSet<DraftLOB> DraftLOB { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "OTPDiaryList":
                    {
                        return OTPDiaryList(param.Parameter);
                    }
                case "OTPViewListList":
                    {
                        return OTPViewListList(param.Parameter);
                    }

                case "CheckMobileNo":
                    {
                        return CheckMobileNo(param.Parameter);
                    }

                case "OTPRegistarionDetailsList":
                    {
                        return OTPRegistarionDetailsList(param.Parameter);
                    }
                case "OTPMobileList":
                    {
                        return OTPMobileList(param.Parameter);
                    }

                case "GetMinisterMinistryName":
                    {
                        return GetMinisterMinistryName(param.Parameter);

                    }



                case "CheckMobileNoExist":
                    {
                        return CheckMobileNoExist(param.Parameter);
                    }
                case "GetPaperLaidDepartmentDetails":
                    {
                        return GetPaperLaidDepartmentDetails(param.Parameter);
                    }
                case "GetAllPaperCategoryType":
                    {
                        return GetAllPaperCategoryType(param.Parameter);
                    }
                case "GetPendingDepartmentPaperLaidList":
                    {
                        return GetPendingDepartmentPaperLaidList(param.Parameter);
                    }
                case "GetPaperLaidByEventId":
                    {
                        return GetPaperLaidByEventId(param.Parameter);
                    }
                case "GetPaperLaidByDeptId":
                    {
                        return GetPaperLaidByDeptId(param.Parameter);
                    }
                case "GetPaperByPaperIdAndPriority":
                    {
                        return GetPaperByPaperIdAndPriority(param.Parameter);
                    }
                case "GetPaperLaidByPaperIdAndPriority":
                    {
                        return GetPaperLaidByPaperIdAndPriority(param.Parameter);
                    }
                case "GetPaperLaidByEventIdAndPriorty":
                    {
                        return GetPaperLaidByEventIdAndPriorty(param.Parameter);
                    }
                case "GetPaperLaidByPaperIdAndWithNoPriority":
                    {
                        return GetPaperLaidByPaperIdAndWithNoPriority(param.Parameter);
                    }
                case "GetPaperByPaperIdWithNoPriority":
                    {
                        return GetPaperByPaperIdWithNoPriority(param.Parameter);
                    }
                case "GetPaperLaidByEventIdWithNoPriority":
                    {
                        return GetPaperLaidByEventIdWithNoPriority(param.Parameter);
                    }
                case "GetPaperLaidTempById":
                    {
                        return GetPaperLaidTempById(param.Parameter);
                    }
                case "UpdateLOBRecordIdIntPaperLaidVS":
                    {
                        return UpdateLOBRecordIdIntPaperLaidVS(param.Parameter);
                    }
                //                    updated by nitin
                case "GetPaperLaidCategoryDetails":
                    {
                        return GetPaperLaidCategoryDetails(param.Parameter);
                    }
                case "GetDocumentType":
                    {
                        return GetDocumentType(param.Parameter);
                    }
                case "GetSessionDates":
                    {
                        return GetSessionDates(param.Parameter);
                    }
                case "GetSessionDate":
                    {
                        return GetSessionDate();
                    }
                case "GetQuestionNumber":
                    {
                        return GetQuestionNumber(param.Parameter);
                    }
                case "GetNotices":
                    {
                        return GetNotices(param.Parameter);
                    }
                case "SubmittPaperLaidEntry":
                    {
                        return SubmittPaperLaidEntry(param.Parameter);
                    }
                case "SubmittOTPEntry":
                    {
                        return SubmittOTPEntry(param.Parameter);
                    }

                case "UserRegistrationDetails":
                    {
                        return UserRegistrationDetails(param.Parameter);
                    }

                case "SubmittMobileUser":
                    {
                        return SubmittMobileUser(param.Parameter);
                    }
                case "GetFileSaveDetails":
                    {
                        return GetFileSaveDetails(param.Parameter);
                    }
                case "GetTempDocFile":
                    {
                        return GetTempDocFile(param.Parameter);
                    }

                case "SaveDetailsInTempPaperLaid":
                    {
                        return SaveDetailsInTempPaperLaid(param.Parameter);
                    }
                case "UpdateDeptActiveId":
                    {
                        return UpdateDeptActiveId(param.Parameter);
                    }
                case "GetQuestionDetails":
                    {
                        return GetQuestionDetails(param.Parameter);
                    }
                case "UpdateQuestionPaperLaidId":
                    {
                        return UpdateQuestionPaperLaidId(param.Parameter);
                    }

                case "UpdateLocked":
                    {
                        return UpdateLocked(param.Parameter);
                    }


                case "UpdateDepartmentId":
                    {
                        return UpdateDepartmentId(param.Parameter);
                    }

                case "UpdateNoticeDepartmentId":
                    {
                        return UpdateNoticeDepartmentId(param.Parameter);
                    }

                case "UpdateNoticeDepartmentMinistryId":
                    {
                        return UpdateNoticeDepartmentMinistryId(param.Parameter);
                    }


                case "UpdateQuestionDepartmentMinistryId":
                    {
                        return UpdateQuestionDepartmentMinistryId(param.Parameter);
                    }
                case "UpdateIsAcknowledgmentDateByDiaryNumber":
                    {
                        return UpdateIsAcknowledgmentDateByDiaryNumber(param.Parameter);
                    }

                case "UpdateIsAcknowledgmentDateByNoticeId":
                    {
                        return UpdateIsAcknowledgmentDateByNoticeId(param.Parameter);
                    }
                case "GetNoticeDetails":
                    {
                        return GetNoticeDetails(param.Parameter);
                    }
                case "CheckBillNUmber":
                    {
                        return CheckBillNUmber(param.Parameter);
                    }
                case "UpdateNoticePaperLaidId":
                    {
                        return UpdateNoticePaperLaidId(param.Parameter);
                    }
                case "InsertBillDetails":
                    {
                        return InsertBillDetails(param.Parameter);
                    }
                case "SubmitPendingDepartmentPaperLaid":
                    {
                        return SubmitPendingDepartmentPaperLaid(param.Parameter);
                    }
                case "EditPendingPaperLaidDetials":
                    {
                        return EditPendingPaperLaidDetials(param.Parameter);
                    }
                case "UpdatePaperLaidEntry":
                    {
                        return UpdatePaperLaidEntry(param.Parameter);
                    }

                case "GetExisitngFileDetails":
                    {
                        return GetExisitngFileDetails(param.Parameter);
                    }

                case "UpdateDetailsInTempPaperLaid":
                    {
                        return UpdateDetailsInTempPaperLaid(param.Parameter);
                    }
                case "GetSubmittedPaperLaidList":
                    {
                        return GetSubmittedPaperLaidList(param.Parameter);
                    }
                case "DeletePaperLaidByID":
                    {
                        return DeletePaperLaidByID(param.Parameter);
                    }
                case "UpdatePaperLaidFileByID":
                    {
                        return UpdatePaperLaidFileByID(param.Parameter);
                    }
                case "UpdatePaperLaidOtherFileByID":
                    {
                        return UpdatePaperLaidOtherFileByID(param.Parameter);
                    }

                case "ShowPaperLaidDetailByID":
                    {
                        return ShowPaperLaidDetailByID(param.Parameter);
                    }
                case "GetSubmittedPaperLaidByID":
                    {
                        return GetSubmittedPaperLaidByID(param.Parameter);
                    }
                case "GetFileVersion":
                    {
                        return GetFileVersion(param.Parameter);
                    }
                case "GetPaperLaidById":
                    {
                        return GetPaperLaidById(param.Parameter);
                    }
                case "GetDepartmentNameMinistryDetailsByMinisterID":
                    {
                        return GetDepartmentNameMinistryDetailsByMinisterID(param.Parameter);
                    }
                case "GetPendingQuestionsByType":
                    {
                        return GetPendingQuestionsByType(param.Parameter);
                    }
                case "GetSubmittedQuestionsByType":
                    {
                        return GetSubmittedQuestionsByType(param.Parameter);
                    }
                case "GetLaidInHouseQuestionsByType":
                    {
                        return GetLaidInHouseQuestionsByType(param.Parameter);
                    }
                case "GetPendingForSubQuestionsByType":
                    {
                        return GetPendingForSubQuestionsByType(param.Parameter);
                    }
                case "GetPendingToLayQuestionsByType":
                    {
                        return GetPendingToLayQuestionsByType(param.Parameter);
                    }
                case "GetCountForQuestionTypes":
                    {
                        return GetCountForQuestionTypes(param.Parameter);
                    }
                case "GetQuetionEventID":
                    {
                        return GetQuetionEventID(param.Parameter);
                    }
                case "UpdateMinisterActivePaperId":
                    {
                        return UpdateMinisterActivePaperId(param.Parameter);
                    }
                case "GetQuestionByPaperLaidID":
                    {
                        return GetQuestionByPaperLaidID(param.Parameter);
                    }
                case "GetPaperLaidByPaperLaidId":
                    {
                        return GetPaperLaidByPaperLaidId(param.Parameter);
                    }
                case "UpcomingLOBByQuestionType":
                    {
                        return UpcomingLOBByQuestionType(param.Parameter);
                    }
                case "GetStarredQuestionCounter":
                    {
                        return GetStarredQuestionCounter(param.Parameter);
                    }
                case "GetUnstarredQuestionCounter":
                    {
                        return GetUnstarredQuestionCounter(param.Parameter);
                    }
                case "GetAllQuestions":
                    {
                        return GetAllQuestions(param.Parameter);
                    }
                case "SendingPendingForSubQuestionsByType":
                    {
                        return SendingPendingForSubQuestionsByType(param.Parameter);
                    }
                case "UpdateDepartmentMinisterActivePaperId":
                    {
                        return UpdateDepartmentMinisterActivePaperId(param.Parameter);
                    }
                case "UpdateBillsDepartmentMinisterActivePaperId":
                    {
                        return UpdateBillsDepartmentMinisterActivePaperId(param.Parameter);
                    }

                case "UpdateBillsRegister":
                    {
                        return UpdateBillsRegister(param.Parameter);
                    }
                case "InsertSignPathAttachment":
                    {
                        return InsertSignPathAttachment(param.Parameter);
                    }

                case "InsertSignPathAttachmenttQuestion":
                    {
                        return InsertSignPathAttachmenttQuestion(param.Parameter);
                    }

                case "GeteRefNum":
                    {
                        return GeteRefNum(param.Parameter);
                    }


                case "ShowPapertoSendLaidDetailByID":
                    {
                        return ShowPapertoSendLaidDetailByID(param.Parameter);
                    }

                case "SendingPendingForNoticeByType":
                    {
                        return SendingPendingForNoticeByType(param.Parameter);
                    }
                case "GetActiveDeptActiveId":
                    {
                        return GetActiveDeptActiveId(param.Parameter);
                    }
                case "GetDepartmentProgressCounter":
                    {
                        return GetDepartmentProgressCounter(param.Parameter);
                    }
                case "GetDepartmentDashboardItemsCounter":
                    {
                        return GetDepartmentDashboardItemsCounter(param.Parameter);
                    }
                case "GetDepartmentMinisterDashboardItemsCounter":
                    {
                        return GetDepartmentMinisterDashboardItemsCounter(param.Parameter);
                    }
                case "GetAuthorizedEmployees":
                    {
                        return GetAuthorizedEmployees(param.Parameter);
                    }
                case "getDepartmentIdByQuestionId":
                    {
                        return getDepartmentIdByQuestionId(param.Parameter);
                    }

                case "getDepartmentIdByNoticeId":
                    {
                        return getDepartmentIdByNoticeId(param.Parameter);
                    }

                case "getMinistryIdByNoticeId":
                    {
                        return getMinistryIdByNoticeId(param.Parameter);
                    }

                case "getMinistryIdByQuestionId":
                    {
                        return getMinistryIdByQuestionId(param.Parameter);
                    }


                case "getDepartmentIdByPaperId":
                    {
                        return getDepartmentIdByPaperId(param.Parameter);
                    }
                case "DraftRepliesQuestionsListBeforeSent":
                    {
                        return DraftRepliesQuestionsListBeforeSent(param.Parameter);
                    }


                case "SubmittChangeDeptEntry":
                    {
                        return SubmittChangeDeptEntry(param.Parameter);
                    }

                case "BeforeFixation":
                    {
                        return BeforeFixation(param.Parameter);
                    }
                case "AfterFixation":
                    {
                        return AfterFixation(param.Parameter);
                    }

                case "GetMemberNameByIDs":
                    {
                        return GetMemberNameByIDs(param.Parameter);
                    }
                case "GetDynamicMainMenuByUserId":
                    {
                        return GetDynamicMainMenuByUserId(param.Parameter);
                    }

                case "GetDashBoardValue":
                    {
                        return GetDashBoardValue(param.Parameter);
                    }
                case "GetDashBoardValueLocal":
                    {
                        return GetDashBoardValueLocal(param.Parameter);
                    }
                case "UpdateOneTimeUSerLocked":
                    {
                        return UpdateOneTimeUSerLocked(param.Parameter);
                    }
                case "CheckMobileDiaryExist":
                    {
                        return CheckMobileDiaryExist(param.Parameter);
                    }
                case "SaveDetailsInTempPaperLaidOTP":
                    {
                        return SaveDetailsInTempPaperLaidOTP(param.Parameter);
                    }

                case "CheckExistinigData":
                    {
                        return CheckExistinigData(param.Parameter);
                    }
                case "GetPaperLaidNumberById":
                    {
                        return GetPaperLaidNumberById(param.Parameter);
                    }
                case "GetBillDetails":
                    {
                        return GetBillDetails(param.Parameter);
                    }

                //Added Code venkat for Dynamic Menu
                case "PendingForReplyQuestionsList1":
                    {
                        return PendingForReplyQuestionsList1(param.Parameter);
                    }
                case "DraftRepliesQuestionsList1":
                    {
                        return DraftRepliesQuestionsList1(param.Parameter);
                    }
                case "RepliesSentQuestionsList1":
                    {
                        return RepliesSentQuestionsList1(param.Parameter);
                    }

                case "GetNoticeReplySentCount":
                    {
                        return GetNoticeReplySentCount(param.Parameter);
                    }

                case "GetNoticeULOBList":
                    {
                        return GetNoticeULOBList(param.Parameter);
                    }
                case "GetNoticeLIHList":
                    {
                        return GetNoticeLIHList(param.Parameter);
                    }

                case "GetNoticePLIHList":
                    {
                        return GetNoticePLIHList(param.Parameter);
                    }
                case "GetDraftBillsCount":
                    {
                        return GetDraftBillsCount(param.Parameter);
                    }
                case "GetBillsSentCount":
                    {
                        return GetBillsSentCount(param.Parameter);
                    }
                case "GetDraftOthePapersCountForOtherpaperlaid":
                    {
                        return GetDraftOthePapersCountForOtherpaperlaid(param.Parameter);
                    }
                case "GetSubmittedOtherPaperLaidList": { return GetSubmittedOtherPaperLaidList(param.Parameter); }
                case "GetMemberList":
                    {
                        return GetMemberList(param.Parameter);
                    }
                case "PaperLaidFilePath":
                    {
                        return PaperLaidFilePath(param.Parameter);
                    }

                case "GetFilesDetials":
                    {
                        return GetFilesDetials(param.Parameter);
                    }
                case "GetFilesVersionDetials":
                    {
                        return GetFilesVersionDetials(param.Parameter);
                    }

                case "DraftRepliescount":
                    {
                        return DraftRepliescount(param.Parameter);
                    }

                case "NoticeDraftRepliescount":
                    {
                        return NoticeDraftRepliescount(param.Parameter);
                    }
                case "GetPasswordMobileUser":
                    {
                        return GetPasswordMobileUser(param.Parameter);
                    }
                //added for showlob
                case "GetShowLob":
                    {
                        return GetShowLob(param.Parameter);
                    }
                case "_showSQuestion":
                    {
                        return _showSQuestion(param.Parameter);
                    }
                case "_showUnQuestion":
                    {
                        return _showUnQuestion(param.Parameter);
                    }
                case "_showBillPassedorIntroduced":
                    {
                        return _showBillPassedorIntroduced(param.Parameter);
                    }
                case "_showBreifOfProceeding":
                    {
                        return _showBreifOfProceeding(param.Parameter);
                    }
                case "_showHouseProceeding":
                    {
                        return _showHouseProceeding(param.Parameter);
                    }
                case "_showPostUpon":
                    {
                        return _showPostUpon(param.Parameter);
                    }
                case "_showProvisonalCalender":
                    {
                        return _showProvisonalCalender(param.Parameter);
                    }
                case "_showRotationalMinister":
                    {
                        return _showRotationalMinister(param.Parameter);
                    }
                case "GetAssemblyId":
                    {
                        return GetAssemblyId();
                    }
                case "GetSessionId":
                    {
                        return GetSessionId();
                    }
                case "GetPapersLaid":
                    {
                        return GetPapersLaid(param.Parameter);
                    }
                case "GetFilesDetialsPaperLaid":
                    {
                        return GetFilesDetialsPaperLaid(param.Parameter);
                    }
                case "GetUserNameByAadharId":
                    {
                        return GetUserNameByAadharId(param.Parameter);
                    }
                case "GetLastChangeDepartment":
                    {
                        return GetLastChangeDepartment(param.Parameter);
                    }

                case "GetSMSGatewayList":
                    {
                        return GetSMSGatewayList();
                    }


                case "GetEventNamesList": { return GetEventNamesList(); }

                case "GetSessionDateIdByDate":
                    {
                        return GetSessionDateIdByDate(param.Parameter);
                    }
                case "GetPaperLaidByDeptId2":
                    {
                        return GetPaperLaidByDeptId2(param.Parameter);
                    }
                case "GetMemberByID":
                    {
                        return GetMemberByID(param.Parameter);
                    }
                case "GetSentNotices":
                    {
                        return GetSentNotices(param.Parameter);
                    }
                case "AssemblyCode":
                    {
                        return AssemblyCode(param.Parameter);
                    }
                case "SessionCode":
                    {
                        return SessionCode(param.Parameter);
                    }
                //For LOB New
                case "GetNoticeCount":
                    {
                        return GetNoticeCount(param.Parameter);
                    }
                case "GetOtherpaperCount":
                    {
                        return GetOtherpaperCount(param.Parameter);
                    }
                case "GetCRCount":
                    {
                        return GetCRCount(param.Parameter);
                    }
                case "GetLaidCRCount":
                    {
                        return GetLaidCRCount(param.Parameter);
                    }
                case "GetPendingToLayCRCount":
                    {
                        return GetPendingToLayCRCount(param.Parameter);
                    }
                case "GetForLayingCRCount":
                    {
                        return GetForLayingCRCount(param.Parameter);
                    }
                case "BillspaperCount":
                    {
                        return BillspaperCount(param.Parameter);
                    }
                case "BillsLaidCount":
                    {
                        return BillsLaidCount(param.Parameter);
                    }
                case "BillsPendingToLayCount":
                    {
                        return BillsPendingToLayCount(param.Parameter);
                    }
                case "BillsForLayingCount":
                    {
                        return BillsForLayingCount(param.Parameter);
                    }
                case "GetCommitteeReports":
                    {
                        return GetCommitteeReports(param.Parameter);
                    }
                case "GetNoticeTypeNamebyId":
                    {
                        return GetNoticeTypeNamebyId(param.Parameter);
                    }
                case "GetNoticeDetailsById":
                    {
                        return GetNoticeDetailsById(param.Parameter);
                    }
                case "GetPaperLaidTempId":
                    {
                        return GetPaperLaidTempId(param.Parameter);
                    }
                case "GetOnlineNoticesDetailsById":
                    {
                        return GetOnlineNoticesDetailsById(param.Parameter);
                    }
                case "GetLaidDateById":
                    {
                        return GetLaidDateById(param.Parameter);
                    }
                case "GetNoticeRuleByNoticeTypeID":
                    {
                        return GetNoticeRuleByNoticeTypeID(param.Parameter);
                    }
                case "GetLaidNoticeCount":
                    {
                        return GetLaidNoticeCount(param.Parameter);
                    }
                case "GetPendingtoLayNoticeCount":
                    {
                        return GetPendingtoLayNoticeCount(param.Parameter);
                    }
                case "GetForLayingNoticeCount":
                    {
                        return GetForLayingNoticeCount(param.Parameter);
                    }
                case "GetLaidOtherPaperCount":
                    {
                        return GetLaidOtherPaperCount(param.Parameter);
                    }
                case "GetPendingtoLayOtherPaperCount":
                    {
                        return GetPendingtoLayOtherPaperCount(param.Parameter);
                    }
                case "GetPapersForLayingCount":
                    {
                        return GetPapersForLayingCount(param.Parameter);
                    }

                case "CheckIsLOBAttached":
                    {
                        return CheckIsLOBAttached(param.Parameter);
                    }
                case "UpdateNoticeIsSecCheck":
                    {
                        return UpdateNoticeIsSecCheck(param.Parameter);
                    }
                case "UpdateOtherPaperIsVsSecMark":
                    {
                        return UpdateOtherPaperIsVsSecMark(param.Parameter);
                    }
                case "CheckNextSrNo":
                    {
                        return CheckNextSrNo(param.Parameter);
                    }
                case "CheckPreviousSrNo":
                    {
                        return CheckPreviousSrNo(param.Parameter);
                    }
                case "CheckNextSubSrNo":
                    {
                        return CheckNextSubSrNo(param.Parameter);
                    }
                case "CheckPreviousSubSrNo":
                    {
                        return CheckPreviousSubSrNo(param.Parameter);
                    }
                    //end----------------------------------------------
            }
            return null;
        }






        static object GetLastChangeDepartment(object param)
        {
            mchangeDepartmentAuditTrail tQuestions = new mchangeDepartmentAuditTrail();

            mchangeDepartmentAuditTrail model = param as mchangeDepartmentAuditTrail;


            PaperLaidContext pCtxt = new PaperLaidContext();
            tPaperLaidV paperMdel = new tPaperLaidV();
            var tQuestions1 = (from mdl in pCtxt.mchangeDepartmentAuditTrails
                               where mdl.DiaryNumber == model.DiaryNumber
                               orderby mdl.ChangedDate descending
                               select mdl).FirstOrDefault();

            if (tQuestions1 != null)
            {
                tQuestions.UserName = tQuestions1.UserName;

                tQuestions.ChangedDateString = Convert.ToDateTime(tQuestions1.ChangedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
            }
            else
            {
                tQuestions.UserName = "";
                tQuestions.ChangedDate = null;

            }
            return tQuestions;
        }

        static object GetFilesDetialsPaperLaid(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            var data = (from dept in paperLaidCntxtDB.tMemberNotice where dept.NoticeId == model.NoticeId select dept).ToList();


            var results = data.ToList();
            model.memberNoticeList1 = results;

            return model;
        }
        public static List<AdminLOB> GetPapersLaid(object param)
        {
            PaperLaidContext db = new PaperLaidContext();
            AdminLOB model = param as AdminLOB;
            var lobs = (from msedate in db.AdminLOB
                        where msedate.SessionId == model.SessionId && msedate.AssemblyId == model.AssemblyId && msedate.SessionDate == model.SessionDate && msedate.IsDeleted == false
                        select msedate).OrderBy(c => c.SrNo1).ThenBy(n => n.SrNo2).ThenBy(n => n.SrNo3).ToList();

            return lobs;
        }
        public static List<SiteSettings> GetAssemblyId()
        {
            PaperLaidContext db = new PaperLaidContext();
            var data = db.mSiteSettings.Where(a => a.SettingName == "Assembly").ToList();
            return data;
        }

        public static List<SiteSettings> GetSessionId()
        {
            PaperLaidContext db = new PaperLaidContext();
            var data = db.mSiteSettings.Where(a => a.SettingName == "OldSession").ToList();
            return data;
        }

        public static AssemblySessionFilePath _showProvisonalCalender(object param)
        {
            try
            {
                PaperLaidContext db = new PaperLaidContext();
                //string ids = param as string;
                //string[] array = ids.Split(',');
                //int SessionId = int.Parse(array[0]);
                //int AssemblyId = int.Parse(array[1]);
                mSessionDate model = param as mSessionDate;
                var showProvisonalCalender = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionId == model.SessionId && cutmotions.AssemblyId == model.AssemblyId && cutmotions.TypeofDocumentId == 1 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showProvisonalCalender != null)
                {
                    if (!string.IsNullOrEmpty(showProvisonalCalender.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showProvisonalCalender.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static AssemblySessionFilePath _showRotationalMinister(object param)
        {
            try
            {
                PaperLaidContext db = new PaperLaidContext();
                mSessionDate model = param as mSessionDate;
                var showRotationalMinister = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionId == model.SessionId && cutmotions.AssemblyId == model.AssemblyId && cutmotions.TypeofDocumentId == 2 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showRotationalMinister != null)
                {
                    if (!string.IsNullOrEmpty(showRotationalMinister.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showRotationalMinister.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static AssemblySessionFilePath _showBillPassedorIntroduced(object param)
        {

            try
            {
                PaperLaidContext db = new PaperLaidContext();
                int id = Convert.ToInt32(param);
                var showBillPassedorIntroduced = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionDateId == id && cutmotions.TypeofDocumentId == 6 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showBillPassedorIntroduced != null)
                {
                    if (!string.IsNullOrEmpty(showBillPassedorIntroduced.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showBillPassedorIntroduced.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static AssemblySessionFilePath _showBreifOfProceeding(object param)
        {
            try
            {
                PaperLaidContext db = new PaperLaidContext();
                int id = Convert.ToInt32(param);
                var showBreifOfProceeding = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionDateId == id && cutmotions.TypeofDocumentId == 7 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showBreifOfProceeding != null)
                {
                    if (!string.IsNullOrEmpty(showBreifOfProceeding.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showBreifOfProceeding.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static AssemblySessionFilePath _showHouseProceeding(object param)
        {
            try
            {
                PaperLaidContext db = new PaperLaidContext();
                int id = Convert.ToInt32(param);
                var showHouseProceeding = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionDateId == id && cutmotions.TypeofDocumentId == 8 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showHouseProceeding != null)
                {
                    if (!string.IsNullOrEmpty(showHouseProceeding.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showHouseProceeding.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static AssemblySessionFilePath _showPostUpon(object param)
        {
            try
            {
                PaperLaidContext db = new PaperLaidContext();
                int id = Convert.ToInt32(param);
                var showCutMotions = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionDateId == id && cutmotions.TypeofDocumentId == 9 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showCutMotions != null)
                {
                    if (!string.IsNullOrEmpty(showCutMotions.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showCutMotions.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static AssemblySessionFilePath _showUnQuestion(object param)
        {
            try
            {
                PaperLaidContext db = new PaperLaidContext();
                int id = Convert.ToInt32(param);
                var showUnQuestion = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionDateId == id && cutmotions.TypeofDocumentId == 5 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showUnQuestion != null)
                {
                    if (!string.IsNullOrEmpty(showUnQuestion.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showUnQuestion.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static AssemblySessionFilePath _showSQuestion(object param)
        {
            try
            {
                PaperLaidContext db = new PaperLaidContext();
                int id = Convert.ToInt32(param);
                var showSQuestion = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionDateId == id && cutmotions.TypeofDocumentId == 4 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showSQuestion != null)
                {
                    if (!string.IsNullOrEmpty(showSQuestion.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showSQuestion.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static AssemblySessionFilePath GetShowLob(object param)
        {
            try
            {
                PaperLaidContext db = new PaperLaidContext();
                int id = Convert.ToInt32(param);
                //mSessionDate model = param as mSessionDate;
                var showLOB = (from cutmotions in db.tAssemblyFiles where cutmotions.SessionDateId == id && cutmotions.TypeofDocumentId == 3 select cutmotions).FirstOrDefault();

                AssemblySessionFilePath resultdata = new AssemblySessionFilePath();

                if (showLOB != null)
                {
                    if (!string.IsNullOrEmpty(showLOB.UploadFile))
                    {
                        var fileacesSetting = (from setings in db.mSiteSettings where setings.SettingName == "FileAccessingUrlPathLocal" select setings).FirstOrDefault();
                        resultdata.IsExist = true;
                        resultdata.FilePath = fileacesSetting.SettingValue + showLOB.UploadFile;

                    }
                    else
                    {
                        resultdata.IsExist = false;
                        resultdata.FilePath = null;
                    }
                }
                else
                {
                    resultdata.IsExist = false;
                    resultdata.FilePath = null;
                }


                return resultdata;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        static object GetSubmittedPaperLaidByID(object param)
        {
            PaperLaidContext db = new PaperLaidContext();

            tPaperLaidV model = param as tPaperLaidV;



            var siteSettingData = (from a in db.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;

            model.DepartmentSubmitList = (from tPaper in db.tPaperLaidV
                                          where model.PaperLaidId == tPaper.PaperLaidId
                                          join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                                          join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                                          join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                                          join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
                                          join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                                          select new PaperMovementModel
                                          {
                                              DeptSubmittedDate = tPaperTemp.DeptSubmittedDate,
                                              PaperTypeName = mPaperCategory.Name,
                                              actualFilePath = tPaperTemp.SignedFilePath,
                                              FilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
                                              DeptSubmittedBy = tPaperTemp.DeptSubmittedBy,
                                              PaperLaidId = model.PaperLaidId,
                                              PaperLaidTempId = tPaperTemp.PaperLaidTempId,
                                              DeptActivePaperId = tPaper.DeptActivePaperId,
                                              version = tPaperTemp.Version,
                                              DocFileVersion = tPaperTemp.DocFileVersion,
                                              MergeDiaryNo = tPaper.MergeDiaryNo,
                                              IsClubbed = tPaper.IsClubbed,
                                              SupFilePath = tPaperTemp.FilePath + tPaperTemp.SupFileName,
                                              SupFileVersion = tPaperTemp.SupVersion,
                                              FileStructurePath = FileStructurePath,

                                              DocFilePath = tPaperTemp.FilePath + tPaperTemp.DocFileName

                                          }).ToList();
            int i = 0;


            foreach (var mod in model.DepartmentSubmitList)
            {
                if (i == 0)
                {
                    model.FilePath = mod.FilePath;
                    model.FileStructurePath = FileStructurePath;
                    model.PaperLaidId = mod.PaperLaidId;
                    model.DocFileVersion = mod.DocFileVersion;
                    model.SupFileVersion = mod.SupFileVersion;
                    model.PaperLaidTempId = mod.PaperLaidTempId;

                    i++;
                }
            }
            return model;
        }

        private static object CheckExistinigData(object p)
        {

            PaperLaidContext pCtxt = new PaperLaidContext();
            tUserRegistrationDetails update = p as tUserRegistrationDetails;

            var isexist = (from usrdschash in pCtxt.tUserRegistrationDetails where usrdschash.MobileNumber == update.MobileNumber && usrdschash.DiaryNumber == update.DiaryNumber && usrdschash.DocumentTypeId == update.DocumentTypeId select usrdschash).Count() > 0;
            if (isexist == true)
            {
                return "SMS already sent to this Mobile Number...";
            }
            else
            {
                return "Success";
            }

        }

        private static object CheckMobileDiaryExist(object p)
        {

            PaperLaidContext pCtxt = new PaperLaidContext();
            tUserRegistrationDetails update = p as tUserRegistrationDetails;

            var isexist = (from usrdschash in pCtxt.tUserRegistrationDetails where usrdschash.MobileNumber == update.MobileNumber && usrdschash.DiaryNumber == update.DiaryNumber && usrdschash.DocumentTypeId == update.DocumentTypeId select usrdschash).Count() > 0;
            return isexist;

        }

        public static object OTPDiaryList(object param)
        {

            OTPRegistrationAuditTrialModel model = param as OTPRegistrationAuditTrialModel;

            PaperLaidContext pCtxt = new PaperLaidContext();


            var query = (from OTPRegistration in pCtxt.tOTPRegistrationAuditTrial
                         where (OTPRegistration.DiaryNumber == model.DiaryNumber)

                         select new OTPRegistrationAuditTrialModel
                         {
                             DiaryNumber = OTPRegistration.DiaryNumber,
                             MobileNumber = OTPRegistration.MobileNumber,
                             MainReplyDocPath = OTPRegistration.MainReplyDocPath,
                             OTPId = OTPRegistration.OTPId

                         }).Distinct().ToList();


            model.OTPRegistrationModel = query;



            return model;
        }


        public static object OTPViewListList(object param)
        {

            OTPRegistrationAuditTrialModel model = param as OTPRegistrationAuditTrialModel;

            PaperLaidContext pCtxt = new PaperLaidContext();


            var query = (from OTPRegistration in pCtxt.tUserRegistrationDetails
                         where (OTPRegistration.DiaryNumber == model.DiaryNumber && OTPRegistration.AadharId == model.AadharId)

                         select new OTPRegistrationAuditTrialModel
                         {
                             DiaryNumber = OTPRegistration.DiaryNumber,
                             MobileNumber = OTPRegistration.MobileNumber,
                             AadharId = OTPRegistration.AadharId,
                             UserName = "T" + OTPRegistration.MobileNumber,
                         }).ToList();


            model.OTPRegistrationModel = query;



            return model;
        }



        public static object CheckMobileNo(object param)
        {

            OTPRegistrationAuditTrialModel model = param as OTPRegistrationAuditTrialModel;

            PaperLaidContext pCtxt = new PaperLaidContext();


            var query = (from OTPRegistration in pCtxt.tOneTimeUserRegistration
                         where (OTPRegistration.MobileNumber == model.MobileNumber)

                         select new OTPRegistrationAuditTrialModel
                         {

                             MobileNumber = OTPRegistration.MobileNumber,
                             AddressLocations = OTPRegistration.AddressLocations,
                             UserName = OTPRegistration.UserName

                         }).Distinct().ToList();
            model.OTPRegistrationModel = query;
            return model;
        }


        public static object OTPRegistarionDetailsList(object param)
        {

            tUserRegistrationDetails model = param as tUserRegistrationDetails;

            PaperLaidContext pCtxt = new PaperLaidContext();


            var query = (from OTPRegistration in pCtxt.tUserRegistrationDetails where OTPRegistration.DiaryNumber == model.DiaryNumber select OTPRegistration).ToList();

            model.UserRegistrationDetailsModel = query;

            return model;
        }



        public static object OTPMobileList(object param)
        {

            OTPRegistrationAuditTrialModel model = param as OTPRegistrationAuditTrialModel;

            var MobileNo = model.MobileNumber;

            PaperLaidContext pCtxt = new PaperLaidContext();


            var query = (from OTPRegistration in pCtxt.tOTPRegistrationAuditTrial
                         where (OTPRegistration.MobileNumber == MobileNo)

                         select new OTPRegistrationAuditTrialModel
                         {
                             DiaryNumber = OTPRegistration.DiaryNumber,
                             MobileNumber = OTPRegistration.MobileNumber,
                             MainReplyDocPath = OTPRegistration.MainReplyDocPath,
                             OTPId = OTPRegistration.OTPId,
                             DocTypeId = OTPRegistration.DocTypeId

                         }).Distinct().ToList();



            model.OTPRegistrationModel = query;



            return model;
        }

        private static object CheckMobileNoExist(object p)
        {
            var MobileNo = (string)p;
            using (var ctx = new PaperLaidContext())
            {

                var isexist = (from OTPReg in ctx.tOneTimeUserRegistration where OTPReg.MobileNumber == MobileNo select OTPReg).Count() > 0;

                return isexist;
            }
        }

        static object GetDynamicMainMenuByUserId(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();

            if (model.AssemblyId == 0 && model.SessionId == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionId = Convert.ToInt16(SessionCode);
                model.AssemblyId = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionId && m.AssemblyID == model.AssemblyId select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyId select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;

            model.mAssemblyList = (from A in pCtxt.mAssembly
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSession
                                 where S.AssemblyID == model.AssemblyId
                                 select S).ToList();

            return model;
        }



        static object GetDashBoardValueLocal(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();
            ModuleContext MCtx = new ModuleContext();


            List<mUserModules> LUM = new List<mUserModules>();
            //Get Main Menu Dynamically
            var MenuList = (from M in MCtx.tUserAccessRequest
                            where M.UserID == model.UserID && M.IsAcceptedDate != null
                            select new { M.AccessID, M.ActionControlId }
                    ).ToList();

            if (MenuList != null)
            {
                foreach (var item in MenuList)
                {
                    var MenuName = (from UA in MCtx.tUserAccessActions
                                    join UM in MCtx.mUserModules on UA.ModuleId equals UM.ModuleId
                                    where UA.UserAccessActionsId == item.AccessID
                                    select new { UM.ModuleId, UM.ModuleNameLocal }).FirstOrDefault();


                    mUserModules Umodule = new mUserModules();
                    Umodule.ModuleId = MenuName.ModuleId;
                    Umodule.ModuleNameLocal = MenuName.ModuleNameLocal;
                    Umodule.ActionIds = item.ActionControlId;
                    LUM.Add(Umodule);
                }
            }
            model.MenuList = LUM;
            if (model.AssemblyId == 0 && model.SessionId == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionId = Convert.ToInt16(SessionCode);
                model.AssemblyId = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionId && m.AssemblyID == model.AssemblyId select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyId select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;


            model.mAssemblyList = (from A in pCtxt.mAssembly
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSession
                                 where S.AssemblyID == model.AssemblyId
                                 select S).ToList();
            return model;


        }

        static object GetDashBoardValue(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();
            ModuleContext MCtx = new ModuleContext();


            List<mUserModules> LUM = new List<mUserModules>();
            //Get Main Menu Dynamically
            var MenuList = (from M in MCtx.tUserAccessRequest
                            where M.UserID == model.UserID && M.IsAcceptedDate != null
                            select new
                            {
                                AccessOrder=M.AccessOrder,
                                AccessID = M.AccessID,
                                ActionControlId = M.ActionControlId,
                                SubAccessByUser = M.SubAccessByUser,
                                ModuleId = (from UA in MCtx.tUserAccessActions
                                            join UM in MCtx.mUserModules on UA.ModuleId equals UM.ModuleId
                                            where UA.UserAccessActionsId == M.AccessID && UM.Isactive == true
                                            select UM.ModuleId).FirstOrDefault(),
                                ModuleName = (from UA in MCtx.tUserAccessActions
                                              join UM in MCtx.mUserModules on UA.ModuleId equals UM.ModuleId
                                              where UA.UserAccessActionsId == M.AccessID && UM.Isactive == true
                                              select UM.ModuleName).FirstOrDefault()

                            }
                    ).OrderBy (x=>x.AccessOrder).ToList();

            if (MenuList != null)
            {
                foreach (var item in MenuList)
                {
                    //var MenuName = (from UA in MCtx.tUserAccessActions
                    //                join UM in MCtx.mUserModules on UA.ModuleId equals UM.ModuleId
                    //                where UA.UserAccessActionsId == item.AccessID
                    //                select new { UM.ModuleId, UM.ModuleName }).FirstOrDefault();
                    if (item.ModuleId != 0)
                    {
                        mUserModules Umodule = new mUserModules();
                        Umodule.ModuleId = item.ModuleId;
                        Umodule.ModuleName = item.ModuleName;
                        Umodule.ActionIds = item.ActionControlId;
                        Umodule.SubUserTypeName = item.SubAccessByUser;//this is use for SubAcess
                        LUM.Add(Umodule);

                    }
                   
                }
            }
            model.MenuList = LUM;
            
            if (model.AssemblyId == 0 && model.SessionId == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionId = Convert.ToInt16(SessionCode);
                model.AssemblyId = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionId && m.AssemblyID == model.AssemblyId select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyId select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;


            model.mAssemblyList = (from A in pCtxt.mAssembly
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSession
                                 where S.AssemblyID == model.AssemblyId
                                 select S).OrderByDescending(x=>x.StartDate).ToList ();
            return model;


        }
        static object AfterFixation(object param)
        {

            PaperLaidContext db = new PaperLaidContext();
            tPaperLaidV objPaperLaid = param as tPaperLaidV;
            //mSessionDate mdept = param as mSessionDate;
            var data = (from dept in db.mSessionDates where dept.AssemblyId == objPaperLaid.AssemblyId && dept.SessionId == objPaperLaid.SessionId select dept).ToList();

            //var data = db.mSessionDates.ToList();
            return data;
        }

        static object BeforeFixation(object param)
        {
            tPaperLaidV objPaperLaid = param as tPaperLaidV;
            string csv = objPaperLaid.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            PaperLaidContext db = new PaperLaidContext();
            var data = (from dept in db.mDepartmentPdfPath where ids.Contains(dept.deptId) && dept.AssemblyID == objPaperLaid.AssemblyId && dept.SessionID == objPaperLaid.SessionId select dept).ToList();
            return data;

        }

        static object UpdateNoticePaperLaidId(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            tMemberNotice updatePaperLaid = param as tMemberNotice;
            var tMemberNotice = (from mdl in paperLaidCntxtDB.tMemberNotice where mdl.NoticeId == updatePaperLaid.NoticeId select mdl).SingleOrDefault();
            if (tMemberNotice != null)
            {
                tMemberNotice.PaperLaidId = updatePaperLaid.PaperLaidId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }
            return tMemberNotice;

        }


        static object SubmittChangeDeptEntry(object param)
        {

            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            mchangeDepartmentAuditTrail submitPaperEntry = param as mchangeDepartmentAuditTrail;
            paperLaidCntxtDB.mchangeDepartmentAuditTrails.Add(submitPaperEntry);

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            // submitPaperEntry.PaperLaidId = 
            return submitPaperEntry;

        }

        static string getDepartmentIdByQuestionId(object param)
        {
            string deptId = "";
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidV mdl = param as tPaperLaidV;

            deptId = (from mdlQuestions in paperLaidCntxtDB.tQuestions
                      where mdlQuestions.QuestionID == mdl.QuestionID
                      select mdlQuestions.DepartmentID).SingleOrDefault();
            return deptId;
        }

        static string getDepartmentIdByNoticeId(object param)
        {
            string deptId = "";
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidV mdl = param as tPaperLaidV;

            deptId = (from mdlNotice in paperLaidCntxtDB.tMemberNotice
                      where mdlNotice.NoticeId == mdl.NoticeID
                      select mdlNotice.DepartmentId).SingleOrDefault();
            return deptId;
        }


        static int? getMinistryIdByNoticeId(object param)
        {
            int? ministryId = null;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidV mdl = param as tPaperLaidV;

            ministryId = (from mdlNotice in paperLaidCntxtDB.tMemberNotice
                          where mdlNotice.NoticeId == mdl.NoticeID
                          select mdlNotice.MinistryId).SingleOrDefault();
            return ministryId;
        }



        static int? getMinistryIdByQuestionId(object param)
        {
            int? ministryId = null;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidV mdl = param as tPaperLaidV;

            ministryId = (from mdlQuestions in paperLaidCntxtDB.tQuestions
                          where mdlQuestions.QuestionID == mdl.QuestionID
                          select mdlQuestions.MinistryId).SingleOrDefault();
            return ministryId;
        }

        static string getDepartmentIdByPaperId(object param)
        {
            string deptId = "";
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidV mdl = param as tPaperLaidV;

            deptId = (from mdlpaper in paperLaidCntxtDB.tPaperLaidV
                      where mdlpaper.PaperLaidId == mdl.PaperLaidId
                      select mdlpaper.DepartmentId).SingleOrDefault();
            return deptId;
        }


        static string GetUserNameByAadharId(object param)
        {
            string Name = "";
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            AdhaarDetails mdl = param as AdhaarDetails;


            Name = (from AdDetails in paperLaidCntxtDB.AdhaarDetails
                    where AdDetails.AdhaarID == mdl.AdhaarID
                    select AdDetails.Name).SingleOrDefault();


            return Name;
        }


        static object GetAuthorizedEmployees(object param)
        {
            tPaperLaidV mdl = param as tPaperLaidV;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            var UserInfo = (from model in paperLaidCntxtDB.AuthorisedEmployee
                            join Login in paperLaidCntxtDB.mUsers on model.UserId equals Login.UserId
                            where model.IsActive == true
                            && model.UserId == mdl.UserID
                            select model).ToList();

            return UserInfo;
        }

        static tPaperLaidV GetPaperLaidByPaperLaidId(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();
            paperLaid = (from mdl in plCntxt.tPaperLaidV where mdl.PaperLaidId == paperLaid.PaperLaidId select mdl).FirstOrDefault();

            return paperLaid;
        }

        static object GetQuestionByPaperLaidID(object param)
        {


            tQuestion tQuestions = new tQuestion();

            tQuestion model = param as tQuestion;

            //int questionNumber = Convert.ToInt32(param);
            PaperLaidContext pCtxt = new PaperLaidContext();
            tPaperLaidV paperMdel = new tPaperLaidV();
            tQuestions = (from questions in pCtxt.tQuestions
                          where questions.PaperLaidId == model.PaperLaidId
                          select questions).FirstOrDefault();

            var item = (from questions in pCtxt.tQuestions
                        select new QuestionModelCustom
                        {
                            MemberName = (from mc in pCtxt.mMember where mc.MemberCode == tQuestions.MemberID select mc.Name).FirstOrDefault(),

                        }).FirstOrDefault();
            tQuestions.RMemberName = item.MemberName;
            return tQuestions;


            //PaperLaidContext DBContext = new PaperLaidContext();
            //tQuestion questionModel = param as tQuestion;

            //var query = (from questions in DBContext.tQuestions
            //             where questions.PaperLaidId == questionModel.PaperLaidId
            //             select questions).FirstOrDefault();


            //return query;
        }

        public static object UpdateMinisterActivePaperId(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');
            tPaperLaidTemp pt = new tPaperLaidTemp();
            int LOBRecordId = 0;

            foreach (var item in obja)
            {

                using (var obj1 = new PaperLaidContext())
                {
                    long id = Convert.ToInt32(item);

                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
                                        where PaperLaidModel.PaperLaidId == id
                                        select PaperLaidModel).FirstOrDefault();
                    if (PaperLaidObj != null)
                    {
                        PaperLaidObj.MinisterActivePaperId = PaperLaidObj.DeptActivePaperId;
                        if (PaperLaidObj.LOBPaperTempId != null && PaperLaidObj.LOBPaperTempId != 0 && PaperLaidObj.LOBRecordId != null && PaperLaidObj.LOBRecordId != 0)
                        {
                            PaperLaidObj.LOBPaperTempId = PaperLaidObj.MinisterActivePaperId;
                            LOBRecordId = Convert.ToInt32(PaperLaidObj.LOBRecordId);
                        }
                    }

                    obj1.SaveChanges();
                    obj1.Close();
                }
            }



            return LOBRecordId;
        }

        static tPaperLaidV GetDepartmentNameMinistryDetailsByMinisterID(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var mMinistry = (from mdl in plCntxt.mMinistry where mdl.MinistryID == paperLaid.MinistryId select mdl).SingleOrDefault();
            paperLaid.MinistryName = mMinistry.MinistryName;
            //paperLaid.MinisterName = mMinistry.MinisterName;
            paperLaid.MinistryNameLocal = mMinistry.MinistryNameLocal;
            var DepartmentName = (from mod in plCntxt.mDepartment where paperLaid.DepartmentId == mod.deptId select mod.deptname).SingleOrDefault();

            paperLaid.DeparmentName = DepartmentName;
            paperLaid.Description = (from mod in plCntxt.tQuestions where mod.DiaryNumber == paperLaid.DiaryNumber select mod.MainQuestion).SingleOrDefault();
            return paperLaid;
        }

        static object GetQuetionEventID(object param)
        {
            mEvent mEvents = param as mEvent;
            PaperLaidContext qxt = new PaperLaidContext();
            int paperCategoryTypeId = (from mdl in qxt.mPaperCategoryType
                                       where mdl.Name.Contains("ques")
                                       select mdl.PaperCategoryTypeId).SingleOrDefault();

            mEvents = (from mdl in qxt.mEvent
                       where mdl.PaperCategoryTypeId == paperCategoryTypeId //&& mdl.EventName.Contains("hour") By Ajay
                       select mdl).SingleOrDefault();
            return mEvents;
        }



        static object GetMinisterMinistryName(object param)
        {
            string DeptId = param.ToString();
            PaperLaidContext ctxt = new PaperLaidContext();

            var query = (from mdl in ctxt.mMInistryDepartment
                         where mdl.DeptID == DeptId
                         join mdl1 in ctxt.mMinistry on mdl.MinistryID equals mdl1.MinistryID
                         select new mMinisteryMinisterModel
                         {
                             MinistryID = mdl1.MinistryID
                         }).SingleOrDefault();
            return query;

        }

        static tPaperLaidV GetPaperLaidByEventId(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;


            if (paperLaid.DepartmentId == "0")
            {
                if (paperLaid.IsLaid == false)
                {

                    PaperLaidContext plCntxt = new PaperLaidContext();
                    if (paperLaid.EventId == 36)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
       (mdl.EventId == paperLaid.EventId || mdl.EventId == 3 && mdl.IsLaid == paperLaid.IsLaid && mdl.DeptActivePaperId != null)
    || (mdl.EventId == paperLaid.EventId || mdl.EventId == 3 && mdl.IsLaid == null && mdl.DeptActivePaperId != null)
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else if (paperLaid.EventId == 73)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         (mdl.EventId == paperLaid.EventId || mdl.EventId == 50

                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null) || (mdl.EventId == paperLaid.EventId || mdl.EventId == 50

                                                         && mdl.IsLaid == null
                                                         //&& mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null)
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         (mdl.EventId == paperLaid.EventId

                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         //&& mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null) || (mdl.EventId == paperLaid.EventId

                                                         && mdl.IsLaid == null
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null)
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                }


                else if (paperLaid.IsLaid == true)
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    if (paperLaid.EventId == 36)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         (mdl.EventId == paperLaid.EventId || mdl.EventId == 3)
                                                         //&& mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         //&& mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else if (paperLaid.EventId == 73)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId || mdl.EventId == 50
                                                         //&& mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId
                                                         //&& mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }

                }
                else  /////ALL case
                {


                    PaperLaidContext plCntxt = new PaperLaidContext();
                    if (paperLaid.EventId == 36)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId || mdl.EventId == 3

                                                         //&& mdl.PaperlaidPriority != 0
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();

                    }
                    else if (paperLaid.EventId == 73)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId || mdl.EventId == 50

                                                         //&& mdl.PaperlaidPriority != 0
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();

                    }
                    else
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId

                                                         //&& mdl.PaperlaidPriority != 0
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();

                    }


                }

            }



            else /////if dep is selected
            {
                if (paperLaid.IsLaid == true)
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    if (paperLaid.EventId == 36)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId || mdl.EventId == 3
                                                         && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         //&& mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else if (paperLaid.EventId == 73)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId || mdl.EventId == 50
                                                         && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId
                                                         && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                }


                else if (paperLaid.IsLaid == false)
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    if (paperLaid.EventId == 36)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         (mdl.EventId == paperLaid.EventId || mdl.EventId == 3
                                                         && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null) || (mdl.EventId == paperLaid.EventId || mdl.EventId == 3
                                                         && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == null
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null)
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else if (paperLaid.EventId == 73)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         (mdl.EventId == paperLaid.EventId || mdl.EventId == 50
                                                         && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == paperLaid.IsLaid
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null) || (mdl.EventId == paperLaid.EventId || mdl.EventId == 50
                                                         && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         && mdl.IsLaid == null
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null)
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
           (mdl.EventId == paperLaid.EventId && mdl.DepartmentId == paperLaid.DepartmentId && mdl.IsLaid == paperLaid.IsLaid && mdl.DeptActivePaperId != null)
        || (mdl.EventId == paperLaid.EventId && mdl.DepartmentId == paperLaid.DepartmentId && mdl.IsLaid == null && mdl.DeptActivePaperId != null)
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }

                }
                else
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    if (paperLaid.EventId == 36)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId || mdl.EventId == 3
                                                           && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else if (paperLaid.EventId == 73)
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId || mdl.EventId == 50
                                                           && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         //  && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                    else
                    {
                        paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                     where
                                                         mdl.EventId == paperLaid.EventId
                                                           && mdl.DepartmentId == paperLaid.DepartmentId
                                                         //&& mdl.PaperlaidPriority != 0
                                                         // && mdl.MinisterActivePaperId != null
                                                         && mdl.DeptActivePaperId != null
                                                     orderby mdl.PaperlaidPriority ascending
                                                     select mdl).ToList();
                    }
                }





            }



            return paperLaid;
        }

        static tPaperLaidV GetPaperByPaperIdAndPriority(object param)
        {


            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();

            paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                         where mdl.EventId == paperLaid.EventId
                                             //&& mdl.DepartmentId == paperLaid.DepartmentId
                                             // && mdl.MinisterActivePaperId != null
                                             && mdl.DeptActivePaperId != null
                                             && mdl.IsLaid == null
                                         //&& mdl.SessionId == 7
                                         orderby mdl.DepartmentId ascending
                                         select mdl).ToList();

            return paperLaid;
        }
        static tPaperLaidV GetPaperLaidByPaperIdAndPriority(object param)
        {


            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();

            paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                         where mdl.EventId == paperLaid.EventId
                                             //&& mdl.DepartmentId == paperLaid.DepartmentId
                                             // && mdl.MinisterActivePaperId != null
                                             && mdl.DeptActivePaperId != null
                                             && mdl.IsLaid != null
                                         //&& mdl.SessionId == 7
                                         orderby mdl.DepartmentId ascending
                                         select mdl).ToList();

            return paperLaid;
        }
        static tPaperLaidV GetPaperLaidByEventIdAndPriorty(object param)
        {

            tPaperLaidV paperLaid = param as tPaperLaidV;
            if (paperLaid.DepartmentId == "0")
            {
                if (paperLaid.IsLaid == false)
                {

                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where
                                                     (mdl.EventId == paperLaid.EventId
                                                     && mdl.PaperlaidPriority == paperLaid.PaperlaidPriority
                                                     && mdl.IsLaid == paperLaid.IsLaid
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null) || (mdl.EventId == paperLaid.EventId
                                                     && mdl.PaperlaidPriority == paperLaid.PaperlaidPriority
                                                     && mdl.IsLaid == null
                                                     && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null)
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();


                }
                else if (paperLaid.IsLaid == true)
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where mdl.EventId == paperLaid.EventId
                                                     && mdl.PaperlaidPriority == paperLaid.PaperlaidPriority

                                                     && mdl.IsLaid == paperLaid.IsLaid
                                                     //&& mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null
                                                 //&& mdl.LOBRecordId == null
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();
                }

                else
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where
                                                     mdl.EventId == paperLaid.EventId
                                                      && mdl.PaperlaidPriority == paperLaid.PaperlaidPriority


                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();






                }



            }


            else
            {


                if (paperLaid.IsLaid == false)
                {

                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where
                                                     (mdl.EventId == paperLaid.EventId
                                                      && mdl.PaperlaidPriority == paperLaid.PaperlaidPriority
                                                       && mdl.DepartmentId == paperLaid.DepartmentId
                                                     && mdl.IsLaid == paperLaid.IsLaid
                                                     //&& mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null) || (mdl.EventId == paperLaid.EventId
                                                      && mdl.PaperlaidPriority == paperLaid.PaperlaidPriority
                                                       && mdl.DepartmentId == paperLaid.DepartmentId
                                                     && mdl.IsLaid == null
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null)
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();

                }
                else if (paperLaid.IsLaid == true)
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where mdl.EventId == paperLaid.EventId
                                                     && mdl.PaperlaidPriority == paperLaid.PaperlaidPriority
                                                      && mdl.DepartmentId == paperLaid.DepartmentId
                                                     && mdl.IsLaid == paperLaid.IsLaid
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null
                                                 //&& mdl.LOBRecordId == null
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();
                }

                else
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where
                                                     mdl.EventId == paperLaid.EventId
                                                      && mdl.PaperlaidPriority == paperLaid.PaperlaidPriority
                                                       && mdl.DepartmentId == paperLaid.DepartmentId

                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();







                }



            }
            return paperLaid;
        }
        static tPaperLaidV GetPaperLaidByDeptId(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;

            PaperLaidContext plCntxt = new PaperLaidContext();

            if (paperLaid.EventId == 36)
            {
                paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                             where mdl.EventId == paperLaid.EventId || mdl.EventId == 3
                                                 && mdl.IsLaid == false || mdl.IsLaid == null
                                             // && mdl.MinisterActivePaperId != null
                                             && mdl.DeptActivePaperId != null
                                             orderby mdl.PaperlaidPriority ascending
                                             select mdl).ToList();
            }
            if (paperLaid.EventId == 73)
            {
                paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                             where mdl.EventId == paperLaid.EventId || mdl.EventId == 50
                                                   && mdl.IsLaid == false || mdl.IsLaid == null
                                             // && mdl.MinisterActivePaperId != null
                                             && mdl.DeptActivePaperId != null
                                             orderby mdl.PaperlaidPriority ascending
                                             select mdl).ToList();
            }
            else
            {
                paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                             where (mdl.EventId == paperLaid.EventId && mdl.IsLaid == false && mdl.DeptActivePaperId != null) ||
                                             (mdl.EventId == paperLaid.EventId && mdl.IsLaid == null && mdl.DeptActivePaperId != null)
                                             orderby mdl.PaperlaidPriority ascending
                                             select mdl).ToList();

            }
            return paperLaid;


        }

        static tPaperLaidV GetPaperLaidByPaperIdAndWithNoPriority(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();
            paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                         where mdl.EventId == paperLaid.EventId
                                             && mdl.PaperlaidPriority == 0
                                             && mdl.IsLaid != null
                                             // && mdl.MinisterActivePaperId != null
                                             && mdl.DeptActivePaperId != null
                                         //&& mdl.LOBRecordId == null
                                         orderby mdl.PaperlaidPriority ascending
                                         select mdl).ToList();
            return paperLaid;
        }
        static tPaperLaidV GetPaperByPaperIdWithNoPriority(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();
            paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                         where mdl.EventId == paperLaid.EventId
                                             && mdl.PaperlaidPriority == 0
                                             && mdl.IsLaid == null
                                             //&& mdl.MinisterActivePaperId != null
                                             && mdl.DeptActivePaperId != null
                                         //&& mdl.LOBRecordId == null
                                         orderby mdl.PaperlaidPriority ascending
                                         select mdl).ToList();
            return paperLaid;
        }
        static tPaperLaidV GetPaperLaidByEventIdWithNoPriority(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            if (paperLaid.DepartmentId == "0")
            {


                if (paperLaid.IsLaid == false)
                {

                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where
                                                     (mdl.EventId == paperLaid.EventId
                                                      && mdl.PaperlaidPriority == 0

                                                     && mdl.IsLaid == paperLaid.IsLaid
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null) || (mdl.EventId == paperLaid.EventId
                                                      && mdl.PaperlaidPriority == 0

                                                     && mdl.IsLaid == null
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null)
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();

                }
                else if (paperLaid.IsLaid == true)
                {

                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where mdl.EventId == paperLaid.EventId

                                                     && mdl.PaperlaidPriority == 0
                                                     && mdl.IsLaid == paperLaid.IsLaid
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null
                                                 // && mdl.LOBRecordId == null
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();
                }
                else
                {

                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where
                                                     mdl.EventId == paperLaid.EventId

                                                     && mdl.PaperlaidPriority == 0
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();


                }
            }


            else ///////if dep is selected
            {


                if (paperLaid.IsLaid == true)
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where
                                                     mdl.EventId == paperLaid.EventId
                                                     && mdl.DepartmentId == paperLaid.DepartmentId
                                                     && mdl.PaperlaidPriority == 0
                                                     && mdl.IsLaid == paperLaid.IsLaid
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();


                }


                else if (paperLaid.IsLaid == false)
                {
                    PaperLaidContext plCntxt = new PaperLaidContext();
                    paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV
                                                 where
                                                     (mdl.EventId == paperLaid.EventId
                                                     && mdl.DepartmentId == paperLaid.DepartmentId
                                                     && mdl.PaperlaidPriority == 0
                                                     && mdl.IsLaid == paperLaid.IsLaid
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null) || (mdl.EventId == paperLaid.EventId
                                                     && mdl.DepartmentId == paperLaid.DepartmentId
                                                     && mdl.PaperlaidPriority == 0
                                                     && mdl.IsLaid == null
                                                     // && mdl.MinisterActivePaperId != null
                                                     && mdl.DeptActivePaperId != null)
                                                 orderby mdl.PaperlaidPriority ascending
                                                 select mdl).ToList();


                }




            }
            return paperLaid;
        }


        static tBillRegister GetPaperLaidNumberById(object param)
        {
            tBillRegister paperLaidNumber = param as tBillRegister;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var result = (from mdl in plCntxt.tBillRegister where mdl.PaperLaidId == paperLaidNumber.PaperLaidId select mdl).FirstOrDefault();
            paperLaidNumber = result;
            return paperLaidNumber;
        }
        static mBills GetBillDetails(object param)
        {
            mBills paperLaidNumber = param as mBills;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var result = (from mdl in plCntxt.tBills where mdl.BillNo == paperLaidNumber.BillNo select mdl).FirstOrDefault();
            paperLaidNumber = result;
            return paperLaidNumber;
        }
        static string UpdateLOBRecordIdIntPaperLaidVS(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();

            using (var obj = new PaperLaidContext())
            {
                var PaperLaidObj = obj.tPaperLaidV.Where(m => m.LOBRecordId == paperLaid.LOBRecordId).FirstOrDefault();
                if (PaperLaidObj != null)
                {
                    PaperLaidObj.LOBRecordId = null;
                    PaperLaidObj.LOBPaperTempId = null;
                }
                obj.SaveChanges();
                obj.Close();
            }


            using (var obj = new PaperLaidContext())
            {
                var PaperLaidObj = obj.tPaperLaidV.Where(m => m.PaperLaidId == paperLaid.PaperLaidId).FirstOrDefault();
                if (PaperLaidObj != null)
                {
                    PaperLaidObj.LOBRecordId = paperLaid.LOBRecordId;
                    PaperLaidObj.DesireLayingDate = paperLaid.DesireLayingDate;
                    PaperLaidObj.LOBPaperTempId = paperLaid.LOBPaperTempId;
                }
                //obj.tBillRegisterVs.Add(model);
                obj.SaveChanges();
                obj.Close();
            }
            return null;
        }

        static List<mPaperCategoryType> GetAllPaperCategoryType(object param)
        {
            PaperLaidContext db = new PaperLaidContext();

            mPaperCategoryType model = param as mPaperCategoryType;


            var query = (from dist in db.mPaperCategoryType
                         select dist);

            string searchText = (model.Name != null && model.Name != "") ? model.Name.ToLower() : "";
            if (searchText != "")
            {
                query = query.Where(a =>
                    a.Name != null && a.Name.ToLower().Contains(searchText));

            }

            return query.ToList();
        }

        static object GetPaperLaidDepartmentDetails(object param)
        {
            tPaperLaidV model = new tPaperLaidV();
            //    AssemblyContext assemblyContext = new AssemblyContext();
            DepartmentContext departmentContext = new DepartmentContext();
            //    MinisteryContext ministryContext = new MinisteryContext();
            SessionContext SessionContext = new SessionContext();

            //model.mAssembly = (from a in assemblyContext.mAssemblies
            //                   orderby a.AssemblyID descending
            //                   select a).ToList();
            //mAssembly obj = new mAssembly();
            //obj.AssemblyID = 0;
            //obj.AssemblyName = "Select Assembly";
            //model.mAssembly.Add(obj);

            //model.mMinister = (from minister in ministryContext.mMinisteryVS
            //                   orderby minister.MinisteryName ascending
            //                   select minister).ToList();

            //mMinisteryVS ministerModel = new mMinisteryVS();
            //ministerModel.MinisteryID = 0;
            //ministerModel.MinisteryName = "Select Minister";
            //model.mMinister.Add(ministerModel);

            //model.mDepartment = (from department in departmentContext.objmDepartment
            //                     orderby department.deptname ascending
            //                     select department).ToList();

            //mDepartment mdepartment = new mDepartment();
            //mdepartment.deptId = "0";
            //mdepartment.deptname = "Select Department";
            //model.mDepartment.Add(mdepartment);

            //model.mSession = (from session in SessionContext.mSession
            //                  orderby session.SessionName ascending
            //                  select session).ToList();

            //mSession mSession = new mSession();
            //mSession.SessionID = 0;
            //mSession.SessionName = "Select Session";
            //model.mSession.Add(mSession);



            return model;
        }

        // Paper pending list
        static List<PaperMovementModel> GetPendingDepartmentPaperLaidList(object param)
        {
            PaperLaidContext db = new PaperLaidContext();
            PaperMovementModel model = param as PaperMovementModel;

            int? EventId = null;
            int? PaperCategoryTypeId = null;
            if (model.EventId != 0 && model.PaperCategoryTypeId != 0)
            {
                EventId = model.EventId;
            }
            else if (model.PaperCategoryTypeId != 0)
            {
                PaperCategoryTypeId = model.PaperCategoryTypeId;
            }

            var query = (from m in db.tPaperLaidV
                         join c in db.mEvent on m.EventId equals c.EventId
                         join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         where m.CommitteeId == null && t.DeptSubmittedDate == null
                         && t.DeptSubmittedBy == null
                         && (!EventId.HasValue || m.EventId == model.EventId)
                         select new PaperMovementModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             DeparmentName = m.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)m.PaperLaidId,
                             actualFilePath = t.FilePath + t.FileName,
                             PaperCategoryTypeId = c.PaperCategoryTypeId,
                             BusinessType = c.EventName,
                             version = t.Version,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault()

                         }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            foreach (var item in query)
            {
                switch (item.PaperCategoryTypeId)
                {
                    case 1:
                        item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                        break;
                    case 2:
                        item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                        break;
                    case 4:
                        item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                        break;
                    default:
                        break;
                }
            }

            if (PaperCategoryTypeId != null)
            {
                var filterQuery = query.Where(foo => foo.PaperCategoryTypeId == PaperCategoryTypeId);
                return filterQuery.ToList();
            }
            return query.ToList();
        }

        // Nitin code
        static object GetSessionDates(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            mSessionDate sessionDates = new mSessionDate();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            paperLaid.mSessionDates = (from mdl2 in plCntxt.mSessionDates
                                       where mdl2.SessionId == paperLaid.SessionId && mdl2.AssemblyId == paperLaid.AssemblyId
                                       select mdl2).ToList();

            foreach (var item in paperLaid.mSessionDates)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }
            mSessionDate obj = new mSessionDate();
            obj.Id = 0;
            obj.DisplayDate = "Select Desired Date to be Lay";
            paperLaid.mSessionDates.Add(obj);

            return paperLaid.mSessionDates;

        }
        public static int CurrentAssemblyCode()
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            mSessionDate sessionDates = new mSessionDate();
            return Convert.ToInt16((from mdl in plCntxt.mSiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault());

        }

        public static int AssemblyCode(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            mSessionDate sessionDates = new mSessionDate();
            return Convert.ToInt16((from mdl in plCntxt.mSiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault());

        }
        public static int SessionCode(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            mSessionDate sessionDates = new mSessionDate();
            return Convert.ToInt16((from mdl in plCntxt.mSiteSettings where mdl.SettingName == "OldSession" select mdl.SettingValue).SingleOrDefault());
        }

        public static int CurrentSessionCode()
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            mSessionDate sessionDates = new mSessionDate();
            return Convert.ToInt16((from mdl in plCntxt.mSiteSettings where mdl.SettingName == "OldSession" select mdl.SettingValue).SingleOrDefault());
        }

        public static List<mSessionDate> GetCurrentSessionDates()
        {
            int AssemblyCode = CurrentAssemblyCode();
            int SessionCode = CurrentSessionCode();
            PaperLaidContext plCntxt = new PaperLaidContext();
            var Res = (from sessiondates in plCntxt.mSessionDates where sessiondates.SessionId == SessionCode && sessiondates.IsSitting == true && sessiondates.AssemblyId == AssemblyCode select sessiondates).OrderBy(a => a.SessionDate);
            return Res.OrderByDescending(a => a.SessionDate).ToList();
            // OrderByDescending(a => a.PublishedDate);
        }
        static List<mSessionDate> GetSessionDate()
        {
            int AssemblyCode = CurrentAssemblyCode();
            int SessionCode = CurrentSessionCode();
            PaperLaidContext plCntxt = new PaperLaidContext();
            var Res = (from sessiondates in plCntxt.mSessionDates where sessiondates.SessionId == SessionCode && sessiondates.IsSitting == true && sessiondates.AssemblyId == AssemblyCode select sessiondates).OrderBy(a => a.SessionDate);
            return Res.OrderByDescending(a => a.SessionDate).ToList();

        }

        static object GetDocumentType(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            mDocumentType documents = new mDocumentType();
            mSessionDate sessionDates = new mSessionDate();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            paperLaid.mDocumentTypes = (from mdl in plCntxt.mDocumentTypes select mdl).ToList();
            mDocumentType obj = new mDocumentType();
            obj.DocumentTypeID = 0;
            obj.DocumentType = "Select Paper Type";
            paperLaid.mDocumentTypes.Add(obj);
            return paperLaid.mDocumentTypes;
        }

        static object GetPaperLaidCategoryDetails(object param)
        {
            int[] ids = (int[])param;

            int id = Convert.ToInt32(ids[0]);
            int noticeTypeId = ids[1];
            PaperLaidContext cnxt = new PaperLaidContext();
            tPaperLaidV model = new tPaperLaidV();

            string categoryName = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == id select mdl.Name).SingleOrDefault();

            if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                model.tQuestioType = (from mdl in cnxt.tQuestionTypes select mdl).ToList();
                tQuestionType obj = new tQuestionType();
                obj.QuestionTypeName = "select Question Type";
                obj.QuestionTypeId = 0;
                model.tQuestioType.Add(obj);
                model.categoryType = "question";
            }
            else if (categoryName.IndexOf("notice", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                model.tMemberNotice = (from mdl in cnxt.tMemberNotice where mdl.NoticeTypeID == noticeTypeId select mdl).ToList();
                tMemberNotice ob = new tMemberNotice();
                ob.NoticeId = 0;
                ob.NoticeNumber = "Select Notice Number";
                model.tMemberNotice.Add(ob);
                model.categoryType = "notice";
            }
            else if (categoryName.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                model.mBillTypes = (from mdl in cnxt.mBillTypes select mdl).ToList();
                model.categoryType = "bill";
            }
            else
            { model.categoryType = "other"; }
            return model;
        }

        static object GetFileSaveDetails(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext cnxt = new PaperLaidContext();
            int? catedoryId = (from mdl in cnxt.mEvent
                               where mdl.EventId == model.EventId
                               select mdl.PaperCategoryTypeId).SingleOrDefault();
            string respectivePaper = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == catedoryId select mdl.Name).SingleOrDefault();

            var FileUpdate = (from paperLaidTemp in cnxt.tPaperLaidTemp
                              where paperLaidTemp.PaperLaidId == model.PaperLaidId
                                  && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
                              select paperLaidTemp).FirstOrDefault();

            if (FileUpdate != null)
            {
                if (FileUpdate.Version != null)
                {
                    model.Count = Convert.ToInt32(FileUpdate.Version);
                }
                else
                {
                    model.Count = 1;
                }
            }
            else
            {
                int count = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
                int newVersion = count + 1;
                model.Count = newVersion;
            }


            if (FileUpdate != null)
            {
                if (FileUpdate.SupVersion != null)
                {
                    model.SupPDFCount = Convert.ToInt32(FileUpdate.SupVersion);
                }
                else
                {
                    model.SupPDFCount = 1;
                }
            }
            else
            {
                int SupPDFCount = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
                int newVersionSup = SupPDFCount + 1;
                model.SupPDFCount = newVersionSup;
            }

            if (FileUpdate != null)
            {
                if (FileUpdate.DocFileVersion != null)
                {
                    model.MainDocCount = Convert.ToInt32(FileUpdate.DocFileVersion);
                }
                else
                {
                    model.MainDocCount = 1;
                }
            }
            else
            {
                int MainDocCount = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
                int newVersionDoc = MainDocCount + 1;
                model.MainDocCount = newVersionDoc;
            }

            model.RespectivePaper = respectivePaper;

            return model;
        }

        static tPaperLaidV GetQuestionNumber(object param)
        {
            tPaperLaidV mod = param as tPaperLaidV;
            // int questionId = Convert.ToInt32(param);
            PaperLaidContext pCtxt = new PaperLaidContext();
            tPaperLaidV paperMdel = new tPaperLaidV();
            paperMdel.tQuestion = (from mdl in pCtxt.tQuestions
                                   where mdl.QuestionType == mod.QuestionTypeId && mdl.DepartmentID == mod.DepartmentId
                                       && mdl.PaperLaidId == null
                                   select mdl).ToList();
            return paperMdel;
        }

        static object GetQuestionDetails(object param)
        {
            tQuestion tQuestions = new tQuestion();

            tQuestion model = param as tQuestion;

            //int questionNumber = Convert.ToInt32(param);
            PaperLaidContext pCtxt = new PaperLaidContext();
            tPaperLaidV paperMdel = new tPaperLaidV();
            tQuestions = (from mdl in pCtxt.tQuestions where mdl.QuestionID == model.QuestionID && mdl.QuestionType == model.QuestionType select mdl).FirstOrDefault();

            var item = (from questions in pCtxt.tQuestions
                        select new QuestionModelCustom
                        {
                            MemberName = (from mc in pCtxt.mMember where mc.MemberCode == tQuestions.MemberID select mc.Name).FirstOrDefault(),

                        }).FirstOrDefault();

            tQuestions.RMemberName = item.MemberName;
            tQuestions.MemberN = item.MemberName;
            return tQuestions;
        }

        static object GetNoticeDetails(object param)
        {
            string noticeNumber = param.ToString();
            PaperLaidContext pCtxt = new PaperLaidContext();
            tMemberNotice model = new tMemberNotice();
            model = (from mdl in pCtxt.tMemberNotice where mdl.NoticeNumber == noticeNumber select mdl).SingleOrDefault();
            model.Subject = Regex.Replace(model.Subject, @"<[^>]+>|&nbsp;", "").Trim();
            model.Notice = Regex.Replace(model.Notice, @"<[^>]+>|&nbsp;", "").Trim();
            string[] date = model.NoticeDate.ToString().Split(' ');
            model.DateNotice = date[0];
            return model;
        }

        static object CheckBillNUmber(object param)
        {
            string billNumber = param.ToString();
            PaperLaidContext pCtxt = new PaperLaidContext();
            tBillRegister model = new tBillRegister();
            model = (from mdl in pCtxt.tBillRegister where mdl.BillNumber == billNumber select mdl).SingleOrDefault();
            return model;
        }

        static object GetNotices(object param)
        {
            int noticeId = Convert.ToInt32(param);
            PaperLaidContext pCtxt = new PaperLaidContext();
            tPaperLaidV paperMdel = new tPaperLaidV();
            paperMdel.tMemberNotice = (from mdl in pCtxt.tMemberNotice where mdl.NoticeTypeID == noticeId && mdl.PaperLaidId == null select mdl).ToList();
            return paperMdel;
        }

        static object SubmittPaperLaidEntry(object param)
        {
            if (null == param)
            {
                return null;
            }

            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidV submitPaperEntry = param as tPaperLaidV;

            if (submitPaperEntry.PaperLaidId == 0)
            {
                paperLaidCntxtDB.tPaperLaidV.Add(submitPaperEntry);
            }
            else
            {
                var ActiveDeptID = (from tPL in paperLaidCntxtDB.tPaperLaidV where tPL.PaperLaidId == submitPaperEntry.PaperLaidId select tPL.DeptActivePaperId).SingleOrDefault();
                if (ActiveDeptID != null)
                {
                    submitPaperEntry.DeptActivePaperId = ActiveDeptID;
                }
                paperLaidCntxtDB.tPaperLaidV.Attach(submitPaperEntry);
                paperLaidCntxtDB.Entry(submitPaperEntry).State = EntityState.Modified;
            }
            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperEntry;

        }


        static object SubmittOTPEntry(object param)
        {
            tOneTimeUserRegistration model = param as tOneTimeUserRegistration;
            PaperLaidContext db = new PaperLaidContext();
            model = db.tOneTimeUserRegistration.Add(model);
            db.SaveChanges();
            db.Close();
            return model;
        }

        static object UserRegistrationDetails(object param)
        {
            tUserRegistrationDetails model = param as tUserRegistrationDetails;
            PaperLaidContext db = new PaperLaidContext();
            model = db.tUserRegistrationDetails.Add(model);
            db.SaveChanges();
            db.Close();
            return model;
        }

        static object SubmittMobileUser(object param)
        {
            tTempMobileUser model = param as tTempMobileUser;
            PaperLaidContext db = new PaperLaidContext();
            model = db.tTempMobileUser.Add(model);
            db.SaveChanges();
            db.Close();
            return model;
        }


        static object SaveDetailsInTempPaperLaid(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;

            var query = (from paperLaidTemp in paperLaidCntxtDB.tPaperLaidTemp
                         where paperLaidTemp.PaperLaidId == submitPaperTemp.PaperLaidId
                         && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
                         select paperLaidTemp).SingleOrDefault();

            if (query != null)
            {
                paperLaidCntxtDB.tPaperLaidTemp.Attach(query);
                query.FileName = submitPaperTemp.FileName;
                query.FilePath = submitPaperTemp.FilePath;
                query.DocFileName = submitPaperTemp.DocFileName;
                query.SupFileName = submitPaperTemp.SupFileName;
                query.SupDocFileName = submitPaperTemp.SupDocFileName;
                query.SupVersion = submitPaperTemp.SupVersion;
                query.Version = submitPaperTemp.Version;
                query.DocFileVersion = submitPaperTemp.DocFileVersion;

                var RefNumber1 = submitPaperTemp.AssemblyId + "/" + submitPaperTemp.SessionId + "/" + submitPaperTemp.PaperLaidId + "/" + "V" + submitPaperTemp.Version;
                submitPaperTemp.evidhanReferenceNumber = RefNumber1;
                query.evidhanReferenceNumber = submitPaperTemp.evidhanReferenceNumber;
            }
            else
            {
                submitPaperTemp.DeptSubmittedDate = null;
                paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
            }

            var RefNumber = submitPaperTemp.AssemblyId + "/" + submitPaperTemp.SessionId + "/" + submitPaperTemp.PaperLaidId + "/" + "V" + submitPaperTemp.Version;
            submitPaperTemp.evidhanReferenceNumber = RefNumber;

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;
        }

        static object SaveDetailsInTempPaperLaidOTP(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;

            var query = (from paperLaidTemp in paperLaidCntxtDB.tPaperLaidTemp
                         where paperLaidTemp.PaperLaidId == submitPaperTemp.PaperLaidId
                         && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
                         select paperLaidTemp).SingleOrDefault();

            if (query != null)
            {
                paperLaidCntxtDB.tPaperLaidTemp.Attach(query);
                query.FilePath = submitPaperTemp.FilePath;
                query.DocFileName = submitPaperTemp.DocFileName;
                query.SupDocFileName = submitPaperTemp.SupDocFileName;
            }
            else
            {
                submitPaperTemp.DeptSubmittedDate = null;
                paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
            }

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;
        }



        static object GetActiveDeptActiveId(object param)
        {
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidV submitPaperEntry = param as tPaperLaidV;
            long? ActiveDeptID = (from tPL in paperLaidCntxtDB.tPaperLaidV where tPL.PaperLaidId == submitPaperEntry.PaperLaidId select tPL.DeptActivePaperId).SingleOrDefault();
            if (ActiveDeptID != null)
            {
                return ActiveDeptID;
            }
            return ActiveDeptID;
        }

        static object UpdateDeptActiveId(object param)
        {

            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;

            var tPaperLaid = (from mdl in paperLaidCntxtDB.tPaperLaidV where mdl.PaperLaidId == submitPaperTemp.PaperLaidId select mdl).SingleOrDefault();
            if (tPaperLaid != null)
            {
                tPaperLaid.DeptActivePaperId = submitPaperTemp.PaperLaidTempId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return tPaperLaid;

        }

        static object UpdateQuestionPaperLaidId(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            tQuestion updatePaperLaid = param as tQuestion;
            var tQuestion = (from mdl in paperLaidCntxtDB.tQuestions where mdl.QuestionID == updatePaperLaid.QuestionID select mdl).SingleOrDefault();
            if (tQuestion != null)
            {
                tQuestion.PaperLaidId = updatePaperLaid.PaperLaidId;
                tQuestion.IsFinalApproved = true;
                tQuestion.IsAcknowledgmentDate = updatePaperLaid.IsAcknowledgmentDate;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return tQuestion;

        }

        static object UpdateLocked(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            tUserRegistrationDetails updateLocked = param as tUserRegistrationDetails;


            var tUserRegistrationDetails = (from mdl in paperLaidCntxtDB.tUserRegistrationDetails where mdl.DiaryNumber == updateLocked.DiaryNumber select mdl).FirstOrDefault();
            if (tUserRegistrationDetails != null)
            {
                tUserRegistrationDetails.IsLocked = true;

                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return tUserRegistrationDetails;

        }

        static object UpdateOneTimeUSerLocked(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            tOneTimeUserRegistration updateOneTimeLocked = param as tOneTimeUserRegistration;
            var diaryNo = updateOneTimeLocked.DiaryNumber;
            tOneTimeUserRegistration obj = paperLaidCntxtDB.tOneTimeUserRegistration.Single(m => m.DiaryNumber == diaryNo);
            //var tOneTimeUserRegistration = (from mdl in paperLaidCntxtDB.tOneTimeUserRegistration where mdl.DiaryNumber == updateOneTimeLocked.DiaryNumber select mdl).FirstOrDefault();
            if (updateOneTimeLocked.DiaryNumber != null)
            {

                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return null;

        }


        static object UpdateDepartmentId(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            tQuestion updatePaperLaid = param as tQuestion;
            var tQuestion = (from mdl in paperLaidCntxtDB.tQuestions where mdl.QuestionID == updatePaperLaid.QuestionID select mdl).SingleOrDefault();



            if (tQuestion != null)
            {
                tQuestion.DepartmentID = updatePaperLaid.DepartmentID;
                tQuestion.OldDepartmentId = updatePaperLaid.OldDepartmentId;
                tQuestion.DepartmentID = updatePaperLaid.DepartmentID;
                tQuestion.OldMinistryId = updatePaperLaid.OldMinistryId;
                tQuestion.MinistryId = updatePaperLaid.MinistryId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return tQuestion;

        }

        static object UpdateNoticeDepartmentId(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            tMemberNotice updatePaperLaid = param as tMemberNotice;
            var tNotice = (from mdl in paperLaidCntxtDB.tMemberNotice where mdl.NoticeId == updatePaperLaid.NoticeId select mdl).SingleOrDefault();



            if (tNotice != null)
            {
                tNotice.DepartmentId = updatePaperLaid.DepartmentId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return tNotice;

        }


        static object UpdateNoticeDepartmentMinistryId(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            tMemberNotice updatePaperLaid = param as tMemberNotice;
            var tNotice = (from mdl in paperLaidCntxtDB.tMemberNotice where mdl.NoticeId == updatePaperLaid.NoticeId select mdl).SingleOrDefault();

            if (tNotice != null)
            {
                tNotice.DepartmentId = updatePaperLaid.DepartmentId;

                tNotice.OldDepartmentId = updatePaperLaid.OldDepartmentId;
                tNotice.DepartmentId = updatePaperLaid.DepartmentId;
                tNotice.OldMinistryId = updatePaperLaid.OldMinistryId;
                tNotice.MinistryId = updatePaperLaid.MinistryId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return tNotice;

        }


        public static object UpdateQuestionDepartmentMinistryId(object param)
        {
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tQuestion model1 = param as tQuestion;
            var DiaryId = model1.MergeDiaryNo;
            if (DiaryId != "")
            {
                tQuestion obj = paperLaidCntxtDB.tQuestions.Single(m => m.DiaryNumber == model1.DiaryNumber);
                obj.OldDepartmentId = model1.OldDepartmentId;
                obj.DepartmentID = model1.DepartmentID;
                obj.OldMinistryId = model1.OldMinistryId;
                obj.MinistryId = model1.MinistryId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

                string csv = model1.MergeDiaryNo;

                string[] obja = csv.Split(',');

                foreach (var item in obja)
                {

                    using (var obj1 = new PaperLaidContext())
                    {
                        string id = item;

                        var PaperLaidObj = (from PaperLaidModel in obj1.tQuestions
                                            where PaperLaidModel.DiaryNumber == id
                                            select PaperLaidModel).FirstOrDefault();
                        tQuestion updatePaperLaid = param as tQuestion;

                        if (PaperLaidObj != null)
                        {



                            PaperLaidObj.OldDepartmentId = updatePaperLaid.OldDepartmentId;
                            PaperLaidObj.DepartmentID = updatePaperLaid.DepartmentID;
                            PaperLaidObj.OldMinistryId = updatePaperLaid.OldMinistryId;
                            PaperLaidObj.MinistryId = updatePaperLaid.MinistryId;
                            obj1.SaveChanges();
                            obj1.Close();

                        }


                    }
                }

            }
            else
            {

                tQuestion updatePaperLaid = param as tQuestion;
                using (var obj1 = new PaperLaidContext())
                {
                    var tQuestion1 = (from mdl in obj1.tQuestions
                                      where mdl.QuestionID == updatePaperLaid.QuestionID

                                      select mdl).SingleOrDefault();

                    if (tQuestion1 != null)
                    {

                        tQuestion1.OldDepartmentId = updatePaperLaid.OldDepartmentId;
                        tQuestion1.DepartmentID = updatePaperLaid.DepartmentID;
                        tQuestion1.OldMinistryId = updatePaperLaid.OldMinistryId;
                        tQuestion1.MinistryId = updatePaperLaid.MinistryId;
                        obj1.SaveChanges();
                        obj1.Close();

                    }
                }

                return updatePaperLaid;


            }

            return model1;

        }


        public static object UpdateIsAcknowledgmentDateByDiaryNumber(object param)
        {

            tQuestion model1 = param as tQuestion;

            //tQuestion updatePaperLaid = param as tQuestion;
            using (var obj1 = new PaperLaidContext())
            {
                var tQuestion1 = (from mdl in obj1.tQuestions
                                  where mdl.QuestionID == model1.QuestionID

                                  select mdl).FirstOrDefault();

                if (tQuestion1 != null)
                {

                    tQuestion1.IsAcknowledgmentDate = model1.IsAcknowledgmentDate;
                    tQuestion1.UserName = model1.UserName;
                    tQuestion1.AadharId = model1.AadharId;
                    obj1.SaveChanges();
                    obj1.Close();

                }
            }

            return model1;


        }


        public static object UpdateIsAcknowledgmentDateByNoticeId(object param)
        {

            tMemberNotice model1 = param as tMemberNotice;

            tMemberNotice updatePaperLaid = param as tMemberNotice;
            using (var obj1 = new PaperLaidContext())
            {
                var tQuestion1 = (from mdl in obj1.tMemberNotice
                                  where mdl.NoticeId == updatePaperLaid.NoticeId

                                  select mdl).SingleOrDefault();

                if (tQuestion1 != null)
                {

                    tQuestion1.IsAcknowledgmentDate = updatePaperLaid.IsAcknowledgmentDate;
                    tQuestion1.AadharId = updatePaperLaid.AadharId;
                    obj1.SaveChanges();
                    obj1.Close();

                }
            }

            return updatePaperLaid;


        }



        static object InsertBillDetails(object param)
        {
            if (null == param)
            { return null; }
            tBillRegister bills = new tBillRegister();
            tPaperLaidV mdl = param as tPaperLaidV;
            bills.PaperLaidId = (int)mdl.PaperLaidId;
            bills.BillDate = Convert.ToDateTime(mdl.BillDate);
            bills.BillNumber = mdl.BillNUmberValue + " of " + mdl.BillNumberYear;
            bills.AssemblyId = mdl.AssemblyCode;
            bills.SessionId = mdl.SessionCode;
            bills.MinisterId = mdl.MinistryId;
            bills.DepartmentId = mdl.DepartmentId;
            bills.BillTypeId = mdl.BillTypeId;
            bills.IsBillAct = false;
            bills.IsBillReturn = false;
            bills.IsBillSigned = false;
            bills.IsCirculation = false;
            bills.IsClauseConsideration = false;
            bills.IsElicitingOpinion = false;
            bills.IsFinancialBillRule1 = false;
            bills.IsFinancialBillRule2 = false;
            bills.IsForwordedToMinister = false;
            bills.IsIntentioned = false;
            bills.IsIntentionedToMinister = false;
            bills.IsIntentionReturn = false;
            bills.IsIntentionSigned = false;
            bills.IsMoneyBill = false;
            bills.IsRecommendedArticle = false;
            bills.IsReferSelectCommitte = false;
            bills.IsSubmitted = false;
            bills.IsThirdReading = false;
            bills.IsWithdrawal = false;
            bills.ShortTitle = mdl.Description;
            using (PaperLaidContext pCtxt = new PaperLaidContext())
            {
                bills = pCtxt.tBillRegister.Add(bills);
                pCtxt.SaveChanges();
                pCtxt.Close();
            }
            return bills;
        }

        static object SubmitPendingDepartmentPaperLaid(object param)
        {
            tPaperLaidTemp mdl = param as tPaperLaidTemp;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

            var tPaperLaidTemps = (from md in paperLaidCntxtDB.tPaperLaidTemp
                                   where md.PaperLaidId == mdl.PaperLaidId && md.Version ==
                                       (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == mdl.PaperLaidId select bdMax.Version).Max()
                                   select md).SingleOrDefault();
            //var tPaperLaidTemps = (from model in paperLaidCntxtDB.tPaperLaidTemp
            //                       where model.PaperLaidId == mdl.PaperLaidId && model.PaperLaidTempId == query.PaperLaidTempId
            //                       select model).SingleOrDefault();

            if (tPaperLaidTemps != null)
            {
                tPaperLaidTemps.DeptSubmittedDate = mdl.DeptSubmittedDate;
                tPaperLaidTemps.DeptSubmittedBy = mdl.DeptSubmittedBy;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }


            return tPaperLaidTemps;
        }

        static object EditPendingPaperLaidDetials(object param)
        {
            tPaperLaidV mdl = param as tPaperLaidV;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            mdl = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == mdl.PaperLaidId select model).SingleOrDefault();
            var fileInfo = (from model in paperLaidCntxtDB.tPaperLaidTemp
                            join pld in paperLaidCntxtDB.tPaperLaidV on model.PaperLaidId equals pld.PaperLaidId
                            where model.PaperLaidId == mdl.PaperLaidId
                            && model.DeptSubmittedBy == null
                            && model.DeptSubmittedDate == null
                            && pld.DeptActivePaperId == model.PaperLaidTempId
                            select model).SingleOrDefault();
            var Qinfo = (from model in paperLaidCntxtDB.tQuestions
                         where model.PaperLaidId == mdl.PaperLaidId
                         select model).SingleOrDefault();
            mdl.FileName = fileInfo.FileName;
            mdl.DocFileName = fileInfo.DocFileName;
            mdl.SupFileName = fileInfo.SupFileName;
            mdl.SupDocFileName = fileInfo.SupDocFileName;
            mdl.FilePath = fileInfo.FilePath + fileInfo.FileName;
            mdl.DocFilePath = fileInfo.FilePath + fileInfo.DocFileName;
            mdl.SupFilePath = fileInfo.FilePath + fileInfo.SupFileName;
            mdl.SupDocFilepath = fileInfo.FilePath + fileInfo.SupDocFileName;
            mdl.FileVersion = fileInfo.Version;
            mdl.SupFileVersion = fileInfo.SupVersion;
            mdl.DocFileVersion = fileInfo.DocFileVersion;
            mdl.FileStructurePath = fileInfo.FileStructurePath;
            mdl.DepartmentId = Qinfo.DepartmentID;
            mdl.MinistryId = Qinfo.MinistryId.Value;
            //  mdl.QuestionTypeId = Qinfo.QuestionType;
            mdl.DeparmentName = (from model in paperLaidCntxtDB.mDepartment
                                 where model.deptId == mdl.DepartmentId
                                 select model.deptname).SingleOrDefault();
            mdl.MinistryName = (from model in paperLaidCntxtDB.mMinistry
                                where model.MinistryID == mdl.MinistryId
                                select model.MinistryName).SingleOrDefault();
            return mdl;
        }

        static object UpdatePaperLaidEntry(object param)
        {
            tPaperLaidV updatedDetails = param as tPaperLaidV;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            var existingDetails = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == updatedDetails.PaperLaidId select model).SingleOrDefault();

            if (existingDetails != null)
            {
                existingDetails.EventId = updatedDetails.EventId;
                existingDetails.MinistryId = updatedDetails.MinistryId;
                existingDetails.DepartmentId = updatedDetails.DepartmentId;
                existingDetails.SessionId = updatedDetails.SessionCode;
                existingDetails.PaperTypeID = updatedDetails.PaperTypeID;
                existingDetails.Title = updatedDetails.Title;
                existingDetails.Description = updatedDetails.Description;
                existingDetails.Remark = updatedDetails.Remark;

                existingDetails.PaperLaidId = updatedDetails.PaperLaidId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }
            return existingDetails;
        }

        static object UpdateDetailsInTempPaperLaid(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
            var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidTempId == submitPaperTemp.PaperLaidTempId select mdl).SingleOrDefault();
            if (existingTPaperLaidTemp != null)
            {
                existingTPaperLaidTemp.FileName = submitPaperTemp.FileName;
                //    existingTPaperLaidTemp.FilePath = submitPaperTemp.FilePath;
            }

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;
        }

        static object GetExisitngFileDetails(object param)
        {

            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidV details = param as tPaperLaidV;

            var existingTPaperLaidTemp = (from md in paperLaidCntxtDB.tPaperLaidTemp
                                          where md.PaperLaidId == details.PaperLaidId && md.Version ==
                                              (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == details.PaperLaidId select bdMax.Version).Max()
                                          select md).SingleOrDefault();
            if (existingTPaperLaidTemp != null)
            {
                return existingTPaperLaidTemp;
            }

            return null;
        }

        static tPaperLaidV UpcomingLOBByQuestionType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            var query = (from questions in pCtxt.tQuestions
                         join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                         where questions.QuestionType == model.QuestionTypeId
                          && ids.Contains(questions.DepartmentID)
                             //New
                             && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId

                             && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
                             && paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
                             && paperLaid.DesireLayingDate > DateTime.Now && paperLaid.LaidDate == null && paperLaid.LOBRecordId != null
                             && paperLaid.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId
                         //paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                         select new PaperSendModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                             Subject = paperLaid.Title,
                             PaperLaidId = questions.PaperLaidId,
                             Version = paperLaidTemp.Version,
                             DeptActivePaperId = paperLaid.DeptActivePaperId,
                             DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                             PaperSent = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                             FileName = paperLaidTemp.FileName,
                             DiaryNumber = questions.DiaryNumber,
                             DeActivateFlag = questions.DeActivateFlag,
                             IsPostpone = questions.IsPostpone,
                             IsPostponeDate = questions.IsPostponeDate,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                         }).ToList();


            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionSendModel = results;

            return model;
        }

        // Paper pending list
        static List<PaperMovementModel> GetSubmittedPaperLaidList(object param)
        {
            PaperLaidContext db = new PaperLaidContext();
            PaperMovementModel model = param as PaperMovementModel;

            int? EventId = null;
            int? PaperCategoryTypeId = null;
            if (model.EventId != 0 && model.PaperCategoryTypeId != 0)
            {
                EventId = model.EventId;
            }
            else if (model.PaperCategoryTypeId != 0)
            {
                PaperCategoryTypeId = model.PaperCategoryTypeId;
            }

            var query = (from m in db.tPaperLaidV
                         join c in db.mEvent on m.EventId equals c.EventId
                         join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         where m.DeptActivePaperId == t.PaperLaidTempId
                         && (!EventId.HasValue || m.EventId == model.EventId)
                         select new PaperMovementModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             DeparmentName = m.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)m.PaperLaidId,
                             actualFilePath = t.FilePath + t.FileName,
                             PaperCategoryTypeId = c.PaperCategoryTypeId,
                             BusinessType = c.EventName,
                             version = t.Version,
                             DeptSubmittedBy = t.DeptSubmittedBy,
                             DeptSubmittedDate = t.DeptSubmittedDate,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault()

                         }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            foreach (var item in query)
            {
                switch (item.PaperCategoryTypeId)
                {
                    case 1:
                        item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                        break;
                    case 2:
                        item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                        break;
                    case 4:
                        item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                        break;
                    default:
                        break;
                }
            }

            if (PaperCategoryTypeId != null)
            {
                var filterQuery = query.Where(foo => foo.PaperCategoryTypeId == PaperCategoryTypeId);
                return filterQuery.ToList();
            }

            return query.ToList();
        }

        static object DeletePaperLaidByID(object param)
        {
            tPaperLaidV tPaperLaid = param as tPaperLaidV;
            PaperLaidContext db = new PaperLaidContext();

            //remove from tPaperLaidV

            var query = (from mdl in db.tPaperLaidV where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            db.tPaperLaidV.Remove(query);


            //Remove from tPaperLaidTemp

            var query1 = (from mdl in db.tPaperLaidTemp where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            db.tPaperLaidTemp.Remove(query1);

            //Remove From Bill if exist

            var query2 = (from mdl in db.tBillRegister where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            if (query2 != null)
            {
                db.tBillRegister.Remove(query2);
            }

            //Remove Id form tQuestion if exist

            var query3 = (from mdl in db.tQuestions where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            if (query3 != null)
            {
                query3.PaperLaidId = null;
            }
            //Remove Id form tMemberNotice if exist

            var query4 = (from mdl in db.tMemberNotice where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            if (query4 != null)
            {
                query4.PaperLaidId = null;
            }
            db.SaveChanges();
            db.Close();
            return null;
        }


        static object UpdatePaperLaidOtherFileByID(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;

            var query = (from paperLaidTemp in paperLaidCntxtDB.tPaperLaidTemp
                         where paperLaidTemp.PaperLaidId == submitPaperTemp.PaperLaidId
                         select paperLaidTemp).SingleOrDefault();

            if (query != null)
            {
                paperLaidCntxtDB.tPaperLaidTemp.Attach(query);
                query.FileName = submitPaperTemp.FileName;
                query.FilePath = submitPaperTemp.FilePath;
                query.DocFileName = submitPaperTemp.DocFileName;
                query.SupFileName = submitPaperTemp.SupFileName;
                query.SupDocFileName = submitPaperTemp.SupDocFileName;
                query.SupVersion = submitPaperTemp.SupVersion;
                query.Version = submitPaperTemp.Version;
                query.DeptSubmittedDate = null;
                query.DeptSubmittedBy = null;
            }
            else
            {
                submitPaperTemp.DeptSubmittedDate = null;
                paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
            }

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;



        }

        static object UpdatePaperLaidFileByID(object param)
        {

            tPaperLaidTemp model = param as tPaperLaidTemp;

            PaperLaidContext db = new PaperLaidContext();
            if (model == null)
            {
                return null;
            }
            model.DeptSubmittedDate = null;
            model.DeptSubmittedBy = null;
            model = db.tPaperLaidTemp.Add(model);
            db.SaveChanges();
            db.Close();
            return model;
        }

        //static object ShowPaperLaidDetailByID(object param)
        //{
        //    PaperLaidContext db = new PaperLaidContext();
        //    PaperMovementModel model = param as PaperMovementModel;

        //    var query = (from tPaper in db.tPaperLaidV
        //                 where model.PaperLaidId == tPaper.PaperLaidId
        //                 join questions in db.tQuestions on tPaper.PaperLaidId equals questions.PaperLaidId
        //                 join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
        //                 join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
        //                 join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
        //                 join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
        //                 join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
        //                 where tPaper.DeptActivePaperId == tPaperTemp.PaperLaidTempId
        //                 select new PaperMovementModel
        //                 {
        //                     EventName = mEvents.EventName,
        //                     MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
        //                     DeparmentName = mDept.deptname,
        //                     PaperLaidId = tPaper.PaperLaidId,
        //                     actualFilePath = tPaperTemp.FileName,
        //                     ReplyPathFileLocation = tPaperTemp.SignedFilePath,
        //                     ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
        //                     Title = tPaper.Title,
        //                     Description = tPaper.Description,
        //                     Remark = tPaper.Remark,
        //                     PaperCategoryTypeId = mPaperCategory.PaperCategoryTypeId,
        //                     PaperCategoryTypeName = mPaperCategory.Name,
        //                     PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
        //                     LaidDate = tPaper.LaidDate,
        //                     version = tPaperTemp.Version,
        //                     QuestionTypeId = questions.QuestionType,
        //                     AssemblyId = tPaper.AssemblyId,
        //                     SessionId = tPaper.SessionId,
        //                     Status = (int)QuestionDashboardStatus.RepliesSent,
        //                     DepartmentId = tPaper.DepartmentId,
        //                     DiaryNumber = questions.DiaryNumber,
        //                     MemberName = (from member in db.mMember
        //                                   where member.MemberCode == questions.MemberID
        //                                   select member.Name).FirstOrDefault(),
        //                     ReferenceMemberCode = questions.ReferenceMemberCode,
        //                     IsClubbed = questions.IsClubbed,
        //                     MergeDiaryNo = questions.MergeDiaryNo,
        //                     DocFileName = tPaperTemp.DocFileName,
        //                     SupFileName = tPaperTemp.SupFileName,
        //                     SupDocFileName = tPaperTemp.SupDocFileName,
        //                     DocFilePath = tPaperTemp.FilePath + tPaperTemp.DocFileName,
        //                     DesireLayingDate = questions.SessionDateId == null ? (DateTime?)null : (from sessionDates in db.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
        //                 }).FirstOrDefault();


        //    var categoryName = query.PaperCategoryTypeName;
        //    if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
        //    {
        //        int? Number = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId && mdl.IsFinalApproved == true select mdl.QuestionNumber).FirstOrDefault();

        //        ////New
        //        //var questionDet = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId select mdl).FirstOrDefault();
        //        //if (questionDet != null)
        //        //{
        //        //    query.MemberName = (from member in db.mMember
        //        //                        where member.MemberCode == questionDet.MemberID
        //        //                        select member.Name).FirstOrDefault();
        //        //}
        //        query.Number = Number.ToString();
        //    }


        //    return query;
        //}

        static object GetFileVersion(object param)
        {
            PaperLaidContext db = new PaperLaidContext();
            PaperMovementModel paper = param as PaperMovementModel;
            tPaperLaidV model = new tPaperLaidV();

            model.Count = (from tPaperTemp in db.tPaperLaidTemp
                           where tPaperTemp.PaperLaidId == paper.PaperLaidId
                           select tPaperTemp.PaperLaidId).Count();
            model.Count = model.Count + 1;

            model.SupPDFCount = (from tPaperTemp in db.tPaperLaidTemp
                                 where tPaperTemp.PaperLaidId == paper.PaperLaidId
                                 select tPaperTemp.PaperLaidId).Count();
            if (paper.SupFileName != null)
            {
                model.SupPDFCount = model.SupPDFCount + 1;
            }
            model.MainDocCount = (from tPaperTemp in db.tPaperLaidTemp
                                  where tPaperTemp.PaperLaidId == paper.PaperLaidId
                                  select tPaperTemp.PaperLaidId).Count();
            if (paper.DocFileName != null)
            {
                model.MainDocCount = model.MainDocCount + 1;
            }
            return model;
        }

        static object ShowPaperLaidDetailByID(object param)
        {
            PaperLaidContext db = new PaperLaidContext();
            PaperMovementModel model = param as PaperMovementModel;

            var siteSettingData = (from a in db.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;

            var query = (from tPaper in db.tPaperLaidV
                         where model.PaperLaidId == tPaper.PaperLaidId
                         join questions in db.tQuestions on tPaper.PaperLaidId equals questions.PaperLaidId
                         join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                         join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                         join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                         join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
                         join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                         where tPaper.DeptActivePaperId == tPaperTemp.PaperLaidTempId
                         select new PaperMovementModel
                         {
                             EventName = mEvents.EventName,
                             MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
                             DeparmentName = mDept.deptname,
                             PaperLaidId = tPaper.PaperLaidId,
                             actualFilePath = tPaperTemp.FileName,
                             ReplyPathFileLocation = FileStructurePath + tPaperTemp.SignedFilePath,
                             ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                             Title = tPaper.Title,
                             Description = tPaper.Description,
                             Remark = tPaper.Remark,
                             PaperCategoryTypeId = mPaperCategory.PaperCategoryTypeId,
                             PaperCategoryTypeName = mPaperCategory.Name,
                             PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
                             LaidDate = tPaper.LaidDate,
                             PaperLaidTempId = tPaperTemp.PaperLaidTempId,
                             version = tPaperTemp.Version,
                             QuestionTypeId = questions.QuestionType,
                             AssemblyId = tPaper.AssemblyId,
                             SessionId = tPaper.SessionId,
                             Status = (int)QuestionDashboardStatus.RepliesSent,
                             DepartmentId = tPaper.DepartmentId,
                             DiaryNumber = questions.DiaryNumber,
                             MemberName = (from member in db.mMember
                                           where member.MemberCode == questions.MemberID
                                           select member.Name).FirstOrDefault(),
                             ReferenceMemberCode = questions.ReferenceMemberCode,
                             IsClubbed = questions.IsClubbed,
                             MergeDiaryNo = questions.MergeDiaryNo,
                             DocFileName = tPaperTemp.DocFileName,
                             SupFileName = tPaperTemp.SupFileName,
                             SupDocFileName = tPaperTemp.SupDocFileName,
                             DocFilePath = tPaperTemp.FilePath + tPaperTemp.DocFileName,
                             DesireLayingDate = questions.SessionDateId == null ? (DateTime?)null : (from sessionDates in db.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
                         }).FirstOrDefault();


            var categoryName = query.PaperCategoryTypeName;
            if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                int? Number = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId && mdl.IsFinalApproved == true select mdl.QuestionNumber).FirstOrDefault();

                ////New
                //var questionDet = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId select mdl).FirstOrDefault();
                //if (questionDet != null)
                //{
                //    query.MemberName = (from member in db.mMember
                //                        where member.MemberCode == questionDet.MemberID
                //                        select member.Name).FirstOrDefault();
                //}
                query.Number = Number.ToString();
            }


            return query;
        }

        #region Newly Added (Ram)

        //Dated : 2nd-June.
        #region TotalQuestionsByType (PendingForReply, DraftReplies, RepliesSent)
        //Added Code venkat for Dynamic Menu
        public static object PendingForReplyQuestionsList(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;
            //Added Code venkat for Dynamic Menu
            string DiaryNumber = "";
            string tiTle = "";
            int? Qnum = 0;
            // int? MemberId = 0;
            if (model.DiaryNumber != null && model.DiaryNumber != "null")
            {
                DiaryNumber = model.DiaryNumber;
            }
            if (model.Title != null && model.Title != "null")
            {
                tiTle = model.Title;
            }
            if (model.QuestionNumber != 0 && model.QuestionNumber != null)
            {
                Qnum = model.QuestionNumber;
            }
            //if (model.MemberId != 0 && model.MemberId != null)
            //{
            //    MemberId = model.MemberId;
            //}
            //end-------------------------------------
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            var PendingForReply = (from questions in pCtxt.tQuestions
                                       //join m in pCtxt.tMemberNotice on questions.MemberID equals m.MemberId
                                   where questions.QuestionType == model.QuestionTypeId
                                   && ids.Contains(questions.DepartmentID)
                                   && questions.PaperLaidId == null
                                   && questions.AssemblyID == model.AssemblyId
                                   && questions.SessionID == model.SessionId
                                   && (questions.QuestionStatus == (int)Questionstatus.QuestionSent)
                                     && questions.IsBracket == null
                                   //Added Code venkat for Dynamic Menu
                                   //&& (DiaryNumber == "" || questions.DiaryNumber == DiaryNumber)
                                   //&& (tiTle == "" || questions.Subject == tiTle)
                                   //&& (Qnum == 0 || questions.QuestionNumber == Qnum)
                                   orderby questions.QuestionID ascending
                                   //&& (MemberId == 0 || questions.MemberID == MemberId)
                                   //end--------------------------------------------
                                   select new QuestionModelCustom
                                   {
                                       QuestionID = questions.QuestionID,
                                       QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                       ConvertQuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                       Subject = questions.Subject,
                                       PaperLaidId = questions.PaperLaidId,
                                       Version = null,
                                       IsAcknowledgmentDate = questions.IsAcknowledgmentDate,
                                       DeptActivePaperId = null,
                                       SubmittedDate = questions.SubmittedDate,
                                       SessionDateId = questions.SessionDateId,
                                       Status = (int)QuestionDashboardStatus.PendingForReply,
                                       DiaryNumber = questions.DiaryNumber,
                                       DeActivateFlag = questions.DeActivateFlag,
                                       IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                       IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                       IsBracket = questions.IsBracket,
                                       ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                       IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                       MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                       DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                   }).ToList();

            foreach (var item in PendingForReply)
            {
                item.QuestionNumber = item.ConvertQuestionNumber;
                if (item.QuestionNumber != null)
                {

                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;

                }
                else
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = "";
                }
                item.SubmittedDateString = Convert.ToDateTime(item.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                if (item.IsAcknowledgmentDate != null)
                {
                    item.IsAcknowledgmentDateString = Convert.ToDateTime(item.IsAcknowledgmentDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                }
                else
                {
                    item.IsAcknowledgmentDate = null;
                }

            }

            foreach (var val in PendingForReply)
            {
                string s = val.ReferenceMemberCode;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item = (from questions in pCtxt.tQuestions
                                    select new QuestionModelCustom
                                    {
                                        MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                                    }).FirstOrDefault();

                        if (string.IsNullOrEmpty(val.RMemberName))
                        {
                            val.RMemberName = item.MemberName;
                        }
                        else
                        {
                            val.RMemberName = val.RMemberName + "," + item.MemberName;
                        }


                    }
                }


            }

            return PendingForReply;
        }
        //Added Code venkat for Dynamic Menu

        public static object PaperLaidFilePath(object param)
        {
            tPaperLaidTemp model = param as tPaperLaidTemp;
            PaperLaidContext pCtxt = new PaperLaidContext();
            var PaperLaidId = (from a in pCtxt.tPaperLaidTemp
                               where a.PaperLaidId == model.PaperLaidId
                               orderby a.Version descending
                               select a).Take(1).FirstOrDefault();
            return PaperLaidId;

        }

        public static object DraftRepliesQuestionsListBeforeSent(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();
            //Added Code venkat for Dynamic Menu
            string diarynumber = "";
            string tiTle = "";
            int? Qnum = 0;
            int? MemberId = 0;
            if (model.DiaryNumber != null && model.DiaryNumber != "null")
            {
                diarynumber = model.DiaryNumber;
            }
            if (model.Title != null && model.Title != "null")
            {
                tiTle = model.Title;
            }
            if (model.QuestionNumber != 0 && model.QuestionNumber != null)
            {
                Qnum = model.QuestionNumber;
            }
            //if (model.MemberId != 0 && model.MemberId != null)
            //{
            //    MemberId = model.MemberId;
            //}

            //end--------------------------------
            string csv = model.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            var siteSettingData = (from a in pCtxt.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;


            var DraftReplies = (from questions in pCtxt.tQuestions
                                join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                //join m in pCtxt.tMemberNotice on questions.MemberID equals m.MemberId
                                join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                where questions.QuestionType == model.QuestionTypeId
                                  && ids.Contains(questions.DepartmentID)
                                  && questions.PaperLaidId != null
                                  && paperLaidTemp.DeptSubmittedBy == null
                                  && paperLaidTemp.DeptSubmittedDate == null
                                  && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                  && questions.AssemblyID == model.AssemblyId
                                   && questions.SessionID == model.SessionId
                                   && (diarynumber == "" || questions.DiaryNumber == diarynumber)
                                   && (tiTle == "" || questions.Subject == tiTle)
                                    && (Qnum == 0 || questions.QuestionNumber == Qnum)
                                //  && (MemberId == 0 || questions.MemberID == MemberId)
                                select new QuestionModelCustom
                                {
                                    QuestionID = questions.QuestionID,
                                    QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                    ConvertQuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                    Subject = paperLaid.Title,
                                    PaperLaidId = questions.PaperLaidId,
                                    Version = paperLaidTemp.Version,
                                    DeptActivePaperId = paperLaid.DeptActivePaperId,
                                    SessionDateId = questions.SessionDateId,
                                    IsAcknowledgmentDate = questions.IsAcknowledgmentDate,
                                    SubmittedDate = questions.SubmittedDate,

                                    evidhanReferenceNumber = paperLaidTemp.evidhanReferenceNumber,
                                    Status = (int)QuestionDashboardStatus.DraftReplies,
                                    DiaryNumber = questions.DiaryNumber,
                                    PaperSent = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                    //PaperSent = FileStructurePath + paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                    DeActivateFlag = questions.DeActivateFlag,
                                    IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                    IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                    ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                    IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                    IsBracket = questions.IsBracket,
                                    MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                }).ToList();


            foreach (var item in DraftReplies)
            {
                item.QuestionNumber = item.ConvertQuestionNumber;

                if (item.QuestionNumber != null)
                {

                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }
                else
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = "";
                }
                item.SubmittedDateString = Convert.ToDateTime(item.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                if (item.IsAcknowledgmentDate != null)
                {
                    item.IsAcknowledgmentDateString = Convert.ToDateTime(item.IsAcknowledgmentDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                }
                else
                {
                    item.IsAcknowledgmentDate = null;
                }

            }
            return DraftReplies;
        }
        //Added Code venkat for Dynamic Menu
        public static object DraftRepliesQuestionsList(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;
            //Added Code venkat for Dynamic Menu
            string diarynum = "";
            string tiTle = "";
            int? Qnum = 0;
            int? MemberId = 0;
            if (model.DiaryNumber != null && model.DiaryNumber != "null")
            {
                diarynum = model.DiaryNumber;
            }
            if (model.Title != null && model.Title != "null")
            {
                tiTle = model.Title;
            }
            if (model.QuestionNumber != 0 && model.QuestionNumber != null)
            {
                Qnum = model.QuestionNumber;
            }
            //if (model.MemberId != 0 && model.MemberId != null)
            //{
            //    MemberId = model.MemberId;
            //}

            //end------------------------------------

            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            //Get Pending for reply questions.
            var DraftReplies = (from questions in pCtxt.tQuestions
                                join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                //join m in pCtxt.tMemberNotice on questions.MemberID equals m.MemberId
                                where questions.QuestionType == model.QuestionTypeId
                                && questions.AssemblyID == model.AssemblyId
                                && questions.SessionID == model.SessionId
                                && ids.Contains(questions.DepartmentID)
                                && questions.PaperLaidId != null
                                && paperLaidTemp.FileName != null
                                && paperLaidTemp.DeptSubmittedBy == null
                                && paperLaidTemp.DeptSubmittedDate == null
                                && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                && questions.IsFinalApproved == true
                                 //Added Code venkat for Dynamic Menu
                                 && (diarynum == "" || questions.DiaryNumber == diarynum)
                                   && (tiTle == "" || questions.Subject == tiTle)
                                    && (Qnum == 0 || questions.QuestionNumber == Qnum)
                                //&& paperLaidTemp.Version!=null
                                //&& (MemberId == 0 || questions.MemberID == MemberId)
                                orderby paperLaidTemp.DeptSubmittedDate ascending

                                select new QuestionModelCustom
                                {
                                    QuestionID = questions.QuestionID,
                                    QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                    //DiaryNumber=diarynum,
                                    Subject = paperLaid.Title,
                                    PaperLaidId = questions.PaperLaidId,
                                    Version = paperLaidTemp.Version,
                                    DeptActivePaperId = paperLaid.DeptActivePaperId,
                                    IsAcknowledgmentDate = questions.IsAcknowledgmentDate,
                                    SessionDateId = questions.SessionDateId,
                                    Status = (int)QuestionDashboardStatus.DraftReplies,
                                    DiaryNumber = questions.DiaryNumber,
                                    PaperSent = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                    DeActivateFlag = questions.DeActivateFlag,
                                    IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                    IsBracket = questions.IsBracket,
                                    IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                    ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                    IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                    MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                }).ToList();


            return DraftReplies;
        }



        public static object RepliesSentQuestionsListAll(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);


            //Get Submitted Questions.
            var RepliesSent = (from questions in pCtxt.tQuestions
                               join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                               join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                               where questions.QuestionType == model.QuestionTypeId
                                   && ids.Contains(questions.DepartmentID)
                                   && questions.AssemblyID == model.AssemblyId
                                   && questions.SessionID == model.SessionId
                                   && questions.PaperLaidId != null
                                   && paperLaidTemp.DeptSubmittedBy != null
                                   && paperLaidTemp.DeptSubmittedDate != null
                                   && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                               select new QuestionModelCustom
                               {
                                   QuestionID = questions.QuestionID,
                                   QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                   Subject = paperLaid.Title,
                                   PaperLaidId = questions.PaperLaidId,
                                   Version = paperLaidTemp.Version,
                                   DeptActivePaperId = paperLaid.DeptActivePaperId,
                                   SessionDateId = questions.SessionDateId,
                                   DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                                   PaperSent = paperLaidTemp.SignedFilePath,
                                   FileName = paperLaidTemp.FileName,
                                   DiaryNumber = questions.DiaryNumber,
                                   DeActivateFlag = questions.DeActivateFlag,
                                   IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                   IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                   ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                   IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                   IsBracket = questions.IsBracket,
                                   Status = (int)QuestionDashboardStatus.RepliesSent,
                                   MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                   DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                               }).ToList();

            foreach (var item in RepliesSent)
            {

                var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;

                var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;

            }

            return RepliesSent;
        }

        #endregion

        static object GetAllQuestions(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            List<QuestionModelCustom> customQuestions = new List<QuestionModelCustom>();

            PaperLaidContext pCtxt = new PaperLaidContext();

            #region Get All Questions By Type
            //Get Pending Questions.
            var PendingForReply = PendingForReplyQuestionsList(param) as List<QuestionModelCustom>;

            //Get Submitted Questions.
            var RepliesSent = RepliesSentQuestionsListAll(param) as List<QuestionModelCustom>;

            //Get Pending for reply questions.
            var DraftReplies = DraftRepliesQuestionsListBeforeSent(param) as List<QuestionModelCustom>;

            #endregion

            customQuestions.AddRange(PendingForReply);
            customQuestions.AddRange(RepliesSent);
            customQuestions.AddRange(DraftReplies);

            model.ResultCount = PendingForReply.Count() + RepliesSent.Count;
            //+ DraftReplies.Count


            model.tQuestionModel = customQuestions.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();



            return model;
        }

        static tPaperLaidV GetPendingQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidContext pCtxt = new PaperLaidContext();

            var query = PendingForReplyQuestionsList(param) as List<QuestionModelCustom>;

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }

        static tPaperLaidV GetSubmittedQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidContext pCtxt = new PaperLaidContext();
            var query = RepliesSentQuestionsList(param) as List<PaperSendModelCustom>;
            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.TotalStaredSubmitted = totalRecords;
            model.tQuestionSendModel = results;

            return model;
        }



        static tPaperLaidV GetPendingForSubQuestionsByTypeNew(object param)
        {
            /// tPaperLaidV model = param as tPaperLaidV;
            tPaperLaidV Newmodel = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            var query = DraftRepliesQuestionsList(param) as List<QuestionModelCustom>;
            int tscount = query.Count();
            int totalRecords = query.Count();
            var results = query.Skip((Newmodel.PageIndex - 1) * Newmodel.PAGE_SIZE).Take(Newmodel.PAGE_SIZE).ToList();



            //model.ResultCount = totalRecords;
            if (Newmodel.QuestionTypeId == 1)
            {
                Newmodel.TotalStaredPendingForSubmissionNew = tscount;
            }
            else if (Newmodel.QuestionTypeId == 2)
            {
                Newmodel.TotalUnStaredPendingForSubmissionNew = tscount;
            }
            Newmodel.tQuestionModel = results;

            return Newmodel;
        }


        static tPaperLaidV GetPendingForSubQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidContext pCtxt = new PaperLaidContext();
            var query = DraftRepliesQuestionsListBeforeSent(param) as List<QuestionModelCustom>;
            var query1 = DraftRepliesQuestionsList(param) as List<QuestionModelCustom>;
            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }

        //Get all the questions Which are laid in house.
        //TODO : Change the way it is working at the moment (As we are dealing with questions here we should be able to pass and get instance of Question model)
        static tPaperLaidV GetLaidInHouseQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            var query = (from questions in pCtxt.tQuestions
                         join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                         join msessiondate in pCtxt.mSessionDates on questions.SessionDateId equals msessiondate.Id
                         where questions.QuestionType == model.QuestionTypeId
                             && ids.Contains(questions.DepartmentID)
                             && questions.AssemblyID == model.AssemblyId
                             && questions.SessionID == model.SessionId
                             && questions.PaperLaidId != null
                             && paperLaidTemp.DeptSubmittedBy != null
                             && paperLaidTemp.DeptSubmittedDate != null
                             && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                             && msessiondate.SessionDate < System.DateTime.Now
                         select new PaperSendModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                             Subject = paperLaid.Title,
                             PaperLaidId = questions.PaperLaidId,
                             Version = paperLaidTemp.Version,
                             DeptActivePaperId = paperLaid.DeptActivePaperId,
                             SessionDateId = questions.SessionDateId,
                             DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                             PaperSent = paperLaidTemp.SignedFilePath,
                             FileName = paperLaidTemp.FileName,
                             DiaryNumber = questions.DiaryNumber,
                             DeActivateFlag = questions.DeActivateFlag,
                             IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                             IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                             ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                             IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                             Status = (int)QuestionDashboardStatus.RepliesSent,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionSendModel = results;

            return model;
        }

        static tPaperLaidV GetPendingToLayQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();
            var query = (from questions in pCtxt.tQuestions
                         join paperLaidC in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaidC.PaperLaidId
                         join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId

                             //New
                             && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId

                             && questions.PaperLaidId != null && paperLaidC.LaidDate == null && paperLaidC.DesireLayingDate > DateTime.Now
                             && paperLaidC.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidC.LOBRecordId == null
                             && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
                             && paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
                         select new PaperSendModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                             Subject = paperLaidC.Title,
                             PaperLaidId = questions.PaperLaidId,
                             Version = paperLaidTemp.Version,
                             DeptActivePaperId = paperLaidC.DeptActivePaperId,
                             DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                             //Fetch the signed file by minister.
                             PaperSent = paperLaidTemp.SignedFilePath,
                             FileName = paperLaidTemp.FileName,
                             DiaryNumber = questions.DiaryNumber,
                             DeActivateFlag = questions.DeActivateFlag,
                             IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                             IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                             ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                             IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionSendModel = results;

            return model;
        }

        static object GetCountForQuestionTypes(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            using (var ctx = new PaperLaidContext())
            {
                var draftcount = (from a in ctx.tPaperLaidV

                                  where a.AssemblyId == model.AssemblyCode
                                      && a.SessionId == model.SessionCode && (a.DeptActivePaperId == null || a.DeptActivePaperId == 0) &&
                                      (a.EventId == 3 || a.EventId == 6)
                                  select a).Count();
                model.TotalBillDraftCount = draftcount;
            }

            using (var ctx = new PaperLaidContext())
            {
                var sentcount = (from a in ctx.tPaperLaidV
                                 where a.AssemblyId == model.AssemblyCode
                                     && a.SessionId == model.SessionCode && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0) &&
                                     (a.EventId == 3 || a.EventId == 6)
                                 select a).Count();
                model.TotalBillSentCount = sentcount;
            }

            model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;
            model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            model.TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalStaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
            model.TotalStaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
            return model;
        }

        static object GetDepartmentProgressCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            decimal Progress = 0;
            List<tPaperLaidV> lstObj = new List<tPaperLaidV>();

            //Starred Questions

            tPaperLaidV obj = new tPaperLaidV();
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount + GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            obj.ProgressText = "Starred Questions";
            if (model.TotalStaredReceived > 0)
            {
                Progress = ((decimal)model.TotalStaredSubmitted / (decimal)model.TotalStaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }

            lstObj.Add(obj);

            //Unstarred Questions
            obj = new tPaperLaidV();
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount + GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            obj.ProgressText = "Unstarred Questions";
            if (model.TotalStaredReceived > 0)
            {
                Progress = ((decimal)model.TotalStaredSubmitted / (decimal)model.TotalStaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }
            lstObj.Add(obj);


            //Notices
            obj = new tPaperLaidV();
            model.TotalUnstaredReceived = GetCountNoticeInbox(model) + GetCountNoticeSent(model) + GetCountNoticePending(model);
            model.TotalUnstaredSubmitted = GetCountNoticeSent(model);
            obj.ProgressText = "Notices";
            if (model.TotalUnstaredReceived > 0)
            {
                Progress = ((decimal)model.TotalUnstaredSubmitted / (decimal)model.TotalUnstaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }
            lstObj.Add(obj);


            //Bills
            obj = new tPaperLaidV();
            model.TotalUnstaredReceived = GetCountBillsPending(model) + GetCountBillsSent(model);
            model.TotalUnstaredSubmitted = GetCountBillsSent(model);
            obj.ProgressText = "Bills";
            if (model.TotalUnstaredReceived > 0)
            {
                Progress = ((decimal)model.TotalUnstaredSubmitted / (decimal)model.TotalUnstaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }
            lstObj.Add(obj);

            //Others Paper Type
            obj = new tPaperLaidV();
            model.TotalUnstaredReceived = GetCountOthersPending(model) + GetCountOthersSent(model);
            model.TotalUnstaredSubmitted = GetCountBillsSent(model);
            obj.ProgressText = "Others Papers";
            if (model.TotalUnstaredReceived > 0)
            {
                Progress = ((decimal)model.TotalUnstaredSubmitted / (decimal)model.TotalUnstaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }
            lstObj.Add(obj);


            model.DepartmentProgressList = lstObj;
            return model;
        }

        //Notices
        private static int GetCountNoticeInbox(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            using (var ctx = new BillPaperLaidContext())
            {
                var Inboxcount = (from a in ctx.tMemberNotice
                                  where a.AssemblyID == model.AssemblyCode
                                      && a.SessionID == model.SessionCode
                                      && a.PaperLaidId == null
                                      && a.DepartmentId != null
                                      && a.MinistryId != null
                                      && ids.Contains(a.DepartmentId)
                                  select a).Count();
                return Inboxcount;
            }

        }
        private static int GetCountNoticeSent(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            using (var ctx = new BillPaperLaidContext())
            {
                var noticesentReplycount = (from a in ctx.tPaperLaidV
                                            join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                            join c in ctx.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                            where a.AssemblyId == model.AssemblyCode
                                                && a.SessionId == model.SessionCode
                                                && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                && b.DeptSubmittedBy != null
                                                && b.DeptSubmittedDate != null
                                                && a.DeptActivePaperId == b.PaperLaidTempId
                                                 && ids.Contains(a.DepartmentId)
                                            select b).Count();

                return noticesentReplycount;
            }

        }
        private static int GetCountNoticePending(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            using (var ctx = new BillPaperLaidContext())
            {
                var noticependingReplycount = (from a in ctx.tPaperLaidV
                                               join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                               join c in ctx.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                               where a.AssemblyId == model.AssemblyCode
                                                   && a.SessionId == model.SessionCode
                                                   && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                   && b.DeptSubmittedBy == null
                                                   && b.DeptSubmittedDate == null
                                                   && a.DeptActivePaperId == b.PaperLaidTempId
                                                   && ids.Contains(a.DepartmentId)
                                               select b).Count();

                return noticependingReplycount;
            }

        }

        //Bills
        private static int GetCountBillsSent(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            using (var ctx = new BillPaperLaidContext())
            {
                var noticesentReplycount = (from a in ctx.tPaperLaidV
                                            join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                            join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                            where a.AssemblyId == model.AssemblyCode
                                                && a.SessionId == model.SessionCode
                                                && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                && b.DeptSubmittedBy != null
                                                && b.DeptSubmittedDate != null
                                                && a.DeptActivePaperId == b.PaperLaidTempId
                                                && ids.Contains(a.DepartmentId)
                                            select b).Count();

                return noticesentReplycount;
            }

        }
        private static int GetCountBillsPending(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            using (var ctx = new BillPaperLaidContext())
            {
                var noticependingReplycount = (from a in ctx.tPaperLaidV
                                               join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                               join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                               where a.AssemblyId == model.AssemblyCode
                                                   && a.SessionId == model.SessionCode
                                                   && (a.DeptActivePaperId == null)
                                                   && b.DeptSubmittedBy == null
                                                   && b.DeptSubmittedDate == null
                                                   && ids.Contains(a.DepartmentId)
                                               select b).Count();

                return noticependingReplycount;
            }

        }

        //Others Papers
        private static int GetCountOthersSent(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            int paperCategoryId = 5;
            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            using (var ctx = new BillPaperLaidContext())
            {
                var noticesentReplycount = (from a in ctx.tPaperLaidV
                                            join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                            join c in ctx.mEvent on a.EventId equals c.EventId
                                            where a.AssemblyId == model.AssemblyCode
                                                && a.SessionId == model.SessionCode
                                                && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                && b.DeptSubmittedBy != null
                                                && b.DeptSubmittedDate != null
                                                && a.DeptActivePaperId == b.PaperLaidTempId
                                                && ids.Contains(a.DepartmentId)
                                                && c.PaperCategoryTypeId == paperCategoryId
                                            select b).Count();

                return noticesentReplycount;
            }

        }
        private static int GetCountOthersPending(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            int paperCategoryId = 5;
            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            using (var ctx = new BillPaperLaidContext())
            {
                var noticependingReplycount = (from a in ctx.tPaperLaidV
                                               join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                               join c in ctx.mEvent on a.EventId equals c.EventId
                                               where a.AssemblyId == model.AssemblyCode
                                                   && a.SessionId == model.SessionCode
                                                   && (a.DeptActivePaperId == null)
                                                   && b.DeptSubmittedBy == null
                                                   && b.DeptSubmittedDate == null
                                                   && ids.Contains(a.DepartmentId)
                                                   && c.PaperCategoryTypeId == paperCategoryId
                                               select b).Count();

                return noticependingReplycount;
            }

        }

        static object GetDepartmentDashboardItemsCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            //Starred Questions Count
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.TotalStaredPendingForSubmission = GetPendingQuestionsByType(model).ResultCount;
            // model.TotalStaredQuestions = model.TotalStaredPendingForSubmission + GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;


            //UnStarred Questions Count
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.TotalUnstaredPendingForSubmission = GetPendingQuestionsByType(model).ResultCount;
            // model.TotalUnstaredQuestions = model.TotalUnstaredPendingForSubmission + GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;


            //Notices
            //  model.NoticeTotalCount = GetCountNoticeInbox(model) + GetCountNoticeSent(model) + GetCountNoticePending(model);
            model.NoticeInboxCount = GetCountNoticeInbox(model);

            //Bills
            // model.TotalDeptBillsCount = GetCountBillsPending(model) + GetCountBillsSent(model);

            //Others Papers
            // model.TotalOtherPapersCount = GetCountOthersPending(model) + GetCountOthersSent(model);

            return model;
        }

        static object GetDepartmentMinisterDashboardItemsCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            //Starred Questions Count
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalStaredQuestions = GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;


            //UnStarred Questions Count
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.TotalUnstaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalUnstaredQuestions = GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;


            //Notices
            model.NoticeTotalCount = GetCountNoticeSent(model) + GetCountNoticePending(model);
            model.NoticeInboxCount = GetCountNoticePending(model);

            //Bills
            model.TotalDeptBillsCount = GetCountBillsPending(model) + GetCountBillsSent(model);

            //Others Papers
            model.TotalOtherPapersCount = GetCountOthersPending(model) + GetCountOthersSent(model);

            return model;
        }

        //static object GetStarredQuestionCounter(object param)
        //{
        //    tPaperLaidV model = param as tPaperLaidV;
        //    model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;
        //    model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
        //    model.TotalStarredUpcomingLOB = UpcomingLOBByQuestionType(model).ResultCount;
        //    model.TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
        //    model.TotalStaredPendingForSubmissionNew = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
        //    model.TotalStaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
        //    model.TotalStaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
        //    model.TotalStaredQuestions = model.TotalStaredReceived + model.TotalStaredPendingForSubmissionNew;
        //    //model.TotalStaredQuestions = model.TotalStaredReceived + model.TotalStaredSubmitted + model.TotalStaredPendingForSubmission;
        //    return model;
        //}

        //static object GetUnstarredQuestionCounter(object param)
        //{
        //    tPaperLaidV model = param as tPaperLaidV;
        //    model.TotalUnstaredReceived = GetPendingQuestionsByType(model).ResultCount;
        //    model.TotalUnstaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
        //    model.TotalUnstarredUpcomingLOB = UpcomingLOBByQuestionType(model).ResultCount;
        //    model.TotalUnstaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
        //    model.TotalUnStaredPendingForSubmissionNew = SendingPendingForSubQuestionsByType(model).ResultCount;
        //    model.TotalUnstaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
        //    model.TotalUnstaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
        //    model.TotalUnstaredQuestions = model.TotalUnstaredReceived + model.TotalUnStaredPendingForSubmissionNew;
        //    //model.TotalUnstaredQuestions = model.TotalUnstaredReceived + model.TotalUnstaredSubmitted + model.TotalUnstaredPendingForSubmission; ;
        //    return model;
        //}

        static object GetStarredQuestionCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            //model.ResultCount = GetPendingQuestionsByType(model).ResultCount;
            //model.ResultCount = GetSubmittedQuestionsByType(model).ResultCount;
            //model.ResultCount = UpcomingLOBByQuestionType(model).ResultCount;
            //model.ResultCount = GetPendingForSubQuestionsByType(model).ResultCount;
            ////model.ResultCount = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
            //model.ResultCount = GetLaidInHouseQuestionsByType(model).ResultCount;
            //model.ResultCount = GetPendingToLayQuestionsByType(model).ResultCount;
            model.TotalStaredQuestions = model.TotalStaredReceived + model.TotalStaredPendingForSubmissionNew;
            model.TotalStaredPendingForSubmissionNew = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
            //model.TotalStaredQuestions = model.TotalStaredReceived + model.TotalStaredSubmitted + model.TotalStaredPendingForSubmission;
            return model;
        }

        static object GetUnstarredQuestionCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            //model.ResultCount = GetPendingQuestionsByType(model).ResultCount;
            //model.ResultCount = GetSubmittedQuestionsByType(model).ResultCount;
            //model.ResultCount = UpcomingLOBByQuestionType(model).ResultCount;
            //model.ResultCount = GetPendingForSubQuestionsByType(model).ResultCount;
            //model.ResultCount = SendingPendingForSubQuestionsByType(model).ResultCount;
            ////model.ResultCount = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
            //model.ResultCount = GetLaidInHouseQuestionsByType(model).ResultCount;
            //model.ResultCount = GetPendingToLayQuestionsByType(model).ResultCount;
            //model.ResultCount = SendingPendingForSubQuestionsByType(model).ResultCount;
            model.TotalUnStaredPendingForSubmissionNew = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
            model.TotalUnstaredQuestions = model.TotalUnstaredReceived + model.TotalUnStaredPendingForSubmissionNew;
            //model.TotalUnstaredQuestions = model.TotalUnstaredReceived + model.TotalUnstaredSubmitted + model.TotalUnstaredPendingForSubmission; ;
            return model;
        }


        static tPaperLaidV SendingPendingForSubQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidContext pCtxt = new PaperLaidContext();
            var query = DraftRepliesQuestionsListForSend(param) as List<PaperDraftModelCustom>;
            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionDraftModel = results;
            //model.TotalStaredPendingForSubmissionNew = totalRecords;

            return model;
        }

        static tPaperLaidV SendingPendingForNoticeByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidContext pCtxt = new PaperLaidContext();
            var query = DraftRepliesNoticeListForSend(param) as List<NoticePaperSendModelCustom>;
            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tNoticeSendModel = results;

            return model;
        }
        //Added Code venkat for Dynamic Menu
        public static object DraftRepliesQuestionsListForSend(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            var siteSettingData = (from a in pCtxt.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;


            //Added Code venkat for Dynamic Menu
            string DiaryNumber = "";
            string tiTle = "";
            int? Qnum = 0;
            int? MemberId = 0;
            if (model.DiaryNumber != "null" && model.DiaryNumber != null)
            {
                DiaryNumber = model.DiaryNumber;
            }
            if (model.Title != "null" && model.Title != null)
            {
                tiTle = model.Title;
            }
            if (model.QuestionNumber != 0 && model.QuestionNumber != null)
            {
                Qnum = model.QuestionNumber;
            }
            //if (model.MemberId != 0 && model.MemberId != null)
            //{
            //    MemberId = model.MemberId;
            //}
            //end--------------------------------------
            //Get Pending for reply questions.
            var DraftReplies = (from questions in pCtxt.tQuestions
                                join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                //join m in pCtxt.tMemberNotice on questions.MemberID equals m.MemberId
                                where questions.QuestionType == model.QuestionTypeId
                                && questions.AssemblyID == model.AssemblyId
                                && questions.SessionID == model.SessionId
                                && ids.Contains(questions.DepartmentID)
                                && questions.PaperLaidId != null
                                && paperLaidTemp.FileName != null
                                && paperLaidTemp.DeptSubmittedBy == null
                                && paperLaidTemp.DeptSubmittedDate == null
                                && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                && questions.IsFinalApproved == true
                                 //Added Code venkat for Dynamic Menu
                                 && (DiaryNumber == "" || questions.DiaryNumber == DiaryNumber)
                                   && (tiTle == "" || questions.Subject == tiTle)
                                    && (Qnum == 0 || questions.QuestionNumber == Qnum)
                                //&& paperLaidTemp.Version!=null
                                // && (MemberId == 0 || questions.MemberID == MemberId)
                                orderby paperLaidTemp.DeptSubmittedDate ascending
                                //end--------------------------------
                                select new PaperDraftModelCustom
                                {
                                    QuestionID = questions.QuestionID,
                                    ConvertQuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                    Subject = paperLaid.Title,
                                    PaperLaidId = questions.PaperLaidId,
                                    Version = paperLaidTemp.Version,
                                    DeptActivePaperId = paperLaid.DeptActivePaperId,
                                    //PaperSent = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                    PaperSent = FileStructurePath + paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                    DiaryNumber = questions.DiaryNumber,
                                    SubmittedDate = questions.SubmittedDate,
                                    Status = (int)QuestionDashboardStatus.DraftReplies,
                                    DeActivateFlag = questions.DeActivateFlag,
                                    evidhanReferenceNumber = "",
                                    IsPostpone = questions.IsPostpone,
                                    IsClubbed = questions.IsClubbed,
                                    DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                                    IsPostponeDate = questions.IsPostponeDate,
                                    MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                }).ToList();

            foreach (var item in DraftReplies)
            {
                item.QuestionNumber = item.ConvertQuestionNumber;

                var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;


                item.evidhanReferenceNumber = model.AssemblyId + "/" + model.SessionId + "/" + item.PaperLaidId + "/" + "V" + item.Version;

                var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;

                item.SubmittedDateString = Convert.ToDateTime(item.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");

            }

            return DraftReplies;
        }


        //Sujeet
        //Added Code venkat for Dynamic Menu
        public static object DraftRepliesNoticeListForSend(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            var siteSettingData = (from a in pCtxt.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;

            //Added Code venkat for Dynamic Menu
            string NoticeNumber = "";
            string tiTle = "";
            //int? MemberId = 0;
            if (model.NoticeNumber != null && model.NoticeNumber != "" && model.NoticeNumber != "null")
            {
                NoticeNumber = model.NoticeNumber;
            }
            if (model.Title != null && model.Title != "" && model.Title != "null")
            {
                tiTle = model.Title;
            }
            //if (model.MemberId != 0 && model.MemberId != null)
            //{
            //    MemberId = model.MemberId;
            //}
            //end----------------------------
            //Get Pending for reply questions.
            var DraftReplies = (from notice in pCtxt.tMemberNotice
                                join paperLaidTemp in pCtxt.tPaperLaidTemp on notice.PaperLaidId equals paperLaidTemp.PaperLaidId
                                join paperLaid in pCtxt.tPaperLaidV on notice.PaperLaidId equals paperLaid.PaperLaidId
                                join E in pCtxt.mEvent on notice.NoticeTypeID equals E.EventId
                                where notice.AssemblyID == model.AssemblyId
                                && notice.SessionID == model.SessionId
                                && ids.Contains(notice.DepartmentId)
                                && notice.PaperLaidId != null
                                && paperLaidTemp.DeptSubmittedBy == null
                                && paperLaidTemp.DeptSubmittedDate == null
                                && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                  //Added Code venkat for Dynamic Menu
                                  && (NoticeNumber == "" || notice.NoticeNumber == NoticeNumber)
                                    && (tiTle == "" || paperLaid.Title == tiTle)
                                // && (MemberId == 0 || notice.MemberId == MemberId)
                                //end---------------------------
                                select new NoticePaperSendModelCustom
                                {
                                    QuestionID = notice.NoticeId,
                                    QuestionNumber = notice.NoticeNumber,
                                    Subject = notice.Subject,
                                    PaperLaidId = notice.PaperLaidId,
                                    Version = paperLaidTemp.Version,
                                    DeptActivePaperId = paperLaid.DeptActivePaperId,
                                    PaperSent = FileStructurePath + paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                    NoticeId = notice.NoticeId,
                                    evidhanReferenceNumber = paperLaidTemp.evidhanReferenceNumber,
                                    Status = (int)QuestionDashboardStatus.DraftReplies,
                                    RuleNo = E.RuleNo,
                                    MemberName = (from mc in pCtxt.mMember where mc.MemberCode == notice.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    DesireLayingDate = paperLaid.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == paperLaid.DesireLayingDateId select m.SessionDate).FirstOrDefault()
                                }).ToList();

            return DraftReplies;
        }

        static object ShowPaperLaidDetailByIDSend(object param)
        {
            PaperLaidContext db = new PaperLaidContext();
            PaperMovementModel model = param as PaperMovementModel;

            var query = (from tPaper in db.tPaperLaidV
                         where model.PaperLaidId == tPaper.PaperLaidId
                         join questions in db.tQuestions on tPaper.PaperLaidId equals questions.PaperLaidId
                         join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                         join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                         join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                         join tPaperTemp in db.tPaperLaidTemp on tPaper.DeptActivePaperId equals tPaperTemp.PaperLaidTempId
                         join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                         where tPaper.DeptActivePaperId == tPaperTemp.PaperLaidTempId
                         select new PaperMovementModel
                         {
                             EventName = mEvents.EventName,
                             MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
                             DeparmentName = mDept.deptname,
                             PaperLaidId = tPaper.PaperLaidId,
                             actualFilePath = tPaperTemp.FileName,
                             ReplyPathFileLocation = tPaperTemp.FilePath + tPaperTemp.FileName,
                             ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                             Title = tPaper.Title,
                             Description = tPaper.Description,
                             Remark = tPaper.Remark,
                             PaperCategoryTypeId = mEvents.PaperCategoryTypeId,
                             PaperCategoryTypeName = mPaperCategory.Name,
                             PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
                             DocFileVersion = tPaperTemp.DocFileVersion,
                             LaidDate = tPaper.LaidDate,
                             version = tPaperTemp.Version,
                             IsClubbed = tPaper.IsClubbed,
                             MergeDiaryNo = tPaper.MergeDiaryNo,
                             QuestionTypeId = questions.QuestionType,
                             DiaryNumber = questions.DiaryNumber,
                             MemberName = (from member in db.mMember
                                           where member.MemberCode == questions.MemberID
                                           select member.Name).FirstOrDefault(),
                             DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in db.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null

                         }).FirstOrDefault();
            var categoryName = query.PaperCategoryTypeName;
            if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                int? Number = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId && mdl.IsFinalApproved == true select mdl.QuestionNumber).FirstOrDefault();

                ////New
                //var questionDet = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId select mdl).FirstOrDefault();
                //if (questionDet != null)
                //{
                //    query.MemberName = (from member in db.mMember
                //                        where member.MemberCode == questionDet.MemberID
                //                        select member.Name).FirstOrDefault();
                //}
                query.Number = Number.ToString();
            }



            return query;
        }


        public static object InsertSignPathAttachment(object param)
        {
            tPaperLaidTemp pT = param as tPaperLaidTemp;
            string[] obja = pT.FilePath.Split(',');

            foreach (var item in obja)
            {
                using (var obj1 = new PaperLaidContext())
                {
                    long id = Convert.ToInt32(item);

                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
                                        join ministryModel in obj1.tPaperLaidTemp on PaperLaidModel.PaperLaidId equals ministryModel.PaperLaidId
                                        where PaperLaidModel.PaperLaidId == id
                                        && PaperLaidModel.DeptActivePaperId == ministryModel.PaperLaidTempId
                                        select ministryModel).FirstOrDefault();

                    pT.evidhanReferenceNumber = PaperLaidObj.evidhanReferenceNumber;
                    pT.PaperLaidId = PaperLaidObj.PaperLaidId;

                    if (PaperLaidObj != null)
                    {
                        var filename = PaperLaidObj.FileName;
                        string[] Afile = filename.Split('.');
                        filename = Afile[0];
                        var path = "/PaperLaid/" + pT.AssemblyId + "/" + pT.SessionId + "/Signed/" + filename + "_signed.pdf";
                        PaperLaidObj.SignedFilePath = path;
                        PaperLaidObj.MinisterSubmittedDate = pT.MinisterSubmittedDate;
                        //var RefNumber = pT.AssemblyId + "/" + pT.SessionId + "/" + pT.PaperLaidId + "/" + "V" + PaperLaidObj.Version;
                        PaperLaidObj.evidhanReferenceNumber = pT.evidhanReferenceNumber;
                        PaperLaidObj.MinisterSubmittedBy = pT.MinisterSubmittedBy;
                        PaperLaidObj.evidhanReferenceNumber = pT.evidhanReferenceNumber;
                        PaperLaidObj.DeptSubmittedBy = pT.DeptSubmittedBy;
                        PaperLaidObj.DeptSubmittedDate = pT.DeptSubmittedDate;
                    }


                    obj1.SaveChanges();
                    obj1.Close();
                }
            }

            string meg = "";
            return meg;
        }



        static object GeteRefNum(object param)
        {
            tPaperLaidTemp model = param as tPaperLaidTemp;
            PaperLaidContext obj1 = new PaperLaidContext();
            long id = Convert.ToInt32(model.PaperLaidId);
            var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
                                join ministryModel in obj1.tPaperLaidTemp on PaperLaidModel.PaperLaidId equals ministryModel.PaperLaidId
                                where PaperLaidModel.PaperLaidId == id
                                orderby ministryModel.PaperLaidTempId descending
                                select ministryModel).FirstOrDefault();
            return PaperLaidObj;
        }

        public static object InsertSignPathAttachmenttQuestion(object param)
        {
            tQuestion pT = param as tQuestion;
            string[] obja = pT.FilePath.Split(',');

            foreach (var item in obja)
            {
                using (var obj1 = new PaperLaidContext())
                {
                    long id = Convert.ToInt32(item);

                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV

                                        join QuestionModel in obj1.tQuestions on PaperLaidModel.PaperLaidId equals QuestionModel.PaperLaidId
                                        where PaperLaidModel.PaperLaidId == id

                                        select QuestionModel).FirstOrDefault();
                    if (PaperLaidObj != null)
                    {

                        PaperLaidObj.AnswerAttachLocation = pT.AnswerAttachLocation;

                    }

                    obj1.SaveChanges();
                    obj1.Close();
                }
            }

            string meg = "";
            return meg;
        }


        //public static object InsertSignPathAttachmenttQuestion(object param)
        //{
        //    tQuestion pT = param as tQuestion;
        //    string[] obja = pT.FilePath.Split(',');

        //    foreach (var item in obja)
        //    {
        //        using (var obj1 = new PaperLaidContext())
        //        {

        //            var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV

        //                                join QuestionModel in obj1.tQuestions on PaperLaidModel.PaperLaidId equals QuestionModel.PaperLaidId
        //                                where PaperLaidModel.PaperLaidId == QuestionModel.PaperLaidId

        //                                select QuestionModel).FirstOrDefault();
        //            if (PaperLaidObj != null)
        //            {
        //                //var filename = pT.FileName;
        //                //string[] Afile = filename.Split('.');
        //                //filename = Afile[0];
        //                //var path = "/PaperLaid/" + pT.AssemblyID + "/" + pT.SessionID + "/Signed/" + filename + "_signed.pdf";
        //                //PaperLaidObj.AnswerAttachLocation = path;

        //                PaperLaidObj.AnswerAttachLocation = pT.AnswerAttachLocation;

        //            }

        //            obj1.SaveChanges();
        //            obj1.Close();
        //        }
        //    }

        //    string meg = "";
        //    return meg;
        //}


        public static object UpdateBillsDepartmentMinisterActivePaperId(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');

            foreach (var item in obja)
            {

                using (var obj1 = new PaperLaidContext())
                {
                    long id = Convert.ToInt32(item);

                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
                                        where PaperLaidModel.PaperLaidId == id
                                        select PaperLaidModel).FirstOrDefault();

                    var PaperLaidTempObj = (from PaperLaidModel in obj1.tPaperLaidV
                                            join ministryModel in obj1.tPaperLaidTemp on PaperLaidModel.PaperLaidId equals ministryModel.PaperLaidId
                                            where PaperLaidModel.PaperLaidId == id
                                                  && PaperLaidModel.DeptActivePaperId == ministryModel.PaperLaidTempId
                                            select ministryModel).FirstOrDefault();

                    if (PaperLaidObj != null)
                    {
                        PaperLaidObj.DeptActivePaperId = PaperLaidTempObj.PaperLaidTempId;
                        PaperLaidObj.MinisterActivePaperId = PaperLaidTempObj.PaperLaidTempId;


                        if (PaperLaidObj.LOBPaperTempId != null && PaperLaidObj.LOBPaperTempId != 0 && PaperLaidObj.LOBRecordId != null && PaperLaidObj.LOBRecordId != 0)
                        {
                            PaperLaidObj.LOBPaperTempId = PaperLaidTempObj.PaperLaidTempId;
                        }
                    }
                    //obj.tBillRegisterVs.Add(model);
                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return meg;
        }

        public static object UpdateBillsRegister(object param)
        {

            tBillRegister pT = param as tBillRegister;
            string[] obja = pT.FilePath.Split(',');
            //tBillRegister submitPaperTemp = param as tBillRegister;

            ////string obj = submitPaperTemp.Ids;
            //string obj = param as string;
            //string[] obja = obj.Split(',');

            //foreach (var item in obja)
            //{
            //    using (var obj1 = new PaperLaidContext())
            //    {
            //        long id = Convert.ToInt16(item);

            //        var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
            //                            join ministryModel in obj1.tPaperLaidTemp on PaperLaidModel.PaperLaidId equals ministryModel.PaperLaidId
            //                            where PaperLaidModel.PaperLaidId == id
            //                            && PaperLaidModel.DeptActivePaperId == ministryModel.PaperLaidTempId
            //                            select ministryModel).FirstOrDefault();
            //        if (PaperLaidObj != null)
            //        {
            //            var filename = PaperLaidObj.FileName;
            //            string[] Afile = filename.Split('.');
            //            filename = Afile[0];
            //            var path = "/PaperLaid/" + pT.AssemblyId + "/" + pT.SessionId + "/Signed/" + filename + "_signed.pdf";
            //            PaperLaidObj.SignedFilePath = path;
            //            PaperLaidObj.MinisterSubmittedDate = pT.MinisterSubmittedDate;
            //            PaperLaidObj.MinisterSubmittedBy = pT.MinisterSubmittedBy;

            //            PaperLaidObj.DeptSubmittedBy = pT.DeptSubmittedBy;
            //            PaperLaidObj.DeptSubmittedDate = pT.MinisterSubmittedDate;
            //        }

            //        obj1.SaveChanges();
            //        obj1.Close();
            //    }
            //}


            foreach (var item in obja)
            {

                using (var obj1 = new PaperLaidContext())
                {
                    long id = Convert.ToInt32(item);

                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidTemp
                                        where PaperLaidModel.PaperLaidId == id
                                        select PaperLaidModel).FirstOrDefault();

                    var PaperLaidTempObj = (from PaperLaidModel in obj1.tPaperLaidV
                                            join ministryModel in obj1.tBillRegister on PaperLaidModel.PaperLaidId equals ministryModel.PaperLaidId
                                            where PaperLaidModel.PaperLaidId == id

                                            select ministryModel).FirstOrDefault();

                    if (PaperLaidTempObj != null)
                    {

                        PaperLaidTempObj.DeptSubmittedDate = DateTime.Now;
                        PaperLaidTempObj.SignedFilePath = PaperLaidObj.SignedFilePath;
                    }
                    //obj.tBillRegisterVs.Add(model);
                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return meg;
        }

        //  static object UpdateBillsRegister(object param)
        //{

        //    if (null == param)
        //    {
        //        return null;
        //    }
        //    PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
        //    tBillRegister submitPaperTemp = param as tBillRegister;

        //    var tPaperLaid = (from mdl in paperLaidCntxtDB.tBillRegister where mdl.PaperLaidId == submitPaperTemp.PaperLaidId select mdl).SingleOrDefault();
        //    if (tPaperLaid != null)
        //    {
        //        tPaperLaid.DeptSubmittedDate = DateTime.Now;
        //        tPaperLaid.SignedFilePath = submitPaperTemp.SignedFilePath;
        //        paperLaidCntxtDB.SaveChanges();
        //        paperLaidCntxtDB.Close();

        //    }
        //    return tPaperLaid;

        //}





        public static object UpdateDepartmentMinisterActivePaperId(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');

            foreach (var item in obja)
            {

                using (var obj1 = new PaperLaidContext())
                {
                    long id = Convert.ToInt32(item);

                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
                                        where PaperLaidModel.PaperLaidId == id
                                        select PaperLaidModel).FirstOrDefault();

                    var PaperLaidTempObj = (from PaperLaidModel in obj1.tPaperLaidV
                                            join ministryModel in obj1.tPaperLaidTemp on PaperLaidModel.PaperLaidId equals ministryModel.PaperLaidId
                                            where PaperLaidModel.PaperLaidId == id
                                                  && PaperLaidModel.DeptActivePaperId == ministryModel.PaperLaidTempId
                                            select ministryModel).FirstOrDefault();

                    if (PaperLaidObj != null)
                    {
                        PaperLaidObj.DeptActivePaperId = PaperLaidTempObj.PaperLaidTempId;
                        PaperLaidObj.MinisterActivePaperId = PaperLaidTempObj.PaperLaidTempId;
                        PaperLaidTempObj.evidhanReferenceNumber = PaperLaidTempObj.evidhanReferenceNumber;

                        if (PaperLaidObj.LOBPaperTempId != null && PaperLaidObj.LOBPaperTempId != 0 && PaperLaidObj.LOBRecordId != null && PaperLaidObj.LOBRecordId != 0)
                        {
                            PaperLaidObj.LOBPaperTempId = PaperLaidTempObj.PaperLaidTempId;
                        }
                    }
                    //obj.tBillRegisterVs.Add(model);
                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return meg;
        }




        static object ShowPapertoSendLaidDetailByID(object param)
        {
            PaperLaidContext db = new PaperLaidContext();
            PaperMovementModel model = param as PaperMovementModel;

            var query = (from tPaper in db.tPaperLaidV
                         where model.PaperLaidId == tPaper.PaperLaidId
                         join questions in db.tQuestions on tPaper.PaperLaidId equals questions.PaperLaidId
                         join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                         join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                         join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                         join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
                         join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                         where tPaper.DeptActivePaperId == tPaperTemp.PaperLaidTempId
                         select new PaperMovementModel
                         {
                             EventName = mEvents.EventName,
                             MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
                             DeparmentName = mDept.deptname,
                             PaperLaidId = tPaper.PaperLaidId,
                             actualFilePath = tPaperTemp.FileName,
                             ReplyPathFileLocation = tPaperTemp.FilePath + tPaperTemp.FileName,
                             ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                             Title = tPaper.Title,
                             Description = tPaper.Description,
                             Remark = tPaper.Remark,
                             PaperCategoryTypeId = mEvents.PaperCategoryTypeId,
                             PaperCategoryTypeName = mPaperCategory.Name,
                             PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
                             LaidDate = tPaper.LaidDate,
                             version = tPaperTemp.Version,
                             QuestionTypeId = questions.QuestionType,
                             Status = (int)QuestionDashboardStatus.DraftReplies,
                             MemberName = (from member in db.mMember
                                           where member.MemberCode == questions.MemberID
                                           select member.Name).FirstOrDefault(),
                             DesireLayingDate = (from sessionDates in db.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
                         }).FirstOrDefault();
            var categoryName = query.PaperCategoryTypeName;
            if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                int? Number = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId select mdl.QuestionNumber).FirstOrDefault();


                query.Number = Number.ToString();
            }
            return query;
        }


        static List<PaperMovementModel> GetMemberNameByIDs(object param)
        {

            PaperLaidContext db = new PaperLaidContext();
            PaperMovementModel model = param as PaperMovementModel;

            var query = (from tPaper in db.tPaperLaidV
                         where model.PaperLaidId == tPaper.PaperLaidId
                         join questions in db.tQuestions on tPaper.PaperLaidId equals questions.PaperLaidId
                         join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                         join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                         join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                         join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
                         join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                         where tPaper.DeptActivePaperId == tPaperTemp.PaperLaidTempId
                         select new PaperMovementModel
                         {
                             EventName = mEvents.EventName,
                             MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
                             DeparmentName = mDept.deptname,
                             PaperLaidId = tPaper.PaperLaidId,
                             actualFilePath = tPaperTemp.FileName,
                             ReplyPathFileLocation = tPaperTemp.SignedFilePath,
                             ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                             Title = tPaper.Title,
                             Description = tPaper.Description,
                             PaperLaidTempId = tPaperTemp.PaperLaidTempId,
                             Remark = tPaper.Remark,
                             PaperCategoryTypeId = mPaperCategory.PaperCategoryTypeId,
                             PaperCategoryTypeName = mPaperCategory.Name,
                             PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
                             LaidDate = tPaper.LaidDate,
                             version = tPaperTemp.Version,
                             SupFileVersion = tPaperTemp.SupVersion,
                             QuestionTypeId = questions.QuestionType,
                             AssemblyId = tPaper.AssemblyId,
                             SessionId = tPaper.SessionId,
                             Status = (int)QuestionDashboardStatus.RepliesSent,
                             DepartmentId = tPaper.DepartmentId,
                             DiaryNumber = questions.DiaryNumber,
                             //QuestionNumber = questions.QuestionNumber,
                             QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                             ReferenceMemberCode = questions.ReferenceMemberCode,
                             IsClubbed = questions.IsClubbed,
                             MergeDiaryNo = questions.MergeDiaryNo,
                             MergeQuestion = questions.ReferencedNumber,
                             DocFileName = tPaperTemp.DocFileName,
                             SupDocFileName = tPaperTemp.SupDocFileName,
                             SupFileName = tPaperTemp.SupFileName,
                             SupFilePath = tPaperTemp.FilePath + tPaperTemp.SupFileName,
                             MemberName = (from member in db.mMember
                                           where member.MemberCode == questions.MemberID
                                           select member.Name).Distinct().FirstOrDefault(),

                             DesireLayingDate = questions.SessionDateId == null ? (DateTime?)null : (from sessionDates in db.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
                         }).Distinct().ToList();

            List<tAuditTrial> obJA = new List<tAuditTrial>();

            foreach (var val in query)
            {
                string s = val.ReferenceMemberCode;
                var AuditTrialList = (from a in db.tAuditTrial
                                      where a.paperLaidID == val.PaperLaidId
                                      select a).ToList();

                if (AuditTrialList != null)
                {
                    foreach (var item in AuditTrialList)
                    {
                        tAuditTrial obj = new tAuditTrial();
                        obj.documentReferenceNumber = item.documentReferenceNumber;
                        obj.DateTime = item.DateTime;
                        obJA.Add(obj);
                    }

                    val.AuditTrialList = obJA;
                }




                if (s != null && s != "")
                {

                    string[] values = s.Split(',');

                    var df = (from n in values select n).Distinct();

                    foreach (var item in df)
                    {

                        int MemId = int.Parse(item);

                        var item1 = (from questions in db.tQuestions
                                     select new QuestionModelCustom
                                     {
                                         MemberName = (from mc in db.mMember where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).Distinct().FirstOrDefault(),

                                     }).Distinct().FirstOrDefault();

                        if (string.IsNullOrEmpty(val.MemberName))
                        {
                            val.MemberName = item1.MemberName;
                        }
                        else
                        {
                            //val.MemberName =  "," + item1.MemberName.Replace(val.MemberName, "");

                            val.MemberName = val.MemberName.Replace("Shri", "") + "," + item1.MemberName.Replace(val.MemberName, "");

                        }
                    }


                }

            }



            return query;
        }

        //static List<PaperMovementModel> GetMemberNameByIDs(object param)
        //{

        //    PaperLaidContext db = new PaperLaidContext();
        //    PaperMovementModel model = param as PaperMovementModel;

        //    var query = (from tPaper in db.tPaperLaidV
        //                 where model.PaperLaidId == tPaper.PaperLaidId
        //                 join questions in db.tQuestions on tPaper.PaperLaidId equals questions.PaperLaidId
        //                 join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
        //                 join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
        //                 join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
        //                 join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
        //                 join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
        //                 where tPaper.DeptActivePaperId == tPaperTemp.PaperLaidTempId
        //                 select new PaperMovementModel
        //                 {
        //                     EventName = mEvents.EventName,
        //                     MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
        //                     DeparmentName = mDept.deptname,
        //                     PaperLaidId = tPaper.PaperLaidId,
        //                     actualFilePath = tPaperTemp.FileName,
        //                     ReplyPathFileLocation = tPaperTemp.SignedFilePath,
        //                     ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
        //                     Title = tPaper.Title,
        //                     Description = tPaper.Description,
        //                     PaperLaidTempId = tPaperTemp.PaperLaidTempId,
        //                     Remark = tPaper.Remark,
        //                     PaperCategoryTypeId = mPaperCategory.PaperCategoryTypeId,
        //                     PaperCategoryTypeName = mPaperCategory.Name,
        //                     PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
        //                     LaidDate = tPaper.LaidDate,
        //                     version = tPaperTemp.Version,
        //                     SupFileVersion = tPaperTemp.SupVersion,
        //                     QuestionTypeId = questions.QuestionType,
        //                     AssemblyId = tPaper.AssemblyId,
        //                     SessionId = tPaper.SessionId,
        //                     Status = (int)QuestionDashboardStatus.RepliesSent,
        //                     DepartmentId = tPaper.DepartmentId,
        //                     DiaryNumber = questions.DiaryNumber,
        //                     //QuestionNumber = questions.QuestionNumber,
        //                     QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
        //                     ReferenceMemberCode = questions.ReferenceMemberCode,
        //                     IsClubbed = questions.IsClubbed,
        //                     MergeDiaryNo = questions.MergeDiaryNo,
        //                     MergeQuestion = questions.ReferencedNumber,
        //                     DocFileName = tPaperTemp.DocFileName,
        //                     SupDocFileName = tPaperTemp.SupDocFileName,
        //                     SupFileName = tPaperTemp.SupFileName,
        //                     SupFilePath = tPaperTemp.FilePath + tPaperTemp.SupFileName,
        //                     MemberName = (from member in db.mMember
        //                                   where member.MemberCode == questions.MemberID
        //                                   select member.Name).FirstOrDefault(),

        //                     DesireLayingDate = questions.SessionDateId == null ? (DateTime?)null : (from sessionDates in db.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
        //                 }).ToList();


        //    foreach (var val in query)
        //    {
        //        string s = val.ReferenceMemberCode;
        //        if (s != null && s != "")
        //        {
        //            string[] values = s.Split(',');

        //            for (int i = 0; i < values.Length; i++)
        //            {
        //                int MemId = int.Parse(values[i]);
        //                var item = (from questions in db.tQuestions
        //                            select new QuestionModelCustom
        //                            {
        //                                MemberName = (from mc in db.mMember where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

        //                            }).FirstOrDefault();

        //                if (string.IsNullOrEmpty(val.MemberName))
        //                {
        //                    val.MemberName = item.MemberName;
        //                }
        //                else
        //                {
        //                    val.MemberName = val.MemberName + "," + item.MemberName;
        //                }


        //            }
        //        }

        //    }



        //    return query;
        //}


        #endregion

        #region Added code venkat for Dynamic Menu
        //Added code venkat for Dynamic Menu
        static object PendingForReplyQuestionsList1(object param)
        {
            try
            {

                tPaperLaidV model = param as tPaperLaidV;

                PaperLaidContext pCtxt = new PaperLaidContext();
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                if (model.ModuleId == 6)
                {
                    model.QuestionTypeId = (int)QuestionType.StartedQuestion;
                    model.TotalStaredReceived1 = GetPendingQuestionsByType(model).ResultCount;//pending
                    model.TotalStaredPendingForSubmission1 = GetPendingForSubQuestionsByType(model).ResultCount;
                    return model;
                }

                else if (model.ModuleId == 7)
                {
                    model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                    model.TotalStaredReceived1 = GetPendingQuestionsByType(model).ResultCount;
                    model.TotalStaredPendingForSubmission1 = GetPendingForSubQuestionsByType(model).ResultCount;
                    return model;
                }
                else if (model.ModuleId == 8)
                {
                    var NoticesPendingForReply = (from NT in pCtxt.tMemberNotice
                                                  where NT.AssemblyID == model.AssemblyId
                                                           && NT.SessionID == model.SessionId
                                                           && NT.PaperLaidId == null
                                                           && NT.MinistryId != null
                                                           && NT.DepartmentId != null
                                                           && NT.IsSubmitted == true
                                                           && ids.Contains(NT.DepartmentId)
                                                  select NT).Count();
                    var NoticesDrafts = (from a in pCtxt.tPaperLaidV
                                         join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                         join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                         where a.AssemblyId == model.AssemblyId
                                             && a.SessionId == model.SessionId
                                             && (a.DeptActivePaperId != null)
                                             && b.DeptSubmittedBy == null
                                             && b.DeptSubmittedDate == null
                                             && a.DeptActivePaperId == b.PaperLaidTempId
                                               && ids.Contains(a.DepartmentId)
                                         select new
                                         {
                                             paperLaidId = a.PaperLaidId,
                                             MinistryId = a.MinistryId,
                                             MinistryName = a.MinistryName,
                                             DepartmentId = a.DepartmentId,
                                             DeparmentName = a.DeparmentName,
                                             FileName = b.FileName,
                                             FilePath = b.FilePath,
                                             tpaperLaidTempId = b.PaperLaidTempId,
                                             FileVersion = b.Version,
                                             Title = c.Subject,
                                             NoticeId = c.NoticeId,
                                             NoticeNumber = c.NoticeNumber,
                                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                             PaperSent = b.FilePath + b.FileName,
                                             DesireLayingDateId = a.DesireLayingDateId
                                         }).Count();
                    //NoticesDrafts +
                    var resultCount = NoticesPendingForReply;
                    model.TotalStaredReceived1 = resultCount;
                    //return NoticesPendingForReply;
                    return model;
                }


            }
            catch (Exception ex)
            {

                throw ex;
            }

            return null;

        }

        public static object DraftRepliesQuestionsList1(object param)
        {
            try
            {
                tPaperLaidV model = param as tPaperLaidV;
                PaperLaidContext pCtxt = new PaperLaidContext();
                string csv = model.DepartmentId;

                if (model.ModuleId == 6)
                {
                    model.QuestionTypeId = (int)QuestionType.StartedQuestion;
                    model.TotalStaredPendingForSubmissionNew = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
                    model.ResultCount = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
                    return model;
                }

                else if (model.ModuleId == 7)
                {
                    model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                    model.TotalUnStaredPendingForSubmissionNew = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
                    model.ResultCount = GetPendingForSubQuestionsByTypeNew(model).ResultCount;
                    //model.ResultCount = SendingPendingForSubQuestionsByType(model).ResultCount;
                    return model;
                }
                else if (model.ModuleId == 8)
                {
                    IEnumerable<string> ids = csv.Split(',').Select(str => str);
                    var noticedraftreplycount = (from a in pCtxt.tPaperLaidV
                                                 join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                                 join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                                 where a.AssemblyId == model.AssemblyId
                                                     && a.SessionId == model.SessionId
                                                     && (a.DeptActivePaperId != null)
                                                     && b.DeptSubmittedBy == null
                                                     && b.DeptSubmittedDate == null
                                                     && a.DeptActivePaperId == b.PaperLaidTempId
                                                     && ids.Contains(c.DepartmentId)
                                                 select a).Count();
                    model.ResultCount = noticedraftreplycount;
                    model.NoticeDraftCount = noticedraftreplycount;

                    return model;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return null;

            //Get Pending for reply questions.

        }

        public static object RepliesSentQuestionsList(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            var siteSettingData = (from a in pCtxt.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;

            string diarynumber = "";
            string tiTle = "";
            int? Qnum = 0;
            //int? MemberId = 0;
            if (model.DiaryNumber != "null" && model.DiaryNumber != null)
            {
                diarynumber = model.DiaryNumber;
            }
            if (model.Title != "null" && model.Title != null)
            {
                tiTle = model.Title;
            }
            if (model.QuestionNumber != 0 && model.MemberId != null)
            {
                Qnum = model.QuestionNumber;
            }


            //Get Submitted Questions.
            var RepliesSent = (from questions in pCtxt.tQuestions
                               join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                               //join m in pCtxt.tMemberNotice on questions.MemberID equals m.MemberId
                               join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                               where questions.QuestionType == model.QuestionTypeId
                                   && ids.Contains(questions.DepartmentID)
                                   && questions.AssemblyID == model.AssemblyId
                                   && questions.SessionID == model.SessionId
                                   && questions.PaperLaidId != null
                                   && paperLaidTemp.DeptSubmittedBy != null
                                   && paperLaidTemp.DeptSubmittedDate != null
                                   && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                   && (diarynumber == "" || questions.DiaryNumber == diarynumber)
                                   && (tiTle == "" || questions.Subject == tiTle)
                                    && (Qnum == 0 || questions.QuestionNumber == Qnum)

                               //  && (MemberId == 0 || questions.MemberID == MemberId)
                               select new PaperSendModelCustom
                               {
                                   QuestionID = questions.QuestionID,
                                   QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                   Subject = paperLaid.Title,
                                   PaperLaidId = questions.PaperLaidId,
                                   Version = paperLaidTemp.Version,
                                   DeptActivePaperId = paperLaid.DeptActivePaperId,
                                   SessionDateId = questions.SessionDateId,
                                   PaperLaidTempId = paperLaidTemp.PaperLaidTempId,
                                   DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                                   evidhanReferenceNumber = paperLaidTemp.evidhanReferenceNumber,
                                   //PaperSent = paperLaidTemp.SignedFilePath,
                                   PaperSent = FileStructurePath + paperLaidTemp.SignedFilePath,
                                   FileName = paperLaidTemp.FileName,
                                   DiaryNumber = questions.DiaryNumber,
                                   DeActivateFlag = questions.DeActivateFlag,
                                   IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                   IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                   ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                   IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                   IsBracket = questions.IsBracket,
                                   Status = (int)QuestionDashboardStatus.RepliesSent,
                                   MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).Distinct().FirstOrDefault(),
                                   DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                               }).ToList();

            foreach (var item in RepliesSent)
            {

                var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                //item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;

                var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");

                if (item.DesireLayingDate != null)
                {
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }
                else
                {
                    item.DesiredLayingDate = "";
                }

            }

            return RepliesSent;
        }

        public static object RepliesSentQuestionsList1(object param)
        {
            try
            {
                tPaperLaidV model = param as tPaperLaidV;
                PaperLaidContext pCtxt = new PaperLaidContext();

                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                // int QtypeId = 0;
                if (model.ModuleId == 6)
                {
                    model.QuestionTypeId = (int)QuestionType.StartedQuestion;
                    model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
                    model.ResultCount = GetSubmittedQuestionsByType(model).ResultCount;
                    return model;
                }

                else if (model.ModuleId == 7)
                {
                    model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
                    model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
                    model.ResultCount = GetSubmittedQuestionsByType(model).ResultCount;
                    return model;
                }
                else if (model.ModuleId == 8)
                {
                    var noticesentReplycount = (from a in pCtxt.tPaperLaidV
                                                join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                                join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                                where a.AssemblyId == model.AssemblyId
                                             && a.SessionId == model.SessionId
                                             && (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
                                             && b.DeptSubmittedBy != null
                                             && b.DeptSubmittedDate != null
                                             && a.DeptActivePaperId == b.PaperLaidTempId
                                             && ids.Contains(a.DepartmentId)
                                                select a).Count();
                    model.ResultCount = noticesentReplycount;
                    model.TotalStaredSubmitted = noticesentReplycount;
                    return model;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return null;

            //Get Submitted Questions.

        }

        static object GetNoticeReplySentCount(object param)
        {
            try
            {
                tPaperLaidV model = param as tPaperLaidV;
                PaperLaidContext pCtxt = new PaperLaidContext();
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                var noticesentReplycount = (from a in pCtxt.tPaperLaidV
                                            join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                            join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                            where a.AssemblyId == model.AssemblyId
                                         && a.SessionId == model.SessionId
                                         && (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
                                         && b.DeptSubmittedBy != null
                                         && b.DeptSubmittedDate != null
                                         && a.DeptActivePaperId == b.PaperLaidTempId
                                         && ids.Contains(a.DepartmentId)
                                            select a).Count();
                model.ResultCount = noticesentReplycount;
                return noticesentReplycount;
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static object GetNoticeULOBList(object param)
        {
            try
            {
                tPaperLaidV model = param as tPaperLaidV;
                PaperLaidContext pCtxt = new PaperLaidContext();
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                int paperCategoryId = 5;
                var currentdate = DateTime.Now;
                if (model.ModuleId == 8)
                {
                    var noticeULOBCount = (from a in pCtxt.tPaperLaidV
                                           join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                           join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                           where b.DeptSubmittedDate != null && b.DeptSubmittedBy != null && b.MinisterSubmittedBy != 0 && b.MinisterSubmittedDate != null && a.DesireLayingDate >= currentdate
                                           && a.LaidDate == null && a.LOBRecordId != null
                                           && ids.Contains(a.DepartmentId)
                                           select a).Count();
                    model.ResultCount = noticeULOBCount;
                    return noticeULOBCount;
                }
                else if (model.ModuleId == 23)
                {
                    //var billULOBCount = (from a in pCtxt.tPaperLaidV
                    //                     join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                    //                     join c in pCtxt.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                    //                     where b.DeptSubmittedDate != null && b.DeptSubmittedBy != null && b.MinisterSubmittedBy != 0
                    //                     && b.MinisterSubmittedDate != null && a.DesireLayingDate >= currentdate
                    //                     && a.LaidDate == null && a.LOBRecordId != null
                    //                     && ids.Contains(a.DepartmentId)
                    var billULOBCount = (from a in pCtxt.tPaperLaidV
                                         join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                         join c in pCtxt.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                         where b.DeptSubmittedDate != null
                                         && b.DeptSubmittedBy != null
                                         && b.MinisterSubmittedBy != 0
                                         && b.MinisterSubmittedDate != null
                                         && ((from m in pCtxt.mSessionDates where m.SessionId == a.DesireLayingDateId select m.SessionDate).FirstOrDefault() >= currentdate)
                                         && a.LaidDate == null
                                         && a.LOBRecordId != null
                                         && ids.Contains(a.DepartmentId)
                                         select a).Count();
                    model.ResultCount = billULOBCount;
                    return billULOBCount;
                }

                else if (model.ModuleId == 10)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID

                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId
                                 && t.DeptSubmittedBy != null
                                 && t.DeptSubmittedDate != null
                                 && t.MinisterSubmittedBy != null
                                 && t.MinisterSubmittedDate != null
                                 && m.DesireLayingDate > DateTime.Now
                                 && m.LaidDate == null
                                 && m.LOBRecordId != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId
                                 && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                 && m.AssemblyId == model.AssemblyId
                                && m.SessionId == model.SessionId
                                 select m).Count();
                    model.ResultCount = query;
                    return query;

                }
                else if (model.ModuleId == 17)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID
                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId
                                 && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
                                 && m.LaidDate == null && m.LOBRecordId != null
                                 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId
                                 && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                  && m.AssemblyId == model.AssemblyId
                                  && m.SessionId == model.SessionId
                                 select m).Count();
                    model.ResultCount = query;
                    return query;

                }
                else if (model.ModuleId == 16)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID
                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId
                                 && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
                                 && m.LaidDate == null && m.LOBRecordId != null
                                 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId
                                 && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                  && m.AssemblyId == model.AssemblyId
                                  && m.SessionId == model.SessionId
                                 select m).Count();
                    model.ResultCount = query;
                    return query;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return null;

        }

        static object GetNoticeLIHList(object param)
        {
            try
            {
                tPaperLaidV model = param as tPaperLaidV;
                var currentdate = DateTime.Now;
                int paperCategoryId = 5;
                PaperLaidContext pCtxt = new PaperLaidContext();
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                if (model.ModuleId == 8)
                {
                    var noticeLIHCount = (from a in pCtxt.tPaperLaidV
                                          join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                          join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                          where c.PaperLaidId != null
                                          && ids.Contains(a.DepartmentId)
                                          && a.LOBRecordId != null && a.DeptActivePaperId == b.PaperLaidTempId
                                          && a.MinisterActivePaperId != null && a.LaidDate != null && a.LaidDate <= currentdate && b.DeptSubmittedBy != null && b.DeptSubmittedDate != null
                                          && b.MinisterSubmittedBy != null && b.MinisterSubmittedDate != null
                                          select a).Count();
                    model.ResultCount = noticeLIHCount;
                    return noticeLIHCount;
                }
                else if (model.ModuleId == 23)
                {
                    var billLIHCount = (from a in pCtxt.tPaperLaidV
                                        join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                        join c in pCtxt.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                        where c.PaperLaidId != null
                                        && ids.Contains(a.DepartmentId)
                                        && a.LOBRecordId != null && a.DeptActivePaperId == b.PaperLaidTempId
                                        && a.MinisterActivePaperId != null && a.LaidDate != null && a.LaidDate <= currentdate && b.DeptSubmittedBy != null && b.DeptSubmittedDate != null
                                        && b.MinisterSubmittedBy != null && b.MinisterSubmittedDate != null
                                        select a).Count();
                    model.ResultCount = billLIHCount;
                    return billLIHCount;
                }
                else if (model.ModuleId == 10)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID
                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
                                 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null && m.DesireLayingDate > DateTime.Now && m.LaidDate != null && m.LOBRecordId != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                 && m.AssemblyId == model.AssemblyId
                                 && m.SessionId == model.SessionId

                                 select m).Count();
                    model.ResultCount = query;
                    return query;
                }
                else if (model.ModuleId == 17)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID
                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId
                                 && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
                                 && m.LaidDate == null && m.LOBRecordId != null
                                 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId
                                 && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                  && m.AssemblyId == model.AssemblyId
                                  && m.SessionId == model.SessionId
                                 select m).Count();
                    model.ResultCount = query;
                    return query;

                }
                else if (model.ModuleId == 16)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID
                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId
                                 && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
                                 && m.LaidDate == null && m.LOBRecordId != null
                                 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId
                                 && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                  && m.AssemblyId == model.AssemblyId
                                  && m.SessionId == model.SessionId
                                 select m).Count();
                    model.ResultCount = query;
                    return query;

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return null;


        }

        static object GetNoticePLIHList(object param)
        {
            try
            {
                tPaperLaidV model = param as tPaperLaidV;
                var currentdate = DateTime.Now;
                int paperCategoryId = 5;
                PaperLaidContext pCtxt = new PaperLaidContext();
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                if (model.ModuleId == 8)
                {
                    var query = (from notice in pCtxt.tMemberNotice
                                 join paperLaidC in pCtxt.tPaperLaidV on notice.PaperLaidId equals paperLaidC.PaperLaidId
                                 join paperLaidTemp in pCtxt.tPaperLaidTemp on notice.PaperLaidId equals paperLaidTemp.PaperLaidId
                                 where
                                    notice.PaperLaidId != null
                                   && paperLaidC.LaidDate == null
                                   && ((from m in pCtxt.mSessionDates where m.SessionId == paperLaidC.DesireLayingDateId select m.SessionDate).FirstOrDefault() > currentdate)
                                   && paperLaidC.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId
                                   && paperLaidC.LOBRecordId == null
                                   && paperLaidTemp.DeptSubmittedBy != null
                                   && paperLaidTemp.DeptSubmittedDate != null
                                   && paperLaidTemp.MinisterSubmittedBy != null
                                   && paperLaidTemp.MinisterSubmittedDate != null
                                   && ids.Contains(notice.DepartmentId)
                                 select notice).Count();

                    model.ResultCount = query;
                    return query;
                }
                else if (model.ModuleId == 23)
                {
                    var billPLIHCount = (from bill in pCtxt.tBillRegister
                                         join paperLaidC in pCtxt.tPaperLaidV on bill.PaperLaidId equals paperLaidC.PaperLaidId
                                         join paperLaidTemp in pCtxt.tPaperLaidTemp on bill.PaperLaidId equals paperLaidTemp.PaperLaidId
                                         where
                                             ids.Contains(bill.DepartmentId)
                                             && bill.PaperLaidId != null && paperLaidC.LaidDate == null && paperLaidC.DesireLayingDate > currentdate
                                             && paperLaidC.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidC.LOBRecordId == null
                                             && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
                                             && paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
                                         select paperLaidC).Count();
                    model.ResultCount = billPLIHCount;
                    return billPLIHCount;
                }
                else if (model.ModuleId == 10)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID
                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId
                                 && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
                                 && m.LaidDate == null && m.LOBRecordId != null
                                 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId
                                 && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                  && m.AssemblyId == model.AssemblyId
                                  && m.SessionId == model.SessionId
                                 select m).Count();
                    model.ResultCount = query;
                    return query;

                }
                else if (model.ModuleId == 16)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID
                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId
                                 && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
                                 && m.LaidDate == null && m.LOBRecordId != null
                                 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId
                                 && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                  && m.AssemblyId == model.AssemblyId
                                  && m.SessionId == model.SessionId
                                 select m).Count();
                    model.ResultCount = query;
                    return query;

                }
                else if (model.ModuleId == 17)
                {
                    var query = (from m in pCtxt.tPaperLaidV
                                 join c in pCtxt.mEvent on m.EventId equals c.EventId
                                 join ministery in pCtxt.mMinistry on m.MinistryId equals ministery.MinistryID
                                 join t in pCtxt.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                 where m.DeptActivePaperId == t.PaperLaidTempId
                                 && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
                                 && m.LaidDate == null && m.LOBRecordId != null
                                 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null
                                 && m.MinisterActivePaperId == t.PaperLaidTempId
                                 && c.PaperCategoryTypeId == paperCategoryId
                                 && ids.Contains(m.DepartmentId)
                                  && m.AssemblyId == model.AssemblyId
                                  && m.SessionId == model.SessionId
                                 select m).Count();
                    model.ResultCount = query;
                    return query;

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return null;
        }

        static object GetBillsSentCount(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            var currentdate = DateTime.Now;
            PaperLaidContext pCtxt = new PaperLaidContext();
            string csv = model.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            var billsentcount = (from a in pCtxt.tPaperLaidV
                                 join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                 join c in pCtxt.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                 where a.AssemblyId == model.AssemblyId
                                     && a.SessionId == model.SessionId
                                     && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                     && b.DeptSubmittedBy != null
                                     && b.DeptSubmittedDate != null
                                     && a.DeptActivePaperId == b.PaperLaidTempId
                                     && ids.Contains(a.DepartmentId)
                                 select b).Count();
            model.ResultCount = billsentcount;
            return billsentcount;
        }

        static object GetDraftBillsCount(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            var currentdate = DateTime.Now;
            PaperLaidContext pCtxt = new PaperLaidContext();
            string csv = model.DepartmentId;

            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            var billdraftcount = (from a in pCtxt.tPaperLaidV
                                  join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                  join c in pCtxt.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                  where a.AssemblyId == model.AssemblyId
                                      && a.SessionId == model.SessionId
                                    && a.DeptActivePaperId == b.PaperLaidTempId
                                      && b.DeptSubmittedBy == null
                                      && b.DeptSubmittedDate == null
                                      && b.MinisterSubmittedBy == null
                                      && b.MinisterSubmittedDate == null
                                       && ids.Contains(a.DepartmentId)


                                  select a).Count();

            //var billdraftcount = (from a in pCtxt.tPaperLaidV
            //                      join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
            //                      join c in pCtxt.tBillRegister on a.PaperLaidId equals c.PaperLaidId
            //                      where a.AssemblyId == model.AssemblyId
            //                          && a.SessionId == model.SessionId
            //                          && (a.DeptActivePaperId == null || a.DeptActivePaperId == 0)
            //                          && b.DeptSubmittedBy == null
            //                          && b.DeptSubmittedDate == null
            //                           && b.MinisterSubmittedBy == null
            //                          && b.MinisterSubmittedDate == null
            //                      && ids.Contains(a.DepartmentId)

            //                      select a).Count();
            model.ResultCount = billdraftcount;
            return billdraftcount;
        }

        static object GetDraftOthePapersCountForOtherpaperlaid(object param)
        {
            try
            {
                tPaperLaidV mod = param as tPaperLaidV;
                PaperLaidContext db = new PaperLaidContext();
                int paperCategoryId = 5;
                string csv = mod.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);

                int count = (from m in db.tPaperLaidV
                             join c in db.mEvent on m.EventId equals c.EventId
                             join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
                             join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                             where t.DeptSubmittedDate == null
                             && c.PaperCategoryTypeId == paperCategoryId
                             && t.DeptSubmittedBy == null
                             && ids.Contains(m.DepartmentId)
                             && m.AssemblyId == mod.AssemblyId
                             && m.SessionId == mod.SessionId
                             && m.DeptActivePaperId == t.PaperLaidTempId
                             select new PaperMovementModel
                             {
                             }).Count();
                mod.OtherPaperCount = count;
                return mod;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static object GetSubmittedOtherPaperLaidList(object param)
        {
            tPaperLaidV mod = param as tPaperLaidV;
            PaperLaidContext db = new PaperLaidContext();
            int paperCategoryId = 5;
            string csv = mod.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            int? ONumber = 0;
            int? Bname = 0;
            if (mod.QuestionNumber != 0 && mod.QuestionNumber != null)
            {
                ONumber = mod.QuestionNumber;
            }

            if (mod.EventId != 0 && mod.EventId != null)
            {
                Bname = mod.EventId;
            }


            int count = (from m in db.tPaperLaidV
                         join c in db.mEvent on m.EventId equals c.EventId
                         join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         where t.DeptSubmittedDate != null
                         && c.PaperCategoryTypeId == paperCategoryId
                         && t.DeptSubmittedBy != null
                         && ids.Contains(m.DepartmentId)
                         && m.AssemblyId == mod.AssemblyId
                         && m.SessionId == mod.SessionId
                        && m.DeptActivePaperId == t.PaperLaidTempId
                         && (ONumber == 0 || ONumber == m.PaperLaidId)
                            && (Bname == 0 || Bname == c.EventId)
                         select new PaperMovementModel
                         {
                         }).Count();


            //int count = (from m in db.tPaperLaidV
            //             join c in db.mEvent on m.EventId equals c.EventId
            //             join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
            //             join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
            //             where m.DeptActivePaperId == t.PaperLaidTempId
            //             && c.PaperCategoryTypeId == paperCategoryId
            //             && ids.Contains(m.DepartmentId)
            //             && m.AssemblyId == mod.AssemblyId
            //             && m.SessionId == mod.SessionId
            //             select new PaperMovementModel
            //             {
            //             }).Count();
            mod.Count = count;
            return mod;
        }

        static object GetMemberList(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext db = new PaperLaidContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            if (model.ModuleId == 6)
            {
                if (model.PaperEntryType == "Pending For Reply")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Draft Replies")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Reply Sent")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
            }
            else if (model.ModuleId == 7)
            {
                if (model.PaperEntryType == "Pending For Reply")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Draft Replies")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Reply Sent")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
            }
            else if (model.ModuleId == 8)
            {
                if (model.PaperEntryType == "Pending For Reply")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Draft Replies")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Reply Sent")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Upcoming LOB")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Laid In The House")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Pending To Lay")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
            }
            else if (model.ModuleId == 9)
            {
                if (model.PaperEntryType == "Draft Memorandum")
                {
                    //var MinMisList = (from Min in db.mMember
                    //                  join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                    //                  where minis.AssemblyID == model.AssemblyId
                    //                  orderby Min.MemberID
                    //                  select Min).ToList();
                    var MinMisList = (from mbr in db.mMember
                                      join minstry in db.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                      where minstry.AssemblyID == model.AssemblyId
                                      orderby minstry.MinistryID
                                      select mbr).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Draft Sent")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Upcoming LOB")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Laid In The House")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Pending To Lay")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }

            }
            else if (model.ModuleId == 10)
            {
                if (model.PaperEntryType == "Draft Other Papers")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Other Papers Sent")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Upcoming LOB")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Laid In The House")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }
                else if (model.PaperEntryType == "Pending To Lay")
                {
                    var MinMisList = (from Min in db.mMember
                                      join minis in db.mMemberAssembly on Min.MemberCode equals minis.MemberID
                                      where minis.AssemblyID == model.AssemblyId
                                      orderby Min.MemberID
                                      select Min).ToList();
                    return MinMisList;
                }

            }
            return null;
        }

        private static object GetEventNamesList()
        {
            PaperLaidContext db = new PaperLaidContext();
            var data = db.mEvent.ToList();
            return data.ToList();
        }
        #endregion
        //sameer 



        static object GetFilesDetials(object param)
        {
            tPaperLaidTemp model = param as tPaperLaidTemp;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            var data = (from dept in paperLaidCntxtDB.tPaperLaidTemp where dept.PaperLaidId == model.PaperLaidId select dept).ToList();


            var results = data.ToList();
            model.objList = results;

            return model;
        }


        static object GetFilesVersionDetials(object param)
        {
            tPaperLaidTemp model = param as tPaperLaidTemp;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            var data = (from dept in paperLaidCntxtDB.tPaperLaidTemp where dept.PaperLaidTempId == model.PaperLaidTempId select dept).ToList();


            var results = data.ToList();
            model.objList = results;

            return model;
        }
        static object GetTempDocFile(object param)
        {
            tOTPRegistrationAuditTrial model = param as tOTPRegistrationAuditTrial;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            var data = (from dept in paperLaidCntxtDB.tOTPRegistrationAuditTrial where dept.MobileNumber == model.MobileNumber select dept).ToList();


            var results = data.ToList();
            model.OTPRegistrationModel = results;

            return model;
        }


        public static object DraftRepliescount(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            string csv = model.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);




            var StrarredPendingCountBeforeSend = (from questions in pCtxt.tQuestions
                                                  join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                                  join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                                  where questions.QuestionType == 1
                                                  && questions.PaperLaidId != null
                                                  && paperLaidTemp.DeptSubmittedBy == null
                                                  && paperLaidTemp.FileName != null
                                                     && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                                   && paperLaidTemp.DeptSubmittedDate == null
                                                   && questions.AssemblyID == model.AssemblyId
                                                   && questions.SessionID == model.SessionId
                                                 && ids.Contains(questions.DepartmentID)
                                                  select new QuestionModelCustom
                                                  {

                                                  }).Count();
            model.TotalStaredReceivedBeforeSend = StrarredPendingCountBeforeSend;

            var UnStrarredPendingCountBeforeSend = (from questions in pCtxt.tQuestions
                                                    join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                                    join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                                    where questions.QuestionType == 2
                                                    && questions.PaperLaidId != null
                                                     //&& (questions.QuestionStatus == (int)Questionstatus.QuestionSent)
                                                     && paperLaidTemp.DeptSubmittedBy == null
                                                       && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                                     && paperLaidTemp.DeptSubmittedDate == null
                                                     && questions.AssemblyID == model.AssemblyId
                                                     && questions.SessionID == model.SessionId
                                                   && ids.Contains(questions.DepartmentID)
                                                    select new QuestionModelCustom
                                                    {

                                                    }).Count();
            model.TotalUnStaredReceivedBeforeSend = UnStrarredPendingCountBeforeSend;

            var StrarredPendingCount = (from questions in pCtxt.tQuestions

                                        where questions.QuestionType == 1
                                        && questions.PaperLaidId == null
                                        && (questions.QuestionStatus == (int)Questionstatus.QuestionSent)
                                         && questions.AssemblyID == model.AssemblyId
                                         && questions.SessionID == model.SessionId
                                       && ids.Contains(questions.DepartmentID)
                                        && questions.IsBracket == null
                                        select new QuestionModelCustom
                                        {

                                        }).Count();

            model.TotalStaredReceived = StrarredPendingCount;

            var UnStrarredPendingCount = (from questions in pCtxt.tQuestions

                                          where questions.QuestionType == 2
                                          && questions.PaperLaidId == null
                                         && (questions.QuestionStatus == (int)Questionstatus.QuestionSent)
                                          && questions.AssemblyID == model.AssemblyId
                                          && questions.SessionID == model.SessionId
                                         && ids.Contains(questions.DepartmentID)
                                          && questions.IsBracket == null
                                          select new QuestionModelCustom
                                          {

                                          }).Count();

            model.TotalUnstaredReceived = UnStrarredPendingCount;


            var StrarredCount = (from questions in pCtxt.tQuestions
                                 join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                 join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId

                                 where questions.QuestionType == 1
                                  && questions.PaperLaidId != null
                                  && paperLaidTemp.DeptSubmittedBy == null
                                  && paperLaidTemp.DeptSubmittedDate == null
                                  && questions.AssemblyID == model.AssemblyId
                                  && questions.SessionID == model.SessionId
                                  && ids.Contains(paperLaid.DepartmentId)
                                  && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                  && questions.IsFinalApproved == true
                                 //&& paperLaidTemp.Version!=null
                                 select new QuestionModelCustom
                                 {

                                 }).Count();
            model.TotalStaredPendingForSubmissionNew = StrarredCount;

            var UnStrarredCount = (from questions in pCtxt.tQuestions
                                   join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                   join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId

                                   where questions.QuestionType == 2
                                    && questions.PaperLaidId != null
                                    && paperLaidTemp.DeptSubmittedBy == null
                                    && paperLaidTemp.DeptSubmittedDate == null
                                    && questions.AssemblyID == model.AssemblyId
                                    && questions.SessionID == model.SessionId
                                    && ids.Contains(paperLaid.DepartmentId)
                                   && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                  && questions.IsFinalApproved == true
                                   select new QuestionModelCustom
                                   {

                                   }).Count();
            model.TotalUnStaredPendingForSubmissionNew = UnStrarredCount;

            var RepliesSent = (from questions in pCtxt.tQuestions
                               join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                               join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                               where questions.QuestionType == 1
                                   && questions.AssemblyID == model.AssemblyId
                                   && questions.SessionID == model.SessionId
                                   && questions.PaperLaidId != null
                                   && paperLaidTemp.DeptSubmittedBy != null
                                   && paperLaidTemp.DeptSubmittedDate != null
                                && ids.Contains(paperLaid.DepartmentId)
                                && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                               select new PaperSendModelCustom
                               {

                               }).Count();
            model.TotalStaredReplySent = RepliesSent;
            var TotalUnStaredReplySent = (from questions in pCtxt.tQuestions
                                          join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                          join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                          where questions.QuestionType == 2
                                              && questions.AssemblyID == model.AssemblyId
                                              && questions.SessionID == model.SessionId
                                              && questions.PaperLaidId != null
                                              && paperLaidTemp.DeptSubmittedBy != null
                                              && paperLaidTemp.DeptSubmittedDate != null
                                              && ids.Contains(paperLaid.DepartmentId)
                                               && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                          select new PaperSendModelCustom
                                          {

                                          }).Count();
            model.TotalUnStaredReplySent = TotalUnStaredReplySent;


            var noticedraftreplycount = (from a in pCtxt.tPaperLaidV
                                         join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                         join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                         where a.AssemblyId == model.AssemblyId
                                             && a.SessionId == model.SessionId
                                             && (a.DeptActivePaperId != null)
                                             && b.DeptSubmittedBy == null
                                             && b.DeptSubmittedDate == null
                                             && a.DeptActivePaperId == b.PaperLaidTempId
                                          && ids.Contains(a.DepartmentId)
                                         select a).Count();
            model.NoticeDraftCount = noticedraftreplycount;

            var noticesentReplycount = (from a in pCtxt.tPaperLaidV
                                        join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                        join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                        where a.AssemblyId == model.AssemblyId
                                     && a.SessionId == model.SessionId
                                     && (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
                                     && b.DeptSubmittedBy != null
                                     && b.DeptSubmittedDate != null
                                     && a.DeptActivePaperId == b.PaperLaidTempId
                                     && ids.Contains(a.DepartmentId)
                                        select a).Count();

            model.NoticeSentCount = noticesentReplycount;
            model.TotalStaredQuestions = model.TotalStaredReceived + model.TotalStaredReceivedBeforeSend;

            //+ model.TotalStaredReceivedBeforeSend
            //model.TotalStaredReceived
            //model.TotalStaredPendingForSubmissionNew

            model.TotalUnstaredQuestions = model.TotalUnstaredReceived + model.TotalUnStaredReceivedBeforeSend;
            //+ model.TotalUnStaredReceivedBeforeSend

            //model.TotalUnStaredPendingForSubmissionNew
            model.TotalStaredPendingAndDraft = model.TotalStaredReceived + model.TotalStaredPendingForSubmissionNew;
            model.TotalUnStaredPendingAndDraft = model.TotalUnstaredReceived + model.TotalUnStaredPendingForSubmissionNew;

            return model;
        }

        public static object NoticeDraftRepliescount(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();


            string csv = model.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);


            var NoticesPendingForReply = (from NT in pCtxt.tMemberNotice
                                          where NT.AssemblyID == model.AssemblyId
                                                   && NT.SessionID == model.SessionId
                                                   && NT.PaperLaidId == null
                                                   && NT.MinistryId != null
                                                   && NT.IsSubmitted == true
                                                   && ids.Contains(NT.DepartmentId)
                                          select NT).Count();

            model.NoticePendingCount = NoticesPendingForReply;

            var noticedraftreplycount = (from a in pCtxt.tPaperLaidV
                                         join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                         join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                         where a.AssemblyId == model.AssemblyId
                                             && a.SessionId == model.SessionId
                                             && (a.DeptActivePaperId != null)
                                             && b.DeptSubmittedBy == null
                                             && b.DeptSubmittedDate == null
                                             && a.DeptActivePaperId == b.PaperLaidTempId
                                             && ids.Contains(a.DepartmentId)
                                         select a).Count();
            model.NoticeDraftCount = noticedraftreplycount;


            var noticesentReplycount = (from a in pCtxt.tPaperLaidV
                                        join b in pCtxt.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                        join c in pCtxt.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                        where a.AssemblyId == model.AssemblyId
                                     && a.SessionId == model.SessionId
                                     && (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
                                     && b.DeptSubmittedBy != null
                                     && b.DeptSubmittedDate != null
                                     && a.DeptActivePaperId == b.PaperLaidTempId
                                     && ids.Contains(a.DepartmentId)
                                        select a).Count();



            model.NoticeSentCount = noticesentReplycount;
            model.NoticeTotalCount = model.NoticePendingCount + model.NoticeDraftCount;

            return model;
        }


        static object GetPasswordMobileUser(object param)
        {
            tUserRegistrationDetails model = param as tUserRegistrationDetails;
            PaperLaidContext db = new PaperLaidContext();
            var get = (from a in db.tTempMobileUser
                       where a.UserId == model.UserId
                       select a).FirstOrDefault();

            return get;
        }

        private static object GetSMSGatewayList()
        {
            PaperLaidContext db = new PaperLaidContext();
            var data = db.tSmsGateway.ToList();
            return data.ToList();
        }


        //For LOB New
        public static object GetSentNotices(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            if (model.IsSubmitted == true)  //pending Papers
            {
                var data = (from Notice in pCtxt.tMemberNotices
                            where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID)
                            && (Notice.PaperLaidId == null)
                            && (Notice.NoticeTypeID != 74) && (Notice.IsBracketCM == null)
                            orderby Notice.MemberName descending
                            select Notice).ToList();

                var data1 = (from Notice in pCtxt.tMemberNotices
                             join tp in pCtxt.tPaperLaidVs on Notice.PaperLaidId equals tp.PaperLaidId
                             where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID)
                             && (Notice.PaperLaidId != null) && (tp.IsLaid == null) && (tp.LOBRecordId == null)
                             && (Notice.NoticeTypeID != 74) && (Notice.IsBracketCM == null)
                             orderby Notice.MemberName descending
                             select Notice).ToList();

                var mergeresult = data.Concat(data1);
                var results = mergeresult.ToList();
                model.memberNoticeList = results;
            }

            else if (model.IsSubmitted == null && model.IsLOBAttached == true)
            {
                var data = (from Notice in pCtxt.tMemberNotices
                            join tp in pCtxt.tPaperLaidVs on Notice.PaperLaidId equals tp.PaperLaidId
                            where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID)
                            && (Notice.PaperLaidId != null) && (tp.IsLaid == null) && (tp.LOBRecordId != null)
                            && (Notice.NoticeTypeID != 74)
                            orderby Notice.MemberName descending
                            select Notice).ToList();
                var results = data.ToList();

                model.memberNoticeList = results;

            }
            //get Only Laid notice List
            else
            {
                var data = (from Notice in pCtxt.tMemberNotices
                            join tp in pCtxt.tPaperLaidVs on Notice.PaperLaidId equals tp.PaperLaidId
                            where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID) && (Notice.IsSubmitted == true)
                            && (Notice.PaperLaidId != null)
                            && (tp.IsLaid == true)
                            orderby Notice.MemberName ascending
                            select Notice).ToList();
                var results = data.ToList();

                model.memberNoticeList = results;

            }
            return model;
        }
        static object GetMemberByID(object param)
        {
            PaperLaidContext deptContext = new PaperLaidContext();
            mMember model = param as mMember;
            int MemberID = model.MemberID;
            model = (from mem in deptContext.mMember where mem.MemberCode == MemberID select mem).FirstOrDefault();

            return model;
        }
        static tPaperLaidV GetPaperLaidById(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();
            paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV where mdl.PaperLaidId == paperLaid.PaperLaidId select mdl).ToList();

            return paperLaid;

        }
        static tPaperLaidTemp GetPaperLaidTempById(object param)
        {
            tPaperLaidTemp paperLaid = param as tPaperLaidTemp;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var result = (from mdl in plCntxt.tPaperLaidTemp where mdl.PaperLaidTempId == paperLaid.PaperLaidTempId select mdl).FirstOrDefault();
            paperLaid = result;
            return paperLaid;
        }
        //Count List
        static int GetNoticeCount(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice model = param as tMemberNotice;
            var purchCount = (from purchase in pCtxt.tMemberNotices
                              where (purchase.AssemblyID == model.AssemblyID) && (purchase.SessionID == model.SessionID)
                              && (purchase.NoticeTypeID != 74) && (purchase.IsBracketCM == null)
                              select purchase).Count();

            return purchCount;

        }
        static int GetLaidNoticeCount(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var data = (from Notice in pCtxt.tMemberNotices
                        join tp in pCtxt.tPaperLaidVs on Notice.PaperLaidId equals tp.PaperLaidId
                        where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID) && (Notice.IsSubmitted == true)
                        && (Notice.PaperLaidId != null)
                        && (Notice.NoticeTypeID != 74)
                        && (tp.IsLaid == true)
                        orderby Notice.MemberName ascending
                        select Notice).Count();
            var results = data;
            return results;

        }
        static int GetPendingtoLayNoticeCount(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var data = (from Notice in pCtxt.tMemberNotices
                        where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID)
                        && (Notice.PaperLaidId == null) && (Notice.NoticeTypeID != 74) && (Notice.IsBracketCM == null)
                        orderby Notice.MemberName descending
                        select Notice).ToList();

            var data1 = (from Notice in pCtxt.tMemberNotices
                         join tp in pCtxt.tPaperLaidVs on Notice.PaperLaidId equals tp.PaperLaidId
                         where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID)
                         && (Notice.PaperLaidId != null) && (tp.IsLaid == null) && (tp.LOBRecordId == null)
                         && (Notice.NoticeTypeID != 74) && (Notice.IsBracketCM == null)
                         orderby Notice.MemberName descending
                         select Notice).ToList();

            var mergeresult = data.Concat(data1);
            var results = mergeresult.Count();
            return results;

        }
        static int GetForLayingNoticeCount(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            //var data = (from Notice in pCtxt.tMemberNotices
            //            where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID)
            //            && (Notice.PaperLaidId == null)
            //            orderby Notice.MemberName descending
            //            select Notice).ToList();

            var data1 = (from Notice in pCtxt.tMemberNotices
                         join tp in pCtxt.tPaperLaidVs on Notice.PaperLaidId equals tp.PaperLaidId
                         where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID)
                         && (Notice.PaperLaidId == tp.PaperLaidId) && (tp.IsLaid == null) && (tp.LOBRecordId != null)
                         && (Notice.NoticeTypeID != 74)
                         orderby Notice.MemberName descending
                         select Notice).ToList();

            // var mergeresult = data.Concat(data1);
            var results = data1.Count();
            return results;

        }




        static int GetOtherpaperCount(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            tPaperLaidV model = param as tPaperLaidV;
            var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                                join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                //join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.DeptActivePaperId != null
                                && mdl.MinistryId != null && tps.DeptSubmittedDate >= model.DateFromis)
                                //&& tps.DeptSubmittedDate >= model.DateFromis && tps.DeptSubmittedDate <= model.DateTois && mdl.MinistryId != null)
                                //&& mdl.AssemblyId==model.AssemblyId && mdl.SessionId==model.SessionId
                                select mdl).Count();




            return ladpaperlist;

        }
        static int GetLaidOtherPaperCount(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            List<tPaperLaidV> LaidPaperslist = new List<tPaperLaidV>();

            var LaidPapers = (from mdl in plCntxt.tPaperLaidV
                              join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                              join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                              where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.IsLaid == true
                              && mdl.DeptActivePaperId != null && mdl.MinistryId != null && tps.DeptSubmittedDate >= paperLaid.DateFromis)
                              //&& mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId==paperLaid.SessionId)
                              //&& mdl.DepartmentId == paperLaid.DepartmentId && tps.DeptSubmittedDate >= paperLaid.DateFromis)
                              orderby tps.DeptSubmittedDate descending
                              select mdl).ToList();
            LaidPaperslist.AddRange(LaidPapers);
            var CountIs = LaidPaperslist.Count();
            return CountIs;

        }
        static int GetPendingtoLayOtherPaperCount(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            List<tPaperLaidV> PendingPaperslist = new List<tPaperLaidV>();

            var PapersList = (from mdl in plCntxt.tPaperLaidV
                              join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                              join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                              where (mevent.IsLOB == true && mdl.IsLaid == false && mdl.DeptActivePaperId != null && mevent.PaperCategoryTypeId == 5
                                     && mdl.MinistryId != null && mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                                     && mdl.LOBRecordId == null)
                                 || (mevent.IsLOB == true && mdl.IsLaid == null && mdl.MinistryId != null && mevent.PaperCategoryTypeId == 5
                                //&& mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId)
                                && tps.DeptSubmittedDate >= paperLaid.DateFromis && mdl.LOBRecordId == null)

                              //orderby tps.DeptSubmittedDate descending
                              select mdl).ToList();
            PendingPaperslist.AddRange(PapersList);
            var CountIs = PendingPaperslist.Count();
            return CountIs;

        }
        static int GetPapersForLayingCount(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            List<tPaperLaidV> PendingPaperslist = new List<tPaperLaidV>();

            var PapersList = (from mdl in plCntxt.tPaperLaidV
                              join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                              join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                              where (mevent.IsLOB == true && mdl.IsLaid == false && mdl.DeptActivePaperId != null && mevent.PaperCategoryTypeId == 5
                                     && mdl.MinistryId != null && mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                                     && mdl.LOBRecordId != null)
                                 || (mevent.IsLOB == true && mdl.IsLaid == null && mdl.MinistryId != null && mevent.PaperCategoryTypeId == 5
                                //&& mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId)
                                && tps.DeptSubmittedDate >= paperLaid.DateFromis && mdl.LOBRecordId != null)

                              //orderby tps.DeptSubmittedDate descending
                              select mdl).ToList();
            PendingPaperslist.AddRange(PapersList);
            var CountIs = PendingPaperslist.Count();
            return CountIs;

        }
        static int BillspaperCount(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            tPaperLaidV model = param as tPaperLaidV;
            var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                join c in plCntxt.mEvent on mdl.EventId equals c.EventId
                                join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                //join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                where (c.IsLOB == true &&  c.PaperCategoryTypeId == 3   && mdl.DeptActivePaperId != null
                                && mdl.MinistryId != null)
                                //&& tps.DeptSubmittedDate >= model.DateFromis && tps.DeptSubmittedDate <= model.DateTois && mdl.MinistryId != null)
                                && mdl.AssemblyId == model.AssemblyId && mdl.SessionId == model.SessionId
                                select mdl).Count();

            return ladpaperlist;

        }
        static int BillsLaidCount(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            List<tPaperLaidV> PendingPaperslist = new List<tPaperLaidV>();


            var BillsPapers = (from mdl in plCntxt.tPaperLaidV
                               join c in plCntxt.mEvent on mdl.EventId equals c.EventId
                               join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                               where c.PaperCategoryTypeId == 3 && mdl.IsLaid == true && mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                               && mdl.DeptActivePaperId != null
                               // && tps.DeptSubmittedDate >= paperLaid.DateFromis
                               //orderby tps.DeptSubmittedDate ascending
                               select mdl).ToList();

            PendingPaperslist.AddRange(BillsPapers);
            var CountIs = PendingPaperslist.Count();
            return CountIs;

        }
        static int BillsPendingToLayCount(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            List<tPaperLaidV> PendingPaperslist = new List<tPaperLaidV>();

            var BillsPapers = (from mdl in plCntxt.tPaperLaidV
                               join c in plCntxt.mEvent on mdl.EventId equals c.EventId
                               join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                               where mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                               && c.PaperCategoryTypeId == 3 && mdl.IsLaid == null && mdl.DeptActivePaperId != null && mdl.LOBRecordId == null
                               // orderby tps.DeptSubmittedDate ascending
                               select mdl).ToList();

            PendingPaperslist.AddRange(BillsPapers);
            var CountIs = PendingPaperslist.Count();
            return CountIs;

        }
        static int BillsForLayingCount(object param)
        {
            PaperLaidContext plCntxt = new PaperLaidContext();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            List<tPaperLaidV> PendingPaperslist = new List<tPaperLaidV>();

            var BillsPapers = (from mdl in plCntxt.tPaperLaidV
                               join c in plCntxt.mEvent on mdl.EventId equals c.EventId
                               join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                               where mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                               && c.PaperCategoryTypeId == 3 && mdl.IsLaid == null && mdl.DeptActivePaperId != null && mdl.LOBRecordId != null
                               // orderby tps.DeptSubmittedDate ascending
                               select mdl).ToList();

            PendingPaperslist.AddRange(BillsPapers);
            var CountIs = PendingPaperslist.Count();
            return CountIs;

        }
        static int GetCRCount(object param)
        {
            PaperLaidContext pCtxt = new PaperLaidContext();
            tPaperLaidV model = param as tPaperLaidV;
            var ladpaperlist = (from adminlob in pCtxt.tCommitteeReport
                                where (adminlob.AssemblyID == model.AssemblyId) && (adminlob.SessionID == model.SessionId)
                                //&& (adminlob.IsFreeze == true)
                                select adminlob).Count();


            return ladpaperlist;

        }
        static int GetLaidCRCount(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();

            var CommitteeReports = (from adminlob in plCntxt.tCommitteeReport
                                    where (adminlob.AssemblyID == model.AssemblyId) && (adminlob.SessionID == model.SessionId) && (adminlob.IsFreeze == true)
                                    select adminlob).ToList();

            var results = CommitteeReports.ToList();
            var CountIs = results.Count();
            return CountIs;

        }
        static int GetPendingToLayCRCount(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();


            var CommitteeReports = (from adminlob in plCntxt.tCommitteeReport
                                    join tp in plCntxt.tPaperLaidV on adminlob.PaperLaidId equals tp.PaperLaidId
                                    where (adminlob.AssemblyID == model.AssemblyId) && (adminlob.SessionID == model.SessionId)
                                    && (adminlob.IsFreeze == false || adminlob.IsFreeze == null) && (tp.LOBRecordId == null)
                                    select adminlob).ToList();
            var results = CommitteeReports.ToList();
            var CountIs = results.Count();
            return CountIs;

        }
        static int GetForLayingCRCount(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();


            var CommitteeReports = (from adminlob in plCntxt.tCommitteeReport
                                    join tp in plCntxt.tPaperLaidV on adminlob.PaperLaidId equals tp.PaperLaidId
                                    where (adminlob.AssemblyID == model.AssemblyId) && (adminlob.SessionID == model.SessionId)
                                    && (adminlob.IsFreeze == false || adminlob.IsFreeze == null) && (tp.LOBRecordId != null)
                                    select adminlob).ToList();
            var results = CommitteeReports.ToList();
            var CountIs = results.Count();
            return CountIs;

        }
        public static object GetSessionDateIdByDate(object param)
        {
            //mSession modal = new mSession();
            // mSessions paperLaid = param as mSessions;
            mSessionDate paperLaid = param as mSessionDate;
            PaperLaidContext plCntxt = new PaperLaidContext();
            paperLaid.SessionId = (from mdl in plCntxt.mSessionDates
                                   where mdl.SessionDate == paperLaid.SessionDate
                                      && mdl.AssemblyId == paperLaid.AssemblyId
                                   select mdl.Id).FirstOrDefault();
            //modal=((from mdl in plCntxt.mSessionDates where mdl.SessionDate == paperLaid.SessionDate
            //                        && mdl.AssemblyId == paperLaid.AssemblyId 
            //                        select mdl.Id).

            return paperLaid;

        }
        static tPaperLaidV GetPaperLaidByDeptId2(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            List<tPaperLaidV> laidpaperlist = new List<tPaperLaidV>();
            PaperLaidContext plCntxt = new PaperLaidContext();

            if (paperLaid.EventId == 5) // For Bills
            {
                if (paperLaid.IsLaid == false && paperLaid.IsLOBAttached == true)
                {
                    //36
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        where mdl.IsLaid == null && mdl.MinistryId != null
                                        && mdl.DeptActivePaperId != null && mdl.LOBRecordId != null &&
                                        mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                                        orderby tps.DeptSubmittedDate ascending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);
                }
                else if (paperLaid.IsLaid == null || paperLaid.IsLaid == false)
                {
                    //36
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        //join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                        //where mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                                        where  mdl.IsLaid == null && mdl.MinistryId != null
                                        && mdl.DeptActivePaperId != null && mdl.LOBRecordId == null &&
                                        mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                                        //&& tps.DeptSubmittedDate <= paperLaid.DateTois
                                        orderby tps.DeptSubmittedDate ascending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);
                }
                else if (paperLaid.IsLaid == true)
                {
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        // join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                        where  mdl.IsLaid == true
                                         && mdl.MinistryId != null && mdl.DeptActivePaperId != null &&
                                          mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId
                                        //&& tps.DeptSubmittedDate <= paperLaid.DateTois
                                        orderby tps.DeptSubmittedDate ascending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);
                }
            }

            else if (paperLaid.EventId == 4) // For Notices
            {
                if (paperLaid.IsLaid == null)
                {
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        //join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 3 && mdl.DeptActivePaperId != null
                                        && mdl.MinistryId != null
                                         //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois) ||
                                         && mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId) ||
                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 3 && mdl.DeptActivePaperId != null
                                        && mdl.MinistryId != null
                                        //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
                                        && mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId)
                                        orderby mdl.PaperlaidPriority ascending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);

                }
                else if (paperLaid.IsLaid == true)
                {
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        // join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 3 && mdl.IsLaid == paperLaid.IsLaid
                                        && mdl.DeptActivePaperId != null && mdl.MinistryId != null
                                         //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
                                         && mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId)
                                        orderby mdl.PaperlaidPriority ascending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);
                }
                else
                {
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        //   join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 3 && mdl.IsLaid == paperLaid.IsLaid
                                        && mdl.DeptActivePaperId != null && mdl.MinistryId != null
                                          //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois) ||
                                          && mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId) ||
                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 3 && mdl.IsLaid == null
                                        && mdl.DeptActivePaperId != null && mdl.MinistryId != null
                                        //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
                                        && mdl.AssemblyId == paperLaid.AssemblyId && mdl.SessionId == paperLaid.SessionId)
                                        orderby mdl.PaperlaidPriority ascending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);
                }
            }

            else //For other papers
            {
                if (paperLaid.IsLaid == null)
                {
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        // join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 3 && mdl.DeptActivePaperId != null
                                         && mdl.MinistryId != null && tps.DeptSubmittedDate >= paperLaid.DateFromis)
                                        //&& mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId)
                                        //  && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
                                        || (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 3 && mdl.DeptActivePaperId != null
                                        //&& mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId)
                                        && mdl.MinistryId != null && tps.DeptSubmittedDate >= paperLaid.DateFromis)
                                        //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois

                                        orderby tps.DeptSubmittedDate descending
                                        select mdl).ToList();

                    laidpaperlist.AddRange(ladpaperlist);

                }
                else if (paperLaid.IsLaid == false && paperLaid.IsLOBAttached == true)
                {
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        where (mevent.IsLOB == true && mdl.IsLaid == paperLaid.IsLaid && mevent.PaperCategoryTypeId == 3
                                        && mdl.DeptActivePaperId != null && mdl.MinistryId != null && tps.DeptSubmittedDate >= paperLaid.DateFromis
                                        && mdl.LOBRecordId != null)
                                        || (mevent.IsLOB == true && mdl.IsLaid == null && mevent.PaperCategoryTypeId == 3
                                        && mdl.MinistryId != null && tps.DeptSubmittedDate >= paperLaid.DateFromis && mdl.LOBRecordId != null)

                                        orderby tps.DeptSubmittedDate descending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);
                }
                else if (paperLaid.IsLaid == true)
                {
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        // join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 3 && mdl.IsLaid == paperLaid.IsLaid
                                         //&& mdl.DeptActivePaperId != null && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId
                                         && mdl.DeptActivePaperId != null && mdl.MinistryId != null
                                         //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
                                         && tps.DeptSubmittedDate >= paperLaid.DateFromis)
                                        orderby tps.DeptSubmittedDate descending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);
                }
                else
                {
                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
                                        join tps in plCntxt.tPaperLaidTemp on mdl.DeptActivePaperId equals tps.PaperLaidTempId
                                        //join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
                                        where (mevent.IsLOB == true && mdl.IsLaid == paperLaid.IsLaid && mevent.PaperCategoryTypeId == 3
                                        && mdl.DeptActivePaperId != null && mdl.MinistryId != null && tps.DeptSubmittedDate >= paperLaid.DateFromis
                                        && mdl.LOBRecordId == null)
                                        //&& mdl.DeptActivePaperId != null && mdl.MinistryId == Id)                                                
                                        // && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
                                        ||
                                        (mevent.IsLOB == true && mdl.IsLaid == null && mevent.PaperCategoryTypeId == 3 && mdl.MinistryId != null
                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && mdl.LOBRecordId == null)
                                        //&& mdl.MinistryId == Id)                                               
                                        //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois                                               
                                        orderby tps.DeptSubmittedDate descending
                                        select mdl).ToList();
                    laidpaperlist.AddRange(ladpaperlist);
                }
            }
            paperLaid.ListtPaperLaidV = laidpaperlist.ToList();
            return paperLaid;
        }

        //static tPaperLaidV GetPaperLaidByDeptId2(object param)
        //{
        //    tPaperLaidV paperLaid = param as tPaperLaidV;
        //    List<tPaperLaidV> laidpaperlist = new List<tPaperLaidV>();

        //    PaperLaidContext plCntxt = new PaperLaidContext();

        //    var MinisIds = (from RM in plCntxt.tRotationMinister
        //                    where (RM.SessionDateId == paperLaid.SessionDateId)
        //                    select RM.MinistryId).FirstOrDefault();
        //    MinisIds = MinisIds.ToString().Trim();
        //    string[] StrId = MinisIds.Split(',');

        //    for (int i = 0; i < StrId.Length; i++)
        //    {
        //        int Id = Convert.ToInt16(StrId[i]);
        //        if (paperLaid.EventId == 5) // For Bills
        //        {
        //            var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                where (mdl.EventId == paperLaid.EventId && mdl.IsLaid == false && mdl.DeptActivePaperId != null && mdl.MinistryId == Id) ||
        //                                (mdl.EventId == paperLaid.EventId && mdl.IsLaid == null && mdl.DeptActivePaperId != null && mdl.MinistryId == Id)
        //                                 && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois
        //                                orderby mdl.PaperlaidPriority ascending
        //                                select mdl).ToList();
        //            laidpaperlist.AddRange(ladpaperlist);
        //        }

        //        else if (paperLaid.EventId == 4) // For Notices
        //        {


        //            if (paperLaid.DepartmentId != "0")
        //            {
        //                if (paperLaid.IsLaid == null)
        //                {

        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.DeptActivePaperId != null
        //                                        && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois) ||
        //                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.DeptActivePaperId != null
        //                                        && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();

        //                    laidpaperlist.AddRange(ladpaperlist);

        //                }
        //                else if (paperLaid.IsLaid == true)
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.IsLaid == paperLaid.IsLaid
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)

        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }
        //                else
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.IsLaid == paperLaid.IsLaid
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois) ||
        //                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.IsLaid == null
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }
        //            }
        //            else
        //            {
        //                if (paperLaid.IsLaid == null)
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.DeptActivePaperId != null && mdl.MinistryId == Id
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois) ||
        //                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.DeptActivePaperId != null && mdl.MinistryId == Id
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }
        //                else if (paperLaid.IsLaid == true)
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.IsLaid == paperLaid.IsLaid
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)

        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }
        //                else
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.IsLaid == paperLaid.IsLaid
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id)
        //                                            //&& tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                                        ||
        //                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.IsLaid == null
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id)
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois
        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }

        //            }



        //        }

        //        else //For other papers
        //        {


        //            if (paperLaid.DepartmentId != "0")
        //            {
        //                if (paperLaid.IsLaid == null)
        //                {
        //                    //var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                    //                    where (mdl.IsLaid == false && mdl.DeptActivePaperId != null && mdl.MinistryId == Id) ||
        //                    //                     (mdl.IsLaid == null && mdl.DeptActivePaperId != null && mdl.MinistryId == Id)
        //                    //                    orderby mdl.PaperlaidPriority ascending
        //                    //                    select mdl).ToList();
        //                    //laidpaperlist.AddRange(ladpaperlist);

        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.DeptActivePaperId != null
        //                                        && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId)
        //                                            //  && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                                        ||
        //                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.DeptActivePaperId != null
        //                                        && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId)
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois
        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();

        //                    laidpaperlist.AddRange(ladpaperlist);

        //                }
        //                else if (paperLaid.IsLaid == true)
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.IsLaid == paperLaid.IsLaid
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)

        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }
        //                else
        //                {
        //                    //var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                    //                    join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                    //                    join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                    //                    where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 4 && mdl.IsLaid == paperLaid.IsLaid
        //                    //                    && mdl.DeptActivePaperId != null && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId)
        //                    //                   // && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                    //                   ||
        //                    //                    (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.IsLaid == null
        //                    //                    && mdl.DeptActivePaperId != null && mdl.MinistryId == Id && mdl.DepartmentId == paperLaid.DepartmentId)
        //                    //                   // && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                    //                    orderby mdl.PaperlaidPriority ascending
        //                    //                    select mdl).ToList();
        //                    //laidpaperlist.AddRange(ladpaperlist);
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mdl.IsLaid == paperLaid.IsLaid
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id)
        //                                            // && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                                       ||
        //                                        (mevent.IsLOB == true && mdl.IsLaid == null
        //                                         && mdl.MinistryId == Id)
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois
        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }
        //            }
        //            else
        //            {
        //                if (paperLaid.IsLaid == null)
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.DeptActivePaperId != null && mdl.MinistryId == Id
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois) ||
        //                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.DeptActivePaperId != null && mdl.MinistryId == Id
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }
        //                else if (paperLaid.IsLaid == true)
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.IsLaid == paperLaid.IsLaid
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)

        //                                        orderby mdl.PaperlaidPriority ascending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }
        //                else
        //                {
        //                    var ladpaperlist = (from mdl in plCntxt.tPaperLaidV
        //                                        join mevent in plCntxt.mEvent on mdl.EventId equals mevent.EventId
        //                                        join tps in plCntxt.tPaperLaidTemp on mdl.PaperLaidId equals tps.PaperLaidId
        //                                        where (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.IsLaid == paperLaid.IsLaid
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois) ||
        //                                        (mevent.IsLOB == true && mevent.PaperCategoryTypeId == 5 && mdl.IsLaid == null
        //                                        && mdl.DeptActivePaperId != null && mdl.MinistryId == Id
        //                                        && tps.DeptSubmittedDate >= paperLaid.DateFromis && tps.DeptSubmittedDate <= paperLaid.DateTois)
        //                                        orderby tps.DeptSubmittedDate descending
        //                                        select mdl).ToList();
        //                    laidpaperlist.AddRange(ladpaperlist);
        //                }

        //            }


        //        }

        //    }


        //    //var newList = laidpaperlist.OrderByDescending(x => x.DeptSubmittedDate).ToList();
        //    //paperLaid.ListtPaperLaidV = newList;
        //    // paperLaid.ListtPaperLaidV = laidpaperlist.ToList();
        //    paperLaid.ListtPaperLaidV = laidpaperlist.OrderByDescending(x => x.DeptSubmittedDate).ToList();
        //    paperLaid.ListtPaperLaidV = paperLaid.ListtPaperLaidV.OrderByDescending(m => m.DeptSubmittedDate).ToList();
        //    return paperLaid;


        //}

        public static object GetCommitteeReports(object param)
        {
            //tPaperLaidV paperLaid = param as tPaperLaidV;
            List<tPaperLaidV> laidpaperlist = new List<tPaperLaidV>();
            tCommitteeReport model = param as tCommitteeReport;
            PaperLaidContext plCntxt = new PaperLaidContext();
            if (model.IsLOBAttached == true && model.IsFreeze == false)
            {
                var ladpaperlist = (from adminlob in plCntxt.tCommitteeReport
                                    join tp in plCntxt.tPaperLaidV on adminlob.PaperLaidId equals tp.PaperLaidId
                                    where (adminlob.AssemblyID == model.AssemblyID) && (adminlob.SessionID == model.SessionID) && (adminlob.IsFreeze == false || adminlob.IsFreeze == null)
                                    && (tp.LOBRecordId != null)
                                    select adminlob).ToList();
                var results = ladpaperlist.ToList();
                model.committeereportList = results;

            }
            else if (model.IsFreeze == null || model.IsFreeze == false)
            {
                var ladpaperlist = (from adminlob in plCntxt.tCommitteeReport
                                    join tp in plCntxt.tPaperLaidV on adminlob.PaperLaidId equals tp.PaperLaidId
                                    where (adminlob.AssemblyID == model.AssemblyID) && (adminlob.SessionID == model.SessionID) && (adminlob.IsFreeze == false || adminlob.IsFreeze == null)
                                      && (tp.LOBRecordId == null)
                                    select adminlob).ToList();
                var results = ladpaperlist.ToList();
                model.committeereportList = results;

            }
            else if (model.IsFreeze == true)
            {
                var ladpaperlist = (from adminlob in plCntxt.tCommitteeReport
                                    where (adminlob.AssemblyID == model.AssemblyID) && (adminlob.SessionID == model.SessionID) && (adminlob.IsFreeze == true)
                                    select adminlob).ToList();


                var results = ladpaperlist.ToList();
                model.committeereportList = results;
            }





            //int totalRecords = query.Count();
            //var results = query.ToList();

            //model.ResultCount = totalRecords;
            //model.ListtPaperLaidVNotice = results;

            return model;
        }
        private static object GetNoticeTypeNamebyId(object param)
        {
            int Id = Convert.ToInt32(param);
            //mEvent db = new mEvent();
            PaperLaidContext pCtxt = new PaperLaidContext();
            var Res = (from mdl in pCtxt.mEvent where mdl.EventId == Id select mdl.EventName).SingleOrDefault();
            pCtxt.Close();
            return Res;
        }
        static object GetNoticeDetailsById(object param)
        {
            tMemberNotice model1 = param as tMemberNotice;
            //  int noticeNumber = param.ToInt16();
            PaperLaidContext pCtxt = new PaperLaidContext();
            tMemberNotice model = new tMemberNotice();
            model = (from mdl in pCtxt.tMemberNotice where mdl.NoticeId == model1.NoticeId select mdl).SingleOrDefault();
            //model.Subject = Regex.Replace(model.Subject, @"<[^>]+>|&nbsp;", "").Trim();
            //model.Notice = Regex.Replace(model.Notice, @"<[^>]+>|&nbsp;", "").Trim();
            //string[] date = model.NoticeDate.ToString().Split(' ');
            //model.DateNotice = date[0];
            return model;
        }
        static tPaperLaidTemp GetPaperLaidTempId(object param)
        {


            tPaperLaidTemp paperLaid = param as tPaperLaidTemp;
            PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();
            var data = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidId == paperLaid.PaperLaidId select mdl).ToList();


            var results = data.ToList();
            paperLaid.ListPaperLaidTemp = results;

            return paperLaid;
        }
        static object GetOnlineNoticesDetailsById(object param)
        {
            OnlineNotices model1 = param as OnlineNotices;
            //  int noticeNumber = param.ToInt16();
            PaperLaidContext pCtxt = new PaperLaidContext();
            OnlineNotices model = new OnlineNotices();
            model = (from mdl in pCtxt.OnlineNotices where mdl.ManualNoticeId == model1.ManualNoticeId select mdl).SingleOrDefault();
            //model.Subject = Regex.Replace(model.Subject, @"<[^>]+>|&nbsp;", "").Trim();
            //model.Notice = Regex.Replace(model.Notice, @"<[^>]+>|&nbsp;", "").Trim();
            //string[] date = model.NoticeDate.ToString().Split(' ');
            //model.DateNotice = date[0];
            return model;
        }


        static DraftLOB GetLaidDateById(object param)
        {
            DraftLOB Dlob = param as DraftLOB;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var result = (from mdl in plCntxt.DraftLOB where mdl.Id == Dlob.Id select mdl).FirstOrDefault();
            if (result == null)
            {
                Dlob.SessionDateLocal = "e";

            }
            else
            {
                Dlob.SessionDate = result.SessionDate;
            }

            return Dlob;
        }
        static object GetNoticeRuleByNoticeTypeID(object param)
        {
            PaperLaidContext deptContext = new PaperLaidContext();
            mEvent model = param as mEvent;
            // int MemberID = model.MemberID;
            int a = model.EventId;
            model = (from mem in deptContext.mEvent where mem.EventId == a select mem).FirstOrDefault();
            return model;
        }
        static tPaperLaidV CheckIsLOBAttached(object param)
        {
            tPaperLaidV Dlob = param as tPaperLaidV;
            PaperLaidContext plCntxt = new PaperLaidContext();

            var Check = (from mdl in plCntxt.tPaperLaidV where mdl.PaperLaidId == Dlob.PaperLaidId select mdl).FirstOrDefault();


            //  var result = (from mdl in plCntxt.DraftLOB where mdl.PaperLaidId == Dlob.PaperLaidId select mdl).FirstOrDefault();
            if (Check != null)
            {
                Dlob.LOBRecordId = Check.LOBRecordId;
            }
            else
            {
                Dlob.LOBRecordId = 0;
            }
            //Dlob.SessionDate = result.SessionDate;
            return Dlob;

        }
        static tMemberNotice UpdateNoticeIsSecCheck(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            var tPaperLaid = (from mdl in pCtxt.tMemberNotices where mdl.NoticeNumber == model.NoticeNumber select mdl).SingleOrDefault();
            if (tPaperLaid != null)
            {
                if (tPaperLaid.IsSecChecked == model.IsSecChecked)
                {

                }
                else
                {
                    tPaperLaid.IsSecChecked = model.IsSecChecked;
                    pCtxt.SaveChanges();
                    pCtxt.Close();
                }



            }
            return tPaperLaid;
        }
        //UpdateOtherPaperIsVsSecMark
        static tPaperLaidV UpdateOtherPaperIsVsSecMark(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidContext pCtxt = new PaperLaidContext();

            var tPaperLaid = (from mdl in pCtxt.tPaperLaidV where mdl.PaperLaidId == model.PaperLaidId select mdl).SingleOrDefault();
            if (tPaperLaid != null)
            {
                if (tPaperLaid.IsVSSecMarked == model.IsVSSecMarked)
                {

                }
                else
                {
                    tPaperLaid.IsVSSecMarked = model.IsVSSecMarked;
                    pCtxt.SaveChanges();
                    pCtxt.Close();
                }



            }
            return tPaperLaid;
        }

        static DraftLOB CheckNextSrNo(object param)
        {
            DraftLOB lob = param as DraftLOB;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var result = (from mdl in plCntxt.DraftLOB
                          where mdl.SrNo1 == lob.SrNo1 && mdl.SrNo2 == lob.SrNo2 && mdl.LOBId == lob.LOBId
&& mdl.AssemblyId == 13 && mdl.SessionId == 5
                          select mdl).FirstOrDefault();
            if (result == null)
            {
                lob.PPTLocation = "null";
            }
            else
            {
                lob.PPTLocation = "NotNull";
            }
            return lob;
        }
        static DraftLOB CheckPreviousSrNo(object param)
        {
            DraftLOB lob = param as DraftLOB;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var result = (from mdl in plCntxt.DraftLOB
                          where mdl.SrNo1 == lob.SrNo1 && mdl.SrNo2 == lob.SrNo2 && mdl.LOBId == lob.LOBId
                              && mdl.AssemblyId == 13 && mdl.SessionId == 5
                          select mdl).FirstOrDefault();
            if (result == null)
            {
                lob.PPTLocation = "null";
            }
            else
            {
                lob.PPTLocation = "NotNull";
            }
            return lob;
        }

        static DraftLOB CheckNextSubSrNo(object param)
        {
            DraftLOB lob = param as DraftLOB;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var result = (from mdl in plCntxt.DraftLOB
                          where mdl.SrNo1 == lob.SrNo1 && mdl.SrNo2 == lob.SrNo2 && mdl.SrNo3 == lob.SrNo3 && mdl.LOBId == lob.LOBId
                              && mdl.AssemblyId == 13 && mdl.SessionId == 5
                          select mdl).FirstOrDefault();
            if (result == null)
            {
                lob.PPTLocation = "null";
            }
            else
            {
                lob.PPTLocation = "NotNull";
            }
            return lob;
        }
        static DraftLOB CheckPreviousSubSrNo(object param)
        {
            DraftLOB lob = param as DraftLOB;
            PaperLaidContext plCntxt = new PaperLaidContext();
            var result = (from mdl in plCntxt.DraftLOB
                          where mdl.SrNo1 == lob.SrNo1 && mdl.SrNo2 == lob.SrNo2 && mdl.SrNo3 == lob.SrNo3 && mdl.LOBId == lob.LOBId
                              && mdl.AssemblyId == 13 && mdl.SessionId == 5
                          select mdl).FirstOrDefault();
            if (result == null)
            {
                lob.PPTLocation = "null";
            }
            else
            {
                lob.PPTLocation = "NotNull";
            }
            return lob;
        }

    }
}
