﻿using SBL.DAL;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.PISModules;
using SBL.DomainModel.Models.PISMolues;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
namespace SBL.Domain.Context.PISModule
{
    public class PISDesignationContext : DBBase<PISDesignationContext>
    {
        public PISDesignationContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<mPISDesignation> mPISDesignation { get; set; }
        public DbSet<tPISEmpSanctionpost> tPISEmpSanctionpost { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<DistrictModel> DistrictModel { get; set; }

        public virtual DbSet<mOffice> mOffices { get; set; }
        public virtual DbSet<tPISEmployeePersonal> tPISEmployeePersonal { get; set; }
        public virtual DbSet<Emppersonal_Log> Emppersonal_Log { get; set; }
        public DbSet<SanFillPostPdf> SanFillPostPdf { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetAllDesignation":
                    {
                        return GetAllDesignation(param.Parameter);
                    }
                case "GetAllSenctionPost":
                    {
                        return GetAllSenctionPost(param.Parameter);
                    }
                case "GetSanctionPostList":
                    {
                        return GetSanctionPostList(param.Parameter);
                    }
                case "CreateSantionPost":
                    {
                        return CreateSantionPost(param.Parameter);
                    }
                case "GetSantionpostForm":
                    {
                        return GetSantionpostForm(param.Parameter);
                    }
                case "GetSanctionPostListBiID":
                    {
                        return GetSanctionPostListBiID(param.Parameter);
                    }
                case "UpdateSanctionPost":
                    {
                        return UpdateSanctionPost(param.Parameter);
                    }
                case "GetSantionpostByid":
                    {
                        return GetSantionpostByid(param.Parameter);
                    }
                case "UpdateToSanctionPost":
                    {
                        return UpdateToSanctionPost(param.Parameter);
                    }
                case "DeleteSantion":
                    {
                        return DeleteSantion(param.Parameter);
                    }
                case "GelAllEmpList":
                    {
                        return GelAllEmpList(param.Parameter);
                    }
                case "CreateEmployee":
                    {
                        return CreateEmployee(param.Parameter);
                    }
                case "GetEmpCount":
                    {
                        return GetEmpCount(param.Parameter);
                    }
                case "UpdateEmployee":
                    {
                        return UpdateEmployee(param.Parameter);
                    }
                case "GetEmpInfoById":
                    {
                        return GetEmpInfoById(param.Parameter);
                    }
                case "CheckAdharIsExist":
                    {
                        return CheckAdharIsExist(param.Parameter);
                    }
                case "DeleteEmployee":
                    {
                        return DeleteEmployee(param.Parameter);
                    }
                case "UpdateEmployeeByPopup":
                    {
                        return UpdateEmployeeByPopup(param.Parameter);
                    }
                case "GetDesigCodeById":
                    {
                        return GetDesigCodeById(param.Parameter);
                    }
                case "GetAllDesignationByDeptId":
                    {
                        return GetAllDesignationByDeptId(param.Parameter);
                    }
                case "CheckDuplicate":
                    {
                        return CheckDuplicate(param.Parameter);
                    }
                case "CheckFilledPost":
                    {
                        return CheckFilledPost(param.Parameter);
                    }
                case "CheckSanctionPost":
                    {
                        return CheckSanctionPost(param.Parameter);
                    }
                case "GelAllRelievedEmpList":
                    {
                        return GelAllRelievedEmpList(param.Parameter);
                    }
                case "GetAllDesignationByOfficeId":
                    {
                        return GetAllDesignationByOfficeId(param.Parameter);
                    }
                case "GetAllDistrict":
                    {
                        return GetAllDistrict();
                    }
                case "GelAllEmpListById":
                    {
                        return GelAllEmpListById(param.Parameter);
                    }
                case "CheckEmpExist":
                    {
                        return CheckEmpExist(param.Parameter);
                    }
                case "GelEmpListByOfficeId":
                    {
                        return GelEmpListByOfficeId(param.Parameter);
                    }
                case "GetPostByOfficeId":
                    {
                        return GetPostByOfficeId(param.Parameter);
                    }

                case "GetAllSanFillPostsByOfficeId":
                    {
                        return GetAllSanFillPostsByOfficeId(param.Parameter);
                    }
                case "CreateSanFillPost":
                    {
                        return CreateSanFillPost(param.Parameter);
                    }
                case "UpdateSanFillPost":
                    {
                        return UpdateSanFillPost(param.Parameter);
                    }
                case "GetSanFillPostsByid":
                    {
                        return GetSanFillPostsByid(param.Parameter);
                    }
            }
            return null;
        }


        #region Designation
        static object CheckEmpExist(object param)
        {
            tPISEmpSanctionpost ToUpdate = param as tPISEmpSanctionpost;
            bool ReturnValue = false;
            try
            {

                using (PISDesignationContext _context = new PISDesignationContext())
                {

                    if (ToUpdate.Id != null && ToUpdate.Id != 0)
                    {
                        tPISEmpSanctionpost obj = _context.tPISEmpSanctionpost.Single(m => m.Id == ToUpdate.Id);
                        ReturnValue = _context.tPISEmployeePersonal.Any(sp => sp.deptid == obj.Deptid && sp.POfficeId == obj.POfficeId && sp.PDesignationId == obj.PDesignationId);
                    }
                    else
                    {
                        ReturnValue = _context.tPISEmployeePersonal.Any(sp => sp.deptid == ToUpdate.Deptid && sp.POfficeId == ToUpdate.POfficeId && sp.PDesignationId == ToUpdate.PDesignationId);

                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ReturnValue; ;
        }

        static object DeleteSantion(object param)
        {
            tPISEmpSanctionpost ToUpdate = param as tPISEmpSanctionpost;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {

                    tPISEmpSanctionpost obj = _context.tPISEmpSanctionpost.Single(m => m.Id == ToUpdate.Id);

                    _context.tPISEmpSanctionpost.Remove(obj);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ToUpdate;
        }
        static object UpdateToSanctionPost(object param)
        {
            tPISEmpSanctionpost ToUpdate = param as tPISEmpSanctionpost;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {

                    tPISEmpSanctionpost obj = _context.tPISEmpSanctionpost.Single(m => m.Id == ToUpdate.Id);
                    obj.officeid = (from sp in _context.mOffices where sp.OfficeId == ToUpdate.POfficeId select sp.OfficeCode).FirstOrDefault().ToString();
                    obj.POfficeId = ToUpdate.POfficeId;
                    obj.Deptid = ToUpdate.Deptid;
                    obj.desigcd = ToUpdate.desigcd;
                    obj.Priority = ToUpdate.Priority;
                    obj.PDesignationId = ToUpdate.PDesignationId;
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ToUpdate;
        }
        public static object GetAllDesignation(object param)
        {

            try
            {
                string AadharId = param as string;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.mPISDesignation
                                orderby md.Id descending
                                select md).ToList();


                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetAllDesignationByDeptId(object param)
        {

            try
            {
                string DeptID = param as string;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.mPISDesignation
                                where md.deptid == DeptID.Trim()
                                orderby md.designame ascending
                                select md).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetAllDesignationByOfficeId(object param)
        {

            try
            {
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.tPISEmpSanctionpost
                                join des in db.mPISDesignation on md.PDesignationId equals des.Id
                                where md.Deptid == model.Deptid.Trim() && md.POfficeId == model.POfficeId
                                orderby des.designame ascending
                                select des).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetSantionpostByid(object param)
        {

            try
            {
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.tPISEmpSanctionpost
                                where md.Id == model.Id
                                orderby md.Id descending

                                select md).SingleOrDefault();

                    string deptname = (from d in db.mDepartment where d.deptId == data.Deptid select d.deptname).SingleOrDefault();
                    data.DepartmentName = deptname;
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object CheckDuplicate(object param)
        {

            try
            {
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                List<tPISEmpSanctionpost> result = new List<tPISEmpSanctionpost>();
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    if (model.type == "update")
                    {
                        var data = (from md in db.tPISEmpSanctionpost
                                    join dep in db.mDepartment on md.Deptid equals dep.deptId
                                    join des in db.mPISDesignation on model.PDesignationId equals des.Id
                                    join off in db.mOffices on md.POfficeId equals off.OfficeId
                                    orderby md.Id descending
                                    where md.Deptid == model.Deptid && md.PDesignationId == model.PDesignationId && md.POfficeId == model.POfficeId
                                    select new
                                    {
                                        md.Id,
                                        dep.deptname,
                                        des.designame,
                                        off.officename,
                                        md.sanctpost,
                                        md.Filledpost

                                    }).ToList();


                        if (data != null && data.Count() > 0)
                        {
                            foreach (var item in data)
                            {
                                tPISEmpSanctionpost Reply = new tPISEmpSanctionpost();
                                Reply.Id = item.Id;
                                Reply.DepartmentName = item.deptname;
                                Reply.DesignationName = item.designame;
                                Reply.OfficeName = item.officename;
                                Reply.sanctpost = item.sanctpost;
                                Reply.Filledpost = item.Filledpost;
                                //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                                result.Add(Reply);

                            }
                        }
                        var sanction = (from md in db.tPISEmpSanctionpost where md.Id == model.Id select md).FirstOrDefault();
                        if (sanction.POfficeId == model.POfficeId && sanction.PDesignationId == model.PDesignationId)
                        {
                            result = null;
                        }

                    }
                    else
                    {
                        foreach (var id in model.AllDesignationIds)
                        {
                            Int32 desigid = Convert.ToInt32(id);

                            var data = (from md in db.tPISEmpSanctionpost
                                        join dep in db.mDepartment on md.Deptid equals dep.deptId
                                        join des in db.mPISDesignation on desigid equals des.Id
                                        join off in db.mOffices on md.POfficeId equals off.OfficeId
                                        orderby md.Id descending
                                        where md.Deptid == model.Deptid && md.PDesignationId == desigid && md.POfficeId == model.POfficeId
                                        select new
                                        {
                                            md.Id,
                                            dep.deptname,
                                            des.designame,
                                            off.officename,
                                            md.sanctpost,
                                            md.Filledpost

                                        }).ToList();


                            if (data != null && data.Count() > 0)
                            {
                                foreach (var item in data)
                                {
                                    tPISEmpSanctionpost Reply = new tPISEmpSanctionpost();
                                    Reply.Id = item.Id;
                                    Reply.DepartmentName = item.deptname;
                                    Reply.DesignationName = item.designame;
                                    Reply.OfficeName = item.officename;
                                    Reply.sanctpost = item.sanctpost;
                                    Reply.Filledpost = item.Filledpost;
                                    //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                                    result.Add(Reply);

                                }
                            }

                        }
                    }
                    return result;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetAllSenctionPost(object param)
        {

            try
            {

                // string Deptid = param as string;
                tPISEmpSanctionpost smodel = param as tPISEmpSanctionpost;
                string SubID = smodel.SubId; 
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.tPISEmpSanctionpost
                                join dep in db.mDepartment on md.Deptid equals dep.deptId
                                join des in db.mPISDesignation on md.PDesignationId equals des.Id
                                join off in db.mOffices on md.POfficeId equals off.OfficeId
                                where md.Deptid == smodel.Deptid && md.POfficeId == smodel.POfficeId
                                orderby md.Id descending
                                select new
                                {
                                    md.Id,
                                    dep.deptname,
                                    des.designame,
                                    off.officename,
                                    md.sanctpost,
                                    md.Filledpost,
                                    md.Priority

                                }).ToList();

                    List<tPISEmpSanctionpost> result = new List<tPISEmpSanctionpost>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            tPISEmpSanctionpost Reply = new tPISEmpSanctionpost();
                            Reply.Id = item.Id;
                            Reply.DepartmentName = item.deptname;
                            Reply.DesignationName = item.designame;
                            Reply.OfficeName = item.officename;
                            Reply.sanctpost = item.sanctpost;
                            Reply.Filledpost = item.Filledpost;
                            Reply.Priority = item.Priority;
                            //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                            result.Add(Reply);

                        }
                    }

                    return result.OrderBy(p => p.OfficeName).ThenBy(p => p.Priority).ThenBy(p => p.DesignationName).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetPostByOfficeId(object param)
        {

            try
            {

                // string Deptid = param as string;
                tPISEmpSanctionpost smodel = param as tPISEmpSanctionpost;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.tPISEmpSanctionpost
                                join dep in db.mDepartment on md.Deptid equals dep.deptId
                                join des in db.mPISDesignation on md.PDesignationId equals des.Id
                                join off in db.mOffices on md.POfficeId equals off.OfficeId
                                where md.Deptid == smodel.Deptid && md.POfficeId==smodel.POfficeId
                                orderby md.Id descending
                                select new
                                {
                                    md.Id,
                                    dep.deptname,
                                    des.designame,
                                    off.officename,
                                    md.sanctpost,
                                    md.Filledpost,
                                    md.Priority

                                }).ToList();

                    List<tPISEmpSanctionpost> result = new List<tPISEmpSanctionpost>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            tPISEmpSanctionpost Reply = new tPISEmpSanctionpost();
                            Reply.Id = item.Id;
                            Reply.DepartmentName = item.deptname;
                            Reply.DesignationName = item.designame;
                            Reply.OfficeName = item.officename;
                            Reply.sanctpost = item.sanctpost;
                            Reply.Filledpost = item.Filledpost;
                            Reply.Priority = item.Priority;
                            //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                            result.Add(Reply);

                        }
                    }

                    return result.OrderBy(p => p.OfficeName).ThenBy(p => p.Priority).ThenBy(p => p.DesignationName).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        static string GetDesigCodeById(object param)
        {
            mPISDesignation ToUpdate = param as mPISDesignation;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {

                    bool ss = _context.mPISDesignation.Any();
                    if (ss)
                    {
                        var data = (from md in _context.mPISDesignation
                                    where md.Id == ToUpdate.Id
                                    select md.desigcode).SingleOrDefault();
                        return data;
                    }
                    else
                    {
                        return "0";
                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            //return data;
        }
        public static object CreateSantionPost(object param)
        {
            try
            {
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    model.officeid = (from sp in db.mOffices where sp.OfficeId == model.POfficeId select sp.OfficeCode).FirstOrDefault().ToString();

                    db.tPISEmpSanctionpost.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
        public static object GetSantionpostForm(object param)
        {

            try
            {
                string AadharId = param as string;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.tPISEmpSanctionpost
                                join dep in db.mDepartment on md.Deptid equals dep.deptId
                                join des in db.mPISDesignation on md.PDesignationId equals des.Id
                                join off in db.mOffices on md.POfficeId equals off.OfficeId
                                orderby md.Id descending
                                select new
                                {
                                    md.Id,
                                    dep.deptname,
                                    des.designame,
                                    off.officename,
                                    md.sanctpost,
                                    md.Filledpost

                                }).ToList();




                    List<tPISEmpSanctionpost> result = new List<tPISEmpSanctionpost>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            tPISEmpSanctionpost Reply = new tPISEmpSanctionpost();
                            Reply.Id = item.Id;
                            Reply.DepartmentName = item.deptname;
                            Reply.DesignationName = item.designame;
                            Reply.OfficeName = item.officename;
                            Reply.sanctpost = item.sanctpost;
                            Reply.Filledpost = item.Filledpost;
                            //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                            result.Add(Reply);

                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetSanctionPostListBiID(object param)
        {

            try
            {
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.tPISEmpSanctionpost
                                join dep in db.mDepartment on md.Deptid equals dep.deptId
                                join des in db.mPISDesignation on md.PDesignationId equals des.Id
                                join off in db.mOffices on md.POfficeId equals off.OfficeId
                                where md.Id == model.Id
                                orderby md.Id descending
                                select new
                                {
                                    md.Id,
                                    md.officeid,
                                    md.Deptid,
                                    md.POfficeId,
                                    md.PDesignationId,
                                    md.desigcd,
                                    md.sanctpost,
                                    md.Filledpost,
                                    dep.deptname,
                                    des.designame,
                                    md.Priority,
                                    off.officename

                                }).ToList();

                    tPISEmpSanctionpost result = new tPISEmpSanctionpost();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {

                            result.Id = item.Id;
                            result.DepartmentName = item.deptname;
                            result.DesignationName = item.designame;
                            result.OfficeName = item.officename;
                            result.Deptid = item.Deptid;
                            result.desigcd = item.desigcd;
                            result.POfficeId = item.POfficeId;
                            result.PDesignationId = item.PDesignationId;
                            result.officeid = item.officeid;
                            result.sanctpost = item.sanctpost;
                            result.Filledpost = item.Filledpost;
                            result.Priority = item.Priority;
                        }
                    }
                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        static object UpdateSanctionPost(object param)
        {
            tPISEmpSanctionpost ToUpdate = param as tPISEmpSanctionpost;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {
                    _context.tPISEmpSanctionpost.Attach(ToUpdate);
                    _context.Entry(ToUpdate).State = EntityState.Modified;
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ToUpdate;
        }
        #endregion
        #region SanctionPost
        static object GetSanctionPostList(object param)
        {
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {
                    tPISEmpSanctionpost model = param as tPISEmpSanctionpost;

                    var result = (from sanction in _context.tPISEmpSanctionpost
                                  orderby sanction.Id descending
                                  select sanction).ToList();

                    return result;

                }

            }
            catch (Exception ee)
            {
                throw ee;
            }
            return null;
        }
        #endregion
        #region PISEmployee

        public static object GelAllRelievedEmpList(object param)
        {
            try
            {

                DateTime toddate = DateTime.Today.Date;
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                using (PISDesignationContext db = new PISDesignationContext())
                {


                    var data = (from md in db.tPISEmployeePersonal
                                where md.dofretirecurr <= toddate && md.deptid == model.Deptid && md.POfficeId==model.POfficeId
                                orderby md.ID descending
                                select md).ToList();

                    List<tPISEmployeePersonal> result = new List<tPISEmployeePersonal>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            tPISEmployeePersonal Reply = new tPISEmployeePersonal();
                            Reply.ID = item.ID;
                            Reply.AadharID = item.AadharID;
                            Reply.DepartmentName = (from dep in db.mDepartment where dep.deptId == item.deptid.Trim() select dep.deptname).SingleOrDefault();
                            Reply.DesignationName = (from des in db.mPISDesignation where des.Id == item.PDesignationId select des.designame).SingleOrDefault();
                            Reply.OfficeName = (from off in db.mOffices where off.OfficeId == item.POfficeId select off.officename).SingleOrDefault();
                            Reply.empfname = item.empfname;
                            Reply.empmname = item.empfname;
                            Reply.emplname = item.emplname;
                            Reply.empfmh = item.empfmh;


                            Reply.empphoto = item.empphoto;
                            Reply.dofjoin = item.dofjoin;
                            Reply.dofretirecurr = item.dofretirecurr;
                            Reply.empgender = item.empgender;
                            Reply.PDesignationId = item.PDesignationId;
                            Reply.officeid = item.officeid;
                            //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                            result.Add(Reply);

                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static object GelAllEmpListById(object param)
        {
            try
            {
                tPISEmployeePersonal model = param as tPISEmployeePersonal;
                DateTime toddate = DateTime.Today.Date;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var mydata = (from md in db.tPISEmpSanctionpost
                                  where md.Id == model.ID
                                  select md).FirstOrDefault();

                    var data = (from md in db.tPISEmployeePersonal
                                where (md.dofretirecurr == null || md.dofretirecurr >= toddate) && md.POfficeId == mydata.POfficeId && md.PDesignationId == mydata.PDesignationId && md.deptid == mydata.Deptid
                                orderby md.ID descending
                                select md).ToList();

                    List<tPISEmployeePersonal> result = new List<tPISEmployeePersonal>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            tPISEmployeePersonal Reply = new tPISEmployeePersonal();
                            Reply.ID = item.ID;
                            Reply.AadharID = item.AadharID;
                            Reply.DepartmentName = (from dep in db.mDepartment where dep.deptId == item.deptid.Trim() select dep.deptname).SingleOrDefault();
                            Reply.DesignationName = (from des in db.mPISDesignation where des.Id == item.PDesignationId select des.designame).SingleOrDefault();
                            Reply.OfficeName = (from off in db.mOffices where off.OfficeId == item.POfficeId select off.officename).SingleOrDefault();
                            Reply.empfname = item.empfname;
                            Reply.empmname = item.empfname;
                            Reply.emplname = item.emplname;
                            Reply.empfmh = item.empfmh;


                            Reply.empphoto = item.empphoto;
                            Reply.dofjoin = item.dofjoin;
                            Reply.dofretirecurr = item.dofretirecurr;
                            Reply.empgender = item.empgender;
                            Reply.PDesignationId = item.PDesignationId;
                            Reply.officeid = item.officeid;
                            //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                            result.Add(Reply);

                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GelAllEmpList(object param)
        {
            try
            {
                DateTime toddate = DateTime.Today.Date;
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.tPISEmployeePersonal
                                where (md.dofretirecurr == null || md.dofretirecurr >= toddate) && md.deptid == model.Deptid && md.POfficeId ==model.POfficeId
                                orderby md.ID descending
                                select md).ToList();

                    List<tPISEmployeePersonal> result = new List<tPISEmployeePersonal>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            tPISEmployeePersonal Reply = new tPISEmployeePersonal();
                            Reply.ID = item.ID;
                            Reply.AadharID = item.AadharID;
                            Reply.DepartmentName = (from dep in db.mDepartment where dep.deptId == item.deptid.Trim() select dep.deptname).SingleOrDefault();
                            Reply.DesignationName = (from des in db.mPISDesignation where des.Id == item.PDesignationId select des.designame).SingleOrDefault();
                            Reply.OfficeName = (from off in db.mOffices where off.OfficeId == item.POfficeId select off.officename).SingleOrDefault();
                            Reply.empfname = item.empfname;
                            Reply.empmname = item.empfname;
                            Reply.emplname = item.emplname;
                            Reply.empfmh = item.empfmh;


                            Reply.empphoto = item.empphoto;
                            Reply.dofjoin = item.dofjoin;
                            Reply.dofretirecurr = item.dofretirecurr;
                            Reply.empgender = item.empgender;
                            Reply.PDesignationId = item.PDesignationId;
                            Reply.officeid = item.officeid;
                            //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                            result.Add(Reply);

                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GelEmpListByOfficeId(object param)
        {
            try
            {
                DateTime toddate = DateTime.Today.Date;
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.tPISEmployeePersonal
                                where md.IsRelieved!=true && md.deptid == model.Deptid && model.POfficeId==md.POfficeId
                                orderby md.ID descending
                                select md).ToList();

                    List<tPISEmployeePersonal> result = new List<tPISEmployeePersonal>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            tPISEmployeePersonal Reply = new tPISEmployeePersonal();
                            Reply.ID = item.ID;
                            Reply.AadharID = item.AadharID;
                            Reply.DepartmentName = (from dep in db.mDepartment where dep.deptId == item.deptid.Trim() select dep.deptname).SingleOrDefault();
                            Reply.DesignationName = (from des in db.mPISDesignation where des.Id == item.PDesignationId select des.designame).SingleOrDefault();
                            Reply.OfficeName = (from off in db.mOffices where off.OfficeId == item.POfficeId select off.officename).SingleOrDefault();
                            Reply.empfname = item.empfname;
                            Reply.empmname = item.empfname;
                            Reply.emplname = item.emplname;
                            Reply.empfmh = item.empfmh;


                            Reply.empphoto = item.empphoto;
                            Reply.dofjoin = item.dofjoin;
                            Reply.dofretirecurr = item.dofretirecurr;
                            Reply.empgender = item.empgender;
                            Reply.PDesignationId = item.PDesignationId;
                            Reply.officeid = item.officeid;
                            //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                            result.Add(Reply);

                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object CreateEmployee(object param)
        {
            try
            {
                tPISEmployeePersonal model = param as tPISEmployeePersonal;
                using (PISDesignationContext db = new PISDesignationContext())
                {


                    db.tPISEmployeePersonal.Add(model);
                    db.SaveChanges();
                    Int64 id = model.ID;

                    var data = (from md in db.tPISEmployeePersonal
                                where md.ID == id
                                select md).SingleOrDefault();

                    db.Close();
                    return data;

                }

                // return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
        public static object GetEmpInfoById(object param)
        {
            try
            {
                tPISEmployeePersonal model = param as tPISEmployeePersonal;
                using (PISDesignationContext db = new PISDesignationContext())
                {



                    var data = (from md in db.tPISEmployeePersonal
                                where md.ID == model.ID
                                select md).SingleOrDefault();

                    db.Close();
                    return data;

                }

                // return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
        public static object CheckAdharIsExist(object param)
        {
            try
            {
                tPISEmployeePersonal model = param as tPISEmployeePersonal;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    DateTime toddate = DateTime.Today.Date;


                    var data = (from md in db.tPISEmployeePersonal

                                where md.AadharID == model.AadharID && md.IsRelieved!=true
                                select md).OrderByDescending(s=>s.ID).FirstOrDefault();

                    db.Close();
                    return data;

                }

                // return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        static object UpdateEmployee(object param)
        {
            tPISEmployeePersonal ToUpdate = param as tPISEmployeePersonal;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {
                    var Rowffid = (from ds in _context.mOffices where ds.OfficeId == ToUpdate.POfficeId select ds.OfficeCode).SingleOrDefault();
                    tPISEmployeePersonal obj = _context.tPISEmployeePersonal.Single(m => m.ID == ToUpdate.ID);
                    obj.officeid = Rowffid.ToString();
                    obj.POfficeId = ToUpdate.POfficeId;
                    obj.deptid = ToUpdate.deptid;
                    obj.currdesig = ToUpdate.currdesig;
                    obj.PDesignationId = ToUpdate.PDesignationId;
                    obj.dofjoin = ToUpdate.dofjoin;
                    obj.dofretirecurr = ToUpdate.dofretirecurr;
                    obj.empfname = ToUpdate.empfname;
                    obj.empdob = ToUpdate.empdob;
                    obj.empgender = ToUpdate.empgender;
                 
                    _context.SaveChanges();
                    _context.Close();


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ToUpdate;
        }
        static object UpdateEmployeeByPopup(object param)
        {
            tPISEmployeePersonal ToUpdate = param as tPISEmployeePersonal;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {

                    tPISEmployeePersonal obj = _context.tPISEmployeePersonal.Single(m => m.ID == ToUpdate.ID);
                    // obj.officeid = ToUpdate.officeid;
                    // obj.POfficeId = ToUpdate.POfficeId;
                    // obj.deptid = ToUpdate.deptid;
                    // obj.currdesig = ToUpdate.currdesig;
                    // obj.dofjoin = ToUpdate.dofjoin;
                    // obj.dofretirecurr = ToUpdate.dofretirecurr;
                    obj.empfname = ToUpdate.empfname;
                    obj.empfmh = ToUpdate.empfmh;
                    //obj.empdob = ToUpdate.empdob;
                    obj.empgender = ToUpdate.empgender;
                    obj.Address = ToUpdate.Address;
                    obj.PinCode = ToUpdate.PinCode;
                    obj.empphoto = ToUpdate.empphoto;
                    obj.Mobile = ToUpdate.Mobile;
                    obj.Email = ToUpdate.Email;
                    obj.emphmdist = ToUpdate.emphmdist;
                    obj.SalaryCode = ToUpdate.SalaryCode;
                    obj.PISCode = ToUpdate.PISCode;
                    obj.desigdescription = ToUpdate.desigdescription;
                    _context.SaveChanges();
                    _context.Close();


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ToUpdate;
        }
        static Int64 GetEmpCount(object param)
        {
            //tPISEmployeePersonal ToUpdate = param as tPISEmployeePersonal;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {

                    bool ss = _context.tPISEmployeePersonal.Any();
                    if (ss)
                    {
                        var data = (from md in _context.tPISEmployeePersonal
                                    select md.ID).Max();
                        return data;
                    }
                    else
                    {
                        return 0;
                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            //return data;
        }

        //static object DeleteEmployee(object param)
        //{
        //    tPISEmployeePersonal ToUpdate = param as tPISEmployeePersonal;
        //    try
        //    {
        //        using (PISDesignationContext _context = new PISDesignationContext())
        //        {

        //            tPISEmployeePersonal obj = _context.tPISEmployeePersonal.Single(m => m.ID == ToUpdate.ID);

        //            var oldData = (from md in _context.tPISEmpSanctionpost
        //                           where md.Deptid == obj.deptid && md.PDesignationId == obj.PDesignationId && md.POfficeId == obj.POfficeId
        //                           select md).SingleOrDefault();
        //            oldData.Filledpost = oldData.Filledpost - 1;
        //            //_context.SaveChanges();
        //            _context.tPISEmployeePersonal.Remove(obj);
        //            _context.SaveChanges();
        //            _context.Close();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return ToUpdate;
        //}
        static object DeleteEmployee(object param)
        {
       
            tPISEmployeePersonal ToUpdate = param as tPISEmployeePersonal;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {
                    Emppersonal_Log objlog = new Emppersonal_Log();
                    tPISEmployeePersonal obj = _context.tPISEmployeePersonal.Single(m => m.ID == ToUpdate.ID);
                    objlog.ID = obj.ID;
                    objlog.transid = obj.transid;
                    objlog.AadharID = obj.AadharID;
                    objlog.empcd = obj.empcd;
                    objlog.officeid = obj.officeid;
                    objlog.empfname = obj.empfname;
                    objlog.empmname = obj.empmname;
                    objlog.emplname = obj.emplname;
                    objlog.empnamelocal = obj.empnamelocal;
                    objlog.empdob = obj.empdob;
                    objlog.empfmh = obj.empfmh;
                    objlog.empfmhname = obj.empfmhname;
                    objlog.empmarst = obj.empmarst;
                    objlog.empspouse = obj.empspouse;
                    objlog.empidmasrk = obj.empidmasrk;
                    objlog.empcaste = obj.empcaste;
                    objlog.empreligion = obj.empreligion;
                    objlog.empcategory = obj.empcategory;
                    objlog.emphmst = obj.emphmst;
                    objlog.emphmdist = obj.emphmdist;
                    objlog.emphmtown = obj.emphmtown;
                    objlog.esalarycd = obj.esalarycd;
                    objlog.emphomeoff = obj.emphomeoff;
                    objlog.empmedcer = obj.empmedcer;
                    objlog.empcharcer = obj.empcharcer;
                    objlog.empphoto = obj.empphoto;
                    objlog.empfinger = obj.empfinger;
                    objlog.emprail = obj.emprail;
                    objlog.empremark = obj.empremark;
                    objlog.empgender = obj.empgender;
                    objlog.empblood = obj.empblood;
                    objlog.dofvalid = obj.dofvalid;
                    objlog.empheight = obj.empheight;
                    objlog.currdesig = obj.currdesig;
                    objlog.nextincrement = obj.nextincrement;

                    objlog.doeoffdesigincre = obj.doeoffdesigincre;
                    objlog.cadre_cd = obj.cadre_cd;
                    objlog.deptid = obj.deptid;
                    objlog.DEuserid = obj.DEuserid;
                    objlog.VALIDuserid = obj.VALIDuserid;
                    objlog.verify = obj.verify;
                    objlog.branchcd = obj.branchcd;
                    objlog.postdeptid = obj.postdeptid;
                    objlog.desigdescription = obj.desigdescription;

                    objlog.estabdeptid = obj.estabdeptid;
                    objlog.estabofficeid = obj.estabofficeid;
                    objlog.dofjoin = obj.dofjoin;
                    objlog.dofretirecurr = obj.dofretirecurr;
                    objlog.modeofreccurr = obj.modeofreccurr;
                    objlog.emptypecurr = obj.emptypecurr;
                    objlog.POfficeId = obj.POfficeId;

                    objlog.Email = obj.Email;
                    objlog.Address = obj.Address;
                    objlog.Mobile = obj.Mobile;
                    objlog.PinCode = obj.PinCode;
                    objlog.PDesignationId = obj.PDesignationId;
                    objlog.DateOFBirth = obj.DateOFBirth;
                    objlog.PISCode = obj.PISCode;
                    objlog.SalaryCode = obj.SalaryCode;
                    _context.Emppersonal_Log.Add(objlog);
                    var oldData = (from md in _context.tPISEmpSanctionpost
                                   where md.Deptid == obj.deptid && md.PDesignationId == obj.PDesignationId && md.POfficeId == obj.POfficeId
                                   select md).SingleOrDefault();
                    oldData.Filledpost = oldData.Filledpost - 1;

                    _context.tPISEmployeePersonal.Remove(obj);

                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ToUpdate;
        }

        public static object CheckSanctionPost(object param)
        {

            try
            {
                tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
                bool IsSuccess = false;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    // Int32 desigid = Convert.ToInt32(model.PDesignationId);
                    //model.POfficeId = Convert.ToInt32(model.officeid);
                    var data = (from md in db.tPISEmpSanctionpost
                                where md.Id == model.Id
                                select md).SingleOrDefault();
                    if (data != null)
                    {
                        if (data.Filledpost > model.sanctpost)
                        {
                            IsSuccess = false;
                        }
                        else
                        {
                            IsSuccess = true;
                        }
                    }
                    return IsSuccess;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object CheckFilledPost(object param)
        {

            try
            {
                List<tPISEmpSanctionpost> result = new List<tPISEmpSanctionpost>();
                tPISEmployeePersonal model = param as tPISEmployeePersonal;
                //PISDesignationContext ts = new PISDesignationContext();
                //  bool IsSuccess = false;
                // string msg = "";
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    Int32 desigid = Convert.ToInt32(model.PDesignationId);
                    model.POfficeId = Convert.ToInt32(model.POfficeId);

                    var emp = (from md in db.tPISEmployeePersonal
                               where md.ID == model.ID
                               select md).SingleOrDefault();
                    var data = (from md in db.tPISEmpSanctionpost
                                where md.Deptid == model.deptid && md.PDesignationId == desigid && md.POfficeId == model.POfficeId
                                select md).SingleOrDefault();

                    var oldData = (from md in db.tPISEmpSanctionpost
                                   where md.Deptid == emp.deptid && md.PDesignationId == emp.PDesignationId && md.POfficeId == emp.POfficeId
                                   select md).SingleOrDefault();
                    if (data == null)
                    {


                        result = PISDesignationContext.getWarningData(model);
                        // msg = "Selected sanctioned post is not available";
                    }
                    ///case1 office or designation not changed

                    else if (model.deptid == emp.deptid && model.POfficeId == emp.POfficeId && model.PDesignationId == emp.PDesignationId)
                    {
                        if (emp.POfficeId != null && emp.PDesignationId != null)
                        {
                            //msg = "true";
                            result = null;
                        }
                        else if ((data.sanctpost > data.Filledpost) || (data.Filledpost == null && data.sanctpost >= 1))
                        {
                            if (data.Filledpost == null || data.Filledpost == 0)
                            {
                                data.Filledpost = 1;
                                db.SaveChanges();
                                //msg = "true";
                                result = null;
                            }
                            else
                            {
                                // msg = "true";
                                result = null;
                            }
                        }
                        else
                        {
                            //msg = "Sanctioned post is not available";
                            result = PISDesignationContext.getWarningData(model);
                        }
                    }
                    ///case2 change office or designation 
                    else if (model.deptid != emp.deptid || model.POfficeId != emp.POfficeId || model.PDesignationId != emp.PDesignationId)
                    {
                        if ((data.sanctpost > data.Filledpost) || (data.Filledpost == null && data.sanctpost >= 1))
                        {
                            if (data.Filledpost == null || data.Filledpost == 0)
                            {
                                data.Filledpost = 1;
                                db.SaveChanges();
                                // msg = "true";
                                result = null;
                                if (oldData != null)
                                {
                                    if (oldData.PDesignationId != null)
                                    {
                                        oldData.Filledpost = oldData.Filledpost - 1;
                                        db.SaveChanges();
                                        //msg = "true";
                                        result = null;
                                    }
                                    else
                                    {
                                        oldData.Filledpost = 1;
                                        db.SaveChanges();
                                        // msg = "true";
                                        result = null;
                                    }
                                }

                            }
                            else
                            {
                                data.Filledpost = data.Filledpost + 1;
                                db.SaveChanges();
                                // msg = "true";
                                result = null;
                                if (oldData != null)
                                {
                                    if (oldData.PDesignationId != null)
                                    {
                                        oldData.Filledpost = oldData.Filledpost - 1;
                                        db.SaveChanges();
                                        //msg = "true";
                                        result = null;
                                    }
                                    else
                                    {
                                        oldData.Filledpost = 1;
                                        db.SaveChanges();
                                        //msg = "true";
                                        result = null;
                                    }
                                }
                            }
                        }
                        else
                        {
                            result = PISDesignationContext.getWarningData(model);
                            // msg = "Sanctioned post is not available";
                        }
                    }


                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static List<tPISEmpSanctionpost> getWarningData(tPISEmployeePersonal model)
        {
            //tPISEmpSanctionpost model = param as tPISEmpSanctionpost;
            List<tPISEmpSanctionpost> result = new List<tPISEmpSanctionpost>();
            using (PISDesignationContext db = new PISDesignationContext())
            {
                var returnMdl = (from md in db.tPISEmpSanctionpost
                                 join dep in db.mDepartment on md.Deptid equals dep.deptId
                                 join des in db.mPISDesignation on model.PDesignationId equals des.Id
                                 join off in db.mOffices on md.POfficeId equals off.OfficeId
                                 orderby md.Id descending
                                 where md.Deptid == model.deptid && md.PDesignationId == model.PDesignationId && md.POfficeId == model.POfficeId
                                 select new
                                 {
                                     md.Id,
                                     dep.deptname,
                                     des.designame,
                                     off.officename,
                                     md.sanctpost,
                                     md.Filledpost

                                 }).ToList();


                if (returnMdl != null && returnMdl.Count() > 0)
                {
                    foreach (var item in returnMdl)
                    {
                        tPISEmpSanctionpost Reply = new tPISEmpSanctionpost();
                        Reply.Id = item.Id;
                        Reply.DepartmentName = item.deptname;
                        Reply.DesignationName = item.designame;
                        Reply.OfficeName = item.officename;
                        Reply.sanctpost = item.sanctpost;
                        Reply.Filledpost = item.Filledpost;
                        //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                        result.Add(Reply);

                    }
                }
                return result;
            }
        }
        public static object GetAllDistrict()
        {
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {

                    var query = (from a in _context.DistrictModel
                                 orderby a.DistrictName ascending
                                 where a.IsDeleted == null
                                 select a).ToList();
                    return query;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion


        //
        public static object GetAllSanFillPostsByOfficeId(object param)
        {

            try
            {

                // string Deptid = param as string;
                tPISEmpSanctionpost smodel = param as tPISEmpSanctionpost;
                string POfficeId = Convert.ToString(smodel.POfficeId);
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.SanFillPostPdf
                                where md.officeid == POfficeId
                                orderby md.Id descending
                                select new
                                {
                                    md.Id,
                                    md.Description,
                                    md.officeid,
                                    md.PdfLocation,
                                    md.ModifiedDate,
                                    md.ModifiedBy,
                                    md.CreatedBy,
                                    md.CreatedDate,


                                }).ToList();

                    List<SanFillPostPdf> result = new List<SanFillPostPdf>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            SanFillPostPdf Reply = new SanFillPostPdf();
                            Reply.Id = item.Id;
                            Reply.CreatedDate = item.CreatedDate;
                            Reply.CreatedBy = item.CreatedBy;
                            Reply.ModifiedBy = item.ModifiedBy;
                            Reply.ModifiedDate = item.ModifiedDate;
                            Reply.Description = item.Description;
                            Reply.officeid = item.officeid;
                            Reply.PdfLocation = item.PdfLocation;
                            //Reply.OfficeName = (from off in db.mOffices where off.OfficeCode == item.officeid select off).SingleOrDefault();
                            result.Add(Reply);

                        }
                    }

                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object CreateSanFillPost(object param)
        {
            try
            {
                SanFillPostPdf model = param as SanFillPostPdf;
                using (PISDesignationContext db = new PISDesignationContext())
                {


                    db.SanFillPostPdf.Add(model);
                    db.SaveChanges();
                    string id = model.officeid;

                    var data = (from md in db.SanFillPostPdf
                                where md.officeid == id
                                select md).SingleOrDefault();

                    db.Close();
                    return data;

                }

                // return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
        static object UpdateSanFillPost(object param)
        {
            tPISEmpSanctionpost ToUpdate = param as tPISEmpSanctionpost;
            try
            {
                using (PISDesignationContext _context = new PISDesignationContext())
                {

                    SanFillPostPdf obj = _context.SanFillPostPdf.Single(m => m.Id == ToUpdate.Id);
                    //obj.officeid = (from sp in _context.mOffices where sp.OfficeId == ToUpdate.POfficeId select sp.OfficeCode).FirstOrDefault().ToString();
                    obj.officeid = Convert.ToString(ToUpdate.POfficeId);
                    obj.Description = ToUpdate.Description;
                    obj.PdfLocation = ToUpdate.FileLocation;
                    obj.ModifiedDate = DateTime.Now;
                    // obj.ModifiedBy =;
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ToUpdate;
        }
        public static object GetSanFillPostsByid(object param)
        {

            try
            {
                SanFillPostPdf model = param as SanFillPostPdf;
                using (PISDesignationContext db = new PISDesignationContext())
                {
                    var data = (from md in db.SanFillPostPdf
                                where md.Id == model.Id
                                orderby md.Id descending

                                select md).SingleOrDefault();


                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
