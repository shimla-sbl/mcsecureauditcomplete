﻿using SBL.DAL;
using SBL.DomainModel.Models.PISMolues;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.PISModule
{
    public class PISSanctionedPostContext : DBBase<PISSanctionedPostContext>
    {
        public PISSanctionedPostContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<tPISEmpSanctionpost> SanctionPost { get; set; }


        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetSanctionPostList":
                    {
                        return GetSanctionPostList(param.Parameter);
                    }
            }
            return null;
        }

        static object GetSanctionPostList(object param)
        {
            try
            {
                using (PISSanctionedPostContext _context = new PISSanctionedPostContext())
                {
                    tPISEmpSanctionpost model = param as tPISEmpSanctionpost;

                    var result = (from sanction in _context.SanctionPost
                                  orderby sanction.Id descending
                                  select sanction).ToList();

                    return result;
                  
                }

            }
            catch (Exception ee)
            {
                throw ee;
            }
            return null;
        }

    }
}
