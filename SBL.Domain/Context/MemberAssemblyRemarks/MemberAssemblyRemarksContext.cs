﻿using SBL.DAL;
using SBL.DomainModel.Models.MemberAssemblyRemarks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.MemberAssemblyRemarks
{
  public  class MemberAssemblyRemarksContext:DBBase<MemberAssemblyRemarksContext>
    {
      public MemberAssemblyRemarksContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
      public virtual DbSet<mMemberAssemblyRemarks> mMemberAssemblyRemarks { get; set; }

       public static object Execute(SBL.Service.Common.ServiceParameter param)
       {
           if (null == param)
           {
               return null;
           }

           switch (param.Method)
           {
               case "CreateMemberAssemblyRemarks": { CreateMemberAssemblyRemarks(param.Parameter); break; }
               case "GetAllMemberAssemblyRemarks": { return GetAllMemberAssemblyRemarks(); }
               case "UpdateMemberAsemblyRemarks": { return UpdateMemberAsemblyRemarks(param.Parameter); }
               case "GetMemberAssemblyRemarksBasedOnId": { return GetMemberAssemblyRemarksBasedOnId(param.Parameter); }
               case "DeleteMemberAssemblyRemarks": { return DeleteMemberAssemblyRemarks(param.Parameter); }

           }
           return null;
       }

       static void CreateMemberAssemblyRemarks(object param)
       {
           try
           {
               using (MemberAssemblyRemarksContext db = new MemberAssemblyRemarksContext())
               {
                   mMemberAssemblyRemarks model = param as mMemberAssemblyRemarks;
                   model.CreatedDate = DateTime.Now;
                   model.ModifiedDate = DateTime.Now;
                   db.mMemberAssemblyRemarks.Add(model);
                   db.SaveChanges();
                   db.Close();

               }

           }
           catch (Exception ex)
           {

               throw ex;
           }
       }

       static List<mMemberAssemblyRemarks> GetAllMemberAssemblyRemarks()
       {
           try
           {
               using (MemberAssemblyRemarksContext db = new MemberAssemblyRemarksContext())
               {
                   var Data = db.mMemberAssemblyRemarks.Where(m => m.IsDeleted == null).OrderByDescending(a => a.MemberAssemblyRemarksID).ToList();
                   return Data;
               }

           }
           catch (Exception ex)
           {

               throw ex;
           }
       }

       static object UpdateMemberAsemblyRemarks(object param)
       {
           try
           {
               using (MemberAssemblyRemarksContext db = new MemberAssemblyRemarksContext())
               {
                   mMemberAssemblyRemarks model = param as mMemberAssemblyRemarks;
                   model.CreatedDate = DateTime.Now;
                   model.ModifiedDate = DateTime.Now;
                   db.mMemberAssemblyRemarks.Attach(model);
                   db.Entry(model).State = EntityState.Modified;
                   db.SaveChanges();
                   db.Close();

               }
               return GetAllMemberAssemblyRemarks();
           }
           catch (Exception ex)
           {

               throw ex;
           }
       }

       static mMemberAssemblyRemarks GetMemberAssemblyRemarksBasedOnId(object param)
       {
           try
           {
               using (MemberAssemblyRemarksContext db = new MemberAssemblyRemarksContext())
               {
                   mMemberAssemblyRemarks model = param as mMemberAssemblyRemarks;
                   var data = db.mMemberAssemblyRemarks.SingleOrDefault(a => a.MemberAssemblyRemarksID == model.MemberAssemblyRemarksID);
                   return data;
               }
           }
           catch (Exception ex)
           {

               throw ex;
           }
       }

       static object DeleteMemberAssemblyRemarks(object param)
       {
           try
           {
               using (MemberAssemblyRemarksContext db = new MemberAssemblyRemarksContext())
               {
                   mMemberAssemblyRemarks model = param as mMemberAssemblyRemarks;
                   mMemberAssemblyRemarks data = db.mMemberAssemblyRemarks.SingleOrDefault(a => a.MemberAssemblyRemarksID == model.MemberAssemblyRemarksID);
                   if(data!=null)
                   {
                       data.IsDeleted = true;
                   }
                  // db.mMemberAssemblyRemarks.Remove(data);
                   db.SaveChanges();
                   db.Close();


               }

               return GetAllMemberAssemblyRemarks();
           }
           catch (Exception ex)
           {

               throw ex;
           }
       }
    }
}
