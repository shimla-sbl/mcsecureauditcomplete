﻿



namespace SBL.Domain.Context.AlbumCategory
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SBL.DAL;
    using System.Data.Entity;
    using SBL.DomainModel.Models.Member;
    using SBL.Service.Common;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.ComplexModel;
    using System.Data;
    using SBL.DomainModel.Models.District;
    using SBL.DomainModel.Models.States;
    using SBL.DomainModel.Models.Party;
    using SBL.DomainModel.Models.AlbumCategory;
    using SBL.DomainModel.Models.Category;
    
    #endregion
    public class AlbumCategorycontext : DBBase<AlbumCategorycontext>
    {
        public AlbumCategorycontext()
            : base("eVidhan")
        
            { this.Configuration.ProxyCreationEnabled = false; }

        public virtual DbSet<AlbumCategory> tAlbumcategory { get; set; }
        public DbSet<Category> Category { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetAlbumCategory":
                    {
                        return GetAlbumCategory(param.Parameter);
                    }
                case "AddAlbumCategory":
                    {
                        return AddAlbumCategory(param.Parameter);
                    }
                case "EditAlbumCategory":
                    {
                        return EditAlbumCategory(param.Parameter);
                    }
                case "UpdateAlbumCategory":
                    {
                        return UpdateAlbumCategory(param.Parameter);
                    }
                case "DeleteAlbumCategory":
                    {
                        return DeleteAlbumCategory(param.Parameter);
                    }
                case "IsAlbumCategoryIdChildExist": { return IsAlbumCategoryIdChildExist(param.Parameter); }
            }
            return null;
        }

        private static object DeleteAlbumCategory(object param)
        {
            AlbumCategorycontext db = new AlbumCategorycontext();
            
            int Id = Convert.ToInt32(param);
           var Res=(from e in db.tAlbumcategory  where e.AlbumCategoryID==Id select e).SingleOrDefault();
           db.tAlbumcategory.Remove(Res);
           db.SaveChanges();
           return Res;


        }

        private static object UpdateAlbumCategory(object param)
        {
            AlbumCategorycontext db = new AlbumCategorycontext();
            AlbumCategory model = param as AlbumCategory;
            model.CreatedDate = DateTime.Now;
            model.ModifiedDate = DateTime.Now;
            db.tAlbumcategory.Attach(model);
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
            return model;
        }

        private static object EditAlbumCategory(object  param)
        {
            int Id = Convert.ToInt32(param);
            AlbumCategorycontext CommCtx = new AlbumCategorycontext();
            var Res = (from e in CommCtx.tAlbumcategory where e.AlbumCategoryID == Id select e).FirstOrDefault();
            CommCtx.Close();
            return Res;
        }

        private static object AddAlbumCategory(object param)
        {
            AlbumCategorycontext db = new AlbumCategorycontext();
            AlbumCategory model = param as AlbumCategory;
            model.CreatedDate = DateTime.Now;
            model.ModifiedDate = DateTime.Now;
            db.tAlbumcategory.Add(model);
            db.SaveChanges();
            return model;

        }

         static List<AlbumCategory> GetAlbumCategory(object param)
        {
            int Id = Convert.ToInt32(param);
            AlbumCategorycontext db = new AlbumCategorycontext();

            var Res = (from e in db.tAlbumcategory where e.MemberCode == Id select e).ToList();


            return Res;
        }









         private static object IsAlbumCategoryIdChildExist(object param)
         {
             try
             {
                 using (AlbumCategorycontext db = new AlbumCategorycontext())
                 {
                     AlbumCategory model = (AlbumCategory)param;
                     var Res = (from e in db.Category
                                where (e.AlbumCategoryId == model.AlbumCategoryID)
                                select e).Count();

                     if (Res == 0)
                     {
                        
                             return false;

                     }
                     else
                     {
                         return true;

                     }

                 }
                 return true;


             }


             catch (Exception ex)
             {

                 throw ex;
             }

         }






    }
}
