﻿using SBL.DAL;
using SBL.DomainModel.Models.ItemSubscription;
using SBL.DomainModel.Models.PressClipping;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.PressClipping
{
    public class PressClipContext : DBBase<PressClipContext>
    {
        public PressClipContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mPressClipping> mPressClipping { get; set; }
        public DbSet<mPressClippingsPDF> mPressClippingsPDF { get; set; }
        public virtual DbSet<mItemSubscription> ItemSubscription { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreatePressClip":
                    {
                        return CreatePressClip(param.Parameter);
                    }
                case "UpdatePressClip":
                    {
                        return UpdatePressClip(param.Parameter);
                    }
                case "DeletePressClip":
                    {
                        return DeletePressClip(param.Parameter);
                    }
                case "DeletePressClipPDF":
                    {
                        return DeletePressClipPDF(param.Parameter);
                    }
                case "GetAllPressClipDetails":
                    {
                        return GetAllPressClipDetails(param.Parameter);
                    }
                case "GetPressClipById":
                    {
                        return GetPressClipById(param.Parameter);
                    }
                case "SearchPressClipping":
                    {
                        return SearchPressClip(param.Parameter);
                    }
                case "GetOldPublishedPressClippingsDetails":
                    {
                        return GetOldPublishedPressClippingsDetails(param.Parameter);
                    }
                case "GetTodayPublishedPressClippingsDetails":
                    {
                        return GetTodayPublishedPressClippingsDetails(param.Parameter);
                    }

                case "GetAllPressClipDetailsPDF":
                    {
                        return GetAllPressClipDetailsPDF();
                    }
                case "CreatePressClipPDFInfo":
                    {
                        return CreatePressClipPDFInfo(param.Parameter);
                    }
                case "EditPressClipPDFInfo":
                    {
                        return EditPressClipPDFInfo(param.Parameter);
                    }
                case "CheckPressclipingPDFDate":
                    {
                        return CheckPressclipingPDFDate(param.Parameter);
                    }
                case "GetPressClipPDFById":
                    {
                        return GetPressClipPDFById(param.Parameter);
                    }
            }

            return null;
        }

        public static object CreatePressClip(object param)
        {
            try
            {
                int PressClipId = 0;
                using (PressClipContext db = new PressClipContext())
                {
                    mPressClipping model = param as mPressClipping;
                    model.CreationDate = DateTime.Today;
                    model.ModifiedWhen = DateTime.Today;
                    db.mPressClipping.Add(model);
                    db.SaveChanges();
                    db.Close();
                    PressClipId = model.PressClipID;
                }

                return PressClipId;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdatePressClip(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {
                    mPressClipping model = param as mPressClipping;
                    model.CreationDate = DateTime.Today;
                    model.ModifiedWhen = DateTime.Today;
                    db.mPressClipping.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeletePressClip(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {
                    mPressClipping parameter = param as mPressClipping;
                    mPressClipping stateToRemove = db.mPressClipping.SingleOrDefault(a => a.PressClipID == parameter.PressClipID);
                    db.mPressClipping.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllPressClipDetails(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from a in db.mPressClipping join ItemSub in db.ItemSubscription on a.NewsPaperId equals ItemSub.ItemSubscriptionID select new { PressClip = a, NewsPaperName = ItemSub.ItemName }).OrderByDescending(a => a.PressClip.NewsDate).ToList();//db.mNews.ToList();

                    List<mPressClipping> result = new List<mPressClipping>();

                    foreach (var a in data)
                    {
                        mPressClipping partdata = new mPressClipping();

                        partdata = a.PressClip;
                        partdata.NewsPaperName = a.NewsPaperName;

                        result.Add(partdata);
                    }
                    return result.Take(300).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //added by robin
        public static object GetOldPublishedPressClippingsDetails(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {

                    var data = (from a in db.mPressClipping
                                join ItemSub in db.ItemSubscription on a.NewsPaperId equals ItemSub.ItemSubscriptionID
                                where a.Status == true && a.CreationDate != DateTime.Today
                                select new
                                {
                                    PressClip = a,
                                    NewsPaperName = ItemSub.ItemName
                                }).OrderByDescending(a => a.PressClip.CreationDate).ToList();

                    List<mPressClipping> result = new List<mPressClipping>();

                    foreach (var a in data)
                    {
                        mPressClipping partdata = new mPressClipping();

                        partdata = a.PressClip;
                        partdata.NewsPaperName = a.NewsPaperName;

                        result.Add(partdata);
                    }

                    return result.Take(300).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetTodayPublishedPressClippingsDetails(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {

                    var data = (from a in db.mPressClipping
                                join ItemSub in db.ItemSubscription on a.NewsPaperId equals ItemSub.ItemSubscriptionID
                                where a.Status == true && a.CreationDate == DateTime.Today
                                select new
                                {
                                    PressClip = a,
                                    NewsPaperName = ItemSub.ItemName
                                }).OrderByDescending(a => a.PressClip.CreationDate).ToList();

                    List<mPressClipping> result = new List<mPressClipping>();

                    foreach (var a in data)
                    {
                        mPressClipping partdata = new mPressClipping();

                        partdata = a.PressClip;
                        partdata.NewsPaperName = a.NewsPaperName;

                        result.Add(partdata);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object SearchPressClip(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string NewsPaper = Result[0];
            string Subject = Result[1];
            string ContKey = Result[2];
            string NewsDateFrom = Result[3];
            string NewsDateTo = Result[4];
            string Category = Result[5];
            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            int NewsPaperId = 0;
            int.TryParse(NewsPaper, out NewsPaperId);

            DateTime.TryParse(NewsDateFrom, out FromDate);
            DateTime.TryParse(NewsDateTo, out ToDate);
            PressClipContext db = new PressClipContext();


            var SearchData = (from PC in db.mPressClipping
                              orderby PC.CreationDate
                              where PC.NewsPaperId == NewsPaperId || PC.Subject.Contains(Subject.Trim()) || PC.ContentKeywords.Contains(ContKey.Trim())
                        || (PC.NewsDate >= FromDate && PC.NewsDate <= ToDate)
                        || PC.Category.Contains(Category.Trim())
                              select PC).ToList();

            var ResultData = (from a in SearchData join ItemSub in db.ItemSubscription on a.NewsPaperId equals ItemSub.ItemSubscriptionID select new { PressClip = a, NewsPaperName = ItemSub.ItemName }).OrderByDescending(a => a.PressClip.NewsDate).ToList();//db.mNews.ToList();


            List<mPressClipping> result = new List<mPressClipping>();

            foreach (var a in ResultData)
            {
                mPressClipping partdata = new mPressClipping();

                partdata = a.PressClip;
                partdata.NewsPaperName = a.NewsPaperName;

                result.Add(partdata);
            }




            return result;
        }

        public static object GetPressClipById(object param)
        {
            mPressClipping parameter = param as mPressClipping;

            PressClipContext db = new PressClipContext();

            var query = db.mPressClipping.SingleOrDefault(a => a.PressClipID == parameter.PressClipID);

            return query;
        }

        public static object GetAllPressClipDetailsPDF()
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from a in db.mPressClippingsPDF
                                select a).ToList();

                    //List<mPressClippingsPDF> result = new List<mPressClippingsPDF>();

                    return data.Take(300).OrderByDescending(y =>y.CreationDate).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object GetPressClipPDFById(object param)
        {
            try
            {
                mPressClippingsPDF parameter = param as mPressClippingsPDF;

                PressClipContext db = new PressClipContext();

                var query = db.mPressClippingsPDF.FirstOrDefault(a => a.PressClipPDFID == parameter.PressClipPDFID);

                return query;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public static object CreatePressClipPDFInfo(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {
                    mPressClippingsPDF model = param as mPressClippingsPDF;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mPressClippingsPDF.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeletePressClipPDF(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {
                    mPressClippingsPDF parameter = param as mPressClippingsPDF;
                    mPressClippingsPDF stateToRemove = db.mPressClippingsPDF.SingleOrDefault(a => a.PressClipPDFID == parameter.PressClipPDFID);
                    db.mPressClippingsPDF.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object CheckPressclipingPDFDate(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {
                    mPressClippingsPDF parameter = param as mPressClippingsPDF;
                    //var check = (from a in db.mPressClippingsPDF
                    //             where a.NewsDate == parameter.NewsDate
                    //             select a).FirstOrDefault();
                    mPressClippingsPDF check = db.mPressClippingsPDF.FirstOrDefault(a => a.NewsDate == parameter.NewsDate);


                    return check;
                }
            }
            catch
            {
                throw;
            }
        }

        public static object EditPressClipPDFInfo(object param)
        {
            try
            {
                using (PressClipContext db = new PressClipContext())
                {
                    mPressClippingsPDF model = param as mPressClippingsPDF; 
                    db.mPressClippingsPDF.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return null;
            }
            catch
            {
                throw;
            }
        }
    }
}
