﻿using SBL.DAL;
using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.SessionType
{
    public class SessionTypeContext : DBBase<SessionTypeContext>
    {
        public SessionTypeContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public virtual DbSet<mSessionType> mSessionTypes { get; set; }
        public virtual DbSet<mSession> mSessions { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                #region SessionType

                case "CreateSessionType": { CreateSessionType(param.Parameter); break; }
                case "UpdateSessionType": { return UpdateSessionType(param.Parameter); }
                case "DeleteSessionType": { return DeleteSessionType(param.Parameter); }
                case "GetAllSessionType": { return GetAllSessionType(); }
                case "GetSessionTypeById": { return GetSessionTypeById(param.Parameter); }
                case "IsSessionTypeIdChildExist": { return IsSessionTypeIdChildExist(param.Parameter); }
                #endregion
            }
            return null;
        }

        #region For SessionType

        static void CreateSessionType(object param)
        {
            try
            {
                using (SessionTypeContext db = new SessionTypeContext())
                {
                    mSessionType model = param as mSessionType;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mSessionTypes.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateSessionType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SessionTypeContext db = new SessionTypeContext())
            {
                mSessionType model = param as mSessionType;
                model.CreatedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                db.mSessionTypes.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllSessionType();
        }

        static object DeleteSessionType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SessionTypeContext db = new SessionTypeContext())
            {
                mSessionType parameter = param as mSessionType;
                mSessionType typeToRemove = db.mSessionTypes.SingleOrDefault(a => a.TypeID == parameter.TypeID);
                if(typeToRemove!=null)
                {
                    typeToRemove.IsDeleted = true;
                }
                //db.mSessionTypes.Remove(typeToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllSessionType();
        }

        static List<mSessionType> GetAllSessionType()
        {
            SessionTypeContext db = new SessionTypeContext();
            //var query = db.mSessionTypes.ToList();
            var query = (from a in db.mSessionTypes
                         orderby a.TypeID descending
                         where a.IsDeleted == null
             select a).ToList();
            return query;

        }

        static mSessionType GetSessionTypeById(object param)
        {
            mSessionType parameter = param as mSessionType;
            SessionTypeContext db = new SessionTypeContext();
            var query = db.mSessionTypes.SingleOrDefault(a => a.TypeID == parameter.TypeID);
            return query;
        }

        private static object IsSessionTypeIdChildExist(object param)
       {
           try
           {
               using (SessionTypeContext db = new SessionTypeContext())
               {
                   mSessionType model = (mSessionType)param;
                   var Res = (from e in db.mSessions
                              where ( e.SessionType ==model.TypeID)
                              select e).Count();

                   if (Res == 0)
                   {
                       
                     return false;


                   }
                                         
                   else
                   {
                       return true;

                   }

               }
               //return null;


           }


           catch (Exception ex)
           {

               throw ex;
           }

       }
        #endregion


    }
}
