﻿namespace SBL.Domain.Context.Passes
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SBL.DAL;
    using System.Data.Entity;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Department;
    using System.Data;
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Secretory;
    using SBL.DomainModel.Models.Office;

    #endregion

    public class DepartmentPassesContext : DBBase<DepartmentPassesContext>
    {
        public DepartmentPassesContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mDepartmentPasses> mPasses { get; set; }
        public DbSet<PassCategory> PassCategory { get; set; }
        public DbSet<mDepartment> PassDepartment { get; set; }
        public DbSet<mSecretoryDepartment> SecretoryDepartment { get; set; }
        public DbSet<tDepartmentOfficeMapped> tDepartmentOfficeMapped { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "AddPassRequest":
                    {
                        return AddPassRequest(param.Parameter);
                    }
                case "GetPassCategories":
                    {
                        return GetPassCategories(param.Parameter);
                    }
                case "GetPassCategoriesForAdmin":
                    {
                        return GetPassCategoriesForAdmin(param.Parameter);
                    }
                    
                case "GetDepartmentPassesByDeptId":
                    {
                        return GetDepartmentPassesByDeptId(param.Parameter);
                    }
                case "GetDepartmentVehiclePassesByDeptId":
                    {
                        return GetDepartmentVehiclePassesByDeptId(param.Parameter);
                    }
                case "SubmitDepartmentPassRequest":
                    {
                        return SubmitDepartmentPassRequest(param.Parameter);
                    }
                case "UpdatePassRequest":
                    {
                        return UpdatePassRequest(param.Parameter);
                    }
                case "GetDepartmentPassesById":
                    {
                        return GetDepartmentPassesById(param.Parameter);
                    }
                case "GetDepartmentByids":
                    {
                        return GetDepartmentByids(param.Parameter);
                    }
                case "GetSecretatiatDepartments":
                    {
                        return GetSecretatiatDepartments(param.Parameter);
                    }
                case "GetPassCategoriesForVehicles": { return GetPassCategoriesForVehicles(param.Parameter); }
                case "GetDepartmentCategorys": { return GetDepartmentCategorys(param.Parameter); }
                case "GetMasterPassById": { return GetMasterPassById(param.Parameter); }

            }
            return null;
        }

        static object GetPassCategoriesForVehicles(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                var result = (from Passcategory in db.PassCategory orderby Passcategory.Name select Passcategory).ToList();
                result = result.Where(m => m.PassAllowsCreation == ((int)PassAllowsCreation.Vehicle).ToString()).ToList();
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object AddPassRequest(object param)
        {            
                using (var db = new DepartmentPassesContext())
                {
                    using (var dbContextTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {                            
                            mDepartmentPasses model = param as mDepartmentPasses;
                            model.Status = (int)PassStatus.Pending;
                            db.mPasses.Add(model);
                            var retVal = db.SaveChanges();
                            dbContextTransaction.Commit(); 
                            return retVal;
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                        }
                        finally
                        {
                            db.Close();
                            
                        }                       
                   
                    }
                }           
            return null;
        }

        static object GetMasterPassById(object param)
        {
            mDepartmentPasses masterPass = param as mDepartmentPasses;
            try
            {
                using (DepartmentPassesContext _context = new DepartmentPassesContext())
                {
                    return _context.mPasses.SingleOrDefault(a => a.PassID == masterPass.PassID);
                }
            }
            catch
            {
                throw;
            }
        }
        static object GetPassCategories(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                var result = (from Passcategory in db.PassCategory orderby Passcategory.Name select Passcategory).ToList();
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        static object GetPassCategoriesForAdmin(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                var result = (from Passcategory in db.PassCategory where Passcategory.PassCategoryID != 17 && Passcategory.PassCategoryID!=7 orderby Passcategory.Name select Passcategory).ToList();
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetDepartmentByids(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                mDepartmentPasses model = (mDepartmentPasses)param;
                string csv = model.DepartmentID;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                var result = (from mdl in db.PassDepartment where ids.Contains(mdl.deptId) select mdl).ToList();
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetDepartmentCategorys(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                //  mDepartment model = new mDepartment();

                var result = (from mdl in db.PassDepartment select mdl).ToList();
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetSecretatiatDepartments(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                mDepartmentPasses model = (mDepartmentPasses)param;
                string csv = model.DepartmentID;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                var result = (from mdl in db.PassDepartment
                              join mdlsec in db.SecretoryDepartment on mdl.deptId equals mdlsec.DepartmentID
                              select mdl).ToList();
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetDepartmentPassesByDeptId(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                mDepartmentPasses model = (mDepartmentPasses)param;


                string csv = model.DepartmentID;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);

                if (model.SessionCode <= 7 && model.AssemblyCode <= 12)
                {
                    var result1 = (from DepartmentPasses in db.mPasses
                                   where ids.Contains(DepartmentPasses.DepartmentID)
                                       && DepartmentPasses.AssemblyCode == model.AssemblyCode && DepartmentPasses.SessionCode == model.SessionCode
                                       && DepartmentPasses.IsVehiclaPass == false
                                   orderby DepartmentPasses.PassID descending
                                   select DepartmentPasses).ToList();
                    return result1;

                }
                else
                {
                    var result = (from DepartmentPasses in db.mPasses
                                  where ids.Contains(DepartmentPasses.DepartmentID) && DepartmentPasses.IsRequestUserID == model.IsRequestUserID
                                      && DepartmentPasses.AssemblyCode == model.AssemblyCode && DepartmentPasses.SessionCode == model.SessionCode
                                      && DepartmentPasses.IsVehiclaPass == false
                                  orderby DepartmentPasses.PassID descending
                                  select DepartmentPasses).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetDepartmentVehiclePassesByDeptId(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                mDepartmentPasses model = (mDepartmentPasses)param;

                string csv = model.DepartmentID;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                if (model.SessionCode <= 7 && model.AssemblyCode <= 12)
                {
                    var result = (from DepartmentPasses in db.mPasses
                                  where ids.Contains(DepartmentPasses.DepartmentID) && DepartmentPasses.IsVehiclaPass == true
                                      && DepartmentPasses.AssemblyCode == model.AssemblyCode && DepartmentPasses.SessionCode == model.SessionCode
                                  orderby DepartmentPasses.PassID
                                  descending
                                  select DepartmentPasses).ToList();
                    return result;
                }
                else
                {

                    var result = (from DepartmentPasses in db.mPasses
                                  where ids.Contains(DepartmentPasses.DepartmentID) && DepartmentPasses.IsVehiclaPass == true && DepartmentPasses.IsRequestUserID == model.IsRequestUserID
                                      && DepartmentPasses.AssemblyCode == model.AssemblyCode && DepartmentPasses.SessionCode == model.SessionCode
                                  orderby DepartmentPasses.PassID
                                  descending
                                  select DepartmentPasses).ToList();
                    return result;

                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        static object GetDepartmentPassesById(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                mDepartmentPasses model = (mDepartmentPasses)param;
                var result = (from DepartmentPasses in db.mPasses where DepartmentPasses.PassID == model.PassID orderby DepartmentPasses.PassID descending select DepartmentPasses).FirstOrDefault();
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }



        static object SubmitDepartmentPassRequest(object param)
        {
            try
            {
                DepartmentPassesContext db = new DepartmentPassesContext();
                mDepartmentPasses model = (mDepartmentPasses)param;

                mDepartmentPasses obj = db.mPasses.Single(m => m.PassID == model.PassID);
                obj.IsRequested = true;
                obj.RequestedDate = System.DateTime.Now;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        static object UpdatePassRequest(object param)
        {
            mDepartmentPasses PassToUpdate = param as mDepartmentPasses;
            try
            {
                using (DepartmentPassesContext _context = new DepartmentPassesContext())
                {

                    using (var dbContextTransaction = _context.Database.BeginTransaction())
                    {
                        try
                        {
                            _context.mPasses.Attach(PassToUpdate);
                            _context.Entry(PassToUpdate).State = EntityState.Modified;
                            _context.SaveChanges();
                            dbContextTransaction.Commit(); 
                        }
                        catch (Exception)
                        {
                            dbContextTransaction.Rollback();
                        }
                        finally
                        {
                            _context.Close();

                        }

                    }                    
                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return PassToUpdate;
        }


    }
}
