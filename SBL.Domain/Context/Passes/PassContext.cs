﻿namespace SBL.Domain.Context.Passes
{
    #region Namespace Reffrences
    using SBL.DAL;
    using SBL.Domain.Context.Assembly;
    using SBL.Domain.Context.Session;
    using SBL.Domain.Context.SiteSetting;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Media;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Session;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Xml.Linq;

    #endregion

    public class PassContext : DBBase<PassContext>
    {
        public PassContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<Pass> Passes { get; set; }
        public DbSet<PassCategory> PassCategories { get; set; }
        public DbSet<Journalist> Journalists { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<mDepartment> Departments { get; set; }
        public DbSet<mPasses> mPasses { get; set; }
        public DbSet<mPassesDeleted> mPassesDeleted { get; set; }


        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                #region Pass Category
                case "CreatePassCategory": { CreatePassCategory(param.Parameter); break; }
                case "UpdatePassCategory": { UpdatePassCategory(param.Parameter); break; }
                case "DeletePassCategory": { DeletePassCategory(param.Parameter); break; }
                case "ChangePassCategoryStatus": { ChangePassCategoryStatus(param.Parameter); break; }
                case "GetAllPassCategories": { return GetAllPassCategories(); }
                case "GetAllActivePassCategories": { return GetAllActivePassCategories(); }
                case "GetPassCategoryById": { return GetPassCategoryById(param.Parameter); }
                case "CheckPassCategoryForVehicle": { return CheckPassCategoryForVehicle(param.Parameter); }
                case "GetMasterPassByPassCodes": { return GetMasterPassByPassCodes(param.Parameter); }

                #endregion



                #region Pass

                case "IssueNewPass": { IssueNewPass(param.Parameter); break; }
                case "GetPassById": { return GetPassById(param.Parameter); }
                case "UpdatePass": { UpdatePass(param.Parameter); break; }
                case "DeletePass": { DeletePass(param.Parameter); break; }
                case "ChangePassStatus": { ChangePassStatus(param.Parameter); break; }

                case "GetAllPasses": { return GetAllPasses(param.Parameter); }
                case "GetAllRequestedPasses": { return GetAllRequestedPasses(param.Parameter); }
                case "GetRequestedPassesCount": { return GetRequestedPassesCount(param.Parameter); }
                case "GetAllApprovedPasses": { return GetAllApprovedPasses(param.Parameter); }
                case "GetApprovedPassesCount": { return GetApprovedPassesCount(param.Parameter); }
                case "GetAllRejectedPasses": { return GetAllRejectedPasses(param.Parameter); }
                case "GetRejectedPassesCount": { return GetRejectedPassesCount(param.Parameter); }

                case "ApprovePassRequest": { ApprovePassRequest(param.Parameter); break; }
                case "ApproveMultiplePassRequest": { ApproveMultiplePassRequest(param.Parameter); break; }
                case "RejectPassRequest": { RejectPassRequest(param.Parameter); break; }
                case "RejectMultiplePassRequest": { RejectMultiplePassRequest(param.Parameter); break; }
                case "PassesForPrinting": { return PassesForPrinting(param.Parameter); }
                case "GetDetailsByPassCode":
                    {
                        return GetDetailsByPassCode(param.Parameter);
                    }

                case "GetPassCategaryName":
                    { return GetPassCategaryName(param.Parameter); }
                #endregion
            }
            return null;
        }

        static object GetPassCategaryName(object p)
        {
            mPasses model = p as mPasses;
            PassContext pCtxt = new PassContext();
            string categaryName = (from mdl in pCtxt.PassCategories where mdl.PassCategoryID == model.RequestedPassCategoryID select mdl.Name).SingleOrDefault();
            return categaryName;
        }




        #region Pass Category


        static void CreatePassCategory(object param)
        {
            try
            {
                using (PassContext db = new PassContext())
                {
                    PassCategory PassCategoryToCreate = param as PassCategory;

                    //PassCategoryToCreate.CreatedDate = DateTime.Now;
                    //PassCategoryToCreate.ModifiedDate = DateTime.Now;
                    db.PassCategories.Add(PassCategoryToCreate);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetAllPassCategories()
        {
            try
            {
                using (PassContext db = new PassContext())
                {
                    return db.PassCategories.Where(c => c.IsDeleted == null).OrderByDescending(p => p.PassCategoryID).ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetAllActivePassCategories()
        {
            try
            {
                using (PassContext db = new PassContext())
                {
                    var result = db.PassCategories.Where(a => a.IsActive == true);
                    return result.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetPassCategoryById(object param)
        {
            PassCategory PassCategory = param as PassCategory;
            try
            {
                using (PassContext db = new PassContext())
                {
                    return db.PassCategories.SingleOrDefault(a => a.PassCategoryID == PassCategory.PassCategoryID);
                }
            }
            catch
            {
                throw;
            }
        }

        static void UpdatePassCategory(object param)
        {
            PassCategory PassCategoryToUpdate = param as PassCategory;
            try
            {
                using (PassContext db = new PassContext())
                {
                    //PassCategoryToUpdate.CreatedDate = DateTime.Now;
                    //PassCategoryToUpdate.ModifiedDate = DateTime.Now;
                    db.PassCategories.Attach(PassCategoryToUpdate);
                    db.Entry(PassCategoryToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }

        }

        static void DeletePassCategory(object param)
        {
            PassCategory Parameter = param as PassCategory;
            try
            {
                using (PassContext db = new PassContext())
                {
                    var PassCategoryToDelete = db.PassCategories.SingleOrDefault(a => a.PassCategoryID == Parameter.PassCategoryID);
                    if (PassCategoryToDelete != null)
                    {
                        PassCategoryToDelete.IsDeleted = true;
                    }
                    //db.PassCategories.Remove(PassCategoryToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }

        }

        static void ChangePassCategoryStatus(object param)
        {
            PassCategory PassCategory = param as PassCategory;
            try
            {
                using (PassContext db = new PassContext())
                {
                    var PassCategoryToUpdate = db.PassCategories.SingleOrDefault(a => a.PassCategoryID == PassCategory.PassCategoryID);

                    bool IsActive = (PassCategoryToUpdate.IsActive) ? false : true;
                    PassCategoryToUpdate.IsActive = IsActive;
                    db.PassCategories.Attach(PassCategoryToUpdate);
                    db.Entry(PassCategoryToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }

        }

        static object CheckPassCategoryForVehicle(object param)
        {
            PassCategory PassCategory = param as PassCategory;
            try
            {
                using (PassContext db = new PassContext())
                {
                    var result = db.PassCategories.SingleOrDefault(a => a.PassCategoryID == PassCategory.PassCategoryID);
                    if (result.ForVehicle)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch
            {
                throw;
            }

        }



        #endregion



        #region Pass

        static void IssueNewPass(object param)
        {
            try
            {
                using (PassContext db = new PassContext())
                {
                    Pass PassToCreate = param as Pass;
                    db.Passes.Add(PassToCreate);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetPassById(object param)
        {
            Pass Pass = param as Pass;
            try
            {
                using (PassContext db = new PassContext())
                {
                    return db.Passes.Include("PassCategory").SingleOrDefault(a => a.PassID == Pass.PassID);
                }
            }
            catch
            {
                throw;
            }
        }

        static void UpdatePass(object param)
        {
            Pass PassToUpdate = param as Pass;
            try
            {
                using (PassContext db = new PassContext())
                {
                    db.Passes.Attach(PassToUpdate);
                    db.Entry(PassToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }

        }

        static void DeletePass(object param)
        {
            Pass Parameter = param as Pass;
            try
            {
                using (PassContext db = new PassContext())
                {
                    var PassToDelete = db.Passes.SingleOrDefault(a => a.PassID == Parameter.PassID);
                    db.Passes.Remove(PassToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }

        }

        static void ChangePassStatus(object param)
        {
            Pass Pass = param as Pass;
            try
            {
                using (PassContext db = new PassContext())
                {
                    var PassToUpdate = db.Passes.SingleOrDefault(a => a.PassID == Pass.PassID);

                    bool IsActive = (PassToUpdate.IsActive) ? false : true;
                    PassToUpdate.IsActive = IsActive;
                    db.Passes.Attach(PassToUpdate);
                    db.Entry(PassToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }

        }

        static object GetAllPasses(object param)
        {
            Pass Parameter = param as Pass;
            int AssemblyID = Parameter.AssemblyID;
            int SessionID = Parameter.SessionID;
            int PassCategoryId = Parameter.PassCategoryID;
            try
            {
                using (PassContext db = new PassContext())
                {
                    if (PassCategoryId == 0)
                    {
                        return db.Passes.Include("PassCategory").Where(a => a.AssemblyID == AssemblyID && a.SessionID == SessionID).OrderByDescending(a => a.IssueDate).ToList();
                    }
                    else
                    {
                        return db.Passes.Include("PassCategory").Where(a => a.AssemblyID == AssemblyID && a.SessionID == SessionID && a.PassCategoryID == PassCategoryId).OrderByDescending(a => a.IssueDate).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetAllRequestedPasses(object param)
        {
            Pass Parameter = param as Pass;
            int AssemblyID = Parameter.AssemblyID;
            int SessionID = Parameter.SessionID;
            int PassCategoryId = Parameter.PassCategoryID;
            try
            {
                using (PassContext db = new PassContext())
                {
                    if (PassCategoryId == 0)
                    {
                        return db.Passes.Include("PassCategory").Where(a => a.Status == 1 && a.AssemblyID == AssemblyID && a.SessionID == SessionID).OrderByDescending(a => a.IssueDate).ToList();
                    }
                    else
                    {
                        return db.Passes.Include("PassCategory").Where(a => a.Status == 1 && a.AssemblyID == AssemblyID && a.SessionID == SessionID && a.PassCategoryID == PassCategoryId).OrderByDescending(a => a.IssueDate).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static int GetRequestedPassesCount(object param)
        {
            Pass Pass = param as Pass;
            try
            {
                var result = (List<Pass>)GetAllRequestedPasses(Pass);
                return result.Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetAllApprovedPasses(object param)
        {
            Pass Parameter = param as Pass;
            int AssemblyID = Parameter.AssemblyID;
            int SessionID = Parameter.SessionID;
            int PassCategoryId = Parameter.PassCategoryID;
            try
            {
                using (PassContext db = new PassContext())
                {
                    if (PassCategoryId == 0)
                    {
                        return db.Passes.Include("PassCategory").Where(a => a.Status == 2 && a.AssemblyID == AssemblyID && a.SessionID == SessionID).OrderByDescending(a => a.IssueDate).ToList();
                    }
                    else
                    {
                        return db.Passes.Include("PassCategory").Where(a => a.Status == 2 && a.AssemblyID == AssemblyID && a.SessionID == SessionID && a.PassCategoryID == PassCategoryId).OrderByDescending(a => a.IssueDate).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static int GetApprovedPassesCount(object param)
        {
            Pass Pass = param as Pass;
            try
            {
                var result = (List<Pass>)GetAllApprovedPasses(Pass);
                return result.Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetAllRejectedPasses(object param)
        {
            Pass Parameter = param as Pass;
            int AssemblyID = Parameter.AssemblyID;
            int SessionID = Parameter.SessionID;
            int PassCategoryId = Parameter.PassCategoryID;
            try
            {
                using (PassContext db = new PassContext())
                {
                    if (PassCategoryId == 0)
                    {
                        return db.Passes.Include("PassCategory").Where(a => a.Status == 3 && a.AssemblyID == AssemblyID && a.SessionID == SessionID).OrderByDescending(a => a.IssueDate).ToList();
                    }
                    else
                    {
                        return db.Passes.Include("PassCategory").Where(a => a.Status == 3 && a.AssemblyID == AssemblyID && a.SessionID == SessionID && a.PassCategoryID == PassCategoryId).OrderByDescending(a => a.IssueDate).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static int GetRejectedPassesCount(object param)
        {
            Pass Pass = param as Pass;
            try
            {
                var result = (List<Pass>)GetAllRejectedPasses(Pass);
                return result.Count();
            }
            catch
            {
                throw;
            }
        }

        static void ApprovePassRequest(object param)
        {
            Pass Pass = param as Pass;
            try
            {
                using (PassContext db = new PassContext())
                {
                    var PassToApprove = db.Passes.SingleOrDefault(a => a.PassID == Pass.PassID);
                    PassToApprove.Status = 2; //For Approve
                    db.Passes.Attach(PassToApprove);
                    db.Entry(PassToApprove).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static void ApproveMultiplePassRequest(object param)
        {
            List<Pass> PassesToApprove = param as List<Pass>;
            try
            {
                using (PassContext db = new PassContext())
                {
                    foreach (var item in PassesToApprove)
                    {
                        var PassToApprove = db.Passes.SingleOrDefault(a => a.PassID == item.PassID);
                        PassToApprove.Status = 2; //For Approve
                        db.Passes.Attach(PassToApprove);
                        db.Entry(PassToApprove).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static void RejectPassRequest(object param)
        {
            Pass Pass = param as Pass;
            try
            {
                using (PassContext db = new PassContext())
                {
                    var PassToReject = db.Passes.SingleOrDefault(a => a.PassID == Pass.PassID);
                    PassToReject.Status = 3; //For Reject
                    db.Passes.Attach(PassToReject);
                    db.Entry(PassToReject).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static void RejectMultiplePassRequest(object param)
        {
            List<Pass> PassesToReject = param as List<Pass>;
            try
            {
                using (PassContext db = new PassContext())
                {
                    foreach (var item in PassesToReject)
                    {
                        var PassToReject = db.Passes.SingleOrDefault(a => a.PassID == item.PassID);
                        PassToReject.Status = 3; //For Approve
                        db.Passes.Attach(PassToReject);
                        db.Entry(PassToReject).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object PassesForPrinting(object param)
        {
            List<Pass> PassesToPrint = param as List<Pass>;
            List<Pass> result = new List<Pass>();
            try
            {
                using (PassContext db = new PassContext())
                {
                    foreach (var item in PassesToPrint)
                    {
                        var pass = db.Passes.Include("PassCategory").SingleOrDefault(a => a.PassID == item.PassID && a.Status == 2);
                        result.Add(pass);
                    }
                }
                return result.ToList();
            }
            catch
            {
                throw;
            }
        }
        static object GetDetailsByPassCode(object param)
        {
            mPasses model = param as mPasses;
            try
            {
                using (PassContext db = new PassContext())
                {
                    var query = (from mdl in db.mPasses where mdl.PassCode == model.PassCode && mdl.SessionCode == model.SessionCode && mdl.AssemblyCode == model.AssemblyCode && mdl.IsApproved == true select mdl).SingleOrDefault();
                    return query;
                }
                return null;
            }
            catch
            {
                throw;
            }
        }

        #endregion


        #region Chnaged Service
        static object GetMasterPassByPassCodes(object param)
        {
            bool find = false;
            Int32 masterPassID = (Int32)param;
          //  ErrorLog.WriteToLog(null, masterPassID.ToString());

            SiteSettingContext settingContext = new SiteSettingContext();

            // Get SiteSettings 
            var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

            int AssemblyID = Convert.ToInt16(AssemblyCode);
            int SessionID = Convert.ToInt16(SessionCode);
            if (masterPassID != 0)
            {
                try
                {
                    using (PassContext _context = new PassContext())
                    {
                        string passDetailsAsXML = "<Pass>";

                        var passDetails = _context.mPasses.SingleOrDefault(a => a.PassCode == masterPassID && a.AssemblyCode == AssemblyID && a.SessionCode == SessionID);//&& a.SessionDateFrom.CompareTo(startDateAsString) <= 0 && a.SessionDateFrom.CompareTo(startDateAsString) >= 0);
                        if (passDetails != null)
                        {

                            passDetails.Photo = "/securefilestructure/images/pass/photo/" + AssemblyCode + "/" + SessionCode + "/" + passDetails.Photo;
                           // ErrorLog.WriteToLog(null, passDetails.Photo);
                            DateTime fromDate = new DateTime(Convert.ToInt32(passDetails.SessionDateFrom.Split('/')[2]), Convert.ToInt32(passDetails.SessionDateFrom.Split('/')[1]), Convert.ToInt32(passDetails.SessionDateFrom.Split('/')[0]));
                            DateTime ToDate = new DateTime(Convert.ToInt32(passDetails.SessionDateTo.Split('/')[2]), Convert.ToInt32(passDetails.SessionDateTo.Split('/')[1]), Convert.ToInt32(passDetails.SessionDateTo.Split('/')[0]));

                            if (fromDate.Date <= DateTime.Now.Date && ToDate.Date >= DateTime.Now.Date)
                            {
                                find = true;
                                passDetails.passStatus = 1;
                                passDetailsAsXML += XMLForQRCodes(passDetails);
                            }
                            else
                            {
                                find = true;
                                passDetails.passStatus = 0;
                                passDetailsAsXML += XMLForQRCodes(passDetails);
                            }

                        }
                        var DeleytedpassDetails = _context.mPassesDeleted.SingleOrDefault(a => a.PassCode == masterPassID && a.AssemblyCode == AssemblyID && a.SessionCode == SessionID);//&& a.SessionDateFrom.CompareTo(startDateAsString) <= 0 && a.SessionDateFrom.CompareTo(startDateAsString) >= 0);
                        if (DeleytedpassDetails != null)
                        {

                            DateTime fromDate = new DateTime(Convert.ToInt32(DeleytedpassDetails.SessionDateFrom.Split('/')[2]), Convert.ToInt32(DeleytedpassDetails.SessionDateFrom.Split('/')[1]), Convert.ToInt32(DeleytedpassDetails.SessionDateFrom.Split('/')[0]));
                            DateTime ToDate = new DateTime(Convert.ToInt32(DeleytedpassDetails.SessionDateTo.Split('/')[2]), Convert.ToInt32(DeleytedpassDetails.SessionDateTo.Split('/')[1]), Convert.ToInt32(DeleytedpassDetails.SessionDateTo.Split('/')[0]));

                            if (fromDate.Date <= DateTime.Now.Date && ToDate.Date >= DateTime.Now.Date)
                            {
                                find = true;
                                DeleytedpassDetails.passStatus = 1;
                                DeleytedpassDetails.OrgPassCode = (from list in _context.mPasses where list.AadharID == DeleytedpassDetails.AadharID && list.Name == DeleytedpassDetails.Name && list.SessionCode == DeleytedpassDetails.SessionCode select list.PassCode).FirstOrDefault();
                                passDetailsAsXML += XMLForQRCodesForDeleted(DeleytedpassDetails);
                            }
                            else
                            {
                                find = true;
                                DeleytedpassDetails.passStatus = 0;
                                DeleytedpassDetails.OrgPassCode = (from list in _context.mPasses where list.AadharID == DeleytedpassDetails.AadharID && list.Name == DeleytedpassDetails.Name && list.SessionCode == DeleytedpassDetails.SessionCode select list.PassCode).FirstOrDefault();
                                passDetailsAsXML += XMLForQRCodesForDeleted(DeleytedpassDetails);
                            }


                        }

                        passDetailsAsXML = passDetailsAsXML + "</Pass>";
                        if (!find) { passDetailsAsXML = ""; }
                        return passDetailsAsXML;
                    }
                }
                catch
                {
                    throw;
                }
            }
            return null;
        }
        public static string XMLForQRCodes(mPasses model)
        {
            XElement passParentNode = new System.Xml.Linq.XElement("PassCode");

            XElement passChildChildNode1 = new System.Xml.Linq.XElement("Name");
            passParentNode.Add(passChildChildNode1);

            XElement passChildChildNode2 = new System.Xml.Linq.XElement("Department");
            passParentNode.Add(passChildChildNode2);

            XElement passChildChildNode3 = new System.Xml.Linq.XElement("Designation");
            passParentNode.Add(passChildChildNode3);

            XElement passChildChildNode4 = new System.Xml.Linq.XElement("Address");
            passParentNode.Add(passChildChildNode4);

            XElement passChildChildNode5 = new System.Xml.Linq.XElement("From");
            passParentNode.Add(passChildChildNode5);

            XElement passChildChildNode6 = new System.Xml.Linq.XElement("To");
            passParentNode.Add(passChildChildNode6);

            XElement passChildChildNode7 = new System.Xml.Linq.XElement("PassCategoryName");
            passParentNode.Add(passChildChildNode7);

            XElement passChildChildNode8 = new System.Xml.Linq.XElement("VehicleNumber");
            passParentNode.Add(passChildChildNode8);

            XElement passChildChildNode9 = new System.Xml.Linq.XElement("Photo");
            passParentNode.Add(passChildChildNode9);

            XElement passChildChildNode10 = new System.Xml.Linq.XElement("GateNumber");
            passParentNode.Add(passChildChildNode10);

            XElement passChildChildNode11 = new System.Xml.Linq.XElement("Gender");
            passParentNode.Add(passChildChildNode11);

            XElement passChildChildNode12 = new System.Xml.Linq.XElement("Age");
            passParentNode.Add(passChildChildNode12);

            XElement passChildChildNode13 = new System.Xml.Linq.XElement("AssemblyCode");
            passParentNode.Add(passChildChildNode13);

            XElement passChildChildNode14 = new System.Xml.Linq.XElement("SessionCode");
            passParentNode.Add(passChildChildNode14);

            XElement passChildChildNode15 = new System.Xml.Linq.XElement("AssemblyName");
            passParentNode.Add(passChildChildNode15);

            XElement passChildChildNode16 = new System.Xml.Linq.XElement("SessionName");
            passParentNode.Add(passChildChildNode16);

            XElement passChildChildNode17 = new System.Xml.Linq.XElement("SessionDate");
            passParentNode.Add(passChildChildNode17);


            XElement passChildChildNode18 = new System.Xml.Linq.XElement("Status");
            passParentNode.Add(passChildChildNode18);

            passParentNode.SetAttributeValue("ID", model.PassCode);

            string information = model.Name + " (" + model.Gender + ", " + model.Age + " Years)";

            passChildChildNode1.ReplaceNodes(model.Name != null ? information : "");
            passChildChildNode2.ReplaceNodes(model.OrganizationName != null ? model.OrganizationName : "");
            passChildChildNode3.ReplaceNodes(model.Designation != null ? model.Designation : "");
            passChildChildNode4.ReplaceNodes(model.Address != null ? model.Address : "");
            passChildChildNode5.ReplaceNodes(model.SessionDateFrom != null ? model.SessionDateFrom : "");
            passChildChildNode6.ReplaceNodes(model.SessionDateTo != null ? model.SessionDateTo : "");
            passChildChildNode7.ReplaceNodes(model.ApprovedPassCategoryID != 0 ? GetPassCategoryNameByID(model.ApprovedPassCategoryID) : "");
            passChildChildNode8.ReplaceNodes(model.VehicleNumber != null ? model.VehicleNumber : "");
            passChildChildNode9.ReplaceNodes(!string.IsNullOrEmpty(model.Photo) ? model.Photo : string.Empty);
            passChildChildNode10.ReplaceNodes(!string.IsNullOrEmpty(model.GateNumber) ? model.GateNumber : string.Empty);

            passChildChildNode11.ReplaceNodes(!string.IsNullOrEmpty(model.Gender) ? model.Gender : string.Empty);
            passChildChildNode12.ReplaceNodes(model.Age);
            passChildChildNode13.ReplaceNodes(model.AssemblyCode);
            passChildChildNode14.ReplaceNodes(model.SessionCode);

            mSession Mdl = new mSession();
            Mdl.SessionCode = model.SessionCode;
            Mdl.AssemblyID = model.AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = model.AssemblyCode;

            AssemblyContext assemblyContext = new AssemblyContext();

            string SessionName = Convert.ToString(SessionContext.GetSessionNameBySessionCode(Mdl));
            string AssesmblyName = Convert.ToString(AssemblyContext.GetAssemblyNameByAssemblyCode(assmblyMdl));

            passChildChildNode15.ReplaceNodes(!string.IsNullOrEmpty(SessionName) ? SessionName : ""); //Assembly Name.
            passChildChildNode16.ReplaceNodes(!string.IsNullOrEmpty(AssesmblyName) ? AssesmblyName : ""); //Session Name.

            passChildChildNode17.ReplaceNodes(DateTime.Now);
            passChildChildNode18.ReplaceNodes(model.passStatus);
            return passParentNode.ToString();
        }

        public static string XMLForQRCodesForDeleted(mPassesDeleted model)
        {
            XElement passParentNode = new System.Xml.Linq.XElement("DeletedPassCode");

            XElement passChildChildNode18 = new System.Xml.Linq.XElement("CurrentPassCode");
            passParentNode.Add(passChildChildNode18);


            XElement passChildChildNode1 = new System.Xml.Linq.XElement("Name");
            passParentNode.Add(passChildChildNode1);

            XElement passChildChildNode2 = new System.Xml.Linq.XElement("Department");
            passParentNode.Add(passChildChildNode2);

            XElement passChildChildNode3 = new System.Xml.Linq.XElement("Designation");
            passParentNode.Add(passChildChildNode3);

            XElement passChildChildNode4 = new System.Xml.Linq.XElement("Address");
            passParentNode.Add(passChildChildNode4);

            XElement passChildChildNode5 = new System.Xml.Linq.XElement("From");
            passParentNode.Add(passChildChildNode5);

            XElement passChildChildNode6 = new System.Xml.Linq.XElement("To");
            passParentNode.Add(passChildChildNode6);

            XElement passChildChildNode7 = new System.Xml.Linq.XElement("PassCategoryName");
            passParentNode.Add(passChildChildNode7);

            XElement passChildChildNode8 = new System.Xml.Linq.XElement("VehicleNumber");
            passParentNode.Add(passChildChildNode8);

            XElement passChildChildNode9 = new System.Xml.Linq.XElement("Photo");
            passParentNode.Add(passChildChildNode9);

            XElement passChildChildNode10 = new System.Xml.Linq.XElement("GateNumber");
            passParentNode.Add(passChildChildNode10);

            XElement passChildChildNode11 = new System.Xml.Linq.XElement("Gender");
            passParentNode.Add(passChildChildNode11);

            XElement passChildChildNode12 = new System.Xml.Linq.XElement("Age");
            passParentNode.Add(passChildChildNode12);

            XElement passChildChildNode13 = new System.Xml.Linq.XElement("AssemblyCode");
            passParentNode.Add(passChildChildNode13);

            XElement passChildChildNode14 = new System.Xml.Linq.XElement("SessionCode");
            passParentNode.Add(passChildChildNode14);

            XElement passChildChildNode15 = new System.Xml.Linq.XElement("AssemblyName");
            passParentNode.Add(passChildChildNode15);

            XElement passChildChildNode16 = new System.Xml.Linq.XElement("SessionName");
            passParentNode.Add(passChildChildNode16);

            XElement passChildChildNode17 = new System.Xml.Linq.XElement("SessionDate");
            passParentNode.Add(passChildChildNode17);

            XElement passChildChildNode19 = new System.Xml.Linq.XElement("Status");
            passParentNode.Add(passChildChildNode19);
            passParentNode.SetAttributeValue("ID", model.PassCode);

            string information = model.Name + " (" + model.Gender + ", " + model.Age + " Years)";

            passChildChildNode1.ReplaceNodes(model.Name != null ? information : "");
            passChildChildNode2.ReplaceNodes(model.OrganizationName != null ? model.OrganizationName : "");
            passChildChildNode3.ReplaceNodes(model.Designation != null ? model.Designation : "");
            passChildChildNode4.ReplaceNodes(model.Address != null ? model.Address : "");
            passChildChildNode5.ReplaceNodes(model.SessionDateFrom != null ? model.SessionDateFrom : "");
            passChildChildNode6.ReplaceNodes(model.SessionDateTo != null ? model.SessionDateTo : "");
            passChildChildNode7.ReplaceNodes(model.ApprovedPassCategoryID != 0 ? GetPassCategoryNameByID(model.ApprovedPassCategoryID) : "");
            passChildChildNode8.ReplaceNodes(model.VehicleNumber != null ? model.VehicleNumber : "");
            passChildChildNode9.ReplaceNodes(!string.IsNullOrEmpty(model.Photo) ? model.Photo : string.Empty);
            passChildChildNode10.ReplaceNodes(!string.IsNullOrEmpty(model.GateNumber) ? model.GateNumber : string.Empty);

            passChildChildNode11.ReplaceNodes(!string.IsNullOrEmpty(model.Gender) ? model.Gender : string.Empty);
            passChildChildNode12.ReplaceNodes(model.Age);
            passChildChildNode13.ReplaceNodes(model.AssemblyCode);
            passChildChildNode14.ReplaceNodes(model.SessionCode);

            mSession Mdl = new mSession();
            Mdl.SessionCode = model.SessionCode;
            Mdl.AssemblyID = model.AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = model.AssemblyCode;

            AssemblyContext assemblyContext = new AssemblyContext();

            string SessionName = Convert.ToString(SessionContext.GetSessionNameBySessionCode(Mdl));
            string AssesmblyName = Convert.ToString(AssemblyContext.GetAssemblyNameByAssemblyCode(assmblyMdl));

            passChildChildNode15.ReplaceNodes(!string.IsNullOrEmpty(SessionName) ? SessionName : ""); //Assembly Name.
            passChildChildNode16.ReplaceNodes(!string.IsNullOrEmpty(AssesmblyName) ? AssesmblyName : ""); //Session Name.

            passChildChildNode17.ReplaceNodes(DateTime.Now);
            passChildChildNode18.ReplaceNodes(model.OrgPassCode);
            passChildChildNode19.ReplaceNodes(model.passStatus);
            return passParentNode.ToString();
        }

        public static string GetPassCategoryNameByID(int passCategoryID)
        {
            var passCategoryModel = GetPassCategoryById(new PassCategory { PassCategoryID = passCategoryID }) as PassCategory;
            if (passCategoryModel != null)
            {
                return passCategoryModel.Name;
            }
            return "";
        }

        #endregion

    }
}
