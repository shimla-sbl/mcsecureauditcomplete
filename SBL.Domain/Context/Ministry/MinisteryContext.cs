﻿
using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Secretory;
using SBL.DomainModel.Models.SiteSetting;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;


namespace SBL.Domain.Context.Ministry
{
    class MinisteryContext : DBBase<MinisteryContext>
    {
        public MinisteryContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public virtual DbSet<mMinsitryMinister> mMinsitryMinisteries { get; set; }
        public virtual DbSet<mMinistry> mMinsitry { get; set; }
        public virtual DbSet<mDepartment> mDepartments { get; set; }
        public virtual DbSet<mMinistryDepartment> mMinistryDepartments { get; set; }
        public virtual DbSet<mAssembly> mAssembly { get; set; }
        public virtual DbSet<mMember> mMember { get; set; }
        public DbSet<mSecretoryDepartment> SecDept { get; set; }
        public virtual DbSet<SiteSettings> SiteSettings { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {

                case "CreateMinistry": { CreateMinistry(param.Parameter); break; }
                case "UpdateMinistry": { return UpdateMinistry(param.Parameter); }
                case "DeleteMinistry": { return DeleteMinistry(param.Parameter); }
                case "GetAllMinistry1": { return GetAllMinistry1(); }
                case "GetAllMinistryList": { return GetAllMinistryList(); }
                case "GetMinistryById": { return GetMinistryById(param.Parameter); }
                case "GetAllMembers": { return GetAllMembers(); }
                case "IsMinistryIdChildExist": { return IsMinistryIdChildExist(param.Parameter); }


                case "CreateMinistryDepartment": { CreateMinistryDepartment(param.Parameter); break; }
                case "UpdateMinistryDepartment": { return UpdateMinistryDepartment(param.Parameter); }
                case "DeleteMinistryDepartment": { return DeleteMinistryDepartment(param.Parameter); }
                case "GetAllMinistry1Department": { return GetAllMinistry1Department(); }
                case "GetMinistryByIdDepartment": { return GetMinistryByIdDepartment(param.Parameter); }
                case "GetAllOderId": { return GetAllOderId(); }



                case "CreateMinsitryMinister": { CreateMinsitryMinister(param.Parameter); break; }
                case "UpdateMinsitryMinister": { return UpdateMinsitryMinister(param.Parameter); }
                case "DeleteMinsitryMinister": { return DeleteMinsitryMinister(param.Parameter); }
                case "GetAllMinsitryMinister": { return GetAllMinsitryMinister(); }
                case "GetMinsitryMinisterById": { return GetMinsitryMinisterById(param.Parameter); }

                case "GetAllMinsitryMinisterByAID": { return GetAllMinsitryMinisterByAID(param.Parameter); }


                case "GetAllMinistry":
                    {
                        return GetAllMinistryDetails(param.Parameter);
                    }
                case "GetMinisterMinistry":
                    {
                        return GetMinisterMinistry();
                    }
                case "GetDepartmentByMinistery":
                    {
                        return GetDepartmentByMinistery(param.Parameter);
                    }

                case "GetMinisterByDepartment":
                    {
                        return GetMinisterByDepartment(param.Parameter);
                    }

                case "GetDepartmentlist":
                    {
                        return GetDepartmentlist(param.Parameter);
                    }

                //added by nitin

                case "GetAllMinistryDetailsByDepartment":
                    {
                        return GetAllMinistryDetailsByDepartment(param.Parameter);
                    }
                case "GetMinisteryInfoById":
                    {
                        return GetMinisteryInfoById(param.Parameter);
                    }
                case "GetDepartment":
                    {
                        return GetDepartment(param.Parameter);
                    }
                case "GetMinistersonly":
                    {
                        return GetMinistersonly(param.Parameter);
                    }
                case "GetAllMinistryByDepartmentIds":
                    {
                        return GetAllMinistryByDepartmentIds(param.Parameter);
                    }
                case "GetMinisterInfoByMinistryId":
                    {
                        return GetMinisterInfoByMinistryId(param.Parameter);
                    }
                case "GetAllDeptment":
                    {
                        return GetAllDeptment(param.Parameter);
                    }

                case "GetMembersNull":
                    {
                        return GetMembersNull();
                    }
                case "GetAllMinistryNull":
                    {
                        return GetAllMinistryNull();
                    }
                case "GetMinistersProofTypist":
                    {
                        return GetMinistersProofTypist(param.Parameter);
                    }
                case "GetMinistersListByAssemblyID":
                    {
                        return GetMinistersListByAssemblyID(param.Parameter);
                    }
                    //done by robin
                case "GetAllMembersByAssemblyId":
                    {
                        return GetAllMembersByAssemblyId(param.Parameter);
                    }
                case "GetMinisterNameByMinistryId":
                    {
                        return GetMinisterNameByMinistryId(param.Parameter);
                    }

                    
                    
            } return null;
        }

        static List<mMinistry> GetMinistersListByAssemblyID(object param)
        {
            MinisteryContext db = new MinisteryContext();
            int assemblycode = Convert.ToInt16(param);
            var data = (from ministry in db.mMinsitry
                        join Assem in db.mAssembly on ministry.AssemblyID equals Assem.AssemblyCode into JoinAssemblyName
                        from assem in JoinAssemblyName.DefaultIfEmpty()
                        where ministry.AssemblyID == assemblycode
                        //join MemName in db.mMember on ministry.MemberCode equals MemName.MemberCode into JoinMemName
                        //from memname in JoinMemName.DefaultIfEmpty()
                        where ministry.IsDeleted == null
                        select new
                        {
                            ministry,
                            assem.AssemblyName,
                            //memname.Name
                        });
            List<mMinistry> list = new List<mMinistry>();
            foreach (var item in data)
            {
                item.ministry.GetAssemblyName = item.AssemblyName;
                //item.ministry.GetMemberName = item.Name;
                list.Add(item.ministry);

            }
            return list.ToList();


        }

        private static object IsMinistryIdChildExist(object param)
        {
            try
            {
                using (MinisteryContext db = new MinisteryContext())
                {
                    mMinistry model = (mMinistry)param;
                    var Res = (from e in db.mMinsitryMinisteries
                               where (e.MinistryID == model.MinistryID)
                               select e).Count();

                    if (Res == 0)
                    {
                        var MinDept = (from e in db.mMinistryDepartments
                                       where (e.MinistryID == model.MinistryID)
                                       select e).Count();
                        if (MinDept == 0)
                        {
                            return false;
                        }


                    }

                    else
                    {
                        return true;

                    }

                }
                return true;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateMinistry(object param)
        {
            try
            {
                using (MinisteryContext db = new MinisteryContext())
                {
                    mMinistry model = param as mMinistry;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMinsitry.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        //static object UpdateMinistry(object param)
        //{
        //    if (null == param)
        //    {
        //        return null;
        //    }
        //    using (MinisteryContext db = new MinisteryContext())
        //    {
        //        mMinistry model = param as mMinistry;
        //        model.CreatedDate = DateTime.Now;
        //        model.ModifiedWhen = DateTime.Now;

        //        db.mMinsitry.Attach(model);
        //        db.Entry(model).State = EntityState.Modified;
        //        db.SaveChanges();
        //        db.Close();
        //    }
        //    return GetAllMinistry1();
        //}
        static object UpdateMinistry(object param)
        {
            if (null == param)
            {
                return null;
            }
            //using (MinisteryContext db = new MinisteryContext())
            //{
            //    mMinistry model = param as mMinistry;
            //    model.CreatedDate = DateTime.Now;
            //    model.ModifiedWhen = DateTime.Now;

            //    db.mMinsitry.Attach(model);
            //    db.Entry(model).State = EntityState.Modified;
            //    db.SaveChanges();
            //    db.Close();
            //}
            mMinistry parameter = param as mMinistry;
            using (MinisteryContext ctx = new MinisteryContext())
            {
                var query = (from q in ctx.mMinsitry
                             where q.MinistryID == parameter.MinistryID
                             select q).First();
                query.MinistryName = parameter.MinistryName;
                query.AssemblyID = parameter.AssemblyID;
                query.MinistryNameLocal = parameter.MinistryNameLocal;
                query.AdditionalMinistry = parameter.AdditionalMinistry;
                query.ModifiedWhen = System.DateTime.Now;
                ctx.SaveChanges();
                ctx.Close();
                // int result = ctx.SaveChanges();
                // return result;
            }

            return GetAllMinistry1();
        }

        static object DeleteMinistry(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MinisteryContext db = new MinisteryContext())
            {
                mMinistry parameter = param as mMinistry;
                mMinistry ministryToRemove = db.mMinsitry.SingleOrDefault(a => a.MinistryID == parameter.MinistryID);
                if (ministryToRemove != null)
                {
                    ministryToRemove.IsDeleted = true;
                }
                //db.mMinsitry.Remove(ministryToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllMinistry1();
        }

        static mMinistry GetMinistryById(object param)
        {
            mMinistry parameter = param as mMinistry;
            MinisteryContext db = new MinisteryContext();
            var query = db.mMinsitry.SingleOrDefault(a => a.MinistryID == parameter.MinistryID);
            return query;
        }

        static List<mMinistry> GetAllMinistryList()
        {
            MinisteryContext db = new MinisteryContext();

            var data = (from ministry in db.mMinsitry
                        join Assem in db.mAssembly on ministry.AssemblyID equals Assem.AssemblyCode into JoinAssemblyName
                        from assem in JoinAssemblyName.DefaultIfEmpty()
                        //join MemName in db.mMember on ministry.MemberCode equals MemName.MemberCode into JoinMemName
                        //from memname in JoinMemName.DefaultIfEmpty()
                        where ministry.IsDeleted == null
                        select new
                        {
                            ministry,
                            assem.AssemblyName,
                            //memname.Name
                        });
            List<mMinistry> list = new List<mMinistry>();
            foreach (var item in data)
            {
                item.ministry.GetAssemblyName = item.AssemblyName;
                //item.ministry.GetMemberName = item.Name;
                list.Add(item.ministry);

            }
            return list.ToList();


        }

        static List<mMinistry> GetAllMinistry1()
        {
            MinisteryContext db = new MinisteryContext();

            var data = (from m in db.mMinsitry
                        orderby m.MinistryName ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;
            //var query = db.mMinsitry.ToList();
            //return query;

        }

        static List<mMinistry> GetAllMinistryNull()
        {
            MinisteryContext db = new MinisteryContext();

            var data = (from m in db.mMinsitry
                        orderby m.MinistryName ascending
                        where m.MinistryID == 0
                        select m).ToList();
            return data;

        }

        static List<mMember> GetAllMembers()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mMembers
                        orderby m.MemberCode ascending
                        where m.IsDeleted == null
                        select m).ToList();
            foreach (var item in data)
            {
                item.Name = item.Name + " (" + item.MemberCode + ")";
            }
            return data;
            //var query = db.mMembers.ToList();
            //return query;
        }


        static List<mMember> GetAllMembersByAssemblyId(object param)
        {
            DBManager db = new DBManager();
            //var assemblyId = Convert.ToInt32(param);
            var assembModel = Convert.ToInt32(param);
            var data = (from m in db.mMembers
                        join n in db.mMemberAssembly on m.MemberCode equals n.MemberID
                        join c in db.mConstituency on n.ConstituencyCode equals c.ConstituencyCode
                        orderby m.MemberCode ascending
                        where m.IsDeleted != true && n.AssemblyID == assembModel
                        && c.AssemblyID == assembModel && c.IsDeleted !=true & n.IsDeleted !=true
                        select new {
                        MemberCode = m.MemberCode,
                        Name = m.Name +"("+c.ConstituencyName+")"
                        }).ToList();
                        //select new mMember{
                        //MemberCode = m.MemberCode,
                        //Name = m.Name +"("+c.ConstituencyName+")"
                        //}).ToList();
            
            List<mMember> Memberlist = new List<mMember>();
            foreach (var item in data)
            {
                mMember memObj = new mMember();
                memObj.MemberCode = item.MemberCode;
                memObj.Name = item.Name;
                Memberlist.Add(memObj);
            }
            return Memberlist;
        }

        static List<mMember> GetMembersNull()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mMembers
                        orderby m.MemberCode ascending
                        where m.MemberCode == 0
                        select m).ToList();
            return data;
        }


        static void CreateMinistryDepartment(object param)
        {
            try
            {
                using (MinisteryContext db = new MinisteryContext())
                {
                    mMinistryDepartment model = param as mMinistryDepartment;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMinistryDepartments.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateMinistryDepartment(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MinisteryContext db = new MinisteryContext())
            {
                mMinistryDepartment model = param as mMinistryDepartment;
                model.CreatedDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;
                db.mMinistryDepartments.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllMinistry1Department();
        }

        static object DeleteMinistryDepartment(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MinisteryContext db = new MinisteryContext())
            {
                mMinistryDepartment parameter = param as mMinistryDepartment;
                mMinistryDepartment ministryToRemove = db.mMinistryDepartments.SingleOrDefault(a => a.MinistryDepartmentsID == parameter.MinistryDepartmentsID);

                if (ministryToRemove != null)
                {
                    ministryToRemove.IsDeleted = true;
                }
                //db.mMinistryDepartments.Remove(ministryToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllMinistry1Department();
        }

        static mMinistryDepartment GetMinistryByIdDepartment(object param)
        {
            mMinistryDepartment parameter = param as mMinistryDepartment;
            MinisteryContext db = new MinisteryContext();
            var query = db.mMinistryDepartments.SingleOrDefault(a => a.MinistryDepartmentsID == parameter.MinistryDepartmentsID);
            return query;
        }
        static List<mMinistryDepartment> GetAllOderId()
        {
            MinisteryContext db = new MinisteryContext();
            var data = (from m in db.mMinistryDepartments
                        orderby m.OrderID ascending
                        select m).ToList();
            return data;

        }

        static List<mMinistryDepartment> GetAllMinistry1Department()
        {
            //MinisteryContext db = new MinisteryContext();
            //var query = db.mMinistryDepartments.ToList();
            //return query;

            DBManager db = new DBManager();
            var data = (from mindept in db.mMinistryDepartments
                        join assembly in db.mAssemblies on mindept.AssemblyID equals assembly.AssemblyCode into joinAssemblyName
                        from assem in joinAssemblyName.DefaultIfEmpty()
                        join dept in db.mDepartments on mindept.DeptID equals dept.deptId into joindeptName
                        from deptment in joindeptName.DefaultIfEmpty()
                        join ministry in db.mMinistries on mindept.MinistryID equals ministry.MinistryID into joinministryName
                        from ministryName in joinministryName.DefaultIfEmpty()
                        where mindept.IsDeleted == null
                        select new
                        {
                            mindept,
                            assem.AssemblyName,
                            deptment.deptname,
                            ministryName.MinistryName,
                            ministryName.MinisterName


                        }).ToList();

            List<mMinistryDepartment> list = new List<mMinistryDepartment>();
            foreach (var item in data)
            {
                item.mindept.GetAssemblyName = item.AssemblyName;
                item.mindept.GetDeptName = item.deptname;
                //item.mindept.GetMinistryName = item.MinistryName;
                item.mindept.GetMinistryName = item.MinistryName + " " + "(" + item.MinisterName + ")";
                list.Add(item.mindept);
            }

            return list.OrderBy(q => q.GetMinistryName).ToList();
        }



        static void CreateMinsitryMinister(object param)
        {
            try
            {
                using (MinisteryContext db = new MinisteryContext())
                {
                    mMinsitryMinister model = param as mMinsitryMinister;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMinsitryMinisteries.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateMinsitryMinister(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MinisteryContext db = new MinisteryContext())
            {
                mMinsitryMinister model = param as mMinsitryMinister;
                model.CreatedDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mMinsitryMinisteries.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllMinsitryMinister();
        }

        static object DeleteMinsitryMinister(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MinisteryContext db = new MinisteryContext())
            {
                mMinsitryMinister parameter = param as mMinsitryMinister;
                mMinsitryMinister ministryToRemove = db.mMinsitryMinisteries.SingleOrDefault(a => a.MinsitryMinistersID == parameter.MinsitryMinistersID);
                if (ministryToRemove != null)
                {
                    ministryToRemove.IsDeleted = true;
                }
                //db.mMinsitryMinisteries.Remove(ministryToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllMinsitryMinister();
        }

        static mMinsitryMinister GetMinsitryMinisterById(object param)
        {
            mMinsitryMinister parameter = param as mMinsitryMinister;
            MinisteryContext db = new MinisteryContext();
            var query = db.mMinsitryMinisteries.SingleOrDefault(a => a.MinsitryMinistersID == parameter.MinsitryMinistersID);
            return query;
        }

        static string GetMinisterNameByMinistryId(object param)
        {
            string minName = "";
            string parameter = param as string;
            int MinId = Convert.ToInt32(parameter);
            MinisteryContext db = new MinisteryContext();
            var q = (from assemblyid in db.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);            
            minName = (from minst in db.mMinsitryMinisteries where minst.MinistryID == MinId && minst.AssemblyID==aid select minst.MinisterName).FirstOrDefault();
            return minName;
        }

        static List<mMinsitryMinister> GetAllMinsitryMinister()
        {
            MinisteryContext db = new MinisteryContext();
            var q = (from assemblyid in db.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);

            var data = (from ministryMini in db.mMinsitryMinisteries
                        join Assem in db.mAssembly on ministryMini.AssemblyID equals Assem.AssemblyCode into JoinAssemblyName
                        from assem in JoinAssemblyName.DefaultIfEmpty()
                        join MemName in db.mMember on ministryMini.MemberCode equals MemName.MemberCode into JoinMemName
                        from memname in JoinMemName.DefaultIfEmpty()
                        join MinName in db.mMinsitry on ministryMini.MinistryID equals MinName.MinistryID into JoinMinName
                        from minname in JoinMinName.DefaultIfEmpty()
                        orderby memname.Name
                        where ministryMini.IsDeleted == null && minname.AssemblyID == aid
                        select new
                        {
                            ministryMini,
                            assem.AssemblyName,
                            memname.Name,
                            minname.MinistryName
                        });
            List<mMinsitryMinister> list = new List<mMinsitryMinister>();
            foreach (var item in data)
            {
                item.ministryMini.GetAssemblyName = item.AssemblyName;
                item.ministryMini.GetMemberName = item.Name;
                item.ministryMini.GetMinistryName = item.MinistryName;
                list.Add(item.ministryMini);

            }
            return list.ToList();

        }

        static List<mMinsitryMinister> GetAllMinsitryMinisterByAID(object param)
        {
            int assembCOde = Convert.ToInt16(param);
            MinisteryContext db = new MinisteryContext();
            //var q = (from assemblyid in db.SiteSettings
            //         where assemblyid.SettingName == "Assembly"
            //         select assemblyid.SettingValue).FirstOrDefault();
            //int aid = Convert.ToInt32(q);

            var data = (from ministryMini in db.mMinsitryMinisteries
                        join Assem in db.mAssembly on ministryMini.AssemblyID equals Assem.AssemblyCode into JoinAssemblyName
                        from assem in JoinAssemblyName.DefaultIfEmpty()
                        join MemName in db.mMember on ministryMini.MemberCode equals MemName.MemberCode into JoinMemName
                        from memname in JoinMemName.DefaultIfEmpty()
                        join MinName in db.mMinsitry on ministryMini.MinistryID equals MinName.MinistryID into JoinMinName
                        from minname in JoinMinName.DefaultIfEmpty()
                        orderby memname.Name
                        where ministryMini.IsDeleted == null && ministryMini.AssemblyID == assembCOde
                        select new
                        {
                            ministryMini,
                            assem.AssemblyName,
                            memname.Name,
                            minname.MinistryName
                        });
            List<mMinsitryMinister> list = new List<mMinsitryMinister>();
            foreach (var item in data)
            {
                item.ministryMini.GetAssemblyName = item.AssemblyName;
                item.ministryMini.GetMemberName = item.Name;
                item.ministryMini.GetMinistryName = item.MinistryName;
                list.Add(item.ministryMini);

            }
            return list.ToList();

        }


        static object GetDepartmentlist(object param)
        {
            MinisteryContext DBContext = new MinisteryContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;
            var departmentList = (from ministryDept in DBContext.mMinistryDepartments
                                  join dept in DBContext.mDepartments on ministryDept.DeptID equals dept.deptId
                                  where ministryDept.MinistryID == 0
                                  select new tMemberNotice
                                  {
                                      DepartmentId = "",
                                      DepartmentName = "Select Minister"

                                  }).ToList();

            return departmentList;

        }
        static List<mMinisteryMinisterModel> GetAllMinistryDetails(object param)
        {
            MinisteryContext db = new MinisteryContext();

            var q = (from assemblyid in db.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);

            var query = (from ministerModel in db.mMinsitryMinisteries
                         join ministryModel in db.mMinsitry on ministerModel.MinistryID equals ministryModel.MinistryID
                         where ministryModel.AssemblyID == aid
                         select new mMinisteryMinisterModel
                         {

                             MinistryName = ministryModel.MinistryName,
                             MinsitryMinistersID = ministerModel.MinsitryMinistersID,
                             MinisterName = ministryModel.MinisterName,
                             MinistryID = ministryModel.MinistryID,
                             MinisterMinistryName = ministerModel.MinisterName + ", " + ministryModel.MinistryName
                         });


            return query.ToList();
        }

        static object GetMinisterMinistry()
        {
            MinisteryContext DBContext = new MinisteryContext();

            var q = (from assemblyid in DBContext.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);
            var MinMisList = (from Min in DBContext.mMinsitry
                              join minis in DBContext.mMinsitryMinisteries on Min.MinistryID equals minis.MinistryID
                              where Min.IsActive == true && Min.AssemblyID == aid
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName,
                                  MemberCode = minis.MemberCode
                              }).ToList();

            return MinMisList;
        }

        static object GetDepartmentByMinistery(object param)
        {
            MinisteryContext DBContext = new MinisteryContext();
            
            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;
            var q = (from assemblyid in DBContext.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);
            var DepartmentList = (from ministryDept in DBContext.mMinistryDepartments
                                  join dept in DBContext.mDepartments on ministryDept.DeptID equals dept.deptId
                                  where ministryDept.MinistryID == obj.MinistryID && ministryDept.AssemblyID == aid && ministryDept.IsDeleted == null
                                  select new tMemberNotice
                                  {
                                      DepartmentId = dept.deptId,
                                      DepartmentName = dept.deptname

                                  }).ToList();

            return DepartmentList;

        }


        static object GetMinisterByDepartment(object param)
        {
            MinisteryContext DBContext = new MinisteryContext();

            if (param == null)
            {
                return null;
            }

            var q = (from assemblyid in DBContext.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);

            mDepartment obj = param as mDepartment;
            var MinisterList = (from ministryDept in DBContext.mMinistryDepartments
                                join dept in DBContext.mMinsitry on ministryDept.MinistryID equals dept.MinistryID
                                where ministryDept.DeptID == obj.deptId
                                && dept.IsActive == true && dept.AssemblyID == aid
                                orderby dept.MinisterName ascending
                                select new tMemberNotice
                                {
                                    MinisterId = ministryDept.MinistryID,
                                    MinisterName = dept.MinisterName

                                }).ToList();

            return MinisterList;

        }

        #region nitin

        static List<mMinisteryMinisterModel> GetAllMinistryDetailsByDepartment(object param)
        {
            MinisteryContext db = new MinisteryContext();
            var q = (from assemblyid in db.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);

            tPaperLaidV paper = param as tPaperLaidV;
            var query = (from ministerModel in db.mMinsitryMinisteries

                         join ministryModel in db.mMinsitry on ministerModel.MinistryID equals ministryModel.MinistryID
                         where ministryModel.AssemblyID == aid && ministryModel.IsActive == true
                         orderby ministryModel.OrderID
                         select new mMinisteryMinisterModel
                         {

                             MinistryName = ministryModel.MinistryName,
                             MinsitryMinistersID = ministerModel.MinsitryMinistersID,
                             MinisterName = ministryModel.MinisterName,
                             MinistryID = ministryModel.MinistryID,
                             MinisterNameLocal = ministryModel.MinistryNameLocal,
                             MinisterMinistryName = ministerModel.MinisterName + ", " + ministryModel.MinistryName
                         }).ToList();


            return query.ToList();
        }

        static List<mMinisteryMinisterModel> GetAllMinistryByDepartmentIds(object param)
        {
            
            

            MinisteryContext db = new MinisteryContext();
            tPaperLaidV paper = param as tPaperLaidV;

            mDepartment model = param as mDepartment;

            var q = (from assemblyid in db.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);

            string csv = paper.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            var query = (from ministerModel in db.mMinsitryMinisteries
                         join minDept in db.mMinistryDepartments on ministerModel.MinistryID equals minDept.MinistryID
                         join ministryModel in db.mMinsitry on ministerModel.MinistryID equals ministryModel.MinistryID
                         where ids.Contains(minDept.DeptID)
                               && ministryModel.MinistryID != 12
                               && ministerModel.AssemblyID == aid
                                && minDept.AssemblyID == aid
                         orderby ministryModel.OrderID
                         select ministryModel).ToList().Distinct();

            foreach (var item in query)
            {
                var query1 = (from ministerModel1 in db.mMinsitryMinisteries where ministerModel1.MinistryID == item.MinistryID && ministerModel1.AssemblyID == aid select ministerModel1.MinisterName).FirstOrDefault();


                item.MinistryName = query1 + ", " + item.MinistryName;
            }

            return query.GroupBy(m => new { m.MinistryName, m.MinistryID }).Select(group =>
                               new mMinisteryMinisterModel { MinisterMinistryName = group.Key.MinistryName, MinistryID = group.Key.MinistryID }).ToList();

        }
        #endregion


        static object GetMinisteryInfoById(object param)
        {
            MinisteryContext DBContext = new MinisteryContext();
            mMinistry Ministry = (mMinistry)param;
            var Ministry1 = (from Minsitries in DBContext.mMinsitry
                             where Minsitries.MinistryID == Ministry.MinistryID
                             select Minsitries).FirstOrDefault();
            return Ministry1;
        }
        static object GetDepartment(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            MinisteryContext DBContext = new MinisteryContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;

            var departmentList = (from ministryDept in DBContext.mDepartments
                                  join mSecDept in DBContext.SecDept on ministryDept.deptId equals mSecDept.DepartmentID
                                  orderby ministryDept.deptname ascending
                                  select new tMemberNotice
                                  {
                                      DepartmentId = ministryDept.deptId,
                                      DepartmentName = ministryDept.deptname

                                  }).ToList();

            return departmentList;

        }
        public static object GetMinistersonly(object param)
        {
           
         //   int AssemblyId = (int)param;
            MinisteryContext DBContext = new MinisteryContext();

            var q = (from assemblyid in DBContext.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);

            var MinMisList = (from Min in DBContext.mMinsitry
                              join minis in DBContext.mMinsitryMinisteries on Min.MinistryID equals minis.MinistryID
                              where Min.IsActive == true && Min.AssemblyID == aid
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName + "(" +aid + ")" ,
                                  MemberCode = minis.MemberCode
                              }).ToList();

            return MinMisList;
        }

        static object GetMinisterInfoByMinistryId(object param)
        {
            mMinistry model = (mMinistry)param;
            MinisteryContext DBContext = new MinisteryContext();
            var MinMisList = (from Min in DBContext.mMinsitry
                              join minis in DBContext.mMinsitryMinisteries on Min.MinistryID equals minis.MinistryID
                              where Min.MinistryID == model.MinistryID
                              select minis).ToList();

            return MinMisList;
        }
        static object GetAllDeptment(object param)
        {
            MinisteryContext DBContext = new MinisteryContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;
            var departmentList = (from ministryDept in DBContext.mMinistryDepartments
                                  join dept in DBContext.mDepartments on ministryDept.DeptID equals dept.deptId
                                  join SDept in DBContext.SecDept on dept.deptId equals SDept.DepartmentID
                                  orderby dept.deptname ascending
                                  select new tMemberNotice
                                  {
                                      DepartmentId = dept.deptId,
                                      DepartmentName = dept.deptname

                                  }).Distinct().ToList();

            return departmentList;

        }

        public static object GetMinistersProofTypist(object param)
        {
            MinisteryContext DBContext = new MinisteryContext();
            tMemberNotice Obj = param as tMemberNotice;
            var MinMisList = (from Min in DBContext.mMinsitry
                              join minis in DBContext.mMinsitryMinisteries on Min.MinistryID equals minis.MinistryID
                              where Min.IsActive == true && Min.AssemblyID == Obj.AssemblyID
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName,
                                  MemberCode = minis.MemberCode
                              }).ToList();

            return MinMisList;
        } 

    }
}


