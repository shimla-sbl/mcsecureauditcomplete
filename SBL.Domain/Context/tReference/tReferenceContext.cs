﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.DomainModel.Models.Member;
using SBL.Service.Common;

namespace SBL.Domain.Context.tReference
{

    public class tReferenceContext : DBBase<tReferenceContext>
    {
        public tReferenceContext()
            : base("eVidhan")
        {
        }
        public DbSet<tReferenceMaterial> tReferenceMaterial { get; set; }
        public DbSet<mMember> mMember { get; set; }
        public DbSet<mReferenceMaterialType> mReferenceMaterialType { get; set; }
        public static object Execute(ServiceParameter param)
        {

            if (param != null)
            {
                switch (param.Method)
                {
                    case "AddReference": { return AddReference(param.Parameter); }
                    case "GetReference": { return GetReference(param.Parameter); }
                    //case "EditGetReference": { return EditCategory(param.Parameter); }
                    //case "UpdateGetReference": { return UpdateCategory(param.Parameter); }
                    case "DeleteRefrence": { return DeleteRefrence(param.Parameter); }
                     case "GetReferenceMaterialList": { return GetReferenceMaterialList(); }
                     case "GetRefrenceById": { return GetRefrenceById(param.Parameter); }
                        
                }
            }
            return null;
        }       

        public static object GetReference(object param)
        {
            tReferenceMaterial val = param as tReferenceMaterial;
            using (tReferenceContext ctx = new tReferenceContext())
            {
                var query = (from a in ctx.tReferenceMaterial
                             //  join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                             //from ps in temp.DefaultIfEmpty()
                             where a.AssemblyID == val.AssemblyID && a.SessionID == val.SessionID && a.IsActive ==true
                             select new
                             {
                                 ID = a.ID,
                                 ReferenceMaterialText = a.ReferenceMaterialText,
                                 // Department = ps.deptname ?? "OTHER",
                                 ReferenceMaterialAttachment = a.ReferenceMaterialAttachment,
                                 // PdfDateTime = a.ref,
                                 //SessionDates = a.MemberName
                                 MemberName = (from EN in ctx.mMember where EN.MemberCode == a.MemberCode select EN.Name).FirstOrDefault(),
                                 MaterialTypeName = (from mc in ctx.mReferenceMaterialType where mc.ID == a.ReferenceMaterialTypeId select mc.TypeText).FirstOrDefault(),
                                 CreateDate = a.CreateDate,
                             }).ToList().OrderByDescending(a => a.ID); //  (a => a.DraftId;.eFileAttachmentId)
                List<tReferenceMaterial> lst = new List<tReferenceMaterial>();
                foreach (var item in query)
                {
                    tReferenceMaterial mdl = new tReferenceMaterial();
                    mdl.ID = item.ID;
                    mdl.ReferenceMaterialText = item.ReferenceMaterialText;
                    mdl.ReferenceMaterialAttachment = item.ReferenceMaterialAttachment;
                    mdl.MemberName = item.MemberName;
                    mdl.MaterialTypeName = item.MaterialTypeName;
                    mdl.CreateDate = item.CreateDate;
                    lst.Add(mdl);
                }
                return lst;

            }
        }

        static object AddReference(object param)
        {
            if (null == param)
            {
                return 0;
            }
            tReferenceMaterial Mdl = param as tReferenceMaterial;

            using (tReferenceContext db = new tReferenceContext())
            {
                if (Mdl.Mode == "Add")
                {
                    Mdl.CreateDate = DateTime.Now;
                    Mdl.IsActive=true;
                    db.tReferenceMaterial.Add(Mdl);
                    db.SaveChanges();                   
                    return 0;                 
                }
                else if (Mdl.Mode == "Edit")
                {
                    tReferenceMaterial obj = db.tReferenceMaterial.Single(m => m.ID == Mdl.ID);
                    obj.ReferenceMaterialText = Mdl.ReferenceMaterialText;
                    obj.ReferenceMaterialTextLocal = Mdl.ReferenceMaterialTextLocal;                   
                    obj.MemberCode = Mdl.MemberCode;
                    obj.ReferenceMaterialDescription = Mdl.ReferenceMaterialDescription;
                    obj.ReferenceMaterialTypeId = Mdl.ReferenceMaterialTypeId;
                    obj.ReferenceMaterialAttachment = Mdl.ReferenceMaterialAttachment;
                    obj.AssemblyID = Mdl.AssemblyID;
                    obj.SessionID = Mdl.SessionID;
                    obj.CreateUser = Mdl.CreateUser;
                    Mdl.IsActive = true;
                    db.SaveChanges();
                    return 0;
                }
                else
                {
                    return -1;
                }
                // db.Close();
            }
        }

        static List<mReferenceMaterialType> GetReferenceMaterialList()
        {

            tReferenceContext db = new tReferenceContext();
            var query = (from p in db.mReferenceMaterialType
                         orderby p.TypeText
                         select p);
            return query.ToList();
        }

        static object GetRefrenceById(object param)
        {
            try
            {
                var Id = Convert.ToInt32(param);
                using (tReferenceContext db = new tReferenceContext())
                {
                    var a = (from data in db.tReferenceMaterial
                             where data.ID == Id && data.IsActive == true
                             select data).FirstOrDefault();
                    return a;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        static object DeleteRefrence(object param)
        {
            try
            {
                var Id = Convert.ToInt32(param);
                using (tReferenceContext db = new tReferenceContext())
                {
                    var a = (from data in db.tReferenceMaterial
                             where data.ID == Id && data.IsActive == true
                             select data).FirstOrDefault();
                    a.IsActive = false;
                    db.tReferenceMaterial.Attach(a);
                    db.Entry(a).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                    return a;                  
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

      
        /// <summary>

    }
}
