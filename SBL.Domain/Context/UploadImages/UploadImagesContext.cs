﻿using SBL.DAL;
using SBL.Domain.Migrations;
using SBL.DomainModel.Models.UploadImages;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.UploadImages
{
    public class UploadImagesContext : DBBase<UploadImagesContext>
    {
        public UploadImagesContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mUploadImages> mUploadImages { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "AddImages":
                    {
                        return AddImages(param.Parameter);
                    }
                case "DeleteImages":
                    {
                        return DeleteImages(param.Parameter);
                    }
                case "GetAllUploadImages":
                    {
                        return GetAllUploadImages(param.Parameter);
                    }
                case "GetUploadImagesById":
                    {
                        return GetUploadImagesById(param.Parameter);
                    }
                case "DeleteByParentId":
                    {
                        return DeleteByParentId(param.Parameter);
                    }
            }

            return null;
        }

        public static object AddImages(object param)
        {
            try
            {
                using (UploadImagesContext db = new UploadImagesContext())
                {
                    mUploadImages model = param as mUploadImages;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mUploadImages.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }



        public static object DeleteByParentId(object param)
        {
            try
            {
                using (UploadImagesContext db = new UploadImagesContext())
                {
                    mUploadImages parameter = param as mUploadImages;
                    mUploadImages stateToRemove = db.mUploadImages.SingleOrDefault(a => a.ParentId == parameter.ParentId);
                    db.mUploadImages.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }
        public static object DeleteImages(object param)
        {
            try
            {
                using (UploadImagesContext db = new UploadImagesContext())
                {
                    mUploadImages parameter = param as mUploadImages;
                    mUploadImages stateToRemove = db.mUploadImages.SingleOrDefault(a => a.UploadImagesID == parameter.UploadImagesID);
                    db.mUploadImages.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllUploadImages(object param)
        {

            try
            {

                string Result = param as string;
                string[] Array = Result.Split(',');
                int ParentId = 0;
                int.TryParse(Array[0], out ParentId);
                string Module = Array[1];
                using (UploadImagesContext db = new UploadImagesContext())
                {
                    var data = (from d in db.mUploadImages
                                orderby d.UploadImagesID descending
                                where d.ParentId == ParentId && d.Module == Module
                                select d).ToList();
                    return data;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




        public static object GetUploadImagesById(object param)
        {
            mUploadImages parameter = param as mUploadImages;

            UploadImagesContext db = new UploadImagesContext();

            var query = db.mUploadImages.SingleOrDefault(a => a.UploadImagesID == parameter.UploadImagesID);

            return query;
        }
    }
}
