﻿

namespace SBL.Domain.Context.AssemblyRemarks
{
    #region Namespace Reffrences
    using SBL.DAL;
    using SBL.DomainModel.Models.AssemblyRemarks;
    using SBL.DomainModel.Models.Assembly;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;


    #endregion
    public class AssemblyRemarksContext : DBBase<AssemblyRemarksContext>
    {
        public AssemblyRemarksContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public virtual DbSet<mAssemblyRemarks> mAssemblyRemarks { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateAssemblyRemarks": { CreateAssemblyRemarks(param.Parameter); break; }
                case "GetAllAssemblyRemarks": { return GetAllAssemblyRemarks(); }
                case "UpdateAsemblyRemarks": { return UpdateAsemblyRemarks(param.Parameter); }
                case "GetAssemblyRemarksBasedOnId": { return GetAssemblyRemarksBasedOnId(param.Parameter); }
                case "DeleteAssemblyRemarks": { return DeleteAssemblyRemarks(param.Parameter); }

            }
            return null;
        }

        static void CreateAssemblyRemarks(object param)
        {
            try
            {
                using (AssemblyRemarksContext db = new AssemblyRemarksContext())
                {
                    mAssemblyRemarks model = param as mAssemblyRemarks;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mAssemblyRemarks.Add(model);
                    db.SaveChanges();
                    db.Close();

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mAssemblyRemarks> GetAllAssemblyRemarks()
        {
            try
            {
                using (AssemblyRemarksContext db = new AssemblyRemarksContext())
                //{
                //    var Data = db.mAssemblyRemarks.Where(a => a.IsDeleted == null).OrderByDescending(a => a.AssemblyRemarksID).ToList();
                //    return Data;
                //}
                {
                    var data = (from assemre in db.mAssemblyRemarks
                                join Assem in db.mAssembly on assemre.AssemblyID equals Assem.AssemblyCode into JoinAssemblyName
                                from assemb in JoinAssemblyName.DefaultIfEmpty()

                                where assemre.IsDeleted == null
                                select new
                                {
                                    assemre,
                                    assemb.AssemblyName,
                                    

                                });
                    List<mAssemblyRemarks> list = new List<mAssemblyRemarks>();
                    foreach (var item in data)
                    {
                        item.assemre.GetAssemblyName = item.AssemblyName;

                        list.Add(item.assemre);
                    }
                    return list.OrderByDescending(s => s.AssemblyRemarksID).ToList();

                }





            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateAsemblyRemarks(object param)
        {
            try
            {
                using (AssemblyRemarksContext db = new AssemblyRemarksContext())
                {
                    mAssemblyRemarks model = param as mAssemblyRemarks;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mAssemblyRemarks.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllAssemblyRemarks();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static mAssemblyRemarks GetAssemblyRemarksBasedOnId(object param)
        {
            try
            {
                using (AssemblyRemarksContext db = new AssemblyRemarksContext())
                {
                    mAssemblyRemarks model = param as mAssemblyRemarks;
                    var data = db.mAssemblyRemarks.SingleOrDefault(a => a.AssemblyRemarksID == model.AssemblyRemarksID);
                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteAssemblyRemarks(object param)
        {
            try
            {
                using (AssemblyRemarksContext db = new AssemblyRemarksContext())
                {
                    mAssemblyRemarks model = param as mAssemblyRemarks;
                    mAssemblyRemarks data = db.mAssemblyRemarks.SingleOrDefault(a => a.AssemblyRemarksID == model.AssemblyRemarksID);
                    if (data != null)
                    {
                        data.IsDeleted = true;
                    }
                   // db.mAssemblyRemarks.Remove(data);
                    db.SaveChanges();
                    db.Close();


                }

                return GetAllAssemblyRemarks();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



    }
}
