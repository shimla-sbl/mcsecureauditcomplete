﻿using SBL.DAL;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.StaffManagement;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.eFileCommiteeMeetings
{
    public class eFileContext : DBBase<eFileContext>
    {
        public eFileContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<SBL.DomainModel.Models.eFile.eFile> eFiles { get; set; }
        public DbSet<SBL.DomainModel.Models.Department.mDepartment> departments { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.tCommittee> committees { get; set; }
        public DbSet<SBL.DomainModel.Models.Assembly.mAssembly> Assembly { get; set; }
        public DbSet<SBL.DomainModel.Models.Session.mSession> Sessions { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.mCommitteeType> committeeTypes { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFileAttachment> eFileAttachments { get; set; }
        public DbSet<SBL.DomainModel.Models.User.mUsers> users { get; set; }
        public DbSet<SBL.DomainModel.Models.Document.mDocumentType> documentType { get; set; }
        public DbSet<SBL.DomainModel.Models.Member.mMember> members { get; set; }
        public DbSet<SBL.DomainModel.Models.Office.mOffice> mOffice { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFilePaperNature> eFilePaperNature { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFilePaperType> eFilePaperType { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.COmmitteeProceeding> CommitteeProceeding { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.CommitteeMembersUpload> CommitteeMembersUpload { get; set; }
        public DbSet<SBL.DomainModel.Models.Member.mMemberAssembly> mMemberAssembly { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.TreeViewStructure> TreeViewStructure { get; set; }
        public DbSet<mStaff> mStaff { get; set; }
        public DbSet<SBL.DomainModel.Models.Constituency.mConstituency> mConstituency { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFileType> eFileType { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.CommitteeTypePermission> CommitteeTypePermission { get; set; }
        public DbSet<mBranches> mBranches { get; set; }

        //eFileNoting
        public DbSet<eFileNoting> eFileNoting { get; set; }
        public DbSet<tCommitteMeetingPapers> tCommitteMeetingPapers { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "GetCommitteUser": { return GetCommitteUser(param.Parameter); }
                case "AddeFile": { return AddeFile(param.Parameter); }
                case "GetAlleFile": { return GetAlleFile(param.Parameter); }
                case "GeteFile": { return GeteFile(param.Parameter); }
                case "GetCommitteeeFile": { return GetCommitteeeFile(param.Parameter); }
                case "AddeFileAttachments": return AddeFileAttachments(param.Parameter);
                case "GetAlleFileAttachments": return GetAlleFileAttachments(param.Parameter);
                case "GetAlleFileAttachmentsDep": return GetAlleFileAttachmentsDep(param.Parameter);
                case "GetAlleFileAttachmentsDep2": return GetAlleFileAttachmentsDep2(param.Parameter);
                case "GetAlleFileAttachmentsFromDep": return GetAlleFileAttachmentsFromDep(param.Parameter);
                case "GeteFileAttachmentDep": return GeteFileAttachmentDep(param.Parameter);
                case "GeteFileAttachment": return GeteFileAttachment(param.Parameter);
                case "GeteFileSearch": return GeteFileSearch(param.Parameter);
                case "EditeFile": return EditeFile(param.Parameter);
                case "eFileDocumentIndex": return eFileDocumentIndex(param.Parameter);
                case "GetDepartmentMembers": return GetDepartmentMembers(param.Parameter);
                case "GeteFileAttachmentonDepartment": return GeteFileAttachmentonDepartment(param.Parameter);
                case "GeteFileDepartmentReply": return GeteFileDepartmentReply(param.Parameter);
                case "eFileDeActive": { return FileDeActive(param.Parameter); }
                case "eFileActive": { return eFileActive(param.Parameter); }
                case "GeteFileReport": { return GeteFileReport(); }
                case "eFileAttachmentStatus": { return eFileAttachmentStatus(param.Parameter); }
                case "UpdateFileSendStatus": { return UpdateFileStatus(param.Parameter); }
                case "GeteFileDetailsByFileId": { return GeteFileDetailsByFileId(param.Parameter); }

                //shashi
                case "UpdateeFileNewDescriptionByFileId": { return UpdateeFileNewDescriptionByFileId(param.Parameter); }
                case "UpdateeFileDetailsByFileId": { return UpdateeFileDetailsByFileId(param.Parameter); }
                case "GetSendPaperDetailsByDeptId": { return GetSendPaperDetailsByDeptId(param.Parameter); }
                case "GetReceivePaperDetailsByDeptId": { return GetReceivePaperDetailsByDeptId(param.Parameter); }
                case "eFileList": { return eFileList(param.Parameter); }
                case "UpdateReceivePaperSingle": { return UpdateReceivePaperSingle(param.Parameter); }
                case "GetValuesByRefID": { return GetValuesByRefID(param.Parameter); }
                case "GetAlleFileAttachmentsByDeptIDandeFileID": { return GetAlleFileAttachmentsByDeptIDandeFileID(param.Parameter); }
                case "AttachPapertoEFileMul": { return AttachPapertoEFileMul(param.Parameter); }
                case "Get_eFilePaperNatureList": { return Get_eFilePaperNatureList(param.Parameter); }
                case "Get_eFilePaperTypeList": { return Get_eFilePaperTypeList(param.Parameter); }
                case "eFileListDeactive": { return eFileListDeactive(param.Parameter); }
                case "GetPaperDetailsByRefNo": { return GetPaperDetailsByRefNo(param.Parameter); }
                case "AttacheFiletoCommitteeMul": { return AttacheFiletoCommitteeMul(param.Parameter); }
                case "AddeFileNotings": { return AddeFileNotings(param.Parameter); }
                case "eFileListALLStatus": { return eFileListALLStatus(param.Parameter); }
                case "GetPNo": { return GetPNo(param.Parameter); }
                case "GeteFileNotingDetailsByeFileID": { return GeteFileNotingDetailsByeFileID(param.Parameter); }
                case "GeteNotingIDByeFileIdandSeqNo": { return GeteNotingIDByeFileIdandSeqNo(param.Parameter); }
                case "UpdateLasteFileNotings": { return UpdateLasteFileNotings(param.Parameter); }
                case "Get_eFilePaperNatureList_Receive": { return Get_eFilePaperNatureList_Receive(param.Parameter); }
                case "GetCommitteeProoceedingByDept": { return GetCommitteeProoceedingByDept(param.Parameter); }
                case "GetCommitteeUploadedPdf": { return GetCommitteeUploadedPdf(param.Parameter); }
                case "GetTreeViewStructure": { return GetTreeViewStructure(param.Parameter); }
                case "eFileListByCommitteeId": { return eFileListByCommitteeId(param.Parameter); }
                case "eFileListDeactiveByCommitteeId": { return eFileListDeactiveByCommitteeId(param.Parameter); }
                case "GetMembersByAssemblyID": { return GetMembersByAssemblyID(param.Parameter); }
                case "GetStaffDetails": { return GetStaffDetails(param.Parameter); }
                case "eFileTypeDetails": { return eFileTypeDetails(param.Parameter); }
                case "GetGeneratedPdfList": { return GetGeneratedPdfList(param.Parameter); }
                case "AttachPapertoEFileMulR": { return AttachPapertoEFileMulR(param.Parameter); }
                case "saveCommitteeProceeding": { return saveCommitteeProceeding(param.Parameter); }
                case "GetAlleFileAttachmentsByeFileID": { return GetAlleFileAttachmentsByeFileID(param.Parameter); }
                case "CheckUpdateDiary": { return CheckUpdateDiary(param.Parameter); }
                case "GBranchList": { return GBranchList(param.Parameter); }
                //case "BranchListCom": { return BranchListCom(param.Parameter); }


                // New Rquirements
                case "GetReceiveSentPaperDetailsByDeptId": { return GetReceiveSentPaperDetailsByDeptId(param.Parameter); }
                case "GetReceivePapersListByDeptIdForMeeting": { return GetReceivePapersListByDeptIdForMeeting(param.Parameter); }
                case "GetSentPapersListByDeptIdForMeeting": { return GetSentPapersListByDeptIdForMeeting(param.Parameter); }
                case "GetSentReceivePaperDetailsByDeptId": { return GetSentReceivePaperDetailsByDeptId(param.Parameter); }
                case "GetReceiveSentPaperDetailsByPaperstIds": { return GetReceiveSentPaperDetailsByPaperstIds(param.Parameter); }
                case "GetCommiteeMettingAllData": { return GetCommiteeMettingAllData(param.Parameter); }
                case "SaveNewDateForMeetingByPaperstIds": { return SaveNewDateForMeetingByPaperstIds(param.Parameter); }
                case "DeleteItemsForMeetingbyDate": { return DeleteItemsForMeetingbyDate(param.Parameter); }
                case "CancelItemsForMeetingbyDate": { return CancelItemsForMeetingbyDate(param.Parameter); }
                case "GetCommiteeMettingAllDataByYear": { return GetCommiteeMettingAllDataByYear(param.Parameter); }
                case "GetDateDataForMeeting": { return GetDateDataForMeeting(param.Parameter); }
                case "PhosponedDateForMeetingByPaperstIds": { return PhosponedDateForMeetingByPaperstIds(param.Parameter); }
                case "SaveManualsValuesForMeetingsByDate": { return SaveManualsValuesForMeetingsByDate(param.Parameter); }
                case "GetItemsForMeetingbyDate": { return GetItemsForMeetingbyDate(param.Parameter); }
                case "TransferReceiveSentPaperByPaperstIds": { return TransferReceiveSentPaperByPaperstIds(param.Parameter); }

                //
            }
            return null;
        }


        #region Receive Paper List

        public static object GetReceiveSentPaperDetailsByDeptId(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false //&& a.ToOfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = EFile.eFileNumber,
                                     DiaryNo = a.DiaryNo
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }

        #endregion Receive Paper List


        #region Receive Paper List for Meetings

        public static object GetReceivePapersListByDeptIdForMeeting(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            string strBranchId = Convert.ToString(str[2]);
            string PTypePending = Convert.ToString(str[3]);
            int BrachId = 0;
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (!string.IsNullOrEmpty(strBranchId))
            {
                BrachId = Convert.ToInt32(strBranchId);
            }

            using (eFileContext ctx = new eFileContext())
            {
                if (PTypePending == "Pending")
                {

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.BranchId == BrachId && a.AssignedDate == null
                                 select a
                            );

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {


                            using (eFileContext ctxInner = new eFileContext())
                            {
                                var queryCountResult = (from a in ctxInner.tCommitteMeetingPapers
                                                        where a.PapersId == item.PapersId
                                                        select a.Id).Count();
                                if (queryCountResult == 1)
                                {
                                    var queryResult = (from eFile in ctxInner.eFileAttachments
                                                       join b in ctxInner.departments on eFile.DepartmentId equals b.deptId into temp
                                                       from ps in temp.DefaultIfEmpty()
                                                       //join EFile in ctxInner.eFiles on eFile.ReFileID equals EFile.eFileID
                                                       where eFile.eFileAttachmentId == item.PapersId
                                                       select new
                                                       {
                                                           eFileAttachmentId = eFile.eFileAttachmentId,
                                                           Department = ps.deptname ?? "OTHER",
                                                           eFileName = eFile.eFileName,
                                                           eFilePath = eFile.eFilePath,
                                                           Title = eFile.Title,
                                                           Description = eFile.Description,
                                                           PaperStatus = eFile.PaperStatus,
                                                           eFIleID = eFile.eFileID,
                                                           PaperRefNo = eFile.PaperRefNo,
                                                           RevedOption = eFile.RevedOption,
                                                           RecvDetails = eFile.RecvDetails,
                                                           ToRecvDetails = eFile.ToRecvDetails,
                                                           eMode = eFile.eMode,
                                                           PType = eFile.PType,
                                                           SendStatus = eFile.SendStatus,
                                                           DeptAbbr = ps.deptabbr,
                                                           DepartmentName = ps.deptname,
                                                           eFilePathWord = eFile.eFilePathWord,
                                                           ReFileID = eFile.ReFileID,
                                                           //FileName = EFile.eFileNumber

                                                       }).FirstOrDefault();


                                    if (queryResult != null)
                                    {
                                        CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                                        obj.Id = item.Id;
                                        obj.PapersId = item.PapersId;
                                        obj.RefNo = item.RefNo;
                                        obj.Subject = item.Subject;
                                        obj.AssignedDate = item.AssignedDate != null ? item.AssignedDate : (DateTime?)null;
                                        obj.StrAssignedDate = item.AssignedDate != null ? Convert.ToString(item.AssignedDate.Value.Date) : "";
                                        obj.PdfPath = item.PdfPath;
                                        obj.ReFileID = queryResult.ReFileID;
                                        obj.eFileID = queryResult.eFIleID;
                                        //obj.MainEFileName = queryResult.FileName;
                                        obj.eFileName = queryResult.eFileName;
                                        obj.PaperRefNo = queryResult.PaperRefNo;
                                        obj.RecvDetails = queryResult.RecvDetails;
                                        obj.DepartmentName = queryResult.Department;
                                        obj.DeptAbbr = queryResult.DeptAbbr;
                                        obj.eFilePath = queryResult.eFilePath;
                                        obj.eFilePathWord = queryResult.eFilePathWord;
                                        obj.eFileAttachmentId = item.Id;
                                        obj.AssignAllDates = string.Join(", ", ctxInner.tCommitteMeetingPapers.Where(p => p.PapersId == item.PapersId && p.AssignedDate != null).Select(p => (p.AssignedDate.Value.Day.ToString() + "/" + p.AssignedDate.Value.Month.ToString() + "/" + p.AssignedDate.Value.Year.ToString())));
                                        //(from t in ctxInner.tCommitteMeetingPapers where t.AssignedDate != null select t.AssignedDate).FirstOrDefault(); 

                                        lst.Add(obj);
                                    }

                                    ctxInner.Close();
                                }
                            }

                        }

                        return lst.ToList();
                    }
                    return query.ToList();

                }
                else if (PTypePending == "Fixed")
                {

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.BranchId == BrachId
                                 && (a.AssignedDate == null || a.PapersId == 0)
                                 select a
                            );

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {

                            using (eFileContext ctxInner = new eFileContext())
                            {
                                var queryResult = (from eFile in ctxInner.eFileAttachments
                                                   join b in ctxInner.departments on eFile.DepartmentId equals b.deptId into temp
                                                   from ps in temp.DefaultIfEmpty()
                                                   //join EFile in ctxInner.eFiles on eFile.ReFileID equals EFile.eFileID
                                                   where eFile.eFileAttachmentId == item.PapersId
                                                   select new
                                                   {
                                                       eFileAttachmentId = eFile.eFileAttachmentId,
                                                       Department = ps.deptname ?? "OTHER",
                                                       eFileName = eFile.eFileName,
                                                       eFilePath = eFile.eFilePath,
                                                       Title = eFile.Title,
                                                       Description = eFile.Description,
                                                       PaperStatus = eFile.PaperStatus,
                                                       eFIleID = eFile.eFileID,
                                                       PaperRefNo = eFile.PaperRefNo,
                                                       RevedOption = eFile.RevedOption,
                                                       RecvDetails = eFile.RecvDetails,
                                                       ToRecvDetails = eFile.ToRecvDetails,
                                                       eMode = eFile.eMode,
                                                       PType = eFile.PType,
                                                       SendStatus = eFile.SendStatus,
                                                       DeptAbbr = ps.deptabbr,
                                                       DepartmentName = ps.deptname,
                                                       eFilePathWord = eFile.eFilePathWord,
                                                       ReFileID = eFile.ReFileID,
                                                       //FileName = EFile.eFileNumber

                                                   }).FirstOrDefault();



                                CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                                obj.Id = item.Id;
                                obj.PapersId = item.PapersId;
                                obj.RefNo = item.RefNo;
                                obj.Subject = item.Subject;
                                obj.AssignedDate = item.AssignedDate != null ? item.AssignedDate : (DateTime?)null;
                                obj.StrAssignedDate = item.AssignedDate != null ? Convert.ToString(item.AssignedDate.Value.Date) : "";
                                obj.PdfPath = item.PdfPath;
                                obj.eFileAttachmentId = item.Id;
                                obj.AssignAllDates = string.Join(", ", ctxInner.tCommitteMeetingPapers.Where(p => p.PapersId == item.PapersId && p.AssignedDate != null).Select(p => (p.AssignedDate.Value.Day.ToString() + "/" + p.AssignedDate.Value.Month.ToString() + "/" + p.AssignedDate.Value.Year.ToString())));
                                if (queryResult != null)
                                {
                                    obj.ReFileID = queryResult.ReFileID;
                                    obj.eFileID = queryResult.eFIleID;
                                   // obj.MainEFileName = queryResult.FileName;
                                    obj.eFileName = queryResult.eFileName;
                                    obj.PaperRefNo = queryResult.PaperRefNo;
                                    obj.RecvDetails = queryResult.RecvDetails;
                                    obj.DepartmentName = queryResult.Department;
                                    obj.DeptAbbr = queryResult.DeptAbbr;
                                    obj.eFilePath = queryResult.eFilePath;
                                    obj.eFilePathWord = queryResult.eFilePathWord;
                                }
                                //(from t in ctxInner.tCommitteMeetingPapers where t.AssignedDate != null select t.AssignedDate).FirstOrDefault(); 

                                lst.Add(obj);


                                ctxInner.Close();
                            }

                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }
                else
                {
                    string strDateForFolderStructure = "";
                    string strDay = "";
                    string strMonth="";
                    string strYear = "";
                    int day = (Convert.ToInt32(PTypePending.Substring(0, 2)));
                    int month = (Convert.ToInt32(PTypePending.Substring(3, 2)));
                    int year = (Convert.ToInt32(PTypePending.Substring(6, 4)));
                    strDay = Convert.ToString(day);
                    strMonth = Convert.ToString(month);
                    strYear = Convert.ToString(year);

                    if (strDay.Length == 1)
                        strDay = "0" + strDay;
                    if (strMonth.Length == 1)
                        strMonth = "0" + strMonth;
                    strDateForFolderStructure = strDay + " " + strMonth + " " + strYear;


                    DateTime dtSelected = new DateTime(year, month, day);
                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.BranchId == BrachId && a.AssignedDate == dtSelected
                                 select a
                            );

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {

                            using (eFileContext ctxInner = new eFileContext())
                            {
                                var queryResult = (from eFile in ctxInner.eFileAttachments
                                                   join b in ctxInner.departments on eFile.DepartmentId equals b.deptId into temp
                                                   from ps in temp.DefaultIfEmpty()
                                                   //join EFile in ctxInner.eFiles on eFile.ReFileID equals EFile.eFileID
                                                   where eFile.eFileAttachmentId == item.PapersId
                                                   select new
                                                   {
                                                       eFileAttachmentId = eFile.eFileAttachmentId,
                                                       Department = ps.deptname ?? "OTHER",
                                                       eFileName = eFile.eFileName,
                                                       eFilePath = eFile.eFilePath,
                                                       Title = eFile.Title,
                                                       Description = eFile.Description,
                                                       PaperStatus = eFile.PaperStatus,
                                                       eFIleID = eFile.eFileID,
                                                       PaperRefNo = eFile.PaperRefNo,
                                                       RevedOption = eFile.RevedOption,
                                                       RecvDetails = eFile.RecvDetails,
                                                       ToRecvDetails = eFile.ToRecvDetails,
                                                       eMode = eFile.eMode,
                                                       PType = eFile.PType,
                                                       SendStatus = eFile.SendStatus,
                                                       DeptAbbr = ps.deptabbr,
                                                       DepartmentName = ps.deptname,
                                                       eFilePathWord = eFile.eFilePathWord,
                                                       ReFileID = eFile.ReFileID,
                                                       //FileName = EFile.eFileNumber

                                                   }).FirstOrDefault();



                                CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                                obj.Id = item.Id;
                                obj.PapersId = item.PapersId;
                                obj.RefNo = item.RefNo;
                                obj.Subject = item.Subject;
                                obj.AssignedDate = item.AssignedDate != null ? item.AssignedDate : (DateTime?)null;
                                obj.StrAssignedDate = item.AssignedDate != null ? Convert.ToString(item.AssignedDate.Value.Date.Day.ToString() + "/" + item.AssignedDate.Value.Date.Month.ToString() + "/" + item.AssignedDate.Value.Date.Year.ToString()) : "";
                                obj.PdfPath = item.PdfPath;
                                obj.eFilePath = "/ePaper/MeetingPapers/" + strDateForFolderStructure + "/" + item.PdfPath;
                                obj.eFileAttachmentId = item.Id;
                                obj.AssignAllDates = item.AssignedDate != null ? Convert.ToString(item.AssignedDate.Value.Date.Day.ToString() + "/" + item.AssignedDate.Value.Date.Month.ToString() + "/" + item.AssignedDate.Value.Date.Year.ToString()) : "";
                                if (queryResult != null)
                                {
                                    obj.ReFileID = queryResult.ReFileID;
                                    obj.eFileID = queryResult.eFIleID;
                                    //obj.MainEFileName = queryResult.FileName;
                                    obj.eFileName = queryResult.eFileName;
                                    obj.PaperRefNo = queryResult.PaperRefNo;
                                    obj.RecvDetails = queryResult.RecvDetails;
                                    obj.DepartmentName = queryResult.Department;
                                    obj.DeptAbbr = queryResult.DeptAbbr;                                    
                                    obj.eFilePathWord = queryResult.eFilePathWord;
                                }

                                //(from t in ctxInner.tCommitteMeetingPapers where t.AssignedDate != null select t.AssignedDate).FirstOrDefault(); 

                                lst.Add(obj);


                                ctxInner.Close();
                            }

                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }



            }

        }

        #endregion Receive Paper List for Meetings

        #region Sent Paper List for Meetings

        public static object GetSentPapersListByDeptIdForMeeting(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            string strBranchId = Convert.ToString(str[2]);
            int BrachId = 0;
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (!string.IsNullOrEmpty(strBranchId))
            {
                BrachId = Convert.ToInt32(strBranchId);
            }

            using (eFileContext ctx = new eFileContext())
            {
                var query = (from a in ctx.tCommitteMeetingPapers
                             where a.PaperType == "Sent" && a.BranchId == BrachId
                             select a
                             );

                if (query != null)
                {
                    List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                    foreach (var item in query)
                    {

                        using (eFileContext ctxInner = new eFileContext())
                        {
                            var queryResult = (from eFile in ctxInner.eFileAttachments
                                               join b in ctxInner.departments on eFile.DepartmentId equals b.deptId into temp
                                               from ps in temp.DefaultIfEmpty()
                                               join EFile in ctxInner.eFiles on eFile.ReFileID equals EFile.eFileID
                                               where eFile.eFileAttachmentId == item.PapersId
                                               select new
                                               {
                                                   eFileAttachmentId = eFile.eFileAttachmentId,
                                                   Department = ps.deptname ?? "OTHER",
                                                   eFileName = eFile.eFileName,
                                                   eFilePath = eFile.eFilePath,
                                                   Title = eFile.Title,
                                                   Description = eFile.Description,
                                                   PaperStatus = eFile.PaperStatus,
                                                   eFIleID = eFile.eFileID,
                                                   PaperRefNo = eFile.PaperRefNo,
                                                   RevedOption = eFile.RevedOption,
                                                   RecvDetails = eFile.RecvDetails,
                                                   ToRecvDetails = eFile.ToRecvDetails,
                                                   eMode = eFile.eMode,
                                                   PType = eFile.PType,
                                                   SendStatus = eFile.SendStatus,
                                                   DeptAbbr = ps.deptabbr,
                                                   DepartmentName = ps.deptname,
                                                   eFilePathWord = eFile.eFilePathWord,
                                                   ReFileID = eFile.ReFileID,
                                                   FileName = EFile.eFileNumber

                                               }).FirstOrDefault();

                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            obj.Id = item.Id;
                            obj.PapersId = item.PapersId;
                            obj.RefNo = item.RefNo;
                            obj.Subject = item.Subject;
                            obj.AssignedDate = item.AssignedDate != null ? item.AssignedDate : (DateTime?)null;
                            obj.StrAssignedDate = item.AssignedDate != null ? Convert.ToString(item.AssignedDate.Value.Date) : "";
                            obj.PdfPath = item.PdfPath;
                            if (queryResult != null)
                            {
                                obj.ReFileID = queryResult.ReFileID;
                                obj.eFileID = queryResult.eFIleID;
                                obj.MainEFileName = queryResult.FileName;
                                obj.eFileName = queryResult.eFileName;
                                obj.PaperRefNo = queryResult.PaperRefNo;
                                obj.RecvDetails = queryResult.RecvDetails;
                                obj.DepartmentName = queryResult.Department;
                                obj.DeptAbbr = queryResult.DeptAbbr;
                                obj.eFilePath = queryResult.eFilePath;
                                obj.eFilePathWord = queryResult.eFilePathWord;
                                obj.eFileAttachmentId = queryResult.eFileAttachmentId;
                            }
                            lst.Add(obj);
                            ctxInner.Close();
                        }

                    }

                    return lst.ToList();
                }
                return query.ToList();
            }

        }

        #endregion Sent Paper List for Meetings

        #region Sent Paper List
        public static object GetSentReceivePaperDetailsByDeptId(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.ToDepartmentID equals b.deptId
                                 into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.eFileID equals EFile.eFileID
                                 join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID

                                 where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false //&& a.OfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = EFile.eFileNumber,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     ToDepartmentId = a.ToDepartmentID,
                                     SendDate = a.SendDate,
                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.SendDate = item.SendDate;
                        mdl.ToDepartmentID = item.ToDepartmentId;
                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;
                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.ToDepartmentID equals b.deptId
                                 into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID

                                 where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false //&& a.OfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     ToDepartmentId = a.ToDepartmentID,
                                     SendDate = a.SendDate,
                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.SendDate = item.SendDate;
                        mdl.ToDepartmentID = item.ToDepartmentId;
                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;
                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }
        #endregion Sent Paper List

        #region Paper List by Papers Ids
        public static object GetReceiveSentPaperDetailsByPaperstIds(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            string PapersIds = Convert.ToString(str[2]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (!string.IsNullOrEmpty(PapersIds))
            {
                PapersIds = PapersIds.TrimEnd(',');
            }

            if (PapersIds != "")
            {
                List<int> TagIds = PapersIds.Split(',').Select(int.Parse).ToList();

                if (Officecode == 0)
                {
                    using (eFileContext ctx = new eFileContext())
                    {
                        var query = (from a in ctx.eFileAttachments
                                     where TagIds.Contains(a.eFileAttachmentId) //&& a.OfficeCode == Officecode
                                     select new
                                     {
                                         eFileAttachmentId = a.eFileAttachmentId,
                                         PaperNature = a.PaperNature,
                                         PaperNatureDays = a.PaperNatureDays ?? "0",
                                         eFilePath = a.eFilePath,
                                         Title = a.Title,
                                         Description = a.Description,
                                         CreateDate = a.CreatedDate,
                                         PaperStatus = a.PaperStatus,
                                         eFIleID = a.eFileID,
                                         PaperRefNo = a.PaperRefNo,
                                         ToDepartmentId = a.ToDepartmentID,
                                         SendDate = a.SendDate,
                                         ToPaperNature = a.ToPaperNature,
                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                         LinkedRefNo = a.LinkedRefNo,
                                         RevedOption = a.RevedOption,
                                         RecvDetails = a.RecvDetails,
                                         ToRecvDetails = a.ToRecvDetails,
                                         eMode = a.eMode,
                                         eFilePathWord = a.eFilePathWord
                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                        List<eFileAttachment> lst = new List<eFileAttachment>();
                        foreach (var item in query)
                        {
                            eFileAttachment mdl = new eFileAttachment();
                            mdl.eFileAttachmentId = item.eFileAttachmentId;
                            mdl.PaperNature = item.PaperNature;
                            mdl.PaperNatureDays = item.PaperNatureDays;
                            mdl.eFilePath = item.eFilePath;
                            mdl.Title = item.Title;
                            mdl.Description = item.Description;
                            mdl.CreatedDate = item.CreateDate;
                            mdl.PaperStatus = item.PaperStatus;
                            mdl.eFileID = item.eFIleID;
                            mdl.PaperRefNo = item.PaperRefNo;
                            mdl.SendDate = item.SendDate;
                            mdl.ToDepartmentID = item.ToDepartmentId;
                            mdl.ToPaperNature = item.ToPaperNature;
                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                            mdl.LinkedRefNo = item.LinkedRefNo;
                            mdl.RevedOption = item.RevedOption;
                            mdl.RecvDetails = item.RecvDetails;
                            mdl.ToRecvDetails = item.ToRecvDetails;
                            mdl.eMode = item.eMode;
                            mdl.eFilePathWord = item.eFilePathWord;
                            lst.Add(mdl);
                        }

                        return lst;
                    }
                }
                else
                {
                    using (eFileContext ctx = new eFileContext())
                    {
                        var query = (from a in ctx.eFileAttachments
                                     join b in ctx.departments on a.ToDepartmentID equals b.deptId
                                     into temp
                                     from ps in temp.DefaultIfEmpty()
                                     join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID

                                     where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false //&& a.OfficeCode == Officecode
                                     select new
                                     {
                                         eFileAttachmentId = a.eFileAttachmentId,
                                         Department = ps.deptname ?? "OTHER",
                                         eFileName = a.eFileName,
                                         PaperNature = a.PaperNature,
                                         PaperNatureDays = a.PaperNatureDays ?? "0",
                                         eFilePath = a.eFilePath,
                                         Title = a.Title,
                                         Description = a.Description,
                                         CreateDate = a.CreatedDate,
                                         PaperStatus = a.PaperStatus,
                                         eFIleID = a.eFileID,
                                         PaperRefNo = a.PaperRefNo,
                                         ToDepartmentId = a.ToDepartmentID,
                                         SendDate = a.SendDate,
                                         ToPaperNature = a.ToPaperNature,
                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                         LinkedRefNo = a.LinkedRefNo,
                                         RevedOption = a.RevedOption,
                                         RecvDetails = a.RecvDetails,
                                         ToRecvDetails = a.ToRecvDetails,
                                         eMode = a.eMode,
                                         RecvdPaperNature = c.PaperNature,
                                         DocuemntPaperType = d.PaperType,
                                         DeptAbbr = ps.deptabbr,
                                         eFilePathWord = a.eFilePathWord
                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                        List<eFileAttachment> lst = new List<eFileAttachment>();
                        foreach (var item in query)
                        {
                            eFileAttachment mdl = new eFileAttachment();
                            mdl.DepartmentName = item.Department;
                            mdl.eFileAttachmentId = item.eFileAttachmentId;
                            mdl.eFileName = item.eFileName;
                            mdl.PaperNature = item.PaperNature;
                            mdl.PaperNatureDays = item.PaperNatureDays;
                            mdl.eFilePath = item.eFilePath;
                            mdl.Title = item.Title;
                            mdl.Description = item.Description;
                            mdl.CreatedDate = item.CreateDate;
                            mdl.PaperStatus = item.PaperStatus;
                            mdl.eFileID = item.eFIleID;
                            mdl.PaperRefNo = item.PaperRefNo;
                            mdl.SendDate = item.SendDate;
                            mdl.ToDepartmentID = item.ToDepartmentId;
                            mdl.ToPaperNature = item.ToPaperNature;
                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                            mdl.LinkedRefNo = item.LinkedRefNo;
                            mdl.RevedOption = item.RevedOption;
                            mdl.RecvDetails = item.RecvDetails;
                            mdl.ToRecvDetails = item.ToRecvDetails;
                            mdl.eMode = item.eMode;
                            mdl.RecvdPaperNature = item.RecvdPaperNature;
                            mdl.DocuemntPaperType = item.DocuemntPaperType;
                            mdl.DeptAbbr = item.DeptAbbr;
                            mdl.eFilePathWord = item.eFilePathWord;
                            lst.Add(mdl);
                        }

                        return lst;
                    }
                }
            }
            else
            {
                return null;
            }
        }
        #endregion Sent Paper List


        #region Transfer Paper List by Papers Ids
        public static object TransferReceiveSentPaperByPaperstIds(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            string PapersIds = Convert.ToString(str[2]);
            string PaperType = Convert.ToString(str[3]);
            string AadharId = Convert.ToString(str[4]);
            string StrBranchID = Convert.ToString(str[5]);
            int BranchId = 0;
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();

            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (!string.IsNullOrEmpty(PapersIds))
            {
                PapersIds = PapersIds.TrimEnd(',');
            }
            if (!string.IsNullOrEmpty(StrBranchID))
            {
                BranchId = Convert.ToInt32(StrBranchID);
            }
            List<int> TagIds = PapersIds.Split(',').Select(int.Parse).ToList();
            try
            {
                using (eFileContext ctx = new eFileContext())
                {
                    foreach (var item in TagIds)
                    {
                        var queryResult = (from a in ctx.eFileAttachments where a.eFileAttachmentId == item select a).SingleOrDefault();
                        var dublicateData = (from a in ctx.tCommitteMeetingPapers where a.PapersId == item select a).SingleOrDefault();
                        tCommitteMeetingPapers objCommitteMeetingPapers = new tCommitteMeetingPapers();
                        objCommitteMeetingPapers.PapersId = item;
                        objCommitteMeetingPapers.Subject = queryResult.Title;
                        objCommitteMeetingPapers.PaperType = PaperType;
                        objCommitteMeetingPapers.RefNo = queryResult.PaperRefNo;
                        if (queryResult.eFilePath != null && queryResult.eFilePath != "")
                        {
                            objCommitteMeetingPapers.PdfPath = queryResult.eFilePath.Substring(queryResult.eFilePath.LastIndexOf("/") + 1);
                        }
                        else
                        {
                            objCommitteMeetingPapers.PdfPath = "";
                        }
                        objCommitteMeetingPapers.CreateDate = System.DateTime.Now;
                        objCommitteMeetingPapers.CreatedBy = AadharId;
                        objCommitteMeetingPapers.BranchId = BranchId;
                        if (dublicateData == null)
                        {
                            ctx.tCommitteMeetingPapers.Add(objCommitteMeetingPapers);
                            ctx.SaveChanges();
                        }

                    }
                }
                return "Success";
            }
            catch (Exception)
            {

                return "fail";
            }


        }
        #endregion Transfer Paper List by Papers Ids

        #region Get Committee Meeting All Data
        public static object GetCommiteeMettingAllData(object param)
        {
            try
            {
                using (eFileContext ctx = new eFileContext())
                {

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 select a.AssignedDate
                                 ).Distinct();

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {
                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            string day = Convert.ToString(item.Value.Date.Day);
                            string month = Convert.ToString(item.Value.Date.Month);
                            string year = Convert.ToString(item.Value.Date.Year);
                            if (day.Length == 1)
                                day = "0" + day;
                            if (month.Length == 1)
                                month = "0" + month;
                            obj.StrAssignedDate = day + "/" + month + "/" + year;
                            lst.Add(obj);
                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Get Committee Meeting All Data

        #region Get Committee Meeting All Data by Year
        public static object GetCommiteeMettingAllDataByYear(object param)
        {
            string[] str = param as string[];
            int selectedYear = Convert.ToInt16(Convert.ToString(str[0]));
            int selectedBranch = Convert.ToInt16(Convert.ToString(str[1]));
            try
            {
                using (eFileContext ctx = new eFileContext())
                {

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.AssignedDate.Value.Year == selectedYear && a.BranchId == selectedBranch
                                 select a.AssignedDate
                                 ).Distinct();

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {
                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            string day = Convert.ToString(item.Value.Date.Day);
                            string month = Convert.ToString(item.Value.Date.Month);
                            string year = Convert.ToString(item.Value.Date.Year);
                            if (day.Length == 1)
                                day = "0" + day;
                            if (month.Length == 1)
                                month = "0" + month;
                            obj.StrAssignedDate = day + "/" + month + "/" + year;
                            lst.Add(obj);
                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Get Committee Meeting All Data

        #region Save And Get Committee Meeting All Data
        public static object SaveNewDateForMeetingByPaperstIds(object param)
        {


            string[] str = param as string[];
            string AssignedDate = Convert.ToString(str[0]);
            string PapersIds = Convert.ToString(str[1]);
            string AadharId = Convert.ToString(str[2]);
            string strBranchId = Convert.ToString(str[3]);
            int BranchId = 0;
            List<string> Paperslist = new List<string>();
            if (!string.IsNullOrEmpty(PapersIds))
            {
                PapersIds = PapersIds.TrimEnd(',');
                Paperslist = PapersIds.Split(',').Distinct().ToList();
            }
            if (!string.IsNullOrEmpty(strBranchId))
            {
                BranchId = Convert.ToInt32(strBranchId);
            }

            int day = (Convert.ToInt32(AssignedDate.Substring(0, 2)));
            int month = (Convert.ToInt32(AssignedDate.Substring(3, 2)));
            int year = (Convert.ToInt32(AssignedDate.Substring(6, 4)));

            DateTime dt = new DateTime(year, month, day);

            //Save Data

            try
            {
                using (eFileContext ctx = new eFileContext())
                {

                    if (Paperslist.Count > 0)
                    {

                        foreach (var item in Paperslist)
                        {
                            tCommitteMeetingPapers objCommitteMeetingPapers = new tCommitteMeetingPapers();
                            int PaperId = Convert.ToInt32(item);

                            var ManPaperID = (from a in ctx.tCommitteMeetingPapers where a.Id == PaperId select a).SingleOrDefault();


                            var dublicateData = (from a in ctx.tCommitteMeetingPapers where a.Id == PaperId && a.AssignedDate == dt && a.BranchId == BranchId select a).SingleOrDefault();

                            if (ManPaperID != null)
                            {

                                objCommitteMeetingPapers.PapersId = Convert.ToInt32(ManPaperID.PapersId);
                                objCommitteMeetingPapers.Subject = ManPaperID.Subject;
                                objCommitteMeetingPapers.RefNo = ManPaperID.RefNo;
                                objCommitteMeetingPapers.AssignedDate = dt;
                                objCommitteMeetingPapers.PdfPath = ManPaperID.PdfPath;
                            }
                            var queryResult = (from a in ctx.eFileAttachments where a.eFileAttachmentId == ManPaperID.PapersId select a).SingleOrDefault();
                            if (queryResult != null)
                            {
                                if (queryResult.eFilePath != null && queryResult.eFilePath != "")
                                {
                                    objCommitteMeetingPapers.PdfPath = queryResult.eFilePath.Substring(queryResult.eFilePath.LastIndexOf("/") + 1);
                                }
                            }

                            objCommitteMeetingPapers.CreateDate = System.DateTime.Now;
                            objCommitteMeetingPapers.CreatedBy = AadharId;
                            objCommitteMeetingPapers.BranchId = BranchId;
                            if (dublicateData == null)
                            {
                                ctx.tCommitteMeetingPapers.Add(objCommitteMeetingPapers);
                                ctx.SaveChanges();
                            }
                        }

                    }



                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.AssignedDate == dt && a.BranchId == BranchId
                                 select a
                                 );

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {
                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            obj.Id = item.Id;
                            obj.PapersId = item.PapersId;
                            obj.RefNo = item.RefNo;
                            obj.Subject = item.Subject;
                            obj.AssignedDate = item.AssignedDate;
                            obj.StrAssignedDate = Convert.ToString(item.AssignedDate.Value.Date);
                            obj.PdfPath = item.PdfPath;
                            lst.Add(obj);
                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }


            }
            catch
            {
                throw;
            }
        }

        #endregion Save And Get Committee Meeting All Data

        #region Phosponed And Get Committee Meeting All Data By Date
        public static object PhosponedDateForMeetingByPaperstIds(object param)
        {


            string[] str = param as string[];
            string AssignedDate = Convert.ToString(str[0]);
            string PapersIds = Convert.ToString(str[1]);
            string AadharId = Convert.ToString(str[2]);
            List<string> Paperslist = new List<string>();
            if (!string.IsNullOrEmpty(PapersIds))
            {
                PapersIds = PapersIds.TrimEnd(',');
                Paperslist = PapersIds.Split(',').Distinct().ToList();
            }

            int day = (Convert.ToInt32(AssignedDate.Substring(0, 2)));
            int month = (Convert.ToInt32(AssignedDate.Substring(3, 2)));
            int year = (Convert.ToInt32(AssignedDate.Substring(6, 4)));

            DateTime dt = new DateTime(year, month, day);

            //Save Data

            try
            {
                using (eFileContext ctx = new eFileContext())
                {
                    if (Paperslist.Count > 0)
                    {

                        foreach (var item in Paperslist)
                        {
                            int PaperId = Convert.ToInt32(item);
                            using (var obj = new eFileContext())
                            {
                                var ManPaperID = (from a in ctx.tCommitteMeetingPapers where a.Id == PaperId select a.PapersId).SingleOrDefault();
                                var dublicateEntryData = (from a in obj.tCommitteMeetingPapers where a.PapersId == ManPaperID && a.AssignedDate == dt select a).FirstOrDefault();

                                if (dublicateEntryData == null)
                                {
                                    var dublicateData = (from a in obj.tCommitteMeetingPapers where a.Id == PaperId select a).FirstOrDefault();
                                    if (dublicateData != null)
                                    {
                                        dublicateData.AssignedDate = dt;
                                    }
                                    obj.Entry(dublicateData).State = EntityState.Modified;
                                    obj.SaveChanges();
                                    obj.Close();
                                }
                            }

                        }
                    }

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.AssignedDate == dt
                                 select a
                                 );

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {
                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            obj.Id = item.Id;
                            obj.PapersId = item.PapersId;
                            obj.RefNo = item.RefNo;
                            obj.Subject = item.Subject;
                            obj.AssignedDate = item.AssignedDate;
                            obj.StrAssignedDate = Convert.ToString(item.AssignedDate.Value.Date);
                            obj.PdfPath = item.PdfPath;
                            lst.Add(obj);
                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Save And Get Committee Meeting All Data

        #region Delete And Get Committee Meeting All Data
        public static object DeleteItemsForMeetingbyDate(object param)
        {


            string[] str = param as string[];
            string PapersIds = Convert.ToString(str[0]);
            string AadharId = Convert.ToString(str[1]);
            List<string> Paperslist = new List<string>();
            if (!string.IsNullOrEmpty(PapersIds))
            {
                PapersIds = PapersIds.TrimEnd(',');
                Paperslist = PapersIds.Split(',').Distinct().ToList();
            }


            try
            {
                using (eFileContext ctx = new eFileContext())
                {


                    if (Paperslist.Count > 0)
                    {


                        foreach (var item in Paperslist)
                        {
                            tCommitteMeetingPapers objCommitteMeetingPapers = new tCommitteMeetingPapers();
                            int PaperId = Convert.ToInt32(item);
                            var dublicateData = (from a in ctx.tCommitteMeetingPapers where a.Id == PaperId select a).SingleOrDefault();
                            if (dublicateData != null)
                            {
                                ctx.tCommitteMeetingPapers.Remove(dublicateData);
                                ctx.SaveChanges();

                            }

                        }
                    }

                    return null;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Delete And Get Committee Meeting All

        #region Cancel And Get Committee Meeting All Data
        public static object CancelItemsForMeetingbyDate(object param)
        {


            string[] str = param as string[];
            string AssignedDate = Convert.ToString(str[0]);
            string PapersIds = Convert.ToString(str[1]);
            string AadharId = Convert.ToString(str[2]);
            List<string> Paperslist = new List<string>();
            if (!string.IsNullOrEmpty(PapersIds))
            {
                PapersIds = PapersIds.TrimEnd(',');
                Paperslist = PapersIds.Split(',').Distinct().ToList();
            }

            int day = (Convert.ToInt32(AssignedDate.Substring(0, 2)));
            int month = (Convert.ToInt32(AssignedDate.Substring(3, 2)));
            int year = (Convert.ToInt32(AssignedDate.Substring(6, 4)));

            DateTime dt = new DateTime(year, month, day);

            //Save Data

            try
            {
                using (eFileContext ctx = new eFileContext())
                {


                    if (Paperslist.Count > 0)
                    {


                        foreach (var item in Paperslist)
                        {
                            tCommitteMeetingPapers objCommitteMeetingPapers = new tCommitteMeetingPapers();
                            int PaperId = Convert.ToInt32(item);
                            var dublicateData = (from a in ctx.tCommitteMeetingPapers where a.Id == PaperId select a).SingleOrDefault();
                            if (dublicateData != null)
                            {
                                ctx.tCommitteMeetingPapers.Remove(dublicateData);
                                ctx.SaveChanges();

                            }

                        }
                    }

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.AssignedDate == dt
                                 select a
                                 );

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {
                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            obj.Id = item.Id;
                            obj.PapersId = item.PapersId;
                            obj.RefNo = item.RefNo;
                            obj.Subject = item.Subject;
                            obj.AssignedDate = item.AssignedDate;
                            obj.StrAssignedDate = Convert.ToString(item.AssignedDate.Value.Date);
                            obj.PdfPath = item.PdfPath;
                            lst.Add(obj);
                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Cancel And Get Committee Meeting All


        #region Get Committee Meeting All Data
        public static object GetItemsForMeetingbyDate(object param)
        {


            string[] str = param as string[];
            string AssignedDate = Convert.ToString(str[0]);
            string PapersIds = Convert.ToString(str[1]);
            string AadharId = Convert.ToString(str[2]);
            List<string> Paperslist = new List<string>();
            if (!string.IsNullOrEmpty(PapersIds))
            {
                PapersIds = PapersIds.TrimEnd(',');
                Paperslist = PapersIds.Split(',').Distinct().ToList();
            }
            List<int> TagIds1 = PapersIds.Split(',').Select(int.Parse).ToList();

            int day = (Convert.ToInt32(AssignedDate.Substring(0, 2)));
            int month = (Convert.ToInt32(AssignedDate.Substring(3, 2)));
            int year = (Convert.ToInt32(AssignedDate.Substring(6, 4)));

            DateTime dt = new DateTime(year, month, day);

            //Save Data

            try
            {
                using (eFileContext ctx = new eFileContext())
                {

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.AssignedDate == dt && TagIds1.Contains(a.Id)
                                 select a
                                 );
                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {
                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            obj.Id = item.Id;
                            obj.PapersId = item.PapersId;
                            obj.RefNo = item.RefNo;
                            obj.Subject = item.Subject;
                            obj.AssignedDate = item.AssignedDate;
                            obj.StrAssignedDate = Convert.ToString(item.AssignedDate.Value.Date);
                            obj.PdfPath = item.PdfPath;
                            lst.Add(obj);
                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Delete And Get Committee Meeting All Data



        #region Get Committee Meeting All Data by Date
        public static object GetDateDataForMeeting(object param)
        {


            string[] str = param as string[];
            string AssignedDate = Convert.ToString(str[0]);
            string AadharId = Convert.ToString(str[1]);
            string strBranchId = Convert.ToString(str[2]);
            int BranchId = 0;
            List<string> Paperslist = new List<string>();

            string day = AssignedDate.Substring(0, 2);
            string month = AssignedDate.Substring(3, 2);
            string year = AssignedDate.Substring(6, 4);
            if (day.Length == 1)
                day = "0" + day;
            if (month.Length == 1)
                month = "0" + month;
            if (!string.IsNullOrEmpty(strBranchId))
            {
                BranchId = Convert.ToInt32(strBranchId);
            }
            DateTime dt = new DateTime(Convert.ToInt16(year), Convert.ToInt16(month), Convert.ToInt16(day));

            try
            {
                using (eFileContext ctx = new eFileContext())
                {

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.AssignedDate == dt.Date && a.BranchId == BranchId
                                 select a
                                 );
                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {
                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            obj.Id = item.Id;
                            obj.PapersId = item.PapersId;
                            obj.RefNo = item.RefNo;
                            obj.Subject = item.Subject;
                            obj.AssignedDate = item.AssignedDate;
                            obj.StrAssignedDate = Convert.ToString(item.AssignedDate.Value.Date);
                            obj.PdfPath = item.PdfPath;
                            lst.Add(obj);
                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Save And Get Committee Meeting All Data

        #region Save Manual Data And Get Committee Meeting All Data
        public static object SaveManualsValuesForMeetingsByDate(object param)
        {

            string[] str = param as string[];
            string AssignedDate = Convert.ToString(str[0]);
            string Subject = Convert.ToString(str[1]);
            string AadharId = Convert.ToString(str[2]);
            string filePath = Convert.ToString(str[3]);
            string strBranchId = Convert.ToString(str[4]);
            int BranchId = 0;
            if (!string.IsNullOrEmpty(strBranchId))
            {
                BranchId = Convert.ToInt32(strBranchId);
            }
            List<string> Paperslist = new List<string>();


            int day = (Convert.ToInt32(AssignedDate.Substring(0, 2)));
            int month = (Convert.ToInt32(AssignedDate.Substring(3, 2)));
            int year = (Convert.ToInt32(AssignedDate.Substring(6, 4)));

            DateTime dt = new DateTime(year, month, day);

            //Save Data

            try
            {
                using (eFileContext ctx = new eFileContext())
                {

                    tCommitteMeetingPapers objCommitteMeetingPapers = new tCommitteMeetingPapers();
                    objCommitteMeetingPapers.PapersId = 0;
                    objCommitteMeetingPapers.Subject = Subject;
                    objCommitteMeetingPapers.RefNo = "";
                    objCommitteMeetingPapers.AssignedDate = dt;
                    objCommitteMeetingPapers.PdfPath = filePath;
                    objCommitteMeetingPapers.CreateDate = System.DateTime.Now;
                    objCommitteMeetingPapers.CreatedBy = AadharId;
                    objCommitteMeetingPapers.BranchId = BranchId;
                    ctx.tCommitteMeetingPapers.Add(objCommitteMeetingPapers);
                    ctx.SaveChanges();

                    var query = (from a in ctx.tCommitteMeetingPapers
                                 where a.AssignedDate == dt && a.BranchId == BranchId
                                 select a
                                 );

                    if (query != null)
                    {
                        List<CommitteMeetingPapersModel> lst = new List<CommitteMeetingPapersModel>();
                        foreach (var item in query)
                        {
                            CommitteMeetingPapersModel obj = new CommitteMeetingPapersModel();
                            obj.Id = item.Id;
                            obj.PapersId = item.PapersId;
                            obj.RefNo = item.RefNo;
                            obj.Subject = item.Subject;
                            obj.AssignedDate = item.AssignedDate;
                            obj.StrAssignedDate = Convert.ToString(item.AssignedDate.Value.Date);
                            obj.PdfPath = item.PdfPath;
                            lst.Add(obj);
                        }

                        return lst.ToList();
                    }
                    return query.ToList();
                }


            }

            catch
            {
                throw;
            }
        }

        #endregion Save And Get Committee Meeting All Data


        public static object UpdateFileStatus(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int AttachId = Convert.ToInt32(param);
                    var SelectUpdateresult = (from attachments in context.eFileAttachments where attachments.eFileAttachmentId == AttachId select attachments).FirstOrDefault();
                    SBL.DomainModel.Models.eFile.eFileAttachment attchmentUpdate = new DomainModel.Models.eFile.eFileAttachment();
                    attchmentUpdate = SelectUpdateresult;
                    attchmentUpdate.SendStatus = true;
                    context.Entry(attchmentUpdate).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public static object GeteFileReport()
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    List<SBL.DomainModel.Models.eFile.eFileAttachment> objeFiles = (from values in context.eFileAttachments where values.Type == "Report" select values).ToList();
                    return objeFiles;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public static object eFileActive(object param)
        {
            //int eFileId;
            try
            {
                int eFileId = Convert.ToInt32(param);

                using (eFileContext Context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFile objeFiles = (from values in Context.eFiles where values.eFileID == eFileId select values).FirstOrDefault();
                    objeFiles.Status = true;
                    Context.eFiles.Attach(objeFiles);
                    Context.Entry(objeFiles).State = EntityState.Modified;
                    Context.SaveChanges();
                    return objeFiles.CommitteeId;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        public static object FileDeActive(object param)
        {
            //int eFileId;
            try
            {
                int eFileId = Convert.ToInt32(param);

                using (eFileContext Context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFile objeFiles = (from values in Context.eFiles where values.eFileID == eFileId select values).FirstOrDefault();
                    objeFiles.Status = false;
                    Context.eFiles.Attach(objeFiles);
                    Context.Entry(objeFiles).State = EntityState.Modified;
                    Context.SaveChanges();
                    return objeFiles.CommitteeId;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        public static object eFileAttachmentStatus(object param)
        {
            //SBL.DomainModel.Models.eFile.eFileAttachment model = (SBL.DomainModel.Models.eFile.eFileAttachment)param;
            //int eFileId;
            try
            {
                int eFileId = Convert.ToInt32(param);

                using (eFileContext Context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFileAttachment objeFiles = (from values in Context.eFileAttachments where values.eFileAttachmentId == eFileId select values).FirstOrDefault();
                    objeFiles.SendStatus = true;
                    Context.eFileAttachments.Attach(objeFiles);
                    Context.Entry(objeFiles).State = EntityState.Modified;
                    Context.SaveChanges();
                    return objeFiles.SendStatus;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GetCommitteeeFile(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    string departmentId = Convert.ToString(param);
                    var result = (from value in context.eFiles
                                  where value.DepartmentId == departmentId
                                  select value).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GeteFileAttachment(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int eAttachId = Convert.ToInt32(param);
                    var result = (from value in context.eFileAttachments where value.eFileAttachmentId == eAttachId select new SBL.DomainModel.Models.eFile.DepartmentReply { eFileName = value.eFileName, eFileID = value.eFileID }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GeteFileDepartmentReply(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int ParentId = Convert.ToInt32(param);
                    ParentId = 1;
                    var result = (from value in context.eFileAttachments where value.Type == "FromDepartment" && value.ParentId == ParentId select new SBL.DomainModel.Models.eFile.DepartmentReply { CreatedDate = value.CreatedDate, Description = value.Description, eFileAttachmentId = value.eFileAttachmentId, Title = value.Title }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GeteFileAttachmentonDepartment(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    string Id = Convert.ToString(param);
                    var result = (from value in context.eFileAttachments where value.SendStatus == false && value.Type == "CommitteeToDept" && value.ParentId == 0 select value).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GetDepartmentMembers(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    string Id = Convert.ToString(param);
                    var result = (from users in context.users
                                  join member in context.members
                                      on users.AadarId equals member.AadhaarNo
                                  //where users.DeptId == "HPD0012"
                                  select new SBL.DomainModel.Models.Member.DepartmentMembers { MemberID = member.MemberID, Email = member.Email, Mobile = member.Mobile }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object eFileDocumentIndex(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int Id = Convert.ToInt32(param);
                    var result = (from attachments in context.eFileAttachments
                                  join docType in context.documentType
                                      on attachments.DocumentTypeId equals docType.DocumentTypeID
                                  where attachments.eFileID == Id
                                  //select new SBL.DomainModel.Models.Document.mDocumentType
                                  //{
                                  //    DocumentType = docType.DocumentType,
                                  //    DocumentTypeID = docType.DocumentTypeID
                                  //}).ToList();
                                  select docType).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object EditeFile(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int eFileId = Convert.ToInt32(param);
                    SBL.DomainModel.Models.eFile.eFile eFile = (from data in context.eFiles
                                                                where data.eFileID == eFileId
                                                                select data).FirstOrDefault();
                    return eFile;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GeteFileSearch(object param)
        {
            SBL.DomainModel.Models.eFile.eFileSearch search = new DomainModel.Models.eFile.eFileSearch();
            search = param as SBL.DomainModel.Models.eFile.eFileSearch;
            using (eFileContext context = new eFileContext())
            {
                SBL.DomainModel.Models.eFile.eFileListAll eFileListAll = new DomainModel.Models.eFile.eFileListAll();
                eFileListAll.eFileLists = (from efilevalues in context.eFiles
                                           join departmentvalues in context.departments
                                           on efilevalues.DepartmentId equals departmentvalues.deptId
                                           join user in context.users
                                           on efilevalues.CreatedBy equals user.UserId
                                           where (departmentvalues.deptname.Contains(search.Department) ||
                                           efilevalues.eFileNumber.Contains(search.eFileNo) ||
                                           efilevalues.eFileSubject.Contains(search.Subject))
                                           && efilevalues.CommitteeId == search.CommitteeId
                                           && efilevalues.Status == search.Status
                                           select new SBL.DomainModel.Models.eFile.eFileList
                                           {
                                               Department = departmentvalues.deptname,
                                               eFileID = efilevalues.eFileID,
                                               eFileNumber = efilevalues.eFileNumber,
                                               eFileSubject = efilevalues.eFileSubject,
                                               Status = efilevalues.Status,
                                               CreateDate = efilevalues.CreatedDate,
                                               CreatedBy = user.UserName
                                           }).ToList();
                eFileListAll.Departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();

                return eFileListAll;
            }
        }

        private static object GetAlleFileAttachments(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var list = (from attachments in context.eFileAttachments where attachments.eFileID == eFileId select attachments).ToList();
                return list;
            }
        }

        private static object GeteFileAttachmentDep(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var list = (from attachments in context.eFileAttachments where attachments.eFileAttachmentId == eFileId select attachments).FirstOrDefault();
                return list;
            }
        }

        private static object GetAlleFileAttachmentsDep(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var list = (from attachments in context.eFileAttachments select attachments).ToList();
                return list;
            }
        }

        private static object GetAlleFileAttachmentsDep2(object param)
        {
            SBL.DomainModel.Models.eFile.eFileSearch search = new DomainModel.Models.eFile.eFileSearch();
            search = param as SBL.DomainModel.Models.eFile.eFileSearch;
            using (eFileContext context = new eFileContext())
            {
                var eFileListAll = (from efilevalues in context.eFileAttachments
                                    join departmentvalues in context.departments
                                    on efilevalues.DepartmentId equals departmentvalues.deptId
                                    join user in context.users
                                    on efilevalues.CreatedBy equals user.UserId
                                    join efiles in context.eFiles
                                    on efilevalues.eFileID equals efiles.eFileID
                                    where efilevalues.Type == "SendNoticetoDept"
                                    select new SBL.DomainModel.Models.eFile.eFileList
                                    {
                                        Department = departmentvalues.deptname,
                                        eFileAttachmentId = efilevalues.eFileAttachmentId,
                                        eFileID = efilevalues.eFileID,
                                        eFileNumber = efilevalues.eFileName,
                                        eFileSubject = efilevalues.Description,
                                        CreateDate = efilevalues.CreatedDate,
                                        CreatedBy = user.UserName,
                                        Status = efilevalues.SendStatus,
                                        Committee = (from committees in context.committees where committees.CommitteeId == efiles.CommitteeId select committees.CommitteeName).FirstOrDefault(),
                                        CommitteeType = (from committees in context.committees join committeTypes in context.committeeTypes on committees.CommitteeTypeId equals committeTypes.CommitteeTypeId where committees.CommitteeId == efiles.CommitteeId select committeTypes.CommitteeTypeName).FirstOrDefault(),
                                        Assembly = (from committees in context.committees join assembly in context.Assembly on committees.AssemblyID equals assembly.AssemblyCode where committees.CommitteeId == efiles.CommitteeId select assembly.AssemblyName).FirstOrDefault(),
                                        Session = (from committees in context.committees join session in context.Sessions on committees.SessionId equals session.SessionCode where committees.CommitteeId == efiles.CommitteeId select session.SessionName).FirstOrDefault()
                                    }).ToList();

                return eFileListAll;
            }
        }

        private static object GetAlleFileAttachmentsFromDep(object param)
        {
            SBL.DomainModel.Models.eFile.eFileSearch search = new DomainModel.Models.eFile.eFileSearch();
            search = param as SBL.DomainModel.Models.eFile.eFileSearch;
            using (eFileContext context = new eFileContext())
            {
                var eFileListAll = (from efilevalues in context.eFileAttachments
                                    join departmentvalues in context.departments
                                    on efilevalues.DepartmentId equals departmentvalues.deptId
                                    join user in context.users
                                    on efilevalues.CreatedBy equals user.UserId
                                    join efiles in context.eFiles
                                    on efilevalues.eFileID equals efiles.eFileID
                                    where efilevalues.Type == "ReplyByDep"
                                    select new SBL.DomainModel.Models.eFile.eFileList
                                    {
                                        Department = departmentvalues.deptname,
                                        eFileAttachmentId = efilevalues.eFileAttachmentId,
                                        eFileID = efilevalues.eFileID,
                                        eFileNumber = efilevalues.eFileName,
                                        eFileSubject = efilevalues.Description,
                                        CreateDate = efilevalues.CreatedDate,
                                        CreatedBy = user.UserName,
                                        Status = efilevalues.SendStatus,
                                        Committee = (from committees in context.committees where committees.CommitteeId == efiles.CommitteeId select committees.CommitteeName).FirstOrDefault(),
                                        CommitteeType = (from committees in context.committees join committeTypes in context.committeeTypes on committees.CommitteeTypeId equals committeTypes.CommitteeTypeId where committees.CommitteeId == efiles.CommitteeId select committeTypes.CommitteeTypeName).FirstOrDefault(),
                                        Assembly = (from committees in context.committees join assembly in context.Assembly on committees.AssemblyID equals assembly.AssemblyCode where committees.CommitteeId == efiles.CommitteeId select assembly.AssemblyName).FirstOrDefault(),
                                        Session = (from committees in context.committees join session in context.Sessions on committees.SessionId equals session.SessionCode where committees.CommitteeId == efiles.CommitteeId select session.SessionName).FirstOrDefault()
                                    }).ToList();

                return eFileListAll;
            }
        }

        private static object AddeFileAttachments(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFileAttachment model = param as SBL.DomainModel.Models.eFile.eFileAttachment;
                    //context..Add(model);
                    context.eFileAttachments.Add(model);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        private static object AddeFile(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
                model.Status = true;
                context.eFiles.Add(model);
                context.SaveChanges();
                context.Close();
            }
            return null;
        }

        private static SBL.DomainModel.Models.eFile.eFileListAll GetAlleFile(object param)
        {
            //using (eFileContext context = new eFileContext())
            //{
            //    int CommitteeId=Convert.ToInt32(param);
            //    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles join departmentvalues in context.departments
            //                                                                                                 on efilevalues.DepartmentId equals departmentvalues.deptId
            //                                                                                                 join user in context.users
            //                                                                                                 on efilevalues.CreatedBy equals user.UserId
            //                                                              where efilevalues.CommitteeId == CommitteeId
            //                                                              select new SBL.DomainModel.Models.eFile.eFileList
            //                                                              {
            //                                                                    Department=departmentvalues.deptname,
            //                                                                     eFileID=efilevalues.eFileID,
            //                                                                      eFileNumber=efilevalues.eFileNumber,
            //                                                                       eFileSubject=efilevalues.eFileSubject,
            //                                                                        Status=efilevalues.Status,
            //                                                                        CreateDate=efilevalues.CreatedDate,
            //                                                                        CreatedBy=user.UserName
            //                                                              }).ToList().OrderByDescending(x=>x.eFileID).ToList();
            //    return eFileList;
            //}

            //...................................

            using (eFileContext context = new eFileContext())
            {
                int CommitteeId = Convert.ToInt32(param);
                List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                          join departmentvalues in context.departments
                                                                          on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                          join user in context.users
                                                                          on efilevalues.CreatedBy equals user.UserId
                                                                          where efilevalues.CommitteeId == CommitteeId
                                                                          && efilevalues.Status == true
                                                                          select new SBL.DomainModel.Models.eFile.eFileList
                                                                          {
                                                                              Department = departmentvalues.deptname,
                                                                              eFileID = efilevalues.eFileID,
                                                                              eFileNumber = efilevalues.eFileNumber,
                                                                              eFileSubject = efilevalues.eFileSubject,
                                                                              Status = efilevalues.Status,
                                                                              CreateDate = efilevalues.CreatedDate,
                                                                              CreatedBy = user.UserName,
                                                                              olddesc = efilevalues.OldDescription,
                                                                              newdesc = efilevalues.NewDescription,
                                                                              CommitteeId = efilevalues.CommitteeId
                                                                          }).ToList().OrderByDescending(x => x.eFileID).ToList();
                List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                eFileListAlls.eFileLists = eFileList;
                eFileListAlls.Departments = departments;
                return eFileListAlls;
            }
        }

        private static SBL.DomainModel.Models.eFile.eFileList GeteFile(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var eFileList = (from efilevalues in context.eFiles
                                 join departmentvalues in context.departments
                                 on efilevalues.DepartmentId equals departmentvalues.deptId
                                 where efilevalues.eFileID == eFileId
                                 select new SBL.DomainModel.Models.eFile.eFileList
                                 {
                                     Department = departmentvalues.deptname,
                                     eFileID = efilevalues.eFileID,
                                     eFileNumber = efilevalues.eFileNumber,
                                     eFileSubject = efilevalues.eFileSubject,
                                     Status = efilevalues.Status
                                 }).FirstOrDefault();
                return eFileList;
            }
        }

        private static object GetCommitteUser(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                Guid UserId = Guid.Parse(param.ToString());
                var obj = (from CommitteeType in context.CommitteeTypePermission
                           where CommitteeType.UserId == UserId
                           select CommitteeType).FirstOrDefault();
                return obj;
            }
        }



        public static object GeteFileDetailsByFileId(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int eFileId = Convert.ToInt32(param.ToString());
                    //var eFileList = (from efilevalues in context.eFiles
                    //                 join officevalues in context.mOffice
                    //                 on efilevalues.eFileID equals officevalues.OfficeId
                    //                 where efilevalues.eFileID == eFileId
                    //                 select efilevalues).FirstOrDefault();
                    var eFileList = (from efilevalues in context.eFiles
                                     join officevalues in context.mOffice
                                     on efilevalues.Officecode equals officevalues.OfficeId
                                     join dept in context.departments on efilevalues.DepartmentId equals dept.deptId
                                     where efilevalues.eFileID == eFileId
                                     select new
                                     {
                                         //  FileId=efilevalues.eFileID,
                                         OfficeName = officevalues.officename,
                                         //FileNo=efilevalues.eFileNumber,
                                         //Subject=efilevalues.eFileSubject,
                                         DepartmentId = efilevalues.DepartmentId,
                                         FileNumber = efilevalues.eFileNumber,
                                         OldDescription = efilevalues.OldDescription,
                                         NewDescription = efilevalues.NewDescription,
                                         FromYear = efilevalues.FromYear,
                                         ToYear = efilevalues.ToYear,
                                         eFileSubject = efilevalues.eFileSubject,
                                         Officecode = efilevalues.Officecode,
                                         DepartmentName = dept.deptname,
                                         eFileTypeID = efilevalues.eFileTypeID
                                     }).FirstOrDefault();
                    SBL.DomainModel.Models.eFile.eFile obj = new DomainModel.Models.eFile.eFile();
                    if (eFileList != null)
                    {
                        // obj.eFileID = eFileList.FileId;
                        obj.OfficeName = eFileList.OfficeName;
                        obj.eFileNumber = eFileList.FileNumber;
                        // obj.FileNo = eFileList.FileId;
                        // obj.Subject = eFileList.FileId;
                        obj.OldDescription = eFileList.OldDescription;
                        obj.NewDescription = eFileList.NewDescription;
                        obj.FromYear = eFileList.FromYear;
                        obj.ToYear = eFileList.ToYear;
                        obj.DepartmentId = eFileList.DepartmentId;
                        obj.eFileSubject = eFileList.eFileSubject;
                        obj.Officecode = eFileList.Officecode;
                        obj.DepartmentName = eFileList.DepartmentName;
                        obj.eFileTypeID = eFileList.eFileTypeID;
                    }
                    return obj;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static object UpdateeFileDetailsByFileId(object param)
        {
            try
            {
                SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
                eFileContext ctx = new eFileContext();
                SBL.DomainModel.Models.eFile.eFile obj = ctx.eFiles.Single(m => m.eFileID == model.eFileID);

                obj.FromYear = model.FromYear;
                obj.ToYear = model.ToYear;
                obj.DepartmentId = model.DepartmentId;
                obj.Officecode = model.Officecode;
                obj.eFileNumber = model.eFileNumber;
                obj.eFileSubject = model.eFileSubject;
                obj.OldDescription = model.OldDescription;
                obj.NewDescription = model.NewDescription;

                ctx.SaveChanges();
                return 1; //Updated
            }
            catch
            {
                return 0; //Error
            }
        }

        public static object UpdateeFileNewDescriptionByFileId(object param)
        {
            try
            {
                SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
                eFileContext ctx = new eFileContext();
                SBL.DomainModel.Models.eFile.eFile obj = ctx.eFiles.Single(m => m.eFileID == model.eFileID);

                obj.NewDescription = model.NewDescription;

                ctx.SaveChanges();
                return 1; //Updated
            }
            catch
            {
                return 0; //Error
            }
        }

        #region Receive Paper List

        public static object GetReceivePaperDetailsByDeptId(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false //&& a.ToOfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }

        #endregion Receive Paper List

        #region Send paper List

        public static object GetSendPaperDetailsByDeptId(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.ToDepartmentID equals b.deptId
                                 into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID

                                 where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false //&& a.OfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     ToDepartmentId = a.ToDepartmentID,
                                     SendDate = a.SendDate,
                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.SendDate = item.SendDate;
                        mdl.ToDepartmentID = item.ToDepartmentId;
                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;
                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.ToDepartmentID equals b.deptId
                                 into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID

                                 where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false //&& a.OfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     ToDepartmentId = a.ToDepartmentID,
                                     SendDate = a.SendDate,
                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.SendDate = item.SendDate;
                        mdl.ToDepartmentID = item.ToDepartmentId;
                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;
                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }

        #endregion Send paper List

        #region eFile List Details

        private static eFileListAll eFileList(object param)
        {
            using (eFileContext context = new eFileContext())
            {

                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);

                int Officecode = 0;
                int.TryParse(str[1], out Officecode);

                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                // string DistDept = deptlist.Aggregate((a, x) => a + "," + x);

                if (Officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                              //&& efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  CommitteeAbbr = Comms.Abbreviation,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                              // && efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  CommitteeAbbr = Comms.Abbreviation,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        private static eFileListAll eFileListDeactive(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);
                //     int Officecode = Convert.ToInt32(str[1]);
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (Officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == false && efilevalues.IsDeleted == false
                                                                              //&& efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == false && efilevalues.IsDeleted == false
                                                                              // && efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        private static eFileListAll eFileListALLStatus(object param)
        {
            SBL.DomainModel.Models.eFile.eFile mdl = param as SBL.DomainModel.Models.eFile.eFile;
            using (eFileContext context = new eFileContext())
            {
                string DeptId = mdl.DepartmentId;
                int officecode = mdl.Officecode;
                int CommId = mdl.CommitteeId;
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) &&
                                                                              efilevalues.CommitteeId == CommId && efilevalues.IsDeleted == false
                                                                              // && efilevalues.Officecode == officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) &&
                                                                              efilevalues.CommitteeId == CommId && efilevalues.IsDeleted == false
                                                                              // && efilevalues.Officecode == officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        private static eFileListAll eFileListByCommitteeId(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);
                //  int Officecode = Convert.ToInt32(str[1]);
                int CommitteeID = Convert.ToInt32(str[2]);
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (Officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                             && efilevalues.CommitteeId == CommitteeID
                                                                              //&& efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  // && efilevalues.Officecode == Officecode 
                                                                              && efilevalues.CommitteeId == CommitteeID
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        private static eFileListAll eFileListDeactiveByCommitteeId(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);
                //  int Officecode = Convert.ToInt32(str[1]);
                int CommitteeID = Convert.ToInt32(str[2]);
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (Officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == false && efilevalues.IsDeleted == false
                                                                               && efilevalues.CommitteeId == CommitteeID
                                                                              //&& efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == false && efilevalues.IsDeleted == false
                                                                                  //&& efilevalues.Officecode == Officecode 
                                                                              && efilevalues.CommitteeId == CommitteeID
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        #endregion eFile List Details

        public static object UpdateReceivePaperSingle(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;
                using (eFileContext ctx = new eFileContext())
                {
                    eFileAttachment model = ctx.eFileAttachments.Single(a => a.eFileAttachmentId == mdl.eFileAttachmentId);
                    model.eFileID = mdl.eFileID;
                    model.ReceivedDate = mdl.ReceivedDate;
                    model.PaperStatus = mdl.PaperStatus;
                    ctx.SaveChanges();
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static eFileAttachment GetValuesByRefID(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFileAttachments where _ObjeFileContext.PaperRefNo == _ReferenceNo && _ObjeFileContext.PaperNature != 3 select _ObjeFileContext).FirstOrDefault();
            return query;
        }

        private static object GetAlleFileAttachmentsByDeptIDandeFileID(object param)
        {
            eFileAttachment mdl = param as eFileAttachment;
            using (eFileContext context = new eFileContext())
            {
                //int eFileId = mdl.eFileID;

                var list = (from attachments in context.eFileAttachments where attachments.eFileID == mdl.eFileID && attachments.ToDepartmentID == mdl.DepartmentId select attachments).ToList();
                return list;
            }
        }
        private static object GetAlleFileAttachmentsByeFileID(object param)
        {
            eFileAttachment mdl = param as eFileAttachment;
            using (eFileContext context = new eFileContext())
            {
                //int eFileId = mdl.eFileID;

                var obj = (from attachments in context.eFileAttachments where attachments.eFileAttachmentId == mdl.eFileAttachmentId select attachments).FirstOrDefault();
                return obj;
            }
        }

        public static object AttachPapertoEFileMul(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;

                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {
                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            eFileAttachment model = ctx.eFileAttachments.Single(a => a.eFileAttachmentId == mdl.eFileAttachmentId);
                            model.eFileID = mdl.eFileID;
                            //model.ReceivedDate = mdl.ReceivedDate;
                            //model.PaperStatus = mdl.PaperStatus;
                            ctx.SaveChanges();
                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static object AttachPapertoEFileMulR(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;

                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {
                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            eFileAttachment model = ctx.eFileAttachments.Single(a => a.eFileAttachmentId == mdl.eFileAttachmentId);
                            model.ReFileID = mdl.ReFileID;
                            //model.ReceivedDate = mdl.ReceivedDate;
                            //model.PaperStatus = mdl.PaperStatus;
                            ctx.SaveChanges();
                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static object AttacheFiletoCommitteeMul(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;

                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {
                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            SBL.DomainModel.Models.eFile.eFile model = ctx.eFiles.Single(a => a.eFileID == mdl.eFileAttachmentId);
                            model.CommitteeId = mdl.eFileID;
                            //model.ReceivedDate = mdl.ReceivedDate;
                            //model.PaperStatus = mdl.PaperStatus;
                            ctx.SaveChanges();
                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static object Get_eFilePaperNatureList(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFilePaperNature select _ObjeFileContext).ToList();
            return query;
        }

        public static object Get_eFilePaperNatureList_Receive(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFilePaperNature where _ObjeFileContext.PaperNatureID != 1 && _ObjeFileContext.PaperNatureID != 4 && _ObjeFileContext.PaperNatureID != 5 select _ObjeFileContext).ToList();
            return query;
        }

        public static object Get_eFilePaperTypeList(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFilePaperType select _ObjeFileContext).ToList();
            return query;
        }

        public static object GetPaperDetailsByRefNo(object param)
        {
            string RefNo = Convert.ToString(param);
            using (eFileContext ctx = new eFileContext())
            {
                var query = (from a in ctx.eFileAttachments
                             join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                             from ps in temp.DefaultIfEmpty()
                             join bd in ctx.departments on a.ToDepartmentID equals bd.deptId into tempbd
                             from psbd in tempbd.DefaultIfEmpty()
                             join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                             join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                             where a.PaperRefNo == RefNo
                             select new
                             {
                                 eFileAttachmentId = a.eFileAttachmentId,
                                 Department = ps.deptname ?? "OTHER",
                                 eFileName = a.eFileName,
                                 PaperNature = a.PaperNature,
                                 PaperNatureDays = a.PaperNatureDays ?? "0",
                                 eFilePath = a.eFilePath,
                                 Title = a.Title,
                                 Description = a.Description,
                                 CreateDate = a.CreatedDate,
                                 PaperStatus = a.PaperStatus,
                                 eFIleID = a.eFileID,
                                 PaperRefNo = a.PaperRefNo,
                                 DepartmentId = a.DepartmentId,
                                 OfficeCode = a.OfficeCode,
                                 ToDepartmentID = a.ToDepartmentID,
                                 ToOfficeCode = a.ToOfficeCode,
                                 ReplyStatus = a.ReplyStatus,
                                 ReplyDate = a.ReplyDate,
                                 ReceivedDate = a.ReceivedDate,
                                 DocumentTypeId = a.DocumentTypeId,

                                 ToPaperNature = a.ToPaperNature,
                                 ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                 LinkedRefNo = a.LinkedRefNo,
                                 RevedOption = a.RevedOption,
                                 RecvDetails = a.RecvDetails,
                                 ToRecvDetails = a.ToRecvDetails,
                                 eMode = a.eMode,
                                 RecvdPaperNature = c.PaperNature,
                                 DocuemntPaperType = d.PaperType,
                                 PType = a.PType,
                                 SendStatus = a.SendStatus,
                                 DeptAbbr = ps.deptabbr,
                                 ToDepartment = psbd.deptname,
                                 ToDepAbbr = psbd.deptabbr,
                                 PaperNo = a.PaperNo
                             }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                List<eFileAttachment> lst = new List<eFileAttachment>();
                foreach (var item in query)
                {
                    eFileAttachment mdl = new eFileAttachment();
                    mdl.DepartmentName = item.Department;
                    mdl.eFileAttachmentId = item.eFileAttachmentId;
                    mdl.eFileName = item.eFileName;
                    mdl.PaperNature = item.PaperNature;
                    mdl.PaperNatureDays = item.PaperNatureDays;
                    mdl.eFilePath = item.eFilePath;
                    mdl.Title = item.Title;
                    mdl.Description = item.Description;
                    mdl.CreatedDate = item.CreateDate;
                    mdl.PaperStatus = item.PaperStatus;
                    mdl.eFileID = item.eFIleID;
                    mdl.PaperRefNo = item.PaperRefNo;
                    mdl.DepartmentId = item.DepartmentId;
                    mdl.OfficeCode = item.OfficeCode;
                    mdl.ToDepartmentID = item.ToDepartmentID;
                    mdl.ToOfficeCode = item.ToOfficeCode;
                    mdl.ReplyStatus = item.ReplyStatus;
                    mdl.ReplyDate = item.ReplyDate;
                    mdl.ReceivedDate = item.ReceivedDate;
                    mdl.DocumentTypeId = item.DocumentTypeId;

                    mdl.ToPaperNature = item.ToPaperNature;
                    mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                    mdl.LinkedRefNo = item.LinkedRefNo;
                    mdl.RevedOption = item.RevedOption;
                    mdl.RecvDetails = item.RecvDetails;
                    mdl.ToRecvDetails = item.ToRecvDetails;
                    mdl.eMode = item.eMode;
                    mdl.RecvdPaperNature = item.RecvdPaperNature;

                    mdl.DocuemntPaperType = item.DocuemntPaperType;
                    mdl.PType = item.PType;
                    mdl.SendStatus = item.SendStatus;
                    mdl.DeptAbbr = item.DeptAbbr;
                    mdl.ToDepartmentName = item.ToDepartment;
                    mdl.ToDeptAbbr = item.ToDepAbbr;
                    mdl.PaperNo = item.PaperNo;
                    lst.Add(mdl);
                }

                return lst;
            }
        }

        #region eFile Noting

        private static object AddeFileNotings(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    eFileNoting model = param as eFileNoting;
                    //context..Add(model);
                    context.eFileNoting.Add(model);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        private static object UpdateLasteFileNotings(object param)
        {
            eFileNoting mdl = param as eFileNoting;
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    eFileNoting model = context.eFileNoting.Single(a => a.eNotingID == mdl.eNotingID);
                    model.eNotingFileName = mdl.eNotingFileName;
                    model.NotingStatus = mdl.NotingStatus;
                    model.ModifiedDate = mdl.ModifiedDate;
                    model.FilePath = mdl.FilePath;
                    model.Remark = mdl.Remark;
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public static object GetPNo(object param)
        {
            int efileid = Convert.ToInt32(param);
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    var Maxval = context.eFileNoting
                        .Where(a => a.eFileID == efileid)
                        .Max(a => a.FileUploadSeq);
                    if (Maxval == null)
                        Maxval = 0;

                    return Maxval;
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static object GeteNotingIDByeFileIdandSeqNo(object param)
        {
            eFileNoting model = param as eFileNoting;

            using (eFileContext context = new eFileContext())
            {
                var Maxval = (from a in context.eFileNoting
                              where a.eFileID == model.eFileID && a.FileUploadSeq == model.FileUploadSeq && a.IsDeleted == false
                              select a.eNotingID).FirstOrDefault();

                return Maxval;
            }
        }

        private static object GeteFileNotingDetailsByeFileID(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var list = (from attachments in context.eFileNoting where attachments.eFileID == eFileId select attachments).ToList();
                return list;
            }
        }

        #endregion eFile Noting

        private static object saveCommitteeProceeding(object param)
        {
            using (eFileContext dbContext = new eFileContext())
            {
                COmmitteeProceeding committe = param as COmmitteeProceeding;
                committe.CreatedDate = DateTime.Now;
                dbContext.CommitteeProceeding.Add(committe);
                dbContext.SaveChanges();
                dbContext.Close();
            }
            return null;
        }

        private static object updateCommitteeProceeding(object param)
        {
            using (eFileContext dbContext = new eFileContext())
            {
                COmmitteeProceeding committe = param as COmmitteeProceeding;
                dbContext.CommitteeProceeding.Add(committe);
                dbContext.SaveChanges();
            }
            return null;
        }

        private static object deleteCommitteeProceeding(object param)
        {
            using (eFileContext dbContext = new eFileContext())
            {
                COmmitteeProceeding committe = param as COmmitteeProceeding;
                dbContext.CommitteeProceeding.Add(committe);
                dbContext.SaveChanges();
            }
            return null;
        }

        private static object GetCommitteeProoceedingByDept(object param)
        {
            Guid UserID = new Guid();
            Guid.TryParse(param as string, out UserID);

            using (eFileContext context = new eFileContext())
            {

                var val = (from a in context.CommitteeTypePermission
                           where a.UserId == UserID
                           select a.CommitteeTypeId).Count();
                //For Repoter Binding 
                if (val == 0)
                {
                    var list = (from a in context.CommitteeProceeding
                                join b in context.committees on a.CommitteeId equals b.CommitteeId
                                where a.IsDeleted == false
                                select new
                                {
                                    ProID = a.ProID,
                                    CommitteeName = b.CommitteeName,
                                    ProFilePathPdf = a.ProFilePathPdf,
                                    ProFilePathWord = a.ProFilePathWord,
                                    Remark = a.Remark,
                                    CreatedDate = a.CreatedDate,
                                    CommitteeId = a.CommitteeId
                                }).OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<COmmitteeProceeding> mdl = new List<COmmitteeProceeding>();
                    foreach (var item in list)
                    {
                        COmmitteeProceeding m = new COmmitteeProceeding();
                        m.ProID = item.ProID;
                        m.CommitteeName = item.CommitteeName;
                        m.ProFilePathPdf = item.ProFilePathPdf;
                        m.ProFilePathWord = item.ProFilePathWord;
                        m.Remark = item.Remark;
                        m.CreatedDate = item.CreatedDate;
                        m.CommitteeId = item.CommitteeId;
                        m.IsReporter = true;
                        mdl.Add(m);
                    }
                    return mdl;
                }
                //Other than Repoter Binding 
                else
                {
                    var perm = (from a in context.CommitteeTypePermission
                                where a.UserId == UserID
                                select a.CommitteeTypeId).ToArray();
                    var list = (from a in context.CommitteeProceeding
                                join b in context.committees on a.CommitteeId equals b.CommitteeId
                                where a.IsDeleted == false && (perm).Contains(a.CommitteeId)
                                select new
                                {
                                    ProID = a.ProID,
                                    CommitteeName = b.CommitteeName,
                                    ProFilePathPdf = a.ProFilePathPdf,
                                    ProFilePathWord = a.ProFilePathWord,
                                    Remark = a.Remark,
                                    CreatedDate = a.CreatedDate,
                                    CommitteeId = a.CommitteeId
                                }).OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<COmmitteeProceeding> mdl = new List<COmmitteeProceeding>();
                    foreach (var item in list)
                    {
                        COmmitteeProceeding m = new COmmitteeProceeding();
                        m.ProID = item.ProID;
                        m.CommitteeName = item.CommitteeName;
                        m.ProFilePathPdf = item.ProFilePathPdf;
                        m.ProFilePathWord = item.ProFilePathWord;
                        m.Remark = item.Remark;
                        m.CreatedDate = item.CreatedDate;
                        m.CommitteeId = item.CommitteeId;
                        m.IsReporter = false;
                        mdl.Add(m);
                    }
                    return mdl;
                }

            }
        }

        private static object GetCommitteeUploadedPdf(object param)
        {
            CommitteeMembersUpload model = param as CommitteeMembersUpload;
            int CommId = model.CommitteeId;
            using (eFileContext context = new eFileContext())
            {
                var list = (from a in context.CommitteeMembersUpload
                            where a.CommitteeId == CommId
                            select a).OrderByDescending(a => a.CommStatus).ToList();
                return list;
            }
        }

        private static object GetGeneratedPdfList(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                var list = (from a in context.CommitteeMembersUpload
                            where a.IsDeleted == false
                            select a).OrderByDescending(a => a.CommStatus).ToList();
                return list;
            }
        }

        private static object GetTreeViewStructure(object param)
        {
            TreeViewStructure objTreeViewStructure = param as TreeViewStructure;
            using (eFileContext context = new eFileContext())
            {
                var list = (from treeview in context.TreeViewStructure
                            select treeview).ToList();
                return list;
            }
        }

        private static object GetMembersByAssemblyID(object param)
        {
            SBL.DomainModel.Models.Member.mMemberAssembly parameter = param as SBL.DomainModel.Models.Member.mMemberAssembly;
            using (eFileContext context = new eFileContext())
            {
                var query = (from a in context.mMemberAssembly
                             join b in context.members on a.MemberID equals b.MemberCode
                             join c in context.mConstituency on
                                 // a.ConstituencyCode equals c.ConstituencyCode
                             new { code = a.ConstituencyCode, Id = parameter.AssemblyID } equals new { code = c.ConstituencyCode, Id = c.AssemblyID.Value }
                             where a.AssemblyID == parameter.AssemblyID
                             select new
                             {
                                 Name = b.Name + "( " + c.ConstituencyName + ")",
                                 MemberID = b.MemberID
                             }

                             ).OrderBy(a => a.Name);

                List<mMember> mdl = new List<mMember>();
                foreach (var item in query)
                {
                    mMember a = new mMember();
                    a.Name = item.Name;
                    a.MemberID = item.MemberID;
                    mdl.Add(a);
                }

                return mdl;
            }
        }

        private static object GetStaffDetails(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                var query = (from a in context.mStaff
                             where a.isActive == true && a.isAlive == true && a.Group == "Gazetted"
                             select a).OrderBy(a => a.StaffName).ToList();

                return query;
            }
        }

        // Add eFile Type
        private static object eFileTypeDetails(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    var result = (from a in context.eFileType
                                  where a.IsDeleted == 0
                                  select a).OrderBy(a => a.eType).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        public static object CheckUpdateDiary(object param)
        {
            eFileContext context = new eFileContext();
            eFileAttachment Obj = param as eFileAttachment;
            int CYear = Convert.ToInt32(Obj.Year); //Current Year
            string msg = "";
            var Number = (from Efile in context.eFileAttachments
                          where Efile.CurYear == CYear
                          orderby Efile.CurDNo descending
                          select Efile.CurDNo).FirstOrDefault();

            if (Number == 0)
            {
                Obj.DiaryNo = "1/" + Obj.Year;
                Obj.CurDNo = 1;
            }
            else
            {
                Number = Number + 1;
                Obj.CurDNo = Number;
                Obj.DiaryNo = Number + "/" + Obj.Year;
            }


            try
            {
                eFileAttachment PaperObj = context.eFileAttachments.Single(m => m.eFileAttachmentId == Obj.eFileAttachmentId);
                PaperObj.DiaryNo = Obj.DiaryNo;
                PaperObj.CurYear = CYear;
                PaperObj.CurDNo = Obj.CurDNo;
                context.SaveChanges();
                msg = Obj.DiaryNo;
            }

            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return Obj;
        }

        static List<mBranches> GBranchList(object param)
        {
            eFileContext context = new eFileContext();
            mBranches model = param as mBranches;
            List<mBranches> returnToCaller = new List<mBranches>();

            var BID = (from Val in context.users
                       join b in context.mStaff on Val.AadarId equals b.AadharID
                       where Val.UserId == model.UserId
                       select b.BranchIDs).FirstOrDefault();

            if (BID != null)
            {
                string[] StrId = BID.Split(',');
                for (int i = 0; i < StrId.Length; i++)
                {
                    // Convert String Id into int Id
                    int Id = Convert.ToInt16(StrId[i]);

                    //Get Branch List Name By Id
                    var List = (from B in context.mBranches
                                where B.BranchId == Id
                                select B).FirstOrDefault();
                    mBranches tempModel = new mBranches();
                    tempModel.BranchName = List.BranchName;
                    tempModel.BranchId = List.BranchId;
                    returnToCaller.Add(tempModel);
                }
            }

            return returnToCaller.ToList();
        }


    }
}