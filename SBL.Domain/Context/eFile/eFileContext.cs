﻿using SBL.DAL;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.eFile;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.StaffManagement;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data.Entity.SqlServer;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.RecipientGroups;
using System.Globalization;
using SBL.Domain.Context.SiteSetting;
using SBL.DomainModel.Models.User;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Sockets;
using SBL.DomainModel.ComplexModel.LinqKitSource;

namespace SBL.Domain.Context.eFile
{
    public class eFileContext : DBBase<eFileContext>
    {
        public eFileContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<SBL.DomainModel.Models.eFile.eFile> eFiles { get; set; }
        public DbSet<SBL.DomainModel.Models.Department.mDepartment> departments { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.tCommittee> committees { get; set; }
        public DbSet<SBL.DomainModel.Models.Assembly.mAssembly> Assembly { get; set; }
        public DbSet<SBL.DomainModel.Models.Session.mSession> Sessions { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.mCommitteeType> committeeTypes { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFileAttachment> eFileAttachments { get; set; }
        public DbSet<SBL.DomainModel.Models.User.mUsers> users { get; set; }
        public DbSet<SBL.DomainModel.Models.Document.mDocumentType> documentType { get; set; }
        public DbSet<SBL.DomainModel.Models.Member.mMember> members { get; set; }
        public DbSet<SBL.DomainModel.Models.Office.mOffice> mOffice { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFilePaperNature> eFilePaperNature { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFilePaperType> eFilePaperType { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.COmmitteeProceeding> CommitteeProceeding { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.CommitteeMembersUpload> CommitteeMembersUpload { get; set; }
        public DbSet<SBL.DomainModel.Models.Member.mMemberAssembly> mMemberAssembly { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.TreeViewStructure> TreeViewStructure { get; set; }
        public DbSet<mStaff> mStaff { get; set; }
        public DbSet<SBL.DomainModel.Models.Constituency.mConstituency> mConstituency { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFileType> eFileType { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.CommitteeTypePermission> CommitteeTypePermission { get; set; }
        public DbSet<mBranches> mBranches { get; set; }
        public DbSet<tMovementFile> tMovementFile { get; set; }
        public DbSet<tDrafteFile> tDrafteFile { get; set; }
        public DbSet<mMinistry> mMinistry { get; set; }
        //eFileNoting
        public DbSet<eFileNoting> eFileNoting { get; set; }
        public DbSet<tCommitteMeetingPapers> tCommitteMeetingPapers { get; set; }
        public DbSet<RecipientGroupMember> tRecipientGroupMember { get; set; }
        public DbSet<FileNotingMovement> FileNotingMovement { get; set; }
        public DbSet<mCommitteeReplyItemType> mCommitteeReplyItemType { get; set; }
        public DbSet<eFilePaperSno> eFilePaperSno { get; set; }
        public DbSet<tCommitteReplyPendency> tCommitteReplyPendency { get; set; }
        public DbSet<tBranchesCommittee> tBranchesCommittee { get; set; }
        public DbSet<AuthEmployee> AuthEmployee { get; set; }
        // new table 
        public DbSet<tDrafteFile_Data> tDrafteFile_Data { get; set; }
        public DbSet<ReadTableData> ReadTableData { get; set; }

        //New Table By Lakshay
        public DbSet<pHouseCommitteeFiles> pHouseCommitteeFiles { get; set; }
        public DbSet<pHouseCommitteeSubFiles> pHouseCommitteeSubFiles { get; set; }
        public DbSet<pHouseCommitteeItemPendency> pHouseCommitteeItemPendency { get; set; }
        public DbSet<pHouseCommitteeSubItemPendency> pHouseCommitteeSubItemPendency { get; set; }
        public DbSet<mCommitteeReplySubItemType> mCommitteeReplySubItemType { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "GetCommitteUser": { return GetCommitteUser(param.Parameter); }
                case "AddeFile": { return AddeFile(param.Parameter); }
                case "GetAlleFile": { return GetAlleFile(param.Parameter); }
                case "GeteFile": { return GeteFile(param.Parameter); }
                case "GetCommitteeeFile": { return GetCommitteeeFile(param.Parameter); }
                case "AddeFileAttachments": return AddeFileAttachments(param.Parameter);
                case "GetAlleFileAttachments": return GetAlleFileAttachments(param.Parameter);
                case "GetAlleFileAttachmentsDep": return GetAlleFileAttachmentsDep(param.Parameter);
                case "GetAlleFileAttachmentsDep2": return GetAlleFileAttachmentsDep2(param.Parameter);
                case "GetAlleFileAttachmentsFromDep": return GetAlleFileAttachmentsFromDep(param.Parameter);
                case "GeteFileAttachmentDep": return GeteFileAttachmentDep(param.Parameter);
                case "GeteFileAttachment": return GeteFileAttachment(param.Parameter);
                case "GeteFileSearch": return GeteFileSearch(param.Parameter);
                case "EditeFile": return EditeFile(param.Parameter);
                case "eFileDocumentIndex": return eFileDocumentIndex(param.Parameter);
                case "GetDepartmentMembers": return GetDepartmentMembers(param.Parameter);
                case "GeteFileAttachmentonDepartment": return GeteFileAttachmentonDepartment(param.Parameter);
                case "GeteFileDepartmentReply": return GeteFileDepartmentReply(param.Parameter);
                case "eFileDeActive": { return FileDeActive(param.Parameter); }
                case "eFileActive": { return eFileActive(param.Parameter); }
                case "GeteFileReport": { return GeteFileReport(); }
                case "eFileAttachmentStatus": { return eFileAttachmentStatus(param.Parameter); }
                case "UpdateFileSendStatus": { return UpdateFileStatus(param.Parameter); }
                case "GeteFileDetailsByFileId": { return GeteFileDetailsByFileId(param.Parameter); }

                //shashi
                case "UpdateeFileNewDescriptionByFileId": { return UpdateeFileNewDescriptionByFileId(param.Parameter); }
                case "UpdateeFileDetailsByFileId": { return UpdateeFileDetailsByFileId(param.Parameter); }
                case "GetSendPaperDetailsByDeptId": { return GetSendPaperDetailsByDeptId(param.Parameter); }
                case "GetReceivePaperDetailsByDeptId": { return GetReceivePaperDetailsByDeptId(param.Parameter); }
                case "eFileList": { return eFileList(param.Parameter); }
                case "UpdateReceivePaperSingle": { return UpdateReceivePaperSingle(param.Parameter); }
                case "GetValuesByRefID": { return GetValuesByRefID(param.Parameter); }
                case "GetAlleFileAttachmentsByDeptIDandeFileID": { return GetAlleFileAttachmentsByDeptIDandeFileID(param.Parameter); }
                case "AttachPapertoEFileMul": { return AttachPapertoEFileMul(param.Parameter); }
                case "Get_eFilePaperNatureList": { return Get_eFilePaperNatureList(param.Parameter); }
                case "Get_eFilePaperTypeList": { return Get_eFilePaperTypeList(param.Parameter); }
                case "eFileListDeactive": { return eFileListDeactive(param.Parameter); }
                case "GetPaperDetailsByRefNo": { return GetPaperDetailsByRefNo(param.Parameter); }
                case "AttacheFiletoCommitteeMul": { return AttacheFiletoCommitteeMul(param.Parameter); }
                case "AddeFileNotings": { return AddeFileNotings(param.Parameter); }
                case "eFileListALLStatus": { return eFileListALLStatus(param.Parameter); }
                case "GetPNo": { return GetPNo(param.Parameter); }
                case "GeteFileNotingDetailsByeFileID": { return GeteFileNotingDetailsByeFileID(param.Parameter); }
                case "GeteNotingIDByeFileIdandSeqNo": { return GeteNotingIDByeFileIdandSeqNo(param.Parameter); }
                case "UpdateLasteFileNotings": { return UpdateLasteFileNotings(param.Parameter); }
                case "Get_eFilePaperNatureList_Receive": { return Get_eFilePaperNatureList_Receive(param.Parameter); }
                case "GetCommitteeProoceedingByDept": { return GetCommitteeProoceedingByDept(param.Parameter); }
                case "GetCommitteeUploadedPdf": { return GetCommitteeUploadedPdf(param.Parameter); }
                case "GetTreeViewStructure": { return GetTreeViewStructure(param.Parameter); }
                case "eFileListByCommitteeId": { return eFileListByCommitteeId(param.Parameter); }
                case "eFileListDeactiveByCommitteeId": { return eFileListDeactiveByCommitteeId(param.Parameter); }
                case "GetMembersByAssemblyID": { return GetMembersByAssemblyID(param.Parameter); }
                case "GetStaffDetails": { return GetStaffDetails(param.Parameter); }
                case "eFileTypeDetails": { return eFileTypeDetails(param.Parameter); }
                case "GetGeneratedPdfList": { return GetGeneratedPdfList(param.Parameter); }
                case "AttachPapertoEFileMulR": { return AttachPapertoEFileMulR(param.Parameter); }
                case "saveCommitteeProceeding": { return saveCommitteeProceeding(param.Parameter); }
                case "GetAlleFileAttachmentsByeFileID": { return GetAlleFileAttachmentsByeFileID(param.Parameter); }
                case "CheckUpdateDiary": { return CheckUpdateDiary(param.Parameter); }
                case "GBranchList": { return GBranchList(param.Parameter); }
                case "BranchsForAasign": { return BranchsForAasign(param.Parameter); }
                case "GetEmployeeByBranch": { return GetEmployeeByBranch(param.Parameter); }
                case "UpdateMovements": { return UpdateMovements(param.Parameter); }
                case "GetReceivePaperDetailsByDeptIdHC": { return GetReceivePaperDetailsByDeptIdHC(param.Parameter); }
                case "MovementList": { return MovementList(param.Parameter); }
                case "Draftpapers": { return Draftpapers(param.Parameter); }
                case "GetDraftList": { return GetDraftList(param.Parameter); }
                case "DeleteDraftPapers": { return DeleteDraftPapers(param.Parameter); }
                case "GetDepartment": { return GetDepartment(param.Parameter); }
                case "GetMembers": { return GetMembers(param.Parameter); }
                case "GetOtherThanHP": { return GetOtherThanHP(param.Parameter); }
                case "GetDataList": { return GetDataList(param.Parameter); }
                case "UpdateDraftDatabyId": { return UpdateDraftDatabyId(param.Parameter); }
                case "UpdateNoting": { return UpdateNoting(param.Parameter); }
                case "NotingList": { return NotingList(param.Parameter); }
                case "DraftMovementList": { return DraftMovementList(param.Parameter); }
                case "NotingMovementList": { return NotingMovementList(param.Parameter); }
                case "SendDraftDatabyId": { return SendDraftDatabyId(param.Parameter); }
                case "SentToDept": { return SentToDept(param.Parameter); }
                case "GetSendList": { return GetSendList(param.Parameter); }
                case "DraftpapersSent": { return DraftpapersSent(param.Parameter); }
                case "NotingMovementListFiles": { return NotingMovementListFiles(param.Parameter); }
                case "GetBranchId": { return GetBranchId(param.Parameter); }
                case "GetBranchIdDraftList": { return GetBranchIdDraftList(param.Parameter); }
                case "GetDataPathsforMerge": { return GetDataPathsforMerge(param.Parameter); }
                case "GetDetailsSentList": { return GetDetailsSentList(param.Parameter); }
                case "GetFileItemList": { return GetFileItemList(param.Parameter); }
                case "NewDraftEntry": { return NewDraftEntry(param.Parameter); }
                case "AttachPapertoEFileMulDraftR": { return AttachPapertoEFileMulDraftR(param.Parameter); }
                case "GetPendencyData": { return GetPendencyData(param.Parameter); }
                case "GetDeptfodisply": { return GetDeptfodisply(param.Parameter); }
                case "NewDeptDraftEntry": { return NewDeptDraftEntry(param.Parameter); }
                case "SentDept": { return SentDept(param.Parameter); }
                case "GetComtbyBranch": { return GetComtbyBranch(param.Parameter); }
                case "GetAuthorizeEmp": { return GetAuthorizeEmp(param.Parameter); }
                case "Get_eFilePaperNatureList_ReceiveDept": { return Get_eFilePaperNatureList_ReceiveDept(param.Parameter); }
                case "Get_eFilePaperNatureList_ReceiveDeptDraftNew": { return Get_eFilePaperNatureList_ReceiveDeptDraftNew(param.Parameter); }
                case "NotingBacklog": { return NotingBacklog(param.Parameter); }


                // robin
                case "eFileListR": { return eFileListR(param.Parameter); }
                case "GetDesigNameByAadharId": { return GetDesigNameByAadharId(param.Parameter); }
                case "GetReceivePaperForVerify": { return GetReceivePaperForVerify(param.Parameter); }
                case "GetCommittees": { return GetCommittees(param.Parameter); }
                case "GetCommitteeProoceeding_new": { return GetCommitteeProoceeding_new(param.Parameter); }
                case "GetCommitteeProoceeding_ById": { return GetCommitteeProoceeding_ById(param.Parameter); }
                case "LockedCommitteeProceeding": { return LockedCommitteeProceeding(param.Parameter); }
                case "GetCommitteeProoceeding_ByCommittee": { return GetCommitteeProoceeding_ByCommittee(param.Parameter); }
                case "Save_ReadDataInTable": { return Save_ReadDataInTable(param.Parameter); }
                case "check_draftIdinEfile": { return check_draftIdinEfile(param.Parameter); }
                ////Get read datalist by robin
                case "GetReadDataTableList": { return GetReadDataTableList(param.Parameter); }//GetAllBranchList
                case "GetAllBranchList": { return GetAllBranchList(param.Parameter); }
                case "Procedding_Draftpapers": { return Procedding_Draftpapers(param.Parameter); }
                case "GetBranchByCommitteeId": { return GetBranchByCommitteeId(param.Parameter); }
                case "GetIPAddress": { return GetIPAddress(); }
                case "GetMACAddress": { return GetMACAddress(); }
                case "GetReceivePaperDetailsByDeptIdHC_onclick": { return GetReceivePaperDetailsByDeptIdHC_onclick(param.Parameter); }

                //For New House Committe Module

                case "nGeteFilesByCommitteeId": { return nGeteFilesByCommitteeId(param.Parameter); }
                case "NeFileList": { return NeFileList(param.Parameter); }
                case "SaveDraftedPaper": { return SaveDraftedPaper(param.Parameter); }
                //case "GetAllHouseComFiles": { return GetAllHouseComFiles(param.Parameter); }
                case "NAlleFileList": { return NAlleFileList(param.Parameter); }
                case "GetFileDetailsByeFileId": { return GetFileDetailsByeFileId(param.Parameter); }
                case "SaveToComposedLetter": { return SaveToComposedLetter(param.Parameter); }
                case "GetCustomCommFiles": { return GetCustomCommFiles(param.Parameter); }
                case "GetSubCommFilesByParentId": { return GetSubCommFilesByParentId(param.Parameter); }
                case "GetDetailsByParentId": { return GetDetailsByParentId(param.Parameter); }
                case "nGetCommiteeIDAndNameByBranchID": { return nGetCommiteeIDAndNameByBranchID(param.Parameter); }
                case "GetSearchedList": { return GetSearchedList(param.Parameter); }
                //case "UpdateLetterStatus": { return UpdateLetterStatus(param.Parameter); }
                // case "UpdateChildLetterStatus": { return UpdateChildLetterStatus(param.Parameter); }

                //For new eFiles
                case "GetAlleFiles": { return GetAlleFiles(param.Parameter); }
                case "SaveNewFileEntry": { return SaveNewFileEntry(param.Parameter); }
                case "GeteFilesListToLink": { return GeteFilesListToLink(param.Parameter); }
                case "GeteFileById": { return GeteFileById(param.Parameter); }
                case "UpdateFileEntry": { return UpdateFileEntry(param.Parameter); }
                case "GetAllDocumentsByFileId": { return GetAllDocumentsByFileId(param.Parameter); }
                case "GetFilesByLinkedId": { return GetFilesByLinkedId(param.Parameter); }
                case "GeteFileDetailsByID": { return GeteFileDetailsByID(param.Parameter); }


                //For CommitteeItemPendency
                case "SaveNewPendency": { return SaveNewPendency(param.Parameter); }
                case "SaveToComposedPendency": { return SaveToComposedPendency(param.Parameter); }
                case "GetSubItemList": { return GetSubItemList(param.Parameter); }
                case "GetCustomFileItemList": { return GetCustomFileItemList(param.Parameter); }
                case "nGeteFilesByCommnDeptId": { return nGeteFilesByCommnDeptId(param.Parameter); }
                case "GetAllCommitteeList": { return GetAllCommitteeList(param.Parameter); }

                case "UpdateAcceptStatus": { return UpdateAcceptStatus(param.Parameter); }
                case "RejectDocumentsStatus": { return RejectDocumentsStatus(param.Parameter); }
                    
                    



            }
            return null;
        }
        //public static string GetIPAddress()
        //{

        //    try
        //    {

        //        string ipAddress = "";
        //        IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
        //        ipAddress = Convert.ToString(ipHostInfo.AddressList.FirstOrDefault(address => address.AddressFamily == AddressFamily.InterNetwork));


        //        return ipAddress;
        //    }
        //    catch
        //    {
        //        return "";
        //    }

        //}
        //public static string GetMACAddress()
        //{
        //    NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
        //    String sMacAddress = string.Empty;
        //    foreach (NetworkInterface adapter in nics)
        //    {
        //        if (sMacAddress == String.Empty)// only return MAC Address from first card
        //        {
        //            IPInterfaceProperties properties = adapter.GetIPProperties();
        //            sMacAddress = adapter.GetPhysicalAddress().ToString();
        //        }
        //    } return sMacAddress;
        //}
        public static object UpdateFileStatus(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int AttachId = Convert.ToInt32(param);
                    var SelectUpdateresult = (from attachments in context.eFileAttachments where attachments.eFileAttachmentId == AttachId select attachments).FirstOrDefault();
                    SBL.DomainModel.Models.eFile.eFileAttachment attchmentUpdate = new DomainModel.Models.eFile.eFileAttachment();
                    attchmentUpdate = SelectUpdateresult;
                    attchmentUpdate.SendStatus = true;
                    context.Entry(attchmentUpdate).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public static object GeteFileReport()
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    List<SBL.DomainModel.Models.eFile.eFileAttachment> objeFiles = (from values in context.eFileAttachments where values.Type == "Report" select values).ToList();
                    return objeFiles;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        public static object eFileActive(object param)
        {
            //int eFileId;
            try
            {
                int eFileId = Convert.ToInt32(param);

                using (eFileContext Context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFile objeFiles = (from values in Context.eFiles where values.eFileID == eFileId select values).FirstOrDefault();
                    objeFiles.Status = true;
                    Context.eFiles.Attach(objeFiles);
                    Context.Entry(objeFiles).State = EntityState.Modified;
                    Context.SaveChanges();
                    return objeFiles.CommitteeId;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        public static object FileDeActive(object param)
        {
            //int eFileId;
            try
            {
                int eFileId = Convert.ToInt32(param);

                using (eFileContext Context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFile objeFiles = (from values in Context.eFiles where values.eFileID == eFileId select values).FirstOrDefault();
                    objeFiles.Status = false;
                    Context.eFiles.Attach(objeFiles);
                    Context.Entry(objeFiles).State = EntityState.Modified;
                    Context.SaveChanges();
                    return objeFiles.CommitteeId;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        public static object eFileAttachmentStatus(object param)
        {
            //SBL.DomainModel.Models.eFile.eFileAttachment model = (SBL.DomainModel.Models.eFile.eFileAttachment)param;
            //int eFileId;
            try
            {
                int eFileId = Convert.ToInt32(param);

                using (eFileContext Context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFileAttachment objeFiles = (from values in Context.eFileAttachments where values.eFileAttachmentId == eFileId select values).FirstOrDefault();
                    objeFiles.SendStatus = true;
                    Context.eFileAttachments.Attach(objeFiles);
                    Context.Entry(objeFiles).State = EntityState.Modified;
                    Context.SaveChanges();
                    return objeFiles.SendStatus;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GetCommitteeeFile(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    string departmentId = Convert.ToString(param);
                    var result = (from value in context.eFiles
                                  where value.DepartmentId == departmentId
                                  select value).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GeteFileAttachment(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int eAttachId = Convert.ToInt32(param);
                    var result = (from value in context.eFileAttachments where value.eFileAttachmentId == eAttachId select new SBL.DomainModel.Models.eFile.DepartmentReply { eFileName = value.eFileName, eFileID = value.eFileID }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GeteFileDepartmentReply(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int ParentId = Convert.ToInt32(param);
                    ParentId = 1;
                    var result = (from value in context.eFileAttachments where value.Type == "FromDepartment" && value.ParentId == ParentId select new SBL.DomainModel.Models.eFile.DepartmentReply { CreatedDate = value.CreatedDate, Description = value.Description, eFileAttachmentId = value.eFileAttachmentId, Title = value.Title }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GeteFileAttachmentonDepartment(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    string Id = Convert.ToString(param);
                    var result = (from value in context.eFileAttachments where value.SendStatus == false && value.Type == "CommitteeToDept" && value.ParentId == 0 select value).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GetDepartmentMembers(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    string Id = Convert.ToString(param);
                    var result = (from users in context.users
                                  join member in context.members
                                      on users.AadarId equals member.AadhaarNo
                                  //where users.DeptId == "HPD0012"
                                  select new SBL.DomainModel.Models.Member.DepartmentMembers { MemberID = member.MemberID, Email = member.Email, Mobile = member.Mobile }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object eFileDocumentIndex(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int Id = Convert.ToInt32(param);
                    var result = (from attachments in context.eFileAttachments
                                  join docType in context.documentType
                                      on attachments.DocumentTypeId equals docType.DocumentTypeID
                                  where attachments.eFileID == Id
                                  //select new SBL.DomainModel.Models.Document.mDocumentType
                                  //{
                                  //    DocumentType = docType.DocumentType,
                                  //    DocumentTypeID = docType.DocumentTypeID
                                  //}).ToList();
                                  select docType).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object EditeFile(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int eFileId = Convert.ToInt32(param);
                    SBL.DomainModel.Models.eFile.eFile eFile = (from data in context.eFiles
                                                                where data.eFileID == eFileId
                                                                select data).FirstOrDefault();
                    return eFile;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        private static object GeteFileSearch(object param)
        {
            SBL.DomainModel.Models.eFile.eFileSearch search = new DomainModel.Models.eFile.eFileSearch();
            search = param as SBL.DomainModel.Models.eFile.eFileSearch;
            using (eFileContext context = new eFileContext())
            {
                SBL.DomainModel.Models.eFile.eFileListAll eFileListAll = new DomainModel.Models.eFile.eFileListAll();
                eFileListAll.eFileLists = (from efilevalues in context.eFiles
                                           join departmentvalues in context.departments
                                           on efilevalues.DepartmentId equals departmentvalues.deptId
                                           join user in context.users
                                           on efilevalues.CreatedBy equals user.UserId
                                           where (departmentvalues.deptname.Contains(search.Department) ||
                                           efilevalues.eFileNumber.Contains(search.eFileNo) ||
                                           efilevalues.eFileSubject.Contains(search.Subject))
                                           && efilevalues.CommitteeId == search.CommitteeId
                                           && efilevalues.Status == search.Status
                                           select new SBL.DomainModel.Models.eFile.eFileList
                                           {
                                               Department = departmentvalues.deptname,
                                               eFileID = efilevalues.eFileID,
                                               eFileNumber = efilevalues.eFileNumber,
                                               eFileSubject = efilevalues.eFileSubject,
                                               Status = efilevalues.Status,
                                               CreateDate = efilevalues.CreatedDate,
                                               CreatedBy = user.UserName
                                           }).ToList();
                eFileListAll.Departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();

                return eFileListAll;
            }
        }

        private static object GetAlleFileAttachments(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var list = (from attachments in context.eFileAttachments where attachments.eFileID == eFileId select attachments).ToList();
                return list;
            }
        }

        private static object GeteFileAttachmentDep(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var list = (from attachments in context.eFileAttachments where attachments.eFileAttachmentId == eFileId select attachments).FirstOrDefault();
                return list;
            }
        }

        private static object GetAlleFileAttachmentsDep(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var list = (from attachments in context.eFileAttachments select attachments).ToList();
                return list;
            }
        }

        private static object GetAlleFileAttachmentsDep2(object param)
        {
            SBL.DomainModel.Models.eFile.eFileSearch search = new DomainModel.Models.eFile.eFileSearch();
            search = param as SBL.DomainModel.Models.eFile.eFileSearch;
            using (eFileContext context = new eFileContext())
            {
                var eFileListAll = (from efilevalues in context.eFileAttachments
                                    join departmentvalues in context.departments
                                    on efilevalues.DepartmentId equals departmentvalues.deptId
                                    join user in context.users
                                    on efilevalues.CreatedBy equals user.UserId
                                    join efiles in context.eFiles
                                    on efilevalues.eFileID equals efiles.eFileID
                                    where efilevalues.Type == "SendNoticetoDept"
                                    select new SBL.DomainModel.Models.eFile.eFileList
                                    {
                                        Department = departmentvalues.deptname,
                                        eFileAttachmentId = efilevalues.eFileAttachmentId,
                                        eFileID = efilevalues.eFileID,
                                        eFileNumber = efilevalues.eFileName,
                                        eFileSubject = efilevalues.Description,
                                        CreateDate = efilevalues.CreatedDate,
                                        CreatedBy = user.UserName,
                                        Status = efilevalues.SendStatus,
                                        Committee = (from committees in context.committees where committees.CommitteeId == efiles.CommitteeId select committees.CommitteeName).FirstOrDefault(),
                                        CommitteeType = (from committees in context.committees join committeTypes in context.committeeTypes on committees.CommitteeTypeId equals committeTypes.CommitteeTypeId where committees.CommitteeId == efiles.CommitteeId select committeTypes.CommitteeTypeName).FirstOrDefault(),
                                        Assembly = (from committees in context.committees join assembly in context.Assembly on committees.AssemblyID equals assembly.AssemblyCode where committees.CommitteeId == efiles.CommitteeId select assembly.AssemblyName).FirstOrDefault(),
                                        Session = (from committees in context.committees join session in context.Sessions on committees.SessionId equals session.SessionCode where committees.CommitteeId == efiles.CommitteeId select session.SessionName).FirstOrDefault()
                                    }).ToList();

                return eFileListAll;
            }
        }

        private static object GetAlleFileAttachmentsFromDep(object param)
        {
            SBL.DomainModel.Models.eFile.eFileSearch search = new DomainModel.Models.eFile.eFileSearch();
            search = param as SBL.DomainModel.Models.eFile.eFileSearch;
            using (eFileContext context = new eFileContext())
            {
                var eFileListAll = (from efilevalues in context.eFileAttachments
                                    join departmentvalues in context.departments
                                    on efilevalues.DepartmentId equals departmentvalues.deptId
                                    join user in context.users
                                    on efilevalues.CreatedBy equals user.UserId
                                    join efiles in context.eFiles
                                    on efilevalues.eFileID equals efiles.eFileID
                                    where efilevalues.Type == "ReplyByDep"
                                    select new SBL.DomainModel.Models.eFile.eFileList
                                    {
                                        Department = departmentvalues.deptname,
                                        eFileAttachmentId = efilevalues.eFileAttachmentId,
                                        eFileID = efilevalues.eFileID,
                                        eFileNumber = efilevalues.eFileName,
                                        eFileSubject = efilevalues.Description,
                                        CreateDate = efilevalues.CreatedDate,
                                        CreatedBy = user.UserName,
                                        Status = efilevalues.SendStatus,
                                        Committee = (from committees in context.committees where committees.CommitteeId == efiles.CommitteeId select committees.CommitteeName).FirstOrDefault(),
                                        CommitteeType = (from committees in context.committees join committeTypes in context.committeeTypes on committees.CommitteeTypeId equals committeTypes.CommitteeTypeId where committees.CommitteeId == efiles.CommitteeId select committeTypes.CommitteeTypeName).FirstOrDefault(),
                                        Assembly = (from committees in context.committees join assembly in context.Assembly on committees.AssemblyID equals assembly.AssemblyCode where committees.CommitteeId == efiles.CommitteeId select assembly.AssemblyName).FirstOrDefault(),
                                        Session = (from committees in context.committees join session in context.Sessions on committees.SessionId equals session.SessionCode where committees.CommitteeId == efiles.CommitteeId select session.SessionName).FirstOrDefault()
                                    }).ToList();

                return eFileListAll;
            }
        }

        private static object AddeFileAttachments(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFileAttachment model = param as SBL.DomainModel.Models.eFile.eFileAttachment;
                    //context..Add(model);
                    context.eFileAttachments.Add(model);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        private static object AddeFile(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
                model.Status = true;
                context.eFiles.Add(model);
                context.SaveChanges();
                context.Close();
            }
            return null;
        }

        private static SBL.DomainModel.Models.eFile.eFileListAll GetAlleFile(object param)
        {
            //using (eFileContext context = new eFileContext())
            //{
            //    int CommitteeId=Convert.ToInt32(param);
            //    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles join departmentvalues in context.departments
            //                                                                                                 on efilevalues.DepartmentId equals departmentvalues.deptId
            //                                                                                                 join user in context.users
            //                                                                                                 on efilevalues.CreatedBy equals user.UserId
            //                                                              where efilevalues.CommitteeId == CommitteeId
            //                                                              select new SBL.DomainModel.Models.eFile.eFileList
            //                                                              {
            //                                                                    Department=departmentvalues.deptname,
            //                                                                     eFileID=efilevalues.eFileID,
            //                                                                      eFileNumber=efilevalues.eFileNumber,
            //                                                                       eFileSubject=efilevalues.eFileSubject,
            //                                                                        Status=efilevalues.Status,
            //                                                                        CreateDate=efilevalues.CreatedDate,
            //                                                                        CreatedBy=user.UserName
            //                                                              }).ToList().OrderByDescending(x=>x.eFileID).ToList();
            //    return eFileList;
            //}

            //...................................

            using (eFileContext context = new eFileContext())
            {
                int CommitteeId = Convert.ToInt32(param);
                List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                          join departmentvalues in context.departments
                                                                          on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                          join user in context.users
                                                                          on efilevalues.CreatedBy equals user.UserId
                                                                          where efilevalues.CommitteeId == CommitteeId
                                                                          && efilevalues.Status == true
                                                                          select new SBL.DomainModel.Models.eFile.eFileList
                                                                          {
                                                                              Department = departmentvalues.deptname,
                                                                              eFileID = efilevalues.eFileID,
                                                                              eFileNumber = efilevalues.eFileNumber,
                                                                              eFileSubject = efilevalues.eFileSubject,
                                                                              Status = efilevalues.Status,
                                                                              CreateDate = efilevalues.CreatedDate,
                                                                              CreatedBy = user.UserName,
                                                                              olddesc = efilevalues.OldDescription,
                                                                              newdesc = efilevalues.NewDescription,
                                                                              CommitteeId = efilevalues.CommitteeId
                                                                          }).ToList().OrderByDescending(x => x.eFileID).ToList();
                List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                eFileListAlls.eFileLists = eFileList;
                eFileListAlls.Departments = departments;
                return eFileListAlls;
            }
        }

        private static SBL.DomainModel.Models.eFile.eFileList GeteFile(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var eFileList = (from efilevalues in context.eFiles
                                 join departmentvalues in context.departments
                                 on efilevalues.DepartmentId equals departmentvalues.deptId
                                 where efilevalues.eFileID == eFileId
                                 select new SBL.DomainModel.Models.eFile.eFileList
                                 {
                                     Department = departmentvalues.deptname,
                                     eFileID = efilevalues.eFileID,
                                     eFileNumber = efilevalues.eFileNumber,
                                     eFileSubject = efilevalues.eFileSubject,
                                     Status = efilevalues.Status
                                 }).FirstOrDefault();
                return eFileList;
            }
        }

        private static object GetCommitteUser(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                Guid UserId = Guid.Parse(param.ToString());
                var obj = (from CommitteeType in context.CommitteeTypePermission
                           where CommitteeType.UserId == UserId
                           select CommitteeType).FirstOrDefault();
                return obj;
            }
        }



        public static object GeteFileDetailsByFileId(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int eFileId = Convert.ToInt32(param.ToString());
                    //var eFileList = (from efilevalues in context.eFiles
                    //                 join officevalues in context.mOffice
                    //                 on efilevalues.eFileID equals officevalues.OfficeId
                    //                 where efilevalues.eFileID == eFileId
                    //                 select efilevalues).FirstOrDefault();
                    var eFileList = (from efilevalues in context.eFiles
                                     //join officevalues in context.mOffice
                                     // on efilevalues.Officecode equals officevalues.OfficeId
                                     //  join dept in context.departments on efilevalues.DepartmentId equals dept.deptId
                                     join dept in context.departments on efilevalues.SelectedDeptId equals dept.deptId
                                     where efilevalues.eFileID == eFileId
                                     select new
                                     {
                                         //  FileId=efilevalues.eFileID,
                                         //   OfficeName = officevalues.officename,
                                         //FileNo=efilevalues.eFileNumber,
                                         //Subject=efilevalues.eFileSubject,
                                         DepartmentId = efilevalues.DepartmentId,
                                         FileNumber = efilevalues.eFileNumber,
                                         OldDescription = efilevalues.OldDescription,
                                         NewDescription = efilevalues.NewDescription,
                                         FromYear = efilevalues.FromYear,
                                         ToYear = efilevalues.ToYear,
                                         eFileSubject = efilevalues.eFileSubject,
                                         Officecode = efilevalues.Officecode,
                                         DepartmentName = dept.deptname,
                                         eFileTypeID = efilevalues.eFileTypeID,
                                         SelectedDeptId = efilevalues.SelectedDeptId,


                                     }).FirstOrDefault();
                    SBL.DomainModel.Models.eFile.eFile obj = new DomainModel.Models.eFile.eFile();
                    if (eFileList != null)
                    {
                        // obj.eFileID = eFileList.FileId;
                        //  obj.OfficeName = eFileList.OfficeName;
                        obj.eFileNumber = eFileList.FileNumber;
                        // obj.FileNo = eFileList.FileId;
                        // obj.Subject = eFileList.FileId;
                        obj.OldDescription = eFileList.OldDescription;
                        obj.NewDescription = eFileList.NewDescription;
                        obj.FromYear = eFileList.FromYear;
                        obj.ToYear = eFileList.ToYear;
                        obj.DepartmentId = eFileList.DepartmentId;
                        obj.eFileSubject = eFileList.eFileSubject;
                        obj.Officecode = eFileList.Officecode;
                        obj.DepartmentName = eFileList.DepartmentName;
                        obj.eFileTypeID = eFileList.eFileTypeID;
                        obj.SelectedDeptId = eFileList.SelectedDeptId;
                    }
                    return obj;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public static object GeteFileDetailsByFileId(object param)
        //{
        //    try
        //    {
        //        using (eFileContext context = new eFileContext())
        //        {
        //            int eFileId = Convert.ToInt32(param.ToString());
        //            //var eFileList = (from efilevalues in context.eFiles
        //            //                 join officevalues in context.mOffice
        //            //                 on efilevalues.eFileID equals officevalues.OfficeId
        //            //                 where efilevalues.eFileID == eFileId
        //            //                 select efilevalues).FirstOrDefault();
        //            var eFileList = (from efilevalues in context.eFiles
        //                             join officevalues in context.mOffice
        //                             on efilevalues.Officecode equals officevalues.OfficeId
        //                             join dept in context.departments on efilevalues.DepartmentId equals dept.deptId
        //                             where efilevalues.eFileID == eFileId
        //                             select new
        //                             {
        //                                 //  FileId=efilevalues.eFileID,
        //                                 OfficeName = officevalues.officename,
        //                                 //FileNo=efilevalues.eFileNumber,
        //                                 //Subject=efilevalues.eFileSubject,
        //                                 DepartmentId = efilevalues.DepartmentId,
        //                                 FileNumber = efilevalues.eFileNumber,
        //                                 OldDescription = efilevalues.OldDescription,
        //                                 NewDescription = efilevalues.NewDescription,
        //                                 FromYear = efilevalues.FromYear,
        //                                 ToYear = efilevalues.ToYear,
        //                                 eFileSubject = efilevalues.eFileSubject,
        //                                 Officecode = efilevalues.Officecode,
        //                                 DepartmentName = dept.deptname,
        //                                 eFileTypeID = efilevalues.eFileTypeID
        //                             }).FirstOrDefault();
        //            SBL.DomainModel.Models.eFile.eFile obj = new DomainModel.Models.eFile.eFile();
        //            if (eFileList != null)
        //            {
        //                // obj.eFileID = eFileList.FileId;
        //                obj.OfficeName = eFileList.OfficeName;
        //                obj.eFileNumber = eFileList.FileNumber;
        //                // obj.FileNo = eFileList.FileId;
        //                // obj.Subject = eFileList.FileId;
        //                obj.OldDescription = eFileList.OldDescription;
        //                obj.NewDescription = eFileList.NewDescription;
        //                obj.FromYear = eFileList.FromYear;
        //                obj.ToYear = eFileList.ToYear;
        //                obj.DepartmentId = eFileList.DepartmentId;
        //                obj.eFileSubject = eFileList.eFileSubject;
        //                obj.Officecode = eFileList.Officecode;
        //                obj.DepartmentName = eFileList.DepartmentName;
        //                obj.eFileTypeID = eFileList.eFileTypeID;
        //            }
        //            return obj;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public static object UpdateeFileDetailsByFileId(object param)
        {
            try
            {
                SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
                eFileContext ctx = new eFileContext();
                SBL.DomainModel.Models.eFile.eFile obj = ctx.eFiles.Single(m => m.eFileID == model.eFileID);

                obj.FromYear = model.FromYear;
                obj.ToYear = model.ToYear;
                obj.DepartmentId = model.DepartmentId;
                obj.Officecode = model.Officecode;
                obj.eFileNumber = model.eFileNumber;
                obj.eFileSubject = model.eFileSubject;
                obj.OldDescription = model.OldDescription;
                obj.NewDescription = model.NewDescription;
                obj.CommitteeId = model.CommitteeId;
                obj.SelectedBranchId = model.SelectedBranchId;
                obj.SelectedDeptId = model.SelectedDeptId;
                ctx.SaveChanges();
                return 1; //Updated
            }
            catch
            {
                return 0; //Error
            }
        }

        public static object UpdateeFileNewDescriptionByFileId(object param)
        {
            try
            {
                SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
                eFileContext ctx = new eFileContext();
                SBL.DomainModel.Models.eFile.eFile obj = ctx.eFiles.Single(m => m.eFileID == model.eFileID);

                obj.NewDescription = model.NewDescription;

                ctx.SaveChanges();
                return 1; //Updated
            }
            catch
            {
                return 0; //Error
            }
        }

        #region Receive Paper List

        public static object GetReceivePaperDetailsByDeptId(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false //&& a.ToOfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }

        #endregion Receive Paper List

        #region Send paper List

        public static object GetSendPaperDetailsByDeptId(object param)
        {
            string[] str = param as string[];
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            // int Officecode = Convert.ToInt32(str[1]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.ToDepartmentID equals b.deptId
                                 into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID

                                 where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false //&& a.OfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     ToDepartmentId = a.ToDepartmentID,
                                     SendDate = a.SendDate,
                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.SendDate = item.SendDate;
                        mdl.ToDepartmentID = item.ToDepartmentId;
                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;
                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.ToDepartmentID equals b.deptId
                                 into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID

                                 where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false //&& a.OfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     ToDepartmentId = a.ToDepartmentID,
                                     SendDate = a.SendDate,
                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.SendDate = item.SendDate;
                        mdl.ToDepartmentID = item.ToDepartmentId;
                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;
                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }

        #endregion Send paper List

        #region eFile List Details

        private static eFileListAll eFileList(object param)
        {
            using (eFileContext context = new eFileContext())
            {

                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);

                string cid = Convert.ToString(str[3]);
                if (cid == "")
                {
                    cid = "0";
                }
                Int32 CommitteeId = Convert.ToInt32(cid);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);

                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }

                if (Officecode == 0)
                {
                    if (CommitteeId == 0)
                    {

                        List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                                  join departmentvalues in context.departments
                                                                                  on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                                  //join user in context.users
                                                                                  //on efilevalues.CreatedBy equals user.UserId
                                                                                  join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId// into Comms_leftjoin
                                                                                  //from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                                  join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin
                                                                                  //  from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                                  where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  // && (efilevalues.CommitteeId == CommitteeId)
                                                                                  //&& efilevalues.Officecode == Officecode
                                                                                  select new SBL.DomainModel.Models.eFile.eFileList
                                                                                  {
                                                                                      Department = departmentvalues.deptname,
                                                                                      eFileID = efilevalues.eFileID,
                                                                                      eFileNumber = efilevalues.eFileNumber,
                                                                                      eFileSubject = efilevalues.eFileSubject,
                                                                                      Status = efilevalues.Status,
                                                                                      CreateDate = efilevalues.CreatedDate,
                                                                                      //CreatedBy = user.UserName,
                                                                                      olddesc = efilevalues.OldDescription,
                                                                                      newdesc = efilevalues.NewDescription,
                                                                                      CommitteeId = efilevalues.CommitteeId,
                                                                                      DepartmentId = efilevalues.DepartmentId,
                                                                                      DeptAbbr = departmentvalues.deptabbr,
                                                                                      Committee = Comms.CommitteeName,
                                                                                      CommitteeAbbr = Comms.Abbreviation,
                                                                                      FromYear = efilevalues.FromYear,
                                                                                      ToYear = efilevalues.ToYear,
                                                                                      eFileTypeName = etypeobj.eType
                                                                                  }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                        List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                        SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                        eFileListAlls.eFileLists = eFileList;
                        eFileListAlls.Departments = departments;
                        return eFileListAlls;
                    }

                    else
                    {



                        List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                                  join departmentvalues in context.departments
                                                                                  on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                                  //join user in context.users
                                                                                  //on efilevalues.CreatedBy equals user.UserId
                                                                                  join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId// into Comms_leftjoin
                                                                                  //from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                                  join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin
                                                                                  //  from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                                  where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  && (efilevalues.CommitteeId == CommitteeId)
                                                                                  //&& efilevalues.Officecode == Officecode
                                                                                  select new SBL.DomainModel.Models.eFile.eFileList
                                                                                  {
                                                                                      Department = departmentvalues.deptname,
                                                                                      eFileID = efilevalues.eFileID,
                                                                                      eFileNumber = efilevalues.eFileNumber,
                                                                                      eFileSubject = efilevalues.eFileSubject,
                                                                                      Status = efilevalues.Status,
                                                                                      CreateDate = efilevalues.CreatedDate,
                                                                                      //CreatedBy = user.UserName,
                                                                                      olddesc = efilevalues.OldDescription,
                                                                                      newdesc = efilevalues.NewDescription,
                                                                                      CommitteeId = efilevalues.CommitteeId,
                                                                                      DepartmentId = efilevalues.DepartmentId,
                                                                                      DeptAbbr = departmentvalues.deptabbr,
                                                                                      Committee = Comms.CommitteeName,
                                                                                      CommitteeAbbr = Comms.Abbreviation,
                                                                                      FromYear = efilevalues.FromYear,
                                                                                      ToYear = efilevalues.ToYear,
                                                                                      eFileTypeName = etypeobj.eType
                                                                                  }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                        List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                        SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                        eFileListAlls.eFileLists = eFileList;
                        eFileListAlls.Departments = departments;
                        return eFileListAlls;
                    }
                }
                else
                {
                    if (CommitteeId == 0)
                    {
                        List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                                  join departmentvalues in context.departments
                                                                                  on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                                  join user in context.users
                                                                                  on efilevalues.CreatedBy equals user.UserId
                                                                                  join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId //into Comms_leftjoin
                                                                                  // from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                                  join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin
                                                                                  // from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                                  where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  // && efilevalues.Officecode == Officecode
                                                                                  //&& (CommitteeId.CommitteeId == CommitteeId)
                                                                                  select new SBL.DomainModel.Models.eFile.eFileList
                                                                                  {
                                                                                      Department = departmentvalues.deptname,
                                                                                      eFileID = efilevalues.eFileID,
                                                                                      eFileNumber = efilevalues.eFileNumber,
                                                                                      eFileSubject = efilevalues.eFileSubject,
                                                                                      Status = efilevalues.Status,
                                                                                      CreateDate = efilevalues.CreatedDate,
                                                                                      CreatedBy = user.UserName,
                                                                                      olddesc = efilevalues.OldDescription,
                                                                                      newdesc = efilevalues.NewDescription,
                                                                                      CommitteeId = efilevalues.CommitteeId,
                                                                                      DepartmentId = efilevalues.DepartmentId,
                                                                                      DeptAbbr = departmentvalues.deptabbr,
                                                                                      Committee = Comms.CommitteeName,
                                                                                      CommitteeAbbr = Comms.Abbreviation,
                                                                                      FromYear = efilevalues.FromYear,
                                                                                      ToYear = efilevalues.ToYear,
                                                                                      eFileTypeName = etypeobj.eType
                                                                                  }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                        List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                        SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                        eFileListAlls.eFileLists = eFileList;
                        eFileListAlls.Departments = departments;
                        return eFileListAlls;
                    }
                    else
                    {
                        List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                                  join departmentvalues in context.departments
                                                                                  on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                                  join user in context.users
                                                                                  on efilevalues.CreatedBy equals user.UserId
                                                                                  join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId //into Comms_leftjoin
                                                                                  // from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                                  join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin
                                                                                  // from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                                  where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                      // && efilevalues.Officecode == Officecode
                                                                                    && (efilevalues.CommitteeId == CommitteeId)
                                                                                  select new SBL.DomainModel.Models.eFile.eFileList
                                                                                  {
                                                                                      Department = departmentvalues.deptname,
                                                                                      eFileID = efilevalues.eFileID,
                                                                                      eFileNumber = efilevalues.eFileNumber,
                                                                                      eFileSubject = efilevalues.eFileSubject,
                                                                                      Status = efilevalues.Status,
                                                                                      CreateDate = efilevalues.CreatedDate,
                                                                                      CreatedBy = user.UserName,
                                                                                      olddesc = efilevalues.OldDescription,
                                                                                      newdesc = efilevalues.NewDescription,
                                                                                      CommitteeId = efilevalues.CommitteeId,
                                                                                      DepartmentId = efilevalues.DepartmentId,
                                                                                      DeptAbbr = departmentvalues.deptabbr,
                                                                                      Committee = Comms.CommitteeName,
                                                                                      CommitteeAbbr = Comms.Abbreviation,
                                                                                      FromYear = efilevalues.FromYear,
                                                                                      ToYear = efilevalues.ToYear,
                                                                                      eFileTypeName = etypeobj.eType
                                                                                  }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                        List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                        SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                        eFileListAlls.eFileLists = eFileList;
                        eFileListAlls.Departments = departments;
                        return eFileListAlls;
                    }
                }
            }
        }

        private static eFileListAll eFileListR(object param)
        {
            using (eFileContext context = new eFileContext())
            {

                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);

                int Officecode = 0;
                int.TryParse(str[1], out Officecode);

                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                // string DistDept = deptlist.Aggregate((a, x) => a + "," + x);

                string strBranchId = Convert.ToString(str[2]);
                int BrachId = 0;

                if (!string.IsNullOrEmpty(strBranchId))
                {
                    BrachId = Convert.ToInt32(strBranchId);
                }

                var VarCommittee = (from c in context.tBranchesCommittee
                                    join b in context.committees on c.CommitteeId equals b.CommitteeId
                                    where c.BranchId == BrachId
                                    select new
                                    {
                                        id = c.CommitteeId,
                                        name = b.CommitteeName
                                    }).FirstOrDefault();

                //string[] value = new string[2];
                //if (VarCommittee != null)
                //{
                //    value[0] = Convert.ToString(VarCommittee.id);
                //    value[1] = VarCommittee.name;
                //}

                if (Officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              // on efilevalues.SelectedDeptId equals departmentvalues.deptId
                                                                              //join user in context.users
                                                                              //on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                              && efilevalues.CommitteeId == VarCommittee.id
                                                                              //&& efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  //Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  //CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  //  DeptAbbr = departmentvalues.deptabbr,
                                                                                  DeptAbbr = (from mc in context.departments where mc.deptId == efilevalues.SelectedDeptId select mc.deptabbr).FirstOrDefault(),
                                                                                  Committee = Comms.CommitteeName,
                                                                                  CommitteeAbbr = Comms.Abbreviation,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType,
                                                                                  Department = (from mc in context.departments where mc.deptId == efilevalues.SelectedDeptId select mc.deptname).FirstOrDefault()

                                                                              }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              // on efilevalues.SelectedDeptId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  // && efilevalues.Officecode == Officecode
                                                                             && efilevalues.CommitteeId == VarCommittee.id
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  //Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = (from mc in context.departments where mc.deptId == efilevalues.SelectedDeptId select mc.deptabbr).FirstOrDefault(),
                                                                                  // DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  CommitteeAbbr = Comms.Abbreviation,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType,
                                                                                  Department = (from mc in context.departments where mc.deptId == efilevalues.SelectedDeptId select mc.deptname).FirstOrDefault()
                                                                              }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        private static eFileListAll eFileListDeactive(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);
                //     int Officecode = Convert.ToInt32(str[1]);
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (Officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == false && efilevalues.IsDeleted == false
                                                                              //&& efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == false && efilevalues.IsDeleted == false
                                                                              // && efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        private static eFileListAll eFileListALLStatus(object param)
        {
            SBL.DomainModel.Models.eFile.eFile mdl = param as SBL.DomainModel.Models.eFile.eFile;
            using (eFileContext context = new eFileContext())
            {
                string DeptId = mdl.DepartmentId;
                int officecode = mdl.Officecode;
                int CommId = mdl.CommitteeId;
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) &&
                                                                              efilevalues.CommitteeId == CommId && efilevalues.IsDeleted == false
                                                                              // && efilevalues.Officecode == officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) &&
                                                                              efilevalues.CommitteeId == CommId && efilevalues.IsDeleted == false
                                                                              // && efilevalues.Officecode == officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        private static eFileListAll eFileListByCommitteeId(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);
                //  int Officecode = Convert.ToInt32(str[1]);
                int CommitteeID = Convert.ToInt32(str[2]);
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (Officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                             && efilevalues.CommitteeId == CommitteeID
                                                                              //&& efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  // && efilevalues.Officecode == Officecode 
                                                                              && efilevalues.CommitteeId == CommitteeID
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        private static eFileListAll eFileListDeactiveByCommitteeId(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);
                //  int Officecode = Convert.ToInt32(str[1]);
                int CommitteeID = Convert.ToInt32(str[2]);
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (Officecode == 0)
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == false && efilevalues.IsDeleted == false
                                                                               && efilevalues.CommitteeId == CommitteeID
                                                                              //&& efilevalues.Officecode == Officecode
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
                else
                {
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join departmentvalues in context.departments
                                                                              on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                              join user in context.users
                                                                              on efilevalues.CreatedBy equals user.UserId
                                                                              join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                              from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                              join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                              from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                              where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == false && efilevalues.IsDeleted == false
                                                                                  //&& efilevalues.Officecode == Officecode 
                                                                              && efilevalues.CommitteeId == CommitteeID
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  Department = departmentvalues.deptname,
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  Status = efilevalues.Status,
                                                                                  CreateDate = efilevalues.CreatedDate,
                                                                                  CreatedBy = user.UserName,
                                                                                  olddesc = efilevalues.OldDescription,
                                                                                  newdesc = efilevalues.NewDescription,
                                                                                  CommitteeId = efilevalues.CommitteeId,
                                                                                  DepartmentId = efilevalues.DepartmentId,
                                                                                  DeptAbbr = departmentvalues.deptabbr,
                                                                                  Committee = Comms.CommitteeName,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileTypeName = etypeobj.eType
                                                                              }).ToList().OrderByDescending(x => x.CreateDate).ToList();
                    List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    eFileListAlls.Departments = departments;
                    return eFileListAlls;
                }
            }
        }

        #endregion eFile List Details

        public static object UpdateReceivePaperSingle(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;
                using (eFileContext ctx = new eFileContext())
                {
                    eFileAttachment model = ctx.eFileAttachments.Single(a => a.eFileAttachmentId == mdl.eFileAttachmentId);
                    model.eFileID = mdl.eFileID;
                    model.ReceivedDate = mdl.ReceivedDate;
                    model.PaperStatus = mdl.PaperStatus;
                    ctx.SaveChanges();
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static eFileAttachment GetValuesByRefID(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFileAttachments where _ObjeFileContext.PaperRefNo == _ReferenceNo && _ObjeFileContext.PaperNature != 3 select _ObjeFileContext).FirstOrDefault();
            return query;
        }

        private static object GetAlleFileAttachmentsByDeptIDandeFileID(object param)
        {
            eFileAttachment mdl = param as eFileAttachment;
            using (eFileContext context = new eFileContext())
            {
                //int eFileId = mdl.eFileID;

                var list = (from attachments in context.eFileAttachments where attachments.eFileID == mdl.eFileID && attachments.ToDepartmentID == mdl.DepartmentId select attachments).ToList();
                return list;
            }
        }
        private static object GetAlleFileAttachmentsByeFileID(object param)
        {
            eFileAttachment mdl = param as eFileAttachment;
            using (eFileContext context = new eFileContext())
            {
                //int eFileId = mdl.eFileID;

                var obj = (from attachments in context.eFileAttachments where attachments.eFileAttachmentId == mdl.eFileAttachmentId select attachments).FirstOrDefault();
                return obj;
            }
        }

        public static object AttachPapertoEFileMul(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;

                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {
                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            eFileAttachment model = ctx.eFileAttachments.Single(a => a.eFileAttachmentId == mdl.eFileAttachmentId);
                            model.eFileID = mdl.eFileID;
                            model.ReFileID = mdl.eFileID;
                            //model.ReceivedDate = mdl.ReceivedDate;
                            //model.PaperStatus = mdl.PaperStatus;
                            ctx.SaveChanges();
                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static object AttachPapertoEFileMulR(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;

                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {
                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            eFileAttachment model = ctx.eFileAttachments.Single(a => a.eFileAttachmentId == mdl.eFileAttachmentId);
                            model.ReFileID = mdl.ReFileID;
                            model.eFileID = mdl.ReFileID;
                            //model.ReceivedDate = mdl.ReceivedDate;
                            //model.PaperStatus = mdl.PaperStatus;
                            ctx.SaveChanges();
                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static object AttacheFiletoCommitteeMul(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;

                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {
                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            SBL.DomainModel.Models.eFile.eFile model = ctx.eFiles.Single(a => a.eFileID == mdl.eFileAttachmentId);
                            model.CommitteeId = mdl.eFileID;
                            //model.ReceivedDate = mdl.ReceivedDate;
                            //model.PaperStatus = mdl.PaperStatus;
                            ctx.SaveChanges();
                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static object Get_eFilePaperNatureList(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFilePaperNature select _ObjeFileContext).ToList();
            return query;
        }

        //public static object Get_eFilePaperNatureList_Receive(object param)
        //{
        //    string _ReferenceNo = Convert.ToString(param);
        //    eFileContext ctxt = new eFileContext();
        //    var query = (from _ObjeFileContext in ctxt.eFilePaperNature where _ObjeFileContext.PaperNatureID != 1 && _ObjeFileContext.PaperNatureID != 4 && _ObjeFileContext.PaperNatureID != 5 select _ObjeFileContext).ToList();
        //    return query;
        //}

        public static object Get_eFilePaperNatureList_Receive(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFilePaperNature where _ObjeFileContext.ActiveVs == true select _ObjeFileContext).ToList();
            return query;
        }
        public static object Get_eFilePaperNatureList_ReceiveDept(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFilePaperNature where _ObjeFileContext.IsActive == true select _ObjeFileContext).ToList();
            return query;
        }
        public static object Get_eFilePaperNatureList_ReceiveDeptDraftNew(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFilePaperNature where _ObjeFileContext.ActiveDept == true select _ObjeFileContext).ToList();
            return query;
        }
        public static object Get_eFilePaperTypeList(object param)
        {
            string _ReferenceNo = Convert.ToString(param);
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.eFilePaperType select _ObjeFileContext).ToList();
            return query;
        }

        public static object GetPaperDetailsByRefNo(object param)
        {
            string RefNo = Convert.ToString(param);
            using (eFileContext ctx = new eFileContext())
            {
                var query = (from a in ctx.eFileAttachments
                             join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                             from ps in temp.DefaultIfEmpty()
                             join bd in ctx.departments on a.ToDepartmentID equals bd.deptId into tempbd
                             from psbd in tempbd.DefaultIfEmpty()
                             join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                             join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                             where a.PaperRefNo == RefNo
                             select new
                             {
                                 eFileAttachmentId = a.eFileAttachmentId,
                                 Department = ps.deptname ?? "OTHER",
                                 eFileName = a.eFileName,
                                 PaperNature = a.PaperNature,
                                 PaperNatureDays = a.PaperNatureDays ?? "0",
                                 eFilePath = a.eFilePath,
                                 Title = a.Title,
                                 Description = a.Description,
                                 CreateDate = a.CreatedDate,
                                 PaperStatus = a.PaperStatus,
                                 eFIleID = a.eFileID,
                                 PaperRefNo = a.PaperRefNo,
                                 DepartmentId = a.DepartmentId,
                                 OfficeCode = a.OfficeCode,
                                 ToDepartmentID = a.ToDepartmentID,
                                 ToOfficeCode = a.ToOfficeCode,
                                 ReplyStatus = a.ReplyStatus,
                                 ReplyDate = a.ReplyDate,
                                 ReceivedDate = a.ReceivedDate,
                                 DocumentTypeId = a.DocumentTypeId,

                                 ToPaperNature = a.ToPaperNature,
                                 ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                 LinkedRefNo = a.LinkedRefNo,
                                 RevedOption = a.RevedOption,
                                 RecvDetails = a.RecvDetails,
                                 ToRecvDetails = a.ToRecvDetails,
                                 eMode = a.eMode,
                                 RecvdPaperNature = c.PaperNature,
                                 DocuemntPaperType = d.PaperType,
                                 PType = a.PType,
                                 SendStatus = a.SendStatus,
                                 DeptAbbr = ps.deptabbr,
                                 ToDepartment = psbd.deptname,
                                 ToDepAbbr = psbd.deptabbr,
                                 PaperNo = a.PaperNo
                             }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                List<eFileAttachment> lst = new List<eFileAttachment>();
                foreach (var item in query)
                {
                    eFileAttachment mdl = new eFileAttachment();
                    mdl.DepartmentName = item.Department;
                    mdl.eFileAttachmentId = item.eFileAttachmentId;
                    mdl.eFileName = item.eFileName;
                    mdl.PaperNature = item.PaperNature;
                    mdl.PaperNatureDays = item.PaperNatureDays;
                    mdl.eFilePath = item.eFilePath;
                    mdl.Title = item.Title;
                    mdl.Description = item.Description;
                    mdl.CreatedDate = item.CreateDate;
                    mdl.PaperStatus = item.PaperStatus;
                    mdl.eFileID = item.eFIleID;
                    mdl.PaperRefNo = item.PaperRefNo;
                    mdl.DepartmentId = item.DepartmentId;
                    mdl.OfficeCode = item.OfficeCode;
                    mdl.ToDepartmentID = item.ToDepartmentID;
                    mdl.ToOfficeCode = item.ToOfficeCode;
                    mdl.ReplyStatus = item.ReplyStatus;
                    mdl.ReplyDate = item.ReplyDate;
                    mdl.ReceivedDate = item.ReceivedDate;
                    mdl.DocumentTypeId = item.DocumentTypeId;

                    mdl.ToPaperNature = item.ToPaperNature;
                    mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                    mdl.LinkedRefNo = item.LinkedRefNo;
                    mdl.RevedOption = item.RevedOption;
                    mdl.RecvDetails = item.RecvDetails;
                    mdl.ToRecvDetails = item.ToRecvDetails;
                    mdl.eMode = item.eMode;
                    mdl.RecvdPaperNature = item.RecvdPaperNature;

                    mdl.DocuemntPaperType = item.DocuemntPaperType;
                    mdl.PType = item.PType;
                    mdl.SendStatus = item.SendStatus;
                    mdl.DeptAbbr = item.DeptAbbr;
                    mdl.ToDepartmentName = item.ToDepartment;
                    mdl.ToDeptAbbr = item.ToDepAbbr;
                    mdl.PaperNo = item.PaperNo;
                    lst.Add(mdl);
                }

                return lst;
            }
        }

        #region eFile Noting

        private static object AddeFileNotings(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    eFileNoting model = param as eFileNoting;
                    //context..Add(model);
                    context.eFileNoting.Add(model);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        private static object UpdateLasteFileNotings(object param)
        {
            eFileNoting mdl = param as eFileNoting;
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    eFileNoting model = context.eFileNoting.Single(a => a.eNotingID == mdl.eNotingID);
                    model.eNotingFileName = mdl.eNotingFileName;
                    model.NotingStatus = mdl.NotingStatus;
                    model.ModifiedDate = mdl.ModifiedDate;
                    model.FilePath = mdl.FilePath;
                    model.Remark = mdl.Remark;
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public static object GetPNo(object param)
        {
            int efileid = Convert.ToInt32(param);
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    var Maxval = context.eFileNoting
                        .Where(a => a.eFileID == efileid)
                        .Max(a => a.FileUploadSeq);
                    if (Maxval == null)
                        Maxval = 0;

                    return Maxval;
                }
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public static object GeteNotingIDByeFileIdandSeqNo(object param)
        {
            eFileNoting model = param as eFileNoting;

            using (eFileContext context = new eFileContext())
            {
                var Maxval = (from a in context.eFileNoting
                              where a.eFileID == model.eFileID && a.FileUploadSeq == model.FileUploadSeq && a.IsDeleted == false
                              select a.eNotingID).FirstOrDefault();

                return Maxval;
            }
        }

        private static object GeteFileNotingDetailsByeFileID(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var list = (from attachments in context.eFileNoting where attachments.eFileID == eFileId select attachments).ToList();
                return list;
            }
        }

        #endregion eFile Noting

        private static object saveCommitteeProceeding(object param)
        {
            using (eFileContext dbContext = new eFileContext())
            {
                COmmitteeProceeding committe = param as COmmitteeProceeding;
                if (committe.ProID != 0)
                {
                    COmmitteeProceeding obj = dbContext.CommitteeProceeding.Single(m => m.ProID == committe.ProID);
                    obj.Remark = committe.Remark;
                    //obj.DeptID = committe.DeptID;
                    obj.CommitteeId = committe.CommitteeId;
                    obj.ProFileNamePdf = committe.ProFileNamePdf;
                    obj.ProFileNameWord = committe.ProFileNameWord;
                    obj.ProFilePathPdf = committe.ProFilePathPdf;
                    obj.ProFilePathWord = committe.ProFilePathWord;
                    obj.CreatedDate = DateTime.Now;
                    dbContext.SaveChanges();
                }
                else
                {
                    committe.CreatedDate = DateTime.Now;
                    committe.ForwardBy = null;
                    committe.ForwardDate = null;
                    dbContext.CommitteeProceeding.Add(committe);
                    dbContext.SaveChanges();
                }
                dbContext.Close();
            }
            return null;
        }
        private static object LockedCommitteeProceeding(object param)
        {
            using (eFileContext dbContext = new eFileContext())
            {
                COmmitteeProceeding committe = param as COmmitteeProceeding;

                COmmitteeProceeding obj = dbContext.CommitteeProceeding.Single(m => m.ProID == committe.ProID);
                obj.IsActive = true;
                obj.ForwardBy = committe.ForwardBy;
                obj.ForwardDate = DateTime.Now;
                dbContext.SaveChanges();
                dbContext.Close();

            }

            return null;
        }
        private static object updateCommitteeProceeding(object param)
        {
            using (eFileContext dbContext = new eFileContext())
            {
                COmmitteeProceeding committe = param as COmmitteeProceeding;
                dbContext.CommitteeProceeding.Add(committe);
                dbContext.SaveChanges();
            }
            return null;
        }

        private static object deleteCommitteeProceeding(object param)
        {
            using (eFileContext dbContext = new eFileContext())
            {
                COmmitteeProceeding committe = param as COmmitteeProceeding;
                dbContext.CommitteeProceeding.Add(committe);
                dbContext.SaveChanges();
            }
            return null;
        }

        private static object GetCommitteeProoceedingByDept(object param)
        {
            Guid UserID = new Guid();
            Guid.TryParse(param as string, out UserID);

            using (eFileContext context = new eFileContext())
            {

                var val = (from a in context.CommitteeTypePermission
                           where a.UserId == UserID
                           select a.CommitteeTypeId).Count();
                //For Repoter Binding 
                if (val == 0)
                {
                    var list = (from a in context.CommitteeProceeding
                                join b in context.committees on a.CommitteeId equals b.CommitteeId
                                where a.IsDeleted == false
                                select new
                                {
                                    ProID = a.ProID,
                                    CommitteeName = b.CommitteeName,
                                    ProFilePathPdf = a.ProFilePathPdf,
                                    ProFilePathWord = a.ProFilePathWord,
                                    Remark = a.Remark,
                                    CreatedDate = a.CreatedDate,
                                    CommitteeId = a.CommitteeId
                                }).OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<COmmitteeProceeding> mdl = new List<COmmitteeProceeding>();
                    foreach (var item in list)
                    {
                        COmmitteeProceeding m = new COmmitteeProceeding();
                        m.ProID = item.ProID;
                        m.CommitteeName = item.CommitteeName;
                        m.ProFilePathPdf = item.ProFilePathPdf;
                        m.ProFilePathWord = item.ProFilePathWord;
                        m.Remark = item.Remark;
                        m.CreatedDate = item.CreatedDate;
                        m.CommitteeId = item.CommitteeId;
                        m.IsReporter = true;
                        mdl.Add(m);
                    }
                    return mdl;
                }
                //Other than Repoter Binding 
                else
                {
                    var perm = (from a in context.CommitteeTypePermission
                                where a.UserId == UserID
                                select a.CommitteeTypeId).ToArray();
                    var list = (from a in context.CommitteeProceeding
                                join b in context.committees on a.CommitteeId equals b.CommitteeId
                                where a.IsDeleted == false && (perm).Contains(a.CommitteeId)
                                select new
                                {
                                    ProID = a.ProID,
                                    CommitteeName = b.CommitteeName,
                                    ProFilePathPdf = a.ProFilePathPdf,
                                    ProFilePathWord = a.ProFilePathWord,
                                    Remark = a.Remark,
                                    CreatedDate = a.CreatedDate,
                                    CommitteeId = a.CommitteeId
                                }).OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<COmmitteeProceeding> mdl = new List<COmmitteeProceeding>();
                    foreach (var item in list)
                    {
                        COmmitteeProceeding m = new COmmitteeProceeding();
                        m.ProID = item.ProID;
                        m.CommitteeName = item.CommitteeName;
                        m.ProFilePathPdf = item.ProFilePathPdf;
                        m.ProFilePathWord = item.ProFilePathWord;
                        m.Remark = item.Remark;
                        m.CreatedDate = item.CreatedDate;
                        m.CommitteeId = item.CommitteeId;
                        m.IsReporter = false;
                        mdl.Add(m);
                    }
                    return mdl;
                }

            }
        }
        private static object GetCommitteeProoceeding_new(object param)
        {
            //Guid UserID = new Guid();
            //Guid.TryParse(param as string, out UserID);
            COmmitteeProceeding committe = param as COmmitteeProceeding;
            if (committe.CurrentDesignation == "Chief Reporter" || committe.CurrentDesignation == "Editor of Debate")
            {
                using (eFileContext context = new eFileContext())
                {

                    var list = (from a in context.CommitteeProceeding
                                join b in context.committees on a.CommitteeId equals b.CommitteeId
                                join mu in context.users on a.Aadharid equals mu.AadarId
                                where a.IsDeleted == false
                                select new
                                {
                                    ProID = a.ProID,
                                    CommitteeName = b.CommitteeName,
                                    ProFilePathPdf = a.ProFilePathPdf,
                                    ProFilePathWord = a.ProFilePathWord,
                                    Remark = a.Remark,
                                    CreatedDate = a.CreatedDate,
                                    CommitteeId = a.CommitteeId,
                                    Isactive = a.IsActive,
                                    ReporterName = mu.UserName  // reporter name
                                }).OrderByDescending(a => a.ProID).ToList();
                    //   .OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<COmmitteeProceeding> mdl = new List<COmmitteeProceeding>();
                    foreach (var item in list)
                    {
                        COmmitteeProceeding m = new COmmitteeProceeding();
                        m.ProID = item.ProID;
                        m.CommitteeName = item.CommitteeName;
                        m.ProFilePathPdf = item.ProFilePathPdf;
                        m.ProFilePathWord = item.ProFilePathWord;
                        m.Remark = item.Remark;
                        m.CreatedDate = item.CreatedDate;
                        m.CommitteeId = item.CommitteeId;
                        m.IsReporter = true;
                        m.IsActive = item.Isactive;
                        m.CreateBy = item.ReporterName;
                        mdl.Add(m);
                    }
                    return mdl;

                }
            }
            else
            {
                using (eFileContext context = new eFileContext())
                {
                    var list = (from a in context.CommitteeProceeding
                                join b in context.committees on a.CommitteeId equals b.CommitteeId
                                where a.IsDeleted == false && a.Aadharid == committe.Aadharid
                                select new
                                {
                                    ProID = a.ProID,
                                    CommitteeName = b.CommitteeName,
                                    ProFilePathPdf = a.ProFilePathPdf,
                                    ProFilePathWord = a.ProFilePathWord,
                                    Remark = a.Remark,
                                    CreatedDate = a.CreatedDate,
                                    CommitteeId = a.CommitteeId,
                                    Isactive = a.IsActive,
                                }).OrderByDescending(a => a.ProID).ToList();
                    //   .OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<COmmitteeProceeding> mdl = new List<COmmitteeProceeding>();
                    foreach (var item in list)
                    {
                        COmmitteeProceeding m = new COmmitteeProceeding();
                        m.ProID = item.ProID;
                        m.CommitteeName = item.CommitteeName;
                        m.ProFilePathPdf = item.ProFilePathPdf;
                        m.ProFilePathWord = item.ProFilePathWord;
                        m.Remark = item.Remark;
                        m.CreatedDate = item.CreatedDate;
                        m.CommitteeId = item.CommitteeId;
                        m.IsReporter = true;
                        m.IsActive = item.Isactive;
                        mdl.Add(m);
                    }
                    return mdl;
                }
            }
        }
        private static object GetCommitteeProoceeding_ByCommittee(object param)
        {
            // COmmitteeProceeding committe = param as COmmitteeProceeding;
            Int32 ComID = Convert.ToInt32(param);
            using (eFileContext context = new eFileContext())
            {

                var list = (from a in context.CommitteeProceeding
                            join b in context.committees on a.CommitteeId equals b.CommitteeId
                            // join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                            //  from ps in temp.DefaultIfEmpty()
                            //join m in context.users on a.Aadharid equals m.AdhaarID into temp
                            // from ps in temp.DefaultIfEmpty()
                            where a.IsDeleted == false && a.CommitteeId == ComID && a.IsActive == true
                            select new
                            {
                                ProID = a.ProID,
                                CommitteeName = b.CommitteeName,
                                ProFilePathPdf = a.ProFilePathPdf,
                                ProFilePathWord = a.ProFilePathWord,
                                Remark = a.Remark,
                                CreatedDate = a.CreatedDate,
                                forwardDate = a.ForwardDate,
                                CommitteeId = a.CommitteeId,
                                Isactive = a.IsActive,
                                Forwardby = (from mc in context.users where mc.AadarId == a.ForwardBy select (mc.UserName)).FirstOrDefault(),
                                // uploadby =    ps.UserName +" ( " + ps.Designation + " ) " ,
                            }).OrderByDescending(a => a.ProID).ToList();

                List<COmmitteeProceeding> mdl = new List<COmmitteeProceeding>();
                foreach (var item in list)
                {
                    COmmitteeProceeding m = new COmmitteeProceeding();
                    m.ProID = item.ProID;
                    m.CommitteeName = item.CommitteeName;
                    m.ProFilePathPdf = item.ProFilePathPdf;
                    m.ProFilePathWord = item.ProFilePathWord;
                    m.Remark = item.Remark;
                    m.CreatedDate = item.CreatedDate;
                    m.CommitteeId = item.CommitteeId;
                    m.IsReporter = true;
                    m.IsActive = item.Isactive;
                    m.ForwardBy = item.Forwardby;
                    m.ForwardDate = item.forwardDate;
                    mdl.Add(m);
                }
                return mdl;

            }
        }
        private static object GetCommitteeProoceeding_ById(object param)
        {

            COmmitteeProceeding committe = param as COmmitteeProceeding;
            COmmitteeProceeding model = new COmmitteeProceeding();
            using (eFileContext context = new eFileContext())
            {
                var query = (from a in context.CommitteeProceeding
                             // join b in context.committees on a.CommitteeId equals b.CommitteeId
                             where a.ProID == committe.ProID
                             select a).FirstOrDefault();
                model.Remark = query.Remark;
                model.ProID = query.ProID;
                // model.DeptID = query.DeptID;
                model.CommitteeId = query.CommitteeId;
                model.Remark = query.Remark;
                model.ProFileNamePdf = query.ProFileNamePdf;
                model.ProFileNameWord = query.ProFileNameWord;
                model.ProFilePathPdf = query.ProFilePathPdf;
                model.ProFilePathWord = query.ProFilePathWord;
            }

            return model;


        }



        private static object GetCommitteeUploadedPdf(object param)
        {
            CommitteeMembersUpload model = param as CommitteeMembersUpload;
            int CommId = model.CommitteeId;
            using (eFileContext context = new eFileContext())
            {
                var list = (from a in context.CommitteeMembersUpload
                            where a.CommitteeId == CommId
                            select a).OrderByDescending(a => a.CommStatus).ToList();
                return list;
            }
        }

        private static object GetGeneratedPdfList(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                var list = (from a in context.CommitteeMembersUpload
                            where a.IsDeleted == false
                            select a).OrderByDescending(a => a.CommStatus).ToList();
                return list;
            }
        }

        private static object GetTreeViewStructure(object param)
        {
            TreeViewStructure objTreeViewStructure = param as TreeViewStructure;
            using (eFileContext context = new eFileContext())
            {
                var list = (from treeview in context.TreeViewStructure
                            select treeview).ToList();
                return list;
            }
        }

        private static object GetMembersByAssemblyID(object param)
        {
            SBL.DomainModel.Models.Member.mMemberAssembly parameter = param as SBL.DomainModel.Models.Member.mMemberAssembly;
            using (eFileContext context = new eFileContext())
            {
                var query = (from a in context.mMemberAssembly
                             join b in context.members on a.MemberID equals b.MemberCode
                             join c in context.mConstituency on
                                 // a.ConstituencyCode equals c.ConstituencyCode
                             new { code = a.ConstituencyCode, Id = parameter.AssemblyID } equals new { code = c.ConstituencyCode, Id = c.AssemblyID.Value }
                             where a.AssemblyID == parameter.AssemblyID
                             select new
                             {
                                 Name = b.Name + "( " + c.ConstituencyName + ")",
                                 MemberID = b.MemberID
                             }

                             ).OrderBy(a => a.Name);

                List<mMember> mdl = new List<mMember>();
                foreach (var item in query)
                {
                    mMember a = new mMember();
                    a.Name = item.Name;
                    a.MemberID = item.MemberID;
                    mdl.Add(a);
                }

                return mdl;
            }
        }

        private static object GetStaffDetails(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                var query = (from a in context.mStaff
                             where a.isActive == true && a.isAlive == true && a.Group == "Gazetted"
                             select a).OrderBy(a => a.StaffName).ToList();

                return query;
            }
        }

        // Add eFile Type
        private static object eFileTypeDetails(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    var result = (from a in context.eFileType
                                  where a.IsDeleted == 0
                                  select a).OrderBy(a => a.eType).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                string exception = ex.ToString();
            }
            return null;
        }

        public static object CheckUpdateDiary(object param)
        {
            eFileContext context = new eFileContext();
            eFileAttachment Obj = param as eFileAttachment;
            int CYear = Convert.ToInt32(Obj.Year); //Current Year
            string msg = "";
            var Number = (from Efile in context.eFileAttachments
                          where Efile.CurYear == CYear
                          orderby Efile.CurDNo descending
                          select Efile.CurDNo).FirstOrDefault();

            if (Number == 0)
            {
                Obj.DiaryNo = "1/" + Obj.Year;
                Obj.CurDNo = 1;
            }
            else
            {
                Number = Number + 1;
                Obj.CurDNo = Number;
                Obj.DiaryNo = Number + "/" + Obj.Year;
            }


            try
            {
                eFileAttachment PaperObj = context.eFileAttachments.Single(m => m.eFileAttachmentId == Obj.eFileAttachmentId);
                PaperObj.DiaryNo = Obj.DiaryNo;
                PaperObj.CurYear = CYear;
                PaperObj.CurDNo = Obj.CurDNo;
                context.SaveChanges();
                msg = Obj.DiaryNo;
            }

            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return Obj;
        }

        static List<mBranches> GBranchList(object param)
        {
            eFileContext context = new eFileContext();
            mBranches model = param as mBranches;
            List<mBranches> returnToCaller = new List<mBranches>();

            var BID = (from Val in context.users
                       join b in context.mStaff on Val.AadarId equals b.AadharID
                       where Val.UserId == model.UserId
                       select b.BranchIDs).FirstOrDefault();

            if (BID != null)
            {
                string[] StrId = BID.Split(',');
                for (int i = 0; i < StrId.Length; i++)
                {
                    // Convert String Id into int Id
                    int Id = Convert.ToInt16(StrId[i]);

                    //Get Branch List Name By Id
                    var List = (from B in context.mBranches
                                where B.BranchId == Id
                                select B).FirstOrDefault();
                    mBranches tempModel = new mBranches();
                    tempModel.BranchName = List.BranchName;
                    tempModel.BranchId = List.BranchId;
                    returnToCaller.Add(tempModel);
                }
            }

            return returnToCaller.ToList();
        }

        static object GetAllBranchList(object param)
        {
            eFileContext context = new eFileContext();
            var list = (from a in context.mBranches
                        select a).ToList();
            return list;
        }

        static List<mBranches> BranchsForAasign(object param)
        {
            eFileContext context = new eFileContext();
            mBranches model = param as mBranches;
            var list = (from Branch in context.mBranches select Branch).OrderBy(a => a.BranchOrder).ToList();
            return list.ToList();
        }

        static List<mStaff> GetEmployeeByBranch(object param)
        {
            eFileContext context = new eFileContext();
            mStaff obj = param as mStaff;
            var Emp = (from List in context.mStaff
                       where List.BranchIDs.Contains(obj.BranchIDs)
                       select List).ToList();
            return Emp.ToList();

        }


        public static object UpdateMovements(object param)
        {
            eFileContext context = new eFileContext();
            mBranches Obj = param as mBranches;
            tMovementFile MoveObj = new tMovementFile();
            try
            {
                var Assgnfrom = (from AF in context.mStaff where AF.AadharID == Obj.AssignfrmAadharId select AF).FirstOrDefault();
                var AssgnTo = (from AT in context.mStaff where AT.AadharID == Obj.AadharID select AT).FirstOrDefault();
                eFileAttachment PaperObj = context.eFileAttachments.Single(m => m.eFileAttachmentId == Obj.ID);
                PaperObj.CurrentBranchId = Obj.BranchId;
                PaperObj.CurrentEmpAadharID = Obj.AadharID;
                PaperObj.AssignDateTime = Obj.ModifiedDate;
                context.SaveChanges();
                MoveObj.eFileAttachmentId = Obj.ID;
                MoveObj.BranchId = Obj.BranchId;
                MoveObj.EmpAadharId = Obj.AadharID;
                MoveObj.Remarks = Obj.Remark;
                MoveObj.AssignDateTime = Obj.ModifiedDate;
                MoveObj.EmpName = AssgnTo.StaffName;
                MoveObj.EmpDesignation = AssgnTo.Designation;
                MoveObj.AssignfrmAadharId = Obj.AssignfrmAadharId;
                MoveObj.AssignfrmName = Assgnfrom.StaffName;
                MoveObj.AssignfrmDesignation = Assgnfrom.Designation;
                context.tMovementFile.Add(MoveObj);
                context.SaveChanges();
                context.Close();
            }

            catch (Exception ex)
            {

            }

            return Obj;
        }

        public static object GetReceivePaperDetailsByDeptIdHC(object param)
        {
            string[] str = param as string[];
            var V = "";
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            string Aadharid = str[2];
            int Year = Convert.ToInt32(str[4]);
            int PaperType = Convert.ToInt32(str[5]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }

            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    if ((Aadharid == "626192220638") || (Aadharid == "636142914885"))
                    {

                        if (Year == 0)
                        {
                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         from Ef in EFileTemp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         //join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()
                                         where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false
                                         //&& a.IsVerified == true
                                         select new
                                         {
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,

                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             FileName = Ef.eFileNumber,
                                             DiaryNo = a.DiaryNo,
                                             VerifiedBy = a.VerifiedBy,
                                             VerifiedDate = a.VerifiedDate,
                                             IsVerified = a.IsVerified,
                                             BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                             Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                             CountsAPS = a.CountsAPS,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             Annexurepath = a.AnnexPdfPath,
                                             CommitteeName = cmf.CommitteeName,
                                             commiteeId = a.CommitteeId,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks,
                                             IsDataRead = (from ef in ctx.ReadTableData
                                                           where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                           select ef.TablePrimaryId).FirstOrDefault(),
                                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                            List<eFileAttachment> lst = new List<eFileAttachment>();
                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.IsVerified = item.IsVerified;
                                mdl.VerifiedDate = item.VerifiedDate;
                                mdl.VerifiedBy = item.VerifiedBy;
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;

                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.MainEFileName = item.FileName;
                                mdl.DiaryNo = item.DiaryNo;
                                mdl.BranchName = item.BranchName;
                                mdl.EmpName = item.EmpName;
                                mdl.Desingnation = item.Desingnation;
                                mdl.AbbrevBranch = item.AbbrevBranch;
                                mdl.ItemName = item.ItemName;
                                mdl.CountsAPS = item.CountsAPS;
                                mdl.AnnexPdfPath = item.Annexurepath;
                                mdl.CommitteeId = item.commiteeId;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                                lst.Add(mdl);
                            }

                            return lst;
                        }
                        else
                        {
                            DateTime Dt = DateTime.Now.AddYears(-Year);
                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         from Ef in EFileTemp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()
                                         where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false
                                             //&& a.IsVerified == true
                                         && a.CreatedDate >= Dt && a.CreatedDate <= DateTime.Now
                                         select new
                                         {
                                             VerifiedBy = a.VerifiedBy,
                                             VerifiedDate = a.VerifiedDate,
                                             IsVerified = a.IsVerified,
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,
                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             FileName = Ef.eFileNumber,
                                             DiaryNo = a.DiaryNo,
                                             BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                             Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                             CountsAPS = a.CountsAPS,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             Annexurepath = a.AnnexPdfPath,
                                             CommitteeName = cmf.CommitteeName,
                                             commiteeId = a.CommitteeId,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks,
                                             IsDataRead = (from ef in ctx.ReadTableData
                                                           where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                           select ef.TablePrimaryId).FirstOrDefault(),
                                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                            List<eFileAttachment> lst = new List<eFileAttachment>();
                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.IsVerified = item.IsVerified;
                                mdl.VerifiedDate = item.VerifiedDate;
                                mdl.VerifiedBy = item.VerifiedBy;
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;

                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.MainEFileName = item.FileName;
                                mdl.DiaryNo = item.DiaryNo;
                                mdl.BranchName = item.BranchName;
                                mdl.EmpName = item.EmpName;
                                mdl.Desingnation = item.Desingnation;
                                mdl.AbbrevBranch = item.AbbrevBranch;
                                mdl.ItemName = item.ItemName;
                                mdl.CountsAPS = item.CountsAPS;
                                mdl.AnnexPdfPath = item.Annexurepath;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.CommitteeId = item.commiteeId;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                                lst.Add(mdl);
                            }

                            return lst;
                        }

                    }

                    else
                    {
                        // string currentaid = curr
                        if (str[3] != "" && str[0] == "HPD0001") //For Vidhan Sabha Department Users
                        {
                            int CBranchID = Convert.ToInt32(str[3]);
                            List<eFileAttachment> Mainlst = new List<eFileAttachment>();
                            List<eFileAttachment> lst = new List<eFileAttachment>();

                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         from Ef in EFileTemp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()
                                         // for read data
                                         //  join rd in ctx.ReadTableData  on a.eFileAttachmentId equals rd.TablePrimaryId  into rdtemp
                                         // from rds in rdtemp.DefaultIfEmpty()
                                         //  where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.CommitteeId == CBranchID && a.CurrentEmpAadharID == Aadharid
                                         where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.CommitteeId == CBranchID

                                         select new
                                         {
                                             VerifiedBy = a.VerifiedBy,
                                             VerifiedDate = a.VerifiedDate,
                                             IsVerified = a.IsVerified,
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,
                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             FileName = Ef.eFileNumber,
                                             DiaryNo = a.DiaryNo,
                                             Annexurepath = a.AnnexPdfPath,
                                             BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             CurrentAadharId = a.CurrentEmpAadharID,
                                             AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                             Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                             CountsAPS = a.CountsAPS,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             CommitteeName = cmf.CommitteeName,
                                             committeeid = a.CommitteeId,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks,
                                             //IsDataRead = (from ef in ctx.eFileAttachments
                                             //             join rd1 in ctx.ReadTableData on a.eFileAttachmentId equals rd1.TablePrimaryId into rdtemp1
                                             //             from rds1 in rdtemp1.DefaultIfEmpty()
                                             //             where ef.CurrentEmpAadharID == Aadharid select rds1.TablePrimaryId).FirstOrDefault(),
                                             IsDataRead = (from ef in ctx.ReadTableData
                                                           where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                           select ef.TablePrimaryId).FirstOrDefault(),
                                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.IsVerified = item.IsVerified;
                                mdl.VerifiedDate = item.VerifiedDate;
                                mdl.VerifiedBy = item.VerifiedBy;
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;

                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.MainEFileName = item.FileName;
                                mdl.DiaryNo = item.DiaryNo;
                                mdl.BranchName = item.BranchName;
                                mdl.EmpName = item.EmpName;
                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                mdl.Desingnation = item.Desingnation;
                                mdl.AbbrevBranch = item.AbbrevBranch;
                                mdl.AnnexPdfPath = item.Annexurepath;
                                mdl.ItemName = item.ItemName;
                                mdl.CountsAPS = item.CountsAPS;
                                mdl.CommitteeId = item.committeeid;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                                lst.Add(mdl);
                                Mainlst.Add(mdl);
                            }

                            return Mainlst;

                        }


                        else //For Others Deparments
                        {
                            List<eFileAttachment> Mainlst = new List<eFileAttachment>();
                            List<eFileAttachment> lst = new List<eFileAttachment>();
                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         from Ef in EFileTemp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()

                                         where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.IsBackLog != true
                                         select new
                                         {
                                             VerifiedBy = a.VerifiedBy,
                                             VerifiedDate = a.VerifiedDate,
                                             IsVerified = a.IsVerified,
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,
                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             FileName = Ef.eFileNumber,
                                             DiaryNo = a.DiaryNo,
                                             Annexurepath = a.AnnexPdfPath,
                                             BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             CurrentAadharId = a.CurrentEmpAadharID,
                                             TOCCType = a.TOCCtype,
                                             CountsAPS = a.CountsAPS,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             CommitteeName = cmf.CommitteeName,
                                             CommitteeId = a.CommitteeId,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks,
                                             IsDataRead = (from ef in ctx.ReadTableData
                                                           where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                           select ef.TablePrimaryId).FirstOrDefault(),
                                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.IsVerified = item.IsVerified;
                                mdl.VerifiedDate = item.VerifiedDate;
                                mdl.VerifiedBy = item.VerifiedBy;
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;

                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.MainEFileName = item.FileName;
                                mdl.DiaryNo = item.DiaryNo;
                                mdl.BranchName = item.BranchName;
                                mdl.EmpName = item.EmpName;
                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                mdl.TOCCtype = item.TOCCType;
                                mdl.AnnexPdfPath = item.Annexurepath;
                                mdl.ItemName = item.ItemName;
                                mdl.CountsAPS = item.CountsAPS;
                                mdl.CommitteeId = item.CommitteeId;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                                lst.Add(mdl);
                                Mainlst.Add(mdl);
                            }
                            return Mainlst;



                        }



                    }

                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                 //join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                 from cmf in comtemp.DefaultIfEmpty()
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
                                 select new
                                 {
                                     VerifiedBy = a.VerifiedBy,
                                     VerifiedDate = a.VerifiedDate,
                                     IsVerified = a.IsVerified,
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo,
                                     CountsAPS = a.CountsAPS,
                                     CommitteeName = cmf.CommitteeName,
                                     CommitteeId = a.CommitteeId,
                                     IsRejected = a.IsRejected,
                                     RejectedRemarks = a.RejectedRemarks,
                                     IsDataRead = (from ef in ctx.ReadTableData
                                                   where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                   select ef.TablePrimaryId).FirstOrDefault(),
                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.IsVerified = item.IsVerified;
                        mdl.VerifiedDate = item.VerifiedDate;
                        mdl.VerifiedBy = item.VerifiedBy;
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        mdl.ItemName = item.ItemName;
                        mdl.CountsAPS = item.CountsAPS;
                        mdl.CommitteeId = item.CommitteeId;
                        mdl.CommitteeName = item.CommitteeName;
                        mdl.IsRejected = item.IsRejected;
                        mdl.RejectedRemarks = item.RejectedRemarks;
                        mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }

        public static object GetReceivePaperDetailsByDeptIdHC_onclick(object param)
        {
            string[] str = param as string[];
            var V = "";
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            string Aadharid = str[2];
            int Year = Convert.ToInt32(str[4]);
            int PaperType = Convert.ToInt32(str[5]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }

            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    if ((Aadharid == "626192220638") || (Aadharid == "636142914885"))
                    {

                        // var querys = (from a in ctx.ReadTableData where a.UserAadharId == "626192220638" select a.TablePrimaryId).ToList();
                        List<string> readdata = new List<string>();
                        var qu = string.Join(",", ctx.ReadTableData.Where(p => p.UserAadharId == Aadharid).Select(p => p.TablePrimaryId.ToString()));
                        readdata = qu.Split(',').Distinct().ToList();
                        DateTime Dt = DateTime.Now.AddYears(-Year);
                        var query = (from a in ctx.eFileAttachments
                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                     from ps in temp.DefaultIfEmpty()
                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                     from Ef in EFileTemp.DefaultIfEmpty()
                                     // join rd1 in ctx.ReadTableData on a equals rd1.UserAadharId into EFileTemp
                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                     join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                     // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                     from cmf in comtemp.DefaultIfEmpty()
                                     //  join rd in ctx.ReadTableData on rd=>rd.TablePrimaryId !=  a.eFileAttachmentId 
                                     where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false
                                         //&& a.IsVerified == true
                                     && a.CreatedDate >= Dt && a.CreatedDate <= DateTime.Now
                                   && !(readdata.Contains(a.eFileAttachmentId.ToString()))
                                     // &&a.eFileAttachmentId Contains qu)
                                     // &&!(from rd in ctx.ReadTableData
                                     // select rd.UserAadharId).Contains("626192220638")

                                     select new
                                     {
                                         VerifiedBy = a.VerifiedBy,
                                         VerifiedDate = a.VerifiedDate,
                                         IsVerified = a.IsVerified,
                                         eFileAttachmentId = a.eFileAttachmentId,
                                         Department = ps.deptname ?? "OTHER",
                                         eFileName = a.eFileName,
                                         PaperNature = a.PaperNature,
                                         PaperNatureDays = a.PaperNatureDays ?? "0",
                                         eFilePath = a.eFilePath,
                                         Title = a.Title,
                                         Description = a.Description,
                                         CreateDate = a.CreatedDate,
                                         PaperStatus = a.PaperStatus,
                                         eFIleID = a.eFileID,
                                         PaperRefNo = a.PaperRefNo,
                                         DepartmentId = a.DepartmentId,
                                         OfficeCode = a.OfficeCode,
                                         ToDepartmentID = a.ToDepartmentID,
                                         ToOfficeCode = a.ToOfficeCode,
                                         ReplyStatus = a.ReplyStatus,
                                         ReplyDate = a.ReplyDate,
                                         ReceivedDate = a.ReceivedDate,
                                         DocumentTypeId = a.DocumentTypeId,

                                         ToPaperNature = a.ToPaperNature,
                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                         LinkedRefNo = a.LinkedRefNo,
                                         RevedOption = a.RevedOption,
                                         RecvDetails = a.RecvDetails,
                                         ToRecvDetails = a.ToRecvDetails,
                                         eMode = a.eMode,
                                         RecvdPaperNature = c.PaperNature,
                                         DocuemntPaperType = d.PaperType,
                                         PType = a.PType,
                                         SendStatus = a.SendStatus,
                                         DeptAbbr = ps.deptabbr,
                                         eFilePathWord = a.eFilePathWord,
                                         ReFileID = a.ReFileID,
                                         FileName = Ef.eFileNumber,
                                         DiaryNo = a.DiaryNo,
                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                         CountsAPS = a.CountsAPS,
                                         ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                         Annexurepath = a.AnnexPdfPath,
                                         CommitteeName = cmf.CommitteeName,
                                         commiteeId = a.CommitteeId,
                                         IsRejected = a.IsRejected,
                                         RejectedRemarks = a.RejectedRemarks,
                                         IsDataRead = (from ef in ctx.ReadTableData
                                                       where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                       select ef.TablePrimaryId).FirstOrDefault(),
                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                        List<eFileAttachment> lst = new List<eFileAttachment>();
                        foreach (var item in query)
                        {
                            eFileAttachment mdl = new eFileAttachment();
                            mdl.IsVerified = item.IsVerified;
                            mdl.VerifiedDate = item.VerifiedDate;
                            mdl.VerifiedBy = item.VerifiedBy;
                            mdl.DepartmentName = item.Department;
                            mdl.eFileAttachmentId = item.eFileAttachmentId;
                            mdl.eFileName = item.eFileName;
                            mdl.PaperNature = item.PaperNature;
                            mdl.PaperNatureDays = item.PaperNatureDays;
                            mdl.eFilePath = item.eFilePath;
                            mdl.Title = item.Title;
                            mdl.Description = item.Description;
                            mdl.CreatedDate = item.CreateDate;
                            mdl.PaperStatus = item.PaperStatus;
                            mdl.eFileID = item.eFIleID;
                            mdl.PaperRefNo = item.PaperRefNo;
                            mdl.DepartmentId = item.DepartmentId;
                            mdl.OfficeCode = item.OfficeCode;
                            mdl.ToDepartmentID = item.ToDepartmentID;
                            mdl.ToOfficeCode = item.ToOfficeCode;
                            mdl.ReplyStatus = item.ReplyStatus;
                            mdl.ReplyDate = item.ReplyDate;
                            mdl.ReceivedDate = item.ReceivedDate;
                            mdl.DocumentTypeId = item.DocumentTypeId;

                            mdl.ToPaperNature = item.ToPaperNature;
                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                            mdl.LinkedRefNo = item.LinkedRefNo;
                            mdl.RevedOption = item.RevedOption;
                            mdl.RecvDetails = item.RecvDetails;
                            mdl.ToRecvDetails = item.ToRecvDetails;
                            mdl.eMode = item.eMode;
                            mdl.RecvdPaperNature = item.RecvdPaperNature;

                            mdl.DocuemntPaperType = item.DocuemntPaperType;
                            mdl.PType = item.PType;
                            mdl.SendStatus = item.SendStatus;
                            mdl.DeptAbbr = item.DeptAbbr;
                            mdl.eFilePathWord = item.eFilePathWord;
                            mdl.ReFileID = item.ReFileID;
                            mdl.MainEFileName = item.FileName;
                            mdl.DiaryNo = item.DiaryNo;
                            mdl.BranchName = item.BranchName;
                            mdl.EmpName = item.EmpName;
                            mdl.Desingnation = item.Desingnation;
                            mdl.AbbrevBranch = item.AbbrevBranch;
                            mdl.ItemName = item.ItemName;
                            mdl.CountsAPS = item.CountsAPS;
                            mdl.AnnexPdfPath = item.Annexurepath;
                            mdl.CommitteeName = item.CommitteeName;
                            mdl.CommitteeId = item.commiteeId;
                            mdl.IsRejected = item.IsRejected;
                            mdl.RejectedRemarks = item.RejectedRemarks;
                            mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                            lst.Add(mdl);
                        }

                        return lst;
                    }

                    else
                    {
                        // string currentaid = curr
                        if (str[3] != "" && str[0] == "HPD0001") //For Vidhan Sabha Department Users
                        {

                            int CBranchID = Convert.ToInt32(str[3]);
                            List<eFileAttachment> Mainlst = new List<eFileAttachment>();
                            List<eFileAttachment> lst = new List<eFileAttachment>();
                            List<string> readdata = new List<string>();
                            var qu = string.Join(",", ctx.ReadTableData.Where(p => p.CommitteeId == CBranchID.ToString() && p.UserAadharId == Aadharid).Select(p => p.TablePrimaryId.ToString()));
                            readdata = qu.Split(',').Distinct().ToList();
                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         from Ef in EFileTemp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()
                                         // for read data
                                         //  join rd in ctx.ReadTableData  on a.eFileAttachmentId equals rd.TablePrimaryId  into rdtemp
                                         // from rds in rdtemp.DefaultIfEmpty()
                                         //  where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.CommitteeId == CBranchID && a.CurrentEmpAadharID == Aadharid
                                         where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.CommitteeId == CBranchID
                                         && !(readdata.Contains(a.eFileAttachmentId.ToString()))
                                         select new
                                         {
                                             VerifiedBy = a.VerifiedBy,
                                             VerifiedDate = a.VerifiedDate,
                                             IsVerified = a.IsVerified,
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,
                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             FileName = Ef.eFileNumber,
                                             DiaryNo = a.DiaryNo,
                                             Annexurepath = a.AnnexPdfPath,
                                             BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             CurrentAadharId = a.CurrentEmpAadharID,
                                             AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                             Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                             CountsAPS = a.CountsAPS,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             CommitteeName = cmf.CommitteeName,
                                             committeeid = a.CommitteeId,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks,
                                             //IsDataRead = (from ef in ctx.eFileAttachments
                                             //             join rd1 in ctx.ReadTableData on a.eFileAttachmentId equals rd1.TablePrimaryId into rdtemp1
                                             //             from rds1 in rdtemp1.DefaultIfEmpty()
                                             //             where ef.CurrentEmpAadharID == Aadharid select rds1.TablePrimaryId).FirstOrDefault(),
                                             IsDataRead = (from ef in ctx.ReadTableData
                                                           where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                           select ef.TablePrimaryId).FirstOrDefault(),
                                         }).ToList().OrderByDescending(a => a.CreateDate);

                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.IsVerified = item.IsVerified;
                                mdl.VerifiedDate = item.VerifiedDate;
                                mdl.VerifiedBy = item.VerifiedBy;
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;

                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.MainEFileName = item.FileName;
                                mdl.DiaryNo = item.DiaryNo;
                                mdl.BranchName = item.BranchName;
                                mdl.EmpName = item.EmpName;
                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                mdl.Desingnation = item.Desingnation;
                                mdl.AbbrevBranch = item.AbbrevBranch;
                                mdl.AnnexPdfPath = item.Annexurepath;
                                mdl.ItemName = item.ItemName;
                                mdl.CountsAPS = item.CountsAPS;
                                mdl.CommitteeId = item.committeeid;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                                lst.Add(mdl);
                                Mainlst.Add(mdl);
                            }

                            return Mainlst;

                        }


                        else //For Others Deparments
                        {
                            List<eFileAttachment> Mainlst = new List<eFileAttachment>();
                            List<eFileAttachment> lst = new List<eFileAttachment>();
                            List<string> readdata = new List<string>();
                            //  var qu = string.Join(",", ctx.ReadTableData.Where(p => p.CommitteeId == CBranchID.ToString()).Select(p => p.TablePrimaryId.ToString()));
                            var qu = string.Join(",", ctx.ReadTableData.Select(p => p.TablePrimaryId.ToString()));
                            readdata = qu.Split(',').Distinct().ToList();
                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         from Ef in EFileTemp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()

                                         where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.IsBackLog != true
                                        && !(readdata.Contains(a.eFileAttachmentId.ToString()))
                                         select new
                                         {
                                             VerifiedBy = a.VerifiedBy,
                                             VerifiedDate = a.VerifiedDate,
                                             IsVerified = a.IsVerified,
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,
                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             FileName = Ef.eFileNumber,
                                             DiaryNo = a.DiaryNo,
                                             Annexurepath = a.AnnexPdfPath,
                                             BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             CurrentAadharId = a.CurrentEmpAadharID,
                                             TOCCType = a.TOCCtype,
                                             CountsAPS = a.CountsAPS,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             CommitteeName = cmf.CommitteeName,
                                             CommitteeId = a.CommitteeId,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks,
                                             IsDataRead = (from ef in ctx.ReadTableData
                                                           where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                           select ef.TablePrimaryId).FirstOrDefault(),
                                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.IsVerified = item.IsVerified;
                                mdl.VerifiedDate = item.VerifiedDate;
                                mdl.VerifiedBy = item.VerifiedBy;
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;

                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.MainEFileName = item.FileName;
                                mdl.DiaryNo = item.DiaryNo;
                                mdl.BranchName = item.BranchName;
                                mdl.EmpName = item.EmpName;
                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                mdl.TOCCtype = item.TOCCType;
                                mdl.AnnexPdfPath = item.Annexurepath;
                                mdl.ItemName = item.ItemName;
                                mdl.CountsAPS = item.CountsAPS;
                                mdl.CommitteeId = item.CommitteeId;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                                lst.Add(mdl);
                                Mainlst.Add(mdl);
                            }
                            return Mainlst;



                        }



                    }

                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    List<string> readdata = new List<string>();
                    // var qu = string.Join(",", ctx.ReadTableData.Where(p => p.CommitteeId == CBranchID.ToString()).Select(p => p.TablePrimaryId.ToString()));
                    var qu = string.Join(",", ctx.ReadTableData.Select(p => p.TablePrimaryId.ToString()));
                    readdata = qu.Split(',').Distinct().ToList();
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                 //join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
                                 from cmf in comtemp.DefaultIfEmpty()
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
                                    && !(readdata.Contains(a.eFileAttachmentId.ToString()))
                                 select new
                                 {
                                     VerifiedBy = a.VerifiedBy,
                                     VerifiedDate = a.VerifiedDate,
                                     IsVerified = a.IsVerified,
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo,
                                     CountsAPS = a.CountsAPS,
                                     CommitteeName = cmf.CommitteeName,
                                     CommitteeId = a.CommitteeId,
                                     IsRejected = a.IsRejected,
                                     RejectedRemarks = a.RejectedRemarks,
                                     IsDataRead = (from ef in ctx.ReadTableData
                                                   where ef.TablePrimaryId == a.eFileAttachmentId && ef.UserAadharId == Aadharid
                                                   select ef.TablePrimaryId).FirstOrDefault(),
                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.IsVerified = item.IsVerified;
                        mdl.VerifiedDate = item.VerifiedDate;
                        mdl.VerifiedBy = item.VerifiedBy;
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        mdl.ItemName = item.ItemName;
                        mdl.CountsAPS = item.CountsAPS;
                        mdl.CommitteeId = item.CommitteeId;
                        mdl.CommitteeName = item.CommitteeName;
                        mdl.IsRejected = item.IsRejected;
                        mdl.RejectedRemarks = item.RejectedRemarks;
                        mdl.IsDataRead = Convert.ToString(item.IsDataRead);
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }

        //public static object GetReceivePaperDetailsByDeptIdHC(object param)
        //{
        //    string[] str = param as string[];
        //    var V = "";
        //    string DeptId = Convert.ToString(str[0]);
        //    int Officecode = 0;
        //    int.TryParse(str[1], out Officecode);
        //    string Aadharid = str[2];
        //    int Year = Convert.ToInt32(str[4]);
        //    int PaperType = Convert.ToInt32(str[5]);
        //    List<string> deptlist = new List<string>();
        //    if (!string.IsNullOrEmpty(DeptId))
        //    {
        //        deptlist = DeptId.Split(',').Distinct().ToList();
        //    }

        //    if (Officecode == 0)
        //    {
        //        using (eFileContext ctx = new eFileContext())
        //        {
        //            if (Aadharid == "626192220638")
        //            {

        //                if (Year == 0)
        //                {
        //                    var query = (from a in ctx.eFileAttachments
        //                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                 from Ef in EFileTemp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                 join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
        //                                 //join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
        //                                 from cmf in comtemp.DefaultIfEmpty()
        //                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false
        //                                 //&& a.IsVerified == true
        //                                 select new
        //                                 {
        //                                     eFileAttachmentId = a.eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName = a.eFileName,
        //                                     PaperNature = a.PaperNature,
        //                                     PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                     eFilePath = a.eFilePath,
        //                                     Title = a.Title,
        //                                     Description = a.Description,
        //                                     CreateDate = a.CreatedDate,
        //                                     PaperStatus = a.PaperStatus,
        //                                     eFIleID = a.eFileID,
        //                                     PaperRefNo = a.PaperRefNo,
        //                                     DepartmentId = a.DepartmentId,
        //                                     OfficeCode = a.OfficeCode,
        //                                     ToDepartmentID = a.ToDepartmentID,
        //                                     ToOfficeCode = a.ToOfficeCode,
        //                                     ReplyStatus = a.ReplyStatus,
        //                                     ReplyDate = a.ReplyDate,
        //                                     ReceivedDate = a.ReceivedDate,
        //                                     DocumentTypeId = a.DocumentTypeId,

        //                                     ToPaperNature = a.ToPaperNature,
        //                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = a.LinkedRefNo,
        //                                     RevedOption = a.RevedOption,
        //                                     RecvDetails = a.RecvDetails,
        //                                     ToRecvDetails = a.ToRecvDetails,
        //                                     eMode = a.eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = a.PType,
        //                                     SendStatus = a.SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = a.eFilePathWord,
        //                                     ReFileID = a.ReFileID,
        //                                     FileName = Ef.eFileNumber,
        //                                     DiaryNo = a.DiaryNo,
        //                                     VerifiedBy = a.VerifiedBy,
        //                                     VerifiedDate = a.VerifiedDate,
        //                                     IsVerified = a.IsVerified,
        //                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                     CountsAPS = a.CountsAPS,
        //                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                     Annexurepath = a.AnnexPdfPath,
        //                                     CommitteeName = cmf.CommitteeName,
        //                                     commiteeId = a.CommitteeId,
        //                                     IsRejected = a.IsRejected,
        //                                     RejectedRemarks = a.RejectedRemarks
        //                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    foreach (var item in query)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.IsVerified = item.IsVerified;
        //                        mdl.VerifiedDate = item.VerifiedDate;
        //                        mdl.VerifiedBy = item.VerifiedBy;
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.Desingnation = item.Desingnation;
        //                        mdl.AbbrevBranch = item.AbbrevBranch;
        //                        mdl.ItemName = item.ItemName;
        //                        mdl.CountsAPS = item.CountsAPS;
        //                        mdl.AnnexPdfPath = item.Annexurepath;
        //                        mdl.CommitteeId = item.commiteeId;
        //                        mdl.CommitteeName = item.CommitteeName;
        //                        mdl.IsRejected = item.IsRejected;
        //                        mdl.RejectedRemarks = item.RejectedRemarks;
        //                        lst.Add(mdl);
        //                    }

        //                    return lst;
        //                }
        //                else
        //                {
        //                    DateTime Dt = DateTime.Now.AddYears(-Year);
        //                    var query = (from a in ctx.eFileAttachments
        //                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                 from Ef in EFileTemp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                 join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
        //                                 // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
        //                                 from cmf in comtemp.DefaultIfEmpty()
        //                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false
        //                                     //&& a.IsVerified == true
        //                                 && a.CreatedDate >= Dt && a.CreatedDate <= DateTime.Now
        //                                 select new
        //                                 {
        //                                     VerifiedBy = a.VerifiedBy,
        //                                     VerifiedDate = a.VerifiedDate,
        //                                     IsVerified = a.IsVerified,
        //                                     eFileAttachmentId = a.eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName = a.eFileName,
        //                                     PaperNature = a.PaperNature,
        //                                     PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                     eFilePath = a.eFilePath,
        //                                     Title = a.Title,
        //                                     Description = a.Description,
        //                                     CreateDate = a.CreatedDate,
        //                                     PaperStatus = a.PaperStatus,
        //                                     eFIleID = a.eFileID,
        //                                     PaperRefNo = a.PaperRefNo,
        //                                     DepartmentId = a.DepartmentId,
        //                                     OfficeCode = a.OfficeCode,
        //                                     ToDepartmentID = a.ToDepartmentID,
        //                                     ToOfficeCode = a.ToOfficeCode,
        //                                     ReplyStatus = a.ReplyStatus,
        //                                     ReplyDate = a.ReplyDate,
        //                                     ReceivedDate = a.ReceivedDate,
        //                                     DocumentTypeId = a.DocumentTypeId,

        //                                     ToPaperNature = a.ToPaperNature,
        //                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = a.LinkedRefNo,
        //                                     RevedOption = a.RevedOption,
        //                                     RecvDetails = a.RecvDetails,
        //                                     ToRecvDetails = a.ToRecvDetails,
        //                                     eMode = a.eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = a.PType,
        //                                     SendStatus = a.SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = a.eFilePathWord,
        //                                     ReFileID = a.ReFileID,
        //                                     FileName = Ef.eFileNumber,
        //                                     DiaryNo = a.DiaryNo,
        //                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                     CountsAPS = a.CountsAPS,
        //                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                     Annexurepath = a.AnnexPdfPath,
        //                                     CommitteeName = cmf.CommitteeName,
        //                                     commiteeId = a.CommitteeId,
        //                                     IsRejected = a.IsRejected,
        //                                     RejectedRemarks = a.RejectedRemarks
        //                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    foreach (var item in query)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.IsVerified = item.IsVerified;
        //                        mdl.VerifiedDate = item.VerifiedDate;
        //                        mdl.VerifiedBy = item.VerifiedBy;
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.Desingnation = item.Desingnation;
        //                        mdl.AbbrevBranch = item.AbbrevBranch;
        //                        mdl.ItemName = item.ItemName;
        //                        mdl.CountsAPS = item.CountsAPS;
        //                        mdl.AnnexPdfPath = item.Annexurepath;
        //                        mdl.CommitteeName = item.CommitteeName;
        //                        mdl.CommitteeId = item.commiteeId;
        //                        mdl.IsRejected = item.IsRejected;
        //                        mdl.RejectedRemarks = item.RejectedRemarks;
        //                        lst.Add(mdl);
        //                    }

        //                    return lst;
        //                }

        //            }

        //            else
        //            {
        //                if (str[3] != "") //For Vidhan Sabha Department Users
        //                {
        //                    int CBranchID = Convert.ToInt32(str[3]);
        //                    List<eFileAttachment> Mainlst = new List<eFileAttachment>();
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    var s = string.Join(",", ctx.tMovementFile.Where(p => p.EmpAadharId == Aadharid)
        //                            .Select(p => p.eFileAttachmentId.ToString()));
        //                    if (s != "" && PaperType == 2)
        //                    {

        //                        List<int> TagIds1 = s.Split(',').Select(int.Parse).ToList();
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID && !TagIds1.Contains(p.eFileAttachmentId))
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        List<int> TagIds = s.Split(',').Select(int.Parse).ToList();
        //                        var query = (from a in ctx.eFileAttachments
        //                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                     from ps in temp.DefaultIfEmpty()
        //                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                     from Ef in EFileTemp.DefaultIfEmpty()
        //                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                     join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
        //                                     // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
        //                                     from cmf in comtemp.DefaultIfEmpty()
        //                                     where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && TagIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                     select new
        //                                     {
        //                                         VerifiedBy = a.VerifiedBy,
        //                                         VerifiedDate = a.VerifiedDate,
        //                                         IsVerified = a.IsVerified,
        //                                         eFileAttachmentId = a.eFileAttachmentId,
        //                                         Department = ps.deptname ?? "OTHER",
        //                                         eFileName = a.eFileName,
        //                                         PaperNature = a.PaperNature,
        //                                         PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                         eFilePath = a.eFilePath,
        //                                         Title = a.Title,
        //                                         Description = a.Description,
        //                                         CreateDate = a.CreatedDate,
        //                                         PaperStatus = a.PaperStatus,
        //                                         eFIleID = a.eFileID,
        //                                         PaperRefNo = a.PaperRefNo,
        //                                         DepartmentId = a.DepartmentId,
        //                                         OfficeCode = a.OfficeCode,
        //                                         ToDepartmentID = a.ToDepartmentID,
        //                                         ToOfficeCode = a.ToOfficeCode,
        //                                         ReplyStatus = a.ReplyStatus,
        //                                         ReplyDate = a.ReplyDate,
        //                                         ReceivedDate = a.ReceivedDate,
        //                                         DocumentTypeId = a.DocumentTypeId,
        //                                         ToPaperNature = a.ToPaperNature,
        //                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                         LinkedRefNo = a.LinkedRefNo,
        //                                         RevedOption = a.RevedOption,
        //                                         RecvDetails = a.RecvDetails,
        //                                         ToRecvDetails = a.ToRecvDetails,
        //                                         eMode = a.eMode,
        //                                         RecvdPaperNature = c.PaperNature,
        //                                         DocuemntPaperType = d.PaperType,
        //                                         PType = a.PType,
        //                                         SendStatus = a.SendStatus,
        //                                         DeptAbbr = ps.deptabbr,
        //                                         eFilePathWord = a.eFilePathWord,
        //                                         ReFileID = a.ReFileID,
        //                                         FileName = Ef.eFileNumber,
        //                                         DiaryNo = a.DiaryNo,
        //                                         Annexurepath = a.AnnexPdfPath,
        //                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                         CurrentAadharId = a.CurrentEmpAadharID,
        //                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                         CountsAPS = a.CountsAPS,
        //                                         ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                         CommitteeName = cmf.CommitteeName,
        //                                         committeeid = a.CommitteeId,
        //                                         IsRejected = a.IsRejected,
        //                                         RejectedRemarks = a.RejectedRemarks
        //                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                        foreach (var item in query)
        //                        {
        //                            eFileAttachment mdl = new eFileAttachment();
        //                            mdl.IsVerified = item.IsVerified;
        //                            mdl.VerifiedDate = item.VerifiedDate;
        //                            mdl.VerifiedBy = item.VerifiedBy;
        //                            mdl.DepartmentName = item.Department;
        //                            mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                            mdl.eFileName = item.eFileName;
        //                            mdl.PaperNature = item.PaperNature;
        //                            mdl.PaperNatureDays = item.PaperNatureDays;
        //                            mdl.eFilePath = item.eFilePath;
        //                            mdl.Title = item.Title;
        //                            mdl.Description = item.Description;
        //                            mdl.CreatedDate = item.CreateDate;
        //                            mdl.PaperStatus = item.PaperStatus;
        //                            mdl.eFileID = item.eFIleID;
        //                            mdl.PaperRefNo = item.PaperRefNo;
        //                            mdl.DepartmentId = item.DepartmentId;
        //                            mdl.OfficeCode = item.OfficeCode;
        //                            mdl.ToDepartmentID = item.ToDepartmentID;
        //                            mdl.ToOfficeCode = item.ToOfficeCode;
        //                            mdl.ReplyStatus = item.ReplyStatus;
        //                            mdl.ReplyDate = item.ReplyDate;
        //                            mdl.ReceivedDate = item.ReceivedDate;
        //                            mdl.DocumentTypeId = item.DocumentTypeId;

        //                            mdl.ToPaperNature = item.ToPaperNature;
        //                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                            mdl.LinkedRefNo = item.LinkedRefNo;
        //                            mdl.RevedOption = item.RevedOption;
        //                            mdl.RecvDetails = item.RecvDetails;
        //                            mdl.ToRecvDetails = item.ToRecvDetails;
        //                            mdl.eMode = item.eMode;
        //                            mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                            mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                            mdl.PType = item.PType;
        //                            mdl.SendStatus = item.SendStatus;
        //                            mdl.DeptAbbr = item.DeptAbbr;
        //                            mdl.eFilePathWord = item.eFilePathWord;
        //                            mdl.ReFileID = item.ReFileID;
        //                            mdl.MainEFileName = item.FileName;
        //                            mdl.DiaryNo = item.DiaryNo;
        //                            mdl.BranchName = item.BranchName;
        //                            mdl.EmpName = item.EmpName;
        //                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                            mdl.Desingnation = item.Desingnation;
        //                            mdl.AbbrevBranch = item.AbbrevBranch;
        //                            mdl.AnnexPdfPath = item.Annexurepath;
        //                            mdl.ItemName = item.ItemName;
        //                            mdl.CountsAPS = item.CountsAPS;
        //                            mdl.CommitteeId = item.committeeid;
        //                            mdl.CommitteeName = item.CommitteeName;
        //                            mdl.IsRejected = item.IsRejected;
        //                            mdl.RejectedRemarks = item.RejectedRemarks;
        //                            lst.Add(mdl);
        //                            Mainlst.Add(mdl);
        //                        }
        //                        if (V != "")
        //                        {
        //                            List<int> NullIds = V.Split(',').Select(int.Parse).ToList();
        //                            var query1 = (from a in ctx.eFileAttachments
        //                                          join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                          from ps in temp.DefaultIfEmpty()
        //                                          join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                          from Ef in EFileTemp.DefaultIfEmpty()
        //                                          join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                          join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                          join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
        //                                          //join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
        //                                          from cmf in comtemp.DefaultIfEmpty()

        //                                          where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && NullIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                          select new
        //                                          {
        //                                              VerifiedBy = a.VerifiedBy,
        //                                              VerifiedDate = a.VerifiedDate,
        //                                              IsVerified = a.IsVerified,
        //                                              eFileAttachmentId = a.eFileAttachmentId,
        //                                              Department = ps.deptname ?? "OTHER",
        //                                              eFileName = a.eFileName,
        //                                              PaperNature = a.PaperNature,
        //                                              PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                              eFilePath = a.eFilePath,
        //                                              Title = a.Title,
        //                                              Description = a.Description,
        //                                              CreateDate = a.CreatedDate,
        //                                              PaperStatus = a.PaperStatus,
        //                                              eFIleID = a.eFileID,
        //                                              PaperRefNo = a.PaperRefNo,
        //                                              DepartmentId = a.DepartmentId,
        //                                              OfficeCode = a.OfficeCode,
        //                                              ToDepartmentID = a.ToDepartmentID,
        //                                              ToOfficeCode = a.ToOfficeCode,
        //                                              ReplyStatus = a.ReplyStatus,
        //                                              ReplyDate = a.ReplyDate,
        //                                              ReceivedDate = a.ReceivedDate,
        //                                              DocumentTypeId = a.DocumentTypeId,
        //                                              ToPaperNature = a.ToPaperNature,
        //                                              ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                              LinkedRefNo = a.LinkedRefNo,
        //                                              RevedOption = a.RevedOption,
        //                                              RecvDetails = a.RecvDetails,
        //                                              ToRecvDetails = a.ToRecvDetails,
        //                                              eMode = a.eMode,
        //                                              RecvdPaperNature = c.PaperNature,
        //                                              DocuemntPaperType = d.PaperType,
        //                                              PType = a.PType,
        //                                              SendStatus = a.SendStatus,
        //                                              DeptAbbr = ps.deptabbr,
        //                                              eFilePathWord = a.eFilePathWord,
        //                                              ReFileID = a.ReFileID,
        //                                              FileName = Ef.eFileNumber,
        //                                              DiaryNo = a.DiaryNo,
        //                                              Annexurepath = a.AnnexPdfPath,
        //                                              BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                              EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                              CurrentAadharId = a.CurrentEmpAadharID,
        //                                              AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                              Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                              CountsAPS = a.CountsAPS,
        //                                              ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                              CommitteeName = cmf.CommitteeName,
        //                                              CommitteeId = a.CommitteeId,
        //                                              IsRejected = a.IsRejected,
        //                                              RejectedRemarks = a.RejectedRemarks
        //                                          }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                            foreach (var item in query1)
        //                            {
        //                                eFileAttachment mdl = new eFileAttachment();
        //                                mdl.IsVerified = item.IsVerified;
        //                                mdl.VerifiedDate = item.VerifiedDate;
        //                                mdl.VerifiedBy = item.VerifiedBy;
        //                                mdl.DepartmentName = item.Department;
        //                                mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                                mdl.eFileName = item.eFileName;
        //                                mdl.PaperNature = item.PaperNature;
        //                                mdl.PaperNatureDays = item.PaperNatureDays;
        //                                mdl.eFilePath = null;
        //                                mdl.Title = item.Title;
        //                                mdl.Description = item.Description;
        //                                mdl.CreatedDate = item.CreateDate;
        //                                mdl.PaperStatus = item.PaperStatus;
        //                                mdl.eFileID = item.eFIleID;
        //                                mdl.PaperRefNo = item.PaperRefNo;
        //                                mdl.DepartmentId = item.DepartmentId;
        //                                mdl.OfficeCode = item.OfficeCode;
        //                                mdl.ToDepartmentID = item.ToDepartmentID;
        //                                mdl.ToOfficeCode = item.ToOfficeCode;
        //                                mdl.ReplyStatus = item.ReplyStatus;
        //                                mdl.ReplyDate = item.ReplyDate;
        //                                mdl.ReceivedDate = item.ReceivedDate;
        //                                mdl.DocumentTypeId = item.DocumentTypeId;

        //                                mdl.ToPaperNature = item.ToPaperNature;
        //                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                                mdl.LinkedRefNo = item.LinkedRefNo;
        //                                mdl.RevedOption = item.RevedOption;
        //                                mdl.RecvDetails = item.RecvDetails;
        //                                mdl.ToRecvDetails = item.ToRecvDetails;
        //                                mdl.eMode = item.eMode;
        //                                mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                                mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                                mdl.PType = item.PType;
        //                                mdl.SendStatus = item.SendStatus;
        //                                mdl.DeptAbbr = item.DeptAbbr;
        //                                mdl.eFilePathWord = null;
        //                                mdl.ReFileID = item.ReFileID;
        //                                mdl.MainEFileName = item.FileName;
        //                                mdl.DiaryNo = item.DiaryNo;
        //                                mdl.BranchName = item.BranchName;
        //                                mdl.EmpName = item.EmpName;
        //                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                                mdl.Desingnation = item.Desingnation;
        //                                mdl.AbbrevBranch = item.AbbrevBranch;
        //                                mdl.AnnexPdfPath = item.Annexurepath;
        //                                mdl.ItemName = item.ItemName;
        //                                mdl.CountsAPS = item.CountsAPS;
        //                                mdl.CommitteeId = item.CommitteeId;
        //                                mdl.CommitteeName = item.CommitteeName;
        //                                mdl.IsRejected = item.IsRejected;
        //                                mdl.RejectedRemarks = item.RejectedRemarks;
        //                                Mainlst.Add(mdl);
        //                            }
        //                        }

        //                        return Mainlst;

        //                    }
        //                    else if (s != "" && PaperType == 1)
        //                    {
        //                        List<int> TagIds1 = s.Split(',').Select(int.Parse).ToList();
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID && !TagIds1.Contains(p.eFileAttachmentId))
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        List<int> TagIds = s.Split(',').Select(int.Parse).ToList();
        //                        var query = (from a in ctx.eFileAttachments
        //                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                     from ps in temp.DefaultIfEmpty()
        //                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                     from Ef in EFileTemp.DefaultIfEmpty()
        //                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                     //join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
        //                                     join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
        //                                     from cmf in comtemp.DefaultIfEmpty()
        //                                     where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && TagIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                     select new
        //                                     {
        //                                         VerifiedBy = a.VerifiedBy,
        //                                         VerifiedDate = a.VerifiedDate,
        //                                         IsVerified = a.IsVerified,
        //                                         eFileAttachmentId = a.eFileAttachmentId,
        //                                         Department = ps.deptname ?? "OTHER",
        //                                         eFileName = a.eFileName,
        //                                         PaperNature = a.PaperNature,
        //                                         PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                         eFilePath = a.eFilePath,
        //                                         Title = a.Title,
        //                                         Description = a.Description,
        //                                         CreateDate = a.CreatedDate,
        //                                         PaperStatus = a.PaperStatus,
        //                                         eFIleID = a.eFileID,
        //                                         PaperRefNo = a.PaperRefNo,
        //                                         DepartmentId = a.DepartmentId,
        //                                         OfficeCode = a.OfficeCode,
        //                                         ToDepartmentID = a.ToDepartmentID,
        //                                         ToOfficeCode = a.ToOfficeCode,
        //                                         ReplyStatus = a.ReplyStatus,
        //                                         ReplyDate = a.ReplyDate,
        //                                         ReceivedDate = a.ReceivedDate,
        //                                         DocumentTypeId = a.DocumentTypeId,
        //                                         ToPaperNature = a.ToPaperNature,
        //                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                         LinkedRefNo = a.LinkedRefNo,
        //                                         RevedOption = a.RevedOption,
        //                                         RecvDetails = a.RecvDetails,
        //                                         ToRecvDetails = a.ToRecvDetails,
        //                                         eMode = a.eMode,
        //                                         RecvdPaperNature = c.PaperNature,
        //                                         DocuemntPaperType = d.PaperType,
        //                                         PType = a.PType,
        //                                         SendStatus = a.SendStatus,
        //                                         DeptAbbr = ps.deptabbr,
        //                                         eFilePathWord = a.eFilePathWord,
        //                                         ReFileID = a.ReFileID,
        //                                         FileName = Ef.eFileNumber,
        //                                         DiaryNo = a.DiaryNo,
        //                                         Annexurepath = a.AnnexPdfPath,
        //                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                         CurrentAadharId = a.CurrentEmpAadharID,
        //                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                         CountsAPS = a.CountsAPS,
        //                                         ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                         CommitteeName = cmf.CommitteeName,
        //                                         CommitteeId = a.CommitteeId,
        //                                         IsRejected = a.IsRejected,
        //                                         RejectedRemarks = a.RejectedRemarks

        //                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                        foreach (var item in query)
        //                        {
        //                            eFileAttachment mdl = new eFileAttachment();
        //                            mdl.IsVerified = item.IsVerified;
        //                            mdl.VerifiedDate = item.VerifiedDate;
        //                            mdl.VerifiedBy = item.VerifiedBy;
        //                            mdl.DepartmentName = item.Department;
        //                            mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                            mdl.eFileName = item.eFileName;
        //                            mdl.PaperNature = item.PaperNature;
        //                            mdl.PaperNatureDays = item.PaperNatureDays;
        //                            mdl.eFilePath = item.eFilePath;
        //                            mdl.Title = item.Title;
        //                            mdl.Description = item.Description;
        //                            mdl.CreatedDate = item.CreateDate;
        //                            mdl.PaperStatus = item.PaperStatus;
        //                            mdl.eFileID = item.eFIleID;
        //                            mdl.PaperRefNo = item.PaperRefNo;
        //                            mdl.DepartmentId = item.DepartmentId;
        //                            mdl.OfficeCode = item.OfficeCode;
        //                            mdl.ToDepartmentID = item.ToDepartmentID;
        //                            mdl.ToOfficeCode = item.ToOfficeCode;
        //                            mdl.ReplyStatus = item.ReplyStatus;
        //                            mdl.ReplyDate = item.ReplyDate;
        //                            mdl.ReceivedDate = item.ReceivedDate;
        //                            mdl.DocumentTypeId = item.DocumentTypeId;

        //                            mdl.ToPaperNature = item.ToPaperNature;
        //                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                            mdl.LinkedRefNo = item.LinkedRefNo;
        //                            mdl.RevedOption = item.RevedOption;
        //                            mdl.RecvDetails = item.RecvDetails;
        //                            mdl.ToRecvDetails = item.ToRecvDetails;
        //                            mdl.eMode = item.eMode;
        //                            mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                            mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                            mdl.PType = item.PType;
        //                            mdl.SendStatus = item.SendStatus;
        //                            mdl.DeptAbbr = item.DeptAbbr;
        //                            mdl.eFilePathWord = item.eFilePathWord;
        //                            mdl.ReFileID = item.ReFileID;
        //                            mdl.MainEFileName = item.FileName;
        //                            mdl.DiaryNo = item.DiaryNo;
        //                            mdl.BranchName = item.BranchName;
        //                            mdl.EmpName = item.EmpName;
        //                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                            mdl.Desingnation = item.Desingnation;
        //                            mdl.AbbrevBranch = item.AbbrevBranch;
        //                            mdl.AnnexPdfPath = item.Annexurepath;
        //                            mdl.ItemName = item.ItemName;
        //                            mdl.CountsAPS = item.CountsAPS;
        //                            mdl.CommitteeId = item.CommitteeId;
        //                            mdl.CommitteeName = item.BranchName;
        //                            mdl.IsRejected = item.IsRejected;
        //                            mdl.RejectedRemarks = item.RejectedRemarks;
        //                            lst.Add(mdl);
        //                            Mainlst.Add(mdl);
        //                        }
        //                        return lst;
        //                    }
        //                    else
        //                    {
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID)
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        if (V != "")
        //                        {
        //                            List<int> NullIds = V.Split(',').Select(int.Parse).ToList();
        //                            var query1 = (from a in ctx.eFileAttachments
        //                                          join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                          from ps in temp.DefaultIfEmpty()
        //                                          join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                          from Ef in EFileTemp.DefaultIfEmpty()
        //                                          join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                          join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                          join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
        //                                          // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
        //                                          from cmf in comtemp.DefaultIfEmpty()
        //                                          where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && NullIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                          select new
        //                                          {
        //                                              VerifiedBy = a.VerifiedBy,
        //                                              VerifiedDate = a.VerifiedDate,
        //                                              IsVerified = a.IsVerified,
        //                                              eFileAttachmentId = a.eFileAttachmentId,
        //                                              Department = ps.deptname ?? "OTHER",
        //                                              eFileName = a.eFileName,
        //                                              PaperNature = a.PaperNature,
        //                                              PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                              eFilePath = a.eFilePath,
        //                                              Title = a.Title,
        //                                              Description = a.Description,
        //                                              CreateDate = a.CreatedDate,
        //                                              PaperStatus = a.PaperStatus,
        //                                              eFIleID = a.eFileID,
        //                                              PaperRefNo = a.PaperRefNo,
        //                                              DepartmentId = a.DepartmentId,
        //                                              OfficeCode = a.OfficeCode,
        //                                              ToDepartmentID = a.ToDepartmentID,
        //                                              ToOfficeCode = a.ToOfficeCode,
        //                                              ReplyStatus = a.ReplyStatus,
        //                                              ReplyDate = a.ReplyDate,
        //                                              ReceivedDate = a.ReceivedDate,
        //                                              DocumentTypeId = a.DocumentTypeId,
        //                                              ToPaperNature = a.ToPaperNature,
        //                                              ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                              LinkedRefNo = a.LinkedRefNo,
        //                                              RevedOption = a.RevedOption,
        //                                              RecvDetails = a.RecvDetails,
        //                                              ToRecvDetails = a.ToRecvDetails,
        //                                              eMode = a.eMode,
        //                                              RecvdPaperNature = c.PaperNature,
        //                                              DocuemntPaperType = d.PaperType,
        //                                              PType = a.PType,
        //                                              SendStatus = a.SendStatus,
        //                                              DeptAbbr = ps.deptabbr,
        //                                              eFilePathWord = a.eFilePathWord,
        //                                              ReFileID = a.ReFileID,
        //                                              FileName = Ef.eFileNumber,
        //                                              DiaryNo = a.DiaryNo,
        //                                              Annexurepath = a.AnnexPdfPath,
        //                                              BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                              EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                              CurrentAadharId = a.CurrentEmpAadharID,
        //                                              AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                              Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                              CountsAPS = a.CountsAPS,
        //                                              ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                              CommitteeName = cmf.CommitteeName,
        //                                              CommitteeId = a.CommitteeId,
        //                                              IsRejected = a.IsRejected,
        //                                              RejectedRemarks = a.RejectedRemarks

        //                                          }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                            foreach (var item in query1)
        //                            {
        //                                eFileAttachment mdl = new eFileAttachment();
        //                                mdl.IsVerified = item.IsVerified;
        //                                mdl.VerifiedDate = item.VerifiedDate;
        //                                mdl.VerifiedBy = item.VerifiedBy;
        //                                mdl.DepartmentName = item.Department;
        //                                mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                                mdl.eFileName = item.eFileName;
        //                                mdl.PaperNature = item.PaperNature;
        //                                mdl.PaperNatureDays = item.PaperNatureDays;
        //                                mdl.eFilePath = null;
        //                                mdl.Title = item.Title;
        //                                mdl.Description = item.Description;
        //                                mdl.CreatedDate = item.CreateDate;
        //                                mdl.PaperStatus = item.PaperStatus;
        //                                mdl.eFileID = item.eFIleID;
        //                                mdl.PaperRefNo = item.PaperRefNo;
        //                                mdl.DepartmentId = item.DepartmentId;
        //                                mdl.OfficeCode = item.OfficeCode;
        //                                mdl.ToDepartmentID = item.ToDepartmentID;
        //                                mdl.ToOfficeCode = item.ToOfficeCode;
        //                                mdl.ReplyStatus = item.ReplyStatus;
        //                                mdl.ReplyDate = item.ReplyDate;
        //                                mdl.ReceivedDate = item.ReceivedDate;
        //                                mdl.DocumentTypeId = item.DocumentTypeId;

        //                                mdl.ToPaperNature = item.ToPaperNature;
        //                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                                mdl.LinkedRefNo = item.LinkedRefNo;
        //                                mdl.RevedOption = item.RevedOption;
        //                                mdl.RecvDetails = item.RecvDetails;
        //                                mdl.ToRecvDetails = item.ToRecvDetails;
        //                                mdl.eMode = item.eMode;
        //                                mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                                mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                                mdl.PType = item.PType;
        //                                mdl.SendStatus = item.SendStatus;
        //                                mdl.DeptAbbr = item.DeptAbbr;
        //                                mdl.eFilePathWord = null;
        //                                mdl.ReFileID = item.ReFileID;
        //                                mdl.MainEFileName = item.FileName;
        //                                mdl.DiaryNo = item.DiaryNo;
        //                                mdl.BranchName = item.BranchName;
        //                                mdl.EmpName = item.EmpName;
        //                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                                mdl.Desingnation = item.Desingnation;
        //                                mdl.AbbrevBranch = item.AbbrevBranch;
        //                                mdl.AnnexPdfPath = item.Annexurepath;
        //                                mdl.ItemName = item.ItemName;
        //                                mdl.CountsAPS = item.CountsAPS;
        //                                mdl.CommitteeId = item.CommitteeId;
        //                                mdl.CommitteeName = item.CommitteeName;
        //                                mdl.IsRejected = item.IsRejected;
        //                                mdl.RejectedRemarks = item.RejectedRemarks;
        //                                Mainlst.Add(mdl);
        //                            }
        //                        }




        //                        return Mainlst;

        //                    }


        //                }

        //                else //For Others Deparments
        //                {
        //                    List<eFileAttachment> Mainlst = new List<eFileAttachment>();
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    var query = (from a in ctx.eFileAttachments
        //                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                 from Ef in EFileTemp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                 join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
        //                                 // join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
        //                                 from cmf in comtemp.DefaultIfEmpty()
        //                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.IsBackLog != true
        //                                 select new
        //                                 {
        //                                     VerifiedBy = a.VerifiedBy,
        //                                     VerifiedDate = a.VerifiedDate,
        //                                     IsVerified = a.IsVerified,
        //                                     eFileAttachmentId = a.eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName = a.eFileName,
        //                                     PaperNature = a.PaperNature,
        //                                     PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                     eFilePath = a.eFilePath,
        //                                     Title = a.Title,
        //                                     Description = a.Description,
        //                                     CreateDate = a.CreatedDate,
        //                                     PaperStatus = a.PaperStatus,
        //                                     eFIleID = a.eFileID,
        //                                     PaperRefNo = a.PaperRefNo,
        //                                     DepartmentId = a.DepartmentId,
        //                                     OfficeCode = a.OfficeCode,
        //                                     ToDepartmentID = a.ToDepartmentID,
        //                                     ToOfficeCode = a.ToOfficeCode,
        //                                     ReplyStatus = a.ReplyStatus,
        //                                     ReplyDate = a.ReplyDate,
        //                                     ReceivedDate = a.ReceivedDate,
        //                                     DocumentTypeId = a.DocumentTypeId,
        //                                     ToPaperNature = a.ToPaperNature,
        //                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = a.LinkedRefNo,
        //                                     RevedOption = a.RevedOption,
        //                                     RecvDetails = a.RecvDetails,
        //                                     ToRecvDetails = a.ToRecvDetails,
        //                                     eMode = a.eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = a.PType,
        //                                     SendStatus = a.SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = a.eFilePathWord,
        //                                     ReFileID = a.ReFileID,
        //                                     FileName = Ef.eFileNumber,
        //                                     DiaryNo = a.DiaryNo,
        //                                     Annexurepath = a.AnnexPdfPath,
        //                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     CurrentAadharId = a.CurrentEmpAadharID,
        //                                     TOCCType = a.TOCCtype,
        //                                     CountsAPS = a.CountsAPS,
        //                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                     CommitteeName = cmf.CommitteeName,
        //                                     CommitteeId = a.CommitteeId,
        //                                     IsRejected = a.IsRejected,
        //                                     RejectedRemarks = a.RejectedRemarks

        //                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                    foreach (var item in query)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.IsVerified = item.IsVerified;
        //                        mdl.VerifiedDate = item.VerifiedDate;
        //                        mdl.VerifiedBy = item.VerifiedBy;
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                        mdl.TOCCtype = item.TOCCType;
        //                        mdl.AnnexPdfPath = item.Annexurepath;
        //                        mdl.ItemName = item.ItemName;
        //                        mdl.CountsAPS = item.CountsAPS;
        //                        mdl.CommitteeId = item.CommitteeId;
        //                        mdl.CommitteeName = item.CommitteeName;
        //                        mdl.IsRejected = item.IsRejected;
        //                        mdl.RejectedRemarks = item.RejectedRemarks;
        //                        lst.Add(mdl);
        //                        Mainlst.Add(mdl);
        //                    }
        //                    return Mainlst;



        //                }



        //            }

        //        }
        //    }
        //    else
        //    {
        //        using (eFileContext ctx = new eFileContext())
        //        {
        //            var query = (from a in ctx.eFileAttachments
        //                         join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                         from ps in temp.DefaultIfEmpty()
        //                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                         from Ef in EFileTemp.DefaultIfEmpty()
        //                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
        //                         //join cm in ctx.mBranches on a.CommitteeId equals cm.BranchCode into comtemp
        //                         from cmf in comtemp.DefaultIfEmpty()
        //                         where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
        //                         select new
        //                         {
        //                             VerifiedBy = a.VerifiedBy,
        //                             VerifiedDate = a.VerifiedDate,
        //                             IsVerified = a.IsVerified,
        //                             eFileAttachmentId = a.eFileAttachmentId,
        //                             Department = ps.deptname ?? "OTHER",
        //                             eFileName = a.eFileName,
        //                             PaperNature = a.PaperNature,
        //                             PaperNatureDays = a.PaperNatureDays ?? "0",
        //                             eFilePath = a.eFilePath,
        //                             Title = a.Title,
        //                             Description = a.Description,
        //                             CreateDate = a.CreatedDate,
        //                             PaperStatus = a.PaperStatus,
        //                             eFIleID = a.eFileID,
        //                             PaperRefNo = a.PaperRefNo,
        //                             DepartmentId = a.DepartmentId,
        //                             OfficeCode = a.OfficeCode,
        //                             ToDepartmentID = a.ToDepartmentID,
        //                             ToOfficeCode = a.ToOfficeCode,
        //                             ReplyStatus = a.ReplyStatus,
        //                             ReplyDate = a.ReplyDate,
        //                             ReceivedDate = a.ReceivedDate,
        //                             DocumentTypeId = a.DocumentTypeId,

        //                             ToPaperNature = a.ToPaperNature,
        //                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                             LinkedRefNo = a.LinkedRefNo,
        //                             RevedOption = a.RevedOption,
        //                             RecvDetails = a.RecvDetails,
        //                             ToRecvDetails = a.ToRecvDetails,
        //                             eMode = a.eMode,
        //                             RecvdPaperNature = c.PaperNature,
        //                             DocuemntPaperType = d.PaperType,
        //                             PType = a.PType,
        //                             SendStatus = a.SendStatus,
        //                             DeptAbbr = ps.deptabbr,
        //                             eFilePathWord = a.eFilePathWord,
        //                             ReFileID = a.ReFileID,
        //                             FileName = Ef.eFileNumber,
        //                             DiaryNo = a.DiaryNo,
        //                             CountsAPS = a.CountsAPS,
        //                             CommitteeName = cmf.CommitteeName,
        //                             CommitteeId = a.CommitteeId,
        //                             IsRejected = a.IsRejected,
        //                             RejectedRemarks = a.RejectedRemarks,

        //                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);
        //            List<eFileAttachment> lst = new List<eFileAttachment>();
        //            foreach (var item in query)
        //            {
        //                eFileAttachment mdl = new eFileAttachment();
        //                mdl.IsVerified = item.IsVerified;
        //                mdl.VerifiedDate = item.VerifiedDate;
        //                mdl.VerifiedBy = item.VerifiedBy;
        //                mdl.DepartmentName = item.Department;
        //                mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                mdl.eFileName = item.eFileName;
        //                mdl.PaperNature = item.PaperNature;
        //                mdl.PaperNatureDays = item.PaperNatureDays;
        //                mdl.eFilePath = item.eFilePath;
        //                mdl.Title = item.Title;
        //                mdl.Description = item.Description;
        //                mdl.CreatedDate = item.CreateDate;
        //                mdl.PaperStatus = item.PaperStatus;
        //                mdl.eFileID = item.eFIleID;
        //                mdl.PaperRefNo = item.PaperRefNo;
        //                mdl.DepartmentId = item.DepartmentId;
        //                mdl.OfficeCode = item.OfficeCode;
        //                mdl.ToDepartmentID = item.ToDepartmentID;
        //                mdl.ToOfficeCode = item.ToOfficeCode;
        //                mdl.ReplyStatus = item.ReplyStatus;
        //                mdl.ReplyDate = item.ReplyDate;
        //                mdl.ReceivedDate = item.ReceivedDate;
        //                mdl.DocumentTypeId = item.DocumentTypeId;

        //                mdl.ToPaperNature = item.ToPaperNature;
        //                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                mdl.LinkedRefNo = item.LinkedRefNo;
        //                mdl.RevedOption = item.RevedOption;
        //                mdl.RecvDetails = item.RecvDetails;
        //                mdl.ToRecvDetails = item.ToRecvDetails;
        //                mdl.eMode = item.eMode;
        //                mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                mdl.PType = item.PType;
        //                mdl.SendStatus = item.SendStatus;
        //                mdl.DeptAbbr = item.DeptAbbr;
        //                mdl.eFilePathWord = item.eFilePathWord;
        //                mdl.ReFileID = item.ReFileID;
        //                mdl.MainEFileName = item.FileName;
        //                mdl.DiaryNo = item.DiaryNo;
        //                mdl.ItemName = item.ItemName;
        //                mdl.CountsAPS = item.CountsAPS;
        //                mdl.CommitteeId = item.CommitteeId;
        //                mdl.CommitteeName = item.CommitteeName;
        //                mdl.IsRejected = item.IsRejected;
        //                mdl.RejectedRemarks = item.RejectedRemarks;
        //                lst.Add(mdl);
        //            }

        //            return lst;
        //        }
        //    }
        //}

        //public static object GetReceivePaperDetailsByDeptIdHC(object param)
        //{
        //    string[] str = param as string[];
        //    var V = "";
        //    string DeptId = Convert.ToString(str[0]);
        //    int Officecode = 0;
        //    int.TryParse(str[1], out Officecode);
        //    string Aadharid = str[2];
        //    int Year = Convert.ToInt32(str[4]);
        //    int PaperType = Convert.ToInt32(str[5]);
        //    List<string> deptlist = new List<string>();
        //    if (!string.IsNullOrEmpty(DeptId))
        //    {
        //        deptlist = DeptId.Split(',').Distinct().ToList();
        //    }

        //    if (Officecode == 0)
        //    {
        //        using (eFileContext ctx = new eFileContext())
        //        {
        //            if (Aadharid == "626192220638")
        //            {

        //                if (Year == 0)
        //                {
        //                    var query = (from a in ctx.eFileAttachments
        //                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                 from Ef in EFileTemp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false
        //                                 && a.IsVerified==true
        //                                 select new
        //                                 {
        //                                     eFileAttachmentId = a.eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName = a.eFileName,
        //                                     PaperNature = a.PaperNature,
        //                                     PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                     eFilePath = a.eFilePath,
        //                                     Title = a.Title,
        //                                     Description = a.Description,
        //                                     CreateDate = a.CreatedDate,
        //                                     PaperStatus = a.PaperStatus,
        //                                     eFIleID = a.eFileID,
        //                                     PaperRefNo = a.PaperRefNo,
        //                                     DepartmentId = a.DepartmentId,
        //                                     OfficeCode = a.OfficeCode,
        //                                     ToDepartmentID = a.ToDepartmentID,
        //                                     ToOfficeCode = a.ToOfficeCode,
        //                                     ReplyStatus = a.ReplyStatus,
        //                                     ReplyDate = a.ReplyDate,
        //                                     ReceivedDate = a.ReceivedDate,
        //                                     DocumentTypeId = a.DocumentTypeId,

        //                                     ToPaperNature = a.ToPaperNature,
        //                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = a.LinkedRefNo,
        //                                     RevedOption = a.RevedOption,
        //                                     RecvDetails = a.RecvDetails,
        //                                     ToRecvDetails = a.ToRecvDetails,
        //                                     eMode = a.eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = a.PType,
        //                                     SendStatus = a.SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = a.eFilePathWord,
        //                                     ReFileID = a.ReFileID,
        //                                     FileName = Ef.eFileNumber,
        //                                     DiaryNo = a.DiaryNo,
        //                                     VerifiedBy = a.VerifiedBy,
        //                                     VerifiedDate = a.VerifiedDate,
        //                                     IsVerified = a.IsVerified,
        //                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                     CountsAPS = a.CountsAPS,
        //                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                     Annexurepath = a.AnnexPdfPath,
        //                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    foreach (var item in query)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.IsVerified = item.IsVerified;
        //                        mdl.VerifiedDate = item.VerifiedDate;
        //                        mdl.VerifiedBy = item.VerifiedBy;
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.Desingnation = item.Desingnation;
        //                        mdl.AbbrevBranch = item.AbbrevBranch;
        //                        mdl.ItemName = item.ItemName;
        //                        mdl.CountsAPS = item.CountsAPS;
        //                        mdl.AnnexPdfPath = item.Annexurepath;
        //                        lst.Add(mdl);
        //                    }

        //                    return lst;
        //                }
        //                else
        //                {
        //                    DateTime Dt = DateTime.Now.AddYears(-Year);
        //                    var query = (from a in ctx.eFileAttachments
        //                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                 from Ef in EFileTemp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false
        //                                 && a.IsVerified == true
        //                                 && a.CreatedDate >= Dt && a.CreatedDate <= DateTime.Now
        //                                 select new
        //                                 {
        //                                     VerifiedBy = a.VerifiedBy,
        //                                     VerifiedDate = a.VerifiedDate,
        //                                     IsVerified = a.IsVerified,
        //                                     eFileAttachmentId = a.eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName = a.eFileName,
        //                                     PaperNature = a.PaperNature,
        //                                     PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                     eFilePath = a.eFilePath,
        //                                     Title = a.Title,
        //                                     Description = a.Description,
        //                                     CreateDate = a.CreatedDate,
        //                                     PaperStatus = a.PaperStatus,
        //                                     eFIleID = a.eFileID,
        //                                     PaperRefNo = a.PaperRefNo,
        //                                     DepartmentId = a.DepartmentId,
        //                                     OfficeCode = a.OfficeCode,
        //                                     ToDepartmentID = a.ToDepartmentID,
        //                                     ToOfficeCode = a.ToOfficeCode,
        //                                     ReplyStatus = a.ReplyStatus,
        //                                     ReplyDate = a.ReplyDate,
        //                                     ReceivedDate = a.ReceivedDate,
        //                                     DocumentTypeId = a.DocumentTypeId,

        //                                     ToPaperNature = a.ToPaperNature,
        //                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = a.LinkedRefNo,
        //                                     RevedOption = a.RevedOption,
        //                                     RecvDetails = a.RecvDetails,
        //                                     ToRecvDetails = a.ToRecvDetails,
        //                                     eMode = a.eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = a.PType,
        //                                     SendStatus = a.SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = a.eFilePathWord,
        //                                     ReFileID = a.ReFileID,
        //                                     FileName = Ef.eFileNumber,
        //                                     DiaryNo = a.DiaryNo,
        //                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                     CountsAPS = a.CountsAPS,
        //                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                     Annexurepath = a.AnnexPdfPath,
        //                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    foreach (var item in query)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.IsVerified = item.IsVerified;
        //                        mdl.VerifiedDate = item.VerifiedDate;
        //                        mdl.VerifiedBy = item.VerifiedBy;
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.Desingnation = item.Desingnation;
        //                        mdl.AbbrevBranch = item.AbbrevBranch;
        //                        mdl.ItemName = item.ItemName;
        //                        mdl.CountsAPS = item.CountsAPS;
        //                        mdl.AnnexPdfPath = item.Annexurepath;
        //                        lst.Add(mdl);
        //                    }

        //                    return lst;
        //                }

        //            }

        //            else
        //            {
        //                if (str[3] != "") //For Vidhan Sabha Department Users
        //                {
        //                    int CBranchID = Convert.ToInt32(str[3]);
        //                    List<eFileAttachment> Mainlst = new List<eFileAttachment>();
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    var s = string.Join(",", ctx.tMovementFile.Where(p => p.EmpAadharId == Aadharid)
        //                            .Select(p => p.eFileAttachmentId.ToString()));
        //                    if (s != "" && PaperType == 2)
        //                    {

        //                        List<int> TagIds1 = s.Split(',').Select(int.Parse).ToList();
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID && !TagIds1.Contains(p.eFileAttachmentId))
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        List<int> TagIds = s.Split(',').Select(int.Parse).ToList();
        //                        var query = (from a in ctx.eFileAttachments
        //                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                     from ps in temp.DefaultIfEmpty()
        //                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                     from Ef in EFileTemp.DefaultIfEmpty()
        //                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                     where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && TagIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                     select new
        //                                     {
        //                                         VerifiedBy = a.VerifiedBy,
        //                                         VerifiedDate = a.VerifiedDate,
        //                                         IsVerified = a.IsVerified,
        //                                         eFileAttachmentId = a.eFileAttachmentId,
        //                                         Department = ps.deptname ?? "OTHER",
        //                                         eFileName = a.eFileName,
        //                                         PaperNature = a.PaperNature,
        //                                         PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                         eFilePath = a.eFilePath,
        //                                         Title = a.Title,
        //                                         Description = a.Description,
        //                                         CreateDate = a.CreatedDate,
        //                                         PaperStatus = a.PaperStatus,
        //                                         eFIleID = a.eFileID,
        //                                         PaperRefNo = a.PaperRefNo,
        //                                         DepartmentId = a.DepartmentId,
        //                                         OfficeCode = a.OfficeCode,
        //                                         ToDepartmentID = a.ToDepartmentID,
        //                                         ToOfficeCode = a.ToOfficeCode,
        //                                         ReplyStatus = a.ReplyStatus,
        //                                         ReplyDate = a.ReplyDate,
        //                                         ReceivedDate = a.ReceivedDate,
        //                                         DocumentTypeId = a.DocumentTypeId,
        //                                         ToPaperNature = a.ToPaperNature,
        //                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                         LinkedRefNo = a.LinkedRefNo,
        //                                         RevedOption = a.RevedOption,
        //                                         RecvDetails = a.RecvDetails,
        //                                         ToRecvDetails = a.ToRecvDetails,
        //                                         eMode = a.eMode,
        //                                         RecvdPaperNature = c.PaperNature,
        //                                         DocuemntPaperType = d.PaperType,
        //                                         PType = a.PType,
        //                                         SendStatus = a.SendStatus,
        //                                         DeptAbbr = ps.deptabbr,
        //                                         eFilePathWord = a.eFilePathWord,
        //                                         ReFileID = a.ReFileID,
        //                                         FileName = Ef.eFileNumber,
        //                                         DiaryNo = a.DiaryNo,
        //                                         Annexurepath = a.AnnexPdfPath,
        //                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                         CurrentAadharId = a.CurrentEmpAadharID,
        //                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                         CountsAPS = a.CountsAPS,
        //                                         ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),

        //                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                        foreach (var item in query)
        //                        {
        //                            eFileAttachment mdl = new eFileAttachment();
        //                            mdl.IsVerified = item.IsVerified;
        //                            mdl.VerifiedDate = item.VerifiedDate;
        //                            mdl.VerifiedBy = item.VerifiedBy;
        //                            mdl.DepartmentName = item.Department;
        //                            mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                            mdl.eFileName = item.eFileName;
        //                            mdl.PaperNature = item.PaperNature;
        //                            mdl.PaperNatureDays = item.PaperNatureDays;
        //                            mdl.eFilePath = item.eFilePath;
        //                            mdl.Title = item.Title;
        //                            mdl.Description = item.Description;
        //                            mdl.CreatedDate = item.CreateDate;
        //                            mdl.PaperStatus = item.PaperStatus;
        //                            mdl.eFileID = item.eFIleID;
        //                            mdl.PaperRefNo = item.PaperRefNo;
        //                            mdl.DepartmentId = item.DepartmentId;
        //                            mdl.OfficeCode = item.OfficeCode;
        //                            mdl.ToDepartmentID = item.ToDepartmentID;
        //                            mdl.ToOfficeCode = item.ToOfficeCode;
        //                            mdl.ReplyStatus = item.ReplyStatus;
        //                            mdl.ReplyDate = item.ReplyDate;
        //                            mdl.ReceivedDate = item.ReceivedDate;
        //                            mdl.DocumentTypeId = item.DocumentTypeId;

        //                            mdl.ToPaperNature = item.ToPaperNature;
        //                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                            mdl.LinkedRefNo = item.LinkedRefNo;
        //                            mdl.RevedOption = item.RevedOption;
        //                            mdl.RecvDetails = item.RecvDetails;
        //                            mdl.ToRecvDetails = item.ToRecvDetails;
        //                            mdl.eMode = item.eMode;
        //                            mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                            mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                            mdl.PType = item.PType;
        //                            mdl.SendStatus = item.SendStatus;
        //                            mdl.DeptAbbr = item.DeptAbbr;
        //                            mdl.eFilePathWord = item.eFilePathWord;
        //                            mdl.ReFileID = item.ReFileID;
        //                            mdl.MainEFileName = item.FileName;
        //                            mdl.DiaryNo = item.DiaryNo;
        //                            mdl.BranchName = item.BranchName;
        //                            mdl.EmpName = item.EmpName;
        //                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                            mdl.Desingnation = item.Desingnation;
        //                            mdl.AbbrevBranch = item.AbbrevBranch;
        //                            mdl.AnnexPdfPath = item.Annexurepath;
        //                            mdl.ItemName = item.ItemName;
        //                            mdl.CountsAPS = item.CountsAPS;
        //                            lst.Add(mdl);
        //                            Mainlst.Add(mdl);
        //                        }
        //                        if (V != "")
        //                        {
        //                            List<int> NullIds = V.Split(',').Select(int.Parse).ToList();
        //                            var query1 = (from a in ctx.eFileAttachments
        //                                          join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                          from ps in temp.DefaultIfEmpty()
        //                                          join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                          from Ef in EFileTemp.DefaultIfEmpty()
        //                                          join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                          join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                          where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && NullIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                          select new
        //                                          {
        //                                              VerifiedBy = a.VerifiedBy,
        //                                              VerifiedDate = a.VerifiedDate,
        //                                              IsVerified = a.IsVerified,
        //                                              eFileAttachmentId = a.eFileAttachmentId,
        //                                              Department = ps.deptname ?? "OTHER",
        //                                              eFileName = a.eFileName,
        //                                              PaperNature = a.PaperNature,
        //                                              PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                              eFilePath = a.eFilePath,
        //                                              Title = a.Title,
        //                                              Description = a.Description,
        //                                              CreateDate = a.CreatedDate,
        //                                              PaperStatus = a.PaperStatus,
        //                                              eFIleID = a.eFileID,
        //                                              PaperRefNo = a.PaperRefNo,
        //                                              DepartmentId = a.DepartmentId,
        //                                              OfficeCode = a.OfficeCode,
        //                                              ToDepartmentID = a.ToDepartmentID,
        //                                              ToOfficeCode = a.ToOfficeCode,
        //                                              ReplyStatus = a.ReplyStatus,
        //                                              ReplyDate = a.ReplyDate,
        //                                              ReceivedDate = a.ReceivedDate,
        //                                              DocumentTypeId = a.DocumentTypeId,
        //                                              ToPaperNature = a.ToPaperNature,
        //                                              ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                              LinkedRefNo = a.LinkedRefNo,
        //                                              RevedOption = a.RevedOption,
        //                                              RecvDetails = a.RecvDetails,
        //                                              ToRecvDetails = a.ToRecvDetails,
        //                                              eMode = a.eMode,
        //                                              RecvdPaperNature = c.PaperNature,
        //                                              DocuemntPaperType = d.PaperType,
        //                                              PType = a.PType,
        //                                              SendStatus = a.SendStatus,
        //                                              DeptAbbr = ps.deptabbr,
        //                                              eFilePathWord = a.eFilePathWord,
        //                                              ReFileID = a.ReFileID,
        //                                              FileName = Ef.eFileNumber,
        //                                              DiaryNo = a.DiaryNo,
        //                                              Annexurepath = a.AnnexPdfPath,
        //                                              BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                              EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                              CurrentAadharId = a.CurrentEmpAadharID,
        //                                              AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                              Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                              CountsAPS = a.CountsAPS,
        //                                              ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),

        //                                          }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                            foreach (var item in query1)
        //                            {
        //                                eFileAttachment mdl = new eFileAttachment();
        //                                mdl.IsVerified = item.IsVerified;
        //                                mdl.VerifiedDate = item.VerifiedDate;
        //                                mdl.VerifiedBy = item.VerifiedBy;
        //                                mdl.DepartmentName = item.Department;
        //                                mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                                mdl.eFileName = item.eFileName;
        //                                mdl.PaperNature = item.PaperNature;
        //                                mdl.PaperNatureDays = item.PaperNatureDays;
        //                                mdl.eFilePath = null;
        //                                mdl.Title = item.Title;
        //                                mdl.Description = item.Description;
        //                                mdl.CreatedDate = item.CreateDate;
        //                                mdl.PaperStatus = item.PaperStatus;
        //                                mdl.eFileID = item.eFIleID;
        //                                mdl.PaperRefNo = item.PaperRefNo;
        //                                mdl.DepartmentId = item.DepartmentId;
        //                                mdl.OfficeCode = item.OfficeCode;
        //                                mdl.ToDepartmentID = item.ToDepartmentID;
        //                                mdl.ToOfficeCode = item.ToOfficeCode;
        //                                mdl.ReplyStatus = item.ReplyStatus;
        //                                mdl.ReplyDate = item.ReplyDate;
        //                                mdl.ReceivedDate = item.ReceivedDate;
        //                                mdl.DocumentTypeId = item.DocumentTypeId;

        //                                mdl.ToPaperNature = item.ToPaperNature;
        //                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                                mdl.LinkedRefNo = item.LinkedRefNo;
        //                                mdl.RevedOption = item.RevedOption;
        //                                mdl.RecvDetails = item.RecvDetails;
        //                                mdl.ToRecvDetails = item.ToRecvDetails;
        //                                mdl.eMode = item.eMode;
        //                                mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                                mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                                mdl.PType = item.PType;
        //                                mdl.SendStatus = item.SendStatus;
        //                                mdl.DeptAbbr = item.DeptAbbr;
        //                                mdl.eFilePathWord = null;
        //                                mdl.ReFileID = item.ReFileID;
        //                                mdl.MainEFileName = item.FileName;
        //                                mdl.DiaryNo = item.DiaryNo;
        //                                mdl.BranchName = item.BranchName;
        //                                mdl.EmpName = item.EmpName;
        //                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                                mdl.Desingnation = item.Desingnation;
        //                                mdl.AbbrevBranch = item.AbbrevBranch;
        //                                mdl.AnnexPdfPath = item.Annexurepath;
        //                                mdl.ItemName = item.ItemName;
        //                                mdl.CountsAPS = item.CountsAPS;
        //                                Mainlst.Add(mdl);
        //                            }
        //                        }

        //                        return Mainlst;

        //                    }
        //                    else if (s != "" && PaperType == 1)
        //                    {
        //                        List<int> TagIds1 = s.Split(',').Select(int.Parse).ToList();
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID && !TagIds1.Contains(p.eFileAttachmentId))
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        List<int> TagIds = s.Split(',').Select(int.Parse).ToList();
        //                        var query = (from a in ctx.eFileAttachments
        //                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                     from ps in temp.DefaultIfEmpty()
        //                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                     from Ef in EFileTemp.DefaultIfEmpty()
        //                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                     where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && TagIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                     select new
        //                                     {
        //                                         VerifiedBy = a.VerifiedBy,
        //                                         VerifiedDate = a.VerifiedDate,
        //                                         IsVerified = a.IsVerified,
        //                                         eFileAttachmentId = a.eFileAttachmentId,
        //                                         Department = ps.deptname ?? "OTHER",
        //                                         eFileName = a.eFileName,
        //                                         PaperNature = a.PaperNature,
        //                                         PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                         eFilePath = a.eFilePath,
        //                                         Title = a.Title,
        //                                         Description = a.Description,
        //                                         CreateDate = a.CreatedDate,
        //                                         PaperStatus = a.PaperStatus,
        //                                         eFIleID = a.eFileID,
        //                                         PaperRefNo = a.PaperRefNo,
        //                                         DepartmentId = a.DepartmentId,
        //                                         OfficeCode = a.OfficeCode,
        //                                         ToDepartmentID = a.ToDepartmentID,
        //                                         ToOfficeCode = a.ToOfficeCode,
        //                                         ReplyStatus = a.ReplyStatus,
        //                                         ReplyDate = a.ReplyDate,
        //                                         ReceivedDate = a.ReceivedDate,
        //                                         DocumentTypeId = a.DocumentTypeId,
        //                                         ToPaperNature = a.ToPaperNature,
        //                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                         LinkedRefNo = a.LinkedRefNo,
        //                                         RevedOption = a.RevedOption,
        //                                         RecvDetails = a.RecvDetails,
        //                                         ToRecvDetails = a.ToRecvDetails,
        //                                         eMode = a.eMode,
        //                                         RecvdPaperNature = c.PaperNature,
        //                                         DocuemntPaperType = d.PaperType,
        //                                         PType = a.PType,
        //                                         SendStatus = a.SendStatus,
        //                                         DeptAbbr = ps.deptabbr,
        //                                         eFilePathWord = a.eFilePathWord,
        //                                         ReFileID = a.ReFileID,
        //                                         FileName = Ef.eFileNumber,
        //                                         DiaryNo = a.DiaryNo,
        //                                         Annexurepath = a.AnnexPdfPath,
        //                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                         CurrentAadharId = a.CurrentEmpAadharID,
        //                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                         CountsAPS = a.CountsAPS,
        //                                         ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),

        //                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                        foreach (var item in query)
        //                        {
        //                            eFileAttachment mdl = new eFileAttachment();
        //                            mdl.IsVerified = item.IsVerified;
        //                            mdl.VerifiedDate = item.VerifiedDate;
        //                            mdl.VerifiedBy = item.VerifiedBy;
        //                            mdl.DepartmentName = item.Department;
        //                            mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                            mdl.eFileName = item.eFileName;
        //                            mdl.PaperNature = item.PaperNature;
        //                            mdl.PaperNatureDays = item.PaperNatureDays;
        //                            mdl.eFilePath = item.eFilePath;
        //                            mdl.Title = item.Title;
        //                            mdl.Description = item.Description;
        //                            mdl.CreatedDate = item.CreateDate;
        //                            mdl.PaperStatus = item.PaperStatus;
        //                            mdl.eFileID = item.eFIleID;
        //                            mdl.PaperRefNo = item.PaperRefNo;
        //                            mdl.DepartmentId = item.DepartmentId;
        //                            mdl.OfficeCode = item.OfficeCode;
        //                            mdl.ToDepartmentID = item.ToDepartmentID;
        //                            mdl.ToOfficeCode = item.ToOfficeCode;
        //                            mdl.ReplyStatus = item.ReplyStatus;
        //                            mdl.ReplyDate = item.ReplyDate;
        //                            mdl.ReceivedDate = item.ReceivedDate;
        //                            mdl.DocumentTypeId = item.DocumentTypeId;

        //                            mdl.ToPaperNature = item.ToPaperNature;
        //                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                            mdl.LinkedRefNo = item.LinkedRefNo;
        //                            mdl.RevedOption = item.RevedOption;
        //                            mdl.RecvDetails = item.RecvDetails;
        //                            mdl.ToRecvDetails = item.ToRecvDetails;
        //                            mdl.eMode = item.eMode;
        //                            mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                            mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                            mdl.PType = item.PType;
        //                            mdl.SendStatus = item.SendStatus;
        //                            mdl.DeptAbbr = item.DeptAbbr;
        //                            mdl.eFilePathWord = item.eFilePathWord;
        //                            mdl.ReFileID = item.ReFileID;
        //                            mdl.MainEFileName = item.FileName;
        //                            mdl.DiaryNo = item.DiaryNo;
        //                            mdl.BranchName = item.BranchName;
        //                            mdl.EmpName = item.EmpName;
        //                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                            mdl.Desingnation = item.Desingnation;
        //                            mdl.AbbrevBranch = item.AbbrevBranch;
        //                            mdl.AnnexPdfPath = item.Annexurepath;
        //                            mdl.ItemName = item.ItemName;
        //                            mdl.CountsAPS = item.CountsAPS;
        //                            lst.Add(mdl);
        //                            Mainlst.Add(mdl);
        //                        }
        //                        return lst;
        //                    }
        //                    else
        //                    {
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID)
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        if (V != "")
        //                        {
        //                            List<int> NullIds = V.Split(',').Select(int.Parse).ToList();
        //                            var query1 = (from a in ctx.eFileAttachments
        //                                          join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                          from ps in temp.DefaultIfEmpty()
        //                                          join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                          from Ef in EFileTemp.DefaultIfEmpty()
        //                                          join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                          join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                          where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && NullIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                          select new
        //                                          {
        //                                              VerifiedBy = a.VerifiedBy,
        //                                              VerifiedDate = a.VerifiedDate,
        //                                              IsVerified = a.IsVerified,
        //                                              eFileAttachmentId = a.eFileAttachmentId,
        //                                              Department = ps.deptname ?? "OTHER",
        //                                              eFileName = a.eFileName,
        //                                              PaperNature = a.PaperNature,
        //                                              PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                              eFilePath = a.eFilePath,
        //                                              Title = a.Title,
        //                                              Description = a.Description,
        //                                              CreateDate = a.CreatedDate,
        //                                              PaperStatus = a.PaperStatus,
        //                                              eFIleID = a.eFileID,
        //                                              PaperRefNo = a.PaperRefNo,
        //                                              DepartmentId = a.DepartmentId,
        //                                              OfficeCode = a.OfficeCode,
        //                                              ToDepartmentID = a.ToDepartmentID,
        //                                              ToOfficeCode = a.ToOfficeCode,
        //                                              ReplyStatus = a.ReplyStatus,
        //                                              ReplyDate = a.ReplyDate,
        //                                              ReceivedDate = a.ReceivedDate,
        //                                              DocumentTypeId = a.DocumentTypeId,
        //                                              ToPaperNature = a.ToPaperNature,
        //                                              ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                              LinkedRefNo = a.LinkedRefNo,
        //                                              RevedOption = a.RevedOption,
        //                                              RecvDetails = a.RecvDetails,
        //                                              ToRecvDetails = a.ToRecvDetails,
        //                                              eMode = a.eMode,
        //                                              RecvdPaperNature = c.PaperNature,
        //                                              DocuemntPaperType = d.PaperType,
        //                                              PType = a.PType,
        //                                              SendStatus = a.SendStatus,
        //                                              DeptAbbr = ps.deptabbr,
        //                                              eFilePathWord = a.eFilePathWord,
        //                                              ReFileID = a.ReFileID,
        //                                              FileName = Ef.eFileNumber,
        //                                              DiaryNo = a.DiaryNo,
        //                                              Annexurepath = a.AnnexPdfPath,
        //                                              BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                              EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                              CurrentAadharId = a.CurrentEmpAadharID,
        //                                              AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                              Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                              CountsAPS = a.CountsAPS,
        //                                              ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),

        //                                          }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                            foreach (var item in query1)
        //                            {
        //                                eFileAttachment mdl = new eFileAttachment();
        //                                mdl.IsVerified = item.IsVerified;
        //                                mdl.VerifiedDate = item.VerifiedDate;
        //                                mdl.VerifiedBy = item.VerifiedBy;
        //                                mdl.DepartmentName = item.Department;
        //                                mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                                mdl.eFileName = item.eFileName;
        //                                mdl.PaperNature = item.PaperNature;
        //                                mdl.PaperNatureDays = item.PaperNatureDays;
        //                                mdl.eFilePath = null;
        //                                mdl.Title = item.Title;
        //                                mdl.Description = item.Description;
        //                                mdl.CreatedDate = item.CreateDate;
        //                                mdl.PaperStatus = item.PaperStatus;
        //                                mdl.eFileID = item.eFIleID;
        //                                mdl.PaperRefNo = item.PaperRefNo;
        //                                mdl.DepartmentId = item.DepartmentId;
        //                                mdl.OfficeCode = item.OfficeCode;
        //                                mdl.ToDepartmentID = item.ToDepartmentID;
        //                                mdl.ToOfficeCode = item.ToOfficeCode;
        //                                mdl.ReplyStatus = item.ReplyStatus;
        //                                mdl.ReplyDate = item.ReplyDate;
        //                                mdl.ReceivedDate = item.ReceivedDate;
        //                                mdl.DocumentTypeId = item.DocumentTypeId;

        //                                mdl.ToPaperNature = item.ToPaperNature;
        //                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                                mdl.LinkedRefNo = item.LinkedRefNo;
        //                                mdl.RevedOption = item.RevedOption;
        //                                mdl.RecvDetails = item.RecvDetails;
        //                                mdl.ToRecvDetails = item.ToRecvDetails;
        //                                mdl.eMode = item.eMode;
        //                                mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                                mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                                mdl.PType = item.PType;
        //                                mdl.SendStatus = item.SendStatus;
        //                                mdl.DeptAbbr = item.DeptAbbr;
        //                                mdl.eFilePathWord = null;
        //                                mdl.ReFileID = item.ReFileID;
        //                                mdl.MainEFileName = item.FileName;
        //                                mdl.DiaryNo = item.DiaryNo;
        //                                mdl.BranchName = item.BranchName;
        //                                mdl.EmpName = item.EmpName;
        //                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                                mdl.Desingnation = item.Desingnation;
        //                                mdl.AbbrevBranch = item.AbbrevBranch;
        //                                mdl.AnnexPdfPath = item.Annexurepath;
        //                                mdl.ItemName = item.ItemName;
        //                                mdl.CountsAPS = item.CountsAPS;
        //                                Mainlst.Add(mdl);
        //                            }
        //                        }




        //                        return Mainlst;

        //                    }


        //                }

        //                else //For Others Deparments
        //                {
        //                    List<eFileAttachment> Mainlst = new List<eFileAttachment>();
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    var query = (from a in ctx.eFileAttachments
        //                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                 from Ef in EFileTemp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false
        //                                 select new
        //                                 {
        //                                     VerifiedBy = a.VerifiedBy,
        //                                     VerifiedDate = a.VerifiedDate,
        //                                     IsVerified = a.IsVerified,
        //                                     eFileAttachmentId = a.eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName = a.eFileName,
        //                                     PaperNature = a.PaperNature,
        //                                     PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                     eFilePath = a.eFilePath,
        //                                     Title = a.Title,
        //                                     Description = a.Description,
        //                                     CreateDate = a.CreatedDate,
        //                                     PaperStatus = a.PaperStatus,
        //                                     eFIleID = a.eFileID,
        //                                     PaperRefNo = a.PaperRefNo,
        //                                     DepartmentId = a.DepartmentId,
        //                                     OfficeCode = a.OfficeCode,
        //                                     ToDepartmentID = a.ToDepartmentID,
        //                                     ToOfficeCode = a.ToOfficeCode,
        //                                     ReplyStatus = a.ReplyStatus,
        //                                     ReplyDate = a.ReplyDate,
        //                                     ReceivedDate = a.ReceivedDate,
        //                                     DocumentTypeId = a.DocumentTypeId,
        //                                     ToPaperNature = a.ToPaperNature,
        //                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = a.LinkedRefNo,
        //                                     RevedOption = a.RevedOption,
        //                                     RecvDetails = a.RecvDetails,
        //                                     ToRecvDetails = a.ToRecvDetails,
        //                                     eMode = a.eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = a.PType,
        //                                     SendStatus = a.SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = a.eFilePathWord,
        //                                     ReFileID = a.ReFileID,
        //                                     FileName = Ef.eFileNumber,
        //                                     DiaryNo = a.DiaryNo,
        //                                     Annexurepath = a.AnnexPdfPath,
        //                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     CurrentAadharId = a.CurrentEmpAadharID,
        //                                     TOCCType = a.TOCCtype,
        //                                     CountsAPS = a.CountsAPS,
        //                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                    foreach (var item in query)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.IsVerified = item.IsVerified;
        //                        mdl.VerifiedDate = item.VerifiedDate;
        //                        mdl.VerifiedBy = item.VerifiedBy;
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                        mdl.TOCCtype = item.TOCCType;
        //                        mdl.AnnexPdfPath = item.Annexurepath;
        //                        mdl.ItemName = item.ItemName;
        //                        mdl.CountsAPS = item.CountsAPS;
        //                        lst.Add(mdl);
        //                        Mainlst.Add(mdl);
        //                    }
        //                    return Mainlst;



        //                }



        //            }

        //        }
        //    }
        //    else
        //    {
        //        using (eFileContext ctx = new eFileContext())
        //        {
        //            var query = (from a in ctx.eFileAttachments
        //                         join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                         from ps in temp.DefaultIfEmpty()
        //                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                         from Ef in EFileTemp.DefaultIfEmpty()
        //                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                         where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
        //                         select new
        //                         {
        //                             VerifiedBy = a.VerifiedBy,
        //                             VerifiedDate = a.VerifiedDate,
        //                             IsVerified = a.IsVerified,
        //                             eFileAttachmentId = a.eFileAttachmentId,
        //                             Department = ps.deptname ?? "OTHER",
        //                             eFileName = a.eFileName,
        //                             PaperNature = a.PaperNature,
        //                             PaperNatureDays = a.PaperNatureDays ?? "0",
        //                             eFilePath = a.eFilePath,
        //                             Title = a.Title,
        //                             Description = a.Description,
        //                             CreateDate = a.CreatedDate,
        //                             PaperStatus = a.PaperStatus,
        //                             eFIleID = a.eFileID,
        //                             PaperRefNo = a.PaperRefNo,
        //                             DepartmentId = a.DepartmentId,
        //                             OfficeCode = a.OfficeCode,
        //                             ToDepartmentID = a.ToDepartmentID,
        //                             ToOfficeCode = a.ToOfficeCode,
        //                             ReplyStatus = a.ReplyStatus,
        //                             ReplyDate = a.ReplyDate,
        //                             ReceivedDate = a.ReceivedDate,
        //                             DocumentTypeId = a.DocumentTypeId,

        //                             ToPaperNature = a.ToPaperNature,
        //                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                             LinkedRefNo = a.LinkedRefNo,
        //                             RevedOption = a.RevedOption,
        //                             RecvDetails = a.RecvDetails,
        //                             ToRecvDetails = a.ToRecvDetails,
        //                             eMode = a.eMode,
        //                             RecvdPaperNature = c.PaperNature,
        //                             DocuemntPaperType = d.PaperType,
        //                             PType = a.PType,
        //                             SendStatus = a.SendStatus,
        //                             DeptAbbr = ps.deptabbr,
        //                             eFilePathWord = a.eFilePathWord,
        //                             ReFileID = a.ReFileID,
        //                             FileName = Ef.eFileNumber,
        //                             DiaryNo = a.DiaryNo,
        //                             CountsAPS = a.CountsAPS,
        //                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
        //                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);
        //            List<eFileAttachment> lst = new List<eFileAttachment>();
        //            foreach (var item in query)
        //            {
        //                eFileAttachment mdl = new eFileAttachment();
        //                mdl.IsVerified = item.IsVerified;
        //                mdl.VerifiedDate = item.VerifiedDate;
        //                mdl.VerifiedBy = item.VerifiedBy;
        //                mdl.DepartmentName = item.Department;
        //                mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                mdl.eFileName = item.eFileName;
        //                mdl.PaperNature = item.PaperNature;
        //                mdl.PaperNatureDays = item.PaperNatureDays;
        //                mdl.eFilePath = item.eFilePath;
        //                mdl.Title = item.Title;
        //                mdl.Description = item.Description;
        //                mdl.CreatedDate = item.CreateDate;
        //                mdl.PaperStatus = item.PaperStatus;
        //                mdl.eFileID = item.eFIleID;
        //                mdl.PaperRefNo = item.PaperRefNo;
        //                mdl.DepartmentId = item.DepartmentId;
        //                mdl.OfficeCode = item.OfficeCode;
        //                mdl.ToDepartmentID = item.ToDepartmentID;
        //                mdl.ToOfficeCode = item.ToOfficeCode;
        //                mdl.ReplyStatus = item.ReplyStatus;
        //                mdl.ReplyDate = item.ReplyDate;
        //                mdl.ReceivedDate = item.ReceivedDate;
        //                mdl.DocumentTypeId = item.DocumentTypeId;

        //                mdl.ToPaperNature = item.ToPaperNature;
        //                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                mdl.LinkedRefNo = item.LinkedRefNo;
        //                mdl.RevedOption = item.RevedOption;
        //                mdl.RecvDetails = item.RecvDetails;
        //                mdl.ToRecvDetails = item.ToRecvDetails;
        //                mdl.eMode = item.eMode;
        //                mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                mdl.PType = item.PType;
        //                mdl.SendStatus = item.SendStatus;
        //                mdl.DeptAbbr = item.DeptAbbr;
        //                mdl.eFilePathWord = item.eFilePathWord;
        //                mdl.ReFileID = item.ReFileID;
        //                mdl.MainEFileName = item.FileName;
        //                mdl.DiaryNo = item.DiaryNo;
        //                mdl.ItemName = item.ItemName;
        //                mdl.CountsAPS = item.CountsAPS;
        //                lst.Add(mdl);
        //            }

        //            return lst;
        //        }
        //    }
        //}
        public static object MovementList(object param)
        {

            tMovementFile model = param as tMovementFile;
            eFileContext pCtxt = new eFileContext();
            var query = (from Movemnt in pCtxt.tMovementFile
                         where (Movemnt.eFileAttachmentId == model.ID)
                         select new HouseComModel
                         {
                             ID = Movemnt.ID,
                             efileID = Movemnt.eFileAttachmentId,
                             BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                             EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                             AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                             // EmpName = (from EN in pCtxt.mStaff where EN.AadharID == Movemnt.EmpAadharId select EN.StaffName).FirstOrDefault(),
                             Remarks = Movemnt.Remarks,
                             AssignDateTime = Movemnt.AssignDateTime,
                         }).OrderByDescending(a => a.AssignDateTime).ToList();
            model.HouseComModel = query.ToList();
            return model;
        }



        public static object Draftpapers(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;
                tDrafteFile DraftObj = new tDrafteFile();
                FileNotingMovement MoveObj = new FileNotingMovement();
                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {

                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            var Q = (from Efile in ctx.tDrafteFile
                                     where Efile.eFileAttachmentId == mdl.eFileAttachmentId
                                     select Efile).Count();
                            if (Q == 0)
                            {
                                eFileAttachment model = ctx.eFileAttachments.Single(a => a.eFileAttachmentId == mdl.eFileAttachmentId);
                                DraftObj.eFileAttachmentId = model.eFileAttachmentId;
                                DraftObj.eFileID = model.ReFileID;
                                DraftObj.CreatedDate = model.CreatedDate;
                                DraftObj.DocumentTypeId = model.DocumentTypeId;
                                DraftObj.eFileName = model.eFileName;
                                DraftObj.Description = model.Description;
                                DraftObj.SendStatus = model.SendStatus;
                                DraftObj.DepartmentId = model.DepartmentId;
                                DraftObj.Title = model.Title;
                                DraftObj.eFileNameId = model.eFileNameId;
                                DraftObj.ParentId = model.ParentId;
                                DraftObj.SplitFileCount = model.SplitFileCount;
                                DraftObj.ReportLayingHouse = model.ReportLayingHouse;
                                DraftObj.Approve = model.Approve;
                                DraftObj.OfficeCode = model.OfficeCode;
                                DraftObj.PaperNature = model.PaperNature;
                                DraftObj.PaperNatureDays = model.PaperNatureDays;
                                DraftObj.ToDepartmentID = model.ToDepartmentID;
                                DraftObj.ToOfficeCode = model.ToOfficeCode;
                                DraftObj.PaperStatus = model.PaperStatus;
                                DraftObj.SendDate = model.SendDate;
                                DraftObj.PType = model.PType;
                                DraftObj.eFilePath = model.eFilePath;
                                DraftObj.ReceivedDate = model.ReceivedDate;
                                DraftObj.PaperRefNo = model.PaperRefNo;
                                DraftObj.ReplyStatus = model.ReplyStatus;
                                DraftObj.ReplyDate = model.ReplyDate;
                                DraftObj.ReplyDate = model.ReplyDate;
                                DraftObj.LinkedRefNo = model.LinkedRefNo;
                                DraftObj.ToPaperNature = model.ToPaperNature;
                                DraftObj.ToPaperNatureDays = model.ToPaperNatureDays;
                                DraftObj.RevedOption = model.RevedOption;
                                DraftObj.ToRecvDetails = model.ToRecvDetails;
                                DraftObj.eMode = model.eMode;
                                DraftObj.PaperTicketValue = model.PaperTicketValue;
                                DraftObj.DispatchtoWhom = model.DispatchtoWhom;
                                DraftObj.eFilePathWord = model.eFilePathWord;
                                DraftObj.ReFileID = model.ReFileID;
                                DraftObj.DraftBy = mdl.CurrentEmpAadharID;
                                DraftObj.DraftDate = DateTime.Now;
                                DraftObj.IsDeleted = model.IsDeleted;
                                DraftObj.CurrentBranchId = model.CurrentBranchId;
                                DraftObj.CurrentEmpAadharID = mdl.CurrentEmpAadharID;
                                DraftObj.isDraft = true;
                                DraftObj.CountAPS = model.CountsAPS;
                                DraftObj.ItemID = model.ItemId;
                                DraftObj.AnnexPdf = model.AnnexPdfPath;
                                ctx.tDrafteFile.Add(DraftObj);
                                ctx.SaveChanges();
                                SBL.DomainModel.Models.eFile.eFileAttachment obj = ctx.eFileAttachments.Single(m => m.eFileAttachmentId == mdl.eFileAttachmentId);
                                obj.creDraftID = DraftObj.ID;
                                ctx.SaveChanges();
                                SBL.DomainModel.Models.eFile.tDrafteFile Updateobj = ctx.tDrafteFile.Single(m => m.ID == DraftObj.ID);
                                Updateobj.DraftId = DraftObj.ID;
                                ctx.SaveChanges();

                                MoveObj.BranchId = model.CurrentBranchId;
                                MoveObj.EmpAadharId = model.DraftedBy;
                                MoveObj.Noting = "NOTINUSE";
                                MoveObj.AssignDateTime = model.CreatedDate;
                                MoveObj.EmpName = "ss";
                                MoveObj.EmpDesignation = "ss";
                                MoveObj.AssignfrmAadharId = mdl.CurrentEmpAadharID;
                                MoveObj.AssignfrmName = "ss";
                                MoveObj.AssignfrmDesignation = "ss";
                                MoveObj.FileId = 0;
                                MoveObj.DraftId = DraftObj.ID;
                                ctx.FileNotingMovement.Add(MoveObj);
                                ctx.SaveChanges();
                                // ctx.Close();

                                // ctx.Close();
                            }
                            else
                            {
                                return 2;
                            }

                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static object Procedding_Draftpapers(object param)
        {
            try
            {
                COmmitteeProceeding mdl = param as COmmitteeProceeding;
                tDrafteFile DraftObj = new tDrafteFile();
                FileNotingMovement MoveObj = new FileNotingMovement();
                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.ProcIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.ProcIds[i].ToString()))
                        {

                            mdl.ProID = mdl.ProcIds[i];
                            var Q = (from Efile in ctx.tDrafteFile
                                     where Efile.eFileAttachmentId == mdl.ProID
                                     select Efile).Count();
                            if (Q == 0)
                            {
                                COmmitteeProceeding model = ctx.CommitteeProceeding.Single(a => a.ProID == mdl.ProID); //priy
                                DraftObj.eFileAttachmentId = model.ProID;
                                DraftObj.eFileID = 0;
                                DraftObj.CreatedDate = model.CreatedDate;
                                DraftObj.DocumentTypeId = 5;
                                DraftObj.eFileName = "";
                                DraftObj.Description = model.Remark;
                                DraftObj.SendStatus = false;
                                DraftObj.DepartmentId = "HPD0001";
                                DraftObj.ToDepartmentID = "HPD0001";
                                DraftObj.Title = model.Remark;
                                DraftObj.eFileName = "";
                                DraftObj.ParentId = 0;
                                DraftObj.SplitFileCount = 0;
                                DraftObj.ReportLayingHouse = 0;
                                DraftObj.Approve = 0;
                                DraftObj.OfficeCode = model.OfficeCode;
                                DraftObj.PaperNature = 3; // Static 
                                DraftObj.PaperNatureDays = "";
                                DraftObj.ToOfficeCode = 0;
                                DraftObj.PaperStatus = "";
                                DraftObj.SendDate = DateTime.Now;
                                DraftObj.PType = "";
                                DraftObj.eFilePath = "";
                                DraftObj.ReceivedDate = DateTime.Now;
                                DraftObj.PaperRefNo = "";
                                DraftObj.ReplyStatus = "";
                                DraftObj.ReplyDate = DateTime.Now;
                                DraftObj.ReplyDate = DateTime.Now;
                                DraftObj.LinkedRefNo = "";
                                DraftObj.ToPaperNature = 0;
                                DraftObj.ToPaperNatureDays = "";
                                DraftObj.RevedOption = "";
                                DraftObj.ToRecvDetails = "";
                                DraftObj.eMode = 0;
                                DraftObj.PaperTicketValue = "";
                                DraftObj.DispatchtoWhom = "";
                                DraftObj.eFilePathWord = model.ProFilePathWord;
                                int BranchID = (from m in ctx.tBranchesCommittee where m.CommitteeId == model.CommitteeId select m.BranchId).SingleOrDefault();
                                DraftObj.DraftBy = mdl.Aadharid;
                                DraftObj.DraftDate = DateTime.Now;
                                DraftObj.IsDeleted = model.IsDeleted;
                                DraftObj.CurrentBranchId = BranchID;
                                DraftObj.CurrentEmpAadharID = mdl.Aadharid;//from controller
                                DraftObj.isDraft = true;
                                //  DraftObj.AnnexPdf = model.ProFilePathPdf;
                                DraftObj.AnnexPdf = "";
                                DraftObj.eFilePath = model.ProFilePathPdf;
                                DraftObj.IpAddress = mdl.IpAddress;
                                DraftObj.MacAddress = mdl.MacAddress;
                                ctx.tDrafteFile.Add(DraftObj);
                                ctx.SaveChanges();
                                // SBL.DomainModel.Models.eFile.eFileAttachment obj = ctx.eFileAttachments.Single(m => m.eFileAttachmentId == mdl.eFileAttachmentId);
                                // obj.creDraftID = DraftObj.ID;
                                // ctx.SaveChanges();
                                SBL.DomainModel.Models.eFile.tDrafteFile Updateobj = ctx.tDrafteFile.Single(m => m.ID == DraftObj.ID);
                                var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == mdl.Aadharid select AF).FirstOrDefault();
                                var AssgnTo = (from AT in ctx.mStaff where AT.AadharID == mdl.Aadharid select AT).FirstOrDefault();
                                Updateobj.DraftId = DraftObj.ID;
                                ctx.SaveChanges();
                                MoveObj.eFileAttachmentId = model.ProID;
                                MoveObj.BranchId = BranchID;
                                MoveObj.EmpAadharId = mdl.Aadharid;
                                MoveObj.Noting = "NOTINUSE";
                                MoveObj.AssignDateTime = model.CreatedDate;
                                MoveObj.EmpName = AssgnTo.StaffName;
                                MoveObj.EmpDesignation = AssgnTo.Designation;
                                MoveObj.AssignfrmAadharId = mdl.Aadharid;
                                MoveObj.AssignfrmName = Assgnfrom.StaffName;
                                MoveObj.AssignfrmDesignation = Assgnfrom.Designation;
                                MoveObj.FileId = 0;
                                MoveObj.DraftId = DraftObj.ID;
                                ctx.FileNotingMovement.Add(MoveObj);
                                ctx.SaveChanges();
                                ctx.Close();

                                // ctx.Close();
                            }
                            else
                            {
                                return 2;
                            }

                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }
        public static object DraftpapersSent(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;
                tDrafteFile DraftObj = new tDrafteFile();
                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {

                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            var Q = (from Efile in ctx.tDrafteFile
                                     where Efile.eFileAttachmentId == mdl.eFileAttachmentId
                                     select Efile).Count();
                            if (Q == 0)
                            {
                                eFileAttachment model = ctx.eFileAttachments.Single(a => a.eFileAttachmentId == mdl.eFileAttachmentId);
                                DraftObj.eFileAttachmentId = model.eFileAttachmentId;
                                DraftObj.eFileID = model.eFileID;
                                DraftObj.CreatedDate = model.CreatedDate;
                                DraftObj.DocumentTypeId = model.DocumentTypeId;
                                DraftObj.eFileName = model.eFileName;
                                DraftObj.Description = model.Description;
                                DraftObj.SendStatus = model.SendStatus;
                                DraftObj.DepartmentId = model.ToDepartmentID;
                                DraftObj.Title = model.Title;
                                DraftObj.eFileNameId = model.eFileNameId;
                                DraftObj.ParentId = model.ParentId;
                                DraftObj.SplitFileCount = model.SplitFileCount;
                                DraftObj.ReportLayingHouse = model.ReportLayingHouse;
                                DraftObj.Approve = model.Approve;
                                DraftObj.OfficeCode = model.OfficeCode;
                                DraftObj.PaperNature = model.PaperNature;
                                DraftObj.PaperNatureDays = model.PaperNatureDays;
                                DraftObj.ToDepartmentID = model.DepartmentId;
                                DraftObj.ToOfficeCode = model.ToOfficeCode;
                                DraftObj.PaperStatus = model.PaperStatus;
                                DraftObj.SendDate = model.SendDate;
                                DraftObj.PType = model.PType;
                                DraftObj.eFilePath = model.eFilePath;
                                DraftObj.ReceivedDate = model.ReceivedDate;
                                DraftObj.PaperRefNo = model.PaperRefNo;
                                DraftObj.ReplyStatus = model.ReplyStatus;
                                DraftObj.ReplyDate = model.ReplyDate;
                                DraftObj.ReplyDate = model.ReplyDate;
                                DraftObj.LinkedRefNo = model.LinkedRefNo;
                                DraftObj.ToPaperNature = model.ToPaperNature;
                                DraftObj.ToPaperNatureDays = model.ToPaperNatureDays;
                                DraftObj.RevedOption = model.RevedOption;
                                DraftObj.ToRecvDetails = model.ToRecvDetails;
                                DraftObj.eMode = model.eMode;
                                DraftObj.PaperTicketValue = model.PaperTicketValue;
                                DraftObj.DispatchtoWhom = model.DispatchtoWhom;
                                DraftObj.eFilePathWord = model.eFilePathWord;
                                DraftObj.ReFileID = model.ReFileID;
                                DraftObj.DraftBy = mdl.CurrentEmpAadharID;
                                DraftObj.DraftDate = DateTime.Now;
                                DraftObj.IsDeleted = model.IsDeleted;
                                DraftObj.CurrentBranchId = model.CurrentBranchId;
                                DraftObj.CurrentEmpAadharID = mdl.CurrentEmpAadharID;
                                DraftObj.isDraft = true;
                                DraftObj.AnnexPdf = model.AnnexPdfPath;
                                ctx.tDrafteFile.Add(DraftObj);
                                ctx.SaveChanges();
                                // ctx.Close();
                            }
                            else
                            {
                                return 2;
                            }

                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        public static object GetDraftList(object param)
        {
            eFileAttachments val = param as eFileAttachments;
            string[] ne = param as string[];//
            int PaperType = Convert.ToInt32(ne[5]);
            if (ne[4] == "Commissioner (Department)")
            {
                string[] str = param as string[];
                var V = "";
                string DeptId = Convert.ToString(str[0]);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);
                string Aadharid = str[2];

                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                using (eFileContext ctx = new eFileContext())
                {

                    var query = (from a in ctx.tDrafteFile
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID   // a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where a.CurrentEmpAadharID == Aadharid && a.isDraft == true
                                 select new
                                 {
                                     DraftId = a.ID,
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,

                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     //   DiaryNo = a.DiaryNo,
                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                     eFilePathPDFN = a.eFilePathPDF,
                                     AnnexPDF = a.AnnexPdf,
                                     eFilePathDOCN = a.eFilePathDOC,
                                     ToListComplN = a.ToAllRecipName,
                                     CCListComplN = a.CCAllRecipName,
                                     CurrentAadharId = a.CurrentEmpAadharID,
                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                     CountsAPS = a.CountAPS,
                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemID select Item.ItemTypeName).FirstOrDefault(),
                                 }).ToList().OrderByDescending(a => a.DraftId); //  (a => a.DraftId;.eFileAttachmentId)
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DraftId = item.DraftId;
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        //    mdl.DiaryNo = item.DiaryNo;
                        mdl.BranchName = item.BranchName;
                        mdl.AnnexPDF = item.AnnexPDF;
                        mdl.eFilePathPDFN = item.eFilePathPDFN;
                        mdl.eFilePathWordN = item.eFilePathDOCN;
                        mdl.ToListComplN = item.ToListComplN;
                        mdl.CCListComplN = item.CCListComplN;
                        mdl.EmpName = item.EmpName;
                        mdl.CurrentEmpAadharID = item.CurrentAadharId;
                        mdl.Desingnation = item.Desingnation;
                        mdl.AbbrevBranch = item.AbbrevBranch;
                        mdl.ItemName = item.ItemName;
                        mdl.CountsAPS = item.CountsAPS;
                        lst.Add(mdl);


                    }

                    return lst;

                }
            }
            else
            {


                string[] str = param as string[];

                if (str[3] != "" && PaperType == 2)
                {
                    var V = "";
                    string DeptId = Convert.ToString(str[0]);
                    int Officecode = 0;
                    int.TryParse(str[1], out Officecode);
                    string Aadharid = str[2];

                    int Branchid = Convert.ToInt32(str[3]);
                    List<string> deptlist = new List<string>();
                    if (!string.IsNullOrEmpty(DeptId))
                    {
                        deptlist = DeptId.Split(',').Distinct().ToList();
                    }
                    using (eFileContext ctx = new eFileContext())
                    {

                        var query = (from a in ctx.tDrafteFile
                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                     from ps in temp.DefaultIfEmpty()
                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                     from Ef in EFileTemp.DefaultIfEmpty()
                                     join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID   //  a.ToPaperNature equals c.PaperNatureID
                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                     where a.CurrentBranchId == Branchid && a.ToDepartmentID == "HPD0001" && a.isDraft == true
                                     select new
                                     {
                                         DraftId = a.ID,
                                         eFileAttachmentId = a.eFileAttachmentId,
                                         Department = ps.deptname ?? "OTHER",
                                         eFileName = a.eFileName,
                                         PaperNature = a.PaperNature,
                                         PaperNatureDays = a.PaperNatureDays ?? "0",
                                         eFilePath = a.eFilePath,
                                         Title = a.Title,
                                         Description = a.Description,
                                         CreateDate = a.CreatedDate,
                                         PaperStatus = a.PaperStatus,
                                         eFIleID = a.eFileID,
                                         PaperRefNo = a.PaperRefNo,
                                         DepartmentId = a.DepartmentId,
                                         OfficeCode = a.OfficeCode,
                                         ToDepartmentID = a.ToDepartmentID,
                                         ToOfficeCode = a.ToOfficeCode,
                                         ReplyStatus = a.ReplyStatus,
                                         ReplyDate = a.ReplyDate,
                                         ReceivedDate = a.ReceivedDate,
                                         DocumentTypeId = a.DocumentTypeId,

                                         ToPaperNature = a.ToPaperNature,
                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                         LinkedRefNo = a.LinkedRefNo,
                                         RevedOption = a.RevedOption,
                                         RecvDetails = a.RecvDetails,
                                         ToRecvDetails = a.ToRecvDetails,
                                         eMode = a.eMode,
                                         RecvdPaperNature = c.PaperNature,
                                         DocuemntPaperType = d.PaperType,
                                         PType = a.PType,
                                         SendStatus = a.SendStatus,
                                         DeptAbbr = ps.deptabbr,
                                         eFilePathWord = a.eFilePathWord,
                                         ReFileID = a.ReFileID,
                                         FileName = Ef.eFileNumber,
                                         //   DiaryNo = a.DiaryNo,
                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                         eFilePathPDFN = a.eFilePathPDF,
                                         AnnexPDF = a.AnnexPdf,
                                         eFilePathDOCN = a.eFilePathDOC,
                                         ToListComplN = a.ToAllRecipName,
                                         CCListComplN = a.CCAllRecipName,
                                         CurrentAadharId = a.CurrentEmpAadharID,
                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                         CountsAPS = a.CountAPS,
                                         ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemID select Item.ItemTypeName).FirstOrDefault(),
                                     }).ToList().OrderByDescending(a => a.DraftId);
                        List<eFileAttachment> lst = new List<eFileAttachment>();
                        foreach (var item in query)
                        {
                            eFileAttachment mdl = new eFileAttachment();
                            mdl.DraftId = item.DraftId;
                            mdl.DepartmentName = item.Department;
                            mdl.eFileAttachmentId = item.eFileAttachmentId;
                            mdl.eFileName = item.eFileName;
                            mdl.PaperNature = item.PaperNature;
                            mdl.PaperNatureDays = item.PaperNatureDays;
                            mdl.eFilePath = item.eFilePath;
                            mdl.Title = item.Title;
                            mdl.Description = item.Description;
                            mdl.CreatedDate = item.CreateDate;
                            mdl.PaperStatus = item.PaperStatus;
                            mdl.eFileID = item.eFIleID;
                            mdl.PaperRefNo = item.PaperRefNo;
                            mdl.DepartmentId = item.DepartmentId;
                            mdl.OfficeCode = item.OfficeCode;
                            mdl.ToDepartmentID = item.ToDepartmentID;
                            mdl.ToOfficeCode = item.ToOfficeCode;
                            mdl.ReplyStatus = item.ReplyStatus;
                            mdl.ReplyDate = item.ReplyDate;
                            mdl.ReceivedDate = item.ReceivedDate;
                            mdl.DocumentTypeId = item.DocumentTypeId;

                            mdl.ToPaperNature = item.ToPaperNature;
                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                            mdl.LinkedRefNo = item.LinkedRefNo;
                            mdl.RevedOption = item.RevedOption;
                            mdl.RecvDetails = item.RecvDetails;
                            mdl.ToRecvDetails = item.ToRecvDetails;
                            mdl.eMode = item.eMode;
                            mdl.RecvdPaperNature = item.RecvdPaperNature;

                            mdl.DocuemntPaperType = item.DocuemntPaperType;
                            mdl.PType = item.PType;
                            mdl.SendStatus = item.SendStatus;
                            mdl.DeptAbbr = item.DeptAbbr;
                            mdl.eFilePathWord = item.eFilePathWord;
                            mdl.ReFileID = item.ReFileID;
                            mdl.MainEFileName = item.FileName;
                            //    mdl.DiaryNo = item.DiaryNo;
                            mdl.BranchName = item.BranchName;
                            mdl.eFilePathPDFN = item.eFilePathPDFN;
                            mdl.AnnexPDF = item.AnnexPDF;
                            mdl.eFilePathWordN = item.eFilePathDOCN;
                            mdl.ToListComplN = item.ToListComplN;
                            mdl.CCListComplN = item.CCListComplN;
                            mdl.EmpName = item.EmpName;
                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
                            mdl.Desingnation = item.Desingnation;
                            mdl.AbbrevBranch = item.AbbrevBranch;
                            mdl.ItemName = item.ItemName;
                            mdl.CountsAPS = item.CountsAPS;
                            lst.Add(mdl);


                        }

                        return lst;

                    }
                }
                else if (str[3] != "" && PaperType == 1)
                {
                    var V = "";
                    string DeptId = Convert.ToString(str[0]);
                    int Officecode = 0;
                    int.TryParse(str[1], out Officecode);
                    string Aadharid = str[2];

                    int Branchid = Convert.ToInt32(str[3]);
                    List<string> deptlist = new List<string>();
                    if (!string.IsNullOrEmpty(DeptId))
                    {
                        deptlist = DeptId.Split(',').Distinct().ToList();
                    }
                    using (eFileContext ctx = new eFileContext())
                    {

                        var query = (from a in ctx.tDrafteFile
                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                     from ps in temp.DefaultIfEmpty()
                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                     from Ef in EFileTemp.DefaultIfEmpty()
                                     join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID  // a.ToPaperNature equals c.PaperNatureID
                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                     where a.CurrentBranchId == Branchid && a.ToDepartmentID == "HPD0001" && a.isDraft == true && (a.DraftBy == Aadharid || a.CurrentEmpAadharID == Aadharid)
                                     select new
                                     {
                                         DraftId = a.ID,
                                         eFileAttachmentId = a.eFileAttachmentId,
                                         Department = ps.deptname ?? "OTHER",
                                         eFileName = a.eFileName,
                                         PaperNature = a.PaperNature,
                                         PaperNatureDays = a.PaperNatureDays ?? "0",
                                         eFilePath = a.eFilePath,
                                         Title = a.Title,
                                         Description = a.Description,
                                         CreateDate = a.CreatedDate,
                                         PaperStatus = a.PaperStatus,
                                         eFIleID = a.eFileID,
                                         PaperRefNo = a.PaperRefNo,
                                         DepartmentId = a.DepartmentId,
                                         OfficeCode = a.OfficeCode,
                                         ToDepartmentID = a.ToDepartmentID,
                                         ToOfficeCode = a.ToOfficeCode,
                                         ReplyStatus = a.ReplyStatus,
                                         ReplyDate = a.ReplyDate,
                                         ReceivedDate = a.ReceivedDate,
                                         DocumentTypeId = a.DocumentTypeId,

                                         ToPaperNature = a.ToPaperNature,
                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                         LinkedRefNo = a.LinkedRefNo,
                                         RevedOption = a.RevedOption,
                                         RecvDetails = a.RecvDetails,
                                         ToRecvDetails = a.ToRecvDetails,
                                         eMode = a.eMode,
                                         RecvdPaperNature = c.PaperNature,
                                         DocuemntPaperType = d.PaperType,
                                         PType = a.PType,
                                         SendStatus = a.SendStatus,
                                         DeptAbbr = ps.deptabbr,

                                         eFilePathWord = a.eFilePathWord,
                                         ReFileID = a.ReFileID,
                                         FileName = Ef.eFileNumber,
                                         //   DiaryNo = a.DiaryNo,
                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                         eFilePathPDFN = a.eFilePathPDF,
                                         AnnexPDF = a.AnnexPdf,
                                         eFilePathDOCN = a.eFilePathDOC,
                                         ToListComplN = a.ToAllRecipName,
                                         CCListComplN = a.CCAllRecipName,
                                         CurrentAadharId = a.CurrentEmpAadharID,
                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                         CountsAPS = a.CountAPS,
                                         ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemID select Item.ItemTypeName).FirstOrDefault(),
                                     }).ToList().OrderByDescending(a => a.DraftId);
                        List<eFileAttachment> lst = new List<eFileAttachment>();
                        foreach (var item in query)
                        {
                            eFileAttachment mdl = new eFileAttachment();
                            mdl.DraftId = item.DraftId;
                            mdl.DepartmentName = item.Department;
                            mdl.eFileAttachmentId = item.eFileAttachmentId;
                            mdl.eFileName = item.eFileName;
                            mdl.PaperNature = item.PaperNature;
                            mdl.PaperNatureDays = item.PaperNatureDays;
                            mdl.eFilePath = item.eFilePath;
                            mdl.Title = item.Title;
                            mdl.Description = item.Description;
                            mdl.CreatedDate = item.CreateDate;
                            mdl.PaperStatus = item.PaperStatus;
                            mdl.eFileID = item.eFIleID;
                            mdl.PaperRefNo = item.PaperRefNo;
                            mdl.DepartmentId = item.DepartmentId;
                            mdl.OfficeCode = item.OfficeCode;
                            mdl.ToDepartmentID = item.ToDepartmentID;
                            mdl.ToOfficeCode = item.ToOfficeCode;
                            mdl.ReplyStatus = item.ReplyStatus;
                            mdl.ReplyDate = item.ReplyDate;
                            mdl.ReceivedDate = item.ReceivedDate;
                            mdl.DocumentTypeId = item.DocumentTypeId;

                            mdl.ToPaperNature = item.ToPaperNature;
                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                            mdl.LinkedRefNo = item.LinkedRefNo;
                            mdl.RevedOption = item.RevedOption;
                            mdl.RecvDetails = item.RecvDetails;
                            mdl.ToRecvDetails = item.ToRecvDetails;
                            mdl.eMode = item.eMode;
                            mdl.RecvdPaperNature = item.RecvdPaperNature;

                            mdl.DocuemntPaperType = item.DocuemntPaperType;
                            mdl.PType = item.PType;
                            mdl.SendStatus = item.SendStatus;
                            mdl.DeptAbbr = item.DeptAbbr;
                            mdl.eFilePathWord = item.eFilePathWord;
                            mdl.ReFileID = item.ReFileID;
                            mdl.MainEFileName = item.FileName;
                            //    mdl.DiaryNo = item.DiaryNo;
                            mdl.BranchName = item.BranchName;
                            mdl.AnnexPDF = item.AnnexPDF;
                            mdl.eFilePathPDFN = item.eFilePathPDFN;
                            mdl.eFilePathWordN = item.eFilePathDOCN;
                            mdl.ToListComplN = item.ToListComplN;
                            mdl.CCListComplN = item.CCListComplN;
                            mdl.EmpName = item.EmpName;
                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
                            mdl.Desingnation = item.Desingnation;
                            mdl.AbbrevBranch = item.AbbrevBranch;
                            mdl.ItemName = item.ItemName;
                            mdl.CountsAPS = item.CountsAPS;
                            lst.Add(mdl);


                        }

                        return lst;

                    }
                }
                else
                {
                    string DeptId = Convert.ToString(str[0]);
                    List<string> deptlist = new List<string>();
                    if (!string.IsNullOrEmpty(DeptId))
                    {
                        deptlist = DeptId.Split(',').Distinct().ToList();
                    }

                    using (eFileContext ctx = new eFileContext())
                    {

                        var query = (from a in ctx.tDrafteFile
                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                     from ps in temp.DefaultIfEmpty()
                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                     from Ef in EFileTemp.DefaultIfEmpty()
                                     join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID //on a.ToPaperNature equals c.PaperNatureID
                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                     where deptlist.Contains(a.ToDepartmentID) && a.isDraft == true
                                     select new
                                     {
                                         DraftId = a.ID,
                                         eFileAttachmentId = a.eFileAttachmentId,
                                         Department = ps.deptname ?? "OTHER",
                                         eFileName = a.eFileName,
                                         PaperNature = a.PaperNature,
                                         PaperNatureDays = a.PaperNatureDays ?? "0",
                                         eFilePath = a.eFilePath,
                                         Title = a.Title,
                                         Description = a.Description,
                                         CreateDate = a.CreatedDate,
                                         PaperStatus = a.PaperStatus,
                                         eFIleID = a.eFileID,
                                         PaperRefNo = a.PaperRefNo,
                                         DepartmentId = a.DepartmentId,
                                         OfficeCode = a.OfficeCode,
                                         ToDepartmentID = a.ToDepartmentID,
                                         ToOfficeCode = a.ToOfficeCode,
                                         ReplyStatus = a.ReplyStatus,
                                         ReplyDate = a.ReplyDate,
                                         ReceivedDate = a.ReceivedDate,
                                         DocumentTypeId = a.DocumentTypeId,

                                         ToPaperNature = a.ToPaperNature,
                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                         LinkedRefNo = a.LinkedRefNo,
                                         RevedOption = a.RevedOption,
                                         RecvDetails = a.RecvDetails,
                                         ToRecvDetails = a.ToRecvDetails,
                                         eMode = a.eMode,
                                         RecvdPaperNature = c.PaperNature,
                                         DocuemntPaperType = d.PaperType,
                                         PType = a.PType,
                                         SendStatus = a.SendStatus,
                                         DeptAbbr = ps.deptabbr,
                                         eFilePathWord = a.eFilePathWord,
                                         ReFileID = a.ReFileID,
                                         FileName = Ef.eFileNumber,
                                         //   DiaryNo = a.DiaryNo,
                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                         AnnexPDF = a.AnnexPdf,
                                         eFilePathPDFN = a.eFilePathPDF,
                                         eFilePathDOCN = a.eFilePathDOC,
                                         ToListComplN = a.ToAllRecipName,
                                         CCListComplN = a.CCAllRecipName,
                                         CurrentAadharId = a.CurrentEmpAadharID,
                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                         CountsAPS = a.CountAPS,
                                         ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemID select Item.ItemTypeName).FirstOrDefault(),
                                         Count = (from Item in ctx.eFileAttachments where Item.ReplyRefNo == a.PaperRefNo select Item).Count(),
                                     }).ToList().OrderByDescending(a => a.DraftId);
                        List<eFileAttachment> lst = new List<eFileAttachment>();
                        foreach (var item in query)
                        {
                            eFileAttachment mdl = new eFileAttachment();
                            mdl.DraftId = item.DraftId;
                            mdl.DepartmentName = item.Department;
                            mdl.eFileAttachmentId = item.eFileAttachmentId;
                            mdl.eFileName = item.eFileName;
                            mdl.PaperNature = item.PaperNature;
                            mdl.PaperNatureDays = item.PaperNatureDays;
                            mdl.eFilePath = item.eFilePath;
                            mdl.Title = item.Title;
                            mdl.Description = item.Description;
                            mdl.CreatedDate = item.CreateDate;
                            mdl.PaperStatus = item.PaperStatus;
                            mdl.eFileID = item.eFIleID;
                            mdl.PaperRefNo = item.PaperRefNo;
                            mdl.DepartmentId = item.DepartmentId;
                            mdl.OfficeCode = item.OfficeCode;
                            mdl.ToDepartmentID = item.ToDepartmentID;
                            mdl.ToOfficeCode = item.ToOfficeCode;
                            mdl.ReplyStatus = item.ReplyStatus;
                            mdl.ReplyDate = item.ReplyDate;
                            mdl.ReceivedDate = item.ReceivedDate;
                            mdl.DocumentTypeId = item.DocumentTypeId;

                            mdl.ToPaperNature = item.ToPaperNature;
                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                            mdl.LinkedRefNo = item.LinkedRefNo;
                            mdl.RevedOption = item.RevedOption;
                            mdl.RecvDetails = item.RecvDetails;
                            mdl.ToRecvDetails = item.ToRecvDetails;
                            mdl.eMode = item.eMode;
                            mdl.RecvdPaperNature = item.RecvdPaperNature;

                            mdl.DocuemntPaperType = item.DocuemntPaperType;
                            mdl.PType = item.PType;
                            mdl.SendStatus = item.SendStatus;
                            mdl.DeptAbbr = item.DeptAbbr;
                            mdl.eFilePathWord = item.eFilePathWord;
                            mdl.ReFileID = item.ReFileID;
                            mdl.MainEFileName = item.FileName;
                            //    mdl.DiaryNo = item.DiaryNo;
                            mdl.BranchName = item.BranchName;
                            mdl.AnnexPDF = item.AnnexPDF;
                            mdl.eFilePathPDFN = item.eFilePathPDFN;
                            mdl.eFilePathWordN = item.eFilePathDOCN;
                            mdl.ToListComplN = item.ToListComplN;
                            mdl.CCListComplN = item.CCListComplN;
                            mdl.EmpName = item.EmpName;
                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
                            mdl.Desingnation = item.Desingnation;
                            mdl.AbbrevBranch = item.AbbrevBranch;
                            mdl.ItemName = item.ItemName;
                            mdl.CountsAPS = item.CountsAPS;
                            mdl.CountRply = item.Count;
                            lst.Add(mdl);


                        }

                        return lst;

                    }
                }
            }



        }


        public static object DeleteDraftPapers(object param) //ppr
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;
                tDrafteFile DraftObj = new tDrafteFile();
                //  SBL.DomainModel.Models.eFile.eFile obj = new DomainModel.Models.eFile.eFile();
                int? ExistDraftId = 0;
                using (eFileContext ctx = new eFileContext())
                {

                    ExistDraftId = (from rf in ctx.eFiles where rf.FileAttched_DraftId == mdl.eFileAttachmentId select rf.FileAttched_DraftId).FirstOrDefault();
                    if (ExistDraftId == null)
                    {
                    }
                    else
                    {
                        SBL.DomainModel.Models.eFile.eFile obj = ctx.eFiles.Single(a => a.FileAttched_DraftId == mdl.eFileAttachmentId);
                        obj.FileAttched_DraftId = null;
                        ctx.SaveChanges();
                    }


                    var Delete = ctx.tDrafteFile.SingleOrDefault(a => a.ID == mdl.eFileAttachmentId);

                    ctx.tDrafteFile.Remove(Delete);
                    ctx.SaveChanges();
                    ctx.Close();
                }
                return 0;  //Success
            }

            catch
            {
                return 1; //Error
            }
        }

        static List<mDepartment> GetDepartment(object param)
        {
            eFileContext context = new eFileContext();
            mBranches NewObj = param as mBranches;
            var data = (from mdl in context.departments where mdl.deptname.Contains(">") select mdl).OrderBy(z => z.deptname).ToList();
            var data2 = (from mdl in context.departments where !mdl.deptname.Contains(">") select mdl).OrderBy(z => z.deptname).ToList();
            NewObj.DeptList = data2.ToList<mDepartment>();
            foreach (var session in data)
            {
                NewObj.DeptList.Add(new mDepartment()
                {
                    deptname = session.deptname,
                    deptId = session.deptId
                });
            }
            return NewObj.DeptList.ToList();

        }

        static List<HouseComModel> GetMembers(object param)
        {
            eFileContext context = new eFileContext();
            mBranches NewObj = param as mBranches;
            var Memlist = (from M in context.members
                           join MA in context.mMemberAssembly on M.MemberCode equals MA.MemberID
                           join Const in context.mConstituency on MA.ConstituencyCode equals Const.ConstituencyCode
                           where (Const.AssemblyID == NewObj.AssemblyID)
                               && (MA.AssemblyID == NewObj.AssemblyID)
                               && !(from ExcepMinis in context.mMinistry where ExcepMinis.AssemblyID == NewObj.AssemblyID select ExcepMinis.MemberCode).Contains(M.MemberCode)
                                && M.Active == true

                           orderby M.Name
                           select new HouseComModel
                           {
                               MId = MA.MemberID,
                               MemberName = M.Name,
                               ConstituencyName = Const.ConstituencyName,
                               ConstituencyCode = Const.ConstituencyCode
                           }).ToList();

            foreach (var item in Memlist)
            {
                item.MemberName = item.MemberName + " (" + item.ConstituencyName + "-" + item.ConstituencyCode.ToString() + ")";
            }
            NewObj.MemberList = Memlist;
            return NewObj.MemberList.ToList();
        }

        static List<HouseComModel> GetOtherThanHP(object param)
        {
            eFileContext context = new eFileContext();
            mBranches NewObj = param as mBranches;
            var Otherlist = (from R in context.tRecipientGroupMember
                             where (R.GroupID == 176)
                             orderby R.Name
                             select new HouseComModel
                             {
                                 OId = R.GroupMemberID,
                                 OName = R.Name,
                                 OEmail = R.Email,
                                 ONumber = R.MobileNo,
                             }).ToList();

            foreach (var item in Otherlist)
            {
                item.OName = item.OName + " (" + item.OEmail + "-" + item.ONumber.ToString() + ")";
            }
            NewObj.OtherThanList = Otherlist;
            return NewObj.OtherThanList.ToList();
        }
        public static object GetDataList(object param)
        {

            eFileAttachment model = param as eFileAttachment;
            eFileContext pCtxt = new eFileContext();
            var Q = (from Draft in pCtxt.tDrafteFile
                     where Draft.ID == model.DraftId
                     select Draft).FirstOrDefault();
            model.DocumentTypeId = Q.DocumentTypeId;
            model.PaperNature = Q.PaperNature;
            model.Title = Q.Title;
            model.ToListComplN = Q.ToAllRecipName;
            model.CCListComplN = Q.CCAllRecipName;

            //new added
            model.ToListComplIds = Q.ToAllRecipIds;
            //model.CCListComplIds = Q.CCAllRecipIds;
            //model.ItemId = Q.ItemID;
            //model.ItemName = Q.ItemName;
            model.ToDepIds = Q.TODepts;
            //model.CCDepIds= Q.CCDepts;
            //model.ToMemIds = Q.TOMembers;
            model.DepartmentId = Q.DepartmentId;
            //

            model.eFilePathPDFN = Q.eFilePathPDF;
            model.eFilePathWordN = Q.eFilePathDOC;
            model.ItemTypeName = Q.ItemID;
            model.ReplyRefNo = Q.PaperRefNo;
            model.CurrentBranchId = Q.CurrentBranchId;


            model.ItemName = (from Item in pCtxt.mCommitteeReplyItemType where Item.Id == Q.ItemID select Item.ItemTypeName).FirstOrDefault();
            model.CountsAPS = Q.CountAPS;
            model.PaperRefrenceManualy = Q.PaperRefrenceManualy;
            // model.PaperDraft = Q.PaperDraft;
            if (Q.DepartmentId != "")
            {
                model.DepartmentName = (from d in pCtxt.departments where d.deptId == Q.DepartmentId select d.deptname).FirstOrDefault();
            }
            return model;
        }

        public static object UpdateDraftDatabyId(object param)
        {
            try
            {
                eFileAttachment model = param as eFileAttachment;
                eFileContext ctx = new eFileContext();
                SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.ID == model.DraftId);
                obj.CountAPS = model.CountsAPS;
                obj.ItemID = model.ItemTypeName;
                obj.DocumentTypeId = model.DocumentTypeId;
                obj.Description = model.Title;
                obj.eFilePathPDF = model.eFilePath;
                obj.eFilePathDOC = model.eFilePathWord;
                obj.TODepts = model.ToDepIds;
                obj.CCDepts = model.CCDepIds;
                obj.TOMembers = model.ToMemIds;
                obj.CCMembers = model.CCMemIds;
                obj.ToOthers = model.ToOtherIds;
                obj.CCOthers = model.CCOtherIds;
                obj.ToAllRecipName = model.ToListComplN;
                obj.CCAllRecipIds = model.CCListComplIds;
                obj.CCAllRecipName = model.CCListComplN;
                // obj.PaperDraft = model.PaperDraft;

                obj.ToAllRecipIds = model.ToListComplIds;
                obj.DepartmentId = model.ToDepIds;

                obj.Title = model.Title;
                obj.PaperNature = model.PaperNature;
                obj.PaperRefrenceManualy = model.PaperRefrenceManualy;
                ctx.SaveChanges();
                return 1; //Updated
            }
            catch
            {
                return 0; //Error
            }
        }


        public static object UpdateNoting(object param)
        {
            eFileContext context = new eFileContext();
            mBranches Obj = param as mBranches;
            FileNotingMovement MoveObj = new FileNotingMovement();
            try
            {
                var Assgnfrom = (from AF in context.mStaff where AF.AadharID == Obj.AssignfrmAadharId select AF).FirstOrDefault();
                var AssgnTo = (from AT in context.mStaff where AT.AadharID == Obj.AadharID select AT).FirstOrDefault();
                tDrafteFile PaperObj = context.tDrafteFile.Single(m => m.ID == Obj.ID);
                PaperObj.CurrentEmpAadharID = Obj.AadharID;
                PaperObj.AssignDateTime = Obj.ModifiedDate;
                context.SaveChanges();
                MoveObj.eFileAttachmentId = PaperObj.eFileAttachmentId;
                MoveObj.BranchId = Obj.BranchId;
                MoveObj.EmpAadharId = Obj.AadharID;
                MoveObj.Noting = Obj.Remark;
                MoveObj.AssignDateTime = Obj.ModifiedDate;
                MoveObj.EmpName = AssgnTo.StaffName;
                MoveObj.EmpDesignation = AssgnTo.Designation;
                MoveObj.AssignfrmAadharId = Obj.AssignfrmAadharId;
                MoveObj.AssignfrmName = Assgnfrom.StaffName;
                MoveObj.AssignfrmDesignation = Assgnfrom.Designation;
                MoveObj.FileId = Obj.efileID;
                MoveObj.DraftId = Obj.ID;
                context.FileNotingMovement.Add(MoveObj);
                context.SaveChanges();
                context.Close();
            }

            catch (Exception ex)
            {

            }

            return Obj;
        }

        public static object NotingList(object param)
        {

            tMovementFile model = param as tMovementFile;
            eFileContext pCtxt = new eFileContext();
            var query = (from Movemnt in pCtxt.FileNotingMovement
                         //  where (Movemnt.eFileAttachmentId == model.ID)
                         where (Movemnt.DraftId == model.ID && Movemnt.Noting != "NOTINUSE")
                         select new HouseComModel
                         {
                             ID = Movemnt.ID,
                             efileID = Movemnt.eFileAttachmentId,
                             BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                             EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                             AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                             // EmpName = (from EN in pCtxt.mStaff where EN.AadharID == Movemnt.EmpAadharId select EN.StaffName).FirstOrDefault(),
                             Remarks = Movemnt.Noting,
                             AssignDateTime = Movemnt.AssignDateTime,
                         }).OrderByDescending(a => a.AssignDateTime).ToList();
            model.HouseComModel = query.ToList();
            return model;
        }


        public static object DraftMovementList(object param)
        {
            tMovementFile model = param as tMovementFile;
            eFileContext pCtxt = new eFileContext();
            var Q = (from Efile in pCtxt.FileNotingMovement
                     where Efile.eFileAttachmentId == model.ID && Efile.EmpAadharId == model.AssignfrmAadharId
                     select Efile).Count();
            if (Q == 0)
            {
                model.Value = false;
                var Q2 = (from Efile in pCtxt.FileNotingMovement
                          where Efile.eFileAttachmentId == model.ID && Efile.AssignfrmAadharId == model.AssignfrmAadharId
                          select Efile).Count();
                if (Q2 == 0)
                {
                    model.Value = false;
                    var Q3 = (from Efile in pCtxt.eFileAttachments
                              where Efile.eFileAttachmentId == model.ID && Efile.CurrentEmpAadharID == model.AssignfrmAadharId
                              select Efile).Count();

                    if (Q3 == 0)
                    {
                        model.Value = false;
                    }
                    else
                    {
                        var query = (from Movemnt in pCtxt.tMovementFile
                                     where (Movemnt.eFileAttachmentId == model.ID)
                                     select new HouseComModel
                                     {
                                         ID = Movemnt.ID,
                                         efileID = Movemnt.eFileAttachmentId,
                                         BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                                         EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                                         AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                                         Remarks = Movemnt.Remarks,
                                         AssignDateTime = Movemnt.AssignDateTime,
                                     }).OrderByDescending(a => a.AssignDateTime).ToList();
                        model.HouseComModel = query.ToList();
                        model.Value = true;
                    }
                }
                else
                {
                    var query = (from Movemnt in pCtxt.tMovementFile
                                 where (Movemnt.eFileAttachmentId == model.ID)
                                 select new HouseComModel
                                 {
                                     ID = Movemnt.ID,
                                     efileID = Movemnt.eFileAttachmentId,
                                     BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                                     EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                                     AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                                     Remarks = Movemnt.Remarks,
                                     AssignDateTime = Movemnt.AssignDateTime,
                                 }).OrderByDescending(a => a.AssignDateTime).ToList();
                    model.HouseComModel = query.ToList();
                    model.Value = true;
                }
            }
            else
            {
                var query = (from Movemnt in pCtxt.tMovementFile
                             where (Movemnt.eFileAttachmentId == model.ID)
                             select new HouseComModel
                             {
                                 ID = Movemnt.ID,
                                 efileID = Movemnt.eFileAttachmentId,
                                 BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                                 EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                                 AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                                 Remarks = Movemnt.Remarks,
                                 AssignDateTime = Movemnt.AssignDateTime,
                             }).OrderByDescending(a => a.AssignDateTime).ToList();
                model.HouseComModel = query.ToList();
                model.Value = true;
            }

            return model;
        }

        public static object NotingMovementList(object param)
        {
            tMovementFile model = param as tMovementFile;
            eFileContext pCtxt = new eFileContext();
            var Q = (from Efile in pCtxt.FileNotingMovement
                     where Efile.DraftId == model.ID && Efile.EmpAadharId == model.AssignfrmAadharId //&& Efile.Noting != "NOTINUSE"
                     select Efile).Count();
            if (Q == 0)
            {
                model.Value = false;
                var Q2 = (from Efile in pCtxt.FileNotingMovement
                          where Efile.DraftId == model.ID && Efile.AssignfrmAadharId == model.AssignfrmAadharId // && Efile.Noting != "NOTINUSE"
                          select Efile).Count();
                if (Q2 == 0)
                {
                    model.Value = false;
                    var Q3 = (from Efile in pCtxt.eFileAttachments
                              where Efile.DraftId == model.ID && Efile.CurrentEmpAadharID == model.AssignfrmAadharId   // && Efile.Noting != "NOTINUSE"
                              select Efile).Count();

                    if (Q3 == 0)
                    {
                        model.Value = false;
                    }
                    else
                    {
                        var query = (from Movemnt in pCtxt.FileNotingMovement
                                     join SN in pCtxt.users on Movemnt.AssignfrmAadharId equals SN.AadarId
                                     join MA in pCtxt.tDrafteFile on Movemnt.DraftId equals MA.DraftId
                                     join EFile in pCtxt.eFiles on MA.ReFileID equals EFile.eFileID into EFileTemp
                                     from Ef in EFileTemp.DefaultIfEmpty()
                                     where (Movemnt.DraftId == model.ID && Movemnt.Noting != "NOTINUSE")
                                     select new HouseComModel
                                     {
                                         ID = Movemnt.ID,
                                         efileID = Movemnt.eFileAttachmentId,
                                         BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                                         EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                                         AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                                         Remarks = Movemnt.Noting,
                                         AssignDateTime = Movemnt.AssignDateTime,
                                         Subject = Ef.eFileSubject,
                                         eFileName = Ef.eFileNumber,
                                         SignPath = (from mc in pCtxt.users where mc.AadarId == Movemnt.AssignfrmAadharId select mc.SignaturePath).FirstOrDefault(),
                                         MarkedAadharId = Movemnt.EmpAadharId,
                                         MarkedName = Movemnt.EmpName,
                                         MarkedDesignation = Movemnt.EmpDesignation,
                                     }).OrderBy(a => a.AssignDateTime).ToList();

                        model.HouseComModel = query.ToList();
                        model.Value = true;
                    }
                }
                else
                {
                    var query = (from Movemnt in pCtxt.FileNotingMovement
                                 join SN in pCtxt.users on Movemnt.AssignfrmAadharId equals SN.AadarId
                                 join MA in pCtxt.tDrafteFile on Movemnt.DraftId equals MA.DraftId
                                 join EFile in pCtxt.eFiles on MA.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 where (Movemnt.DraftId == model.ID && Movemnt.Noting != "NOTINUSE")
                                 select new HouseComModel
                                 {
                                     ID = Movemnt.ID,
                                     efileID = Movemnt.eFileAttachmentId,
                                     BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                                     EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                                     AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                                     Remarks = Movemnt.Noting,
                                     AssignDateTime = Movemnt.AssignDateTime,
                                     Subject = Ef.eFileSubject,
                                     eFileName = Ef.eFileNumber,
                                     SignPath = (from mc in pCtxt.users where mc.AadarId == Movemnt.AssignfrmAadharId select mc.SignaturePath).FirstOrDefault(),
                                     MarkedAadharId = Movemnt.EmpAadharId,
                                     MarkedName = Movemnt.EmpName,
                                     MarkedDesignation = Movemnt.EmpDesignation,
                                 }).OrderBy(a => a.AssignDateTime).ToList();
                    model.HouseComModel = query.ToList();
                    model.Value = true;
                }
            }
            else
            {
                var query = (from Movemnt in pCtxt.FileNotingMovement
                             join SN in pCtxt.users on Movemnt.AssignfrmAadharId equals SN.AadarId
                             join MA in pCtxt.tDrafteFile on Movemnt.DraftId equals MA.DraftId
                             join EFile in pCtxt.eFiles on MA.ReFileID equals EFile.eFileID into EFileTemp
                             from Ef in EFileTemp.DefaultIfEmpty()
                             where (Movemnt.DraftId == model.ID && Movemnt.Noting != "NOTINUSE")
                             select new HouseComModel
                             {
                                 ID = Movemnt.ID,
                                 efileID = Movemnt.eFileAttachmentId,
                                 BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                                 EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                                 AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                                 Remarks = Movemnt.Noting,
                                 AssignDateTime = Movemnt.AssignDateTime,
                                 Subject = Ef.eFileSubject,
                                 eFileName = Ef.eFileNumber,
                                 SignPath = (from mc in pCtxt.users where mc.AadarId == Movemnt.AssignfrmAadharId select mc.SignaturePath).FirstOrDefault(),
                                 MarkedAadharId = Movemnt.EmpAadharId,
                                 MarkedName = Movemnt.EmpName,
                                 MarkedDesignation = Movemnt.EmpDesignation,
                             }).OrderBy(a => a.AssignDateTime).ToList();
                model.HouseComModel = query.ToList();
                model.Value = true;
            }

            return model;
        }


        public static object SendDraftDatabyId(object param)
        {
            try
            {
                eFileAttachment model = param as eFileAttachment;
                eFileContext ctx = new eFileContext();
                model.YearId = (from run in ctx.eFilePaperSno
                                where run.year == DateTime.Now.Year
                                select run.id).FirstOrDefault();
                model.Type = "SendPaperToVS";

                var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.eFileAttachmentId).FirstOrDefault();
                model.PaperRefNo = System.DateTime.Now.Year.ToString() + "/" + Convert.ToString(lastNumber + 1);
                model.ReFileID = 0;
                model.eFileID = 0;
                model.eFileNameId = model.CreatedBy;
                model.ParentId = 0;
                model.SplitFileCount = 0;
                model.ReportLayingHouse = 0;
                model.Approve = 0;
                model.OfficeCode = 0;
                model.CurDNo = 0;
                model.CurYear = System.DateTime.Now.Year;
                model.CurrentBranchId = 0;
                model.ToOfficeCode = 0;
                model.SendDate = System.DateTime.Now;
                model.ReceivedDate = System.DateTime.Now;
                model.ReplyDate = System.DateTime.Now;
                model.AssignDateTime = System.DateTime.Now;
                model.DraftId = model.DraftId;
                model.RevedOption = "H";
                model.ToPaperNature = model.PaperNature;
                model.PType = "O";
                model.PaperStatus = "Y";
                model.eMode = 1;
                model.ItemId = model.ItemTypeName;
                model.ItemNumber = model.ItemNumber;
                model.ReplyRefNo = model.ReplyRefNo;
                model.CommitteeId = model.CommitteeId;
                model.eFilePathWord = model.eFilePathWord;
                //model.CommitteeName = 
                //model.CommitteeName = (from a in ctx.committees where a.CommitteeId == model.CommitteeId select a.CommitteeName).FirstOrDefault();
                //model.DepartmentName = (from a in ctx.departments where a.deptId == model.DeptIds select a.deptname).FirstOrDefault();
                ctx.eFileAttachments.Add(model);
                ctx.SaveChanges();
                eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == model.YearId);
                Sno.runno = lastNumber + 1;
                ctx.SaveChanges();
                var Delete = ctx.tDrafteFile.SingleOrDefault(a => a.ID == model.DraftId);
                ctx.tDrafteFile.Remove(Delete);
                ctx.SaveChanges();
                ctx.Close();
                //SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.ID == model.DraftId);
                //obj.isDraft = false;
                //ctx.SaveChanges();  
                // int Count = (from Item in ctx.eFileAttachments where Item.ReplyRefNo == model.ReplyRefNo select Item).Count();
                //if (Count == model.CountsAPS || model.CountsAPS ==null)
                // {
                //     SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.ID == model.DraftId);
                //     obj.isDraft = false;
                //     ctx.SaveChanges();     
                // }


                return model; //Updated
            }
            catch
            {
                return 0; //Error
            }
        }

        public static object SentToDept(object param)
        {
            try
            {
                eFileAttachment val = param as eFileAttachment;
                eFileAttachment DraftObj = new eFileAttachment();
                eFilePaperSno upsNo = new eFilePaperSno();
                tDrafteFile mdl = param as tDrafteFile;
                FileNotingMovement MoveObj = new FileNotingMovement();
                tDrafteFile_Data mdldata = new tDrafteFile_Data();
                tMovementFile MovementSent = new tMovementFile();

                using (eFileContext ctx = new eFileContext())
                {

                    var Q = (from Efile in ctx.tDrafteFile
                             where Efile.ID == val.eFileAttachmentId
                             select Efile).FirstOrDefault();

                    val.YearId = (from run in ctx.eFilePaperSno
                                  where run.year == DateTime.Now.Year
                                  select run.id).FirstOrDefault();

                    List<eFileAttachment> DraftObjList = new List<eFileAttachment>();

                    List<string> ReffList = new List<string>();
                    tDrafteFile model1 = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);


                    if (Q.TODepts != null && Q.TODepts != "")
                    {
                        string[] values = Q.TODepts.Split(',');
                        tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);
                        for (int i = 0; i < values.Length; i++)
                        {
                            // var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.eFileAttachmentId).FirstOrDefault();
                            var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.PaperRefNo).FirstOrDefault();
                            if (lastNumber == null)
                            {
                                int year = DateTime.Now.Year;
                                lastNumber = "year/0";
                            }

                            string[] parts = lastNumber.Split('/');
                            int.Parse(parts[1]);
                            string ToDeptIds = values[i];
                            //tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);
                            DraftObj.DepartmentName = model.ToAllRecipName;//department names
                            DraftObj.ItemSubject = model.Description;//subject name
                            //DraftObj.CommitteeName="wa";                            
                            DraftObj.DraftId = model.ID;
                            DraftObj.eFileID = model.eFileID;
                            DraftObj.DocumentTypeId = model.DocumentTypeId;
                            DraftObj.eFileName = model.eFileName;
                            DraftObj.Description = model.Description;
                            DraftObj.SendStatus = model.SendStatus;
                            DraftObj.DepartmentId = "HPD0001";
                            DraftObj.Title = model.Title;
                            DraftObj.eFileNameId = model.eFileNameId;
                            DraftObj.ParentId = model.ParentId;
                            DraftObj.SplitFileCount = model.SplitFileCount;
                            DraftObj.ReportLayingHouse = model.ReportLayingHouse;
                            DraftObj.Approve = model.Approve;
                            DraftObj.OfficeCode = model.OfficeCode;
                            DraftObj.PaperNature = model.PaperNature;
                            DraftObj.PaperNatureDays = model.PaperNatureDays;
                            DraftObj.ToDepartmentID = ToDeptIds;
                            DraftObj.TOCCtype = "To";
                            DraftObj.ToMemberId = null;
                            DraftObj.ToOtherId = null;
                            DraftObj.ToOfficeCode = model.ToOfficeCode;
                            DraftObj.PaperStatus = model.PaperStatus;

                            //DraftObj.SendDate = model.SendDate;
                            DraftObj.PType = model.PType;
                            DraftObj.eFilePath = val.eFilePath;
                            DraftObj.AnnexPdfPath = model.eFilePathPDF;
                            DraftObj.ReceivedDate = DateTime.Now;
                            DraftObj.CreatedDate = DateTime.Now;
                            DraftObj.SendDate = DateTime.Now;
                            DraftObj.ReplyDate = DateTime.Now;
                            DraftObj.AssignDateTime = DateTime.Now;
                            DraftObj.ModifiedDate = DateTime.Now;
                            // DraftObj.PaperRefNo = model.PaperRefNo;
                            DraftObj.PaperRefNo = DateTime.Now.Year.ToString() + "/" + Convert.ToString(int.Parse(parts[1]) + 1);
                            DraftObj.ReplyStatus = model.ReplyStatus;

                            //ReffArray
                            ReffList.Add(DraftObj.PaperRefNo);
                            //DraftObj.PaperRefNoList.Add(DraftObj.PaperRefNo);//for paper REff no..
                            DraftObj.PaperRefNoList = ReffList;
                            DraftObj.LinkedRefNo = model.LinkedRefNo;
                            DraftObj.ToPaperNature = model.ToPaperNature;
                            DraftObj.ToPaperNatureDays = model.ToPaperNatureDays;
                            DraftObj.RevedOption = model.RevedOption;
                            DraftObj.ToRecvDetails = model.ToRecvDetails;
                            DraftObj.eMode = model.eMode;
                            DraftObj.PaperTicketValue = model.PaperTicketValue;
                            DraftObj.DispatchtoWhom = model.DispatchtoWhom;
                            DraftObj.eFilePathWord = model.eFilePathDOC;
                            DraftObj.ReFileID = model.ReFileID;
                            //DraftObj.DraftBy = mdl.CurrentEmpAadharID;
                            //DraftObj.DraftDate = DateTime.Now;
                            DraftObj.IsDeleted = model.IsDeleted;
                            DraftObj.CurrentBranchId = model.CurrentBranchId;
                            DraftObj.CurrentEmpAadharID = model.CurrentEmpAadharID;
                            DraftObj.CurDNo = 0;
                            DraftObj.CountsAPS = model.CountAPS;
                            DraftObj.ItemId = model.ItemID;
                            DraftObj.CommitteeId = val.CommitteeId;
                            DraftObj.PaperRefrenceManualy = model.PaperRefrenceManualy;

                            // DraftObj.PaperDraft = model.PaperDraft;
                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;


                            // DraftObj.PaperDraft = model.PaperDraft;

                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;
                            // DraftObj.PaperDraft = model.PaperDraft;

                            ctx.eFileAttachments.Add(DraftObj);
                            ctx.SaveChanges();
                            //SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.ID == val.eFileAttachmentId);
                            //obj.isDraft = false;
                            //ctx.SaveChanges();
                            //eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == val.YearId);
                            //Sno.runno = int.Parse(parts[1]) + 1;
                            //ctx.SaveChanges();
                            //DraftObjList.Add(DraftObj);

                            SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.ID == val.eFileAttachmentId);
                            obj.isDraft = false;

                            ctx.SaveChanges();
                            eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == val.YearId);
                            Sno.runno = int.Parse(parts[1]) + 1;
                            ctx.SaveChanges();
                            DraftObjList.Add(DraftObj);
                            // make  FileAttched_DraftId in mefile  null again
                            var FileAttched_exist = (from eFiles in ctx.eFiles where eFiles.FileAttched_DraftId == val.eFileAttachmentId select eFiles.FileAttched_DraftId.ToString()).FirstOrDefault();
                            if (FileAttched_exist == "" || FileAttched_exist == null)
                            {

                            }
                            else
                            {
                                SBL.DomainModel.Models.eFile.eFile obj1 = ctx.eFiles.Single(m => m.FileAttched_DraftId == val.eFileAttachmentId);
                                obj1.FileAttched_DraftId = null;
                                ctx.SaveChanges();
                            }
                            //  Adde data in filenoting movement file

                            MoveObj.BranchId = model.CurrentBranchId;
                            MoveObj.EmpAadharId = model.CurrentEmpAadharID;
                            MoveObj.Noting = "Approve And Sent";
                            MoveObj.AssignDateTime = DateTime.Now;
                            var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == model.DraftBy select AF).FirstOrDefault();  //ppp
                            var AssgnTo = (from AT in ctx.mStaff where AT.AadharID == model.CurrentEmpAadharID select AT).FirstOrDefault();  //pp
                            MoveObj.EmpName = AssgnTo.StaffName;
                            MoveObj.EmpDesignation = AssgnTo.Designation;
                            MoveObj.AssignfrmAadharId = model.CurrentEmpAadharID;
                            MoveObj.AssignfrmName = AssgnTo.StaffName;
                            MoveObj.AssignfrmDesignation = AssgnTo.Designation;
                            MoveObj.FileId = model.eFileID;
                            MoveObj.DraftId = obj.ID;
                            ctx.FileNotingMovement.Add(MoveObj);
                            ctx.SaveChanges();
                            //  Adde data in  tmovementfile file

                            MovementSent.eFileAttachmentId = DraftObj.eFileAttachmentId;
                            MovementSent.BranchId = model.CurrentBranchId;
                            MovementSent.EmpAadharId = model.CurrentEmpAadharID;

                            MovementSent.EmpName = AssgnTo.StaffName;
                            MovementSent.EmpDesignation = AssgnTo.StaffName;
                            MovementSent.AssignfrmAadharId = model.CurrentEmpAadharID;
                            MovementSent.AssignfrmName = AssgnTo.StaffName;
                            MovementSent.AssignfrmDesignation = AssgnTo.StaffName;
                            ctx.tMovementFile.Add(MovementSent);
                            ctx.SaveChanges();

                            //  ctx.Close();
                        }
                        //// Add data in tdraftfile replicate table///
                        //////// priyanka
                        //mdldata.ID = model.ID;
                        //mdldata.eFileAttachmentId = model.eFileAttachmentId;
                        //mdldata.eFileID = model.eFileID;
                        //mdldata.DocumentTypeId = model.DocumentTypeId;
                        //mdldata.Description = model.Title;
                        //mdldata.CreatedDate = model.CreatedDate;
                        //mdldata.DraftBy = model.DraftBy;
                        //mdldata.DraftDate = model.CreatedDate;
                        //mdldata.SendStatus = model.SendStatus;
                        //mdldata.DepartmentId = model.DepartmentId;
                        //mdldata.Title = model.Title;
                        //mdldata.eFileNameId = model.eFileNameId;
                        //mdldata.eFileName = model.eFileName;
                        //mdldata.Type = model.Type;
                        //mdldata.SplitFileCount = model.SplitFileCount;
                        //mdldata.PaperNature = model.PaperNature;
                        //mdldata.ToDepartmentID = model.ToDepartmentID;
                        //mdldata.PaperStatus = model.PaperStatus; ;
                        //mdldata.SendDate = model.CreatedDate;
                        //mdldata.PType = model.PType;
                        //mdldata.ReceivedDate = model.CreatedDate;
                        //mdldata.ReplyDate = model.CreatedDate;
                        //mdldata.PaperRefNo = model.PaperRefNo;
                        //mdldata.ToPaperNature = model.ToPaperNature;
                        //mdldata.eMode = model.eMode;
                        //mdldata.ReportLayingHouse = model.ReportLayingHouse;
                        //mdldata.Approve = model.Approve;
                        //mdldata.CurrentBranchId = model.CurrentBranchId;
                        ////  mdldata.CurrentBranchId = model.CurrentBranchId;
                        //mdldata.CurrentEmpAadharID = model.CurrentEmpAadharID;
                        //mdldata.AssignDateTime = model.AssignDateTime;
                        //mdldata.eFilePathPDF = model.eFilePathPDF;
                        //mdldata.eFilePath = model.eFilePath;
                        //mdldata.eFilePathDOC = model.eFilePathDOC;
                        //mdldata.eFilePathWord = model.eFilePathWord;
                        //mdldata.TODepts = model.TODepts;
                        //mdldata.CCDepts = model.CCDepts;
                        //mdldata.TOMembers = model.TOMembers;
                        //mdldata.CCMembers = model.CCMembers;
                        //mdldata.ToOthers = model.ToOthers;
                        //mdldata.CCOthers = model.CCOthers;
                        //mdldata.ToAllRecipName = model.ToAllRecipName;
                        //mdldata.ToAllRecipIds = model.ToAllRecipIds;
                        //mdldata.CCAllRecipIds = model.CCAllRecipIds;
                        //mdldata.CCAllRecipName = model.CCAllRecipName;
                        //mdldata.CountAPS = model.CountAPS;
                        //mdldata.ItemID = model.ItemID;
                        //mdldata.ToPaperNature = model.PaperNature;
                        //mdldata.isDraft = model.isDraft;
                        //mdldata.PaperRefrenceManualy = model.PaperRefrenceManualy;
                        //mdldata.PaperDraft = model.PaperDraft;
                        //mdldata.DraftId = model.ID;
                        //ctx.tDrafteFile_Data.Add(mdldata);
                        //ctx.SaveChanges();
                        /////// end data  in tdraftfile replicate table//
                    }
                    if (Q.TOMembers != null && Q.TOMembers != "")
                    {
                        string[] values = Q.TOMembers.Split(',');
                        tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);
                        for (int i = 0; i < values.Length; i++)
                        {
                            var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.PaperRefNo).FirstOrDefault();
                            if (lastNumber == null)
                            {
                                int year = DateTime.Now.Year;
                                lastNumber = "year/0";
                            }
                            string[] parts = lastNumber.Split('/');
                            int.Parse(parts[1]);
                            int? ToMemberIds = int.Parse(values[i]);

                            DraftObj.DepartmentName = model.ToAllRecipName;//department names
                            DraftObj.ItemSubject = model.Description;//subject name
                            DraftObj.DraftId = model.ID;
                            DraftObj.eFileID = model.eFileID;
                            DraftObj.CreatedDate = model.CreatedDate;
                            DraftObj.DocumentTypeId = model.DocumentTypeId;
                            DraftObj.eFileName = model.eFileName;
                            DraftObj.Description = model.Description;
                            DraftObj.SendStatus = model.SendStatus;
                            DraftObj.DepartmentId = "HPD0001";
                            DraftObj.Title = model.Title;
                            DraftObj.eFileNameId = model.eFileNameId;
                            DraftObj.ParentId = model.ParentId;
                            DraftObj.SplitFileCount = model.SplitFileCount;
                            DraftObj.ReportLayingHouse = model.ReportLayingHouse;
                            DraftObj.Approve = model.Approve;
                            DraftObj.OfficeCode = model.OfficeCode;
                            DraftObj.PaperNature = model.PaperNature;
                            DraftObj.PaperNatureDays = model.PaperNatureDays;
                            DraftObj.ToDepartmentID = null;
                            DraftObj.ToMemberId = ToMemberIds;
                            DraftObj.ToOtherId = null;
                            DraftObj.TOCCtype = "To";
                            DraftObj.ToOfficeCode = model.ToOfficeCode;
                            DraftObj.PaperStatus = model.PaperStatus;
                            DraftObj.SendDate = DateTime.Now;
                            //DraftObj.SendDate = model.SendDate;
                            DraftObj.PType = model.PType;
                            DraftObj.eFilePath = val.eFilePath;
                            //DraftObj.ReceivedDate = model.ReceivedDate;
                            DraftObj.ReceivedDate = DateTime.Now;
                            // DraftObj.PaperRefNo = model.PaperRefNo;
                            DraftObj.PaperRefNo = DateTime.Now.Year.ToString() + "/" + Convert.ToString(int.Parse(parts[1]) + 1);

                            //ReffArray
                            ReffList.Add(DraftObj.PaperRefNo);
                            //DraftObj.PaperRefNoList.Add(DraftObj.PaperRefNo);//for paper REff no..
                            DraftObj.PaperRefNoList = ReffList;

                            DraftObj.ReplyStatus = model.ReplyStatus;
                            DraftObj.ReplyDate = DateTime.Now;
                            DraftObj.LinkedRefNo = model.LinkedRefNo;
                            DraftObj.ToPaperNature = model.ToPaperNature;
                            DraftObj.ToPaperNatureDays = model.ToPaperNatureDays;
                            DraftObj.RevedOption = model.RevedOption;
                            DraftObj.ToRecvDetails = model.ToRecvDetails;
                            DraftObj.eMode = model.eMode;
                            DraftObj.PaperTicketValue = model.PaperTicketValue;
                            DraftObj.DispatchtoWhom = model.DispatchtoWhom;
                            DraftObj.eFilePathWord = model.eFilePathDOC;
                            DraftObj.ReFileID = model.ReFileID;
                            //DraftObj.DraftBy = mdl.CurrentEmpAadharID;
                            //DraftObj.AssignDateTime = DateTime.Now;
                            DraftObj.IsDeleted = model.IsDeleted;
                            DraftObj.CurrentBranchId = model.CurrentBranchId;
                            DraftObj.CurrentEmpAadharID = model.CurrentEmpAadharID;
                            DraftObj.CurDNo = 0;
                            DraftObj.CountsAPS = model.CountAPS;
                            DraftObj.ItemId = model.ItemID;
                            DraftObj.ModifiedDate = DateTime.Now;
                            DraftObj.CommitteeId = val.CommitteeId;
                            DraftObj.PaperRefrenceManualy = model.PaperRefrenceManualy;   //jkhj
                            // DraftObj.PaperDraft = model.PaperDraft;
                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;

                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;
                            ctx.eFileAttachments.Add(DraftObj);
                            ctx.SaveChanges();

                            SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.eFileAttachmentId == val.eFileAttachmentId);
                            obj.isDraft = false;
                            ctx.SaveChanges();
                            var FileAttched_exist = (from eFiles in ctx.eFiles where eFiles.FileAttched_DraftId == val.eFileAttachmentId select eFiles.FileAttched_DraftId.ToString()).FirstOrDefault();
                            if (FileAttched_exist == "" || FileAttched_exist == null)
                            {

                            }
                            else
                            {
                                SBL.DomainModel.Models.eFile.eFile obj1 = ctx.eFiles.Single(m => m.FileAttched_DraftId == val.eFileAttachmentId);
                                obj1.FileAttched_DraftId = null;
                                ctx.SaveChanges();
                            }
                            eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == val.YearId);
                            Sno.runno = int.Parse(parts[1]) + 1;
                            ctx.SaveChanges();
                            DraftObjList.Add(DraftObj);
                            MoveObj.BranchId = model.CurrentBranchId;
                            MoveObj.EmpAadharId = model.CurrentEmpAadharID;
                            MoveObj.Noting = "Approve And Sent";
                            MoveObj.AssignDateTime = DateTime.Now;
                            var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == model.DraftBy select AF).FirstOrDefault();  //ppp
                            var AssgnTo = (from AT in ctx.mStaff where AT.AadharID == model.CurrentEmpAadharID select AT).FirstOrDefault();  //pp
                            MoveObj.EmpName = AssgnTo.StaffName;
                            MoveObj.EmpDesignation = AssgnTo.Designation;
                            MoveObj.AssignfrmAadharId = model.DraftBy;
                            MoveObj.AssignfrmName = AssgnTo.StaffName;
                            MoveObj.AssignfrmDesignation = AssgnTo.Designation;
                            MoveObj.FileId = model.eFileID;
                            MoveObj.DraftId = obj.ID;
                            ctx.FileNotingMovement.Add(MoveObj);
                            ctx.SaveChanges();
                            ctx.Close();
                        }
                        //// Add data in tdraftfile replicate table///
                        //mdldata.ID = model.ID;
                        //mdldata.eFileAttachmentId = model.eFileAttachmentId;
                        //mdldata.eFileID = model.eFileID;
                        //mdldata.DocumentTypeId = model.DocumentTypeId;
                        //mdldata.Description = model.Title;
                        //mdldata.CreatedDate = model.CreatedDate;
                        //mdldata.DraftBy = model.DraftBy;
                        //mdldata.DraftDate = model.CreatedDate;
                        //mdldata.SendStatus = model.SendStatus;
                        //mdldata.DepartmentId = model.DepartmentId;
                        //mdldata.Title = model.Title;
                        //mdldata.eFileNameId = model.eFileNameId;
                        //mdldata.eFileName = model.eFileName;
                        //mdldata.Type = model.Type;
                        //mdldata.SplitFileCount = model.SplitFileCount;
                        //mdldata.PaperNature = model.PaperNature;
                        //mdldata.ToDepartmentID = model.ToDepartmentID;
                        //mdldata.PaperStatus = model.PaperStatus; ;
                        //mdldata.SendDate = model.CreatedDate;
                        //mdldata.PType = model.PType;
                        //mdldata.ReceivedDate = model.CreatedDate;
                        //mdldata.ReplyDate = model.CreatedDate;
                        //mdldata.PaperRefNo = model.PaperRefNo;
                        //mdldata.ToPaperNature = model.ToPaperNature;
                        //mdldata.eMode = model.eMode;
                        //mdldata.ReportLayingHouse = model.ReportLayingHouse;
                        //mdldata.Approve = model.Approve;
                        //mdldata.CurrentBranchId = model.CurrentBranchId;
                        ////  mdldata.CurrentBranchId = model.CurrentBranchId;
                        //mdldata.CurrentEmpAadharID = model.CurrentEmpAadharID;
                        //mdldata.AssignDateTime = model.AssignDateTime;
                        //mdldata.eFilePathPDF = model.eFilePathPDF;
                        //mdldata.eFilePath = model.eFilePath;
                        //mdldata.eFilePathDOC = model.eFilePathDOC;
                        //mdldata.eFilePathWord = model.eFilePathWord;
                        //mdldata.TODepts = model.TODepts;
                        //mdldata.CCDepts = model.CCDepts;
                        //mdldata.TOMembers = model.TOMembers;
                        //mdldata.CCMembers = model.CCMembers;
                        //mdldata.ToOthers = model.ToOthers;
                        //mdldata.CCOthers = model.CCOthers;
                        //mdldata.ToAllRecipName = model.ToAllRecipName;
                        //mdldata.ToAllRecipIds = model.ToAllRecipIds;
                        //mdldata.CCAllRecipIds = model.CCAllRecipIds;
                        //mdldata.CCAllRecipName = model.CCAllRecipName;
                        //mdldata.CountAPS = model.CountAPS;
                        //mdldata.ItemID = model.ItemID;
                        //mdldata.ToPaperNature = model.PaperNature;
                        //mdldata.isDraft = model.isDraft;
                        //mdldata.PaperRefrenceManualy = model.PaperRefrenceManualy;
                        //mdldata.PaperDraft = model.PaperDraft;
                        //mdldata.DraftId = model.ID;
                        //ctx.tDrafteFile_Data.Add(mdldata);
                        //ctx.SaveChanges();
                        /////// end data  in tdraftfile replicate table//

                    }

                    if (Q.ToOthers != null && Q.ToOthers != "")
                    {
                        string[] values = Q.ToOthers.Split(',');
                        tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);
                        for (int i = 0; i < values.Length; i++)
                        {
                            var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.PaperRefNo).FirstOrDefault();
                            if (lastNumber == null)
                            {
                                int year = DateTime.Now.Year;
                                lastNumber = "year/0";
                            }
                            string[] parts = lastNumber.Split('/');
                            int.Parse(parts[1]);
                            int? ToOthers = int.Parse(values[i]);

                            DraftObj.DepartmentName = model.ToAllRecipName;//department names
                            DraftObj.ItemSubject = model.Description;//subject name
                            DraftObj.DraftId = model.ID;
                            DraftObj.eFileID = model.eFileID;
                            DraftObj.CreatedDate = model.CreatedDate;
                            DraftObj.DocumentTypeId = model.DocumentTypeId;
                            DraftObj.eFileName = model.eFileName;
                            DraftObj.Description = model.Description;
                            DraftObj.SendStatus = model.SendStatus;
                            DraftObj.DepartmentId = "HPD0001";
                            DraftObj.Title = model.Title;
                            DraftObj.eFileNameId = model.eFileNameId;
                            DraftObj.ParentId = model.ParentId;
                            DraftObj.SplitFileCount = model.SplitFileCount;
                            DraftObj.ReportLayingHouse = model.ReportLayingHouse;
                            DraftObj.Approve = model.Approve;
                            DraftObj.OfficeCode = model.OfficeCode;
                            DraftObj.PaperNature = model.PaperNature;
                            DraftObj.PaperNatureDays = model.PaperNatureDays;
                            DraftObj.ToDepartmentID = null;
                            DraftObj.ToMemberId = null;
                            DraftObj.ToOtherId = ToOthers;
                            DraftObj.TOCCtype = "To";
                            DraftObj.ToOfficeCode = model.ToOfficeCode;
                            DraftObj.PaperStatus = model.PaperStatus;
                            DraftObj.SendDate = DateTime.Now;
                            //DraftObj.SendDate = model.SendDate;
                            DraftObj.PType = model.PType;
                            DraftObj.eFilePath = val.eFilePath;
                            //DraftObj.ReceivedDate = model.ReceivedDate;
                            DraftObj.ReceivedDate = DateTime.Now;
                            // DraftObj.PaperRefNo = model.PaperRefNo;
                            DraftObj.PaperRefNo = DateTime.Now.Year.ToString() + "/" + Convert.ToString(int.Parse(parts[1]) + 1);

                            //ReffArray
                            ReffList.Add(DraftObj.PaperRefNo);
                            //DraftObj.PaperRefNoList.Add(DraftObj.PaperRefNo);//for paper REff no..
                            DraftObj.PaperRefNoList = ReffList;
                            DraftObj.ReplyStatus = model.ReplyStatus;
                            DraftObj.ReplyDate = DateTime.Now;
                            DraftObj.LinkedRefNo = model.LinkedRefNo;
                            DraftObj.ToPaperNature = model.ToPaperNature;
                            DraftObj.ToPaperNatureDays = model.ToPaperNatureDays;
                            DraftObj.RevedOption = model.RevedOption;
                            DraftObj.ToRecvDetails = model.ToRecvDetails;
                            DraftObj.eMode = model.eMode;
                            DraftObj.PaperTicketValue = model.PaperTicketValue;
                            DraftObj.DispatchtoWhom = model.DispatchtoWhom;
                            DraftObj.eFilePathWord = model.eFilePathDOC;
                            DraftObj.ReFileID = model.ReFileID;
                            //DraftObj.DraftBy = mdl.CurrentEmpAadharID;
                            //DraftObj.AssignDateTime = DateTime.Now;
                            DraftObj.IsDeleted = model.IsDeleted;
                            DraftObj.CurrentBranchId = model.CurrentBranchId;
                            DraftObj.CurrentEmpAadharID = model.CurrentEmpAadharID;
                            DraftObj.CurDNo = 0;
                            DraftObj.ModifiedDate = DateTime.Now;
                            DraftObj.CountsAPS = model.CountAPS;
                            DraftObj.ItemId = model.ItemID;
                            DraftObj.CommitteeId = val.CommitteeId;
                            DraftObj.PaperRefrenceManualy = model.PaperRefrenceManualy;
                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;
                            //  DraftObj.PaperDraft = model.PaperDraft;
                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;

                            ctx.eFileAttachments.Add(DraftObj);
                            ctx.SaveChanges();

                            SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.eFileAttachmentId == val.eFileAttachmentId);
                            obj.isDraft = false;
                            ctx.SaveChanges();
                            var FileAttched_exist = (from eFiles in ctx.eFiles where eFiles.FileAttched_DraftId == val.eFileAttachmentId select eFiles.FileAttched_DraftId.ToString()).FirstOrDefault();
                            if (FileAttched_exist == "" || FileAttched_exist == null)
                            {

                            }
                            else
                            {
                                SBL.DomainModel.Models.eFile.eFile obj1 = ctx.eFiles.Single(m => m.FileAttched_DraftId == val.eFileAttachmentId);
                                obj1.FileAttched_DraftId = null;
                                ctx.SaveChanges();
                            }
                            eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == val.YearId);
                            Sno.runno = int.Parse(parts[1]) + 1;
                            ctx.SaveChanges();
                            DraftObjList.Add(DraftObj);
                            MoveObj.BranchId = model.CurrentBranchId;
                            MoveObj.EmpAadharId = model.CurrentEmpAadharID;
                            MoveObj.Noting = "Approve And Sent";
                            MoveObj.AssignDateTime = DateTime.Now;
                            var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == model.DraftBy select AF).FirstOrDefault();  //ppp
                            var AssgnTo = (from AT in ctx.mStaff where AT.AadharID == model.CurrentEmpAadharID select AT).FirstOrDefault();  //pp
                            MoveObj.EmpName = AssgnTo.StaffName;
                            MoveObj.EmpDesignation = AssgnTo.Designation;
                            MoveObj.AssignfrmAadharId = model.DraftBy;
                            MoveObj.AssignfrmName = AssgnTo.StaffName;
                            MoveObj.AssignfrmDesignation = AssgnTo.Designation;
                            MoveObj.FileId = model.eFileID;
                            MoveObj.DraftId = obj.ID;
                            ctx.FileNotingMovement.Add(MoveObj);
                            ctx.SaveChanges();
                            ctx.Close();
                        }
                        //// Add data in tdraftfile replicate table///
                        //mdldata.ID = model.ID;
                        //mdldata.eFileAttachmentId = model.eFileAttachmentId;
                        //mdldata.eFileID = model.eFileID;
                        //mdldata.DocumentTypeId = model.DocumentTypeId;
                        //mdldata.Description = model.Title;
                        //mdldata.CreatedDate = model.CreatedDate;
                        //mdldata.DraftBy = model.DraftBy;
                        //mdldata.DraftDate = model.CreatedDate;
                        //mdldata.SendStatus = model.SendStatus;
                        //mdldata.DepartmentId = model.DepartmentId;
                        //mdldata.Title = model.Title;
                        //mdldata.eFileNameId = model.eFileNameId;
                        //mdldata.eFileName = model.eFileName;
                        //mdldata.Type = model.Type;
                        //mdldata.SplitFileCount = model.SplitFileCount;
                        //mdldata.PaperNature = model.PaperNature;
                        //mdldata.ToDepartmentID = model.ToDepartmentID;
                        //mdldata.PaperStatus = model.PaperStatus; ;
                        //mdldata.SendDate = model.CreatedDate;
                        //mdldata.PType = model.PType;
                        //mdldata.ReceivedDate = model.CreatedDate;
                        //mdldata.ReplyDate = model.CreatedDate;
                        //mdldata.PaperRefNo = model.PaperRefNo;
                        //mdldata.ToPaperNature = model.ToPaperNature;
                        //mdldata.eMode = model.eMode;
                        //mdldata.ReportLayingHouse = model.ReportLayingHouse;
                        //mdldata.Approve = model.Approve;
                        //mdldata.CurrentBranchId = model.CurrentBranchId;
                        ////  mdldata.CurrentBranchId = model.CurrentBranchId;
                        //mdldata.CurrentEmpAadharID = model.CurrentEmpAadharID;
                        //mdldata.AssignDateTime = model.AssignDateTime;
                        //mdldata.eFilePathPDF = model.eFilePathPDF;
                        //mdldata.eFilePath = model.eFilePath;
                        //mdldata.eFilePathDOC = model.eFilePathDOC;
                        //mdldata.eFilePathWord = model.eFilePathWord;
                        //mdldata.TODepts = model.TODepts;
                        //mdldata.CCDepts = model.CCDepts;
                        //mdldata.TOMembers = model.TOMembers;
                        //mdldata.CCMembers = model.CCMembers;
                        //mdldata.ToOthers = model.ToOthers;
                        //mdldata.CCOthers = model.CCOthers;
                        //mdldata.ToAllRecipName = model.ToAllRecipName;
                        //mdldata.ToAllRecipIds = model.ToAllRecipIds;
                        //mdldata.CCAllRecipIds = model.CCAllRecipIds;
                        //mdldata.CCAllRecipName = model.CCAllRecipName;
                        //mdldata.CountAPS = model.CountAPS;
                        //mdldata.ItemID = model.ItemID;
                        //mdldata.ToPaperNature = model.PaperNature;
                        //mdldata.isDraft = model.isDraft;
                        //mdldata.PaperRefrenceManualy = model.PaperRefrenceManualy;
                        //mdldata.PaperDraft = model.PaperDraft;
                        //mdldata.DraftId = model.ID;
                        //ctx.tDrafteFile_Data.Add(mdldata);
                        //ctx.SaveChanges();

                        /////// end data  in tdraftfile replicate table//
                    }

                    if (Q.CCDepts != null && Q.CCDepts != "")
                    {
                        string[] values = Q.CCDepts.Split(',');
                        tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);
                        for (int i = 0; i < values.Length; i++)
                        {
                            var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.PaperRefNo).FirstOrDefault();
                            if (lastNumber == null)
                            {
                                int year = DateTime.Now.Year;
                                lastNumber = "year/0";
                            }
                            string[] parts = lastNumber.Split('/');
                            int.Parse(parts[1]);
                            string CCDepts = values[i];
                            //tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);
                            DraftObj.DepartmentName = model.ToAllRecipName;//department names
                            DraftObj.ItemSubject = model.Description;//subject name
                            DraftObj.DraftId = model.ID;
                            DraftObj.eFileID = model.eFileID;
                            DraftObj.CreatedDate = model.CreatedDate;
                            DraftObj.DocumentTypeId = model.DocumentTypeId;
                            DraftObj.eFileName = model.eFileName;
                            DraftObj.Description = model.Description;
                            DraftObj.SendStatus = model.SendStatus;
                            DraftObj.DepartmentId = "HPD0001";
                            DraftObj.Title = model.Title;
                            DraftObj.eFileNameId = model.eFileNameId;
                            DraftObj.ParentId = model.ParentId;
                            DraftObj.SplitFileCount = model.SplitFileCount;
                            DraftObj.ReportLayingHouse = model.ReportLayingHouse;
                            DraftObj.Approve = model.Approve;
                            DraftObj.OfficeCode = model.OfficeCode;
                            DraftObj.PaperNature = model.PaperNature;
                            DraftObj.PaperNatureDays = model.PaperNatureDays;
                            DraftObj.ToDepartmentID = CCDepts;
                            DraftObj.ToMemberId = null;
                            DraftObj.ToOtherId = null;
                            DraftObj.TOCCtype = "Cc";
                            DraftObj.ToOfficeCode = model.ToOfficeCode;
                            DraftObj.PaperStatus = model.PaperStatus;
                            DraftObj.SendDate = DateTime.Now;
                            //DraftObj.SendDate = model.SendDate;
                            DraftObj.PType = model.PType;
                            DraftObj.eFilePath = val.eFilePath;
                            //DraftObj.ReceivedDate = model.ReceivedDate;
                            DraftObj.ReceivedDate = DateTime.Now;
                            // DraftObj.PaperRefNo = model.PaperRefNo;
                            DraftObj.PaperRefNo = DateTime.Now.Year.ToString() + "/" + Convert.ToString(int.Parse(parts[1]) + 1);

                            //ReffArray
                            ReffList.Add(DraftObj.PaperRefNo);
                            //DraftObj.PaperRefNoList.Add(DraftObj.PaperRefNo);//for paper REff no..
                            DraftObj.PaperRefNoList = ReffList;

                            DraftObj.ReplyStatus = model.ReplyStatus;
                            DraftObj.ReplyDate = DateTime.Now;
                            DraftObj.LinkedRefNo = model.LinkedRefNo;
                            DraftObj.ToPaperNature = model.ToPaperNature;
                            DraftObj.ToPaperNatureDays = model.ToPaperNatureDays;
                            DraftObj.RevedOption = model.RevedOption;
                            DraftObj.ToRecvDetails = model.ToRecvDetails;
                            DraftObj.eMode = model.eMode;
                            DraftObj.PaperTicketValue = model.PaperTicketValue;
                            DraftObj.DispatchtoWhom = model.DispatchtoWhom;
                            DraftObj.eFilePathWord = model.eFilePathDOC;
                            DraftObj.ReFileID = model.ReFileID;
                            //DraftObj.DraftBy = mdl.CurrentEmpAadharID;
                            //DraftObj.AssignDateTime = DateTime.Now;
                            DraftObj.IsDeleted = model.IsDeleted;
                            DraftObj.CurrentBranchId = model.CurrentBranchId;
                            DraftObj.CurrentEmpAadharID = model.CurrentEmpAadharID;
                            DraftObj.CurDNo = 0;
                            DraftObj.ModifiedDate = DateTime.Now;
                            DraftObj.CountsAPS = model.CountAPS;
                            DraftObj.ItemId = model.ItemID;
                            DraftObj.CommitteeId = val.CommitteeId;
                            DraftObj.PaperRefrenceManualy = model.PaperRefrenceManualy;
                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;
                            //DraftObj.PaperDraft = model.PaperDraft;
                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;

                            ctx.eFileAttachments.Add(DraftObj);
                            ctx.SaveChanges();

                            SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.eFileAttachmentId == val.eFileAttachmentId);
                            obj.isDraft = false;
                            ctx.SaveChanges();
                            var FileAttched_exist = (from eFiles in ctx.eFiles where eFiles.FileAttched_DraftId == val.eFileAttachmentId select eFiles.FileAttched_DraftId.ToString()).FirstOrDefault();
                            if (FileAttched_exist == "" || FileAttched_exist == null)
                            {

                            }
                            else
                            {
                                SBL.DomainModel.Models.eFile.eFile obj1 = ctx.eFiles.Single(m => m.FileAttched_DraftId == val.eFileAttachmentId);
                                obj1.FileAttched_DraftId = null;
                                ctx.SaveChanges();
                            }
                            eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == val.YearId);
                            Sno.runno = int.Parse(parts[1]) + 1;
                            ctx.SaveChanges();
                            DraftObjList.Add(DraftObj);
                            MoveObj.BranchId = model.CurrentBranchId;
                            MoveObj.EmpAadharId = model.CurrentEmpAadharID;
                            MoveObj.Noting = "Approve And Sent";
                            MoveObj.AssignDateTime = DateTime.Now;
                            var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == model.DraftBy select AF).FirstOrDefault();  //ppp
                            var AssgnTo = (from AT in ctx.mStaff where AT.AadharID == model.CurrentEmpAadharID select AT).FirstOrDefault();  //pp
                            MoveObj.EmpName = AssgnTo.StaffName;
                            MoveObj.EmpDesignation = AssgnTo.Designation;
                            MoveObj.AssignfrmAadharId = model.DraftBy;
                            MoveObj.AssignfrmName = AssgnTo.StaffName;
                            MoveObj.AssignfrmDesignation = AssgnTo.Designation;
                            MoveObj.FileId = model.eFileID;
                            MoveObj.DraftId = obj.ID;
                            DraftObj.PaperRefrenceManualy = model.PaperRefrenceManualy;
                            //  DraftObj.PaperDraft = model.PaperDraft;
                            ctx.FileNotingMovement.Add(MoveObj);
                            ctx.SaveChanges();
                            ctx.Close();
                        }

                    }
                    if (Q.CCMembers != null && Q.CCMembers != "")
                    {
                        string[] values = Q.CCMembers.Split(',');
                        tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);
                        for (int i = 0; i < values.Length; i++)
                        {
                            var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.PaperRefNo).FirstOrDefault();
                            if (lastNumber == null)
                            {
                                int year = DateTime.Now.Year;
                                lastNumber = "year/0";
                            }
                            string[] parts = lastNumber.Split('/');
                            int.Parse(parts[1]);
                            int? CCMembrs = int.Parse(values[i]);

                            DraftObj.DepartmentName = model.ToAllRecipName;//department names
                            DraftObj.ItemSubject = model.Description;//subject name
                            DraftObj.DraftId = model.ID;
                            DraftObj.eFileID = model.eFileID;
                            DraftObj.CreatedDate = model.CreatedDate;
                            DraftObj.DocumentTypeId = model.DocumentTypeId;
                            DraftObj.eFileName = model.eFileName;
                            DraftObj.Description = model.Description;
                            DraftObj.SendStatus = model.SendStatus;
                            DraftObj.DepartmentId = "HPD0001";
                            DraftObj.Title = model.Title;
                            DraftObj.eFileNameId = model.eFileNameId;
                            DraftObj.ParentId = model.ParentId;
                            DraftObj.SplitFileCount = model.SplitFileCount;
                            DraftObj.ReportLayingHouse = model.ReportLayingHouse;
                            DraftObj.Approve = model.Approve;
                            DraftObj.OfficeCode = model.OfficeCode;
                            DraftObj.PaperNature = model.PaperNature;
                            DraftObj.PaperNatureDays = model.PaperNatureDays;
                            DraftObj.ToDepartmentID = null;
                            DraftObj.ToMemberId = CCMembrs;
                            DraftObj.ToOtherId = null;
                            DraftObj.TOCCtype = "Cc";
                            DraftObj.ToOfficeCode = model.ToOfficeCode;
                            DraftObj.PaperStatus = model.PaperStatus;
                            DraftObj.SendDate = DateTime.Now;
                            //DraftObj.SendDate = model.SendDate;
                            DraftObj.PType = model.PType;
                            DraftObj.eFilePath = val.eFilePath;
                            //DraftObj.ReceivedDate = model.ReceivedDate;
                            DraftObj.ReceivedDate = DateTime.Now;
                            // DraftObj.PaperRefNo = model.PaperRefNo;
                            DraftObj.PaperRefNo = DateTime.Now.Year.ToString() + "/" + Convert.ToString(int.Parse(parts[1]) + 1);

                            //ReffArray
                            ReffList.Add(DraftObj.PaperRefNo);
                            //DraftObj.PaperRefNoList.Add(DraftObj.PaperRefNo);//for paper REff no..
                            DraftObj.PaperRefNoList = ReffList;

                            DraftObj.ReplyStatus = model.ReplyStatus;
                            DraftObj.ReplyDate = DateTime.Now;
                            DraftObj.LinkedRefNo = model.LinkedRefNo;
                            DraftObj.ToPaperNature = model.ToPaperNature;
                            DraftObj.ToPaperNatureDays = model.ToPaperNatureDays;
                            DraftObj.RevedOption = model.RevedOption;
                            DraftObj.ToRecvDetails = model.ToRecvDetails;
                            DraftObj.eMode = model.eMode;
                            DraftObj.PaperTicketValue = model.PaperTicketValue;
                            DraftObj.DispatchtoWhom = model.DispatchtoWhom;
                            DraftObj.eFilePathWord = model.eFilePathDOC;
                            DraftObj.ReFileID = model.ReFileID;
                            //DraftObj.DraftBy = mdl.CurrentEmpAadharID;
                            // DraftObj.AssignDateTime = DateTime.Now;
                            DraftObj.IsDeleted = model.IsDeleted;
                            DraftObj.CurrentBranchId = model.CurrentBranchId;
                            DraftObj.CurrentEmpAadharID = model.CurrentEmpAadharID;
                            DraftObj.CurDNo = model.eFileID;
                            DraftObj.ModifiedDate = DateTime.Now;
                            DraftObj.CountsAPS = model.CountAPS;
                            DraftObj.ItemId = model.ItemID;
                            DraftObj.CommitteeId = val.CommitteeId;
                            DraftObj.PaperRefrenceManualy = model.PaperRefrenceManualy;

                            // DraftObj.PaperDraft = model.PaperDraft;
                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;


                            // DraftObj.PaperDraft = model.PaperDraft;

                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;
                            // DraftObj.PaperDraft = model.PaperDraft;

                            ctx.eFileAttachments.Add(DraftObj);
                            ctx.SaveChanges();

                            SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.eFileAttachmentId == val.eFileAttachmentId);
                            obj.isDraft = false;
                            ctx.SaveChanges();
                            var FileAttched_exist = (from eFiles in ctx.eFiles where eFiles.FileAttched_DraftId == val.eFileAttachmentId select eFiles.FileAttched_DraftId.ToString()).FirstOrDefault();
                            if (FileAttched_exist == "" || FileAttched_exist == null)
                            {

                            }
                            else
                            {
                                SBL.DomainModel.Models.eFile.eFile obj1 = ctx.eFiles.Single(m => m.FileAttched_DraftId == val.eFileAttachmentId);
                                obj1.FileAttched_DraftId = null;
                                ctx.SaveChanges();
                            }
                            eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == val.YearId);
                            Sno.runno = int.Parse(parts[1]) + 1;
                            ctx.SaveChanges();
                            DraftObjList.Add(DraftObj);
                            MoveObj.BranchId = model.CurrentBranchId;
                            MoveObj.EmpAadharId = model.CurrentEmpAadharID;
                            MoveObj.Noting = "Approve And Sent";
                            MoveObj.AssignDateTime = DateTime.Now;
                            var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == model.DraftBy select AF).FirstOrDefault();  //ppp
                            var AssgnTo = (from AT in ctx.mStaff where AT.AadharID == model.CurrentEmpAadharID select AT).FirstOrDefault();  //pp
                            MoveObj.EmpName = AssgnTo.StaffName;
                            MoveObj.EmpDesignation = AssgnTo.Designation;
                            MoveObj.AssignfrmAadharId = model.DraftBy;
                            MoveObj.AssignfrmName = AssgnTo.StaffName;
                            MoveObj.AssignfrmDesignation = AssgnTo.Designation;
                            MoveObj.FileId = model.eFileID;
                            MoveObj.DraftId = obj.ID;
                            ctx.FileNotingMovement.Add(MoveObj);
                            ctx.SaveChanges();
                            ctx.Close();
                        }

                    }

                    if (Q.CCOthers != null && Q.CCOthers != "")
                    {
                        string[] values = Q.CCOthers.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.PaperRefNo).FirstOrDefault();
                            if (lastNumber == null)
                            {
                                int year = DateTime.Now.Year;
                                lastNumber = "year/0";
                            }
                            string[] parts = lastNumber.Split('/');
                            int.Parse(parts[1]);
                            int? CCOtherIds = int.Parse(values[i]);
                            tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == val.eFileAttachmentId);
                            DraftObj.DepartmentName = model.ToAllRecipName;//department names
                            DraftObj.ItemSubject = model.Description;//subject name
                            DraftObj.DraftId = model.ID;
                            DraftObj.eFileID = model.eFileID;
                            DraftObj.CreatedDate = model.CreatedDate;
                            DraftObj.DocumentTypeId = model.DocumentTypeId;
                            DraftObj.eFileName = model.eFileName;
                            DraftObj.Description = model.Description;
                            DraftObj.SendStatus = model.SendStatus;
                            DraftObj.DepartmentId = "HPD0001";
                            DraftObj.Title = model.Title;
                            DraftObj.eFileNameId = model.eFileNameId;
                            DraftObj.ParentId = model.ParentId;
                            DraftObj.SplitFileCount = model.SplitFileCount;
                            DraftObj.ReportLayingHouse = model.ReportLayingHouse;
                            DraftObj.Approve = model.Approve;
                            DraftObj.OfficeCode = model.OfficeCode;
                            DraftObj.PaperNature = model.PaperNature;
                            DraftObj.PaperNatureDays = model.PaperNatureDays;
                            DraftObj.ToDepartmentID = null;
                            DraftObj.ToMemberId = null;
                            DraftObj.ToOtherId = CCOtherIds;
                            DraftObj.TOCCtype = "Cc";
                            DraftObj.ToOfficeCode = model.ToOfficeCode;
                            DraftObj.PaperStatus = model.PaperStatus;
                            DraftObj.SendDate = DateTime.Now;
                            //DraftObj.SendDate = model.SendDate;
                            DraftObj.PType = model.PType;
                            DraftObj.eFilePath = val.eFilePath;//model.eFilePathPDF;
                            //DraftObj.ReceivedDate = model.ReceivedDate;
                            DraftObj.ReceivedDate = DateTime.Now;
                            // DraftObj.PaperRefNo = model.PaperRefNo;
                            DraftObj.PaperRefNo = DateTime.Now.Year.ToString() + "/" + Convert.ToString(int.Parse(parts[1]) + 1);

                            //ReffArray
                            ReffList.Add(DraftObj.PaperRefNo);
                            //DraftObj.PaperRefNoList.Add(DraftObj.PaperRefNo);//for paper REff no..
                            DraftObj.PaperRefNoList = ReffList;

                            DraftObj.ReplyStatus = model.ReplyStatus;
                            DraftObj.ReplyDate = DateTime.Now;
                            DraftObj.LinkedRefNo = model.LinkedRefNo;
                            DraftObj.ToPaperNature = model.ToPaperNature;
                            DraftObj.ToPaperNatureDays = model.ToPaperNatureDays;
                            DraftObj.RevedOption = model.RevedOption;
                            DraftObj.ToRecvDetails = model.ToRecvDetails;
                            DraftObj.eMode = model.eMode;
                            DraftObj.PaperTicketValue = model.PaperTicketValue;
                            DraftObj.DispatchtoWhom = model.DispatchtoWhom;
                            DraftObj.eFilePathWord = model.eFilePathDOC;
                            DraftObj.ReFileID = model.ReFileID;
                            //DraftObj.DraftBy = mdl.CurrentEmpAadharID;
                            //DraftObj.AssignDateTime = DateTime.Now;
                            DraftObj.IsDeleted = model.IsDeleted;
                            DraftObj.CurrentBranchId = model.CurrentBranchId;
                            DraftObj.CurrentEmpAadharID = model.CurrentEmpAadharID;
                            DraftObj.CurDNo = 0;
                            DraftObj.ModifiedDate = DateTime.Now;
                            DraftObj.CountsAPS = model.CountAPS;
                            DraftObj.ItemId = model.ItemID;
                            DraftObj.CommitteeId = val.CommitteeId;

                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;



                            DraftObj.IpAddress = val.IpAddress;
                            DraftObj.MacAddress = val.MacAddress;

                            ctx.eFileAttachments.Add(DraftObj);
                            ctx.SaveChanges();

                            SBL.DomainModel.Models.eFile.tDrafteFile obj = ctx.tDrafteFile.Single(m => m.eFileAttachmentId == val.eFileAttachmentId);
                            obj.isDraft = false;
                            ctx.SaveChanges();
                            var FileAttched_exist = (from eFiles in ctx.eFiles where eFiles.FileAttched_DraftId == val.eFileAttachmentId select eFiles.FileAttched_DraftId.ToString()).FirstOrDefault();
                            if (FileAttched_exist == "" || FileAttched_exist == null)
                            {

                            }
                            else
                            {
                                SBL.DomainModel.Models.eFile.eFile obj1 = ctx.eFiles.Single(m => m.FileAttched_DraftId == val.eFileAttachmentId);
                                obj1.FileAttched_DraftId = null;
                                ctx.SaveChanges();
                            }
                            eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == val.YearId);
                            Sno.runno = int.Parse(parts[1]) + 1;
                            ctx.SaveChanges();
                            DraftObjList.Add(DraftObj);
                            MoveObj.BranchId = model.CurrentBranchId;
                            MoveObj.EmpAadharId = model.CurrentEmpAadharID;
                            MoveObj.Noting = "Approve And Sent";
                            MoveObj.AssignDateTime = DateTime.Now;
                            var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == model.DraftBy select AF).FirstOrDefault();  //ppp
                            var AssgnTo = (from AT in ctx.mStaff where AT.AadharID == model.CurrentEmpAadharID select AT).FirstOrDefault();  //pp
                            MoveObj.EmpName = AssgnTo.StaffName;
                            MoveObj.EmpDesignation = AssgnTo.Designation;
                            MoveObj.AssignfrmAadharId = model.DraftBy;
                            MoveObj.AssignfrmName = AssgnTo.StaffName;
                            MoveObj.AssignfrmDesignation = AssgnTo.Designation;
                            MoveObj.FileId = model.eFileID;
                            MoveObj.DraftId = obj.ID;
                            DraftObj.PaperRefrenceManualy = model.PaperRefrenceManualy;
                            //  DraftObj.PaperDraft = model.PaperDraft;
                            ctx.FileNotingMovement.Add(MoveObj);
                            ctx.SaveChanges();
                            ctx.Close();
                        }
                        //// Add data in tdraftfile replicate table///
                        //mdldata.ID = model.ID;
                        //mdldata.eFileAttachmentId = model.eFileAttachmentId;
                        //mdldata.eFileID = model.eFileID;
                        //mdldata.DocumentTypeId = model.DocumentTypeId;
                        //mdldata.Description = model.Title;
                        //mdldata.CreatedDate = model.CreatedDate;
                        //mdldata.DraftBy = model.DraftBy;
                        //mdldata.DraftDate = model.CreatedDate;
                        //mdldata.SendStatus = model.SendStatus;
                        //mdldata.DepartmentId = model.DepartmentId;
                        //mdldata.Title = model.Title;
                        //mdldata.eFileNameId = model.eFileNameId;
                        //mdldata.eFileName = model.eFileName;
                        //mdldata.Type = model.Type;
                        //mdldata.SplitFileCount = model.SplitFileCount;
                        //mdldata.PaperNature = model.PaperNature;
                        //mdldata.ToDepartmentID = model.ToDepartmentID;
                        //mdldata.PaperStatus = model.PaperStatus; ;
                        //mdldata.SendDate = model.CreatedDate;
                        //mdldata.PType = model.PType;
                        //mdldata.ReceivedDate = model.CreatedDate;
                        //mdldata.ReplyDate = model.CreatedDate;
                        //mdldata.PaperRefNo = model.PaperRefNo;
                        //mdldata.ToPaperNature = model.ToPaperNature;
                        //mdldata.eMode = model.eMode;
                        //mdldata.ReportLayingHouse = model.ReportLayingHouse;
                        //mdldata.Approve = model.Approve;
                        //mdldata.CurrentBranchId = model.CurrentBranchId;
                        ////  mdldata.CurrentBranchId = model.CurrentBranchId;
                        //mdldata.CurrentEmpAadharID = model.CurrentEmpAadharID;
                        //mdldata.AssignDateTime = model.AssignDateTime;
                        //mdldata.eFilePathPDF = model.eFilePathPDF;
                        //mdldata.eFilePath = model.eFilePath;
                        //mdldata.eFilePathDOC = model.eFilePathDOC;
                        //mdldata.eFilePathWord = model.eFilePathWord;
                        //mdldata.TODepts = model.TODepts;
                        //mdldata.CCDepts = model.CCDepts;
                        //mdldata.TOMembers = model.TOMembers;
                        //mdldata.CCMembers = model.CCMembers;
                        //mdldata.ToOthers = model.ToOthers;
                        //mdldata.CCOthers = model.CCOthers;
                        //mdldata.ToAllRecipName = model.ToAllRecipName;
                        //mdldata.ToAllRecipIds = model.ToAllRecipIds;
                        //mdldata.CCAllRecipIds = model.CCAllRecipIds;
                        //mdldata.CCAllRecipName = model.CCAllRecipName;
                        //mdldata.CountAPS = model.CountAPS;
                        //mdldata.ItemID = model.ItemID;
                        //mdldata.ToPaperNature = model.PaperNature;
                        //mdldata.isDraft = model.isDraft;
                        //mdldata.PaperRefrenceManualy = model.PaperRefrenceManualy;
                        //mdldata.PaperDraft = model.PaperDraft;
                        //mdldata.DraftId = model.ID;
                        //ctx.tDrafteFile_Data.Add(mdldata);
                        //ctx.SaveChanges();

                        /////// end data  in tdraftfile replicate table//

                    }
                    // Add data in tdraftfile replicate table///
                    ////// priyanka
                    mdldata.ID = model1.ID;
                    mdldata.eFileAttachmentId = model1.eFileAttachmentId;
                    mdldata.eFileID = model1.eFileID;
                    mdldata.DocumentTypeId = model1.DocumentTypeId;
                    mdldata.Description = model1.Title;
                    mdldata.CreatedDate = model1.CreatedDate;
                    mdldata.DraftBy = model1.DraftBy;
                    mdldata.DraftDate = model1.CreatedDate;
                    mdldata.SendStatus = model1.SendStatus;
                    mdldata.DepartmentId = model1.DepartmentId;
                    mdldata.Title = model1.Title;
                    mdldata.eFileNameId = model1.eFileNameId;
                    mdldata.eFileName = model1.eFileName;
                    mdldata.Type = model1.Type;
                    mdldata.SplitFileCount = model1.SplitFileCount;
                    mdldata.PaperNature = model1.PaperNature;
                    mdldata.ToDepartmentID = model1.ToDepartmentID;
                    mdldata.PaperStatus = model1.PaperStatus; ;
                    mdldata.SendDate = model1.CreatedDate;
                    mdldata.PType = model1.PType;
                    mdldata.ReceivedDate = model1.CreatedDate;
                    mdldata.ReplyDate = model1.CreatedDate;
                    mdldata.PaperRefNo = model1.PaperRefNo;
                    mdldata.ToPaperNature = model1.ToPaperNature;
                    mdldata.eMode = model1.eMode;
                    mdldata.ReportLayingHouse = model1.ReportLayingHouse;
                    mdldata.Approve = model1.Approve;
                    mdldata.CurrentBranchId = model1.CurrentBranchId;
                    //  mdldata.CurrentBranchId = model.CurrentBranchId;
                    mdldata.CurrentEmpAadharID = model1.CurrentEmpAadharID;
                    mdldata.AssignDateTime = model1.AssignDateTime;
                    mdldata.eFilePathPDF = model1.eFilePathPDF;
                    mdldata.eFilePath = model1.eFilePath;
                    mdldata.eFilePathDOC = model1.eFilePathDOC;
                    mdldata.eFilePathWord = model1.eFilePathWord;
                    mdldata.TODepts = model1.TODepts;
                    mdldata.CCDepts = model1.CCDepts;
                    mdldata.TOMembers = model1.TOMembers;
                    mdldata.CCMembers = model1.CCMembers;
                    mdldata.ToOthers = model1.ToOthers;
                    mdldata.CCOthers = model1.CCOthers;
                    mdldata.ToAllRecipName = model1.ToAllRecipName;
                    mdldata.ToAllRecipIds = model1.ToAllRecipIds;
                    mdldata.CCAllRecipIds = model1.CCAllRecipIds;
                    mdldata.CCAllRecipName = model1.CCAllRecipName;
                    mdldata.CountAPS = model1.CountAPS;
                    mdldata.ItemID = model1.ItemID;
                    mdldata.ToPaperNature = model1.PaperNature;
                    mdldata.isDraft = model1.isDraft;
                    mdldata.PaperRefrenceManualy = model1.PaperRefrenceManualy;

                    // mdldata.PaperDraft = model1.PaperDraft;

                    // mdldata.PaperDraft = model1.PaperDraft;

                    mdldata.IpAddress = val.IpAddress;
                    mdldata.MacAddress = val.MacAddress;
                    // mdldata.PaperDraft = model1.PaperDraft;

                    mdldata.DraftId = model1.ID;
                    mdldata.IpAddress = val.IpAddress;
                    mdldata.MacAddress = val.MacAddress;

                    ctx.tDrafteFile_Data.Add(mdldata);
                    ctx.SaveChanges();
                    ///// end data  in tdraftfile replicate table//
                    var Delete = ctx.tDrafteFile.SingleOrDefault(a => a.ID == val.eFileAttachmentId);
                    ctx.tDrafteFile.Remove(Delete);
                    ctx.SaveChanges();
                    ctx.Close();

                    //return ReffList;  //Success
                    return DraftObjList;
                }


            }
            catch
            {
                return 1; //Error
            }
        }

        public static object GetSendList(object param)
        {
            string[] str = param as string[];
            var V = "";
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            string Aadharid = str[2];
            int Year = Convert.ToInt32(str[4]);
            int PaperType = Convert.ToInt32(str[5]);
            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }

            if (Officecode == 0)
            {
                using (eFileContext ctx = new eFileContext())
                {
                    if ((Aadharid == "626192220638") || (Aadharid == "636142914885"))
                    {
                        if (Year == 0)
                        {
                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         from Ef in EFileTemp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID      // on a.ToPaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()
                                         where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false //&& a.ToOfficeCode == Officecode
                                         select new
                                         {
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,
                                             SendDate = a.SendDate,
                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             FileName = Ef.eFileNumber,
                                             DiaryNo = a.DiaryNo,
                                             Annexurepath = a.AnnexPdfPath,
                                             BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                             Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                             CountsAPS = a.CountsAPS,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             ReplyRefNo = a.ReplyRefNo,
                                             CommitteeName = cmf.CommitteeName,
                                             commiteeId = a.CommitteeId,
                                             ComAbbr = cmf.Abbreviation,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks
                                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                            List<eFileAttachment> lst = new List<eFileAttachment>();
                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;
                                mdl.SendDate = item.SendDate;
                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.MainEFileName = item.FileName;
                                mdl.DiaryNo = item.DiaryNo;
                                mdl.BranchName = item.BranchName;
                                mdl.EmpName = item.EmpName;
                                mdl.Desingnation = item.Desingnation;
                                mdl.AbbrevBranch = item.AbbrevBranch;
                                mdl.AnnexPdfPath = item.Annexurepath;
                                mdl.ItemName = item.ItemName;
                                mdl.CountsAPS = item.CountsAPS;
                                mdl.ReplyRefNo = item.ReplyRefNo;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.CommitteeId = item.commiteeId;
                                mdl.ComAbbr = item.ComAbbr;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                lst.Add(mdl);
                            }

                            return lst;
                        }
                        else
                        {
                            DateTime Dt = DateTime.Now.AddYears(-Year);
                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         from Ef in EFileTemp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID // on a.ToPaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()
                                         where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false
                                         && a.SendDate >= Dt && a.SendDate <= DateTime.Now
                                         select new
                                         {
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,
                                             SendDate = a.SendDate,
                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             FileName = Ef.eFileNumber,
                                             DiaryNo = a.DiaryNo,
                                             Annexurepath = a.AnnexPdfPath,
                                             BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                             Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                             CountsAPS = a.CountsAPS,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             ReplyRefNo = a.ReplyRefNo,
                                             CommitteeName = cmf.CommitteeName,
                                             commiteeId = a.CommitteeId,
                                             ComAbbr = cmf.Abbreviation,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks
                                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                            List<eFileAttachment> lst = new List<eFileAttachment>();
                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;
                                mdl.SendDate = item.SendDate;
                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.MainEFileName = item.FileName;
                                mdl.DiaryNo = item.DiaryNo;
                                mdl.BranchName = item.BranchName;
                                mdl.EmpName = item.EmpName;
                                mdl.Desingnation = item.Desingnation;
                                mdl.AbbrevBranch = item.AbbrevBranch;
                                mdl.AnnexPdfPath = item.Annexurepath;
                                mdl.ItemName = item.ItemName;
                                mdl.CountsAPS = item.CountsAPS;
                                mdl.ReplyRefNo = item.ReplyRefNo;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.CommitteeId = item.commiteeId;
                                mdl.ComAbbr = item.ComAbbr;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                lst.Add(mdl);
                            }

                            return lst;
                        }

                    }

                    else
                    {
                        if (str[3] != "") //For Vidhan Sabha Department Users
                        {
                            int CBranchID = Convert.ToInt32(str[3]);
                            List<eFileAttachment> Mainlst = new List<eFileAttachment>();
                            List<eFileAttachment> lst = new List<eFileAttachment>();
                            var s = string.Join(",", ctx.tMovementFile.Where(p => p.EmpAadharId == Aadharid)
                                    .Select(p => p.eFileAttachmentId.ToString()));
                            if (s != "" && PaperType == 2)
                            {
                                List<int> TagIds1 = s.Split(',').Select(int.Parse).ToList();
                                V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID && !TagIds1.Contains(p.eFileAttachmentId))
                                         .Select(p => p.eFileAttachmentId.ToString()));
                                List<int> TagIds = s.Split(',').Select(int.Parse).ToList();
                                var query = (from a in ctx.eFileAttachments
                                             join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
                                             from ps in temp.DefaultIfEmpty()
                                             join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                             from Ef in EFileTemp.DefaultIfEmpty()
                                             join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID  // on a.ToPaperNature equals c.PaperNatureID
                                             join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                             join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                             from cmf in comtemp.DefaultIfEmpty()
                                             where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && a.CurrentBranchId == CBranchID //&& TagIds.Contains(a.eFileAttachmentId)

                                             select new
                                             {
                                                 eFileAttachmentId = a.eFileAttachmentId,
                                                 Department = ps.deptname ?? "OTHER",
                                                 eFileName = a.eFileName,
                                                 PaperNature = a.PaperNature,
                                                 PaperNatureDays = a.PaperNatureDays ?? "0",
                                                 eFilePath = a.eFilePath,
                                                 Title = a.Title,
                                                 Description = a.Description,
                                                 CreateDate = a.CreatedDate,
                                                 PaperStatus = a.PaperStatus,
                                                 eFIleID = a.eFileID,
                                                 PaperRefNo = a.PaperRefNo,
                                                 DepartmentId = a.DepartmentId,
                                                 OfficeCode = a.OfficeCode,
                                                 ToDepartmentID = a.ToDepartmentID,
                                                 ToOfficeCode = a.ToOfficeCode,
                                                 ReplyStatus = a.ReplyStatus,
                                                 ReplyDate = a.ReplyDate,
                                                 ReceivedDate = a.ReceivedDate,
                                                 DocumentTypeId = a.DocumentTypeId,
                                                 SendDate = a.SendDate,
                                                 ToPaperNature = a.ToPaperNature,
                                                 ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                                 LinkedRefNo = a.LinkedRefNo,
                                                 RevedOption = a.RevedOption,
                                                 RecvDetails = a.RecvDetails,
                                                 ToRecvDetails = a.ToRecvDetails,
                                                 eMode = a.eMode,
                                                 RecvdPaperNature = c.PaperNature,
                                                 DocuemntPaperType = d.PaperType,
                                                 PType = a.PType,
                                                 SendStatus = a.SendStatus,
                                                 DeptAbbr = ps.deptabbr,
                                                 eFilePathWord = a.eFilePathWord,
                                                 ReFileID = a.ReFileID,
                                                 FileName = Ef.eFileNumber,
                                                 DiaryNo = a.DiaryNo,
                                                 Annexurepath = a.AnnexPdfPath,
                                                 BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                                 EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                                 CurrentAadharId = a.CurrentEmpAadharID,
                                                 AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                                 Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                                 CountsAPS = a.CountsAPS,
                                                 ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                                 ReplyRefNo = a.ReplyRefNo,
                                                 CommitteeName = cmf.CommitteeName,
                                                 commiteeId = a.CommitteeId,
                                                 comabbr = cmf.Abbreviation,
                                                 IsRejected = a.IsRejected,
                                                 RejectedRemarks = a.RejectedRemarks
                                             }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                                foreach (var item in query)
                                {
                                    eFileAttachment mdl = new eFileAttachment();
                                    mdl.DepartmentName = item.Department;
                                    mdl.eFileAttachmentId = item.eFileAttachmentId;
                                    mdl.eFileName = item.eFileName;
                                    mdl.PaperNature = item.PaperNature;
                                    mdl.PaperNatureDays = item.PaperNatureDays;
                                    mdl.eFilePath = item.eFilePath;
                                    mdl.Title = item.Title;
                                    mdl.Description = item.Description;
                                    mdl.CreatedDate = item.CreateDate;
                                    mdl.PaperStatus = item.PaperStatus;
                                    mdl.eFileID = item.eFIleID;
                                    mdl.PaperRefNo = item.PaperRefNo;
                                    mdl.DepartmentId = item.DepartmentId;
                                    mdl.OfficeCode = item.OfficeCode;
                                    mdl.ToDepartmentID = item.ToDepartmentID;
                                    mdl.ToOfficeCode = item.ToOfficeCode;
                                    mdl.ReplyStatus = item.ReplyStatus;
                                    mdl.ReplyDate = item.ReplyDate;
                                    mdl.ReceivedDate = item.ReceivedDate;
                                    mdl.DocumentTypeId = item.DocumentTypeId;

                                    mdl.ToPaperNature = item.ToPaperNature;
                                    mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                    mdl.LinkedRefNo = item.LinkedRefNo;
                                    mdl.RevedOption = item.RevedOption;
                                    mdl.RecvDetails = item.RecvDetails;
                                    mdl.ToRecvDetails = item.ToRecvDetails;
                                    mdl.eMode = item.eMode;
                                    mdl.RecvdPaperNature = item.RecvdPaperNature;
                                    mdl.SendDate = item.SendDate;
                                    mdl.DocuemntPaperType = item.DocuemntPaperType;
                                    mdl.PType = item.PType;
                                    mdl.SendStatus = item.SendStatus;
                                    mdl.DeptAbbr = item.DeptAbbr;
                                    mdl.eFilePathWord = item.eFilePathWord;
                                    mdl.ReFileID = item.ReFileID;
                                    mdl.MainEFileName = item.FileName;
                                    mdl.DiaryNo = item.DiaryNo;
                                    mdl.BranchName = item.BranchName;
                                    mdl.EmpName = item.EmpName;
                                    mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                    mdl.Desingnation = item.Desingnation;
                                    mdl.AbbrevBranch = item.AbbrevBranch;
                                    mdl.AnnexPdfPath = item.Annexurepath;
                                    mdl.ItemName = item.ItemName;
                                    mdl.CountsAPS = item.CountsAPS;
                                    mdl.ReplyRefNo = item.ReplyRefNo;
                                    mdl.CommitteeName = item.CommitteeName;
                                    mdl.CommitteeId = item.commiteeId;
                                    mdl.ComAbbr = item.comabbr;
                                    mdl.IsRejected = item.IsRejected;
                                    mdl.RejectedRemarks = item.RejectedRemarks;
                                    lst.Add(mdl);
                                    Mainlst.Add(mdl);
                                }
                                if (V != "")
                                {
                                    List<int> NullIds = V.Split(',').Select(int.Parse).ToList();
                                    var query1 = (from a in ctx.eFileAttachments
                                                  join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                                  from ps in temp.DefaultIfEmpty()
                                                  join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                                  from Ef in EFileTemp.DefaultIfEmpty()
                                                  join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID  // a.ToPaperNature equals c.PaperNatureID
                                                  join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                                  join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                                  from cmf in comtemp.DefaultIfEmpty()
                                                  where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && a.CurrentBranchId == CBranchID // && NullIds.Contains(a.eFileAttachmentId)

                                                  select new
                                                  {
                                                      eFileAttachmentId = a.eFileAttachmentId,
                                                      Department = ps.deptname ?? "OTHER",
                                                      eFileName = a.eFileName,
                                                      PaperNature = a.PaperNature,
                                                      PaperNatureDays = a.PaperNatureDays ?? "0",
                                                      eFilePath = a.eFilePath,
                                                      Title = a.Title,
                                                      Description = a.Description,
                                                      CreateDate = a.CreatedDate,
                                                      PaperStatus = a.PaperStatus,
                                                      eFIleID = a.eFileID,
                                                      PaperRefNo = a.PaperRefNo,
                                                      DepartmentId = a.DepartmentId,
                                                      OfficeCode = a.OfficeCode,
                                                      ToDepartmentID = a.ToDepartmentID,
                                                      ToOfficeCode = a.ToOfficeCode,
                                                      ReplyStatus = a.ReplyStatus,
                                                      ReplyDate = a.ReplyDate,
                                                      ReceivedDate = a.ReceivedDate,
                                                      DocumentTypeId = a.DocumentTypeId,
                                                      SendDate = a.SendDate,
                                                      ToPaperNature = a.ToPaperNature,
                                                      ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                                      LinkedRefNo = a.LinkedRefNo,
                                                      RevedOption = a.RevedOption,
                                                      RecvDetails = a.RecvDetails,
                                                      ToRecvDetails = a.ToRecvDetails,
                                                      eMode = a.eMode,
                                                      RecvdPaperNature = c.PaperNature,
                                                      DocuemntPaperType = d.PaperType,
                                                      PType = a.PType,
                                                      SendStatus = a.SendStatus,
                                                      DeptAbbr = ps.deptabbr,
                                                      eFilePathWord = a.eFilePathWord,
                                                      ReFileID = a.ReFileID,
                                                      FileName = Ef.eFileNumber,
                                                      DiaryNo = a.DiaryNo,
                                                      Annexurepath = a.AnnexPdfPath,
                                                      BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                                      EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                                      CurrentAadharId = a.CurrentEmpAadharID,
                                                      AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                                      Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                                      CountsAPS = a.CountsAPS,
                                                      ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                                      ReplyRefNo = a.ReplyRefNo,
                                                      CommitteeName = cmf.CommitteeName,
                                                      commiteeId = a.CommitteeId,
                                                      comabbr = cmf.Abbreviation,
                                                      IsRejected = a.IsRejected,
                                                      RejectedRemarks = a.RejectedRemarks

                                                  }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                                    foreach (var item in query1)
                                    {
                                        eFileAttachment mdl = new eFileAttachment();
                                        mdl.DepartmentName = item.Department;
                                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                                        mdl.eFileName = item.eFileName;
                                        mdl.PaperNature = item.PaperNature;
                                        mdl.PaperNatureDays = item.PaperNatureDays;
                                        mdl.eFilePath = null;
                                        mdl.Title = item.Title;
                                        mdl.Description = item.Description;
                                        mdl.CreatedDate = item.CreateDate;
                                        mdl.PaperStatus = item.PaperStatus;
                                        mdl.eFileID = item.eFIleID;
                                        mdl.PaperRefNo = item.PaperRefNo;
                                        mdl.DepartmentId = item.DepartmentId;
                                        mdl.OfficeCode = item.OfficeCode;
                                        mdl.ToDepartmentID = item.ToDepartmentID;
                                        mdl.ToOfficeCode = item.ToOfficeCode;
                                        mdl.ReplyStatus = item.ReplyStatus;
                                        mdl.ReplyDate = item.ReplyDate;
                                        mdl.ReceivedDate = item.ReceivedDate;
                                        mdl.DocumentTypeId = item.DocumentTypeId;
                                        mdl.SendDate = item.SendDate;
                                        mdl.ToPaperNature = item.ToPaperNature;
                                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                        mdl.LinkedRefNo = item.LinkedRefNo;
                                        mdl.RevedOption = item.RevedOption;
                                        mdl.RecvDetails = item.RecvDetails;
                                        mdl.ToRecvDetails = item.ToRecvDetails;
                                        mdl.eMode = item.eMode;
                                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                                        mdl.PType = item.PType;
                                        mdl.SendStatus = item.SendStatus;
                                        mdl.DeptAbbr = item.DeptAbbr;
                                        mdl.eFilePathWord = null;
                                        mdl.ReFileID = item.ReFileID;
                                        mdl.MainEFileName = item.FileName;
                                        mdl.DiaryNo = item.DiaryNo;
                                        mdl.BranchName = item.BranchName;
                                        mdl.EmpName = item.EmpName;
                                        mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                        mdl.Desingnation = item.Desingnation;
                                        mdl.AbbrevBranch = item.AbbrevBranch;
                                        mdl.AnnexPdfPath = item.Annexurepath;
                                        mdl.ItemName = item.ItemName;
                                        mdl.CountsAPS = item.CountsAPS;
                                        mdl.ReplyRefNo = item.ReplyRefNo;
                                        mdl.CommitteeName = item.CommitteeName;
                                        mdl.CommitteeId = item.commiteeId;
                                        mdl.ComAbbr = item.comabbr;
                                        mdl.IsRejected = item.IsRejected;
                                        mdl.RejectedRemarks = item.RejectedRemarks;
                                        // lst.Add(mdl);
                                        Mainlst.Add(mdl);
                                    }
                                }

                                return Mainlst;

                            }
                            else if (s != "" && PaperType == 1)
                            {
                                List<int> TagIds1 = s.Split(',').Select(int.Parse).ToList();
                                V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID && !TagIds1.Contains(p.eFileAttachmentId))
                                         .Select(p => p.eFileAttachmentId.ToString()));
                                List<int> TagIds = s.Split(',').Select(int.Parse).ToList();
                                var query = (from a in ctx.eFileAttachments
                                             join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
                                             from ps in temp.DefaultIfEmpty()
                                             join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                             from Ef in EFileTemp.DefaultIfEmpty()
                                             join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID  //on a.ToPaperNature equals c.PaperNatureID
                                             join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                             join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                             from cmf in comtemp.DefaultIfEmpty()
                                             where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false
                                                 //&& TagIds.Contains(a.eFileAttachmentId) //data not saved in tMovementFile
                                             && a.CurrentBranchId == CBranchID

                                             select new
                                             {
                                                 eFileAttachmentId = a.eFileAttachmentId,
                                                 Department = ps.deptname ?? "OTHER",
                                                 eFileName = a.eFileName,
                                                 PaperNature = a.PaperNature,
                                                 PaperNatureDays = a.PaperNatureDays ?? "0",
                                                 eFilePath = a.eFilePath,
                                                 Title = a.Title,
                                                 Description = a.Description,
                                                 CreateDate = a.CreatedDate,
                                                 PaperStatus = a.PaperStatus,
                                                 eFIleID = a.eFileID,
                                                 PaperRefNo = a.PaperRefNo,
                                                 DepartmentId = a.DepartmentId,
                                                 OfficeCode = a.OfficeCode,
                                                 ToDepartmentID = a.ToDepartmentID,
                                                 ToOfficeCode = a.ToOfficeCode,
                                                 ReplyStatus = a.ReplyStatus,
                                                 ReplyDate = a.ReplyDate,
                                                 ReceivedDate = a.ReceivedDate,
                                                 DocumentTypeId = a.DocumentTypeId,
                                                 SendDate = a.SendDate,
                                                 ToPaperNature = a.ToPaperNature,
                                                 ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                                 LinkedRefNo = a.LinkedRefNo,
                                                 RevedOption = a.RevedOption,
                                                 RecvDetails = a.RecvDetails,
                                                 ToRecvDetails = a.ToRecvDetails,
                                                 eMode = a.eMode,
                                                 RecvdPaperNature = c.PaperNature,
                                                 DocuemntPaperType = d.PaperType,
                                                 PType = a.PType,
                                                 SendStatus = a.SendStatus,
                                                 DeptAbbr = ps.deptabbr,
                                                 eFilePathWord = a.eFilePathWord,
                                                 ReFileID = a.ReFileID,
                                                 FileName = Ef.eFileNumber,
                                                 DiaryNo = a.DiaryNo,
                                                 Annexurepath = a.AnnexPdfPath,
                                                 BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                                 EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                                 CurrentAadharId = a.CurrentEmpAadharID,
                                                 AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                                 Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                                 CountsAPS = a.CountsAPS,
                                                 ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                                 ReplyRefNo = a.ReplyRefNo,
                                                 CommitteeName = cmf.CommitteeName,
                                                 commiteeId = a.CommitteeId,
                                                 comabbr = cmf.Abbreviation,
                                                 IsRejected = a.IsRejected,
                                                 RejectedRemarks = a.RejectedRemarks

                                             }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                                foreach (var item in query)
                                {
                                    eFileAttachment mdl = new eFileAttachment();
                                    mdl.DepartmentName = item.Department;
                                    mdl.eFileAttachmentId = item.eFileAttachmentId;
                                    mdl.eFileName = item.eFileName;
                                    mdl.PaperNature = item.PaperNature;
                                    mdl.PaperNatureDays = item.PaperNatureDays;
                                    mdl.eFilePath = item.eFilePath;
                                    mdl.Title = item.Title;
                                    mdl.Description = item.Description;
                                    mdl.CreatedDate = item.CreateDate;
                                    mdl.PaperStatus = item.PaperStatus;
                                    mdl.eFileID = item.eFIleID;
                                    mdl.PaperRefNo = item.PaperRefNo;
                                    mdl.DepartmentId = item.DepartmentId;
                                    mdl.OfficeCode = item.OfficeCode;
                                    mdl.ToDepartmentID = item.ToDepartmentID;
                                    mdl.ToOfficeCode = item.ToOfficeCode;
                                    mdl.ReplyStatus = item.ReplyStatus;
                                    mdl.ReplyDate = item.ReplyDate;
                                    mdl.ReceivedDate = item.ReceivedDate;
                                    mdl.DocumentTypeId = item.DocumentTypeId;
                                    mdl.SendDate = item.SendDate;
                                    mdl.ToPaperNature = item.ToPaperNature;
                                    mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                    mdl.LinkedRefNo = item.LinkedRefNo;
                                    mdl.RevedOption = item.RevedOption;
                                    mdl.RecvDetails = item.RecvDetails;
                                    mdl.ToRecvDetails = item.ToRecvDetails;
                                    mdl.eMode = item.eMode;
                                    mdl.RecvdPaperNature = item.RecvdPaperNature;

                                    mdl.DocuemntPaperType = item.DocuemntPaperType;
                                    mdl.PType = item.PType;
                                    mdl.SendStatus = item.SendStatus;
                                    mdl.DeptAbbr = item.DeptAbbr;
                                    mdl.eFilePathWord = item.eFilePathWord;
                                    mdl.ReFileID = item.ReFileID;
                                    mdl.MainEFileName = item.FileName;
                                    mdl.DiaryNo = item.DiaryNo;
                                    mdl.BranchName = item.BranchName;
                                    mdl.EmpName = item.EmpName;
                                    mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                    mdl.Desingnation = item.Desingnation;
                                    mdl.AbbrevBranch = item.AbbrevBranch;
                                    mdl.AnnexPdfPath = item.Annexurepath;
                                    mdl.ItemName = item.ItemName;
                                    mdl.CountsAPS = item.CountsAPS;
                                    mdl.ReplyRefNo = item.ReplyRefNo;
                                    mdl.CommitteeName = item.CommitteeName;
                                    mdl.CommitteeId = item.commiteeId;
                                    mdl.ComAbbr = item.comabbr;
                                    mdl.IsRejected = item.IsRejected;
                                    mdl.RejectedRemarks = item.RejectedRemarks;
                                    lst.Add(mdl);
                                    Mainlst.Add(mdl);
                                }
                                return lst;
                            }

                            else
                            {
                                V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID)
                                         .Select(p => p.eFileAttachmentId.ToString()));
                                if (V != "")
                                {
                                    List<int> NullIds = V.Split(',').Select(int.Parse).ToList();
                                    var query1 = (from a in ctx.eFileAttachments
                                                  join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
                                                  from ps in temp.DefaultIfEmpty()
                                                  join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                                  from Ef in EFileTemp.DefaultIfEmpty()
                                                  join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID //on a.ToPaperNature equals c.PaperNatureID
                                                  join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                                  join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                                  from cmf in comtemp.DefaultIfEmpty()
                                                  where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && a.CurrentBranchId == CBranchID //&& NullIds.Contains(a.eFileAttachmentId)

                                                  select new
                                                  {
                                                      eFileAttachmentId = a.eFileAttachmentId,
                                                      Department = ps.deptname ?? "OTHER",
                                                      eFileName = a.eFileName,
                                                      PaperNature = a.PaperNature,
                                                      PaperNatureDays = a.PaperNatureDays ?? "0",
                                                      eFilePath = a.eFilePath,
                                                      Title = a.Title,
                                                      Description = a.Description,
                                                      CreateDate = a.CreatedDate,
                                                      PaperStatus = a.PaperStatus,
                                                      eFIleID = a.eFileID,
                                                      PaperRefNo = a.PaperRefNo,
                                                      DepartmentId = a.DepartmentId,
                                                      OfficeCode = a.OfficeCode,
                                                      ToDepartmentID = a.ToDepartmentID,
                                                      ToOfficeCode = a.ToOfficeCode,
                                                      ReplyStatus = a.ReplyStatus,
                                                      ReplyDate = a.ReplyDate,
                                                      ReceivedDate = a.ReceivedDate,
                                                      DocumentTypeId = a.DocumentTypeId,
                                                      SendDate = a.SendDate,
                                                      ToPaperNature = a.ToPaperNature,
                                                      ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                                      LinkedRefNo = a.LinkedRefNo,
                                                      RevedOption = a.RevedOption,
                                                      RecvDetails = a.RecvDetails,
                                                      ToRecvDetails = a.ToRecvDetails,
                                                      eMode = a.eMode,
                                                      RecvdPaperNature = c.PaperNature,
                                                      DocuemntPaperType = d.PaperType,
                                                      PType = a.PType,
                                                      SendStatus = a.SendStatus,
                                                      DeptAbbr = ps.deptabbr,
                                                      eFilePathWord = a.eFilePathWord,
                                                      ReFileID = a.ReFileID,
                                                      FileName = Ef.eFileNumber,
                                                      DiaryNo = a.DiaryNo,
                                                      Annexurepath = a.AnnexPdfPath,
                                                      BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                                      EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                                      AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                                      Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                                      CountsAPS = a.CountsAPS,
                                                      ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                                      CurrentAadharId = a.CurrentEmpAadharID,
                                                      ReplyRefNo = a.ReplyRefNo,
                                                      CommitteeName = cmf.CommitteeName,
                                                      commiteeId = a.CommitteeId,
                                                      comabbr = cmf.Abbreviation,
                                                      IsRejected = a.IsRejected,
                                                      RejectedRemarks = a.RejectedRemarks
                                                  }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                                    foreach (var item in query1)
                                    {
                                        eFileAttachment mdl = new eFileAttachment();
                                        mdl.DepartmentName = item.Department;
                                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                                        mdl.eFileName = item.eFileName;
                                        mdl.PaperNature = item.PaperNature;
                                        mdl.PaperNatureDays = item.PaperNatureDays;
                                        mdl.eFilePath = null;
                                        mdl.Title = item.Title;
                                        mdl.Description = item.Description;
                                        mdl.CreatedDate = item.CreateDate;
                                        mdl.PaperStatus = item.PaperStatus;
                                        mdl.eFileID = item.eFIleID;
                                        mdl.PaperRefNo = item.PaperRefNo;
                                        mdl.DepartmentId = item.DepartmentId;
                                        mdl.OfficeCode = item.OfficeCode;
                                        mdl.ToDepartmentID = item.ToDepartmentID;
                                        mdl.ToOfficeCode = item.ToOfficeCode;
                                        mdl.ReplyStatus = item.ReplyStatus;
                                        mdl.ReplyDate = item.ReplyDate;
                                        mdl.ReceivedDate = item.ReceivedDate;
                                        mdl.DocumentTypeId = item.DocumentTypeId;
                                        mdl.SendDate = item.SendDate;
                                        mdl.ToPaperNature = item.ToPaperNature;
                                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                        mdl.LinkedRefNo = item.LinkedRefNo;
                                        mdl.RevedOption = item.RevedOption;
                                        mdl.RecvDetails = item.RecvDetails;
                                        mdl.ToRecvDetails = item.ToRecvDetails;
                                        mdl.eMode = item.eMode;
                                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                                        mdl.PType = item.PType;
                                        mdl.SendStatus = item.SendStatus;
                                        mdl.DeptAbbr = item.DeptAbbr;
                                        mdl.eFilePathWord = null;
                                        mdl.ReFileID = item.ReFileID;
                                        mdl.MainEFileName = item.FileName;
                                        mdl.DiaryNo = item.DiaryNo;
                                        mdl.BranchName = item.BranchName;
                                        mdl.EmpName = item.EmpName;
                                        mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                        mdl.Desingnation = item.Desingnation;
                                        mdl.AbbrevBranch = item.AbbrevBranch;
                                        mdl.AnnexPdfPath = item.Annexurepath;
                                        mdl.ItemName = item.ItemName;
                                        mdl.CountsAPS = item.CountsAPS;
                                        mdl.ReplyRefNo = item.ReplyRefNo;
                                        mdl.CommitteeName = item.CommitteeName;
                                        mdl.CommitteeId = item.commiteeId;
                                        mdl.ComAbbr = item.comabbr;
                                        mdl.IsRejected = item.IsRejected;
                                        mdl.RejectedRemarks = item.RejectedRemarks;
                                        Mainlst.Add(mdl);
                                    }
                                }
                                else  //Code by Sujeet
                                {
                                    var query1 = (from a in ctx.eFileAttachments
                                                  join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
                                                  from ps in temp.DefaultIfEmpty()
                                                  join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                                  from Ef in EFileTemp.DefaultIfEmpty()
                                                  join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID //on a.ToPaperNature equals c.PaperNatureID
                                                  join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                                  join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                                  from cmf in comtemp.DefaultIfEmpty()
                                                  where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false
                                                  && a.CurrentBranchId == CBranchID

                                                  select new
                                                  {
                                                      eFileAttachmentId = a.eFileAttachmentId,
                                                      Department = ps.deptname ?? "OTHER",
                                                      eFileName = a.eFileName,
                                                      PaperNature = a.PaperNature,
                                                      PaperNatureDays = a.PaperNatureDays ?? "0",
                                                      eFilePath = a.eFilePath,
                                                      Title = a.Title,
                                                      Description = a.Description,
                                                      CreateDate = a.CreatedDate,
                                                      PaperStatus = a.PaperStatus,
                                                      eFIleID = a.eFileID,
                                                      PaperRefNo = a.PaperRefNo,
                                                      DepartmentId = a.DepartmentId,
                                                      OfficeCode = a.OfficeCode,
                                                      ToDepartmentID = a.ToDepartmentID,
                                                      ToOfficeCode = a.ToOfficeCode,
                                                      ReplyStatus = a.ReplyStatus,
                                                      ReplyDate = a.ReplyDate,
                                                      ReceivedDate = a.ReceivedDate,
                                                      DocumentTypeId = a.DocumentTypeId,
                                                      SendDate = a.SendDate,
                                                      ToPaperNature = a.ToPaperNature,
                                                      ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                                      LinkedRefNo = a.LinkedRefNo,
                                                      RevedOption = a.RevedOption,
                                                      RecvDetails = a.RecvDetails,
                                                      ToRecvDetails = a.ToRecvDetails,
                                                      eMode = a.eMode,
                                                      RecvdPaperNature = c.PaperNature,
                                                      DocuemntPaperType = d.PaperType,
                                                      PType = a.PType,
                                                      SendStatus = a.SendStatus,
                                                      DeptAbbr = ps.deptabbr,
                                                      eFilePathWord = a.eFilePathWord,
                                                      ReFileID = a.ReFileID,
                                                      FileName = Ef.eFileNumber,
                                                      DiaryNo = a.DiaryNo,
                                                      Annexurepath = a.AnnexPdfPath,
                                                      BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                                      EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                                      AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                                      Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                                      CountsAPS = a.CountsAPS,
                                                      ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                                      CurrentAadharId = a.CurrentEmpAadharID,
                                                      ReplyRefNo = a.ReplyRefNo,
                                                      CommitteeName = cmf.CommitteeName,
                                                      commiteeId = a.CommitteeId,
                                                      comabbr = cmf.Abbreviation,
                                                      IsRejected = a.IsRejected,
                                                      RejectedRemarks = a.RejectedRemarks
                                                  }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                                    foreach (var item in query1)
                                    {
                                        eFileAttachment mdl = new eFileAttachment();
                                        mdl.DepartmentName = item.Department;
                                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                                        mdl.eFileName = item.eFileName;
                                        mdl.PaperNature = item.PaperNature;
                                        mdl.PaperNatureDays = item.PaperNatureDays;
                                        mdl.eFilePath = null;
                                        mdl.Title = item.Title;
                                        mdl.Description = item.Description;
                                        mdl.CreatedDate = item.CreateDate;
                                        mdl.PaperStatus = item.PaperStatus;
                                        mdl.eFileID = item.eFIleID;
                                        mdl.PaperRefNo = item.PaperRefNo;
                                        mdl.DepartmentId = item.DepartmentId;
                                        mdl.OfficeCode = item.OfficeCode;
                                        mdl.ToDepartmentID = item.ToDepartmentID;
                                        mdl.ToOfficeCode = item.ToOfficeCode;
                                        mdl.ReplyStatus = item.ReplyStatus;
                                        mdl.ReplyDate = item.ReplyDate;
                                        mdl.ReceivedDate = item.ReceivedDate;
                                        mdl.DocumentTypeId = item.DocumentTypeId;
                                        mdl.SendDate = item.SendDate;
                                        mdl.ToPaperNature = item.ToPaperNature;
                                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                        mdl.LinkedRefNo = item.LinkedRefNo;
                                        mdl.RevedOption = item.RevedOption;
                                        mdl.RecvDetails = item.RecvDetails;
                                        mdl.ToRecvDetails = item.ToRecvDetails;
                                        mdl.eMode = item.eMode;
                                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                                        mdl.PType = item.PType;
                                        mdl.SendStatus = item.SendStatus;
                                        mdl.DeptAbbr = item.DeptAbbr;
                                        mdl.eFilePathWord = null;
                                        mdl.ReFileID = item.ReFileID;
                                        mdl.MainEFileName = item.FileName;
                                        mdl.DiaryNo = item.DiaryNo;
                                        mdl.BranchName = item.BranchName;
                                        mdl.EmpName = item.EmpName;
                                        mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                        mdl.Desingnation = item.Desingnation;
                                        mdl.AbbrevBranch = item.AbbrevBranch;
                                        mdl.AnnexPdfPath = item.Annexurepath;
                                        mdl.ItemName = item.ItemName;
                                        mdl.CountsAPS = item.CountsAPS;
                                        mdl.ReplyRefNo = item.ReplyRefNo;
                                        mdl.CommitteeName = item.CommitteeName;
                                        mdl.CommitteeId = item.commiteeId;
                                        mdl.ComAbbr = item.comabbr;
                                        mdl.IsRejected = item.IsRejected;
                                        mdl.RejectedRemarks = item.RejectedRemarks;
                                        Mainlst.Add(mdl);
                                    }
                                }

                                return Mainlst;

                            }
                        }
                        else //For Others Deparments
                        {
                            List<eFileAttachment> Mainlst = new List<eFileAttachment>();
                            List<eFileAttachment> lst = new List<eFileAttachment>();

                            //(from a in ctx.eFileAttachments
                            //                     join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
                            //                     from ps in temp.DefaultIfEmpty()
                            //                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                            //                     from Ef in EFileTemp.DefaultIfEmpty()
                            //                     join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID //on a.ToPaperNature equals c.PaperNatureID
                            //                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                            //                     where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && NullIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

                            var query = (from a in ctx.eFileAttachments
                                         join b in ctx.departments on a.ToDepartmentID equals b.deptId
                                         into temp
                                         from ps in temp.DefaultIfEmpty()
                                         join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
                                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                         from cmf in comtemp.DefaultIfEmpty()

                                         where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && a.IsBackLog != true
                                         //var query = (from a in ctx.eFileAttachments
                                         //             join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                         //             from ps in temp.DefaultIfEmpty()
                                         //             join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                         //             from Ef in EFileTemp.DefaultIfEmpty()
                                         //             join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                         //             join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                         //             where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false
                                         select new
                                         {
                                             eFileAttachmentId = a.eFileAttachmentId,
                                             Department = ps.deptname ?? "OTHER",
                                             eFileName = a.eFileName,
                                             PaperNature = a.PaperNature,
                                             PaperNatureDays = a.PaperNatureDays ?? "0",
                                             eFilePath = a.eFilePath,
                                             Title = a.Title,
                                             Description = a.Description,
                                             CreateDate = a.CreatedDate,
                                             PaperStatus = a.PaperStatus,
                                             eFIleID = a.eFileID,
                                             PaperRefNo = a.PaperRefNo,
                                             DepartmentId = a.DepartmentId,
                                             OfficeCode = a.OfficeCode,
                                             ToDepartmentID = a.ToDepartmentID,
                                             ToOfficeCode = a.ToOfficeCode,
                                             ReplyStatus = a.ReplyStatus,
                                             ReplyDate = a.ReplyDate,
                                             ReceivedDate = a.ReceivedDate,
                                             DocumentTypeId = a.DocumentTypeId,
                                             ToPaperNature = a.ToPaperNature,
                                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                             LinkedRefNo = a.LinkedRefNo,
                                             RevedOption = a.RevedOption,
                                             RecvDetails = a.RecvDetails,
                                             ToRecvDetails = a.ToRecvDetails,
                                             eMode = a.eMode,
                                             RecvdPaperNature = c.PaperNature,
                                             DocuemntPaperType = d.PaperType,
                                             PType = a.PType,
                                             SendStatus = a.SendStatus,
                                             DeptAbbr = ps.deptabbr,
                                             eFilePathWord = a.eFilePathWord,
                                             ReFileID = a.ReFileID,
                                             SendDate = a.SendDate,
                                             ReplyRefNo = a.ReplyRefNo,
                                             ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                             // DiaryNo = a.DiaryNo,
                                             //BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                             //EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                             CurrentAadharId = a.CurrentEmpAadharID,
                                             CommitteeName = cmf.CommitteeName,
                                             commiteeId = a.CommitteeId,
                                             comabbr = cmf.Abbreviation,
                                             IsRejected = a.IsRejected,
                                             RejectedRemarks = a.RejectedRemarks
                                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);

                            foreach (var item in query)
                            {
                                eFileAttachment mdl = new eFileAttachment();
                                mdl.DepartmentName = item.Department;
                                mdl.eFileAttachmentId = item.eFileAttachmentId;
                                mdl.eFileName = item.eFileName;
                                mdl.PaperNature = item.PaperNature;
                                mdl.PaperNatureDays = item.PaperNatureDays;
                                mdl.eFilePath = item.eFilePath;
                                mdl.Title = item.Title;
                                mdl.Description = item.Description;
                                mdl.CreatedDate = item.CreateDate;
                                mdl.PaperStatus = item.PaperStatus;
                                mdl.eFileID = item.eFIleID;
                                mdl.PaperRefNo = item.PaperRefNo;
                                mdl.DepartmentId = item.DepartmentId;
                                mdl.OfficeCode = item.OfficeCode;
                                mdl.ToDepartmentID = item.ToDepartmentID;
                                mdl.ToOfficeCode = item.ToOfficeCode;
                                mdl.ReplyStatus = item.ReplyStatus;
                                mdl.ReplyDate = item.ReplyDate;
                                mdl.ReceivedDate = item.ReceivedDate;
                                mdl.DocumentTypeId = item.DocumentTypeId;

                                mdl.ToPaperNature = item.ToPaperNature;
                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                                mdl.LinkedRefNo = item.LinkedRefNo;
                                mdl.RevedOption = item.RevedOption;
                                mdl.RecvDetails = item.RecvDetails;
                                mdl.ToRecvDetails = item.ToRecvDetails;
                                mdl.eMode = item.eMode;
                                mdl.RecvdPaperNature = item.RecvdPaperNature;
                                mdl.SendDate = item.SendDate;
                                mdl.DocuemntPaperType = item.DocuemntPaperType;
                                mdl.PType = item.PType;
                                mdl.SendStatus = item.SendStatus;
                                mdl.DeptAbbr = item.DeptAbbr;
                                mdl.eFilePathWord = item.eFilePathWord;
                                mdl.ReFileID = item.ReFileID;
                                mdl.ReplyRefNo = item.ReplyRefNo;
                                mdl.ItemName = item.ItemName;
                                // mdl.MainEFileName = item.FileName;
                                //  mdl.DiaryNo = item.DiaryNo;
                                //mdl.BranchName = item.BranchName;
                                //mdl.EmpName = item.EmpName;
                                //mdl.CurrentEmpAadharID = item.CurrentAadharId;
                                mdl.CommitteeName = item.CommitteeName;
                                mdl.CommitteeId = item.commiteeId;
                                mdl.ComAbbr = item.comabbr;
                                mdl.IsRejected = item.IsRejected;
                                mdl.RejectedRemarks = item.RejectedRemarks;
                                lst.Add(mdl);
                                Mainlst.Add(mdl);
                            }
                            return Mainlst;



                        }


                    }

                }
            }
            else
            {
                using (eFileContext ctx = new eFileContext())
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 join cm in ctx.committees on a.CommitteeId equals cm.CommitteeId into comtemp
                                 from cmf in comtemp.DefaultIfEmpty()
                                 where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo,  ///pp
                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                     CommitteeName = cmf.CommitteeName,
                                     commiteeId = a.CommitteeId,
                                     comabbr = cmf.Abbreviation,
                                     IsRejected = a.IsRejected,
                                     RejectedRemarks = a.RejectedRemarks

                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        mdl.ItemName = item.ItemName;
                        mdl.CommitteeName = item.CommitteeName;
                        mdl.CommitteeId = item.commiteeId;
                        mdl.ComAbbr = item.comabbr;
                        mdl.IsRejected = item.IsRejected;
                        mdl.RejectedRemarks = item.RejectedRemarks;
                        lst.Add(mdl);
                    }

                    return lst;
                }
            }
        }

        public static object SentDept(object param)
        {
            try
            {
                eFileAttachment model = param as eFileAttachment;
                eFileContext ctx = new eFileContext();
                model.YearId = (from run in ctx.eFilePaperSno
                                where run.year == DateTime.Now.Year
                                select run.id).FirstOrDefault();
                model.Type = "SendPaperToVS";
                int Count = (from Item in ctx.eFileAttachments where Item.ReplyRefNo == model.ReplyRefNo select Item).Count();
                var lastNumber = (from eF in ctx.eFileAttachments orderby eF.eFileAttachmentId descending select eF.eFileAttachmentId).FirstOrDefault();
                model.PaperRefNo = System.DateTime.Now.Year.ToString() + "/" + Convert.ToString(lastNumber + 1);
                model.ReFileID = 0;
                model.eFileID = 0;
                model.eFileNameId = model.CreatedBy;
                model.ParentId = 0;
                model.SplitFileCount = 0;
                model.ReportLayingHouse = 0;
                model.Approve = 0;
                model.OfficeCode = 0;
                model.CurDNo = 0;
                model.CurYear = System.DateTime.Now.Year;
                model.CurrentBranchId = 0;
                model.ToOfficeCode = 0;
                model.SendDate = System.DateTime.Now;
                model.ReceivedDate = System.DateTime.Now;
                model.ReplyDate = System.DateTime.Now;
                model.AssignDateTime = System.DateTime.Now;
                model.DraftId = model.DraftId;
                model.RevedOption = "H";
                model.ToPaperNature = model.PaperNature;
                model.PType = "O";
                model.PaperStatus = "Y";
                model.eMode = 1;
                model.ItemId = model.ItemTypeName;
                model.ItemNumber = model.ItemNumber;
                model.ReplyRefNo = model.ReplyRefNo;
                model.CommitteeId = model.CommitteeId;
                model.eFilePath = model.eFilePath;
                model.eFilePathWord = model.eFilePathWord;
                ctx.eFileAttachments.Add(model);
                ctx.SaveChanges();
                eFilePaperSno Sno = ctx.eFilePaperSno.Single(m => m.id == model.YearId);
                Sno.runno = lastNumber + 1;
                ctx.SaveChanges();
                return model;
            }
            catch
            {
                return 0; //Error
            }
        }





        //public static object GetSendList(object param)
        //{
        //    string[] str = param as string[];
        //    var V = "";
        //    string DeptId = Convert.ToString(str[0]);
        //    int Officecode = 0;
        //    int.TryParse(str[1], out Officecode);
        //    string Aadharid = str[2];
        //    int Year = Convert.ToInt32(str[4]);
        //    int PaperType = Convert.ToInt32(str[5]);
        //    List<string> deptlist = new List<string>();

        //    if (!string.IsNullOrEmpty(DeptId))
        //    {
        //        deptlist = DeptId.Split(',').Distinct().ToList();
        //    }

        //    if (Officecode == 0)
        //    {
        //        using (eFileContext ctx = new eFileContext())
        //        {
        //            if (Aadharid == "626192220638")
        //            {
        //                if (Year == 0)
        //                {
        //                    List<eFileAttachment> Mainlst = new List<eFileAttachment>();
        //                    var queryNull = (from a in ctx.eFileAttachments                                      
        //                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                 from Ef in EFileTemp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                     where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && a.DraftId==null
        //                                 select new
        //                                 {
        //                                     eFileAttachmentId = a.eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName =a.eFileName,
        //                                     PaperNature = a.PaperNature,
        //                                     PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                     eFilePath = a.eFilePath,
        //                                     Title = a.Title,
        //                                     Description = a.Description,
        //                                     CreateDate = a.CreatedDate,
        //                                     PaperStatus =a.PaperStatus,
        //                                     eFIleID = a.eFileID,
        //                                     PaperRefNo = a.PaperRefNo,
        //                                     DepartmentId = a.DepartmentId,
        //                                     OfficeCode = a.OfficeCode,
        //                                     ToDepartmentID = a.ToDepartmentID,
        //                                     ToOfficeCode = a.ToOfficeCode,
        //                                     ReplyStatus =a.ReplyStatus,
        //                                     ReplyDate = a.ReplyDate,
        //                                     ReceivedDate = a.ReceivedDate,
        //                                     DocumentTypeId = a.DocumentTypeId,
        //                                     SendDate = a.SendDate,
        //                                     ToPaperNature = a.ToPaperNature,
        //                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = a.LinkedRefNo,
        //                                     RevedOption = a.RevedOption,
        //                                     RecvDetails = a.RecvDetails,
        //                                     ToRecvDetails = a.ToRecvDetails,
        //                                     eMode = a.eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = a.PType,
        //                                     SendStatus = a.SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = a.eFilePathWord,
        //                                     ReFileID = a.ReFileID,
        //                                     FileName = Ef.eFileNumber,
        //                                     DiaryNo = a.DiaryNo,
        //                                     DraftId = a.DraftId,
        //                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                 }).OrderByDescending(a => a.eFileAttachmentId).ToList();
        //                    List<eFileAttachment> lst1 = new List<eFileAttachment>();
        //                    foreach (var item in queryNull)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;
        //                        mdl.SendDate = item.SendDate;
        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.Desingnation = item.Desingnation;
        //                        mdl.AbbrevBranch = item.AbbrevBranch;
        //                        mdl.DraftId = item.DraftId;
        //                        lst1.Add(mdl);
        //                        Mainlst.Add(mdl);
        //                    }
        //                   // return Mainlst;

        //                    var query = (from a in ctx.eFileAttachments 
        //                                 group a by a.DraftId into g
        //                                 join b in ctx.departments on g.FirstOrDefault().DepartmentId equals b.deptId into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join EFile in ctx.eFiles on g.FirstOrDefault().ReFileID equals EFile.eFileID into EFileTemp
        //                                 from Ef in EFileTemp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on g.FirstOrDefault().ToPaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on g.FirstOrDefault().DocumentTypeId equals d.PaperTypeID
        //                                 where deptlist.Contains(g.FirstOrDefault().DepartmentId) && g.FirstOrDefault().IsDeleted == false                                   
        //                                 select new
        //                                 {
        //                                     eFileAttachmentId = g.FirstOrDefault().eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName = g.FirstOrDefault().eFileName,
        //                                     PaperNature = g.FirstOrDefault().PaperNature,
        //                                     PaperNatureDays = g.FirstOrDefault().PaperNatureDays ?? "0",
        //                                     eFilePath = g.FirstOrDefault().eFilePath,
        //                                     Title = g.FirstOrDefault().Title,
        //                                     Description = g.FirstOrDefault().Description,
        //                                     CreateDate = g.FirstOrDefault().CreatedDate,
        //                                     PaperStatus = g.FirstOrDefault().PaperStatus,
        //                                     eFIleID = g.FirstOrDefault().eFileID,
        //                                     PaperRefNo = g.FirstOrDefault().PaperRefNo,
        //                                     DepartmentId = g.FirstOrDefault().DepartmentId,
        //                                     OfficeCode = g.FirstOrDefault().OfficeCode,
        //                                     ToDepartmentID = g.FirstOrDefault().ToDepartmentID,
        //                                     ToOfficeCode = g.FirstOrDefault().ToOfficeCode,
        //                                     ReplyStatus = g.FirstOrDefault().ReplyStatus,
        //                                     ReplyDate = g.FirstOrDefault().ReplyDate,
        //                                     ReceivedDate = g.FirstOrDefault().ReceivedDate,
        //                                     DocumentTypeId = g.FirstOrDefault().DocumentTypeId,
        //                                     SendDate = g.FirstOrDefault().SendDate,
        //                                     ToPaperNature = g.FirstOrDefault().ToPaperNature,
        //                                     ToPaperNatureDays = g.FirstOrDefault().ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = g.FirstOrDefault().LinkedRefNo,
        //                                     RevedOption = g.FirstOrDefault().RevedOption,
        //                                     RecvDetails = g.FirstOrDefault().RecvDetails,
        //                                     ToRecvDetails = g.FirstOrDefault().ToRecvDetails,
        //                                     eMode = g.FirstOrDefault().eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = g.FirstOrDefault().PType,
        //                                     SendStatus = g.FirstOrDefault().SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = g.FirstOrDefault().eFilePathWord,
        //                                     ReFileID = g.FirstOrDefault().ReFileID,
        //                                     FileName = Ef.eFileNumber,
        //                                     DiaryNo = g.FirstOrDefault().DiaryNo,
        //                                     DraftId = g.FirstOrDefault().DraftId,
        //                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == g.FirstOrDefault().CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == g.FirstOrDefault().CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == g.FirstOrDefault().CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == g.FirstOrDefault().CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                 }).OrderByDescending(a => a.eFileAttachmentId).ToList();
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    foreach (var item in query)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;
        //                        mdl.SendDate = item.SendDate;
        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.Desingnation = item.Desingnation;
        //                        mdl.AbbrevBranch = item.AbbrevBranch;
        //                        mdl.DraftId = item.DraftId;
        //                        lst.Add(mdl);
        //                        Mainlst.Add(mdl);
        //                    }
        //                    return Mainlst;
        //                }
        //                else
        //                {
        //                    DateTime Dt = DateTime.Now.AddYears(-Year);
        //                    List<eFileAttachment> Mainlst = new List<eFileAttachment>();
        //                    var queryNull = (from a in ctx.eFileAttachments
        //                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                     from ps in temp.DefaultIfEmpty()
        //                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                     from Ef in EFileTemp.DefaultIfEmpty()
        //                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                       where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false  && a.SendDate >= Dt && a.SendDate <= DateTime.Now && a.DraftId==null
        //                                     select new
        //                                     {
        //                                         eFileAttachmentId = a.eFileAttachmentId,
        //                                         Department = ps.deptname ?? "OTHER",
        //                                         eFileName = a.eFileName,
        //                                         PaperNature = a.PaperNature,
        //                                         PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                         eFilePath = a.eFilePath,
        //                                         Title = a.Title,
        //                                         Description = a.Description,
        //                                         CreateDate = a.CreatedDate,
        //                                         PaperStatus = a.PaperStatus,
        //                                         eFIleID = a.eFileID,
        //                                         PaperRefNo = a.PaperRefNo,
        //                                         DepartmentId = a.DepartmentId,
        //                                         OfficeCode = a.OfficeCode,
        //                                         ToDepartmentID = a.ToDepartmentID,
        //                                         ToOfficeCode = a.ToOfficeCode,
        //                                         ReplyStatus = a.ReplyStatus,
        //                                         ReplyDate = a.ReplyDate,
        //                                         ReceivedDate = a.ReceivedDate,
        //                                         DocumentTypeId = a.DocumentTypeId,
        //                                         SendDate = a.SendDate,
        //                                         ToPaperNature = a.ToPaperNature,
        //                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                         LinkedRefNo = a.LinkedRefNo,
        //                                         RevedOption = a.RevedOption,
        //                                         RecvDetails = a.RecvDetails,
        //                                         ToRecvDetails = a.ToRecvDetails,
        //                                         eMode = a.eMode,
        //                                         RecvdPaperNature = c.PaperNature,
        //                                         DocuemntPaperType = d.PaperType,
        //                                         PType = a.PType,
        //                                         SendStatus = a.SendStatus,
        //                                         DeptAbbr = ps.deptabbr,
        //                                         eFilePathWord = a.eFilePathWord,
        //                                         ReFileID = a.ReFileID,
        //                                         FileName = Ef.eFileNumber,
        //                                         DiaryNo = a.DiaryNo,
        //                                         DraftId = a.DraftId,
        //                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                     }).OrderByDescending(a => a.eFileAttachmentId).ToList();
        //                    List<eFileAttachment> lst1 = new List<eFileAttachment>();
        //                    foreach (var item in queryNull)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;
        //                        mdl.SendDate = item.SendDate;
        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                        mdl.MainEFileName = item.FileName;
        //                        mdl.DiaryNo = item.DiaryNo;
        //                        mdl.BranchName = item.BranchName;
        //                        mdl.EmpName = item.EmpName;
        //                        mdl.Desingnation = item.Desingnation;
        //                        mdl.AbbrevBranch = item.AbbrevBranch;
        //                        mdl.DraftId = item.DraftId;
        //                        lst1.Add(mdl);
        //                        Mainlst.Add(mdl);
        //                    }

        //                    var querybyDate = (from a in ctx.eFileAttachments
        //                                     join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                     from ps in temp.DefaultIfEmpty()
        //                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                     from Ef in EFileTemp.DefaultIfEmpty()
        //                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                     where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && a.SendDate >= Dt && a.SendDate <= DateTime.Now && a.DraftId != null
        //                                     select new
        //                                     {
        //                                         eFileAttachmentId = a.eFileAttachmentId,
        //                                         Department = ps.deptname ?? "OTHER",
        //                                         eFileName = a.eFileName,
        //                                         PaperNature = a.PaperNature,
        //                                         PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                         eFilePath = a.eFilePath,
        //                                         Title = a.Title,
        //                                         Description = a.Description,
        //                                         CreateDate = a.CreatedDate,
        //                                         PaperStatus = a.PaperStatus,
        //                                         eFIleID = a.eFileID,
        //                                         PaperRefNo = a.PaperRefNo,
        //                                         DepartmentId = a.DepartmentId,
        //                                         OfficeCode = a.OfficeCode,
        //                                         ToDepartmentID = a.ToDepartmentID,
        //                                         ToOfficeCode = a.ToOfficeCode,
        //                                         ReplyStatus = a.ReplyStatus,
        //                                         ReplyDate = a.ReplyDate,
        //                                         ReceivedDate = a.ReceivedDate,
        //                                         DocumentTypeId = a.DocumentTypeId,
        //                                         SendDate = a.SendDate,
        //                                         ToPaperNature = a.ToPaperNature,
        //                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                         LinkedRefNo = a.LinkedRefNo,
        //                                         RevedOption = a.RevedOption,
        //                                         RecvDetails = a.RecvDetails,
        //                                         ToRecvDetails = a.ToRecvDetails,
        //                                         eMode = a.eMode,
        //                                         RecvdPaperNature = c.PaperNature,
        //                                         DocuemntPaperType = d.PaperType,
        //                                         PType = a.PType,
        //                                         SendStatus = a.SendStatus,
        //                                         DeptAbbr = ps.deptabbr,
        //                                         eFilePathWord = a.eFilePathWord,
        //                                         ReFileID = a.ReFileID,
        //                                         FileName = Ef.eFileNumber,
        //                                         DiaryNo = a.DiaryNo,
        //                                         DraftId = a.DraftId,
        //                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                     }).OrderByDescending(a => a.eFileAttachmentId).GroupBy(x=>x.DraftId).ToList();
        //                    List<eFileAttachment> lst2 = new List<eFileAttachment>();
        //                    foreach (var item in querybyDate)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.DepartmentName = item.FirstOrDefault().Department;
        //                        mdl.eFileAttachmentId = item.FirstOrDefault().eFileAttachmentId;
        //                        mdl.eFileName = item.FirstOrDefault().eFileName;
        //                        mdl.PaperNature = item.FirstOrDefault().PaperNature;
        //                        mdl.PaperNatureDays = item.FirstOrDefault().PaperNatureDays;
        //                        mdl.eFilePath = item.FirstOrDefault().eFilePath;
        //                        mdl.Title = item.FirstOrDefault().Title;
        //                        mdl.Description = item.FirstOrDefault().Description;
        //                        mdl.CreatedDate = item.FirstOrDefault().CreateDate;
        //                        mdl.PaperStatus = item.FirstOrDefault().PaperStatus;
        //                        mdl.eFileID = item.FirstOrDefault().eFIleID;
        //                        mdl.PaperRefNo = item.FirstOrDefault().PaperRefNo;
        //                        mdl.DepartmentId = item.FirstOrDefault().DepartmentId;
        //                        mdl.OfficeCode = item.FirstOrDefault().OfficeCode;
        //                        mdl.ToDepartmentID = item.FirstOrDefault().ToDepartmentID;
        //                        mdl.ToOfficeCode = item.FirstOrDefault().ToOfficeCode;
        //                        mdl.ReplyStatus = item.FirstOrDefault().ReplyStatus;
        //                        mdl.ReplyDate = item.FirstOrDefault().ReplyDate;
        //                        mdl.ReceivedDate = item.FirstOrDefault().ReceivedDate;
        //                        mdl.DocumentTypeId = item.FirstOrDefault().DocumentTypeId;

        //                        mdl.ToPaperNature = item.FirstOrDefault().ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.FirstOrDefault().ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.FirstOrDefault().LinkedRefNo;
        //                        mdl.RevedOption = item.FirstOrDefault().RevedOption;
        //                        mdl.RecvDetails = item.FirstOrDefault().RecvDetails;
        //                        mdl.ToRecvDetails = item.FirstOrDefault().ToRecvDetails;
        //                        mdl.eMode = item.FirstOrDefault().eMode;
        //                        mdl.RecvdPaperNature = item.FirstOrDefault().RecvdPaperNature;
        //                        mdl.SendDate = item.FirstOrDefault().SendDate;
        //                        mdl.DocuemntPaperType = item.FirstOrDefault().DocuemntPaperType;
        //                        mdl.PType = item.FirstOrDefault().PType;
        //                        mdl.SendStatus = item.FirstOrDefault().SendStatus;
        //                        mdl.DeptAbbr = item.FirstOrDefault().DeptAbbr;
        //                        mdl.eFilePathWord = item.FirstOrDefault().eFilePathWord;
        //                        mdl.ReFileID = item.FirstOrDefault().ReFileID;
        //                        mdl.MainEFileName = item.FirstOrDefault().FileName;
        //                        mdl.DiaryNo = item.FirstOrDefault().DiaryNo;
        //                        mdl.BranchName = item.FirstOrDefault().BranchName;
        //                        mdl.EmpName = item.FirstOrDefault().EmpName;
        //                        mdl.Desingnation = item.FirstOrDefault().Desingnation;
        //                        mdl.AbbrevBranch = item.FirstOrDefault().AbbrevBranch;
        //                        mdl.DraftId = item.FirstOrDefault().DraftId;
        //                        lst2.Add(mdl);
        //                        Mainlst.Add(mdl);
        //                    }
        //                    //return lst;

        //                    //var query = (from a in ctx.eFileAttachments
        //                    //                 group a by a.DraftId into g
        //                    //                 join b in ctx.departments on g.FirstOrDefault().DepartmentId equals b.deptId into temp
        //                    //                 from ps in temp.DefaultIfEmpty()
        //                    //                 join EFile in ctx.eFiles on g.FirstOrDefault().ReFileID equals EFile.eFileID into EFileTemp
        //                    //                 from Ef in EFileTemp.DefaultIfEmpty()
        //                    //                 join c in ctx.eFilePaperNature on g.FirstOrDefault().ToPaperNature equals c.PaperNatureID
        //                    //                 join d in ctx.eFilePaperType on g.FirstOrDefault().DocumentTypeId equals d.PaperTypeID
        //                    //             where deptlist.Contains(g.FirstOrDefault().DepartmentId) && g.FirstOrDefault().IsDeleted == false && g.DefaultIfEmpty().FirstOrDefault().SendDate >= Dt && g.DefaultIfEmpty().FirstOrDefault().SendDate <= DateTime.Now 

        //                    //             select new
        //                    //                 {
        //                    //                     eFileAttachmentId = g.FirstOrDefault().eFileAttachmentId,
        //                    //                     Department = ps.deptname ?? "OTHER",
        //                    //                     eFileName = g.FirstOrDefault().eFileName,
        //                    //                     PaperNature = g.FirstOrDefault().PaperNature,
        //                    //                     PaperNatureDays = g.FirstOrDefault().PaperNatureDays ?? "0",
        //                    //                     eFilePath = g.FirstOrDefault().eFilePath,
        //                    //                     Title = g.FirstOrDefault().Title,
        //                    //                     Description = g.FirstOrDefault().Description,
        //                    //                     CreateDate = g.FirstOrDefault().CreatedDate,
        //                    //                     PaperStatus = g.FirstOrDefault().PaperStatus,
        //                    //                     eFIleID = g.FirstOrDefault().eFileID,
        //                    //                     PaperRefNo = g.FirstOrDefault().PaperRefNo,
        //                    //                     DepartmentId = g.FirstOrDefault().DepartmentId,
        //                    //                     OfficeCode = g.FirstOrDefault().OfficeCode,
        //                    //                     ToDepartmentID = g.FirstOrDefault().ToDepartmentID,
        //                    //                     ToOfficeCode = g.FirstOrDefault().ToOfficeCode,
        //                    //                     ReplyStatus = g.FirstOrDefault().ReplyStatus,
        //                    //                     ReplyDate = g.FirstOrDefault().ReplyDate,
        //                    //                     ReceivedDate = g.FirstOrDefault().ReceivedDate,
        //                    //                     DocumentTypeId = g.FirstOrDefault().DocumentTypeId,
        //                    //                     SendDate = g.FirstOrDefault().SendDate,
        //                    //                     ToPaperNature = g.FirstOrDefault().ToPaperNature,
        //                    //                     ToPaperNatureDays = g.FirstOrDefault().ToPaperNatureDays ?? "0",
        //                    //                     LinkedRefNo = g.FirstOrDefault().LinkedRefNo,
        //                    //                     RevedOption = g.FirstOrDefault().RevedOption,
        //                    //                     RecvDetails = g.FirstOrDefault().RecvDetails,
        //                    //                     ToRecvDetails = g.FirstOrDefault().ToRecvDetails,
        //                    //                     eMode = g.FirstOrDefault().eMode,
        //                    //                     RecvdPaperNature = c.PaperNature,
        //                    //                     DocuemntPaperType = d.PaperType,
        //                    //                     PType = g.FirstOrDefault().PType,
        //                    //                     SendStatus = g.FirstOrDefault().SendStatus,
        //                    //                     DeptAbbr = ps.deptabbr,
        //                    //                     eFilePathWord = g.FirstOrDefault().eFilePathWord,
        //                    //                     ReFileID = g.FirstOrDefault().ReFileID,
        //                    //                     FileName = Ef.eFileNumber,
        //                    //                     DiaryNo = g.FirstOrDefault().DiaryNo,
        //                    //                     DraftId = g.FirstOrDefault().DraftId,
        //                    //                     BranchName = (from mc in ctx.mBranches where mc.BranchId == g.FirstOrDefault().CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                    //                     EmpName = (from EN in ctx.mStaff where EN.AadharID == g.FirstOrDefault().CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                    //                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == g.FirstOrDefault().CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                    //                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == g.FirstOrDefault().CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                    //                 }).OrderByDescending(a => a.eFileAttachmentId).ToList();
        //                    //List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    //foreach (var item in query)
        //                    //{
        //                    //    eFileAttachment mdl = new eFileAttachment();
        //                    //    mdl.DepartmentName = item.Department;
        //                    //    mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                    //    mdl.eFileName = item.eFileName;
        //                    //    mdl.PaperNature = item.PaperNature;
        //                    //    mdl.PaperNatureDays = item.PaperNatureDays;
        //                    //    mdl.eFilePath = item.eFilePath;
        //                    //    mdl.Title = item.Title;
        //                    //    mdl.Description = item.Description;
        //                    //    mdl.CreatedDate = item.CreateDate;
        //                    //    mdl.PaperStatus = item.PaperStatus;
        //                    //    mdl.eFileID = item.eFIleID;
        //                    //    mdl.PaperRefNo = item.PaperRefNo;
        //                    //    mdl.DepartmentId = item.DepartmentId;
        //                    //    mdl.OfficeCode = item.OfficeCode;
        //                    //    mdl.ToDepartmentID = item.ToDepartmentID;
        //                    //    mdl.ToOfficeCode = item.ToOfficeCode;
        //                    //    mdl.ReplyStatus = item.ReplyStatus;
        //                    //    mdl.ReplyDate = item.ReplyDate;
        //                    //    mdl.ReceivedDate = item.ReceivedDate;
        //                    //    mdl.DocumentTypeId = item.DocumentTypeId;

        //                    //    mdl.ToPaperNature = item.ToPaperNature;
        //                    //    mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                    //    mdl.LinkedRefNo = item.LinkedRefNo;
        //                    //    mdl.RevedOption = item.RevedOption;
        //                    //    mdl.RecvDetails = item.RecvDetails;
        //                    //    mdl.ToRecvDetails = item.ToRecvDetails;
        //                    //    mdl.eMode = item.eMode;
        //                    //    mdl.RecvdPaperNature = item.RecvdPaperNature;
        //                    //    mdl.SendDate = item.SendDate;
        //                    //    mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                    //    mdl.PType = item.PType;
        //                    //    mdl.SendStatus = item.SendStatus;
        //                    //    mdl.DeptAbbr = item.DeptAbbr;
        //                    //    mdl.eFilePathWord = item.eFilePathWord;
        //                    //    mdl.ReFileID = item.ReFileID;
        //                    //    mdl.MainEFileName = item.FileName;
        //                    //    mdl.DiaryNo = item.DiaryNo;
        //                    //    mdl.BranchName = item.BranchName;
        //                    //    mdl.EmpName = item.EmpName;
        //                    //    mdl.Desingnation = item.Desingnation;
        //                    //    mdl.AbbrevBranch = item.AbbrevBranch;
        //                    //    mdl.DraftId = item.DraftId;
        //                    //    lst.Add(mdl);
        //                    //    Mainlst.Add(mdl);
        //                    //}
        //                    return Mainlst;

        //                }

        //            }

        //            else
        //            {
        //                if (str[3] != "") //For Vidhan Sabha Department Users
        //                {
        //                    int CBranchID = Convert.ToInt32(str[3]);
        //                    List<eFileAttachment> Mainlst = new List<eFileAttachment>();
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    var s = string.Join(",", ctx.tMovementFile.Where(p => p.EmpAadharId == Aadharid)
        //                            .Select(p => p.eFileAttachmentId.ToString()));
        //                    if (s != "" && PaperType==2)
        //                    {
        //                        List<int> TagIds1 = s.Split(',').Select(int.Parse).ToList();
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID && !TagIds1.Contains(p.eFileAttachmentId))
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        List<int> TagIds = s.Split(',').Select(int.Parse).ToList();
        //                        var query = (from a in ctx.eFileAttachments
        //                                     join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
        //                                     from ps in temp.DefaultIfEmpty()
        //                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                     from Ef in EFileTemp.DefaultIfEmpty()
        //                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                     where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && TagIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                     select new
        //                                     {
        //                                         eFileAttachmentId = a.eFileAttachmentId,
        //                                         Department = ps.deptname ?? "OTHER",
        //                                         eFileName = a.eFileName,
        //                                         PaperNature = a.PaperNature,
        //                                         PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                         eFilePath = a.eFilePath,
        //                                         Title = a.Title,
        //                                         Description = a.Description,
        //                                         CreateDate = a.CreatedDate,
        //                                         PaperStatus = a.PaperStatus,
        //                                         eFIleID = a.eFileID,
        //                                         PaperRefNo = a.PaperRefNo,
        //                                         DepartmentId = a.DepartmentId,
        //                                         OfficeCode = a.OfficeCode,
        //                                         ToDepartmentID = a.ToDepartmentID,
        //                                         ToOfficeCode = a.ToOfficeCode,
        //                                         ReplyStatus = a.ReplyStatus,
        //                                         ReplyDate = a.ReplyDate,
        //                                         ReceivedDate = a.ReceivedDate,
        //                                         DocumentTypeId = a.DocumentTypeId,
        //                                         ToPaperNature = a.ToPaperNature,
        //                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                         LinkedRefNo = a.LinkedRefNo,
        //                                         RevedOption = a.RevedOption,
        //                                         RecvDetails = a.RecvDetails,
        //                                         ToRecvDetails = a.ToRecvDetails,
        //                                         eMode = a.eMode,
        //                                         RecvdPaperNature = c.PaperNature,
        //                                         DocuemntPaperType = d.PaperType,
        //                                         PType = a.PType,
        //                                         SendStatus = a.SendStatus,
        //                                         DeptAbbr = ps.deptabbr,
        //                                         eFilePathWord = a.eFilePathWord,
        //                                         ReFileID = a.ReFileID,
        //                                         FileName = Ef.eFileNumber,
        //                                         DiaryNo = a.DiaryNo,
        //                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                         CurrentAadharId = a.CurrentEmpAadharID,
        //                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                        foreach (var item in query)
        //                        {
        //                            eFileAttachment mdl = new eFileAttachment();
        //                            mdl.DepartmentName = item.Department;
        //                            mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                            mdl.eFileName = item.eFileName;
        //                            mdl.PaperNature = item.PaperNature;
        //                            mdl.PaperNatureDays = item.PaperNatureDays;
        //                            mdl.eFilePath = item.eFilePath;
        //                            mdl.Title = item.Title;
        //                            mdl.Description = item.Description;
        //                            mdl.CreatedDate = item.CreateDate;
        //                            mdl.PaperStatus = item.PaperStatus;
        //                            mdl.eFileID = item.eFIleID;
        //                            mdl.PaperRefNo = item.PaperRefNo;
        //                            mdl.DepartmentId = item.DepartmentId;
        //                            mdl.OfficeCode = item.OfficeCode;
        //                            mdl.ToDepartmentID = item.ToDepartmentID;
        //                            mdl.ToOfficeCode = item.ToOfficeCode;
        //                            mdl.ReplyStatus = item.ReplyStatus;
        //                            mdl.ReplyDate = item.ReplyDate;
        //                            mdl.ReceivedDate = item.ReceivedDate;
        //                            mdl.DocumentTypeId = item.DocumentTypeId;

        //                            mdl.ToPaperNature = item.ToPaperNature;
        //                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                            mdl.LinkedRefNo = item.LinkedRefNo;
        //                            mdl.RevedOption = item.RevedOption;
        //                            mdl.RecvDetails = item.RecvDetails;
        //                            mdl.ToRecvDetails = item.ToRecvDetails;
        //                            mdl.eMode = item.eMode;
        //                            mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                            mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                            mdl.PType = item.PType;
        //                            mdl.SendStatus = item.SendStatus;
        //                            mdl.DeptAbbr = item.DeptAbbr;
        //                            mdl.eFilePathWord = item.eFilePathWord;
        //                            mdl.ReFileID = item.ReFileID;
        //                            mdl.MainEFileName = item.FileName;
        //                            mdl.DiaryNo = item.DiaryNo;
        //                            mdl.BranchName = item.BranchName;
        //                            mdl.EmpName = item.EmpName;
        //                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                            mdl.Desingnation = item.Desingnation;
        //                            mdl.AbbrevBranch = item.AbbrevBranch;
        //                            lst.Add(mdl);
        //                            Mainlst.Add(mdl);
        //                        }
        //                        if (V != "")
        //                        {
        //                            List<int> NullIds = V.Split(',').Select(int.Parse).ToList();
        //                            var query1 = (from a in ctx.eFileAttachments
        //                                          join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                                          from ps in temp.DefaultIfEmpty()
        //                                          join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                          from Ef in EFileTemp.DefaultIfEmpty()
        //                                          join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                          join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                          where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && NullIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                          select new
        //                                          {
        //                                              eFileAttachmentId = a.eFileAttachmentId,
        //                                              Department = ps.deptname ?? "OTHER",
        //                                              eFileName = a.eFileName,
        //                                              PaperNature = a.PaperNature,
        //                                              PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                              eFilePath = a.eFilePath,
        //                                              Title = a.Title,
        //                                              Description = a.Description,
        //                                              CreateDate = a.CreatedDate,
        //                                              PaperStatus = a.PaperStatus,
        //                                              eFIleID = a.eFileID,
        //                                              PaperRefNo = a.PaperRefNo,
        //                                              DepartmentId = a.DepartmentId,
        //                                              OfficeCode = a.OfficeCode,
        //                                              ToDepartmentID = a.ToDepartmentID,
        //                                              ToOfficeCode = a.ToOfficeCode,
        //                                              ReplyStatus = a.ReplyStatus,
        //                                              ReplyDate = a.ReplyDate,
        //                                              ReceivedDate = a.ReceivedDate,
        //                                              DocumentTypeId = a.DocumentTypeId,
        //                                              ToPaperNature = a.ToPaperNature,
        //                                              ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                              LinkedRefNo = a.LinkedRefNo,
        //                                              RevedOption = a.RevedOption,
        //                                              RecvDetails = a.RecvDetails,
        //                                              ToRecvDetails = a.ToRecvDetails,
        //                                              eMode = a.eMode,
        //                                              RecvdPaperNature = c.PaperNature,
        //                                              DocuemntPaperType = d.PaperType,
        //                                              PType = a.PType,
        //                                              SendStatus = a.SendStatus,
        //                                              DeptAbbr = ps.deptabbr,
        //                                              eFilePathWord = a.eFilePathWord,
        //                                              ReFileID = a.ReFileID,
        //                                              FileName = Ef.eFileNumber,
        //                                              DiaryNo = a.DiaryNo,
        //                                              BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                              EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                              CurrentAadharId = a.CurrentEmpAadharID,
        //                                              AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                              Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                          }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                            foreach (var item in query1)
        //                            {
        //                                eFileAttachment mdl = new eFileAttachment();
        //                                mdl.DepartmentName = item.Department;
        //                                mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                                mdl.eFileName = item.eFileName;
        //                                mdl.PaperNature = item.PaperNature;
        //                                mdl.PaperNatureDays = item.PaperNatureDays;
        //                                mdl.eFilePath = null;
        //                                mdl.Title = item.Title;
        //                                mdl.Description = item.Description;
        //                                mdl.CreatedDate = item.CreateDate;
        //                                mdl.PaperStatus = item.PaperStatus;
        //                                mdl.eFileID = item.eFIleID;
        //                                mdl.PaperRefNo = item.PaperRefNo;
        //                                mdl.DepartmentId = item.DepartmentId;
        //                                mdl.OfficeCode = item.OfficeCode;
        //                                mdl.ToDepartmentID = item.ToDepartmentID;
        //                                mdl.ToOfficeCode = item.ToOfficeCode;
        //                                mdl.ReplyStatus = item.ReplyStatus;
        //                                mdl.ReplyDate = item.ReplyDate;
        //                                mdl.ReceivedDate = item.ReceivedDate;
        //                                mdl.DocumentTypeId = item.DocumentTypeId;

        //                                mdl.ToPaperNature = item.ToPaperNature;
        //                                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                                mdl.LinkedRefNo = item.LinkedRefNo;
        //                                mdl.RevedOption = item.RevedOption;
        //                                mdl.RecvDetails = item.RecvDetails;
        //                                mdl.ToRecvDetails = item.ToRecvDetails;
        //                                mdl.eMode = item.eMode;
        //                                mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                                mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                                mdl.PType = item.PType;
        //                                mdl.SendStatus = item.SendStatus;
        //                                mdl.DeptAbbr = item.DeptAbbr;
        //                                mdl.eFilePathWord = null;
        //                                mdl.ReFileID = item.ReFileID;
        //                                mdl.MainEFileName = item.FileName;
        //                                mdl.DiaryNo = item.DiaryNo;
        //                                mdl.BranchName = item.BranchName;
        //                                mdl.EmpName = item.EmpName;
        //                                mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                                mdl.Desingnation = item.Desingnation;
        //                                mdl.AbbrevBranch = item.AbbrevBranch;
        //                                // lst.Add(mdl);
        //                                Mainlst.Add(mdl);
        //                            }
        //                        }

        //                        return Mainlst;

        //                    }
        //                    else if (s != "" && PaperType == 1)
        //                    {
        //                        List<int> TagIds1 = s.Split(',').Select(int.Parse).ToList();
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID && !TagIds1.Contains(p.eFileAttachmentId))
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        List<int> TagIds = s.Split(',').Select(int.Parse).ToList();
        //                        var query = (from a in ctx.eFileAttachments
        //                                     join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
        //                                     from ps in temp.DefaultIfEmpty()
        //                                     join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                     from Ef in EFileTemp.DefaultIfEmpty()
        //                                     join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                     join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                     where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && TagIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                     select new
        //                                     {
        //                                         eFileAttachmentId = a.eFileAttachmentId,
        //                                         Department = ps.deptname ?? "OTHER",
        //                                         eFileName = a.eFileName,
        //                                         PaperNature = a.PaperNature,
        //                                         PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                         eFilePath = a.eFilePath,
        //                                         Title = a.Title,
        //                                         Description = a.Description,
        //                                         CreateDate = a.CreatedDate,
        //                                         PaperStatus = a.PaperStatus,
        //                                         eFIleID = a.eFileID,
        //                                         PaperRefNo = a.PaperRefNo,
        //                                         DepartmentId = a.DepartmentId,
        //                                         OfficeCode = a.OfficeCode,
        //                                         ToDepartmentID = a.ToDepartmentID,
        //                                         ToOfficeCode = a.ToOfficeCode,
        //                                         ReplyStatus = a.ReplyStatus,
        //                                         ReplyDate = a.ReplyDate,
        //                                         ReceivedDate = a.ReceivedDate,
        //                                         DocumentTypeId = a.DocumentTypeId,
        //                                         ToPaperNature = a.ToPaperNature,
        //                                         ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                         LinkedRefNo = a.LinkedRefNo,
        //                                         RevedOption = a.RevedOption,
        //                                         RecvDetails = a.RecvDetails,
        //                                         ToRecvDetails = a.ToRecvDetails,
        //                                         eMode = a.eMode,
        //                                         RecvdPaperNature = c.PaperNature,
        //                                         DocuemntPaperType = d.PaperType,
        //                                         PType = a.PType,
        //                                         SendStatus = a.SendStatus,
        //                                         DeptAbbr = ps.deptabbr,
        //                                         eFilePathWord = a.eFilePathWord,
        //                                         ReFileID = a.ReFileID,
        //                                         FileName = Ef.eFileNumber,
        //                                         DiaryNo = a.DiaryNo,
        //                                         BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                         EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                         CurrentAadharId = a.CurrentEmpAadharID,
        //                                         AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                         Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                     }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                        foreach (var item in query)
        //                        {
        //                            eFileAttachment mdl = new eFileAttachment();
        //                            mdl.DepartmentName = item.Department;
        //                            mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                            mdl.eFileName = item.eFileName;
        //                            mdl.PaperNature = item.PaperNature;
        //                            mdl.PaperNatureDays = item.PaperNatureDays;
        //                            mdl.eFilePath = item.eFilePath;
        //                            mdl.Title = item.Title;
        //                            mdl.Description = item.Description;
        //                            mdl.CreatedDate = item.CreateDate;
        //                            mdl.PaperStatus = item.PaperStatus;
        //                            mdl.eFileID = item.eFIleID;
        //                            mdl.PaperRefNo = item.PaperRefNo;
        //                            mdl.DepartmentId = item.DepartmentId;
        //                            mdl.OfficeCode = item.OfficeCode;
        //                            mdl.ToDepartmentID = item.ToDepartmentID;
        //                            mdl.ToOfficeCode = item.ToOfficeCode;
        //                            mdl.ReplyStatus = item.ReplyStatus;
        //                            mdl.ReplyDate = item.ReplyDate;
        //                            mdl.ReceivedDate = item.ReceivedDate;
        //                            mdl.DocumentTypeId = item.DocumentTypeId;

        //                            mdl.ToPaperNature = item.ToPaperNature;
        //                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                            mdl.LinkedRefNo = item.LinkedRefNo;
        //                            mdl.RevedOption = item.RevedOption;
        //                            mdl.RecvDetails = item.RecvDetails;
        //                            mdl.ToRecvDetails = item.ToRecvDetails;
        //                            mdl.eMode = item.eMode;
        //                            mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                            mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                            mdl.PType = item.PType;
        //                            mdl.SendStatus = item.SendStatus;
        //                            mdl.DeptAbbr = item.DeptAbbr;
        //                            mdl.eFilePathWord = item.eFilePathWord;
        //                            mdl.ReFileID = item.ReFileID;
        //                            mdl.MainEFileName = item.FileName;
        //                            mdl.DiaryNo = item.DiaryNo;
        //                            mdl.BranchName = item.BranchName;
        //                            mdl.EmpName = item.EmpName;
        //                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                            mdl.Desingnation = item.Desingnation;
        //                            mdl.AbbrevBranch = item.AbbrevBranch;
        //                            lst.Add(mdl);
        //                            Mainlst.Add(mdl);
        //                        }
        //                        return lst;
        //                    }

        //                    else
        //                    {
        //                        V = string.Join(",", ctx.tMovementFile.Where(p => p.BranchId == CBranchID)
        //                                 .Select(p => p.eFileAttachmentId.ToString()));
        //                        List<int> NullIds = V.Split(',').Select(int.Parse).ToList();
        //                        var query1 = (from a in ctx.eFileAttachments
        //                                      join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
        //                                      from ps in temp.DefaultIfEmpty()
        //                                      join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                                      from Ef in EFileTemp.DefaultIfEmpty()
        //                                      join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                                      join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                      where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false && NullIds.Contains(a.eFileAttachmentId) && a.CurrentBranchId == CBranchID

        //                                      select new
        //                                      {
        //                                          eFileAttachmentId = a.eFileAttachmentId,
        //                                          Department = ps.deptname ?? "OTHER",
        //                                          eFileName = a.eFileName,
        //                                          PaperNature = a.PaperNature,
        //                                          PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                          eFilePath = a.eFilePath,
        //                                          Title = a.Title,
        //                                          Description = a.Description,
        //                                          CreateDate = a.CreatedDate,
        //                                          PaperStatus = a.PaperStatus,
        //                                          eFIleID = a.eFileID,
        //                                          PaperRefNo = a.PaperRefNo,
        //                                          DepartmentId = a.DepartmentId,
        //                                          OfficeCode = a.OfficeCode,
        //                                          ToDepartmentID = a.ToDepartmentID,
        //                                          ToOfficeCode = a.ToOfficeCode,
        //                                          ReplyStatus = a.ReplyStatus,
        //                                          ReplyDate = a.ReplyDate,
        //                                          ReceivedDate = a.ReceivedDate,
        //                                          DocumentTypeId = a.DocumentTypeId,
        //                                          ToPaperNature = a.ToPaperNature,
        //                                          ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                          LinkedRefNo = a.LinkedRefNo,
        //                                          RevedOption = a.RevedOption,
        //                                          RecvDetails = a.RecvDetails,
        //                                          ToRecvDetails = a.ToRecvDetails,
        //                                          eMode = a.eMode,
        //                                          RecvdPaperNature = c.PaperNature,
        //                                          DocuemntPaperType = d.PaperType,
        //                                          PType = a.PType,
        //                                          SendStatus = a.SendStatus,
        //                                          DeptAbbr = ps.deptabbr,
        //                                          eFilePathWord = a.eFilePathWord,
        //                                          ReFileID = a.ReFileID,
        //                                          FileName = Ef.eFileNumber,
        //                                          DiaryNo = a.DiaryNo,
        //                                          BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                          EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                          AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                                          Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                                          CurrentAadharId = a.CurrentEmpAadharID,
        //                                      }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                        foreach (var item in query1)
        //                        {
        //                            eFileAttachment mdl = new eFileAttachment();
        //                            mdl.DepartmentName = item.Department;
        //                            mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                            mdl.eFileName = item.eFileName;
        //                            mdl.PaperNature = item.PaperNature;
        //                            mdl.PaperNatureDays = item.PaperNatureDays;
        //                            mdl.eFilePath = null;
        //                            mdl.Title = item.Title;
        //                            mdl.Description = item.Description;
        //                            mdl.CreatedDate = item.CreateDate;
        //                            mdl.PaperStatus = item.PaperStatus;
        //                            mdl.eFileID = item.eFIleID;
        //                            mdl.PaperRefNo = item.PaperRefNo;
        //                            mdl.DepartmentId = item.DepartmentId;
        //                            mdl.OfficeCode = item.OfficeCode;
        //                            mdl.ToDepartmentID = item.ToDepartmentID;
        //                            mdl.ToOfficeCode = item.ToOfficeCode;
        //                            mdl.ReplyStatus = item.ReplyStatus;
        //                            mdl.ReplyDate = item.ReplyDate;
        //                            mdl.ReceivedDate = item.ReceivedDate;
        //                            mdl.DocumentTypeId = item.DocumentTypeId;

        //                            mdl.ToPaperNature = item.ToPaperNature;
        //                            mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                            mdl.LinkedRefNo = item.LinkedRefNo;
        //                            mdl.RevedOption = item.RevedOption;
        //                            mdl.RecvDetails = item.RecvDetails;
        //                            mdl.ToRecvDetails = item.ToRecvDetails;
        //                            mdl.eMode = item.eMode;
        //                            mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                            mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                            mdl.PType = item.PType;
        //                            mdl.SendStatus = item.SendStatus;
        //                            mdl.DeptAbbr = item.DeptAbbr;
        //                            mdl.eFilePathWord = null;
        //                            mdl.ReFileID = item.ReFileID;
        //                            mdl.MainEFileName = item.FileName;
        //                            mdl.DiaryNo = item.DiaryNo;
        //                            mdl.BranchName = item.BranchName;
        //                            mdl.EmpName = item.EmpName;
        //                            mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                            mdl.Desingnation = item.Desingnation;
        //                            mdl.AbbrevBranch = item.AbbrevBranch;
        //                            // lst.Add(mdl);
        //                            Mainlst.Add(mdl);
        //                        }


        //                        return Mainlst;

        //                    }
        //                }
        //                else //For Others Deparments
        //                {
        //                    List<eFileAttachment> Mainlst = new List<eFileAttachment>();
        //                    List<eFileAttachment> lst = new List<eFileAttachment>();
        //                    var query = (from a in ctx.eFileAttachments
        //                                 join b in ctx.departments on a.ToDepartmentID equals b.deptId
        //                                 into temp
        //                                 from ps in temp.DefaultIfEmpty()
        //                                 join c in ctx.eFilePaperNature on a.PaperNature equals c.PaperNatureID
        //                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                                 where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false 
        //                    //var query = (from a in ctx.eFileAttachments
        //                    //             join b in ctx.departments on a.DepartmentId equals b.deptId into temp
        //                    //             from ps in temp.DefaultIfEmpty()
        //                    //             join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                    //             from Ef in EFileTemp.DefaultIfEmpty()
        //                    //             join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                    //             join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                    //             where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false
        //                                 select new
        //                                 {
        //                                     eFileAttachmentId = a.eFileAttachmentId,
        //                                     Department = ps.deptname ?? "OTHER",
        //                                     eFileName = a.eFileName,
        //                                     PaperNature = a.PaperNature,
        //                                     PaperNatureDays = a.PaperNatureDays ?? "0",
        //                                     eFilePath = a.eFilePath,
        //                                     Title = a.Title,
        //                                     Description = a.Description,
        //                                     CreateDate = a.CreatedDate,
        //                                     PaperStatus = a.PaperStatus,
        //                                     eFIleID = a.eFileID,
        //                                     PaperRefNo = a.PaperRefNo,
        //                                     DepartmentId = a.DepartmentId,
        //                                     OfficeCode = a.OfficeCode,
        //                                     ToDepartmentID = a.ToDepartmentID,
        //                                     ToOfficeCode = a.ToOfficeCode,
        //                                     ReplyStatus = a.ReplyStatus,
        //                                     ReplyDate = a.ReplyDate,
        //                                     ReceivedDate = a.ReceivedDate,
        //                                     DocumentTypeId = a.DocumentTypeId,
        //                                     ToPaperNature = a.ToPaperNature,
        //                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                                     LinkedRefNo = a.LinkedRefNo,
        //                                     RevedOption = a.RevedOption,
        //                                     RecvDetails = a.RecvDetails,
        //                                     ToRecvDetails = a.ToRecvDetails,
        //                                     eMode = a.eMode,
        //                                     RecvdPaperNature = c.PaperNature,
        //                                     DocuemntPaperType = d.PaperType,
        //                                     PType = a.PType,
        //                                     SendStatus = a.SendStatus,
        //                                     DeptAbbr = ps.deptabbr,
        //                                     eFilePathWord = a.eFilePathWord,
        //                                     ReFileID = a.ReFileID,
        //                                     SendDate = a.SendDate,
        //                                    // DiaryNo = a.DiaryNo,
        //                                     //BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                                     //EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                                     //CurrentAadharId = a.CurrentEmpAadharID
        //                                 }).ToList().OrderByDescending(a => a.eFileAttachmentId);

        //                    foreach (var item in query)
        //                    {
        //                        eFileAttachment mdl = new eFileAttachment();
        //                        mdl.DepartmentName = item.Department;
        //                        mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                        mdl.eFileName = item.eFileName;
        //                        mdl.PaperNature = item.PaperNature;
        //                        mdl.PaperNatureDays = item.PaperNatureDays;
        //                        mdl.eFilePath = item.eFilePath;
        //                        mdl.Title = item.Title;
        //                        mdl.Description = item.Description;
        //                        mdl.CreatedDate = item.CreateDate;
        //                        mdl.PaperStatus = item.PaperStatus;
        //                        mdl.eFileID = item.eFIleID;
        //                        mdl.PaperRefNo = item.PaperRefNo;
        //                        mdl.DepartmentId = item.DepartmentId;
        //                        mdl.OfficeCode = item.OfficeCode;
        //                        mdl.ToDepartmentID = item.ToDepartmentID;
        //                        mdl.ToOfficeCode = item.ToOfficeCode;
        //                        mdl.ReplyStatus = item.ReplyStatus;
        //                        mdl.ReplyDate = item.ReplyDate;
        //                        mdl.ReceivedDate = item.ReceivedDate;
        //                        mdl.DocumentTypeId = item.DocumentTypeId;

        //                        mdl.ToPaperNature = item.ToPaperNature;
        //                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                        mdl.LinkedRefNo = item.LinkedRefNo;
        //                        mdl.RevedOption = item.RevedOption;
        //                        mdl.RecvDetails = item.RecvDetails;
        //                        mdl.ToRecvDetails = item.ToRecvDetails;
        //                        mdl.eMode = item.eMode;
        //                        mdl.RecvdPaperNature = item.RecvdPaperNature;
        //                        mdl.SendDate = item.SendDate;
        //                        mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                        mdl.PType = item.PType;
        //                        mdl.SendStatus = item.SendStatus;
        //                        mdl.DeptAbbr = item.DeptAbbr;
        //                        mdl.eFilePathWord = item.eFilePathWord;
        //                        mdl.ReFileID = item.ReFileID;
        //                       // mdl.MainEFileName = item.FileName;
        //                      //  mdl.DiaryNo = item.DiaryNo;
        //                        //mdl.BranchName = item.BranchName;
        //                        //mdl.EmpName = item.EmpName;
        //                        //mdl.CurrentEmpAadharID = item.CurrentAadharId;
        //                        lst.Add(mdl);
        //                        Mainlst.Add(mdl);
        //                    }
        //                    return Mainlst;



        //                }


        //            }

        //        }
        //    }
        //    else
        //    {
        //        using (eFileContext ctx = new eFileContext())
        //        {
        //            var query = (from a in ctx.eFileAttachments
        //                         join b in ctx.departments on a.ToDepartmentID equals b.deptId into temp
        //                         from ps in temp.DefaultIfEmpty()
        //                         join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
        //                         from Ef in EFileTemp.DefaultIfEmpty()
        //                         join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
        //                         join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
        //                         where deptlist.Contains(a.DepartmentId) && a.IsDeleted == false// && a.ToOfficeCode == Officecode
        //                         select new
        //                         {
        //                             eFileAttachmentId = a.eFileAttachmentId,
        //                             Department = ps.deptname ?? "OTHER",
        //                             eFileName = a.eFileName,
        //                             PaperNature = a.PaperNature,
        //                             PaperNatureDays = a.PaperNatureDays ?? "0",
        //                             eFilePath = a.eFilePath,
        //                             Title = a.Title,
        //                             Description = a.Description,
        //                             CreateDate = a.CreatedDate,
        //                             PaperStatus = a.PaperStatus,
        //                             eFIleID = a.eFileID,
        //                             PaperRefNo = a.PaperRefNo,
        //                             DepartmentId = a.DepartmentId,
        //                             OfficeCode = a.OfficeCode,
        //                             ToDepartmentID = a.ToDepartmentID,
        //                             ToOfficeCode = a.ToOfficeCode,
        //                             ReplyStatus = a.ReplyStatus,
        //                             ReplyDate = a.ReplyDate,
        //                             ReceivedDate = a.ReceivedDate,
        //                             DocumentTypeId = a.DocumentTypeId,

        //                             ToPaperNature = a.ToPaperNature,
        //                             ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
        //                             LinkedRefNo = a.LinkedRefNo,
        //                             RevedOption = a.RevedOption,
        //                             RecvDetails = a.RecvDetails,
        //                             ToRecvDetails = a.ToRecvDetails,
        //                             eMode = a.eMode,
        //                             RecvdPaperNature = c.PaperNature,
        //                             DocuemntPaperType = d.PaperType,
        //                             PType = a.PType,
        //                             SendStatus = a.SendStatus,
        //                             DeptAbbr = ps.deptabbr,
        //                             eFilePathWord = a.eFilePathWord,
        //                             ReFileID = a.ReFileID,
        //                             FileName = Ef.eFileNumber,
        //                             DiaryNo = a.DiaryNo
        //                         }).ToList().OrderByDescending(a => a.eFileAttachmentId);
        //            List<eFileAttachment> lst = new List<eFileAttachment>();
        //            foreach (var item in query)
        //            {
        //                eFileAttachment mdl = new eFileAttachment();
        //                mdl.DepartmentName = item.Department;
        //                mdl.eFileAttachmentId = item.eFileAttachmentId;
        //                mdl.eFileName = item.eFileName;
        //                mdl.PaperNature = item.PaperNature;
        //                mdl.PaperNatureDays = item.PaperNatureDays;
        //                mdl.eFilePath = item.eFilePath;
        //                mdl.Title = item.Title;
        //                mdl.Description = item.Description;
        //                mdl.CreatedDate = item.CreateDate;
        //                mdl.PaperStatus = item.PaperStatus;
        //                mdl.eFileID = item.eFIleID;
        //                mdl.PaperRefNo = item.PaperRefNo;
        //                mdl.DepartmentId = item.DepartmentId;
        //                mdl.OfficeCode = item.OfficeCode;
        //                mdl.ToDepartmentID = item.ToDepartmentID;
        //                mdl.ToOfficeCode = item.ToOfficeCode;
        //                mdl.ReplyStatus = item.ReplyStatus;
        //                mdl.ReplyDate = item.ReplyDate;
        //                mdl.ReceivedDate = item.ReceivedDate;
        //                mdl.DocumentTypeId = item.DocumentTypeId;

        //                mdl.ToPaperNature = item.ToPaperNature;
        //                mdl.ToPaperNatureDays = item.ToPaperNatureDays;
        //                mdl.LinkedRefNo = item.LinkedRefNo;
        //                mdl.RevedOption = item.RevedOption;
        //                mdl.RecvDetails = item.RecvDetails;
        //                mdl.ToRecvDetails = item.ToRecvDetails;
        //                mdl.eMode = item.eMode;
        //                mdl.RecvdPaperNature = item.RecvdPaperNature;

        //                mdl.DocuemntPaperType = item.DocuemntPaperType;
        //                mdl.PType = item.PType;
        //                mdl.SendStatus = item.SendStatus;
        //                mdl.DeptAbbr = item.DeptAbbr;
        //                mdl.eFilePathWord = item.eFilePathWord;
        //                mdl.ReFileID = item.ReFileID;
        //                mdl.MainEFileName = item.FileName;
        //                mdl.DiaryNo = item.DiaryNo;
        //                lst.Add(mdl);
        //            }

        //            return lst;
        //        }
        //    }
        //}
        //public static object NotingMovementListFiles(object param)
        //{
        //    eFileAttachment model = param as eFileAttachment;
        //    eFileContext pCtxt = new eFileContext();
        //    var Q = (from Efile in pCtxt.FileNotingMovement
        //             where Efile.FileId == model.eFileID
        //             select Efile).Count();
        //    if (Q != 0)
        //    {
        //        var query = (from Movemnt in pCtxt.FileNotingMovement
        //                     join SN in pCtxt.users on Movemnt.AssignfrmAadharId equals SN.AadarId
        //                     join MA in pCtxt.tDrafteFile on Movemnt.eFileAttachmentId equals MA.eFileAttachmentId
        //                     join EFile in pCtxt.eFiles on MA.ReFileID equals EFile.eFileID into EFileTemp
        //                     from Ef in EFileTemp.DefaultIfEmpty()
        //                     where (Movemnt.FileId == model.eFileID)
        //                     select new HouseComModel
        //                     {
        //                         ID = Movemnt.ID,
        //                         efileID = Movemnt.eFileAttachmentId,
        //                         BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
        //                         EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
        //                         AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
        //                         Remarks = Movemnt.Noting,
        //                         AssignDateTime = Movemnt.AssignDateTime,
        //                         Subject = Ef.eFileSubject,
        //                         eFileName = Ef.eFileNumber,
        //                         SignPath = (from mc in pCtxt.users where mc.AadarId == Movemnt.AssignfrmAadharId select mc.SignaturePath).FirstOrDefault(),
        //                     }).OrderByDescending(a => a.AssignDateTime).ToList();

        //        model.HouseComModel = query.ToList();
        //    }

        //    return model;
        //}

        public static object NotingMovementListFiles(object param)
        {
            eFileAttachment model = param as eFileAttachment;
            eFileContext pCtxt = new eFileContext();
            var Q = (from Efile in pCtxt.FileNotingMovement
                     where Efile.FileId == model.eFileID
                     select Efile).Count();
            if (Q != 0)
            {

                var query = (from Movemnt in pCtxt.FileNotingMovement
                             join SN in pCtxt.users on Movemnt.AssignfrmAadharId equals SN.AadarId
                             //join MA in pCtxt.tDrafteFile on Movemnt.DraftId equals MA.ID   //&& MA.eFileAttachmentId =  
                             join EFile in pCtxt.eFiles on Movemnt.FileId equals EFile.eFileID into EFileTemp
                             from Ef in EFileTemp.DefaultIfEmpty()
                             where (Movemnt.FileId == model.eFileID && Movemnt.FileId != 0)
                             // where (Movemnt.FileId != 0)
                             select new HouseComModel
                             {
                                 ID = Movemnt.ID,
                                 efileID = Movemnt.eFileAttachmentId,
                                 BranchName = (from mc in pCtxt.mBranches where mc.BranchId == Movemnt.BranchId select mc.BranchName).FirstOrDefault(),
                                 EmpName = Movemnt.EmpDesignation + "(" + Movemnt.EmpName + ")",
                                 AssignfrmName = Movemnt.AssignfrmDesignation + "(" + Movemnt.AssignfrmName + ")",
                                 Remarks = Movemnt.Noting,
                                 AssignDateTime = Movemnt.AssignDateTime,
                                 Subject = Ef.eFileSubject,
                                 eFileName = Ef.eFileNumber,
                                 SignPath = (from mc in pCtxt.users where mc.AadarId == Movemnt.AssignfrmAadharId select mc.SignaturePath).FirstOrDefault(),
                                 MarkedAadharId = Movemnt.EmpAadharId,
                                 MarkedName = Movemnt.EmpName,
                                 MarkedDesignation = Movemnt.EmpDesignation,
                                 //MarkedAadharId = Movemnt.AssignfrmAadharId,//.EmpAadharId,
                                 //MarkedName = Movemnt.AssignfrmName,//.EmpName,
                                 //MarkedDesignation = Movemnt.AssignfrmDesignation,//.EmpDesignation,

                             }).OrderBy(a => a.AssignDateTime).ToList();

                model.HouseComModel = query.ToList();
            }

            return model;
        }

        public static object GetBranchId(object param)
        {

            eFileContext context = new eFileContext();
            mBranches model = param as mBranches;
            var list = (from iD in context.eFileAttachments where iD.eFileAttachmentId == model.ID select iD.CurrentBranchId).FirstOrDefault();
            return list;

        }

        public static object GetBranchIdDraftList(object param)
        {

            eFileContext context = new eFileContext();
            mBranches model = param as mBranches;
            var list = (from iD in context.tDrafteFile where iD.ID == model.ID select iD.CurrentBranchId).FirstOrDefault();
            return list;

        }

        public static object GetDataPathsforMerge(object param)
        {

            eFileAttachment model = param as eFileAttachment;
            eFileContext pCtxt = new eFileContext();
            var Q = (from Draft in pCtxt.tDrafteFile
                     where Draft.ID == model.eFileAttachmentId
                     select Draft).FirstOrDefault();
            model.eFilePathPDFN = Q.eFilePathPDF;
            model.eFilePathWordN = Q.eFilePathDOC;
            //  model.PaperDraft = Q.PaperDraft;
            model.CurrentBranchId = Q.CurrentBranchId;
            return model;
        }


        public static object GetDetailsSentList(object param)
        {
            string[] str = param as string[];
            int draftId = Convert.ToInt32(str[4]);
            int Committee_id = Convert.ToInt32(str[5]);
            using (eFileContext ctx = new eFileContext())
            {

                List<eFileAttachment> Mainlst = new List<eFileAttachment>();
                var queryNull = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where a.DraftId == draftId && a.CommitteeId == Committee_id
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,
                                     SendDate = a.SendDate,
                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo,
                                     DraftId = a.DraftId,
                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                 }).OrderByDescending(a => a.eFileAttachmentId).ToList();
                List<eFileAttachment> lst1 = new List<eFileAttachment>();
                foreach (var item in queryNull)
                {
                    eFileAttachment mdl = new eFileAttachment();
                    mdl.DepartmentName = item.Department;
                    mdl.eFileAttachmentId = item.eFileAttachmentId;
                    mdl.eFileName = item.eFileName;
                    mdl.PaperNature = item.PaperNature;
                    mdl.PaperNatureDays = item.PaperNatureDays;
                    mdl.eFilePath = item.eFilePath;
                    mdl.Title = item.Title;
                    mdl.Description = item.Description;
                    mdl.CreatedDate = item.CreateDate;
                    mdl.PaperStatus = item.PaperStatus;
                    mdl.eFileID = item.eFIleID;
                    mdl.PaperRefNo = item.PaperRefNo;
                    mdl.DepartmentId = item.DepartmentId;
                    mdl.OfficeCode = item.OfficeCode;
                    mdl.ToDepartmentID = item.ToDepartmentID;
                    mdl.ToOfficeCode = item.ToOfficeCode;
                    mdl.ReplyStatus = item.ReplyStatus;
                    mdl.ReplyDate = item.ReplyDate;
                    mdl.ReceivedDate = item.ReceivedDate;
                    mdl.DocumentTypeId = item.DocumentTypeId;

                    mdl.ToPaperNature = item.ToPaperNature;
                    mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                    mdl.LinkedRefNo = item.LinkedRefNo;
                    mdl.RevedOption = item.RevedOption;
                    mdl.RecvDetails = item.RecvDetails;
                    mdl.ToRecvDetails = item.ToRecvDetails;
                    mdl.eMode = item.eMode;
                    mdl.RecvdPaperNature = item.RecvdPaperNature;
                    mdl.SendDate = item.SendDate;
                    mdl.DocuemntPaperType = item.DocuemntPaperType;
                    mdl.PType = item.PType;
                    mdl.SendStatus = item.SendStatus;
                    mdl.DeptAbbr = item.DeptAbbr;
                    mdl.eFilePathWord = item.eFilePathWord;
                    mdl.ReFileID = item.ReFileID;
                    mdl.MainEFileName = item.FileName;
                    mdl.DiaryNo = item.DiaryNo;
                    mdl.BranchName = item.BranchName;
                    mdl.EmpName = item.EmpName;
                    mdl.Desingnation = item.Desingnation;
                    mdl.AbbrevBranch = item.AbbrevBranch;
                    mdl.DraftId = item.DraftId;
                    lst1.Add(mdl);
                    Mainlst.Add(mdl);
                }
                return Mainlst;
            }

        }

        public static object GetFileItemList(object param)
        {
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.mCommitteeReplyItemType where _ObjeFileContext.IsActive == true select _ObjeFileContext).ToList().OrderBy(x => x.ItemTypeName).ToList();

            return query;
        }


        public static object NewDraftEntry(object param)
        {
            try
            {

                eFileAttachment model = param as eFileAttachment;
                FileNotingMovement MoveObj = new FileNotingMovement();
                eFileContext ctx = new eFileContext();
                var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == model.DraftedBy select AF).FirstOrDefault();
                var AssgnTo = (from AT in ctx.mStaff where AT.AadharID == model.DraftedBy select AT).FirstOrDefault();
                tDrafteFile obj = new tDrafteFile();
                tDrafteFile UpdateObj = new tDrafteFile();
                obj.DocumentTypeId = model.DocumentTypeId;
                obj.Description = model.Title;
                obj.CreatedDate = model.CreatedDate;
                obj.DraftBy = model.DraftedBy;
                obj.DraftDate = model.CreatedDate;
                obj.DepartmentId = model.ToDepIds;
                obj.Title = model.Title;
                obj.eFileNameId = model.eFileNameId;
                obj.PaperNature = model.PaperNature;
                obj.ToDepartmentID = model.ToDepartmentID;
                obj.PaperStatus = "Y";
                obj.SendDate = model.CreatedDate;
                obj.PType = "O";
                obj.ReceivedDate = model.CreatedDate;
                obj.ReplyDate = model.CreatedDate;
                obj.PaperRefNo = null;
                obj.ToPaperNature = model.ToPaperNature;
                obj.eMode = 1;
                obj.CurrentBranchId = model.CurrentBranchId;
                //  obj.CurrentBranchId = model.CurrentBranchId;
                obj.CurrentEmpAadharID = model.DraftedBy;
                obj.AssignDateTime = model.CreatedDate;
                obj.eFilePathPDF = model.eFilePath;
                obj.eFilePathDOC = model.eFilePathWord;
                obj.TODepts = model.ToDepIds;
                obj.CCDepts = model.CCDepIds;
                obj.TOMembers = model.ToMemIds;
                obj.CCMembers = model.CCMemIds;
                obj.ToOthers = model.ToOtherIds;
                obj.CCOthers = model.CCOtherIds;
                obj.ToAllRecipName = model.ToListComplN;
                obj.ToAllRecipIds = model.ToListComplIds;
                obj.CCAllRecipIds = model.CCListComplIds;
                obj.CCAllRecipName = model.CCListComplN;
                obj.CountAPS = model.CountsAPS;
                obj.ItemID = model.ItemTypeName;
                obj.ToPaperNature = model.PaperNature;
                obj.isDraft = true;
                obj.PaperRefrenceManualy = model.PaperRefrenceManualy;
                //  obj.PaperDraft = model.PaperDraft;
                //   obj.ItemName = model.ItemTypeName;
                ctx.tDrafteFile.Add(obj);
                ctx.SaveChanges();
                tDrafteFile PaperObj = ctx.tDrafteFile.Single(m => m.ID == obj.ID);
                PaperObj.DraftId = obj.ID;
                ctx.SaveChanges();
                MoveObj.BranchId = model.CurrentBranchId;
                MoveObj.EmpAadharId = model.DraftedBy;
                MoveObj.Noting = "NOTINUSE";
                MoveObj.AssignDateTime = model.CreatedDate;
                MoveObj.EmpName = AssgnTo.StaffName;
                MoveObj.EmpDesignation = AssgnTo.Designation;
                MoveObj.AssignfrmAadharId = model.DraftedBy;
                MoveObj.AssignfrmName = Assgnfrom.StaffName;
                MoveObj.AssignfrmDesignation = Assgnfrom.Designation;
                MoveObj.FileId = 0;
                MoveObj.DraftId = obj.ID;
                ctx.FileNotingMovement.Add(MoveObj);
                ctx.SaveChanges();
                ctx.Close();

                return 1; //Updated
            }
            catch
            {
                return 0; //Error
            }
        }


        public static object AttachPapertoEFileMulDraftR(object param)
        {
            try
            {
                eFileAttachment mdl = param as eFileAttachment;
                SBL.DomainModel.Models.eFile.eFile modelfile = param as SBL.DomainModel.Models.eFile.eFile;

                using (eFileContext ctx = new eFileContext())
                {
                    for (int i = 0; i < mdl.eFileAttachmentIds.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(mdl.eFileAttachmentIds[i].ToString()))
                        {
                            mdl.eFileAttachmentId = mdl.eFileAttachmentIds[i];
                            tDrafteFile model = ctx.tDrafteFile.Single(a => a.ID == mdl.eFileAttachmentId);
                            if (mdl.SettingValue_file == "True")
                            {
                                SBL.DomainModel.Models.eFile.eFile obj = ctx.eFiles.Single(a => a.eFileID == mdl.ReFileID);
                                if (obj.FileAttched_DraftId == null || obj.FileAttched_DraftId == 0)
                                {
                                    //update tdraftfile
                                    model.ReFileID = mdl.ReFileID;
                                    model.eFileID = mdl.ReFileID;
                                    //update mefile
                                    obj.FileAttched_DraftId = model.DraftId;

                                    ctx.SaveChanges();


                                }
                                else
                                {
                                    return 2;
                                }
                            }
                            else
                            {
                                SBL.DomainModel.Models.eFile.eFile obj = ctx.eFiles.Single(a => a.eFileID == mdl.ReFileID);
                                model.ReFileID = mdl.ReFileID;
                                model.eFileID = mdl.ReFileID;
                                //update mefile
                                // obj.FileAttched_DraftId = model.DraftId;

                                ctx.SaveChanges();


                            }
                        }

                    }

                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }



        static object GetPendencyData(object param)
        {

            eFileContext qCtxDB = new eFileContext();
            eFileAttachment model = param as eFileAttachment;

            var V = (from Pendency in qCtxDB.eFileAttachments
                     where Pendency.ItemId == model.ItemId && Pendency.ItemNumber == model.ItemNumber && Pendency.ReplyRefNo == model.ReplyRefNo
                     select Pendency).Count();

            if (V == 0)
            {
                var VC = (from Pendency in qCtxDB.tCommitteReplyPendency
                          where Pendency.TypeNo == model.ItemNumber && Pendency.ItemTypeId == model.ItemId
                          select Pendency).Count();
                if (VC == 0)
                {
                    model.YearId = 0;
                    model.ReplyStatus = "Invalid No !!!";
                }
                else
                {
                    var Q = (from Pendency in qCtxDB.tCommitteReplyPendency
                             where Pendency.TypeNo == model.ItemNumber && Pendency.ItemTypeId == model.ItemId
                             select Pendency).FirstOrDefault();
                    if (Q != null)
                    {
                        model.ItemSubject = Q.Subject;
                        model.ItemDescription = Q.ItemDescription;
                        model.ItemNumber = Q.TypeNo;
                        model.YearId = 1;

                    }
                }


            }
            if (V != 0)
            {
                model.YearId = 2;
                model.ReplyStatus = "Reference No Already Replied !!!";
            }


            return model;
        }


        public static object GetDeptfodisply(object param)
        {

            eFileAttachment model = param as eFileAttachment;
            eFileContext pCtxt = new eFileContext();
            model.DepartmentName = (from d in pCtxt.departments where d.deptId == model.DepartmentId select d.deptname).FirstOrDefault();
            return model;
        }


        public static object NewDeptDraftEntry(object param)
        {
            try
            {
                eFileAttachment model = param as eFileAttachment;
                eFileContext ctx = new eFileContext();
                tDrafteFile obj = new tDrafteFile();
                obj.DocumentTypeId = model.DocumentTypeId;
                obj.Description = model.Title;
                obj.CreatedDate = model.CreatedDate;
                obj.DraftBy = model.DraftedBy;
                obj.DraftDate = model.CreatedDate;
                obj.DepartmentId = model.DepartmentId;
                obj.Title = model.Title;
                obj.eFileNameId = model.eFileNameId;
                obj.PaperNature = model.PaperNature;
                obj.ToDepartmentID = model.ToDepartmentID;
                obj.PaperStatus = "Y";
                obj.SendDate = model.CreatedDate;
                obj.PType = "O";
                obj.ReceivedDate = model.CreatedDate;
                obj.ReplyDate = model.CreatedDate;
                obj.PaperRefNo = null;
                obj.ToPaperNature = model.ToPaperNature;
                obj.eMode = 1;
                obj.CurrentBranchId = model.CurrentBranchId;
                obj.CurrentBranchId = model.CurrentBranchId;
                obj.CurrentEmpAadharID = model.DraftedBy;
                obj.AssignDateTime = model.CreatedDate;
                obj.eFilePathPDF = model.eFilePath;
                obj.eFilePathDOC = model.eFilePathWord;
                obj.TODepts = model.ToDepIds;
                obj.CCDepts = model.CCDepIds;
                obj.TOMembers = model.ToMemIds;
                obj.CCMembers = model.CCMemIds;
                obj.ToOthers = model.ToOtherIds;
                obj.CCOthers = model.CCOtherIds;
                obj.ToAllRecipName = model.ToListComplN;
                obj.ToAllRecipIds = model.ToListComplIds;
                obj.CCAllRecipIds = model.CCListComplIds;
                obj.CCAllRecipName = model.CCListComplN;
                obj.CountAPS = model.CountsAPS;
                obj.ItemID = model.ItemTypeName;
                obj.ToPaperNature = model.PaperNature;
                obj.isDraft = true;
                //   obj.ItemName = model.ItemTypeName;
                ctx.tDrafteFile.Add(obj);
                ctx.SaveChanges();
                return 1; //Updated
            }
            catch
            {
                return 0; //Error
            }



        }

        public static object GetComtbyBranch(object param)
        {
            eFileContext ctx = new eFileContext();
            SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
            int CommitteeId = (from m in ctx.tBranchesCommittee where m.BranchId == model.BranchId select m.CommitteeId).SingleOrDefault();
            return CommitteeId;

        }
        public static object GetBranchByCommitteeId(object param)
        {
            eFileContext ctx = new eFileContext();
            // SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
            COmmitteeProceeding mdl = param as COmmitteeProceeding;
            int CommitteeId = (from m in ctx.tBranchesCommittee where m.CommitteeId == mdl.CommitteeId select m.BranchId).SingleOrDefault();
            return CommitteeId;

        }



        private static object GetAuthorizeEmp(object p)
        {

            try
            {
                using (eFileContext SCtx = new eFileContext())
                {
                    eFileAttachments model = p as eFileAttachments;
                    var data = (from a in SCtx.AuthEmployee where a.AadharId == model.CurrentSessionAId select a).Count();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static object GetDesigNameByAadharId(object param)
        {
            eFileContext ctx = new eFileContext();
            SBL.DomainModel.Models.eFile.eFile model = param as SBL.DomainModel.Models.eFile.eFile;
            //  mBranches model = param as mBranches;
            string designame = (from m in ctx.mStaff where m.AadharID == model.AadharID select m.Designation).SingleOrDefault();
            return designame;

        }

        public static object GetReceivePaperForVerify(object param)
        {
            string[] str = param as string[];
            var V = "";
            string DeptId = Convert.ToString(str[0]);
            int Officecode = 0;
            int.TryParse(str[1], out Officecode);
            string Aadharid = str[2];

            int CommId = Convert.ToInt32(str[6]);

            int Year = Convert.ToInt32(str[4]);
            int PaperType = Convert.ToInt32(str[5]);
            string status = str[7];
            Boolean statusvalue = true;
            //if(status=="Ac")
            //{
            //    statusvalue = true;
            //}
            //else if (status == "Re")
            //{
            //    statusvalue = false;
            //}

            List<string> deptlist = new List<string>();
            if (!string.IsNullOrEmpty(DeptId))
            {
                deptlist = DeptId.Split(',').Distinct().ToList();
            }
            using (eFileContext ctx = new eFileContext())
            {
                if (status == "Ac")
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.CommitteeId == CommId && a.IsVerified == true
                                 //orderby a.eFileAttachmentId descending
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo,
                                     VerifiedBy = a.VerifiedBy,
                                     VerifiedDate = a.VerifiedDate,
                                     IsVerified = a.IsVerified,
                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                     CountsAPS = a.CountsAPS,
                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                     Annexurepath = a.AnnexPdfPath,
                                     IsRejected = a.IsRejected,
                                 }).OrderByDescending(x => x.eFileAttachmentId).ToList();//.OrderBy(x =>x.CreateDate).Reverse();
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.IsVerified = item.IsVerified;
                        mdl.VerifiedDate = item.VerifiedDate;
                        mdl.VerifiedBy = item.VerifiedBy;
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        mdl.BranchName = item.BranchName;
                        mdl.EmpName = item.EmpName;
                        mdl.Desingnation = item.Desingnation;
                        mdl.AbbrevBranch = item.AbbrevBranch;
                        mdl.ItemName = item.ItemName;
                        mdl.CountsAPS = item.CountsAPS;
                        mdl.AnnexPdfPath = item.Annexurepath;
                        mdl.IsRejected = item.IsRejected;
                        lst.Add(mdl);
                    }
                    return lst;
                }
                else if (status == "Re")
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.CommitteeId == CommId && a.IsRejected == true
                                 //orderby a.eFileAttachmentId descending
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo,
                                     VerifiedBy = a.VerifiedBy,
                                     VerifiedDate = a.VerifiedDate,
                                     IsVerified = a.IsVerified,
                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                     CountsAPS = a.CountsAPS,
                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                     Annexurepath = a.AnnexPdfPath,
                                     IsRejected = a.IsRejected,
                                 }).OrderByDescending(x => x.eFileAttachmentId).ToList();//.OrderBy(x =>x.CreateDate).Reverse();
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.IsVerified = item.IsVerified;
                        mdl.VerifiedDate = item.VerifiedDate;
                        mdl.VerifiedBy = item.VerifiedBy;
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        mdl.BranchName = item.BranchName;
                        mdl.EmpName = item.EmpName;
                        mdl.Desingnation = item.Desingnation;
                        mdl.AbbrevBranch = item.AbbrevBranch;
                        mdl.ItemName = item.ItemName;
                        mdl.CountsAPS = item.CountsAPS;
                        mdl.AnnexPdfPath = item.Annexurepath;
                        mdl.IsRejected = item.IsRejected;
                        lst.Add(mdl);
                    }
                    return lst;
                }
                else if (status == "Pe")
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.CommitteeId == CommId && a.IsVerified == false && a.IsRejected == false
                                 //orderby a.eFileAttachmentId descending
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo,
                                     VerifiedBy = a.VerifiedBy,
                                     VerifiedDate = a.VerifiedDate,
                                     IsVerified = a.IsVerified,
                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                     CountsAPS = a.CountsAPS,
                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                     Annexurepath = a.AnnexPdfPath,
                                     IsRejected = a.IsRejected,
                                 }).OrderByDescending(x => x.eFileAttachmentId).ToList();//.OrderBy(x =>x.CreateDate).Reverse();
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.IsVerified = item.IsVerified;
                        mdl.VerifiedDate = item.VerifiedDate;
                        mdl.VerifiedBy = item.VerifiedBy;
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        mdl.BranchName = item.BranchName;
                        mdl.EmpName = item.EmpName;
                        mdl.Desingnation = item.Desingnation;
                        mdl.AbbrevBranch = item.AbbrevBranch;
                        mdl.ItemName = item.ItemName;
                        mdl.CountsAPS = item.CountsAPS;
                        mdl.AnnexPdfPath = item.Annexurepath;
                        mdl.IsRejected = item.IsRejected;
                        lst.Add(mdl);
                    }
                    return lst;
                }
                else
                {
                    var query = (from a in ctx.eFileAttachments
                                 join b in ctx.departments on a.DepartmentId equals b.deptId into temp
                                 from ps in temp.DefaultIfEmpty()
                                 join EFile in ctx.eFiles on a.ReFileID equals EFile.eFileID into EFileTemp
                                 from Ef in EFileTemp.DefaultIfEmpty()
                                 join c in ctx.eFilePaperNature on a.ToPaperNature equals c.PaperNatureID
                                 join d in ctx.eFilePaperType on a.DocumentTypeId equals d.PaperTypeID
                                 where deptlist.Contains(a.ToDepartmentID) && a.IsDeleted == false && a.CommitteeId == CommId
                                 //orderby a.eFileAttachmentId descending
                                 select new
                                 {
                                     eFileAttachmentId = a.eFileAttachmentId,
                                     Department = ps.deptname ?? "OTHER",
                                     eFileName = a.eFileName,
                                     PaperNature = a.PaperNature,
                                     PaperNatureDays = a.PaperNatureDays ?? "0",
                                     eFilePath = a.eFilePath,
                                     Title = a.Title,
                                     Description = a.Description,
                                     CreateDate = a.CreatedDate,
                                     PaperStatus = a.PaperStatus,
                                     eFIleID = a.eFileID,
                                     PaperRefNo = a.PaperRefNo,
                                     DepartmentId = a.DepartmentId,
                                     OfficeCode = a.OfficeCode,
                                     ToDepartmentID = a.ToDepartmentID,
                                     ToOfficeCode = a.ToOfficeCode,
                                     ReplyStatus = a.ReplyStatus,
                                     ReplyDate = a.ReplyDate,
                                     ReceivedDate = a.ReceivedDate,
                                     DocumentTypeId = a.DocumentTypeId,

                                     ToPaperNature = a.ToPaperNature,
                                     ToPaperNatureDays = a.ToPaperNatureDays ?? "0",
                                     LinkedRefNo = a.LinkedRefNo,
                                     RevedOption = a.RevedOption,
                                     RecvDetails = a.RecvDetails,
                                     ToRecvDetails = a.ToRecvDetails,
                                     eMode = a.eMode,
                                     RecvdPaperNature = c.PaperNature,
                                     DocuemntPaperType = d.PaperType,
                                     PType = a.PType,
                                     SendStatus = a.SendStatus,
                                     DeptAbbr = ps.deptabbr,
                                     eFilePathWord = a.eFilePathWord,
                                     ReFileID = a.ReFileID,
                                     FileName = Ef.eFileNumber,
                                     DiaryNo = a.DiaryNo,
                                     VerifiedBy = a.VerifiedBy,
                                     VerifiedDate = a.VerifiedDate,
                                     IsVerified = a.IsVerified,
                                     BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                     EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                     AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                     Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                     CountsAPS = a.CountsAPS,
                                     ItemName = (from Item in ctx.mCommitteeReplyItemType where Item.Id == a.ItemId select Item.ItemTypeName).FirstOrDefault(),
                                     Annexurepath = a.AnnexPdfPath,
                                     IsRejected = a.IsRejected,
                                 }).OrderByDescending(x => x.eFileAttachmentId).ToList();//.OrderBy(x =>x.CreateDate).Reverse();
                    List<eFileAttachment> lst = new List<eFileAttachment>();
                    foreach (var item in query)
                    {
                        eFileAttachment mdl = new eFileAttachment();
                        mdl.IsVerified = item.IsVerified;
                        mdl.VerifiedDate = item.VerifiedDate;
                        mdl.VerifiedBy = item.VerifiedBy;
                        mdl.DepartmentName = item.Department;
                        mdl.eFileAttachmentId = item.eFileAttachmentId;
                        mdl.eFileName = item.eFileName;
                        mdl.PaperNature = item.PaperNature;
                        mdl.PaperNatureDays = item.PaperNatureDays;
                        mdl.eFilePath = item.eFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        mdl.CreatedDate = item.CreateDate;
                        mdl.PaperStatus = item.PaperStatus;
                        mdl.eFileID = item.eFIleID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.DepartmentId = item.DepartmentId;
                        mdl.OfficeCode = item.OfficeCode;
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ToOfficeCode = item.ToOfficeCode;
                        mdl.ReplyStatus = item.ReplyStatus;
                        mdl.ReplyDate = item.ReplyDate;
                        mdl.ReceivedDate = item.ReceivedDate;
                        mdl.DocumentTypeId = item.DocumentTypeId;

                        mdl.ToPaperNature = item.ToPaperNature;
                        mdl.ToPaperNatureDays = item.ToPaperNatureDays;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.RevedOption = item.RevedOption;
                        mdl.RecvDetails = item.RecvDetails;
                        mdl.ToRecvDetails = item.ToRecvDetails;
                        mdl.eMode = item.eMode;
                        mdl.RecvdPaperNature = item.RecvdPaperNature;

                        mdl.DocuemntPaperType = item.DocuemntPaperType;
                        mdl.PType = item.PType;
                        mdl.SendStatus = item.SendStatus;
                        mdl.DeptAbbr = item.DeptAbbr;
                        mdl.eFilePathWord = item.eFilePathWord;
                        mdl.ReFileID = item.ReFileID;
                        mdl.MainEFileName = item.FileName;
                        mdl.DiaryNo = item.DiaryNo;
                        mdl.BranchName = item.BranchName;
                        mdl.EmpName = item.EmpName;
                        mdl.Desingnation = item.Desingnation;
                        mdl.AbbrevBranch = item.AbbrevBranch;
                        mdl.ItemName = item.ItemName;
                        mdl.CountsAPS = item.CountsAPS;
                        mdl.AnnexPdfPath = item.Annexurepath;
                        mdl.IsRejected = item.IsRejected;
                        lst.Add(mdl);
                    }
                    return lst;
                }
            }
        }

        static object GetCommittees(object param)
        {
            SiteSettingContext settingContext = new SiteSettingContext();
            // CommitteeContext CommCtx = new CommitteeContext();
            eFileContext ctx = new eFileContext();
            var Assembly = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            Int32 AssemblyCode = Convert.ToInt32(Assembly);
            var query = (from cmt in ctx.committees

                         where (cmt.AssemblyID == AssemblyCode)
                         select new CommitteeSearchModel
                         //select new tCommittee
                         {
                             CommitteeId = cmt.CommitteeId,
                             CommitteeName = cmt.CommitteeName
                         }

                         );

            return query.ToList();
        }

        public static object NotingBacklog(object param)
        {
            eFileContext context = new eFileContext();
            //mBranches Obj = param as mBranches;
            eFileAttachment Obj = param as eFileAttachment;
            tMovementFile MoveObj = new tMovementFile();
            try
            {
                var Assgnfrom = (from AF in context.mStaff where AF.AadharID == Obj.CurrentEmpAadharID select AF).FirstOrDefault();
                MoveObj.eFileAttachmentId = Obj.eFileAttachmentId;
                MoveObj.BranchId = Obj.CurrentBranchId;
                MoveObj.EmpAadharId = Obj.CurrentEmpAadharID;
                //   MoveObj.Remarks = Obj.Remark;
                //  MoveObj.AssignDateTime = Obj.ModifiedDate;
                MoveObj.EmpName = Assgnfrom.StaffName;
                MoveObj.EmpDesignation = Assgnfrom.Designation;
                MoveObj.AssignfrmAadharId = Obj.CurrentEmpAadharID;
                MoveObj.AssignfrmName = Assgnfrom.StaffName;
                MoveObj.AssignfrmDesignation = Assgnfrom.Designation;
                context.tMovementFile.Add(MoveObj);
                context.SaveChanges();
                context.Close();

            }

            catch (Exception ex)
            {
                return 0;
            }

            return 1;
        }
        public static object Save_ReadDataInTable(object param)
        {
            eFileContext context = new eFileContext();
            ReadTableData obj = param as ReadTableData;
            try
            {
                int PrimarykeyId = (from rf in context.ReadTableData where rf.TableName == "teFileAttachment" && rf.TablePrimaryId == obj.TablePrimaryId && rf.UserAadharId == obj.UserAadharId select rf.TablePrimaryId).FirstOrDefault();
                if (PrimarykeyId == obj.TablePrimaryId)
                { }
                else
                {
                    obj.TableName = "teFileAttachment";
                    obj.ReadDateTime = DateTime.Now;
                    context.ReadTableData.Add(obj);
                    context.SaveChanges();
                    context.Close();
                }

            }

            catch (Exception ex)
            {
                return 0;
            }
            return 1;
        }
        public static object check_draftIdinEfile(object param)
        {

            eFileContext context = new eFileContext();
            mBranches obj = param as mBranches;
            SBL.DomainModel.Models.eFile.eFile obj1 = new DomainModel.Models.eFile.eFile();
            int? ExistDraftId = 0;
            try
            {
                // var Assgnfrom = (from AF in ctx.mStaff where AF.AadharID == model.DraftBy select AF).FirstOrDefault();  //ppp
                ExistDraftId = (from rf in context.eFiles where rf.eFileID == obj.efileID select rf.FileAttched_DraftId).FirstOrDefault();
                if (ExistDraftId == null)
                {
                    ExistDraftId = 0;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
            return ExistDraftId;
        }

        static object GetReadDataTableList(object param)
        {
            //string adharId = Convert.ToString(param);
            string[] ne = param as string[];
            string adharID = ne[2];
            int committeeID = Convert.ToInt16(ne[3]);
            eFileContext ctx = new eFileContext();
            if (committeeID == 0)
            {
                var list = (from a in ctx.ReadTableData
                            where a.UserAadharId == adharID
                            select a).ToList();
                return list;
            }
            else
            {
                var list = (from a in ctx.ReadTableData
                            join b in ctx.eFileAttachments on a.TablePrimaryId equals b.eFileAttachmentId
                            where a.UserAadharId == adharID && b.CommitteeId == committeeID
                            select a).ToList();
                return list;
            }
        }
        public static string GetMACAddress()
        {
            NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            String sMacAddress = string.Empty;
            foreach (NetworkInterface adapter in nics)
            {
                if (sMacAddress == String.Empty)// only return MAC Address from first card
                {
                    IPInterfaceProperties properties = adapter.GetIPProperties();
                    sMacAddress = adapter.GetPhysicalAddress().ToString();
                }
            } return sMacAddress;
        }

        public static string GetIPAddress()
        {

            try
            {

                string ipAddress = "";
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                ipAddress = Convert.ToString(ipHostInfo.AddressList.FirstOrDefault(address => address.AddressFamily == AddressFamily.InterNetwork));


                return ipAddress;
            }
            catch
            {
                return "";
            }

        }


        ////////////////////////////////////////////////////////////////////////////////////
        //For New House Committee Module
        ///////////////////////////////////////////////////////////////////////////////////

        //public static object GetCustomCommFiles(object param)
        //{
        //    List<pHouseCommitteeFiles> Parentlist = new List<pHouseCommitteeFiles>();
        //    List<pHouseCommitteeSubFiles> Childlist = new List<pHouseCommitteeSubFiles>();

        //    using (eFileContext ctx = new eFileContext())
        //    {
        //        var ParentList = (from a in ctx.pHouseCommitteeFiles
        //                          select new
        //                          {
        //                              Id = a.Id,
        //                              ParentId = a.Id,
        //                              eFileName = a.eFileName,
        //                              PaperNature = a.PaperNature,
        //                              eFilePath = a.eFilePath,
        //                              AnnexPdfPath = a.AnnexPdfPath,
        //                              Title = a.Title,
        //                              Description = a.Description,
        //                              CreateDate = a.CreatedDate,
        //                              eFIleID = a.eFileID,
        //                              PaperRefNo = a.PaperRefNo,
        //                              ByDeptId = a.ToDepartmentID,
        //                              ToOfficeCode = a.ToOfficeCode,
        //                              SelectedPaperDate = a.SelectedPaperDate,
        //                              ToDepartmentID = a.ToDepartmentID,
        //                              ToDepartmentName = (from mc in ctx.departments where mc.deptId == a.ToDepartmentID select mc.deptname).FirstOrDefault(),
        //                              ByDepartmentId = a.ByDepartmentId,
        //                              ByDepartmentName = (from mc in ctx.departments where mc.deptId == a.ByDepartmentId select mc.deptname).FirstOrDefault(),
        //                              ByOfficeCode = a.ByOfficeCode,
        //                              ItemName = a.ItemName,
        //                              ItemId = a.ItemId,
        //                              DocumentTypeId = a.DocumentTypeId,
        //                              LinkedRefNo = a.LinkedRefNo,
        //                              SendStatus = a.SendStatus,
        //                              BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
        //                              CommitteeName = (from mc in ctx.committees where mc.CommitteeId == a.CommitteeId select mc.CommitteeName).FirstOrDefault(),
        //                              CurrentAadharId = a.CurrentEmpAadharID,
        //                              EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
        //                              AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
        //                              Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
        //                          }).ToList().OrderByDescending(a => a.CreateDate);

        //        var CList = from element in ctx.pHouseCommitteeSubFiles
        //                    group element by element.ParentId
        //                        into groups
        //                        select groups.OrderByDescending(p => p.CreatedDate).FirstOrDefault();

        //        var ChildList = CList.ToList();
        //        foreach (var item1 in ChildList)
        //        {
        //            pHouseCommitteeSubFiles mdl = new pHouseCommitteeSubFiles();
        //            mdl.RecordId = item1.RecordId;
        //            mdl.eFileName = item1.eFileName;
        //            //mdl.PaperNature = item1.PaperNature;
        //            mdl.ParentId = item1.ParentId;
        //            mdl.CommitteeName = item1.CommitteeName;
        //            mdl.eFilePath = item1.eFilePath;
        //            mdl.AnnexPdfPath = item1.AnnexPdfPath;
        //            mdl.Title = item1.Title;
        //            mdl.Description = item1.Description;
        //            //mdl.ItemName=item1.
        //            mdl.ToDepartmentName = (from mc in ctx.departments where mc.deptId == item1.ToDepartmentID select mc.deptname).FirstOrDefault();
        //            mdl.ByDepartmentName = (from mc in ctx.departments where mc.deptId == item1.ByDepartmentId select mc.deptname).FirstOrDefault();
        //            mdl.CreatedDate = item1.CreatedDate;
        //            mdl.ItemId = item1.ItemId;
        //            mdl.ItemName = item1.ItemName;
        //            mdl.eFileID = item1.eFileID;
        //            mdl.PaperRefNo = item1.PaperRefNo;
        //            mdl.SelectedPaperDate = item1.SelectedPaperDate;
        //            //  mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreatedDate).ToString("dd/MM/yyyy");               
        //            mdl.ToDepartmentID = item1.ToDepartmentID;
        //            mdl.DocumentTypeId = item1.DocumentTypeId;
        //            mdl.PaperNature = item1.PaperNature;
        //            mdl.LinkedRefNo = item1.LinkedRefNo;
        //            mdl.SendStatus = item1.SendStatus;
        //            Childlist.Add(mdl);
        //        }
        //        foreach (var item1 in ParentList)
        //        {
        //            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
        //            mdl.Id = item1.Id;
        //            mdl.ParentId = item1.ParentId;
        //            mdl.eFileName = item1.eFileName;
        //            //mdl.PaperNature = item1.PaperNature;
        //            mdl.CommitteeName = item1.CommitteeName;
        //            mdl.eFilePath = item1.eFilePath;
        //            mdl.AnnexPdfPath = item1.AnnexPdfPath;
        //            mdl.Title = item1.Title;
        //            mdl.Description = item1.Description;
        //            //mdl.ItemName=item1.
        //            mdl.ToDepartmentName = item1.ToDepartmentName;
        //            mdl.ByDepartmentName = item1.ByDepartmentName;
        //            mdl.CreatedDate = item1.CreateDate;
        //            mdl.ItemId = item1.ItemId;
        //            mdl.ItemName = item1.ItemName;
        //            mdl.eFileID = item1.eFIleID;
        //            mdl.PaperRefNo = item1.PaperRefNo;
        //            mdl.SelectedPaperDate = item1.SelectedPaperDate;
        //            //mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreateDate).ToString("dd/MM/yyyy");               
        //            mdl.ToDepartmentID = item1.ToDepartmentID;
        //            mdl.DocumentTypeId = item1.DocumentTypeId;
        //            mdl.PaperNature = item1.PaperNature;
        //            mdl.LinkedRefNo = item1.LinkedRefNo;
        //            mdl.SendStatus = item1.SendStatus;
        //            mdl.CurrentEmpAadharID = item1.CurrentAadharId;
        //            Parentlist.Add(mdl);
        //        }
        //    }
        //    List<pHouseCommitteeFiles> list = new List<pHouseCommitteeFiles>();

        //    foreach (var item in Parentlist)
        //    {

        //        var Check = Childlist.Where(a => a.ParentId == item.Id).ToList();
        //        if (Check.Count != 0)
        //        {
        //            var Check1 = Childlist.Where(a => a.ParentId == item.Id).ToList();
        //            foreach (var item1 in Check1)
        //            {
        //                pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
        //                mdl.RecordId = item1.RecordId;
        //                mdl.Id = item1.ParentId;
        //                mdl.ParentId = item1.ParentId;

        //                mdl.eFileName = item1.eFileName;
        //                mdl.PaperNature = item1.PaperNature;
        //                mdl.CommitteeName = item1.CommitteeName;
        //                mdl.eFilePath = item1.eFilePath;
        //                mdl.AnnexPdfPath = item1.AnnexPdfPath;
        //                mdl.Title = item1.Title;
        //                mdl.Description = item1.Description;
        //                mdl.ToDepartmentName = item1.ToDepartmentName;
        //                mdl.ByDepartmentName = item1.ByDepartmentName;
        //                mdl.CreatedDate = item1.CreatedDate;
        //                mdl.ItemId = item1.ItemId;
        //                mdl.ItemName = item1.ItemName;
        //                mdl.eFileID = item1.eFileID;
        //                mdl.PaperRefNo = item1.PaperRefNo;
        //                mdl.SelectedPaperDate = item1.SelectedPaperDate;
        //                // mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreatedDate).ToString("dd/MM/yyyy");
        //                mdl.ToDepartmentID = item1.ToDepartmentID;
        //                mdl.DocumentTypeId = item1.DocumentTypeId;
        //                mdl.PaperNature = item1.PaperNature;
        //                mdl.LinkedRefNo = item1.LinkedRefNo;
        //                mdl.SendStatus = item1.SendStatus;
        //                list.Add(mdl);
        //            }
        //        }
        //        else
        //        {

        //            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
        //            mdl.Id = item.Id;
        //            mdl.RecordId = item.RecordId;
        //            mdl.ParentId = item.ParentId;
        //            mdl.eFileName = item.eFileName;
        //            mdl.PaperNature = item.PaperNature;
        //            mdl.CommitteeName = item.CommitteeName;
        //            mdl.eFilePath = item.eFilePath;
        //            mdl.AnnexPdfPath = item.AnnexPdfPath;
        //            mdl.Title = item.Title;
        //            mdl.Description = item.Description;
        //            mdl.ToDepartmentName = item.ToDepartmentName;
        //            mdl.ByDepartmentName = item.ByDepartmentName;
        //            mdl.CreatedDate = item.CreatedDate;
        //            mdl.ItemId = item.ItemId;
        //            mdl.ItemName = item.ItemName;
        //            mdl.eFileID = item.eFileID;
        //            mdl.PaperRefNo = item.PaperRefNo;
        //            mdl.SelectedPaperDate = item.SelectedPaperDate;
        //            //mdl.SelectedPaperDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
        //            mdl.ToDepartmentID = item.ToDepartmentID;
        //            mdl.DocumentTypeId = item.DocumentTypeId;
        //            mdl.PaperNature = item.PaperNature;
        //            mdl.LinkedRefNo = item.LinkedRefNo;
        //            mdl.SendStatus = item.SendStatus;
        //            list.Add(mdl);
        //        }

        //    }
        //    var ListIs = list.OrderByDescending(a => a.CreatedDate).ToList();
        //    pHouseCommitteeFiles Cmodel = param as pHouseCommitteeFiles;
        //    if (Cmodel.CurrentDeptId != "")  //not VS user
        //    {
        //        var CheckList = ListIs.Where(a => a.ToDepartmentID == Cmodel.CurrentDeptId).ToList();
        //        return CheckList;
        //    }
        //    else
        //    {
        //        return ListIs;
        //    }

        //}

        public static object GetCustomCommFiles(object param)
        {
            List<pHouseCommitteeFiles> Parentlist = new List<pHouseCommitteeFiles>();
            List<pHouseCommitteeSubFiles> Childlist = new List<pHouseCommitteeSubFiles>();
            pHouseCommitteeFiles Cmodel = param as pHouseCommitteeFiles;
            using (eFileContext ctx = new eFileContext())
            {
                var ParentList = (from a in ctx.pHouseCommitteeFiles
                                  select new
                                  {
                                      Id = a.Id,
                                      ParentId = a.Id,
                                      eFileName = a.eFileName,
                                      PaperNature = a.PaperNature,
                                      eFilePath = a.eFilePath,
                                      ReplyPdfPath = a.ReplyPdfPath,
                                      AnnexPdfPath = a.AnnexPdfPath,
                                      AnnexFilesNameByUser = a.AnnexFilesNameByUser,
                                      DocFilePath = a.DocFilePath,
                                      DocFilesNameByUser = a.DocFilesNameByUser,
                                      Title = a.Title,
                                      Description = a.Description,
                                      CreateDate = a.CreatedDate,
                                      eFIleID = a.eFileID,
                                      PaperRefNo = a.PaperRefNo,
                                      ByDeptId = a.ToDepartmentID,
                                      ToOfficeCode = a.ToOfficeCode,
                                      SelectedPaperDate = a.SelectedPaperDate,
                                      ToDepartmentID = a.ToDepartmentID,
                                      ToDepartmentName = (from mc in ctx.departments where mc.deptId == a.ToDepartmentID select mc.deptname).FirstOrDefault(),
                                      ToDepartmentNameAbbrev = (from mc in ctx.departments where mc.deptId == a.ToDepartmentID select mc.deptabbr).FirstOrDefault(),
                                      ByDepartmentId = a.ByDepartmentId,
                                      ByDepartmentName = (from mc in ctx.departments where mc.deptId == a.ByDepartmentId select mc.deptname).FirstOrDefault(),
                                      ByOfficeCode = a.ByOfficeCode,
                                      ItemName = a.ItemName,
                                      ItemId = a.ItemId,
                                      DocumentTypeId = a.DocumentTypeId,
                                      LinkedRefNo = a.LinkedRefNo,
                                      SendStatus = a.SendStatus,
                                      IsVerified = a.IsVerified,                              
                                      VerifiedBy = (from EN in ctx.mStaff where EN.AadharID == a.VerifiedBy select EN.StaffName).FirstOrDefault(),

                                      
                                      RejectReason = a.RejectReason,
                                      BranchName = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                      CommitteeId = a.CommitteeId,
                                      CommitteeName = (from mc in ctx.committees where mc.CommitteeId == a.CommitteeId select mc.CommitteeName).FirstOrDefault(),
                                      CommitteeNameAbbrev = (from mc in ctx.committees where mc.CommitteeId == a.CommitteeId select mc.Abbreviation).FirstOrDefault(),
                                      CurrentAadharId = a.CurrentEmpAadharID,
                                      EmpName = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                      AbbrevBranch = (from mc in ctx.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                      Desingnation = (from EN in ctx.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                  }).ToList().OrderByDescending(a => a.CreateDate);
                List<pHouseCommitteeSubFiles> queryAll = new List<pHouseCommitteeSubFiles>();
                //var CList;
                if (Cmodel.CurrentDeptId != "")//not vs user/only for single dept
                {
                    var CList = from element in ctx.pHouseCommitteeSubFiles
                                where element.SendStatus == true
                                group element by element.ParentId
                                    into groups
                                    select groups.OrderByDescending(p => p.CreatedDate).FirstOrDefault();
                    queryAll = CList.ToList();
                }
                else //for vs users
                {
                    var CList = from element in ctx.pHouseCommitteeSubFiles
                                group element by element.ParentId
                                    into groups
                                    select groups.OrderByDescending(p => p.CreatedDate).FirstOrDefault();
                    queryAll = CList.ToList();
                }
                var ChildList = queryAll.ToList();
                foreach (var item1 in ChildList)
                {
                    pHouseCommitteeSubFiles mdl = new pHouseCommitteeSubFiles();
                    mdl.RecordId = item1.RecordId;
                    mdl.eFileName = item1.eFileName;
                    //mdl.PaperNature = item1.PaperNature;
                    mdl.ParentId = item1.ParentId;
                    mdl.CommitteeName = item1.CommitteeName;
                    mdl.CommitteeId = item1.CommitteeId;
                    mdl.CommitteeNameAbbrev = (from mc in ctx.committees where mc.CommitteeId == item1.CommitteeId select mc.Abbreviation).FirstOrDefault();
                    mdl.eFilePath = item1.eFilePath;

                    if (item1.ReplyPdfPath != null)
                    {
                        string ReplyPdfPath = item1.ReplyPdfPath;
                        ReplyPdfPath.Replace(" ", string.Empty);
                        mdl.ReplyPdfPath = ReplyPdfPath;
                    }
                    else
                    {
                        mdl.ReplyPdfPath = item1.ReplyPdfPath; ;
                    }

                    if (item1.AnnexPdfPath != null)
                    {
                        string AnnexPdfPath = item1.AnnexPdfPath;
                        AnnexPdfPath.Replace(" ", string.Empty);
                        mdl.AnnexPdfPath = AnnexPdfPath;
                    }
                    else
                    {
                        mdl.AnnexPdfPath = item1.AnnexPdfPath; ;
                    }



                    // mdl.AnnexPdfPath = item1.AnnexPdfPath;
                    if (item1.AnnexFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                        mdl.AnnexFilesNameByUser = str;

                    }
                    else
                    {
                        mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                    }


                    if (item1.DocFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item1.DocFilesNameByUser, @"\s+", "");
                        mdl.DocFilesNameByUser = str;

                    }
                    else
                    {
                        mdl.DocFilesNameByUser = item1.DocFilesNameByUser;
                    }


                    mdl.DocFilePath = item1.DocFilePath;
                    mdl.Title = item1.Title;
                    mdl.Description = item1.Description;
                    //mdl.ItemName=item1.
                    mdl.ToDepartmentName = (from mc in ctx.departments where mc.deptId == item1.ToDepartmentID select mc.deptname).FirstOrDefault();
                    mdl.ToDepartmentNameAbbrev = (from mc in ctx.departments where mc.deptId == item1.ToDepartmentID select mc.deptabbr).FirstOrDefault();
                    mdl.ByDepartmentName = (from mc in ctx.departments where mc.deptId == item1.ByDepartmentId select mc.deptname).FirstOrDefault();
                    mdl.CreatedDate = item1.CreatedDate;
                    mdl.ItemId = item1.ItemId;
                    mdl.ItemName = item1.ItemName;
                    mdl.eFileID = item1.eFileID;
                    mdl.PaperRefNo = item1.PaperRefNo;
                    mdl.SelectedPaperDate = item1.SelectedPaperDate;
                    //  mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreatedDate).ToString("dd/MM/yyyy");               
                    mdl.ToDepartmentID = item1.ToDepartmentID;
                    mdl.ByDepartmentId = item1.ByDepartmentId;
                    mdl.DocumentTypeId = item1.DocumentTypeId;
                    mdl.PaperNature = item1.PaperNature;
                    mdl.LinkedRefNo = item1.LinkedRefNo;
                    mdl.SendStatus = item1.SendStatus;
                    mdl.IsVerified = item1.IsVerified;
                    mdl.VerifiedBy = (from EN in ctx.mStaff where EN.AadharID == item1.VerifiedBy select EN.StaffName).FirstOrDefault();

                    mdl.RejectReason = item1.RejectReason;
                    Childlist.Add(mdl);
                }
                foreach (var item1 in ParentList)
                {
                    pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                    mdl.Id = item1.Id;
                    mdl.ParentId = item1.ParentId;
                    mdl.eFileName = item1.eFileName;
                    //mdl.PaperNature = item1.PaperNature;
                    mdl.CommitteeName = item1.CommitteeName;
                    mdl.CommitteeId = item1.CommitteeId;
                    mdl.CommitteeNameAbbrev = (from mc in ctx.committees where mc.CommitteeId == item1.CommitteeId select mc.Abbreviation).FirstOrDefault();
                   
                    mdl.eFilePath = item1.eFilePath;
                    //mdl.ReplyPdfPath = item1.ReplyPdfPath;
                    //mdl.AnnexPdfPath = item1.AnnexPdfPath;


                    if (item1.ReplyPdfPath != null)
                    {
                        string ReplyPdfPath = item1.ReplyPdfPath;
                        ReplyPdfPath.Replace(" ", string.Empty);
                        mdl.ReplyPdfPath = ReplyPdfPath;
                    }
                    else
                    {
                        mdl.ReplyPdfPath = item1.ReplyPdfPath; ;
                    }

                    if (item1.AnnexPdfPath != null)
                    {
                        string AnnexPdfPath = item1.AnnexPdfPath;
                        AnnexPdfPath.Replace(" ", string.Empty);
                        mdl.AnnexPdfPath = AnnexPdfPath;
                    }
                    else
                    {
                        mdl.AnnexPdfPath = item1.AnnexPdfPath; ;
                    }



                    if (item1.AnnexFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                        mdl.AnnexFilesNameByUser = str;
                    }
                    else
                    {
                        mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                    }

                    mdl.DocFilePath = item1.DocFilePath;
                    if (item1.DocFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item1.DocFilesNameByUser, @"\s+", "");
                        mdl.DocFilesNameByUser = str;
                    }
                    else
                    {
                        mdl.DocFilesNameByUser = item1.DocFilesNameByUser;
                    }


                    mdl.Title = item1.Title;
                    mdl.Description = item1.Description;
                    //mdl.ItemName=item1.
                    mdl.ToDepartmentName = item1.ToDepartmentName;
                    mdl.ByDepartmentName = item1.ByDepartmentName;
                    mdl.ToDepartmentNameAbbrev = item1.ToDepartmentNameAbbrev;
                    mdl.CreatedDate = item1.CreateDate;
                    mdl.ItemId = item1.ItemId;
                    mdl.ItemName = item1.ItemName;
                    mdl.eFileID = item1.eFIleID;
                    mdl.PaperRefNo = item1.PaperRefNo;
                    mdl.SelectedPaperDate = item1.SelectedPaperDate;
                    //mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreateDate).ToString("dd/MM/yyyy");               
                    mdl.ToDepartmentID = item1.ToDepartmentID;
                    mdl.ByDepartmentId = item1.ByDepartmentId;
                   
                    mdl.DocumentTypeId = item1.DocumentTypeId;
                    mdl.PaperNature = item1.PaperNature;
                    mdl.LinkedRefNo = item1.LinkedRefNo;
                    mdl.SendStatus = item1.SendStatus;
                    mdl.IsVerified = item1.IsVerified;
                    mdl.VerifiedBy = (from EN in ctx.mStaff where EN.AadharID == item1.VerifiedBy select EN.StaffName).FirstOrDefault();

                    mdl.RejectReason = item1.RejectReason;
                    mdl.CurrentEmpAadharID = item1.CurrentAadharId;
                    Parentlist.Add(mdl);
                }
            }
            List<pHouseCommitteeFiles> list = new List<pHouseCommitteeFiles>();

            foreach (var item in Parentlist)
            {

                var Check = Childlist.Where(a => a.ParentId == item.Id).ToList();
                if (Check.Count != 0)
                {
                    var Check1 = Childlist.Where(a => a.ParentId == item.Id).ToList();
                    foreach (var item1 in Check1)
                    {
                        pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                        mdl.RecordId = item1.RecordId;
                        mdl.Id = item1.ParentId;
                        mdl.ParentId = item1.ParentId;

                        mdl.eFileName = item1.eFileName;
                        mdl.PaperNature = item1.PaperNature;
                        mdl.CommitteeId = item1.CommitteeId;
                        mdl.CommitteeName = item1.CommitteeName;
                        mdl.CommitteeNameAbbrev = item1.CommitteeNameAbbrev;
                   
                        mdl.eFilePath = item1.eFilePath;
                        //mdl.ReplyPdfPath = item1.ReplyPdfPath;
                        //mdl.AnnexPdfPath = item1.AnnexPdfPath;


                        if (item1.ReplyPdfPath != null)
                        {
                            string ReplyPdfPath = item1.ReplyPdfPath;
                            ReplyPdfPath.Replace(" ", string.Empty);
                            mdl.ReplyPdfPath = ReplyPdfPath;
                        }
                        else
                        {
                            mdl.ReplyPdfPath = item1.ReplyPdfPath; ;
                        }

                        if (item1.AnnexPdfPath != null)
                        {
                            string AnnexPdfPath = item1.AnnexPdfPath;
                            AnnexPdfPath.Replace(" ", string.Empty);
                            mdl.AnnexPdfPath = AnnexPdfPath;
                        }
                        else
                        {
                            mdl.AnnexPdfPath = item1.AnnexPdfPath; ;
                        }


                        if (item1.AnnexFilesNameByUser != null)
                        {
                            string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                            mdl.AnnexFilesNameByUser = str;
                        }
                        else
                        {
                            mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                        }



                        mdl.DocFilePath = item1.DocFilePath;
                        if (item1.DocFilesNameByUser != null)
                        {
                            string str = System.Text.RegularExpressions.Regex.Replace(item1.DocFilesNameByUser, @"\s+", "");
                            mdl.DocFilesNameByUser = str;
                        }
                        else
                        {
                            mdl.DocFilesNameByUser = item1.DocFilesNameByUser;
                        }
                        mdl.Title = item1.Title;
                        mdl.Description = item1.Description;
                        mdl.ToDepartmentName = item1.ToDepartmentName;
                        mdl.ByDepartmentName = item1.ByDepartmentName;
                        mdl.ToDepartmentNameAbbrev = item1.ToDepartmentNameAbbrev;
                   
                        mdl.CreatedDate = item1.CreatedDate;
                        mdl.ItemId = item1.ItemId;
                        mdl.ItemName = item1.ItemName;
                        mdl.eFileID = item1.eFileID;
                        mdl.PaperRefNo = item1.PaperRefNo;
                        mdl.SelectedPaperDate = item1.SelectedPaperDate;
                        // mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreatedDate).ToString("dd/MM/yyyy");
                        mdl.ToDepartmentID = item1.ToDepartmentID;
                        mdl.ByDepartmentId = item1.ByDepartmentId;
                        mdl.DocumentTypeId = item1.DocumentTypeId;
                        mdl.PaperNature = item1.PaperNature;
                        mdl.LinkedRefNo = item1.LinkedRefNo;
                        mdl.SendStatus = item1.SendStatus;
                        mdl.IsVerified = item1.IsVerified;
                        mdl.VerifiedBy = item1.VerifiedBy;

                        mdl.RejectReason = item1.RejectReason;
                        list.Add(mdl);
                    }
                }
                else
                {

                    pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                    mdl.Id = item.Id;
                    mdl.RecordId = item.RecordId;
                    mdl.ParentId = item.ParentId;
                    mdl.eFileName = item.eFileName;
                    mdl.PaperNature = item.PaperNature;
                    mdl.CommitteeName = item.CommitteeName;
                    mdl.CommitteeId = item.CommitteeId;
                    mdl.CommitteeNameAbbrev = item.CommitteeNameAbbrev;
                    mdl.eFilePath = item.eFilePath;
                    //mdl.ReplyPdfPath = item.ReplyPdfPath;
                    //mdl.AnnexPdfPath = item.AnnexPdfPath;

                    if (item.ReplyPdfPath != null)
                    {
                        string ReplyPdfPath = item.ReplyPdfPath;
                        ReplyPdfPath.Replace(" ", string.Empty);
                        mdl.ReplyPdfPath = ReplyPdfPath;
                    }
                    else
                    {
                        mdl.ReplyPdfPath = item.ReplyPdfPath; ;
                    }

                    if (item.AnnexPdfPath != null)
                    {
                        string AnnexPdfPath = item.AnnexPdfPath;
                        AnnexPdfPath.Replace(" ", string.Empty);
                        mdl.AnnexPdfPath = AnnexPdfPath;
                    }
                    else
                    {
                        mdl.AnnexPdfPath = item.AnnexPdfPath; ;
                    }


                    if (item.AnnexFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                        mdl.AnnexFilesNameByUser = str;
                    }
                    else
                    {
                        mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                    }



                    mdl.DocFilePath = item.DocFilePath;
                    if (item.DocFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item.DocFilesNameByUser, @"\s+", "");
                        mdl.DocFilesNameByUser = str;
                    }
                    else
                    {
                        mdl.DocFilesNameByUser = item.DocFilesNameByUser;
                    }
                    mdl.Title = item.Title;
                    mdl.Description = item.Description;
                    mdl.ToDepartmentName = item.ToDepartmentName;
                    mdl.ByDepartmentName = item.ByDepartmentName;
                    mdl.CreatedDate = item.CreatedDate;
                    mdl.ItemId = item.ItemId;
                    mdl.ItemName = item.ItemName;
                    mdl.eFileID = item.eFileID;
                    mdl.PaperRefNo = item.PaperRefNo;
                    mdl.SelectedPaperDate = item.SelectedPaperDate;
                    //mdl.SelectedPaperDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
                    mdl.ToDepartmentID = item.ToDepartmentID;
                    mdl.ByDepartmentId = item.ByDepartmentId;
                    mdl.ToDepartmentNameAbbrev = item.ToDepartmentNameAbbrev;
                   
                    mdl.DocumentTypeId = item.DocumentTypeId;
                    mdl.PaperNature = item.PaperNature;
                    mdl.LinkedRefNo = item.LinkedRefNo;
                    mdl.SendStatus = item.SendStatus;
                    mdl.IsVerified = item.IsVerified;
                    mdl.VerifiedBy = item.VerifiedBy;

                    mdl.RejectReason = item.RejectReason;
                    list.Add(mdl);
                }

            }
            var ListIs = list.OrderByDescending(a => a.CreatedDate).ToList();

            List<pHouseCommitteeFiles> listnew = new List<pHouseCommitteeFiles>();
            ////////////////Changed for listing
            if (Cmodel.CurrentDeptId.Contains(','))//not VS user/For HOD Having multiple Dept
            {
                string[] str1 = new string[4];
                str1 = Cmodel.CurrentDeptId.Split(',');
                foreach (var item in str1)
                {
                    string CurrentDeptId = item;
                    var CheckList = ListIs.Where(a => a.ToDepartmentID == CurrentDeptId && a.SendStatus == true).ToList();
                    //  var CheckList1 = ListIs.Where(a => a.ByDepartmentId == CurrentDeptId && a.SendStatus == true).ToList();
                    listnew.AddRange(CheckList);
                }
                listnew = listnew.OrderByDescending(a => a.CreatedDate).ToList();
                return listnew;
            }
            else if (Cmodel.CurrentDeptId != "")//not VS user
            {
                var CheckList = ListIs.Where(a => a.ToDepartmentID == Cmodel.CurrentDeptId).ToList();
                var checkit = CheckList.Where(a => a.SendStatus == true).ToList();
                checkit = checkit.OrderByDescending(a => a.CreatedDate).ToList();
                return checkit;
            }
            else//Vs user
            {
                //Get commite By BarnchId
                using (eFileContext ctxCommitee = new eFileContext())
                {
                    foreach (var item in Cmodel.tCommitteeList)
                    {
                        int CommitteId = item.CommitteeId;
                        var CheckList = ListIs.Where(a => a.CommitteeId == CommitteId).ToList();
                        listnew.AddRange(CheckList);

                    }
                }
                listnew = listnew.OrderByDescending(a => a.CreatedDate).ToList();
                return listnew;
            }
        }
        public static object GetSubCommFilesByParentId(object param)
        {
            List<pHouseCommitteeFiles> Parentlist = new List<pHouseCommitteeFiles>();
            List<pHouseCommitteeSubFiles> Childlist = new List<pHouseCommitteeSubFiles>();
            List<pHouseCommitteeFiles> list = new List<pHouseCommitteeFiles>();
            SiteSettingContext settingContext = new SiteSettingContext();
            eFileContext db = new eFileContext();
            pHouseCommitteeFiles model = param as pHouseCommitteeFiles;
            if (model.CurrentDeptId != "HPD0001")//not vs user/only for single dept
            {
                var plist = (from dist in db.pHouseCommitteeFiles
                             where dist.Id == model.ParentId && dist.SendStatus == true
                             orderby dist.CreatedDate descending
                             select dist);
                Parentlist = plist.ToList();
                var clist = (from dist in db.pHouseCommitteeSubFiles
                             where dist.ParentId == model.ParentId && dist.SendStatus == true
                             orderby dist.CreatedDate descending
                             select dist);
                Childlist = clist.ToList();
            }
            else //for VS Users Only
            {
                var plist = (from dist in db.pHouseCommitteeFiles
                             where dist.Id == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Parentlist = plist.ToList();
                var clist = (from dist in db.pHouseCommitteeSubFiles
                             where dist.ParentId == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Childlist = clist.ToList();
            }

            List<pHouseCommitteeFiles> list1 = new List<pHouseCommitteeFiles>();
            foreach (var item in Parentlist)
            {
                pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                mdl.Id = item.Id;
                mdl.ParentId = item.ParentId;
                mdl.eFileName = item.eFileName;
                mdl.PaperNature = item.PaperNature;
                mdl.CommitteeName = item.CommitteeName;
                mdl.eFilePath = item.eFilePath;
                mdl.ReplyPdfPath = item.ReplyPdfPath;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                mdl.DocFilePath = item.DocFilePath;
                mdl.DocFilesNameByUser = item.DocFilesNameByUser;
                mdl.Title = item.Title;
                mdl.Description = item.Description;
                mdl.ToDepartmentName = item.ToDepartmentName;
                mdl.ByDepartmentName = item.ByDepartmentName;
                mdl.ToDepartmentID = item.ToDepartmentID;
                mdl.ByDepartmentId = item.ByDepartmentId;
                mdl.CreatedDate = item.CreatedDate;

                mdl.ItemId = item.ItemId;
                mdl.ItemName = item.ItemName;
                mdl.eFileID = item.eFileID;
                mdl.PaperRefNo = item.PaperRefNo;
                mdl.SelectedPaperDate = item.SelectedPaperDate;
                //mdl.SelectedPaperDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");

                mdl.DocumentTypeId = item.DocumentTypeId;
                mdl.PaperNature = item.PaperNature;
                mdl.LinkedRefNo = item.LinkedRefNo;
                mdl.SendStatus = item.SendStatus;
                mdl.IsVerified = item.IsVerified;
                mdl.RejectReason = item.RejectReason;
                var FileAccessingUrlPath = (from itemis in settingContext.SiteSettings where itemis.SettingName == "FileAccessingUrlPath" select itemis.SettingValue).SingleOrDefault();
                mdl.FileAccessingUrlPath = FileAccessingUrlPath;
                mdl.IsVerified = item.IsVerified;
                mdl.VerifiedBy = (from EN in db.mStaff where EN.AadharID == item.VerifiedBy select EN.StaffName).FirstOrDefault();
              // mdl.VerifiedBy = item.VerifiedBy;

                mdl.RejectReason=item.RejectReason;
                mdl.CurrentDeptId = model.CurrentDeptId;
                list.Add(mdl);
            }
            foreach (var item in Childlist)
            {
                pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                mdl.RecordId = item.RecordId;
                mdl.Id = item.ParentId;
                mdl.ParentId = item.ParentId;
                mdl.eFileName = item.eFileName;
                mdl.PaperNature = item.PaperNature;
                mdl.CommitteeName = item.CommitteeName;
                mdl.eFilePath = item.eFilePath;
                mdl.ReplyPdfPath = item.ReplyPdfPath;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                mdl.DocFilePath = item.DocFilePath;
                mdl.DocFilesNameByUser = item.DocFilesNameByUser;
                mdl.Title = item.Title;
                mdl.Description = item.Description;
                mdl.ToDepartmentName = item.ToDepartmentName;
                mdl.ByDepartmentName = item.ByDepartmentName;
                mdl.CreatedDate = item.CreatedDate;
                mdl.ItemId = item.ItemId;
                mdl.ItemName = item.ItemName;
                mdl.eFileID = item.eFileID;
                mdl.PaperRefNo = item.PaperRefNo;
                mdl.SelectedPaperDate = item.SelectedPaperDate;
                // mdl.SelectedPaperDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
                mdl.ToDepartmentID = item.ToDepartmentID;
                mdl.ByDepartmentId = item.ByDepartmentId;
                mdl.DocumentTypeId = item.DocumentTypeId;
                mdl.PaperNature = item.PaperNature;
                mdl.LinkedRefNo = item.LinkedRefNo;
                mdl.SendStatus = item.SendStatus;
                mdl.IsVerified = item.IsVerified;
                mdl.RejectReason = item.RejectReason;
                var FileAccessingUrlPath = (from itemis in settingContext.SiteSettings where itemis.SettingName == "FileAccessingUrlPath" select itemis.SettingValue).SingleOrDefault();
                mdl.FileAccessingUrlPath = FileAccessingUrlPath;
                mdl.IsVerified = item.IsVerified;
                mdl.VerifiedBy = (from EN in db.mStaff where EN.AadharID == item.VerifiedBy select EN.StaffName).FirstOrDefault();

                mdl.RejectReason = item.RejectReason;
                mdl.CurrentDeptId = model.CurrentDeptId;
                list.Add(mdl);
            }
            var ListIs = list.OrderByDescending(a => a.CreatedDate).ToList();
            var Index = ListIs.Count - 1;
            ListIs.RemoveAt(0);
            return ListIs;
        }
        public static object nGetCommiteeIDAndNameByBranchID(object param)
        {
            string[] str = param as string[];
            int BrachId = 0;
            List<tCommittee> returnToCaller = new List<tCommittee>();
            string strBranchId = Convert.ToString(str[0]);

            if (!string.IsNullOrEmpty(strBranchId))
            {
                BrachId = Convert.ToInt32(strBranchId);
            }
            using (eFileContext ctxCommitee = new eFileContext())
            {
                var VarCommittee = (from c in ctxCommitee.tBranchesCommittee
                                    join b in ctxCommitee.committees on c.CommitteeId equals b.CommitteeId
                                    where c.BranchId == BrachId
                                    select new
                                    {
                                        id = c.CommitteeId,
                                        name = b.CommitteeName
                                    }).FirstOrDefault();

                tCommittee tempModel = new tCommittee();
                tempModel.CommitteeId = VarCommittee.id;
                tempModel.CommitteeName = VarCommittee.name;
                returnToCaller.Add(tempModel);
                return returnToCaller;
            }
        }
        private static eFileListAll NeFileList(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                string[] str = param as string[];
                string DeptId = Convert.ToString(str[0]);
                string cid = Convert.ToString(str[3]);
                if (cid == "")
                {
                    cid = "0";
                }
                Int32 CommitteeId = Convert.ToInt32(cid);
                int Officecode = 0;
                int.TryParse(str[1], out Officecode);
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                if (Officecode == 0)
                {
                    if (CommitteeId == 0)
                    {
                        List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                                  join departmentvalues in context.departments
                                                                                  on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                                  //join user in context.users
                                                                                  //on efilevalues.CreatedBy equals user.UserId
                                                                                  join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId// into Comms_leftjoin
                                                                                  //from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                                  join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin
                                                                                  //  from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                                  where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  // && (efilevalues.CommitteeId == CommitteeId)
                                                                                  //&& efilevalues.Officecode == Officecode
                                                                                  select new SBL.DomainModel.Models.eFile.eFileList
                                                                                  {
                                                                                      Department = departmentvalues.deptname,
                                                                                      eFileID = efilevalues.eFileID,
                                                                                      eFileNumber = efilevalues.eFileNumber,
                                                                                      eFileSubject = efilevalues.eFileSubject,
                                                                                      Status = efilevalues.Status,
                                                                                      CreateDate = efilevalues.CreatedDate,
                                                                                      //CreatedBy = user.UserName,
                                                                                      olddesc = efilevalues.OldDescription,
                                                                                      newdesc = efilevalues.NewDescription,
                                                                                      CommitteeId = efilevalues.CommitteeId,
                                                                                      DepartmentId = efilevalues.DepartmentId,
                                                                                      DeptAbbr = departmentvalues.deptabbr,
                                                                                      Committee = Comms.CommitteeName,
                                                                                      CommitteeAbbr = Comms.Abbreviation,
                                                                                      FromYear = efilevalues.FromYear,
                                                                                      ToYear = efilevalues.ToYear,
                                                                                      SelectedDeptId = efilevalues.SelectedDeptId,
                                                                                      eFileTypeName = etypeobj.eType
                                                                                  }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                        List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                        SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                        eFileListAlls.eFileLists = eFileList;
                        eFileListAlls.Departments = departments;
                        return eFileListAlls;
                    }
                    else
                    {
                        List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                                  join departmentvalues in context.departments
                                                                                  on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                                  //join user in context.users
                                                                                  //on efilevalues.CreatedBy equals user.UserId
                                                                                  join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId// into Comms_leftjoin
                                                                                  //from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                                  join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin
                                                                                  //  from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                                  where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  && (efilevalues.CommitteeId == CommitteeId)
                                                                                  //&& efilevalues.Officecode == Officecode
                                                                                  select new SBL.DomainModel.Models.eFile.eFileList
                                                                                  {
                                                                                      Department = departmentvalues.deptname,
                                                                                      eFileID = efilevalues.eFileID,
                                                                                      eFileNumber = efilevalues.eFileNumber,
                                                                                      eFileSubject = efilevalues.eFileSubject,
                                                                                      Status = efilevalues.Status,
                                                                                      CreateDate = efilevalues.CreatedDate,
                                                                                      //CreatedBy = user.UserName,
                                                                                      olddesc = efilevalues.OldDescription,
                                                                                      newdesc = efilevalues.NewDescription,
                                                                                      CommitteeId = efilevalues.CommitteeId,
                                                                                      DepartmentId = efilevalues.DepartmentId,
                                                                                      DeptAbbr = departmentvalues.deptabbr,
                                                                                      Committee = Comms.CommitteeName,
                                                                                      CommitteeAbbr = Comms.Abbreviation,
                                                                                      FromYear = efilevalues.FromYear,
                                                                                      ToYear = efilevalues.ToYear,
                                                                                      SelectedDeptId = efilevalues.SelectedDeptId,
                                                                                      eFileTypeName = etypeobj.eType
                                                                                  }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                        List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                        SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                        eFileListAlls.eFileLists = eFileList;
                        eFileListAlls.Departments = departments;
                        return eFileListAlls;
                    }
                }
                else
                {
                    if (CommitteeId == 0)
                    {
                        List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                                  join departmentvalues in context.departments
                                                                                  on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                                  join user in context.users
                                                                                  on efilevalues.CreatedBy equals user.UserId
                                                                                  join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId //into Comms_leftjoin
                                                                                  // from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                                  join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin
                                                                                  // from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                                  where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                  // && efilevalues.Officecode == Officecode
                                                                                  //&& (CommitteeId.CommitteeId == CommitteeId)
                                                                                  select new SBL.DomainModel.Models.eFile.eFileList
                                                                                  {
                                                                                      Department = departmentvalues.deptname,
                                                                                      eFileID = efilevalues.eFileID,
                                                                                      eFileNumber = efilevalues.eFileNumber,
                                                                                      eFileSubject = efilevalues.eFileSubject,
                                                                                      Status = efilevalues.Status,
                                                                                      CreateDate = efilevalues.CreatedDate,
                                                                                      CreatedBy = user.UserName,
                                                                                      olddesc = efilevalues.OldDescription,
                                                                                      newdesc = efilevalues.NewDescription,
                                                                                      CommitteeId = efilevalues.CommitteeId,
                                                                                      DepartmentId = efilevalues.DepartmentId,
                                                                                      DeptAbbr = departmentvalues.deptabbr,
                                                                                      Committee = Comms.CommitteeName,
                                                                                      CommitteeAbbr = Comms.Abbreviation,
                                                                                      FromYear = efilevalues.FromYear,
                                                                                      ToYear = efilevalues.ToYear,
                                                                                      SelectedDeptId = efilevalues.SelectedDeptId,
                                                                                      eFileTypeName = etypeobj.eType
                                                                                  }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                        List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                        SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                        eFileListAlls.eFileLists = eFileList;
                        eFileListAlls.Departments = departments;
                        return eFileListAlls;
                    }
                    else
                    {
                        List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                                  join departmentvalues in context.departments
                                                                                  on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                                  join user in context.users
                                                                                  on efilevalues.CreatedBy equals user.UserId
                                                                                  join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId //into Comms_leftjoin
                                                                                  // from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                                  join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin
                                                                                  // from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                                  where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                                      // && efilevalues.Officecode == Officecode
                                                                                    && (efilevalues.CommitteeId == CommitteeId)
                                                                                  select new SBL.DomainModel.Models.eFile.eFileList
                                                                                  {
                                                                                      Department = departmentvalues.deptname,
                                                                                      eFileID = efilevalues.eFileID,
                                                                                      eFileNumber = efilevalues.eFileNumber,
                                                                                      eFileSubject = efilevalues.eFileSubject,
                                                                                      Status = efilevalues.Status,
                                                                                      CreateDate = efilevalues.CreatedDate,
                                                                                      CreatedBy = user.UserName,
                                                                                      olddesc = efilevalues.OldDescription,
                                                                                      newdesc = efilevalues.NewDescription,
                                                                                      CommitteeId = efilevalues.CommitteeId,
                                                                                      DepartmentId = efilevalues.DepartmentId,
                                                                                      DeptAbbr = departmentvalues.deptabbr,
                                                                                      Committee = Comms.CommitteeName,
                                                                                      CommitteeAbbr = Comms.Abbreviation,
                                                                                      FromYear = efilevalues.FromYear,
                                                                                      ToYear = efilevalues.ToYear,
                                                                                      SelectedDeptId = efilevalues.SelectedDeptId,
                                                                                      eFileTypeName = etypeobj.eType
                                                                                  }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                        List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                        SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                        eFileListAlls.eFileLists = eFileList;
                        eFileListAlls.Departments = departments;
                        return eFileListAlls;
                    }
                }
            }
        }
        public static object GetFileDetailsByeFileId(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int eFileId = Convert.ToInt32(param.ToString());
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              join dept in context.departments on efilevalues.SelectedDeptId equals dept.deptId
                                                                              where efilevalues.eFileID == eFileId
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  DepartmentId = efilevalues.SelectedDeptId,
                                                                                  Department = (from Item in context.departments where Item.deptId == efilevalues.SelectedDeptId select Item.deptname).FirstOrDefault(),

                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                                  eFileTypeId = efilevalues.eFileTypeID,
                                                                                  eFileTypeName = (from Item in context.mCommitteeReplyItemType where Item.Id == efilevalues.eFileTypeID select Item.ItemTypeName).FirstOrDefault(),

                                                                              }).ToList().OrderBy(x => x.eFileID).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    return eFileListAlls;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static object SaveDraftedPaper(object param)
        {
            try
            {
                pHouseCommitteeFiles mdl = param as pHouseCommitteeFiles;
                //eFileAttachment DraftObj = new eFileAttachment();                
                pHouseCommitteeFiles DraftObj = new pHouseCommitteeFiles();
                using (eFileContext ctx = new eFileContext())
                {
                    DraftObj.eFileID = mdl.eFileID;
                    DraftObj.eFileName = mdl.eFileName;
                    DraftObj.DocumentTypeId = mdl.DocumentTypeId;
                    DraftObj.Description = mdl.Description;
                    DraftObj.Title = mdl.Title;
                    DraftObj.SendStatus = mdl.SendStatus;
                    DraftObj.ByDepartmentId = mdl.ByDepartmentId;
                    DraftObj.ToDepartmentID = mdl.ToDepartmentID;
                    // DraftObj.SelectedPaperDate = mdl.SelectedDate;
                    //DraftObj.LetterStatusType=mdl.LetterStatusType;
                    DraftObj.SelectedPaperDate = mdl.SelectedPaperDate;
                    DraftObj.ParentId = mdl.ParentId;
                    DraftObj.ByOfficeCode = mdl.ByOfficeCode;
                    DraftObj.ToOfficeCode = mdl.ToOfficeCode;
                    DraftObj.PaperNature = mdl.PaperNature;

                    DraftObj.SendDateToDept = null;
                    DraftObj.ReceivedDateByDept = null;

                    DraftObj.ItemId = mdl.ItemId;
                    DraftObj.ItemName = mdl.ItemName;
                    DraftObj.CommitteeId = mdl.CommitteeId;
                    DraftObj.CommitteeName = mdl.CommitteeName;
                    DraftObj.eFilePath = mdl.eFilePath;

                    DraftObj.AnnexPdfPath = mdl.AnnexPdfPath;
                    DraftObj.AnnexFilesNameByUser = mdl.AnnexFilesNameByUser;
                    DraftObj.LinkedRefNo = mdl.LinkedRefNo;
                    DraftObj.IsDeleted = mdl.IsDeleted;
                    DraftObj.CurYear = mdl.CurYear;
                    DraftObj.CurrentBranchId = mdl.CurrentBranchId;
                    DraftObj.CurrentEmpAadharID = mdl.CurrentEmpAadharID;

                    var lastNumber = (from eF in ctx.pHouseCommitteeFiles orderby eF.Id descending select eF.Id).FirstOrDefault();
                    DraftObj.PaperRefNo = System.DateTime.Now.Year.ToString() + "/" + Convert.ToString(lastNumber + 1);

                    DraftObj.CreatedDate = mdl.CreatedDate;
                    DraftObj.CreatedBy = mdl.CreatedBy;
                    ctx.pHouseCommitteeFiles.Add(DraftObj);
                    ctx.SaveChanges();
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }
        public static object GetDetailsByParentId(object param)
        {
            List<pHouseCommitteeFiles> Parentlist = new List<pHouseCommitteeFiles>();
            List<pHouseCommitteeSubFiles> Childlist = new List<pHouseCommitteeSubFiles>();
            List<pHouseCommitteeFiles> list = new List<pHouseCommitteeFiles>();
            eFileContext db = new eFileContext();
            pHouseCommitteeFiles model = param as pHouseCommitteeFiles;
            var plist = (from dist in db.pHouseCommitteeFiles
                         where dist.Id == model.ParentId
                         orderby dist.CreatedDate descending
                         select dist);
            var ListIs = plist.ToList();
            return ListIs;
        }
        private static eFileListAll NAlleFileList(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                          join departmentvalues in context.departments
                                                                          on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                          join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId// into Comms_leftjoin                                                                                  
                                                                          //join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID //into etypeobj_leftjoin                                                                                  
                                                                          where efilevalues.Status == true && efilevalues.IsDeleted == false
                                                                          //deptlist.Contains(efilevalues.DepartmentId) && 
                                                                          select new SBL.DomainModel.Models.eFile.eFileList
                                                                          {
                                                                              Department = departmentvalues.deptname,
                                                                              eFileID = efilevalues.eFileID,
                                                                              eFileNumber = efilevalues.FromYear + "(" + efilevalues.eFileNumber + ")",
                                                                              eFileSubject = efilevalues.eFileSubject,
                                                                              Status = efilevalues.Status,
                                                                              CreateDate = efilevalues.CreatedDate,
                                                                              olddesc = efilevalues.OldDescription,
                                                                              newdesc = efilevalues.NewDescription,
                                                                              CommitteeId = efilevalues.CommitteeId,
                                                                              DepartmentId = efilevalues.DepartmentId,
                                                                              DeptAbbr = departmentvalues.deptabbr,
                                                                              Committee = Comms.CommitteeName,
                                                                              CommitteeAbbr = Comms.Abbreviation,
                                                                              FromYear = efilevalues.FromYear,
                                                                              ToYear = efilevalues.ToYear,
                                                                              SelectedDeptId = efilevalues.SelectedDeptId,
                                                                              //  eFileTypeName = etypeobj.eType
                                                                              //}).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                                                                          }).ToList().OrderByDescending(x => x.FromYear).ToList();
                List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                eFileListAlls.eFileLists = eFileList;
                eFileListAlls.Departments = departments;
                return eFileListAlls;
            }
        }
        public static object SaveToComposedLetter(object param)
        {
            try
            {
                pHouseCommitteeSubFiles mdl = param as pHouseCommitteeSubFiles;
                pHouseCommitteeSubFiles DraftObj = new pHouseCommitteeSubFiles();
                using (eFileContext ctx = new eFileContext())
                {
                    DraftObj.eFileID = mdl.eFileID;
                    DraftObj.eFileName = mdl.eFileName;
                    DraftObj.DocumentTypeId = mdl.DocumentTypeId;
                    DraftObj.Description = mdl.Description;
                    DraftObj.Title = mdl.Title;
                    DraftObj.SendStatus = mdl.SendStatus;
                    DraftObj.ByDepartmentId = mdl.ByDepartmentId;
                    DraftObj.ToDepartmentID = mdl.ToDepartmentID;
                    // DraftObj.SelectedPaperDate = mdl.SelectedDate;
                    //DraftObj.LetterStatusType=mdl.LetterStatusType;
                    DraftObj.SelectedPaperDate = mdl.SelectedPaperDate;
                    DraftObj.ParentId = Convert.ToInt16(mdl.ParentId);
                    DraftObj.ByOfficeCode = mdl.ByOfficeCode;
                    DraftObj.ToOfficeCode = mdl.ToOfficeCode;
                    DraftObj.PaperNature = mdl.PaperNature;

                    DraftObj.SendDateToDept = null;
                    DraftObj.ReceivedDateByDept = null;

                    DraftObj.ItemId = mdl.ItemId;
                    DraftObj.ItemName = mdl.ItemName;
                    DraftObj.CommitteeId = mdl.CommitteeId;
                    DraftObj.CommitteeName = mdl.CommitteeName;
                    DraftObj.eFilePath = mdl.eFilePath;
                    DraftObj.ReplyPdfPath = mdl.ReplyPdfPath;
                    DraftObj.AnnexPdfPath = mdl.AnnexPdfPath;
                    DraftObj.AnnexFilesNameByUser = mdl.AnnexFilesNameByUser;
                    DraftObj.DocFilePath = mdl.DocFilePath;
                    DraftObj.DocFilesNameByUser = mdl.DocFilesNameByUser;
                    DraftObj.LinkedRefNo = mdl.LinkedRefNo;
                    DraftObj.IsDeleted = mdl.IsDeleted;
                    DraftObj.CurYear = mdl.CurYear;
                    // DraftObj.CurrentBranchId = mdl.CurrentBranchId;
                    DraftObj.CurrentEmpAadharID = mdl.CurrentEmpAadharID;

                    var lastNumber = (from eF in ctx.pHouseCommitteeSubFiles orderby eF.RecordId descending select eF.RecordId).FirstOrDefault();
                    DraftObj.PaperRefNo = System.DateTime.Now.Year.ToString() + "/" + Convert.ToString(lastNumber + 1);

                    DraftObj.CreatedDate = mdl.CreatedDate;
                    DraftObj.CreatedBy = mdl.CreatedBy;
                    ctx.pHouseCommitteeSubFiles.Add(DraftObj);
                    ctx.SaveChanges();

                    //For Updating Item Pendency Table 
                    if (mdl.PaperNature != 3)
                    {
                        var LinkedRecordId = (from eF in ctx.pHouseCommitteeSubFiles orderby eF.RecordId descending select eF.RecordId).FirstOrDefault();
                        var eFileId = (from eF in ctx.pHouseCommitteeSubFiles orderby eF.RecordId descending select eF.eFileID).FirstOrDefault();
                        var CommitteeId = (from eF in ctx.pHouseCommitteeSubFiles orderby eF.RecordId descending select eF.CommitteeId).FirstOrDefault();
                        string[] s = mdl.PendencyIDs.Split(',');
                        for (int i = 0; i < s.Length; i++)
                        {
                            if (s[i] != "")
                            {
                                string[] Ids = s[i].Split('_');
                                string comp = Ids[0].Replace("'", "");
                                comp = comp.Trim();
                                if (comp == "MP") //Update in Main Pendency Table
                                {

                                    int PID = Convert.ToInt16(Ids[1].Replace("'", ""));
                                    SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeItemPendency obj = ctx.pHouseCommitteeItemPendency.Single(m => m.PendencyId == PID);
                                    obj.IsLinked = true;
                                    obj.Linked_RecordId = LinkedRecordId;
                                    obj.eFileID = eFileId;
                                    obj.CommitteeId = CommitteeId;
                                    ctx.SaveChanges();
                                }
                                else //Update in sub Item pendency table
                                {

                                    int PID = Convert.ToInt16(Ids[1].Replace("'", ""));
                                    SBL.DomainModel.Models.CommitteeReport.pHouseCommitteeSubItemPendency obj = ctx.pHouseCommitteeSubItemPendency.Single(m => m.SubPendencyId == PID);
                                    obj.IsLinked = true;
                                    obj.Linked_RecordId = LinkedRecordId;
                                    obj.eFileID = eFileId;
                                    obj.CommitteeId = CommitteeId;
                                    ctx.SaveChanges();

                                }

                            }
                        }
                    }
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }
        public static object nGeteFilesByCommitteeId(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    int CommitteeId = Convert.ToInt32(param.ToString());
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              where efilevalues.CommitteeId == CommitteeId && efilevalues.IsDeleted == false
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                              }).ToList().OrderByDescending(x => x.FromYear).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    return eFileListAlls;
                    //return obj;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static object nGeteFilesByCommnDeptId(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    //int CommitteeId = Convert.ToInt32(param.ToString());
                    eFileList Cmodel = param as eFileList;
                    List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                              where efilevalues.CommitteeId == Cmodel.CommitteeId && efilevalues.IsDeleted == false
                                                                              && efilevalues.SelectedDeptId == Cmodel.DepartmentId
                                                                              select new SBL.DomainModel.Models.eFile.eFileList
                                                                              {
                                                                                  eFileID = efilevalues.eFileID,
                                                                                  eFileNumber = efilevalues.eFileNumber,
                                                                                  FromYear = efilevalues.FromYear,
                                                                                  ToYear = efilevalues.ToYear,
                                                                                  eFileSubject = efilevalues.eFileSubject,
                                                                              }).ToList().OrderByDescending(x => x.FromYear).ToList();
                    SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                    eFileListAlls.eFileLists = eFileList;
                    return eFileListAlls;
                    //return obj;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        //For new Efiles Module
        ////////////////////////////////////////////////////////////////////////////////
        private static eFileListAll GetAlleFiles(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                pHouseCommitteeFiles Cmodel = param as pHouseCommitteeFiles;
                //string[] str = param as string[];
                string DeptId = Convert.ToString(Cmodel.CurrentDeptId);

                //int Officecode = 0;
                //int.TryParse(str[1], out Officecode);

                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(DeptId))
                {
                    deptlist = DeptId.Split(',').Distinct().ToList();
                }
                string DistDept = deptlist.Aggregate((a, x) => a + "," + x);

                List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                          join departmentvalues in context.departments
                                                                          on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                          // on efilevalues.SelectedDeptId equals departmentvalues.deptId
                                                                          //join user in context.users
                                                                          //on efilevalues.CreatedBy equals user.UserId
                                                                          join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                          from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                          //join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                          join etypeobj in context.mCommitteeReplyItemType on efilevalues.eFileTypeID equals etypeobj.Id into etypeobj_leftjoin
                                                                          from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                          where deptlist.Contains(efilevalues.DepartmentId) && efilevalues.IsDeleted == false
                                                                          // && efilevalues.CommitteeId == VarCommittee.id
                                                                          //&& efilevalues.Officecode == Officecode
                                                                          select new SBL.DomainModel.Models.eFile.eFileList
                                                                          {
                                                                              //Department = departmentvalues.deptname,
                                                                              eFileID = efilevalues.eFileID,
                                                                              eFileNumber = efilevalues.eFileNumber,
                                                                              eFileSubject = efilevalues.eFileSubject,
                                                                              Status = efilevalues.Status,
                                                                              CreateDate = efilevalues.CreatedDate,
                                                                              //CreatedBy = user.UserName,
                                                                              olddesc = efilevalues.OldDescription,
                                                                              newdesc = efilevalues.NewDescription,
                                                                              CommitteeId = efilevalues.CommitteeId,
                                                                              DepartmentId = efilevalues.DepartmentId,
                                                                              //  DeptAbbr = departmentvalues.deptabbr,
                                                                              DeptAbbr = (from mc in context.departments where mc.deptId == efilevalues.SelectedDeptId select mc.deptabbr).FirstOrDefault(),
                                                                              Committee = Comms.CommitteeName,
                                                                              CommitteeAbbr = Comms.Abbreviation,
                                                                              FromYear = efilevalues.FromYear,
                                                                              ToYear = efilevalues.ToYear,
                                                                              eFileTypeName = etypeobj.ItemTypeName,
                                                                              FileAttched_DraftId = efilevalues.FileAttched_DraftId,
                                                                              Department = (from mc in context.departments where mc.deptId == efilevalues.SelectedDeptId select mc.deptname).FirstOrDefault()

                                                                              //}).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                                                                          }).ToList().OrderByDescending(x => x.FromYear).ToList();
                List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();


                //For Showing List according to the branches assigned for user
                var ListIs = eFileList.OrderByDescending(a => a.FromYear).ToList();

                List<eFileList> listnew = new List<eFileList>();
                using (eFileContext ctxCommitee = new eFileContext())
                {
                    foreach (var item in Cmodel.tCommitteeList)
                    {
                        int CommitteId = item.CommitteeId;
                        var CheckList = ListIs.Where(a => a.CommitteeId == CommitteId).ToList();
                        listnew.AddRange(CheckList);
                    }
                }
                SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                eFileListAlls.eFileLists = listnew;




                //SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                //eFileListAlls.eFileLists = eFileList;
                //eFileListAlls.Departments = departments;
                return eFileListAlls;
            }
        }
        public static object SaveNewFileEntry(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {
                    SBL.DomainModel.Models.eFile.eFile model = new SBL.DomainModel.Models.eFile.eFile();
                    eFileList mdl = param as eFileList;
                    model.CommitteeId = mdl.CommitteeId;
                    model.DepartmentId = mdl.DepartmentId;
                    model.SelectedDeptId = mdl.SelectedDeptId;
                    model.FromYear = mdl.FromYear;
                    model.ToYear = mdl.ToYear;
                    model.eFileTypeID = mdl.eFileTypeId;
                    model.eFileNumber = mdl.eFileNumber;
                    model.eFileSubject = mdl.eFileSubject;
                    model.PreviousLinkId = mdl.PreviousLinkId;
                    //model.FileAttched_DraftId = mdl.FileAttched_DraftId;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    model.Status = true;
                    context.eFiles.Add(model);
                    context.SaveChanges();
                    if (mdl.PreviousLinkId != null)
                    {
                        SBL.DomainModel.Models.eFile.eFile obj = context.eFiles.Single(m => m.eFileID == mdl.PreviousLinkId);
                        obj.FutureLinkId = model.eFileID;
                        context.SaveChanges();
                    }
                    return 0;
                }
            }
            catch
            {
                return 1; //Error
            }
        }
        private static SBL.DomainModel.Models.eFile.eFileList GeteFileById(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                var eFileList = (from efilevalues in context.eFiles
                                 join departmentvalues in context.departments
                                 on efilevalues.DepartmentId equals departmentvalues.deptId
                                 where efilevalues.eFileID == eFileId
                                 select new SBL.DomainModel.Models.eFile.eFileList
                                 {
                                     CommitteeId = efilevalues.CommitteeId,
                                     DepartmentId = efilevalues.SelectedDeptId,
                                     Department = departmentvalues.deptname,
                                     eFileID = efilevalues.eFileID,
                                     eFileNumber = efilevalues.eFileNumber,
                                     eFileSubject = efilevalues.eFileSubject,
                                     eFileTypeId = efilevalues.eFileTypeID,
                                     FromYear = efilevalues.FromYear,
                                     ToYear = efilevalues.ToYear,
                                     FileAttched_DraftId = efilevalues.FileAttched_DraftId,
                                     Status = efilevalues.Status

                                 }).FirstOrDefault();
                return eFileList;
            }
        }
        public static object GetAllDocumentsByFileId(object param)
        {
            List<pHouseCommitteeFiles> Parentlist = new List<pHouseCommitteeFiles>();
            List<pHouseCommitteeSubFiles> Childlist = new List<pHouseCommitteeSubFiles>();

            pHouseCommitteeFiles pHouseCommitteeFiles = param as pHouseCommitteeFiles;
            List<pHouseCommitteeFiles> list = new List<pHouseCommitteeFiles>();
            //int eFileid = Convert.ToInt16(param);
            int eFileid = pHouseCommitteeFiles.eFileID;

            using (eFileContext ctx = new eFileContext())
            {
                if (pHouseCommitteeFiles.CurrentDeptId != "HPD0001")// vs user
                {
                    var ParentList = (from a in ctx.pHouseCommitteeFiles
                                      where a.eFileID == eFileid && a.SendStatus == true
                                      select a
                                     ).ToList().OrderByDescending(a => a.CreatedDate);


                    var CList = from element in ctx.pHouseCommitteeSubFiles
                                where element.SendStatus == true
                                group element by element.ParentId
                                    into groups
                                    select groups.OrderByDescending(p => p.CreatedDate).FirstOrDefault();
                    var ChildList = CList.ToList();
                    var pmm = ParentList.ToList();
                    foreach (var item in pmm)
                    {

                        var Check = ChildList.Where(a => a.ParentId == item.Id).ToList();
                        if (Check.Count != 0)
                        {
                            var Check1 = ChildList.Where(a => a.ParentId == item.Id).ToList();
                            foreach (var item1 in Check1)
                            {
                                pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                                mdl.RecordId = item1.RecordId;
                                mdl.Id = item1.ParentId;
                                mdl.ParentId = item1.ParentId;

                                mdl.eFileName = item1.eFileName;
                                mdl.PaperNature = item1.PaperNature;
                                mdl.CommitteeName = (from mc in ctx.committees where mc.CommitteeId == item1.CommitteeId select mc.CommitteeName).FirstOrDefault();
                                mdl.CommitteeNameAbbrev = (from mc in ctx.committees where mc.CommitteeId == item1.CommitteeId select mc.Abbreviation).FirstOrDefault();

                                mdl.eFilePath = item1.eFilePath;
                                mdl.AnnexPdfPath = item1.AnnexPdfPath;
                                mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                                mdl.DocFilePath = item1.DocFilePath;
                                mdl.DocFilesNameByUser = item1.DocFilesNameByUser;
                                mdl.ReplyPdfPath = item1.ReplyPdfPath;

                                mdl.Title = item1.Title;
                                mdl.Description = item1.Description;

                                mdl.ToDepartmentName = (from mc in ctx.departments where mc.deptId == item1.ToDepartmentID select mc.deptname).FirstOrDefault();
                                mdl.ToDepartmentNameAbbrev = (from mc in ctx.departments where mc.deptId == item1.ToDepartmentID select mc.deptabbr).FirstOrDefault();


                                mdl.ByDepartmentName = (from mc in ctx.departments where mc.deptId == item1.ByDepartmentId select mc.deptname).FirstOrDefault();
                                mdl.CreatedDate = item1.CreatedDate;
                                mdl.ItemId = item1.ItemId;
                                mdl.ItemName = item1.ItemName;
                                mdl.eFileID = item1.eFileID;
                                mdl.PaperRefNo = item1.PaperRefNo;
                                mdl.SelectedPaperDate = item1.SelectedPaperDate;
                                // mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreatedDate).ToString("dd/MM/yyyy");
                                mdl.ToDepartmentID = item1.ToDepartmentID;
                                mdl.ByDepartmentId = item1.ByDepartmentId;
                                mdl.DocumentTypeId = item1.DocumentTypeId;
                                mdl.PaperNature = item1.PaperNature;
                                mdl.LinkedRefNo = item1.LinkedRefNo;
                                mdl.SendStatus = item1.SendStatus;

                                mdl.IsVerified = item1.IsVerified;
                               // mdl.VerifiedBy = item1.VerifiedBy;
                                mdl.VerifiedBy = (from EN in ctx.mStaff where EN.AadharID == item1.VerifiedBy select EN.StaffName).FirstOrDefault();
                                mdl.RejectReason = item1.RejectReason;
                                list.Add(mdl);
                            }
                        }
                        else
                        {

                            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                            mdl.Id = item.Id;
                            mdl.RecordId = item.RecordId;
                            mdl.ParentId = item.ParentId;
                            mdl.eFileName = item.eFileName;
                            mdl.PaperNature = item.PaperNature;
                            mdl.CommitteeName = (from mc in ctx.committees where mc.CommitteeId == item.CommitteeId select mc.CommitteeName).FirstOrDefault();
                            mdl.CommitteeNameAbbrev = (from mc in ctx.committees where mc.CommitteeId == item.CommitteeId select mc.Abbreviation).FirstOrDefault();

                            mdl.eFilePath = item.eFilePath;
                            mdl.AnnexPdfPath = item.AnnexPdfPath;
                            mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                            mdl.DocFilePath = item.DocFilePath;
                            mdl.DocFilesNameByUser = item.DocFilesNameByUser;
                            mdl.ReplyPdfPath = item.ReplyPdfPath;

                            mdl.Title = item.Title;
                            mdl.Description = item.Description;

                            mdl.ToDepartmentName = (from mc in ctx.departments where mc.deptId == item.ToDepartmentID select mc.deptname).FirstOrDefault();
                            mdl.ToDepartmentNameAbbrev = (from mc in ctx.departments where mc.deptId == item.ToDepartmentID select mc.deptabbr).FirstOrDefault();

                            mdl.ByDepartmentName = (from mc in ctx.departments where mc.deptId == item.ByDepartmentId select mc.deptname).FirstOrDefault();

                            mdl.CreatedDate = item.CreatedDate;
                            mdl.ItemId = item.ItemId;
                            mdl.ItemName = item.ItemName;
                            mdl.eFileID = item.eFileID;
                            mdl.PaperRefNo = item.PaperRefNo;
                            mdl.SelectedPaperDate = item.SelectedPaperDate;
                            //mdl.SelectedPaperDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
                            mdl.ToDepartmentID = item.ToDepartmentID;
                            mdl.ByDepartmentId = item.ByDepartmentId;
                            mdl.DocumentTypeId = item.DocumentTypeId;
                            mdl.PaperNature = item.PaperNature;
                            mdl.LinkedRefNo = item.LinkedRefNo;
                            mdl.SendStatus = item.SendStatus;

                            mdl.IsVerified = item.IsVerified;
                           // mdl.VerifiedBy = item.VerifiedBy;
                            mdl.VerifiedBy = (from EN in ctx.mStaff where EN.AadharID == item.VerifiedBy select EN.StaffName).FirstOrDefault();
                            mdl.RejectReason = item.RejectReason;
                            list.Add(mdl);
                        }

                    }
                }
                else
                {
                    var ParentList = (from a in ctx.pHouseCommitteeFiles
                                      where a.eFileID == eFileid
                                      select a
                                     ).ToList().OrderByDescending(a => a.CreatedDate);


                    var CList = from element in ctx.pHouseCommitteeSubFiles
                                group element by element.ParentId
                                    into groups
                                    select groups.OrderByDescending(p => p.CreatedDate).FirstOrDefault();
                    var ChildList = CList.ToList();
                    var pmm = ParentList.ToList();
                    foreach (var item in pmm)
                    {

                        var Check = ChildList.Where(a => a.ParentId == item.Id).ToList();
                        if (Check.Count != 0)
                        {
                            var Check1 = ChildList.Where(a => a.ParentId == item.Id).ToList();
                            foreach (var item1 in Check1)
                            {
                                pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                                mdl.RecordId = item1.RecordId;
                                mdl.Id = item1.ParentId;
                                mdl.ParentId = item1.ParentId;

                                mdl.eFileName = item1.eFileName;
                                mdl.PaperNature = item1.PaperNature;
                                mdl.CommitteeName = (from mc in ctx.committees where mc.CommitteeId == item1.CommitteeId select mc.CommitteeName).FirstOrDefault();
                                mdl.CommitteeNameAbbrev = (from mc in ctx.committees where mc.CommitteeId == item1.CommitteeId select mc.Abbreviation).FirstOrDefault();

                                mdl.eFilePath = item1.eFilePath;
                                mdl.AnnexPdfPath = item1.AnnexPdfPath;
                                mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                                mdl.DocFilePath = item1.DocFilePath;
                                mdl.DocFilesNameByUser = item1.DocFilesNameByUser;
                                mdl.ReplyPdfPath = item1.ReplyPdfPath;

                                mdl.Title = item1.Title;
                                mdl.Description = item1.Description;

                                mdl.ToDepartmentName = (from mc in ctx.departments where mc.deptId == item1.ToDepartmentID select mc.deptname).FirstOrDefault();
                                mdl.ToDepartmentNameAbbrev = (from mc in ctx.departments where mc.deptId == item1.ToDepartmentID select mc.deptabbr).FirstOrDefault();

                                mdl.ByDepartmentName = (from mc in ctx.departments where mc.deptId == item1.ByDepartmentId select mc.deptname).FirstOrDefault();
                                mdl.CreatedDate = item1.CreatedDate;
                                mdl.ItemId = item1.ItemId;
                                mdl.ItemName = item1.ItemName;
                                mdl.eFileID = item1.eFileID;
                                mdl.PaperRefNo = item1.PaperRefNo;
                                mdl.SelectedPaperDate = item1.SelectedPaperDate;
                                // mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreatedDate).ToString("dd/MM/yyyy");
                                mdl.ToDepartmentID = item1.ToDepartmentID;
                                mdl.ByDepartmentId = item1.ByDepartmentId;
                                mdl.DocumentTypeId = item1.DocumentTypeId;
                                mdl.PaperNature = item1.PaperNature;
                                mdl.LinkedRefNo = item1.LinkedRefNo;
                                mdl.SendStatus = item1.SendStatus;

                                mdl.IsVerified = item1.IsVerified;
                                //mdl.VerifiedBy = item1.VerifiedBy;
                                mdl.VerifiedBy = (from EN in ctx.mStaff where EN.AadharID == item1.VerifiedBy select EN.StaffName).FirstOrDefault();
                                mdl.RejectReason = item1.RejectReason;
                                list.Add(mdl);
                            }
                        }
                        else
                        {

                            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                            mdl.Id = item.Id;
                            mdl.RecordId = item.RecordId;
                            mdl.ParentId = item.ParentId;
                            mdl.eFileName = item.eFileName;
                            mdl.PaperNature = item.PaperNature;
                            mdl.CommitteeName = (from mc in ctx.committees where mc.CommitteeId == item.CommitteeId select mc.CommitteeName).FirstOrDefault();
                            mdl.CommitteeNameAbbrev = (from mc in ctx.committees where mc.CommitteeId == item.CommitteeId select mc.Abbreviation).FirstOrDefault();

                            mdl.eFilePath = item.eFilePath;
                            mdl.AnnexPdfPath = item.AnnexPdfPath;
                            mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                            mdl.DocFilePath = item.DocFilePath;
                            mdl.DocFilesNameByUser = item.DocFilesNameByUser;
                            mdl.ReplyPdfPath = item.ReplyPdfPath;

                            mdl.Title = item.Title;
                            mdl.Description = item.Description;

                            mdl.ToDepartmentName = (from mc in ctx.departments where mc.deptId == item.ToDepartmentID select mc.deptname).FirstOrDefault();
                            mdl.ToDepartmentNameAbbrev = (from mc in ctx.departments where mc.deptId == item.ToDepartmentID select mc.deptabbr).FirstOrDefault();

                            mdl.ByDepartmentName = (from mc in ctx.departments where mc.deptId == item.ByDepartmentId select mc.deptname).FirstOrDefault();

                            mdl.CreatedDate = item.CreatedDate;
                            mdl.ItemId = item.ItemId;
                            mdl.ItemName = item.ItemName;
                            mdl.eFileID = item.eFileID;
                            mdl.PaperRefNo = item.PaperRefNo;
                            mdl.SelectedPaperDate = item.SelectedPaperDate;
                            //mdl.SelectedPaperDate = Convert.ToDateTime(item.CreatedDate).ToString("dd/MM/yyyy");
                            mdl.ToDepartmentID = item.ToDepartmentID;
                            mdl.ByDepartmentId = item.ByDepartmentId;
                            mdl.DocumentTypeId = item.DocumentTypeId;
                            mdl.PaperNature = item.PaperNature;
                            mdl.LinkedRefNo = item.LinkedRefNo;
                            mdl.SendStatus = item.SendStatus;

                            mdl.IsVerified = item.IsVerified;
                            //mdl.VerifiedBy = item.VerifiedBy;
                            mdl.VerifiedBy = (from EN in ctx.mStaff where EN.AadharID == item.VerifiedBy select EN.StaffName).FirstOrDefault();
                            mdl.RejectReason = item.RejectReason;
                            list.Add(mdl);
                        }

                    }
                }
                //var ListIs = list.OrderByDescending(a => a.CreatedDate).ToList();
                list = list.OrderByDescending(a => a.CreatedDate).ToList();
                // pHouseCommitteeFiles Cmodel = param as pHouseCommitteeFiles;
                return list;
            }
        }
        private static eFileListAll GetFilesByLinkedId(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param);
                List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                          join departmentvalues in context.departments
                                                                          on efilevalues.DepartmentId equals departmentvalues.deptId
                                                                          join Comms in context.committees on efilevalues.CommitteeId equals Comms.CommitteeId into Comms_leftjoin
                                                                          from Comms in Comms_leftjoin.DefaultIfEmpty()
                                                                          //join etypeobj in context.eFileType on efilevalues.eFileTypeID equals etypeobj.eTypeID into etypeobj_leftjoin
                                                                          join etypeobj in context.mCommitteeReplyItemType on efilevalues.eFileTypeID equals etypeobj.Id into etypeobj_leftjoin
                                                                          from etypeobj in etypeobj_leftjoin.DefaultIfEmpty()
                                                                          where efilevalues.eFileID == eFileId
                                                                          select new SBL.DomainModel.Models.eFile.eFileList
                                                                          {
                                                                              //Department = departmentvalues.deptname,
                                                                              eFileID = efilevalues.eFileID,
                                                                              eFileNumber = efilevalues.eFileNumber,
                                                                              eFileSubject = efilevalues.eFileSubject,
                                                                              Status = efilevalues.Status,
                                                                              CreateDate = efilevalues.CreatedDate,
                                                                              //CreatedBy = user.UserName,
                                                                              olddesc = efilevalues.OldDescription,
                                                                              newdesc = efilevalues.NewDescription,
                                                                              CommitteeId = efilevalues.CommitteeId,
                                                                              DepartmentId = efilevalues.DepartmentId,
                                                                              //  DeptAbbr = departmentvalues.deptabbr,
                                                                              DeptAbbr = (from mc in context.departments where mc.deptId == efilevalues.SelectedDeptId select mc.deptabbr).FirstOrDefault(),
                                                                              Committee = Comms.CommitteeName,
                                                                              CommitteeAbbr = Comms.Abbreviation,
                                                                              FromYear = efilevalues.FromYear,
                                                                              ToYear = efilevalues.ToYear,
                                                                              eFileTypeName = etypeobj.ItemTypeName,
                                                                              FileAttched_DraftId = efilevalues.FileAttched_DraftId,
                                                                              Department = (from mc in context.departments where mc.deptId == efilevalues.SelectedDeptId select mc.deptname).FirstOrDefault()

                                                                          }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                List<SBL.DomainModel.Models.Department.mDepartment> departments = (from dept in context.departments select dept).OrderBy(x => x.deptname).ToList();
                SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                eFileListAlls.eFileLists = eFileList;
                eFileListAlls.Departments = departments;
                return eFileListAlls;

            }
        }
        public static object UpdateFileEntry(object param)
        {
            try
            {
                using (eFileContext context = new eFileContext())
                {

                    SBL.DomainModel.Models.eFile.eFile model = new SBL.DomainModel.Models.eFile.eFile();
                    eFileList mdl = param as eFileList;
                    model.eFileID = mdl.eFileID;
                    var SelectUpdateresult = (from attachments in context.eFiles where attachments.eFileID == model.eFileID select attachments).FirstOrDefault();
                    SelectUpdateresult.eFileNumber = mdl.eFileNumber;
                    SelectUpdateresult.CommitteeId = mdl.CommitteeId;
                    SelectUpdateresult.SelectedDeptId = mdl.SelectedDeptId;
                    SelectUpdateresult.DepartmentId = mdl.DepartmentId;
                    SelectUpdateresult.FromYear = mdl.FromYear;
                    SelectUpdateresult.ToYear = mdl.ToYear;
                    SelectUpdateresult.eFileTypeID = mdl.eFileTypeId;
                    SelectUpdateresult.eFileSubject = mdl.eFileSubject;
                    SelectUpdateresult.Status = mdl.Status;
                    SelectUpdateresult.PreviousLinkId = mdl.PreviousLinkId;
                    //SelectUpdateresult.FileAttched_DraftId = mdl.FileAttched_DraftId;
                    context.Entry(SelectUpdateresult).State = EntityState.Modified;
                    context.SaveChanges();
                    if (mdl.PreviousLinkId != null)
                    {
                        SBL.DomainModel.Models.eFile.eFile obj = context.eFiles.Single(m => m.eFileID == mdl.PreviousLinkId);
                        obj.FutureLinkId = model.eFileID;
                        context.SaveChanges();
                    }
                    context.Close();
                    return 0;
                }
            }
            catch
            {
                return 1; //Error
            }
        }
        private static eFileListAll GeteFilesListToLink(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                string[] str = param as string[];
                int CommitteeId = Convert.ToInt32(str[3]);
                //string DeptId = str[4];
                List<SBL.DomainModel.Models.eFile.eFileList> eFileList = (from efilevalues in context.eFiles
                                                                          where efilevalues.IsDeleted == false && efilevalues.CommitteeId == CommitteeId
                                                                          //&& efilevalues.SelectedDeptId == DeptId
                                                                          select new SBL.DomainModel.Models.eFile.eFileList
                                                                          {
                                                                              eFileID = efilevalues.eFileID,
                                                                              eFileNumber = efilevalues.eFileNumber
                                                                          }).ToList().OrderBy(x => x.Committee).ThenBy(x => x.eFileNumber).ToList();
                SBL.DomainModel.Models.eFile.eFileListAll eFileListAlls = new DomainModel.Models.eFile.eFileListAll();
                eFileListAlls.eFileLists = eFileList;
                return eFileListAlls;
            }
        }
        private static eFileList GeteFileDetailsByID(object param)
        {
            using (eFileContext context = new eFileContext())
            {
                int eFileId = Convert.ToInt32(param.ToString());
                SBL.DomainModel.Models.eFile.eFileList eFileList = new DomainModel.Models.eFile.eFileList();
                var a = (from efilevalues in context.eFiles
                         where efilevalues.eFileID == eFileId
                         select efilevalues
                                  ).ToList();

                foreach (var item in a)
                {
                    eFileList.FromYear = item.FromYear;
                    eFileList.ToYear = item.ToYear;
                    eFileList.eFileNumber = item.eFileNumber;
                    eFileList.eFileSubject = item.eFileSubject;
                    eFileList.PreviousLinkId = item.PreviousLinkId;
                    if (item.PreviousLinkId != null)
                    {
                        eFileList.PreviousFileNumber = (from mc in context.eFiles where mc.eFileID == item.PreviousLinkId select mc.eFileNumber).FirstOrDefault();
                    }
                    if (item.FutureLinkId != null)
                    {
                        eFileList.FutureFileNumber = (from mc in context.eFiles where mc.eFileID == item.FutureLinkId select mc.eFileNumber).FirstOrDefault();
                    }

                    eFileList.FutureLinkId = item.FutureLinkId;
                }
                return eFileList;
            }
        }



        /////////////////////////////////////
        //For Item Pendency New
        ////////////////////////////////////////

        public static object SaveNewPendency(object param)
        {
            try
            {
                pHouseCommitteeItemPendency mdl = param as pHouseCommitteeItemPendency;
                //eFileAttachment DraftObj = new eFileAttachment();                
                pHouseCommitteeItemPendency DraftObj = new pHouseCommitteeItemPendency();
                using (eFileContext ctx = new eFileContext())
                {


                    DraftObj.SentByDepartmentId = mdl.SentByDepartmentId;
                    DraftObj.ItemTypeId = mdl.ItemTypeId;
                    DraftObj.ItemTypeName = mdl.ItemTypeName;
                    if (mdl.ItemTypeId == 10)
                    {
                        DraftObj.SubItemTypeId = mdl.SubItemTypeId;
                        DraftObj.SubItemTypeName = mdl.SubItemTypeName;
                        DraftObj.CAGYear = mdl.CAGYear;

                    }
                    else
                    {
                        DraftObj.FinancialYear = mdl.FinancialYear;
                    }
                    DraftObj.ItemNumber = mdl.ItemNumber;
                    DraftObj.ItemNatureId = mdl.ItemNatureId;
                    DraftObj.ItemNature = mdl.ItemNature;
                    DraftObj.ItemDescription = mdl.ItemDescription;
                    DraftObj.Status = mdl.Status;
                    DraftObj.CreatedBy = mdl.CreatedBy;
                    DraftObj.ModifiedBy = mdl.ModifiedBy;
                    DraftObj.SentOnDate = DateTime.Now;
                    DraftObj.CreatedDate = DateTime.Now;
                    DraftObj.ModifiedDate = DateTime.Now;
                    DraftObj.AnnexPdfPath = mdl.AnnexPdfPath;
                    DraftObj.ItemReplyPdfPath = mdl.ItemReplyPdfPath;
                    DraftObj.AnnexFilesNameByUser = mdl.AnnexFilesNameByUser;
                    DraftObj.DocFileName = mdl.DocFileName;
                    DraftObj.DocFilePath = mdl.DocFilePath;
                    ctx.pHouseCommitteeItemPendency.Add(DraftObj);
                    ctx.SaveChanges();
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }
        public static object SaveToComposedPendency(object param)
        {
            try
            {
                pHouseCommitteeSubItemPendency mdl = param as pHouseCommitteeSubItemPendency;
                //eFileAttachment DraftObj = new eFileAttachment();                
                pHouseCommitteeSubItemPendency DraftObj = new pHouseCommitteeSubItemPendency();
                using (eFileContext ctx = new eFileContext())
                {
                    DraftObj.ParentId = mdl.ParentId;
                    DraftObj.SentOnDateByVS = mdl.SentOnDateByVS;
                    DraftObj.SentByDepartmentId = mdl.SentByDepartmentId;
                    DraftObj.ItemTypeId = mdl.ItemTypeId;
                    DraftObj.ItemTypeName = mdl.ItemTypeName;


                    if (mdl.ItemTypeId == 10)
                    {
                        DraftObj.SubItemTypeId = mdl.SubItemTypeId;
                        DraftObj.SubItemTypeName = mdl.SubItemTypeName;
                        DraftObj.CAGYear = mdl.CAGYear;

                    }
                    else
                    {
                        DraftObj.FinancialYear = mdl.FinancialYear;
                    }



                    DraftObj.ItemNumber = mdl.ItemNumber;
                    DraftObj.ItemNatureId = mdl.ItemNatureId;
                    DraftObj.ItemNature = mdl.ItemNature;
                    DraftObj.ItemDescription = mdl.ItemDescription;
                    DraftObj.Status = mdl.Status;
                    DraftObj.CreatedBy = mdl.CreatedBy;
                    DraftObj.ModifiedBy = mdl.ModifiedBy;
                    DraftObj.SentOnDate = DateTime.Now;
                    DraftObj.CreatedDate = DateTime.Now;
                    DraftObj.ModifiedDate = DateTime.Now;
                    DraftObj.ItemReplyPdfPath = mdl.ItemReplyPdfPath;
                    DraftObj.AnnexPdfPath = mdl.AnnexPdfPath;
                    DraftObj.DocFileName = mdl.DocFileName;
                    DraftObj.DocFilePath = mdl.DocFilePath;
                    DraftObj.AnnexFilesNameByUser = mdl.AnnexFilesNameByUser;
                    ctx.pHouseCommitteeSubItemPendency.Add(DraftObj);
                    ctx.SaveChanges();
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }


        public static object GetSubItemList(object param)
        {
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.mCommitteeReplySubItemType where _ObjeFileContext.IsActive == true select _ObjeFileContext).ToList();
            return query;
        }

        public static object GetCustomFileItemList(object param)
        {
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.mCommitteeReplyItemType where _ObjeFileContext.IsActive == true select _ObjeFileContext).OrderBy(s => s.ItemTypeName).Take(2).ToList();
            return query;
        }

        public static object GetAllCommitteeList(object param)
        {
            eFileContext ctxt = new eFileContext();
            var query = (from _ObjeFileContext in ctxt.committees select _ObjeFileContext).ToList();
            return query;
        }


        public static object UpdateAcceptStatus(object param)
        {
            eFileContext ctxt = new eFileContext();
            pHouseCommitteeSubFiles mdl = param as pHouseCommitteeSubFiles;            
            //int PID = mdl.RecordId;
            SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles obj = ctxt.pHouseCommitteeSubFiles.Single(m => m.RecordId == mdl.RecordId);
            obj.IsVerified = true;
            obj.VerifiedBy = mdl.VerifiedBy;
            ctxt.SaveChanges();

            return 0; 
        }

        public static object RejectDocumentsStatus(object param)
        {
            eFileContext ctxt = new eFileContext();
            pHouseCommitteeSubFiles mdl = param as pHouseCommitteeSubFiles;
            //int PID = mdl.RecordId;
            SBL.DomainModel.Models.eFile.pHouseCommitteeSubFiles obj = ctxt.pHouseCommitteeSubFiles.Single(m => m.RecordId == mdl.RecordId);
            obj.IsVerified = false;
            obj.VerifiedBy = mdl.VerifiedBy;
            obj.RejectReason = mdl.RejectReason;
            ctxt.SaveChanges();
            return 0;
        }


        public static object GetSearchedList(object param)
        {
            pHouseCommitteeFiles model = param as pHouseCommitteeFiles;
            List<pHouseCommitteeFiles> listnew = new List<pHouseCommitteeFiles>();
            List<pHouseCommitteeSubFiles> Childlist = new List<pHouseCommitteeSubFiles>();
            List<pHouseCommitteeFiles> list = new List<pHouseCommitteeFiles>();

            string[] arr = param as string[];
            int nItemTypeId = Convert.ToInt16(arr[0]);

            int nCommId = Convert.ToInt16(arr[1]);
            string nDeptId = arr[2];
            string Curr_Dept_Id = arr[3];
            eFileContext dbn = new eFileContext();
            using (eFileContext db = new eFileContext())
            {
                var predicate = PredicateBuilder.True<npHouseCommitteeFiles>();
                if (nItemTypeId != 0)
                    predicate = predicate.And(q => q.ItemId == nItemTypeId);

                if (nDeptId != "0")
                    predicate = predicate.And(q => q.ByDepartmentId == nDeptId || q.ToDepartmentID == nDeptId);
                if (nCommId != 0)
                    predicate = predicate.And(q => q.CommitteeId == nCommId);

                if (Curr_Dept_Id == "HPD0001")
                {
                    
                }
                else
                {
                    predicate = predicate.And(q => q.SendStatus == true);
                
                }



                var Parentlist = (from a in db.pHouseCommitteeFiles
                                  select new npHouseCommitteeFiles
                                  {
                                      Id = a.Id,
                                      ParentId = a.Id,
                                      eFileName = a.eFileName,
                                      PaperNature = a.PaperNature,
                                      eFilePath = a.eFilePath,
                                      ReplyPdfPath = a.ReplyPdfPath,
                                      AnnexPdfPath = a.AnnexPdfPath,
                                      AnnexFilesNameByUser = a.AnnexFilesNameByUser,
                                      DocFilePath = a.DocFilePath,
                                      DocFilesNameByUser = a.DocFilesNameByUser,
                                      Title = a.Title,
                                      Description = a.Description,
                                      CreatedDate = a.CreatedDate,
                                      eFileID = a.eFileID,
                                      PaperRefNo = a.PaperRefNo,
                                      //ToDepartmentID = a.ToDepartmentID,
                                      ToOfficeCode = a.ToOfficeCode,
                                      SelectedPaperDate = a.SelectedPaperDate,
                                      ToDepartmentID = a.ToDepartmentID,
                                      ToDepartmentName = (from mc in db.departments where mc.deptId == a.ToDepartmentID select mc.deptname).FirstOrDefault(),
                                      ByDepartmentId = a.ByDepartmentId,
                                      ByDepartmentName = (from mc in db.departments where mc.deptId == a.ByDepartmentId select mc.deptname).FirstOrDefault(),
                                      ByOfficeCode = a.ByOfficeCode,
                                      ItemName = a.ItemName,
                                      ItemId = a.ItemId,
                                      DocumentTypeId = a.DocumentTypeId,
                                      LinkedRefNo = a.LinkedRefNo,
                                      SendStatus = a.SendStatus,
                                      IsVerified = a.IsVerified,
                                      RejectReason=a.RejectReason,
                                      //CurrentBranchId = (from mc in db.mBranches where mc.BranchId == a.CurrentBranchId select mc.BranchName).FirstOrDefault(),
                                      CommitteeId = a.CommitteeId,
                                      CommitteeName = (from mc in db.committees where mc.CommitteeId == a.CommitteeId select mc.CommitteeName).FirstOrDefault(),
                                      CommitteeNameAbbrev = (from mc in db.committees where mc.CommitteeId == a.CommitteeId select mc.Abbreviation).FirstOrDefault(),
                                      CurrentEmpAadharID = a.CurrentEmpAadharID,
                                      // EmpName = (from EN in db.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.StaffName).FirstOrDefault(),
                                      // AbbrevBranch = (from mc in db.mBranches where mc.BranchId == a.CurrentBranchId select mc.Abbreviation).FirstOrDefault(),
                                      //  Desingnation = (from EN in db.mStaff where EN.AadharID == a.CurrentEmpAadharID select EN.Designation).FirstOrDefault(),
                                  }).AsExpandable().Where(predicate).OrderByDescending(a => a.Id);


                List<pHouseCommitteeSubFiles> queryAll = new List<pHouseCommitteeSubFiles>();
                var CList = from element in db.pHouseCommitteeSubFiles
                            group element by element.ParentId
                                into groups
                                select groups.OrderByDescending(p => p.CreatedDate).FirstOrDefault();
                queryAll = CList.ToList();
                var ChildList = queryAll.ToList();

                foreach (var item in Parentlist)
                {
                    var Check = ChildList.Where(a => a.ParentId == item.Id).ToList();
                    if (Check.Count != 0)
                    {
                        foreach (var item1 in Check)
                        {
                            pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                            mdl.RecordId = item1.RecordId;
                            mdl.ParentId = item1.ParentId;
                            mdl.Id = item1.ParentId;
                            mdl.eFileName = item1.eFileName;
                            //mdl.PaperNature = item1.PaperNature;

                            mdl.CommitteeName = item1.CommitteeName;
                            mdl.CommitteeId = item1.CommitteeId;
                            mdl.CommitteeNameAbbrev = (from mc in db.committees where mc.CommitteeId == item1.CommitteeId select mc.Abbreviation).FirstOrDefault();
                                     
                            mdl.eFilePath = item1.eFilePath;

                            if (item1.ReplyPdfPath != null)
                            {
                                string ReplyPdfPath = item1.ReplyPdfPath;
                                ReplyPdfPath.Replace(" ", string.Empty);
                                mdl.ReplyPdfPath = ReplyPdfPath;
                            }
                            else
                            {
                                mdl.ReplyPdfPath = item1.ReplyPdfPath; ;
                            }

                            if (item1.AnnexPdfPath != null)
                            {
                                string AnnexPdfPath = item1.AnnexPdfPath;
                                AnnexPdfPath.Replace(" ", string.Empty);
                                mdl.AnnexPdfPath = AnnexPdfPath;
                            }
                            else
                            {
                                mdl.AnnexPdfPath = item1.AnnexPdfPath; ;
                            }



                            // mdl.AnnexPdfPath = item1.AnnexPdfPath;
                            if (item1.AnnexFilesNameByUser != null)
                            {
                                string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                                mdl.AnnexFilesNameByUser = str;

                            }
                            else
                            {
                                mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                            }


                            if (item1.DocFilesNameByUser != null)
                            {
                                string str = System.Text.RegularExpressions.Regex.Replace(item1.DocFilesNameByUser, @"\s+", "");
                                mdl.DocFilesNameByUser = str;

                            }
                            else
                            {
                                mdl.DocFilesNameByUser = item1.DocFilesNameByUser;
                            }


                            mdl.DocFilePath = item1.DocFilePath;
                            mdl.Title = item1.Title;
                            mdl.Description = item1.Description;
                            //mdl.ItemName=item1.
                            mdl.ToDepartmentName = (from mc in dbn.departments where mc.deptId == item1.ToDepartmentID select mc.deptname).FirstOrDefault();
                            mdl.ByDepartmentName = (from mc in dbn.departments where mc.deptId == item1.ByDepartmentId select mc.deptname).FirstOrDefault();
                            mdl.CreatedDate = item1.CreatedDate;
                            mdl.ItemId = item1.ItemId;
                            mdl.ItemName = item1.ItemName;
                            mdl.eFileID = item1.eFileID;
                            mdl.PaperRefNo = item1.PaperRefNo;
                            mdl.SelectedPaperDate = item1.SelectedPaperDate;
                            //  mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreatedDate).ToString("dd/MM/yyyy");               
                            mdl.ToDepartmentID = item1.ToDepartmentID;
                            mdl.ByDepartmentId = item1.ByDepartmentId;
                            mdl.DocumentTypeId = item1.DocumentTypeId;
                            mdl.PaperNature = item1.PaperNature;
                            mdl.LinkedRefNo = item1.LinkedRefNo;
                            mdl.SendStatus = item1.SendStatus;
                            mdl.IsVerified = item1.IsVerified;
                            mdl.RejectReason = item1.RejectReason;
                            list.Add(mdl);
                        }

                    }
                    else
                    {
                        pHouseCommitteeFiles mdl = new pHouseCommitteeFiles();
                        mdl.ParentId = item.Id;
                        mdl.Id = item.Id;
                        mdl.eFileName = item.eFileName;
                        //mdl.PaperNature = item1.PaperNature;
                        mdl.ParentId = item.ParentId;
                        mdl.CommitteeName = item.CommitteeName;
                        mdl.CommitteeId = item.CommitteeId;
                        mdl.CommitteeNameAbbrev = (from mc in db.committees where mc.CommitteeId == item.CommitteeId select mc.Abbreviation).FirstOrDefault();
                        mdl.eFilePath = item.eFilePath;

                        if (item.ReplyPdfPath != null)
                        {
                            string ReplyPdfPath = item.ReplyPdfPath;
                            ReplyPdfPath.Replace(" ", string.Empty);
                            mdl.ReplyPdfPath = ReplyPdfPath;
                        }
                        else
                        {
                            mdl.ReplyPdfPath = item.ReplyPdfPath; ;
                        }

                        if (item.AnnexPdfPath != null)
                        {
                            string AnnexPdfPath = item.AnnexPdfPath;
                            AnnexPdfPath.Replace(" ", string.Empty);
                            mdl.AnnexPdfPath = AnnexPdfPath;
                        }
                        else
                        {
                            mdl.AnnexPdfPath = item.AnnexPdfPath; ;
                        }



                        // mdl.AnnexPdfPath = item1.AnnexPdfPath;
                        if (item.AnnexFilesNameByUser != null)
                        {
                            string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                            mdl.AnnexFilesNameByUser = str;

                        }
                        else
                        {
                            mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                        }


                        if (item.DocFilesNameByUser != null)
                        {
                            string str = System.Text.RegularExpressions.Regex.Replace(item.DocFilesNameByUser, @"\s+", "");
                            mdl.DocFilesNameByUser = str;

                        }
                        else
                        {
                            mdl.DocFilesNameByUser = item.DocFilesNameByUser;
                        }


                        mdl.DocFilePath = item.DocFilePath;
                        mdl.Title = item.Title;
                        mdl.Description = item.Description;
                        //mdl.ItemName=item1.
                        mdl.ToDepartmentName = (from mc in dbn.departments where mc.deptId == item.ToDepartmentID select mc.deptname).FirstOrDefault();
                        mdl.ByDepartmentName = (from mc in dbn.departments where mc.deptId == item.ByDepartmentId select mc.deptname).FirstOrDefault();
                        mdl.CreatedDate = item.CreatedDate;
                        mdl.ItemId = item.ItemId;
                        mdl.ItemName = item.ItemName;
                        mdl.eFileID = item.eFileID;
                        mdl.PaperRefNo = item.PaperRefNo;
                        mdl.SelectedPaperDate = item.SelectedPaperDate;
                        //  mdl.SelectedPaperDate = Convert.ToDateTime(item1.CreatedDate).ToString("dd/MM/yyyy");               
                        mdl.ToDepartmentID = item.ToDepartmentID;
                        mdl.ByDepartmentId = item.ByDepartmentId;
                        mdl.DocumentTypeId = item.DocumentTypeId;
                        mdl.PaperNature = item.PaperNature;
                        mdl.LinkedRefNo = item.LinkedRefNo;
                        mdl.SendStatus = item.SendStatus;
                        mdl.IsVerified = item.IsVerified;
                        mdl.RejectReason = item.RejectReason;
                        list.Add(mdl);
                    }
                }

                listnew = list.OrderByDescending(a => a.CreatedDate).ToList();
                //model.HouseCommitteeItemPendency = listnew;
                return listnew;
            }

        }
    }
}