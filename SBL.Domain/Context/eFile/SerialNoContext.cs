﻿using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.eFile;
using System.Data.Entity;
using SBL.Service.Common;
using SBL.Domain.Migrations;
using SBL.DomainModel.Extension;



namespace SBL.Domain.Context.eFile
{

    public class SerialNoContext : DBBase<SerialNoContext>
    {
        public SerialNoContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<eFilePaperSno> eFilePaperSno { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateSerialNo": { CreateSerialNo(param.Parameter); break; }
                case "UpdateSerialNo": { return UpdateSerialNo(param.Parameter); }
                case "DeleteSerialNo": { return DeleteSerialNo(param.Parameter); }
                case "GetAllSerialNo": { return GetAllSerialNo(); }
                case "GetSerialNoById": { return GetSerialNoById(param.Parameter); }

            }
            return null;
        }
        static void CreateSerialNo(object param)
        {
            try
            {
                using (SerialNoContext db = new SerialNoContext())
                {
                    eFilePaperSno model = param as eFilePaperSno;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.eFilePaperSno.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateSerialNo(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SerialNoContext db = new SerialNoContext())
            {
                eFilePaperSno model = param as eFilePaperSno;
                model.CreatedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                db.eFilePaperSno.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllSerialNo();
        }

        static object DeleteSerialNo(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SerialNoContext db = new SerialNoContext())
            {
                eFilePaperSno parameter = param as eFilePaperSno;
                eFilePaperSno stateToRemove = db.eFilePaperSno.SingleOrDefault(a => a.id == parameter.id);
                if (stateToRemove != null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllSerialNo();
        }
        static List<eFilePaperSno> GetAllSerialNo()
        {
            SerialNoContext db = new SerialNoContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.eFilePaperSno
                         orderby a.id descending
                         where a.IsDeleted == false
                         select a).ToList();

            return query;
        }

        static eFilePaperSno GetSerialNoById(object param)
        {
            eFilePaperSno parameter = param as eFilePaperSno;
            SerialNoContext db = new SerialNoContext();
            var query = db.eFilePaperSno.SingleOrDefault(a => a.id == parameter.id);
            return query;
        }


    }

}