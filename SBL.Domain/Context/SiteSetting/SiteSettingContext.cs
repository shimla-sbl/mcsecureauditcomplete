﻿
using System;
using System.Linq;
using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using System.Data.Entity;
using SBL.Service.Common;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.PaperLaid;
using System.Collections.Generic;
using System.Data;

namespace SBL.Domain.Context.SiteSetting
{
    public class SiteSettingContext : DBBase<SiteSettingContext>
    {
        public SiteSettingContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mSession> mSessions { get; set; }
        public DbSet<SiteSettings> SiteSettings { get; set; }
        public DbSet<mAssembly> mAssemblies { get; set; }

        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {

                case "GetAllSiteSettingsList": { return GetAllSiteSettingsList(); }

                case "CreateSiteSettings": { CreateSiteSettings(param.Parameter); break; }

                case "UpdateSiteSettings": { return UpdateSiteSettings(param.Parameter); }

                case "GetSiteSettingsById": { return GetSiteSettingsById(param.Parameter); }

                case "DeleteSiteSettings": { return DeleteSiteSettings(param.Parameter); }

                case "GetSettings":
                    {
                        return GetSettings(param.Parameter);
                    }
                case "GetQASettings":
                    {
                        return GetQASettings(param.Parameter);
                    }
                case "SetSettings":
                    {
                        return SetSettings(param.Parameter);
                    }
                case "GetAllSiteSettings":
                    {
                        return GetAllSiteSettings(param.Parameter);
                    }

                case "GetNewsFileSetting":
                    {
                        return GetNewsFileSetting(param.Parameter);
                    }

                case "GetNoticeFileSetting":
                    {
                        return GetNoticeFileSetting(param.Parameter);
                    }

                case "GetGalleryFileSetting":
                    {
                        return GetGalleryFileSetting(param.Parameter);
                    }
                case "GetMediaFileSetting":
                    {
                        return GetMediaFileSetting(param.Parameter);
                    }
                case "GetSpeechFileSetting":
                    {
                        return GetSpeechFileSetting(param.Parameter);
                    }

                case "GetToursFileSetting":
                    {
                        return GetToursFileSetting(param.Parameter);
                    }

                case "GetDISFileSetting":
                    {
                        return GetDISFileSetting(param.Parameter);
                    }
                case "GetFileAcessSettingLocal":
                    {
                        return GetFileAcessSettingLocal(param.Parameter);
                    }
                case "GetFileAcessSetting":
                    {
                        return GetFileAcessSetting(param.Parameter);
                    }
                case "GetGalleryThumbFileSetting":
                    {
                        return GetGalleryThumbFileSetting(param.Parameter);
                    }
                case "GetMediaThumbFileSetting":
                    {
                        return GetMediaThumbFileSetting(param.Parameter);
                    }
                case "GetNewsThumbFileSetting":
                    {
                        return GetNewsThumbFileSetting(param.Parameter);
                    }
                //Added by Umang
                case "GetDISFileSettingLocation":
                    {
                        return GetDISFileSettingLocation(param.Parameter);
                    }
                case "GetDISFileSetting1":
                    {
                        return GetDISFileSetting1(param.Parameter);
                    }

                case "GetMemberFileSetting":
                    {
                        return GetMemberFileSetting(param.Parameter);
                    }

                case "GetMemberThumbFileSetting":
                    {
                        return GetMemberThumbFileSetting(param.Parameter);
                    }

                //sameer for BillWorks
                case "GetBillsFileSetting":
                    {
                        return GetBillsFileSetting(param.Parameter);
                    }
                case "GetprintingFileSetting":
                    {
                        return GetprintingFileSetting(param.Parameter);
                    }


                case "GetDepartmentFileSetting":
                    {
                        return GetDepartmentFileSetting(param.Parameter);
                    }
                case "GetHeadingSetting":
                    {
                        return GetHeadingSetting(param.Parameter);
                    }

                case "GetSecureFileSettingLocation":
                    {
                        return GetSecureFileSettingLocation(param.Parameter);
                    }
                case "PassFileLocalPath":
                    {
                        return PassFileLocalPath(param.Parameter);
                    }
                case "PassFileLocation":
                    {
                        return PassFileLocation(param.Parameter);
                    }
                case "GetDepartmentPrintSetting":
                    {
                        return GetDepartmentPrintSetting(param.Parameter);
                    }
                case "GetMinisterPrintSetting":
                    {
                        return GetMinisterPrintSetting(param.Parameter);
                    }
                case "FinalApprovedSMSEMAIL":
                    {
                        return FinalApprovedSMSEMAIL(param.Parameter);
                    }
                case "OnlineQButtonShow":
                    {
                        return OnlineQButtonShow(param.Parameter);
                    }

                case "GetDISUnsecureFileSetting":
                    {
                        return GetDISUnsecureFileSetting(param.Parameter);
                    }
                case "TimeBarredSettings":
                    {
                        return TimeBarredSettings(param.Parameter);
                    }
                case "GetNewsDetailsFileSetting":
                    {
                        return GetNewsDetailsFileSetting(param.Parameter);
                    }
                case "LocalFilePath":
                    {
                        return LocalFilePath(param.Parameter);
                    }
				case "GetCommitteeMemberPDFSetting":
					{
						return GetCommitteeMemberPDFSetting(param.Parameter);
					}
                case "GetSmsGatewayValue":
                    {
                        return GetSmsGatewayValue(param.Parameter);
                    }
                case "ComSendButtonDisp":
                    {
                        return ComSendButtonDisp(param.Parameter);
                    }
                case "Get_StateCode":
                    {
                        return Get_StateCode(param.Parameter);
                    }
                    //by robin
                case "IsMsgRequiredInHouseCommittee":
                    {
                        return IsMsgRequiredInHouseCommittee(param.Parameter);
                    }
                case "IsMailRequiredInHouseCommittee":
                    {
                        return IsMailRequiredInHouseCommittee(param.Parameter);
                    }
                case "IsSingleFileMovement":
                    {
                        return IsSingleFileMovement(param.Parameter);
                    }
                case "ChkGrievanceImage": 
                    {
                        return ChkGrievanceImage(param.Parameter);  
                     }
                case "PublicFileAccessingUrlPath":
                    {
                        return PublicFileAccessingUrlPath(param.Parameter);
                    }
                //By lakshay
                case "GetCurrentAssemblyId":
                    {
                        return GetCurrentAssemblyId(param.Parameter);
                    }
            }
            return null;
        }


        private static object GetDepartmentFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "DepartmentFileLocation" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object GetDepartmentPrintSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "IsDepartmentPrint" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        private static object GetMinisterPrintSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "IsMinisterPrint" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        static object DeleteSiteSettings(object param)
        {
            try
            {
                using (SiteSettingContext db = new SiteSettingContext())
                {
                    SiteSettings model = param as SiteSettings;
                    SiteSettings remove = db.SiteSettings.SingleOrDefault(s => s.SettingId == model.SettingId);
                    if (remove != null)
                    {
                        remove.IsDeleted = true;
                    }
                    // db.SiteSettings.Remove(remove);
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllSiteSettingsList();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static SiteSettings GetSiteSettingsById(object param)
        {
            try
            {
                using (SiteSettingContext db = new SiteSettingContext())
                {
                    SiteSettings model = param as SiteSettings;
                    var data = db.SiteSettings.SingleOrDefault(s => s.SettingId == model.SettingId);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateSiteSettings(object param)
        {
            try
            {
                using (SiteSettingContext db = new SiteSettingContext())
                {
                    SiteSettings model = param as SiteSettings;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.SiteSettings.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllSiteSettingsList();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static void CreateSiteSettings(object param)
        {
            try
            {
                using (SiteSettingContext db = new SiteSettingContext())
                {
                    SiteSettings model = param as SiteSettings;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.SiteSettings.Add(model);
                    db.SaveChanges();
                    db.Close();

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<SiteSettings> GetAllSiteSettingsList()
        {
            try
            {
                using (SiteSettingContext db = new SiteSettingContext())
                {
                    var data = db.SiteSettings.Where(t => t.IsDeleted == null).OrderByDescending(s => s.SettingId).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        private static object GetToursFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "ToursFileLocation" select a).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object GetFileAcessSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();//26

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object LocalFilePath(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "LocalFilePath" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object GetFileAcessSettingLocal(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "FileAccessingUrlPathLocal" select a).FirstOrDefault();//26

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        private static object GetDISFileSettingLocation(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static object PublicFileAccessingUrlPath(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "PublicFileAccessingUrlPath" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
        private static object GetSecureFileSettingLocation(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object GetDISFileSetting1(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "FileLocation2" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object GetDISFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "FileLocation" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static object ChkGrievanceImage(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "GrievanceImage" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object GetDISUnsecureFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "FileLocation3" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        private static object PassFileLocalPath(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "PassFileLocalPath" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static object PassFileLocation(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "PassFileLocation" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        private static object GetSpeechFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "SpeechsFileLocation" select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object GetGalleryFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "GalleryFileLocation" select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        private static object GetGalleryThumbFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName.Contains("GalleryThumbFileLocation") select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }




        private static object GetMemberFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "MemberFileLocation" select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        private static object GetMemberThumbFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName.Contains("MemberThumbFileLocation") select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }




        private static object GetMediaFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "MediaFileLocation" select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        private static object GetMediaThumbFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName.Contains("MediaThumbFileLocation") select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }





        private static object GetNewsThumbFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName.Contains("NewsThumbFileLocation") select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        private static object GetNewsDetailsFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName.Contains("NewsDetailsFileLocation") select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object GetNoticeFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "NoticeFileLocation" select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object GetNewsFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "NewsFileLocation" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }


        static object GetSettings(object param)
        {
            SiteSettingContext SCtx = new SiteSettingContext();
            tPaperLaidV model = param as tPaperLaidV;
            string AssemblyCode = (from mdl in SCtx.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            string SessionCode = (from mdl in SCtx.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

            model.SessionCode = Convert.ToInt32(SessionCode);
            model.AssemblyCode = Convert.ToInt32(AssemblyCode);
            return model;
        }
        static object GetQASettings(object param)
        {
            SiteSettingContext SCtx = new SiteSettingContext();
            SiteSettings model = param as SiteSettings;
            string AssemblyCode = (from mdl in SCtx.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            string SessionCode = (from mdl in SCtx.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

            model.SessionCode = Convert.ToInt32(SessionCode);
            model.AssemblyCode = Convert.ToInt32(AssemblyCode);
            return model;
        }

        static object GetAllSiteSettings(object param)
        {
            SiteSettingContext settingContext = new SiteSettingContext();
            SiteSettings model = new SiteSettings();
            model = param as SiteSettings;

            string AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            string SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();
            string DSCApplicable = (from mdl in settingContext.SiteSettings where mdl.SettingName == "IsDSCApplicable" select mdl.SettingValue).SingleOrDefault();

            model.CurrentSecretery = (from mdl in settingContext.SiteSettings where mdl.SettingName == "CurrentSecretery" select mdl.SettingValueLocal).SingleOrDefault();
            model.CurrentSpeaker = (from mdl in settingContext.SiteSettings where mdl.SettingName == "CurrentSpeaker" select mdl.SettingValueLocal).SingleOrDefault();
            model.AccessFilePath = (from mdl in settingContext.SiteSettings where mdl.SettingName == "FileLocation" select mdl.SettingValue).SingleOrDefault();
            model.SessionCode = Convert.ToInt32(SessionCode);
            model.AssemblyCode = Convert.ToInt32(AssemblyCode);
            model.DSCApplicable = DSCApplicable;
            return model;
        }

        static object SetSettings(object param)
        {
            //       SiteSettings model = param as SiteSettings;

            //       var

            //       var vals = 0;
            //       if (model.AssemblyId != null && model.AssemblyId != "")
            //       {
            //           using (var obj = new UserContext())
            //           {
            //               var Obj = obj.mSiteSettingsVS.Where(m => m.SettingName == "Assembly").FirstOrDefault();
            //               if (Obj != null)
            //               {
            //                   Obj.SettingValue = model.AssemblyId;
            //                   vals = obj.SaveChanges();
            //                   obj.Close();
            //               }
            //               else
            //               {
            //                   mSiteSettingsVS SiteSettingsVS = new mSiteSettingsVS();
            //                   SiteSettingsVS.SettingName = "Assembly";
            //                   SiteSettingsVS.SettingValue = model.AssemblyId;
            //                   SiteSettingsVS.IsActive = true;
            //                   obj.mSiteSettingsVS.Add(SiteSettingsVS);
            //                   vals = obj.SaveChanges();
            //                   obj.Close();
            //               }
            //           }
            //       }
            //       if (model.SessionId != null && model.SessionId != "")
            //       {
            //           using (var obj = new UserContext())
            //           {
            //               var Obj = obj.mSiteSettingsVS.Where(m => m.SettingName == "Session").FirstOrDefault();
            //               if (Obj != null)
            //               {
            //                   Obj.SettingValue = model.SessionId;
            //                   vals = obj.SaveChanges();
            //                   obj.Close();
            //               }
            //               else
            //               {
            //                   mSiteSettingsVS SiteSettingsVS = new mSiteSettingsVS();
            //                   SiteSettingsVS.SettingName = "Session";
            //                   SiteSettingsVS.SettingValue = model.SessionId;
            //                   SiteSettingsVS.IsActive = true;
            //                   obj.mSiteSettingsVS.Add(SiteSettingsVS);
            //                   vals = obj.SaveChanges();
            //                   obj.Close();
            //               }
            //           }
            //       }
            return "";
        }
        private static object GetBillsFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "BillsFileLocation" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object GetprintingFileSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "PrintingFilesLocation" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object FinalApprovedSMSEMAIL(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "SMSEMAILFinalApproved" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object OnlineQButtonShow(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "OnlineQuestionButtonShow" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object TimeBarredSettings(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "TimeBarred" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }



        private static object GetHeadingSetting(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "DeptMinisterInformation" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

		private static object GetCommitteeMemberPDFSetting(object p)
		{

			try
			{
				using (SiteSettingContext SCtx = new SiteSettingContext())
				{
					var data = (from a in SCtx.SiteSettings where a.SettingName == "CommitteeMemberPDF" select a).ToList();

					return data;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

        private static object GetSmsGatewayValue(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "SmsGatewayValue" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object ComSendButtonDisp(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "CommitteeSendButtonDisplay" select a).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static string Get_StateCode(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "StateCode" select a.SettingValue).FirstOrDefault();

                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object IsMsgRequiredInHouseCommittee(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "IsMsgRequiredInHouseCommittee" select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object IsMailRequiredInHouseCommittee(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "IsMailRequiredInHouseCommittee" select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private static object IsSingleFileMovement(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "IsSingleFileMovementRequired" select a).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        private static object GetCurrentAssemblyId(object p)
        {

            try
            {
                using (SiteSettingContext SCtx = new SiteSettingContext())
                {
                    var data = (from a in SCtx.SiteSettings where a.SettingName == "Assembly" select a).FirstOrDefault();//25

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
