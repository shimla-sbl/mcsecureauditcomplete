﻿
using SBL.DAL;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.User;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.Office
{
    public class OfficeContext : DBBase<OfficeContext>
    {
        public OfficeContext() : base("eVidhan") { }
        public virtual DbSet<mOffice> mOffices { get; set; }
        public virtual DbSet<mDepartment> mDepartment { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {

            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {

                case "GetAllOfficeList": { return GetAllOfficeList(); }
                case "GetOfficeListByDepartmentID": { return GetOfficeListByDepartmentID(param.Parameter); }
                case "GetOfficeNameByOfficeID": { return GetOfficeNameByOfficeID(param.Parameter); }
                case "GetDepartmentNameByDepartmentId": { return GetDepartmentNameByDepartmentId(param.Parameter); }

                case "GetOfficeNameByOfficeIdNew": { return GetOfficeNameByOfficeIdNew(param.Parameter); }
            } return null;
        }

        static List<mOffice> GetAllOfficeList()
        {
            try
            {
                using (OfficeContext db = new OfficeContext())
                {
                    var data = (from p in db.mOffices
                                orderby p.officename ascending
                                select p).ToList();
                    return data;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        static List<mOffice> GetOfficeListByDepartmentID(object param)
        {
            try
            {
                using (OfficeContext db = new OfficeContext())
                {
                    mUsers model = param as mUsers;
                    if (model.DeptId == "" || model.DeptId == null)
                    {
                        return null;
                    }
                    else
                    {
                        if (model.DeptId.Contains(','))
                        {
                            var data = (from p in db.mOffices
                                        orderby p.officename ascending
                                        where model.DeptId.Contains(p.deptid)// == model.DeptId
                                        select p).ToList();
                            return data;
                        }
                        else
                        {
                            var data = (from p in db.mOffices
                                        orderby p.officename ascending
                                        where p.deptid == model.DeptId
                                        select p).ToList();
                            return data;
                        }
                    }

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        static mOffice GetOfficeNameByOfficeID(object param)
        {
            try
            {
                mOffice model = param as mOffice;
                using (OfficeContext db = new OfficeContext())
                {
                    return db.mOffices.SingleOrDefault(a => a.OfficeId == model.OfficeId);

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        static mDepartment GetDepartmentNameByDepartmentId(object param)
        {
            try
            {
                mDepartment model = param as mDepartment;
                using (OfficeContext db = new OfficeContext())
                {
                    return db.mDepartment.SingleOrDefault(a => a.deptId == model.deptId);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        static object GetOfficeNameByOfficeIdNew(object param)
        {
            String stOfficeID = param as String;
            int intOfficeID = Convert.ToInt32(stOfficeID);
            OfficeContext deptContext = new OfficeContext();
            var query = (from a in deptContext.mOffices
                         where a.OfficeId == intOfficeID
                         select a.officename).FirstOrDefault();


            return query;
        }
 
    }
}

