﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Forms;
using SBL.DomainModel.Models.SiteSetting;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;


namespace SBL.Domain.Context.Forms
{
   
        public class FormsContext : DBBase<FormsContext>
    {



        public FormsContext() : base("eVidhan") { }

        public DbSet<tForms> tForms { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }

        public DbSet<tFormsTypeofDocuments> tFormsTypeofDocuments { get; set; }
        public DbSet<SiteSettings> SiteSettings { get; set; }
        public DbSet<tUserMannuals> tUserMannuals { get; set; }
        public DbSet<tFAQ> tFAQ { get; set; }
        public DbSet<tSiteMap> tSiteMap { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "GetAllForms": { return GetAllForms(param.Parameter); }

                    case "AddForms": { return AddForms(param.Parameter); }

                    case "UpdateForms": { return UpdateForms(param.Parameter); }

                    case "GetFormsById": { return GetFormsById(param.Parameter); }

                    case "DeleteFormsById": { return DeleteFormsById(param.Parameter); }

                    case "GetFormsTypeofDocumentLst": { return GetFormsTypeofDocumentLst(param.Parameter); }

                    case "GetAllFAQ": { return GetAllFAQ(); }

                    case "GetAllSiteMap": { return GetAllSiteMap(); }

                    case "AddFAQ": { return AddFAQ(param.Parameter); }

                    case "AddSiteMap": { return AddSiteMap(param.Parameter); }

                    case "DeleteFAQById": { DeleteFAQById(param.Parameter); break; }

                    case "GetFAQById": { return GetFAQById(param.Parameter); }

                    case "DeleteSiteMapById": { DeleteSiteMapById(param.Parameter); break; }

                    case "GetSiteMapById": { return GetSiteMapById(param.Parameter); }

                    case "UpdateFAQ": { return UpdateFAQ(param.Parameter); }

                    case "UpdateSiteMap": { return UpdateSiteMap(param.Parameter); }

                    case "GetAllHD": { return GetAllHD(); }

                    case "AddHD": { return AddHD(param.Parameter); }

                    case "GetHDById": { return GetHDById(param.Parameter); }

                    case "UpdateHD": { return UpdateHD(param.Parameter); }

                    case "DeleteHD": { DeleteHD(param.Parameter); break; }

                    case "CheckId":
                        {
                            return CheckId(param.Parameter);
                        }
                }
            }
            return null;
        }



        private static object AddForms(object p)
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {
                    tForms data = p as tForms;

                    data.ModifiedDate = DateTime.Now;
                    data.CreatedDate = DateTime.Now;
                    data.Status = true;

                    db.tForms.Add(data);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            //throw new NotImplementedException();
        }

        private static object UpdateForms(object p)
        {

            try
            {
                using (FormsContext db = new FormsContext())
                {
                    tForms updateresult = p as tForms;
                    var actualResult = (from assemblyfile in db.tForms
                                        where
                                            assemblyfile.FormId == updateresult.FormId
                                        select assemblyfile).FirstOrDefault();

                    actualResult.AssemblyId = updateresult.AssemblyId;
                    //actualResult.SessionDateId = updateresult.SessionDateId;
                    //actualResult.SessionId = updateresult.SessionId;
                    actualResult.TypeofFormId = updateresult.TypeofFormId;
                    actualResult.UploadFile = updateresult.UploadFile;
                    actualResult.Status = updateresult.Status;
                    actualResult.ModifiedDate = DateTime.Now;
                    actualResult.ModifiedBy = updateresult.ModifiedBy;
                    actualResult.Description = updateresult.Description;

                    db.tForms.Attach(actualResult);
                    db.Entry(actualResult).State = EntityState.Modified;
                    db.SaveChanges();


                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return true;
          //  throw new NotImplementedException();
        }

        private static object DeleteFormsById(object p)
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {
                    tForms id = p as tForms;

                    var result = (from assemblyfile in db.tForms
                                  where
                                      assemblyfile.FormId == id.FormId
                                  select assemblyfile).FirstOrDefault();

                    db.tForms.Remove(result);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static object GetAllForms(object p)
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {
                    var relaresult = (from asblyFiles in db.tForms
                                      join assembly in db.mAssembly on asblyFiles.AssemblyId equals assembly.AssemblyCode
                                      //join session in db.mSession on
                                      //   new { SessionCode = asblyFiles.SessionId, assemblyId = asblyFiles.AssemblyId } equals new { SessionCode = session.SessionCode, assemblyId = session.AssemblyID }
                                      //join sessiondate
                                      //    in db.mSessionDate on asblyFiles.SessionDateId equals sessiondate.Id into nullsessiondate
                                      //from sessdate in nullsessiondate.DefaultIfEmpty()
                                      join asmblydoctype in db.tFormsTypeofDocuments on asblyFiles.TypeofFormId
                                      equals asmblydoctype.TypeofFormId
                                      orderby asblyFiles.FormId descending
                                      select
                                          new
                                          {
                                              assemblyFile = asblyFiles,
                                              assemblyName = assembly.AssemblyName,
                                              //sessionName = session.SessionName,
                                              //sessiondateName = sessdate != null ? sessdate.SessionDate : default(DateTime),
                                              docTypeName = asmblydoctype.TypeofFormName
                                          }).ToList();


                    List<tForms> assemblyFileList = new List<tForms>();

                    var filecessSetings = (from a in db.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();


                    foreach (var item in relaresult)
                    {
                        tForms assemblyFile = new tForms();
                        assemblyFile = item.assemblyFile;
                        assemblyFile.FileAcessPath = filecessSetings.SettingValue;
                        //assemblyFile.UploadFile = filecessSetings.SettingValue + item.assemblyFile.UploadFile;
                        assemblyFile.AssemblyName = item.assemblyName;
                        //assemblyFile.SessionName = item.sessionName;
                        //assemblyFile.SessionDate = item.sessiondateName.ToString("dd/MM/yyyy") == "01/01/0001" ? "" : item.sessiondateName.ToString("dd/MM/yyyy");
                        assemblyFile.FormDocTypeName = item.docTypeName;
                        assemblyFileList.Add(assemblyFile);
                    }

                    return assemblyFileList;
                    //return (from assFiles in db.tAssemblyFiles select assFiles).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object GetFormsById(object p)
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {
                    tForms id = p as tForms;
                    var filecessSetings = (from a in db.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
                    var result = (from assemblyfile in db.tForms
                                  where
                                      assemblyfile.FormId == id.FormId
                                  select assemblyfile).FirstOrDefault();
                    result.FileAcessPath = filecessSetings.SettingValue;
                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static object GetFormsTypeofDocumentLst(object p)
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {
                    var result = (from assemblyfile in db.tFormsTypeofDocuments

                                  select assemblyfile).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        static object CheckId(object param)
        {

            FormsContext qCtxDB = new FormsContext();
            tForms updateSQ = param as tForms;

            var query = (from tQ in qCtxDB.tForms
                         where tQ.TypeofFormId == updateSQ.TypeofFormId
                         select tQ).ToList().Count();

            return query.ToString();
        }

        static object AddFAQ(object param)
        {

            try
            {
                tFAQ notice = param as tFAQ;
                using (FormsContext db = new FormsContext())
                {
                    notice.FAQ = notice.FAQ;
                    notice.FAA = notice.FAA;
                    //  notice.Id = notice.Id;
                    notice.IsPublished = notice.IsPublished;
                    db.tFAQ.Add(notice);

                    db.SaveChanges();

                    db.Close();


                }
                return notice;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;




        }


        static object GetAllFAQ()
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {

                    var data = (from value in db.tFAQ
                                where value.IsPublished == true
                                select value).ToList();
                    return data;
                    //var notice = db.mNotice.ToList();
                    //return notice;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            // return null;
        }

        static object GetAllSiteMap()
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {

                    var data = (from value in db.tSiteMap
                                select value).ToList();
                    return data;
                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            // return null;
        }

        static object GetFAQById(object param)
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {
                    tFAQ noticeToEdit = param as tFAQ;

                    var data = db.tFAQ.SingleOrDefault(o => o.Id == noticeToEdit.Id);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }
        static void DeleteFAQById(object param)
        {
            try
            {
                tFAQ DataTodelete = param as tFAQ;
                int id = DataTodelete.Id;
                using (FormsContext db = new FormsContext())
                {
                    var FAQToDelete = db.tFAQ.SingleOrDefault(a => a.Id == id);
                    db.tFAQ.Remove(FAQToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetSiteMapById(object param)
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {
                    tSiteMap noticeToEdit = param as tSiteMap;
                    var data = db.tSiteMap.SingleOrDefault(o => o.Id == noticeToEdit.Id);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }
        static void DeleteSiteMapById(object param)
        {
            try
            {
                tSiteMap DataTodelete = param as tSiteMap;
                int id = DataTodelete.Id;
                using (FormsContext db = new FormsContext())
                {
                    var FAQToDelete = db.tSiteMap.SingleOrDefault(a => a.Id == id);
                    db.tSiteMap.Remove(FAQToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object UpdateSiteMap(object param)
        {
            try
            {
                tSiteMap UpdateFAQ = param as tSiteMap;
                using (FormsContext db = new FormsContext())
                {
                    var data = (from a in db.tSiteMap where UpdateFAQ.Id == a.Id select a).FirstOrDefault();

                    data.Section = UpdateFAQ.Section;
                    data.Title = UpdateFAQ.Title;
                    data.URLPath = UpdateFAQ.URLPath;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateFAQ;
            }
            catch
            {
                throw;
            }
        }


        static object UpdateFAQ(object param)
        {
            try
            {
                tFAQ UpdateFAQ = param as tFAQ;
                using (FormsContext db = new FormsContext())
                {
                    var data = (from a in db.tFAQ where UpdateFAQ.Id == a.Id select a).FirstOrDefault();

                    data.FAQ = UpdateFAQ.FAQ;
                    data.FAA = UpdateFAQ.FAA;
                    data.IsPublished = UpdateFAQ.IsPublished;
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateFAQ;
            }
            catch
            {
                throw;
            }
        }



        static object GetAllHD()
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {

                    var data = (from value in db.tUserMannuals
                                select value).ToList();
                    return data;
                    // var notice = db.mNotice.ToList();
                    //return notice;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            // return null;
        }
        static object AddHD(object param)
        {

            try
            {
                tUserMannuals notice = param as tUserMannuals;
                using (FormsContext db = new FormsContext())
                {

                    notice.Title = notice.Title;
                    notice.Description = notice.Description;
                    db.tUserMannuals.Add(notice);

                    db.SaveChanges();
                    if (!string.IsNullOrEmpty(notice.FilePath))
                    {

                        notice.FilePath = notice.FilePath;
                        db.tUserMannuals.Attach(notice);
                        db.Entry(notice).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    db.Close();


                }

                return string.IsNullOrEmpty(notice.FilePath) ? "" : notice.FilePath;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;




        }
        static object GetHDById(object param)
        {
            try
            {
                using (FormsContext db = new FormsContext())
                {
                    tUserMannuals noticeToEdit = param as tUserMannuals;

                    var data = db.tUserMannuals.SingleOrDefault(o => o.MannualID == noticeToEdit.MannualID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }
        static object UpdateHD(object param)
        {
            try
            {
                tUserMannuals UpdateHD = param as tUserMannuals;
                using (FormsContext db = new FormsContext())
                {
                    var data = (from a in db.tUserMannuals where UpdateHD.MannualID == a.MannualID select a).FirstOrDefault();

                    data.Title = UpdateHD.Title;
                    data.Description = UpdateHD.Description;

                    data.FilePath = UpdateHD.FilePath;

                    //data.ModifiedBy = DateTime.Now;
                    db.tUserMannuals.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateHD;
            }
            catch
            {
                throw;
            }
        }
        static void DeleteHD(object param)
        {
            try
            {
                tUserMannuals DataTodelete = param as tUserMannuals;
                int id = DataTodelete.MannualID;
                using (FormsContext db = new FormsContext())
                {
                    var HDToDelete = db.tUserMannuals.SingleOrDefault(a => a.MannualID == id);
                    db.tUserMannuals.Remove(HDToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object AddSiteMap(object param)
        {

            try
            {
                tSiteMap notice = param as tSiteMap;
                using (FormsContext db = new FormsContext())
                {
                    notice.Section = notice.Section;
                    notice.Title = notice.Title;
                    //  notice.Id = notice.Id;
                    notice.URLPath = notice.URLPath;
                    db.tSiteMap.Add(notice);
                    db.SaveChanges();
                    db.Close();


                }
                return notice;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;




        }
    }
}
