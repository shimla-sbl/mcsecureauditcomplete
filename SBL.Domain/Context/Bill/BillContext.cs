﻿using SBL.DAL;
using SBL.DomainModel.Models.Bill;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Bill
{
    public class BillContext : DBBase<BillContext>
    {

        public BillContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mBillType> mBillType { get; set; }
        public DbSet<mBillStatus> mBillStatus { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                //For BillType
                case "CreateBill": { CreateBill(param.Parameter); break; }
                case "UpdateBill": { return UpdateBill(param.Parameter); }
                case "DeleteBill": { return DeleteBill(param.Parameter); }
                case "GetAllBill": { return GetAllBill(); }
                case "GetBillById": { return GetBillById(param.Parameter); }

                //For BillStatus
                case "CreateBillStatus": { CreateBillStatus(param.Parameter); break; }
                case "UpdateBillStatus": { return UpdateBillStatus(param.Parameter); }
                case "DeleteBillStatus": { return DeleteBillStatus(param.Parameter); }
                case "GetAllBillStatus": { return GetAllBillStatus(); }
                case "GetBillStatusById": { return GetBillStatusById(param.Parameter); }
            }
            return null;
        }
        //For BillStatus
        static List<mBillStatus> GetAllBillStatus()
        {
            BillContext db = new BillContext();
            var data = (from d in db.mBillStatus
                        orderby d.BillStatusId descending
                        where d.IsDeleted == null
                        select d).ToList();
            return data;

        }

        static void CreateBillStatus(object param)
        {
            try
            {
                using (BillContext db = new BillContext())
                {
                    mBillStatus model = param as mBillStatus;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mBillStatus.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateBillStatus(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (BillContext db = new BillContext())
            {
                mBillStatus model = param as mBillStatus;
                model.CreatedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                db.mBillStatus.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllBillStatus();
        }

        static object DeleteBillStatus(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (BillContext db = new BillContext())
            {
                mBillStatus parameter = param as mBillStatus;
                mBillStatus desigToRemove = db.mBillStatus.SingleOrDefault(a => a.BillStatusId == parameter.BillStatusId);
                if (desigToRemove != null)
                {
                    desigToRemove.IsDeleted = true;
                }
                //db.mBillStatus.Remove(desigToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllBillStatus();
        }

        static mBillStatus GetBillStatusById(object param)
        {
            mBillStatus parameter = param as mBillStatus;
            BillContext db = new BillContext();
            var query = db.mBillStatus.SingleOrDefault(a => a.BillStatusId == parameter.BillStatusId);
            return query;
        }


        //For BillType
        static void CreateBill(object param)
        {
            try
            {
                using (BillContext db = new BillContext())
                {
                    mBillType model = param as mBillType;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mBillType.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateBill(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (BillContext db = new BillContext())
            {
                mBillType model = param as mBillType;
                model.CreatedDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mBillType.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllBill();
        }

        static object DeleteBill(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (BillContext db = new BillContext())
            {
                mBillType parameter = param as mBillType;
                mBillType partyToRemove = db.mBillType.SingleOrDefault(a => a.BillTypeId == parameter.BillTypeId);
                if (partyToRemove != null)
                {
                    partyToRemove.IsDeleted = true;
                }
                //db.mBillType.Remove(partyToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllBill();
        }

        static List<mBillType> GetAllBill()
        {
            BillContext db = new BillContext();

            //var query = db.mBillType.ToList();
            var query = (from a in db.mBillType
                         orderby a.BillTypeId descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mBillType GetBillById(object param)
        {
            mBillType parameter = param as mBillType;
            BillContext db = new BillContext();
            var query = db.mBillType.SingleOrDefault(a => a.BillTypeId == parameter.BillTypeId);
            return query;
        }



    }
}
