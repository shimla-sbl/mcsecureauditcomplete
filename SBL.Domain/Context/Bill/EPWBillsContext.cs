﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.EPWSanctionBill;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.StaffManagement;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.Bill
{
    public class EPWBillsContext : DBBase<EPWBillsContext>
    {
        public EPWBillsContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<EVidhanEPWBill> EVidhanEPWBill { get; set; }
        public DbSet<mMember> mMember { get; set; }
        public DbSet<mStaff> mStaff { get; set; }
        public DbSet<mMemberDesignation> mMemberDesignations { get; set; }
        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public DbSet<EVidhanEPWBillTransaction> EVidhanEPWBillTransaction { get; set; }
        public DbSet<EvidhanMasterPhoneBill> EvidhanMasterPhoneBill { get; set; }
        public DbSet<SiteSettings> SiteSettings { get; set; }
        public DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                //For BillType
                case "getAllMLAList": { return getAllMLAList(param.Parameter); }
                case "getAllEmployeeList": { return getAllEmployeeList(param.Parameter); }
                case "UpdateBillersInfo": { return UpdateBillersInfo(param.Parameter); }
                case "SaveNewBillTransaction": { return SaveNewBillTransaction(param.Parameter); }
                case "BillHIstoryWithBillerID": { return BillHIstoryWithBillerID(param.Parameter); }
                case "GetBasicBillerINFO": { return GetBasicBillerINFO(param.Parameter); }
                case "SaveNewMasterRecordForPhoneBill": { return SaveNewMasterRecordForPhoneBill(param.Parameter); }
                case "ShowPhoneBillRecordForCPSnMinsters": { return ShowPhoneBillRecordForCPSnMinsters(param.Parameter); }
                case "ShowPhoneBillRecordForOfficeOrMembers": { return ShowPhoneBillRecordForOfficeOrMembers(param.Parameter); }
                case "UpdateBillersInfoForPhoneForSomeID": { return UpdateBillersInfoForPhoneForSomeID(param.Parameter); }
                case "GetBasicBillerINFOForOfficePhone": { return GetBasicBillerINFOForOfficePhone(param.Parameter); }
                case "GetBasicBillerINFOForMinistersPhone": { return GetBasicBillerINFOForMinistersPhone(param.Parameter); }
                case "PhoneBillHIstoryWithBillerID": { return PhoneBillHIstoryWithBillerID(param.Parameter); }
                case "GetMemberListForPhoneMaster": { return GetMemberListForPhoneMaster(param.Parameter); }
                case "EPWBillTransactionOfIdForEdit": { return EPWBillTransactionOfIdForEdit(param.Parameter); }
                case "SavingEPWBillTransactionOfIdAfterEdit": { return SavingEPWBillTransactionOfIdAfterEdit(param.Parameter); }
                case "SearchCriteriaForSanctionEBill": { return SearchCriteriaForSanctionEBill(param.Parameter); }
                case "SearchCriteriaForSanctionPBill": { return SearchCriteriaForSanctionPBill(param.Parameter); }
                case "SearchCriteriaForSanctionWBill": { return SearchCriteriaForSanctionWBill(param.Parameter); }
                case "SearchCriteriaForSanctionOfficeBill": { return SearchCriteriaForSanctionOfficeBill(param.Parameter); }
                case "SearchCriteriaForSanctionMinistersBill": { return SearchCriteriaForSanctionMinistersBill(param.Parameter); }
            }
            return null;
        }

        public static object getAllMLAList(object param)
        {
            EVidhanEPWBill parameter = param as EVidhanEPWBill;
            EPWBillsContext db = new EPWBillsContext();
            var data = (from m in db.mMember
                        join n in db.mMemberAssembly on m.MemberCode equals n.MemberID
                        //join r in db.mConstituency on n.ConstituencyCode equals r.ConstituencyCode
                        // where (m.MemberCode == n.MemberID) && (n.AssemblyID == intValue) && (r.AssemblyID == intValue) && (m.Active == true)
                        join a in db.mMemberDesignations on n.DesignationID equals a.memDesigcode into t
                        from z in t.DefaultIfEmpty()
                        orderby m.MemberCode
                        select new
                        {
                            Name = m.Name,
                            MemberCode = m.MemberCode,
                            memDesigname = z.memDesigname,
                        }).GroupBy(x => x.Name).Select(y => y.FirstOrDefault()).ToList();

            List<EVidhanEPWBill> result = new List<EVidhanEPWBill>();

            foreach (var x in data)
            {
                EVidhanEPWBill objEVidhanEPWBill = new EVidhanEPWBill();

                objEVidhanEPWBill.BillersDesignation = x.memDesigname;
                objEVidhanEPWBill.BillersID = x.MemberCode;
                objEVidhanEPWBill.BillersName = x.Name;
                result.Add(objEVidhanEPWBill);
            }
            return result.OrderBy(m => m.BillersName).ToList();
        }

        public static object getAllEmployeeList(object param)
        {
            EVidhanEPWBill parameter = param as EVidhanEPWBill;
            EPWBillsContext db = new EPWBillsContext();

            var data = (from m in db.mStaff
                        orderby m.StaffName
                        select new
                        {
                            Name = m.StaffName,
                            StaffCode = m.StaffID,
                            memDesigname = m.Designation,
                        }).ToList();

            List<EVidhanEPWBill> result = new List<EVidhanEPWBill>();

            foreach (var x in data)
            {
                EVidhanEPWBill objEVidhanEPWBill = new EVidhanEPWBill();

                objEVidhanEPWBill.BillersDesignation = x.memDesigname;
                objEVidhanEPWBill.BillersID = x.StaffCode;
                objEVidhanEPWBill.BillersName = x.Name;
                result.Add(objEVidhanEPWBill);
            }
            return result.OrderBy(m => m.BillersName).ToList();
        }

        public static object UpdateBillersInfo(object param)
        {
            EVidhanEPWBill parameter = param as EVidhanEPWBill;
            EPWBillsContext db = new EPWBillsContext();

            var data = (from m in db.EVidhanEPWBill
                        where m.BillersID == parameter.BillersID
                        && m.BillType == parameter.BillType
                        select m).FirstOrDefault();

            if (data == null)
            {
                db.EVidhanEPWBill.Add(parameter);
                db.SaveChanges();
                db.Close();
            }
            else
            {
                //data.AmountPayable = parameter.AmountPayable;
                //data.BillDate = parameter.BillDate;
                data.BillersDesignation = parameter.BillersDesignation;
                data.BillersID = parameter.BillersID;
                data.BillersName = parameter.BillersName;
                //data.BillNumber = parameter.BillNumber;
                //data.BillRangeFrom = parameter.BillRangeFrom;
                //data.BillRangeTo = parameter.BillRangeTo;
                data.BillType = parameter.BillType;
                data.DateOfPhoneInstalled = parameter.DateOfPhoneInstalled;
                data.ElectricityMeterNo = parameter.ElectricityMeterNo;
                data.IsMLA = parameter.IsMLA;
                data.PlaceWherePhoneInstalled = parameter.PlaceWherePhoneInstalled;
                //data.Remarks = parameter.Remarks;
                data.SecurityDepositOfPhone = data.SecurityDepositOfPhone;
                //data.SubmitDate = data.SubmitDate;
                data.TelephoneNo = data.TelephoneNo;
                data.TelephoneOwnOrGovt = data.TelephoneOwnOrGovt;
                //data.TelephoneRentalRangeFrom = data.TelephoneRentalRangeFrom;
                //data.TelephoneRentalRangeTo = data.TelephoneRentalRangeTo;
                //data.TotalAmount = data.TotalAmount;
                data.WaterMeterNo = data.WaterMeterNo;
                db.EVidhanEPWBill.Attach(data);
                db.Entry(data).State = EntityState.Modified;
                db.SaveChanges();
            }
            return null;
        }

        public static object SaveNewBillTransaction(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                db.EVidhanEPWBillTransaction.Add(parameter);
                db.SaveChanges();
                db.Close();
                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object GetMemberListForPhoneMaster(object param)
        {
            try
            {
                EPWBillsContext db = new EPWBillsContext();
                var objcurrentAssembly = (from f in db.SiteSettings where (f.SettingName == "Assembly") select new { f.SettingValue }).FirstOrDefault();
                var intValue = Convert.ToInt16(objcurrentAssembly.SettingValue);
                var mid = (from minister in db.mMinsitryMinister
                           join n in db.mMemberAssembly on minister.MemberCode equals n.MemberID
                           join f in db.mMemberDesignations on n.DesignationID equals f.memDesigcode
                           where (minister.AssemblyID == intValue) && (n.AssemblyID == intValue)
                           select new fillListGenricInt
                           {
                               Text = minister.MinisterName + " - " + f.memDesigname,
                               value = minister.MemberCode
                           }).ToList();
                //var data = (from m in db.mMember
                //            join n in db.mMemberAssembly on m.MemberCode equals n.MemberID
                //            join f in db.mMemberDesignations on n.DesignationID equals f.memDesigcode
                //            where (n.AssemblyID == intValue) && (m.Active == true)
                //            orderby m.MemberCode
                //            select new fillListGenricInt
                //            {
                //            Text=m.Name+" - "+f.memDesigname,
                //            value=m.MemberCode
                //            }).ToList();

                return mid;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object BillHIstoryWithBillerID(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var datalist = (from a in db.EVidhanEPWBillTransaction
                                where a.BillersID == parameter.BillersID
                                && a.BillType == parameter.BillType
                                && a.IsMLA == parameter.IsMLA
                                && a.IsMinster == false
                                select a).ToList();
                return datalist;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public static object PhoneBillHIstoryWithBillerID(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var datalist = (from a in db.EVidhanEPWBillTransaction
                                where a.BillersID == parameter.BillersID
                                && a.BillType == parameter.BillType
                                && a.IsMLA == parameter.IsMLA
                                && a.IsMinster == parameter.IsMinster
                                select a).ToList();
                return datalist;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public static object GetBasicBillerINFO(object param)
        {
            EVidhanEPWBill parameter = param as EVidhanEPWBill;
            EPWBillsContext db = new EPWBillsContext();
            var result = (from a in db.EVidhanEPWBill
                          where a.BillersID == parameter.BillersID
                          && a.BillType == parameter.BillType
                          && a.IsMLA == parameter.IsMLA
                          select a).FirstOrDefault();
            return result;
        }

        public static object BillHIstoryForMembersWithCriteria(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                int Month = parameter.FinancialMonth;
                int Year = parameter.FinancialYear;
                DateTime FirstDateOfMonth = new DateTime(Year, Month, 1);
                FirstDateOfMonth = FirstDateOfMonth.AddDays(-(FirstDateOfMonth.Day - 1));
                DateTime LastDateOfMonth = new DateTime(Year, Month, 1);
                LastDateOfMonth = LastDateOfMonth.AddMonths(1);
                LastDateOfMonth = LastDateOfMonth.AddDays(-(LastDateOfMonth.Day));

                var result = (from a in db.EVidhanEPWBillTransaction
                              where a.BillersID == parameter.BillersID
                              && a.BillType == parameter.BillType
                              && a.IsMLA == true
                              && a.IsMinster == false
                              //  && a.BillDate>=FirstDateOfMonth && a.BillDate<=LastDateOfMonth
                              select a).ToList();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SaveNewMasterRecordForPhoneBill(object param)
        {
            try
            {
                EvidhanMasterPhoneBill parameter = param as EvidhanMasterPhoneBill;
                EPWBillsContext db = new EPWBillsContext();
                db.EvidhanMasterPhoneBill.Add(parameter);
                db.SaveChanges();
                db.Close();
                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object ShowPhoneBillRecordForCPSnMinsters(object param)
        {
            try
            {
                EPWBillsContext db = new EPWBillsContext();
                var result = (from a in db.EvidhanMasterPhoneBill
                              where a.isMinister == true
                              select a).ToList();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static object ShowPhoneBillRecordForOfficeOrMembers(object param)
        {
            try
            {
                EPWBillsContext db = new EPWBillsContext();
                var result = (from a in db.EvidhanMasterPhoneBill
                              where a.isMinister == false
                              select a).ToList();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static object GetBasicBillerINFOForOfficePhone(object param)
        {
            try
            {
                EvidhanMasterPhoneBill parameter = param as EvidhanMasterPhoneBill;
                EPWBillsContext db = new EPWBillsContext();
                var result = (from a in db.EvidhanMasterPhoneBill
                              where a.isMinister == false && a.MasterPhoneBillID == parameter.MasterPhoneBillID
                              select a).SingleOrDefault();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static object GetBasicBillerINFOForMinistersPhone(object param)
        {
            try
            {
                EvidhanMasterPhoneBill parameter = param as EvidhanMasterPhoneBill;
                EPWBillsContext db = new EPWBillsContext();
                var result = (from a in db.EvidhanMasterPhoneBill
                              where a.isMinister == true && a.MasterPhoneBillID == parameter.MasterPhoneBillID
                              select a).SingleOrDefault();
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object UpdateBillersInfoForPhoneForSomeID(object param)
        {
            try
            {
                EvidhanMasterPhoneBill parameter = param as EvidhanMasterPhoneBill;
                EPWBillsContext db = new EPWBillsContext();
                var data = (from m in db.EvidhanMasterPhoneBill
                            where m.MasterPhoneBillID == parameter.MasterPhoneBillID
                            && m.isMinister == parameter.isMinister
                            select m).FirstOrDefault();
                if (data != null)
                {   
                    data.DateWhenPhoneInstalled = parameter.DateWhenPhoneInstalled;//
                    data.NameOfPersonOrDepart = parameter.NameOfPersonOrDepart;//
                    data.PlaceWherePhoneIsInstalled = parameter.PlaceWherePhoneIsInstalled;//
                    data.SecurityOfPhone = parameter.SecurityOfPhone;//
                    data.TelephoneNumber = parameter.TelephoneNumber;//
                    data.TelephoneOwnOrGovernment = parameter.TelephoneOwnOrGovernment;//
                    db.EvidhanMasterPhoneBill.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SavingEPWBillTransactionOfIdAfterEdit(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var data = (from m in db.EVidhanEPWBillTransaction
                            where m.EPWBillTransactionID == parameter.EPWBillTransactionID
                            && m.BillersID == parameter.BillersID
                            && m.BillType == parameter.BillType
                            && m.IsMLA == parameter.IsMLA
                            && m.IsMinster == parameter.IsMinster
                            select m).FirstOrDefault();
                if (data != null)
                {                    
                    data.AmountPayable = parameter.AmountPayable;//
                    data.BillDate = parameter.BillDate;//
                    if (parameter.BillFileName != null)
                    {
                        data.BillFileName = parameter.BillFileName;//
                    }
                    data.SubmitDateDB = parameter.SubmitDateDB;//
                    data.BillNumber = parameter.BillNumber;//
                    data.BillRangeFrom = parameter.BillRangeFrom;//
                    data.BillRangeTo = parameter.BillRangeTo;//
                    data.Remarks = parameter.Remarks;//
                    data.SubmitDate = parameter.SubmitDate;//
                    data.TelephoneRentalRangeFrom = parameter.TelephoneRentalRangeFrom;//
                    data.TelephoneRentalRangeTo = parameter.TelephoneRentalRangeTo;//
                    data.TotalAmount = parameter.TotalAmount;//
                    db.EVidhanEPWBillTransaction.Attach(data);                    
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object EPWBillTransactionOfIdForEdit(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var data = (from m in db.EVidhanEPWBillTransaction
                            where m.EPWBillTransactionID == parameter.EPWBillTransactionID
                            && m.BillersID == parameter.BillersID
                            && m.BillType == parameter.BillType
                            && m.IsMLA == parameter.IsMLA
                            && m.IsMinster == parameter.IsMinster
                            select m).FirstOrDefault();
                data.SubmitDate = data.SubmitDateDB.ToString("dd/MM/yyyy");
                return data;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SearchCriteriaForSanctionEBill(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var data = (from m in db.EVidhanEPWBillTransaction
                            join n in db.EVidhanEPWBill on m.BillersID equals n.BillersID
                            join k in db.mMember on m.BillersID equals k.MemberCode
                            where m.BillType == parameter.BillType
                            && n.BillType==parameter.BillType
                            && m.IsMLA == parameter.IsMLA
                            && m.IsMinster == parameter.IsMinster
                            && m.SubmitDateDB >= parameter.SanctionSubmitRangeFrom
                            && m.SubmitDateDB <=parameter.SanctionSubmitRangeTo
                            select new {
                                Name = k.Name,
                                SetNo = n.SetNo,
                                MeterNo = n.ElectricityMeterNo,
                                BillDate = m.BillDate,
                                Amount = m.AmountPayable                            
                            }).ToList();
                

                List<MLAElectricitySanction> result = new List<MLAElectricitySanction>();

                foreach (var x in data)
                {
                    MLAElectricitySanction MLAElectricitySanction = new MLAElectricitySanction();

                    MLAElectricitySanction.BillDate = x.BillDate;
                    MLAElectricitySanction.MeterNo = x.MeterNo;
                    MLAElectricitySanction.Name = x.Name;
                    MLAElectricitySanction.SetNo = x.SetNo;
                    MLAElectricitySanction.TotalAmount = x.Amount;
                    result.Add(MLAElectricitySanction);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SearchCriteriaForSanctionPBill(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var data = (from m in db.EVidhanEPWBillTransaction
                            join n in db.EVidhanEPWBill on m.BillersID equals n.BillersID
                            join k in db.mMember on m.BillersID equals k.MemberCode
                            where m.BillType == parameter.BillType
                            && n.BillType == parameter.BillType
                            && m.IsMLA == parameter.IsMLA
                            && m.IsMinster == parameter.IsMinster
                            && m.SubmitDateDB >= parameter.SanctionSubmitRangeFrom
                            && m.SubmitDateDB <= parameter.SanctionSubmitRangeTo
                            select new
                            {
                                Name = k.Name,
                                PhoneNo = n.TelephoneNo,                                
                                DateRange = m.TelephoneRentalRangeFrom + "to" +m.TelephoneRentalRangeTo,
                                Amount = m.AmountPayable
                            }).ToList();


                List<MLAPhoneSanction> result = new List<MLAPhoneSanction>();

                foreach (var x in data)
                {
                    MLAPhoneSanction MLAPhoneSanction = new MLAPhoneSanction();

                    MLAPhoneSanction.DateRange = x.DateRange;
                    MLAPhoneSanction.PhoneNo = x.PhoneNo;
                    MLAPhoneSanction.Name = x.Name;                    
                    MLAPhoneSanction.TotalAmount = x.Amount;
                    result.Add(MLAPhoneSanction);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SearchCriteriaForSanctionWBill(object param)
        {
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var data = (from m in db.EVidhanEPWBillTransaction
                            join n in db.EVidhanEPWBill on m.BillersID equals n.BillersID
                            join k in db.mMember on m.BillersID equals k.MemberCode
                            where m.BillType == parameter.BillType
                            && n.BillType == parameter.BillType
                            && m.IsMLA == parameter.IsMLA
                            && m.IsMinster == parameter.IsMinster
                            && m.SubmitDateDB >= parameter.SanctionSubmitRangeFrom
                            && m.SubmitDateDB <= parameter.SanctionSubmitRangeTo
                            select new
                            {
                                Name = k.Name,
                                SetNo = n.SetNo,
                                MeterNo = n.WaterMeterNo,
                                BillDate = m.BillDate,
                                Amount = m.AmountPayable        
                            }).ToList();


                List<MLAWaterSanction> result = new List<MLAWaterSanction>();

                foreach (var x in data)
                {
                    MLAWaterSanction MLAWaterSanction = new MLAWaterSanction();

                    MLAWaterSanction.BillDate = x.BillDate;
                    MLAWaterSanction.MeterNo = x.MeterNo;
                    MLAWaterSanction.Name = x.Name;
                    MLAWaterSanction.SetNo = x.SetNo;
                    MLAWaterSanction.TotalAmount = x.Amount;
                    result.Add(MLAWaterSanction);
                }

                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SearchCriteriaForSanctionOfficeBill(object param)
        {
            SanctionTotalAmount obj = new SanctionTotalAmount();
           // double totalAmount = 0;
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var data = (from m in db.EVidhanEPWBillTransaction
                            join n in db.EvidhanMasterPhoneBill on m.BillersID equals n.MasterPhoneBillID
                           // join k in db.mMember on m.BillersID equals k.MemberCode
                            where m.BillType == parameter.BillType                            
                            && m.IsMLA == parameter.IsMLA
                            && m.IsMinster == parameter.IsMinster
                            && m.SubmitDateDB >= parameter.SanctionSubmitRangeFrom
                            && m.SubmitDateDB <= parameter.SanctionSubmitRangeTo
                            select new
                            {
                                Amount = m.AmountPayable
                            }).ToList();



                foreach (var x in data)
                {
                    obj.TotalAmount += Convert.ToInt16(x.Amount);
                }
               // obj.TotalAmount = totalAmount;
                return obj;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SearchCriteriaForSanctionMinistersBill(object param)
        {
            SanctionTotalAmount obj = new SanctionTotalAmount();
            //double totalAmount = 0;
            try
            {
                EVidhanEPWBillTransaction parameter = param as EVidhanEPWBillTransaction;
                EPWBillsContext db = new EPWBillsContext();
                var data = (from m in db.EVidhanEPWBillTransaction
                            join n in db.EvidhanMasterPhoneBill on m.BillersID equals n.MasterPhoneBillID
                            // join k in db.mMember on m.BillersID equals k.MemberCode
                            where m.BillType == parameter.BillType
                            && m.IsMLA == parameter.IsMLA
                            && m.IsMinster == parameter.IsMinster
                            && m.SubmitDateDB >= parameter.SanctionSubmitRangeFrom
                            && m.SubmitDateDB <= parameter.SanctionSubmitRangeTo
                            select new
                            {
                                Amount = m.AmountPayable
                            }).ToList();



                foreach (var x in data)
                {
                    obj.TotalAmount += Convert.ToInt16(x.Amount);
                }

                return obj;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
