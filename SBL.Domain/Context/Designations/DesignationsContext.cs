﻿using SBL.DAL;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.PISModules;
using SBL.DomainModel.Models.PISMolues;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Designations
{
    public class DesignationsContext : DBBase<DesignationsContext>
    {
        public DesignationsContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mDesignation> mDesignation { get; set; }
        public DbSet<mPISDesignation> mPISDesignation { get; set; }
        public DbSet<mMemberDesignation> mMemDesignation { get; set; }
        public DbSet<mDocumentType> mDocumentTypes { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateDesignation": { CreateDesignation(param.Parameter); break; }
                case "UpdateDesignation": { return UpdateDesignation(param.Parameter); }
                case "DeleteDesignation": { return DeleteDesignation(param.Parameter); }
                case "GetAllDesignation": { return GetAllDesignation(); }
                case "GetDesignationById": { return GetDesignationById(param.Parameter); }
                case "GetAllDepartments": { return GetAllDepartments(); }
                case "GetDesigcode": { return GetDesigcode(); }
                case "GetDesigcodeById": { return GetDesigcodeById(param.Parameter); }
                //For Document Type
                case "CreateDocumentType": { CreateDocumentType(param.Parameter); break; }
                case "UpdateDocumentType": { return UpdateDocumentType(param.Parameter); }
                case "DeleteDocumentType": { return DeleteDocumentType(param.Parameter); }
                case "GetAllDocumentType": { return GetAllDocumentType(); }
                case "GetDocumentTypeById": { return GetDocumentTypeById(param.Parameter); }
                    //For MemberDesignation
                case "CreateMemDesignation": { CreateMemDesignation(param.Parameter); break; }
                case "UpdateMemDesignation": { return UpdateMemDesignation(param.Parameter); }
                case "DeleteMemDesignation": { return DeleteMemDesignation(param.Parameter); }
                case "GetAllMemDesignation": { return GetAllMemDesignation(); }
                case "GetMemDesignationById": { return GetMemDesignationById(param.Parameter); }
                case "GetMemDesigcode": { return GetMemDesigcode(); }
                case "GetMemDesigcodeById": { return GetMemDesigcodeById(param.Parameter); }

            }
            return null;
        }

        #region Designation
        static string GetDesigcode()
        {
            try
            {
                using (DesignationsContext db = new DesignationsContext())
                {

                    var data = db.mPISDesignation.ToList();
                    if (data.Count > 0)
                    {
                        //var data1 = data.Max(d => d.desigcode);
                        var data1 = (from m in db.mPISDesignation select m.desigcode).Max();

                        return data1;

                    }
                    else
                    {
                        var data1 = (from m in db.mPISDesignation select m.desigcode).Max();
                        return data1;
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static string GetDesigcodeById(object param)
        {
            try
            {
                using (DesignationsContext db = new DesignationsContext())
                {
                    //mDesignation model = param as mDesignation;
                    int id = Convert.ToInt32(param);
                    var data = db.mPISDesignation.SingleOrDefault(d => d.Id == id);
                    return data.desigcode;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mDepartment> GetAllDepartments()
        {
            DBManager db = new DBManager();
            var data = (from d in db.mDepartments
                        orderby d.deptname ascending
                        where d.IsDeleted == null
                        select d).ToList();
            return data;
            //var query = db.mDepartments.ToList();
            //return query;
        }

        static List<mPISDesignation> GetAllDesignation()
        {
            DesignationsContext db = new DesignationsContext();
            var data = (from Desig in db.mPISDesignation
                        join Dept in db.mDepartment on Desig.deptid equals Dept.deptId into joinDeptName
                        from deptn in joinDeptName.DefaultIfEmpty()
                        where Desig.IsDeleted == null
                        select new
                        {
                            Desig,
                            deptn.deptname,


                        });
            //var data1=
            List<mPISDesignation> list = new List<mPISDesignation>();
            foreach (var item in data)
            {
                item.Desig.GetDepartmentName = item.deptname;

                list.Add(item.Desig);

            }
            return list.OrderByDescending(c => c.Id).ToList();

        }

        static void CreateDesignation(object param)
        {
            try
            {
                using (DesignationsContext db = new DesignationsContext())
                {
                    mPISDesignation model = param as mPISDesignation;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mPISDesignation.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateDesignation(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DesignationsContext db = new DesignationsContext())
            {
                mPISDesignation model = param as mPISDesignation;
                model.CreatedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                db.mPISDesignation.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllDesignation();
        }

        static object DeleteDesignation(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DesignationsContext db = new DesignationsContext())
            {
                mPISDesignation parameter = param as mPISDesignation;
                mPISDesignation desigToRemove = db.mPISDesignation.SingleOrDefault(a => a.Id == parameter.Id);
                if (desigToRemove != null)
                {
                    desigToRemove.IsDeleted = true;
                }
                // db.mDesignation.Remove(desigToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllDesignation();
        }

        static mPISDesignation GetDesignationById(object param)
        {
            mPISDesignation parameter = param as mPISDesignation;
            DesignationsContext db = new DesignationsContext();
            var query = db.mPISDesignation.SingleOrDefault(a => a.Id == parameter.Id);
            return query;
        }
        #endregion

        #region DocumentType
        //For DocumentType
        static List<mDocumentType> GetAllDocumentType()
        {
            DesignationsContext db = new DesignationsContext();
            var data = (from d in db.mDocumentTypes
                        orderby d.DocumentTypeID descending
                        where d.IsDeleted == null
                        select d).ToList();
            return data;

        }

        static void CreateDocumentType(object param)
        {
            try
            {
                using (DesignationsContext db = new DesignationsContext())
                {
                    mDocumentType model = param as mDocumentType;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mDocumentTypes.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateDocumentType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DesignationsContext db = new DesignationsContext())
            {
                mDocumentType model = param as mDocumentType;
                model.CreatedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                db.mDocumentTypes.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllDocumentType();
        }

        static object DeleteDocumentType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DesignationsContext db = new DesignationsContext())
            {
                mDocumentType parameter = param as mDocumentType;
                mDocumentType desigToRemove = db.mDocumentTypes.SingleOrDefault(a => a.DocumentTypeID == parameter.DocumentTypeID);
                if (desigToRemove != null)
                {
                    desigToRemove.IsDeleted = true;
                }
                // db.mDocumentTypes.Remove(desigToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllDocumentType();
        }

        static mDocumentType GetDocumentTypeById(object param)
        {
            mDocumentType parameter = param as mDocumentType;
            DesignationsContext db = new DesignationsContext();
            var query = db.mDocumentTypes.SingleOrDefault(a => a.DocumentTypeID == parameter.DocumentTypeID);
            return query;
        }
        #endregion

        #region MemberDesignation
        static int GetMemDesigcode()
        {
            try
            {
                using (DesignationsContext db = new DesignationsContext())
                {

                    var data = db.mMemDesignation.ToList();
                    if (data.Count > 0)
                    {
                        var data1 = data.Max(d => d.memDesigcode);
                        return data1;

                    }
                    else
                    {
                        return 0;
                    }
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static int GetMemDesigcodeById(object param)
        {
            try
            {
                using (DesignationsContext db = new DesignationsContext())
                {
                    //mDesignation model = param as mDesignation;
                    int id = Convert.ToInt32(param);
                    var data = db.mMemDesignation.SingleOrDefault(d => d.memDesigId == id);
                    return data.memDesigcode;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mMemberDesignation> GetAllMemDesignation()
        {
            DesignationsContext db = new DesignationsContext();
            var data = (from d in db.mMemDesignation
                        orderby d.memDesigId descending
                        where d.IsDeleted == null
                        select d).ToList();
            return data;

        }

        static void CreateMemDesignation(object param)
        {
            try
            {
                using (DesignationsContext db = new DesignationsContext())
                {
                    mMemberDesignation model = param as mMemberDesignation;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mMemDesignation.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateMemDesignation(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DesignationsContext db = new DesignationsContext())
            {
                mMemberDesignation model = param as mMemberDesignation;
                model.CreatedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                db.mMemDesignation.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllMemDesignation();
        }

        static object DeleteMemDesignation(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DesignationsContext db = new DesignationsContext())
            {
                mMemberDesignation parameter = param as mMemberDesignation;
                mMemberDesignation desigToRemove = db.mMemDesignation.SingleOrDefault(a => a.memDesigId == parameter.memDesigId);
                if (desigToRemove != null)
                {
                    desigToRemove.IsDeleted = true;
                }
                // db.mDocumentTypes.Remove(desigToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllMemDesignation();
        }

        static mMemberDesignation GetMemDesignationById(object param)
        {
            mMemberDesignation parameter = param as mMemberDesignation;
            DesignationsContext db = new DesignationsContext();
            var query = db.mMemDesignation.SingleOrDefault(a => a.memDesigId == parameter.memDesigId);
            return query;
        }
        #endregion
    }
}
