﻿using SBL.DAL;
using SBL.DomainModel.Models.ItemSubscription;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.ItemSubscription
{
    class ItemSubscriptionContext : DBBase<ItemSubscriptionContext>
    {

        public ItemSubscriptionContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mItemSubscription> ItemSubscription { get; set; }
        public DbSet<mItemSubsDelivery> mItemSubsDelivery { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateItemSubscription":
                    {
                        return CreateItemSubscription(param.Parameter);
                    }
                case "UpdateItemSubscription":
                    {
                        return UpdateItemSubscription(param.Parameter);
                    }
                case "DeleteItemSubscription":
                    {
                        return DeleteItemSubscription(param.Parameter);
                    }
                case "GetAllItemSubscriptionDetails":
                    {
                        return GetAllItemSubscriptionDetails(param.Parameter);
                    }
                case "GetItemSubscriptionById":
                    {
                        return GetItemSubscriptionById(param.Parameter);
                    }
                case "SearchItemSubscription":
                    {
                        return SearchItemSubscription(param.Parameter);
                    }

                case "CreateItemSubsDelivery":
                    {
                        return CreateItemSubsDelivery(param.Parameter);
                    }
                case "UpdateItemSubsDelivery":
                    {
                        return UpdateItemSubsDelivery(param.Parameter);
                    }
                case "DeleteItemSubsDelivery":
                    {
                        return DeleteItemSubsDelivery(param.Parameter);
                    }
                case "GetAllItemSubsDeliveryDetails":
                    {
                        return GetAllItemSubsDeliveryDetails(param.Parameter);
                    }
                case "SearchItemSubsDelivery":
                    {
                        return SearchItemSubsDelivery(param.Parameter);
                    }
                case "GetItemSubsDeliveryById":
                    {
                        return GetItemSubsDeliveryById(param.Parameter);
                    }
                case "SearchItemSubsForDelivery":
                    {
                        return SearchItemSubsForDelivery(param.Parameter);
                    }
                case "GetAllNewspaperSubscriptionDetails":
                    {
                        return GetAllNewspaperSubscriptionDetails(param.Parameter);
                    }
            }

            return null;
        }

        #region Item Subscription
        public static object CreateItemSubscription(object param)
        {
            try
            {
                using (ItemSubscriptionContext db = new ItemSubscriptionContext())
                {
                    mItemSubscription model = param as mItemSubscription;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.ItemSubscription.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateItemSubscription(object param)
        {
            try
            {
                using (ItemSubscriptionContext db = new ItemSubscriptionContext())
                {
                    mItemSubscription model = param as mItemSubscription;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.ItemSubscription.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteItemSubscription(object param)
        {
            try
            {
                using (ItemSubscriptionContext db = new ItemSubscriptionContext())
                {
                    mItemSubscription parameter = param as mItemSubscription;

                    var data = (from ISD in db.mItemSubsDelivery
                                where ISD.ItemSubscriptionId == parameter.ItemSubscriptionID
                                select ISD
                                ).ToList();

                    foreach (var item in data)
                    {
                        mItemSubsDelivery ItemSubsDelivery = db.mItemSubsDelivery.SingleOrDefault(a => a.ItemSubsDeliveryID == item.ItemSubsDeliveryID);
                        db.mItemSubsDelivery.Remove(ItemSubsDelivery);
                        db.SaveChanges();
                    }

                    mItemSubscription stateToRemove = db.ItemSubscription.SingleOrDefault(a => a.ItemSubscriptionID == parameter.ItemSubscriptionID);
                    db.ItemSubscription.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllItemSubscriptionDetails(object param)
        {

            ItemSubscriptionContext db = new ItemSubscriptionContext();

            var data = (from PC in db.ItemSubscription
                        orderby PC.ItemName
                        select PC).ToList();


            return data;
        }

        public static object GetAllNewspaperSubscriptionDetails(object param)
        {

            ItemSubscriptionContext db = new ItemSubscriptionContext();

            var data = (from PC in db.ItemSubscription
                        where PC.ItemType == "Newspaper"
                        orderby PC.ItemName
                        select PC).ToList();
            return data;
        }


        public static object SearchItemSubscription(object param)
        {

            string Data = param as string;
            string[] Result = Data.Split(',');
            string NameofItem = Result[0];
            string ItemType = Result[1];
            string Language = Result[2];
            string Frequency = Result[3];
            string StartDateFrom = Result[4];
            string StartDateTo = Result[5];
            string EndDateFrom = Result[6];
            string EndDateTo = Result[7];
            DateTime SFromDate = new DateTime();
            DateTime SToDate = new DateTime();

            DateTime EFromDate = new DateTime();
            DateTime EToDate = new DateTime();


            DateTime.TryParse(StartDateFrom, out SFromDate);
            DateTime.TryParse(StartDateTo, out SToDate);

            DateTime.TryParse(EndDateFrom, out EFromDate);
            DateTime.TryParse(EndDateTo, out EToDate);

            ItemSubscriptionContext db = new ItemSubscriptionContext();

            var data = (from PM in db.ItemSubscription
                        orderby PM.ItemName
                        where PM.ItemName.Contains(NameofItem.Trim()) || PM.ItemType.Contains(ItemType.Trim())
                        || PM.Language.Contains(Language.Trim()) || PM.Frequancy.Contains(Frequency.Trim())
                        || (PM.SubsStartDate >= SFromDate && PM.SubsStartDate <= SToDate)
                        || (PM.SubsEndDate >= EFromDate && PM.SubsEndDate <= EToDate)
                        select PM).ToList();


            return data;
        }


        public static object GetItemSubscriptionById(object param)
        {
            mItemSubscription parameter = param as mItemSubscription;

            ItemSubscriptionContext db = new ItemSubscriptionContext();

            var query = db.ItemSubscription.SingleOrDefault(a => a.ItemSubscriptionID == parameter.ItemSubscriptionID);

            return query;
        }
        #endregion

        #region Delivery & placement information of Item Subsription

        public static object CreateItemSubsDelivery(object param)
        {
            try
            {
                using (ItemSubscriptionContext db = new ItemSubscriptionContext())
                {
                    mItemSubsDelivery model = param as mItemSubsDelivery;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mItemSubsDelivery.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateItemSubsDelivery(object param)
        {
            try
            {
                using (ItemSubscriptionContext db = new ItemSubscriptionContext())
                {
                    mItemSubsDelivery model = param as mItemSubsDelivery;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mItemSubsDelivery.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteItemSubsDelivery(object param)
        {
            try
            {
                using (ItemSubscriptionContext db = new ItemSubscriptionContext())
                {
                    mItemSubsDelivery parameter = param as mItemSubsDelivery;
                    mItemSubsDelivery stateToRemove = db.mItemSubsDelivery.SingleOrDefault(a => a.ItemSubsDeliveryID == parameter.ItemSubsDeliveryID);
                    db.mItemSubsDelivery.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllItemSubsDeliveryDetails(object param)
        {

            ItemSubscriptionContext db = new ItemSubscriptionContext();

            var data = (from ISD in db.mItemSubsDelivery
                        join IS in db.ItemSubscription on ISD.ItemSubscriptionId equals IS.ItemSubscriptionID
                        orderby ISD.ItemSubsDeliveryID
                        select new
                        {
                            ISD,
                            IS.ItemName,
                            IS.ItemType,
                            IS.Language,
                            IS.Quantity,
                            IS.Frequancy
                        }).ToList();

            List<mItemSubsDelivery> result = new List<mItemSubsDelivery>();
            foreach (var a in data)
            {
                mItemSubsDelivery partdata = new mItemSubsDelivery();

                partdata = a.ISD;
                partdata.ItemName = a.ItemName;
                partdata.ItemType = a.ItemType;
                partdata.Language = a.Language;
                partdata.Quantity = a.Quantity;
                partdata.Frequancy = a.Frequancy;
                result.Add(partdata);
            }

            return result;
        }


        public static object SearchItemSubsDelivery(object param)
        {

            string Data = param as string;
            string[] Result = Data.Split(',');
            string NameofItem = Result[0];
            string ItemType = Result[1];
            string Language = Result[2];

            string StartDateFrom = Result[3];
            string StartDateTo = Result[4];

            DateTime SFromDate = new DateTime();
            DateTime SToDate = new DateTime();



            DateTime.TryParse(StartDateFrom, out SFromDate);
            DateTime.TryParse(StartDateTo, out SToDate);


            ItemSubscriptionContext db = new ItemSubscriptionContext();

            var data = (from ISD in db.mItemSubsDelivery
                        join IS in db.ItemSubscription on ISD.ItemSubscriptionId equals IS.ItemSubscriptionID
                        orderby ISD.ItemSubsDeliveryID
                        where IS.ItemName.Contains(NameofItem.Trim()) || IS.ItemType.Contains(ItemType.Trim()) || IS.Language.Contains(Language.Trim())
                        || (ISD.DateOfReceiving >= SFromDate && ISD.DateOfReceiving <= SToDate)
                        select new
                        {
                            ISD,
                            IS.ItemName,
                            IS.ItemType,
                            IS.Language,
                            IS.Quantity,
                            IS.Frequancy
                        }).ToList();



            List<mItemSubsDelivery> result = new List<mItemSubsDelivery>();
            foreach (var a in data)
            {
                mItemSubsDelivery partdata = new mItemSubsDelivery();

                partdata = a.ISD;
                partdata.ItemName = a.ItemName;
                partdata.ItemType = a.ItemType;
                partdata.Language = a.Language;
                partdata.Quantity = a.Quantity;
                partdata.Frequancy = a.Frequancy;
                result.Add(partdata);
            }
            return result;
        }


        public static object GetItemSubsDeliveryById(object param)
        {
            mItemSubsDelivery parameter = param as mItemSubsDelivery;

            ItemSubscriptionContext db = new ItemSubscriptionContext();

            //  var query = db.mItemSubsDelivery.SingleOrDefault(a => a.ItemSubsDeliveryID == parameter.ItemSubsDeliveryID);

            var data = (from ISD in db.mItemSubsDelivery
                        join IS in db.ItemSubscription on ISD.ItemSubscriptionId equals IS.ItemSubscriptionID
                        orderby ISD.ItemSubsDeliveryID
                        where ISD.ItemSubsDeliveryID == parameter.ItemSubsDeliveryID
                        select new
                        {
                            ISD,
                            IS.ItemName,
                            IS.ItemType,
                            IS.Language,
                            IS.Quantity,
                            IS.Frequancy
                        }).FirstOrDefault();

            mItemSubsDelivery partdata = new mItemSubsDelivery();

            partdata = data.ISD;
            partdata.ItemName = data.ItemName;
            partdata.ItemType = data.ItemType;
            partdata.Language = data.Language;
            partdata.Quantity = data.Quantity;
            partdata.Frequancy = data.Frequancy;
            return partdata;
        }

        public static object SearchItemSubsForDelivery(object param)
        {

            string Data = param as string;
            string[] Result = Data.Split(',');
            string ItemType = Result[0];
            string Language = Result[1];


            ItemSubscriptionContext db = new ItemSubscriptionContext();

            var data = (from PM in db.ItemSubscription
                        orderby PM.ItemName
                        where PM.ItemType.Contains(ItemType.Trim()) && PM.Language.Contains(Language.Trim())
                        select PM).ToList();


            return data;
        }

        #endregion
    }
}
