﻿
using SBL.DAL;
using SBL.DomainModel.Models.LOB;
using SBL.DomainModel.Models.Ministery;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using MoreLinq;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Committee;



namespace SBL.Domain.Context.LOB
{
    class LOBContext : DBBase<LOBContext>
    {
        public LOBContext() : base("eVidhan") { }
        public virtual DbSet<mMinsitryMinister> mMinsitryMinisteries { get; set; }
        public virtual DbSet<mMinistry> mMinsitry { get; set; }

        public virtual DbSet<AdminLOB> AdminLob { get; set; }
        public virtual DbSet<DraftLOB> DraftLOB { get; set; }
        public virtual DbSet<tCommitteeDraft> tCommitteeDraft { get; set; }
        public virtual DbSet<CorrigendumLOB> CorrigendumLOB { get; set; }
        public virtual DbSet<CorrigendumDetails> CorrigendumDetails { get; set; }
        public virtual DbSet<tPaperLaidV> tPaperLaidV { get; set; }
        public virtual DbSet<mBills> mBills { get; set; }
        public virtual DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }
        public virtual DbSet<tCommitteeReport> tCommitteeReport { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {

            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "GetAllMinistry":
                    {
                        return GetAllMinistryDetails(param.Parameter);
                    }
                case "GetLOBBySessionDate":
                    {
                        return GetLOBBySessionDate(param.Parameter);
                    }
                case "GetSearchResultReporters":
                    {
                        return GetSearchResultReporters(param.Parameter);
                    }
                case "AddPageBreake":
                    {
                        return AddPageBreake(param.Parameter);
                    }
                case "RemovePageBreake":
                    {
                        return RemovePageBreake(param.Parameter);
                    }
                case "AddPageBreakeSpeaker":
                    {
                        return AddPageBreakeSpeaker(param.Parameter);
                    }
                case "RemovePageBreakeSpeaker":
                    {
                        return RemovePageBreakeSpeaker(param.Parameter);
                    }
                case "CorrigendumLOBByLOBId":
                    {
                        return CorrigendumLOBByLOBId(param.Parameter);
                    }
                case "InsertCorrigendumLOB":
                    {
                        return InsertCorrigendumLOB(param.Parameter);
                    }
                case "InsertUpdatedCorrigendumDetails":
                    {
                        return InsertUpdatedCorrigendumDetails(param.Parameter);
                    }
                case "DeleteLineAboveSrNo1Corrigendum":
                    {
                        return DeleteLineAboveSrNo1Corrigendum(param.Parameter);
                    }
                case "DeleteLineAboveSrNo2Corrigendum":
                    {
                        return DeleteLineAboveSrNo2Corrigendum(param.Parameter);
                    }
                case "DeleteLineAboveSrNo3Corrigendum":
                    {
                        return DeleteLineAboveSrNo3Corrigendum(param.Parameter);
                    }
                case "InsertLineAboveSrNo1Corrigendum":
                    {
                        return InsertLineAboveSrNo1Corrigendum(param.Parameter);
                    }
                case "InsertLineAboveSrNo2Corrigendum":
                    {
                        return InsertLineAboveSrNo2Corrigendum(param.Parameter);
                    }
                case "InsertLineAboveSrNo3Corrigendum":
                    {
                        return InsertLineAboveSrNo3Corrigendum(param.Parameter);
                    }
                case "SubmitCorrigendumLOB":
                    {
                        return SubmitCorrigendumLOB(param.Parameter);
                    }

                case "ApproveCorrigendumLOB":
                    {
                        return ApproveCorrigendumLOB(param.Parameter);
                    }

                case "UpdateSubmitCorrigendumLOBPath":
                    {
                        return UpdateSubmitCorrigendumLOBPath(param.Parameter);
                    }
                case "UpdateApproveCorrigendumLOBPath":
                    {
                        return UpdateApproveCorrigendumLOBPath(param.Parameter);
                    }
                case "ReturnCorrigendumLOB":
                    {
                        return ReturnCorrigendumLOB(param.Parameter);
                    }
                case "GetCorrigendumPendingForSubmit":
                    {
                        return GetCorrigendumPendingForSubmit(param.Parameter);
                    }
                case "GetCorrigendumSubmitted":
                    {
                        return GetCorrigendumSubmitted(param.Parameter);
                    }
                case "GetCorrigendumSubmittedApprove":
                    {
                        return GetCorrigendumSubmittedApprove(param.Parameter);
                    }
                case "GetCorrigendumPendingApproval":
                    {
                        return GetCorrigendumPendingApproval(param.Parameter);
                    }
                case "GetCorrigendumById":
                    {
                        return GetCorrigendumById(param.Parameter);
                    }

                case "GetLOBUpdatedPapersByparameters":
                    {
                        return GetLOBUpdatedPapersByparameters(param.Parameter);
                    }
                case "UpdateLOBpapers":
                    {
                        return UpdateLOBpapers(param.Parameter);
                    }
                case "GetLOBTextById":
                    {
                        return GetLOBTextById(param.Parameter);
                    }
                case "GetDraftLOBById":
                    {
                        return GetDraftLOBById(param.Parameter);
                    }
                case "GetPapersLaid":
                    {
                        return GetPapersLaid(param.Parameter);
                    }
                case "GetDraftLobPapersLaid":
                    {
                        return GetDraftLobPapersLaid(param.Parameter);
                    }
                case "UpdatePaperLaid":
                    {
                        UpdatePaperLaid(param.Parameter);
                        break;
                    }
                case "GetFilenamePapersLaid":
                    {
                        return GetFilenamePapersLaid(param.Parameter);

                    }
                case "GetDraftFilenamePapersLaid":
                    {
                        return GetDraftFilenamePapersLaid(param.Parameter);

                    }
                case "GetDraftLobLatestPapersLaid":
                    {
                        return GetDraftLobLatestPapersLaid(param.Parameter);

                    }
                case "GetDraftLobLatestPapersForTransfer":
                    {
                        return GetDraftLobLatestPapersForTransfer(param.Parameter);

                    }
                case "AddPageBreakeCOM":
                    {
                        return AddPageBreakeCOM(param.Parameter);
                    }
                case "RemovePageBreakeCOM":
                    {
                        return RemovePageBreakeCOM(param.Parameter);
                    }
                case "CorrigendumCOMByCOMId":
                    {
                        return CorrigendumCOMByCOMId(param.Parameter);
                    }
                case "GetCommitteeSubmit":
                    {
                        return GetCommitteeSubmit(param.Parameter);
                    }


            } return null;
        }
        static List<mMinisteryMinisterModel> GetAllMinistryDetails(object param)
        {
            // MinisteryContext db = new MinisteryContext();

            List<mMinisteryMinisterModel> query = new List<mMinisteryMinisterModel>();
            //var query = (from ministerModel in db.mMinsitryMinisteries
            //              join ministryModel in db.mMinsitry on ministerModel.MinistryID equals ministryModel.MinistryID
            //              select new mMinisteryMinisterModel
            //              {

            //                  MinistryName = ministryModel.MinistryName,
            //                  MinsitryMinistersID = ministerModel.MinsitryMinistersID,
            //                  MinisterName = ministryModel.MinisterName,
            //                  MinistryID = ministryModel.MinistryID,
            //                  MinisterMinistryName = ministerModel.MinisterName + ", " + ministryModel.MinistryName
            //              });


            return query.ToList();
        }

        static List<AdminLOB> GetLOBBySessionDate(object param)
        {
            LOBContext db = new LOBContext();
            AdminLOB adminLOB = (AdminLOB)param;
            var LOB = (from AdminLob in db.AdminLob
                       where AdminLob.SessionDate == adminLOB.SessionDate &&
                       (AdminLob.IsDeleted == null || AdminLob.IsDeleted == null)
                       orderby AdminLob.SrNo1, AdminLob.SrNo2, AdminLob.SrNo3
                       select AdminLob).ToList();


            return LOB;
        }
        static List<AdminLOB> GetPapersLaid(object param)
        {
            LOBContext db = new LOBContext();
            AdminLOB adminLOB = (AdminLOB)param;
            var lobs = (from msedate in db.AdminLob
                        where msedate.SessionId == adminLOB.SessionId
                        && msedate.AssemblyId == adminLOB.AssemblyId
                        && msedate.SessionDate == adminLOB.SessionDate &&
                        (msedate.IsDeleted == false || msedate.IsDeleted == null)
                        orderby msedate.SrNo1, msedate.SrNo2, msedate.SrNo3
                        select msedate).ToList();

            return lobs;
        }
        static List<DraftLOB> GetDraftLobPapersLaid(object param)
        {
            LOBContext db = new LOBContext();
            DraftLOB draft = (DraftLOB)param;
            var lobs = (from msedate in db.DraftLOB
                        where (msedate.SessionId == draft.SessionId || draft.SessionId == null || draft.SessionId == 0)
                        && (msedate.AssemblyId == draft.AssemblyId || draft.AssemblyId == null || draft.AssemblyId == 0)
                        && msedate.SessionDate == draft.SessionDate
                        && (msedate.IsDeleted == false || msedate.IsDeleted == null)
                        orderby msedate.SrNo1, msedate.SrNo2, msedate.SrNo3
                        select msedate).ToList();

            return lobs;
        }
        static List<QuestionModelCustom> GetDraftLobLatestPapersLaid(object param)
        {
            LOBContext db = new LOBContext();
            DraftLOB draft = (DraftLOB)param;
            var lobs = (from msedate in db.DraftLOB
                        where (msedate.SessionId == draft.SessionId || draft.SessionId == null || draft.SessionId == 0)
                        && (msedate.AssemblyId == draft.AssemblyId || draft.AssemblyId == null || draft.AssemblyId == 0)
                        && msedate.SessionDate == draft.SessionDate
                        && (msedate.IsDeleted == false || msedate.IsDeleted == null)
                        orderby msedate.SrNo1, msedate.SrNo2, msedate.SrNo3
                        select new QuestionModelCustom
                        {
                            ConcernedEventId = msedate.ConcernedEventId,
                            PaperLaidId = (from paperLaidVs in db.tPaperLaidV
                                           join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                           where msedate.Id == paperLaidVs.LOBRecordId
                                           && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                           select paperLaidTemp.PaperLaidTempId).FirstOrDefault(),
                            FileName = (from paperLaidVs in db.tPaperLaidV
                                        join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                        where msedate.Id == paperLaidVs.LOBRecordId
                                        && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                        select paperLaidTemp.SignedFilePath).FirstOrDefault(),
                            DocFilePath = (from paperLaidVs in db.tPaperLaidV
                                           join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                           where msedate.Id == paperLaidVs.LOBRecordId
                                           && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                           select paperLaidTemp.FilePath + paperLaidTemp.DocFileName).FirstOrDefault(),
                            IsLaid = (from paperLaidVs in db.tPaperLaidV
                                      join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                      where msedate.Id == paperLaidVs.LOBRecordId
                                      && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                      select paperLaidVs.IsLaid).FirstOrDefault(),
                            DeptSubmittedDate = (from paperLaidVs in db.tPaperLaidV
                                                 join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                                 where msedate.Id == paperLaidVs.LOBRecordId
                                                 && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                                 select paperLaidTemp.DeptSubmittedDate).FirstOrDefault(),
                            DepartmentName = (from paperLaidVs in db.tPaperLaidV
                                              join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                              where msedate.Id == paperLaidVs.LOBRecordId
                                              && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                              select paperLaidVs.DeparmentName).FirstOrDefault(),
                            Version = (from paperLaidVs in db.tPaperLaidV
                                       join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                       where msedate.Id == paperLaidVs.LOBRecordId
                                       && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                       select paperLaidTemp.Version).FirstOrDefault(),

                            Subject = msedate.TextLOB,
                            LocalLobPdf = msedate.PDFLocation,
                            LobId = msedate.Id,
                            SrNo1 = msedate.SrNo1,
                            SrNo2 = msedate.SrNo2,
                            SrNo3 = msedate.SrNo3,

                        }).ToList();

            return lobs.ToList();
        }
        static List<QuestionModelCustom> GetDraftLobLatestPapersForTransfer(object param)
        {
            LOBContext db = new LOBContext();
            DraftLOB draft = (DraftLOB)param;
            var lobs = (from msedate in db.DraftLOB

                        where (msedate.SessionId == draft.SessionId || draft.SessionId == null || draft.SessionId == 0)
                        && (msedate.AssemblyId == draft.AssemblyId || draft.AssemblyId == null || draft.AssemblyId == 0)
                        && msedate.SessionDate == draft.SessionDate
                        && (msedate.IsDeleted == false || msedate.IsDeleted == null)

                        orderby msedate.SrNo1, msedate.SrNo2, msedate.SrNo3

                        select new QuestionModelCustom
                        {
                            ConcernedEventId = msedate.ConcernedEventId,
                            // PaperLaidId = paperLaidTemp.PaperLaidTempId,
                            PaperLaidId = (from paperLaidVs in db.tPaperLaidV
                                           join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                           where msedate.Id == paperLaidVs.LOBRecordId
                                           && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                           select paperLaidTemp.PaperLaidTempId).FirstOrDefault(),
                            //FileName = paperLaidTemp.SignedFilePath,
                            FileName = (from paperLaidVs in db.tPaperLaidV
                                        join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                        where msedate.Id == paperLaidVs.LOBRecordId
                                        && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                        select paperLaidTemp.SignedFilePath).FirstOrDefault(),
                            DocFilePath = msedate.PDFLocation,
                            // IsLaid = paperLaidVs.IsLaid,
                            IsLaid = (from paperLaidVs in db.tPaperLaidV
                                      join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                      where msedate.Id == paperLaidVs.LOBRecordId
                                      && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                      select paperLaidVs.IsLaid).FirstOrDefault(),
                            Subject = msedate.TextLOB,
                            //  DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                            // DepartmentName = paperLaidVs.DeparmentName,
                            DeptSubmittedDate = (from paperLaidVs in db.tPaperLaidV
                                                 join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                                 where msedate.Id == paperLaidVs.LOBRecordId
                                                 && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                                 select paperLaidTemp.DeptSubmittedDate).FirstOrDefault(),
                            DepartmentName = (from paperLaidVs in db.tPaperLaidV
                                              join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                              where msedate.Id == paperLaidVs.LOBRecordId
                                              && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                              select paperLaidVs.DeparmentName).FirstOrDefault(),
                            Version = (from paperLaidVs in db.tPaperLaidV
                                       join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                                       where msedate.Id == paperLaidVs.LOBRecordId
                                       && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                       select paperLaidTemp.Version).FirstOrDefault(),
                            SrNo1 = msedate.SrNo1,
                            SrNo2 = msedate.SrNo2,
                            SrNo3 = msedate.SrNo3,
                            // Version = paperLaidTemp.Version


                        }).ToList();

            return lobs.ToList();
        }
        static string GetFilenamePapersLaid(object param)
        {
            LOBContext db = new LOBContext();
            AdminLOB adminLOB = (AdminLOB)param;
            return (from al in db.AdminLob where al.Id == adminLOB.Id select al.PDFLocation).FirstOrDefault();
        }
        static string GetDraftFilenamePapersLaid(object param)
        {
            LOBContext db = new LOBContext();
            DraftLOB adminLOB = (DraftLOB)param;
            return (from al in db.DraftLOB where al.Id == adminLOB.Id select al.PDFLocation).FirstOrDefault();
        }
        static object GetSearchResultReporters(object param)
        {
            LOBContext db = new LOBContext();
            AdminLOB adminLOB = (AdminLOB)param;
            //var LOB = (from AdminLob in db.AdminLob
            //           join paperLaid in db.tPaperLaidVs on AdminLob.Id equals paperLaid.LOBRecordId
            //           where
            //           (!adminLOB.AssemblyId.HasValue || AdminLob.AssemblyId == adminLOB.AssemblyId)
            //                                      && (!adminLOB.SessionId.HasValue || AdminzLob.SessionId == adminLOB.SessionId)
            //                                      && (!adminLOB.ConcernedEventId.HasValue || AdminLob.ConcernedEventId == adminLOB.ConcernedEventId)
            //                                      && (!adminLOB.SessionDate.HasValue || (AdminLob.SessionDate.Value.Year == adminLOB.SessionDate.Value.Year &&
            //                                      AdminLob.SessionDate.Value.Month == adminLOB.SessionDate.Value.Month && AdminLob.SessionDate.Value.Day == adminLOB.SessionDate.Value.Day))
            //                                      && (!adminLOB.MemberId.HasValue || paperLaid.LOBRecordId == adminLOB.MemberId)
            //           group AdminLob by AdminLob.LOBId into AdminLob1
            //           select AdminLob1).ToList();

            var LOB = (from AdminLob in db.AdminLob
                       where
                       (!adminLOB.AssemblyId.HasValue || AdminLob.AssemblyId == adminLOB.AssemblyId)
                                                  && (!adminLOB.SessionId.HasValue || AdminLob.SessionId == adminLOB.SessionId)
                                                  && (!adminLOB.ConcernedEventId.HasValue || AdminLob.ConcernedEventId == adminLOB.ConcernedEventId)
                                                  && (!adminLOB.SessionDate.HasValue || (AdminLob.SessionDate.Value.Year == adminLOB.SessionDate.Value.Year &&
                                                  AdminLob.SessionDate.Value.Month == adminLOB.SessionDate.Value.Month && AdminLob.SessionDate.Value.Day == adminLOB.SessionDate.Value.Day))
                       //group AdminLob by AdminLob.LOBId into AdminLob1
                       select AdminLob).ToList();

            List<DraftLOB> objDraftList = new List<DraftLOB>();
            if (LOB != null && LOB.Count() > 0)
            {
                DraftLOB objDraft = new DraftLOB();
                foreach (var AdminLob in LOB)
                {
                    objDraft.LOBId = AdminLob.LOBId;
                    objDraft.AssemblyId = AdminLob.AssemblyId;
                    objDraft.AssemblyName = AdminLob.AssemblyName;
                    objDraft.AssemblyNameLocal = AdminLob.AssemblyNameLocal;
                    objDraft.SessionId = AdminLob.SessionId;
                    objDraft.SessionName = AdminLob.SessionName;
                    objDraft.SessionNameLocal = AdminLob.SessionNameLocal;
                    objDraft.SessionDate = AdminLob.SessionDate;
                    objDraft.ConcernedEventId = AdminLob.ConcernedEventId;
                    objDraft.ConcernedEventName = AdminLob.ConcernedEventName;
                    objDraftList.Add(objDraft);
                }
            }

            objDraftList = objDraftList.DistinctBy(x => x.LOBId).ToList();

            var resultCount = objDraftList.Count();
            var results = objDraftList.Skip((adminLOB.PageIndex - 1) * adminLOB.PAGE_SIZE).Take(adminLOB.PAGE_SIZE).ToList();

            adminLOB.ResultCount = resultCount;
            adminLOB.ListDraftLOB = results;
            return adminLOB;
            // return null;
        }



        static object AddPageBreake(object param)
        {
            var model = (DraftLOB)param;
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = db.DraftLOB.SingleOrDefault(a => a.Id == model.Id);

                DraftToUpdate.PageBreak = true;
                db.DraftLOB.Attach(DraftToUpdate);
                db.Entry(DraftToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return true;
        }

        static object RemovePageBreake(object param)
        {
            var model = (DraftLOB)param;
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = db.DraftLOB.SingleOrDefault(a => a.Id == model.Id);

                DraftToUpdate.PageBreak = false;
                db.DraftLOB.Attach(DraftToUpdate);
                db.Entry(DraftToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return true;
        }

        static object AddPageBreakeSpeaker(object param)
        {
            var model = (AdminLOB)param;
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = db.AdminLob.SingleOrDefault(a => a.Id == model.Id);

                DraftToUpdate.IsSpeakerPageBreak = true;
                db.AdminLob.Attach(DraftToUpdate);
                db.Entry(DraftToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return true;
        }

        static object RemovePageBreakeSpeaker(object param)
        {
            var model = (AdminLOB)param;
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = db.AdminLob.SingleOrDefault(a => a.Id == model.Id);

                DraftToUpdate.IsSpeakerPageBreak = false;
                db.AdminLob.Attach(DraftToUpdate);
                db.Entry(DraftToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return true;
        }


        static object CorrigendumLOBByLOBId(object param)
        {
            LOBContext db = new LOBContext();
            DraftLOB adminLOB = (DraftLOB)param;
            var result = (from corrigendumLOB in db.CorrigendumLOB where corrigendumLOB.LOBId == adminLOB.LOBId orderby corrigendumLOB.CorrigendumId descending select corrigendumLOB).FirstOrDefault();

            return result;
        }



        static object InsertCorrigendumLOB(object param)
        {
            var model = (CorrigendumLOB)param;
            LOBContext db = new LOBContext();
            model = db.CorrigendumLOB.Add(model);
            db.SaveChanges();
            return model;
        }


        static object InsertUpdatedCorrigendumDetails(object param)
        {
            var model = (CorrigendumDetails)param;
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = db.CorrigendumDetails.FirstOrDefault(a => a.RecordId == model.RecordId && a.CorrigendumId == model.CorrigendumId);
                if (DraftToUpdate != null)
                {
                    DraftToUpdate.TextLOB = model.TextLOB;
                    DraftToUpdate.PageBreak = model.PageBreak;
                    DraftToUpdate.IsEVoting = model.IsEVoting;
                    DraftToUpdate.ConcernedEventId = model.ConcernedEventId;
                    DraftToUpdate.ConcernedEventName = model.ConcernedEventName;
                    DraftToUpdate.ConcernedEventNameLocal = model.ConcernedEventNameLocal;
                    DraftToUpdate.CommitteeId = model.CommitteeId;
                    DraftToUpdate.CommitteeTitle = model.CommitteeTitle;
                    DraftToUpdate.DeptId = model.DeptId;
                    DraftToUpdate.CommitteeRepTypId = model.CommitteeRepTypId;
                    db.CorrigendumDetails.Attach(DraftToUpdate);
                    db.Entry(DraftToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                else
                {
                    if (Convert.ToBoolean(model.IsPublished))
                    {
                        model.CorrigendumAction = "Add";
                    }
                    else
                    {
                        model.CorrigendumAction = "Update";
                    }
                    db.CorrigendumDetails.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }


            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = db.DraftLOB.FirstOrDefault(a => a.Id == model.RecordId);

                var DraftToUpdate1 = db.DraftLOB.FirstOrDefault(a => a.LOBId == DraftToUpdate.LOBId && (a.IsDeleted == false || a.IsDeleted == null));
                if (DraftToUpdate != null && DraftToUpdate != null)
                {
                    DraftToUpdate.IsCorrigendum = true;
                    DraftToUpdate.IsSubmitted = DraftToUpdate1.IsSubmitted;
                    DraftToUpdate.SubmittedDate = DraftToUpdate1.SubmittedDate;
                    DraftToUpdate.SubmittedLOBPath = DraftToUpdate1.SubmittedLOBPath;
                    DraftToUpdate.IsApproved = DraftToUpdate1.IsApproved;
                    DraftToUpdate.SubmittedTime = DraftToUpdate1.SubmittedTime;
                    //  DraftToUpdate.CommitteeId = DraftToUpdate1.CommitteeId;
                    // DraftToUpdate.CommitteeTitle = DraftToUpdate1.CommitteeTitle;
                    // DraftToUpdate.DeptId = DraftToUpdate1.DeptId;
                    // DraftToUpdate.CommitteeRepTypId = DraftToUpdate1.CommitteeRepTypId;
                    db.DraftLOB.Attach(DraftToUpdate);
                    db.Entry(DraftToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                else
                {

                }
            }

            return true;
        }


        static object DeleteLineAboveSrNo1Corrigendum(object param)
        {
            var model = (CorrigendumDetails)param;
            using (LOBContext db = new LOBContext())
            {
                var DeaftTable = db.DraftLOB.FirstOrDefault(a => a.SrNo1 == model.SrNo1 && a.SessionDate == model.SessionDate);
                var DraftToUpdate1 = db.DraftLOB.FirstOrDefault(a => a.LOBId == DeaftTable.LOBId && (a.IsDeleted == false || a.IsDeleted == null));
                if (DeaftTable != null && DraftToUpdate1 != null)
                {
                    DeaftTable.IsCorrigendum = true;
                    DeaftTable.IsSubmitted = DraftToUpdate1.IsSubmitted;
                    DeaftTable.SubmittedDate = DraftToUpdate1.SubmittedDate;
                    DeaftTable.SubmittedLOBPath = DraftToUpdate1.SubmittedLOBPath;
                    DeaftTable.IsApproved = DraftToUpdate1.IsApproved;
                    db.DraftLOB.Attach(DeaftTable);
                    db.Entry(DeaftTable).State = EntityState.Modified;


                    var DraftToUpdate = db.CorrigendumDetails.FirstOrDefault(a => a.RecordId == DeaftTable.Id && a.CorrigendumId == model.CorrigendumId);
                    if (DraftToUpdate != null)
                    {
                        DraftToUpdate.CorrigendumAction = "Delete";
                        //neew added
                        DraftToUpdate.IsApproved = false;
                        DraftToUpdate.IsSubmitted = false;
                        ///end
                        db.CorrigendumDetails.Attach(DraftToUpdate);
                        db.Entry(DraftToUpdate).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();
                    }
                    else
                    {
                        model.ActionDocumentTypePDF = DeaftTable.ActionDocumentTypePDF;
                        model.ActionDocumentTypePPT = DeaftTable.ActionDocumentTypePPT;
                        model.ActionDocumentTypeVideo = DeaftTable.ActionDocumentTypeVideo;
                        model.AssemblyId = DeaftTable.AssemblyId;
                        model.AssemblyName = DeaftTable.AssemblyName;
                        model.AssemblyNameLocal = DeaftTable.AssemblyNameLocal;
                        model.ConcernedEventId = DeaftTable.ConcernedEventId;
                        model.ConcernedEventName = DeaftTable.ConcernedEventName;
                        model.ConcernedEventNameLocal = DeaftTable.ConcernedEventNameLocal;
                        model.CommitteeId = DeaftTable.CommitteeId;
                        model.CommitteeTitle = DeaftTable.CommitteeTitle;
                        model.DeptId = DeaftTable.DeptId;
                        model.CommitteeRepTypId = DeaftTable.CommitteeRepTypId;
                        model.CreatedBy = DeaftTable.CreatedBy;
                        model.CreatedDate = DeaftTable.CreatedDate;
                        model.DraftLOBxmlLocation = DeaftTable.DraftLOBxmlLocation;
                        model.RecordId = DeaftTable.Id;
                        model.IsApproved = false;
                        model.IsEVoting = DeaftTable.IsEVoting;
                        model.IsPublished = DeaftTable.IsPublished;
                        model.IsSpeakerPageBreak = DeaftTable.IsSpeakerPageBreak;
                        model.IsSubmitted = false;
                        model.LOBId = DeaftTable.LOBId;
                        model.ModifiedBy = DeaftTable.ModifiedBy;
                        model.ModifiedDate = DeaftTable.ModifiedDate;
                        model.PageBreak = DeaftTable.PageBreak;
                        model.PDFLocation = DeaftTable.PDFLocation;
                        model.PPTLocation = DeaftTable.PPTLocation;
                        model.SessionDate = DeaftTable.SessionDate;
                        model.SessionDateLOB = DeaftTable.SessionDateLOB;
                        model.SessionDateLocal = DeaftTable.SessionDateLocal;
                        model.SessionDateSQuestion = DeaftTable.SessionDateSQuestion;
                        model.SessionDateUSQuestion = DeaftTable.SessionDateUSQuestion;
                        model.SessionId = DeaftTable.SessionId;
                        model.SessionName = DeaftTable.SessionName;
                        model.SessionNameLocal = DeaftTable.SessionNameLocal;
                        model.SessionTime = DeaftTable.SessionTime;
                        model.SessionTimeLocal = DeaftTable.SessionTimeLocal;
                        model.SrNo1 = DeaftTable.SrNo1;
                        model.SrNo2 = DeaftTable.SrNo2;
                        model.SrNo3 = DeaftTable.SrNo3;
                        model.SubmittedDate = null;
                        model.SubmittedLOBPath = null;
                        model.TextBrief = DeaftTable.TextBrief;
                        model.TextCurrent = DeaftTable.TextCurrent;
                        model.TextLOB = DeaftTable.TextLOB;
                        model.TextMinister = DeaftTable.TextMinister;
                        model.TextSpeaker = DeaftTable.TextSpeaker;
                        model.VideoLocation = DeaftTable.VideoLocation;


                        model.CorrigendumAction = "Delete";
                        db.CorrigendumDetails.Add(model);
                        db.SaveChanges();
                        db.Close();
                    }
                }
            }
            return true;
        }

        static object DeleteLineAboveSrNo2Corrigendum(object param)
        {
            var model = (CorrigendumDetails)param;
            using (LOBContext db = new LOBContext())
            {
                var DeaftTable = db.DraftLOB.FirstOrDefault(a => a.SrNo1 == model.SrNo1 && a.SrNo2 == model.SrNo2 && a.SessionDate == model.SessionDate);

                var DraftToUpdate1 = db.DraftLOB.FirstOrDefault(a => a.LOBId == DeaftTable.LOBId && (a.IsDeleted == false || a.IsDeleted == null));
                if (DeaftTable != null && DraftToUpdate1 != null)
                {
                    DeaftTable.IsCorrigendum = true;
                    DeaftTable.IsSubmitted = DraftToUpdate1.IsSubmitted;
                    DeaftTable.SubmittedDate = DraftToUpdate1.SubmittedDate;
                    DeaftTable.SubmittedLOBPath = DraftToUpdate1.SubmittedLOBPath;
                    DeaftTable.IsApproved = DraftToUpdate1.IsApproved;
                    db.DraftLOB.Attach(DeaftTable);
                    db.Entry(DeaftTable).State = EntityState.Modified;

                    var DraftToUpdate = db.CorrigendumDetails.FirstOrDefault(a => a.SrNo1 == model.SrNo1 && a.SrNo2 == model.SrNo2 && a.SessionDate == model.SessionDate);
                    if (DraftToUpdate != null)
                    {
                        DraftToUpdate.CorrigendumAction = "Delete";
                        //neew added
                        DraftToUpdate.IsApproved = false;
                        DraftToUpdate.IsSubmitted = false;
                        ///end
                        db.CorrigendumDetails.Attach(DraftToUpdate);
                        db.Entry(DraftToUpdate).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();
                    }
                    else
                    {
                        model.ActionDocumentTypePDF = DeaftTable.ActionDocumentTypePDF;
                        model.ActionDocumentTypePPT = DeaftTable.ActionDocumentTypePPT;
                        model.ActionDocumentTypeVideo = DeaftTable.ActionDocumentTypeVideo;
                        model.AssemblyId = DeaftTable.AssemblyId;
                        model.AssemblyName = DeaftTable.AssemblyName;
                        model.AssemblyNameLocal = DeaftTable.AssemblyNameLocal;
                        model.ConcernedEventId = DeaftTable.ConcernedEventId;
                        model.ConcernedEventName = DeaftTable.ConcernedEventName;
                        model.ConcernedEventNameLocal = DeaftTable.ConcernedEventNameLocal;
                        model.CommitteeId = DeaftTable.CommitteeId;
                        model.CommitteeTitle = DeaftTable.CommitteeTitle;
                        model.DeptId = DeaftTable.DeptId;
                        model.CommitteeRepTypId = DeaftTable.CommitteeRepTypId;
                        model.CreatedBy = DeaftTable.CreatedBy;
                        model.CreatedDate = DeaftTable.CreatedDate;
                        model.DraftLOBxmlLocation = DeaftTable.DraftLOBxmlLocation;
                        model.RecordId = DeaftTable.Id;
                        model.IsApproved = false;
                        model.IsEVoting = DeaftTable.IsEVoting;
                        model.IsPublished = DeaftTable.IsPublished;
                        model.IsSpeakerPageBreak = DeaftTable.IsSpeakerPageBreak;
                        model.IsSubmitted = false;
                        model.LOBId = DeaftTable.LOBId;
                        model.ModifiedBy = DeaftTable.ModifiedBy;
                        model.ModifiedDate = DeaftTable.ModifiedDate;
                        model.PageBreak = DeaftTable.PageBreak;
                        model.PDFLocation = DeaftTable.PDFLocation;
                        model.PPTLocation = DeaftTable.PPTLocation;
                        model.SessionDate = DeaftTable.SessionDate;
                        model.SessionDateLOB = DeaftTable.SessionDateLOB;
                        model.SessionDateLocal = DeaftTable.SessionDateLocal;
                        model.SessionDateSQuestion = DeaftTable.SessionDateSQuestion;
                        model.SessionDateUSQuestion = DeaftTable.SessionDateUSQuestion;
                        model.SessionId = DeaftTable.SessionId;
                        model.SessionName = DeaftTable.SessionName;
                        model.SessionNameLocal = DeaftTable.SessionNameLocal;
                        model.SessionTime = DeaftTable.SessionTime;
                        model.SessionTimeLocal = DeaftTable.SessionTimeLocal;
                        model.SrNo1 = DeaftTable.SrNo1;
                        model.SrNo2 = DeaftTable.SrNo2;
                        model.SrNo3 = DeaftTable.SrNo3;
                        model.SubmittedDate = null;
                        model.SubmittedLOBPath = null;
                        model.TextBrief = DeaftTable.TextBrief;
                        model.TextCurrent = DeaftTable.TextCurrent;
                        model.TextLOB = DeaftTable.TextLOB;
                        model.TextMinister = DeaftTable.TextMinister;
                        model.TextSpeaker = DeaftTable.TextSpeaker;
                        model.VideoLocation = DeaftTable.VideoLocation;


                        model.CorrigendumAction = "Delete";
                        db.CorrigendumDetails.Add(model);
                        db.SaveChanges();
                        db.Close();
                    }
                }
            }
            return true;
        }

        static object DeleteLineAboveSrNo3Corrigendum(object param)
        {
            var model = (CorrigendumDetails)param;
            using (LOBContext db = new LOBContext())
            {
                var DeaftTable = db.DraftLOB.FirstOrDefault(a => a.SrNo1 == model.SrNo1 && a.SrNo2 == model.SrNo2 && a.SrNo3 == model.SrNo3 && a.SessionDate == model.SessionDate);

                var DraftToUpdate1 = db.DraftLOB.FirstOrDefault(a => a.LOBId == DeaftTable.LOBId && (a.IsDeleted == false || a.IsDeleted == null));
                if (DeaftTable != null && DraftToUpdate1 != null)
                {
                    DeaftTable.IsCorrigendum = true;
                    DeaftTable.IsSubmitted = DraftToUpdate1.IsSubmitted;
                    DeaftTable.SubmittedDate = DraftToUpdate1.SubmittedDate;
                    DeaftTable.SubmittedLOBPath = DraftToUpdate1.SubmittedLOBPath;
                    DeaftTable.IsApproved = DraftToUpdate1.IsApproved;
                    db.DraftLOB.Attach(DeaftTable);
                    db.Entry(DeaftTable).State = EntityState.Modified;

                    var DraftToUpdate = db.CorrigendumDetails.FirstOrDefault(a => a.RecordId == model.RecordId && a.CorrigendumId == model.CorrigendumId);
                    if (DraftToUpdate != null)
                    {
                        DraftToUpdate.CorrigendumAction = "Delete";
                        //neew added
                        DraftToUpdate.IsApproved = false;
                        DraftToUpdate.IsSubmitted = false;
                        ///end
                        db.CorrigendumDetails.Attach(DraftToUpdate);
                        db.Entry(DraftToUpdate).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();
                    }
                    else
                    {
                        model.ActionDocumentTypePDF = DeaftTable.ActionDocumentTypePDF;
                        model.ActionDocumentTypePPT = DeaftTable.ActionDocumentTypePPT;
                        model.ActionDocumentTypeVideo = DeaftTable.ActionDocumentTypeVideo;
                        model.AssemblyId = DeaftTable.AssemblyId;
                        model.AssemblyName = DeaftTable.AssemblyName;
                        model.AssemblyNameLocal = DeaftTable.AssemblyNameLocal;
                        model.ConcernedEventId = DeaftTable.ConcernedEventId;
                        model.ConcernedEventName = DeaftTable.ConcernedEventName;
                        model.ConcernedEventNameLocal = DeaftTable.ConcernedEventNameLocal;
                        model.CommitteeId = DeaftTable.CommitteeId;
                        model.CommitteeTitle = DeaftTable.CommitteeTitle;
                        model.DeptId = DeaftTable.DeptId;
                        model.CommitteeRepTypId = DeaftTable.CommitteeRepTypId;
                        model.CreatedBy = DeaftTable.CreatedBy;
                        model.CreatedDate = DeaftTable.CreatedDate;
                        model.DraftLOBxmlLocation = DeaftTable.DraftLOBxmlLocation;
                        model.RecordId = DeaftTable.Id;
                        model.IsApproved = false;
                        model.IsEVoting = DeaftTable.IsEVoting;
                        model.IsPublished = DeaftTable.IsPublished;
                        model.IsSpeakerPageBreak = DeaftTable.IsSpeakerPageBreak;
                        model.IsSubmitted = false;
                        model.LOBId = DeaftTable.LOBId;
                        model.ModifiedBy = DeaftTable.ModifiedBy;
                        model.ModifiedDate = DeaftTable.ModifiedDate;
                        model.PageBreak = DeaftTable.PageBreak;
                        model.PDFLocation = DeaftTable.PDFLocation;
                        model.PPTLocation = DeaftTable.PPTLocation;
                        model.SessionDate = DeaftTable.SessionDate;
                        model.SessionDateLOB = DeaftTable.SessionDateLOB;
                        model.SessionDateLocal = DeaftTable.SessionDateLocal;
                        model.SessionDateSQuestion = DeaftTable.SessionDateSQuestion;
                        model.SessionDateUSQuestion = DeaftTable.SessionDateUSQuestion;
                        model.SessionId = DeaftTable.SessionId;
                        model.SessionName = DeaftTable.SessionName;
                        model.SessionNameLocal = DeaftTable.SessionNameLocal;
                        model.SessionTime = DeaftTable.SessionTime;
                        model.SessionTimeLocal = DeaftTable.SessionTimeLocal;
                        model.SrNo1 = DeaftTable.SrNo1;
                        model.SrNo2 = DeaftTable.SrNo2;
                        model.SrNo3 = DeaftTable.SrNo3;
                        model.SubmittedDate = null;
                        model.SubmittedLOBPath = null;
                        model.TextBrief = DeaftTable.TextBrief;
                        model.TextCurrent = DeaftTable.TextCurrent;
                        model.TextLOB = DeaftTable.TextLOB;
                        model.TextMinister = DeaftTable.TextMinister;
                        model.TextSpeaker = DeaftTable.TextSpeaker;
                        model.VideoLocation = DeaftTable.VideoLocation;


                        model.CorrigendumAction = "Delete";
                        db.CorrigendumDetails.Add(model);
                        db.SaveChanges();
                        db.Close();
                    }
                }
            }
            return true;
        }


        static object InsertLineAboveSrNo1Corrigendum(object param)
        {
            var model = (CorrigendumDetails)param;
            using (LOBContext db = new LOBContext())
            {
                var DeaftTable = db.DraftLOB.FirstOrDefault(a => a.SrNo1 == model.SrNo1 && a.SessionDate == model.SessionDate);

                var DraftToUpdate1 = db.DraftLOB.FirstOrDefault(a => a.LOBId == DeaftTable.LOBId && (a.IsDeleted == false || a.IsDeleted == null));
                if (DeaftTable != null && DraftToUpdate1 != null)
                {
                    DeaftTable.IsCorrigendum = true;
                    DeaftTable.IsSubmitted = DraftToUpdate1.IsSubmitted;
                    DeaftTable.SubmittedDate = DraftToUpdate1.SubmittedDate;
                    DeaftTable.SubmittedLOBPath = DraftToUpdate1.SubmittedLOBPath;
                    DeaftTable.SubmittedTime = DraftToUpdate1.SubmittedTime;
                    DeaftTable.IsApproved = DraftToUpdate1.IsApproved;
                    db.DraftLOB.Attach(DeaftTable);
                    db.Entry(DeaftTable).State = EntityState.Modified;

                    model.ActionDocumentTypePDF = DeaftTable.ActionDocumentTypePDF;
                    model.ActionDocumentTypePPT = DeaftTable.ActionDocumentTypePPT;
                    model.ActionDocumentTypeVideo = DeaftTable.ActionDocumentTypeVideo;
                    model.AssemblyId = DeaftTable.AssemblyId;
                    model.AssemblyName = DeaftTable.AssemblyName;
                    model.AssemblyNameLocal = DeaftTable.AssemblyNameLocal;
                    model.ConcernedEventId = DeaftTable.ConcernedEventId;
                    model.ConcernedEventName = DeaftTable.ConcernedEventName;
                    model.ConcernedEventNameLocal = DeaftTable.ConcernedEventNameLocal;
                    model.CommitteeId = DeaftTable.CommitteeId;
                    model.CommitteeTitle = DeaftTable.CommitteeTitle;
                    model.DeptId = DeaftTable.DeptId;
                    model.CommitteeRepTypId = DeaftTable.CommitteeRepTypId;

                    model.CreatedBy = DeaftTable.CreatedBy;
                    model.CreatedDate = DeaftTable.CreatedDate;
                    model.DraftLOBxmlLocation = DeaftTable.DraftLOBxmlLocation;
                    model.RecordId = DeaftTable.Id;
                    model.IsApproved = false;
                    model.IsEVoting = DeaftTable.IsEVoting;
                    model.IsPublished = DeaftTable.IsPublished;
                    model.IsSpeakerPageBreak = DeaftTable.IsSpeakerPageBreak;
                    model.IsSubmitted = false;
                    model.LOBId = DeaftTable.LOBId;
                    model.ModifiedBy = DeaftTable.ModifiedBy;
                    model.ModifiedDate = DeaftTable.ModifiedDate;
                    model.PageBreak = DeaftTable.PageBreak;
                    model.PDFLocation = DeaftTable.PDFLocation;
                    model.PPTLocation = DeaftTable.PPTLocation;
                    model.SessionDate = DeaftTable.SessionDate;
                    model.SessionDateLOB = DeaftTable.SessionDateLOB;
                    model.SessionDateLocal = DeaftTable.SessionDateLocal;
                    model.SessionDateSQuestion = DeaftTable.SessionDateSQuestion;
                    model.SessionDateUSQuestion = DeaftTable.SessionDateUSQuestion;
                    model.SessionId = DeaftTable.SessionId;
                    model.SessionName = DeaftTable.SessionName;
                    model.SessionNameLocal = DeaftTable.SessionNameLocal;
                    model.SessionTime = DeaftTable.SessionTime;
                    model.SessionTimeLocal = DeaftTable.SessionTimeLocal;
                    model.SrNo1 = DeaftTable.SrNo1;
                    model.SrNo2 = DeaftTable.SrNo2;
                    model.SrNo3 = DeaftTable.SrNo3;
                    model.SubmittedDate = null;
                    model.SubmittedLOBPath = null;
                    model.TextBrief = DeaftTable.TextBrief;
                    model.TextCurrent = DeaftTable.TextCurrent;
                    model.TextLOB = DeaftTable.TextLOB;
                    model.TextMinister = DeaftTable.TextMinister;
                    model.TextSpeaker = DeaftTable.TextSpeaker;
                    model.VideoLocation = DeaftTable.VideoLocation;


                    model.CorrigendumAction = "Add";
                    db.CorrigendumDetails.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            return true;
        }

        static object InsertLineAboveSrNo2Corrigendum(object param)
        {
            var model = (CorrigendumDetails)param;
            using (LOBContext db = new LOBContext())
            {
                var DeaftTable = db.DraftLOB.FirstOrDefault(a => a.SrNo1 == model.SrNo1 && a.SrNo2 == model.SrNo2 && a.SessionDate == model.SessionDate);

                var DraftToUpdate1 = db.DraftLOB.FirstOrDefault(a => a.LOBId == DeaftTable.LOBId && (a.IsDeleted == false || a.IsDeleted == null));
                if (DeaftTable != null && DraftToUpdate1 != null)
                {
                    DeaftTable.IsCorrigendum = true;
                    DeaftTable.IsSubmitted = DraftToUpdate1.IsSubmitted;
                    DeaftTable.SubmittedDate = DraftToUpdate1.SubmittedDate;
                    DeaftTable.SubmittedLOBPath = DraftToUpdate1.SubmittedLOBPath;
                    DeaftTable.SubmittedTime = DraftToUpdate1.SubmittedTime;
                    DeaftTable.IsApproved = DraftToUpdate1.IsApproved;
                    db.DraftLOB.Attach(DeaftTable);
                    db.Entry(DeaftTable).State = EntityState.Modified;

                    model.ActionDocumentTypePDF = DeaftTable.ActionDocumentTypePDF;
                    model.ActionDocumentTypePPT = DeaftTable.ActionDocumentTypePPT;
                    model.ActionDocumentTypeVideo = DeaftTable.ActionDocumentTypeVideo;
                    model.AssemblyId = DeaftTable.AssemblyId;
                    model.AssemblyName = DeaftTable.AssemblyName;
                    model.AssemblyNameLocal = DeaftTable.AssemblyNameLocal;
                    model.ConcernedEventId = DeaftTable.ConcernedEventId;
                    model.ConcernedEventName = DeaftTable.ConcernedEventName;
                    model.ConcernedEventNameLocal = DeaftTable.ConcernedEventNameLocal;
                    model.CommitteeId = DeaftTable.CommitteeId;
                    model.CommitteeTitle = DeaftTable.CommitteeTitle;
                    model.DeptId = DeaftTable.DeptId;
                    model.CommitteeRepTypId = DeaftTable.CommitteeRepTypId;

                    model.CreatedBy = DeaftTable.CreatedBy;
                    model.CreatedDate = DeaftTable.CreatedDate;
                    model.DraftLOBxmlLocation = DeaftTable.DraftLOBxmlLocation;
                    model.RecordId = DeaftTable.Id;
                    model.IsApproved = false;
                    model.IsEVoting = DeaftTable.IsEVoting;
                    model.IsPublished = DeaftTable.IsPublished;
                    model.IsSpeakerPageBreak = DeaftTable.IsSpeakerPageBreak;
                    model.IsSubmitted = false;
                    model.LOBId = DeaftTable.LOBId;
                    model.ModifiedBy = DeaftTable.ModifiedBy;
                    model.ModifiedDate = DeaftTable.ModifiedDate;
                    model.PageBreak = DeaftTable.PageBreak;
                    model.PDFLocation = DeaftTable.PDFLocation;
                    model.PPTLocation = DeaftTable.PPTLocation;
                    model.SessionDate = DeaftTable.SessionDate;
                    model.SessionDateLOB = DeaftTable.SessionDateLOB;
                    model.SessionDateLocal = DeaftTable.SessionDateLocal;
                    model.SessionDateSQuestion = DeaftTable.SessionDateSQuestion;
                    model.SessionDateUSQuestion = DeaftTable.SessionDateUSQuestion;
                    model.SessionId = DeaftTable.SessionId;
                    model.SessionName = DeaftTable.SessionName;
                    model.SessionNameLocal = DeaftTable.SessionNameLocal;
                    model.SessionTime = DeaftTable.SessionTime;
                    model.SessionTimeLocal = DeaftTable.SessionTimeLocal;
                    model.SrNo1 = DeaftTable.SrNo1;
                    model.SrNo2 = DeaftTable.SrNo2;
                    model.SrNo3 = DeaftTable.SrNo3;
                    model.SubmittedDate = null;
                    model.SubmittedLOBPath = null;
                    model.TextBrief = DeaftTable.TextBrief;
                    model.TextCurrent = DeaftTable.TextCurrent;
                    model.TextLOB = DeaftTable.TextLOB;
                    model.TextMinister = DeaftTable.TextMinister;
                    model.TextSpeaker = DeaftTable.TextSpeaker;
                    model.VideoLocation = DeaftTable.VideoLocation;


                    model.CorrigendumAction = "Add";
                    db.CorrigendumDetails.Add(model);
                    db.SaveChanges();
                    db.Close();

                }
            }
            return true;
        }

        static object InsertLineAboveSrNo3Corrigendum(object param)
        {
            var model = (CorrigendumDetails)param;
            using (LOBContext db = new LOBContext())
            {
                var DeaftTable = db.DraftLOB.FirstOrDefault(a => a.SrNo1 == model.SrNo1 && a.SrNo2 == model.SrNo2 && a.SrNo3 == model.SrNo3 && a.SessionDate == model.SessionDate);

                var DraftToUpdate1 = db.DraftLOB.FirstOrDefault(a => a.LOBId == DeaftTable.LOBId && (a.IsDeleted == false || a.IsDeleted == null));
                if (DeaftTable != null && DraftToUpdate1 != null)
                {
                    DeaftTable.IsCorrigendum = true;
                    DeaftTable.IsSubmitted = DraftToUpdate1.IsSubmitted;
                    DeaftTable.SubmittedDate = DraftToUpdate1.SubmittedDate;
                    DeaftTable.SubmittedLOBPath = DraftToUpdate1.SubmittedLOBPath;
                    DeaftTable.SubmittedTime = DraftToUpdate1.SubmittedTime;
                    DeaftTable.IsApproved = DraftToUpdate1.IsApproved;
                    db.DraftLOB.Attach(DeaftTable);
                    db.Entry(DeaftTable).State = EntityState.Modified;

                    model.ActionDocumentTypePDF = DeaftTable.ActionDocumentTypePDF;
                    model.ActionDocumentTypePPT = DeaftTable.ActionDocumentTypePPT;
                    model.ActionDocumentTypeVideo = DeaftTable.ActionDocumentTypeVideo;
                    model.AssemblyId = DeaftTable.AssemblyId;
                    model.AssemblyName = DeaftTable.AssemblyName;
                    model.AssemblyNameLocal = DeaftTable.AssemblyNameLocal;
                    model.ConcernedEventId = DeaftTable.ConcernedEventId;
                    model.ConcernedEventName = DeaftTable.ConcernedEventName;
                    model.ConcernedEventNameLocal = DeaftTable.ConcernedEventNameLocal;
                    model.CommitteeId = DeaftTable.CommitteeId;
                    model.CommitteeTitle = DeaftTable.CommitteeTitle;
                    model.DeptId = DeaftTable.DeptId;
                    model.CommitteeRepTypId = DeaftTable.CommitteeRepTypId;

                    model.CreatedBy = DeaftTable.CreatedBy;
                    model.CreatedDate = DeaftTable.CreatedDate;
                    model.DraftLOBxmlLocation = DeaftTable.DraftLOBxmlLocation;
                    model.RecordId = DeaftTable.Id;
                    model.IsApproved = false;
                    model.IsEVoting = DeaftTable.IsEVoting;
                    model.IsPublished = DeaftTable.IsPublished;
                    model.IsSpeakerPageBreak = DeaftTable.IsSpeakerPageBreak;
                    model.IsSubmitted = false;
                    model.LOBId = DeaftTable.LOBId;
                    model.ModifiedBy = DeaftTable.ModifiedBy;
                    model.ModifiedDate = DeaftTable.ModifiedDate;
                    model.PageBreak = DeaftTable.PageBreak;
                    model.PDFLocation = DeaftTable.PDFLocation;
                    model.PPTLocation = DeaftTable.PPTLocation;
                    model.SessionDate = DeaftTable.SessionDate;
                    model.SessionDateLOB = DeaftTable.SessionDateLOB;
                    model.SessionDateLocal = DeaftTable.SessionDateLocal;
                    model.SessionDateSQuestion = DeaftTable.SessionDateSQuestion;
                    model.SessionDateUSQuestion = DeaftTable.SessionDateUSQuestion;
                    model.SessionId = DeaftTable.SessionId;
                    model.SessionName = DeaftTable.SessionName;
                    model.SessionNameLocal = DeaftTable.SessionNameLocal;
                    model.SessionTime = DeaftTable.SessionTime;
                    model.SessionTimeLocal = DeaftTable.SessionTimeLocal;
                    model.SrNo1 = DeaftTable.SrNo1;
                    model.SrNo2 = DeaftTable.SrNo2;
                    model.SrNo3 = DeaftTable.SrNo3;
                    model.SubmittedDate = null;
                    model.SubmittedLOBPath = null;
                    model.TextBrief = DeaftTable.TextBrief;
                    model.TextCurrent = DeaftTable.TextCurrent;
                    model.TextLOB = DeaftTable.TextLOB;
                    model.TextMinister = DeaftTable.TextMinister;
                    model.TextSpeaker = DeaftTable.TextSpeaker;
                    model.VideoLocation = DeaftTable.VideoLocation;


                    model.CorrigendumAction = "Add";
                    db.CorrigendumDetails.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            return true;
        }



        static object SubmitCorrigendumLOB(object param)
        {
            var model = (DraftLOB)param;

            DraftLOB dre = new DraftLOB();
            dre.LOBId = model.LOBId;
            CorrigendumLOB corrigendumLOB = (CorrigendumLOB)CorrigendumLOBByLOBId(dre);

            //if (corrigendumLOB.IsApproved || corrigendumLOB.IsSubmitted)
            //{
            //    return false;
            //}
            //else
            //{
            //    using (LOBContext db = new LOBContext())
            //    {
            //        var DraftToUpdate = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.CorrigendumId == corrigendumLOB.CorrigendumId select corrigendumDetails).ToList();
            //        if (DraftToUpdate.Count > 0)
            //        {
            //            for (int i = 0; i < DraftToUpdate.Count; i++)
            //            {
            //                var record = DraftToUpdate[i];

            //                record.IsSubmitted = model.IsSubmitted;
            //                record.SubmittedDate = model.SubmittedDate;
            //                record.SubmittedTime = model.SubmittedTime;
            //                db.CorrigendumDetails.Attach(record);
            //                db.Entry(record).State = EntityState.Modified;
            //                db.SaveChanges();

            //                //var DraftTo = (from draft in db.DraftLOB
            //                //               where draft.Id == record.RecordId
            //                //                   && draft.LOBId == record.LOBId
            //                //               select draft).FirstOrDefault();
            //                //if (DraftTo != null)
            //                //{

            //                //    DraftTo.IsSubmitted = model.IsSubmitted;
            //                //    DraftTo.SubmittedDate = model.SubmittedDate;
            //                //    DraftTo.SubmittedTime = model.SubmittedTime;
            //                //    db.DraftLOB.Attach(DraftTo);
            //                //    db.Entry(DraftTo).State = EntityState.Modified;
            //                //    db.SaveChanges();

            //                //}



            //            }
            //            corrigendumLOB.IsSubmitted = true;
            //            corrigendumLOB.SubmittedDate = model.SubmittedDate;
            //            corrigendumLOB.SubmittedTime = model.SubmittedTime;
            //            db.CorrigendumLOB.Attach(corrigendumLOB);
            //            db.Entry(corrigendumLOB).State = EntityState.Modified;
            //            db.SaveChanges();
            //        }
            //        db.Close();

            //    }
            //}


            //if (corrigendumLOB.IsApproved || corrigendumLOB.IsSubmitted)
            //{
            //    return false;
            //}
            //else
            //{
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.LOBId == corrigendumLOB.LOBId select corrigendumDetails).ToList();
                if (DraftToUpdate.Count > 0)
                {
                    for (int i = 0; i < DraftToUpdate.Count; i++)
                    {
                        var record = DraftToUpdate[i];

                        record.IsSubmitted = model.IsSubmitted;
                        record.SubmittedDate = model.SubmittedDate;
                        record.SubmittedTime = model.SubmittedTime;
                        db.CorrigendumDetails.Attach(record);
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();

                        //var DraftTo = (from draft in db.DraftLOB
                        //               where draft.Id == record.RecordId
                        //                   && draft.LOBId == record.LOBId
                        //               select draft).FirstOrDefault();
                        //if (DraftTo != null)
                        //{

                        //    DraftTo.IsSubmitted = model.IsSubmitted;
                        //    DraftTo.SubmittedDate = model.SubmittedDate;
                        //    DraftTo.SubmittedTime = model.SubmittedTime;
                        //    db.DraftLOB.Attach(DraftTo);
                        //    db.Entry(DraftTo).State = EntityState.Modified;
                        //    db.SaveChanges();

                        //}



                    }
                    corrigendumLOB.IsSubmitted = true;
                    corrigendumLOB.SubmittedDate = model.SubmittedDate;
                    corrigendumLOB.SubmittedTime = model.SubmittedTime;
                    db.CorrigendumLOB.Attach(corrigendumLOB);
                    db.Entry(corrigendumLOB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                db.Close();

            }
            // }

            return true;
        }



        static object UpdateSubmitCorrigendumLOBPath(object param)
        {
            var model = (DraftLOB)param;

            DraftLOB dre = new DraftLOB();
            dre.LOBId = model.LOBId;
            CorrigendumLOB corrigendumLOB = (CorrigendumLOB)CorrigendumLOBByLOBId(dre);

            if (corrigendumLOB.IsApproved || !corrigendumLOB.IsSubmitted)
            {
                return false;
            }
            else
            {
                using (LOBContext db = new LOBContext())
                {
                    var DraftToUpdate = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.LOBId == corrigendumLOB.LOBId select corrigendumDetails).ToList();
                    if (DraftToUpdate.Count > 0)
                    {
                        for (int i = 0; i < DraftToUpdate.Count; i++)
                        {
                            var record = DraftToUpdate[i];

                            record.SubmittedLOBPath = model.SubmittedLOBPath;
                            db.CorrigendumDetails.Attach(record);
                            db.Entry(record).State = EntityState.Modified;
                            db.SaveChanges();

                            //var DraftTo = (from draft in db.DraftLOB
                            //               where draft.Id == record.RecordId
                            //                   && draft.LOBId == record.LOBId
                            //               select draft).FirstOrDefault();
                            //if (DraftTo != null)
                            //{

                            //    DraftTo.SubmittedLOBPath = model.SubmittedLOBPath;
                            //    db.DraftLOB.Attach(DraftTo);
                            //    db.Entry(DraftTo).State = EntityState.Modified;
                            //    db.SaveChanges();

                            //}

                        }

                        corrigendumLOB.SubmittedFile = model.SubmittedLOBPath;
                        db.CorrigendumLOB.Attach(corrigendumLOB);
                        db.Entry(corrigendumLOB).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    db.Close();
                }
            }

            return true;
        }


        static object ApproveCorrigendumLOB(object param)
        {
            var model = (AdminLOB)param;

            DraftLOB dre = new DraftLOB();
            dre.LOBId = model.LOBId;
            CorrigendumLOB corrigendumLOB = (CorrigendumLOB)CorrigendumLOBByLOBId(dre);

            //if (corrigendumLOB.IsApproved || !corrigendumLOB.IsSubmitted)
            //{
            //    return false;
            //}
            //else
            //{
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.LOBId == corrigendumLOB.LOBId select corrigendumDetails).ToList();
                if (DraftToUpdate.Count > 0)
                {
                    for (int i = 0; i < DraftToUpdate.Count; i++)
                    {
                        var record = DraftToUpdate[i];

                        record.IsApproved = true;
                        record.ApprovedDate = model.ApprovedDate;
                        record.ApprovedTime = model.ApprovedTime;
                        db.CorrigendumDetails.Attach(record);
                        db.Entry(record).State = EntityState.Modified;
                        db.SaveChanges();

                        var AdminTo = (from admin in db.AdminLob
                                       where admin.DraftRecordId == record.RecordId
                                           && admin.LOBId == record.LOBId
                                       select admin).FirstOrDefault();
                        if (AdminTo != null)
                        {
                            //db.AdminLob.Remove(AdminTo);
                            //db.SaveChanges();
                            var DraftTo = (from draft in db.DraftLOB
                                           where draft.Id == record.RecordId
                                               && draft.LOBId == record.LOBId
                                           select draft).FirstOrDefault();
                            if (DraftTo != null)
                            {

                                AdminTo.ActionDocumentTypePDF = DraftTo.ActionDocumentTypePDF;
                                AdminTo.ActionDocumentTypePPT = DraftTo.ActionDocumentTypePPT;
                                AdminTo.ActionDocumentTypeVideo = DraftTo.ActionDocumentTypeVideo;
                                AdminTo.AssemblyId = DraftTo.AssemblyId;
                                AdminTo.AssemblyName = DraftTo.AssemblyName;
                                AdminTo.AssemblyNameLocal = DraftTo.AssemblyNameLocal;
                                AdminTo.ConcernedEventId = DraftTo.ConcernedEventId;
                                AdminTo.ConcernedEventName = DraftTo.ConcernedEventName;
                                AdminTo.ConcernedEventNameLocal = DraftTo.ConcernedEventNameLocal;
                                AdminTo.CommitteeId = DraftTo.CommitteeId;
                                AdminTo.CommitteeTitle = DraftTo.CommitteeTitle;
                                AdminTo.DeptId = DraftTo.DeptId;
                                AdminTo.CommitteeRepTypId = DraftTo.CommitteeRepTypId;

                                AdminTo.CreatedBy = DraftTo.CreatedBy;
                                AdminTo.CreatedDate = DraftTo.CreatedDate;
                                AdminTo.DraftRecordId = DraftTo.Id;
                                AdminTo.IsEVoting = Convert.ToBoolean(DraftTo.IsEVoting);
                                AdminTo.IsPublished = DraftTo.IsPublished;
                                AdminTo.IsSpeakerPageBreak = DraftTo.IsSpeakerPageBreak;
                                AdminTo.LOBId = DraftTo.LOBId;
                                //AdminTo.ModifiedBy = DraftTo.ModifiedBy;
                                //AdminTo.ModifiedDate = DraftTo.ModifiedDate;
                                AdminTo.PageBreak = DraftTo.PageBreak;
                                AdminTo.PDFLocation = DraftTo.PDFLocation;
                                AdminTo.PPTLocation = DraftTo.PPTLocation;
                                AdminTo.SessionDate = DraftTo.SessionDate;
                                AdminTo.SessionDateLocal = DraftTo.SessionDateLocal;
                                AdminTo.SessionId = DraftTo.SessionId;
                                AdminTo.SessionName = DraftTo.SessionName;
                                AdminTo.SessionNameLocal = DraftTo.SessionNameLocal;
                                AdminTo.SessionTime = DraftTo.SessionTime;
                                AdminTo.SessionTimeLocal = DraftTo.SessionTimeLocal;
                                AdminTo.SrNo1 = DraftTo.SrNo1;
                                AdminTo.SrNo2 = DraftTo.SrNo2;
                                AdminTo.SrNo3 = DraftTo.SrNo3;
                                AdminTo.TextBrief = DraftTo.TextBrief;
                                AdminTo.TextCurrent = DraftTo.TextCurrent;
                                AdminTo.TextLOB = DraftTo.TextLOB;
                                AdminTo.TextMinister = DraftTo.TextMinister;
                                AdminTo.TextSpeaker = DraftTo.TextSpeaker;
                                AdminTo.VideoLocation = DraftTo.VideoLocation;
                                AdminTo.IsDeleted = DraftTo.IsDeleted;
                                AdminTo.IsCorrigendum = DraftTo.IsCorrigendum;
                                AdminTo.SubmittedTime = DraftTo.SubmittedTime;
                                AdminTo.BillNo = DraftTo.BillNo;
                                //AdminTo.ApprovedBy = DraftTo.ApprovedBy;
                                //AdminTo.ApprovedDate = DraftTo.ApprovedDate;
                                //AdminTo.ApprovedTime = DraftTo.ApprovedTime;


                                db.AdminLob.Attach(AdminTo);
                                db.Entry(AdminTo).State = EntityState.Modified;
                                db.SaveChanges();

                                DraftTo.IsApproved = true;
                                db.DraftLOB.Attach(DraftTo);
                                db.Entry(DraftTo).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                        }
                        else
                        {
                            var AdminTo1 = (from admin in db.AdminLob
                                            where admin.LOBId == record.LOBId && (admin.IsDeleted == false || admin.IsDeleted == null)
                                            select admin).FirstOrDefault();

                            var DraftTo = (from draft in db.DraftLOB
                                           where draft.Id == record.RecordId
                                               && draft.LOBId == record.LOBId
                                           select draft).FirstOrDefault();
                            if (DraftTo != null)
                            {
                                AdminLOB adminLOB = new AdminLOB();

                                adminLOB.ActionDocumentTypePDF = DraftTo.ActionDocumentTypePDF;
                                adminLOB.ActionDocumentTypePPT = DraftTo.ActionDocumentTypePPT;
                                adminLOB.ActionDocumentTypeVideo = DraftTo.ActionDocumentTypeVideo;
                                adminLOB.AssemblyId = DraftTo.AssemblyId;
                                adminLOB.AssemblyName = DraftTo.AssemblyName;
                                adminLOB.AssemblyNameLocal = DraftTo.AssemblyNameLocal;
                                adminLOB.ConcernedEventId = DraftTo.ConcernedEventId;
                                adminLOB.ConcernedEventName = DraftTo.ConcernedEventName;
                                adminLOB.ConcernedEventNameLocal = DraftTo.ConcernedEventNameLocal;
                                adminLOB.CommitteeId = DraftTo.CommitteeId;
                                adminLOB.CommitteeTitle = DraftTo.CommitteeTitle;
                                adminLOB.DeptId = DraftTo.DeptId;
                                adminLOB.CommitteeRepTypId = DraftTo.CommitteeRepTypId;
                                adminLOB.CreatedBy = DraftTo.CreatedBy;
                                adminLOB.CreatedDate = DraftTo.CreatedDate;
                                adminLOB.DraftRecordId = DraftTo.Id;
                                adminLOB.IsEVoting = Convert.ToBoolean(DraftTo.IsEVoting);
                                adminLOB.IsPublished = DraftTo.IsPublished;
                                adminLOB.IsSpeakerPageBreak = DraftTo.IsSpeakerPageBreak;
                                adminLOB.LOBId = DraftTo.LOBId;
                                adminLOB.ModifiedBy = DraftTo.ModifiedBy;
                                adminLOB.ModifiedDate = DraftTo.ModifiedDate;
                                adminLOB.PageBreak = DraftTo.PageBreak;
                                adminLOB.PDFLocation = DraftTo.PDFLocation;
                                adminLOB.PPTLocation = DraftTo.PPTLocation;
                                adminLOB.SessionDate = DraftTo.SessionDate;
                                adminLOB.SessionDateLocal = DraftTo.SessionDateLocal;
                                adminLOB.SessionId = DraftTo.SessionId;
                                adminLOB.SessionName = DraftTo.SessionName;
                                adminLOB.SessionNameLocal = DraftTo.SessionNameLocal;
                                adminLOB.SessionTime = DraftTo.SessionTime;
                                adminLOB.SessionTimeLocal = DraftTo.SessionTimeLocal;
                                adminLOB.SrNo1 = DraftTo.SrNo1;
                                adminLOB.SrNo2 = DraftTo.SrNo2;
                                adminLOB.SrNo3 = DraftTo.SrNo3;
                                adminLOB.TextBrief = DraftTo.TextBrief;
                                adminLOB.TextCurrent = DraftTo.TextCurrent;
                                adminLOB.TextLOB = DraftTo.TextLOB;
                                adminLOB.TextMinister = DraftTo.TextMinister;
                                adminLOB.TextSpeaker = DraftTo.TextSpeaker;
                                adminLOB.VideoLocation = DraftTo.VideoLocation;
                                adminLOB.IsDeleted = DraftTo.IsDeleted;
                                adminLOB.IsCorrigendum = DraftTo.IsCorrigendum;
                                adminLOB.BillNo = DraftTo.BillNo;
                                if (AdminTo1 != null)
                                {
                                    adminLOB.ApprovedBy = AdminTo1.ApprovedBy;
                                    adminLOB.ApprovedDate = AdminTo1.ApprovedDate;
                                    adminLOB.ApprovedTime = AdminTo1.ApprovedTime;
                                    adminLOB.SubmittedTime = AdminTo1.SubmittedTime;
                                    adminLOB.LOBPath = AdminTo1.LOBPath;
                                }
                                else
                                {
                                    adminLOB.ApprovedBy = model.ApprovedBy;
                                    adminLOB.ApprovedDate = model.ApprovedDate;
                                    adminLOB.ApprovedTime = model.ApprovedTime;
                                    adminLOB.SubmittedTime = DraftTo.SubmittedTime;
                                }
                                db.AdminLob.Add(adminLOB);
                                db.SaveChanges();

                                DraftTo.IsApproved = true;
                                db.DraftLOB.Attach(DraftTo);
                                db.Entry(DraftTo).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                        }

                    }
                    corrigendumLOB.IsApproved = true;
                    corrigendumLOB.ApprovedDate = model.ApprovedDate;
                    corrigendumLOB.ApprovedTime = model.ApprovedTime;
                    db.CorrigendumLOB.Attach(corrigendumLOB);
                    db.Entry(corrigendumLOB).State = EntityState.Modified;
                    db.SaveChanges();
                }
                ////////added by dharmendra//////////////

                var DraftToAdminUpdate = (from Draft in db.DraftLOB where Draft.LOBId == model.LOBId select Draft).ToList();
                if (DraftToAdminUpdate.Count > 0)
                {
                    for (int i = 0; i < DraftToAdminUpdate.Count; i++)
                    {
                        var record = DraftToAdminUpdate[i];
                        var AdminToupdate = (from admin in db.AdminLob
                                             where admin.DraftRecordId == record.Id
                                                 && admin.LOBId == record.LOBId
                                             select admin).FirstOrDefault();
                        if (AdminToupdate != null)
                        {
                            AdminToupdate.ActionDocumentTypePDF = record.ActionDocumentTypePDF;
                            AdminToupdate.ActionDocumentTypePPT = record.ActionDocumentTypePPT;
                            AdminToupdate.ActionDocumentTypeVideo = record.ActionDocumentTypeVideo;
                            AdminToupdate.AssemblyId = record.AssemblyId;
                            AdminToupdate.AssemblyName = record.AssemblyName;
                            AdminToupdate.AssemblyNameLocal = record.AssemblyNameLocal;
                            AdminToupdate.ConcernedEventId = record.ConcernedEventId;
                            AdminToupdate.ConcernedEventName = record.ConcernedEventName;
                            AdminToupdate.ConcernedEventNameLocal = record.ConcernedEventNameLocal;
                            AdminToupdate.CommitteeId = record.CommitteeId;
                            AdminToupdate.CommitteeTitle = record.CommitteeTitle;
                            AdminToupdate.DeptId = record.DeptId;
                            AdminToupdate.CommitteeRepTypId = record.CommitteeRepTypId;
                            AdminToupdate.CreatedBy = record.CreatedBy;
                            AdminToupdate.CreatedDate = record.CreatedDate;
                            AdminToupdate.DraftRecordId = record.Id;
                            AdminToupdate.IsEVoting = Convert.ToBoolean(record.IsEVoting);
                            AdminToupdate.IsPublished = record.IsPublished;
                            AdminToupdate.IsSpeakerPageBreak = record.IsSpeakerPageBreak;
                            AdminToupdate.LOBId = record.LOBId;
                            //AdminTo.ModifiedBy = DraftTo.ModifiedBy;
                            //AdminTo.ModifiedDate = DraftTo.ModifiedDate;
                            AdminToupdate.PageBreak = record.PageBreak;
                            AdminToupdate.PDFLocation = record.PDFLocation;
                            AdminToupdate.PPTLocation = record.PPTLocation;
                            AdminToupdate.SessionDate = record.SessionDate;
                            AdminToupdate.SessionDateLocal = record.SessionDateLocal;
                            AdminToupdate.SessionId = record.SessionId;
                            AdminToupdate.SessionName = record.SessionName;
                            AdminToupdate.SessionNameLocal = record.SessionNameLocal;
                            AdminToupdate.SessionTime = record.SessionTime;
                            AdminToupdate.SessionTimeLocal = record.SessionTimeLocal;
                            AdminToupdate.SrNo1 = record.SrNo1;
                            AdminToupdate.SrNo2 = record.SrNo2;
                            AdminToupdate.SrNo3 = record.SrNo3;
                            AdminToupdate.TextBrief = record.TextBrief;
                            AdminToupdate.TextCurrent = record.TextCurrent;
                            AdminToupdate.TextLOB = record.TextLOB;
                            AdminToupdate.TextMinister = record.TextMinister;
                            AdminToupdate.TextSpeaker = record.TextSpeaker;
                            AdminToupdate.VideoLocation = record.VideoLocation;
                            AdminToupdate.IsDeleted = record.IsDeleted;
                            AdminToupdate.IsCorrigendum = record.IsCorrigendum;
                            AdminToupdate.SubmittedTime = record.SubmittedTime;
                            AdminToupdate.BillNo = record.BillNo;
                            db.AdminLob.Attach(AdminToupdate);
                            db.Entry(AdminToupdate).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                    }
                }


                db.Close();
            }
            //}

            return true;
        }


        static object UpdateApproveCorrigendumLOBPath(object param)
        {
            var model = (AdminLOB)param;

            DraftLOB dre = new DraftLOB();
            dre.LOBId = model.LOBId;
            CorrigendumLOB corrigendumLOB = (CorrigendumLOB)CorrigendumLOBByLOBId(dre);

            if (!corrigendumLOB.IsApproved || !corrigendumLOB.IsSubmitted)
            {
                return false;
            }
            else
            {
                using (LOBContext db = new LOBContext())
                {
                    var DraftToUpdate = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.LOBId == corrigendumLOB.LOBId select corrigendumDetails).ToList();
                    if (DraftToUpdate.Count > 0)
                    {
                        for (int i = 0; i < DraftToUpdate.Count; i++)
                        {
                            var record = DraftToUpdate[i];
                            record.LOBPath = model.LOBPath;
                            db.CorrigendumDetails.Attach(record);
                            db.Entry(record).State = EntityState.Modified;
                            db.SaveChanges();

                            //var AdminTo = (from admin in db.AdminLob
                            //               where admin.DraftRecordId == record.RecordId
                            //                   && admin.LOBId == record.LOBId
                            //               select admin).FirstOrDefault();

                            //if (AdminTo != null)
                            //{
                            //    AdminTo.LOBPath = model.LOBPath;

                            //    db.AdminLob.Attach(AdminTo);
                            //    db.Entry(AdminTo).State = EntityState.Modified;
                            //    db.SaveChanges();
                            //}
                        }

                        corrigendumLOB.ApprovedFile = model.LOBPath;
                        db.CorrigendumLOB.Attach(corrigendumLOB);
                        db.Entry(corrigendumLOB).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    db.Close();
                }
            }
            return true;
        }


        static object ReturnCorrigendumLOB(object param)
        {
            var model = (DraftLOB)param;

            DraftLOB dre = new DraftLOB();
            dre.LOBId = model.LOBId;
            CorrigendumLOB corrigendumLOB = (CorrigendumLOB)CorrigendumLOBByLOBId(dre);

            if (!corrigendumLOB.IsApproved && corrigendumLOB.IsSubmitted)
            {
                using (LOBContext db = new LOBContext())
                {
                    var DraftToUpdate = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.CorrigendumId == corrigendumLOB.CorrigendumId select corrigendumDetails).ToList();
                    if (DraftToUpdate.Count > 0)
                    {
                        for (int i = 0; i < DraftToUpdate.Count; i++)
                        {
                            var record = DraftToUpdate[i];
                            record.IsSubmitted = model.IsSubmitted;
                            record.SubmittedDate = model.SubmittedDate;
                            record.SubmittedTime = model.SubmittedTime;
                            db.CorrigendumDetails.Attach(record);
                            db.Entry(record).State = EntityState.Modified;
                            db.SaveChanges();


                            //var DraftTo = (from draft in db.DraftLOB
                            //               where draft.Id == record.RecordId
                            //                   && draft.LOBId == record.LOBId
                            //               select draft).FirstOrDefault();
                            //if (DraftTo != null)
                            //{

                            //    DraftTo.IsSubmitted = model.IsSubmitted;
                            //    DraftTo.SubmittedDate = model.SubmittedDate;
                            //    DraftTo.SubmittedTime = model.SubmittedTime;
                            //    db.DraftLOB.Attach(DraftTo);
                            //    db.Entry(DraftTo).State = EntityState.Modified;
                            //    db.SaveChanges();

                            //}

                        }
                        corrigendumLOB.IsSubmitted = false;
                        corrigendumLOB.SubmittedDate = model.SubmittedDate;
                        corrigendumLOB.SubmittedTime = model.SubmittedTime;
                        db.CorrigendumLOB.Attach(corrigendumLOB);
                        db.Entry(corrigendumLOB).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    db.Close();

                }
            }
            else
            {

            }




            return true;
        }

        static object GetCorrigendumPendingForSubmit(object param)
        {
            LOBContext db = new LOBContext();
            var result = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.IsSubmitted == false || corrigendumDetails.IsSubmitted == null select corrigendumDetails).ToList();
            result = result.DistinctBy(m => new { m.LOBId, m.SubmittedDate }).ToList();
            List<DraftLOB> DraftList = new List<DraftLOB>();
            for (int i = 0; i < result.Count; i++)
            {
                DraftLOB model = new DraftLOB();
                model.LOBId = result[i].LOBId;
                model.AssemblyName = result[i].AssemblyName;
                model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                model.SessionName = result[i].SessionName;
                model.SessionNameLocal = result[i].SessionNameLocal;
                model.SessionDate = result[i].SessionDate;
                model.SessionDateLocal = result[i].SessionDateLocal;
                model.SessionNameLocal = result[i].SessionNameLocal;
                model.SessionNameLocal = result[i].SessionNameLocal;
                DraftList.Add(model);
            }

            return DraftList;
        }

        static object GetCorrigendumSubmitted(object param)
        {
            LOBContext db = new LOBContext();
            var result = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.IsSubmitted != false && corrigendumDetails.IsSubmitted != null && corrigendumDetails.SubmittedDate != null select corrigendumDetails).ToList();
            result = result.DistinctBy(m => new { m.LOBId, m.SubmittedDate, m.SubmittedTime }).ToList();
            List<DraftLOB> DraftList = new List<DraftLOB>();
            for (int i = 0; i < result.Count; i++)
            {
                DraftLOB model = new DraftLOB();
                model.LOBId = result[i].LOBId;
                model.AssemblyName = result[i].AssemblyName;
                model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                model.SessionName = result[i].SessionName;
                model.SessionNameLocal = result[i].SessionNameLocal;
                model.SessionDate = result[i].SessionDate;
                model.SessionDateLocal = result[i].SessionDateLocal;
                model.SubmittedDate = result[i].SubmittedDate;
                model.SubmittedLOBPath = result[i].SubmittedLOBPath;
                if (Convert.ToString(result[i].IsApproved) != "")
                {
                    model.IsApproved = result[i].IsApproved;
                }
                else
                {
                    model.IsApproved = false;
                }
                model.SubmittedTime = result[i].SubmittedTime;

                DraftList.Add(model);
            }

            return DraftList;
        }



        static object GetCorrigendumPendingApproval(object param)
        {
            LOBContext db = new LOBContext();
            var result = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.IsSubmitted != false && corrigendumDetails.IsSubmitted != null && corrigendumDetails.SubmittedDate != null && (corrigendumDetails.IsApproved == false || corrigendumDetails.IsApproved == null) select corrigendumDetails).ToList();
            result = result.DistinctBy(m => new { m.LOBId, m.SubmittedDate, m.SubmittedTime }).ToList();
            List<DraftLOB> DraftList = new List<DraftLOB>();
            for (int i = 0; i < result.Count; i++)
            {
                DraftLOB model = new DraftLOB();
                model.LOBId = result[i].LOBId;
                model.AssemblyName = result[i].AssemblyName;
                model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                model.SessionName = result[i].SessionName;
                model.SessionNameLocal = result[i].SessionNameLocal;
                model.SessionDate = result[i].SessionDate;
                model.SessionDateLocal = result[i].SessionDateLocal;
                model.SubmittedDate = result[i].SubmittedDate;
                model.SubmittedLOBPath = result[i].SubmittedLOBPath;
                if (Convert.ToString(result[i].IsApproved) != "")
                {
                    model.IsApproved = result[i].IsApproved;
                }
                else
                {
                    model.IsApproved = false;
                }
                model.SubmittedTime = result[i].SubmittedTime;

                DraftList.Add(model);
            }
            return DraftList;
        }



        static object GetCorrigendumSubmittedApprove(object param)
        {
            LOBContext db = new LOBContext();
            var result = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.IsSubmitted != false && corrigendumDetails.IsSubmitted != null && corrigendumDetails.SubmittedDate != null && corrigendumDetails.IsApproved != false && corrigendumDetails.IsApproved != null select corrigendumDetails).ToList();
            result = result.DistinctBy(m => new { m.LOBId, m.SubmittedDate, m.SubmittedTime }).ToList();
            List<AdminLOB> DraftList = new List<AdminLOB>();
            for (int i = 0; i < result.Count; i++)
            {
                AdminLOB model = new AdminLOB();
                model.LOBId = result[i].LOBId;
                model.AssemblyName = result[i].AssemblyName;
                model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                model.SessionName = result[i].SessionName;
                model.SessionNameLocal = result[i].SessionNameLocal;
                model.SessionDate = result[i].SessionDate;
                model.SessionDateLocal = result[i].SessionDateLocal;
                model.ApprovedDate = result[i].ApprovedDate;
                model.LOBPath = result[i].LOBPath;
                model.ApprovedTime = result[i].ApprovedTime;

                DraftList.Add(model);
            }
            return DraftList;
        }



        static object GetCorrigendumById(object param)
        {
            LOBContext db = new LOBContext();
            CorrigendumLOB model = (CorrigendumLOB)param;
            var result = (from corri in db.CorrigendumLOB where corri.CorrigendumId == model.CorrigendumId select corri).FirstOrDefault();
            return result;
        }

        static object GetDraftLOBById(object param)
        {
            LOBContext db = new LOBContext();
            DraftLOB model = (DraftLOB)param;
            var result = (from Draftlob in db.DraftLOB where Draftlob.Id == model.Id select Draftlob).FirstOrDefault();
            return result;
        }

        static object AddPageBreakeCOM(object param)
        {
            //var model = (DraftLOB)param;
            var model = (tCommitteeDraft)param;
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = db.tCommitteeDraft.SingleOrDefault(a => a.Id == model.Id);

                DraftToUpdate.PageBreak = true;
                db.tCommitteeDraft.Attach(DraftToUpdate);
                db.Entry(DraftToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return true;
        }


        static object RemovePageBreakeCOM(object param)
        {
            var model = (tCommitteeDraft)param;
            using (LOBContext db = new LOBContext())
            {
                var DraftToUpdate = db.tCommitteeDraft.SingleOrDefault(a => a.Id == model.Id);

                DraftToUpdate.PageBreak = false;
                db.tCommitteeDraft.Attach(DraftToUpdate);
                db.Entry(DraftToUpdate).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return true;
        }

        static object CorrigendumCOMByCOMId(object param)
        {
            LOBContext db = new LOBContext();
            DraftLOB adminLOB = (DraftLOB)param;
            // tCommitteeDraft adminLOB = (tCommitteeDraft)param;
            //  var result = (from corrigendumCOM in db.CorrigendumCOM where corrigendumCOM.COMId == adminLOB.COMId orderby corrigendumCOM.CorrigendumId descending select corrigendumCOM).FirstOrDefault();
            var result = (from corrigendumLOB in db.CorrigendumLOB where corrigendumLOB.LOBId == adminLOB.LOBId orderby corrigendumLOB.CorrigendumId descending select corrigendumLOB).FirstOrDefault();

            return result;
        }

        static object GetCommitteeSubmit(object param)
        {
            LOBContext db = new LOBContext();
            var result = (from corrigendumDetails in db.tCommitteeDraft where corrigendumDetails.IsSubmitted == false || corrigendumDetails.IsSubmitted == null select corrigendumDetails).ToList();

            //var result = (from corrigendumDetails in db.CorrigendumDetails where corrigendumDetails.IsSubmitted == false || corrigendumDetails.IsSubmitted == null select corrigendumDetails).ToList();
            result = result.DistinctBy(m => new { m.COMId }).ToList();
            List<tCommitteeDraft> DraftList = new List<tCommitteeDraft>();
            for (int i = 0; i < result.Count; i++)
            {
                tCommitteeDraft model = new tCommitteeDraft();
                model.COMId = result[i].COMId;
                model.AssemblyName = result[i].AssemblyName;
                // model.AssemblyNameLocal = result[i].AssemblyNameLocal;
                model.SessionName = result[i].SessionName;
                //   model.SessionNameLocal = result[i].SessionNameLocal;
                model.MeetingDate = result[i].MeetingDate;
                //  model.SessionDateLocal = result[i].SessionDateLocal;
                //  model.SessionNameLocal = result[i].SessionNameLocal;
                //  model.SessionNameLocal = result[i].SessionNameLocal;
                DraftList.Add(model);
            }

            return DraftList;
        }


        #region UpdatedLOBPapers

        static object GetLOBUpdatedPapersByparameters(object param)
        {
            LOBContext db = new LOBContext();
            AdminLOB adminLOB = (AdminLOB)param;

            var LOB = (from AdminLob in db.AdminLob
                       join tPaperLaid in db.tPaperLaidV on AdminLob.DraftRecordId equals tPaperLaid.LOBRecordId
                       join tPaperLaidTempOld in db.tPaperLaidTemp on tPaperLaid.LOBPaperTempId equals tPaperLaidTempOld.PaperLaidTempId
                       join tPaperLaidTempNew in db.tPaperLaidTemp on tPaperLaid.MinisterActivePaperId equals tPaperLaidTempNew.PaperLaidTempId
                       where AdminLob.SessionDate == adminLOB.SessionDate && AdminLob.AssemblyId == adminLOB.AssemblyId && AdminLob.SessionId == adminLOB.SessionId
                       && tPaperLaid.MinisterActivePaperId > tPaperLaid.LOBPaperTempId

                       select new UpdatedLOBPapersModelcs
                       {
                           ConcernedEventName = AdminLob.ConcernedEventName,
                           PreviousFile = AdminLob.PDFLocation.Remove(0, 1),
                           LatestFile = tPaperLaidTempNew.SignedFilePath,
                           SrNo1 = AdminLob.SrNo1,
                           SrNo2 = AdminLob.SrNo2,
                           SrNo3 = AdminLob.SrNo3,
                           tPaperLaidTempId = tPaperLaid.MinisterActivePaperId,
                           tPaperLaidId = tPaperLaid.PaperLaidId,
                           AdminLobId = AdminLob.Id
                       }).ToList();


            return LOB;
        }
        static object UpdateLOBpapers(object param)
        {
            string id = param.ToString();
            string[] modelId = id.Split(',');
            int adminLobId = Convert.ToInt32(modelId[0]);
            long paperLaidId = Convert.ToInt64(modelId[1]);
            long tempId = Convert.ToInt64(modelId[2]);
            LOBContext LCtxt = new LOBContext();

            var query = (from tPaperLaidVS in LCtxt.tPaperLaidV where tPaperLaidVS.PaperLaidId == paperLaidId select tPaperLaidVS).SingleOrDefault();
            if (query != null)
            {
                UpdatedLOBPapersModelcs LobPaperModel = new UpdatedLOBPapersModelcs();
                query.LOBPaperTempId = query.MinisterActivePaperId;
                var query1 = (from tPaperTemp in LCtxt.tPaperLaidTemp where tPaperTemp.PaperLaidTempId == query.MinisterActivePaperId select tPaperTemp).SingleOrDefault();
                if (query1 != null)
                {
                    LobPaperModel.PreviousFile = query1.SignedFilePath;
                    var PdfLocation = (from AdminLob in LCtxt.AdminLob where AdminLob.Id == adminLobId select AdminLob.PDFLocation).SingleOrDefault();
                    if (PdfLocation != null)
                    {
                        LobPaperModel.LatestFile = PdfLocation.Remove(0, 1);
                        LCtxt.SaveChanges();
                        LCtxt.Close();
                        return LobPaperModel;
                    }
                }

            }
            return null;
        }
        static object GetLOBTextById(object param)
        {
            LOBContext lCtxt = new LOBContext();
            AdminLOB AdminLOB = param as AdminLOB;
            var query = (from admin in lCtxt.AdminLob where AdminLOB.Id == admin.Id select admin).SingleOrDefault();
            if (query != null)
            {
                return query;
            }
            else
            {
                return null;
            }
        }
        private static void UpdatePaperLaid(object param)
        {
            LOBContext lCtxt = new LOBContext();
            AdminLOB AdminLOB = param as AdminLOB;
            tPaperLaidV paperlaid = new tPaperLaidV();
            bool vslaid = false;
            AdminLOB = (from m in lCtxt.AdminLob where m.Id == AdminLOB.Id select m).SingleOrDefault();
            if (AdminLOB.BillNo != null && AdminLOB.BillNo != "")
            {
                mBills mbill = new mBills();
                mbill = (from mb in lCtxt.mBills where mb.BillNo == AdminLOB.BillNo select mb).SingleOrDefault();
                if (mbill != null)
                {
                    if (mbill.IsLaid == true)
                    {
                        mbill.IsLaid = false;
                    }
                    else
                    {
                        mbill.IsLaid = true;
                    }
                    lCtxt.mBills.Attach(mbill);
                    lCtxt.Entry(mbill).State = EntityState.Modified;
                    lCtxt.SaveChanges();
                }
                //lCtxt.Close();
            }


            //Committee Code
            if (AdminLOB.CommitteeId != null && AdminLOB.CommitteeId > 0)
            {
                tCommitteeReport mComReport = new tCommitteeReport();
                mComReport = (from mb in lCtxt.tCommitteeReport where mb.CommitteeId == AdminLOB.CommitteeId && mb.DateOfLaying == AdminLOB.SessionDate && mb.DeptmentID == AdminLOB.DeptId select mb).SingleOrDefault();
                if (mComReport != null)
                {
                    if (mComReport.IsFreeze == true)
                    {
                        mComReport.IsFreeze = false;
                    }
                    else
                    {
                        mComReport.IsFreeze = true;
                    }
                    lCtxt.tCommitteeReport.Attach(mComReport);
                    lCtxt.Entry(mComReport).State = EntityState.Modified;
                    lCtxt.SaveChanges();
                }
                //lCtxt.Close();
            }

            if (AdminLOB.IsLaid == true)
            {
                AdminLOB.IsLaid = false;
            }
            else
            {
                AdminLOB.IsLaid = true;
            }
            paperlaid = (from m in lCtxt.tPaperLaidV where m.LOBRecordId == AdminLOB.DraftRecordId select m).SingleOrDefault();
            if (paperlaid != null)
            {
                // mBills mbill = new mBills();
                //mbill = (from mb in lCtxt.mBills where mb.BillNo == AdminLOB.BillNo select mb).SingleOrDefault();
                if (AdminLOB.IsLaid == true)
                {
                    paperlaid.IsLaid = true;
                }
                else
                {
                    paperlaid.IsLaid = false;
                }
                lCtxt.tPaperLaidV.Attach(paperlaid);
                lCtxt.Entry(paperlaid).State = EntityState.Modified;
                lCtxt.SaveChanges();
                //lCtxt.Close();
            }
            lCtxt.AdminLob.Attach(AdminLOB);
            lCtxt.Entry(AdminLOB).State = EntityState.Modified;
            lCtxt.SaveChanges();
            lCtxt.Close();
            // return GetmQuestionRules();

        }
        #endregion

    }
}


