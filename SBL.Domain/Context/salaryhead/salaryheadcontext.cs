﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Extension;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Loan;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.salaryhead;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.StaffManagement;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;

namespace SBL.Domain.Context.salaryhead
{
    public class salaryheadcontext : DBBase<salaryheadcontext>
    {
        public salaryheadcontext()
            : base("eVidhan")
        {
        }

        public virtual DbSet<salaryheads> salaryheads { get; set; }

        public virtual DbSet<mMember> mMember { get; set; }

        public virtual DbSet<memberCategory> memberCategory { get; set; }

        public virtual DbSet<membersHead> membersHead { get; set; }

        public virtual DbSet<mMemberAssembly> mMemberAssembly { get; set; }

        public virtual DbSet<mAssembly> mAssembly { get; set; }

        public virtual DbSet<SiteSettings> mSiteSettings { get; set; }

        public virtual DbSet<membersSalary> membersSalary { get; set; }

        public virtual DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }

        public virtual DbSet<membersMonthlySalaryDetails> membersMonthlySalaryDetails { get; set; }

        public DbSet<loanDetails> loanDetails { get; set; }

        public DbSet<loaneeDetails> loaneeDetails { get; set; }

        public DbSet<loanTransDetails> loanTransDetails { get; set; }

        public DbSet<LoanType> loanType { get; set; }

        public DbSet<mReimbursementBill> mReimbursementBill { get; set; }

        public DbSet<mEstablishBill> mEstablishBill { get; set; }

        public DbSet<mMemberDesignation> mMemberDesignations { get; set; }

        public DbSet<mStaffDesignation> mStaffDesignation { get; set; }

        public DbSet<mStaff> mStaff { get; set; }

        public DbSet<mBudget> mBudget { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "getallmembers": { return getallmembers(param.Parameter); }
                case "getCategoryList": { return getCategoryList(); }
                case "getSalaryHeadList": { return getSalaryHeadList(param.Parameter); }
                case "saveMembersHeads": { return saveMembersHeads(param.Parameter); }
                case "getMemberName": { return getMemberName(param.Parameter); }
                case "getCatID": { return getCatID(param.Parameter); }
                case "getCatName": { return getCatName(param.Parameter); }
                case "getMembersHeads": { return getMembersHeads(param.Parameter); }
                case "getDesignation": { return getDesignation(param.Parameter); }
                case "savesalary": { return savesalary(param.Parameter); }
                case "savesalarydetails": { return savesalarydetails(param.Parameter); }
                case "checksalary": { return checksalary(param.Parameter); }
                case "getMonthlySalaryHeads": { return getMonthlySalaryHeads(param.Parameter); }
                case "getMonthlySalarydetails": { return getMonthlySalarydetails(param.Parameter); }
                case "checkHeadList": { return checkHeadList(param.Parameter); }
                case "getSalaryHeadListfrommembersalary": { return getSalaryHeadListfrommembersalary(param.Parameter); }
                case "getSalaryHeadByID": { return getSalaryHeadByID(param.Parameter); }
                case "updateMembersHeads": { return updateMembersHeads(param.Parameter); }
                case "checkMonthYear": { return checkMonthYear(param.Parameter); }
                case "getspeakerList": { return getspeakerList(); }
                case "saveSalaryHeads": { return saveSalaryHeads(param.Parameter); }
                case "updateSalaryHeads": { return updateSalaryHeads(param.Parameter); }
                case "getActiveSalaryHeadList": { return getActiveSalaryHeadList(); }
                case "getOrderNo": { return getOrderNo(param.Parameter); }
                case "getSHeadByID": { return getSHeadByID(param.Parameter); }
                case "deleteSHeadsByID": { return deleteSHeadsByID(param.Parameter); }
                case "getAssociateComponents": { return getAssociateComponents(param.Parameter); }
                case "getDetailsByDesignation": { return getDetailsByDesignation(param.Parameter); }
                case "updateSalaryBills": { return updateSalaryBills(param.Parameter); }
                case "getSalaryBillList": { return getSalaryBillList(param.Parameter); }
                case "getsalaryBillByID": { return getsalaryBillByID(param.Parameter); }
                case "changeBillStatus": { return changeBillStatus(param.Parameter); }
                case "generateOuterBill": { return generateOuterBill(param.Parameter); }
                case "generateOuterBillForMembers": { return generateOuterBillForMembers(param.Parameter); }
                case "getHouseRentDeductionList": { return getHouseRentDeductionList(param.Parameter); }
                case "saveBillRemark": { return saveBillRemark(param.Parameter); }
                case "updateBillFilePath": { return updateBillFilePath(param.Parameter); }
                case "getBillFilePath": { return getBillFilePath(param.Parameter); }
                case "getListofScheduleReport": { return getListofScheduleReport(param.Parameter); }
                case "getCurrentMemberList": { return getCurrentMemberList(param.Parameter); }
                case "deleteSalaryBills": { return deleteSalaryBills(param.Parameter); }
                case "UpdateLoan": { return UpdateLoan(param.Parameter); }
                case "getSalaryBillListForMember": { return getSalaryBillListForMember(param.Parameter); }
                case "checkSalaryBill": { return checkSalaryBill(param.Parameter); }
                case "checkSalaryBillList": { return checkSalaryBillList(param.Parameter); }
                case "SalaryBillList": { return SalaryBillList(param.Parameter); }
                case "getEbillDetails": { return getEbillDetails(param.Parameter); }
                case "updateBillDetailsInSalary": { return updateBillDetailsInSalary(param.Parameter); }
                case "getNominalReport": { return getNominalReport(param.Parameter); }
                case "getNominalReportForStaff": { return getNominalReportForStaff(param.Parameter); }
                case "generateFinancialYearSalaryForMembers": { return generateFinancialYearSalaryForMembers(param.Parameter); }
                
                case "GetAllVSMembers": { return GetAllVSMembers(param.Parameter); }
                case "CheckSalary": { return CheckSalary(param.Parameter); }
            }

            return null;
        }

        #region salary Heads

        public static object saveSalaryHeads(object param)
        {
            salaryheads _salaryHeads = param as salaryheads;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                try
                {
                    reOrdering(_salaryHeads.orderNo, 0, _salaryHeads.hType, "save");
                    _salaryHeads.hsubType = "a";
                    context.salaryheads.Add(_salaryHeads);
                    context.SaveChanges();
                    context.Close();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return null;
            }
        }

        public static object updateSalaryHeads(object param)
        {
            try
            {
                salaryheads _sHead = param as salaryheads;
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    // int lastOrderNo = ((salaryheads)getSalaryHeadByID(_sHead.sHeadId)).sHeadId;
                    salaryheads Last = (salaryheads)getSHeadByID(_sHead.sHeadId);
                    reOrdering(_sHead.orderNo, Last.orderNo, _sHead.hType, "update");
                    context.Entry(_sHead).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        public static object getActiveSalaryHeadList()
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var result = (from list in context.salaryheads
                              where list.Status == true
                              select new salaryHeadList
                              {
                                  sHeadID = list.sHeadId,
                                  sHeadName = list.sHeadName
                              }
                                ).ToList();
                return result;
            }
        }

        public static object getSHeadByID(object param)
        {
            int sHeadID = Convert.ToInt32(param);
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var result = (from list in context.salaryheads
                              where list.sHeadId == sHeadID
                              select list).FirstOrDefault();
                return result;
            }
        }

        public static bool checkComponentID(int sHeadID)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int count = (from list in context.salaryheads
                             where list.salaryComponentId == sHeadID
                             select list).Count();
                if (count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public static object getAssociateComponents(object param)
        {
            int sHeadID = Convert.ToInt32(param);
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var result = (from list in context.salaryheads
                              where list.salaryComponentId == sHeadID && list.amountType == "Percentage"
                              select new salaryComponentList
                              {
                                  componentID = list.sHeadId,
                                  perc = list.perc
                              }).ToList();
                return result;
            }
        }

        public static object deleteSHeadsByID(object param)
        {
            int sHeadID = Convert.ToInt32(param);
            if (checkComponentID(sHeadID))
            {
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    try
                    {
                        var result = (from list in context.salaryheads
                                      where list.sHeadId == sHeadID
                                      select list).FirstOrDefault();
                        salaryheads Last = (salaryheads)getSHeadByID(sHeadID);
                        reOrdering(0, result.orderNo, result.hType, "delete");
                        context.salaryheads.Remove(result);
                        context.SaveChanges();
                        context.Close();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        public static object getOrderNo(object param)
        {
            string type = param as string;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var result = (from list in context.salaryheads
                              where list.hType == type
                              orderby list.orderNo descending
                              select new salaryHeadList
                              {
                                  sHeadID = list.orderNo
                              }
                               ).ToList();
                return result;
            }
        }

        public static void reOrdering(int newID, int CurrentID, string hType, string Mode)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int count = (from list in context.salaryheads
                             where list.hType == hType
                             select list).Count();
                if (Mode == "save")
                {
                    if (newID <= count)
                    {
                        try
                        {
                            // count++;
                            for (int start = count; start >= newID; start--)
                            {
                                salaryheads _salaryHeads = (from _list in context.salaryheads
                                                            where _list.hType == hType && _list.orderNo == start
                                                            orderby _list.sHeadId descending
                                                            select _list

                                                                ).FirstOrDefault();
                                _salaryHeads.orderNo = _salaryHeads.orderNo + 1;
                                context.Entry(_salaryHeads).State = EntityState.Modified;
                                context.SaveChanges();
                                // context.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                else
                {
                    if (Mode == "update")
                    {
                        if (newID < CurrentID)
                        {
                            if (newID <= count)
                            {
                                try
                                {
                                    // count++;
                                    for (int start = CurrentID; start >= newID; start--)
                                    {
                                        salaryheads _salaryHeads = (from _list in context.salaryheads
                                                                    where _list.hType == hType && _list.orderNo == start
                                                                    orderby _list.sHeadId descending
                                                                    select _list

                                                                        ).FirstOrDefault();
                                        if (start == CurrentID)
                                        {
                                            _salaryHeads.orderNo = 0;
                                        }
                                        else
                                        {
                                            _salaryHeads.orderNo = _salaryHeads.orderNo + 1;
                                        }
                                        context.Entry(_salaryHeads).State = EntityState.Modified;
                                        context.SaveChanges();
                                        // context.Close();
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                        else
                        {
                            if (newID > CurrentID)
                            {
                                for (int start = CurrentID; start <= newID; start++)
                                {
                                    salaryheads _salaryHeads = (from _list in context.salaryheads
                                                                where _list.hType == hType && _list.orderNo == start
                                                                orderby _list.sHeadId descending
                                                                select _list

                                                                    ).FirstOrDefault();
                                    if (start == CurrentID)
                                    {
                                        _salaryHeads.orderNo = 0;
                                    }
                                    else
                                    {
                                        _salaryHeads.orderNo = _salaryHeads.orderNo - 1;
                                    }
                                    context.Entry(_salaryHeads).State = EntityState.Modified;
                                    context.SaveChanges();
                                    // context.Close();
                                }
                            }
                            else
                            {
                                if (newID == CurrentID)
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Mode == "delete")
                        {
                            for (int start = CurrentID + 1; start <= count; start++)
                            {
                                salaryheads _salaryHeads = (from _list in context.salaryheads
                                                            where _list.hType == hType && _list.orderNo == start
                                                            orderby _list.sHeadId descending
                                                            select _list

                                                                ).FirstOrDefault();
                                _salaryHeads.orderNo = _salaryHeads.orderNo - 1;
                                context.Entry(_salaryHeads).State = EntityState.Modified;
                                context.SaveChanges();
                                // context.Close();
                            }
                        }
                    }
                }
            }
        }

        #endregion salary Heads

        public static object getCurrentMemberList(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                string assemblyid = param as string;
                //var q = (from assemblyid in context.mSiteSettings
                //         where assemblyid.SettingName == "Assembly"
                //         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(assemblyid);
                //List<int?> list = new List<int?>() { 0 };
                List<int?> list = new List<int?>() { 1, 14 };
                var speakerlist = (from member in context.mMember
                                   join massembly in context.mMemberAssembly
                                   on member.MemberCode equals massembly.MemberID
                                   where list.Contains(massembly.DesignationID) &&
                                   massembly.AssemblyID == aid
                                   select member.MemberCode).ToList();
                List<int> ministers = new List<int>();

                var mid = (from minister in context.mMinsitryMinister
                           where minister.AssemblyID == aid && minister.IsDeleted!=true
                           select minister.MemberCode).ToList();
                ministers = mid;
                foreach (var i in speakerlist)
                {
                    ministers.Add(i);
                }

                //For Chief Whip
                // List<int> ChiefWhip = new List<int>();
                var WhipList = (from e in context.mMember
                                join massembly in context.mMemberAssembly
                                on e.MemberCode equals massembly.MemberID
                                where massembly.AssemblyID == aid && e.Active == true && massembly.Active == true
                                && massembly.DesignationID==58
                                select e.MemberCode).ToList();
                 // ChiefWhip = WhipList;
                //
                var query = (from e in context.mMember
                             join massembly in context.mMemberAssembly
                                   on e.MemberCode equals massembly.MemberID
                             where !(mid.Contains(e.MemberCode)) && !(WhipList.Contains(e.MemberCode))
                             &&
                             massembly.AssemblyID == aid
                             //&&
                             //e.Active == true

                             orderby e.Name

                             select new fillCurrentMamber
                             {
                                 MemberName = e.Name,
                                 MemberCode = e.MemberCode
                             }
                                   );
                return query.ToList();
            }
        }

        public static object getallmembers(object param)
        {
            // string assemblyid = param as string;
            int aid = Convert.ToInt32(param);
            // int aid = Convert.ToInt32(assemblyid);
            using (salaryheadcontext context = new salaryheadcontext())
            {
                //var q = (from assemblyid in context.mSiteSettings
                //         where assemblyid.SettingName == "Assembly"
                //         select assemblyid.SettingValue).FirstOrDefault();
                //int aid = assemblyId;
                //var q = (from mem in context.mSiteSettings
                //         where mem.SettingName == "OMLA"
                //         select mem.SettingValue).FirstOrDefault();
                //int id = Convert.ToInt32(q);

                //List<string> omla = new List<string>();
                //if (q != null)
                //{
                //    var query1 = (from member in context.mMember
                //                  join massembly in context.mMemberAssembly
                //                  on member.MemberCode equals massembly.MemberID
                //                  where massembly.MemberID == id && massembly.AssemblyID == aid
                //                  select member.Name);
                //    //  return query1.ToList();


                //omla = query1.ToList();
                //}
              //  List<int?> list = new List<int?>() { 0 };
                List<int?> list = new List<int?>() { 1, 14 };
                var speakerlist = (from member in context.mMember
                                   join massembly in context.mMemberAssembly
                                   on member.MemberCode equals massembly.MemberID
                                   where list.Contains(massembly.DesignationID) &&
                                   massembly.AssemblyID == aid
                                   select member.MemberCode).ToList();
                //For Ministers
                List<int> ministers = new List<int>();
                var mid = (from minister in context.mMinsitryMinister
                           where minister.AssemblyID == aid && minister.IsDeleted != true
                           select minister.MemberCode).ToList();
                ministers = mid;
                foreach (var i in speakerlist)
                {
                    ministers.Add(i);
                }
                //
                
                //For Chief Whip
                List<int> ChiefWhip = new List<int>();
                var WhipList = (from e in context.mMember
                                join massembly in context.mMemberAssembly
                                on e.MemberCode equals massembly.MemberID
                                where massembly.AssemblyID == aid && e.Active == true && massembly.Active == true
                                && massembly.DesignationID == 58
                                select e.MemberCode).ToList();
                ChiefWhip = WhipList;
                //
                foreach (var i in WhipList)
                {
                    ministers.Add(i);
                }
                var query = (from e in context.mMember
                             join massembly in context.mMemberAssembly
                             on e.MemberCode equals massembly.MemberID
                             where !(mid.Contains(e.MemberCode)) &&
                             massembly.AssemblyID == aid
                            // && e.Active == true 
                             && massembly.Active == true

                             orderby e.Name
                             select e).ToList();

                return query;
            }
        }

        //public static object getDetailsByDesignation(object param)
        //{
        //    try
        //    {
        //        using (salaryheadcontext context = new salaryheadcontext())
        //        {
        //            int designationID = Convert.ToInt32(param);

        //            var q = (from assemblyid in context.mSiteSettings
        //                     where assemblyid.SettingName == "Assembly"
        //                     select assemblyid.SettingValue).FirstOrDefault();
        //            int aid = Convert.ToInt32(12);

        //            var query = (from member in context.mMember
        //                         join massembly in context.mMemberAssembly
        //                         on member.MemberCode equals massembly.MemberID
        //                         where massembly.DesignationID == designationID && massembly.AssemblyID == aid
        //                         select member);
        //            return query.ToList();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public static object getDetailsByDesignation(object param)
        {
            try
            {
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    string[] listarry = param as string[];
                    int designationID = Convert.ToInt32(listarry[0]);
                    int assemblyid = Convert.ToInt32(listarry[1]);
                    //int designationID = Convert.ToInt32(param);
                    //int assemblyid = Convert.ToInt32(param);

                    var q = (from mem in context.mSiteSettings
                             where mem.SettingName == "ProtemSpeaker"
                             select mem.SettingValue).FirstOrDefault();
                   

                    if (q != null)
                    {
                        int id = Convert.ToInt32(q);
                        var query = (from member in context.mMember
                                     join massembly in context.mMemberAssembly
                                     on member.MemberCode equals massembly.MemberID
                                     where massembly.MemberID == id && massembly.AssemblyID == assemblyid
                                     select member);
                        return query.ToList();
                    }
                    else
                    {
                        var query = (from member in context.mMember
                                     join massembly in context.mMemberAssembly
                                     on member.MemberCode equals massembly.MemberID
                                     where massembly.DesignationID == designationID && massembly.AssemblyID == assemblyid
                                     select member);
                        return query.ToList();
                    }


                    //var query = (from member in context.mMember
                    //             join massembly in context.mMemberAssembly
                    //             on member.MemberCode equals massembly.MemberID
                    //             where massembly.DesignationID == designationID && massembly.AssemblyID == assemblyid
                    //             select member);
                    //return query.ToList();


                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static object getspeakerList()
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);

                List<int?> list = new List<int?>() { 1, 14 };
                var query = (from member in context.mMember
                             join massembly in context.mMemberAssembly
                             on member.MemberCode equals massembly.MemberID
                             where list.Contains(massembly.DesignationID) && massembly.AssemblyID == aid
                             orderby massembly.DesignationID
                             select member);
                return query.ToList();
            }
        }

        public static object getCategoryList()
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var query = (from categorylist in context.memberCategory
                             select categorylist);

                return query.ToList();
            }
        }

        public static object getMemberName(object param)
        {
            int memberId = Convert.ToInt32(param);

            using (salaryheadcontext context = new salaryheadcontext())
            {
                var query = (from members in context.mMember
                             where members.MemberCode == memberId
                             select members.Prefix + " " + members.Name + " ").FirstOrDefault();

                return query;
            }
        }

        public static object getCatID(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int membercode = Convert.ToInt32(param);
                var query = (from memCatId in context.mMemberAssembly
                             where memCatId.MemberID == membercode
                             select memCatId.Category).FirstOrDefault();
                return query;
            }
        }

        public static object getCatName(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int catID = Convert.ToInt32(param);
                var query = (from cat in context.memberCategory
                             where cat.autoId == catID
                             select cat.categoryName).FirstOrDefault();
                return query;
            }
        }

        public static object getDesignation(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int memberID = Convert.ToInt32(param);

                var assemblyID = (from assembly in context.mMemberAssembly
                                  join mAssemblyOrder in context.mAssembly
                                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                  where assembly.MemberID == memberID
                                  orderby mAssemblyOrder.AssemblyID descending
                                  select assembly.AssemblyID
                                          ).FirstOrDefault();
                var mmemberAssemblies = (from memberAssemblies in context.mMemberAssembly
                                         where memberAssemblies.AssemblyID == assemblyID
                                         && memberAssemblies.MemberID == memberID
                                         select memberAssemblies.Designation
                                        ).FirstOrDefault();
                return mmemberAssemblies;
            }
        }

        public static object getSalaryHeadList(object param)
        {
            //int categoryID = Convert.ToInt32(param);
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var query = (from headlist in context.salaryheads
                             where headlist.LoanID == null
                             orderby headlist.hType, headlist.orderNo

                             select headlist);
                return query.ToList();
            }
        }

        // get members head
        public static object getMembersHeads(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int[] listarry = param as int[];
                int mId = Convert.ToInt32(listarry[0]);
                int memberCatId = listarry[1];
                int monthID = listarry[2];
                int yearid = listarry[3];
                int assemblyID = listarry[4];
                var membersalary = (from mheadlist in context.membersHead
                                    join shead in context.salaryheads
                                    on mheadlist.sHeadId equals shead.sHeadId
                                    where mheadlist.membersCatId == memberCatId && mheadlist.status == true && shead.LoanID == null
                                    orderby shead.hType, shead.orderNo
                                    select new { membersHeads = mheadlist, headName = shead.sHeadName, htype = shead.hType });

                var name = (from members in context.mMember
                            where members.MemberCode == mId
                            select members.Prefix + " " + members.Name + " ").FirstOrDefault();

                //var assemblyID = (from assembly in context.mMemberAssembly
                //                  join mAssemblyOrder in context.mAssembly
                //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                //                  where assembly.MemberID == mId
                //                  orderby mAssemblyOrder.AssemblyID descending
                //                  select assembly.AssemblyID
                //                        ).FirstOrDefault();
                // var assemblyID = Convert.ToInt32(12);
                var designation = (from memberAssemblies in context.mMemberAssembly
                                   join desg in context.mMemberDesignations
                                   on memberAssemblies.DesignationID equals desg.memDesigcode
                                   where memberAssemblies.AssemblyID == assemblyID
                                   && memberAssemblies.MemberID == mId
                                   select desg.memDesigname
                                   ).FirstOrDefault();

                salarystructure _salarys = new salarystructure();
                _salarys.catID = 0;
                _salarys.approved = false;
                _salarys.designationName = designation;
                _salarys.memberCode = mId;
                _salarys.memberName = name;
                _salarys.monthID = 0;
                _salarys.yearId = 0;
                _salarys.netSalary = 0.00;
                _salarys.netSalaryInWords = "";
                _salarys.totalAllowances = 0.00;
                _salarys.totalDeduction = 0.00;
                _salarys.Mode = "save";
                List<HeadList> hlist = new List<HeadList>();
                foreach (var a in membersalary.ToList())
                {
                    HeadList s = new HeadList();
                    s.amount = Convert.ToDouble(a.membersHeads.amount.HasValue ? a.membersHeads.amount : 0);
                    s.headID = a.membersHeads.sHeadId;
                    s.headName = a.headName;
                    s.htype = a.htype;
                    if (a.htype == "cr")
                    {
                        _salarys.totalAllowances += Convert.ToDouble(a.membersHeads.amount.HasValue ? a.membersHeads.amount : 0);
                    }
                    else
                    {
                        _salarys.totalDeduction += Convert.ToDouble(a.membersHeads.amount.HasValue ? a.membersHeads.amount : 0);
                    }
                    hlist.Add(s);
                }
                var loanhead = (from list in context.salaryheads
                                where list.LoanID != null && list.LoanComponentType == "P"
                                select list).ToList();
                foreach (var item in loanhead)
                {
                    double[] arr = checkLoanAmount(mId, item.LoanID, monthID, yearid);
                    if ((arr[0] != 0) && (arr[1] != 0))
                    {
                        HeadList s = new HeadList();
                        s.amount = arr[0];
                        s.headID = item.sHeadId;
                        s.headName = item.sHeadName;
                        s.htype = item.hType;
                        s.loanNumber = Convert.ToInt64(arr[2]);
                        _salarys.totalDeduction += arr[0];

                        hlist.Add(s);
                        var iHead = (from list in context.salaryheads
                                     where list.LoanID == item.LoanID && list.LoanComponentType == "I"
                                     select list).FirstOrDefault();

                        HeadList si = new HeadList();
                        si.amount = arr[1];
                        si.headID = iHead.sHeadId;
                        si.headName = iHead.sHeadName;
                        si.htype = iHead.hType;

                        _salarys.totalDeduction += arr[1];

                        hlist.Add(si);
                    }
                }

                _salarys.headList = hlist;
                _salarys.netSalary = _salarys.totalAllowances - _salarys.totalDeduction;
                return _salarys;
            }
        }

        public static double[] checkLoanAmount(int memberID, int? loanTypeID, int month, int year)
        {
            double pAmount = 0;
            double iAmount = 0;
            long loanNumber = 0;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var loanList = (from loanee in context.loaneeDetails
                                join loandetails in context.loanDetails
                                on loanee.loaneeID equals loandetails.loaneeID
                                where loanee.memberCode == memberID && loandetails.loanTypeID == loanTypeID && loandetails.loanStatus== true
                                select loandetails).ToList();
                if (loanList.Count > 0)
                {
                    foreach (var item in loanList)
                    {
                        var loanTransDetails = (from loanTrans in context.loanTransDetails
                                                where loanTrans.loanNumber == item.loanNumber
                                                && loanTrans.monthID == month
                                                && loanTrans.yearID == year
                                                && loanTrans.paymentMode == "Salary"
                                                && loanTrans.transType == "dr"
                                                orderby new { loanTrans.yearID, loanTrans.monthID, loanTrans.autoID } descending
                                                select loanTrans).ToList();
                        if (loanTransDetails.Count > 0)
                        {
                            foreach (var items in loanTransDetails)
                            {
                                loanNumber = item.loanNumber;
                                pAmount += items.principalReceived;
                                iAmount += items.interestReceived;
                            }
                        }
                        else
                        {
                            var Lastloantrans = (from tran in context.loanTransDetails
                                                 where tran.loanNumber == item.loanNumber
                                                 orderby new { tran.yearID, tran.monthID, tran.autoID } descending
                                                 select tran).FirstOrDefault();
                            if (Lastloantrans.closingBalance > 0)
                            {
                                loanNumber = item.loanNumber;
                                if (Lastloantrans.principalReceived > 0)
                                {
                                    pAmount = Lastloantrans.principalReceived;
                                }
                                else
                                {
                                    pAmount = Math.Round(item.loanAmount / item.totalNoofEMI);
                                }
                            }
                            else
                            {
                                pAmount += 0;
                            }
                            if (Lastloantrans != null)
                            {
                                if (Lastloantrans.closingBalance > 0)
                                {
                                    iAmount += Math.Round((Lastloantrans.closingBalance * item.loanInterestRate) / 1200);
                                }
                                else
                                {
                                    iAmount += 0;
                                }
                            }
                            else
                            {
                                iAmount += 0;
                            }
                        }
                    }
                }

                return new double[] { pAmount, iAmount, loanNumber };
            }
        }

        public static object getMemberHeadsFromMonthlyRecords(object param)
        {
            int[] list = param as int[];
            int memberID = list[0];
            int month = list[1];
            int year = list[2];
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var query = (from _list in context.membersSalary
                             where _list.membersId == memberID && _list.monthID == month && _list.Year == year
                             select _list).ToList();
                return query;
            }
        }

        //save members head

        public static object saveMembersHeads(object param)
        {
            try
            {
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    membersHead memheads = param as membersHead;
                    context.membersHead.Add(memheads);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        public static object updateMembersHeads(object param)
        {
            try
            {
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    membersHead memheads = param as membersHead;
                    context.Entry(memheads).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        // save salary
        public static object savesalary(object param)
        {
            try
            {
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    membersSalary memheads = param as membersSalary;
                    context.membersSalary.Add(memheads);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        public static object UpdateLoan(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int[] listarry = param as int[];
                int monthid = listarry[0];
                int yearId = listarry[1];
                int membercode = listarry[2];
                //   int loannumber = listarry[3];
                int assemblyid = listarry[3];
                //var headlist = (from list in context.membersSalary
                //                where list.monthID == monthid && list.Year == yearId && list.loanNumber != null
                //                select list).ToList();
                var headlist = (from list in context.membersSalary
                                where list.monthID == monthid && list.Year == yearId && list.membersId == membercode && list.loanNumber != null
                                select list).ToList();
                foreach (var item in headlist)
                {
                    var PHead = (from list in context.salaryheads
                                 where list.sHeadId == item.headId
                                 select list).FirstOrDefault();
                    var iHead = (from list in context.salaryheads
                                 where list.LoanID == PHead.LoanID && list.LoanComponentType == "I"
                                 select list).FirstOrDefault();
                    var pAmount = (from list in context.membersSalary
                                   where list.monthID == monthid && list.Year == yearId && list.headId == PHead.sHeadId && list.membersId == item.membersId
                                   select list.amount).FirstOrDefault();
                    var iAmounts = (from list in context.membersSalary
                                    where list.monthID == monthid && list.Year == yearId && list.headId == iHead.sHeadId && list.membersId == item.membersId
                                    select list.amount);
                    deleteLoanInstallment(new long[] { monthid, yearId, Convert.ToInt64(item.loanNumber) });
                    installmentDetails inst = (installmentDetails)getLoanDetailsByLoanID(item.loanNumber);
                    inst.DDOCode = "039";
                    inst.PaymentMode = "Salary";
                    inst.treasuryName = "DTO Shimla";
                    inst.PaymentDate = DateTime.Now.ToString("dd/MM/yyyy");
                    inst.pAmount = pAmount;
                    inst.iAmount = iAmounts.FirstOrDefault();
                    inst.voucherDate = DateTime.Now.ToString();
                    inst.month = monthid;
                    inst.year = yearId;
                    updateLoanInstallment(inst);
                }
                return null;
            }
        }

        public static object getLoanDetailsByLoanID(object param)
        {
            long loanNumber = Convert.ToInt64(param.ToString());
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var result = (from _loan in context.loanDetails
                              join _loanee in context.loaneeDetails
                              on _loan.loaneeID equals _loanee.loaneeID
                              where _loan.loanNumber == loanNumber
                              select new
                              {
                                  laon = _loan,
                                  laonee = _loanee
                              }
                                ).FirstOrDefault();
                var loantrans = (from tran in context.loanTransDetails
                                 where tran.loanNumber == loanNumber
                                 orderby new { tran.yearID, tran.monthID, tran.autoID } descending
                                 select tran).FirstOrDefault();
                installmentDetails inst = new installmentDetails();
                inst.Address = result.laonee.address;
                inst.ConsCode = result.laonee.constituencyCode;
                inst.ConsName = result.laonee.constituencyName;
                mMemberAssembly m = getLastAssembly(result.laonee.memberCode);
                inst.Designation = m.Designation;
                inst.EMI = (from emi in context.loanTransDetails
                            where emi.loanNumber == loanNumber
                            && emi.transType == "dr"
                            select emi
                              ).Count() + 1;
                inst.pAmount = Math.Round(result.laon.loanAmount / result.laon.totalNoofEMI);
                if (loantrans != null)
                {
                    if (loantrans.closingBalance > 0)
                    {
                        inst.iAmount = Math.Round((loantrans.closingBalance * result.laon.loanInterestRate) / 1200);
                    }
                    else
                    {
                        inst.iAmount = 0;
                    }
                }
                else
                {
                    inst.iAmount = 0;
                }

                inst.intRate = result.laon.loanInterestRate;
                inst.loanAccountNumber = result.laon.loanAccountNumber;
                inst.loanAmount = result.laon.loanAmount;
                inst.loanNumber = result.laon.loanNumber;
                inst.loantypeID = result.laon.loanTypeID;
                inst.loanType = (from name in context.loanType
                                 where name.loanID == result.laon.loanTypeID
                                 select name.loanName).FirstOrDefault();
                inst.memberCode = result.laonee.memberCode;
                inst.memberName = result.laonee.memberName;
                inst.mobileNumber = result.laonee.mobile;
                inst.noOFEMI = result.laon.totalNoofEMI;
                inst.paidInt = inst.iAmount;
                var ss = result.laon.loanSanctionDate.Split(' ')[0];
                // inst.sancationDate = string.Format("{0:dd-mm-yyyy}", DateTime.Parse(result.laon.loanSanctionDate.Split(' ')[0]));
                DateTime ToDateD = new DateTime(Convert.ToInt32(ss.Split('/')[2]), Convert.ToInt32(ss.Split('/')[1]), Convert.ToInt32(ss.Split('/')[0]));

                inst.sancationDate = ToDateD.ToString("dd/MM/yyyy");
                return inst;
            }
        }

        public static object deleteLoanInstallment(object param)
        {
            long[] listarry = param as long[];
            int monthid = Convert.ToInt32(listarry[0]);
            int yearId = Convert.ToInt32(listarry[1]);
            long loanNumber = listarry[2];
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var loantrans = (from tran in context.loanTransDetails
                                 where tran.loanNumber == loanNumber && tran.monthID == monthid && tran.yearID == yearId && tran.paymentMode == "Salary"
                                 orderby tran.autoID descending
                                 select tran).ToList();
                if (loantrans.Count > 0)
                {
                    foreach (var item in loantrans)
                    {
                        context.Entry(item).State = EntityState.Deleted;
                        context.SaveChanges();
                    }
                }
                context.Close();

                return null;
            }
        }

        public static object updateLoanInstallment(object param)
        {
            installmentDetails detail = param as installmentDetails;
            using (salaryheadcontext context = new salaryheadcontext())
            {

                try
                {


                    var loantrans = (from tran in context.loanTransDetails
                                     where tran.loanNumber == detail.loanNumber
                                     orderby tran.autoID descending
                                     select tran).FirstOrDefault();
                    loanTransDetails trans = new loanTransDetails();
                    trans.closingBalance = loantrans.openingBalance - detail.pAmount;
                    trans.DDOCode = detail.DDOCode;
                    trans.disbursementAmount = 0;
                    trans.installmentNumber = detail.EMI;
                    trans.interestForMonth = detail.paidInt;
                    trans.interestReceived = detail.iAmount;
                    trans.loanAccontNumber = detail.loanAccountNumber;
                    trans.loanNumber = detail.loanNumber;
                    //trans.monthID = DateTime.Now.Month;
                    //trans.yearID = DateTime.Now.Year;
                    trans.monthID = detail.month;
                    trans.yearID = detail.year;
                    trans.openingBalance = loantrans.closingBalance;
                    trans.closingBalance = loantrans.closingBalance - detail.pAmount;
                    trans.outstandingInterest = loantrans.outstandingInterest + (detail.paidInt - detail.iAmount);
                    trans.payDate = DateTime.Now;
                    trans.paymentCode = detail.PaymentCode;
                    trans.paymentDate = detail.PaymentDate;
                    trans.paymentMode = detail.PaymentMode;
                    trans.principalReceived = detail.pAmount;
                    trans.transType = "dr";
                    trans.treasuryName = detail.treasuryName;
                    DateTime Date = new DateTime();
                    DateTime.TryParse(detail.voucherDate, out Date);
                    trans.voucherDate = Date;
                    trans.voucherNumber = detail.voucherNumber;
                    trans.paymentDate = DateTime.Now.ToString("dd/MM/yyyy");
                    trans.intRate = detail.intRate;

                    context.loanTransDetails.Add(trans);
                    context.SaveChanges();
                    context.Close();
                }
                catch (Exception ex)
                {

                    throw;
                }
                return null;

            }
        }

        //public static mMemberAssembly getLastAssembly(int? memberID)
        //{
        //    using (salaryheadcontext context = new salaryheadcontext())
        //    {
        //        var assemblyID = (from assembly in context.mMemberAssembly
        //                          join mAssemblyOrder in context.mAssembly
        //                          on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
        //                          where assembly.MemberID == memberID
        //                          orderby mAssemblyOrder.AssemblyID descending
        //                          select assembly.AssemblyID
        //                    ).FirstOrDefault();
        //        var memberAssemblies = (from memberAssembly in context.mMemberAssembly
        //                                where memberAssembly.AssemblyID == assemblyID
        //                                && memberAssembly.MemberID == memberID
        //                                select memberAssembly
        //                                ).FirstOrDefault();
        //        return memberAssemblies;
        //    }
        //}



        public static mMemberAssembly getLastAssembly(int? memberID)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var assemblyID = (from assembly in context.mMemberAssembly
                                  join mAssemblyOrder in context.mAssembly
                                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                  where assembly.MemberID == memberID
                                  orderby mAssemblyOrder.AssemblyID descending
                                  select assembly.AssemblyID
                            ).FirstOrDefault();
                var memberAssemblies = (from memberAssembly in context.mMemberAssembly
                                        where memberAssembly.AssemblyID == assemblyID
                                        && memberAssembly.MemberID == memberID
                                        select memberAssembly
                                        ).FirstOrDefault();
                return memberAssemblies;
            }
        }



        //delete salary haeds
        public static object deletesalary(object param)
        {
            try
            {
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    membersSalary memheads = param as membersSalary;
                    context.membersSalary.Remove(memheads);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        // saave salary details
        public static object savesalarydetails(object param)
        {
            try
            {
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    membersMonthlySalaryDetails msalarydetails = param as membersMonthlySalaryDetails;
                    context.membersMonthlySalaryDetails.Add(msalarydetails);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        public static object checksalary(object param)
        {
            int[] list = param as int[];
            int memberID = list[0];
            int month = list[1];
            int year = list[2];
            int assemblyid = list[3];
            int ispart = list[4];
            int catid = list[5];
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var query = (from check in context.membersMonthlySalaryDetails
                             where check.membersid == memberID && check.month == month && check.year == year
                             && check.Ispart == ispart && check.AssemblyId == assemblyid && check.catID == catid
                             select check).Count();

                return query;
            }
        }

        public static object getSalaryHeadByID(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                string[] listarry = param as string[];
                int catID = Convert.ToInt32(listarry[0]);
                int sHeadID = Convert.ToInt32(listarry[1]);
                var query = (from head in context.membersHead
                             where head.membersCatId == catID && head.sHeadId == sHeadID
                             select head).FirstOrDefault();
                return query;
            }
        }

        public static object getMonthlySalaryHeads(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int[] listarry = param as int[];
                int mId = Convert.ToInt32(listarry[0]);
                int monthid = listarry[1];
                int yearId = listarry[2];
                int assemblyID = Convert.ToInt32(listarry[3]);
                int ispart = listarry[4];
                int catid = listarry[5];
                var membersalary = (from list in context.membersSalary
                                    join shead in context.salaryheads
                                   on list.headId equals shead.sHeadId
                                    where list.membersId == mId && list.monthID == monthid && list.Year == yearId
                                    && list.AssemblyId == assemblyID && list.ispart == ispart && list.catID == catid
                                    orderby shead.hType, shead.orderNo
                                    select new { membersalary = list, HeadName = shead.sHeadName, htype = shead.hType });

                var name = (from members in context.mMember
                            where members.MemberCode == mId
                            select members.Prefix + " " + members.Name + " ").FirstOrDefault();

                //var assemblyID = (from assembly in context.mMemberAssembly
                //                  join mAssemblyOrder in context.mAssembly
                //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                //                  where assembly.MemberID == mId
                //                  orderby mAssemblyOrder.AssemblyID descending
                //                  select assembly.AssemblyID
                //                        ).FirstOrDefault();
                var designation = (from memberAssemblies in context.mMemberAssembly
                                   join desg in context.mMemberDesignations
                                   on memberAssemblies.DesignationID equals desg.memDesigcode
                                   where memberAssemblies.AssemblyID == assemblyID
                                   && memberAssemblies.MemberID == mId
                                   select desg.memDesigname
                                    ).FirstOrDefault();

                var monthlydetails = (from list in context.membersMonthlySalaryDetails
                                      where list.membersid == mId && list.month == monthid && list.year == yearId
                                      && list.AssemblyId == assemblyID && list.Ispart == ispart && list.catID == catid
                                      select list).FirstOrDefault();

                salarystructure _salarys = new salarystructure();
                _salarys.catID = 0;
                if (monthlydetails != null)
                {
                    if (monthlydetails.JDCStatus == 1)
                    {
                        _salarys.approved = true;
                    }
                    else
                    {
                        _salarys.approved = false;
                    }

                    _salarys.designationName = designation;
                    _salarys.memberCode = mId;
                    _salarys.memberName = name;
                    _salarys.monthID = monthid;
                    _salarys.yearId = yearId;
                    _salarys.netSalary = monthlydetails.grandtotal;
                    _salarys.netSalaryInWords = "";
                    _salarys.totalAllowances = monthlydetails.totalAllowances;
                    _salarys.totalDeduction = monthlydetails.totalAGDeductionsA;
                }
                _salarys.Mode = "update";
                List<HeadList> hlist = new List<HeadList>();
                if (membersalary.ToList().Count() > 0)
                {
                    foreach (var a in membersalary.ToList())
                    {
                        HeadList s = new HeadList();
                        s.amount = Convert.ToDouble(a.membersalary.amount);
                        s.headID = a.membersalary.headId;
                        s.headName = a.HeadName;
                        s.htype = a.htype;
                        s.loanNumber = a.membersalary.loanNumber;

                        hlist.Add(s);
                    }
                    _salarys.headList = hlist;
                }
                return _salarys;
            }
        }

        public static object getMonthlySalarydetails(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                string[] listarry = param as string[];
                int mId = Convert.ToInt32(listarry[0]);
                int monthid = Convert.ToInt32(listarry[1]);
                int yearId = Convert.ToInt32(listarry[2]);
                var query = (from list in context.membersMonthlySalaryDetails
                             where list.membersid == mId && list.month == monthid && list.year == yearId
                             select list).FirstOrDefault();

                return query;
            }
        }

        public static object checkHeadList(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int catID = Int32.Parse(param.ToString());
                var query = (from check in context.membersHead
                             where check.membersCatId == catID
                             select check).Count();

                return query;
            }
        }

        public static object getSalaryHeadListfrommembersalary(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int cadID = Convert.ToInt32(param);

                var headList = (from shead in context.salaryheads
                                where shead.Status == true
                                && shead.LoanID == null
                                orderby shead.hType, shead.orderNo
                                select shead).ToList();

                List<salaryheads> result = new List<salaryheads>();
                foreach (var m in headList)
                {
                    salaryheads s = new salaryheads();
                    var list = (from _list in context.membersHead
                                where _list.sHeadId == m.sHeadId && _list.membersCatId == cadID
                                select _list).FirstOrDefault();
                    s.amount = 0;
                    s.Fixed = m.Fixed;
                    s.Status = m.Status;
                    if (list != null)
                    {
                        s.amount = list.amount;
                        s.Fixed = list.Fixed;
                        s.Status = list.status;
                    }

                    s.hType = m.hType;
                    s.membersCatID = cadID;
                    s.sHeadId = m.sHeadId;

                    s.sHeadName = m.sHeadName;
                    s.amountType = m.amountType;
                    result.Add(s);
                }
                return result;
            }
        }

        //public static object updateSalaryBills(object param)
        //{
        //    try
        //    {
        //        salarystructure _salarystructure = param as salarystructure;
        //        using (salaryheadcontext context = new salaryheadcontext())
        //        {
        //            foreach (HeadList sheads in _salarystructure.headList)
        //            {
        //                var membersalary = (from list in context.membersSalary
        //                                    where list.membersId == _salarystructure.memberCode
        //                                    && list.monthID == _salarystructure.monthID
        //                                    && list.Year == _salarystructure.yearId
        //                                    && list.headId == sheads.headID
        //                                    select list).FirstOrDefault();
        //                membersalary.amount = sheads.amount;

        //                context.Entry(membersalary).State = EntityState.Modified;
        //                context.SaveChanges();
        //            }

        //            var membersalarydetails = (from list in context.membersMonthlySalaryDetails
        //                                       where list.membersid == _salarystructure.memberCode
        //                                       && list.month == _salarystructure.monthID
        //                                       && list.year == _salarystructure.yearId
        //                                       select list).FirstOrDefault();
        //            membersalarydetails.totalAllowances = _salarystructure.totalAllowances;
        //            membersalarydetails.totalAGDeductionsA = _salarystructure.totalDeduction;
        //            membersalarydetails.grandtotal = _salarystructure.netSalary;

        //            context.Entry(membersalarydetails).State = EntityState.Modified;
        //            context.SaveChanges();
        //            context.Close();
        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public static object deleteSalaryBills(object param)
        //{
        //    try
        //    {
        //        salarystructure _salarystructure = param as salarystructure;
        //        using (salaryheadcontext context = new salaryheadcontext())
        //        {
        //            foreach (HeadList sheads in _salarystructure.headList)
        //            {
        //                var membersalary = (from list in context.membersSalary
        //                                    where list.membersId == _salarystructure.memberCode
        //                                    && list.monthID == _salarystructure.monthID
        //                                    && list.Year == _salarystructure.yearId
        //                                    && list.headId == sheads.headID
        //                                    select list).FirstOrDefault();
        //                membersalary.amount = sheads.amount;

        //                context.Entry(membersalary).State = EntityState.Deleted;
        //                context.SaveChanges();
        //            }

        //            var membersalarydetails = (from list in context.membersMonthlySalaryDetails
        //                                       where list.membersid == _salarystructure.memberCode
        //                                       && list.month == _salarystructure.monthID
        //                                       && list.year == _salarystructure.yearId
        //                                       select list).FirstOrDefault();
        //            membersalarydetails.totalAllowances = _salarystructure.totalAllowances;
        //            membersalarydetails.totalAGDeductionsA = _salarystructure.totalDeduction;
        //            membersalarydetails.grandtotal = _salarystructure.netSalary;

        //            context.Entry(membersalarydetails).State = EntityState.Deleted;
        //            context.SaveChanges();
        //            context.Close();
        //            return null;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        public static object updateSalaryBills(object param)
        {
            try
            {
                salarystructure _salarystructure = param as salarystructure;
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    foreach (HeadList sheads in _salarystructure.headList)
                    {
                        var membersalary = (from list in context.membersSalary
                                            where list.membersId == _salarystructure.memberCode
                                            && list.monthID == _salarystructure.monthID
                                            && list.Year == _salarystructure.yearId
                                            && list.headId == sheads.headID
                                            && list.ispart == _salarystructure.ispart
                                            && list.AssemblyId == _salarystructure.AssemblyId
                                            select list).FirstOrDefault();
                        membersalary.amount = sheads.amount;

                        context.Entry(membersalary).State = EntityState.Modified;
                        context.SaveChanges();
                    }

                    var membersalarydetails = (from list in context.membersMonthlySalaryDetails
                                               where list.membersid == _salarystructure.memberCode
                                               && list.month == _salarystructure.monthID
                                               && list.year == _salarystructure.yearId
                                               && list.Ispart == _salarystructure.ispart
                                               && list.AssemblyId == _salarystructure.AssemblyId
                                               select list).FirstOrDefault();
                    membersalarydetails.totalAllowances = _salarystructure.totalAllowances;
                    membersalarydetails.totalAGDeductionsA = _salarystructure.totalDeduction;
                    membersalarydetails.grandtotal = _salarystructure.netSalary;

                    context.Entry(membersalarydetails).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static object deleteSalaryBills(object param)
        {
            try
            {
                salarystructure _salarystructure = param as salarystructure;
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    foreach (HeadList sheads in _salarystructure.headList)
                    {
                        var membersalary = (from list in context.membersSalary
                                            where list.membersId == _salarystructure.memberCode
                                            && list.monthID == _salarystructure.monthID
                                            && list.Year == _salarystructure.yearId
                                            && list.headId == sheads.headID
                                             && list.ispart == _salarystructure.ispart
                                            && list.AssemblyId == _salarystructure.AssemblyId
                                            select list).FirstOrDefault();
                        membersalary.amount = sheads.amount;

                        context.Entry(membersalary).State = EntityState.Deleted;
                        context.SaveChanges();
                    }

                    var membersalarydetails = (from list in context.membersMonthlySalaryDetails
                                               where list.membersid == _salarystructure.memberCode
                                               && list.month == _salarystructure.monthID
                                               && list.year == _salarystructure.yearId
                                                && list.Ispart == _salarystructure.ispart
                                            && list.AssemblyId == _salarystructure.AssemblyId
                                               select list).FirstOrDefault();
                    membersalarydetails.totalAllowances = _salarystructure.totalAllowances;
                    membersalarydetails.totalAGDeductionsA = _salarystructure.totalDeduction;
                    membersalarydetails.grandtotal = _salarystructure.netSalary;

                    context.Entry(membersalarydetails).State = EntityState.Deleted;
                    context.SaveChanges();
                    context.Close();
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #region outer bill

        public static object updateBillFilePath(object param)
        {
            string[] arr = param as string[];
            int autoid = Convert.ToInt32(arr[0]);
            string path = arr[1];
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var result = (from list in context.membersMonthlySalaryDetails
                              where list.autoid == autoid
                              select list).FirstOrDefault();
                result.filePath = path;
                result.GenerateStatus = 1;
                result.GeneratedDate = DateTime.Now;
                context.Entry(result).State = EntityState.Modified;
                context.SaveChanges();
                context.Close();
                return null;
            }
        }

        public static object getBillFilePath(object param)
        {
            int autoId = Convert.ToInt32(param);
            using (salaryheadcontext context = new salaryheadcontext())
            {
                string filepath = (from list in context.membersMonthlySalaryDetails
                                   where list.autoid == autoId
                                   select list.filePath).FirstOrDefault();
                return filepath;
            }
        }

        //public static object generateOuterBill(object param)
        //{
        //    using (salaryheadcontext context = new salaryheadcontext())
        //    {
        //        int[] listarry = param as int[];
        //        int DesignationID = Convert.ToInt32(listarry[0]);
        //        int monthid = listarry[1];
        //        int yearId = listarry[2];

        //        int assemblyId = listarry[3];
        //        int ispart = listarry[4];
        //        // List<membersSalary> monthlySalary = null;

        //        var CurrentFinancialYear = (from m in context.mSiteSettings
        //                                    where m.SettingName == "CurrentFinancialYear"
        //                                    select m.SettingValue).FirstOrDefault();

        //        var monthlySalary = (from a in context.membersSalary
        //                             join b in context.membersHead on new { HeadId = a.headId } equals new { HeadId = b.sHeadId }
        //                             join c in context.salaryheads on b.sHeadId equals c.sHeadId
        //                             where
        //                               a.catID == DesignationID &&
        //                               b.membersCatId == DesignationID &&
        //                               a.monthID == monthid &&
        //                               a.Year == yearId &&
        //                               a.AssemblyId == assemblyId &&
        //                               a.ispart == ispart
        //                             group new { a, c } by new
        //                             {
        //                                 a.headId,
        //                                 c.sHeadName,
        //                                 c.hType,
        //                                 c.hsubType,
        //                                 c.orderNo
        //                             } into g
        //                             orderby
        //                               g.Key.hType,
        //                               g.Key.orderNo
        //                             select new
        //                             {
        //                                 g.Key.sHeadName,
        //                                 g.Key.hType,
        //                                 g.Key.hsubType,
        //                                 amount = (double)g.Sum(p => p.a.amount)
        //                             }).ToList();
        //        var loanded = (from sheads in context.salaryheads
        //                       join msalary in context.membersSalary on new { SHeadId = sheads.sHeadId } equals new { SHeadId = msalary.headId } into msalary_join
        //                       from msalary in msalary_join.DefaultIfEmpty()
        //                       where
        //                         msalary.catID == DesignationID &&
        //                         sheads.LoanID != null &&
        //                         msalary.monthID == monthid &&
        //                         msalary.Year == yearId &&
        //                         msalary.AssemblyId == assemblyId &&
        //                         msalary.ispart == ispart
        //                       group new { sheads, msalary } by new
        //                       {
        //                           sheads.sHeadId,
        //                           sheads.sHeadName,
        //                           sheads.hType,
        //                           sheads.hsubType,
        //                           sheads.orderNo
        //                       } into g
        //                       orderby
        //                         g.Key.hType,
        //                         g.Key.orderNo
        //                       select new
        //                       {
        //                           g.Key.sHeadName,
        //                           g.Key.hType,
        //                           g.Key.hsubType,
        //                           amount = (double)g.Sum(p => p.msalary.amount)
        //                       }).ToList();

        //        MemberOuterBill _MemberOuterBill = new MemberOuterBill();
        //        _MemberOuterBill.billDate = DateTime.Now;
        //        //_MemberOuterBill.billNO = (from lastbill in context.mEstablishBill
        //        //                           orderby lastbill.BillNumber descending
        //        //                           select lastbill.BillNumber).FirstOrDefault() + 1;

        //        _MemberOuterBill.monthID = monthid;
        //        _MemberOuterBill.yearID = yearId;
        //        _MemberOuterBill.desginationID = DesignationID;


        //        int aid = assemblyId;

        //        if (DesignationID == 1)
        //        {

        //            //_MemberOuterBill.designationName = "Speaker";
        //            //_MemberOuterBill.memberName = (from member in context.mMember
        //            //                               join massembly in context.mMemberAssembly
        //            //                               on member.MemberCode equals massembly.MemberID
        //            //                               where massembly.DesignationID == 1 && massembly.AssemblyID == aid
        //            //                               orderby massembly.DesignationID
        //            //                               select member.Name).FirstOrDefault();
        //            //_MemberOuterBill.memberID = (from member in context.mMember
        //            //                             join massembly in context.mMemberAssembly
        //            //                             on member.MemberCode equals massembly.MemberID
        //            //                             where massembly.DesignationID == 1 && massembly.AssemblyID == aid
        //            //                             orderby massembly.DesignationID
        //            //                             select member.MemberCode).FirstOrDefault();
        //            //_MemberOuterBill.BudgetId = (from budget in context.mBudget
        //            //                             where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
        //            //                             select budget.BudgetID).FirstOrDefault();



        //            var q = (from mem in context.mSiteSettings
        //                     where mem.SettingName == "ProtemSpeaker"
        //                     select mem.SettingValue).FirstOrDefault();
                   
        //            if (q != null)
        //            {
        //                 int id = Convert.ToInt32(q);
        //                _MemberOuterBill.designationName = "ProtemSpeaker";
        //                _MemberOuterBill.memberName = (from member in context.mMember
        //                                               join massembly in context.mMemberAssembly
        //                                               on member.MemberCode equals id
        //                                               where massembly.AssemblyID == aid
        //                                               orderby massembly.DesignationID
        //                                               select member.Name).FirstOrDefault();
        //                _MemberOuterBill.memberID = id;
        //                _MemberOuterBill.BudgetId = (from budget in context.mBudget
        //                                             where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
        //                                             select budget.BudgetID).FirstOrDefault();
        //            }
        //            else
        //            {
        //                _MemberOuterBill.designationName = "Speaker";
        //                _MemberOuterBill.memberName = (from member in context.mMember
        //                                               join massembly in context.mMemberAssembly
        //                                               on member.MemberCode equals massembly.MemberID
        //                                               where massembly.DesignationID == 1 && massembly.AssemblyID == aid
        //                                               orderby massembly.DesignationID
        //                                               select member.Name).FirstOrDefault();
        //                _MemberOuterBill.memberID = (from member in context.mMember
        //                                             join massembly in context.mMemberAssembly
        //                                             on member.MemberCode equals massembly.MemberID
        //                                             where massembly.DesignationID == 1 && massembly.AssemblyID == aid
        //                                             orderby massembly.DesignationID
        //                                             select member.MemberCode).FirstOrDefault();
        //                _MemberOuterBill.BudgetId = (from budget in context.mBudget
        //                                             where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
        //                                             select budget.BudgetID).FirstOrDefault();

        //            }

        //        }
        //        if (DesignationID == 2)
        //        {
        //            _MemberOuterBill.designationName = "Deputy Speaker";
        //            _MemberOuterBill.memberName = (from member in context.mMember
        //                                           join massembly in context.mMemberAssembly
        //                                           on member.MemberCode equals massembly.MemberID
        //                                           where massembly.DesignationID == 14 && massembly.AssemblyID == aid
        //                                           orderby massembly.DesignationID
        //                                           select member.Name).FirstOrDefault();
        //            _MemberOuterBill.memberID = (from member in context.mMember
        //                                         join massembly in context.mMemberAssembly
        //                                         on member.MemberCode equals massembly.MemberID
        //                                         where massembly.DesignationID == 14 && massembly.AssemblyID == aid
        //                                         orderby massembly.DesignationID
        //                                         select member.MemberCode).FirstOrDefault();
        //            _MemberOuterBill.BudgetId = (from budget in context.mBudget
        //                                         where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
        //                                         select budget.BudgetID).FirstOrDefault();
        //        }
        //        _MemberOuterBill.monthName = new DateTime(1900, monthid, 1).ToString("MMMM");
        //        double totalgross = 0.00;
        //        double totalA = 0.00;
        //        double totalB = 0.00;
        //        List<HeadList> _hlist = new List<HeadList>();
        //        foreach (var item in monthlySalary)
        //        {
        //            HeadList head = new HeadList();
        //            head.headName = item.sHeadName;
        //            head.amount = item.amount;
        //            head.htype = item.hType;
        //            head.subType = item.hsubType;

        //            if (item.hType == "cr")
        //            {
        //                totalgross += item.amount;
        //            }
        //            else
        //            {
        //                if (item.hsubType == "a")
        //                {
        //                    totalA += item.amount;
        //                }
        //                else
        //                {
        //                    totalB += item.amount;
        //                }
        //            }
        //            _hlist.Add(head);
        //        }
        //        foreach (var item in loanded)
        //        {
        //            HeadList head = new HeadList();
        //            head.headName = item.sHeadName;
        //            head.amount = item.amount;
        //            head.htype = item.hType;
        //            head.subType = item.hsubType;

        //            if (item.hType == "cr")
        //            {
        //                totalgross += item.amount;
        //            }
        //            else
        //            {
        //                if (item.hsubType == "a")
        //                {
        //                    totalA += item.amount;
        //                }
        //                else
        //                {
        //                    totalB += item.amount;
        //                }
        //            }
        //            _hlist.Add(head);
        //        }
        //        _MemberOuterBill.BudgetId = (from budget in context.mBudget
        //                                     where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "Member"
        //                                     select budget.BudgetID).FirstOrDefault();
        //        _MemberOuterBill.grossTotal = totalgross;
        //        _MemberOuterBill.total_a = totalA;
        //        _MemberOuterBill.total_b = totalB;

        //        _MemberOuterBill.headList = _hlist;
        //        return _MemberOuterBill;
        //    }
        //}
        public static object generateOuterBill(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int[] listarry = param as int[];
                int DesignationID = Convert.ToInt32(listarry[0]);
                int monthid = listarry[1];
                int yearId = listarry[2];

                int assemblyId = listarry[3];
                int ispart = listarry[4];
                // List<membersSalary> monthlySalary = null;

                var CurrentFinancialYear = (from m in context.mSiteSettings
                                            where m.SettingName == "CurrentFinancialYear"
                                            select m.SettingValue).FirstOrDefault();


                var q = (from mem in context.mSiteSettings
                         where mem.SettingName == "ProtemSpeaker"
                         select mem.SettingValue).FirstOrDefault();

                if (q != null)
                {
                    int id = Convert.ToInt32(q);
                    var monthlySalary = (from a in context.membersSalary
                                         join b in context.membersHead on new { HeadId = a.headId } equals new { HeadId = b.sHeadId }
                                         join c in context.salaryheads on b.sHeadId equals c.sHeadId
                                         where
                                           a.catID == DesignationID &&
                                           b.membersCatId == DesignationID &&
                                           a.monthID == monthid &&
                                           a.Year == yearId &&
                                           a.AssemblyId == assemblyId &&
                                           a.ispart == ispart &&
                                           a.membersId == id
                                         group new { a, c } by new
                                         {
                                             a.headId,
                                             c.sHeadName,
                                             c.hType,
                                             c.hsubType,
                                             c.orderNo
                                         } into g
                                         orderby
                                           g.Key.hType,
                                           g.Key.orderNo
                                         select new
                                         {
                                             g.Key.sHeadName,
                                             g.Key.hType,
                                             g.Key.hsubType,
                                             amount = (double)g.Sum(p => p.a.amount)
                                         }).ToList();
                    var loanded = (from sheads in context.salaryheads
                                   join msalary in context.membersSalary on new { SHeadId = sheads.sHeadId } equals new { SHeadId = msalary.headId } into msalary_join
                                   from msalary in msalary_join.DefaultIfEmpty()
                                   where
                                     msalary.catID == DesignationID &&
                                     sheads.LoanID != null &&
                                     msalary.monthID == monthid &&
                                     msalary.Year == yearId &&
                                     msalary.AssemblyId == assemblyId &&
                                     msalary.ispart == ispart &&
                                     msalary.membersId == id
                                   group new { sheads, msalary } by new
                                   {
                                       sheads.sHeadId,
                                       sheads.sHeadName,
                                       sheads.hType,
                                       sheads.hsubType,
                                       sheads.orderNo
                                   } into g
                                   orderby
                                     g.Key.hType,
                                     g.Key.orderNo
                                   select new
                                   {
                                       g.Key.sHeadName,
                                       g.Key.hType,
                                       g.Key.hsubType,
                                       amount = (double)g.Sum(p => p.msalary.amount)
                                   }).ToList();

                    MemberOuterBill _MemberOuterBill = new MemberOuterBill();
                    _MemberOuterBill.billDate = DateTime.Now;
                    //_MemberOuterBill.billNO = (from lastbill in context.mEstablishBill
                    //                           orderby lastbill.BillNumber descending
                    //                           select lastbill.BillNumber).FirstOrDefault() + 1;

                    _MemberOuterBill.monthID = monthid;
                    _MemberOuterBill.yearID = yearId;
                    _MemberOuterBill.desginationID = DesignationID;


                    int aid = assemblyId;

                    if (DesignationID == 1)
                    {

                        //_MemberOuterBill.designationName = "Speaker";
                        //_MemberOuterBill.memberName = (from member in context.mMember
                        //                               join massembly in context.mMemberAssembly
                        //                               on member.MemberCode equals massembly.MemberID
                        //                               where massembly.DesignationID == 1 && massembly.AssemblyID == aid
                        //                               orderby massembly.DesignationID
                        //                               select member.Name).FirstOrDefault();
                        //_MemberOuterBill.memberID = (from member in context.mMember
                        //                             join massembly in context.mMemberAssembly
                        //                             on member.MemberCode equals massembly.MemberID
                        //                             where massembly.DesignationID == 1 && massembly.AssemblyID == aid
                        //                             orderby massembly.DesignationID
                        //                             select member.MemberCode).FirstOrDefault();
                        //_MemberOuterBill.BudgetId = (from budget in context.mBudget
                        //                             where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                        //                             select budget.BudgetID).FirstOrDefault();



                        //var q = (from mem in context.mSiteSettings
                        //         where mem.SettingName == "ProtemSpeaker"
                        //         select mem.SettingValue).FirstOrDefault();

                        if (q != null)
                        {
                            int ida = Convert.ToInt32(q);
                            _MemberOuterBill.designationName = "ProtemSpeaker";
                            _MemberOuterBill.memberName = (from member in context.mMember
                                                           join massembly in context.mMemberAssembly
                                                           on member.MemberCode equals ida
                                                           where massembly.AssemblyID == aid
                                                           orderby massembly.DesignationID
                                                           select member.Name).FirstOrDefault();
                            _MemberOuterBill.memberID = ida;
                            _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                         where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                                                         select budget.BudgetID).FirstOrDefault();
                        }
                        else
                        {
                            _MemberOuterBill.designationName = "Speaker";
                            _MemberOuterBill.memberName = (from member in context.mMember
                                                           join massembly in context.mMemberAssembly
                                                           on member.MemberCode equals massembly.MemberID
                                                           where massembly.DesignationID == 1 && massembly.AssemblyID == aid
                                                           orderby massembly.DesignationID
                                                           select member.Name).FirstOrDefault();
                            _MemberOuterBill.memberID = (from member in context.mMember
                                                         join massembly in context.mMemberAssembly
                                                         on member.MemberCode equals massembly.MemberID
                                                         where massembly.DesignationID == 1 && massembly.AssemblyID == aid
                                                         orderby massembly.DesignationID
                                                         select member.MemberCode).FirstOrDefault();
                            _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                         where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                                                         select budget.BudgetID).FirstOrDefault();

                        }

                    }
                    if (DesignationID == 2)
                    {
                        _MemberOuterBill.designationName = "Deputy Speaker";
                        _MemberOuterBill.memberName = (from member in context.mMember
                                                       join massembly in context.mMemberAssembly
                                                       on member.MemberCode equals massembly.MemberID
                                                       where massembly.DesignationID == 14 && massembly.AssemblyID == aid
                                                       orderby massembly.DesignationID
                                                       select member.Name).FirstOrDefault();
                        _MemberOuterBill.memberID = (from member in context.mMember
                                                     join massembly in context.mMemberAssembly
                                                     on member.MemberCode equals massembly.MemberID
                                                     where massembly.DesignationID == 14 && massembly.AssemblyID == aid
                                                     orderby massembly.DesignationID
                                                     select member.MemberCode).FirstOrDefault();
                        _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                     where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                                                     select budget.BudgetID).FirstOrDefault();
                    }
                    _MemberOuterBill.monthName = new DateTime(1900, monthid, 1).ToString("MMMM");
                    double totalgross = 0.00;
                    double totalA = 0.00;
                    double totalB = 0.00;
                    List<HeadList> _hlist = new List<HeadList>();
                    foreach (var item in monthlySalary)
                    {
                        HeadList head = new HeadList();
                        head.headName = item.sHeadName;
                        head.amount = item.amount;
                        head.htype = item.hType;
                        head.subType = item.hsubType;

                        if (item.hType == "cr")
                        {
                            totalgross += item.amount;
                        }
                        else
                        {
                            if (item.hsubType == "a")
                            {
                                totalA += item.amount;
                            }
                            else
                            {
                                totalB += item.amount;
                            }
                        }
                        _hlist.Add(head);
                    }
                    foreach (var item in loanded)
                    {
                        HeadList head = new HeadList();
                        head.headName = item.sHeadName;
                        head.amount = item.amount;
                        head.htype = item.hType;
                        head.subType = item.hsubType;

                        if (item.hType == "cr")
                        {
                            totalgross += item.amount;
                        }
                        else
                        {
                            if (item.hsubType == "a")
                            {
                                totalA += item.amount;
                            }
                            else
                            {
                                totalB += item.amount;
                            }
                        }
                        _hlist.Add(head);
                    }
                    _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                 where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "Member"
                                                 select budget.BudgetID).FirstOrDefault();
                    _MemberOuterBill.grossTotal = totalgross;
                    _MemberOuterBill.total_a = totalA;
                    _MemberOuterBill.total_b = totalB;

                    _MemberOuterBill.headList = _hlist;
                    return _MemberOuterBill;
                }
                else
                {

                    var monthlySalary = (from a in context.membersSalary
                                         join b in context.membersHead on new { HeadId = a.headId } equals new { HeadId = b.sHeadId }
                                         join c in context.salaryheads on b.sHeadId equals c.sHeadId
                                         where
                                           a.catID == DesignationID &&
                                           b.membersCatId == DesignationID &&
                                           a.monthID == monthid &&
                                           a.Year == yearId &&
                                           a.AssemblyId == assemblyId &&
                                           a.ispart == ispart
                                         group new { a, c } by new
                                         {
                                             a.headId,
                                             c.sHeadName,
                                             c.hType,
                                             c.hsubType,
                                             c.orderNo
                                         } into g
                                         orderby
                                           g.Key.hType,
                                           g.Key.orderNo
                                         select new
                                         {
                                             g.Key.sHeadName,
                                             g.Key.hType,
                                             g.Key.hsubType,
                                             amount = (double)g.Sum(p => p.a.amount)
                                         }).ToList();
                    var loanded = (from sheads in context.salaryheads
                                   join msalary in context.membersSalary on new { SHeadId = sheads.sHeadId } equals new { SHeadId = msalary.headId } into msalary_join
                                   from msalary in msalary_join.DefaultIfEmpty()
                                   where
                                     msalary.catID == DesignationID &&
                                     sheads.LoanID != null &&
                                     msalary.monthID == monthid &&
                                     msalary.Year == yearId &&
                                     msalary.AssemblyId == assemblyId &&
                                     msalary.ispart == ispart
                                   group new { sheads, msalary } by new
                                   {
                                       sheads.sHeadId,
                                       sheads.sHeadName,
                                       sheads.hType,
                                       sheads.hsubType,
                                       sheads.orderNo
                                   } into g
                                   orderby
                                     g.Key.hType,
                                     g.Key.orderNo
                                   select new
                                   {
                                       g.Key.sHeadName,
                                       g.Key.hType,
                                       g.Key.hsubType,
                                       amount = (double)g.Sum(p => p.msalary.amount)
                                   }).ToList();

                    MemberOuterBill _MemberOuterBill = new MemberOuterBill();
                    _MemberOuterBill.billDate = DateTime.Now;
                    //_MemberOuterBill.billNO = (from lastbill in context.mEstablishBill
                    //                           orderby lastbill.BillNumber descending
                    //                           select lastbill.BillNumber).FirstOrDefault() + 1;

                    _MemberOuterBill.monthID = monthid;
                    _MemberOuterBill.yearID = yearId;
                    _MemberOuterBill.desginationID = DesignationID;


                    int aid = assemblyId;

                    if (DesignationID == 1)
                    {

                        //_MemberOuterBill.designationName = "Speaker";
                        //_MemberOuterBill.memberName = (from member in context.mMember
                        //                               join massembly in context.mMemberAssembly
                        //                               on member.MemberCode equals massembly.MemberID
                        //                               where massembly.DesignationID == 1 && massembly.AssemblyID == aid
                        //                               orderby massembly.DesignationID
                        //                               select member.Name).FirstOrDefault();
                        //_MemberOuterBill.memberID = (from member in context.mMember
                        //                             join massembly in context.mMemberAssembly
                        //                             on member.MemberCode equals massembly.MemberID
                        //                             where massembly.DesignationID == 1 && massembly.AssemblyID == aid
                        //                             orderby massembly.DesignationID
                        //                             select member.MemberCode).FirstOrDefault();
                        //_MemberOuterBill.BudgetId = (from budget in context.mBudget
                        //                             where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                        //                             select budget.BudgetID).FirstOrDefault();



                        //var q = (from mem in context.mSiteSettings
                        //         where mem.SettingName == "ProtemSpeaker"
                        //         select mem.SettingValue).FirstOrDefault();

                        if (q != null)
                        {
                            int id = Convert.ToInt32(q);
                            _MemberOuterBill.designationName = "ProtemSpeaker";
                            _MemberOuterBill.memberName = (from member in context.mMember
                                                           join massembly in context.mMemberAssembly
                                                           on member.MemberCode equals id
                                                           where massembly.AssemblyID == aid
                                                           orderby massembly.DesignationID
                                                           select member.Name).FirstOrDefault();
                            _MemberOuterBill.memberID = id;
                            _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                         where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                                                         select budget.BudgetID).FirstOrDefault();
                        }
                        else
                        {
                            _MemberOuterBill.designationName = "Speaker";
                            _MemberOuterBill.memberName = (from member in context.mMember
                                                           join massembly in context.mMemberAssembly
                                                           on member.MemberCode equals massembly.MemberID
                                                           where massembly.DesignationID == 1 && massembly.AssemblyID == aid
                                                           orderby massembly.DesignationID
                                                           select member.Name).FirstOrDefault();
                            _MemberOuterBill.memberID = (from member in context.mMember
                                                         join massembly in context.mMemberAssembly
                                                         on member.MemberCode equals massembly.MemberID
                                                         where massembly.DesignationID == 1 && massembly.AssemblyID == aid
                                                         orderby massembly.DesignationID
                                                         select member.MemberCode).FirstOrDefault();
                            _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                         where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                                                         select budget.BudgetID).FirstOrDefault();

                        }

                    }
                    if (DesignationID == 2)
                    {
                        _MemberOuterBill.designationName = "Deputy Speaker";
                        _MemberOuterBill.memberName = (from member in context.mMember
                                                       join massembly in context.mMemberAssembly
                                                       on member.MemberCode equals massembly.MemberID
                                                       where massembly.DesignationID == 14 && massembly.AssemblyID == aid
                                                       orderby massembly.DesignationID
                                                       select member.Name).FirstOrDefault();
                        _MemberOuterBill.memberID = (from member in context.mMember
                                                     join massembly in context.mMemberAssembly
                                                     on member.MemberCode equals massembly.MemberID
                                                     where massembly.DesignationID == 14 && massembly.AssemblyID == aid
                                                     orderby massembly.DesignationID
                                                     select member.MemberCode).FirstOrDefault();
                        _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                     where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                                                     select budget.BudgetID).FirstOrDefault();
                    }
                    if (DesignationID == 4)
                    {
                        _MemberOuterBill.designationName = "Chief Whip";
                        _MemberOuterBill.memberName = (from member in context.mMember
                                                       join massembly in context.mMemberAssembly
                                                       on member.MemberCode equals massembly.MemberID
                                                       where massembly.DesignationID == 58 && massembly.AssemblyID == aid
                                                       orderby massembly.DesignationID
                                                       select member.Name).FirstOrDefault();
                        _MemberOuterBill.memberID = (from member in context.mMember
                                                     join massembly in context.mMemberAssembly
                                                     on member.MemberCode equals massembly.MemberID
                                                     where massembly.DesignationID == 58 && massembly.AssemblyID == aid
                                                     orderby massembly.DesignationID
                                                     select member.MemberCode).FirstOrDefault();
                        _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                     where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "SPK/DSPK"
                                                     select budget.BudgetID).FirstOrDefault();
                    }
                    _MemberOuterBill.monthName = new DateTime(1900, monthid, 1).ToString("MMMM");
                    double totalgross = 0.00;
                    double totalA = 0.00;
                    double totalB = 0.00;
                    List<HeadList> _hlist = new List<HeadList>();
                    foreach (var item in monthlySalary)
                    {
                        HeadList head = new HeadList();
                        head.headName = item.sHeadName;
                        head.amount = item.amount;
                        head.htype = item.hType;
                        head.subType = item.hsubType;

                        if (item.hType == "cr")
                        {
                            totalgross += item.amount;
                        }
                        else
                        {
                            if (item.hsubType == "a")
                            {
                                totalA += item.amount;
                            }
                            else
                            {
                                totalB += item.amount;
                            }
                        }
                        _hlist.Add(head);
                    }
                    foreach (var item in loanded)
                    {
                        HeadList head = new HeadList();
                        head.headName = item.sHeadName;
                        head.amount = item.amount;
                        head.htype = item.hType;
                        head.subType = item.hsubType;

                        if (item.hType == "cr")
                        {
                            totalgross += item.amount;
                        }
                        else
                        {
                            if (item.hsubType == "a")
                            {
                                totalA += item.amount;
                            }
                            else
                            {
                                totalB += item.amount;
                            }
                        }
                        _hlist.Add(head);
                    }

                   
                   

                    if (DesignationID == 3)
                    {
                        _MemberOuterBill.BudgetId = (from budget in context.mBudget
                                                     where budget.FinancialYear == CurrentFinancialYear && budget.BudgetType == 7 && budget.Gazetted == "Member"
                                                     select budget.BudgetID).FirstOrDefault();
                        _MemberOuterBill.total_a = totalA;
                        _MemberOuterBill.total_b = totalB;  
                    }
                    else
                    {
                        _MemberOuterBill.total_a = totalA + totalB;
                        //_MemberOuterBill.total_b = totalB;
                    }
                    _MemberOuterBill.grossTotal = totalgross;
                    _MemberOuterBill.headList = _hlist;
                    _MemberOuterBill.Financialyear = CurrentFinancialYear;
                    return _MemberOuterBill;
                }


            }
        }

        public static object generateOuterBillForMembers(object param)
        {
            int[] listarry = param as int[];
            int designationID = Convert.ToInt32(listarry[0]);
            int monthid = listarry[1];
            int yearId = listarry[2];
            int assemblyid = listarry[3];
            int ispart = listarry[4];
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int srno = 1;
                double grossTotal = 0;
                double AllgrossTotal = 0;
                double totalDed = 0;
                double AlltotalDed = 0;

                List<membersOuterReport> _membersOuterReport = new List<membersOuterReport>();

                List<string> _colList = new List<string>();
                _colList.Add("SrNo");
                _colList.Add("Member's Name");
                var _allowList = (from list in context.membersHead
                                  join slist in context.salaryheads
                                  on list.sHeadId equals slist.sHeadId
                                  where list.membersCatId == 3 && list.status == true && list.htype == "cr"
                                  orderby slist.orderNo
                                  select slist.sHeadName
                                    ).ToList();
                _colList.AddRange(_allowList);
                _colList.Add("Gross Salary");
                var _dedList = (from list in context.membersHead
                                join slist in context.salaryheads
                                on list.sHeadId equals slist.sHeadId
                                where list.membersCatId == 3 && list.status == true && list.htype == "dr"
                                orderby slist.orderNo
                                select slist.sHeadName
                                   ).ToList();
                var _loanList = (from list in context.salaryheads
                                 where list.LoanID != null
                                 orderby list.orderNo
                                 select list.sHeadName
                                 ).ToList();
                _colList.AddRange(_dedList);
                _colList.AddRange(_loanList);
                _colList.Add("Total Ded");
                _colList.Add("Net Salary");
                membersOuterReport _tableheader = new membersOuterReport();
                _tableheader.columnName = _colList;
                _membersOuterReport.Add(_tableheader);
                _tableheader.monthID = monthid;
                _tableheader.yearID = yearId;
                _tableheader.catID = designationID;
                _tableheader.ispart = ispart;
                List<mMember> _mMemberList = (List<mMember>)getallmembers(assemblyid);
                foreach (var item in _mMemberList)
                {
                    List<string> _colListforMember = new List<string>();

                    var _allowListforMember = (from list in context.membersSalary
                                               join slist in context.salaryheads
                                               on list.headId equals slist.sHeadId
                                               where list.catID == 3 && list.membersId == item.MemberCode
                                               && list.monthID == monthid && list.Year == yearId
                                               && slist.hType == "cr" && list.AssemblyId == assemblyid
                                               && list.ispart == ispart
                                               orderby slist.orderNo
                                               select new { list.amount, list.membersId }
                                        ).ToList();

                    _colListforMember.Add(srno.ToString());
                    _colListforMember.Add((string)getMemberName(item.MemberCode));
                    if (_allowListforMember.Count() > 0)
                    {
                        srno++;
                        foreach (var critem in _allowListforMember)
                        {
                            _colListforMember.Add(critem.amount.ToString());

                            grossTotal += critem.amount;
                        }
                        _colListforMember.Add(grossTotal.ToString());
                        //var _dedListforMember = (from list in context.membersSalary
                        //                         join slist in context.salaryheads
                        //                         on list.headId equals slist.sHeadId
                        //                         where list.catID == designationID && list.membersId == item.MemberCode && list.monthID == monthid && list.Year == yearId && slist.hType == "dr"
                        //                         orderby slist.orderNo
                        //                         select new { list.amount, list.membersId }
                        //                   ).ToList();

                        var _dedListforMember = (from list in context.membersHead
                                                 join mlist in context.membersSalary
                                                 on list.sHeadId equals mlist.headId
                                                 join shead in context.salaryheads
                                                 on list.sHeadId equals shead.sHeadId
                                                 where list.membersCatId == designationID
                                                 && mlist.membersId == item.MemberCode
                                                 && mlist.monthID == monthid
                                                 && mlist.Year == yearId
                                                 && shead.hType == "dr"
                                                 && mlist.catID == 3
                                                 && mlist.AssemblyId == assemblyid && mlist.ispart == ispart

                                                 orderby shead.orderNo
                                                 select new { mlist.amount, mlist.membersId, shead.sHeadName }).ToList();
                        foreach (var dritem in _dedListforMember)
                        {
                            _colListforMember.Add(dritem.amount.ToString());

                            totalDed += dritem.amount;
                        }
                        var _dedloanListforMember = (from list in context.salaryheads
                                                     where list.LoanID != null
                                                     orderby list.orderNo
                                                     select list.sHeadId).ToList();

                        foreach (var loanitem in _dedloanListforMember)
                        {
                            var Llist = (from list in context.membersSalary
                                         join slist in context.salaryheads
                                                  on list.headId equals slist.sHeadId
                                         where list.catID == designationID && list.membersId == item.MemberCode
                                         && list.monthID == monthid && list.Year == yearId
                                         && slist.hType == "dr" && list.headId == loanitem
                                         && list.AssemblyId == assemblyid && list.ispart == ispart
                                         orderby slist.orderNo
                                         select new { list.amount, list.membersId }
                                           ).FirstOrDefault();
                            if (Llist != null)
                            {
                                _colListforMember.Add(Llist.amount.ToString());
                                totalDed += Llist.amount;
                            }
                            else
                            {
                                _colListforMember.Add("0");
                                //totalDed += Llist.amount;
                            }
                        }
                        _colListforMember.Add(totalDed.ToString());
                        _colListforMember.Add((grossTotal - totalDed).ToString());
                        membersOuterReport _tablerows = new membersOuterReport();
                        _tablerows.columnName = _colListforMember;

                        //footer

                        grossTotal = 0;
                        totalDed = 0;
                        _membersOuterReport.Add(_tablerows);
                    }
                }
                List<string> _fcolList = new List<string>();
                _fcolList.Add("");
                _fcolList.Add("Total");
                var _fallowList = (from msalary in context.membersSalary
                                   join shead in context.salaryheads on new { HeadId = msalary.headId } equals new { HeadId = shead.sHeadId }
                                   where
                                    msalary.monthID == monthid &&
                                    msalary.Year == yearId &&
                                    msalary.headtype == "cr"
                                    && msalary.catID == 3
                                   && msalary.AssemblyId == assemblyid
                                   && msalary.ispart == ispart
                                   group new { msalary, shead } by new
                                   {
                                       msalary.headId,
                                       shead.orderNo
                                   } into g
                                   orderby
                                    g.Key.orderNo
                                   select new
                                   {
                                       Column1 = (double?)g.Sum(p => p.msalary.amount)
                                   }).ToList();
                foreach (var critem in _fallowList)
                {
                    _fcolList.Add(critem.Column1.ToString());

                    AllgrossTotal += Convert.ToDouble(critem.Column1);
                }

                _fcolList.Add(AllgrossTotal.ToString());
                var _fdedList = (from shead in context.salaryheads
                                 join msalary in context.membersSalary on new { SHeadId = shead.sHeadId } equals new { SHeadId = msalary.headId }
                                 where
                                  shead.LoanID == null &&
                                  shead.hType == "dr"
                                 &&
                                 msalary.monthID == monthid
                                 &&
                                  msalary.Year == yearId
                                  &&
                                  msalary.catID == 3
                                  && msalary.AssemblyId == assemblyid
                                  && msalary.ispart == ispart
                                 group new { msalary, shead } by new
                                 {
                                     msalary.headId,
                                     shead.orderNo
                                 } into g
                                 orderby
                                  g.Key.orderNo
                                 select new
                                 {
                                     Column1 = (double?)g.Sum(p => p.msalary.amount)
                                 }).ToList();
                foreach (var dritem in _fdedList)
                {
                    _fcolList.Add(dritem.Column1.ToString());

                    AlltotalDed += Convert.ToDouble(dritem.Column1);
                }
                var _ldedList = (from list in context.salaryheads
                                 where list.LoanID != null
                                 orderby list.orderNo
                                 select list.sHeadId).ToList();

                foreach (var loanitem in _ldedList)
                {
                    var Llist = (from list in context.membersSalary
                                 join slist in context.salaryheads
                                          on list.headId equals slist.sHeadId
                                 where list.catID == designationID && list.monthID == monthid
                                 && list.Year == yearId && slist.hType == "dr"
                                 && list.headId == loanitem
                                 && list.AssemblyId == assemblyid
                                 && list.ispart == ispart
                                 orderby slist.orderNo
                                 select new { list.amount, list.membersId }
                                   );
                    if (Llist.ToList().Count() > 0)
                    {
                        _fcolList.Add(Llist.Sum(a => a.amount).ToString());
                        AlltotalDed += Llist.Sum(a => a.amount);
                    }
                    else
                    {
                        _fcolList.Add("0");
                        //totalDed += Llist.amount;
                    }
                }
                _fcolList.Add(AlltotalDed.ToString());

                // _fcolList.Add(totalDed.ToString());
                _fcolList.Add((AllgrossTotal - AlltotalDed).ToString());
                membersOuterReport _ftableheader = new membersOuterReport();
                _ftableheader.columnName = _fcolList;

                _membersOuterReport.Add(_ftableheader);

                return _membersOuterReport;
            }
        }

        public static object getHouseRentDeductionList(object param)
        {
            int[] listarry = param as int[];

            int DesignationID = Convert.ToInt32(listarry[0]);
            int deductionTypeID = Convert.ToInt32(listarry[1]);
            int monthid = listarry[2];
            int yearId = listarry[3];
            int assemblyid = listarry[4];
            int ispart = listarry[5];
            double total = 0;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var result = (from list in context.membersSalary
                              join member in context.mMember
                              on list.membersId equals member.MemberCode
                              where list.monthID == monthid && list.Year == yearId && list.catID == DesignationID && list.headId == deductionTypeID
                              && list.amount != 0
                              && list.AssemblyId == assemblyid
                              && list.ispart == ispart
                              select new HouseRentDed
                              {
                                  amount = list.amount,
                                  memberName = member.Name
                              }).ToList();
                foreach (var item in result)
                {
                    total += item.amount;
                }
                HouseRentDedReport _houseRent = new HouseRentDedReport();
                _houseRent.houserentDedList = result;
                _houseRent.DeductionType = (from htype in context.salaryheads
                                            where htype.sHeadId == deductionTypeID
                                            select htype.sHeadName).FirstOrDefault();
                _houseRent.designationID = DesignationID;
                _houseRent.monthName = new DateTime(2010, monthid, 1).ToString("MMMM", CultureInfo.InvariantCulture);
                _houseRent.monthID = monthid;

                _houseRent.yearID = yearId;
                _houseRent.total = total;
                return _houseRent;
            }
        }

        public static object saveBillRemark(object param)
        {
            string remark = string.Empty;
            string[] arr = param as string[];
            string msg = arr[0];
            int autoid = Convert.ToInt32(arr[1]);
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var details = (from list in context.membersMonthlySalaryDetails
                               where list.autoid == autoid
                               select list).FirstOrDefault();
                remark = details.remark + msg;
                details.remark = remark;
                context.Entry(details).State = EntityState.Modified;
                context.SaveChanges();
                context.Close();
                return remark;
            }
        }

        #endregion outer bill

        #region checkmonthyear

        public static object checkMonthYear(object param)
        {
            int memberID = Convert.ToInt32(param);
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var query = (from monthyear in context.membersMonthlySalaryDetails
                             where monthyear.membersid == memberID
                             orderby monthyear.autoid descending
                             select monthyear).FirstOrDefault();
                return query;
            }
        }

        public static object getSalaryBillList(object param)
        {
            int[] arr = param as int[];
            int monthID = arr[0];
            int yearID = arr[1];
            int status = arr[2];
            int assemblyid = arr[3];
            int ispart = arr[4];
            List<membersMonthlySalaryDetails> result = null;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                switch (status)
                {
                    case 1:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.JDCStatus == 1
                                      && list.AssemblyId == assemblyid
                                      && list.Ispart == ispart
                                      select list
                                   ).ToList();
                            break;
                        }
                    case 2:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.AssemblyId == assemblyid
                                      && list.Ispart == ispart
                                      && list.JDCStatus == 2 || list.SOStatus == 2
                                      select list
                                  ).ToList();
                            break;
                        }
                    case 3:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.AssemblyId == assemblyid
                                      && list.Ispart == ispart
                                      && list.SOStatus == 1
                                      select list
                                     ).ToList();
                            break;
                        }
                    case 0:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.AssemblyId == assemblyid
                                      && list.Ispart == ispart
                                      && list.SOStatus == 0
                                      select list
                                ).ToList();
                            break;
                        }
                    case 4:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.AssemblyId == assemblyid
                                      && list.Ispart == ispart
                                      select list
                                 ).ToList();
                            break;
                        }
                }

                List<salaryBillInfo> _sbList = new List<salaryBillInfo>();
                foreach (var item in result)
                {
                    int mId = item.membersid;
                    salaryBillInfo sbi = new salaryBillInfo();
                    sbi.AutoID = item.autoid;
                    sbi.memberName = (from members in context.mMember
                                      where members.MemberCode == mId
                                      select members.Prefix + " " + members.Name + " ").FirstOrDefault();
                    //var assemblyID = (from assembly in context.mMemberAssembly
                    //                  join mAssemblyOrder in context.mAssembly
                    //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                    //                  where assembly.MemberID == mId
                    //                  orderby mAssemblyOrder.AssemblyID descending
                    //                  select assembly.AssemblyID
                    //                   ).FirstOrDefault();
                    // var assemblyID = 13;
                    sbi.Designation = (from memberAssemblies in context.mMemberAssembly
                                       where memberAssemblies.AssemblyID == assemblyid
                                       && memberAssemblies.MemberID == mId
                                       select memberAssemblies.Designation
                                   ).FirstOrDefault();
                    sbi.memberID = mId;
                    sbi.netSalary = item.grandtotal;
                    sbi.totalAllowances = item.totalAllowances;
                    sbi.totalDeductions = item.totalAGDeductionsA;
                    if (item.SOStatus == 0)
                    {
                        sbi.status = 0;
                    }
                    else
                    {
                        if (item.SOStatus == 2)
                        {
                            sbi.status = 2;
                        }
                        else
                        {
                            if (item.JDCStatus == 0)
                            {
                                sbi.status = 0;
                            }
                            else
                            {
                                if (item.JDCStatus == 1)
                                {
                                    sbi.status = 1;
                                }
                                else
                                {
                                    sbi.status = 2;
                                }
                            }
                        }
                    }
                    sbi.generateStatus = item.GenerateStatus;
                    sbi.viewRemark = item.remark;
                    _sbList.Add(sbi);
                }
                return _sbList;
            }
        }

        public static object getSalaryBillListForMember(object param)
        {
            int[] arr = param as int[];
            int monthID = arr[0];
            int yearID = arr[1];
            int memberCode = arr[2];
            int status = 1;
            List<membersMonthlySalaryDetails> result = null;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                switch (status)
                {
                    case 1:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.JDCStatus == 1 && list.membersid == memberCode && list.encashmentdate != null
                                      select list
                                   ).ToList();
                            break;
                        }
                    case 2:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.JDCStatus == 2 || list.SOStatus == 2 && list.membersid == memberCode && list.encashmentdate != null
                                      select list
                                  ).ToList();
                            break;
                        }
                    case 3:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID && list.membersid == memberCode && list.encashmentdate != null
                                      && list.SOStatus == 1
                                      select list
                                     ).ToList();
                            break;
                        }
                    case 0:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID && list.membersid == memberCode && list.encashmentdate != null
                                      && list.SOStatus == 0
                                      select list
                                ).ToList();
                            break;
                        }
                    case 4:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID && list.membersid == memberCode && list.encashmentdate != null
                                      select list
                                 ).ToList();
                            break;
                        }
                }

                List<salaryBillInfo> _sbList = new List<salaryBillInfo>();
                foreach (var item in result)
                {
                    int mId = item.membersid;
                    salaryBillInfo sbi = new salaryBillInfo();
                    sbi.AutoID = item.autoid;
                    sbi.memberName = (from members in context.mMember
                                      where members.MemberCode == mId
                                      select members.Prefix + " " + members.Name + " ").FirstOrDefault();
                    //var assemblyID = (from assembly in context.mMemberAssembly
                    //                  join mAssemblyOrder in context.mAssembly
                    //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                    //                  where assembly.MemberID == mId
                    //                  orderby mAssemblyOrder.AssemblyID descending
                    //                  select assembly.AssemblyID
                    //                   ).FirstOrDefault();

                    var assemblyID = 13;
                    sbi.Designation = (from memberAssemblies in context.mMemberAssembly
                                       where memberAssemblies.AssemblyID == assemblyID
                                       && memberAssemblies.MemberID == mId
                                       select memberAssemblies.Designation
                                   ).FirstOrDefault();
                    sbi.memberID = mId;
                    sbi.netSalary = item.grandtotal;
                    sbi.totalAllowances = item.totalAllowances;
                    sbi.totalDeductions = item.totalAGDeductionsA;
                    if (item.SOStatus == 0)
                    {
                        sbi.status = 0;
                    }
                    else
                    {
                        if (item.SOStatus == 2)
                        {
                            sbi.status = 2;
                        }
                        else
                        {
                            if (item.JDCStatus == 0)
                            {
                                sbi.status = 0;
                            }
                            else
                            {
                                if (item.JDCStatus == 1)
                                {
                                    sbi.status = 1;
                                }
                                else
                                {
                                    sbi.status = 2;
                                }
                            }
                        }
                    }
                    sbi.generateStatus = item.GenerateStatus;
                    sbi.viewRemark = item.remark;
                    _sbList.Add(sbi);
                }
                return _sbList;
            }
        }

        public static object changeBillStatus(object param)
        {
            try
            {
                string[] arr = param as string[];
                int status = Convert.ToInt32(arr[0]);
                int autoID = Convert.ToInt32(arr[1]);
                string user = arr[2];
                using (salaryheadcontext context = new salaryheadcontext())
                {
                    var monthlydetails = (from list in context.membersMonthlySalaryDetails
                                          where list.autoid == autoID
                                          select list).FirstOrDefault();
                    if (user == "so")
                    {
                        if (status != 1)
                        {
                            monthlydetails.SOStatus = status;
                        }
                        else
                        {
                            monthlydetails.SOStatus = status;
                            monthlydetails.JDCStatus = 0;
                        }
                        monthlydetails.SOActionDate = DateTime.Now;
                    }
                    else
                    {
                        if (user.ToLower() == "jdc")
                        {
                            if (status != 1)
                            {
                                monthlydetails.JDCStatus = status;
                            }
                            else
                            {
                                monthlydetails.SOStatus = status;
                                monthlydetails.JDCStatus = status;
                            }
                            monthlydetails.JDCActionDate = DateTime.Now;
                        }
                    }

                    context.Entry(monthlydetails).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static object getsalaryBillByID(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int autoid = Convert.ToInt32(param);
                var monthlydetails = (from list in context.membersMonthlySalaryDetails
                                      where list.autoid == autoid
                                      select list).FirstOrDefault();
                int mId = monthlydetails.membersid;
                int monthid = monthlydetails.month;
                int yearId = monthlydetails.year;

                var membersalary = (from list in context.membersSalary
                                    join shead in context.salaryheads
                                   on list.headId equals shead.sHeadId
                                    where list.membersId == mId && list.monthID == monthid && list.Year == yearId
                                    orderby shead.hType, shead.orderNo
                                    select new { membersalary = list, HeadName = shead.sHeadName, htype = shead.hType });

                var member = (from members in context.mMember
                              where members.MemberCode == mId
                              select new
                              {
                                  name = members.Prefix + " " + members.Name + " ",
                                  mobile = members.Mobile,
                                  email = members.Email

                              }).FirstOrDefault();

                var assemblyID = (from assembly in context.mMemberAssembly
                                  join mAssemblyOrder in context.mAssembly
                                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                  where assembly.MemberID == mId
                                  orderby mAssemblyOrder.AssemblyID descending
                                  select assembly.AssemblyID
                                        ).FirstOrDefault();
                var designation = (from memberAssemblies in context.mMemberAssembly
                                   join desg in context.mMemberDesignations
                                   on memberAssemblies.DesignationID equals desg.memDesigcode
                                   where memberAssemblies.AssemblyID == assemblyID
                                   && memberAssemblies.MemberID == mId
                                   select desg.memDesigname
                                   ).FirstOrDefault();

                salarystructure _salarys = new salarystructure();
                _salarys.catID = 0;
                _salarys.approved = false;
                _salarys.designationName = designation;
                _salarys.memberCode = mId;
                _salarys.memberName = member.name;
                _salarys.mobileNO = member.mobile;
                _salarys.email = member.email;
                _salarys.monthID = monthid;
                _salarys.yearId = yearId;
                _salarys.netSalary = monthlydetails.grandtotal;
                _salarys.netSalaryInWords = "";
                _salarys.totalAllowances = monthlydetails.totalAllowances;
                _salarys.totalDeduction = monthlydetails.totalAGDeductionsA;
                _salarys.Mode = "update";
                List<HeadList> hlist = new List<HeadList>();
                foreach (var a in membersalary.ToList())
                {
                    HeadList s = new HeadList();
                    s.amount = Convert.ToDouble(a.membersalary.amount);
                    s.headID = a.membersalary.headId;
                    s.headName = a.HeadName;
                    s.htype = a.htype;

                    hlist.Add(s);
                }
                _salarys.headList = hlist;

                return _salarys;
            }
        }

        #endregion checkmonthyear

        #region schedule report

        public static object getListofScheduleReport(object param)
        {
            int[] arr = param as int[];
            int designationID = arr[0];
            int loanTypeID = arr[1];
            int monthID = arr[2];
            int yearID = arr[3];
            int AssemblyId = arr[4];
            int ispart = arr[5];
            double prec = 0, pAmount = 0, iAmount = 0, cbal = 0, obal = 0, outstandingbill = 0;
            int instno = 0;
            List<ScheduleloanList> slist = new List<ScheduleloanList>();
            List<mMember> _mMemberList = (List<mMember>)getallmembers(AssemblyId);
            List<int> lst = _mMemberList.Select(a => a.MemberCode).ToList();



            using (salaryheadcontext context = new salaryheadcontext())
            {


                var query = (from loan in context.loanDetails
                             join loanee in context.loaneeDetails
                             on loan.loaneeID equals loanee.loaneeID
                             join loanType in context.loanType
                             on loan.loanTypeID equals loanType.loanID

                             where loan.loanStatus == true && loan.loanTypeID == loanTypeID //&& lst.Contains(loanee.memberCode)
                             select new
                             {
                                 loan = loan,
                                 loanee = loanee,
                                 loanType = loanType
                             }
                                );
                if (designationID == 1)
                {
                    query = query.Where(a => a.loanee.designationID == "1");
                }
                if (designationID == 14)
                {
                    query = query.Where(a => a.loanee.designationID == "14");
                }
                if (designationID == 4)
                {
                    query = query.Where(a => a.loanee.designationID == "35");
                }
                
                if (designationID != 1 && designationID != 14 && designationID != 4)
                {
                    query = query.Where(a => lst.Contains(a.loanee.memberCode));
                }


                var loanlist = query.ToList();

                var checkquery = (from check in context.membersMonthlySalaryDetails
                                  where check.month == monthID && check.year == yearID && check.AssemblyId == AssemblyId
                                  && check.Ispart == ispart
                                  select check.membersid).ToList();

                var aquery = query.Where(a => checkquery.Contains(a.loanee.memberCode));
                var newloanlist = aquery.ToList();
                //var filteredList = query.Except(aquery);
                //var newloanlist = filteredList.ToList();

                foreach (var item in newloanlist)
                {

                    var loanTransDetails = (from loanTrans in context.loanTransDetails
                                            where loanTrans.loanNumber == item.loan.loanNumber
                                            && loanTrans.monthID == monthID
                                            && loanTrans.yearID == yearID
                                            && loanTrans.paymentMode == "Salary"
                                            && loanTrans.transType == "dr"
                                            && loanTrans.closingBalance > 0
                                            select loanTrans).ToList();
                    if (loanTransDetails.Count > 0)
                    {
                        foreach (var items in loanTransDetails)
                        {
                            pAmount = items.principalReceived;
                            iAmount = items.interestReceived;
                            instno = items.installmentNumber;
                            cbal = items.closingBalance;
                            obal = items.openingBalance;
                            outstandingbill = items.outstandingInterest;
                        }

                        var allloanTrans = (from loanTrans in context.loanTransDetails
                                            where loanTrans.loanNumber == item.loan.loanNumber
                                            && loanTrans.transType == "dr"
                                            select loanTrans).ToList();
                        allloanTrans.Sum(p => p.principalReceived);
                        ScheduleloanList plist = new ScheduleloanList();
                        plist.AccNumber = item.loan.loanAccountNumber;
                        plist.progRecover = allloanTrans.Sum(p => p.principalReceived);
                        plist.balAMount = item.loan.loanAmount - allloanTrans.Sum(p => p.principalReceived);
                        plist.designation = item.loanee.designationID;
                        plist.idedOfMonth = iAmount;
                        plist.instNO = instno;
                        plist.Name = item.loanee.memberName;
                        plist.outstandingBal = outstandingbill;
                        plist.pdedOfMonth = pAmount;
                        plist.totalAdvance = item.loan.loanAmount;
                        mMemberAssembly m = getLastAssembly(item.loanee.memberCode);

                        plist.designation = m.Designation;

                        slist.Add(plist);
                    }
                }

                ScheduleReport report = new ScheduleReport();
                report.accountHead = "";
                report.accountHeadList = null;
                report.monthID = monthID;
                report.yearID = yearID;
                report.LoanName = (from type in context.loanType
                                   where type.loanID == loanTypeID
                                   select type.loanName).FirstOrDefault();
                report.monthName = new DateTime(2010, monthID, 1).ToString("MMMM", CultureInfo.InvariantCulture);
                report.totalRecover = slist.Sum(p => p.pdedOfMonth);
                report.itotalRecover = slist.Sum(p => p.idedOfMonth);
                report.scheduleloanList = slist;
                return report;

            }
            ;
        }

        #endregion schedule report

        #region Bill NO

        public static object checkSalaryBill(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                var CurrentFinancialYear = (from m in context.mSiteSettings
                                            where m.SettingName == "CurrentFinancialYear"
                                            select m.SettingValue).FirstOrDefault();

                int[] arr = param as int[];
                int designationID = arr[0];
                int yearID = arr[2];
                int monthID = arr[1];
                int budgetTypeID = arr[3];
                int assemblyid = arr[4];
                int ispart = arr[5];
                var result = (from list in context.mEstablishBill
                              //join m in context.membersMonthlySalaryDetails
                              //on list.AssemblyId equals assemblyid
                              where list.sMOnthID == monthID
                              && list.sYearID == yearID
                              && list.Designation == designationID
                              && list.BudgetId == budgetTypeID
                              && list.FinancialYear == CurrentFinancialYear
                              && list.IsCanceled == false
                              && list.AssemblyId == assemblyid
                              && list.ispart == ispart

                              select list.BillNumber
                        ).FirstOrDefault();
                if (result != null && result != 0)
                {
                    return result;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static object getEbillDetails(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int[] arr = param as int[];
                int billNO = arr[0];
                int yearID = arr[2];
                int monthID = arr[1];
                var result = (from list in context.mEstablishBill
                              where list.sMOnthID == monthID
                              && list.sYearID == yearID
                                && list.BillNumber == billNO
                              select list.EstablishBillID
                        ).FirstOrDefault();
                if (result != null && result != 0)
                {
                    return result;
                }
                else
                {
                    return 0;
                }
            }
        }

        //public static object updateBillDetailsInSalary(object param)
        //{
        //    using (salaryheadcontext context = new salaryheadcontext())
        //    {
        //        int[] arr = param as int[];
        //        int EbillID = arr[0];
        //        int yearID = arr[2];
        //        int monthID = arr[1];
        //        int memberID = arr[3];
        //        int assemblyid = arr[4];
        //        var result = (from list in context.membersMonthlySalaryDetails
        //                      where list.month == monthID
        //                      && list.year == yearID
        //                        && list.membersid == memberID
        //                        && list.AssemblyId == assemblyid
        //                      select list
        //                ).FirstOrDefault();
        //        if (result != null)
        //        {
        //            result.EstablishmentID = EbillID;
        //            context.Entry(result).State = EntityState.Modified;
        //            context.SaveChanges();
        //        }
        //        return null;
        //    }
        //}
        public static object updateBillDetailsInSalary(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
                int[] arr = param as int[];
                int EbillID = arr[0];
                int yearID = arr[2];
                int monthID = arr[1];
                int memberID = arr[3];
                int assemblyid = arr[4];
                int ispart = arr[5];
                var result = (from list in context.membersMonthlySalaryDetails
                              where list.month == monthID
                              && list.year == yearID
                                && list.membersid == memberID
                                && list.AssemblyId == assemblyid
                                  && list.Ispart == ispart
                              select list
                        ).FirstOrDefault();
                if (result != null)
                {
                    result.EstablishmentID = EbillID;
                    context.Entry(result).State = EntityState.Modified;
                    context.SaveChanges();
                }
                return null;
            }
        }

        #endregion Bill NO

        public static object checkSalaryBillList(object param)
        {
            int[] arr = param as int[];
            int monthID = arr[0];
            int yearID = arr[1];
            int designation = arr[2];
            int assemblyID = arr[3];
            int ispart = arr[4];
            List<membersMonthlySalaryDetails> result = null;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                result = (from list in context.membersMonthlySalaryDetails
                          where list.month == monthID && list.year == yearID
                          && list.catID == designation && list.JDCStatus != 1 && list.AssemblyId == assemblyID
                          && list.Ispart == ispart
                          select list
                         ).ToList();
                List<salaryBillInfo> _sbList = new List<salaryBillInfo>();
                foreach (var item in result)
                {
                    int mId = item.membersid;
                    salaryBillInfo sbi = new salaryBillInfo();
                    sbi.AutoID = item.autoid;
                    sbi.memberName = (from members in context.mMember
                                      where members.MemberCode == mId
                                      select members.Prefix + " " + members.Name + " ").FirstOrDefault();
                    //var assemblyID = (from assembly in context.mMemberAssembly
                    //                  join mAssemblyOrder in context.mAssembly
                    //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                    //                  where assembly.MemberID == mId
                    //                  orderby mAssemblyOrder.AssemblyID descending
                    //                  select assembly.AssemblyID
                    //                   ).FirstOrDefault();
                    sbi.Designation = (from memberAssemblies in context.mMemberAssembly
                                       join desg in context.mMemberDesignations
                                       on memberAssemblies.DesignationID equals desg.memDesigcode
                                       where memberAssemblies.AssemblyID == assemblyID
                                       && memberAssemblies.MemberID == mId
                                       select desg.memDesigname
                                     ).FirstOrDefault();

                    //sbi.Designation = (from memberAssemblies in context.mMemberAssembly

                    //                   where memberAssemblies.AssemblyID == assemblyID
                    //                   && memberAssemblies.MemberID == mId

                    //                   select memberAssemblies.Designation
                    //               ).FirstOrDefault();

                    sbi.memberID = mId;
                    sbi.netSalary = item.grandtotal;
                    sbi.totalAllowances = item.totalAllowances;
                    sbi.totalDeductions = item.totalAGDeductionsA;
                    if (item.SOStatus == 0)
                    {
                        sbi.status = 0;
                    }
                    else
                    {
                        if (item.SOStatus == 2)
                        {
                            sbi.status = 2;
                        }
                        else
                        {
                            if (item.JDCStatus == 0)
                            {
                                sbi.status = 0;
                            }
                            else
                            {
                                if (item.JDCStatus == 1)
                                {
                                    sbi.status = 1;
                                }
                                else
                                {
                                    sbi.status = 2;
                                }
                            }
                        }
                    }
                    sbi.generateStatus = item.GenerateStatus;
                    sbi.viewRemark = item.remark;
                    _sbList.Add(sbi);
                }
                return _sbList;
            }
        }

        public static object SalaryBillList(object param)
        {
            int[] arr = param as int[];
            int monthID = arr[0];
            int yearID = arr[1];
            int designation = arr[2];
            int assemblyID = arr[3];
            int ispart = arr[4];
            List<membersMonthlySalaryDetails> result = null;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                result = (from list in context.membersMonthlySalaryDetails
                          where list.month == monthID && list.year == yearID
                          && list.catID == designation && list.JDCStatus == 1
                          && list.AssemblyId == assemblyID
                          && list.Ispart == ispart
                          select list
                         ).ToList();
                List<salaryBillInfo> _sbList = new List<salaryBillInfo>();
                foreach (var item in result)
                {
                    int mId = item.membersid;
                    salaryBillInfo sbi = new salaryBillInfo();
                    sbi.AutoID = item.autoid;
                    sbi.memberID = item.membersid;
                    sbi.memberName = (from members in context.mMember
                                      where members.MemberCode == mId
                                      select members.Prefix + " " + members.Name + " ").FirstOrDefault();
                    //var assemblyID = (from assembly in context.mMemberAssembly
                    //                  join mAssemblyOrder in context.mAssembly
                    //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                    //                  where assembly.MemberID == mId
                    //                  orderby mAssemblyOrder.AssemblyID descending
                    //                  select assembly.AssemblyID
                    //                   ).FirstOrDefault();
                    sbi.Designation = (from memberAssemblies in context.mMemberAssembly
                                       where memberAssemblies.AssemblyID == assemblyID
                                       && memberAssemblies.MemberID == mId
                                       select memberAssemblies.Designation
                                   ).FirstOrDefault();
                    sbi.memberID = mId;
                    sbi.netSalary = item.grandtotal;
                    sbi.totalAllowances = item.totalAllowances;
                    sbi.totalDeductions = item.totalAGDeductionsA;
                    if (item.SOStatus == 0)
                    {
                        sbi.status = 0;
                    }
                    else
                    {
                        if (item.SOStatus == 2)
                        {
                            sbi.status = 2;
                        }
                        else
                        {
                            if (item.JDCStatus == 0)
                            {
                                sbi.status = 0;
                            }
                            else
                            {
                                if (item.JDCStatus == 1)
                                {
                                    sbi.status = 1;
                                }
                                else
                                {
                                    sbi.status = 2;
                                }
                            }
                        }
                    }
                    sbi.generateStatus = item.GenerateStatus;
                    sbi.viewRemark = item.remark;
                    _sbList.Add(sbi);
                }
                return _sbList;
            }
        }

        #region nominal report

        public static object getNominalReport(object param)
        {
            int[] arr = param as int[];
            var designationID = arr[0];
            var Assemblyid = arr[1];
            using (salaryheadcontext context = new salaryheadcontext())
            {
                double? grossTotal = 0;
                MembersNominalReport nominal = new MembersNominalReport();
                if (designationID == 1)
                {
                    nominal.financialYear = (DateTime.Now.Year + 1) + " - " + (DateTime.Now.Year + 2);
                    nominal.favourOF = "Hon'ble Speaker & Deputy Speaker (Charged)  Under Demand No.1	";
                    nominal.head = "Maj.Head (2011)-02-101-01 ";

                    List<MembersNominalReportLIst> list = new List<MembersNominalReportLIst>();
                    nominal.MembersNominalReportLIst = list;
                    List<string> head = new List<string>();
                    head.Add("Sr. No.");
                    head.Add("Name");
                    var _allowList = (from mslist in context.membersHead
                                      join slist in context.salaryheads
                                      on mslist.sHeadId equals slist.sHeadId
                                      where mslist.membersCatId == 1 && mslist.status == true && mslist.htype == "cr"
                                      orderby slist.orderNo
                                      select slist.sHeadName
                                   ).ToList();
                    head.AddRange(_allowList);
                    head.Add("Total");
                    head.Add("Total For the Year");

                    list.Add(new MembersNominalReportLIst() { Columns = head });
                    //var q = (from assemblyid in context.mSiteSettings
                    //         where assemblyid.SettingName == "Assembly"
                    //         select assemblyid.SettingValue).FirstOrDefault();
                    // int aid = Convert.ToInt32(q);
                    int aid = Convert.ToInt32(Assemblyid);
                    List<int?> splist = new List<int?>() { 1, 14, 58 };
                    var query = (from member in context.mMember
                                 join massembly in context.mMemberAssembly
                                 on member.MemberCode equals massembly.MemberID
                                 where splist.Contains(massembly.DesignationID) && massembly.AssemblyID == aid
                                 orderby massembly.DesignationID
                                 select member).ToList();
                    int catid = 1;
                    int count = 1;
                    foreach (var item in query)
                    {
                        List<string> Row = new List<string>();
                        var _allowListforMember = (from mlist in context.membersHead
                                                   join sslist in context.salaryheads
                                                   on mlist.sHeadId equals sslist.sHeadId
                                                   where (mlist.membersCatId == catid) && sslist.hType == "cr" && mlist.status == true
                                                   orderby sslist.orderNo
                                                   select new { mlist.amount }
                                      ).ToList();

                        Row.Add(count.ToString());
                        count++;
                        //Row.Add(catid.ToString());
                        if (catid == 2)
                        {
                            catid++;
                            catid++;
                        }
                        else
                        {
                            catid++;
                        }

                        Row.Add((string)getMemberName(item.MemberCode));
                        if (_allowListforMember.Count() > 0)
                        {
                            foreach (var critem in _allowListforMember)
                            {
                                Row.Add(critem.amount.ToString());

                                grossTotal += critem.amount;
                            }
                            Row.Add(grossTotal.ToString());
                            Row.Add((grossTotal * 12).ToString());
                            grossTotal = 0;
                        }
                        list.Add(new MembersNominalReportLIst() { Columns = Row });
                    }
                    List<string> footerRow = new List<string>();
                    var total = (from mlist in context.membersHead
                                 join sslist in context.salaryheads
                                 on mlist.sHeadId equals sslist.sHeadId
                                 where (mlist.membersCatId == 1 || mlist.membersCatId == 2 || mlist.membersCatId == 4) && sslist.hType == "cr" && mlist.status == true
                                 group new { mlist, sslist } by new
                                 {
                                     mlist.sHeadId,
                                     sslist.orderNo
                                 } into g
                                 orderby
                                  g.Key.orderNo
                                 select new
                                 {
                                     amount = (double?)g.Sum(p => p.mlist.amount)
                                 }).ToList();
                    footerRow.Add("");

                    footerRow.Add("Total");
                    if (total.Count() > 0)
                    {
                        foreach (var critem in total)
                        {
                            footerRow.Add(critem.amount.ToString());

                            grossTotal += critem.amount;
                        }
                        footerRow.Add(grossTotal.ToString());
                        footerRow.Add((grossTotal * 12).ToString());
                        grossTotal = 0;
                    }
                    list.Add(new MembersNominalReportLIst() { Columns = footerRow });
                }
                if (designationID == 2)
                {
                    nominal.financialYear = (DateTime.Now.Year + 1) + " - " + (DateTime.Now.Year + 2);
                    nominal.favourOF = "Under Demand No.1";
                    nominal.head = "  Major Head (2011)-02-101-03-Vidhan Sabha Member(Voted/N.P.)";
                    List<MembersNominalReportLIst> list = new List<MembersNominalReportLIst>();
                    nominal.MembersNominalReportLIst = list;
                    List<string> head = new List<string>();
                    head.Add("Sr. No.");
                    head.Add("Name");
                    var _allowList = (from mslist in context.membersHead
                                      join slist in context.salaryheads
                                      on mslist.sHeadId equals slist.sHeadId
                                      where mslist.membersCatId == 3 && mslist.status == true && mslist.htype == "cr"
                                      orderby slist.orderNo
                                      select slist.sHeadName
                                   ).ToList();
                    head.AddRange(_allowList);
                    head.Add("Total");
                    head.Add("Total For the Year");

                    list.Add(new MembersNominalReportLIst() { Columns = head });
                    //var q = (from assemblyid in context.mSiteSettings
                    //         where assemblyid.SettingName == "Assembly"
                    //         select assemblyid.SettingValue).FirstOrDefault();
                    int aid = Assemblyid;

                    List<mMember> _mMemberList = (List<mMember>)getallmembers(Assemblyid);
                    int catid = 1;
                    foreach (var item in _mMemberList)
                    {
                        List<string> Row = new List<string>();
                        var _allowListforMember = (from mlist in context.membersHead
                                                   join sslist in context.salaryheads
                                                   on mlist.sHeadId equals sslist.sHeadId
                                                   where (mlist.membersCatId == 3) && sslist.hType == "cr" && mlist.status == true
                                                   orderby sslist.orderNo
                                                   select new { mlist.amount }
                                      ).ToList();

                        Row.Add(catid.ToString());
                        catid++;
                        Row.Add((string)getMemberName(item.MemberCode));
                        if (_allowListforMember.Count() > 0)
                        {
                            foreach (var critem in _allowListforMember)
                            {
                                Row.Add(critem.amount.ToString());

                                grossTotal += critem.amount;
                            }
                            Row.Add(grossTotal.ToString());
                            Row.Add((grossTotal * 12).ToString());
                            grossTotal = 0;
                        }
                        list.Add(new MembersNominalReportLIst() { Columns = Row });
                    }
                    List<string> footerRow = new List<string>();
                    var total = (from mlist in context.membersHead
                                 join sslist in context.salaryheads
                                 on mlist.sHeadId equals sslist.sHeadId
                                 where (mlist.membersCatId == 3) && sslist.hType == "cr" && mlist.status == true
                                 group new { mlist, sslist } by new
                                 {
                                     mlist.sHeadId,
                                     sslist.orderNo
                                 } into g
                                 orderby
                                  g.Key.orderNo
                                 select new
                                 {
                                     amount = (double?)g.Sum(p => p.mlist.amount)
                                 }).ToList();
                    footerRow.Add("");

                    footerRow.Add("Total");
                    if (total.Count() > 0)
                    {
                        foreach (var critem in total)
                        {
                            footerRow.Add((critem.amount * _mMemberList.Count()).ToString());
                            grossTotal += (critem.amount * _mMemberList.Count());
                        }
                        footerRow.Add(grossTotal.ToString());
                        footerRow.Add((grossTotal * 12).ToString());
                        grossTotal = 0;
                    }
                    list.Add(new MembersNominalReportLIst() { Columns = footerRow });
                }
                if (designationID == 3)
                {
                    nominal.financialYear = (DateTime.Now.Year + 1) + " - " + (DateTime.Now.Year + 2);
                    nominal.favourOF = "HP Vidhan Sabha Secretariat Employees Salary";
                    List<MembersNominalReportLIst> list = new List<MembersNominalReportLIst>();
                    nominal.MembersNominalReportLIst = list;
                    List<string> head = new List<string>();
                    head.Add("Sr. No.");
                    head.Add("Name/No. of Post & PB");
                    head.Add("Date of Inc.");
                    head.Add("BASIC");
                    head.Add("TGRADE PAY");
                    head.Add("Inc.");
                    head.Add("Provision of year");
                    head.Add("Sectt. Pay");
                    head.Add("DA");
                    head.Add("Spl. Pay");
                    head.Add("CCA");
                    head.Add("Cap. Allw.");
                    head.Add("HRA");
                    head.Add("Cony. Allow.");
                    head.Add("wash allow.");
                    head.Add("FPA");
                    head.Add("Total Allow.");
                    head.Add("Grand Total");
                    list.Add(new MembersNominalReportLIst() { Columns = head });

                    var query = (from designation in context.mStaffDesignation
                                 select designation).ToList();
                    int catid = 0;

                    foreach (var item in query)
                    {
                        List<string> dRow = new List<string>();
                        dRow.Add("__");
                        dRow.Add(item.designation + "(" + item.post + ")" + item.text + "+" + item.gradePay + "GP" + "+" + item.SecttPay + "SP");
                        list.Add(new MembersNominalReportLIst() { Columns = dRow });
                        var _staffList = (from staff in context.mStaff
                                          where staff.Designation == item.designation
                                          && staff.isActive == true
                                          select staff).ToList();

                        catid++;

                        if (_staffList.Count() > 0)
                        {
                            foreach (var critem in _staffList)
                            {
                                double basic = critem.Basic.HasValue ? critem.Basic.Value : 0;
                                double gradepay = critem.gradePay.HasValue ? critem.gradePay.Value : 0;
                                // double crbasic = critem.Basic.HasValue ? critem.Basic.Value : 0;
                                double inc = ((basic + gradepay) * 3) / 100;
                                double provisionalyear = (basic + gradepay + inc) * 12;
                                double secttpay = critem.SecttPay.HasValue ? critem.SecttPay.Value : 0;
                                double DA = ((provisionalyear + secttpay) * 125) / 100;
                                double splPay = critem.SplPay.HasValue ? critem.SplPay.Value : 0;
                                double cca = 250 * 12;
                                double cap = 275 * 12;
                                double hra = critem.hra.HasValue ? critem.hra.Value : 0;
                                double conyallow = critem.ConyAllow.HasValue ? critem.ConyAllow.Value : 0;
                                double washallow = critem.washAllow.HasValue ? critem.washAllow.Value : 0;
                                double fpa = critem.fpa.HasValue ? critem.fpa.Value : 0;
                                double totalallow = cca + cap + hra + conyallow + washallow + fpa;
                                double grandTotal = provisionalyear + secttpay + DA + cca + cap + hra + conyallow + washallow + fpa;
                                grossTotal += grandTotal;
                                List<string> Row = new List<string>();
                                Row.Add(catid.ToString());
                                Row.Add(critem.StaffName);
                                Row.Add(new DateTime(1999, critem.monthID >= 1 ? critem.monthID : 1, 1).ToString("MMM") + "-" + critem.yearID);
                                Row.Add(basic.ToString());
                                Row.Add(gradepay.ToString());
                                Row.Add(inc.ToString());
                                Row.Add(provisionalyear.ToString());
                                Row.Add(secttpay.ToString());
                                Row.Add(DA.ToString());
                                Row.Add(splPay.ToString());
                                Row.Add(cca.ToString());
                                Row.Add(cap.ToString());
                                Row.Add(hra.ToString());
                                Row.Add(conyallow.ToString());
                                Row.Add(washallow.ToString());
                                Row.Add(fpa.ToString());
                                Row.Add(totalallow.ToString());
                                Row.Add(grandTotal.ToString());
                                // grossTotal += critem.amount;
                                list.Add(new MembersNominalReportLIst() { Columns = Row });
                            }
                            //  Row.Add(grossTotal.ToString());
                            // Row.Add((grossTotal * 12).ToString());
                            // grossTotal = 0;
                        }
                    }
                    List<string> footerRow = new List<string>();

                    footerRow.Add("@@");
                    footerRow.Add("Total");

                    footerRow.Add(grossTotal.ToString());

                    grossTotal = 0;

                    list.Add(new MembersNominalReportLIst() { Columns = footerRow });
                }
                return nominal;
            }

        #endregion nominal report
        }

        public static Object getNominalReportForStaff(object param)
        {
            string[] arr = param as string[];
            int Increment_param = Convert.ToInt32(arr[0]);
            int DA_param = Convert.ToInt32(arr[1]);
            int CCA_param = Convert.ToInt32(arr[2]);
            int CAP_param = Convert.ToInt32(arr[3]);
            int LECAmount_param = Convert.ToInt32(arr[4]);
            int LTCAmount_param = Convert.ToInt32(arr[5]);
            string financialYear = (DateTime.Now.Year + 1) + " - " + (DateTime.Now.Year + 2); ;
            using (salaryheadcontext context = new salaryheadcontext())
            {
                double? grossTotal = 0;
                MembersNominalReport nominal = new MembersNominalReport();
                nominal.financialYear = financialYear;
                nominal.favourOF = " Demand No.1";
                nominal.head = "  Major Head:2011-02-103-01-Vidhan Sabha Staff/SOON/NP (Voted),";
                List<MembersNominalReportLIst> list = new List<MembersNominalReportLIst>();
                nominal.MembersNominalReportLIst = list;
                List<string> head = new List<string>();
                head.Add("Sr. No.");
                head.Add("");
                head.Add("Name/No. of Post & PB");
                head.Add("Date of Inc.");
                head.Add("Basic");
                head.Add("Tgrade Pay");
                head.Add("Inc.");
                head.Add("Provision of year");
                head.Add("Sectt. Pay");
                head.Add("DA");
                head.Add("Spl. Pay");
                head.Add("CCA");
                head.Add("Cap. Allw.");
                head.Add("HRA");
                head.Add("Cony. Allow.");
                head.Add("Wash Allow.");
                head.Add("FPA");
                head.Add("Total Allow.");
                head.Add("Grand Total");
                list.Add(new MembersNominalReportLIst() { Columns = head });

                var query = (from designation in context.mStaffDesignation
                             select designation).ToList();
                int catid = 0;


                foreach (var item in query)
                {
                    int roman = 0;
                    catid++;

                    List<string> dRow = new List<string>();
                    dRow.Add("__");
                    dRow.Add(catid.ToString());
                    dRow.Add(item.designation + "(" + item.post + ")" + item.text + " + " + item.gradePay + "GP" + " + " + item.SecttPay + "SP");
                    list.Add(new MembersNominalReportLIst() { Columns = dRow });
                    var _staffList = (from staff in context.mStaff
                                      where staff.Designation == item.designation
                                      && staff.isActive == true && staff.IsReject == false && staff.Group != "ExStaff" && staff.isNominal == true
                                      select staff).ToList();
                    if (_staffList.Count() > 0)
                    {
                        foreach (var critem in _staffList)
                        {
                            roman++;
                            double basic = critem.Basic.HasValue ? critem.Basic.Value : 0;
                            double gradepay = critem.gradePay.HasValue ? critem.gradePay.Value : 0;
                            double inc = ((basic + gradepay) * Increment_param) / 100;
                            double provisionalyear = (basic + gradepay + inc) * 12;
                            double secttpay = (item.SecttPay.HasValue ? item.SecttPay.Value : 0) * 12;
                            double DA = ((provisionalyear + secttpay) * DA_param) / 100;
                            double splPay = critem.SplPay.HasValue ? critem.SplPay.Value : 0;
                            double cca = CCA_param * 12;
                            double cap = CAP_param * 12;
                            double hra = critem.hra.HasValue ? critem.hra.Value : 0;
                            double conyallow = critem.ConyAllow.HasValue ? critem.ConyAllow.Value : 0;
                            double washallow = critem.washAllow.HasValue ? critem.washAllow.Value : 0;
                            double fpa = critem.fpa.HasValue ? critem.fpa.Value : 0;
                            double totalallow = cca + cap + hra + conyallow + washallow + fpa;
                            double grandTotal = provisionalyear + secttpay + DA + cca + cap + hra + conyallow + washallow + fpa;
                            grossTotal += grandTotal;
                            List<string> Row = new List<string>();
                            Row.Add("");
                            string L_roman = Roman.ToRoman(roman).ToLower();
                            string _roman = "(" + L_roman + ")";
                            Row.Add(_roman);
                            Row.Add(critem.StaffName);
                            //Row.Add(Roman.ToRoman(roman));
                            //Row.Add(critem.StaffName.ToUpper());
                            Row.Add(new DateTime(1999, critem.monthID >= 1 ? critem.monthID : 1, 1).ToString("MMM") + "-" + critem.yearID);
                            Row.Add(Convert.ToInt32(basic).ToString());
                            Row.Add(Convert.ToInt32(gradepay).ToString());
                            Row.Add(inc.RoundUp().ToString());
                            Row.Add(Convert.ToInt32(provisionalyear).ToString());
                            Row.Add(Convert.ToInt32(secttpay).ToString());
                            Row.Add(Convert.ToInt32(DA).ToString());
                            Row.Add(Convert.ToInt32(splPay).ToString());
                            Row.Add(Convert.ToInt32(cca).ToString());
                            Row.Add(Convert.ToInt32(cap).ToString());
                            Row.Add(Convert.ToInt32(hra).ToString());
                            Row.Add(Convert.ToInt32(conyallow).ToString());
                            Row.Add(Convert.ToInt32(washallow).ToString());
                            Row.Add(Convert.ToInt32(fpa).ToString());
                            Row.Add(Convert.ToInt64(totalallow).ToString());
                            Row.Add(Convert.ToInt64(grandTotal).ToString());
                            // grossTotal += critem.amount;
                            list.Add(new MembersNominalReportLIst() { Columns = Row });
                        }
                        //  Row.Add(grossTotal.ToString());
                        // Row.Add((grossTotal * 12).ToString());
                        // grossTotal = 0;
                    }
                    if (_staffList.Count() < item.post)
                    {
                        for (int i = 0; i < (item.post - _staffList.Count()); i++)
                        {
                            roman++;
                            double basic = item.Basic.HasValue ? item.Basic.Value : 0;
                            double gradepay = item.gradePay.HasValue ? item.gradePay.Value : 0;
                            double inc = ((basic + gradepay) * Increment_param) / 100;
                            double provisionalyear = (basic + gradepay + inc) * 12;
                            double secttpay = (item.SecttPay.HasValue ? item.SecttPay.Value : 0) * 12;
                            double DA = ((provisionalyear + secttpay) * DA_param) / 100;
                            double splPay = item.SplPay.HasValue ? item.SplPay.Value : 0;
                            double cca = CCA_param * 12;
                            double cap = CAP_param * 12;
                            double hra = 0;
                            double conyallow = 0;
                            double washallow = 0;
                            double fpa = 0;
                            double totalallow = cca + cap + hra + conyallow + washallow + fpa;
                            double grandTotal = provisionalyear + secttpay + DA + cca + cap + hra + conyallow + washallow + fpa;
                            grossTotal += grandTotal;
                            List<string> Row = new List<string>();
                            Row.Add("");
                            string L_roman = Roman.ToRoman(roman).ToLower();
                            string _roman = "(" + L_roman + ")";
                            Row.Add(_roman);

                            // Row.Add(Roman.ToRoman(roman));
                            Row.Add("Vacant");
                            Row.Add("- -");
                            Row.Add(Convert.ToInt32(basic).ToString());
                            Row.Add(Convert.ToInt32(gradepay).ToString());
                            Row.Add(inc.RoundUp().ToString());
                            Row.Add(Convert.ToInt32(provisionalyear).ToString());
                            Row.Add(Convert.ToInt32(secttpay).ToString());
                            Row.Add(Convert.ToInt32(DA).ToString());
                            Row.Add(Convert.ToInt32(splPay).ToString());
                            Row.Add(Convert.ToInt32(cca).ToString());
                            Row.Add(Convert.ToInt32(cap).ToString());

                            Row.Add(Convert.ToInt32(hra).ToString());
                            Row.Add(Convert.ToInt32(conyallow).ToString());
                            Row.Add(Convert.ToInt32(washallow).ToString());
                            Row.Add(Convert.ToInt32(fpa).ToString());
                            Row.Add(Convert.ToInt64(totalallow).ToString());
                            Row.Add(Convert.ToInt64(grandTotal).ToString());


                            // grossTotal += critem.amount;
                            list.Add(new MembersNominalReportLIst() { Columns = Row });



                        }
                    }
                }
                List<string> footerRow = new List<string>();
                footerRow.Add("@@");
                footerRow.Add("Total");
                footerRow.Add(grossTotal.ToString());

                list.Add(new MembersNominalReportLIst() { Columns = footerRow });

                //other 1
                List<string> other1 = new List<string>();
                other1.Add("**");
                other1.Add("For Salary");
                other1.Add(grossTotal.ToString());

                list.Add(new MembersNominalReportLIst() { Columns = other1 });

                //other 1
                List<string> other2 = new List<string>();
                other2.Add("**");
                other2.Add("Leave en-cash for retirees");
                other2.Add(LECAmount_param.ToString());

                list.Add(new MembersNominalReportLIst() { Columns = other2 });

                //other 1
                List<string> other3 = new List<string>();
                other3.Add("**");
                other3.Add("LTC");
                other3.Add(LTCAmount_param.ToString());

                list.Add(new MembersNominalReportLIst() { Columns = other3 });

                //other 1
                List<string> other4 = new List<string>();
                other4.Add("**");
                other4.Add("Total");
                other4.Add(Math.Round(Convert.ToDouble(grossTotal + LTCAmount_param + LECAmount_param)).ToString());

                list.Add(new MembersNominalReportLIst() { Columns = other4 });

                nominal.Total = Math.Round(Convert.ToDouble(grossTotal + LTCAmount_param + LECAmount_param));
                grossTotal = 0;

                return nominal;
            }
        }
        public static object generateFinancialYearSalaryForMembers(object param)
        {
            int[] listarry = param as int[];
            int MemberID = Convert.ToInt32(listarry[0]);
            int designationID = Convert.ToInt32(listarry[4]);
            int monthid = listarry[1];
            string[] montH = { "SELECT", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            var n = montH[monthid];
            int yearId = listarry[2];
            string mnthyear = n + "-" + yearId;


            using (salaryheadcontext context = new salaryheadcontext())
            {
                int srno = listarry[3];
                double grossTotal = 0;
                double AllgrossTotal = 0;
                double totalDed = 0;
                double AlltotalDed = 0;

                List<membersOuterReport> _membersOuterReport = new List<membersOuterReport>();

                List<string> _colList = new List<string>();
                _colList.Add("SrNo");
                _colList.Add("Month");
                //_colList.Add("Member's Name");
                var _allowList = (from list in context.membersHead
                                  join slist in context.salaryheads
                                  on list.sHeadId equals slist.sHeadId
                                  where list.membersCatId == designationID && list.status == true && list.htype == "cr"
                                  orderby slist.orderNo
                                  select slist.sHeadName
                                    ).ToList();
                _colList.AddRange(_allowList);
                _colList.Add("Gross Salary");
                var _dedList = (from list in context.membersHead
                                join slist in context.salaryheads
                                on list.sHeadId equals slist.sHeadId
                                where list.membersCatId == designationID && list.status == true && list.htype == "dr"
                                orderby slist.orderNo
                                select slist.sHeadName
                                   ).ToList();
                var _loanList = (from list in context.salaryheads
                                 where list.LoanID != null
                                 orderby list.orderNo
                                 select list.sHeadName
                                 ).ToList();
                _colList.AddRange(_dedList);
                _colList.AddRange(_loanList);
                _colList.Add("Total Ded");
                _colList.Add("Net Salary");
                membersOuterReport _tableheader = new membersOuterReport();
                _tableheader.columnName = _colList;
                _tableheader.monthID = monthid;
                _tableheader.yearID = yearId;
                _tableheader.catID = designationID;
                _membersOuterReport.Add(_tableheader);
                //_tableheader.monthID = monthid;
                //_tableheader.yearID = yearId;
                //_tableheader.catID = designationID;

                //List<mMember> _mMemberList = (List<mMember>)getallmembers();
                if (MemberID != null)
                {
                    List<string> _colListforMember = new List<string>();

                    var _allowListforMember = (from list in context.membersSalary
                                               join slist in context.salaryheads
                                               on list.headId equals slist.sHeadId
                                               where list.catID == designationID && list.membersId == MemberID && list.monthID == monthid && list.Year == yearId && slist.hType == "cr"
                                               orderby slist.orderNo
                                               select new { list.amount, list.membersId }
                                        ).ToList();

                    _colListforMember.Add(srno.ToString());
                    _colListforMember.Add(mnthyear.ToString());
                    //_colListforMember.Add((string)getMemberName(MemberID));
                    if (_allowListforMember.Count() > 0)
                    {
                        srno++;
                        foreach (var critem in _allowListforMember)
                        {
                            _colListforMember.Add(critem.amount.ToString());

                            grossTotal += critem.amount;
                        }
                        _colListforMember.Add(grossTotal.ToString());
                        //var _dedListforMember = (from list in context.membersSalary
                        //                         join slist in context.salaryheads
                        //                         on list.headId equals slist.sHeadId
                        //                         where list.catID == designationID && list.membersId == item.MemberCode && list.monthID == monthid && list.Year == yearId && slist.hType == "dr"
                        //                         orderby slist.orderNo
                        //                         select new { list.amount, list.membersId }
                        //                   ).ToList();
                        var _dedListforMember = (from list in context.membersHead
                                                 join mlist in context.membersSalary
                                                 on list.sHeadId equals mlist.headId
                                                 join shead in context.salaryheads
                                                 on list.sHeadId equals shead.sHeadId
                                                 where list.membersCatId == designationID && mlist.membersId == MemberID && mlist.monthID == monthid && mlist.Year == yearId && shead.hType == "dr"
                                                 orderby shead.orderNo
                                                 select new { mlist.amount, mlist.membersId, shead.sHeadName }).ToList();
                        foreach (var dritem in _dedListforMember)
                        {
                            _colListforMember.Add(dritem.amount.ToString());

                            totalDed += dritem.amount;
                        }
                        var _dedloanListforMember = (from list in context.salaryheads
                                                     where list.LoanID != null
                                                     orderby list.orderNo
                                                     select list.sHeadId).ToList();

                        foreach (var loanitem in _dedloanListforMember)
                        {
                            var Llist = (from list in context.membersSalary
                                         join slist in context.salaryheads
                                                  on list.headId equals slist.sHeadId
                                         where list.catID == designationID && list.membersId == MemberID && list.monthID == monthid && list.Year == yearId && slist.hType == "dr" && list.headId == loanitem
                                         orderby slist.orderNo
                                         select new { list.amount, list.membersId }
                                           ).FirstOrDefault();
                            if (Llist != null)
                            {
                                _colListforMember.Add(Llist.amount.ToString());
                                totalDed += Llist.amount;
                            }
                            else
                            {
                                _colListforMember.Add("0");
                                //totalDed += Llist.amount;
                            }
                        }
                        _colListforMember.Add(totalDed.ToString());
                        _colListforMember.Add((grossTotal - totalDed).ToString());
                        membersOuterReport _tablerows = new membersOuterReport();
                        _tablerows.columnName = _colListforMember;

                        //footer

                        grossTotal = 0;
                        totalDed = 0;
                        _membersOuterReport.Add(_tablerows);
                    }
                }
                List<string> _fcolList = new List<string>();
                _fcolList.Add("");
                _fcolList.Add("Total");
                var _fallowList = (from msalary in context.membersSalary
                                   join shead in context.salaryheads on new { HeadId = msalary.headId } equals new { HeadId = shead.sHeadId }
                                   where
                                    msalary.monthID == monthid &&
                                    msalary.membersId == MemberID &&
                                    msalary.Year == yearId &&
                                    msalary.headtype == "cr"
                                    && msalary.catID == designationID
                                   group new { msalary, shead } by new
                                   {
                                       msalary.headId,
                                       shead.orderNo
                                   } into g
                                   orderby
                                    g.Key.orderNo
                                   select new
                                   {
                                       Column1 = (double?)g.Sum(p => p.msalary.amount)
                                   }).ToList();
                foreach (var critem in _fallowList)
                {
                    _fcolList.Add(critem.Column1.ToString());

                    AllgrossTotal += Convert.ToDouble(critem.Column1);
                }

                _fcolList.Add(AllgrossTotal.ToString());
                var _fdedList = (from shead in context.salaryheads
                                 join msalary in context.membersSalary on new { SHeadId = shead.sHeadId } equals new { SHeadId = msalary.headId }
                                 where
                                  shead.LoanID == null &&
                                  shead.hType == "dr"
                                 &&
                                 msalary.monthID == monthid
                                 &&
                                 msalary.membersId == MemberID &&
                                  msalary.Year == yearId
                                  &&
                                  msalary.catID == designationID
                                 group new { msalary, shead } by new
                                 {
                                     msalary.headId,
                                     shead.orderNo
                                 } into g
                                 orderby
                                  g.Key.orderNo
                                 select new
                                 {
                                     Column1 = (double?)g.Sum(p => p.msalary.amount)
                                 }).ToList();
                foreach (var dritem in _fdedList)
                {
                    _fcolList.Add(dritem.Column1.ToString());

                    AlltotalDed += Convert.ToDouble(dritem.Column1);
                }
                var _ldedList = (from list in context.salaryheads
                                 where list.LoanID != null
                                 orderby list.orderNo
                                 select list.sHeadId).ToList();

                foreach (var loanitem in _ldedList)
                {
                    var Llist = (from list in context.membersSalary
                                 join slist in context.salaryheads
                                          on list.headId equals slist.sHeadId
                                 where list.catID == designationID && list.monthID == monthid && list.membersId == MemberID && list.Year == yearId && slist.hType == "dr" && list.headId == loanitem
                                 orderby slist.orderNo
                                 select new { list.amount, list.membersId }
                                   );
                    if (Llist.ToList().Count() > 0)
                    {
                        _fcolList.Add(Llist.Sum(a => a.amount).ToString());
                        AlltotalDed += Llist.Sum(a => a.amount);
                    }
                    else
                    {
                        _fcolList.Add("0");
                        //totalDed += Llist.amount;
                    }
                }
                _fcolList.Add(AlltotalDed.ToString());

                // _fcolList.Add(totalDed.ToString());
                _fcolList.Add((AllgrossTotal - AlltotalDed).ToString());
                membersOuterReport _ftableheader = new membersOuterReport();

                _ftableheader.monthID = monthid;
                _ftableheader.yearID = yearId;
                _ftableheader.catID = designationID;
                _ftableheader.columnName = _fcolList;
                _membersOuterReport.Add(_ftableheader);



                return _membersOuterReport;
            }
        }

       
        public static object GetAllVSMembers(object param)
        {
            // string assemblyid = param as string;
            int aid = Convert.ToInt32(param);
            // int aid = Convert.ToInt32(assemblyid);
            using (salaryheadcontext context = new salaryheadcontext())
            {
                
                //List<string> omla = new List<string>();
                //if (q != null)
                //{
                //    var query1 = (from member in context.mMember
                //                  join massembly in context.mMemberAssembly
                //                  on member.MemberCode equals massembly.MemberID
                //                  where massembly.MemberID == id && massembly.AssemblyID == aid
                //                  select member.Name);
                //    //  return query1.ToList();


                //omla = query1.ToList();
                //}
                //  List<int?> list = new List<int?>() { 0 };
                List<int?> list = new List<int?>() { 1, 14 };
                var speakerlist = (from member in context.mMember
                                   join massembly in context.mMemberAssembly
                                   on member.MemberCode equals massembly.MemberID
                                   where list.Contains(massembly.DesignationID) &&
                                   massembly.AssemblyID == aid
                                   select member.MemberCode).ToList();
                //For Ministers
                List<int> ministers = new List<int>();
                var mid = (from minister in context.mMinsitryMinister
                           where minister.AssemblyID == aid && minister.IsDeleted != true
                           select minister.MemberCode).ToList();
                ministers = mid;
                //foreach (var i in speakerlist)
                //{
                //    ministers.Add(i);
                //}
                //

                
                var query = (from e in context.mMember
                             join massembly in context.mMemberAssembly
                             on e.MemberCode equals massembly.MemberID
                             where !(mid.Contains(e.MemberCode)) &&
                             massembly.AssemblyID == aid 
                             //&& e.Active == true 
                             && massembly.Active == true

                             orderby e.Name
                             select e).ToList();

                return query;
            }
        }

        public static object CheckSalary(object param)
        {
            using (salaryheadcontext context = new salaryheadcontext())
            {
               
                membersSalary mems = param as membersSalary;               
                var result = (from list in context.membersSalary
                              where list.AssemblyId == mems.AssemblyId
                              && list.membersId == mems.membersId
                              && list.monthID == mems.monthID
                              && list.Year == mems.Year
                              && list.ispart == mems.ispart
                              && list.catID == mems.catID
                              select list.membersId
                        ).FirstOrDefault();
                if (result != null && result != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }



        //public static Object getNominalReportForStaff(object param)
        //{
        //    string[] arr = param as string[];
        //    int Increment_param = Convert.ToInt32(arr[0]);
        //    int DA_param = Convert.ToInt32(arr[1]);
        //    int CCA_param = Convert.ToInt32(arr[2]);
        //    int CAP_param = Convert.ToInt32(arr[3]);
        //    int LECAmount_param = Convert.ToInt32(arr[4]);
        //    int LTCAmount_param = Convert.ToInt32(arr[5]);
        //    string financialYear = (DateTime.Now.Year + 1) + " - " + (DateTime.Now.Year + 2); ;
        //    using (salaryheadcontext context = new salaryheadcontext())
        //    {
        //        double? grossTotal = 0;
        //        MembersNominalReport nominal = new MembersNominalReport();
        //        nominal.financialYear = financialYear;
        //        nominal.favourOF = " The Legislative Assembly under Demand No.1";
        //        nominal.head = "  Major Head (2011)-02-103-01-Vidhan Sabha Staff/SOON/NP (Voted)";
        //        List<MembersNominalReportLIst> list = new List<MembersNominalReportLIst>();
        //        nominal.MembersNominalReportLIst = list;
        //        List<string> head = new List<string>();
        //        head.Add("Sr. No.");
        //        head.Add("");
        //        head.Add("Name/No. of Post & PB");
        //        head.Add("Date of Inc.");
        //        head.Add("BASIC");
        //        head.Add("TGRADE PAY");
        //        head.Add("Inc.");
        //        head.Add("Provision of year");
        //        head.Add("Sectt. Pay");
        //        head.Add("DA");
        //        head.Add("Spl. Pay");
        //        head.Add("CCA");
        //        head.Add("Cap. Allw.");
        //        head.Add("HRA");
        //        head.Add("Cony. Allow.");
        //        head.Add("wash allow.");
        //        head.Add("FPA");
        //        head.Add("Total Allow.");
        //        head.Add("Grand Total");
        //        list.Add(new MembersNominalReportLIst() { Columns = head });

        //        var query = (from designation in context.mStaffDesignation
        //                     select designation).ToList();
        //        int catid = 0;


        //        foreach (var item in query)
        //        {
        //            int roman = 0;
        //            catid++;

        //            List<string> dRow = new List<string>();
        //            dRow.Add("__");
        //            dRow.Add(catid.ToString());
        //            dRow.Add(item.designation + "(" + item.post + ")" + item.text + "+" + item.gradePay + "GP" + "+" + item.SecttPay + "SP");
        //            list.Add(new MembersNominalReportLIst() { Columns = dRow });
        //            var _staffList = (from staff in context.mStaff
        //                              where staff.Designation == item.designation
        //                              && staff.isActive == true && staff.IsReject == false && staff.Group != "ExStaff" && staff.isNominal == true
        //                              select staff).ToList();
        //            if (_staffList.Count() > 0)
        //            {
        //                foreach (var critem in _staffList)
        //                {
        //                    roman++;
        //                    double basic = critem.Basic.HasValue ? critem.Basic.Value : 0;
        //                    double gradepay = critem.gradePay.HasValue ? critem.gradePay.Value : 0;
        //                    double inc = ((basic + gradepay) * Increment_param) / 100;
        //                    double provisionalyear = (basic + gradepay + inc) * 12;
        //                    double secttpay = (item.SecttPay.HasValue ? item.SecttPay.Value : 0)*12;
        //                    double DA = ((provisionalyear + secttpay) * DA_param) / 100;
        //                    double splPay = critem.SplPay.HasValue ? critem.SplPay.Value : 0;
        //                    double cca = CCA_param * 12;
        //                    double cap = CAP_param * 12;
        //                    double hra = critem.hra.HasValue ? critem.hra.Value : 0;
        //                    double conyallow = critem.ConyAllow.HasValue ? critem.ConyAllow.Value : 0;
        //                    double washallow = critem.washAllow.HasValue ? critem.washAllow.Value : 0;
        //                    double fpa = critem.fpa.HasValue ? critem.fpa.Value : 0;
        //                    double totalallow = cca + cap + hra + conyallow + washallow + fpa;
        //                    double grandTotal = provisionalyear + secttpay + DA + cca + cap + hra + conyallow + washallow + fpa;
        //                    grossTotal += grandTotal;
        //                    List<string> Row = new List<string>();
        //                    Row.Add("");
        //                    Row.Add(Roman.ToRoman(roman));
        //                    Row.Add(critem.StaffName.ToUpper());
        //                    Row.Add(new DateTime(1999, critem.monthID >= 1 ? critem.monthID : 1, 1).ToString("MMM") + "-" + critem.yearID);
        //                    Row.Add(Convert.ToInt32(basic).ToString());
        //                    Row.Add(Convert.ToInt32(gradepay).ToString());
        //                    Row.Add(inc.RoundUp().ToString());
        //                    Row.Add(Convert.ToInt32(provisionalyear).ToString());
        //                    Row.Add(Convert.ToInt32(secttpay).ToString());
        //                    Row.Add(Convert.ToInt32(DA).ToString());
        //                    Row.Add(Convert.ToInt32(splPay).ToString());
        //                    Row.Add(Convert.ToInt32(cca).ToString());
        //                    Row.Add(Convert.ToInt32(cap).ToString());
        //                    Row.Add(Convert.ToInt32(hra).ToString());
        //                    Row.Add(Convert.ToInt32(conyallow).ToString());
        //                    Row.Add(Convert.ToInt32(washallow).ToString());
        //                    Row.Add(Convert.ToInt32(fpa).ToString());
        //                    Row.Add(Convert.ToInt64(totalallow).ToString());
        //                    Row.Add(Convert.ToInt64(grandTotal).ToString());
        //                    // grossTotal += critem.amount;
        //                    list.Add(new MembersNominalReportLIst() { Columns = Row });
        //                }
        //                //  Row.Add(grossTotal.ToString());
        //                // Row.Add((grossTotal * 12).ToString());
        //                // grossTotal = 0;
        //            }
        //            if (_staffList.Count() < item.post)
        //            {
        //                for (int i = 0; i < (item.post - _staffList.Count()); i++)
        //                {
        //                    roman++;
        //                    double basic = item.Basic.HasValue ? item.Basic.Value : 0;
        //                    double gradepay = item.gradePay.HasValue ? item.gradePay.Value : 0;
        //                    double inc = ((basic + gradepay) * Increment_param) / 100;
        //                    double provisionalyear = (basic + gradepay + inc) * 12;
        //                    double secttpay = (item.SecttPay.HasValue ? item.SecttPay.Value : 0)*12;
        //                    double DA = ((provisionalyear + secttpay) * DA_param) / 100;
        //                    double splPay = item.SplPay.HasValue ? item.SplPay.Value : 0;
        //                    double cca = CCA_param * 12;
        //                    double cap = CAP_param * 12;
        //                    double hra = 0;
        //                    double conyallow = 0;
        //                    double washallow = 0;
        //                    double fpa = 0;
        //                    double totalallow = cca + cap + hra + conyallow + washallow + fpa;
        //                    double grandTotal = provisionalyear + secttpay + DA + cca + cap + hra + conyallow + washallow + fpa;
        //                    grossTotal += grandTotal;
        //                    List<string> Row = new List<string>();
        //                    Row.Add("");
        //                    //Row.Add(Roman.ToRoman(roman));
        //                    //Row.Add("VACANT");
        //                    //Row.Add("- -");
        //                    //Row.Add(basic.ToString());
        //                    //Row.Add(gradepay.ToString());
        //                    //Row.Add(inc.RoundUp().ToString());
        //                    //Row.Add(provisionalyear.ToString());
        //                    //Row.Add(secttpay.ToString());
        //                    //Row.Add(DA.ToString());
        //                    //Row.Add(splPay.ToString());
        //                    //Row.Add(cca.ToString());
        //                    //Row.Add(cap.ToString());
        //                    //Row.Add(hra.ToString());
        //                    //Row.Add(conyallow.ToString());
        //                    //Row.Add(washallow.ToString());
        //                    //Row.Add(fpa.ToString());
        //                    //Row.Add(totalallow.ToString());
        //                    //Row.Add(grandTotal.ToString());

        //                    Row.Add(Roman.ToRoman(roman));
        //                    Row.Add("VACANT");
        //                    Row.Add("- -");
        //                    Row.Add(Convert.ToInt32(basic).ToString());
        //                    Row.Add(Convert.ToInt32(gradepay).ToString());
        //                    Row.Add(inc.RoundUp().ToString());
        //                    Row.Add(Convert.ToInt32(provisionalyear).ToString());
        //                    Row.Add(Convert.ToInt32(secttpay).ToString());
        //                    Row.Add(Convert.ToInt32(DA).ToString());
        //                    Row.Add(Convert.ToInt32(splPay).ToString());
        //                    Row.Add(Convert.ToInt32(cca).ToString());
        //                    Row.Add(Convert.ToInt32(cap).ToString());
        //                    Row.Add(Convert.ToInt32(hra).ToString());
        //                    Row.Add(Convert.ToInt32(conyallow).ToString());
        //                    Row.Add(Convert.ToInt32(washallow).ToString());
        //                    Row.Add(Convert.ToInt32(fpa).ToString());
        //                    Row.Add(Convert.ToInt64(totalallow).ToString());
        //                    Row.Add(Convert.ToInt64(grandTotal).ToString());


        //                    // grossTotal += critem.amount;
        //                    list.Add(new MembersNominalReportLIst() { Columns = Row });
        //                }
        //            }
        //        }
        //        List<string> footerRow = new List<string>();
        //        footerRow.Add("@@");
        //        footerRow.Add("Total");
        //        footerRow.Add(grossTotal.ToString());

        //        list.Add(new MembersNominalReportLIst() { Columns = footerRow });

        //        //other 1
        //        List<string> other1 = new List<string>();
        //        other1.Add("**");
        //        other1.Add("For Salary");
        //        other1.Add(grossTotal.ToString());

        //        list.Add(new MembersNominalReportLIst() { Columns = other1 });

        //        //other 1
        //        List<string> other2 = new List<string>();
        //        other2.Add("**");
        //        other2.Add("Leave en-cash for retirees");
        //        other2.Add(LECAmount_param.ToString());

        //        list.Add(new MembersNominalReportLIst() { Columns = other2 });

        //        //other 1
        //        List<string> other3 = new List<string>();
        //        other3.Add("**");
        //        other3.Add("LTC");
        //        other3.Add(LTCAmount_param.ToString());

        //        list.Add(new MembersNominalReportLIst() { Columns = other3 });

        //        //other 1
        //        List<string> other4 = new List<string>();
        //        other4.Add("**");
        //        other4.Add("Total");
        //        other4.Add(Math.Round(Convert.ToDouble(grossTotal + LTCAmount_param + LECAmount_param)).ToString());

        //        list.Add(new MembersNominalReportLIst() { Columns = other4 });

        //        nominal.Total = Math.Round(Convert.ToDouble(grossTotal + LTCAmount_param + LECAmount_param));
        //        grossTotal = 0;

        //        return nominal;
        //    }
        //}
    }
}