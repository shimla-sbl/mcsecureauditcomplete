﻿using SBL.DAL;
using SBL.DomainModel.Models.OldSalary;
using SBL.DomainModel.Models.salaryhead;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.salaryhead
{
    internal class membersheadscontext : DBBase<membersheadscontext>
    {
        public membersheadscontext()
            : base("eVidhan")
        {
        }
     
        public virtual DbSet<membersHead> membersHead { get; set; }
        public virtual DbSet<MlaDir_old> MlaDir_old { get; set; }
        public virtual DbSet<MlaAdvances_old> MlaAdvances_old { get; set; }
        public virtual DbSet<MlaAllowances_old> MlaAllowances_old { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "getSalaryDetails": { return getSalaryDetails(param.Parameter); }
                case "getsalaryBillByID": { return getsalaryBillByID(param.Parameter); }
                    //getsalaryBillByID
            }
            return null;
        }

        public static List<oldSalaryViewModel> getSalaryDetails(object param)
        {
            string monthYear = param as string;
            List<oldSalaryViewModel> _oldSalaryViewModel = new List<oldSalaryViewModel>();
            using (membersheadscontext dbcontext = new membersheadscontext())
            {
                var mlaAllow = (from salary in dbcontext.MlaAllowances_old
                                where salary.MonthYear == monthYear
                                select salary
                               ).ToList();
                
                foreach (var item in mlaAllow)
                {
                    _oldSalaryViewModel.Add(new oldSalaryViewModel()
                    {
                        MlaAllowances_old = (MlaAllowances_old)item,
                        MlaAdvances_old = (from advance in dbcontext.MlaAdvances_old
                                           where advance.MonthYear == monthYear
                                           &&
                                           advance.MlaCode == item.MlaCode
                                           select advance
                                             ).ToList(),
                        MlaDir_old = (from mla in dbcontext.MlaDir_old
                                      where mla.MlaCode == item.MlaCode
                                      select mla).FirstOrDefault()
                    });
                }
            }
            return _oldSalaryViewModel;
        }

        public static oldSalaryViewModel getsalaryBillByID(object param)
        {
            int ID = Convert.ToInt32(param);
            oldSalaryViewModel _oldSalaryViewModel = new oldSalaryViewModel();
            using (membersheadscontext dbcontext = new membersheadscontext())
            {
                var mlaAllow = (from salary in dbcontext.MlaAllowances_old
                                where salary.AutoID == ID
                                select salary
                               ).FirstOrDefault();

                if (mlaAllow!=null)
                {
                    _oldSalaryViewModel = new  oldSalaryViewModel()
                    {
                        MlaAllowances_old = (MlaAllowances_old)mlaAllow,
                        MlaAdvances_old = (from advance in dbcontext.MlaAdvances_old
                                           where advance.MonthYear == mlaAllow.MonthYear && advance.MlaCode==mlaAllow.MlaCode                                         
                                           select advance
                                             ).ToList(),
                        MlaDir_old = (from mla in dbcontext.MlaDir_old
                                      where mla.MlaCode == mlaAllow.MlaCode
                                      select mla).FirstOrDefault()
                    };
                }
            }
            return _oldSalaryViewModel;
        }
    }
}