﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.Domain.Context.Assembly;
using SBL.Domain.Context.Session;
using SBL.Domain.Context.SiteSetting;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.User;
using SBL.Service.Common;

namespace SBL.Domain.Context.OneTimeUserRegistration
{
     public class OneTimeUserRegistrationContex : DBBase<OneTimeUserRegistrationContex>
    {
         public OneTimeUserRegistrationContex() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

         public virtual DbSet<mAssembly> mAssemblies { get; set; }
         public virtual DbSet<mSession> mSessions { get; set; }
         public virtual DbSet<tUserRegistrationDetails> tUserRegistrationDetails { get; set; }
         public virtual DbSet<tQuestion> tQuestions { get; set; }
         public virtual DbSet<tOTPRegistrationAuditTrial> tOTPRegistrationAuditTrial { get; set; }
         public virtual DbSet<tMemberNotice> tMemberNotices { get; set; }
         public virtual DbSet<mEvent> mEvents { get; set; }
         public virtual DbSet<tOneTimeUserRegistration> tOneTimeUserRegistration { get; set; }
        
         
         
         public static object Execute(ServiceParameter param)
         {
             if (null == param)
             {
                 return null;
             }

             switch (param.Method)
             {
                 case "GetAssemblySessionList":
                     {
                         return GetAssemblySessionList(param.Parameter);
                     }
                     case "GetSQDetails":
                     {
                         return GetSQDetails(param.Parameter);
                     }
                     case "SaveAuditTrialOTPRegistration":
                     {
                         return SaveAuditTrialOTPRegistration(param.Parameter);
                     }
                     case "GetCountForQuestionTypes":
                     {
                         return GetCountForQuestionTypes(param.Parameter);
                     }
                     case "GetUserDetails":
                     {
                         return GetUserDetails(param.Parameter);
                     }
                     case "UpdateProfileUser":
                     {
                         return UpdateProfileUser(param.Parameter);
                     }
                     case "UpdateDateUserRegistration":
                     {
                         return UpdateDateUserRegistration(param.Parameter);
                     }
                     case "SaveAuditTrialOTPRegistrationNotices":
                     {
                         return SaveAuditTrialOTPRegistrationNotices(param.Parameter);
                     }
                     
             }
             return null;
         }

         public static object GetAssemblySessionList(object param)
         {
             OneTimeUserRegistrationModel model = param as OneTimeUserRegistrationModel;
             OneTimeUserRegistrationContex pCtxt = new OneTimeUserRegistrationContex();
             SessionContext sessionContext = new SessionContext();
             SiteSettingContext settingContext = new SiteSettingContext();
             AssemblyContext AsmCtx = new AssemblyContext();

             if (model.AssemblyID == 0 && model.SessionID == 0)
             {
                 // Get SiteSettings 
                 var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                 var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                 model.SessionID = Convert.ToInt16(SessionCode);
                 model.AssemblyID = Convert.ToInt16(AssemblyCode);
             }

             string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionID && m.AssemblyID == model.AssemblyID select m.SessionName).SingleOrDefault();
             string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyID select m.AssemblyName).SingleOrDefault();

             model.SessionName = SessionName;
             model.AssesmblyName = AssemblyName;

             model.mAssemblyList = (from A in pCtxt.mAssemblies
                                    select A).ToList();
             model.sessionList = (from S in pCtxt.mSessions
                                  where S.AssemblyID == model.AssemblyID
                                  select S).ToList();
   
             return model;
         }


         static object GetCountForQuestionTypes(object param)
         {
             OneTimeUserRegistrationModel model = param as OneTimeUserRegistrationModel;
             OneTimeUserRegistrationContex pCtxt = new OneTimeUserRegistrationContex();


             var StarredCnt = (from questions in pCtxt.tUserRegistrationDetails
                          join E in pCtxt.tQuestions on new { DiaryNumber = questions.DiaryNumber, QuestionType = questions.DocumentTypeId, AssemblyId = questions.AssemblyId, SessionId = questions.SessionId } equals new { DiaryNumber = E.DiaryNumber, QuestionType = E.QuestionType, AssemblyId = E.AssemblyID, SessionId = E.SessionID }
                               where (questions.MobileNumber == model.MobileNumber && questions.AssemblyId == model.AssemblyID && questions.SessionId == model.SessionID && questions.DocumentTypeId == 1)
                          select new OneTimeUserRegistrationModel
                          {
                              Id = questions.Id,
                              AssemblyID = questions.AssemblyId,
                              SessionID = questions.SessionId,
                              DocumentTypeId = questions.DocumentTypeId,
                              DiaryNumber = questions.DiaryNumber,
                              IsSubmittedDate = questions.IsSubmittedDate,
                              MobileNumber = questions.MobileNumber,
                              IsLocked = questions.IsLocked,
                              QuestionId = E.QuestionID,
                              QuestionNumber = E.QuestionNumber,
                              Status = (int)QuestionDashboardStatus.RepliesSent,
                              PaperLaiId = E.PaperLaidId,
                          }).ToList().Count(); 
             
                 model.TotalStaredReceived = StarredCnt;

                 var UnstarredCnt = (from questions in pCtxt.tUserRegistrationDetails
                              join E in pCtxt.tQuestions on new { DiaryNumber = questions.DiaryNumber, QuestionType = questions.DocumentTypeId, AssemblyId = questions.AssemblyId, SessionId = questions.SessionId } equals new { DiaryNumber = E.DiaryNumber, QuestionType = E.QuestionType, AssemblyId = E.AssemblyID, SessionId = E.SessionID }
                              where (questions.MobileNumber == model.MobileNumber && questions.AssemblyId == model.AssemblyID && questions.SessionId == model.SessionID && questions.DocumentTypeId == 2)
                              select new OneTimeUserRegistrationModel
                              {
                                  Id = questions.Id,
                                  AssemblyID = questions.AssemblyId,
                                  SessionID = questions.SessionId,
                                  DocumentTypeId = questions.DocumentTypeId,
                                  DiaryNumber = questions.DiaryNumber,
                                  IsSubmittedDate = questions.IsSubmittedDate,
                                  MobileNumber = questions.MobileNumber,
                                  IsLocked = questions.IsLocked,
                                  QuestionId = E.QuestionID,
                                  QuestionNumber = E.QuestionNumber,
                                  Status = (int)QuestionDashboardStatus.RepliesSent,
                                  PaperLaiId = E.PaperLaidId,
                              }).ToList().Count();

                 model.TotalUnStaredReceived = UnstarredCnt;


                //model.RuleId = (from M in pCtxt.tUserRegistrationDetails
                //               where M.MobileNumber == model.MobileNumber && M.DocumentTypeId == 3
                //               select M.RuleId).FirstOrDefault();
               
               
               var NoticeCount = (from Notices in pCtxt.tUserRegistrationDetails
                            //join E in pCtxt.tMemberNotices on new { NoticeNumber = Notices.DiaryNumber, AssemblyId = Notices.AssemblyId, SessionId = Notices.SessionId } equals new { NoticeNumber = E.NoticeNumber, AssemblyId = E.AssemblyID, SessionId = E.SessionID }                          
                                  where (Notices.MobileNumber == model.MobileNumber && Notices.AssemblyId == model.AssemblyID && Notices.SessionId == model.SessionID && Notices.DocumentTypeId==3)
                            select new OneTimeUserRegistrationModel
                            {
                              
                              // NoticeTypeId = E.NoticeId,
                               //EventName= (from M in pCtxt.mEvents
                               //where  M.EventId == E.NoticeTypeID
                               //select M.EventName).FirstOrDefault(),
                               //AssemblyID = Notices.AssemblyId,
                               //SessionID = Notices.SessionId,
                               //DocumentTypeId = Notices.DocumentTypeId,
                               //DiaryNumber = Notices.DiaryNumber,
                               //IsSubmittedDate = Notices.IsSubmittedDate,
                               //MobileNumber = Notices.MobileNumber,
                               //IsLocked = Notices.IsLocked,

                            }).Count();

               model.TotalNoticesReceived = NoticeCount;
             


             return model;
         }


         public static object GetSQDetails(object param)
         {

             tUserRegistrationDetails model = param as tUserRegistrationDetails;
             OneTimeUserRegistrationContex pCtxt = new OneTimeUserRegistrationContex();

           if (model.DocumentTypeId==1)
           {
             var query = (from questions in pCtxt.tUserRegistrationDetails
                       
                          join E in pCtxt.tQuestions on new { DiaryNumber = questions.DiaryNumber, QuestionType = questions.DocumentTypeId, AssemblyId = questions.AssemblyId, SessionId = questions.SessionId } equals new { DiaryNumber = E.DiaryNumber, QuestionType = E.QuestionType, AssemblyId = E.AssemblyID, SessionId = E.SessionID }
                          orderby questions.IsAssignDate descending
                          where (questions.MobileNumber == model.MobileNumber && questions.AssemblyId== model.AssemblyId && questions.SessionId== model.SessionId && questions.DocumentTypeId==model.DocumentTypeId )
                          select new OneTimeUserRegistrationModel
                          {
                             Id=questions.Id,
                             AssemblyID=questions.AssemblyId,
                             SessionID = questions.SessionId,
                             DocumentTypeId=questions.DocumentTypeId,
                             DiaryNumber=questions.DiaryNumber,
                             IsSubmittedDate=questions.IsSubmittedDate,
                             MobileNumber=questions.MobileNumber,
                             IsLocked=questions.IsLocked,
                             QuestionId=E.QuestionID,
                             QuestionNumber=E.QuestionNumber,
                             Status = (int)QuestionDashboardStatus.RepliesSent,
                             PaperLaiId=E.PaperLaidId,
                             RecieveDateTime=questions.IsAssignDate,
                             Subject =E.Subject,
                             NoticeRecievedTime= E.NoticeRecievedTime,
                          }).ToList();

             int totalRecords = query.Count();
             var results = query.ToList();
             model.OneTimeUserRegistrationModel = results;
            
             return model;
           }
          else if (model.DocumentTypeId == 2)
           {
               var query = (from questions in pCtxt.tUserRegistrationDetails
                            join E in pCtxt.tQuestions on new { DiaryNumber = questions.DiaryNumber, QuestionType = questions.DocumentTypeId, AssemblyId = questions.AssemblyId, SessionId = questions.SessionId } equals new { DiaryNumber = E.DiaryNumber, QuestionType = E.QuestionType, AssemblyId = E.AssemblyID, SessionId = E.SessionID }
                            where (questions.MobileNumber == model.MobileNumber && questions.AssemblyId == model.AssemblyId && questions.SessionId == model.SessionId && questions.DocumentTypeId == model.DocumentTypeId)
                            select new OneTimeUserRegistrationModel
                            {
                                Id = questions.Id,
                                AssemblyID = questions.AssemblyId,
                                SessionID = questions.SessionId,
                                DocumentTypeId = questions.DocumentTypeId,
                                DiaryNumber = questions.DiaryNumber,
                                IsSubmittedDate = questions.IsSubmittedDate,
                                MobileNumber = questions.MobileNumber,
                                IsLocked = questions.IsLocked,
                                QuestionId = E.QuestionID,
                                QuestionNumber = E.QuestionNumber,
                                Status = (int)QuestionDashboardStatus.RepliesSent,
                                PaperLaiId = E.PaperLaidId,
                                RecieveDateTime = questions.IsAssignDate,
                                Subject = E.Subject,
                                NoticeRecievedTime = E.NoticeRecievedTime,
                            }).ToList();

               int totalRecords = query.Count();
               var results = query.ToList();
               model.OneTimeUserRegistrationModel = results;

               return model;
           }
           else if ( model.DocumentTypeId == 3)
           {

               //var RuleId = (from M in pCtxt.tUserRegistrationDetails
               //                where M.MobileNumber == model.MobileNumber && M.DocumentTypeId == model.DocumentTypeId
               //                select M).ToList();
               //foreach (var item in RuleId)
               //{
                  
              
               var query = (from Notices in pCtxt.tUserRegistrationDetails
                            //join E in pCtxt.tMemberNotices on new { NoticeNumber = Notices.DiaryNumber, AssemblyId = Notices.AssemblyId, SessionId = Notices.SessionId ,NoticeId =item.RuleId} equals new { NoticeNumber = E.NoticeNumber, AssemblyId = E.AssemblyID, SessionId = E.SessionID,RuleId=item.RuleId}
                            //join R in pCtxt.mEvents on E.NoticeTypeID equals R.EventId
                            join E in pCtxt.tMemberNotices on Notices.RuleId equals E.NoticeId
                            where (Notices.MobileNumber == model.MobileNumber && Notices.AssemblyId == model.AssemblyId && Notices.SessionId == model.SessionId && Notices.DocumentTypeId == model.DocumentTypeId)
                            select new OneTimeUserRegistrationModel
                            {
                              // EventName=R.EventName,
                               NoticeTypeId = E.NoticeId,
                               EventName= (from M in pCtxt.mEvents
                               where  M.EventId == E.NoticeTypeID
                               select M.EventName).FirstOrDefault(),
                               AssemblyID = Notices.AssemblyId,
                               SessionID = Notices.SessionId,
                               DocumentTypeId = Notices.DocumentTypeId,
                               DiaryNumber = Notices.DiaryNumber,
                               IsSubmittedDate = Notices.IsSubmittedDate,
                               MobileNumber = Notices.MobileNumber,
                               IsLocked = Notices.IsLocked,
                               RecieveDateTime = Notices.IsAssignDate,
                               Subject = E.Subject,
                            }).ToList();
               //}
               int totalRecords = query.Count();
               var results = query.ToList();
               model.OneTimeUserRegistrationModel = results;

               return model;
           }
           return null;
         }
         static object SaveAuditTrialOTPRegistration(object param)
         {
             string msg = "";
             OneTimeUserRegistrationContex pCtxt = new OneTimeUserRegistrationContex();
             tPaperLaidV model = param as tPaperLaidV;
             tOTPRegistrationAuditTrial Obj = new tOTPRegistrationAuditTrial();
             try
             {
                 Obj.DiaryNumber = model.DiaryNumber;
                 Obj.MobileNumber = model.MobileNo;
                 Obj.AssemblyID = model.AssemblyId;
                 Obj.SessionID = model.SessionId;
                 Obj.DocTypeId = model.QuestionTypeId;
                 Obj.MainReplyPdfPath = model.TempPdfFile;
                 Obj.MainReplyDocPath = model.TempDocFile;
                 Obj.OTPId = model.ModifiedBy;
                 Obj.UpdatedDT = model.ModifiedWhen;
                 Obj.MainReplySupDocPath = model.TempSuplDocFile;
                 pCtxt.tOTPRegistrationAuditTrial.Add(Obj);
                 pCtxt.SaveChanges();
                 pCtxt.Close();
             }
             catch (Exception ex)
             {
                 msg = ex.Message;
             }
             return msg;
         }


         static object GetUserDetails(object param)
         {
             string msg = "";
             OneTimeUserRegistrationContex pCtxt = new OneTimeUserRegistrationContex();
             tUserRegistrationDetails model = param as tUserRegistrationDetails;

             var query = (from questions in pCtxt.tOneTimeUserRegistration                       
                          where (questions.MobileNumber == model.MobileNumber)
                          select new OneTimeUserRegistrationModel
                          {
                              UserName = questions.UserName,
                              Addresslocation = questions.AddressLocations,
                             
                          }).ToList();

             int totalRecords = query.Count();
             var results = query.ToList();
             model.OneTimeUserRegistrationModel = results;

             return model;
         }

         static object UpdateProfileUser(object param)
         {

             OneTimeUserRegistrationContex pCtxt = new OneTimeUserRegistrationContex();
             tUserRegistrationDetails model = param as tUserRegistrationDetails;
             tOneTimeUserRegistration obj = pCtxt.tOneTimeUserRegistration.Single(m => m.MobileNumber == model.MobileNumber);
             obj.UserName = model.UserName;
             obj.AddressLocations = model.AddressLocations;
             pCtxt.SaveChanges();

             return null;
         }
         static object UpdateDateUserRegistration(object param)
         {

             OneTimeUserRegistrationContex pCtxt = new OneTimeUserRegistrationContex();
             tPaperLaidV model = param as tPaperLaidV;
             tUserRegistrationDetails obj = pCtxt.tUserRegistrationDetails.Single(m => m.MobileNumber == model.MobileNo && m.DiaryNumber ==model.DiaryNumber);
             obj.IsSubmittedDate = DateTime.Now;
             pCtxt.SaveChanges();

             return null;
         }


         static object SaveAuditTrialOTPRegistrationNotices(object param)
         {
             string msg = "";
             OneTimeUserRegistrationContex pCtxt = new OneTimeUserRegistrationContex();
             tPaperLaidV model = param as tPaperLaidV;
             tOTPRegistrationAuditTrial Obj = new tOTPRegistrationAuditTrial();
             try
             {
                 Obj.DiaryNumber = model.DiaryNumber;
                 Obj.MobileNumber = model.MobileNo;
                 Obj.AssemblyID = model.AssemblyId;
                 Obj.SessionID = model.SessionId;
                 Obj.DocTypeId = model.DocTypeId;
                 Obj.MainReplyDocPath = model.TempDocFile;
                 Obj.OTPId = model.ModifiedBy;
                 Obj.UpdatedDT = model.ModifiedWhen;
                 Obj.RuleId = model.RuleId;   
                 pCtxt.tOTPRegistrationAuditTrial.Add(Obj);
                 pCtxt.SaveChanges();
                 pCtxt.Close();
             }
             catch (Exception ex)
             {
                 msg = ex.Message;
             }
             return msg;
         }

    }
   





}
