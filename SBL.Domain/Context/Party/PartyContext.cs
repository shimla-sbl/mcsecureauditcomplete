﻿using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Party;
using SBL.DomainModel.Models.District;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Member;

namespace SBL.Domain.Context.Party
{
    public class PartyContext : DBBase<PartyContext>
    {

        public PartyContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mParty> mParty { get; set; }
        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateParty": { CreateParty(param.Parameter); break; }

                case "UpdateParty": { return UpdateParty(param.Parameter); }
                case "DeleteParty": { return DeleteParty(param.Parameter); }
                case "GetAllParty": { return GetAllParty(); }
                case "GetPartyById": { return GetPartyById(param.Parameter); }
                case "IsPartyIdChildExist": { return IsPartyIdChildExist(param.Parameter); }
            }
            return null;
        }

        private static object IsPartyIdChildExist(object param)
        {
            try
            {
                using (PartyContext db = new PartyContext())
                {
                    mParty model = (mParty)param;
                    var Res = (from e in db.mMemberAssembly
                               where (e.PartyID == model.PartyID)
                               select e).Count();

                    if (Res == 0)
                    {

                        return false;


                    }

                    else
                    {
                        return true;

                    }

                }
                //return null;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }
        static void CreateParty(object param)
        {
            try
            {
                using (PartyContext db = new PartyContext())
                {
                    mParty model = param as mParty;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mParty.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateParty(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (PartyContext db = new PartyContext())
            {
                mParty model = param as mParty;
                model.CreatedDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mParty.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllParty();
        }

        static object DeleteParty(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (PartyContext db = new PartyContext())
            {
                mParty parameter = param as mParty;
                mParty partyToRemove = db.mParty.SingleOrDefault(a => a.PartyID == parameter.PartyID);
                if (partyToRemove!=null)
                {
                    partyToRemove.IsDeleted = true;
                }
                //db.mParty.Remove(partyToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllParty();
        }

        static List<mParty> GetAllParty()
        {
            PartyContext db = new PartyContext();

           // var query = db.mParty.ToList();
            var query = (from a in db.mParty
                         orderby a.PartyID descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mParty GetPartyById(object param)
        {
            mParty parameter = param as mParty;
            PartyContext db = new PartyContext();
            var query = db.mParty.SingleOrDefault(a => a.PartyID == parameter.PartyID);
            return query;
        }

    }
}
