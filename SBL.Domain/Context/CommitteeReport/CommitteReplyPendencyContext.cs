﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.CommitteeReport;
using SBL.DomainModel.Models.eFile;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.ComplexModel.LinqKitSource;
using SBL.DomainModel.Models.Department;
using PagedList;
using SBL.Domain.Context.eFile;
using SBL.Domain.Context.SiteSetting;


namespace SBL.Domain.Context.CommitteeReport
{
    class CommitteReplyPendencyContext : DBBase<CommitteReplyPendencyContext>
    {
        public CommitteReplyPendencyContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<tCommitteReplyPendency> objCommitteReplyPendency { get; set; }
        public DbSet<tCommittee> objtcommittee { get; set; }
        public DbSet<mCommitteeReplyItemType> objCommitteeReplyItemType { get; set; }
        public DbSet<eFilePaperType> objeFilePaperType { get; set; }
        public DbSet<eFilePaperNature> objeFilePaperNature { get; set; }
        public DbSet<mCommitteeReplyStatus> objCommitteeReplyStatus { get; set; }
        public DbSet<tCommitteeReplyPendencyDetail> tCommitteeReplyPendencyDetail { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFile> eFiles { get; set; }
        public DbSet<tBranchesCommittee> tBranchesCommittee { get; set; }
        public DbSet<tCommitteReplyByMaster> objtCommitteReplyByMaster { get; set; }
        public DbSet<eFileAttachment> eFileAttachment { get; set; }
        public DbSet<pHouseCommitteeItemPendency> pHouseCommitteeItemPendency { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetCommitteList":
                    {
                        return GetCommitteList(param.Parameter);
                    }
                case "GetReplyItemType":
                    {
                        return GetReplyItemType(param.Parameter);
                    }
                case "GeteFilePaperType":
                    {
                        return GeteFilePaperType(param.Parameter);
                    }
                case "GeteFilePaperNature":
                    {
                        return GeteFilePaperNature(param.Parameter);
                    }
                case "GetReplyPendencyForReportType":
                    {
                        return GetReplyPendencyForReportType(param.Parameter);
                    }
                case "GetReplyPendencyForReportTypeFDpt":
                    {
                        return GetReplyPendencyForReportTypeFDpt(param.Parameter);
                    }
                case "GetReplyPendencyForReportTypeFDptWD":
                    {
                        return GetReplyPendencyForReportTypeFDptWD(param.Parameter);
                    }
                case "GetReplyPendencyForItemTypeFDptWD":
                    {
                        return GetReplyPendencyForItemTypeFDptWD(param.Parameter);
                    }
                case "GetReplyPendencyForItemType":
                    {
                        return GetReplyPendencyForItemType(param.Parameter);
                    }
                case "GetReplyPendencyForItemTypeFDpt":
                    {
                        return GetReplyPendencyForItemTypeFDpt(param.Parameter);
                    }
                case "GetReplyPendencyForItemTypeAll":
                    {
                        return GetReplyPendencyForItemTypeAll(param.Parameter);
                    }
                case "GetCommitteReplyStatus":
                    {
                        return GetCommitteReplyStatus(param.Parameter);
                    }
                case "SaveCommiteeReplyEntryForm":
                    {
                        return SaveCommiteeReplyEntryForm(param.Parameter);
                    }
                case "SaveByDepartmentOnly":
                    {
                        return SaveByDepartmentOnly(param.Parameter);
                    }

                case "CommitteReplyPendencyForEdit":
                    {
                        return CommitteReplyPendencyForEdit(param.Parameter);
                    }
                case "CommiteePendencyDetailNewReply":
                    {
                        return CommiteePendencyDetailNewReply(param.Parameter);
                    }
                case "ReplyDetailListWithPendencyID":
                    {
                        return ReplyDetailListWithPendencyID(param.Parameter);
                    }
                case "GetPendencyRecordFromReplyID":
                    {
                        return GetPendencyRecordFromReplyID(param.Parameter);
                    }
                case "saveEditNewDeptReplyContext":
                    {
                        return saveEditNewDeptReplyContext(param.Parameter);
                    }
                case "saveEditNewComitteeReplyContext":
                    {
                        return saveEditNewComitteeReplyContext(param.Parameter);
                    }
                case "saveEditNewQuesionforComitteeContext":
                    {
                        return saveEditNewQuesionforComitteeContext(param.Parameter);
                    }
                case "DeletePendencyNewEntryByPendencyId":
                    {
                        return DeletePendencyNewEntryByPendencyId(param.Parameter);
                    }
                case "DeletePendencyReplyDetailsByReplyIdContext":
                    {
                        return DeletePendencyReplyDetailsByReplyIdContext(param.Parameter);
                    }
                case "saveOldNewPendencyReplyContext":
                    {
                        return saveOldNewPendencyReplyContext(param.Parameter);
                    }
                case "eFileWithdepartment":
                    {
                        return eFileWithdepartment(param.Parameter);
                    }
                case "ItemNoCheck":
                    {
                        return ItemNoCheck(param.Parameter);
                    }
                case "GetReplyByMasterRecord":
                    {
                        return GetReplyByMasterRecord(param.Parameter);
                    }
                case "ToVerifyAttachedPaper":
                    {
                        return ToVerifyAttachedPaper(param.Parameter);
                    }
                case "GetReplyPendencyInGriD":
                    {
                        return GetReplyPendencyInGriD(param.Parameter);
                    }
                case "GetReplyPendencyInGriD_Jquerygrid":
                    {
                        return GetReplyPendencyInGriD_Jquerygrid(param.Parameter);
                    }
                case "ToRejectAttachedPaper":
                    {
                        return ToRejectAttachedPaper(param.Parameter);
                    }
                case "GetReplyPendencySummaryCount":
                    {
                        return GetReplyPendencySummaryCount(param.Parameter);//new requirement to display count
                    }
                //New Pendency
                case "SaveNewPendency":
                    {
                        return SaveNewPendency(param.Parameter);//new requirement to display count
                    }
                case "GetAllPendencyNew":
                    {
                        return GetAllPendencyNew(param.Parameter);//new requirement to display count
                    }
                case "GetDetailsByParentId":
                    {
                        return GetDetailsByParentId(param.Parameter);//new requirement to display count
                    }
                case "GetCustomItemPendency":
                    {
                        return GetCustomItemPendency(param.Parameter);//new requirement to display count
                    }
                case "GetItemPendencyByParentId":
                    {
                        return GetItemPendencyByParentId(param.Parameter);//new requirement to display count
                    }

                case "GetItemPendencyByParentIdforLinking":
                    {
                        return GetItemPendencyByParentIdforLinking(param.Parameter);//new requirement to display count
                    }
                case "GetPendencyListforLinking":
                    {
                        return GetPendencyListforLinking(param.Parameter);//new requirement to display count
                    }
                case "CheckItemNumberExists":
                    {
                        return CheckItemNumberExists(param.Parameter);//new requirement
                    }
                case "LockMainPendency":
                    {
                        return LockMainPendency(param.Parameter);//new requirement
                    }
                case "LockSubPendency":
                    {
                        return LockSubPendency(param.Parameter);//new requirement
                    }
                case "GetDetailsByPendencyId":
                    {
                        return GetDetailsByPendencyId(param.Parameter);//new requirement
                    }
                case "GetDetailsBySubPendencyId":
                    {
                        return GetDetailsBySubPendencyId(param.Parameter);//new requirement
                    }
                case "UpdateMainPendency":
                    {
                        return UpdateMainPendency(param.Parameter);//new requirement
                    }
                case "UpdateSubPendency":
                    {
                        return UpdateSubPendency(param.Parameter);//new requirement
                    }
                case "GetDetailsByRecordId":
                    {
                        return GetDetailsByRecordId(param.Parameter);//new requirement to display count
                    }
                case "GetDataBySearchParameter":
                    {
                        return GetDataBySearchParameter(param.Parameter);//new requirement to search record
                    }


            }

            return null;
        }

        public static object GetCommitteList(object param)
        {
            try
            {
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from cmt in db.objtcommittee select cmt).ToList();

                    return query;
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetReplyItemType(object param)
        {
            try
            {
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objCommitteeReplyItemType
                                 select a
                                 );
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetReplyByMasterRecord(object param)
        {
            try
            {
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objtCommitteReplyByMaster
                                 select a
                                 );
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GeteFilePaperType(object param)
        {
            try
            {
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objeFilePaperType
                                 select a
                                 );
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GeteFilePaperNature(object param)
        {
            try
            {
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objeFilePaperNature
                                 select a
                                 );
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetReplyPendencyForReportType(object param)
        {
            try
            {
                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                var predicate = PredicateBuilder.True<tCommitteReplyPendency>();
                if (obj.CommitteeId != 0)
                    predicate = predicate.And(q => q.CommitteeId == obj.CommitteeId);
                if (obj.DepartmentId != null)
                    predicate = predicate.And(q => q.DepartmentId == obj.DepartmentId);
                if (obj.Status != 0)
                    predicate = predicate.And(q => q.Status == obj.Status);

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objCommitteReplyPendency
                                 where obj.ItemTypeId == a.ItemTypeId
                                 && obj.FinancialYear == a.FinancialYear
                                 select a).AsExpandable().Where(predicate).ToList();
                    foreach (var lst in query)
                    {
                        lst.StatusName = (from a in db.objCommitteReplyPendency
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where obj.ItemTypeId == a.ItemTypeId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                        lst.DepartmentName = (from a in db.objCommitteReplyPendency
                                              join b in db.mDepartment on a.DepartmentId equals b.deptId
                                              where lst.DepartmentId == b.deptId
                                              select b.deptname).FirstOrDefault();
                        lst.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                            join b in db.mDepartment on a.DepartmentId equals b.deptId
                                            where lst.DepartmentId == b.deptId
                                            select b.deptabbr).FirstOrDefault();
                    }
                    return query;
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetReplyPendencyForReportTypeFDpt(object param)
        {
            try
            {
                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                var predicate = PredicateBuilder.True<tCommitteReplyPendency>();
                if (obj.CommitteeId != 0)
                    predicate = predicate.And(q => q.CommitteeId == obj.CommitteeId);
                if (obj.DepartmentId != null)
                    predicate = predicate.And(q => q.DepartmentId == obj.DepartmentId);
                if (obj.ReportTypeId != null)
                    predicate = predicate.And(q => q.ReportTypeId == obj.ReportTypeId);
                if (obj.ReportNatureId != null)
                    predicate = predicate.And(q => q.ReportNatureId == obj.ReportNatureId);
                if (obj.Status != 0)
                    predicate = predicate.And(q => q.Status == obj.Status);

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objCommitteReplyPendency
                                 where obj.ItemTypeId == a.ItemTypeId
                                 && a.IsFreeze == true
                                 select a).AsExpandable().Where(predicate).ToList();

                    foreach (var lst in query)
                    {
                        lst.StatusName = (from a in db.objCommitteReplyPendency
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where obj.ItemTypeId == a.ItemTypeId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                        lst.DepartmentName = (from a in db.objCommitteReplyPendency
                                              join b in db.mDepartment on a.DepartmentId equals b.deptId
                                              where lst.DepartmentId == b.deptId
                                              select b.deptname).FirstOrDefault();
                    }
                    return query;
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetReplyPendencyForItemTypeFDptWD(object param)
        {
            try
            {
                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                string csv = obj.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);

                int? CommitteeId = null; int? ItemTypeId = null; int? Status = null;
                if (obj.CommitteeId != 0)
                {
                    CommitteeId = obj.CommitteeId;
                }
                if (obj.ItemTypeId != null)
                {
                    ItemTypeId = obj.ItemTypeId;
                }
                if (obj.Status != 0)
                {
                    Status = obj.Status;
                }


                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var query = (from a in db.objCommitteReplyPendency
                                 join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                 join c in db.mDepartment on a.DepartmentId equals c.deptId
                                 join d in db.objtCommitteReplyByMaster on a.ItemTypeId equals d.PendencyByFkId
                                 where obj.ItemTypeId == d.PendencyByFkId
                               && (!CommitteeId.HasValue || a.CommitteeId == CommitteeId)
                               && (!Status.HasValue || a.Status == Status)
                               && (ids.Contains(a.DepartmentId))
                               && a.IsFreeze == true
                                 select new
                                 {
                                     TypeNo = a.TypeNo,
                                     ItemDescription = a.ItemDescription,
                                     FinancialYear = a.FinancialYear,
                                     SentOnDate = a.SentOnDate,
                                     LastReminderSentDate = a.LastReminderSentDate,
                                     StatusName = b.ReplyStatusName,
                                     FileNumber = a.FileNumber,
                                     PendencyId = a.PendencyId,
                                     departmentName = c.deptname
                                 }).ToList();
                    List<tCommitteReplyPendency> ls2 = new List<tCommitteReplyPendency>();
                    foreach (var item in query)
                    {
                        tCommitteReplyPendency obj1 = new tCommitteReplyPendency();
                        obj1.TypeNo = item.TypeNo;
                        obj1.ItemDescription = item.ItemDescription;
                        obj1.FinancialYear = item.FinancialYear;
                        obj1.SentOnDate = item.SentOnDate;
                        obj1.LastReminderSentDate = item.LastReminderSentDate;
                        obj1.StatusName = item.StatusName;
                        obj1.FileNumber = item.FileNumber;
                        obj1.PendencyId = item.PendencyId;
                        obj1.DepartmentName = item.departmentName;
                        ls2.Add(obj1);
                    }
                    return ls2;
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetReplyPendencyForReportTypeFDptWD(object param)
        {
            try
            {
                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                string csv = obj.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);

                int? CommitteeId = null; int? ReportTypeId = null; int? Status = null;
                if (obj.CommitteeId != 0)
                {
                    CommitteeId = obj.CommitteeId;
                }
                if (obj.ReportTypeId != null)
                {
                    ReportTypeId = obj.ReportTypeId;
                }
                if (obj.Status != 0)
                {
                    Status = obj.Status;
                }

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objCommitteReplyPendency
                                 join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                 join c in db.mDepartment on a.DepartmentId equals c.deptId
                                 join d in db.objtCommitteReplyByMaster on a.ItemTypeId equals d.PendencyByFkId
                                 where obj.ItemTypeId == d.PendencyByFkId
                               && (!CommitteeId.HasValue || a.CommitteeId == CommitteeId)
                               && (a.IsFreeze == true)
                               && (!ReportTypeId.HasValue || a.ReportTypeId == ReportTypeId)
                               && (!Status.HasValue || a.Status == Status)
                               && (ids.Contains(a.DepartmentId))
                                 select new
                                 {
                                     TypeNo = a.TypeNo,
                                     ItemDescription = a.ItemDescription,
                                     FinancialYear = a.FinancialYear,
                                     SentOnDate = a.SentOnDate,
                                     LastReminderSentDate = a.LastReminderSentDate,
                                     StatusName = b.ReplyStatusName,
                                     FileNumber = a.FileNumber,
                                     PendencyId = a.PendencyId,
                                     departmentName = c.deptname
                                 }).ToList();
                    List<tCommitteReplyPendency> ls2 = new List<tCommitteReplyPendency>();
                    foreach (var item in query)
                    {
                        tCommitteReplyPendency obj1 = new tCommitteReplyPendency();
                        obj1.TypeNo = item.TypeNo;
                        obj1.ItemDescription = item.ItemDescription;
                        obj1.FinancialYear = item.FinancialYear;
                        obj1.SentOnDate = item.SentOnDate;
                        obj1.LastReminderSentDate = item.LastReminderSentDate;
                        obj1.StatusName = item.StatusName;
                        obj1.FileNumber = item.FileNumber;
                        obj1.PendencyId = item.PendencyId;
                        obj1.DepartmentName = item.departmentName;
                        ls2.Add(obj1);
                    }
                    return ls2;
                }
            }
            catch
            {
                throw;
            }
        }

        //public static object GetReplyPendencyForItemType(object param)
        //{
        //    try
        //    {
        //        tCommitteReplyPendency obj = param as tCommitteReplyPendency;

        //        int? CommitteeId = null; string DepartmentId = null; int? ItemTypeId = null; int? Status = null; string Financialyear = null;
        //        if (obj.CommitteeId != 0)
        //        {
        //            CommitteeId = obj.CommitteeId;
        //        }
        //        if (obj.DepartmentId != null)
        //        {
        //            DepartmentId = obj.DepartmentId;
        //        }
        //        if (obj.ItemTypeId != null)
        //        {
        //            ItemTypeId = obj.ItemTypeId;
        //        }
        //        if (obj.Status != 0)
        //        {
        //            Status = obj.Status;
        //        }
        //        if (obj.FinancialYear != "Select")
        //        {
        //            Financialyear = obj.FinancialYear;
        //        }
        //        else
        //        {
        //            Financialyear = null;
        //        }

        //        using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
        //        {
        //            var query = (from a in db.objCommitteReplyPendency
        //                         join b in db.objCommitteeReplyStatus on a.Status equals b.Id
        //                         join c in db.mDepartment on a.DepartmentId equals c.deptId
        //                         join d in db.objtCommitteReplyByMaster on a.ItemTypeId equals d.PendencyByFkId
        //                         //join e in db.eFiles on a.FileNumber equals e.eFileID
        //                         where obj.ItemTypeId == d.PendencyByFkId
        //                         && obj.FinancialYear == a.FinancialYear
        //                       && (!CommitteeId.HasValue || a.CommitteeId == CommitteeId)
        //                       && (obj.DepartmentId == null || obj.DepartmentId == string.Empty || a.DepartmentId == DepartmentId)
        //                       && (obj.FinancialYear == null || obj.FinancialYear == string.Empty || a.FinancialYear == Financialyear)
        //                       && (!Status.HasValue || a.Status == Status)

        //                         select new
        //                         {
        //                             TypeNo = a.TypeNo,
        //                             ItemDescription = a.ItemDescription,
        //                             FinancialYear = a.FinancialYear,
        //                             SentOnDate = a.SentOnDate,
        //                             LastReminderSentDate = a.LastReminderSentDate,
        //                             Subject = a.Subject,
        //                             StatusName = b.ReplyStatusName,
        //                             //FileNumber = e.eFileNumber,
        //                             FileNumber = (from z in db.eFiles
        //                                           join f in db.objCommitteReplyPendency on z.eFileID equals f.FileNumber
        //                                           where z.eFileID == f.FileNumber
        //                                           select z.eFileNumber).FirstOrDefault(),
        //                             //eFileID = e.eFileID,
        //                             eFileID = (from z in db.eFiles
        //                                        join f in db.objCommitteReplyPendency on z.eFileID equals f.FileNumber
        //                                        where z.eFileID == f.FileNumber
        //                                        select z.eFileID).FirstOrDefault(),
        //                             //eFileSubject = e.eFileSubject,
        //                             // eFileSubject = ((from z in db.eFiles where z.eFileID == a.FileNumber select z.eFileSubject).FirstOrDefault()),
        //                             eFileSubject = (from z in db.eFiles
        //                                             join f in db.objCommitteReplyPendency on z.eFileID equals f.FileNumber
        //                                             where z.eFileID == f.FileNumber
        //                                             select z.eFileSubject).FirstOrDefault(),
        //                             PendencyId = a.PendencyId,
        //                             CagPageNo = a.CagPageNo,
        //                             departmentName = c.deptname,
        //                             AbbrDeptName = c.deptabbr,
        //                             CreatedDate = a.CreatedDate,
        //                             OrderString1 = a.OrderString1,
        //                             OrderString2 = a.OrderString2,
        //                             OrderString3 = a.OrderString3,
        //                             OrderString4 = a.OrderString4,
        //                             OrderString5 = a.OrderString5,
        //                         }).ToList().OrderBy(x => x.FinancialYear).ThenBy(x => x.departmentName).ThenBy(x => x.OrderString1).ThenBy(x => x.OrderString2).ThenBy(x => x.OrderString3).ThenBy(x => x.OrderString4).ThenBy(x => x.OrderString5);
        //            List<tCommitteReplyPendency> ls2 = new List<tCommitteReplyPendency>();
        //            foreach (var item in query)
        //            {
        //                tCommitteReplyPendency obj1 = new tCommitteReplyPendency();
        //                obj1.TypeNo = item.TypeNo;
        //                obj1.ItemDescription = item.ItemDescription;
        //                obj1.FinancialYear = item.FinancialYear;
        //                obj1.SentOnDate = item.SentOnDate;
        //                obj1.LastReminderSentDate = item.LastReminderSentDate;
        //                obj1.Subject = item.Subject;
        //                obj1.StatusName = item.StatusName;
        //                obj1.eFileNumber = item.FileNumber;
        //                obj1.PendencyId = item.PendencyId;
        //                obj1.CagPageNo = item.CagPageNo;
        //                obj1.DepartmentName = item.departmentName;
        //                obj1.AbbrDeptName = item.AbbrDeptName;
        //                obj1.eFileID = item.eFileID;
        //                obj1.Subject = item.Subject;
        //                obj1.CreatedDate = item.CreatedDate;
        //                obj1.OrderString1 = item.OrderString1;
        //                obj1.OrderString2 = item.OrderString2;
        //                obj1.OrderString3 = item.OrderString3;
        //                obj1.OrderString4 = item.OrderString4;
        //                obj1.OrderString5 = item.OrderString5;
        //                ls2.Add(obj1);
        //            }
        //            return ls2;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public static object GetReplyPendencyForItemType(object param)//done by predicate //
        {
            try
            {
                var pagesize = 1;

                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                pagesize = obj.page;

                var predicate = PredicateBuilder.True<tCommitteReplyPendency>();
                if (obj.CommitteeId != 0)
                    predicate = predicate.And(q => q.CommitteeId == obj.CommitteeId);
                if (obj.DepartmentId != null)
                    predicate = predicate.And(q => q.DepartmentId == obj.DepartmentId);
                if (obj.ItemTypeId != null)
                    predicate = predicate.And(q => q.ItemTypeId == obj.ItemTypeId);
                if (obj.FinancialYear != "Select")
                    predicate = predicate.And(q => q.FinancialYear == obj.FinancialYear);
                if (obj.Status != 1001)
                {
                    if (obj.Status != 0)
                        predicate = predicate.And(q => q.Status == obj.Status);
                }

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    //var query = (from a in db.objCommitteReplyPendency
                    //             where obj.ItemTypeId == a.ItemTypeId
                    //             orderby a.FinancialYear,a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
                    //             select a).AsExpandable().Where(predicate).ToPagedList(pagesize,10);

                    var query = (from a in db.objCommitteReplyPendency
                                 where obj.ItemTypeId == a.ItemTypeId
                                 orderby a.FinancialYear, a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
                                 select a).AsExpandable().Where(predicate).Take(500).ToList();

                    foreach (var lst in query)
                    {
                        lst.StatusName = (from a in db.objCommitteReplyPendency
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where lst.ItemTypeId == a.ItemTypeId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                        lst.DepartmentName = (from a in db.objCommitteReplyPendency
                                              join b in db.mDepartment on a.DepartmentId equals b.deptId
                                              where lst.DepartmentId == b.deptId
                                              && lst.ItemTypeId == a.ItemTypeId
                                              select b.deptname).FirstOrDefault();
                        lst.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                            join b in db.mDepartment on a.DepartmentId equals b.deptId
                                            where lst.DepartmentId == b.deptId
                                            && lst.ItemTypeId == a.ItemTypeId
                                            select b.deptabbr).FirstOrDefault();
                        lst.eFileNumber = (from m in db.eFiles
                                           where m.eFileID == lst.FileNumber
                                           select m.eFileNumber).FirstOrDefault();
                        lst.eFileID = (from m in db.eFiles
                                       where m.eFileID == lst.FileNumber
                                       select m.eFileID).FirstOrDefault();
                        lst.eFileSubject = (from m in db.eFiles
                                            where m.eFileID == lst.FileNumber
                                            select m.eFileSubject).FirstOrDefault();
                    }
                    return query;
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetReplyPendencyForItemTypeAll(object param)
        {
            try
            {
                var pagesize = 1;

                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                pagesize = obj.page;

                var predicate = PredicateBuilder.True<tCommitteReplyPendency>();
                if (obj.CommitteeId != 0)
                    predicate = predicate.And(q => q.CommitteeId == obj.CommitteeId);
                if (obj.DepartmentId != null)
                    predicate = predicate.And(q => q.DepartmentId == obj.DepartmentId);
                if (obj.ItemTypeId != null)
                    predicate = predicate.And(q => q.ItemTypeId == obj.ItemTypeId);
                //if (obj.FinancialYear != "Select")
                //    predicate = predicate.And(q => q.FinancialYear == obj.FinancialYear);
                if (obj.Status != 0)
                    predicate = predicate.And(q => q.Status == obj.Status);

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    //var query = (from a in db.objCommitteReplyPendency
                    //             where obj.ItemTypeId == a.ItemTypeId
                    //             orderby a.FinancialYear,a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
                    //             select a).AsExpandable().Where(predicate).ToPagedList(pagesize,10);

                    var query = (from a in db.objCommitteReplyPendency
                                 where obj.ItemTypeId == a.ItemTypeId
                                 orderby a.FinancialYear, a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
                                 select a).AsExpandable().Where(predicate).ToList();

                    foreach (var lst in query)
                    {
                        lst.StatusName = (from a in db.objCommitteReplyPendency
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where lst.ItemTypeId == a.ItemTypeId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                        lst.DepartmentName = (from a in db.objCommitteReplyPendency
                                              join b in db.mDepartment on a.DepartmentId equals b.deptId
                                              where lst.DepartmentId == b.deptId
                                              && lst.ItemTypeId == a.ItemTypeId
                                              select b.deptname).FirstOrDefault();
                        lst.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                            join b in db.mDepartment on a.DepartmentId equals b.deptId
                                            where lst.DepartmentId == b.deptId
                                            && lst.ItemTypeId == a.ItemTypeId
                                            select b.deptabbr).FirstOrDefault();
                        lst.eFileNumber = (from m in db.eFiles
                                           where m.eFileID == lst.FileNumber
                                           select m.eFileNumber).FirstOrDefault();
                        lst.eFileID = (from m in db.eFiles
                                       where m.eFileID == lst.FileNumber
                                       select m.eFileID).FirstOrDefault();
                        lst.eFileSubject = (from m in db.eFiles
                                            where m.eFileID == lst.FileNumber
                                            select m.eFileSubject).FirstOrDefault();
                    }
                    return query;
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetReplyPendencyForItemTypeFDpt(object param)//alternative for predicate function
        {
            try
            {
                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                int? CommitteeId = null; string DepartmentId = null; int? ItemTypeId = null; int? Status = null;
                if (obj.CommitteeId != 0)
                {
                    CommitteeId = obj.CommitteeId;
                }
                if (obj.DepartmentId != null)
                {
                    DepartmentId = obj.DepartmentId;
                }
                if (obj.ItemTypeId != null)
                {
                    ItemTypeId = obj.ItemTypeId;
                }
                if (obj.Status != 0)
                {
                    Status = obj.Status;
                }

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objCommitteReplyPendency
                                 join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                 join c in db.mDepartment on a.DepartmentId equals c.deptId
                                 join d in db.objtCommitteReplyByMaster on a.ItemTypeId equals d.PendencyByFkId
                                 where obj.ItemTypeId == d.PendencyByFkId
                               && (!CommitteeId.HasValue || a.CommitteeId == CommitteeId)
                               && (obj.DepartmentId == null || obj.DepartmentId == string.Empty || a.DepartmentId == DepartmentId)
                                     //&& (!ItemTypeId.HasValue || a.ItemTypeId == ItemTypeId)
                               && (!Status.HasValue || a.Status == Status)
                               && a.IsFreeze == true
                                 select new
                                 {
                                     TypeNo = a.TypeNo,
                                     ItemDescription = a.ItemDescription,
                                     FinancialYear = a.FinancialYear,
                                     SentOnDate = a.SentOnDate,
                                     LastReminderSentDate = a.LastReminderSentDate,
                                     StatusName = b.ReplyStatusName,
                                     FileNumber = a.FileNumber,
                                     PendencyId = a.PendencyId,
                                     departmentName = c.deptname
                                 }).ToList();
                    List<tCommitteReplyPendency> ls2 = new List<tCommitteReplyPendency>();
                    foreach (var item in query)
                    {
                        tCommitteReplyPendency obj1 = new tCommitteReplyPendency();
                        obj1.TypeNo = item.TypeNo;
                        obj1.ItemDescription = item.ItemDescription;
                        obj1.FinancialYear = item.FinancialYear;
                        obj1.SentOnDate = item.SentOnDate;
                        obj1.LastReminderSentDate = item.LastReminderSentDate;
                        obj1.StatusName = item.StatusName;
                        obj1.FileNumber = item.FileNumber;
                        obj1.PendencyId = item.PendencyId;
                        obj1.DepartmentName = item.departmentName;
                        ls2.Add(obj1);
                    }
                    return ls2;
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetCommitteReplyStatus(object param)
        {
            try
            {
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var query = (from a in db.objCommitteeReplyStatus
                                 where a.IsActive == true
                                 select a);
                    return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        public static object SaveCommiteeReplyEntryForm(object param)
        {
            try
            {
                tCommitteReplyPendency model = param as tCommitteReplyPendency;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.objCommitteReplyPendency
                                   where a.PendencyId == model.PendencyId
                                   select a).FirstOrDefault();

                    if (content == null)//Add logic
                    {
                        model.CreatedDate = Convert.ToString(DateTime.Today);
                        db.objCommitteReplyPendency.Add(model);
                        db.SaveChanges();
                        db.Close();
                    }
                    else
                    {
                        content.AssemblyID = model.AssemblyID;
                        content.CommitteeId = model.CommitteeId;
                        content.DepartmentId = model.DepartmentId;
                        content.eFilePaperNatureList = model.eFilePaperNatureList;
                        content.eFilePaperTypeList = model.eFilePaperTypeList;
                        content.FileNumber = model.FileNumber;
                        content.FinancialYear = model.FinancialYear;
                        content.IsFreeze = model.IsFreeze;
                        content.ItemDescription = model.ItemDescription;
                        content.ItemTypeId = model.ItemTypeId;
                        content.LastReminderSentDate = model.LastReminderSentDate;
                        content.LastReminderSentDateView = model.LastReminderSentDateView;
                        content.LatestReplyId = model.LatestReplyId;
                        content.mCommitteeReplyItemTypeList = model.mCommitteeReplyItemTypeList;
                        content.mCommitteeReplyStatusList = model.mCommitteeReplyStatusList;
                        content.mDepartmentList = model.mDepartmentList;
                        //content.PendencyBy = model.PendencyBy;
                        content.PendencyId = model.PendencyId;
                        content.ReportNatureId = model.ReportNatureId;
                        content.ReportTypeId = model.ReportTypeId;
                        content.SentOnDate = model.SentOnDate;
                        content.SentOnDateVIew = model.SentOnDateVIew;
                        content.SessionDate = model.SessionDate;
                        content.SessionID = model.SessionID;
                        content.Status = model.Status;
                        content.StatusName = model.StatusName;
                        content.Subject = model.Subject;
                        content.OrderString1 = model.OrderString1;
                        content.OrderString2 = model.OrderString2;
                        content.OrderString3 = model.OrderString3;
                        content.OrderString4 = model.OrderString4;
                        content.OrderString5 = model.OrderString5;
                        content.CagPageNo = model.CagPageNo;
                        content.tCommitteeList = model.tCommitteeList;
                        content.tCommittePendencyList = model.tCommittePendencyList;
                        content.TypeNo = model.TypeNo;
                        db.objCommitteReplyPendency.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();
                    }
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SaveByDepartmentOnly(object param)
        {
            try
            {
                tCommitteReplyPendency model = param as tCommitteReplyPendency;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.objCommitteReplyPendency
                                   where a.PendencyId == model.PendencyId
                                   select a).FirstOrDefault();

                    if (content == null)//Add logic
                    {
                        model.CreatedDate = Convert.ToString(DateTime.Today);
                        db.objCommitteReplyPendency.Add(model);
                        db.SaveChanges();
                        db.Close();
                    }
                    else
                    {
                        content.ItemDescription = model.ItemDescription;
                        //content.PendencyBy = model.PendencyBy;                       
                        content.Subject = model.Subject;
                        db.objCommitteReplyPendency.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();
                    }
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        public static object CommitteReplyPendencyForEdit(object param)
        {
            try
            {
                var variable = Convert.ToInt16(param);
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.objCommitteReplyPendency
                                   where a.PendencyId == variable
                                   select a).FirstOrDefault();

                    return content;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object CommiteePendencyDetailNewReply(object param)
        {
            try
            {
                tCommitteeReplyPendencyDetail model = param as tCommitteeReplyPendencyDetail;
                var b = model.PendencyReplyId;
                var c = model.PendencyId;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.tCommitteeReplyPendencyDetail
                                   where a.PendencyReplyId == model.PendencyReplyId
                                   select a).FirstOrDefault();

                    var query = (from a in db.objCommitteReplyPendency
                                 where a.PendencyId == c
                                 select a).FirstOrDefault();
                    query.Status = model.Status ?? 0;
                    db.objCommitteReplyPendency.Attach(query);
                    db.Entry(query).State = EntityState.Modified;
                    db.SaveChanges();


                    if (content == null)//Add logic
                    {
                        model.CreatedDate = DateTime.Today;
                        db.tCommitteeReplyPendencyDetail.Add(model);
                        db.SaveChanges();
                        //db.Close();

                        var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                             where a.PendencyId == model.PendencyId
                                             select a).ToList();
                        db.Close();
                        return returncontent;
                    }
                    else
                    {
                        var content1 = (from a in db.tCommitteeReplyPendencyDetail
                                        where a.PendencyId == model.PendencyId
                                        orderby a.PendencyReplyId descending
                                        select a).FirstOrDefault();
                        var query1 = (from a in db.objCommitteReplyPendency
                                      where a.PendencyId == c
                                      select a).FirstOrDefault();
                        if (content1.PendencyReplyId == model.PendencyReplyId)
                        {
                            query1.Status = model.Status ?? 0;
                            db.objCommitteReplyPendency.Attach(query1);
                            db.Entry(query1).State = EntityState.Modified;
                            db.SaveChanges();
                        }

                        db.tCommitteeReplyPendencyDetail.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();


                        var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                             where a.PendencyId == model.PendencyId
                                             select a).ToList();
                        db.Close();
                        return returncontent;
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object ReplyDetailListWithPendencyID(object param)
        {
            try
            {
                var variable = Convert.ToInt16(param);
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.tCommitteeReplyPendencyDetail
                                   where a.PendencyId == variable
                                   select a).ToList();
                    foreach (var lst in content)
                    {
                        lst.StatusName = (from a in db.tCommitteeReplyPendencyDetail
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where variable == a.PendencyId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                    }
                    return content;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object GetPendencyRecordFromReplyID(object param)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = param as tCommitteeReplyPendencyDetail;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.tCommitteeReplyPendencyDetail
                                   where a.PendencyId == obj.PendencyId
                                   && a.PendencyReplyId == obj.PendencyReplyId
                                   select a).FirstOrDefault();
                    return content;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object saveEditNewDeptReplyContext(object param)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = param as tCommitteeReplyPendencyDetail;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    if (obj.PendencyReplyId == 0)
                    {

                        obj.CreatedDate = DateTime.Today;
                        obj.ReplyDatetime = DateTime.Now;
                        db.tCommitteeReplyPendencyDetail.Add(obj);
                        db.SaveChanges();


                        var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                             where a.PendencyId == obj.PendencyId
                                             select a).ToList();
                        foreach (var lst in returncontent)
                        {
                            lst.StatusName = (from a in db.tCommitteeReplyPendencyDetail
                                              join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                              where obj.PendencyId == a.PendencyId
                                              && lst.Status == b.Id
                                              select b.ReplyStatusName).FirstOrDefault();
                        }
                        db.Close();
                        return returncontent;
                    }
                    else
                    {
                        var content = (from a in db.tCommitteeReplyPendencyDetail
                                       where a.PendencyId == obj.PendencyId
                                       && a.PendencyReplyId == obj.PendencyReplyId
                                       select a).FirstOrDefault();
                        content.ReplyDatetime = DateTime.Now;
                        content.DepartmentReply = obj.DepartmentReply;
                        db.tCommitteeReplyPendencyDetail.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();

                        var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                             where a.PendencyId == obj.PendencyId
                                             select a).ToList();
                        foreach (var lst in returncontent)
                        {
                            lst.StatusName = (from a in db.tCommitteeReplyPendencyDetail
                                              join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                              where obj.PendencyId == a.PendencyId
                                              && lst.Status == b.Id
                                              select b.ReplyStatusName).FirstOrDefault();
                        }
                        db.Close();
                        return returncontent;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object saveEditNewComitteeReplyContext(object param)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = param as tCommitteeReplyPendencyDetail;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.tCommitteeReplyPendencyDetail
                                   where a.PendencyId == obj.PendencyId
                                   && a.PendencyReplyId == obj.PendencyReplyId
                                   select a).FirstOrDefault();
                    content.Recomm_CommentDate = DateTime.Now;
                    content.Status = obj.Status;
                    content.Recomm_CommentsOfCommittee = obj.Recomm_CommentsOfCommittee;
                    db.tCommitteeReplyPendencyDetail.Attach(content);
                    db.Entry(content).State = EntityState.Modified;
                    db.SaveChanges();

                    var content1 = (from a in db.tCommitteeReplyPendencyDetail
                                    where a.PendencyId == obj.PendencyId
                                    orderby a.PendencyReplyId descending
                                    select a).FirstOrDefault();
                    var query1 = (from a in db.objCommitteReplyPendency
                                  where a.PendencyId == obj.PendencyId
                                  select a).FirstOrDefault();
                    if (content1.PendencyReplyId == obj.PendencyReplyId)
                    {
                        query1.LastReminderSentDate = DateTime.Now;
                        query1.Status = obj.Status ?? 0;
                        db.objCommitteReplyPendency.Attach(query1);
                        db.Entry(query1).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                         where a.PendencyId == obj.PendencyId
                                         select a).ToList();

                    foreach (var lst in returncontent)
                    {
                        lst.StatusName = (from a in db.tCommitteeReplyPendencyDetail
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where obj.PendencyId == a.PendencyId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                    }

                    db.Close();
                    return returncontent;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object saveEditNewQuesionforComitteeContext(object param)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = param as tCommitteeReplyPendencyDetail;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.tCommitteeReplyPendencyDetail
                                   where a.PendencyId == obj.PendencyId
                                   && a.PendencyReplyId == obj.PendencyReplyId
                                   select a).FirstOrDefault();
                    content.QuestionaireForCommittee = obj.QuestionaireForCommittee;
                    if (content.Recomm_CommentsOfCommittee == null) { content.Recomm_CommentsOfCommittee = ""; }
                    db.tCommitteeReplyPendencyDetail.Attach(content);
                    db.Entry(content).State = EntityState.Modified;
                    db.SaveChanges();

                    var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                         where a.PendencyId == obj.PendencyId
                                         select a).ToList();
                    foreach (var lst in returncontent)
                    {
                        lst.StatusName = (from a in db.tCommitteeReplyPendencyDetail
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where obj.PendencyId == a.PendencyId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                    }
                    db.Close();
                    return returncontent;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static object DeletePendencyNewEntryByPendencyId(object param)
        {
            try
            {
                var variable = Convert.ToInt16(param);
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.objCommitteReplyPendency
                                   where a.PendencyId == variable
                                   select a).FirstOrDefault();
                    db.objCommitteReplyPendency.Remove(content);

                    var content2 = (from a in db.tCommitteeReplyPendencyDetail
                                    where a.PendencyId == variable
                                    select a).FirstOrDefault();
                    if (content2 != null)
                    {
                        db.tCommitteeReplyPendencyDetail.Remove(content2);
                    }

                    db.SaveChanges();
                    db.Close();
                }
                return null;

            }
            catch (Exception)
            {

                throw;
            }
        }


        public static object DeletePendencyReplyDetailsByReplyIdContext(object param)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = param as tCommitteeReplyPendencyDetail;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.tCommitteeReplyPendencyDetail
                                   where a.PendencyId == obj.PendencyId
                                   && a.PendencyReplyId == obj.PendencyReplyId
                                   select a).FirstOrDefault();
                    db.tCommitteeReplyPendencyDetail.Remove(content);
                    db.SaveChanges();

                    var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                         where a.PendencyId == obj.PendencyId
                                         select a).ToList();
                    foreach (var lst in returncontent)
                    {
                        lst.StatusName = (from a in db.tCommitteeReplyPendencyDetail
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where obj.PendencyId == a.PendencyId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                    }

                    db.Close();
                    return returncontent;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object saveOldNewPendencyReplyContext(object param)
        {
            try
            {
                tCommitteeReplyPendencyDetail obj = param as tCommitteeReplyPendencyDetail;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    if (obj.PendencyReplyId == 0)
                    {

                        //var content = (from a in db.tCommitteeReplyPendencyDetail
                        //               where a.PendencyReplyId == obj.PendencyReplyId
                        //               select a).FirstOrDefault();

                        var query = (from a in db.objCommitteReplyPendency
                                     where a.PendencyId == obj.PendencyId
                                     select a).FirstOrDefault();
                        query.LastReminderSentDate = DateTime.Now;
                        query.Status = obj.Status ?? 0;
                        db.objCommitteReplyPendency.Attach(query);
                        db.Entry(query).State = EntityState.Modified;
                        db.SaveChanges();

                        obj.CreatedDate = DateTime.Today;
                        //obj.ReplyDatetime = DateTime.Now;
                        db.tCommitteeReplyPendencyDetail.Add(obj);
                        db.SaveChanges();


                        var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                             where a.PendencyId == obj.PendencyId
                                             select a).ToList();
                        foreach (var lst in returncontent)
                        {
                            lst.StatusName = (from a in db.tCommitteeReplyPendencyDetail
                                              join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                              where obj.PendencyId == a.PendencyId
                                              && lst.Status == b.Id
                                              select b.ReplyStatusName).FirstOrDefault();
                        }
                        db.Close();
                        return returncontent;
                    }
                    else
                    {//will never get hit...
                        var content = (from a in db.tCommitteeReplyPendencyDetail
                                       where a.PendencyId == obj.PendencyId
                                       && a.PendencyReplyId == obj.PendencyReplyId
                                       select a).FirstOrDefault();
                        content.ReplyDatetime = DateTime.Now;
                        content.DepartmentReply = obj.DepartmentReply;
                        db.tCommitteeReplyPendencyDetail.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();

                        var returncontent = (from a in db.tCommitteeReplyPendencyDetail
                                             where a.PendencyId == obj.PendencyId
                                             select a).ToList();
                        foreach (var lst in returncontent)
                        {
                            lst.StatusName = (from a in db.tCommitteeReplyPendencyDetail
                                              join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                              where obj.PendencyId == a.PendencyId
                                              && lst.Status == b.Id
                                              select b.ReplyStatusName).FirstOrDefault();
                        }
                        db.Close();
                        return returncontent;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object eFileWithdepartment(object param)
        {
            try
            {
                string BranchId = param as string;
                int Cid = GetCommiteeIDByBranchID(BranchId);

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var returnvalue = (from a in db.eFiles
                                       where a.CommitteeId == Cid
                                       select new fillListGenricInt
                                       {
                                           Text = a.eFileNumber,
                                           value = a.eFileID
                                       }).ToList();
                    return returnvalue;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int GetCommiteeIDByBranchID(string BranchId)
        {
            string strBranchId = Convert.ToString(BranchId);
            int BrachId = 0;
            int CommitteeId = 0;
            if (!string.IsNullOrEmpty(strBranchId))
            {
                BrachId = Convert.ToInt32(strBranchId);
            }
            using (CommitteReplyPendencyContext ctxCommitee = new CommitteReplyPendencyContext())
            {
                var VarCommitteeID = (from c in ctxCommitee.tBranchesCommittee where c.BranchId == BrachId select c.CommitteeId).FirstOrDefault();
                CommitteeId = (int)VarCommitteeID;

            }
            return CommitteeId;
        }


        public static object ItemNoCheck(object param)
        {
            try
            {
                tCommitteReplyPendency model = param as tCommitteReplyPendency;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.objCommitteReplyPendency
                                   where a.TypeNo == model.TypeNo
                                   && a.DepartmentId == model.DepartmentId
                                   && a.AssemblyID == model.AssemblyID
                                   && a.SessionID == model.SessionID
                                   && a.FinancialYear == model.FinancialYear
                                   && a.ItemTypeId == model.ItemTypeId
                                   && a.SessionDate == model.SessionDate
                                   select a).FirstOrDefault();
                    return content;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static object ToVerifyAttachedPaper(object param)
        {
            try
            {
                string[] str = param as string[];
                string UserName = Convert.ToString(str[0]);
                int uid = Convert.ToInt16(str[1]);
                if (str[2] == "")
                {
                    str[2] = "0";
                }
                int BranchId = Convert.ToInt16(str[2]);
                // Int32 ComId = Convert.ToInt32(str[3]);
                string AdharId = str[3];
                eFileContext context = new eFileContext();
                tMovementFile MoveObj = new tMovementFile();
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.eFileAttachment
                                   where a.eFileAttachmentId == uid
                                   select a).FirstOrDefault();
                    if (content != null)
                    {
                        content.IsVerified = true;
                        content.VerifiedDate = DateTime.Now;
                        content.VerifiedBy = UserName;
                        //content.CurrentBranchId = BranchId;
                        db.eFileAttachment.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();

                    }

                    var Assgnfrom = (from AF in context.mStaff where AF.AadharID == AdharId select AF).FirstOrDefault();
                    var AssgnTo = (from AT in context.mStaff where AT.AadharID == AdharId select AT).FirstOrDefault();
                    eFileAttachment PaperObj = context.eFileAttachments.Single(m => m.eFileAttachmentId == uid);
                    PaperObj.CurrentBranchId = BranchId;
                    PaperObj.CurrentEmpAadharID = AdharId;
                    PaperObj.AssignDateTime = DateTime.Now;
                    context.SaveChanges();
                    MoveObj.eFileAttachmentId = uid;
                    MoveObj.BranchId = BranchId;
                    MoveObj.EmpAadharId = AdharId;
                    MoveObj.Remarks = "";
                    MoveObj.AssignDateTime = DateTime.Now;
                    MoveObj.EmpName = AssgnTo.StaffName;
                    MoveObj.EmpDesignation = AssgnTo.Designation;
                    MoveObj.AssignfrmAadharId = AdharId;
                    MoveObj.AssignfrmName = Assgnfrom.StaffName;
                    MoveObj.AssignfrmDesignation = Assgnfrom.Designation;
                    context.tMovementFile.Add(MoveObj);
                    context.SaveChanges();
                    context.Close();


                    return null;
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        //public static object GetReplyPendencyInGriD(object param)
        //{
        //    try
        //    {



        //        var pagesize = 1;

        //        tCommitteReplyPendency obj = param as tCommitteReplyPendency;

        //        pagesize = obj.page;

        //        var predicate = PredicateBuilder.True<tCommitteReplyPendency>();
        //        if (obj.CommitteeId != 0)
        //            predicate = predicate.And(q => q.CommitteeId == obj.CommitteeId);
        //        if (obj.DepartmentId != null)
        //            predicate = predicate.And(q => q.DepartmentId == obj.DepartmentId);
        //        if (obj.ItemTypeId != null)
        //            predicate = predicate.And(q => q.ItemTypeId == obj.ItemTypeId);
        //        if (obj.FinancialYear != "Select")
        //            predicate = predicate.And(q => q.FinancialYear == obj.FinancialYear);
        //        if (obj.Status != 0)
        //            predicate = predicate.And(q => q.Status == obj.Status);

        //        using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
        //        {

        //            //var query = (from a in db.objCommitteReplyPendency
        //            //             where obj.ItemTypeId == a.ItemTypeId
        //            //             orderby a.FinancialYear,a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
        //            //             select a).AsExpandable().Where(predicate).ToPagedList(pagesize,10);

        //            var query = (from a in db.objCommitteReplyPendency
        //                         where obj.ItemTypeId == a.ItemTypeId
        //                         orderby a.FinancialYear, a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
        //                         select a).AsExpandable().Where(predicate).Skip((obj.page - 1) * obj.rows).Take(obj.rows).ToList();

        //            var TotalCount = (from a in db.objCommitteReplyPendency
        //                         where obj.ItemTypeId == a.ItemTypeId
        //                         orderby a.FinancialYear, a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
        //                         select a).AsExpandable().Where(predicate).ToList();

        //            obj.totalRecordsCount = TotalCount.Count;

        //            foreach (var lst in query)
        //            {
        //                lst.StatusName = (from a in db.objCommitteReplyPendency
        //                                  join b in db.objCommitteeReplyStatus on a.Status equals b.Id
        //                                  where lst.ItemTypeId == a.ItemTypeId
        //                                  && lst.Status == b.Id
        //                                  select b.ReplyStatusName).FirstOrDefault();
        //                lst.DepartmentName = (from a in db.objCommitteReplyPendency
        //                                      join b in db.mDepartment on a.DepartmentId equals b.deptId
        //                                      where lst.DepartmentId == b.deptId
        //                                      && lst.ItemTypeId == a.ItemTypeId
        //                                      select b.deptname).FirstOrDefault();
        //                lst.AbbrDeptName = (from a in db.objCommitteReplyPendency
        //                                    join b in db.mDepartment on a.DepartmentId equals b.deptId
        //                                    where lst.DepartmentId == b.deptId
        //                                    && lst.ItemTypeId == a.ItemTypeId
        //                                    select b.deptabbr).FirstOrDefault();
        //                lst.eFileNumber = (from m in db.eFiles
        //                                   where m.eFileID == lst.FileNumber
        //                                   select m.eFileNumber).FirstOrDefault();
        //                lst.eFileID = (from m in db.eFiles
        //                               where m.eFileID == lst.FileNumber
        //                               select m.eFileID).FirstOrDefault();
        //                lst.eFileSubject = (from m in db.eFiles
        //                                    where m.eFileID == lst.FileNumber
        //                                    select m.eFileSubject).FirstOrDefault();
        //            }

        //            query.Add(obj);
        //            return query;
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}



        public static object GetReplyPendencyInGriD(object param)
        {
            try
            {
                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                //pagesize = obj.page;

                var predicate = PredicateBuilder.True<tCommitteReplyPendency>();
                if (obj.CommitteeId != 0)
                    predicate = predicate.And(q => q.CommitteeId == obj.CommitteeId);
                if (obj.DepartmentId != null)
                    predicate = predicate.And(q => q.DepartmentId == obj.DepartmentId);
                if (obj.ItemTypeId != null)
                    predicate = predicate.And(q => q.ItemTypeId == obj.ItemTypeId);
                if (obj.FinancialYear != "Select")
                    predicate = predicate.And(q => q.FinancialYear == obj.FinancialYear);
                if (obj.Status != 0)
                    predicate = predicate.And(q => q.Status == obj.Status);

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    //var query = (from a in db.objCommitteReplyPendency
                    //             where obj.ItemTypeId == a.ItemTypeId
                    //             orderby a.FinancialYear,a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
                    //             select a).AsExpandable().Where(predicate).ToPagedList(pagesize,10);

                    var query = (from a in db.objCommitteReplyPendency
                                 where obj.ItemTypeId == a.ItemTypeId
                                 orderby a.FinancialYear, a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
                                 select a).AsExpandable().Where(predicate).Skip((obj.page - 1) * obj.rows).Take(obj.rows);

                    var TotalCount = (from a in db.objCommitteReplyPendency
                                      where obj.ItemTypeId == a.ItemTypeId
                                      orderby a.FinancialYear, a.OrderString1, a.OrderString2 descending, a.OrderString3 descending, a.OrderString4 descending, a.OrderString5 descending, a.OrderString6 descending, a.OrderString7 descending, a.OrderString8 descending
                                      select a).AsExpandable().Where(predicate).ToList();

                    obj.totalRecordsCount = TotalCount.Count;

                    foreach (var lst in query)
                    {
                        lst.StatusName = (from a in db.objCommitteReplyPendency
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where lst.ItemTypeId == a.ItemTypeId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                        lst.DepartmentName = (from a in db.objCommitteReplyPendency
                                              join b in db.mDepartment on a.DepartmentId equals b.deptId
                                              where lst.DepartmentId == b.deptId
                                              && lst.ItemTypeId == a.ItemTypeId
                                              select b.deptname).FirstOrDefault();
                        lst.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                            join b in db.mDepartment on a.DepartmentId equals b.deptId
                                            where lst.DepartmentId == b.deptId
                                            && lst.ItemTypeId == a.ItemTypeId
                                            select b.deptabbr).FirstOrDefault();
                        lst.eFileNumber = (from m in db.eFiles
                                           where m.eFileID == lst.FileNumber
                                           select m.eFileNumber).FirstOrDefault();
                        lst.eFileID = (from m in db.eFiles
                                       where m.eFileID == lst.FileNumber
                                       select m.eFileID).FirstOrDefault();
                        lst.eFileSubject = (from m in db.eFiles
                                            where m.eFileID == lst.FileNumber
                                            select m.eFileSubject).FirstOrDefault();
                    }

                    if (obj.sord == "asc")
                    {
                        switch (obj.sidx)
                        {
                            case "DepartmentId": query = query.OrderBy(_ => _.DepartmentId); break;
                            case "Status": query = query.OrderBy(_ => _.Status); break;
                            case "CommitteeId": query = query.OrderBy(_ => _.CommitteeId); break;
                            case "FinancialYear": query = query.OrderBy(_ => _.FinancialYear); break;
                            case "ItemTypeId": query = query.OrderBy(_ => _.ItemTypeId); break;

                            default: break;
                        }
                    }
                    else
                    {
                        switch (obj.sidx)
                        {
                            case "DepartmentId": query = query.OrderBy(_ => _.DepartmentId); break;
                            case "Status": query = query.OrderBy(_ => _.Status); break;
                            case "CommitteeId": query = query.OrderBy(_ => _.CommitteeId); break;
                            case "FinancialYear": query = query.OrderBy(_ => _.FinancialYear); break;
                            case "ItemTypeId": query = query.OrderBy(_ => _.ItemTypeId); break;

                            default: break;
                        }
                    }

                    query.ToList().Add(obj);
                    return query;
                }
            }
            catch
            {
                throw;
            }
        }

        public static List<tCommitteReplyPendency1> GetReplyPendencyInGriD_Jquerygrid(object param)
        {
            CommitteReplyPendencyContext context = new CommitteReplyPendencyContext();
            var tCommitteReplyPendency = context.Set<tCommitteReplyPendency>();
            var mCommitteeReplyStatus = context.Set<mCommitteeReplyStatus>();
            var mDepartment = context.Set<mDepartment>();
            var eFile = context.Set<SBL.DomainModel.Models.eFile.eFile>();
            //  var mCommitteeReplyStatus = context.Set<mCommitteeReplyStatus>();
            tCommitteReplyPendency model = param as tCommitteReplyPendency;
            tCommitteReplyPendency1 model1 = new tCommitteReplyPendency1();
            List<tCommitteReplyPendency1> myl = new List<tCommitteReplyPendency1>();

            int? CommitteeId = null; int? Status = null; int? ItemTypeId = null;
            if (model.CommitteeId != 0)
            {
                CommitteeId = model.CommitteeId;
            }
            if (model.Status != 0)
            {
                Status = model.Status;
            }
            if (model.FinancialYear == "Select")
            {
                model.FinancialYear = null;
            }
            if (model.ItemTypeId != 0)
            {
                ItemTypeId = model.ItemTypeId;
            }
            // var predicate = PredicateBuilder.True<tCommitteReplyPendency>();
            // if (model.CommitteeId != 0)
            //     predicate = predicate.And(q => q.CommitteeId == model.CommitteeId);
            //// string comID = model.CommitteeId.ToString();
            // if (model.DepartmentId != null)
            //     predicate = predicate.And(q => q.DepartmentId == model.DepartmentId);
            // if (model.ItemTypeId != null)
            //     predicate = predicate.And(q => q.ItemTypeId == model.ItemTypeId);
            // if (model.FinancialYear != "Select")
            //     predicate = predicate.And(q => q.FinancialYear == model.FinancialYear);
            // if (model.Status != 0)
            //     predicate = predicate.And(q => q.Status == model.Status);
            if ((model.DepartmentId == null || model.DepartmentId == "0") && (model.FinancialYear == null))
            {
                var query = (from a in context.objCommitteReplyPendency
                             join b in mCommitteeReplyStatus on a.Status equals b.Id
                             join c in mDepartment on a.DepartmentId equals c.deptId
                             join e in eFile on a.FileNumber equals e.eFileID
                             where a.ItemTypeId == model.ItemTypeId
                                && (!CommitteeId.HasValue || a.CommitteeId == CommitteeId)
                                    && (!Status.HasValue || a.Status == Status)
                             select new tCommitteReplyPendency1
                             {
                                 FinancialYear = a.FinancialYear,
                                 AbbrDeptName = c.deptabbr,
                                 DepartmentName = c.deptname,
                                 TypeNo = a.TypeNo,
                                 Subject = a.Subject,
                                 StatusName = b.ReplyStatusName,
                                 eFileSubject = e.eFileSubject,
                                 eFileNumber = e.eFileNumber,
                                 eFileID = e.eFileID,
                                 ItemTypeId = a.ItemTypeId,
                                 PendencyId = a.PendencyId
                             }).OrderBy(p => p.DepartmentName).Skip((model.page - 1) * model.rows).Take(model.rows);


                if (model.sord == "asc")
                {
                    switch (model.sidx)
                    {
                        case "FinancialYear": query = query.OrderBy(_ => _.FinancialYear); break;
                        case "AbbrDeptName": query = query.OrderBy(_ => _.AbbrDeptName); break;
                        case "TypeNo": query = query.OrderBy(_ => _.TypeNo); break;
                        case "Subject": query = query.OrderBy(_ => _.Subject); break;
                        case "Status": query = query.OrderBy(_ => _.Status); break;
                        case "eFileSubject": query = query.OrderBy(_ => _.eFileSubject); break;
                        case "eFileNumber": query = query.OrderBy(_ => _.eFileNumber); break;
                        case "eFileID": query = query.OrderBy(_ => _.eFileID); break;
                        case "CommitteeId": query = query.OrderBy(_ => _.CommitteeId); break;
                        case "ItemTypeId": query = query.OrderBy(_ => _.ItemTypeId); break;
                        default: break;
                    }
                }
                else
                {
                    switch (model.sidx)
                    {
                        case "FinancialYear": query = query.OrderBy(_ => _.FinancialYear); break;
                        case "AbbrDeptName": query = query.OrderBy(_ => _.AbbrDeptName); break;
                        case "TypeNo": query = query.OrderBy(_ => _.TypeNo); break;
                        case "Subject": query = query.OrderBy(_ => _.Subject); break;
                        case "Status": query = query.OrderBy(_ => _.Status); break;
                        case "eFileSubject": query = query.OrderBy(_ => _.eFileSubject); break;
                        case "eFileNumber": query = query.OrderBy(_ => _.eFileNumber); break;
                        case "eFileID": query = query.OrderBy(_ => _.eFileID); break;
                        case "CommitteeId": query = query.OrderBy(_ => _.CommitteeId); break;
                        case "ItemTypeId": query = query.OrderBy(_ => _.ItemTypeId); break;
                        default: break;
                    }
                }
                var forcount = (from a in context.objCommitteReplyPendency
                                join b in mCommitteeReplyStatus on a.Status equals b.Id
                                join c in mDepartment on a.DepartmentId equals c.deptId
                                join e in eFile on a.FileNumber equals e.eFileID
                                where a.ItemTypeId == model.ItemTypeId
                                && (!CommitteeId.HasValue || a.CommitteeId == CommitteeId)
                                    && (!Status.HasValue || a.Status == Status)
                                select a.ItemTypeId).ToList();
                var pList = query.ToList();
                if (forcount != null)
                {
                    model1.totalRecordsCount = forcount.ToList().Count();
                }
                //model1.totalRecordsCount = forcount.Count;
                pList.Add(model1);
                return pList;
            }
            else
            {
                //where (model.searchString == null || model.searchString == string.Empty || k.OfficeName.ToUpper().Contains(model.searchString.ToUpper()) || k.DesignationName.ToUpper().Contains(model.searchString.ToUpper()))
                {
                    var query = (from a in context.objCommitteReplyPendency
                                 join b in mCommitteeReplyStatus on a.Status equals b.Id
                                 join c in mDepartment on a.DepartmentId equals c.deptId
                                 join e in eFile on a.FileNumber equals e.eFileID
                                 where a.ItemTypeId == model.ItemTypeId
                                && (!CommitteeId.HasValue || a.CommitteeId == CommitteeId)
                                    && (!Status.HasValue || a.Status == Status)
                                    && (model.FinancialYear == null || model.FinancialYear == string.Empty || a.FinancialYear == model.FinancialYear)
                               && (model.DepartmentId == null || model.DepartmentId == string.Empty || a.DepartmentId.ToUpper() == model.DepartmentId.ToUpper())
                                 //  && (obj.DepartmentId == null || obj.DepartmentId == string.Empty || a.DepartmentId == DepartmentId)

                                 select new tCommitteReplyPendency1
                                 {
                                     FinancialYear = a.FinancialYear,
                                     AbbrDeptName = c.deptabbr,
                                     DepartmentName = c.deptname,
                                     TypeNo = a.TypeNo,
                                     Subject = a.Subject,
                                     StatusName = b.ReplyStatusName,
                                     eFileSubject = e.eFileSubject,
                                     eFileNumber = e.eFileNumber,
                                     eFileID = e.eFileID,
                                     ItemTypeId = a.ItemTypeId,
                                     PendencyId = a.PendencyId
                                 }).OrderBy(p => p.DepartmentName).Skip((model.page - 1) * model.rows).Take(model.rows);

                    if (model.sord == "asc")
                    {
                        switch (model.sidx)
                        {
                            case "FinancialYear": query = query.OrderBy(_ => _.FinancialYear); break;
                            case "AbbrDeptName": query = query.OrderBy(_ => _.AbbrDeptName); break;
                            case "TypeNo": query = query.OrderBy(_ => _.TypeNo); break;
                            case "Subject": query = query.OrderBy(_ => _.Subject); break;
                            case "Status": query = query.OrderBy(_ => _.Status); break;
                            case "eFileSubject": query = query.OrderBy(_ => _.eFileSubject); break;
                            case "eFileNumber": query = query.OrderBy(_ => _.eFileNumber); break;
                            case "eFileID": query = query.OrderBy(_ => _.eFileID); break;
                            case "CommitteeId": query = query.OrderBy(_ => _.CommitteeId); break;
                            case "ItemTypeId": query = query.OrderBy(_ => _.ItemTypeId); break;
                            default: break;
                        }
                    }
                    else
                    {
                        switch (model.sidx)
                        {
                            case "FinancialYear": query = query.OrderBy(_ => _.FinancialYear); break;
                            case "AbbrDeptName": query = query.OrderBy(_ => _.AbbrDeptName); break;
                            case "TypeNo": query = query.OrderBy(_ => _.TypeNo); break;
                            case "Subject": query = query.OrderBy(_ => _.Subject); break;
                            case "Status": query = query.OrderBy(_ => _.Status); break;
                            case "eFileSubject": query = query.OrderBy(_ => _.eFileSubject); break;
                            case "eFileNumber": query = query.OrderBy(_ => _.eFileNumber); break;
                            case "eFileID": query = query.OrderBy(_ => _.eFileID); break;
                            case "CommitteeId": query = query.OrderBy(_ => _.CommitteeId); break;
                            case "ItemTypeId": query = query.OrderBy(_ => _.ItemTypeId); break;
                            default: break;
                        }
                    }
                    var forcount = (from a in context.objCommitteReplyPendency
                                    join b in mCommitteeReplyStatus on a.Status equals b.Id
                                    join c in mDepartment on a.DepartmentId equals c.deptId
                                    join e in eFile on a.FileNumber equals e.eFileID
                                    where a.ItemTypeId == model.ItemTypeId
                                    && (!CommitteeId.HasValue || a.CommitteeId == CommitteeId)
                                    && (!Status.HasValue || a.Status == Status)
                                    && (model.FinancialYear == null || model.FinancialYear == string.Empty || a.FinancialYear == model.FinancialYear)
                               && (model.DepartmentId == null || model.DepartmentId == string.Empty || a.DepartmentId.ToUpper() == model.DepartmentId.ToUpper())
                                    select a.ItemTypeId).ToList();
                    var pList = query.ToList();
                    if (forcount != null)
                    {
                        model1.totalRecordsCount = forcount.ToList().Count();
                    }
                    //model1.totalRecordsCount = forcount.Count;
                    pList.Add(model1);
                    return pList;
                }
            }
        }

        public static object ToRejectAttachedPaper(object param)
        {
            try
            {
                eFileAttachment obj = param as eFileAttachment;
                string[] str = param as string[];

                //string Remarks = obj.VerifiedBy;
                int id = obj.eFileAttachmentId;
                // int BranchId = Convert.ToInt16(str[2]);



                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.eFileAttachment
                                   where a.eFileAttachmentId == id
                                   select a).FirstOrDefault();
                    if (content != null)
                    {
                        content.IsVerified = false;
                        content.IsRejected = true;
                        content.VerifiedDate = DateTime.Now;
                        content.VerifiedBy = obj.VerifiedBy;// this will be rejected by
                        content.RejectedRemarks = obj.RejectedRemarks;
                        //content.CurrentBranchId = BranchId;
                        db.eFileAttachment.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                    return content;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object GetReplyPendencySummaryCount(object param)
        {
            try
            {
                tCommitteReplyPendency obj = param as tCommitteReplyPendency;

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.objCommitteReplyPendency
                                 where obj.ItemTypeId == a.ItemTypeId
                                 //&& a.IsFreeze == true
                                 select a);//.ToList();
                    if (obj.FinancialYear != "Select")
                    {
                        query = query.Where(a => a.FinancialYear == obj.FinancialYear);
                    }
                    if (obj.CommitteeId != 0)
                    {
                        query = query.Where(a => a.CommitteeId == obj.CommitteeId);
                    }
                    if (obj.DepartmentId != null)
                    {
                        query = query.Where(a => a.DepartmentId == obj.DepartmentId);
                    }
                    if (obj.ItemTypeId != null)
                    {
                        query = query.Where(a => a.ItemTypeId == obj.ItemTypeId);
                    }
                    if (obj.Status != 0)
                    {
                        query = query.Where(a => a.Status == obj.Status);
                    }
                    var CountDistinct = query.GroupBy(g => new { g.FinancialYear, g.DepartmentId }).Select(g => g.FirstOrDefault());


                    //var toDoList = query.ToList();
                    var toDoList = CountDistinct.ToList();
                    foreach (var lst in toDoList)
                    {
                        lst.StatusName = (from a in db.objCommitteReplyPendency
                                          join b in db.objCommitteeReplyStatus on a.Status equals b.Id
                                          where lst.ItemTypeId == a.ItemTypeId
                                          && lst.Status == b.Id
                                          select b.ReplyStatusName).FirstOrDefault();
                        lst.DepartmentName = (from a in db.objCommitteReplyPendency
                                              join b in db.mDepartment on a.DepartmentId equals b.deptId
                                              where lst.DepartmentId == b.deptId
                                              && lst.ItemTypeId == a.ItemTypeId
                                              select b.deptname).FirstOrDefault();
                        lst.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                            join b in db.mDepartment on a.DepartmentId equals b.deptId
                                            where lst.DepartmentId == b.deptId
                                            && lst.ItemTypeId == a.ItemTypeId
                                            select b.deptabbr).FirstOrDefault();
                        lst.eFileNumber = (from m in db.eFiles
                                           where m.eFileID == lst.FileNumber
                                           select m.eFileNumber).FirstOrDefault();
                        lst.eFileID = (from m in db.eFiles
                                       where m.eFileID == lst.FileNumber
                                       select m.eFileID).FirstOrDefault();
                        lst.eFileSubject = (from m in db.eFiles
                                            where m.eFileID == lst.FileNumber
                                            select m.eFileSubject).FirstOrDefault();
                        // lst.totalRecordsCount = lst.DepartmentId.Count();
                        lst.totalRecordsCount = (from a in db.objCommitteReplyPendency
                                                 where a.DepartmentId == lst.DepartmentId && a.FinancialYear == lst.FinancialYear && a.ItemTypeId == lst.ItemTypeId
                                                 && a.Status == lst.Status && a.CommitteeId == lst.CommitteeId
                                                 select a).ToList().Count();
                        //group a by new { a.DepartmentId } into z///
                        //select z.Distinct()).Count();                      
                    }


                    return toDoList;
                    //return query.ToList();
                }
            }
            catch
            {
                throw;
            }
        }


        ///For new item pendency
        ///
        public static object GetAllPendencyNew(object param)//done by predicate //
        {
            try
            {
                pHouseCommitteeItemPendency obj = param as pHouseCommitteeItemPendency;

                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var query = (from a in db.pHouseCommitteeItemPendency
                                 //where obj.ItemTypeId == a.ItemTypeId                               
                                 select a).AsExpandable().Take(500).ToList();
                    foreach (var lst in query)
                    {

                        lst.DepartmentName = (from a in db.objCommitteReplyPendency
                                              join b in db.mDepartment on a.DepartmentId equals b.deptId
                                              where lst.SentByDepartmentId == b.deptId
                                              select b.deptname).FirstOrDefault();
                        lst.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                            join b in db.mDepartment on a.DepartmentId equals b.deptId
                                            where lst.SentByDepartmentId == b.deptId
                                            select b.deptabbr).FirstOrDefault();
                    }
                    return query;
                }
            }
            catch
            {
                throw;
            }
        }
        public static object GetCustomItemPendency(object param)
        {
            CommitteReplyPendencyContext db = new CommitteReplyPendencyContext();
            List<pHouseCommitteeItemPendency> Parentlist = new List<pHouseCommitteeItemPendency>();
            List<pHouseCommitteeSubItemPendency> Childlist = new List<pHouseCommitteeSubItemPendency>();
            pHouseCommitteeItemPendency Cmodel = param as pHouseCommitteeItemPendency;
            using (eFileContext ctx = new eFileContext())
            {
                var ParentList = (from a in ctx.pHouseCommitteeItemPendency
                                  select a).AsExpandable().ToList();
                Parentlist = ParentList.ToList();
                //var ParentList = from element in ctx.pHouseCommitteeItemPendency
                //                 group element by element.ParentId
                //                  into groups
                //                 select groups.OrderByDescending(p => p.CreatedDate);

                List<pHouseCommitteeSubItemPendency> queryAll = new List<pHouseCommitteeSubItemPendency>();
                var CList = from element in ctx.pHouseCommitteeSubItemPendency
                            group element by element.ParentId
                                into groups
                                select groups.OrderByDescending(p => p.CreatedDate).FirstOrDefault();
                queryAll = CList.ToList();
                Childlist = queryAll;
                var ChildList = queryAll.ToList();

            }
            List<pHouseCommitteeItemPendency> list = new List<pHouseCommitteeItemPendency>();

            foreach (var item in Parentlist)
            {

                var Check = Childlist.Where(a => a.ParentId == item.PendencyId).ToList();
                if (Check.Count != 0)
                {
                    var Check1 = Childlist.Where(a => a.ParentId == item.PendencyId).ToList();
                    foreach (var item1 in Check1)
                    {
                        pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                        mdl.SubPendencyId = item1.SubPendencyId;
                        mdl.MainPendencyId = item1.ParentId;
                        mdl.ParentId = item1.ParentId;
                        //mdl.AssemblyID = item1.AssemblyID;
                        //mdl.SessionID = item1.SessionID;
                        //mdl.SessionDate = item1.SessionDate;
                        mdl.FinancialYear = item1.FinancialYear;
                        mdl.CAGYear = item1.CAGYear;
                        mdl.SentByDepartmentId = item1.SentByDepartmentId;
                        mdl.ItemTypeId = item1.ItemTypeId;
                        mdl.ItemTypeName = item.ItemTypeName;
                        mdl.SubItemTypeId = item.SubItemTypeId;
                        mdl.SubItemTypeName = item.SubItemTypeName;
                        mdl.ItemNature = item1.ItemNature;
                        mdl.ItemNatureId = item1.ItemNatureId;
                        mdl.ItemNumber = item1.ItemNumber;
                        mdl.ItemDescription = item1.ItemDescription;
                        mdl.AnnexPdfPath = item1.AnnexPdfPath;
                        mdl.ItemReplyPdfPath = item1.ItemReplyPdfPath;
                        mdl.DocFileName = item1.DocFileName;
                        mdl.DocFilePath = item1.DocFilePath;
                        mdl.SentOnDate = item1.SentOnDate;
                        //mdl.SentOnDateByVS = item1.SentOnDateByVS;
                        mdl.CommitteeId = item1.CommitteeId;
                        mdl.AnnexPdfPath = item1.AnnexPdfPath;
                        if (item1.AnnexFilesNameByUser != null)
                        {
                            string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                            mdl.AnnexFilesNameByUser = str;
                        }
                        else
                        {
                            mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                        }

                        mdl.DepartmentName = (from a in db.objCommitteReplyPendency
                                              join b in db.mDepartment on a.DepartmentId equals b.deptId
                                              where mdl.SentByDepartmentId == b.deptId
                                              select b.deptname).FirstOrDefault();
                        mdl.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                            join b in db.mDepartment on a.DepartmentId equals b.deptId
                                            where mdl.SentByDepartmentId == b.deptId
                                            select b.deptabbr).FirstOrDefault();


                        mdl.CreatedDate = item1.CreatedDate;
                        mdl.eFileID = item1.eFileID;
                        mdl.IsLocked = item1.IsLocked;
                        mdl.IsLinked = item1.IsLinked;
                        mdl.Linked_RecordId = item1.Linked_RecordId;
                        list.Add(mdl);
                    }
                }
                else
                {
                    pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                    mdl.MainPendencyId = item.PendencyId;
                    mdl.ParentId = item.ParentId;
                    mdl.PendencyId = item.PendencyId;
                    //mdl.AssemblyID = item.AssemblyID;
                    //mdl.SessionID = item.SessionID;
                    //mdl.SessionDate = item.SessionDate;
                    mdl.FinancialYear = item.FinancialYear;
                    mdl.CAGYear = item.CAGYear;
                    mdl.SentByDepartmentId = item.SentByDepartmentId;
                    mdl.ItemTypeId = item.ItemTypeId;
                    mdl.ItemTypeName = item.ItemTypeName;
                    mdl.SubItemTypeId = item.SubItemTypeId;
                    mdl.SubItemTypeName = item.SubItemTypeName;
                    mdl.ItemNature = item.ItemNature;
                    mdl.ItemNatureId = item.ItemNatureId;
                    mdl.ItemNumber = item.ItemNumber;
                    mdl.ItemDescription = item.ItemDescription;
                    mdl.AnnexPdfPath = item.AnnexPdfPath;
                    mdl.DocFileName = item.DocFileName;
                    mdl.DocFilePath = item.DocFilePath;
                    mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                    mdl.SentOnDate = item.SentOnDate;
                    // mdl.SentOnDateByVS = item.SentOnDateByVS;
                    mdl.CommitteeId = item.CommitteeId;
                    mdl.AnnexPdfPath = item.AnnexPdfPath;
                    if (item.AnnexFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                        mdl.AnnexFilesNameByUser = str;
                    }
                    else
                    {
                        mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                    }

                    mdl.DepartmentName = (from a in db.objCommitteReplyPendency
                                          join b in db.mDepartment on a.DepartmentId equals b.deptId
                                          where mdl.SentByDepartmentId == b.deptId
                                          select b.deptname).FirstOrDefault();
                    mdl.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                        join b in db.mDepartment on a.DepartmentId equals b.deptId
                                        where mdl.SentByDepartmentId == b.deptId
                                        select b.deptabbr).FirstOrDefault();

                    mdl.CreatedDate = item.CreatedDate;
                    mdl.eFileID = item.eFileID;
                    mdl.IsLocked = item.IsLocked;
                    mdl.IsLinked = item.IsLinked;
                    mdl.Linked_RecordId = item.Linked_RecordId;
                    list.Add(mdl);
                }

            }
            var ListIs = list.OrderByDescending(a => a.CreatedDate).ToList();

            List<pHouseCommitteeItemPendency> listnew = new List<pHouseCommitteeItemPendency>();
            ////////////////Changed for listing
            if (Cmodel.CurrentDeptId.Contains(','))//not VS user/For HOD Having multiple Dept
            {
                string[] str1 = new string[4];
                str1 = Cmodel.CurrentDeptId.Split(',');
                foreach (var item in str1)
                {
                    string CurrentDeptId = item;
                    var CheckList = ListIs.Where(a => a.SentByDepartmentId == CurrentDeptId).ToList();
                    listnew.AddRange(CheckList);
                }
                listnew = listnew.OrderByDescending(a => a.CreatedDate).ToList();
                return listnew;
            }
            else if (Cmodel.CurrentDeptId != "")//not VS user
            {
                var CheckList = ListIs.Where(a => a.SentByDepartmentId == Cmodel.CurrentDeptId).ToList();
                //var checkit = CheckList.Where(a => a.SendStatus == true).ToList();
                var checkit = CheckList.OrderByDescending(a => a.CreatedDate).ToList();
                return checkit;
            }
            else//Vs user
            {
                listnew = ListIs.OrderByDescending(a => a.CreatedDate).ToList();
                return listnew;
            }
        }
        public static object GetItemPendencyByParentId(object param)
        {
            List<pHouseCommitteeItemPendency> Parentlist = new List<pHouseCommitteeItemPendency>();
            List<pHouseCommitteeSubItemPendency> Childlist = new List<pHouseCommitteeSubItemPendency>();
            List<pHouseCommitteeItemPendency> list = new List<pHouseCommitteeItemPendency>();
            SiteSettingContext settingContext = new SiteSettingContext();
            eFileContext db = new eFileContext();
            pHouseCommitteeItemPendency model = param as pHouseCommitteeItemPendency;
            if (model.CurrentDeptId != "")//not vs user/only for single dept
            {
                var plist = (from dist in db.pHouseCommitteeItemPendency
                             where dist.PendencyId == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Parentlist = plist.ToList();
                var clist = (from dist in db.pHouseCommitteeSubItemPendency
                             where dist.ParentId == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Childlist = clist.ToList();
            }
            else //for VS Users Only
            {
                var plist = (from dist in db.pHouseCommitteeItemPendency
                             where dist.PendencyId == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Parentlist = plist.ToList();
                var clist = (from dist in db.pHouseCommitteeSubItemPendency
                             where dist.ParentId == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Childlist = clist.ToList();
            }

            List<pHouseCommitteeItemPendency> list1 = new List<pHouseCommitteeItemPendency>();
            foreach (var item in Parentlist)
            {
                pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                mdl.MainPendencyId = item.PendencyId;
                mdl.ParentId = item.ParentId;

                //mdl.AssemblyID = item.AssemblyID;
                //mdl.SessionID = item.SessionID;
                //mdl.SessionDate = item.SessionDate;
                mdl.FinancialYear = item.FinancialYear;
                mdl.SentByDepartmentId = item.SentByDepartmentId;
                mdl.ItemTypeId = item.ItemTypeId;
                mdl.ItemTypeName = item.ItemTypeName;
                mdl.ItemNature = item.ItemNature;
                mdl.ItemNatureId = item.ItemNatureId;
                mdl.ItemNumber = item.ItemNumber;
                mdl.ItemDescription = item.ItemDescription;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                mdl.DocFileName = item.DocFileName;
                mdl.DocFilePath = item.DocFilePath;
                mdl.CAGYear = item.CAGYear;
                mdl.SentOnDate = item.SentOnDate;
                //mdl.SentOnDateByVS = item.SentOnDateByVS;
                mdl.CommitteeId = item.CommitteeId;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                if (item.AnnexFilesNameByUser != null)
                {
                    string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                    mdl.AnnexFilesNameByUser = str;
                }
                else
                {
                    mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                }
                mdl.CreatedDate = item.CreatedDate;
                mdl.IsLinked = item.IsLinked;
                mdl.Linked_RecordId = item.Linked_RecordId;
                mdl.IsLocked = item.IsLocked;
                mdl.eFileID = item.eFileID;
                var FileAccessingUrlPath = (from itemis in settingContext.SiteSettings where itemis.SettingName == "FileAccessingUrlPath" select itemis.SettingValue).SingleOrDefault();
                mdl.FileAccessingUrlPath = FileAccessingUrlPath;
                mdl.CurrentDeptId = model.CurrentDeptId;
                list.Add(mdl);
            }
            foreach (var item in Childlist)
            {
                pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                mdl.MainPendencyId = item.ParentId;
                mdl.FinancialYear = item.FinancialYear;
                mdl.SentByDepartmentId = item.SentByDepartmentId;
                mdl.ItemTypeId = item.ItemTypeId;
                mdl.ItemTypeName = item.ItemTypeName;
                mdl.ItemNature = item.ItemNature;
                mdl.ItemNatureId = item.ItemNatureId;
                mdl.ItemNumber = item.ItemNumber;
                mdl.ItemDescription = item.ItemDescription;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                mdl.SentOnDate = item.SentOnDate;
                // mdl.SentOnDateByVS = item.SentOnDateByVS;
                mdl.CommitteeId = item.CommitteeId;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                mdl.DocFilePath = item.DocFilePath;
                mdl.DocFileName = item.DocFileName;
                mdl.CAGYear = item.CAGYear;
                if (item.AnnexFilesNameByUser != null)
                {
                    string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                    mdl.AnnexFilesNameByUser = str;
                }
                else
                {
                    mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                }
                mdl.CreatedDate = item.CreatedDate;
                mdl.IsLinked = item.IsLinked;
                mdl.Linked_RecordId = item.Linked_RecordId;
                mdl.IsLocked = item.IsLocked;
                mdl.eFileID = item.eFileID;
                var FileAccessingUrlPath = (from itemis in settingContext.SiteSettings where itemis.SettingName == "FileAccessingUrlPath" select itemis.SettingValue).SingleOrDefault();
                mdl.FileAccessingUrlPath = FileAccessingUrlPath;
                mdl.CurrentDeptId = model.CurrentDeptId;
                list.Add(mdl);
            }
            //var FilterList = list.Where(a => a.IsLinked == false).ToList();

            var ListIs = list.OrderByDescending(a => a.CreatedDate).ToList();

            //var Index = FilterList.Count - 1;
            ListIs.RemoveAt(0);
            return ListIs;
        }
        public static object SaveNewPendency(object param)
        {
            try
            {
                tCommitteReplyPendency model = param as tCommitteReplyPendency;
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {
                    var content = (from a in db.objCommitteReplyPendency
                                   where a.PendencyId == model.PendencyId
                                   select a).FirstOrDefault();

                    if (content == null)//Add logic
                    {
                        model.CreatedDate = Convert.ToString(DateTime.Today);
                        db.objCommitteReplyPendency.Add(model);
                        db.SaveChanges();
                        db.Close();
                    }
                    else
                    {
                        content.AssemblyID = model.AssemblyID;
                        content.CommitteeId = model.CommitteeId;
                        content.DepartmentId = model.DepartmentId;
                        content.eFilePaperNatureList = model.eFilePaperNatureList;
                        content.eFilePaperTypeList = model.eFilePaperTypeList;
                        content.FileNumber = model.FileNumber;
                        content.FinancialYear = model.FinancialYear;
                        content.IsFreeze = model.IsFreeze;
                        content.ItemDescription = model.ItemDescription;
                        content.ItemTypeId = model.ItemTypeId;
                        content.LastReminderSentDate = model.LastReminderSentDate;
                        content.LastReminderSentDateView = model.LastReminderSentDateView;
                        content.LatestReplyId = model.LatestReplyId;
                        content.mCommitteeReplyItemTypeList = model.mCommitteeReplyItemTypeList;
                        content.mCommitteeReplyStatusList = model.mCommitteeReplyStatusList;
                        content.mDepartmentList = model.mDepartmentList;
                        //content.PendencyBy = model.PendencyBy;
                        content.PendencyId = model.PendencyId;
                        content.ReportNatureId = model.ReportNatureId;
                        content.ReportTypeId = model.ReportTypeId;
                        content.SentOnDate = model.SentOnDate;
                        content.SentOnDateVIew = model.SentOnDateVIew;
                        content.SessionDate = model.SessionDate;
                        content.SessionID = model.SessionID;
                        content.Status = model.Status;
                        content.StatusName = model.StatusName;
                        content.Subject = model.Subject;
                        content.OrderString1 = model.OrderString1;
                        content.OrderString2 = model.OrderString2;
                        content.OrderString3 = model.OrderString3;
                        content.OrderString4 = model.OrderString4;
                        content.OrderString5 = model.OrderString5;
                        content.CagPageNo = model.CagPageNo;
                        content.tCommitteeList = model.tCommitteeList;
                        content.tCommittePendencyList = model.tCommittePendencyList;
                        content.TypeNo = model.TypeNo;
                        db.objCommitteReplyPendency.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();
                    }
                    return null;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static object GetDetailsByParentId(object param)
        {
            eFileContext db = new eFileContext();
            pHouseCommitteeItemPendency model = param as pHouseCommitteeItemPendency;
            var plist = (from dist in db.pHouseCommitteeItemPendency
                         where dist.PendencyId == model.PendencyId
                         select dist);
            var ListIs = plist.ToList();
            return ListIs;
        }
        public static object GetPendencyListforLinking1(object param)
        {
            List<pHouseCommitteeItemPendency> Parentlist = new List<pHouseCommitteeItemPendency>();
            List<pHouseCommitteeSubItemPendency> Childlist = new List<pHouseCommitteeSubItemPendency>();
            pHouseCommitteeItemPendency Cmodel = param as pHouseCommitteeItemPendency;
            using (eFileContext ctx = new eFileContext())
            {
                var ParentList = (from a in ctx.pHouseCommitteeItemPendency
                                  select a).AsExpandable().ToList();
                Parentlist = ParentList.ToList();

                List<pHouseCommitteeSubItemPendency> queryAll = new List<pHouseCommitteeSubItemPendency>();

                var CList = (from element in ctx.pHouseCommitteeSubItemPendency
                             where element.IsLinked == false
                             select element).AsExpandable().ToList();

                Childlist = CList.ToList();
            }
            List<pHouseCommitteeItemPendency> list = new List<pHouseCommitteeItemPendency>();

            foreach (var item in Parentlist)
            {

                //check for is linked 
                if (item.IsLinked == true) //Get not linked latest from childlist
                {

                    var CheckList = Childlist.Where(a => a.ParentId == item.PendencyId).ToList();
                    var nFilterList = CheckList.Where(a => a.IsLinked == false).ToList();
                    var Newlist = nFilterList.OrderByDescending(a => a.CreatedDate).ToList();

                    if (Newlist.Count != 0)
                    {
                        var item1 = Newlist.Where(a => a.ParentId == item.PendencyId).FirstOrDefault();

                        //foreach (var item1 in Newlist)
                        //{
                        pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                        mdl.SubPendencyId = item1.SubPendencyId;
                        mdl.MainPendencyId = item1.ParentId;
                        mdl.ParentId = item1.ParentId;
                        //mdl.AssemblyID = item1.AssemblyID;
                        //mdl.SessionID = item1.SessionID;
                        //mdl.SessionDate = item1.SessionDate;
                        mdl.FinancialYear = item1.FinancialYear;
                        mdl.SentByDepartmentId = item1.SentByDepartmentId;
                        mdl.ItemTypeId = item1.ItemTypeId;
                        mdl.ItemTypeName = item.ItemTypeName;
                        mdl.ItemNature = item1.ItemNature;
                        mdl.ItemNatureId = item1.ItemNatureId;
                        mdl.ItemNumber = item1.ItemNumber;
                        mdl.ItemDescription = item1.ItemDescription;
                        mdl.AnnexPdfPath = item1.AnnexPdfPath;
                        mdl.ItemReplyPdfPath = item1.ItemReplyPdfPath;
                        mdl.DocFilePath = item1.DocFilePath;
                        mdl.DocFileName = item1.DocFileName;
                        mdl.SentOnDate = item1.SentOnDate;
                        //mdl.SentOnDateByVS = item1.SentOnDateByVS;
                        mdl.CommitteeId = item1.CommitteeId;
                        mdl.AnnexPdfPath = item1.AnnexPdfPath;
                        if (item1.AnnexFilesNameByUser != null)
                        {
                            string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                            mdl.AnnexFilesNameByUser = str;
                        }
                        else
                        {
                            mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                        }
                        mdl.CreatedDate = item1.CreatedDate;
                        mdl.IsLinked = item1.IsLinked;
                        mdl.eFileID = item1.eFileID;
                        list.Add(mdl);
                        //}
                    }
                }
                else
                {
                    pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                    mdl.MainPendencyId = item.PendencyId;
                    mdl.ParentId = item.ParentId;
                    mdl.PendencyId = item.PendencyId;
                    mdl.FinancialYear = item.FinancialYear;
                    mdl.SentByDepartmentId = item.SentByDepartmentId;
                    mdl.ItemTypeId = item.ItemTypeId;
                    mdl.ItemTypeName = item.ItemTypeName;
                    mdl.ItemNature = item.ItemNature;
                    mdl.ItemNatureId = item.ItemNatureId;
                    mdl.ItemNumber = item.ItemNumber;
                    mdl.ItemDescription = item.ItemDescription;
                    mdl.AnnexPdfPath = item.AnnexPdfPath;
                    mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                    mdl.DocFilePath = item.DocFilePath;
                    mdl.DocFileName = item.DocFileName;
                    mdl.SentOnDate = item.SentOnDate;
                    // mdl.SentOnDateByVS = item.SentOnDateByVS;
                    mdl.CommitteeId = item.CommitteeId;
                    mdl.AnnexPdfPath = item.AnnexPdfPath;
                    if (item.AnnexFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                        mdl.AnnexFilesNameByUser = str;
                    }
                    else
                    {
                        mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                    }
                    mdl.CreatedDate = item.CreatedDate;
                    mdl.eFileID = item.eFileID;
                    mdl.IsLinked = item.IsLinked;
                    list.Add(mdl);
                }
            }


            List<pHouseCommitteeItemPendency> listnew = new List<pHouseCommitteeItemPendency>();
            ////////////////Changed for listing
            if (Cmodel.CurrentDeptId.Contains(','))//not VS user/For HOD Having multiple Dept
            {
                listnew = list.OrderByDescending(a => a.CreatedDate).ToList();
                var CheckList = listnew.Where(a => a.ItemTypeId == Cmodel.ItemTypeId).ToList();
                var FilterList = CheckList.Where(a => a.IsLinked == false).ToList();
                return FilterList;


            }
            else if (Cmodel.CurrentDeptId != "")//not VS user
            {
                listnew = list.OrderByDescending(a => a.CreatedDate).ToList();
                var CheckList = listnew.Where(a => a.ItemTypeId == Cmodel.ItemTypeId).ToList();

                //var FilterList = CheckList.Where(a => a.IsLinked == false).ToList();
                return CheckList;


            }
            else//Vs user
            {
                listnew = list.OrderByDescending(a => a.CreatedDate).ToList();
                var CheckList = listnew.Where(a => a.ItemTypeId == Cmodel.ItemTypeId).ToList();

                //var FilterList = CheckList.Where(a => a.IsLinked == false).ToList();
                return CheckList;

            }
        }




        public static object GetPendencyListforLinking(object param)
        {
            CommitteReplyPendencyContext db = new CommitteReplyPendencyContext();
            List<pHouseCommitteeItemPendency> Parentlist = new List<pHouseCommitteeItemPendency>();
            List<pHouseCommitteeSubItemPendency> Childlist = new List<pHouseCommitteeSubItemPendency>();
            pHouseCommitteeItemPendency Cmodel = param as pHouseCommitteeItemPendency;
            List<pHouseCommitteeItemPendency> nqueryAll = new List<pHouseCommitteeItemPendency>();
            List<pHouseCommitteeSubItemPendency> queryAll = new List<pHouseCommitteeSubItemPendency>();
            using (eFileContext ctx = new eFileContext())
            {

                var PList = (from a in ctx.pHouseCommitteeItemPendency
                             where a.IsLocked == true
                             select a).AsExpandable().ToList();
                nqueryAll = PList.ToList();
                var ParentList = nqueryAll.ToList();
                Parentlist = ParentList;


                var CList = (from element in ctx.pHouseCommitteeSubItemPendency
                             where element.IsLocked == true
                             select element).AsExpandable().ToList();
                queryAll = CList.ToList();
                var ChildList = queryAll.ToList();
                Childlist = ChildList;

            }
            List<pHouseCommitteeItemPendency> list = new List<pHouseCommitteeItemPendency>();

            foreach (var item in Parentlist)
            {
                var Check = Childlist.Where(a => a.ParentId == item.PendencyId && a.IsLocked == true).ToList();
                if (Check.Count != 0)
                {

                    var item1 = Check.OrderByDescending(c => c.CreatedDate).Take(1).SingleOrDefault();
                    pHouseCommitteeItemPendency mdl1 = new pHouseCommitteeItemPendency();
                    mdl1.SubPendencyId = item1.SubPendencyId;
                    mdl1.MainPendencyId = item1.ParentId;
                    mdl1.ParentId = item1.ParentId;
                    mdl1.CommitteeId = item1.CommitteeId;
                    //mdl.PaperNature = item1.PaperNature;
                    //mdl.CommitteeName = item1.CommitteeName;

                    mdl1.ItemReplyPdfPath = item1.ItemReplyPdfPath;
                    mdl1.AnnexPdfPath = item1.AnnexPdfPath;
                    if (item1.AnnexFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                        mdl1.AnnexFilesNameByUser = str;
                    }
                    else
                    {
                        mdl1.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                    }
                    mdl1.DepartmentName = (from a in db.objCommitteReplyPendency
                                           join b in db.mDepartment on a.DepartmentId equals b.deptId
                                           where mdl1.SentByDepartmentId == b.deptId
                                           select b.deptname).FirstOrDefault();
                    mdl1.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                         join b in db.mDepartment on a.DepartmentId equals b.deptId
                                         where mdl1.SentByDepartmentId == b.deptId
                                         select b.deptabbr).FirstOrDefault();
                    mdl1.DocFileName = item1.DocFileName;
                    mdl1.DocFilePath = item1.DocFilePath;
                    mdl1.ItemDescription = item1.ItemDescription;
                    //mdl.ItemName=item1.
                    mdl1.SentByDepartmentId = item1.SentByDepartmentId;
                    mdl1.CreatedDate = item1.CreatedDate;
                    mdl1.ItemTypeId = item1.ItemTypeId;
                    mdl1.ItemTypeName = item1.ItemTypeName;
                    mdl1.IsLinked = item1.IsLinked;
                    mdl1.IsLocked = item1.IsLocked;
                    mdl1.ItemNumber = item1.ItemNumber;
                    mdl1.ItemNatureId = item1.ItemNatureId;
                    mdl1.ItemNature = item1.ItemNature;
                    mdl1.FinancialYear = item1.FinancialYear;
                    mdl1.CAGYear = item1.CAGYear;
                    mdl1.SubItemTypeId = item1.SubItemTypeId;
                    mdl1.SubItemTypeName = item1.SubItemTypeName;
                    list.Add(mdl1);

                }
                else
                {
                    pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                    mdl.PendencyId = item.PendencyId;
                    mdl.MainPendencyId = item.PendencyId;
                    mdl.ParentId = item.ParentId;
                    mdl.CommitteeId = item.CommitteeId;
                    //mdl.PaperNature = item1.PaperNature;
                    //mdl.CommitteeName = item1.CommitteeName;

                    mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                    mdl.AnnexPdfPath = item.AnnexPdfPath;
                    if (item.AnnexFilesNameByUser != null)
                    {
                        string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                        mdl.AnnexFilesNameByUser = str;
                    }
                    else
                    {
                        mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                    }
                    mdl.DepartmentName = (from a in db.objCommitteReplyPendency
                                          join b in db.mDepartment on a.DepartmentId equals b.deptId
                                          where mdl.SentByDepartmentId == b.deptId
                                          select b.deptname).FirstOrDefault();
                    mdl.AbbrDeptName = (from a in db.objCommitteReplyPendency
                                        join b in db.mDepartment on a.DepartmentId equals b.deptId
                                        where mdl.SentByDepartmentId == b.deptId
                                        select b.deptabbr).FirstOrDefault();
                    mdl.DocFileName = item.DocFileName;
                    mdl.DocFilePath = item.DocFilePath;
                    mdl.ItemDescription = item.ItemDescription;
                    //mdl.ItemName=item1.
                    mdl.SentByDepartmentId = item.SentByDepartmentId;
                    mdl.CreatedDate = item.CreatedDate;
                    mdl.ItemTypeId = item.ItemTypeId;
                    mdl.ItemTypeName = item.ItemTypeName;
                    mdl.IsLinked = item.IsLinked;
                    mdl.IsLocked = item.IsLocked;
                    mdl.ItemNumber = item.ItemNumber;
                    mdl.ItemNatureId = item.ItemNatureId;
                    mdl.ItemNature = item.ItemNature;
                    mdl.FinancialYear = item.FinancialYear;
                    mdl.CAGYear = item.CAGYear;
                    mdl.SubItemTypeId = item.SubItemTypeId;
                    mdl.SubItemTypeName = item.SubItemTypeName;
                    list.Add(mdl);
                }



            }





            var ListIs = list.OrderByDescending(a => a.CreatedDate).ToList();

            List<pHouseCommitteeItemPendency> listnew = new List<pHouseCommitteeItemPendency>();
            ////////////////Changed for listing
            if (Cmodel.CurrentDeptId.Contains(','))//not VS user/For HOD Having multiple Dept
            {
                string[] str1 = new string[4];
                str1 = Cmodel.CurrentDeptId.Split(',');
                foreach (var item in str1)
                {
                    string CurrentDeptId = item;
                    var CheckList = ListIs.Where(a => a.SentByDepartmentId == CurrentDeptId && a.ItemTypeId == Cmodel.ItemTypeId
                                    && a.SentByDepartmentId == Cmodel.SentByDepartmentId).ToList();
                    //  var CheckList1 = ListIs.Where(a => a.ByDepartmentId == CurrentDeptId && a.SendStatus == true).ToList();
                    listnew.AddRange(CheckList);
                }
                listnew = listnew.OrderByDescending(a => a.CreatedDate).ToList();
                return listnew;
            }
            else if (Cmodel.CurrentDeptId != "")//not VS user
            {
                var CheckList = ListIs.Where(a => a.SentByDepartmentId == Cmodel.CurrentDeptId && a.ItemTypeId == Cmodel.ItemTypeId
                      && a.SentByDepartmentId == Cmodel.SentByDepartmentId).ToList();
                // var checkit = CheckList.Where(a => a.IsLinked == false).ToList();
                var checkit = CheckList.Where(a => a.IsLocked == true).ToList();
                checkit = checkit.OrderByDescending(a => a.CreatedDate).ToList();
                return checkit;
            }
            else//Vs user
            {
                //Get commite By BarnchId
                using (eFileContext ctxCommitee = new eFileContext())
                {
                    foreach (var item in Cmodel.tCommitteeList)
                    {
                        int CommitteId = item.CommitteeId;
                        var CheckList = ListIs.Where(a => a.CommitteeId == CommitteId).ToList();
                        listnew.AddRange(CheckList);

                    }
                }
                listnew = listnew.OrderByDescending(a => a.CreatedDate).ToList();
                return listnew;
            }
        }
        public static object GetItemPendencyByParentIdforLinking(object param)
        {
            List<pHouseCommitteeItemPendency> Parentlist = new List<pHouseCommitteeItemPendency>();
            List<pHouseCommitteeSubItemPendency> Childlist = new List<pHouseCommitteeSubItemPendency>();
            List<pHouseCommitteeItemPendency> list = new List<pHouseCommitteeItemPendency>();
            SiteSettingContext settingContext = new SiteSettingContext();
            eFileContext db = new eFileContext();
            pHouseCommitteeItemPendency model = param as pHouseCommitteeItemPendency;
            if (model.CurrentDeptId != "")//not vs user/only for single dept
            {
                var plist = (from dist in db.pHouseCommitteeItemPendency
                             where dist.PendencyId == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Parentlist = plist.ToList();
                var clist = (from dist in db.pHouseCommitteeSubItemPendency
                             where dist.ParentId == model.ParentId && dist.IsLocked == true
                             orderby dist.CreatedDate descending
                             select dist);
                Childlist = clist.ToList();
            }
            else //for VS Users Only
            {
                var plist = (from dist in db.pHouseCommitteeItemPendency
                             where dist.PendencyId == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Parentlist = plist.ToList();
                var clist = (from dist in db.pHouseCommitteeSubItemPendency
                             where dist.ParentId == model.ParentId
                             orderby dist.CreatedDate descending
                             select dist);
                Childlist = clist.ToList();
            }

            List<pHouseCommitteeItemPendency> list1 = new List<pHouseCommitteeItemPendency>();
            foreach (var item in Parentlist)
            {
                pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                mdl.MainPendencyId = item.PendencyId;
                mdl.ParentId = item.ParentId;
                mdl.FinancialYear = item.FinancialYear;
                mdl.SentByDepartmentId = item.SentByDepartmentId;
                mdl.ItemTypeId = item.ItemTypeId;
                mdl.ItemTypeName = item.ItemTypeName;
                mdl.ItemNature = item.ItemNature;
                mdl.ItemNatureId = item.ItemNatureId;
                mdl.ItemNumber = item.ItemNumber;
                mdl.ItemDescription = item.ItemDescription;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                mdl.DocFileName = item.DocFileName;
                mdl.DocFilePath = item.DocFilePath;
                mdl.SentOnDate = item.SentOnDate;
                //mdl.SentOnDateByVS = item.SentOnDateByVS;
                mdl.CommitteeId = item.CommitteeId;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                if (item.AnnexFilesNameByUser != null)
                {
                    string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                    mdl.AnnexFilesNameByUser = str;
                }
                else
                {
                    mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                }
                mdl.CreatedDate = item.CreatedDate;
                mdl.IsLinked = item.IsLinked;
                mdl.IsLocked = item.IsLocked;
                mdl.Linked_RecordId = item.Linked_RecordId;
                mdl.eFileID = item.eFileID;
                var FileAccessingUrlPath = (from itemis in settingContext.SiteSettings where itemis.SettingName == "FileAccessingUrlPath" select itemis.SettingValue).SingleOrDefault();
                mdl.FileAccessingUrlPath = FileAccessingUrlPath;
                list.Add(mdl);
            }
            foreach (var item in Childlist)
            {
                pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                mdl.MainPendencyId = item.ParentId;
                mdl.SubPendencyId = item.SubPendencyId;
                mdl.ParentId = item.ParentId;

                mdl.FinancialYear = item.FinancialYear;
                mdl.SentByDepartmentId = item.SentByDepartmentId;
                mdl.ItemTypeId = item.ItemTypeId;
                mdl.ItemTypeName = item.ItemTypeName;
                mdl.ItemNature = item.ItemNature;
                mdl.ItemNatureId = item.ItemNatureId;
                mdl.ItemNumber = item.ItemNumber;
                mdl.ItemDescription = item.ItemDescription;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                mdl.DocFileName = item.DocFileName;
                mdl.DocFilePath = item.DocFilePath;
                mdl.SentOnDate = item.SentOnDate;
                // mdl.SentOnDateByVS = item.SentOnDateByVS;
                mdl.CommitteeId = item.CommitteeId;
                mdl.AnnexPdfPath = item.AnnexPdfPath;
                if (item.AnnexFilesNameByUser != null)
                {
                    string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                    mdl.AnnexFilesNameByUser = str;
                }
                else
                {
                    mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                }
                mdl.CreatedDate = item.CreatedDate;
                mdl.IsLinked = item.IsLinked;
                mdl.IsLocked = item.IsLocked;
                mdl.Linked_RecordId = item.Linked_RecordId;
                mdl.eFileID = item.eFileID;
                var FileAccessingUrlPath = (from itemis in settingContext.SiteSettings where itemis.SettingName == "FileAccessingUrlPath" select itemis.SettingValue).SingleOrDefault();
                mdl.FileAccessingUrlPath = FileAccessingUrlPath;

                list.Add(mdl);
            }
            // var FilterList = list.Where(a => a.IsLinked == false).ToList();

            var ListIs = list.OrderByDescending(a => a.CreatedDate).ToList();

            //var Index = FilterList.Count - 1;
            ListIs.RemoveAt(0);
            return ListIs;
        }



        public static object CheckItemNumberExists(object param)
        {
            int count = 0;
            List<pHouseCommitteeItemPendency> Parentlist = new List<pHouseCommitteeItemPendency>();
            SiteSettingContext settingContext = new SiteSettingContext();
            eFileContext db = new eFileContext();
            pHouseCommitteeItemPendency model = param as pHouseCommitteeItemPendency;

            if (model.ItemTypeId == 10) //if Audit Para
            {
                var plist = (from dist in db.pHouseCommitteeItemPendency
                             where dist.ItemNumber == model.ItemNumber
                             && dist.SentByDepartmentId == model.SentByDepartmentId
                             && dist.ItemTypeId == model.ItemTypeId
                             && dist.CAGYear == model.CAGYear

                             select dist);

                if (model.SubItemTypeId != null)
                {
                    plist = plist.Where(a => a.SubItemTypeId == model.SubItemTypeId);

                }

                plist.OrderByDescending(a => a.CreatedDate).ToList();
                Parentlist = plist.ToList();
            }
            else
            {

                var plist = (from dist in db.pHouseCommitteeItemPendency
                             where dist.ItemNumber == model.ItemNumber
                             && dist.SentByDepartmentId == model.SentByDepartmentId
                             && dist.ItemTypeId == model.ItemTypeId

                             && dist.FinancialYear == model.FinancialYear


                             orderby dist.CreatedDate descending
                             select dist);
                Parentlist = plist.ToList();
            }


            //Parentlist = plist.ToList();

            if (Parentlist.Count > 0)
            {
                count++;
            }
            else
            {

            }
            return count;
        }
        public static object LockMainPendency(object param)
        {
            eFileContext db = new eFileContext();
            pHouseCommitteeItemPendency model = param as pHouseCommitteeItemPendency;
            pHouseCommitteeItemPendency MainItemPen = new pHouseCommitteeItemPendency();
            int count = 1;
            MainItemPen = (from mb in db.pHouseCommitteeItemPendency where mb.PendencyId == model.PendencyId select mb).SingleOrDefault();
            if (MainItemPen != null)
            {
                MainItemPen.IsLocked = true;
                db.pHouseCommitteeItemPendency.Attach(MainItemPen);
                db.Entry(MainItemPen).State = EntityState.Modified;
                db.SaveChanges();
            }

            return count;
        }

        public static object LockSubPendency(object param)
        {
            eFileContext db = new eFileContext();
            pHouseCommitteeSubItemPendency model = param as pHouseCommitteeSubItemPendency;
            pHouseCommitteeSubItemPendency SubItemPen = new pHouseCommitteeSubItemPendency();
            int count = 1;
            SubItemPen = (from mb in db.pHouseCommitteeSubItemPendency where mb.SubPendencyId == model.SubPendencyId select mb).SingleOrDefault();
            if (SubItemPen != null)
            {
                SubItemPen.IsLocked = true;
                db.pHouseCommitteeSubItemPendency.Attach(SubItemPen);
                db.Entry(SubItemPen).State = EntityState.Modified;
                db.SaveChanges();
            }

            return count;
        }
        public static object GetDetailsByPendencyId(object param)
        {
            eFileContext db = new eFileContext();
            pHouseCommitteeItemPendency model = param as pHouseCommitteeItemPendency;
            var plist = (from dist in db.pHouseCommitteeItemPendency
                         where dist.PendencyId == model.PendencyId
                         select dist);
            var ListIs = plist.ToList();
            return ListIs;
        }

        public static object GetDetailsBySubPendencyId(object param)
        {
            eFileContext db = new eFileContext();
            pHouseCommitteeSubItemPendency model = param as pHouseCommitteeSubItemPendency;
            List<pHouseCommitteeSubItemPendency> Childlist = new List<pHouseCommitteeSubItemPendency>();
            int SID = Convert.ToInt16(model.SubPendencyId);
            var plist = (from dist in db.pHouseCommitteeSubItemPendency
                         where dist.SubPendencyId == SID
                         select dist).ToList();
            var ListIs = plist.ToList();
            return ListIs;
        }


        public static object UpdateMainPendency(object param)
        {
            eFileContext db = new eFileContext();
            pHouseCommitteeItemPendency mdl = param as pHouseCommitteeItemPendency;
            pHouseCommitteeItemPendency DraftObj = new pHouseCommitteeItemPendency();
            int count = 1;
            DraftObj = (from mb in db.pHouseCommitteeItemPendency where mb.PendencyId == mdl.PendencyId select mb).SingleOrDefault();
            if (DraftObj != null)
            {

                DraftObj.SentByDepartmentId = mdl.SentByDepartmentId;
                DraftObj.ItemTypeId = mdl.ItemTypeId;
                DraftObj.ItemTypeName = mdl.ItemTypeName;
                if (mdl.ItemTypeId == 10)
                {
                    DraftObj.SubItemTypeId = mdl.SubItemTypeId;
                    DraftObj.SubItemTypeName = mdl.SubItemTypeName;
                    DraftObj.CAGYear = mdl.CAGYear;
                }
                else
                {
                    DraftObj.FinancialYear = mdl.FinancialYear;
                }
                DraftObj.ItemNumber = mdl.ItemNumber;
                DraftObj.ItemNatureId = mdl.ItemNatureId;
                DraftObj.ItemNature = mdl.ItemNature;
                DraftObj.ItemDescription = mdl.ItemDescription;

                DraftObj.ModifiedBy = mdl.ModifiedBy;
                DraftObj.SentOnDate = DateTime.Now;
                DraftObj.ModifiedDate = DateTime.Now;

                if (mdl.ReplyPdfIsChange == true)
                {
                    DraftObj.ItemReplyPdfPath = mdl.ItemReplyPdfPath;
                }
                else
                {
                    DraftObj.ItemReplyPdfPath = DraftObj.ItemReplyPdfPath;
                }
                if (mdl.AnnexPdfIsChange == true)
                {
                    DraftObj.AnnexPdfPath = mdl.AnnexPdfPath;
                    DraftObj.AnnexFilesNameByUser = mdl.AnnexFilesNameByUser;
                }
                else
                {
                    DraftObj.AnnexPdfPath = DraftObj.AnnexPdfPath;
                    DraftObj.AnnexFilesNameByUser = DraftObj.AnnexFilesNameByUser;
                }
                if (mdl.DocFileIsChange == true)
                {
                    DraftObj.DocFilePath = mdl.DocFilePath;
                    DraftObj.DocFileName = mdl.DocFileName;
                }
                else
                {
                    DraftObj.DocFilePath = DraftObj.DocFilePath;
                    DraftObj.DocFileName = DraftObj.DocFileName;
                }

                db.SaveChanges();
            }

            return count;
        }

        public static object UpdateSubPendency(object param)
        {
            eFileContext db = new eFileContext();
            pHouseCommitteeSubItemPendency mdl = param as pHouseCommitteeSubItemPendency;
            pHouseCommitteeSubItemPendency DraftObj = new pHouseCommitteeSubItemPendency();
            int count = 1;
            DraftObj = (from mb in db.pHouseCommitteeSubItemPendency where mb.SubPendencyId == mdl.SubPendencyId select mb).SingleOrDefault();
            if (DraftObj != null)
            {

                DraftObj.SentByDepartmentId = mdl.SentByDepartmentId;
                DraftObj.ItemTypeId = mdl.ItemTypeId;
                DraftObj.ItemTypeName = mdl.ItemTypeName;
                if (mdl.ItemTypeId == 10)
                {
                    DraftObj.SubItemTypeId = mdl.SubItemTypeId;
                    DraftObj.SubItemTypeName = mdl.SubItemTypeName;
                    DraftObj.CAGYear = mdl.CAGYear;
                }
                else
                {
                    DraftObj.FinancialYear = mdl.FinancialYear;
                }
                DraftObj.ItemNumber = mdl.ItemNumber;
                DraftObj.ItemNatureId = mdl.ItemNatureId;
                DraftObj.ItemNature = mdl.ItemNature;
                DraftObj.ItemDescription = mdl.ItemDescription;

                DraftObj.ModifiedBy = mdl.ModifiedBy;
                DraftObj.SentOnDate = DateTime.Now;
                DraftObj.ModifiedDate = DateTime.Now;

                if (mdl.ReplyPdfIsChange == true)
                {
                    DraftObj.ItemReplyPdfPath = mdl.ItemReplyPdfPath;
                }
                else
                {
                    DraftObj.ItemReplyPdfPath = DraftObj.ItemReplyPdfPath;
                }
                if (mdl.AnnexPdfIsChange == true)
                {
                    DraftObj.AnnexPdfPath = mdl.AnnexPdfPath;
                    DraftObj.AnnexFilesNameByUser = mdl.AnnexFilesNameByUser;
                }
                else
                {
                    DraftObj.AnnexPdfPath = DraftObj.AnnexPdfPath;
                    DraftObj.AnnexFilesNameByUser = DraftObj.AnnexFilesNameByUser;
                }
                if (mdl.DocFileIsChange == true)
                {
                    DraftObj.DocFilePath = mdl.DocFilePath;
                    DraftObj.DocFileName = mdl.DocFileName;
                }
                else
                {
                    DraftObj.DocFilePath = DraftObj.DocFilePath;
                    DraftObj.DocFileName = DraftObj.DocFileName;
                }

                db.SaveChanges();
            }

            return count;
        }


        public static object GetDetailsByRecordId(object param)
        {
            eFileContext db = new eFileContext();
            pHouseCommitteeSubFiles model = param as pHouseCommitteeSubFiles;
            var plist = (from dist in db.pHouseCommitteeSubFiles
                         where dist.RecordId == model.RecordId
                         select dist);
            var ListIs = plist.ToList();
            return ListIs;
        }

        public static object GetDataBySearchParam(object param)
        {
            try
            {
                pHouseCommitteeItemPendency obj = param as pHouseCommitteeItemPendency;
                List<pHouseCommitteeItemPendency> Parentlist = new List<pHouseCommitteeItemPendency>();
                List<pHouseCommitteeSubItemPendency> Childlist = new List<pHouseCommitteeSubItemPendency>();
                List<pHouseCommitteeItemPendency> list = new List<pHouseCommitteeItemPendency>();
                List<pHouseCommitteeItemPendency> listnew = new List<pHouseCommitteeItemPendency>();
                using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
                {

                    var query = (from a in db.pHouseCommitteeItemPendency
                                 where obj.ItemTypeId == a.ItemTypeId
                                 select a);
                    if (obj.ItemTypeId != null)
                    {
                        query = query.Where(a => a.ItemTypeId == obj.ItemTypeId);
                    }
                    if (obj.SubItemTypeId != 0)
                    {
                        query = query.Where(a => a.SubItemTypeId == obj.SubItemTypeId);
                    }

                    if (obj.ItemTypeId == 10)
                    {
                        if (obj.CAGYear != "0")
                        {
                            query = query.Where(a => a.CAGYear == obj.FinancialYear);
                        }
                    }
                    else
                    {
                        if (obj.FinancialYear != "0")
                        {
                            query = query.Where(a => a.FinancialYear == obj.FinancialYear);
                        }
                    }



                    if (obj.SentByDepartmentId != "0")
                    {
                        query = query.Where(a => a.SentByDepartmentId == obj.SentByDepartmentId);
                    }
                    Parentlist = query.ToList();

                    foreach (var item in Parentlist)
                    {

                        var Check = Childlist.Where(a => a.ParentId == item.PendencyId).ToList();
                        if (Check.Count != 0)
                        {
                            var Check1 = Childlist.Where(a => a.ParentId == item.PendencyId).ToList();
                            foreach (var item1 in Check1)
                            {
                                pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                                mdl.SubPendencyId = item1.SubPendencyId;
                                mdl.MainPendencyId = item1.ParentId;
                                mdl.ParentId = item1.ParentId;
                                mdl.FinancialYear = item1.FinancialYear;
                                mdl.CAGYear = item1.CAGYear;
                                mdl.SentByDepartmentId = item1.SentByDepartmentId;
                                mdl.ItemTypeId = item1.ItemTypeId;
                                mdl.ItemTypeName = item.ItemTypeName;
                                mdl.SubItemTypeId = item.SubItemTypeId;
                                mdl.SubItemTypeName = item.SubItemTypeName;
                                mdl.ItemNature = item1.ItemNature;
                                mdl.ItemNatureId = item1.ItemNatureId;
                                mdl.ItemNumber = item1.ItemNumber;
                                mdl.ItemDescription = item1.ItemDescription;
                                mdl.AnnexPdfPath = item1.AnnexPdfPath;
                                mdl.ItemReplyPdfPath = item1.ItemReplyPdfPath;
                                mdl.DocFileName = item1.DocFileName;
                                mdl.DocFilePath = item1.DocFilePath;
                                mdl.SentOnDate = item1.SentOnDate;
                                mdl.CommitteeId = item1.CommitteeId;
                                mdl.AnnexPdfPath = item1.AnnexPdfPath;
                                if (item1.AnnexFilesNameByUser != null)
                                {
                                    string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                                    mdl.AnnexFilesNameByUser = str;
                                }
                                else
                                {
                                    mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                                }
                                mdl.CreatedDate = item1.CreatedDate;
                                mdl.eFileID = item1.eFileID;
                                mdl.IsLocked = item1.IsLocked;
                                mdl.IsLinked = item1.IsLinked;
                                mdl.Linked_RecordId = item1.Linked_RecordId;
                                list.Add(mdl);
                            }
                        }
                        else
                        {
                            pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                            mdl.MainPendencyId = item.PendencyId;
                            mdl.ParentId = item.ParentId;
                            mdl.PendencyId = item.PendencyId;
                            mdl.FinancialYear = item.FinancialYear;
                            mdl.CAGYear = item.CAGYear;
                            mdl.SentByDepartmentId = item.SentByDepartmentId;
                            mdl.ItemTypeId = item.ItemTypeId;
                            mdl.ItemTypeName = item.ItemTypeName;
                            mdl.SubItemTypeId = item.SubItemTypeId;
                            mdl.SubItemTypeName = item.SubItemTypeName;
                            mdl.ItemNature = item.ItemNature;
                            mdl.ItemNatureId = item.ItemNatureId;
                            mdl.ItemNumber = item.ItemNumber;
                            mdl.ItemDescription = item.ItemDescription;
                            mdl.AnnexPdfPath = item.AnnexPdfPath;
                            mdl.DocFileName = item.DocFileName;
                            mdl.DocFilePath = item.DocFilePath;
                            mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                            mdl.SentOnDate = item.SentOnDate;
                            mdl.CommitteeId = item.CommitteeId;
                            mdl.AnnexPdfPath = item.AnnexPdfPath;
                            if (item.AnnexFilesNameByUser != null)
                            {
                                string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                                mdl.AnnexFilesNameByUser = str;
                            }
                            else
                            {
                                mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                            }
                            mdl.CreatedDate = item.CreatedDate;
                            mdl.eFileID = item.eFileID;
                            mdl.IsLocked = item.IsLocked;
                            mdl.IsLinked = item.IsLinked;
                            mdl.Linked_RecordId = item.Linked_RecordId;
                            list.Add(mdl);
                        }

                    }
                    listnew = list.OrderByDescending(a => a.CreatedDate).ToList();
                    return listnew;
                }
            }
            catch
            {
                throw;
            }
        }



        public static object GetDataBySearchParameter(object param)
        {
            pHouseCommitteeItemPendency model = param as pHouseCommitteeItemPendency;
            List<pHouseCommitteeItemPendency> listnew = new List<pHouseCommitteeItemPendency>();
            List<pHouseCommitteeSubItemPendency> Childlist = new List<pHouseCommitteeSubItemPendency>();
            List<pHouseCommitteeItemPendency> list = new List<pHouseCommitteeItemPendency>();

            string[] arr = param as string[];
            int nItemTypeId = Convert.ToInt16(arr[0]);
            int nSubItemTypeId = Convert.ToInt16(arr[1]);
            string FY = arr[2];
            string DeptId = arr[3];
            using (CommitteReplyPendencyContext db = new CommitteReplyPendencyContext())
            {
                var predicate = PredicateBuilder.True<npHouseCommitteItemPendency>();
                if (nItemTypeId != 0)
                    predicate = predicate.And(q => q.ItemTypeId == nItemTypeId);
                if (nItemTypeId == 10)
                {
                    if (FY != "0")
                        predicate = predicate.And(q => q.CAGYear == FY);
                }
                else
                {
                    if (FY != "0")
                        predicate = predicate.And(q => q.FinancialYear == FY);
                }

                if (nSubItemTypeId != 0)
                    predicate = predicate.And(q => q.SubItemTypeId == nSubItemTypeId);

                if (DeptId != "0")
                    predicate = predicate.And(q => q.SentByDepartmentId == DeptId);

                var Parentlist = (from a in db.pHouseCommitteeItemPendency
                                  select new npHouseCommitteItemPendency
                                  {
                                      PendencyId = a.PendencyId,
                                      CommitteeId = a.CommitteeId,
                                      SentByDepartmentId = a.SentByDepartmentId,
                                      ItemTypeId = a.ItemTypeId,
                                      ItemTypeName = a.ItemTypeName,
                                      SubItemTypeId = a.SubItemTypeId,
                                      SubItemTypeName = a.SubItemTypeName,
                                      ItemNumber = a.ItemNumber,
                                      ItemNatureId = a.ItemNatureId,
                                      ItemNature = a.ItemNature,
                                      ItemDescription = a.ItemDescription,
                                      FinancialYear = a.FinancialYear,
                                      CAGYear = a.CAGYear,
                                      SentOnDate = a.SentOnDate,
                                      AnnexPdfPath = a.AnnexPdfPath,
                                      AnnexFilesNameByUser = a.AnnexFilesNameByUser,
                                      ItemReplyPdfPath = a.ItemReplyPdfPath,
                                      ItemReplyPdfNameByUser = a.ItemReplyPdfNameByUser,
                                      DocFilePath = a.DocFilePath,
                                      DocFileName = a.DocFileName,
                                      eFileID = a.eFileID,
                                      IsLocked = a.IsLocked,
                                      IsLinked = a.IsLinked,
                                      Linked_RecordId = a.Linked_RecordId,
                                      Status = a.Status,
                                      AbbrDeptName = (from EN in db.mDepartment where EN.deptId == a.SentByDepartmentId select EN.deptabbr).FirstOrDefault(),
                                      //CommitteeName = (from EN in db.objtcommittee where EN.CommitteeId == a.CommitteeId select EN.CommitteeName).FirstOrDefault(),
                                  }).AsExpandable().Where(predicate).OrderByDescending(a => a.PendencyId);

                foreach (var item in Parentlist)
                {
                    var Check = Childlist.Where(a => a.ParentId == item.PendencyId).ToList();
                    if (Check.Count != 0)
                    {
                        foreach (var item1 in Check)
                        {
                            pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                            mdl.SubPendencyId = item1.SubPendencyId;
                            mdl.MainPendencyId = item1.ParentId;
                            mdl.ParentId = item1.ParentId;
                            mdl.FinancialYear = item1.FinancialYear;
                            mdl.CAGYear = item1.CAGYear;
                            mdl.SentByDepartmentId = item1.SentByDepartmentId;
                            mdl.ItemTypeId = item1.ItemTypeId;
                            mdl.ItemTypeName = item.ItemTypeName;
                            mdl.SubItemTypeId = item.SubItemTypeId;
                            mdl.SubItemTypeName = item.SubItemTypeName;
                            mdl.ItemNature = item1.ItemNature;
                            mdl.ItemNatureId = item1.ItemNatureId;
                            mdl.ItemNumber = item1.ItemNumber;
                            mdl.ItemDescription = item1.ItemDescription;
                            mdl.AnnexPdfPath = item1.AnnexPdfPath;
                            mdl.ItemReplyPdfPath = item1.ItemReplyPdfPath;
                            mdl.DocFileName = item1.DocFileName;
                            mdl.DocFilePath = item1.DocFilePath;
                            mdl.SentOnDate = item1.SentOnDate;
                            mdl.CommitteeId = item1.CommitteeId;
                            mdl.AnnexPdfPath = item1.AnnexPdfPath;
                            if (item1.AnnexFilesNameByUser != null)
                            {
                                string str = System.Text.RegularExpressions.Regex.Replace(item1.AnnexFilesNameByUser, @"\s+", "");
                                mdl.AnnexFilesNameByUser = str;
                            }
                            else
                            {
                                mdl.AnnexFilesNameByUser = item1.AnnexFilesNameByUser;
                            }

                            mdl.AbbrDeptName = item1.AbbrDeptName;
                            mdl.CreatedDate = item1.CreatedDate;
                            mdl.eFileID = item1.eFileID;
                            mdl.IsLocked = item1.IsLocked;
                            mdl.IsLinked = item1.IsLinked;
                            mdl.Linked_RecordId = item1.Linked_RecordId;
                            list.Add(mdl);
                        }
                    }
                    else
                    {
                        pHouseCommitteeItemPendency mdl = new pHouseCommitteeItemPendency();
                        mdl.MainPendencyId = item.PendencyId;
                        mdl.ParentId = item.ParentId;
                        mdl.PendencyId = item.PendencyId;
                        mdl.FinancialYear = item.FinancialYear;
                        mdl.CAGYear = item.CAGYear;
                        mdl.SentByDepartmentId = item.SentByDepartmentId;
                        mdl.AbbrDeptName = item.AbbrDeptName;

                        mdl.ItemTypeId = item.ItemTypeId;
                        mdl.ItemTypeName = item.ItemTypeName;
                        mdl.SubItemTypeId = item.SubItemTypeId;
                        mdl.SubItemTypeName = item.SubItemTypeName;
                        mdl.ItemNature = item.ItemNature;
                        mdl.ItemNatureId = item.ItemNatureId;
                        mdl.ItemNumber = item.ItemNumber;
                        mdl.ItemDescription = item.ItemDescription;
                        mdl.AnnexPdfPath = item.AnnexPdfPath;
                        mdl.DocFileName = item.DocFileName;
                        mdl.DocFilePath = item.DocFilePath;
                        mdl.ItemReplyPdfPath = item.ItemReplyPdfPath;
                        mdl.SentOnDate = item.SentOnDate;
                        mdl.CommitteeId = item.CommitteeId;
                        mdl.AnnexPdfPath = item.AnnexPdfPath;
                        if (item.AnnexFilesNameByUser != null)
                        {
                            string str = System.Text.RegularExpressions.Regex.Replace(item.AnnexFilesNameByUser, @"\s+", "");
                            mdl.AnnexFilesNameByUser = str;
                        }
                        else
                        {
                            mdl.AnnexFilesNameByUser = item.AnnexFilesNameByUser;
                        }
                        mdl.CreatedDate = item.CreatedDate;
                        mdl.eFileID = item.eFileID;
                        mdl.IsLocked = item.IsLocked;
                        mdl.IsLinked = item.IsLinked;
                        mdl.Linked_RecordId = item.Linked_RecordId;
                        list.Add(mdl);
                    }
                }

                listnew = list.OrderByDescending(a => a.CreatedDate).ToList();
                //model.HouseCommitteeItemPendency = listnew;
                return listnew;
            }

        }

    }
}
