﻿using SBL.DAL;
using SBL.DomainModel.Models.CommitteeReport;
using System.Data;
using System.Data.Entity;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.CommitteeReport
{
    class CommitteeReportContext : DBBase<CommitteeReportContext>
    {

        public CommitteeReportContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false;                          }


        public DbSet<tCommitteeReportLib> tCommitteeReportLib { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFile> eFile { get; set; }
        public DbSet<SBL.DomainModel.Models.Department.mDepartment> department { get; set; }
        public DbSet<SBL.DomainModel.Models.User.mUsers> users { get; set; }
        public virtual DbSet<SBL.DomainModel.Models.Assembly.mAssembly> mAssemblies { get; set; }
        public virtual DbSet<SBL.DomainModel.Models.Session.mSession> mSessions { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.tCommittee> objtcommittee { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.mCommitteeType> mCommitteeType { get; set; }
        public DbSet<SBL.DomainModel.Models.eFile.eFileAttachment> attachment { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateCommitteeReport":
                    {
                        return CreateCommitteeReport(param.Parameter);
                    }
                case "UpdateCommitteeReport":
                    {
                        return UpdateCommitteeReport(param.Parameter);
                    }
                case "DeleteCommitteeReport":
                    {
                        return DeleteCommitteeReport(param.Parameter);
                    }
                case "GetAllCommitteeReportDetails":
                    {
                        return GetAllCommitteeReportDetails(param.Parameter);
                    }
                case "GetCommitteeReportById":
                    {
                        return GetCommitteeReportById(param.Parameter);
                    }
                case "eFileCommitteeReport":
                    {
                        return eFileCommitteeReport(param.Parameter);
                    }
                case "GetReviewCommitteeReportSectionOffice":
                    {
                        return GetReviewCommitteeReportSectionOffice(param.Parameter);
                    }
                case "FinalApprovalForCommitteeReport":
                    {
                        return FinalApprovalForCommitteeReport(param.Parameter);
                    }
                case "SpeakerApprovalForCommitteeReport":
                    {
                        return SpeakerApprovalForCommitteeReport(param.Parameter);
                    }

            }

            return null;
        }

        public static object SpeakerApprovalForCommitteeReport(object param)
        {
            try
            {
                using (CommitteeReportContext context = new CommitteeReportContext())
                {
                    string type = Convert.ToString(param);
                    var result = (from attachments in context.attachment where attachments.Type == type && attachments.SendStatus == false  && attachments.Approve == 1 select attachments).ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                string exs = ex.ToString();
            }
            return null;
        }

        public static object FinalApprovalForCommitteeReport(object param)
        {
            try
            {
                using (CommitteeReportContext context = new CommitteeReportContext())
                {
                    string type = Convert.ToString(param);
                    var result = (from attachments in context.attachment where attachments.Type == type && attachments.SendStatus==false && attachments.Approve==1 select attachments).ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                string exs = ex.ToString();
            }
            return null;
        }

        public static object GetReviewCommitteeReportSectionOffice(object param)
        {
            try
            {
                using (CommitteeReportContext context = new CommitteeReportContext())
                {
                    string type=Convert.ToString(param);
                    var result = (from attachments in context.attachment where attachments.Type == type select attachments).ToList();
                    return result;
                }

            }
            catch (Exception ex)
            {
                string exs = ex.ToString();
            }
            return null;
        }


        public static object eFileCommitteeReport(object param)
        {
            try
            {
                using (CommitteeReportContext context = new CommitteeReportContext())
                {
                    int AttachId=Convert.ToInt32(param) ;
                    var result =                                                             
                                 (from assembly in context.mAssemblies join 
                                  sessions in context.mSessions on  assembly.AssemblyID equals sessions.AssemblyID
                                  join committees in context.objtcommittee on sessions.SessionID equals committees.SessionId
                                  join committeeType in context.mCommitteeType on committees.CommitteeTypeId equals committeeType.CommitteeTypeId
                                  join eFiles in context.eFile on committees.CommitteeId equals eFiles.CommitteeId
                                  join attachments in context.attachment on eFiles.eFileID equals attachments.eFileID
                                  join departments in context.department on attachments.DepartmentId equals departments.deptId

                                  where attachments.eFileAttachmentId == AttachId
                                  select new CommitteeReportDetails
                                  {
                                      Assembly = assembly.AssemblyName,
                                      Session = sessions.SessionName,
                                      CommitteeType = committeeType.CommitteeTypeName,
                                      Committee = committees.CommitteeName,
                                      Department = departments.deptname,
                                      eFileNumber = eFiles.eFileNumber,
                                      ReportType = "Original Report",
                                      ReportTitle = attachments.Title,
                                      ReportDescription = attachments.Description,
                                      AttachedPaper = attachments.eFileName,

                                      eFileId = attachments.eFileID,
                                      documentTypeId = attachments.DocumentTypeId,
                                      DepartmentId = attachments.DepartmentId,
                                      Type = attachments.Type,
                                      Approve = attachments.Approve,
                                     eFileAttachmentId=attachments.eFileAttachmentId
                                  }).FirstOrDefault();
                    
                    return result;
                }

            }
            catch (Exception ex)
            {
                string exs = ex.ToString();
            }
            return null;
        }

        public static object CreateCommitteeReport(object param)
        {
            try
            {
                using (CommitteeReportContext db = new CommitteeReportContext())
                {
                    tCommitteeReportLib model = param as tCommitteeReportLib;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tCommitteeReportLib.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateCommitteeReport(object param)
        {
            try
            {
                using (CommitteeReportContext db = new CommitteeReportContext())
                {
                    tCommitteeReportLib model = param as tCommitteeReportLib;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tCommitteeReportLib.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteCommitteeReport(object param)
        {
            try
            {
                using (CommitteeReportContext db = new CommitteeReportContext())
                {
                    tCommitteeReportLib parameter = param as tCommitteeReportLib;
                    tCommitteeReportLib stateToRemove = db.tCommitteeReportLib.SingleOrDefault(a => a.CommitteeReportId == parameter.CommitteeReportId);
                    db.tCommitteeReportLib.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllCommitteeReportDetails(object param)
        {

            CommitteeReportContext db = new CommitteeReportContext();

            var data = (from PC in db.tCommitteeReportLib.OrderByDescending(a => a.CommitteeReportId)
                        select PC).ToList();


            return data;
        }




        public static object GetCommitteeReportById(object param)
        {
            tCommitteeReportLib parameter = param as tCommitteeReportLib;

            CommitteeReportContext db = new CommitteeReportContext();

            var query = db.tCommitteeReportLib.SingleOrDefault(a => a.CommitteeReportId == parameter.CommitteeReportId);

            return query;
        }

    }
}
