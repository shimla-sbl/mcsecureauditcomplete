﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.CommitteeReport
{
    [Table("tCommitteeReplyPendencyDetail")]
    [Serializable]
    public class tCommitteeReplyPendencyDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PendencyReplyId { get; set; }

        public int PendencyId { get; set; }//foriegn key

        public DateTime? ReplyDatetime { get; set; }

        [UIHint("tinymce_jquery_full_LOB"), AllowHtml]
        public string DepartmentReply { get; set; }

        [UIHint("tinymce_jquery_full_LOB"), AllowHtml]
        public string QuestionaireForCommittee { get; set; }//for committee

        public DateTime? Recomm_CommentDate { get; set; }

        [UIHint("tinymce_jquery_full_LOB"), AllowHtml]
        public string Recomm_CommentsOfCommittee { get; set; }

        public int? Status { get; set; }

        [NotMapped]
        public string StatusName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string createdBy { get; set; }

    }
}
