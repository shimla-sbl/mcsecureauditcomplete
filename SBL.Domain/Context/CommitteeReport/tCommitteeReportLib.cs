﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.CommitteeReport
{
    [Table("tCommitteeReportLib")]
    [Serializable]
    public class tCommitteeReportLib
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommitteeReportId { get; set; }

        //[Required]
        public string ReportSource { get; set; }

        //[Required]
        public string NameofCommittee { get; set; }

        //[Required]
        public string Year { get; set; }

        //[Required]
        public string ReportNo { get; set; }

        //[Required]
        public string LocationDetails { get; set; }

        public string Building { get; set; }

        public string Floor { get; set; }

        public string Almirah { get; set; }

        public string Reck { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

    }

    [Serializable]
    public class CommitteeReportDetails
    {
        public string Assembly { get; set; }
        public string Session { get; set; }
        public string CommitteeType { get; set; }
        public string Committee { get; set; }
        public string Department { get; set; }
        public string eFileNumber { get; set; }
        public string ReportType { get; set; }
        public string ReportTitle { get; set; }
        public string ReportDescription { get; set; }
        public string AttachedPaper { get; set; }
        public string ReplyTitle { get; set; }
        public string ReplyDescription { get; set; }
        public string AttachPaper { get; set; }

        public int eFileId { get; set; }
        public int documentTypeId { get; set; }
        public string DepartmentId { get; set; }
        public string Type { get; set; }
        public int Approve { get; set; }
        public int eFileAttachmentId { get; set; }


    }
}


