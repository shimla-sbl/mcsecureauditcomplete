﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.SmsAndEmail
{
    public class SmsAndEmailContext : DBBase<SmsAndEmailContext>
    {
        public SmsAndEmailContext()
            : base("eVidhan")
        {
        }
        public virtual DbSet<mUserModules> mUserModules { get; set; }
        public virtual DbSet<mUserType> mUserType { get; set; }
        public virtual DbSet<mSubUserType> mSubUserType { get; set; }
        public virtual DbSet<tUserAccessActions> tUserAccessActions { get; set; }
        public DbSet<mUsers> mUsers { get; set; }



        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "getAllModuleList": { return getAllModuleList(); }
                case "getModuleUserList": { return getModuleUserList(param.Parameter); }

            }

            return null;
        }
        #region module list
        public static object getAllModuleList()
        {
            using (SmsAndEmailContext context = new SmsAndEmailContext())
            {
                var result = (from list in context.mUserModules
                              select new ModuleList
                              {
                                  moduleID = list.ModuleId,
                                  ModuleName = list.ModuleName
                              }).ToList();
                return result;
            }
            return null;
        }
        public static object getModuleUserList(object param)
        {
            int moduleID = Convert.ToInt32(param.ToString());
            using (SmsAndEmailContext context = new SmsAndEmailContext())
            {
                var result = (from list in context.tUserAccessActions
                              join muser in context.mUsers
                              on list.SubUserTypeID equals muser.UserType
                              where list.ModuleId == moduleID && muser.AadarId != null
                              select new ModuleUserList
                              {
                                  ModuleUseID = muser.AadarId,
                                  ModuleUseName = muser.UserName
                              }).ToList();
                return result;
            }
            return null;
        }
        #endregion
    }
}
