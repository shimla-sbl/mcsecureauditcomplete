﻿using SBL.DAL;
using SBL.DomainModel.Models.PaperLaid;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Event;

namespace SBL.Domain.Context.PaperCategoryType
{
    public class PaperCategoryTypeContext : DBBase<PaperCategoryTypeContext>
    {

        public PaperCategoryTypeContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mPaperCategoryType> mPaperCategoryType { get; set; }
        public DbSet<mEvent> mEvents { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreatePaper": { CreatePaper(param.Parameter); break; }

                case "UpdatePaper": { return UpdatePaper(param.Parameter); }
                case "DeletePaper": { return DeletePaper(param.Parameter); }
                case "GetAllPaper": { return GetAllPaper(); }
                case "GetPaperById": { return GetPaperById(param.Parameter); }
                case "IsPCTIdChildExist": { return IsPCTIdChildExist(param.Parameter); }
            }
            return null;
        }

        private static object IsPCTIdChildExist(object param)
        {
            try
            {
                using (PaperCategoryTypeContext db = new PaperCategoryTypeContext ())
                {
                    mPaperCategoryType model = (mPaperCategoryType )param;
                    var Res = (from e in db.mEvents
                               where (e.PaperCategoryTypeId == model.PaperCategoryTypeId)
                               select e).Count();

                    if (Res == 0)
                    {

                        return false;


                    }

                    else
                    {
                        return true;

                    }

                }
                //return null;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }
        static void CreatePaper(object param)
        {
            try
            {
                using (PaperCategoryTypeContext db = new PaperCategoryTypeContext())
                {
                    mPaperCategoryType model = param as mPaperCategoryType;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mPaperCategoryType.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdatePaper(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (PaperCategoryTypeContext db = new PaperCategoryTypeContext())
            {
                mPaperCategoryType model = param as mPaperCategoryType;
                model.CreationDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mPaperCategoryType.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllPaper();
        }

        static object DeletePaper(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (PaperCategoryTypeContext db = new PaperCategoryTypeContext())
            {
                mPaperCategoryType parameter = param as mPaperCategoryType;
                mPaperCategoryType stateToRemove = db.mPaperCategoryType.SingleOrDefault(a => a.PaperCategoryTypeId == parameter.PaperCategoryTypeId);
                if (stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mPaperCategoryType.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllPaper();
        }

        static List<mPaperCategoryType> GetAllPaper()
        {
            PaperCategoryTypeContext db = new PaperCategoryTypeContext();

            //var query = db.mPaperCategoryType.ToList();
            var query = (from a in db.mPaperCategoryType
                         orderby a.PaperCategoryTypeId descending
                         where a.IsDeleted==null
                         select a).ToList();

            return query;
        }

        static mPaperCategoryType GetPaperById(object param)
        {
            mPaperCategoryType parameter = param as mPaperCategoryType;
            PaperCategoryTypeContext db = new PaperCategoryTypeContext();
            var query = db.mPaperCategoryType.SingleOrDefault(a => a.PaperCategoryTypeId == parameter.PaperCategoryTypeId);
            return query;
        }



    }
}
