﻿using SBL.DAL;
using SBL.DomainModel.Models.Category;
using SBL.DomainModel.Models.Content;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.AContent
{
    class AContentContext : DBBase<AContentContext>
    {
        public AContentContext()
            : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<Content> tContent { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "AddContent": { return AddContent(param.Parameter); }
                    case "GetContents": { return GetContents(); }
                    case "EditContent": { return EditContent(param.Parameter); }
                    case "UpdateContent": { return UpdateContent(param.Parameter); }
                    case "DeleteContent": { DeleteContent(param.Parameter); break; }
                    case "GetContentById": { return GetContentById(param.Parameter); }
                    case "GetAllCategoryType": { return GetAllCategoryType(param.Parameter); }


                    case "AddContentCategory": { return AddContentCategory(param.Parameter); }
                    case "GetContentCategory": { return GetContentCategory(); }
                    case "EditContentCategory": { return EditContentCategory(param.Parameter); }
                    case "UpdateContentCategory": { return UpdateContentCategory(param.Parameter); }
                    case "DeleteContentCategory": { DeleteContentCategory(param.Parameter); break; }
                    case "GetContentCategoryById": { return GetContentCategoryById(param.Parameter); }
                    case "IsContentCategoryChildExist": { return IsContentCategoryChildExist(param.Parameter); }


                }
            }
            return null;
        }
        static object GetAllCategoryType(object param)
        {
            try
            {
                using (AContentContext db = new AContentContext())
                {
                    //Category cc= param as Category;
                    //  Organization Organization = param as Organization;
                    var data = (from value in db.Category where value.CategoryType == 5 && value.Status == 1 select value).ToList();
                    //var data = db.Category.Where(

                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetContents()
        {
            try
            {
                using (AContentContext db = new AContentContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from a in db.tContent join category in db.Category on a.CategoryID equals category.CategoryID select new {content = a ,
                         CategoryTypeName = category.CategoryName
                         }).ToList();//db.tContent.ToList();

                    List<Content> result = new List<Content>();

                    foreach (var a in data)
                    {
                        Content partdata = new Content();
                        partdata = a.content;
                        partdata.CategoryTypeName = a.CategoryTypeName;
                       
                        
                        result.Add(partdata);
                    }

                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object AddContent(object param)
        {
            try
            {
                using (AContentContext newsContext = new AContentContext())
                {
                    Content news = param as Content;
                    news.CreatedDate = DateTime.Now;
                    news.StartPublish = DateTime.Now;
                    news.ModifiedDate = DateTime.Now;
                    news.FinishPublish = DateTime.Now;
                    newsContext.tContent.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object GetContentById(object param)
        {
            try
            {
                using (AContentContext db = new AContentContext())
                {
                    Content NewsToEdit = param as Content;

                    var data = db.tContent.SingleOrDefault(o => o.ContentID == NewsToEdit.ContentID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static object EditContent(object param)
        {
            try
            {
                using (AContentContext newsContext = new AContentContext())
                {
                    var content = param as Content;
                    Content notice = (from value in newsContext.tContent where value.ContentID == content.ContentID select value).FirstOrDefault();
                    return notice;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object UpdateContent(object param)
        {

            try
            {
                Content Updatecontent = param as Content;
                using (AContentContext db = new AContentContext())
                {
                    var data = (from a in db.tContent where Updatecontent.ContentID == a.ContentID select a).FirstOrDefault();


                    data.ModifiedDate = DateTime.Now;
                    data.ContentDescription = Updatecontent.ContentDescription;
                    data.ContentLocalTitle = Updatecontent.ContentLocalTitle;
                    data.ContentLocalDescription = Updatecontent.ContentLocalDescription;
                    data.ContentTitle = Updatecontent.ContentTitle;
                    data.CategoryID = Updatecontent.CategoryID;
                    data.Status = Updatecontent.Status;
                    //data.Images = Updatecontent.Images;
                    //data.FileName = Updatecontent.FileName;
                    db.tContent.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return Updatecontent;
            }
            catch
            {
                throw;
            }
        }

        static void DeleteContent(object param)
        {
            try
            {
                Content DataTodelete = param as Content;
                int id = DataTodelete.ContentID;
                using (AContentContext db = new AContentContext())
                {
                    var NewsToDelete = db.tContent.SingleOrDefault(a => a.ContentID == id);
                    db.tContent.Remove(NewsToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }






        static object GetContentCategory()
        {
            try
            {
                using (AContentContext db = new AContentContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from value in db.Category where value.CategoryType == 5 && value.Status == 1 select value).ToList();
                    // var data = db.Category.ToList();
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static object AddContentCategory(object param)
        {
            try
            {
                using (AContentContext newsContext = new AContentContext())
                {
                    Category news = param as Category;
                    news.CategoryType = 5;
                    news.CreatedDate = DateTime.Now;
                    news.ModifiedDate = DateTime.Now;
                    news.StartPublish = DateTime.Now;
                    news.StopPublish = DateTime.Now;
                    newsContext.Category.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        private static object IsContentCategoryChildExist(object p)
        {
            try
            {
                using (AContentContext ctx = new AContentContext())
                {
                    Category data = (Category)p;

                    var isExist = (from a in ctx.tContent where a.CategoryID == data.CategoryID select a).Count() > 0;

                    return isExist;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        static object EditContentCategory(object param)
        {
            try
            {
                using (AContentContext newsContext = new AContentContext())
                {
                    var category = param as Category;
                    Category notice = (from value in newsContext.Category where value.CategoryID == category.CategoryID select value).FirstOrDefault();
                    return notice;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            //  return null;
        }

        static object UpdateContentCategory(object param)
        {

            try
            {
                Category UpdateNews = param as Category;
                using (AContentContext db = new AContentContext())
                {

                    var data = (from a in db.Category where UpdateNews.CategoryID == a.CategoryID select a).FirstOrDefault();
                    // UpdateNews.CategoryType = "News";
                    //// UpdateNews.CreatedDate = DateTime.Now;
                    // UpdateNews.ModifiedDate = DateTime.Now;
                    // data.StartPublish = UpdateNews.StartPublish;
                    data.CategoryDescription = UpdateNews.CategoryDescription;
                    data.LocalCategoryDescription = UpdateNews.LocalCategoryDescription;
                    data.LocalCategoryName = UpdateNews.LocalCategoryName;
                    data.CategoryName = UpdateNews.CategoryName;
                    data.Status = UpdateNews.Status;
                    data.CategoryID = UpdateNews.CategoryID;
                    db.Category.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateNews;
            }
            catch
            {
                throw;
            }
        }

        static void DeleteContentCategory(object param)
        {
            try
            {
                Category DataTodelete = param as Category;
                int id = DataTodelete.CategoryID;
                using (AContentContext db = new AContentContext())
                {
                    var NewsToDelete = db.Category.SingleOrDefault(a => a.CategoryID == id);
                    db.Category.Remove(NewsToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetContentCategoryById(object param)
        {
            try
            {
                using (AContentContext db = new AContentContext())
                {
                    Category NewsToEdit = param as Category;

                    var data = db.Category.SingleOrDefault(o => o.CategoryID == NewsToEdit.CategoryID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }



    }
}
