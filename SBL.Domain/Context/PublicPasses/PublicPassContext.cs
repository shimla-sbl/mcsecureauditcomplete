﻿namespace SBL.Domain.Context.PublicPasses
{
    #region Namespace reffrences.
    using SBL.DAL;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Officers;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Ministery;
    using SBL.Domain.Context.SiteSetting;
    using SBL.Domain.Context.Assembly;
    using SBL.Domain.Context.Session;

    using SBL.Service.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;

    #endregion

    public class PublicPassContext : DBBase<PublicPassContext>
    {
        #region Constructor Block
        public PublicPassContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        #endregion

        #region Data Table Definition Block
        public virtual DbSet<PublicPass> PublicPasses { get; set; }

        public DbSet<mMember> memberTable { get; set; }
        public DbSet<mMemberAssembly> memberAssemblyTable { get; set; }
        public DbSet<mConstituency> constituencyTable { get; set; }
        public DbSet<mSessionDate> mSessionDates { get; set; }
        public DbSet<mOfficerDetails> mOfficerDetails { get; set; }
        public DbSet<PassCategory> PassCategory { get; set; }
        public DbSet<mMinsitryMinister> mMinsitryMinisters { get; set; }

        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mSession> mSession { get; set; }
        #endregion

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "GetRecomendedByList": { return GetRecomendedByList(param.Parameter); }
                case "GetRecomendedByMinList": { return GetRecomendedByMinList(param.Parameter); }
                case "GetRecomendedByOfficerList": { return GetRecomendedByOfficerList(param.Parameter); }
                case "GetPassCategoriesForReception": { return GetPassCategoriesForReception(param.Parameter); }
                case "GetPrefixTitleList": { return GetPrefixTitleList(param.Parameter); }
                case "CreateNewPublicPass": { return CreateNewPublicPass(param.Parameter); }
                case "GetPassesByStatus": { return GetPassesByStatus(param.Parameter); }
                case "UpdatePublicPass": { return UpdatePublicPass(param.Parameter); }
                case "GetPublicPassById": { return GetPublicPassById(param.Parameter); }
                case "GetSessionDatesList": { return GetSessionDatesList(param.Parameter); }
                case "GetSessionDateByID": { return GetSessionDateByID(param.Parameter); }
                case "GetMemberNameByCode": { return GetMemberNameByCode(param.Parameter); }
                case "MenuItemCountByType": { return MenuItemCountByType(param.Parameter); }
                case "GetAdminAssemblyList": { return GetAdminAssemblyList(param.Parameter); }
                case "GetSessionDatesFromList": { return GetSessionDatesFromList(param.Parameter); }
                case "GetSessionDatesEdit": { return GetSessionDatesEdit(param.Parameter); }
            }
            return null;
        }

        #region Method Definition & Body.

        static object GetRecomendedByList(object param)
        {
            PublicPass model = param as PublicPass;
            PublicPassContext publicPassContext = new PublicPassContext();
            var memberList = (from member in publicPassContext.memberTable
                              join memberAssembly in publicPassContext.memberAssemblyTable on member.MemberCode equals memberAssembly.MemberID
                              join constituency in publicPassContext.constituencyTable on memberAssembly.ConstituencyCode equals constituency.ConstituencyCode
                              where (constituency.AssemblyID == model.AssemblyCode)
                              && (memberAssembly.AssemblyID == model.AssemblyCode)
                              orderby (member.Name)
                              select member).ToList();

            List<mMember> newMemList = new List<mMember>();
            foreach (var item in memberList)
            {
                //item.Name = item.Prefix + " " + item.Name;
                item.Name = item.Name;
                newMemList.Add(item);
            }

            return newMemList;
        }


        static object GetRecomendedByMinList(object param)
        {
            PublicPass model = param as PublicPass;
            PublicPassContext publicPassContext = new PublicPassContext();
            var ministerList = (from minister in publicPassContext.mMinsitryMinisters

                                where (minister.AssemblyID == model.AssemblyCode && minister.IsDeleted==null)
                                orderby (minister.OrderID)
                                select minister).ToList();

            List<mMinsitryMinister> newMinList = new List<mMinsitryMinister>();
            foreach (var item in ministerList)
            {
                //item.MinisterName = item.MinisterName.Substring(item.MinisterName.IndexOf(' ') + 1);
               item.MinisterName = item.MinisterName;
                newMinList.Add(item);
            }

            return newMinList;
        }



        static object GetAdminAssemblyList(object param)
        {
            PublicPass model = param as PublicPass;

            PublicPassContext pCtxt = new PublicPassContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();
            if (model.AssemblyCode == 0 && model.SessionCode == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionCode = Convert.ToInt16(SessionCode);
                model.AssemblyCode = Convert.ToInt16(AssemblyCode);
            }
            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionCode && m.AssemblyID == model.AssemblyCode select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyCode select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;
            model.mAssemblyList = (from A in pCtxt.mAssembly
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSession
                                 where S.AssemblyID == model.AssemblyCode
                                 select S).ToList();
            return model;
        }
        static object GetRecomendedByOfficerList(object param)
        {
            PublicPass model = param as PublicPass;
            PublicPassContext publicPassContext = new PublicPassContext();
            var officersList = (from officers in publicPassContext.mOfficerDetails
                                where officers.IsActive == true
                                select officers).ToList();

            List<mOfficerDetails> newOfficerList = new List<mOfficerDetails>();
            foreach (var item in officersList)
            {
                item.Name = item.Prefix + " " + item.Name;
                newOfficerList.Add(item);
            }

            return officersList;
        }

        static object GetSessionDatesList(object param)
        {
            PublicPass model = param as PublicPass;
            PublicPassContext publicPassContext = new PublicPassContext();
            var sessionDateList = (from sessiondates in publicPassContext.mSessionDates
                                   where sessiondates.SessionId == model.SessionCode && sessiondates.AssemblyId == model.AssemblyCode && (sessiondates.IsDeleted == null || sessiondates.IsDeleted == false)
                                   orderby (sessiondates.SessionDate)
                                   select sessiondates).ToList();
            foreach (var item in sessionDateList)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");
            }
            return sessionDateList;
        }
        static object GetSessionDatesFromList(object param)
        {
            PublicPass model = param as PublicPass;
            PublicPassContext publicPassContext = new PublicPassContext();
            var sessionDateList = (from sessiondates in publicPassContext.mSessionDates
                                   where sessiondates.SessionId == model.SessionCode && sessiondates.AssemblyId == model.AssemblyCode && (sessiondates.IsDeleted == null || sessiondates.IsDeleted == false)
                                   orderby (sessiondates.SessionDate)
                                   select sessiondates).ToList();
            foreach (var item in sessionDateList)
            {
                item.DisplayDate = item.SessionDate.ToString("MM/dd/yyyy");
            }
            return sessionDateList;
        }
        static object GetSessionDatesEdit(object param)
        {
            PublicPass model = param as PublicPass;
            PublicPassContext publicPassContext = new PublicPassContext();
            var sessionDateList = (from sessiondates in publicPassContext.mSessionDates
                                   where sessiondates.SessionId == model.SessionCode && sessiondates.AssemblyId == model.AssemblyCode && (sessiondates.IsDeleted == null || sessiondates.IsDeleted == false)
                                   orderby (sessiondates.SessionDate)
                                   select sessiondates).ToList();
            foreach (var item in sessionDateList)
            {
                item.SessionDateTo = item.SessionDate.ToString("dd/MM/yyyy");
            }
            return sessionDateList;
        }
        static object GetPassCategoriesForReception(object param)
        {
            try
            {
                PublicPassContext db = new PublicPassContext();
                var result = (from Passcategory in db.PassCategory orderby Passcategory.Name select Passcategory).ToList();
                result = result.Where(m => m.PassAllowsCreation == ((int)PassAllowsCreation.Reception).ToString()).ToList();
                return result;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetPrefixTitleList(object param)
        {
            return null;
        }

        static object CreateNewPublicPass(object param)
        {
            PublicPass PassToCreate = param as PublicPass;
            try
            {
                using (PublicPassContext _context = new PublicPassContext())
                {                    
                    _context.PublicPasses.Add(PassToCreate);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return PassToCreate;
        }

        static object UpdatePublicPass(object param)
        {
            PublicPass PassToUpdate = param as PublicPass;
            try
            {
                using (PublicPassContext _context = new PublicPassContext())
                {
                    _context.PublicPasses.Attach(PassToUpdate);
                    _context.Entry(PassToUpdate).State = EntityState.Modified;
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return PassToUpdate;
        }

        static object GetPassesByStatus(object param)
        {
            PublicPass model = param as PublicPass;
            PublicPassContext publicPassContext = new PublicPassContext();
            var publicPassList = (from publicPasses in publicPassContext.PublicPasses
                                  where publicPasses.Status == model.Status && publicPasses.AssemblyCode == model.AssemblyCode
                                  && publicPasses.SessionCode == model.SessionCode
                                  orderby (publicPasses.PassID) descending
                                  select publicPasses).ToList();

            return publicPassList;
        }

        static object GetPublicPassById(object param)
        {
            PublicPass publicPass = param as PublicPass;
            try
            {
                using (PublicPassContext _context = new PublicPassContext())
                {
                    return _context.PublicPasses.SingleOrDefault(a => a.PassID == publicPass.PassID);
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetSessionDateByID(object param)
        {
            //PublicPass model = param as PublicPass;
            //PublicPassContext publicPassContext = new PublicPassContext();

            //var sessionDate = (from sessionDates in publicPassContext.mSessionDates
            //                   where sessionDates.Id == model.SessionDateID
            //                   select sessionDates.SessionDate).FirstOrDefault();

            //return sessionDate;
            return null;
        }

        static object GetMemberNameByCode(object param)
        {
            PublicPass model = param as PublicPass;
            PublicPassContext publicPassContext = new PublicPassContext();
            string memberName = string.Empty;

            int RecommendationByID = 0;

            if (!string.IsNullOrEmpty(model.RecommendationBy) && int.TryParse(model.RecommendationBy, out RecommendationByID))
            {
                memberName = (from member in publicPassContext.memberTable
                              where member.MemberCode == RecommendationByID
                              select member.Name).FirstOrDefault();
            }
            return memberName;
        }

        static object MenuItemCountByType(object param)
        {
            PublicPass model = param as PublicPass;
            var passesListByStatus = GetPassesByStatus(model) as List<PublicPass>;
            return passesListByStatus.ToList();
        }
        #endregion
    }
}
