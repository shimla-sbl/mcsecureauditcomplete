﻿namespace SBL.Domain.Context.Notice
{
    #region Namespace Reffrences
    using SBL.DAL;
    using SBL.Domain.Context.Assembly;
    using SBL.Domain.Context.ATour;
    using SBL.Domain.Context.Department;
    using SBL.Domain.Context.Diaries;
    using SBL.Domain.Context.Event;
    using SBL.Domain.Context.LegislationFixation;
    using SBL.Domain.Context.Member;
    using SBL.Domain.Context.Ministry;
    using SBL.Domain.Context.Questions;
    using SBL.Domain.Context.Session;
    using SBL.Domain.Context.SiteSetting;
    using SBL.Domain.Context.User;
    using SBL.Domain.Context.UserManagement;
    using SBL.DomainModel.ComplexModel;
    using SBL.DomainModel.Models.ActionButtons;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Diaries;
    using SBL.DomainModel.Models.Document;
    using SBL.DomainModel.Models.Employee;
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Event;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Notice;
    using SBL.DomainModel.Models.PaperLaid;
    using SBL.DomainModel.Models.Question;
    using SBL.DomainModel.Models.Secretory;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.UserModule;
    using SBL.Service.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using SBL.DomainModel.Models.AlbumCategory;
    using SBL.DomainModel.Models.Category;
    using SBL.DomainModel.Models.Gallery;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.District;
    using SBL.DomainModel.Models.Districtconsistency;
    using SBL.DomainModel.Models.Tour;
    using SBL.DomainModel.Models.Departmentpdfpath;
    using SBL.DomainModel.Models.SessionDateSignature;
    using SBL.DomainModel.Models.RecipientGroups;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.DomainModel.Models.OTP;

    using SBL.DomainModel.Models.CutMotionDemand;
    using SBL.DomainModel.Models.DeptRegisterModel;
    using SBL.DomainModel.Models.Adhaar;
    using SBL.DomainModel.Models.AssemblyFileSystem;
    using System.Data.Entity.Core.Objects;
    using SBL.DomainModel.Models.Office;
    using SBL.DomainModel.Models.Bill;
    using SBL.DomainModel.Models.LOB;
    using System.Data.SqlClient;

    //using SBL.DomainModel.Models.Departmentpdfpath;



    #endregion




    public class NoticeContext : DBBase<NoticeContext>
    {
        public NoticeContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public virtual DbSet<tMemberNotice> tMemberNotices { get; set; }
        public virtual DbSet<mNoticeType> mNoticeTypes { get; set; }
        public virtual DbSet<mAssembly> mAssemblies { get; set; }
        public virtual DbSet<mSession> mSessions { get; set; }
        public virtual DbSet<mMember> mMembers { get; set; }
        public virtual DbSet<mEvent> mEvents { get; set; }
        public virtual DbSet<tPaperLaidV> tPaperLaidVs { get; set; }
        public virtual DbSet<tPaperLaidTemp> tPaperLaidTemps { get; set; }
        public virtual DbSet<mMinistry> mMinistry { get; set; }
        public virtual DbSet<mDepartment> mDepartments { get; set; }
        public virtual DbSet<mPaperCategoryType> mPaperCategoryTypes { get; set; }
        public DbSet<mDocumentType> mDocumentTypes { get; set; }
        public DbSet<mSessionDate> mSessionDates { get; set; }
        public DbSet<tQuestion> tQuestions { get; set; }
        public DbSet<ActionButtonControl> ActionButtonControl { get; set; }
        public DbSet<TQuestionAuditTrial> TQuestionAuditTrial { get; set; }
        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<mEmployee> mEmployee { get; set; }
        public DbSet<SBL.DomainModel.Models.Constituency.mConstituency> mConstituency { get; set; }
        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public virtual DbSet<SBL.DomainModel.Models.ROM.tRotationMinister> tRotationMinister { get; set; }
        public DbSet<mSecretoryDepartment> SecDept { get; set; }
        public DbSet<OnlineQuestions> OnlineQuestions { get; set; }
        public DbSet<AdminLOB> AdminLOB { get; set; }
        public DbSet<OnlineNotices> OLNotices { get; set; }
        public DbSet<AlbumCategory> AlbumCategory { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<tGallery> tGallery { get; set; }
        public DbSet<DistrictModel> Districts { get; set; }
        public virtual DbSet<tPanchayatVillage> tPanchayatVillage { get; set; }
        public virtual DbSet<Districtconsistency> tDistrictconsistency { get; set; }
        public virtual DbSet<mPanchayat> mPanchayat { get; set; }
        public virtual DbSet<tConstituencyPanchayat> tConstituencyPanchayat { get; set; }
        public DbSet<tMemberTour> tMemberTour { get; set; }
        public DbSet<mUserSubModules> UserSubModules { get; set; }
        public virtual DbSet<mDepartmentPdfPath> mDepartmentPdfPath { get; set; }
        // public DbSet<OnlineQuestions> OnlineQuestions { get; set; }
        public virtual DbSet<tSessionDateSignature> tSessionDateSignature { get; set; }
        public DbSet<RecipientGroup> mRecipientGroup { get; set; }
        public DbSet<RecipientGroupMember> mRecipientGroupMember { get; set; }
        public DbSet<SiteSettings> SiteSettings { get; set; }
        public virtual DbSet<tOTPDetails> tOTPDetails { get; set; }
        public DbSet<tCutMotionDemand> tCutMotionDemand { get; set; }
        public DbSet<mchangeDepartmentAuditTrail> mchangeDepartmentAuditTrail { get; set; }
        public virtual DbSet<mSecretory> mSecretory { get; set; }
        public virtual DbSet<AdhaarDetails> AdhaarDetails { get; set; }
        public virtual DbSet<tAssemblyFiles> tAssemblyFiles { get; set; }
        public virtual DbSet<mNotice> tNoticeBoard { get; set; }
        public virtual DbSet<mOffice> mOffice { get; set; }
        public virtual DbSet<tBillRegister> tBillRegister { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "GetAllNoticeTypes":
                    return GetAllNoticeTypes(param.Parameter);
                case "CreateNoticeType":
                    return CreateNoticeType(param.Parameter);
                case "UpdateNoticeType":
                    return UpdateNoticeType(param.Parameter);
                case "EditNoticeType":
                    return EditNoticeType(param.Parameter);
                case "DeleteNoticeType":
                    return DeleteNoticeType(param.Parameter);
                case "CreateNotice":
                    return CreateNotice(param.Parameter);
                case "GetNoticesByStatus":
                    return GetNoticesByStatus(param.Parameter);
                case "PublishNotice":
                    return PublishNotice(param.Parameter);
                case "DeleteNotice":
                    return DeleteNotice(param.Parameter);
                case "GetNoticeByNoticeID":
                    return GetNoticeByNoticeID(param.Parameter);
                case "PublishNoticeForward":
                    return PublishNoticeForward(param.Parameter);
                case "GetNoticesByStatusID":
                    return GetNoticesByStatusID(param.Parameter);

                case "GetNoticeInBoxList":
                    return GetNoticeInBoxList(param.Parameter);

                case "UpdateNoticeById":

                    return UpdateNoticeById(param.Parameter);

                case "GetNoticeReplyDraftList":

                    return GetNoticeReplyDraftList(param.Parameter);

                case "GetNoticeDraftReplyById":

                    return GetNoticeDraftReplyById(param.Parameter);

                case "GetNoticeReplySentList":

                    return GetNoticeReplySentList(param.Parameter);

                case "GetNoticeReplySentRecordById":
                    return GetNoticeReplySentRecordById(param.Parameter);

                case "UpdateNoticePaperLaidFileByID":
                    {
                        return UpdateNoticePaperLaidFileByID(param.Parameter);
                    }

                case "GetNoticeSubmittedPaperLaidByID":
                    {
                        return GetNoticeSubmittedPaperLaidByID(param.Parameter);
                    }

                case "GetNoticeULOBList":
                    {
                        return GetNoticeULOBList(param.Parameter);
                    }

                case "GetNoticeLIHList":
                    {
                        return GetNoticeLIHList(param.Parameter);
                    }

                case "GetNoticePLIHList":
                    {
                        return GetNoticePLIHList(param.Parameter);
                    }

                case "UpdatePaperLaidEntryForNoticeReplace":
                    {
                        return UpdatePaperLaidEntryForNoticeReplace(param.Parameter);
                    }
                case "GetAllCurrentNoticesList":
                    {
                        return GetAllCurrentNoticesList(param.Parameter);
                    }
                case "UpdateDiaryNotices":
                    {
                        return UpdateDiaryNotices(param.Parameter);
                    }


                //Sameer

                case "GetCountForQuestionTypes":
                    {
                        return GetCountForQuestionTypes(param.Parameter);
                    }
                case "GetPendingQuestionsByType":
                    {
                        return GetPendingQuestionsByType(param.Parameter);
                    }
                case "GetSubmittedQuestionsByType":
                    {
                        return GetSubmittedQuestionsByType(param.Parameter);
                    }
                case "GetLaidInHouseQuestionsByType":
                    {
                        return GetLaidInHouseQuestionsByType(param.Parameter);
                    }
                case "GetPendingForSubQuestionsByType":
                    {
                        return GetPendingForSubQuestionsByType(param.Parameter);
                    }
                case "GetPendingToLayQuestionsByType":
                    {
                        return GetPendingToLayQuestionsByType(param.Parameter);
                    }
                case "GetCountForUnstartQuestionTypes":
                    {
                        return GetCountForUnstartQuestionTypes(param.Parameter);
                    }
                case "GetUnstartQuestionsByType":
                    {
                        return GetUnstartQuestionsByType(param.Parameter);
                    }
                case "GetCountForNotices":
                    {
                        return GetCountForNotices(param.Parameter);
                    }
                case "GetNotices":
                    {
                        return GetNotices(param.Parameter);
                    }
                case "GetQuestionDetails":
                    {
                        return GetQuestionDetails(param.Parameter);
                    }
                case "UpdateQuestions":
                    {
                        return UpdateQuestions(param.Parameter);
                    }
                case "GetAllNotice":
                    {
                        return GetAllNotice(param.Parameter);
                    }
                case "GetDetailsforViewNotices":
                    {
                        return GetDetailsforViewNotices(param.Parameter);
                    }
                case "GetNoticesValue":
                    {
                        return GetNoticesValue(param.Parameter);
                    }
                case "UpdateNotice":
                    {
                        return UpdateNotice(param.Parameter);
                    }
                case "GetNoticeAttachment":
                    {
                        return GetNoticeAttachment(param.Parameter);
                    }
                case "GetUserCode":
                    {
                        return GetUserCode(param.Parameter);
                    }
                case "AssignQuestions":
                    {
                        return AssignQuestions(param.Parameter);
                    }
                case "GetPendingQuestionsForAssign":
                    {
                        return GetPendingQuestionsForAssign(param.Parameter);
                    }
                case "GetQuestionbyTypistId":
                    {
                        return GetQuestionbyTypistId(param.Parameter);
                    }
                case "RoleBackQuestion":
                    {
                        return RoleBackQuestion(param.Parameter);
                    }
                case "GetCountForProofReader":
                    {
                        return GetCountForProofReader(param.Parameter);
                    }
                case "GetUnstartForAssingList":
                    {
                        return GetUnstartForAssingList(param.Parameter);
                    }
                case "GetUnStraredTypistView":
                    {
                        return GetUnStraredTypistView(param.Parameter);
                    }
                case "GetCountForLegisUnstartQuestion":
                    {
                        return GetCountForLegisUnstartQuestion(param.Parameter);
                    }
                case "GetUnstartLegisQuestionsByType":
                    {
                        return GetUnstartLegisQuestionsByType(param.Parameter);
                    }
                case "GetButtons":
                    {
                        return GetButtons(param.Parameter);
                    }
                //case "UpdateContentFreeze":
                //    {
                //        return UpdateContentFreeze(param.Parameter);
                //    }
                case "GetCountForClubbed":
                    {
                        return GetCountForClubbed(param.Parameter);
                    }
                case "GetCStaredQuestions":
                    {
                        return GetCStaredQuestions(param.Parameter);
                    }
                case "GetClubbedQuestions":
                    {
                        return GetClubbedQuestions(param.Parameter);
                    }
                case "GetInactiveQuestion":
                    {
                        return GetInactiveQuestion(param.Parameter);
                    }
                case "GetQForClubWithBracket":
                    {
                        return GetQForClubWithBracket(param.Parameter);
                    }
                case "GetQuestionDetailsForMerging":
                    {
                        return GetQuestionDetailsForMerging(param.Parameter);
                    }
                case "CheckDiaryNumber":
                    {
                        return CheckDiaryNumber(param.Parameter);
                    }
                case "SaveMergeEntry":
                    {
                        return SaveMergeEntry(param.Parameter);
                    }
                case "DeactiveMergeQuestion":
                    {
                        return DeactiveMergeQuestion(param.Parameter);
                    }
                case "GetAssignListQuestions":
                    {
                        return GetAssignListQuestions(param.Parameter);
                    }
                case "GetClubbedQuestionList":
                    {
                        return GetClubbedQuestionList(param.Parameter);
                    }
                case "GetInactiveQuestionList":
                    {
                        return GetInactiveQuestionList(param.Parameter);
                    }
                case "GetCurrentInboxDraftNoticesList":
                    {
                        return GetCurrentInboxDraftNoticesList(param.Parameter);
                    }
                case "GetQDetails":
                    {
                        return GetQDetails(param.Parameter);
                    }
                case "SaveFreezeTrialTable":
                    {
                        return SaveFreezeTrialTable(param.Parameter);
                    }
                case "GetUserforAssigned":
                    {
                        return GetUserforAssigned(param.Parameter);
                    }
                case "GetUserforUnstaredAssigned":
                    {
                        return GetUserforUnstaredAssigned(param.Parameter);
                    }
                case "GetInActiveQuestionDetails":
                    {
                        return GetInActiveQuestionDetails(param.Parameter);
                    }
                case "GetQDetailsByAuditTrial":
                    {
                        return GetQDetailsByAuditTrial(param.Parameter);
                    }
                case "StayUpdateQuestions":
                    {
                        return StayUpdateQuestions(param.Parameter);
                    }
                case "GetDetailsForClubbing":
                    {
                        return GetDetailsForClubbing(param.Parameter);
                    }
                case "CheckDQuestionNo":
                    {
                        return CheckDQuestionNo(param.Parameter);
                    }
                case "GetQDetailsByDiaryNo":
                    {
                        return GetQDetailsByDiaryNo(param.Parameter);
                    }
                case "UnstaredCheckDQuestionNo":
                    {
                        return UnstaredCheckDQuestionNo(param.Parameter);
                    }
                case "UnstaredUpdateQuestions":
                    {
                        return UnstaredUpdateQuestions(param.Parameter);
                    }
                case "UnstaredGetQDetailsByDiaryNo":
                    {
                        return UnstaredGetQDetailsByDiaryNo(param.Parameter);
                    }
                case "GetCountForUnStaredClubbed":
                    {
                        return GetCountForUnStaredClubbed(param.Parameter);
                    }
                case "UnstaredGetQForClubWithBracket":
                    {
                        return UnstaredGetQForClubWithBracket(param.Parameter);
                    }
                case "UnstaredGetBracketedQuestionList":
                    {
                        return UnstaredGetBracketedQuestionList(param.Parameter);
                    }
                case "GetDepartment":
                    {
                        return GetDepartment(param.Parameter);
                    }
                case "GetAllDetailsForDeptwise":
                    {
                        return GetAllDetailsForDeptwise(param.Parameter);
                    }
                case "UpdatemDepartment":
                    {
                        return UpdatemDepartment(param.Parameter);
                    }
                case "GetDepartmentByValue":
                    {
                        return GetDepartmentByValue(param.Parameter);
                    }
                case "GetDepartmentByValueUnstared":
                    {
                        return GetDepartmentByValueUnstared(param.Parameter);
                    }
                case "UpdatemDepartmentUnstarred":
                    {
                        return UpdatemDepartmentUnstarred(param.Parameter);
                    }
                case "GetTotalStarredClubbedQuestion":
                    {
                        return GetTotalStarredClubbedQuestion(param.Parameter);
                    }
                case "GetTotalUnStarredClubbedQuestion":
                    {
                        return GetTotalUnstarredClubbedQuestion(param.Parameter);
                    }
                case "UnstarredGetAllDetailsForDeptwise":
                    {
                        return UnstarredGetAllDetailsForDeptwise(param.Parameter);
                    }
                case "GetDepartmentForNotices":
                    {
                        return GetDepartmentForNotices(param.Parameter);
                    }
                case "GetAllDetailsForNotices":
                    {
                        return GetAllDetailsForNotices(param.Parameter);
                    }
                case "UpdatemDepartmentFornotices":
                    {
                        return UpdatemDepartmentFornotices(param.Parameter);
                    }
                case "GetNoticesForView":
                    {
                        return GetNoticesForView(param.Parameter);
                    }
                case "GetNoticesForGeneratedPdf":
                    {
                        return GetNoticesForGeneratedPdf(param.Parameter);
                    }
                case "GetDetailsForNoticesPDF":
                    {
                        return GetDetailsForNoticesPDF(param.Parameter);
                    }
                case "UpdateTmemberFornotices":
                    {
                        return UpdateTmemberFornotices(param.Parameter);
                    }
                case "GetCountForChanger":
                    {
                        return GetCountForChanger(param.Parameter);
                    }
                case "GetCountUnstartchanger":
                    {
                        return GetCountUnstartchanger(param.Parameter);
                    }
                case "GetStarredQuestions":
                    {
                        return GetStarredQuestions(param.Parameter);
                    }
                case "GetStrarredQDetailsChanger":
                    {
                        return GetStrarredQDetailsChanger(param.Parameter);
                    }
                case "SaveChangerTQuestions":
                    {
                        return SaveChangerTQuestions(param.Parameter);
                    }
                case "GetUnStarredQuestions":
                    {
                        return GetUnStarredQuestions(param.Parameter);
                    }
                case "UnstaredGetAllDetailsForSpdf":
                    {
                        return UnstaredGetAllDetailsForSpdf(param.Parameter);
                    }
                case "GetRotationalMiniterName":
                    {
                        return GetRotationalMiniterName(param.Parameter);
                    }
                case "UnstaredUpdatemSessionDate":
                    {
                        return UnstaredUpdatemSessionDate(param.Parameter);
                    }
                case "GetAllDetailsForSpdf":
                    {
                        return GetAllDetailsForSpdf(param.Parameter);
                    }
                case "UpdatemSessionDate":
                    {
                        return UpdatemSessionDate(param.Parameter);
                    }
                case "GetSessionDatesUnstart":
                    {
                        return GetSessionDatesUnstart(param.Parameter);
                    }
                case "GetSessionDates":
                    {
                        return GetSessionDates(param.Parameter);
                    }
                ///onlineQuestions
                case "GetCountOnlineStartQuestion":
                    {
                        return GetCountOnlineStartQuestion(param.Parameter);
                    }
                case "GetCountOnlineUntartQuestion":
                    {
                        return GetCountOnlineUntartQuestion(param.Parameter);
                    }
                case "GetOnlineStarredQuestions":
                    {
                        return GetOnlineStarredQuestions(param.Parameter);
                    }
                case "SaveUpdateQuestionsTable":
                    {
                        return SaveUpdateQuestionsTable(param.Parameter);
                    }
                case "GetQuestionByIdForEdit":
                    {
                        return GetQuestionByIdForEdit(param.Parameter);
                    }
                case "GetOnlineUnStarredQuestions":
                    {
                        return GetOnlineUnStarredQuestions(param.Parameter);
                    }
                case "SaveUpdateQuestionsUnstarredTable":
                    {
                        return SaveUpdateQuestionsUnstarredTable(param.Parameter);
                    }
                case "GetDetailsForOnlinePDF":
                    {
                        return GetDetailsForOnlinePDF(param.Parameter);
                    }
                case "GetDetailsForOnlineUnstarredPDF":
                    {
                        return GetDetailsForOnlineUnstarredPDF(param.Parameter);
                    }

                case "GetQuestionDetailById":
                    {
                        return GetQuestionDetailById(param.Parameter);
                    }
                case "UpdateFile":
                    {
                        return UpdateFile(param.Parameter);
                    }
                case "SubmitQuestionById":
                    {
                        return SubmitQuestionById(param.Parameter);
                    }
                case "GetAllOnlineSubmission":
                    {
                        return GetAllOnlineSubmission(param.Parameter);
                    }
                case "GetCountOnlineNotice":
                    {
                        return GetCountOnlineNotice(param.Parameter);
                    }
                case "GetAllOnlineNotices": { return GetAllOnlineNotices(param.Parameter); }
                case "GetAllCountForOnlineSubmission":
                    {
                        return GetAllCountForOnlineSubmission(param.Parameter);
                    }
                case "GetOnlineMemberDashboardItemsCounter":
                    { return GetOnlineMemberDashboardItemsCounter(param.Parameter); }

                case "NewEntryForm":
                    {
                        return NewEntryForm(param.Parameter);
                    }
                case "SaveUpdateNotice":
                    {
                        return SaveUpdateNotice(param.Parameter);
                    }
                case "GetNoticeDetailById":
                    {
                        return GetNoticeDetailById(param.Parameter);
                    }
                case "GetNoticeByIdForEdit":
                    {
                        return GetNoticeByIdForEdit(param.Parameter);
                    }
                case "UpdateNoticeFile":
                    {
                        return UpdateNoticeFile(param.Parameter);
                    }
                case "GetMemberMinisterList":
                    {
                        return GetMemberMinisterList();
                    }
                case "GetDynamicMainMenuByUserId":
                    {
                        return GetDynamicMainMenuByUserId(param.Parameter);
                    }
                case "GetSubMenuByMenuId":
                    {
                        return GetSubMenuByMenuId(param.Parameter);
                    }

                case "GetSubMenuByMenuIdHindi":
                    {
                        return GetSubMenuByMenuIdHindi(param.Parameter);
                    }

                case "GetTotalOnlineSubmissionCnt":
                    {
                        return GetTotalOnlineSubmissionCnt(param.Parameter);
                    }
                case "GetTotalTourCnt":
                    {
                        return GetTotalTourCnt(param.Parameter);
                    }
                case "GetTotalGalleryCnt":
                    {
                        return GetTotalGalleryCnt(param.Parameter);
                    }
                case "GetTotalOnlineStarredCnt":
                    {
                        return GetTotalOnlineStarredCnt(param.Parameter);
                    }
                //case "GetTotalPaperlaidDraftRepliesCnt":
                //    {
                //        return GetTotalPaperlaidDraftRepliesCnt(param.Parameter);
                //    }

                case "GetTotalUnstarredCnt":
                    {
                        return GetTotalUnstarredCnt(param.Parameter);
                    }
                case "GetTotalNoticesCnt":
                    {
                        return GetTotalNoticesCnt(param.Parameter);
                    }
                case "GetDepartmentById":
                    {
                        return GetDepartmentById(param.Parameter);
                    }
                case "GetDataByQuestionId":
                    {
                        return GetDataByQuestionId(param.Parameter);
                    }

                case "GetSearchDataByType":
                    {
                        return GetSearchDataByType(param.Parameter);
                    }
                case "GetTotalGalleryCount":
                    {
                        return GetTotalGalleryCount(param.Parameter);
                    }
                case "GetTotalAlbumGalleryCnt":
                    {
                        return GetTotalAlbumGalleryCnt(param.Parameter);
                    }
                case "GetTotalGalleriesCnt":
                    {
                        return GetTotalGalleriesCnt(param.Parameter);
                    }
                case "GetDistricts":
                    {
                        return GetDistricts(param.Parameter);
                    }
                case "AddPanchayat":
                    {
                        return AddPanchayat(param.Parameter);
                    }
                case "DeletePanchayat":
                    {
                        return DeletePanchayat(param.Parameter);
                    }

                case "GetAllPanchayat":
                    {
                        return GetAllPanchayat(param.Parameter);
                    }
                case "GetCName":
                    {
                        return GetCName(param.Parameter);
                    }
                case "GetTotalPanchayatCnt":
                    {
                        return GetTotalPanchayatCnt();
                    }
                case "GetTotalTodayTourCnt":
                    {
                        return GetTotalTodayTourCnt(param.Parameter);
                    }
                case "GetTotalupcomingTourCnt":
                    {
                        return GetTotalupcomingTourCnt(param.Parameter);
                    }
                case "GetTotalRecentTourCnt":
                    {
                        return GetTotalRecentTourCnt(param.Parameter);
                    }
                case "GetTotalUnpublishedTourCnt":
                    {
                        return GetTotalUnpublishedTourCnt(param.Parameter);
                    }
                case "GetRecipientGroupCnt":
                    {
                        return GetRecipientGroupCnt(param.Parameter);
                    }
                case "GetDName":
                    {
                        return GetDName(param.Parameter);
                    }
                case "SubmitNoticeById":
                    {
                        return SubmitNoticeById(param.Parameter);
                    }

                case "SaveUpdateSignature":
                    {
                        return SaveUpdateSignature(param.Parameter);
                    }
                case "GetSignPathByUserId":
                    {
                        return GetSignPathByUserId(param.Parameter);
                    }
                case "UpdatemDepartmentPdfPath":
                    {
                        return UpdatemDepartmentPdfPath(param.Parameter);
                    }
                case "GetValuemDepartmentPdf":
                    {
                        return GetValuemDepartmentPdf(param.Parameter);
                    }
                case "GetDiaryNoForPdfDiaryWiseStarred":
                    {
                        return GetDiaryNoForPdfDiaryWiseStarred(param.Parameter);
                    }
                case "GetAllDetailsByDiaryNo":
                    {
                        return GetAllDetailsByDiaryNo(param.Parameter);
                    }
                case "UnstarredGetDiaryNoForPdfDiaryWiseStarred":
                    {
                        return UnstarredGetDiaryNoForPdfDiaryWiseStarred(param.Parameter);
                    }
                case "UnstarredGetAllDetailsByDiaryNo":
                    {
                        return UnstarredGetAllDetailsByDiaryNo(param.Parameter);
                    }
                case "UpdatemDepartmentPdfUnstarred":
                    {
                        return UpdatemDepartmentPdfUnstarred(param.Parameter);
                    }
                case "GetSessionStamp":
                    {
                        return GetSessionStamp(param.Parameter);
                    }
                case "GetStarredAndUnstarredCnt":
                    {
                        return GetStarredAndUnstarredCnt(param.Parameter);
                    }
                case "GetStarredQuestionsForChanger":
                    {
                        return GetStarredQuestionsForChanger(param.Parameter);
                    }
                case "GetUnStarredQuestionsForChanger":
                    {
                        return GetUnStarredQuestionsForChanger(param.Parameter);
                    }
                case "EditPendingNoticeDetials":
                    {
                        return EditPendingNoticeDetials(param.Parameter);
                    }
                case "GetRecipientGroupMemberCnt":
                    {
                        return GetRecipientGroupMemberCnt(param.Parameter);
                    }
                case "BackToTypistById":
                    {
                        return BackToTypistById(param.Parameter);
                    }
                case "CheckOTP":
                    {
                        return CheckOTP(param.Parameter);
                    }
                case "ApproveGeneratedPDFByDate":
                    {
                        return ApproveGeneratedPDFByDate(param.Parameter);
                    }
                case "BackToTranByDate":
                    {
                        return BackToTranByDate(param.Parameter);
                    }
                case "GetDataByDate":
                    {
                        return GetDataByDate(param.Parameter);
                    }
                case "BackToPRByQuestionId":
                    {
                        return BackToPRByQuestionId(param.Parameter);
                    }
                case "GetDataByUnstarredDate":
                    {
                        return GetDataByUnstarredDate(param.Parameter);
                    }
                case "GetRotationalMiniterNameEnglish":
                    {
                        return GetRotationalMiniterNameEnglish(param.Parameter);
                    }
                case "Istyoistrequired":
                    {
                        return Istyoistrequired(param.Parameter);
                    }
                case "GetNoticesUserCode":
                    {
                        return GetNoticesUserCode(param.Parameter);
                    }
                case "GetNoticesUserforAssigned":
                    {
                        return GetNoticesUserforAssigned(param.Parameter);
                    }
                case "GetPendingNoticesForAssign":
                    {
                        return GetPendingNoticesForAssign(param.Parameter);
                    }
                case "GetNoticesAssignList":
                    {
                        return GetNoticesAssignList(param.Parameter);
                    }
                case "AssignNotices":
                    {
                        return AssignNotices(param.Parameter);
                    }
                case "RoleBackNotices":
                    {
                        return RoleBackNotices(param.Parameter);
                    }
                case "GetCountForNotrices":
                    {
                        return GetCountForNotrices(param.Parameter);
                    }
                case "GetNoticesforTypist":
                    {
                        return GetNoticesforTypist(param.Parameter);
                    }
                case "GetAttachmentNotices":
                    {
                        return GetAttachmentNotices(param.Parameter);
                    }
                case "GetNoticeTypistDetails":
                    {
                        return GetNoticeTypistDetails(param.Parameter);
                    }
                case "StayUpdateNotice":
                    {
                        return StayUpdateNotice(param.Parameter);
                    }
                case "UpdateNoticesTypist":
                    {
                        return UpdateNoticesTypist(param.Parameter);
                    }
                case "GetNoticesforCutMotipn":
                    {
                        return GetNoticesforCutMotipn(param.Parameter);
                    }
                case "GetDataForcutmotion":
                    {
                        return GetDataForcutmotion(param.Parameter);
                    }
                case "GetDataByIdcutmotion":
                    {
                        return GetDataByIdcutmotion(param.Parameter);
                    }
                case "GetCutMotionsForClubWithBracket":
                    {
                        return GetCutMotionsForClubWithBracket(param.Parameter);
                    }
                case "GetCutMotionDetailsForMerging":
                    {
                        return GetCutMotionDetailsForMerging(param.Parameter);
                    }
                case "SaveMergeCutmotionsEntry":
                    {
                        return SaveMergeCutmotionsEntry(param.Parameter);
                    }
                case "GetCutMotionsClubbed":
                    {
                        return GetCutMotionsClubbed(param.Parameter);
                    }
                case "GetclubbedCutMotions":
                    {
                        return GetclubbedCutMotions(param.Parameter);
                    }
                case "UpdateFilePathandName":
                    {
                        return UpdateFilePathandName(param.Parameter);
                    }
                case "GetAllDetailsForCutmotionPdfpdf":
                    {
                        return GetAllDetailsForCutmotionPdfpdf(param.Parameter);
                    }
                case "GetListtTransfered":
                    {
                        return GetListtTransfered(param.Parameter);
                    }
                case "GetDetailsNoPdf":
                    {
                        return GetDetailsNoPdf(param.Parameter);
                    }
                case "GetDataByQuestionIdForFixing":
                    {
                        return GetDataByQuestionIdForFixing(param.Parameter);
                    }
                case "LocktransUnstarred":
                    {
                        return LocktransUnstarred(param.Parameter);
                    }
                case "UnLocktransUnstarred":
                    {
                        return UnLocktransUnstarred(param.Parameter);
                    }
                case "GetDataForNoticeBrackt":
                    {
                        return GetDataForNoticeBrackt(param.Parameter);
                    }
                case "GetNoticesForClubWithBracket":
                    {
                        return GetNoticesForClubWithBracket(param.Parameter);
                    }
                case "GetNoticesClubbed":
                    {
                        return GetNoticesClubbed(param.Parameter);
                    }
                case "SaveMergeNoticeEntry":
                    {
                        return SaveMergeNoticeEntry(param.Parameter);
                    }
                case "GetMembers":
                    {
                        return GetMembers(param.Parameter);
                    }
                ///new added by dharmendra

                case "GetMemberNameByIds":
                    {
                        return GetMemberNameByIds(param.Parameter);
                    }
                case "UpdateDiaryQuestions":
                    {
                        return UpdateDiaryQuestions(param.Parameter);
                    }
                case "SentToProofReader":
                    {
                        return SentToProofReader(param.Parameter);
                    }
                case "NoticeBackToTypist":
                    {
                        return NoticeBackToTypist(param.Parameter);
                    }
                case "CheckQStatus":
                    {
                        return CheckQStatus(param.Parameter);
                    }
                case "GetDQuestionNo":
                    {
                        return GetDQuestionNo(param.Parameter);
                    }
                case "UpdatefixtoUnfix":
                    {
                        return UpdatefixtoUnfix(param.Parameter);
                    }

                case "AssignNodalOffice":
                    {
                        return AssignNodalOffice(param.Parameter);
                    }
                case "DeleteNodalOffice":
                    {
                        return DeleteNodalOffice(param.Parameter);
                    }
                case "GetIntroducedBills":
                    {
                        return GetIntroducedBills(param.Parameter);
                    }

                case "GetDQuestions":
                    {
                        return GetDQuestions(param.Parameter);
                    }
                case "GetDNotices":
                    {
                        return GetDNotices(param.Parameter);
                    }
                case "GetNoticesByTypeId":
                    {
                        return GetNoticesByTypeId(param.Parameter);
                    }
                case "GetDQuestionsByTypeId":
                    {
                        return GetDQuestionsByTypeId(param.Parameter);
                    }

            }
            return null;
        }

        private static object GetDName(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            //int Id = Convert.ToInt32(param);
            string[] str = param as string[];
            // string[] strOutput = new string[2];
            int AssemblyCode = Convert.ToInt32(str[1]);
            int Id = Convert.ToInt32(str[0]);
            var Res = (from mA in pCtxt.mMemberAssembly
                       join dC in pCtxt.mConstituency on mA.ConstituencyCode equals dC.ConstituencyCode
                       join dst in pCtxt.Districts on dC.DistrictCode equals dst.DistrictCode
                       where mA.AssemblyID == AssemblyCode && mA.MemberID == Id && dC.AssemblyID == AssemblyCode
                       select new PanchayatConstituencyModal
                       {
                           DName = dst.DistrictName

                       }).ToList();



            return Res;
        }

        private static object GetTotalUnpublishedTourCnt(object param)
        {
            int Id = Convert.ToInt32(param);
            NoticeContext pCtxt = new NoticeContext();
            DateTime datetime = DateTime.Now.Date;
            var UnpublisedTour = (from AT in pCtxt.tMemberTour
                                  where (AT.MemberId == Id)
                                  && AT.IsPublished == false
                                  select AT.TourId).Count();

            DiaryModel DM = new DiaryModel();
            DM.ResultCount = UnpublisedTour;
            return DM;
        }

        private static object GetTotalRecentTourCnt(object param)
        {
            int Id = Convert.ToInt32(param);
            NoticeContext pCtxt = new NoticeContext();
            DateTime datetime = DateTime.Now.Date;
            // Get Count for Question
            var PreviousTour = (from AT in pCtxt.tMemberTour
                                where (AT.MemberId == Id)
                                && (AT.TourFromDateTime < datetime)
                                && AT.IsPublished == true
                                select AT.TourId).Count();

            DiaryModel DM = new DiaryModel();
            DM.ResultCount = PreviousTour;
            return DM;
        }

        private static object GetTotalupcomingTourCnt(object param)
        {
            int Id = Convert.ToInt32(param);
            NoticeContext pCtxt = new NoticeContext();
            DateTime datetime = DateTime.Now.Date;
            // Get Count for Question
            var UpComingTour = (from AT in pCtxt.tMemberTour
                                where (AT.MemberId == Id)
                                && (AT.TourFromDateTime > datetime)
                                && AT.IsPublished == true
                                select AT.TourId).Count();

            DiaryModel DM = new DiaryModel();
            DM.ResultCount = UpComingTour;
            return DM;
        }

        private static object GetTotalTodayTourCnt(object param)
        {
            int Id = Convert.ToInt32(param);
            NoticeContext pCtxt = new NoticeContext();
            DateTime datetime = DateTime.Now.Date;
            // Get Count for Question
            var TodayTour = (from AT in pCtxt.tMemberTour
                             where (AT.MemberId == Id)
                             && (AT.TourFromDateTime == datetime)
                             && AT.IsPublished == true
                             select AT.TourId).Count();

            DiaryModel DM = new DiaryModel();
            DM.ResultCount = TodayTour;
            return DM;
        }

        private static object GetRecipientGroupCnt(object param)
        {
            int Id = Convert.ToInt32(param);

            NoticeContext pCtxt = new NoticeContext();

            string AssemblyCode = (from mdl in pCtxt.SiteSettings
                                   where mdl.SettingName == "Assembly"
                                   select mdl.SettingValue).SingleOrDefault();

            int CurrentAssembly = Convert.ToInt32(AssemblyCode);

            var ConstituencyCode = (from Members in pCtxt.mMemberAssembly
                                    where Members.MemberID == Id
                                    && Members.AssemblyID == CurrentAssembly
                                    select Members.ConstituencyCode).SingleOrDefault();

            int CurrentConstituencyCode = Convert.ToInt32(ConstituencyCode);

            var recipientGroupCnt = (from groups in pCtxt.mRecipientGroup
                                     where groups.ConstituencyCode == CurrentConstituencyCode
                                     select groups).Count();

            DiaryModel dm = new DiaryModel();
            dm.ResultCount = recipientGroupCnt;
            return dm;
        }

        private static object GetRecipientGroupMemberCnt(object param)
        {
            int Id = Convert.ToInt32(param);

            NoticeContext pCtxt = new NoticeContext();

            string AssemblyCode = (from mdl in pCtxt.SiteSettings
                                   where mdl.SettingName == "Assembly"
                                   select mdl.SettingValue).SingleOrDefault();

            int CurrentAssembly = Convert.ToInt32(AssemblyCode);

            var ConstituencyCode = (from Members in pCtxt.mMemberAssembly
                                    where Members.MemberID == Id
                                    && Members.AssemblyID == CurrentAssembly
                                    select Members.ConstituencyCode).SingleOrDefault();

            int CurrentConstituencyCode = Convert.ToInt32(ConstituencyCode);

            var recipientGroup = (from groups in pCtxt.mRecipientGroup
                                  where groups.ConstituencyCode == CurrentConstituencyCode
                                  select new
                                  {
                                      groups.GroupID
                                  }).ToList();

            List<int> groupIds = new List<int>();

            foreach (var item in recipientGroup)
            {
                groupIds.Add(item.GroupID);
            }

            var recipientGroupMemberCnt = (from Members in pCtxt.mRecipientGroupMember
                                           where groupIds.Contains(Members.GroupID)
                                           select Members).Count();
            DiaryModel dm = new DiaryModel();
            dm.ResultCount = recipientGroupMemberCnt;
            return dm;
        }

        private static object GetAllPanchayat(object param)
        {
            NoticeContext db = new NoticeContext();
            //int Id = Convert.ToInt32(param);
            string[] str = param as string[];
            //  string[] strOutput = new string[2];
            int AssemblyCode = Convert.ToInt32(str[1]);
            int Id = Convert.ToInt32(str[0]);
            var CCode = (from e in db.mMemberAssembly where e.AssemblyID == AssemblyCode && e.MemberID == Id select e.ConstituencyCode).ToList();

            //var CName=from e in db.mConstituency where e.ConstituencyCode==CCode select e;
            List<PanchayatConstituencyModal> list = new List<PanchayatConstituencyModal>();
            foreach (var item in CCode)
            {
                PanchayatConstituencyModal obj = new PanchayatConstituencyModal();
                var Res1 = (from e in db.tConstituencyPanchayat
                            join mp in db.mPanchayat on e.PanchayatCode equals mp.PanchayatCode


                            where e.ConstituencyCode == item
                            select new PanchayatConstituencyModal
                            {
                                PanchayatName = mp.PanchayatName,
                                PanchayatCode = mp.PanchayatCode,
                                ConstituencyPanchayatID = e.ConstituencyPanchayatID
                            }).ToList();

                obj.tCPanchayat = Res1;
                list.Add(obj);
            }


            return list;
        }

        private static object GetCName(object param)
        {
            NoticeContext db = new NoticeContext();
            //int Id = Convert.ToInt32(param);
            string[] str = param as string[];
            //  string[] strOutput = new string[2];
            int AssemblyCode = Convert.ToInt32(str[1]);
            int Id = Convert.ToInt32(str[0]);
            var CCode = (from e in db.mMemberAssembly where e.AssemblyID == AssemblyCode && e.MemberID == Id select e.ConstituencyCode).FirstOrDefault();

            var CName1 = (from e in db.mConstituency where e.ConstituencyCode == CCode where e.AssemblyID == AssemblyCode select e).FirstOrDefault();
            return CName1;
        }

        private static object DeletePanchayat(object param)
        {
            NoticeContext db = new NoticeContext();
            tConstituencyPanchayat model1 = param as tConstituencyPanchayat;
            string obj = model1.Ids;
            string[] a = obj.Split(',');
            foreach (var item in a)
            {



                int Id = Convert.ToInt32(item);
                var Res = (from e in db.tConstituencyPanchayat where e.ConstituencyPanchayatID == Id select e).SingleOrDefault();
                db.tConstituencyPanchayat.Remove(Res);
                db.SaveChanges();
            }

            return model1;

        }

        private static object AddPanchayat(object param)
        {
            tConstituencyPanchayat model1 = param as tConstituencyPanchayat;
            string obj = model1.Ids;
            string[] a = obj.Split(',');
            // int Ids =Convert.ToInt32( param);
            // string obj = param as string;

            NoticeContext db = new NoticeContext();
            // tConstituencyPanchayat model = new tConstituencyPanchayat();
            foreach (var item in a)
            {
                int Id = Convert.ToInt32(item);
                //var Res = from e in db.tConstituencyPanchayat where e.PanchayatCode == Id select e;
                var Res = from e in db.tConstituencyPanchayat where e.PanchayatCode == Id && e.ConstituencyCode == model1.ConstituencyCode select e;

                if (Res.Count() > 0)
                {
                }
                else
                {


                    model1.PanchayatCode = Id;
                    db.tConstituencyPanchayat.Add(model1);

                    db.SaveChanges();
                }

            }

            // int Id = Convert.ToInt32(param);
            // tConstituencyPanchayat model = new tConstituencyPanchayat() ;
            // tConstituencyPanchayat model1 = param as tConstituencyPanchayat;
            // model.PanchayatCode = Id;
            // model1.DistrictCode=
            //mPanchayat model = param as mPanchayat;
            //model.CreatedDate = DateTime.Now;
            //model.ModifiedDate = DateTime.Now;

            return model1;
        }
        private static object GetDistricts(object param)
        {
            NoticeContext pCtxt = new NoticeContext();

            //int Id = Convert.ToInt32(param);

            string[] str = param as string[];
            //   string[] strOutput = new string[2];
            int AssemblyCode = Convert.ToInt32(str[1]);
            int Id = Convert.ToInt32(str[0]);

            var query = (from mA in pCtxt.mMemberAssembly
                         join dC in pCtxt.mConstituency on mA.ConstituencyCode equals dC.ConstituencyCode
                         join tP in pCtxt.tPanchayatVillage on dC.DistrictCode equals tP.DistrictCode
                         join mP in pCtxt.mPanchayat on tP.PanchayatCode equals mP.PanchayatCode
                         //join mD in pCtxt.Districts on dC.DistrictCode equals mD.DistrictId
                         where mA.AssemblyID == AssemblyCode && mA.MemberID == Id && dC.AssemblyID == AssemblyCode
                         select new PanchayatConstituencyModal
                         {
                             PanchayatCode = mP.PanchayatCode,
                             DstCode = tP.DistrictCode,
                             SCode = tP.StateCode,
                             CCode = dC.ConstituencyCode,
                             PanchayatName = mP.PanchayatName,
                             //DName= mD.DistrictName




                         }).Distinct().ToList();

            if (query != null)
            {
                foreach (var item in query)
                {
                    var Res = (from e in pCtxt.tConstituencyPanchayat where e.PanchayatCode == item.PanchayatCode && e.ConstituencyCode == item.CCode select e.PanchayatCode).FirstOrDefault();
                    if (Res != 0)
                    {
                        item.IsMapped = true;
                    }
                    else
                    {
                        item.IsMapped = false;
                    }
                }
            }

            return query;

        }

        private static object GetTotalGalleriesCnt(object param)
        {
            int Id = Convert.ToInt32(param);
            DiaryModel DM = new DiaryModel();
            NoticeContext db = new NoticeContext();

            var data1 = (from a in db.AlbumCategory
                         join c in db.Category
                             on a.AlbumCategoryID equals c.AlbumCategoryId
                         join b in db.tGallery on c.CategoryID equals b.CategoryID
                         where a.MemberCode == Id
                         select new { Gallery = b, CategoryTypeName = c.CategoryName }).Count();
            // var GCnt = (from e in pCtxt.tGallery select e).Count();

            DM.ResultCount = data1;
            return DM;
        }
        private static object GetTotalPanchayatCnt()
        {
            DiaryModel DM = new DiaryModel();
            NoticeContext pCtxt = new NoticeContext();

            var GCnt = (from e in pCtxt.tConstituencyPanchayat select e).Count();

            DM.ResultCount = GCnt;
            return DM;
        }


        private static object GetTotalAlbumGalleryCnt(object param)
        {
            int Id = Convert.ToInt32(param);
            DiaryModel DM = new DiaryModel();
            NoticeContext pCtxt = new NoticeContext();

            //var GCnt = (from e in pCtxt.Category select e).Count();
            var GCnt = (from a in pCtxt.AlbumCategory
                        join c in pCtxt.Category
                             on a.AlbumCategoryID equals c.AlbumCategoryId

                        where a.MemberCode == Id
                        select a).Count();

            DM.ResultCount = GCnt;
            return DM;
        }

        private static object GetTotalGalleryCount(object param)
        {
            int Id = Convert.ToInt32(param);
            DiaryModel DM = new DiaryModel();
            NoticeContext pCtxt = new NoticeContext();

            var GCnt = (from e in pCtxt.AlbumCategory where e.MemberCode == Id select e).Count();

            DM.ResultCount = GCnt;
            return DM;
        }

        private static object DeleteNoticeType(object param)
        {
            int Id = Convert.ToInt32(param);
            NoticeContext db = new NoticeContext();
            var Res = (from e in db.mNoticeTypes where e.NoticeTypeID == Id select e).FirstOrDefault();
            if (Res != null)
            {
                Res.IsDeleted = true;

            }
            // db.mNoticeTypes.Remove(Res);
            db.SaveChanges();
            db.Close();
            return Res;
        }

        private static object EditNoticeType(object param)
        {
            int Id = Convert.ToInt32(param);
            NoticeContext db = new NoticeContext();
            var Res = (from e in db.mNoticeTypes where e.NoticeTypeID == Id select e).FirstOrDefault();
            db.Close();
            return Res;
        }

        private static object UpdateNoticeType(object param)
        {
            NoticeContext db = new NoticeContext();
            mNoticeType model = param as mNoticeType;
            model.CreatedDate = DateTime.Now;
            model.ModifiedDate = DateTime.Now;
            db.mNoticeTypes.Attach(model);
            db.Entry(model).State = EntityState.Modified;

            db.SaveChanges();
            db.Close();

            return model;


        }

        private static object CreateNoticeType(object param)
        {
            NoticeContext db = new NoticeContext();
            mNoticeType model = param as mNoticeType;
            model.CreatedDate = DateTime.Now;
            model.ModifiedDate = DateTime.Now;
            db.mNoticeTypes.Add(model);
            db.SaveChanges();
            return model;

        }



        //Added By Sujeet
        static object GetCurrentInboxDraftNoticesList(object param)
        {
            tMemberNotice data = (tMemberNotice)param;
            tPaperLaidV model = new tPaperLaidV();

            List<tPaperLaidV> modelToReturn = new List<tPaperLaidV>();

            string csv = data.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            using (NoticeContext context = new NoticeContext())
            {

                var siteSettingData = (from a in context.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
                string FileStructurePath = siteSettingData.SettingValue;

                //Notices Pending For reply.
                var NoticesPendingForReply = (from NT in context.tMemberNotices
                                              join E in context.mEvents on NT.NoticeTypeID equals E.EventId

                                              where NT.AssemblyID == data.AssemblyID
                                                       && NT.SessionID == data.SessionID
                                                       && NT.PaperLaidId == null
                                                       && NT.DepartmentId != null
                                                       && NT.MinistryId != null
                                                       && NT.IsSubmitted == true
                                                       && ids.Contains(NT.DepartmentId)
                                              orderby NT.NoticeTypeID descending
                                              select NT).ToList();

                //Notices Drafts.
                var NoticesDrafts = (from a in context.tPaperLaidVs
                                     join b in context.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                     join c in context.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                     join E in context.mEvents on c.NoticeTypeID equals E.EventId
                                     where a.AssemblyId == data.AssemblyID
                                         && a.SessionId == data.SessionID
                                         && (a.DeptActivePaperId != null)
                                         && b.DeptSubmittedBy == null
                                         && b.DeptSubmittedDate == null
                                         && b.FileName != null
                                         && a.DeptActivePaperId == b.PaperLaidTempId
                                           && ids.Contains(a.DepartmentId)
                                     orderby c.NoticeTypeID descending
                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = a.MinistryName,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,

                                         evidhanReferenceNumber = b.evidhanReferenceNumber,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = c.Subject,
                                         NoticeId = c.NoticeId,
                                         NoticeNumber = c.NoticeNumber,
                                         MemberName = (from mc in context.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                         SubmittedDate = c.SubmittedDate,
                                         PaperSent = FileStructurePath + b.FilePath + b.FileName,
                                         DesireLayingDateId = a.DesireLayingDateId,
                                         RuleNo = E.RuleNo

                                     }).ToList();


                var NoticesPendingForReplyModel = NoticesPendingForReply.AsEnumerable().Select(a => new tPaperLaidV
                {
                    NoticeNumber = a.NoticeNumber,
                    MemberName = (from mc in context.mMembers where mc.MemberCode == a.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                    Title = a.Subject,
                    MinistryName = (from m in context.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).SingleOrDefault(),
                    DeparmentName = (from m in context.mDepartments where m.deptId == a.DepartmentId select m.deptname).SingleOrDefault(),
                    Status = (int)QuestionDashboardStatus.PendingForReply,
                    PaperLaidId = a.PaperLaidId != null ? Convert.ToInt64(a.PaperLaidId) : 0,
                    MinistryId = a.MinistryId != null ? Convert.ToInt32(a.MinistryId) : 0,
                    DepartmentId = a.DepartmentId,
                    FileName = a.OriDiaryFileName,
                    IsAcknowledgmentDate = a.IsAcknowledgmentDate,

                    AcknowledgmentDateString = a.IsAcknowledgmentDate != null ? Convert.ToDateTime(a.IsAcknowledgmentDate).ToString("dd/MM/yyyy hh:mm:ss tt") : null,
                    SubmittedDateString = Convert.ToDateTime(a.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt"),

                    FilePath = a.OriDiaryFilePath,
                    tpaperLaidTempId = 0,
                    FileVersion = 0,
                    actualFilePath = a.OriDiaryFilePath + a.OriDiaryFilePath,
                    NoticeID = a.NoticeId,
                    DepartmentSubmitDate = null,
                    PaperSent = "Not Sent",

                    RuleNo = (from m in context.mEvents where m.EventId == a.NoticeTypeID select m.RuleNo).SingleOrDefault(),
                }).ToList();

                var NoticesDraftModel = NoticesDrafts.AsEnumerable().Select(a => new tPaperLaidV
                {
                    NoticeNumber = a.NoticeNumber,
                    MemberName = a.MemberName,
                    Title = a.Title,
                    MinistryName = (from m in context.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).SingleOrDefault(),
                    DeparmentName = (from m in context.mDepartments where m.deptId == a.DepartmentId select m.deptname).SingleOrDefault(),
                    Status = (int)QuestionDashboardStatus.DraftReplies,
                    PaperLaidId = a.paperLaidId,
                    MinistryId = a.MinistryId,
                    DepartmentId = a.DepartmentId,
                    FileName = a.FileName,
                    FilePath = a.FilePath,
                    SubmittedDateString = Convert.ToDateTime(a.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt"),
                    evidhanReferenceNumber = a.evidhanReferenceNumber,
                    tpaperLaidTempId = a.tpaperLaidTempId,
                    FileVersion = a.FileVersion,
                    actualFilePath = a.FilePath + a.FileName,
                    NoticeID = a.NoticeId,
                    RuleNo = a.RuleNo,
                    DepartmentSubmitDate = null,
                    PaperSent = a.PaperSent,
                    DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in context.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).SingleOrDefault()


                }).ToList();





                modelToReturn.AddRange(NoticesPendingForReplyModel);
                modelToReturn.AddRange(NoticesDraftModel);


                var resultCount = NoticesPendingForReplyModel.Count() + NoticesDraftModel.Count();

                var results = modelToReturn.Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();

                model.ResultCount = resultCount;
                model.ListtPaperLaidV = results;
            }

            return model;
        }


        private static object GetNoticeInBoxList(object p)
        {
            tMemberNotice data = (tMemberNotice)p;

            string csv = data.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            using (NoticeContext context = new NoticeContext())
            {
                var query = (from NT in context.tMemberNotices
                             join E in context.mEvents on NT.NoticeTypeID equals E.EventId
                             where NT.AssemblyID == data.AssemblyID
                                      && NT.SessionID == data.SessionID
                                      && NT.PaperLaidId == null
                                      && NT.DepartmentId == data.DepartmentId
                                      && NT.MinistryId != null
                                      && NT.IsSubmitted == true
                                      && ids.Contains(NT.DepartmentId)
                             select NT).ToList();

                if (query != null)
                {
                    if (query.Count != 0)
                    {
                        foreach (var item in query)
                        {
                            tMemberNotice secdata = new tMemberNotice();
                            secdata.Subject = item.Subject;
                            secdata.NoticeId = item.NoticeId;
                            secdata.NoticeNumber = item.NoticeNumber;
                            secdata.MemberName = (from member in context.mMembers where member.MemberCode == item.MemberId select member.Name).FirstOrDefault();
                            secdata.Status = item.Status;
                            secdata.DataStatus = "Pending";
                            secdata.RuleNo = item.RuleNo;
                            data.memberNoticeList.Add(secdata);
                        }

                    }
                    else
                    {
                        data.memberNoticeList = new List<tMemberNotice>();
                    }
                }
                else
                {
                    data.memberNoticeList = new List<tMemberNotice>();
                }

                // return data;
            }


            using (var dbcontext = new NoticeContext())
            {
                var paperLaidList = (from a in dbcontext.tPaperLaidVs
                                     join b in dbcontext.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                     join c in dbcontext.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                     where a.AssemblyId == data.AssemblyID
                                         && a.SessionId == data.SessionID
                                         && (a.DeptActivePaperId == null || a.DeptActivePaperId == 0)
                                         && b.DeptSubmittedBy == null
                                         && b.DeptSubmittedDate == null
                                         && a.DepartmentId == data.DepartmentId
                                         && ids.Contains(a.DepartmentId)
                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = (from m in dbcontext.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = (from m in dbcontext.mDepartments where m.deptId == a.DepartmentId select m.deptname).FirstOrDefault(),
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         NoticeId = c.NoticeId,
                                         NoticeNumber = c.NoticeNumber,
                                         MemberName = (from member in dbcontext.mMembers
                                                       where member.MemberCode == c.MemberId
                                                       select member.Name).FirstOrDefault()

                                     }).ToList();
                if (paperLaidList != null)
                {
                    // data.ResultCount = paperLaidList.Count;
                    foreach (var item in paperLaidList.Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList())
                    {
                        tMemberNotice secdata = new tMemberNotice();
                        secdata.PaperLaidId = item.paperLaidId;
                        secdata.Subject = item.Title;
                        secdata.NoticeId = item.NoticeId;
                        secdata.MinisterName = item.MinistryName;
                        secdata.NoticeNumber = item.NoticeNumber;
                        secdata.MemberName = item.MemberName;
                        secdata.DepartmentName = item.DeparmentName;
                        secdata.DataStatus = "Draft";
                        data.memberNoticeList.Add(secdata);
                    }


                }
                if (data.memberNoticeList.Count != 0)
                {
                    data.ResultCount = data.memberNoticeList.Count;
                    //data.memberNoticeList.Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();
                    data.memberNoticeList.ToList();

                    return data;
                }
                else
                {
                    return data;
                }

            }

        }
        static object GetAllCurrentNoticesList(object param)
        {
            tMemberNotice data = (tMemberNotice)param;
            tPaperLaidV model = new tPaperLaidV();

            List<tPaperLaidV> modelToReturn = new List<tPaperLaidV>();

            string csv = data.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            using (NoticeContext context = new NoticeContext())
            {
                //Notices Pending For reply.
                var NoticesPendingForReply = (from NT in context.tMemberNotices
                                              where NT.AssemblyID == data.AssemblyID
                                                       && NT.SessionID == data.SessionID
                                                       && NT.PaperLaidId == null

                                                       && NT.MinistryId != null
                                                       && NT.IsSubmitted == true
                                                       && ids.Contains(NT.DepartmentId)
                                              select NT).ToList();

                //Notices Drafts.
                var NoticesDrafts = (from a in context.tPaperLaidVs
                                     join b in context.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                     join c in context.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                     where a.AssemblyId == data.AssemblyID
                                         && a.SessionId == data.SessionID
                                         && (a.DeptActivePaperId != null)
                                         && b.DeptSubmittedBy == null
                                         && b.DeptSubmittedDate == null

                                         && a.DeptActivePaperId == b.PaperLaidTempId
                                         && ids.Contains(c.DepartmentId)

                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = a.MinistryName,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         NoticeId = c.NoticeId,
                                         NoticeNumber = c.NoticeNumber,
                                         MemberName = (from mc in context.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                         PaperSent = b.FilePath + b.FileName,
                                         DesireLayingDateId = a.DesireLayingDateId
                                     }).ToList();

                //Notices Sent list.
                var NoticesSent = (from a in context.tPaperLaidVs
                                   join b in context.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                   join c in context.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                   where
                                       (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
                                       && b.DeptSubmittedBy != null
                                       && b.DeptSubmittedDate != null
                                       && a.DeptActivePaperId == b.PaperLaidTempId

                                       && ids.Contains(c.DepartmentId)

                                   select new
                                   {
                                       NoticeNumber = c.NoticeNumber,
                                       MemberName = (from mc in context.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                       paperLaidId = a.PaperLaidId,
                                       MinistryId = a.MinistryId,
                                       MinistryName = a.MinistryName,
                                       DepartmentId = a.DepartmentId,
                                       DeparmentName = a.DeparmentName,
                                       FileName = b.FileName,
                                       FilePath = b.FilePath,
                                       tpaperLaidTempId = b.PaperLaidTempId,
                                       FileVersion = b.Version,
                                       Title = a.Title,
                                       BillSentToMinisterDate = b.DeptSubmittedDate,
                                       NoticeID = c.NoticeId,
                                       PaperSent = b.SignedFilePath,
                                       DesireLayingDateId = a.DesireLayingDateId
                                   }).ToList();

                var NoticesPendingForReplyModel = NoticesPendingForReply.AsEnumerable().Select(a => new tPaperLaidV
                {
                    NoticeNumber = a.NoticeNumber,
                    MemberName = (from mc in context.mMembers where mc.MemberCode == a.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                    Title = a.Subject,
                    MinistryName = (from m in context.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).SingleOrDefault(),
                    DeparmentName = (from m in context.mDepartments where m.deptId == a.DepartmentId select m.deptname).SingleOrDefault(),
                    Status = (int)QuestionDashboardStatus.PendingForReply,
                    PaperLaidId = a.PaperLaidId != null ? Convert.ToInt64(a.PaperLaidId) : 0,
                    MinistryId = a.MinistryId != null ? Convert.ToInt32(a.MinistryId) : 0,
                    DepartmentId = a.DepartmentId,
                    FileName = a.OriDiaryFileName,
                    FilePath = a.OriDiaryFilePath,
                    tpaperLaidTempId = 0,
                    FileVersion = 0,
                    actualFilePath = a.OriDiaryFilePath + a.OriDiaryFilePath,
                    NoticeID = a.NoticeId,
                    //PaperSent = a.PaperLaidId,
                    DepartmentSubmitDate = null


                }).ToList();

                var NoticesDraftModel = NoticesDrafts.AsEnumerable().Select(a => new tPaperLaidV
                {
                    NoticeNumber = a.NoticeNumber,
                    MemberName = a.MemberName,
                    Title = a.Title,
                    MinistryName = (from m in context.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).SingleOrDefault(),
                    DeparmentName = (from m in context.mDepartments where m.deptId == a.DepartmentId select m.deptname).SingleOrDefault(),
                    Status = (int)QuestionDashboardStatus.DraftReplies,
                    PaperLaidId = a.paperLaidId,
                    MinistryId = a.MinistryId,
                    DepartmentId = a.DepartmentId,
                    FileName = a.FileName,
                    FilePath = a.FilePath,
                    tpaperLaidTempId = a.tpaperLaidTempId,
                    FileVersion = a.FileVersion,
                    actualFilePath = a.FilePath + a.FileName,
                    NoticeID = a.NoticeId,
                    DepartmentSubmitDate = null,
                    PaperSent = a.PaperSent,
                    DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in context.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).SingleOrDefault()


                }).ToList();

                //
                var NoticesSentModel = NoticesSent.AsEnumerable().Select(a => new tPaperLaidV
                {
                    NoticeNumber = a.NoticeNumber,
                    MemberName = a.MemberName,
                    MinistryName = (from m in context.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).SingleOrDefault(),
                    DeparmentName = (from m in context.mDepartments where m.deptId == a.DepartmentId select m.deptname).SingleOrDefault(),
                    Status = (int)QuestionDashboardStatus.RepliesSent,
                    DepartmentId = a.DepartmentId,
                    PaperLaidId = a.paperLaidId,
                    MinistryId = a.MinistryId,
                    FileName = a.FileName,
                    FilePath = a.FilePath,
                    tpaperLaidTempId = a.tpaperLaidTempId,
                    FileVersion = a.FileVersion,
                    Title = a.Title,
                    actualFilePath = a.FilePath + a.FileName,
                    NoticeID = a.NoticeID,
                    DepartmentSubmitDate = a.BillSentToMinisterDate,
                    PaperSent = a.PaperSent,
                    DeptSubmittedDate = a.BillSentToMinisterDate,
                    DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in context.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).SingleOrDefault()
                }).ToList();

                modelToReturn.AddRange(NoticesPendingForReplyModel);
                modelToReturn.AddRange(NoticesDraftModel);
                modelToReturn.AddRange(NoticesSentModel);

                var resultCount = NoticesPendingForReplyModel.Count() + NoticesDraftModel.Count() + NoticesSentModel.Count();

                var results = modelToReturn.Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();

                model.ResultCount = resultCount;
                model.ListtPaperLaidV = results;
            }

            return model;
        }

        static object UpdatePaperLaidEntryForNoticeReplace(object param)
        {
            tPaperLaidV updatedDetails = param as tPaperLaidV;
            NoticeContext paperLaidCntxtDB = new NoticeContext();
            var existingDetails = (from model in paperLaidCntxtDB.tPaperLaidVs where model.PaperLaidId == updatedDetails.PaperLaidId select model).SingleOrDefault();

            if (existingDetails != null)
            {

                existingDetails.DeptActivePaperId = updatedDetails.DeptActivePaperId;
                existingDetails.ModifiedBy = updatedDetails.ModifiedBy;
                existingDetails.ModifiedWhen = updatedDetails.ModifiedWhen;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }
            return existingDetails;
        }

        private static object GetNoticePLIHList(object p)
        {
            using (var pCtxt = new NoticeContext())
            {
                // var currentdate = DateTime.Now;

                tPaperLaidV model = p as tPaperLaidV;
                var currentdate = DateTime.Now;

                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);

                var noticePLIHCount = (from notice in pCtxt.tMemberNotices
                                       join paperLaidC in pCtxt.tPaperLaidVs on notice.PaperLaidId equals paperLaidC.PaperLaidId
                                       join paperLaidTemp in pCtxt.tPaperLaidTemps on notice.PaperLaidId equals paperLaidTemp.PaperLaidId
                                       where
                                          notice.PaperLaidId != null
                                         && paperLaidC.LaidDate == null
                                         && ((from m in pCtxt.mSessionDates where m.SessionId == paperLaidC.DesireLayingDateId select m.SessionDate).FirstOrDefault() > currentdate)
                                         && paperLaidC.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId
                                         && paperLaidC.LOBRecordId == null
                                         && paperLaidTemp.DeptSubmittedBy != null
                                         && paperLaidTemp.DeptSubmittedDate != null
                                         && paperLaidTemp.MinisterSubmittedBy != null
                                         && paperLaidTemp.MinisterSubmittedDate != null
                                         && ids.Contains(notice.DepartmentId)
                                       select new
                                       {
                                           paperLaidId = paperLaidC.PaperLaidId,
                                           MinistryId = paperLaidC.MinistryId,
                                           MinistryName = paperLaidC.MinistryName,
                                           DepartmentId = paperLaidC.DepartmentId,
                                           DeparmentName = paperLaidC.DeparmentName,
                                           FileName = paperLaidTemp.FileName,
                                           FilePath = paperLaidTemp.FilePath,
                                           tpaperLaidTempId = paperLaidTemp.PaperLaidTempId,
                                           FileVersion = paperLaidTemp.Version,
                                           Title = paperLaidC.Title,
                                           BillSentToMinisterDate = paperLaidTemp.DeptSubmittedDate,
                                           MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == notice.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                           NoticeNumber = notice.NoticeNumber,
                                           NoticeId = notice.NoticeId
                                       }).ToList();


                if (noticePLIHCount != null)
                {
                    model.ResultCount = noticePLIHCount.Count;
                    model.ListtPaperLaidV = noticePLIHCount.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        NoticeID = a.NoticeId,
                        NoticeNumber = a.NoticeNumber,
                        MemberName = a.MemberName,
                    }).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                    return model;
                }

                return model;


            }
        }

        private static object GetNoticeLIHList(object p)
        {
            using (var ctx = new NoticeContext())
            {
                tPaperLaidV model = p as tPaperLaidV;
                var currentdate = DateTime.Now;
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);

                var noticeLIHCount = (from a in ctx.tPaperLaidVs
                                      join b in ctx.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                      join c in ctx.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                      where c.PaperLaidId != null

                                      && a.LOBRecordId != null
                                      && a.DeptActivePaperId == b.PaperLaidTempId
                                      && a.MinisterActivePaperId != null
                                      && a.LaidDate != null
                                      && a.LaidDate <= currentdate
                                      && b.DeptSubmittedBy != null
                                      && b.DeptSubmittedDate != null
                                      && b.MinisterSubmittedBy != null
                                      && b.MinisterSubmittedDate != null
                                      && ids.Contains(c.DepartmentId)
                                      select new
                                      {
                                          paperLaidId = a.PaperLaidId,
                                          MinistryId = a.MinistryId,
                                          MinistryName = a.MinistryName,
                                          DepartmentId = a.DepartmentId,
                                          DeparmentName = a.DeparmentName,
                                          FileName = b.FileName,
                                          FilePath = b.FilePath,
                                          tpaperLaidTempId = b.PaperLaidTempId,
                                          FileVersion = b.Version,
                                          Title = a.Title,
                                          BillSentToMinisterDate = b.DeptSubmittedDate,
                                          MemberName = (from mc in ctx.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                          NoticeNumber = c.NoticeNumber,
                                          NoticeId = c.NoticeId
                                      }).ToList();

                if (noticeLIHCount != null)
                {
                    model.ResultCount = noticeLIHCount.Count;
                    model.ListtPaperLaidV = noticeLIHCount.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        NoticeID = a.NoticeId,
                        NoticeNumber = a.NoticeNumber,
                        MemberName = a.MemberName,
                    }).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                    return model;
                }

                return model;
            }
        }

        private static object GetNoticeULOBList(object p)
        {
            tPaperLaidV model = p as tPaperLaidV;
            var currentdate = DateTime.Now;
            string csv = model.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            using (var ctx = new NoticeContext())
            {

                var noticeULOBCount = (from a in ctx.tPaperLaidVs
                                       join b in ctx.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                       join c in ctx.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                       where b.DeptSubmittedDate != null
                                       && b.DeptSubmittedBy != null
                                       && b.MinisterSubmittedBy != 0
                                       && b.MinisterSubmittedDate != null
                                       && ((from m in ctx.mSessionDates where m.SessionId == a.DesireLayingDateId select m.SessionDate).FirstOrDefault() >= currentdate)
                                       && a.LaidDate == null
                                       && a.LOBRecordId != null

                                       && ids.Contains(c.DepartmentId)
                                       select new
                                       {
                                           paperLaidId = a.PaperLaidId,
                                           MinistryId = a.MinistryId,
                                           MinistryName = a.MinistryName,
                                           DepartmentId = a.DepartmentId,
                                           DeparmentName = a.DeparmentName,
                                           FileName = b.FileName,
                                           FilePath = b.FilePath,
                                           tpaperLaidTempId = b.PaperLaidTempId,
                                           FileVersion = b.Version,
                                           Title = a.Title,
                                           BillSentToMinisterDate = b.DeptSubmittedDate,
                                           MemberName = (from mc in ctx.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                           NoticeNumber = c.NoticeNumber,
                                           NoticeId = c.NoticeId
                                       }).ToList();

                if (noticeULOBCount != null)
                {
                    model.ResultCount = noticeULOBCount.Count;
                    model.ListtPaperLaidV = noticeULOBCount.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        NoticeID = a.NoticeId,
                        NoticeNumber = a.NoticeNumber,
                        MemberName = a.MemberName,
                    }).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                    return model;
                }

                return model;
            }

        }

        static object GetNoticeSubmittedPaperLaidByID(object param)
        {
            NoticeContext db = new NoticeContext();

            tPaperLaidV model = param as tPaperLaidV;
            //string csv = model.DepartmentId;
            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;
            //IEnumerable<string> ids = csv.Split(',').Select(str => str);
            model.DepartmentSubmitList = (from tPaper in db.tPaperLaidVs
                                          join mEvents in db.mEvents on tPaper.EventId equals mEvents.EventId
                                          join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                                          join mDept in db.mDepartments on tPaper.DepartmentId equals mDept.deptId
                                          join tPaperTemp in db.tPaperLaidTemps on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
                                          join mPaperCategory in db.mPaperCategoryTypes on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                                          where model.PaperLaidId == tPaper.PaperLaidId

                                          select new PaperMovementModel
                                          {
                                              DeptSubmittedDate = tPaperTemp.DeptSubmittedDate,
                                              PaperTypeName = mPaperCategory.Name,
                                              actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
                                              FilePath = tPaperTemp.FilePath,
                                              DeptSubmittedBy = tPaperTemp.DeptSubmittedBy,
                                              PaperLaidId = model.PaperLaidId,
                                              DeptActivePaperId = tPaper.DeptActivePaperId,
                                              version = tPaperTemp.Version,
                                              Description = tPaper.Description,
                                              Title = tPaper.Title,
                                              ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                                              Remark = tPaper.Remark,
                                              MinistryName = tPaper.MinistryName,
                                              FileVersion = tPaperTemp.Version,
                                              FileName = tPaperTemp.FileName,
                                              FileStructurePath = FileStructurePath,
                                              DocFileVersion = tPaperTemp.DocFileVersion,
                                              DocFilePath = tPaperTemp.FilePath + tPaperTemp.DocFileName,
                                              DesireLayingDate = tPaper.DesireLayingDateId == 0 ? (DateTime?)null : (from m in db.mSessionDates where m.Id == tPaper.DesireLayingDateId select m.SessionDate).FirstOrDefault()


                                          }).ToList();


            db.Close();
            tPaperLaidV data = new tPaperLaidV();
            data.PaperLaidId = model.PaperLaidId;
            data = GetNoticeReplySentRecordById(data);
            data.FileStructurePath = FileStructurePath;
            data.DepartmentSubmitList = model.DepartmentSubmitList;

            return data;
        }

        static object UpdateNoticePaperLaidFileByID(object param)
        {
            tPaperLaidTemp model = param as tPaperLaidTemp;

            NoticeContext db = new NoticeContext();
            if (model == null)
            {
                return null;
            }
            model = db.tPaperLaidTemps.Add(model);

            var RefNumber = model.AssemblyId + "/" + model.SessionId + "/" + model.PaperLaidId + "/" + "V" + model.Version;
            model.evidhanReferenceNumber = RefNumber;

            db.SaveChanges();
            model.PaperLaidTempId = model.PaperLaidTempId;
            db.Close();
            return model;
        }



        private static tPaperLaidV GetNoticeReplySentRecordById(object p)
        {
            tPaperLaidV data = (tPaperLaidV)p;

            using (var ctx = new NoticeContext())
            {
                var siteSettingData = (from a in ctx.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
                string FileStructurePath = siteSettingData.SettingValue;
                var paperLaidList = (from a in ctx.tPaperLaidVs
                                     join b in ctx.tPaperLaidTemps
                                         on a.PaperLaidId equals b.PaperLaidId
                                     join c in ctx.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                     join f in ctx.mMinistry on a.MinistryId equals f.MinistryID
                                     where a.PaperLaidId == data.PaperLaidId && a.DeptActivePaperId == b.PaperLaidTempId
                                     select new
                                     {
                                         PaperLaidId = a.PaperLaidId,
                                         AssemblyId = a.AssemblyId,
                                         SessionId = a.SessionId,
                                         //MinistryId = a.MinistryId,
                                         MinistryName = f.MinisterName,
                                         //DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         //DocFileName = a.DocFileName,
                                         DepartmentLayedDate = b.DeptSubmittedDate,
                                         DesireLayingDateId = a.DesireLayingDateId,
                                         DepartmentSubmitedDate = b.DeptSubmittedDate,
                                         Description = a.Description,
                                         ProvisionUnderWhich = a.ProvisionUnderWhich,
                                         Remark = a.Remark,
                                         PaperTypeId = a.PaperTypeID,
                                         NoticeNumber = c.NoticeNumber,
                                         desireLayingDate = a.DesireLayingDate,
                                         SentFile = b.SignedFilePath,
                                         NoticeTypeId = c.NoticeTypeID,
                                         OriDiaryFilePath = c.OriDiaryFilePath,
                                         OriDiaryFileName = c.OriDiaryFileName,
                                         NoticePdfPath = c.NoticePdfPath,
                                         FileStructurePath = FileStructurePath,
                                         // EventId = a.EventId
                                     }).FirstOrDefault();
                if (null != paperLaidList)
                {
                    return new tPaperLaidV
                    {
                        SessionId = paperLaidList.SessionId,
                        AssemblyId = paperLaidList.AssemblyId,
                        // MinistryId = paperLaidList.MinistryId,
                        MinistryName = paperLaidList.MinistryName,
                        //  DepartmentId = paperLaidList.DepartmentId,
                        DeparmentName = paperLaidList.DeparmentName,
                        FileName = paperLaidList.FileName,
                        FilePath = paperLaidList.FilePath,
                        tpaperLaidTempId = paperLaidList.tpaperLaidTempId,
                        FileVersion = paperLaidList.FileVersion,
                        Title = paperLaidList.Title,
                        actualFilePath = paperLaidList.SentFile,
                        FileStructurePath = paperLaidList.FileStructurePath,
                        // DesireLayingDateId = paperLaidList.DesireLayingDateId,
                        Description = paperLaidList.Description,
                        ProvisionUnderWhich = paperLaidList.ProvisionUnderWhich,
                        Remark = paperLaidList.Remark,
                        PaperTypeID = paperLaidList.PaperTypeId,
                        NoticeNumber = paperLaidList.NoticeNumber,
                        PaperLaidId = paperLaidList.PaperLaidId,
                        NoticePdfPath = paperLaidList.NoticePdfPath,
                        //OriDiaryFilePath = paperLaidList.OriDiaryFilePath,
                        //OriDiaryFileName = paperLaidList.OriDiaryFileName,
                        DepartmentSubmitDate = paperLaidList.DepartmentSubmitedDate,
                        BussinessType = paperLaidList.PaperTypeId == 0 ? "" : (from evt in ctx.mEvents where evt.EventId == paperLaidList.NoticeTypeId select evt.EventName).FirstOrDefault(),
                        DesireLayingDate = paperLaidList.DesireLayingDateId == 0 ? (DateTime?)null : (from m in ctx.mSessionDates where m.Id == paperLaidList.DesireLayingDateId select m.SessionDate).FirstOrDefault(),
                        // EventId = paperLaidList.EventId

                    };
                }
                else
                {
                    return null;
                }

            }
        }

        private static object GetNoticeReplySentList(object p)
        {
            tPaperLaidV data = (tPaperLaidV)p;
            List<tPaperLaidV> modelToReturn = new List<tPaperLaidV>();
            // 5 and 6 ids are related to bills in Events.

            string csv = data.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            using (var dbcontext = new NoticeContext())
            {
                var siteSettingData = (from a in dbcontext.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
                string FileStructurePath = siteSettingData.SettingValue;
                var paperLaidList = (from a in dbcontext.tPaperLaidVs
                                     join b in dbcontext.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                     join c in dbcontext.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                     join E in dbcontext.mEvents on c.NoticeTypeID equals E.EventId

                                     where a.AssemblyId == data.AssemblyId
                                         && a.SessionId == data.SessionId
                                         && (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
                                         && b.DeptSubmittedBy != null
                                         && b.DeptSubmittedDate != null
                                         && a.DeptActivePaperId == b.PaperLaidTempId

                                         && ids.Contains(a.DepartmentId)
                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = a.MinistryName,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         DesireLayingDateId = a.DesireLayingDateId,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         evidhanReferenceNumber = b.evidhanReferenceNumber,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         SubmittedDate = c.SubmittedDate,
                                         BillSentToMinisterDate = b.DeptSubmittedDate,
                                         NoticeId = c.NoticeId,
                                         MemberName = (from mc in dbcontext.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                         NoticeNumber = c.NoticeNumber,
                                         DepartmentSubmitDate = b.DeptSubmittedDate,
                                         //PaperSent = FileStructurePath + paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                         PaperSent = FileStructurePath + b.SignedFilePath,
                                         RuleNo = E.RuleNo
                                     }).ToList();

                if (paperLaidList != null)
                {

                    var NoticesSentModel = paperLaidList.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        DepartmentSubmitDate = a.BillSentToMinisterDate,
                        MemberName = a.MemberName,
                        NoticeID = a.NoticeId,
                        NoticeNumber = a.NoticeNumber,
                        evidhanReferenceNumber = a.evidhanReferenceNumber,
                        PaperSent = a.PaperSent,
                        Status = (int)QuestionDashboardStatus.RepliesSent,
                        DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in dbcontext.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).FirstOrDefault(),
                        DeptSubmittedDate = a.BillSentToMinisterDate,
                        SubmittedDateString = Convert.ToDateTime(a.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt"),
                        RuleNo = a.RuleNo,

                    }).ToList(); ;

                    modelToReturn.AddRange(NoticesSentModel);
                    var resultCount = NoticesSentModel.Count();
                    var results = modelToReturn.Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();

                    foreach (var item in modelToReturn)
                    {
                        data.Status = item.Status;

                    }

                    data.ResultCount = resultCount;
                    data.ListtPaperLaidV = results;

                }

                return data;
            }
        }

        private static object GetNoticeDraftReplyById(object p)
        {

            tPaperLaidV data = (tPaperLaidV)p;

            using (var ctx = new NoticeContext())
            {
                var query = (from tQ in ctx.tMemberNotices
                             where (tQ.PaperLaidId == data.PaperLaidId)
                             select tQ.MinistryId).FirstOrDefault();
                var paperLaidList = (from a in ctx.tPaperLaidVs
                                     join b in ctx.tPaperLaidTemps
                                         on a.PaperLaidId equals b.PaperLaidId
                                     where a.PaperLaidId == data.PaperLaidId
                                     && a.DeptActivePaperId == b.PaperLaidTempId
                                     select new
                                     {
                                         PaperLaidId = a.PaperLaidId,
                                         AssemblyId = a.AssemblyId,
                                         SessionId = a.SessionId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = a.MinistryName,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         DesireLayingDateId = a.DesireLayingDateId,
                                         Description = a.Description,
                                         ProvisionUnderWhich = a.ProvisionUnderWhich,
                                         Remark = a.Remark,
                                         PaperTypeId = a.PaperTypeID,
                                         EventId = a.EventId,
                                         DocFileName = b.DocFileName

                                     }).FirstOrDefault();

                return new tPaperLaidV
                {
                    SessionId = paperLaidList.SessionId,
                    AssemblyId = paperLaidList.AssemblyId,
                    MinistryId = query.Value,
                    MinistryName = paperLaidList.MinistryName,
                    DepartmentId = paperLaidList.DepartmentId,
                    DeparmentName = paperLaidList.DeparmentName,
                    FileName = paperLaidList.FileName,
                    FilePath = paperLaidList.FilePath,
                    tpaperLaidTempId = paperLaidList.tpaperLaidTempId,
                    FileVersion = paperLaidList.FileVersion,
                    Title = paperLaidList.Title,
                    actualFilePath = paperLaidList.FilePath + paperLaidList.FileName,
                    DesireLayingDateId = paperLaidList.DesireLayingDateId,
                    Description = paperLaidList.Description,
                    ProvisionUnderWhich = paperLaidList.ProvisionUnderWhich,
                    Remark = paperLaidList.Remark,
                    PaperTypeID = paperLaidList.PaperTypeId,
                    PaperLaidId = paperLaidList.PaperLaidId,
                    EventId = paperLaidList.EventId,
                    DocFileName = paperLaidList.DocFileName,
                    DocFilePath = paperLaidList.FilePath + paperLaidList.DocFileName


                };
            }

        }


        private static object GetNoticeReplyDraftList(object p)
        {

            tPaperLaidV data = (tPaperLaidV)p;

            // 5 and 6 ids are related to bills in Events.

            string csv = data.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            using (var dbcontext = new NoticeContext())
            {
                var paperLaidList = (from a in dbcontext.tPaperLaidVs
                                     join b in dbcontext.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                     join c in dbcontext.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                     where a.AssemblyId == data.AssemblyId
                                         && a.SessionId == data.SessionId
                                         && b.DeptSubmittedBy == null
                                         && b.DeptSubmittedDate == null

                                         && a.DeptActivePaperId == b.PaperLaidTempId
                                         && ids.Contains(a.DepartmentId)
                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = a.MinistryName,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         NoticeId = c.NoticeId,
                                         NoticeNumber = c.NoticeNumber,
                                         MemberName = (from mc in dbcontext.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                     }).ToList();
                if (paperLaidList != null)
                {
                    data.ResultCount = paperLaidList.Count;
                    data.ListtPaperLaidV = paperLaidList.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        NoticeID = a.NoticeId,
                        NoticeNumber = a.NoticeNumber,
                        MemberName = a.MemberName,
                    }).Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();
                    return data;
                }

                return data;
            }


            //  throw new NotImplementedException();
        }


        private static object UpdateNoticeById(object p)
        {
            tMemberNotice model = (tMemberNotice)p;
            using (var ctx = new NoticeContext())
            {
                var data = (from a in ctx.tMemberNotices where a.NoticeId == model.NoticeId select a).FirstOrDefault();

                data.IsAcknowledgmentDate = model.IsAcknowledgmentDate;
                data.PaperLaidId = model.PaperLaidId;
                ctx.SaveChanges();
                return data;

            }
        }

        /*New Code Notice Inbox List*/
        private static object GetNoticeInboxList(object p)
        {

            tPaperLaidV data = (tPaperLaidV)p;

            // 5 and 6 ids are related to bills in Events.

            using (var dbcontext = new NoticeContext())
            {
                var paperLaidList = (from a in dbcontext.tPaperLaidVs
                                     join b in dbcontext.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                     join c in dbcontext.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                     where a.AssemblyId == data.AssemblyId
                                         && a.SessionId == data.SessionId
                                         && b.DeptSubmittedBy == null
                                         && b.DeptSubmittedDate == null
                                         && a.DepartmentId == data.DepartmentId
                                         && a.DeptActivePaperId == b.PaperLaidTempId
                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = a.MinistryName,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         NoticeId = c.NoticeId,
                                         NoticeNumber = c.NoticeNumber,
                                         MemberName = (from mc in dbcontext.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                     }).ToList();
                if (paperLaidList != null)
                {
                    data.ResultCount = paperLaidList.Count;
                    data.ListtPaperLaidV = paperLaidList.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        NoticeID = a.NoticeId,
                        NoticeNumber = a.NoticeNumber,
                        MemberName = a.MemberName,
                    }).Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();
                    return data;
                }

                return data;
            }


            //  throw new NotImplementedException();
        }


        static object GetAllNoticeTypes(object param)
        {
            using (NoticeContext context = new NoticeContext())
            {
                var query = (from NT in context.mNoticeTypes
                             where NT.IsActive == true
                             orderby NT.NoticeTypeID descending
                             where NT.IsDeleted == null
                             select NT);
                return query.ToList();
            }
        }

        static object CreateNotice(object param)
        {
            if (null == param)
            {
                return null;
            }

            MemberContext memberContext = new MemberContext();
            DepartmentContext deptContext = new DepartmentContext();
            MinisteryContext ministryContext = new MinisteryContext();

            tMemberNotice model = param as tMemberNotice;
            using (var dbContext = new NoticeContext())
            {
                if (model != null)
                {
                    if (model.NoticeId == 0)
                    {
                        if (model.MemberId != 0)
                        {
                            //Get the member Name and Name local from the table add to the MemberNotice model.
                            var mem = (from mc in memberContext.mMembers
                                       where mc.MemberCode == model.MemberId
                                       select mc).FirstOrDefault();
                            if (null != mem)
                            {
                                model.MemberName = mem.Name;
                                model.MemberNameLocal = mem.NameLocal;
                            }
                        }

                        dbContext.tMemberNotices.Add(model);
                        dbContext.SaveChanges();

                        dbContext.tMemberNotices.Attach(model);
                        model.NoticeNumber = "HPMS/" + model.AssemblyID + "/" + model.SessionID + "/" + model.NoticeId;
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        if (model.MinistryId != 0)
                        {
                            //Get the minister Name and Name local from the table add to the MemberNotice model.
                            //var ministryModel = (from ministry in ministryContext.mMinsitryMinisteries
                            //                 where ministry.MinistryID == model.MinistryId
                            //                 select ministry).FirstOrDefault();


                            var ministryModel = (from Min in ministryContext.mMinsitry
                                                 join minis in ministryContext.mMinsitryMinisteries on Min.MinistryID equals minis.MinistryID
                                                 where Min.MinistryID == model.MinistryId
                                                 select new mMinisteryMinisterModel
                                                 {
                                                     MinistryID = minis.MinistryID,
                                                     MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName,
                                                     MinisterNameLocal = minis.MinisterNameLocal + ", " + Min.MinistryNameLocal
                                                 }).FirstOrDefault();




                            if (ministryModel != null)
                            {
                                model.MinisterName = ministryModel.MinisterMinistryName;
                                model.MinisterNameLocal = ministryModel.MinisterNameLocal;
                            }
                        }
                        if (!String.IsNullOrEmpty(model.DepartmentId))
                        {
                            //Get the department Name and Name local from the table add to the MemberNotice model.
                            var deptModel = (from dept in deptContext.objmDepartment
                                             where dept.deptId == model.DepartmentId
                                             select dept).FirstOrDefault();
                            if (null != deptModel)
                            {
                                model.DepartmentName = deptModel.deptname;
                                model.DepartmentNameLocal = deptModel.deptnameLocal;
                            }
                        }

                        dbContext.tMemberNotices.Attach(model);
                        dbContext.Entry(model).State = EntityState.Modified;
                        dbContext.SaveChanges();
                    }
                }
                else
                {
                    return null;
                }
                dbContext.Close();
                return model;
            }
        }

        static object GetNoticesByStatus(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            using (var dbContext = new NoticeContext())
            {
                IEnumerable<tMemberNotice> query = (from mn in dbContext.tMemberNotices
                                                    where mn.NoticeStatus == model.NoticeStatus && mn.Actice == true
                                                    orderby mn.NoticeId descending
                                                    select mn).Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();

                dbContext.Close();
                model.memberNoticeList = query.ToList();
                return model;
            }
        }

        static object GetNoticesByStatusID(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            EventContext eventContext = new EventContext();
            using (var dbContext = new NoticeContext())
            {
                IEnumerable<tMemberNoticeModel> query = (from mn in dbContext.tMemberNotices
                                                         where mn.NoticeStatus == model.NoticeStatus && mn.Actice == true
                                                         orderby mn.NoticeId descending
                                                         select new tMemberNoticeModel
                                                         {
                                                             NoticeId = mn.NoticeId,
                                                             SessionID = mn.SessionID,
                                                             MemberId = mn.MemberId,
                                                             NoticeDate = mn.NoticeDate,
                                                             NoticeTime = mn.NoticeTime,
                                                             DepartmentId = mn.DepartmentId,
                                                             DepartmentName = mn.DepartmentName,
                                                             MinisterName = mn.MinisterName,
                                                             Subject = mn.Subject,
                                                             NoticeNumber = mn.NoticeNumber,
                                                             BusinessType = (from ev in dbContext.mEvents
                                                                             where ev.EventId == mn.NoticeTypeID
                                                                             select ev.EventName).FirstOrDefault(),

                                                             SessionName = (from session in dbContext.mSessions
                                                                            where session.SessionCode == mn.SessionID
                                                                            select session.SessionName).FirstOrDefault(),

                                                             MemberName = (from member in dbContext.mMembers
                                                                           where member.MemberCode == mn.MemberId
                                                                           select member.Name).FirstOrDefault(),
                                                         });
                int totalRecords = query.Count();
                var results = query.Skip((model.PageIndex - 1) * model.PageSize).Take(model.PageSize).ToList();

                dbContext.Close();
                model.ResultCount = totalRecords;
                model.memberNoticeModelList = results;
                return model;
            }
        }

        static object PublishNotice(object param)
        {
            //tMemberNotice model = param as tMemberNotice;
            //using (var dbContext = new NoticeContext())
            //{
            //    tMemberNotice objModel = dbContext.tMemberNotices.Single(m => m.NoticeId == model.NoticeId);
            //    objModel.NoticeStatus = (int)NoticeStatusEnum.NoticePublished;
            //    dbContext.SaveChanges();
            //}
            return null;
        }

        static object PublishNoticeForward(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            using (var dbContext = new NoticeContext())
            {
                tMemberNotice objModel = dbContext.tMemberNotices.Single(m => m.NoticeId == model.NoticeId);
                objModel.NoticeStatus = (int)NoticeStatusEnum.NoticePublished;
                dbContext.SaveChanges();
            }
            return model;
        }
        static object DeleteNotice(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            using (var dbContext = new NoticeContext())
            {
                tMemberNotice objModel = dbContext.tMemberNotices.Single(m => m.NoticeId == model.NoticeId);
                objModel.Actice = false;
                dbContext.SaveChanges();
            }
            return model;
        }

        static object GetNoticeByNoticeID(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            using (var dbContext = new NoticeContext())
            {
                var query = (from mn in dbContext.tMemberNotices
                             join a in dbContext.mEvents on mn.NoticeTypeID equals a.EventId
                             where mn.NoticeId == model.NoticeId && mn.Actice == true
                             orderby mn.NoticeId descending
                             select new
                             {
                                 noticeObject = mn,
                                 noticetypename = a.EventName,
                                 OriDiaryFileName = mn.OriDiaryFileName,
                                 OriDiaryFilePath = mn.OriDiaryFilePath,
                                 NoticePdfPath = mn.NoticePdfPath,
                                 PaperLaidId = mn.PaperLaidId,
                                 IsAcknowledgmentDate = mn.IsAcknowledgmentDate,
                                 NoticeNumber = mn.NoticeNumber,
                                 CutMotionFilePath = mn.CutMotionFilePath,
                                 CutMotionFileName = mn.CutMotionFileName,
                                 SubmittedDate = mn.SubmittedDate,
                                 IsTypeChange = mn.IsTypeChange,
                                 OldNoticeTypeID = mn.OldNoticeTypeID,
                                 MinistryId = mn.MinistryId,
                                 TypeChangeDate = mn.TypeChangeDate
                             }).FirstOrDefault();

                query.noticeObject.MemberName = (from member in dbContext.mMembers
                                                 where member.MemberCode == query.noticeObject.MemberId
                                                 select member.Name).FirstOrDefault();
                query.noticeObject.MinisterName = (from member in dbContext.mMinistry
                                                   where member.MinistryID == query.noticeObject.MinistryId
                                                   select member.MinistryName).FirstOrDefault();
                query.noticeObject.DepartmentName = (from member in dbContext.mDepartments
                                                     where member.deptId == query.noticeObject.DepartmentId
                                                     select member.deptname).FirstOrDefault();
                query.noticeObject.BussinessType = query.noticetypename;
                query.noticeObject.NoticeReplyPath = (from val in dbContext.mEvents
                                                      where val.EventId == query.noticeObject.OldNoticeTypeID
                                                      select val.EventName).FirstOrDefault();


                var FL = (from SS in dbContext.SiteSettings
                          where SS.SettingName == "FileAccessingUrlPath"
                          select SS.SettingValue).FirstOrDefault();

                var Attach = FL + "/" + query.CutMotionFilePath + query.CutMotionFileName;

                model = query.noticeObject;
                model.NoticeTypeName = query.noticetypename;
                model.OriDiaryFileName = query.OriDiaryFileName;
                model.OriDiaryFilePath = query.OriDiaryFilePath;
                model.NoticePdfPath = query.NoticePdfPath;
                model.AttachFileName = Attach;
                model.IsAcknowledgmentDate = query.IsAcknowledgmentDate;
                model.AcknowledgmentDateString = Convert.ToDateTime(query.IsAcknowledgmentDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                model.MinistryId = query.MinistryId;
                return model;
            }
        }


        //Sameer
        public static object GetPendingQuestionsForAssign(object param)
        {
            AssignQuestionModel model = param as AssignQuestionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == null) && (questions.IsContentFreeze == null) && (questions.OriDiaryFileName != null) && (questions.OriDiaryFilePath != null)
                         where (questions.QuestionType == (int)QuestionType.StartedQuestion) && (questions.TypistUserId == null)
                         && questions.AssemblyID == model.AssemblyID
                         && questions.SessionID == model.SessionID
                         orderby questions.QuestionID descending
                         select new AssignQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             SubInCatchWord = questions.SubInCatchWord,
                             NoticeDate = questions.NoticeRecievedDate,
                             OriDiaryFileName = questions.OriDiaryFileName,
                             OriDiaryFilePath = questions.OriDiaryFilePath,
                             //   Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();


            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }
        static object GetCountForQuestionTypes(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionID && m.AssemblyID == model.AssemblyID select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyID select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;

            model.mAssemblyList = (from A in pCtxt.mAssemblies
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSessions
                                 where S.AssemblyID == model.AssemblyID
                                 select S).ToList();

            if (model.RoleName == "Vidhan Sabha Typist")
            {
                var StarredCnt = (from questions in pCtxt.tQuestions
                                      //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == model.UId)
                                  where (questions.QuestionType == (int)QuestionType.StartedQuestion) && (questions.TypistUserId == model.UId)
                                  && questions.AssemblyID == model.AssemblyID
                                  && questions.SessionID == model.SessionID
                                  select new TypistQuestionModel
                                  {
                                      QuestionID = questions.QuestionID,
                                      DiaryNumber = questions.DiaryNumber,
                                      Subject = questions.Subject,
                                      NoticeDate = questions.NoticeRecievedDate,
                                      MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                                  }).ToList().Count();

                model.TotalStaredReceived = StarredCnt;

                var UnstarredCnt = (from questions in pCtxt.tQuestions
                                    where (questions.QuestionType == (int)QuestionType.UnstaredQuestion) && (questions.TypistUserId == model.UId)
                                    && questions.AssemblyID == model.AssemblyID
                                    && questions.SessionID == model.SessionID
                                    select new TypistQuestionModel
                                    {
                                        QuestionID = questions.QuestionID,
                                        DiaryNumber = questions.DiaryNumber,
                                        Subject = questions.Subject,
                                        NoticeDate = questions.NoticeRecievedDate,
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                                    }).ToList().Count();

                model.TotalUnStaredReceived = UnstarredCnt;

            }
            else if (model.RoleName == "Vidhan Sabha Proof Reader")
            {
                var StarredCnt = (from questions in pCtxt.tQuestions
                                      //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)
                                  where (questions.QuestionType == (int)QuestionType.StartedQuestion) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)
                                  && questions.AssemblyID == model.AssemblyID
                                  && questions.SessionID == model.SessionID
                                  select new TypistQuestionModel
                                  {
                                      QuestionID = questions.QuestionID,
                                      DiaryNumber = questions.DiaryNumber,
                                      Subject = questions.Subject,
                                      NoticeDate = questions.NoticeRecievedDate,
                                      MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                                  }).ToList().Count();

                model.TotalStaredReceived = StarredCnt;

                var UnstarredCnt = (from questions in pCtxt.tQuestions
                                    where (questions.QuestionType == (int)QuestionType.UnstaredQuestion) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)
                                    && questions.AssemblyID == model.AssemblyID
                                    && questions.SessionID == model.SessionID
                                    select new TypistQuestionModel
                                    {
                                        QuestionID = questions.QuestionID,
                                        DiaryNumber = questions.DiaryNumber,
                                        Subject = questions.Subject,
                                        NoticeDate = questions.NoticeRecievedDate,
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                                    }).ToList().Count();

                model.TotalUnStaredReceived = UnstarredCnt;
            }
            return model;
        }

        public static object GetPendingQuestionsByType(object param)
        {

            TypistQuestionModel model = param as TypistQuestionModel;
            NoticeContext pCtxt = new NoticeContext();
            SiteSettingContext settingContext = new SiteSettingContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            if (model.RoleName == "Vidhan Sabha Typist")
            {
                var query = (from questions in pCtxt.tQuestions
                                 //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == model.UId)
                             where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId == model.UId)
                             && questions.AssemblyID == model.AssemblyID
                             && questions.SessionID == model.SessionID
                             orderby questions.IsProofReading ascending
                             select new TypistQuestionModel
                             {
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 ProofReaderName = questions.ProofReaderUserId,
                                 TypingBy = questions.TypistUserId
                             }).ToList();

                List<TypistQuestionModel> returnToCaller = new List<TypistQuestionModel>();

                foreach (var val in query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList())
                {
                    TypistQuestionModel tempModel = new TypistQuestionModel();
                    tempModel = val;
                    if (val.ProofReaderName != null)
                    {
                        tempModel.UserName = getName(val.ProofReaderName);
                    }

                    if (val.TypingBy != null)
                    {
                        tempModel.TypingBy = getName(val.TypingBy);
                    }


                    returnToCaller.Add(tempModel);
                }

                int totalRecords = query.Count();

                model.ResultCount = totalRecords;

                model.tQuestionModel = returnToCaller;
            }
            else if (model.RoleName == "Vidhan Sabha Proof Reader")
            {
                var query = (from questions in pCtxt.tQuestions
                                 //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)
                             where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)
                             && questions.AssemblyID == model.AssemblyID
                             && questions.SessionID == model.SessionID
                             orderby questions.IsProofReading ascending

                             select new TypistQuestionModel
                             {
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 ProofReaderName = questions.ProofReaderUserId,
                                 TypingBy = questions.TypistUserId
                             }).ToList();

                List<TypistQuestionModel> returnToCaller = new List<TypistQuestionModel>();

                foreach (var val in query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList())
                {
                    TypistQuestionModel tempModel = new TypistQuestionModel();
                    tempModel = val;
                    if (val.ProofReaderName != null)
                    {
                        tempModel.UserName = getName(val.ProofReaderName);
                    }

                    if (val.TypingBy != null)
                    {
                        tempModel.TypingBy = getName(val.TypingBy);
                    }
                    returnToCaller.Add(tempModel);
                }

                int totalRecords = query.Count();
                //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                //model.tQuestionModel = returnToCaller.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                model.tQuestionModel = returnToCaller;
            }



            return model;
        }

        public static string getName(string ProofReaderName)
        {
            var ProofReaderNID = !string.IsNullOrEmpty(ProofReaderName) ? new Guid(ProofReaderName) : new Guid();
            UserContext Ustxt = new UserContext();
            //var UserName = (from User in Ustxt.mUsers
            //                join E in Ustxt.mEmployee on new { UserName1 = User.UserName, DeptId1 = User.DeptId } equals new { UserName1 = E.empcd, DeptId1 = E.deptid }
            //                where User.UserId == ProofReaderNID
            //                select E.empfname + " " + E.empmname + " " + E.emplname).FirstOrDefault();

            var UserName = (from User in Ustxt.mUsers
                                // join E in Ustxt.mEmployee on new { UserName1 = User.UserName, DeptId1 = User.DeptId } equals new { UserName1 = E.empcd, DeptId1 = E.deptid }
                            where User.UserId == ProofReaderNID
                            select User.UserName).FirstOrDefault();
            return UserName;
        }

        static tMemberNotice GetSubmittedQuestionsByType(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == model.QuestionTypeId
                         //where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
                         //    && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null
                         //    && paperLaidTemp.DeptSubmittedDate != null
                         //    && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.UnstartResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }



        static tMemberNotice GetPendingForSubQuestionsByType(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.tQuestions
                         join paperLaidTemp in pCtxt.tPaperLaidTemps on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         join paperLaid in pCtxt.tPaperLaidVs on questions.PaperLaidId equals paperLaid.PaperLaidId
                         where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
                             && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy == null
                             && paperLaidTemp.DeptSubmittedDate == null
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             //Subject = questions.Subject,
                             Subject = paperLaid.Title,
                             PaperLaidId = questions.PaperLaidId,
                             Version = paperLaidTemp.Version,
                             DeptActivePaperId = paperLaid.DeptActivePaperId,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        static tMemberNotice GetLaidInHouseQuestionsByType(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.tQuestions
                         join paperLaidVS in pCtxt.tPaperLaidVs on questions.PaperLaidId equals paperLaidVS.PaperLaidId
                         join paperLaidTemp in pCtxt.tPaperLaidTemps on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
                             && questions.PaperLaidId != null && paperLaidVS.LOBRecordId != null && paperLaidVS.LaidDate != null
                             && paperLaidVS.DeptActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidVS.MinisterActivePaperId != null

                         select questions).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestion = results;

            return model;
        }

        static tMemberNotice GetPendingToLayQuestionsByType(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.tQuestions
                         join paperLaidC in pCtxt.tPaperLaidVs on questions.PaperLaidId equals paperLaidC.PaperLaidId

                         where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
                             && questions.PaperLaidId != null && paperLaidC.LOBRecordId != null && paperLaidC.LaidDate == null

                         select questions).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestion = results;

            return model;
        }
        static object GetCountForLegisUnstartQuestion(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            model.TotalUnStaredReceived = GetUnstartLegisQuestionsByType(model).ResultCount;
            return model;
        }
        static tMemberNotice GetUnstartLegisQuestionsByType(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.QuestionType == model.QuestionTypeId) && (questions.IsContentFreeze == null) && (questions.OriDiaryFileName != null) && (questions.OriDiaryFilePath != null)
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID) && (questions.DeActivateFlag == "" || questions.DeActivateFlag == null) && (questions.TypistUserId != "" || questions.TypistUserId != null)
                         select new QuestionModelCustom
                         {
                             //QuestionID = questions.QuestionID,
                             //QuestionNumber = questions.QuestionNumber,
                             //Subject = questions.Subject,
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }



        static object GetCountForUnstartQuestionTypes(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            if (model.RoleName == "Vidhan Sabha Typist")
            {
                var query = (from questions in pCtxt.tQuestions
                                 //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == model.UId)
                             where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId == model.UId)

                             select new QuestionModelCustom
                             {
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                             }).ToList();

                int totalRecords = query.Count();
                var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                model.tQuestionModel = results;
                model.TotalUnStaredReceived = model.ResultCount;
            }
            else if (model.RoleName == "Vidhan Sabha Proof Reader")
            {
                var query = (from questions in pCtxt.tQuestions
                                 //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId != null)
                             where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)

                             select new QuestionModelCustom
                             {
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                             }).ToList();

                int totalRecords = query.Count();
                var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                model.tQuestionModel = results;
                model.TotalUnStaredReceived = model.ResultCount;
            }
            return model;
        }
        static tMemberNotice GetUnstartQuestionsByType(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            if (model.RoleName == "Vidhan Sabha Typist")
            {
                var query = (from questions in pCtxt.tQuestions
                                 //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == model.UId)
                             where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId == model.UId)

                             select new QuestionModelCustom
                             {
                                 //QuestionID = questions.QuestionID,
                                 //QuestionNumber = questions.QuestionNumber,
                                 //Subject = questions.Subject,
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                             }).ToList();

                int totalRecords = query.Count();
                var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                model.tQuestionModel = results;
            }
            else if (model.RoleName == "Vidhan Sabha Proof Reader")
            {
                var query = (from questions in pCtxt.tQuestions
                                 //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId != null)
                             where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)

                             select new QuestionModelCustom
                             {
                                 //QuestionID = questions.QuestionID,
                                 //QuestionNumber = questions.QuestionNumber,
                                 //Subject = questions.Subject,
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                             }).ToList();

                int totalRecords = query.Count();
                var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                model.tQuestionModel = results;
            }


            return model;
        }


        public static object GetUnStraredTypistView(object param)
        {
            TypistQuestionModel model = param as TypistQuestionModel;

            NoticeContext pCtxt = new NoticeContext();
            SiteSettingContext settingContext = new SiteSettingContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            if (model.RoleName == "Vidhan Sabha Typist")
            {
                var query = (from questions in pCtxt.tQuestions
                                 //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == model.UId)
                             where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId == model.UId)
                             && questions.AssemblyID == model.AssemblyID
                             && questions.SessionID == model.SessionID
                             orderby questions.IsProofReading ascending
                             select new TypistQuestionModel
                             {
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 ProofReaderName = questions.ProofReaderUserId,
                                 TypingBy = questions.TypistUserId
                             }).ToList();
                List<TypistQuestionModel> returnToCaller = new List<TypistQuestionModel>();

                foreach (var val in query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList())
                {
                    TypistQuestionModel tempModel = new TypistQuestionModel();
                    tempModel = val;
                    if (val.ProofReaderName != null)
                    {
                        tempModel.UserName = getName(val.ProofReaderName);
                    }

                    if (val.TypingBy != null)
                    {
                        tempModel.TypingBy = getName(val.TypingBy);
                    }
                    returnToCaller.Add(tempModel);
                }
                int totalRecords = query.Count();
                //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                //model.tQuestionModel = returnToCaller.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                model.tQuestionModel = returnToCaller;
            }
            else if (model.RoleName == "Vidhan Sabha Proof Reader")
            {
                var query = (from questions in pCtxt.tQuestions
                                 //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)
                             where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId != null) && (questions.IsTypistFreeze != null)
                             && questions.AssemblyID == model.AssemblyID
                             && questions.SessionID == model.SessionID
                             orderby questions.IsProofReading ascending

                             select new TypistQuestionModel
                             {
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 ProofReaderName = questions.ProofReaderUserId,
                                 TypingBy = questions.TypistUserId
                             }).ToList();
                List<TypistQuestionModel> returnToCaller = new List<TypistQuestionModel>();

                foreach (var val in query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList())
                {
                    TypistQuestionModel tempModel = new TypistQuestionModel();
                    tempModel = val;
                    if (val.ProofReaderName != null)
                    {
                        tempModel.UserName = getName(val.ProofReaderName);
                    }

                    if (val.TypingBy != null)
                    {
                        tempModel.TypingBy = getName(val.TypingBy);
                    }
                    returnToCaller.Add(tempModel);
                }
                int totalRecords = query.Count();
                //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                //model.tQuestionModel = returnToCaller.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                model.tQuestionModel = returnToCaller;
            }
            return model;
        }



        public static object GetUnstartForAssingList(object param)
        {
            AssignQuestionModel model = param as AssignQuestionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == null) && (questions.IsContentFreeze == null) && (questions.OriDiaryFileName != null) && (questions.OriDiaryFilePath != null)
                         where (questions.QuestionType == (int)QuestionType.UnstaredQuestion) && (questions.TypistUserId == null)
                         && questions.AssemblyID == model.AssemblyID
                         && questions.SessionID == model.SessionID
                         select new AssignQuestionModel
                         {
                             //QuestionID = questions.QuestionID,
                             //QuestionNumber = questions.QuestionNumber,
                             //Subject = questions.Subject,
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             SubInCatchWord = questions.SubInCatchWord,
                             NoticeDate = questions.NoticeRecievedDate,
                             OriDiaryFileName = questions.OriDiaryFileName,
                             OriDiaryFilePath = questions.OriDiaryFilePath,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();


            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }
        static object GetCountForNotices(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            model.NoticeResultCount = GetNotices(model).ResultCount;
            return model;
        }
        static tMemberNotice GetNotices(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from Notices in pCtxt.tMemberNotices


                         select new QuestionModelCustom
                         {
                             //NoticeNumber=Notices.NoticeNumber,
                             //Subject=Notices.Subject,
                             //NoticeId=Notices.NoticeId
                             //QuestionID = questions.QuestionID,
                             //QuestionNumber = questions.QuestionNumber,
                             //Subject = questions.Subject,
                             //MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.NoticeResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }
        static tMemberNotice GetQuestionDetails(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionID == model.QuestionID) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             RefMemCode = questions.ReferenceMemberCode,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MergeQuestion = questions.ReferencedNumber,
                         }).ToList();

            foreach (var val in query)
            {
                string s = val.RefMemCode;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item = (from questions in pCtxt.tQuestions
                                    select new QuestionModelCustom
                                    {
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                                    }).FirstOrDefault();

                        if (string.IsNullOrEmpty(val.CMemName))
                        {
                            val.CMemName = item.MemberName;
                        }
                        else
                        {
                            val.CMemName = val.CMemName + "," + item.MemberName;
                        }


                    }
                }


            }

            foreach (var val in query)
            {
                string s = val.MergeQuestion;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int questionId = int.Parse(values[i]);
                        var item = (from questions in pCtxt.tQuestions
                                    where questions.QuestionID == questionId

                                    select new QuestionModelCustom
                                    {
                                        DiaryNumber = questions.DiaryNumber,

                                    }).FirstOrDefault();
                        if (string.IsNullOrEmpty(val.CDiaryNo))
                        {
                            val.CDiaryNo = item.DiaryNumber;
                        }
                        else
                        {
                            val.CDiaryNo = val.CDiaryNo + "," + item.DiaryNumber;
                        }


                    }
                }


            }
            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }
        static tMemberNotice GetInActiveQuestionDetails(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionID == model.QuestionID) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             RefMemCode = questions.ReferenceMemberCode,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             MergeQuestion = questions.ReferencedNumber,
                         }).ToList();



            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }
        static object GetCountForProofReader(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            model.TotalStaredReceived = GetProofQuestionsByType(model).ResultCount;

            return model;
        }

        static tMemberNotice GetProofQuestionsByType(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.IsContentFreeze == null) && (questions.OriDiaryFilePath != null) && (questions.OriDiaryFileName != null)
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID) && (questions.DeActivateFlag == null || questions.DeActivateFlag == "") && (questions.TypistUserId != null || questions.TypistUserId != "")

                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }

        static tMemberNotice GetButtons(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from Buttons in pCtxt.ActionButtonControl
                         where Buttons.UserId == model.UId
                         select Buttons).ToList();


            model.ButtonList = query.ToList();

            return model;
        }


        static object UpdateQuestions(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;
            TQuestionAuditTrial QuesObj = new TQuestionAuditTrial();
            NoticeContext pCtxt = new NoticeContext();
            tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
            obj.MainQuestion = update.MainQuestion;
            obj.Subject = update.Subject;
            obj.MinistryId = update.MinistryId;
            obj.DepartmentID = update.DepartmentID;
            obj.ProofReaderUserId = update.ProofReaderUserId;
            obj.IsTypistFreeze = true;
            obj.BracketedWithDNo = update.BracketedWithDNo;
            obj.IsQuestionInPart = update.IsQuestionInPart;
            obj.IsHindi = update.IsHindi;
            if (update.BracketedWithDNo != null)
            {
                obj.IsBracket = true;
            }

            // obj.QuestionStatus = 8;
            qCtxDB.SaveChanges();
            var result = SkipQuestionProcessSteps.SkippingQuesProcessingStep4Proof(update.QuestionID);
            string msg = "";

            try
            {

                QuesObj.MainQuesionId = update.QuestionID;
                QuesObj.MemberID = Convert.ToInt32(update.MemberID);
                QuesObj.MainQuestion = update.MainQuestion;
                QuesObj.Subject = update.Subject;
                QuesObj.MinistryId = update.MinistryId;
                QuesObj.DepartmentID = update.DepartmentID;
                QuesObj.EventId = update.EventId;
                QuesObj.NoticeRecievedDate = update.NoticeRecievedDate;
                QuesObj.NoticeRecievedTime = update.NoticeRecievedTime;
                QuesObj.MinistryId = update.MinistryId;
                QuesObj.RecordBy = "Typist";
                QuesObj.ConstituencyName = update.ConstituencyName;
                QuesObj.BracketedWithDNo = update.BracketedWithDNo;
                QuesObj.IsQuestionInPart = update.IsQuestionInPart;
                QuesObj.IsHindi = update.IsHindi;
                if (update.BracketedWithDNo != null)
                {
                    QuesObj.IsBracket = true;
                }
                QuesObj.QuestionType = 1;
                pCtxt.TQuestionAuditTrial.Add(QuesObj);
                pCtxt.SaveChanges();
                pCtxt.Close();
            }


            catch (Exception ex)
            {
                msg = ex.Message;
            }
            if (obj.BracketedWithDNo != null)
            {
                var query = (from NT in qCtxDB.tQuestions
                             where NT.DiaryNumber == obj.BracketedWithDNo && NT.QuestionType == 1
                             select NT.BracketedWithQNo).FirstOrDefault();
                //return query;
                string BQno = query;
                var Qid = (from NT in qCtxDB.tQuestions
                           where NT.DiaryNumber == obj.BracketedWithDNo && NT.QuestionType == 1
                           select NT.QuestionID).FirstOrDefault();
                //var Dno = (from NT in qCtxDB.tQuestions
                //           where NT.DiaryNumber == obj.BracketedWithDNo
                //           select NT.DiaryNumber).FirstOrDefault();

                if (BQno == null)
                {
                    //var query1 = (from NT in qCtxDB.tQuestions
                    //              where NT.DiaryNumber == obj.BracketedWithDNo
                    //              select NT.QuestionID).FirstOrDefault();

                    tQuestion UpdRec = qCtxDB.tQuestions.Single(m => m.DiaryNumber == obj.BracketedWithDNo && m.QuestionType == 1);
                    UpdRec.BracketedWithQNo = Qid + "," + update.QuestionID.ToString();
                    UpdRec.MergeDiaryNo = update.DiaryNumber;
                    qCtxDB.SaveChanges();
                }

                if (BQno != null)
                {
                    //var query1 = (from NT in qCtxDB.tQuestions
                    //              where NT.DiaryNumber == obj.BracketedWithDNo
                    //              select NT.DiaryNumber).FirstOrDefault();

                    tQuestion UpdRec = qCtxDB.tQuestions.Single(m => m.DiaryNumber == obj.BracketedWithDNo && m.QuestionType == 1);

                    UpdRec.BracketedWithQNo = BQno + "," + update.QuestionID.ToString();
                    UpdRec.MergeDiaryNo = UpdRec.MergeDiaryNo + "," + update.DiaryNumber;
                    qCtxDB.SaveChanges();
                }


            }


            return msg;
        }
        static object UnstaredUpdateQuestions(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;
            TQuestionAuditTrial QuesObj = new TQuestionAuditTrial();
            NoticeContext pCtxt = new NoticeContext();
            tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
            obj.MainQuestion = update.MainQuestion;
            obj.Subject = update.Subject;
            obj.MinistryId = update.MinistryId;
            obj.DepartmentID = update.DepartmentID;
            obj.ProofReaderUserId = update.ProofReaderUserId;
            obj.IsTypistFreeze = true;
            obj.BracketedWithDNo = update.BracketedWithDNo;
            obj.IsQuestionInPart = update.IsQuestionInPart;
            obj.IsHindi = update.IsHindi;

            if (update.BracketedWithDNo != null)
            {
                obj.IsBracket = true;
            }

            // obj.QuestionStatus = 8;
            qCtxDB.SaveChanges();
            string msg = "";

            try
            {

                QuesObj.MainQuesionId = update.QuestionID;
                QuesObj.MemberID = Convert.ToInt32(update.MemberID);
                QuesObj.MainQuestion = update.MainQuestion;
                QuesObj.Subject = update.Subject;
                QuesObj.MinistryId = update.MinistryId;
                QuesObj.DepartmentID = update.DepartmentID;
                QuesObj.EventId = update.EventId;
                QuesObj.NoticeRecievedDate = update.NoticeRecievedDate;
                QuesObj.NoticeRecievedTime = update.NoticeRecievedTime;
                QuesObj.MinistryId = update.MinistryId;
                QuesObj.RecordBy = "Typist";
                QuesObj.ConstituencyName = update.ConstituencyName;
                QuesObj.BracketedWithDNo = update.BracketedWithDNo;
                QuesObj.IsQuestionInPart = update.IsQuestionInPart;
                QuesObj.IsHindi = update.IsHindi;
                if (update.BracketedWithDNo != null)
                {
                    QuesObj.IsBracket = true;
                }
                QuesObj.QuestionType = 2;
                pCtxt.TQuestionAuditTrial.Add(QuesObj);
                pCtxt.SaveChanges();
                pCtxt.Close();
            }


            catch (Exception ex)
            {
                msg = ex.Message;
            }
            if (obj.BracketedWithDNo != null)
            {
                var query = (from NT in qCtxDB.tQuestions
                             where NT.DiaryNumber == obj.BracketedWithDNo && NT.QuestionType == 2
                             select NT.BracketedWithQNo).FirstOrDefault();
                //return query;
                string BQno = query;
                var Qid = (from NT in qCtxDB.tQuestions
                           where NT.DiaryNumber == obj.BracketedWithDNo && NT.QuestionType == 2
                           select NT.QuestionID).FirstOrDefault();
                //var Dno = (from NT in qCtxDB.tQuestions
                //           where NT.DiaryNumber == obj.BracketedWithDNo
                //           select NT.DiaryNumber).FirstOrDefault();

                if (BQno == null)
                {
                    //var query1 = (from NT in qCtxDB.tQuestions
                    //              where NT.DiaryNumber == obj.BracketedWithDNo
                    //              select NT.QuestionID).FirstOrDefault();

                    tQuestion UpdRec = qCtxDB.tQuestions.Single(m => m.DiaryNumber == obj.BracketedWithDNo && m.QuestionType == 2);
                    UpdRec.BracketedWithQNo = Qid + "," + update.QuestionID.ToString();
                    UpdRec.MergeDiaryNo = update.DiaryNumber;
                    qCtxDB.SaveChanges();
                }

                if (BQno != null)
                {
                    //var query1 = (from NT in qCtxDB.tQuestions
                    //              where NT.DiaryNumber == obj.BracketedWithDNo
                    //              select NT.DiaryNumber).FirstOrDefault();

                    tQuestion UpdRec = qCtxDB.tQuestions.Single(m => m.DiaryNumber == obj.BracketedWithDNo && m.QuestionType == 2);

                    UpdRec.BracketedWithQNo = BQno + "," + update.QuestionID.ToString();
                    UpdRec.MergeDiaryNo = UpdRec.MergeDiaryNo + "," + update.DiaryNumber;
                    qCtxDB.SaveChanges();
                }


            }


            return msg;
        }
        //static object UpdateContentFreeze(object param)
        //{

        //    QuestionsContext qCtxDB = new QuestionsContext();
        //    tQuestion update = param as tQuestion;
        //    tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
        //    obj.MainQuestion = update.MainQuestion;
        //    obj.Subject = update.Subject;
        //    obj.IsContentFreeze = update.IsContentFreeze;
        //    qCtxDB.SaveChanges();

        //    return null;
        //}

        static tMemberNotice GetNoticesValue(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from Notices in pCtxt.tMemberNotices


                         select new QuestionModelCustom
                         {
                             NoticeNumber = Notices.NoticeNumber,
                             Subject = Notices.Subject,
                             NoticeId = Notices.NoticeId,
                             NoticeDate = Notices.NoticeDate,
                             EventId = Notices.NoticeTypeID,
                             //QuestionID = questions.QuestionID,
                             //QuestionNumber = questions.QuestionNumber,
                             //Subject = questions.Subject,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notices.MemberId select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }
        static object GetAllNotice(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            using (NoticeContext context = new NoticeContext())
            {
                var query = (from NT in context.tMemberNotices
                             select NT);
                return query.ToList();
            }

        }
        static tMemberNotice GetDetailsforViewNotices(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from Notices in pCtxt.tMemberNotices
                         where Notices.NoticeId == model.NoticeId

                         select new QuestionModelCustom
                         {
                             NoticeNumber = Notices.NoticeNumber,
                             Subject = Notices.Subject,
                             NoticeId = Notices.NoticeId,
                             MemberId = Notices.MemberId,
                             Notice = Notices.Notice,
                             NoticeTime = Notices.NoticeTime,
                             NoticeDate = Notices.NoticeDate,
                             EventId = Notices.NoticeTypeID,
                             //QuestionID = questions.QuestionID,
                             //QuestionNumber = questions.QuestionNumber,
                             //Subject = questions.Subject,
                             //MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.NoticeResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }
        static object UpdateNotice(object param)
        {

            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice update = param as tMemberNotice;
            tMemberNotice obj = pCtxt.tMemberNotices.Single(m => m.NoticeId == update.NoticeId);
            obj.Notice = update.Notice;
            obj.Subject = update.Subject;
            pCtxt.SaveChanges();

            return null;
        }
        static List<tQuestionModel> GetNoticeAttachment(object param)
        {
            NoticeContext db = new NoticeContext();

            int NoticeID = Convert.ToInt32(param);


            var query = (from ministerModel in db.tMemberNotices
                         where ministerModel.NoticeId == NoticeID
                         select new tQuestionModel
                         {

                             AnswerAttachLocation = ministerModel.OriDiaryFilePath + ministerModel.OriDiaryFileName
                         });


            return query.ToList();

        }

        static object GetUserCode(object param)
        {
            AssignQuestionModel model = param as AssignQuestionModel;
            UserContext Ustxt = new UserContext();
            //var query = (from User in Ustxt.mUsers
            //             join UserRole in Ustxt.tUserRoles on User.UserId equals UserRole.UserID
            //             join R in Ustxt.mRoles on UserRole.Roleid equals R.RoleId
            //             join E in Ustxt.mEmployee on new { UserName1 = User.UserName, DeptId1 = User.DeptId } equals new { UserName1 = E.empcd, DeptId1 = E.deptid }
            //             where R.RoleName == model.RoleName
            //             //orderby E.empfname ascending
            //             orderby User.UserName ascending
            //             select new AssignQuestionModel
            //             {
            //              UserName = E.empfname + " " + E.empmname + " " + E.emplname,
            //              //  UserName= User.UserName,
            //                 UserId = User.UserId,
            //                 UId = User.UserId
            //             }).ToList();
            var query = (from User in Ustxt.mUsers
                         join UserRole in Ustxt.tUserRoles on User.UserId equals UserRole.UserID
                         join R in Ustxt.mRoles on UserRole.Roleid equals R.RoleId
                         where R.RoleName == model.RoleName
                         orderby User.UserName ascending

                         select new AssignQuestionModel
                         {

                             UserName = User.UserName,
                             TypistUserId = User.UserId,
                             UserId = User.UserId,
                             UId = User.UserId
                         }).ToList();

            var results = query.ToList();
            model.UserList = results;

            return model;
        }
        static object AssignQuestions(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;

            try
            {
                //            using (var scope = new TransactionScope(TransactionScopeOption.Required)) 
                //{  
                //  try
                //  {
                //    //Some stuff
                //    scope.Complete(); 
                //  } 
                //  catch (Exception) 
                //  { 
                //     //Don't need call any rollback like method
                //  }
                //} 
                string s = update.QuestionValue;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int questionId = int.Parse(values[i]);
                        tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == questionId);

                        if (obj.OriDiaryFileName != null || obj.OriDiaryFilePath != null)
                        {
                            obj.TypistUserId = update.TypistUserId;
                            obj.IsAssignTypsitDate = DateTime.Now;
                            obj.QuestionStatus = (int)Questionstatus.QuestionAssignTypist;
                            qCtxDB.SaveChanges();
                            var result = SkipQuestionProcessSteps.SkippingQuesProcessingStep3Typist(questionId);
                        }
                    }
                }
                return "Question Assigned Successfully ! ";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }


        static object RoleBackQuestion(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;


            string s = update.QuestionValue;
            if (s != null && s != "")
            {
                string[] values = s.Split(',');

                for (int i = 0; i < values.Length; i++)
                {
                    int questionId = int.Parse(values[i]);
                    tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == questionId);
                    obj.TypistUserId = null;
                    qCtxDB.SaveChanges();
                }

            }




            return null;
        }

        public static object GetQuestionbyTypistId(object param)
        {
            AssignQuestionModel model = param as AssignQuestionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.TypistUserId == model.QuestionValue) && (questions.QuestionType == model.QuestionTypeId) && (questions.IsContentFreeze == null) && (questions.OriDiaryFileName != null) && (questions.OriDiaryFilePath != null)
                         where (questions.TypistUserId == model.QuestionValue) && (questions.QuestionType == model.QuestionTypeId) && (questions.OriDiaryFileName != null) && (questions.OriDiaryFilePath != null)
                         && questions.AssemblyID == model.AssemblyID
                         && questions.SessionID == model.SessionID

                         select new AssignQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             SubInCatchWord = questions.SubInCatchWord,
                             NoticeDate = questions.NoticeRecievedDate,
                             OriDiaryFileName = questions.OriDiaryFileName,
                             OriDiaryFilePath = questions.OriDiaryFilePath,
                             //Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }
        static object GetCountForUnStaredClubbed(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            model.TotalUnStaredReceivedCl = GetUnstaredCStaredQuestions(model).ResultCount;
            model.TotalUBractedQuestion = GetUnstaredClubbedQuestions(model).ResultCount;
            model.UnstaredTotalClubbedQuestion = GetTotalUnstarredClubbedQuestion(model).ResultCount;
            return model;
        }
        static tMemberNotice GetTotalUnstarredClubbedQuestion(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == 2) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID) && (questions.IsClubbed == true)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             Stats = questions.IsNoticeFreeze,
                             IsHindi = questions.IsHindi,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             MemberNameLocal = (from mch in pCtxt.mMembers where mch.MemberCode == questions.MemberID select mch.NameLocal).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestionModel = results;
            model.tQuestionModel = query;



            return model;

        }

        static tMemberNotice GetUnstaredCStaredQuestions(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.IsClubbed == null) && (questions.DeActivate == null) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionId) && (questions.BracketedWithDNo == null) && (questions.BracketedWithQNo != null) && (questions.QuestionType == 2) && (questions.IsProofReading != null) && (questions.IsProofReadingDate != null)
                         where (questions.QuestionType == 2) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID) && (questions.BracketedWithQNo != null) && (questions.MergeDiaryNo != null) && (questions.IsClubbed == null)

                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;


            return model;
        }
        static tMemberNotice GetUnstaredClubbedQuestions(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionLayingDate == null) && (questions.IsClubbed == null) && (questions.DeActivate == null) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID) && (questions.BracketedWithDNo != null) && (questions.BracketedWithQNo == null) && (questions.QuestionType == 2) && (questions.IsProofReading != null) && (questions.IsProofReadingDate != null)

                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;


            return model;
        }
        static object GetCountForClubbed(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            model.TotalStaredReceivedCl = GetCStaredQuestions(model).ResultCount;
            model.TotalSBracktedQuestion = GetClubbedQuestions(model).ResultCount;
            model.TotalInActiveQuestion = GetInactiveQuestion(model).ResultCount;
            model.TotalClubbedQuestion = GetTotalStarredClubbedQuestion(model).ResultCount;

            return model;
        }

        static tMemberNotice GetTotalStarredClubbedQuestion(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == 1) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID) && (questions.IsClubbed == true)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             Stats = questions.IsNoticeFreeze,
                             IsHindi = questions.IsHindi,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             MemberNameLocal = (from mch in pCtxt.mMembers where mch.MemberCode == questions.MemberID select mch.NameLocal).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestionModel = results;

            model.tQuestionModel = query;

            return model;

        }
        static tMemberNotice GetCStaredQuestions(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.IsClubbed == null) && (questions.DeActivate == null) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionId) && (questions.BracketedWithDNo == null) && (questions.BracketedWithQNo != null) && (questions.IsProofReading != null) && (questions.IsProofReadingDate != null)
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID) && (questions.BracketedWithQNo != null) && (questions.MergeDiaryNo != null) && (questions.IsClubbed == null)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;


            return model;
        }
        static tMemberNotice GetClubbedQuestions(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.IsClubbed == null) && (questions.DeActivate == null) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionId) && (questions.BracketedWithDNo != null) && (questions.BracketedWithQNo == null) && (questions.IsProofReading != null) && (questions.IsProofReadingDate != null)
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID) && (questions.IsBracket == true)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;


            return model;
        }
        static tMemberNotice GetInactiveQuestion(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.QuestionLayingDate == null) && (questions.DeActivate != null) && (questions.AssemblyID == model.AssemblyCode) && (questions.SessionID == model.SessionID)

                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;


            return model;
        }


        public static object GetQForClubWithBracket(object param)
        {
            ClubbedQuestionModel model = param as ClubbedQuestionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.BracketedWithQNo != null) && (questions.IsClubbed == null) && (questions.MergeDiaryNo != null)
                         //where (questions.QuestionType == model.QuestionTypeId) && (questions.QuestionLayingDate == null) && (questions.IsClubbed == null) && (questions.DeActivate == null) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.BracketedWithDNo == null) && (questions.BracketedWithQNo != null) && (questions.QuestionType == 1) && (questions.IsProofReading != null) && (questions.IsProofReadingDate != null)
                         //(questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.DepartmentID == model.DepartmentId) && (questions.DeActivate == null) && (questions.IsClubbed == null) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.IsInitialApproved == null) && (questions.IsInitialApprovedDate == null) && (questions.QuestionStatus == 3) && (questions.FixedNumber == null) && (questions.IsFixed == null) && (questions.IsFixedDate == null)


                         select new ClubbedQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             ClubbedDNo = questions.MergeDiaryNo,
                             //Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        public static object UnstaredGetQForClubWithBracket(object param)
        {
            ClubbedQuestionModel model = param as ClubbedQuestionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.QuestionLayingDate == null) && (questions.IsClubbed == null) && (questions.DeActivate == null) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.BracketedWithDNo == null) && (questions.BracketedWithQNo != null) && (questions.IsProofReading != null) && (questions.IsProofReadingDate != null)
                         //(questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.DepartmentID == model.DepartmentId) && (questions.DeActivate == null) && (questions.IsClubbed == null) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.IsInitialApproved == null) && (questions.IsInitialApprovedDate == null) && (questions.QuestionStatus == 3) && (questions.FixedNumber == null) && (questions.IsFixed == null) && (questions.IsFixedDate == null)


                         select new ClubbedQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             ClubbedDNo = questions.MergeDiaryNo,
                             //Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        static tMemberNotice GetQuestionDetailsForMerging(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query1 = (from questions in pCtxt.tQuestions
                          where (questions.QuestionID == model.QuestionID)
                          select new QuestionModelCustom
                          {
                              BrackWithQIds = questions.BracketedWithQNo,
                              MainDiaryNo = questions.DiaryNumber,
                              IsClubbed = questions.IsClubbed,
                          }).FirstOrDefault();

            model.MainDiaryNo = query1.MainDiaryNo;
            string[] values = query1.BrackWithQIds.Split(',');

            foreach (string val in values.Take(5))
            {
                int questValue = Convert.ToInt32(val);


                var query = (from questions in pCtxt.tQuestions
                             where (questions.QuestionID == questValue)
                             select new QuestionModelCustom
                             {
                                 QuestionID = questions.QuestionID,
                                 QuestionNumber = questions.QuestionNumber,
                                 Subject = questions.Subject,
                                 MemberId = questions.MemberID,
                                 MainQuestion = questions.MainQuestion,
                                 NoticeDate = questions.NoticeRecievedDate,
                                 EventId = questions.EventId,
                                 NoticeTime = questions.NoticeRecievedTime,
                                 IsContentFreeze = questions.IsContentFreeze,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 DiaryNumber = questions.DiaryNumber,
                                 TypistUserId = questions.TypistUserId,
                                 OriDiaryFileName = questions.OriDiaryFileName,
                                 OriDiaryFilePath = questions.OriDiaryFilePath,
                                 SubjectCatchWords = questions.SubInCatchWord,
                                 MinistryId = questions.MinistryId.Value,

                             }).SingleOrDefault();


                model.tQuestionModel.Add(query);
            }



            return model;
        }

        public static object

            SaveMergeEntry(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice NtceObj = (tMemberNotice)param;
            string msg = "";
            try
            {
                tQuestion QuesObj = qCtxDB.tQuestions.Single(m => m.QuestionID == NtceObj.QuestionID);
                QuesObj.Subject = NtceObj.Subject;
                QuesObj.MainQuestion = NtceObj.MainQuestion;
                QuesObj.IsClubbed = true;
                QuesObj.ReferencedNumber = NtceObj.QuesIds;
                QuesObj.ReferenceMemberCode = NtceObj.MemIds;
                QuesObj.IsContentFreeze = true;
                QuesObj.TypistUserId = NtceObj.TypistUserId;
                QuesObj.IsClubbedDate = DateTime.Now;
                // QuesObj.SubInCatchWord = NtceObj.SubInCatchWord;
                // QuesObj.NoticeRecievedDate = DateTime.Now;
                QuesObj.IsProofReading = true;
                QuesObj.IsProofReadingDate = DateTime.Now;
                QuesObj.QuestionStatus = (int)Questionstatus.QuestionFreeze;
                QuesObj.ProofReaderUserId = NtceObj.RoleName;
                QuesObj.MergeDiaryNo = NtceObj.Msg;

                // update Sequence of Member code according to MergeDiaryNo 
                int CurrentMC = QuesObj.MemberID;
                QuesObj.ReferenceMemberCode = "";
                QuesObj.ReferenceMemberCode = CurrentMC.ToString();

                string[] SDN = NtceObj.Msg.Split(',');

                for (int q = 0; q < SDN.Length; q++)
                {
                    string DN = SDN[q].ToString().Trim();


                    var MId = (from Q in qCtxDB.tQuestions
                               where Q.DiaryNumber == DN
                               select Q.MemberID).FirstOrDefault();
                    QuesObj.ReferenceMemberCode = QuesObj.ReferenceMemberCode + "," + MId;
                }


                qCtxDB.SaveChanges();

                if (QuesObj.QuestionType == (int)QuestionType.StartedQuestion)
                {
                    if (QuesObj.IsFinalApproved == true)
                    {
                        msg = "SF_" + QuesObj.DiaryNumber;
                    }
                    else
                    {
                        msg = "SU_" + QuesObj.DiaryNumber;
                    }

                }
                else if (QuesObj.QuestionType == (int)QuestionType.UnstaredQuestion)
                {
                    if (QuesObj.IsFinalApproved == true)
                    {
                        msg = "UF_" + QuesObj.DiaryNumber;
                    }
                    else
                    {
                        msg = "UU_" + QuesObj.DiaryNumber;
                    }
                }
            }



            catch (Exception ex)
            {
                msg = ex.Message;
            }


            return msg;
        }

        public static object CheckDiaryNumber(object param)
        {


            tMemberNotice DiaCtx = (tMemberNotice)param;
            NoticeContext pCtxt = new NoticeContext();

            var DiaryNumber = (from ques in pCtxt.tQuestions
                               where (ques.AssemblyID == DiaCtx.AssemblyID) && (ques.SessionID == DiaCtx.SessionID)
                               && (ques.QuestionType == DiaCtx.QuestionTypeId)
                               orderby ques.QuestionID descending
                               select ques.DiaryNumber).FirstOrDefault();

            if (DiaryNumber == null)
            {
                DiaryNumber = DiaCtx.AssemblyID + "/" + DiaCtx.SessionID + "/1";
            }
            else
            {
                int Num = Convert.ToInt16(DiaryNumber.Substring(DiaryNumber.LastIndexOf("/") + 1));
                Num = Num + 1;
                DiaryNumber = DiaCtx.AssemblyID + "/" + DiaCtx.SessionID + "/" + Num;
            }
            return DiaryNumber;
        }

        static object DeactiveMergeQuestion(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;
            string s = update.QuesIds;
            if (s != null && s != "")
            {
                string[] values = s.Split(',');

                for (int i = 0; i < values.Length; i++)
                {
                    int questionId = int.Parse(values[i]);
                    tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == questionId);
                    obj.DeActivateFlag = "c";
                    obj.DeActivate = true;
                    qCtxDB.SaveChanges();
                }

            }

            return null;
        }


        //New Code

        private static object GetNoticeReplyDraftSignList(object p)
        {

            tPaperLaidV data = (tPaperLaidV)p;

            // 5 and 6 ids are related to bills in Events.

            using (var dbcontext = new NoticeContext())
            {
                var paperLaidList = (from a in dbcontext.tPaperLaidVs
                                     join b in dbcontext.tPaperLaidTemps on a.PaperLaidId equals b.PaperLaidId
                                     join c in dbcontext.tMemberNotices on a.PaperLaidId equals c.PaperLaidId
                                     where a.AssemblyId == data.AssemblyId
                                         && a.SessionId == data.SessionId
                                         && (a.DeptActivePaperId == null || a.DeptActivePaperId == 0)
                                         && b.DeptSubmittedBy == null
                                         && b.DeptSubmittedDate == null
                                         && a.DepartmentId == data.DepartmentId
                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = a.MinistryName,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         NoticeId = c.NoticeId,
                                         NoticeNumber = c.NoticeNumber,
                                         MemberName = (from mc in dbcontext.mMembers where mc.MemberCode == c.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                     }).ToList();
                if (paperLaidList != null)
                {
                    data.ResultCount = paperLaidList.Count;
                    data.ListtPaperLaidV = paperLaidList.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        NoticeID = a.NoticeId,
                        NoticeNumber = a.NoticeNumber,
                        MemberName = a.MemberName,
                    }).Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();
                    return data;
                }

                return data;
            }


            //  throw new NotImplementedException();
        }

        public static object GetAssignListQuestions(object param)
        {
            AssignQuestionModel model = param as AssignQuestionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                             //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == model.QuestionValue) && (questions.IsContentFreeze == null) && (questions.OriDiaryFileName != null) && (questions.OriDiaryFilePath != null)
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.TypistUserId == model.QuestionValue) && (questions.OriDiaryFileName != null) && (questions.OriDiaryFilePath != null)
                         && questions.AssemblyID == model.AssemblyID
                         && questions.SessionID == model.SessionID
                         orderby questions.QuestionID descending
                         select new AssignQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             SubInCatchWord = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             OriDiaryFileName = questions.OriDiaryFileName,
                             OriDiaryFilePath = questions.OriDiaryFilePath,

                             //   Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }
        public static object GetClubbedQuestionList(object param)
        {
            ShowListModelCl model = param as ShowListModelCl;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.IsBracket == true)
                         //(questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.IsClubbed != null) && (questions.IsClubbedDate != null) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.BracketedWithDNo != null)

                         select new ShowListModelCl
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             BDiaryNo = questions.BracketedWithDNo,
                             //Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;


            return model;
        }
        public static object UnstaredGetBracketedQuestionList(object param)
        {
            ShowListModelCl model = param as ShowListModelCl;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.QuestionLayingDate == null) && (questions.IsClubbed == null) && (questions.DeActivate == null) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.BracketedWithDNo != null) && (questions.BracketedWithQNo == null) && (questions.IsProofReading != null) && (questions.IsProofReadingDate != null)
                         //(questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.IsClubbed != null) && (questions.IsClubbedDate != null) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.BracketedWithDNo != null)

                         select new ShowListModelCl
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             BDiaryNo = questions.BracketedWithDNo,
                             //Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;


            return model;
        }
        public static object GetInactiveQuestionList(object param)
        {
            ShowListModelCl model = param as ShowListModelCl;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.QuestionLayingDate == null) && (questions.DeActivate != null) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID)

                         select new ShowListModelCl
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             // Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;


            return model;
        }
        static tMemberNotice GetQDetails(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionID == model.QuestionID)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             ConstituencyName_Local = questions.ConstituencyNo,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsQuestionInPart = questions.IsQuestionInPart,
                             IsHindi = questions.IsHindi,
                             QuestionTypeId = questions.QuestionType,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }


        static tMemberNotice GetQDetailsByDiaryNo(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.DiaryNumber == model.DiaryNo && questions.QuestionType == 1)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }
        static tMemberNotice UnstaredGetQDetailsByDiaryNo(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.DiaryNumber == model.DiaryNo && questions.QuestionType == 2)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }
        static tMemberNotice GetQDetailsByAuditTrial(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.TQuestionAuditTrial
                         where (questions.MainQuesionId == model.QuestionID) && (questions.RecordBy == model.RecordBy)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             ConstituencyName = questions.ConstituencyName,
                             IsHindi = questions.IsHindi,
                             IsQuestionInPart = questions.IsQuestionInPart,
                             // Checked = questions.IsProofReading.HasValue,
                             //  IsTypistFreeze = questions.IsTypistFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }
        static object SaveFreezeTrialTable(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;
            TQuestionAuditTrial QuesObj = new TQuestionAuditTrial();
            NoticeContext pCtxt = new NoticeContext();
            // tMemberNotice NtceObj = (tMemberNotice)param;

            tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
            obj.MainQuestion = update.MainQuestion;
            obj.Subject = update.Subject;
            obj.IsContentFreeze = update.IsContentFreeze;
            obj.IsProofReading = update.IsProofReading;
            obj.IsProofReadingDate = update.IsProofReadingDate;
            obj.QuestionStatus = (int)Questionstatus.QuestionProofReading;
            obj.ProofReaderUserId = update.ProofReaderUserId;
            obj.BracketedWithDNo = update.BracketedWithDNo;
            obj.IsQuestionInPart = update.IsQuestionInPart;
            obj.IsHindi = update.IsHindi;
            obj.MinistryId = update.MinistryId;
            obj.DepartmentID = update.DepartmentID;
            if (update.RoleName == "Vidhan Sabha Proof Reader")
            {
                obj.MemberID = update.MemberID;
                obj.ConstituencyName = update.ConstituencyName;
                obj.ConstituencyNo = update.ConstituencyNo;
            }
            if (update.BracketedWithDNo != null)
            {
                obj.IsBracket = true;
            }

            if (update.BracketedWithDNo == null)
            {
                obj.IsBracket = null;
            }
            qCtxDB.SaveChanges();
            var result = SkipQuestionProcessSteps.SkippingQuesProcessingStep5LegiFreeze(update.QuestionID);
            string msg = "";

            try
            {
                QuesObj.MainQuesionId = update.QuestionID;
                QuesObj.MemberID = Convert.ToInt32(update.MemberID);
                QuesObj.Subject = update.Subject;
                QuesObj.MainQuestion = update.MainQuestion;
                QuesObj.DepartmentID = update.DepartmentID;
                QuesObj.EventId = update.EventId;
                QuesObj.MinistryId = update.MinistryId;
                QuesObj.IsProofReading = update.IsProofReading;
                QuesObj.IsProofReadingDate = update.IsProofReadingDate;
                QuesObj.RecordBy = "ProofReader";
                QuesObj.BracketedWithDNo = update.BracketedWithDNo;
                QuesObj.IsQuestionInPart = update.IsQuestionInPart;
                QuesObj.IsHindi = update.IsHindi;
                QuesObj.MinistryId = update.MinistryId;
                QuesObj.DepartmentID = update.DepartmentID;
                if (update.BracketedWithDNo != null)
                {
                    obj.IsBracket = true;
                }
                pCtxt.TQuestionAuditTrial.Add(QuesObj);
                pCtxt.SaveChanges();
                pCtxt.Close();
            }


            catch (Exception ex)
            {
                msg = ex.Message;
            }


            return msg;
        }
        static object GetUserforAssigned(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            AssignQuestionModel model = param as AssignQuestionModel;
            UserContext Ustxt = new UserContext();
            var query = (from User in Ustxt.mUsers
                         join UserRole in Ustxt.tUserRoles on User.UserId equals UserRole.UserID
                         join R in Ustxt.mRoles on UserRole.Roleid equals R.RoleId
                         // join E in Ustxt.mEmployee on new { UserName1 = User.UserName, DeptId1 = User.DeptId } equals new { UserName1 = E.empcd, DeptId1 = E.deptid }
                         where R.RoleName == model.RoleName
                         orderby User.UserName ascending
                         // orderby E.empfname ascending
                         // && (from tquest in Ustxt.tQuestions where User.UserId == new Guid(tquest.TypistUserId) select tquest.TypistUserId).Contains(User.UserId.ToString())

                         select new AssignQuestionModel
                         {
                             // TypsitUserName = E.empfname + " " + E.empmname + " " + E.emplname,
                             TypsitUserName = User.UserName,
                             TypistUserId = User.UserId,
                             // Count = (from tquest in Ustxt.tQuestions where tquest.TypistUserId== User.UserId  .Count() 

                         }).ToList();
            List<AssignQuestionModel> returnToCaller = new List<AssignQuestionModel>();



            foreach (var val in query)
            {

                string Id = val.TypistUserId.ToString();
                model.TypistUserId = val.TypistUserId;
                var item = (from questions in Ustxt.tQuestions
                                //where questions.TypistUserId == Id && questions.IsContentFreeze == null && questions.QuestionType == 1
                            where questions.TypistUserId == Id && questions.QuestionType == 1
                            && questions.AssemblyID == model.AssemblyID
                            && questions.SessionID == model.SessionID
                            select new AssignQuestionModel
                            {
                                QuestionID = questions.QuestionID,
                                DiaryNumber = questions.DiaryNumber,
                                Subject = questions.Subject,
                                NoticeDate = questions.NoticeRecievedDate,


                            }).ToList().Count();
                model.Count = item;
                if (item != 0)
                {

                    AssignQuestionModel tempModel = new AssignQuestionModel();
                    tempModel = val;
                    tempModel.TypsitUserName = val.TypsitUserName + " (" + model.Count + ")";
                    tempModel.TypistUserId = model.TypistUserId;
                    returnToCaller.Add(tempModel);
                    //model.tMemberNoticeModel = returnToCaller;
                }



            }

            //add UnassignQuestion with Count

            AssignQuestionModel AQM = new AssignQuestionModel();
            AQM.AssemblyID = model.AssemblyID;
            AQM.SessionID = model.SessionID;
            AQM = (AssignQuestionModel)GetPendingQuestionsForAssign(AQM);
            AQM.TypistUserId = new Guid();
            AQM.TypsitUserName = "Unassigned Questions (" + AQM.ResultCount + ")";
            returnToCaller.Add(AQM);
            model.tMemberNoticeModel = returnToCaller;

            // var results = query.ToList();


            return model;
        }
        static object GetUserforUnstaredAssigned(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            AssignQuestionModel model = param as AssignQuestionModel;
            UserContext Ustxt = new UserContext();
            var query = (from User in Ustxt.mUsers
                         join UserRole in Ustxt.tUserRoles on User.UserId equals UserRole.UserID
                         join R in Ustxt.mRoles on UserRole.Roleid equals R.RoleId
                         where R.RoleName == model.RoleName
                         orderby User.UserName ascending

                         select new AssignQuestionModel
                         {

                             TypsitUserName = User.UserName,
                             TypistUserId = User.UserId,

                         }).ToList();
            //var query = (from User in Ustxt.mUsers
            //             join UserRole in Ustxt.tUserRoles on User.UserId equals UserRole.UserID
            //             join R in Ustxt.mRoles on UserRole.Roleid equals R.RoleId
            //             join E in Ustxt.mEmployee on new { UserName1 = User.UserName, DeptId1 = User.DeptId } equals new { UserName1 = E.empcd, DeptId1 = E.deptid }
            //             where R.RoleName == model.RoleName
            //             // && (from tquest in Ustxt.tQuestions where User.UserId == new Guid(tquest.TypistUserId) select tquest.TypistUserId).Contains(User.UserId.ToString())

            //             select new AssignQuestionModel
            //             {
            //                 TypsitUserName = E.empfname + " " + E.empmname + " " + E.emplname,
            //                 TypistUserId = User.UserId,
            //                 // Count = (from tquest in Ustxt.tQuestions where tquest.TypistUserId== User.UserId  .Count() 

            //             }).ToList();
            List<AssignQuestionModel> returnToCaller = new List<AssignQuestionModel>();
            foreach (var val in query)
            {

                string Id = val.TypistUserId.ToString();
                model.TypistUserId = val.TypistUserId;
                var item = (from questions in Ustxt.tQuestions
                                //where questions.TypistUserId == Id && questions.IsContentFreeze == null && questions.QuestionType == 2
                            where questions.TypistUserId == Id && questions.QuestionType == 2
                            && questions.AssemblyID == model.AssemblyID
                            && questions.SessionID == model.SessionID
                            select new AssignQuestionModel
                            {
                                QuestionID = questions.QuestionID,
                                DiaryNumber = questions.DiaryNumber,
                                Subject = questions.Subject,
                                NoticeDate = questions.NoticeRecievedDate,
                                OriDiaryFileName = questions.OriDiaryFileName,
                                OriDiaryFilePath = questions.OriDiaryFilePath,

                            }).ToList().Count();
                model.Count = item;
                if (item != 0)
                {

                    AssignQuestionModel tempModel = new AssignQuestionModel();
                    tempModel = val;
                    tempModel.TypsitUserName = val.TypsitUserName + " (" + model.Count + ")";
                    tempModel.TypistUserId = model.TypistUserId;
                    returnToCaller.Add(tempModel);
                    model.tMemberNoticeModel = returnToCaller;
                }

            }

            //add UnassignQuestion with Count

            AssignQuestionModel AQM = new AssignQuestionModel();
            AQM.AssemblyID = model.AssemblyID;
            AQM.SessionID = model.SessionID;
            AQM = (AssignQuestionModel)GetUnstartForAssingList(AQM);
            AQM.TypistUserId = new Guid();
            AQM.TypsitUserName = "Unassigned Questions (" + AQM.ResultCount + ")";
            returnToCaller.Add(AQM);
            model.tMemberNoticeModel = returnToCaller;

            // var results = query.ToList();


            return model;
        }
        static object StayUpdateQuestions(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;
            tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
            obj.MainQuestion = update.MainQuestion;
            obj.Subject = update.Subject;
            obj.MinistryId = update.MinistryId;
            obj.DepartmentID = update.DepartmentID;
            if (update.RoleName == "Vidhan Sabha Proof Reader")
            {
                obj.MemberID = update.MemberID;
                obj.ConstituencyName = update.ConstituencyName;
                obj.ConstituencyNo = update.ConstituencyNo;
            }
            obj.BracketedWithDNo = update.BracketedWithDNo;
            if (update.BracketedWithDNo != null)
            {
                obj.IsBracket = true;
            }

            if (update.BracketedWithDNo == null)
            {
                obj.IsBracket = null;
            }
            qCtxDB.SaveChanges();
            return null;
        }

        static tMemberNotice GetDetailsForClubbing(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionID == model.QuestionID) && (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             RefMemCode = questions.ReferenceMemberCode,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MergeQuestion = questions.ReferencedNumber,
                         }).ToList();



            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }
        static object CheckDQuestionNo(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel updateSQ = param as tQuestionModel;

            String Exist = "Ok";
            var query = (from tQ in qCtxDB.tQuestions
                         where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 1) && (tQ.DepartmentID == updateSQ.DepartmentId)
                         select tQ).ToList().Count();
            if (query == 1)
            {
                var query3 = (from tQ in qCtxDB.tQuestions
                              where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 1) && (tQ.DepartmentID == updateSQ.DepartmentId) && (tQ.BracketedWithDNo == null)
                              select tQ).ToList().Count();

                if (query3 == 0)
                {

                    var query4 = (from tQ in qCtxDB.tQuestions
                                  where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 1) && (tQ.DepartmentID == updateSQ.DepartmentId) && (tQ.BracketedWithDNo != null)
                                  select tQ.BracketedWithDNo).FirstOrDefault();
                    return query4;
                }
            }
            if (query == 0)
            {
                string DiaryExist = "DeptNotSame";
                var query1 = (from tQ in qCtxDB.tQuestions
                              where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 1)
                              select tQ).ToList().Count();

                if (query1 == 0)
                {
                    String DeptExist = "DiaryNotExist";
                    var query2 = (from tQ in qCtxDB.tQuestions
                                  where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 1) && (tQ.DepartmentID == updateSQ.DepartmentId)
                                  select tQ).ToList().Count();

                    return DeptExist.ToString();
                }
                return DiaryExist.ToString();
            }

            return Exist.ToString();
        }
        static object UnstaredCheckDQuestionNo(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel updateSQ = param as tQuestionModel;

            String Exist = "Ok";
            var query = (from tQ in qCtxDB.tQuestions
                         where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 2) && (tQ.DepartmentID == updateSQ.DepartmentId)
                         select tQ).ToList().Count();
            if (query == 1)
            {
                var query3 = (from tQ in qCtxDB.tQuestions
                              where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 2) && (tQ.DepartmentID == updateSQ.DepartmentId) && (tQ.BracketedWithDNo == null)
                              select tQ).ToList().Count();

                if (query3 == 0)
                {

                    var query4 = (from tQ in qCtxDB.tQuestions
                                  where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 2) && (tQ.DepartmentID == updateSQ.DepartmentId) && (tQ.BracketedWithDNo != null)
                                  select tQ.BracketedWithDNo).FirstOrDefault();
                    return query4;
                }
            }
            if (query == 0)
            {
                string DiaryExist = "DeptNotSame";
                var query1 = (from tQ in qCtxDB.tQuestions
                              where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 2)
                              select tQ).ToList().Count();

                if (query1 == 0)
                {
                    String DeptExist = "DiaryNotExist";
                    var query2 = (from tQ in qCtxDB.tQuestions
                                  where (tQ.DiaryNumber == updateSQ.DiaryNumber) && (tQ.QuestionType == 2) && (tQ.DepartmentID == updateSQ.DepartmentId)
                                  select tQ).ToList().Count();

                    return DeptExist.ToString();
                }
                return DiaryExist.ToString();
            }

            return Exist.ToString();
        }
        static object GetDepartment(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;

            var query = (from ministryDept in pCtxt.mDepartments
                         join mSecDept in pCtxt.SecDept on ministryDept.deptId equals mSecDept.DepartmentID
                         orderby ministryDept.deptname ascending

                         select new QuestionModelCustom
                         {
                             DepartmentId = ministryDept.deptId,
                             DepartmentName = ministryDept.deptname,
                             SfilePath = ministryDept.StarredBeforeFixPath,
                         }).ToList();


            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;

        }
        static object GetDepartmentById(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;

            var query = (from ministryDept in pCtxt.mDepartments
                         join mSecDept in pCtxt.SecDept on ministryDept.deptId equals mSecDept.DepartmentID
                         where ministryDept.deptId == model.DepartmentId
                         orderby ministryDept.deptname ascending

                         select new QuestionModelCustom
                         {
                             DepartmentId = ministryDept.deptId,
                             DepartmentName = ministryDept.deptname,
                             SfilePath = ministryDept.StarredBeforeFixPath,
                         }).ToList();


            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;

        }
        static tMemberNotice GetAllDetailsForDeptwise(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 1 && questions.DepartmentID == model.DepartmentId && questions.QuestionStatus == 3 && questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,

                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),
                         }).ToList();


            foreach (var val in query)
            {
                if (val.IsClubbed != null)
                {
                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tQuestions
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                            ConstituencyName = (from t in pCtxt.mConstituency
                                                                join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                select t.ConstituencyName).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemName))
                            {
                                val.CMemName = item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                            else
                            {
                                val.CMemName = val.CMemName + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                            }


                        }
                    }

                    //if (val.IsHindi == true)
                    //{
                    //    model.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault();
                    //}
                }
                if (val.IsHindi == true)
                {

                    val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                    val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                                                  join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                  where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                  select t.ConstituencyName_Local).FirstOrDefault();
                    val.MinistryNameLocal = (from M in pCtxt.mMinistry
                                             where M.MinistryID == val.MinisterID
                                             select M.MinistryNameLocal).FirstOrDefault();
                    if (val.IsClubbed != null)
                    {
                        string s = val.RefMemCode;
                        if (s != null && s != "")
                        {
                            string[] values = s.Split(',');

                            for (int i = 0; i < values.Length; i++)
                            {
                                int MemId = int.Parse(values[i]);
                                var item = (from questions in pCtxt.tQuestions
                                            select new QuestionModelCustom
                                            {
                                                MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.NameLocal)).FirstOrDefault(),
                                                ConstituencyName = (from t in pCtxt.mConstituency
                                                                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                    select t.ConstituencyName_Local).FirstOrDefault(),
                                            }).FirstOrDefault();

                                if (string.IsNullOrEmpty(val.CMemNameHindi))
                                {
                                    val.CMemNameHindi = item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                                else
                                {
                                    val.CMemNameHindi = val.CMemNameHindi + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                                }


                            }
                        }
                    }
                }
            }

            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        static tMemberNotice GetDataByQuestionId(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from questions in pCtxt.tQuestions
                         where questions.QuestionID == model.QuestionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             AssemblyCode = questions.AssemblyID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,
                             DesireLayingDate = questions.IsFixedDate,
                             DepartmentName = (from dept in pCtxt.mDepartments where dept.deptId == questions.DepartmentID select dept.deptname).FirstOrDefault(),
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),


                         }).ToList();


            foreach (var val in query)
            {
                //add Asseembly ID for further Query by sunil on 28-02-2015 which was not added  
                model.AssemblyID = val.AssemblyCode;

                if (val.IsClubbed != null)
                {

                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');



                        int Cnt;
                        if (values.Length > 5)
                        {
                            Cnt = 5;
                        }
                        else
                        {
                            Cnt = values.Length;
                        }


                        for (int i = 0; i < Cnt; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tQuestions
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                            ConstituencyName = (from t in pCtxt.mConstituency
                                                                join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                select t.ConstituencyName).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemName))
                            {
                                val.CMemName = item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                            else
                            {
                                val.CMemName = val.CMemName + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                            }


                        }
                    }

                }
                if (val.IsHindi == true)
                {

                    val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                    val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                                                  join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                  where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                  select t.ConstituencyName_Local).FirstOrDefault();
                    val.MinistryNameLocal = (from M in pCtxt.mMinistry
                                             where M.MinistryID == val.MinisterID
                                             select M.MinistryNameLocal).FirstOrDefault();
                    if (val.IsClubbed != null)
                    {
                        string s = val.RefMemCode;
                        if (s != null && s != "")
                        {
                            string[] values = s.Split(',');

                            int Cnt;
                            if (values.Length > 5)
                            {
                                Cnt = 5;
                            }
                            else
                            {
                                Cnt = values.Length;
                            }



                            for (int i = 0; i < Cnt; i++)
                            {
                                int MemId = int.Parse(values[i]);
                                var item = (from questions in pCtxt.tQuestions
                                            select new QuestionModelCustom
                                            {
                                                MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.NameLocal)).FirstOrDefault(),
                                                ConstituencyName = (from t in pCtxt.mConstituency
                                                                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                    select t.ConstituencyName_Local).FirstOrDefault(),
                                            }).FirstOrDefault();

                                if (string.IsNullOrEmpty(val.CMemNameHindi))
                                {
                                    val.CMemNameHindi = item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                                else
                                {
                                    val.CMemNameHindi = val.CMemNameHindi + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                                }


                            }
                        }
                    }
                }
            }

            // get Assembly Name and Session Name by Assembly Id and Session Id This is added on 12/01/2015 by Sunil

            var ASID = (from Q in pCtxt.tQuestions
                        where Q.QuestionID == model.QuestionID
                        select new { Q.AssemblyID, Q.SessionID }).FirstOrDefault();

            // For Storing File 
            var FL = (from SS in pCtxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();


            //var AssemblyName = (from A in pCtxt.mAssemblies
            //                    where A.AssemblyCode == ASID.AssemblyID
            //                    select A.AssemblyName).FirstOrDefault();
            //var Session = (from S in pCtxt.mSessions
            //               where S.AssemblyID == ASID.AssemblyID
            //               && S.SessionCode == ASID.SessionID
            //               select new { S.SessionName, S.SessionNameLocal }).FirstOrDefault();


            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;
            model.AssemblyID = ASID.AssemblyID;
            model.SessionID = ASID.SessionID;

            model.FilePath = FL;

            return model;
        }
        static object UpdatemDepartment(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            mDepartment obj = pCtxt.mDepartments.Single(m => m.deptId == model.DepartmentId);
            obj.StarredBeforeFixPath = model.FilePath;
            obj.SLastUpdatedDate = model.Updatedate;
            pCtxt.SaveChanges();
            string msg = "";

            return msg;
        }
        static object GetDepartmentByValue(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;

            var query = (from ministryDept in pCtxt.mDepartments
                         orderby ministryDept.deptname ascending
                         where !ministryDept.deptname.ToUpper().Contains(">") && ministryDept.StarredBeforeFixPath != null

                         select new QuestionModelCustom
                         {
                             DepartmentId = ministryDept.deptId,
                             DepartmentName = ministryDept.deptname,
                             SfilePath = ministryDept.StarredBeforeFixPath,
                             Updatedate = ministryDept.SLastUpdatedDate
                         }).ToList();

            foreach (var val in query)
            {
                tMemberNotice tEMPmodel = param as tMemberNotice;

                if (val.Updatedate != null)
                {
                    tEMPmodel.Updatedate = val.Updatedate;
                }

            }
            int totalRecords = query.Count();
            var results = query.ToList();

            model.TotalQCount = totalRecords;

            model.tQuestionModel = results;

            return model;

        }

        static object GetValuemDepartmentPdf(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;

            var query = (from ministryDept in pCtxt.mDepartmentPdfPath
                         orderby ministryDept.deptname ascending
                         where ministryDept.AssemblyID == model.AssemblyID && ministryDept.SessionID == model.SessionID && ministryDept.StarredBeforeFixPath != null

                         select new QuestionModelCustom
                         {
                             DepartmentId = ministryDept.deptId,
                             DepartmentName = ministryDept.deptname,
                             SfilePath = ministryDept.StarredBeforeFixPath,
                             Updatedate = ministryDept.SLastUpdatedDate
                         }).ToList();

            foreach (var val in query)
            {
                tMemberNotice tEMPmodel = param as tMemberNotice;

                if (val.Updatedate != null)
                {
                    tEMPmodel.Updatedate = val.Updatedate;
                }

            }
            int totalRecords = query.Count();
            var results = query.ToList();

            model.TotalQCount = totalRecords;

            model.tQuestionModel = results;

            return model;

        }



        static object GetDepartmentByValueUnstared(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;

            var query = (from ministryDept in pCtxt.mDepartmentPdfPath
                         orderby ministryDept.deptname ascending
                         where ministryDept.AssemblyID == model.AssemblyID && ministryDept.SessionID == model.SessionID && ministryDept.UnStarredBeforeFixPath != null
                         select new QuestionModelCustom
                         {
                             DepartmentId = ministryDept.deptId,
                             DepartmentName = ministryDept.deptname,
                             SfilePath = ministryDept.UnStarredBeforeFixPath,
                             Updatedate = ministryDept.UnSLastUpdatedDate
                         }).ToList();

            foreach (var val in query)
            {
                tMemberNotice tEMPmodel = param as tMemberNotice;

                if (val.Updatedate != null)
                {
                    tEMPmodel.Updatedate = val.Updatedate;
                }

            }
            int totalRecords = query.Count();
            var results = query.ToList();

            model.TotalQCount = totalRecords;

            model.tQuestionModel = results;

            return model;

        }
        static tMemberNotice UnstarredGetAllDetailsForDeptwise(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 2 && questions.DepartmentID == model.DepartmentId && questions.QuestionStatus == 3 && questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,

                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),
                         }).ToList();


            foreach (var val in query)
            {
                if (val.IsClubbed != null)
                {
                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tQuestions
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                            ConstituencyName = (from t in pCtxt.mConstituency
                                                                join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                select t.ConstituencyName).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemName))
                            {
                                val.CMemName = item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                            else
                            {
                                val.CMemName = val.CMemName + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                            }


                        }
                    }

                    //if (val.IsHindi == true)
                    //{
                    //    model.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault();
                    //}
                }
                if (val.IsHindi == true)
                {

                    val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                    val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                                                  join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                  where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                  select t.ConstituencyName_Local).FirstOrDefault();
                    val.MinistryNameLocal = (from M in pCtxt.mMinistry
                                             where M.MinistryID == val.MinisterID
                                             select M.MinistryNameLocal).FirstOrDefault();
                    if (val.IsClubbed != null)
                    {
                        string s = val.RefMemCode;
                        if (s != null && s != "")
                        {
                            string[] values = s.Split(',');

                            for (int i = 0; i < values.Length; i++)
                            {
                                int MemId = int.Parse(values[i]);
                                var item = (from questions in pCtxt.tQuestions
                                            select new QuestionModelCustom
                                            {
                                                MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.NameLocal)).FirstOrDefault(),
                                                ConstituencyName = (from t in pCtxt.mConstituency
                                                                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                    select t.ConstituencyName_Local).FirstOrDefault(),
                                            }).FirstOrDefault();

                                if (string.IsNullOrEmpty(val.CMemNameHindi))
                                {
                                    val.CMemNameHindi = item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                                else
                                {
                                    val.CMemNameHindi = val.CMemNameHindi + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                                }


                            }
                        }
                    }
                }
            }

            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        static object UpdatemDepartmentUnstarred(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            mDepartment obj = pCtxt.mDepartments.Single(m => m.deptId == model.DepartmentId);
            obj.UnStarredBeforeFixPath = model.FilePath;
            obj.UnSLastUpdatedDate = model.Updatedate;
            pCtxt.SaveChanges();
            string msg = "";

            return msg;
        }

        static object GetDepartmentForNotices(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;

            var query = (from ministryDept in pCtxt.mDepartments
                         orderby ministryDept.deptname ascending
                         where !ministryDept.deptname.ToUpper().Contains(">") && ministryDept.NoticeBeforeFixPath != null
                         select new QuestionModelCustom
                         {
                             DepartmentId = ministryDept.deptId,
                             DepartmentName = ministryDept.deptname,
                             SfilePath = ministryDept.NoticeBeforeFixPath,
                             Updatedate = ministryDept.NoticeBFLUpdatedDate
                         }).ToList();

            foreach (var val in query)
            {
                tMemberNotice tEMPmodel = param as tMemberNotice;

                if (val.Updatedate != null)
                {
                    tEMPmodel.Updatedate = val.Updatedate;
                }

            }
            int totalRecords = query.Count();
            var results = query.ToList();

            model.TotalQCount = totalRecords;

            model.tQuestionModel = results;

            return model;

        }

        static tMemberNotice GetAllDetailsForNotices(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from Notices in pCtxt.tMemberNotices
                         where Notices.DepartmentId == model.DepartmentId && Notices.IsSubmitted == true && Notices.SubmittedDate != null
                         select new QuestionModelCustom
                         {
                             NoticeNumber = Notices.NoticeNumber,
                             NoticeId = Notices.NoticeTypeID,
                             //MinistryId=Notices.MinisterId,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notices.MemberId select mc.NameLocal).FirstOrDefault(),
                             //MinistryName = (from M in pCtxt.mMinistry
                             //                where M.MinistryID == Notices.MinisterId
                             //                select M.MinistryName).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == Notices.MinistryId
                                             select M.MinistryNameLocal).FirstOrDefault(),
                             MinisterName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == Notices.MinistryId
                                             select M.MinisterNameLocal).FirstOrDefault(),

                         }).ToList();
            foreach (var val in query)
            {
                if (val.NoticeId != null)
                {

                    val.EventName = (from e in pCtxt.mEvents
                                     where e.EventId == val.NoticeId
                                     select e.RuleNo).FirstOrDefault();


                }
            }
            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        static object UpdatemDepartmentFornotices(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            mDepartment obj = pCtxt.mDepartments.Single(m => m.deptId == model.DepartmentId);
            obj.NoticeBeforeFixPath = model.FilePath;
            obj.NoticeBFLUpdatedDate = model.Updatedate;
            pCtxt.SaveChanges();
            string msg = "";

            return msg;
        }
        static object GetNoticesForView(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            var query = (from Notice in pCtxt.tMemberNotices
                         where Notice.NoticePdfPath != null && Notice.IsSubmitted == true && Notice.SubmittedDate != null
                         select new QuestionModelCustom
                         {
                             NoticeNumber = Notice.NoticeNumber,
                             NoticeId = Notice.NoticeId,
                             SfilePath = Notice.NoticePdfPath,
                             EventId = Notice.NoticeTypeID

                         }).ToList();
            foreach (var val in query)
            {
                if (val.EventId != null)
                {

                    val.EventName = (from e in pCtxt.mEvents
                                     where e.EventId == val.EventId
                                     select e.RuleNo).FirstOrDefault();


                }
            }
            foreach (var val in query)
            {
                tMemberNotice tEMPmodel = param as tMemberNotice;

                if (val.Updatedate != null)
                {
                    tEMPmodel.Updatedate = val.Updatedate;
                }

            }
            int totalRecords = query.Count();
            var results = query.ToList();

            model.TotalQCount = totalRecords;

            model.tQuestionModel = results;

            return model;

        }
        static object GetNoticesForGeneratedPdf(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();

            var query = (from Notice in pCtxt.tMemberNotices
                         where Notice.NoticePdfPath == null && Notice.IsSubmitted == true && Notice.SubmittedDate != null
                         select new QuestionModelCustom
                         {
                             NoticeNumber = Notice.NoticeNumber,
                             NoticeId = Notice.NoticeId
                         }).ToList();

            foreach (var val in query)
            {
                tMemberNotice tEMPmodel = param as tMemberNotice;

                if (val.Updatedate != null)
                {
                    tEMPmodel.Updatedate = val.Updatedate;
                }

            }
            int totalRecords = query.Count();
            var results = query.ToList();

            model.TotalQCount = totalRecords;

            model.tQuestionModel = results;

            return model;

        }
        static tMemberNotice GetDetailsForNoticesPDF(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from Notices in pCtxt.tMemberNotices
                         where Notices.NoticeId == model.NoticeId
                         select new QuestionModelCustom
                         {
                             NoticeNumber = Notices.NoticeNumber,
                             NoticeId = Notices.NoticeTypeID,
                             SfilePath = Notices.NoticePdfPath,
                             //MinistryId=Notices.MinisterId,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notices.MemberId select mc.NameLocal).FirstOrDefault(),
                             //MinistryName = (from M in pCtxt.mMinistry
                             //                where M.MinistryID == Notices.MinisterId
                             //                select M.MinistryName).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == Notices.MinistryId
                                             select M.MinistryNameLocal).FirstOrDefault(),
                             MinisterName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == Notices.MinistryId
                                             select M.MinisterNameLocal).FirstOrDefault(),

                         }).ToList();
            foreach (var val in query)
            {
                if (val.NoticeId != null)
                {

                    val.EventName = (from e in pCtxt.mEvents
                                     where e.EventId == val.NoticeId
                                     select e.RuleNo).FirstOrDefault();


                }
            }
            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        static object UpdateTmemberFornotices(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice obj = pCtxt.tMemberNotices.Single(m => m.NoticeId == model.NoticeId);
            obj.NoticePdfPath = model.FilePath;
            pCtxt.SaveChanges();
            string msg = "";

            return msg;
        }
        //changer module
        static object GetCountForChanger(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.SubInCatchWord != null) && (questions.MainQuestion != null)
                        && (questions.AssemblyID == model.AssemblyID)
                        && (questions.SessionID == model.SessionID)
                         select new TypistQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.TotalStaredReceived = model.ResultCount;

            return model;
        }
        static object GetCountUnstartchanger(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();


            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.SubInCatchWord != null) && (questions.MainQuestion != null)
                          && (questions.AssemblyID == model.AssemblyID)
                        && (questions.SessionID == model.SessionID)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;
            model.TotalUnStaredReceived = model.ResultCount;

            return model;
        }
        public static object GetStarredQuestions(object param)
        {

            TypistQuestionModel model = param as TypistQuestionModel;
            //  string PUID = model.UId;
            NoticeContext pCtxt = new NoticeContext();


            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.SubInCatchWord != null) && (questions.MainQuestion != null)
                         && (questions.AssemblyID == model.AssemblyID)
                         && (questions.SessionID == model.SessionID)
                         select new TypistQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             ProofReaderName = questions.ProofReaderUserId,

                         }).ToList();

            List<TypistQuestionModel> returnToCaller = new List<TypistQuestionModel>();


            foreach (var val in query)
            {
                TypistQuestionModel tempModel = new TypistQuestionModel();
                tempModel = val;
                if (val.ProofReaderName != null)
                {
                    tempModel.UserName = getName(val.ProofReaderName);
                }
                returnToCaller.Add(tempModel);
            }

            //int totalRecords = query.Count();
            //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = query.Count();
            model.tQuestionModel = returnToCaller;



            return model;
        }
        static tMemberNotice GetStrarredQDetailsChanger(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionID == model.QuestionID)
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsHindi = questions.IsHindi,
                             IsQuestionInPart = questions.IsQuestionInPart,
                             Questionsequence = questions.QuestionSequence,
                             IsLockedTranslator = questions.IsLocktranslator,
                             IsClubbed = questions.IsClubbed,
                             ReferenceMemberCode = questions.ReferenceMemberCode,
                             QuestionTypeId = questions.QuestionType,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            model.NewmemberList = (from M in pCtxt.mMembers
                                   join MA in pCtxt.mMemberAssembly on M.MemberCode equals MA.MemberID
                                   where (MA.AssemblyID == model.AssemblyID)
                                   && !(from ExcepMinis in pCtxt.mMinistry where ExcepMinis.AssemblyID == model.AssemblyID select ExcepMinis.MemberCode).Contains(M.MemberCode)
                                   && M.Active == true
                                   orderby M.Name
                                   select new DiaryModel
                                   {
                                       MemberId = MA.MemberID,
                                       MemberName = M.Name,
                                   }).ToList();
            var MemCodes = (from e in pCtxt.tQuestions where e.QuestionID == model.QuestionID select e.ReferenceMemberCode).FirstOrDefault();
            if (MemCodes != null)
            {
                string[] values = MemCodes.Split(',').Distinct().ToArray();

                for (int i = 0; i < values.Length; i++)
                {
                    int MemId = int.Parse(values[i]);
                    var item = (from MemberName in pCtxt.mMembers where MemberName.MemberCode == MemId select (MemberName.Prefix + " " + MemberName.Name)).FirstOrDefault();

                    if (string.IsNullOrEmpty(model.MemberName))
                    {
                        model.MemberName = item;
                    }
                    else
                    {
                        model.MemberName = model.MemberName + "," + item;
                    }

                }

            }
            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;
            return model;
        }

        static object SaveChangerTQuestions(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;
            TQuestionAuditTrial QuesObj = new TQuestionAuditTrial();
            NoticeContext pCtxt = new NoticeContext();
            tQuestion obj = new tQuestion();
            try
            {
                obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
                obj.MainQuestion = update.MainQuestion;
                obj.Subject = update.Subject;
                obj.IsQuestionInPart = update.IsQuestionInPart;
                obj.IsHindi = update.IsHindi;
                obj.ConstituencyName = update.ConstituencyName;
                obj.QuestionSequence = update.QuestionSequence;
                obj.QuestionType = update.QuestionType;
                if (update.ReferenceMemberCode != null)
                {
                    obj.ReferenceMemberCode = update.ReferenceMemberCode;
                }
                obj.QuestionSequence = update.QuestionSequence;
                qCtxDB.SaveChanges();

                QuesObj.MainQuesionId = update.QuestionID;
                QuesObj.MainQuestion = update.MainQuestion;
                QuesObj.Subject = update.Subject;
                QuesObj.RecordBy = update.userId;
                var aadharid = (from mc in pCtxt.mUsers where mc.UserId == update.UserGuId select mc.AadarId).FirstOrDefault();
                QuesObj.ChangedBy = (from mc in pCtxt.AdhaarDetails where mc.AdhaarID == aadharid select mc.Name).FirstOrDefault();
                QuesObj.ConstituencyName = update.ConstituencyName;
                pCtxt.TQuestionAuditTrial.Add(QuesObj);
                pCtxt.SaveChanges();
                pCtxt.Close();

                if (obj.QuestionType == (int)QuestionType.StartedQuestion)
                {
                    if (obj.IsFinalApproved == true)
                    {
                        obj.Message = "SF_Successfully Updated !";
                    }
                    else
                    {
                        obj.Message = "SU_Successfully Updated !";
                    }
                }
                else if (obj.QuestionType == (int)QuestionType.UnstaredQuestion)
                {
                    if (obj.IsFinalApproved == true)
                    {
                        obj.Message = "UF_Successfully Updated !";
                    }
                    else
                    {
                        obj.Message = "UU_Successfully Updated !";
                    }
                }
                qCtxDB.Close();
            }
            catch (Exception ex)
            {
                obj.Message = ex.Message;
            }
            return obj.Message;
        }
        public static object GetUnStarredQuestions(object param)
        {
            TypistQuestionModel model = param as TypistQuestionModel;
            //  string PUID = model.UId;
            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionType == model.QuestionTypeId) && (questions.SubInCatchWord != null) && (questions.MainQuestion != null)
                         && (questions.AssemblyID == model.AssemblyID)
                         && (questions.SessionID == model.SessionID)
                         select new TypistQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             ProofReaderName = questions.ProofReaderUserId,

                         }).ToList();

            List<TypistQuestionModel> returnToCaller = new List<TypistQuestionModel>();


            foreach (var val in query)
            {
                TypistQuestionModel tempModel = new TypistQuestionModel();
                tempModel = val;
                if (val.ProofReaderName != null)
                {
                    tempModel.UserName = getName(val.ProofReaderName);
                }
                returnToCaller.Add(tempModel);
            }
            //int totalRecords = query.Count();
            //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = query.Count();
            model.tQuestionModel = returnToCaller;
            return model;
        }

        static tMemberNotice UnstaredGetAllDetailsForSpdf(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query = new List<QuestionModelCustom>();

            if (model.DataStatus == "CQ")
            {
                query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 2 && questions.IsFixedDate != null && questions.QuestionNumber != null && questions.IsFixedDate == model.NoticeDate && questions.DeActivateFlag == null
                         orderby questions.QuestionSequence, questions.QuestionNumber ascending
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,
                             IsQuestionInPart = questions.IsQuestionInPart,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             SessionDateLocal = (from Session in pCtxt.mSessionDates
                                                 where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                                 select Session.SessionDateLocal).FirstOrDefault(),
                             SessionDateId = (from Session in pCtxt.mSessionDates
                                              where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                              select Session.Id).FirstOrDefault(),

                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),

                             SessionDateEnglish = (from Session in pCtxt.mSessionDates
                                                   where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                                   select Session.SessionDate_Local).FirstOrDefault(),

                         }).ToList();
            }

            else if (model.DataStatus == "PQ")
            {
                query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 2 && questions.IsFixedDate != null && questions.QuestionNumber != null && questions.IsFixedDate == model.NoticeDate && questions.DeActivateFlag == "RF"
                         orderby questions.QuestionSequence, questions.QuestionNumber ascending
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,
                             IsQuestionInPart = questions.IsQuestionInPart,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             SessionDateLocal = (from Session in pCtxt.mSessionDates
                                                 where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                                 select Session.SessionDateLocal).FirstOrDefault(),
                             SessionDateId = (from Session in pCtxt.mSessionDates
                                              where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                              select Session.Id).FirstOrDefault(),

                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),

                             SessionDateEnglish = (from Session in pCtxt.mSessionDates
                                                   where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                                   select Session.SessionDate_Local).FirstOrDefault(),
                         }).ToList();
            }

            foreach (var val in query)
            {
                val.MinistryNameLocal = (from M in pCtxt.mMinistry
                                         where M.MinistryID == val.MinisterID
                                         select M.MinistryNameLocal).FirstOrDefault();
                if (val.IsClubbed != null)
                {
                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',').Distinct().ToArray();

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tQuestions
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                            ConstituencyName = (from t in pCtxt.mConstituency
                                                                join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                select t.ConstituencyName).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemName))
                            {
                                val.CMemName = item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                            else
                            {
                                val.CMemName = val.CMemName + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                        }
                    }
                }
                if (val.IsHindi == true)
                {

                    val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                    val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                                                  join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                  where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                  select t.ConstituencyName_Local).FirstOrDefault();
                    val.MinistryNameLocal = (from M in pCtxt.mMinistry
                                             where M.MinistryID == val.MinisterID
                                             select M.MinistryNameLocal).FirstOrDefault();
                    if (val.IsClubbed != null)
                    {
                        string s = val.RefMemCode;
                        if (s != null && s != "")
                        {
                            string[] values = s.Split(',').Distinct().ToArray();

                            for (int i = 0; i < values.Length; i++)
                            {
                                int MemId = int.Parse(values[i]);
                                var item = (from questions in pCtxt.tQuestions
                                            select new QuestionModelCustom
                                            {
                                                MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.NameLocal)).FirstOrDefault(),
                                                ConstituencyName = (from t in pCtxt.mConstituency
                                                                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                    select t.ConstituencyName_Local).FirstOrDefault(),
                                            }).FirstOrDefault();

                                if (string.IsNullOrEmpty(val.CMemNameHindi))
                                {
                                    val.CMemNameHindi = item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                                else
                                {
                                    val.CMemNameHindi = val.CMemNameHindi + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                            }
                        }
                    }
                }
            }

            // get Assembly Name and Session Name by Assembly Id and Session Id This is added on 23/01/2015 by Sunil

            var AssemblyName = (from A in pCtxt.mAssemblies
                                where A.AssemblyCode == model.AssemblyID
                                select A.AssemblyName).FirstOrDefault();
            var SName = (from S in pCtxt.mSessions
                         where S.AssemblyID == model.AssemblyID
                         && S.SessionCode == model.SessionID
                         select new { S.SessionName, S.SessionNameLocal }).FirstOrDefault();

            var FL = (from SS in pCtxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();
            var FAP = (from SS in pCtxt.SiteSettings
                       where SS.SettingName == "SecureFileAccessingUrlPath"
                       select SS.SettingValue).FirstOrDefault();

            var last = query.Last();
            var First = query.First();
            model.StampTypeHindi = last.IsHindi;
            model.HeaderHindi = First.IsHindi;
            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            model.SessionName = SName.SessionNameLocal;
            model.AssesmblyName = AssemblyName;
            model.FilePath = FL;
            model.DocFilePath = FL;

            return model;
        }
        public static object GetRotationalMiniterName(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            string BindMinisName = "";
            string AllName = "";
            string LastName = "";
            if (model.SessionID != 0)
            {
                var MinisIds = (from RM in pCtxt.tRotationMinister
                                where (RM.SessionDateId == model.SessionDateId)
                                select RM.MinistryId).FirstOrDefault();

                MinisIds = MinisIds.ToString().Trim();

                string[] StrId = MinisIds.Split(',');

                for (int i = 0; i < StrId.Length; i++)
                { // Convert String Id into int Id
                    int Id = Convert.ToInt16(StrId[i]);

                    //Get Minister Name By Id
                    var MinisName = (from M in pCtxt.mMinistry
                                     where M.MinistryID == Id
                                     select M.MinistryNameLocal).FirstOrDefault();

                    BindMinisName = BindMinisName + MinisName.ToString().Trim() + " - ";
                }
                model.MinisterName = BindMinisName.Substring(0, BindMinisName.Length - 2);
                if (model.MinisterName.LastIndexOf('-') > 1)
                {
                    AllName = model.MinisterName.Substring(0, model.MinisterName.LastIndexOf('-') - 1);
                    LastName = model.MinisterName.Substring(model.MinisterName.LastIndexOf('-') + 1);
                    model.MinisterName = AllName + " तथा" + LastName;
                }
            }
            return model.MinisterName;
        }

        public static object GetRotationalMiniterNameEnglish(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            string BindMinisName = "";
            string AllName = "";
            string LastName = "";
            if (model.SessionID != 0)
            {
                var MinisIds = (from RM in pCtxt.tRotationMinister
                                where (RM.SessionDateId == model.SessionDateId)
                                select RM.MinistryId).FirstOrDefault();

                MinisIds = MinisIds.ToString().Trim();

                string[] StrId = MinisIds.Split(',');

                for (int i = 0; i < StrId.Length; i++)
                { // Convert String Id into int Id
                    int Id = Convert.ToInt16(StrId[i]);

                    //Get Minister Name By Id
                    var MinisName = (from M in pCtxt.mMinistry
                                     where M.MinistryID == Id
                                     select M.MinistryName).FirstOrDefault();

                    BindMinisName = BindMinisName + MinisName.ToString().Trim() + " - ";
                }

                model.MinisterNameEnglish = BindMinisName.Substring(0, BindMinisName.Length - 2);
                if (model.MinisterNameEnglish.LastIndexOf('-') > 1)
                {
                    AllName = model.MinisterNameEnglish.Substring(0, model.MinisterNameEnglish.LastIndexOf('-') - 1);
                    LastName = model.MinisterNameEnglish.Substring(model.MinisterNameEnglish.LastIndexOf('-') + 1);
                    model.MinisterName = AllName + " तथा" + LastName;
                    model.MinisterNameEnglish = AllName + " and" + LastName;
                }
            }
            return model.MinisterNameEnglish;
        }
        static object UnstaredUpdatemSessionDate(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            mSessionDate obj = pCtxt.mSessionDates.Single(m => m.SessionDate == model.NoticeDate);
            pCtxt.mSessionDates.Attach(obj);
            if (model.DataStatus == "CQ")
            {
                obj.UnStartQListPath = model.FilePath;
            }
            else if (model.DataStatus == "PQ")
            {
                obj.UPPQListPath = model.FilePath;
            }
            pCtxt.SaveChanges();
            string msg = "";

            return msg;
        }
        public static tMemberNotice GetAllDetailsForSpdf(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = new List<QuestionModelCustom>();

            if (model.DataStatus == "CQ")
            {
                query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 1 && questions.IsFixedDate != null && questions.QuestionNumber != null 
                         //&& questions.IsFixedDate == model.NoticeDate 
                         
                         && questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID && questions.DeActivateFlag == null
                         orderby questions.QuestionSequence, questions.QuestionNumber ascending
                         //  orderby questions.QuestionNumber ascending
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,
                             IsQuestionInPart = questions.IsQuestionInPart,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             SessionDateLocal = (from Session in pCtxt.mSessionDates
                                                 where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                                 select Session.SessionDateLocal).FirstOrDefault(),
                             SessionDateId = (from Session in pCtxt.mSessionDates
                                              where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                              select Session.Id).FirstOrDefault(),

                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),

                             SessionDateEnglish = (from Session in pCtxt.mSessionDates
                                                   where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                                   select Session.SessionDate_Local).FirstOrDefault(),


                         }).ToList();
            }
            else if (model.DataStatus == "PQ")
            {
                query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 1 && questions.IsFixedDate != null && questions.QuestionNumber != null 
                         && questions.IsFixedDate == model.NoticeDate 
                         //&& questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID && questions.DeActivateFlag == "RF"
                         orderby questions.QuestionSequence, questions.QuestionNumber ascending
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,
                             IsQuestionInPart = questions.IsQuestionInPart,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             SessionDateLocal = (from Session in pCtxt.mSessionDates
                                                 where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                                 select Session.SessionDateLocal).FirstOrDefault(),
                             SessionDateId = (from Session in pCtxt.mSessionDates
                                              where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                              select Session.Id).FirstOrDefault(),

                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),
                             SessionDateEnglish = (from Session in pCtxt.mSessionDates
                                                   where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                                                   select Session.SessionDate_Local).FirstOrDefault(),



                         }).ToList();
            }
            foreach (var val in query)
            {
                if (val.IsClubbed != null)
                {
                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',').Distinct().ToArray();

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tQuestions
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                            ConstituencyName = (from t in pCtxt.mConstituency
                                                                join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                select t.ConstituencyName).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemName))
                            {
                                val.CMemName = item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                            else
                            {
                                val.CMemName = val.CMemName + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                        }
                    }
                }
                if (val.IsHindi == true)
                {
                    val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                    val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                                                  join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                  where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                  select t.ConstituencyName_Local).FirstOrDefault();
                    val.MinistryNameLocal = (from M in pCtxt.mMinistry
                                             where M.MinistryID == val.MinisterID
                                             select M.MinistryNameLocal).FirstOrDefault();
                    if (val.IsClubbed != null)
                    {
                        string s = val.RefMemCode;
                        if (s != null && s != "")
                        {
                            string[] values = s.Split(',').Distinct().ToArray();

                            for (int i = 0; i < values.Length; i++)
                            {
                                int MemId = int.Parse(values[i]);
                                var item = (from questions in pCtxt.tQuestions
                                            select new QuestionModelCustom
                                            {
                                                MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.NameLocal)).FirstOrDefault(),
                                                ConstituencyName = (from t in pCtxt.mConstituency
                                                                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                    select t.ConstituencyName_Local).FirstOrDefault(),
                                            }).FirstOrDefault();

                                if (string.IsNullOrEmpty(val.CMemNameHindi))
                                {
                                    val.CMemNameHindi = item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                                else
                                {
                                    val.CMemNameHindi = val.CMemNameHindi + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                            }
                        }
                    }
                }
            }

            // get Assembly Name and Session Name by Assembly Id and Session Id This is added on 23/01/2015 by Sunil

            var AssemblyName = (from A in pCtxt.mAssemblies
                                where A.AssemblyCode == model.AssemblyID
                                select A.AssemblyName).FirstOrDefault();
            var SName = (from S in pCtxt.mSessions
                         where S.AssemblyID == model.AssemblyID
                         && S.SessionCode == model.SessionID
                         select new { S.SessionName, S.SessionNameLocal }).FirstOrDefault();
            // For Storing File 
            var FL = (from SS in pCtxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();

            // For Accessing File
            var FAP = (from SS in pCtxt.SiteSettings
                       where SS.SettingName == "SecureFileAccessingUrlPath"
                       select SS.SettingValue).FirstOrDefault();


            var last = query.Last();
            var First = query.First();

            model.StampTypeHindi = last.IsHindi;
            model.HeaderHindi = First.IsHindi;
            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;


            model.SessionName = SName.SessionNameLocal;
            model.AssesmblyName = AssemblyName;

            model.FilePath = FL;
            model.DocFilePath = FL;

            return model;
        }
        static object GetSessionStamp(object param)
        {
            // int Id = Convert.ToInt32(param);
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var test = (from questions in pCtxt.tSessionDateSignature
                        where questions.SessionDateId == model.SessionDateId
                        select new QuestionModelCustom
                        {
                            SesNameEnglish = questions.HeaderText,
                            SignatureName = questions.SignatureName,
                            SignatureNameLocal = questions.SignatureNameLocal,
                            SignatureDesignations = questions.SignatureDesignations,
                            SignatureDesignationsLocal = questions.SignatureDesignationsLocal,
                            SignatureDate = questions.SignatureDate,
                            SignatureDateLocal = questions.SignatureDateLocal,
                            SignaturePlace = questions.SignaturePlace,
                            SignaturePlaceLocal = questions.SignaturePlaceLocal
                        });
            var results = test.ToList();
            model.Values = results;

            return model;
        }
        static object UpdatemSessionDate(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            mSessionDate obj = pCtxt.mSessionDates.Single(m => m.SessionDate == model.NoticeDate);
            pCtxt.mSessionDates.Attach(obj);
            if (model.DataStatus == "CQ")
            {
                obj.StartQListPath = model.FilePath;
            }
            else if (model.DataStatus == "PQ")
            {
                obj.SPPQListPath = model.FilePath;
            }
            pCtxt.SaveChanges();
            var result = SkipQuestionProcessSteps.SkipApprovePDFStepOfTranslator(obj.Id);
            string msg = "";

            return msg;
        }
        static tMemberNotice GetSessionDatesUnstart(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from Session in pCtxt.mSessionDates
                         where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID
                         select new QuestionModelCustom
                         {
                             SessionDate = Session.SessionDate,
                             SessionDateLocal = Session.SessionDateLocal,
                             SfilePath = Session.UnStartQListPath,
                             IsSecApproved = Session.IsApprovedUnstart,
                             IsSitting = Session.IsSitting
                         }).ToList();
            var results = query.ToList();
            model.tQuestionModel = results;
            return model;
        }
        static tMemberNotice GetSessionDates(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from Session in pCtxt.mSessionDates
                         where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID
                         select new QuestionModelCustom
                         {
                             SessionDate = Session.SessionDate,
                             SessionDateLocal = Session.SessionDateLocal,
                             SfilePath = Session.StartQListPath,
                             IsSecApproved = Session.IsApproved,
                             IsSitting = Session.IsSitting
                         }).ToList();
            var results = query.ToList();
            model.tQuestionModel = results;

            return model;
        }

        //online member Question Submit Module 

        static object GetAllOnlineSubmission(object param)
        {
            OnlineMemberQmodel model = param as OnlineMemberQmodel;
            NoticeContext pCtxt = new NoticeContext();
            List<OnlineMemberQmodel> NewLsitForBind = new List<OnlineMemberQmodel>();

            var Question = (from questions in pCtxt.OnlineQuestions
                            where questions.MemberID == model.MemberId
                            && questions.AssemblyID == model.AssemblyID
                            && questions.SessionID == model.SessionID
                            select new OnlineMemberQmodel
                            {
                                QuestionID = questions.QuestionID,
                                DiaryNumber = questions.DiaryNumber,
                                Subject = questions.Subject,
                                NoticeDate = questions.MemberSubmitDate,
                                QuestionTypeId = questions.QuestionType,
                                PaperEntryType = "Question",
                            }).ToList();

            var Notice = (from Ntcs in pCtxt.OLNotices //Used Same Field For Notice which is used for Question but Value of notices
                          where Ntcs.MemberID == model.MemberId
                          && Ntcs.AssemblyId == model.AssemblyID
                            && Ntcs.SessionId == model.SessionID
                          select new OnlineMemberQmodel
                          {
                              QuestionID = Ntcs.OnlineNoticeId,
                              DiaryNumber = Ntcs.NoticeNumber,
                              Subject = Ntcs.Subject,
                              NoticeDate = Ntcs.MemberSubmitDate,
                              PaperEntryType = "Notice",
                          }).ToList();

            NewLsitForBind.AddRange(Question);
            NewLsitForBind.AddRange(Notice);

            int i = 1;
            foreach (var item in NewLsitForBind)
            {
                item.UniqueId = i;
                i++;
            }
            model.tQuestionModel = NewLsitForBind;
            return model;

        }
        static object GetAllCountForOnlineSubmission(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            ATourContext ACtx = new ATourContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();
            DateTime datetime = DateTime.Now.Date;

            // Get Count for Question and Notice
            var OLSCnt = (from questions in pCtxt.OnlineQuestions
                          where questions.MemberID == model.MemberId
                          && questions.QuestionType == (int)QuestionType.StartedQuestion
                          select questions.QuestionID).Count();

            var OLUCnt = (from questions in pCtxt.OnlineQuestions
                          where questions.MemberID == model.MemberId
                          && questions.QuestionType == (int)QuestionType.UnstaredQuestion
                          select questions.QuestionID).Count();

            var OLNCnt = (from Ntcs in pCtxt.OLNotices
                          where Ntcs.MemberID == model.MemberId
                          select Ntcs.OnlineNoticeId).Count();

            // Get Count for Tour Programme
            var TodayTour = (from AT in ACtx.tMemberTour
                             where (AT.MemberId == model.MemberId && AT.IsPublished == true)
                             && (AT.TourFromDateTime == datetime)
                             select AT.TourId).Count();

            var UpComingTour = (from AT in ACtx.tMemberTour
                                where (AT.MemberId == model.MemberId && AT.IsPublished == true)
                                && (AT.TourFromDateTime > datetime)
                                select AT.TourId).Count();

            var PreviousTour = (from AT in ACtx.tMemberTour
                                where (AT.MemberId == model.MemberId && AT.IsPublished == true)
                                && (AT.TourFromDateTime < datetime)
                                select AT.TourId).Count();

            var UnpublishedTour = (from AT in ACtx.tMemberTour
                                   where (AT.MemberId == model.MemberId && AT.IsPublished == false)
                                   select AT.TourId).Count();

            // Get SiteSettings 
            var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

            model.TotalValue = OLSCnt + OLUCnt + OLNCnt;
            model.TotalStaredReceived = OLSCnt;
            model.TotalUnStaredReceived = OLUCnt;
            model.TotalNoticesReceived = OLNCnt;

            model.AllMemberTourCount = TodayTour + UpComingTour + PreviousTour;
            model.TodayTourCount = TodayTour;
            model.UpcomingTourCount = UpComingTour;
            model.UnpublishedTourCount = UnpublishedTour;

            model.SessionID = Convert.ToInt16(SessionCode);
            model.AssemblyID = Convert.ToInt16(AssemblyCode);

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionID && m.AssemblyID == model.AssemblyID select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyID select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;
            return model;
        }
        static object GetOnlineMemberDashboardItemsCounter(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            ATourContext ACtx = new ATourContext();
            DateTime datetime = DateTime.Now.Date;

            var OLSCnt = (from questions in pCtxt.OnlineQuestions
                          where questions.MemberID == model.MemberId
                          && questions.AssemblyID == model.AssemblyID
                          && questions.SessionID == model.SessionID
                          && questions.QuestionType == (int)QuestionType.StartedQuestion
                          select questions.QuestionID).Count();

            var OLUCnt = (from questions in pCtxt.OnlineQuestions
                          where questions.MemberID == model.MemberId
                          && questions.AssemblyID == model.AssemblyID
                          && questions.SessionID == model.SessionID
                          && questions.QuestionType == (int)QuestionType.UnstaredQuestion
                          select questions.QuestionID).Count();

            var OLNCnt = (from Ntcs in pCtxt.OLNotices
                          where Ntcs.MemberID == model.MemberId
                          && Ntcs.AssemblyId == model.AssemblyID
                          && Ntcs.SessionId == model.SessionID
                          select Ntcs.OnlineNoticeId).Count();

            // Get Count for Tour Programme
            var TodayTour = (from AT in ACtx.tMemberTour
                             where (AT.MemberId == model.MemberId)
                             && (AT.AssemblyId == model.AssemblyID && AT.IsPublished == true)
                             && (AT.TourFromDateTime == datetime)
                             select AT.TourId).Count();

            var UpComingTour = (from AT in ACtx.tMemberTour
                                where (AT.MemberId == model.MemberId)
                                 && (AT.AssemblyId == model.AssemblyID && AT.IsPublished == true)
                                && (AT.TourFromDateTime > datetime)
                                select AT.TourId).Count();

            var PreviousTour = (from AT in ACtx.tMemberTour
                                where (AT.MemberId == model.MemberId)
                                 && (AT.AssemblyId == model.AssemblyID && AT.IsPublished == true)
                                && (AT.TourFromDateTime < datetime)
                                select AT.TourId).Count();

            var UnpublishedTour = (from AT in ACtx.tMemberTour
                                   where (AT.MemberId == model.MemberId && AT.IsPublished == false)
                                   select AT.TourId).Count();


            model.TotalStaredReceived = OLSCnt;
            model.TotalUnStaredReceived = OLUCnt;
            model.TotalNoticesReceived = OLNCnt;
            model.UnpublishedTourCount = UnpublishedTour;
            model.UpcomingTourCount = UpComingTour;
            model.TodayTourCount = TodayTour;

            model.AllMemberTourCount = TodayTour + UpComingTour + PreviousTour + UnpublishedTour;
            return model;
        }
        static object NewEntryForm(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            UserContext Uctx = new UserContext();
            DiaryModel Obj = param as DiaryModel;
            DiaryModel Obj2 = new DiaryModel();


            if (Obj.PaperEntryType == "Starred Questions" || Obj.PaperEntryType == "Unstarred Questions")
            {
                var CategoryList = (from C in DiaCtx.objmEvent
                                    where C.PaperCategoryTypeId == 1
                                    orderby C.EventName
                                    select C).ToList();
                var signpath = (from U in Uctx.mUsers
                                where U.UserId == Obj.UserId
                                select U.SignaturePath).FirstOrDefault();

                Obj2.eventList = CategoryList;
                Obj2.memMinList = (List<mMinisteryMinisterModel>)MinisteryContext.GetMinistersonly(Obj.AssemblyID);
                Obj2.SignFilePath = Convert.ToString(signpath);
                DiaCtx.Close();
                return Obj2;
            }
            else if (Obj.PaperEntryType == "Notices")
            {
                var CategoryList = (from C in DiaCtx.objmEvent
                                    where C.PaperCategoryTypeId == 4
                                    orderby C.EventName
                                    select C).ToList();
                var signpath = (from U in Uctx.mUsers
                                where U.UserId == Obj.UserId
                                select U.SignaturePath).FirstOrDefault();
                /*Get All Demand*/
                var DemandList = (from Demand in DiaCtx.tCutMotionDemand
                                  select Demand).ToList();
                tCutMotionDemand obj = new tCutMotionDemand();
                obj.DemandName = "Select Demand";
                obj.DemandNo = 0;
                DemandList.Add(obj);
                Obj2.DemandList = DemandList;
                Obj2.eventList = CategoryList;
                Obj2.memMinList = (List<mMinisteryMinisterModel>)MinisteryContext.GetMinistersonly(Obj.AssemblyID);
                Obj2.SignFilePath = Convert.ToString(signpath);
                DiaCtx.Close();
                return Obj2;
            }
            return null;
        }

        static object GetCountOnlineStartQuestion(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.OnlineQuestions
                         where (questions.QuestionType == (int)QuestionType.StartedQuestion)
                         && (questions.MemberID == model.MemberId)
                         select questions.QuestionID).Count();
            //select new OnlineMemberQmodel
            //{
            //    QuestionID = questions.QuestionID,
            //    DiaryNumber = questions.DiaryNumber,
            //    Subject = questions.Subject,
            //    NoticeDate = questions.NoticeRecievedDate,
            //    MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
            //}).ToList();

            return query;
        }
        static object GetCountOnlineUntartQuestion(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.OnlineQuestions
                         where (questions.QuestionType == (int)QuestionType.UnstaredQuestion)
                         && (questions.MemberID == model.MemberId)
                         select questions.QuestionID).Count();
            //             select new OnlineMemberQmodel
            //             {
            //                 QuestionID = questions.QuestionID,
            //                 DiaryNumber = questions.DiaryNumber,
            //                 Subject = questions.Subject,
            //                 NoticeDate = questions.NoticeRecievedDate,
            //                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
            //             }).ToList();

            //int totalRecords = query.Count();
            return query;
        }
        static object GetCountOnlineNotice(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from Ntcs in pCtxt.OLNotices
                         where Ntcs.MemberID == model.MemberId
                         select Ntcs.OnlineNoticeId).Count();
            return query;
        }
        static object GetAllOnlineNotices(object param)
        {
            OnlineMemberQmodel model = param as OnlineMemberQmodel;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from Ntcs in pCtxt.OLNotices
                         where Ntcs.MemberID == model.MemberId
                         && Ntcs.AssemblyId == model.AssemblyID
                         && Ntcs.SessionId == model.SessionID
                         select new OnlineMemberQmodel
                         {
                             NoticeId = Ntcs.OnlineNoticeId,
                             NoticeNumber = Ntcs.NoticeNumber,
                             Subject = Ntcs.Subject,
                             NoticeDate = Ntcs.MemberSubmitDate,

                         }).ToList();
            model.ResultCount = query.Count();
            model.tQuestionModel = query;
            return model;
        }
        public static object GetOnlineStarredQuestions(object param)
        {
            OnlineMemberQmodel model = param as OnlineMemberQmodel;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.OnlineQuestions
                         where (questions.QuestionType == (int)QuestionType.StartedQuestion)
                         && (questions.MemberID == model.MemberId)
                       && (questions.AssemblyID == model.AssemblyID)
                       && (questions.SessionID == model.SessionID)
                         orderby questions.QuestionID descending
                         select new OnlineMemberQmodel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.MemberSubmitDate,
                             //MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                         }).ToList();
            //int totalRecords = query.Count();
            //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            model.ResultCount = query.Count();
            model.tQuestionModel = query;
            return model;
        }
        static object SaveUpdateQuestionsTable(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            DiaryModel update = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();
            OnlineQuestions OLQuesObj = new OnlineQuestions();
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel DMdl = new DiaryModel();

            //Get Constituency Number
            DMdl = (DiaryModel)LegislationFixationContext.GetConstByMemberId(update);
            char[] a = DMdl.ConstituencyName.ToLower().ToCharArray();

            for (int i = 0; i < a.Count(); i++)
            {
                a[i] = i == 0 || a[i - 1] == ' ' ? char.ToUpper(a[i]) : a[i];
            }
            string result = string.Join("", a);
            DMdl.ConstituencyName = result;
            try
            {
                if (update.PaperEntryType == "NewEntry")
                {
                    if (update.BtnCaption == "Submit")
                    {
                        //Get Diary Number
                        update.DiaryNumber = (string)DiariesContext.CheckDiaryNumber(update);

                        // Add record in tQuestions Table
                        if (update.ChkLocalLang == "Local")
                        {
                            QuesObj.IsHindi = true;
                        }
                        else
                        {
                            QuesObj.IsHindi = null;
                        }

                        QuesObj.AssemblyID = update.AssemblyID;
                        QuesObj.SessionID = update.SessionID;
                        QuesObj.DiaryNumber = update.DiaryNumber;
                        QuesObj.MainQuestion = update.MainQuestion;
                        QuesObj.Subject = update.Subject;
                        QuesObj.SubInCatchWord = update.Subject;
                        QuesObj.QuestionStatus = (int)Questionstatus.QuestionPending;
                        QuesObj.Priority = update.Priority;
                        QuesObj.MinistryId = update.MinistryId;
                        //QuesObj.DepartmentID = update.DepartmentId;
                        QuesObj.MemberID = update.MemberId;
                        QuesObj.ConstituencyName = DMdl.ConstituencyName;
                        QuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        QuesObj.EventId = update.EventId;
                        QuesObj.QuestionType = (int)QuestionType.StartedQuestion;
                        QuesObj.IsOnlineSubmitted = true;
                        QuesObj.IsOnlineSubmittedDate = DateTime.Now;
                        qCtxDB.tQuestions.Add(QuesObj);
                        qCtxDB.SaveChanges();

                        // Add record in OnlineQuestionTable
                        OLQuesObj.MainQuestionID = QuesObj.QuestionID;
                        OLQuesObj.DiaryNumber = update.DiaryNumber;
                        OLQuesObj.AssemblyID = update.AssemblyID;
                        OLQuesObj.SessionID = update.SessionID;
                        OLQuesObj.MemberID = update.MemberId;
                        OLQuesObj.MainQuestion = update.MainQuestion;
                        OLQuesObj.EventId = update.EventId;
                        OLQuesObj.Priority = update.Priority;
                        OLQuesObj.Subject = update.Subject;
                        OLQuesObj.SubInCatchWord = update.Subject;
                        OLQuesObj.MinistryId = update.MinistryId;
                        //OLQuesObj.DepartmentID = update.DepartmentId;
                        OLQuesObj.MinistryId = update.MinistryId;
                        OLQuesObj.MemberSubmitDate = DateTime.Now;
                        OLQuesObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        if (update.ChkLocalLang == "Local")
                        {
                            OLQuesObj.IsHindi = true;
                        }
                        else
                        {
                            OLQuesObj.IsHindi = null;
                        }
                        OLQuesObj.ConstituencyName = DMdl.ConstituencyName;
                        OLQuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        OLQuesObj.QuestionType = (int)QuestionType.StartedQuestion;
                        OLQuesObj.QuestPdfLang = update.QuestPdfLang;
                        OLQuesObj.SubmitToVS = true;
                        pCtxt.OnlineQuestions.Add(OLQuesObj);
                        pCtxt.SaveChanges();
                        //Send QuestionId to Controller
                        update.QuestionId = OLQuesObj.QuestionID;
                        update.ConstituencyName = DMdl.ConstituencyName;
                        update.ConstituencyCode = DMdl.ConstituencyCode;
                        update.QuestPdfLang = DMdl.QuestPdfLang;
                        pCtxt.Close();

                        update.Message = "Submitted";

                    }

                    else if (update.BtnCaption == "Save")
                    {
                        if (update.ChkLocalLang == "Local")
                        {
                            OLQuesObj.IsHindi = true;
                        }
                        else { OLQuesObj.IsHindi = null; }
                        // Add record in OnlineQuestionTable
                        OLQuesObj.AssemblyID = update.AssemblyID;
                        OLQuesObj.SessionID = update.SessionID;
                        OLQuesObj.MemberID = update.MemberId;
                        OLQuesObj.MainQuestion = update.MainQuestion;
                        OLQuesObj.EventId = update.EventId;
                        OLQuesObj.Priority = update.Priority;
                        OLQuesObj.Subject = update.Subject;
                        OLQuesObj.SubInCatchWord = update.Subject;
                        OLQuesObj.MinistryId = update.MinistryId;
                        OLQuesObj.DepartmentID = update.DepartmentId;
                        //OLQuesObj.MemberSubmitDate = DateTime.Now;
                        //OLQuesObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        OLQuesObj.ConstituencyName = DMdl.ConstituencyName;
                        OLQuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        OLQuesObj.QuestionType = (int)QuestionType.StartedQuestion;
                        OLQuesObj.SubmitToVS = false;
                        OLQuesObj.QuestPdfLang = update.QuestPdfLang;
                        pCtxt.OnlineQuestions.Add(OLQuesObj);
                        pCtxt.SaveChanges();
                        pCtxt.Close();

                        update.Message = "Saved";
                        update.QuestionId = OLQuesObj.QuestionID;
                    }
                }

                else if (update.PaperEntryType == "UpdateEntry")
                {
                    if (update.BtnCaption == "Submit")
                    {
                        //Get Diary Number
                        update.DiaryNumber = (string)DiariesContext.CheckDiaryNumber(update);

                        // Add record in tQuestions Table
                        if (update.ChkLocalLang == "Local")
                        {
                            QuesObj.IsHindi = true;
                        }
                        else { QuesObj.IsHindi = null; }
                        QuesObj.AssemblyID = update.AssemblyID;
                        QuesObj.SessionID = update.SessionID;
                        QuesObj.DiaryNumber = update.DiaryNumber;
                        QuesObj.MainQuestion = update.MainQuestion;
                        QuesObj.Subject = update.Subject;
                        QuesObj.SubInCatchWord = update.Subject;
                        QuesObj.QuestionStatus = (int)Questionstatus.QuestionPending;
                        QuesObj.Priority = update.Priority;
                        QuesObj.MinistryId = update.MinistryId;
                        //QuesObj.DepartmentID = update.DepartmentId;
                        QuesObj.MemberID = update.MemberId;
                        QuesObj.ConstituencyName = DMdl.ConstituencyName;
                        QuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        QuesObj.EventId = update.EventId;
                        QuesObj.QuestionType = (int)QuestionType.StartedQuestion;
                        QuesObj.IsOnlineSubmitted = true;
                        QuesObj.IsOnlineSubmittedDate = DateTime.Now;
                        qCtxDB.tQuestions.Add(QuesObj);
                        qCtxDB.SaveChanges();


                        // Update record in OnlineQuestionTable
                        OLQuesObj = pCtxt.OnlineQuestions.Single(m => m.QuestionID == update.QuestionId);
                        pCtxt.OnlineQuestions.Attach(OLQuesObj);
                        if (update.ChkLocalLang == "Local")
                        {
                            OLQuesObj.IsHindi = true;
                        }
                        else
                        {
                            OLQuesObj.IsHindi = null;
                        }
                        OLQuesObj.MainQuestionID = QuesObj.QuestionID;
                        OLQuesObj.DiaryNumber = update.DiaryNumber;
                        OLQuesObj.AssemblyID = update.AssemblyID;
                        OLQuesObj.SessionID = update.SessionID;
                        OLQuesObj.MemberID = update.MemberId;
                        OLQuesObj.MainQuestion = update.MainQuestion;
                        OLQuesObj.EventId = update.EventId;
                        OLQuesObj.Priority = update.Priority;
                        OLQuesObj.Subject = update.Subject;
                        OLQuesObj.SubInCatchWord = update.Subject;
                        OLQuesObj.MinistryId = update.MinistryId;
                        //OLQuesObj.DepartmentID = update.DepartmentId;
                        OLQuesObj.MemberSubmitDate = DateTime.Now;
                        OLQuesObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        OLQuesObj.ConstituencyName = DMdl.ConstituencyName;
                        OLQuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        OLQuesObj.QuestionType = (int)QuestionType.StartedQuestion;
                        OLQuesObj.SubmitToVS = true;
                        OLQuesObj.QuestPdfLang = update.QuestPdfLang;
                        pCtxt.SaveChanges();

                        //Send QuestionId to Controller
                        update.QuestionId = OLQuesObj.QuestionID;
                        update.ConstituencyName = DMdl.ConstituencyName;
                        update.ConstituencyCode = DMdl.ConstituencyCode;
                        update.QuestPdfLang = DMdl.QuestPdfLang;
                        pCtxt.Close();


                        update.Message = "Submitted";

                    }

                    else if (update.BtnCaption == "Save")
                    {
                        // Update record in OnlineQuestionTable
                        OLQuesObj = pCtxt.OnlineQuestions.Single(m => m.QuestionID == update.QuestionId);
                        pCtxt.OnlineQuestions.Attach(OLQuesObj);
                        if (update.ChkLocalLang == "Local")
                        {
                            OLQuesObj.IsHindi = true;
                        }
                        else
                        {
                            OLQuesObj.IsHindi = null;
                        }
                        OLQuesObj.AssemblyID = update.AssemblyID;
                        OLQuesObj.SessionID = update.SessionID;
                        OLQuesObj.MemberID = update.MemberId;
                        OLQuesObj.MainQuestion = update.MainQuestion;
                        OLQuesObj.EventId = update.EventId;
                        OLQuesObj.Priority = update.Priority;
                        OLQuesObj.Subject = update.Subject;
                        OLQuesObj.SubInCatchWord = update.Subject;
                        OLQuesObj.MinistryId = update.MinistryId;
                        //OLQuesObj.DepartmentID = update.DepartmentId;
                        //OLQuesObj.MemberSubmitDate = DateTime.Now;
                        //OLQuesObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        OLQuesObj.ConstituencyName = DMdl.ConstituencyName;
                        OLQuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        OLQuesObj.QuestionType = (int)QuestionType.StartedQuestion;
                        OLQuesObj.SubmitToVS = false;
                        OLQuesObj.QuestPdfLang = update.QuestPdfLang;
                        pCtxt.SaveChanges();
                        pCtxt.Close();

                        update.Message = "Updated";

                    }
                }

            }


            catch (Exception ex)
            {
                update.Message = ex.Message;
            }



            return update;
        }
        static object UpdateFile(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel ObjD = param as DiaryModel;
            tQuestion QObj = new tQuestion();
            OnlineQuestions OLObj = new OnlineQuestions();
            try
            {

                OLObj = pCtxt.OnlineQuestions.Single(d => d.QuestionID == ObjD.QuestionId);
                //Update in Online Question Table
                pCtxt.OnlineQuestions.Attach(OLObj);
                OLObj.OriMemFilePath = ObjD.FilePath;
                OLObj.OriMemFileName = ObjD.FileName;
                pCtxt.SaveChanges();
                pCtxt.Close();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Updated";

        }
        static object SubmitQuestionById(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel ObjD = param as DiaryModel;
            tQuestion QObj = new tQuestion();
            OnlineQuestions OLObj = new OnlineQuestions();
            try
            {
                OLObj = pCtxt.OnlineQuestions.Single(d => d.QuestionID == ObjD.QuestionId);
                if (OLObj.IsHindi == true)
                {
                    QObj.IsHindi = true;
                }
                QObj.AssemblyID = OLObj.AssemblyID;
                QObj.SessionID = OLObj.SessionID;
                QObj.DiaryNumber = ObjD.DiaryNumber;
                QObj.MainQuestion = OLObj.MainQuestion;
                QObj.Subject = OLObj.Subject;
                QObj.SubInCatchWord = OLObj.SubInCatchWord;
                QObj.QuestionStatus = (int)Questionstatus.QuestionPending;
                QObj.Priority = OLObj.Priority;
                QObj.MinistryId = OLObj.MinistryId;
                QObj.DepartmentID = OLObj.DepartmentID;
                QObj.MemberID = OLObj.MemberID;
                QObj.ConstituencyName = OLObj.ConstituencyName;
                int Constcode = Convert.ToInt16(OLObj.ConstituencyNo);
                QObj.ConstituencyNo = OLObj.ConstituencyNo;
                QObj.EventId = OLObj.EventId;
                QObj.QuestionType = OLObj.QuestionType;
                QObj.IsOnlineSubmitted = true;
                QObj.IsOnlineSubmittedDate = DateTime.Now;
                QObj.NoticeRecievedDate = DateTime.Now.Date;
                QObj.NoticeRecievedTime = DateTime.Now.TimeOfDay;
                pCtxt.tQuestions.Add(QObj);
                pCtxt.SaveChanges();
                //Skipping steps
                var result1 = SkipQuestionProcessSteps.SkippingQuesProcessingSteps(QObj.QuestionID);
                var result2 = SkipQuestionProcessSteps.SkipQuesProcessStepDiary(QObj.QuestionID);
                //For File Saving Path
                var FL = (from SS in pCtxt.SiteSettings
                          where SS.SettingName == "FileLocation"
                          select SS.SettingValue).FirstOrDefault();

                ObjD.ConsituencyLocal = (from SD in pCtxt.mConstituency
                                         where SD.ConstituencyCode == Constcode && SD.AssemblyID == OLObj.AssemblyID
                                         select SD.ConstituencyName_Local).FirstOrDefault();
                //Update Status in Online Question 
                OLObj.SubmitToVS = true;
                OLObj.MainQuestionID = QObj.QuestionID;
                OLObj.DiaryNumber = ObjD.DiaryNumber;
                OLObj.MemberSubmitDate = DateTime.Now;
                OLObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                OLObj.QuestPdfLang = OLObj.QuestPdfLang;
                pCtxt.SaveChanges();
                pCtxt.Close();

                //Send Field Value to Generate Pdf
                ObjD.SaveFilePath = FL;
                ObjD.Message = "Success";
                ObjD.QuestionId = OLObj.QuestionID;
                ObjD.Subject = OLObj.Subject;
                ObjD.MainQuestion = OLObj.MainQuestion;
                ObjD.MemberId = OLObj.MemberID;
                ObjD.MinistryId = OLObj.MinistryId;
                ObjD.ConstituencyCode = Convert.ToInt16(OLObj.ConstituencyNo);
                ObjD.ConstituencyName = OLObj.ConstituencyName;
                if (OLObj.Priority == 0)
                {
                    ObjD.Priority = null;
                }
                else
                {
                    ObjD.Priority = OLObj.Priority;
                }
                ObjD.QuestPdfLang = OLObj.QuestPdfLang;
                var currdate = Convert.ToDateTime(DateTime.Now).ToString("dd/MM/yyyy");
                var currTime = Convert.ToDateTime(DateTime.Now).ToString("hh:mm tt");

                ObjD.PDfDate = currdate;
                ObjD.PDfTime = currTime;
            }
            catch (Exception ex)
            {
                ObjD.Message = ex.Message;
            }
            return ObjD;
        }

        static DiaryModel GetQuestionByIdForEdit(object param)
        {
            DiaryModel model = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            NoticeContext pCtxt = new NoticeContext();
            UserContext Uctx = new UserContext();

            var signpath = (from U in Uctx.mUsers
                            where U.UserId == model.UserId
                            select U.SignaturePath).FirstOrDefault();

            var query = (from questions in pCtxt.OnlineQuestions
                         where (questions.QuestionID == model.QuestionId)
                         select new DiaryModel
                         {
                             AssemblyID = questions.AssemblyID,
                             SessionID = questions.SessionID,
                             QuestionId = questions.QuestionID,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             MinistryId = questions.MinistryId,
                             //DepartmentId = questions.DepartmentID,
                             ConstituencyName = questions.ConstituencyName,
                             Priority = questions.Priority,
                             IsHindi = questions.IsHindi,
                             QuestionTypeId = questions.QuestionType,
                             QuestPdfLang = questions.QuestPdfLang
                             //MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).FirstOrDefault();

            model = query;

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 1
                           select Evnt).ToList();
            model.eventList = EvtList;

            SiteSettingContext settingContext = new SiteSettingContext();
            var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            int AId = Convert.ToInt16(AssemblyCode);
            /*Get All MinistryMinister*/
            var MinMisList = (from Min in DiaCtx.objmMinistry
                              join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
                              where Min.IsActive == true && Min.AssemblyID == AId
                              orderby Min.OrderID
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
                              }).ToList();
            model.memMinList = MinMisList;

            model.SignFilePath = Convert.ToString(signpath);

            /*Get All Department*/
            //if (query.MinistryId != null && query.MinistryId != 0)
            //{
            //    var DeptList = (from minsDept in DiaCtx.objMinisDepart
            //                    join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
            //                    where minsDept.MinistryID == query.MinistryId
            //                    orderby dept.deptname
            //                    select new DiaryModel
            //                    {
            //                        DepartmentId = dept.deptId,
            //                        DepartmentName = dept.deptname

            //                    }).ToList();

            //    model.DiaryList = DeptList;
            //}
            return model;
        }
        public static object GetOnlineUnStarredQuestions(object param)
        {
            OnlineMemberQmodel model = param as OnlineMemberQmodel;
            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.OnlineQuestions
                         where (questions.QuestionType == (int)QuestionType.UnstaredQuestion)
                         && (questions.MemberID == model.MemberId)
                         && (questions.AssemblyID == model.AssemblyID)
                         && (questions.SessionID == model.SessionID)
                         orderby questions.QuestionID descending
                         select new OnlineMemberQmodel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.MemberSubmitDate,
                             //MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                         }).ToList();

            //int totalRecords = query.Count();
            //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = query.Count();
            model.tQuestionModel = query;

            return model;
        }
        static object SaveUpdateQuestionsUnstarredTable(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            DiaryModel update = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();
            OnlineQuestions OLQuesObj = new OnlineQuestions();
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel DMdl = new DiaryModel();

            //Get Constituency Number
            DMdl = (DiaryModel)LegislationFixationContext.GetConstByMemberId(update);
            char[] a = DMdl.ConstituencyName.ToLower().ToCharArray();

            for (int i = 0; i < a.Count(); i++)
            {
                a[i] = i == 0 || a[i - 1] == ' ' ? char.ToUpper(a[i]) : a[i];

            }
            string result = string.Join("", a);
            DMdl.ConstituencyName = result;
            try
            {
                if (update.PaperEntryType == "NewEntry")
                {
                    if (update.BtnCaption == "Submit")
                    {
                        //Get Diary Number
                        update.DiaryNumber = (string)DiariesContext.CheckDiaryNumber(update);

                        // Add record in tQuestions Table
                        if (update.ChkLocalLang == "Local")
                        {
                            QuesObj.IsHindi = true;
                        }
                        else
                        {
                            QuesObj.IsHindi = null;
                        }
                        QuesObj.AssemblyID = update.AssemblyID;
                        QuesObj.SessionID = update.SessionID;
                        QuesObj.DiaryNumber = update.DiaryNumber;
                        QuesObj.MainQuestion = update.MainQuestion;
                        QuesObj.Subject = update.Subject;
                        QuesObj.SubInCatchWord = update.Subject;
                        QuesObj.QuestionStatus = (int)Questionstatus.QuestionPending;
                        QuesObj.Priority = update.Priority;
                        QuesObj.MinistryId = update.MinistryId;
                        //QuesObj.DepartmentID = update.DepartmentId;
                        QuesObj.MemberID = update.MemberId;
                        QuesObj.ConstituencyName = DMdl.ConstituencyName;
                        QuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        QuesObj.EventId = update.EventId;
                        QuesObj.QuestionType = (int)QuestionType.UnstaredQuestion;
                        QuesObj.IsOnlineSubmitted = true;
                        QuesObj.IsOnlineSubmittedDate = DateTime.Now;
                        qCtxDB.tQuestions.Add(QuesObj);
                        qCtxDB.SaveChanges();

                        // Add record in OnlineQuestionTable
                        OLQuesObj.MainQuestionID = QuesObj.QuestionID;
                        OLQuesObj.DiaryNumber = update.DiaryNumber;
                        OLQuesObj.AssemblyID = update.AssemblyID;
                        OLQuesObj.SessionID = update.SessionID;
                        OLQuesObj.MemberID = update.MemberId;
                        OLQuesObj.MainQuestion = update.MainQuestion;
                        OLQuesObj.EventId = update.EventId;
                        OLQuesObj.Priority = update.Priority;
                        OLQuesObj.Subject = update.Subject;
                        OLQuesObj.SubInCatchWord = update.Subject;
                        OLQuesObj.MinistryId = update.MinistryId;
                        //OLQuesObj.DepartmentID = update.DepartmentId;
                        OLQuesObj.MemberSubmitDate = DateTime.Now;
                        OLQuesObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        if (update.ChkLocalLang == "Local")
                        {
                            OLQuesObj.IsHindi = true;
                        }
                        else
                        {
                            OLQuesObj.IsHindi = null;
                        }
                        OLQuesObj.ConstituencyName = DMdl.ConstituencyName;
                        OLQuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        OLQuesObj.QuestionType = (int)QuestionType.UnstaredQuestion;
                        OLQuesObj.SubmitToVS = true;
                        OLQuesObj.QuestPdfLang = update.QuestPdfLang;
                        pCtxt.OnlineQuestions.Add(OLQuesObj);
                        pCtxt.SaveChanges();

                        //Send QuestionId to Controller
                        update.QuestionId = OLQuesObj.QuestionID;
                        update.ConstituencyCode = DMdl.ConstituencyCode;
                        update.ConstituencyName = DMdl.ConstituencyName;
                        update.QuestPdfLang = DMdl.QuestPdfLang;
                        pCtxt.Close();

                        update.Message = "Submitted";

                    }

                    else if (update.BtnCaption == "Save")
                    {
                        // Add record in OnlineQuestionTable
                        if (update.ChkLocalLang == "Local")
                        {
                            OLQuesObj.IsHindi = true;
                        }
                        else
                        {
                            OLQuesObj.IsHindi = null;
                        }
                        OLQuesObj.AssemblyID = update.AssemblyID;
                        OLQuesObj.SessionID = update.SessionID;
                        OLQuesObj.MemberID = update.MemberId;
                        OLQuesObj.MainQuestion = update.MainQuestion;
                        OLQuesObj.EventId = update.EventId;
                        OLQuesObj.Priority = update.Priority;
                        OLQuesObj.Subject = update.Subject;
                        OLQuesObj.SubInCatchWord = update.Subject;
                        OLQuesObj.MinistryId = update.MinistryId;
                        //OLQuesObj.DepartmentID = update.DepartmentId;
                        OLQuesObj.ConstituencyName = DMdl.ConstituencyName;
                        OLQuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        OLQuesObj.QuestionType = (int)QuestionType.UnstaredQuestion;
                        OLQuesObj.SubmitToVS = false;
                        OLQuesObj.QuestPdfLang = update.QuestPdfLang;
                        pCtxt.OnlineQuestions.Add(OLQuesObj);
                        pCtxt.SaveChanges();
                        pCtxt.Close();

                        update.Message = "Saved";

                    }
                }

                else if (update.PaperEntryType == "UpdateEntry")
                {
                    if (update.BtnCaption == "Submit")
                    {
                        //Get Diary Number
                        update.DiaryNumber = (string)DiariesContext.CheckDiaryNumber(update);

                        // Add record in tQuestions Table
                        if (update.ChkLocalLang == "Local")
                        {
                            QuesObj.IsHindi = true;
                        }
                        else
                        {
                            QuesObj.IsHindi = null;
                        }
                        QuesObj.AssemblyID = update.AssemblyID;
                        QuesObj.SessionID = update.SessionID;
                        QuesObj.DiaryNumber = update.DiaryNumber;
                        QuesObj.MainQuestion = update.MainQuestion;
                        QuesObj.Subject = update.Subject;
                        QuesObj.SubInCatchWord = update.Subject;
                        QuesObj.QuestionStatus = (int)Questionstatus.QuestionPending;
                        QuesObj.Priority = update.Priority;
                        QuesObj.MinistryId = update.MinistryId;
                        //QuesObj.DepartmentID = update.DepartmentId;
                        QuesObj.MemberID = update.MemberId;
                        QuesObj.ConstituencyName = DMdl.ConstituencyName;
                        QuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        QuesObj.EventId = update.EventId;
                        QuesObj.QuestionType = (int)QuestionType.UnstaredQuestion;
                        QuesObj.IsOnlineSubmitted = true;
                        QuesObj.IsOnlineSubmittedDate = DateTime.Now;
                        qCtxDB.tQuestions.Add(QuesObj);
                        qCtxDB.SaveChanges();


                        // Update record in OnlineQuestionTable
                        OLQuesObj = pCtxt.OnlineQuestions.Single(m => m.QuestionID == update.QuestionId);
                        pCtxt.OnlineQuestions.Attach(OLQuesObj);
                        if (update.ChkLocalLang == "Local")
                        {
                            OLQuesObj.IsHindi = true;
                        }
                        else
                        {
                            OLQuesObj.IsHindi = null;
                        }
                        OLQuesObj.MainQuestionID = QuesObj.QuestionID;
                        OLQuesObj.DiaryNumber = update.DiaryNumber;
                        OLQuesObj.AssemblyID = update.AssemblyID;
                        OLQuesObj.SessionID = update.SessionID;
                        OLQuesObj.MemberID = update.MemberId;
                        OLQuesObj.MainQuestion = update.MainQuestion;
                        OLQuesObj.EventId = update.EventId;
                        OLQuesObj.Priority = update.Priority;
                        OLQuesObj.Subject = update.Subject;
                        OLQuesObj.SubInCatchWord = update.Subject;
                        OLQuesObj.MinistryId = update.MinistryId;
                        //OLQuesObj.DepartmentID = update.DepartmentId;
                        OLQuesObj.MemberSubmitDate = DateTime.Now;
                        OLQuesObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        OLQuesObj.ConstituencyName = DMdl.ConstituencyName;
                        OLQuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        OLQuesObj.QuestionType = (int)QuestionType.UnstaredQuestion;
                        OLQuesObj.SubmitToVS = true;
                        OLQuesObj.QuestPdfLang = update.QuestPdfLang;
                        pCtxt.SaveChanges();

                        //Send QuestionId to Controller
                        update.QuestionId = OLQuesObj.QuestionID;
                        update.ConstituencyCode = DMdl.ConstituencyCode;
                        update.ConstituencyName = DMdl.ConstituencyName;
                        update.QuestPdfLang = DMdl.QuestPdfLang;
                        pCtxt.Close();


                        update.Message = "Submitted";

                    }

                    else if (update.BtnCaption == "Save")
                    {
                        // Update record in OnlineQuestionTable
                        OLQuesObj = pCtxt.OnlineQuestions.Single(m => m.QuestionID == update.QuestionId);
                        pCtxt.OnlineQuestions.Attach(OLQuesObj);
                        if (update.ChkLocalLang == "Local")
                        {
                            OLQuesObj.IsHindi = true;
                        }
                        else
                        {

                            OLQuesObj.IsHindi = null;
                        }
                        OLQuesObj.AssemblyID = update.AssemblyID;
                        OLQuesObj.SessionID = update.SessionID;
                        OLQuesObj.MemberID = update.MemberId;
                        OLQuesObj.MainQuestion = update.MainQuestion;
                        OLQuesObj.EventId = update.EventId;
                        OLQuesObj.Priority = update.Priority;
                        OLQuesObj.Subject = update.Subject;
                        OLQuesObj.SubInCatchWord = update.Subject;
                        OLQuesObj.MinistryId = update.MinistryId;
                        //OLQuesObj.DepartmentID = update.DepartmentId;
                        OLQuesObj.ConstituencyName = DMdl.ConstituencyName;
                        OLQuesObj.ConstituencyNo = DMdl.ConstituencyCode.ToString();
                        OLQuesObj.QuestionType = (int)QuestionType.UnstaredQuestion;
                        OLQuesObj.SubmitToVS = false;
                        OLQuesObj.QuestPdfLang = update.QuestPdfLang;
                        pCtxt.SaveChanges();
                        pCtxt.Close();

                        update.Message = "Updated";

                    }
                }

            }


            catch (Exception ex)
            {
                update.Message = ex.Message;
            }



            return update;
        }
        static tMemberNotice GetDetailsForOnlinePDF(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.OnlineQuestions
                         where questions.QuestionType == 1 && questions.MainQuestionID == model.QuestionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.MemberSubmitDate,
                             NoticeTime = questions.MemberSubmitTime,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             ConstituencyName = questions.ConstituencyName,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        static tMemberNotice GetDetailsForOnlineUnstarredPDF(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.OnlineQuestions
                         where questions.QuestionType == 2 && questions.MainQuestionID == model.QuestionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.MemberSubmitDate,
                             NoticeTime = questions.MemberSubmitTime,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             ConstituencyName = questions.ConstituencyName,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }
        public static object GetQuestionDetailById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();
            var Path = "";

            var record = (from Ques in DiaCtx.OnlineQuestions
                          join evt in DiaCtx.objmEvent on Ques.EventId equals evt.EventId
                          join Qest in DiaCtx.objQuestion on Ques.MainQuestionID equals Qest.QuestionID into t
                          from rt in t.DefaultIfEmpty()
                          where Ques.QuestionID == Obj.QuestionId
                          select new DiaryModel
                          {
                              QuestionId = Ques.QuestionID,
                              AssemblyID = Ques.AssemblyID,
                              SessionID = Ques.SessionID,
                              QuestionTypeId = Ques.QuestionType,
                              DiaryNumber = Ques.DiaryNumber,
                              ConstituencyName = Ques.ConstituencyName,
                              CatchWordSubject = Ques.SubInCatchWord,
                              Subject = Ques.Subject,
                              MinistryId = Ques.MinistryId,
                              DepartmentId = Ques.DepartmentID,
                              MainQuestion = Ques.MainQuestion,
                              DateForBind = Ques.MemberSubmitDate,
                              EventName = evt.EventName,
                              Priority = Ques.Priority,
                              IsSubmitToVS = Ques.SubmitToVS,
                              FilePath = Ques.OriMemFilePath,
                              FileName = Ques.OriMemFileName,
                              QuestionNumber = rt.QuestionNumber,
                              IsFinalApprove = rt.IsFinalApproved,
                              SessionDateId = rt.SessionDateId,
                              FixedDate = (from M in DiaCtx.objSessDate where M.Id == rt.SessionDateId select M.SessionDate).FirstOrDefault(),
                              QuestionStatus = rt.QuestionType,
                          }).FirstOrDefault();
            if (record.IsFinalApprove == true)
            {

                if (record.QuestionStatus == 1)
                {
                    Path = (from M in DiaCtx.objSessDate where M.Id == record.SessionDateId select M.StartQListPath).FirstOrDefault();

                }
                else if (record.QuestionStatus == 2)
                {
                    Path = (from M in DiaCtx.objSessDate where M.Id == record.SessionDateId select M.UnStartQListPath).FirstOrDefault();
                }


            }

            var MinMisList = "";
            var Dept = "";
            if (record.MinistryId != null && record.MinistryId != 0)
            {
                MinMisList = (from Min in DiaCtx.objmMinistry
                              where Min.MinistryID == record.MinistryId
                              select Min.MinisterName).FirstOrDefault();

            }


            if (record.DepartmentId != null && record.DepartmentId != "")
            {
                Dept = (from dept in DiaCtx.objmDepartment
                        where dept.deptId == record.DepartmentId
                        select dept.deptname).FirstOrDefault();
            }

            Obj2 = record;
            Obj2.MinisterName = MinMisList.ToString().Trim();
            Obj2.DepartmentName = Dept.ToString().Trim();
            Obj2.StarredQListPath = Path;
            // For Accessing File Path 
            Obj2.AcessPath = (from SS in DiaCtx.mSiteSettings
                              where SS.SettingName == "SecureFileAccessingUrlPath"
                              select SS.SettingValue).FirstOrDefault();

            if (record.QuestionStatus == 1)
            {
                Obj2.Type = "Starred Question";
            }
            else if (record.QuestionStatus == 2)
            {
                Obj2.Type = "UnStarred Question";
            }
            DiaCtx.Close();

            return Obj2;
        }
        public static object SaveUpdateNotice(object param)
        {
            DiaryModel update = param as DiaryModel;
            tMemberNotice NtcsObj = new tMemberNotice();
            OnlineNotices OLNObj = new OnlineNotices();
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel DMdl = new DiaryModel();

            //Get Constituency Number
            DMdl = (DiaryModel)LegislationFixationContext.GetConstByMemberId(update);

            try
            {
                if (update.PaperEntryType == "NewEntry")
                {
                    if (update.BtnCaption == "Submit")
                    {
                        //Get Notice Number
                        update.NoticeNumber = (string)DiariesContext.CheckNoticeNumber(update);

                        // Add record in tMemberNotice Table
                        if (update.ChkLocalLang == "Local")
                        {
                            NtcsObj.IsHindi = true;
                        }
                        else
                        {
                            NtcsObj.IsHindi = null;
                        }

                        NtcsObj.AssemblyID = update.AssemblyID;
                        NtcsObj.SessionID = update.SessionID;
                        NtcsObj.NoticeNumber = update.NoticeNumber;
                        NtcsObj.Notice = update.Notice;
                        NtcsObj.Subject = update.Subject;
                        NtcsObj.SubInCatchWord = update.Subject;
                        NtcsObj.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
                        NtcsObj.Priority = update.Priority;
                        NtcsObj.MinistryId = update.MinistryId;
                        NtcsObj.MemberId = update.MemberId;
                        NtcsObj.NoticeTypeID = update.EventId;
                        NtcsObj.NoticeDate = DateTime.Now.Date;
                        NtcsObj.NoticeTime = DateTime.Now.TimeOfDay;
                        NtcsObj.IsOnlineSubmitted = true;
                        NtcsObj.IsOnlineSubmittedDate = DateTime.Now;
                        pCtxt.tMemberNotices.Add(NtcsObj);
                        pCtxt.SaveChanges();

                        // Add record in OnlineNotice
                        OLNObj.ManualNoticeId = NtcsObj.NoticeId;
                        OLNObj.NoticeNumber = update.NoticeNumber;
                        OLNObj.AssemblyId = update.AssemblyID;
                        OLNObj.SessionId = update.SessionID;
                        OLNObj.MemberID = update.MemberId;
                        OLNObj.NoticeTypeId = update.EventId;
                        OLNObj.Subject = update.Subject;
                        OLNObj.NoticeDetail = update.Notice;
                        OLNObj.MinistryId = update.MinistryId;
                        OLNObj.MemberSubmitDate = DateTime.Now;
                        OLNObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        if (update.ChkLocalLang == "Local")
                        {
                            OLNObj.IsHindi = true;
                        }
                        else
                        {
                            OLNObj.IsHindi = null;
                        }
                        OLNObj.SubmitToVS = true;
                        OLNObj.AttachedFileName = update.FileName;
                        OLNObj.AttachedFilePath = update.FilePath;
                        pCtxt.OLNotices.Add(OLNObj);
                        pCtxt.SaveChanges();

                        //Send NoticeId to Controller
                        update.NoticeId = OLNObj.OnlineNoticeId;
                        update.ConstituencyCode = DMdl.ConstituencyCode;
                        update.ConstituencyName = DMdl.ConstituencyName;
                        pCtxt.Close();
                        update.Message = "Submitted";
                    }

                    else if (update.BtnCaption == "Save")
                    {
                        if (update.ChkLocalLang == "Local")
                        {
                            OLNObj.IsHindi = true;
                        }
                        else
                        {
                            OLNObj.IsHindi = null;
                        }

                        // Add record in OnlineNotice Table
                        OLNObj.ManualNoticeId = NtcsObj.NoticeId;
                        OLNObj.NoticeNumber = update.NoticeNumber;
                        OLNObj.AssemblyId = update.AssemblyID;
                        OLNObj.SessionId = update.SessionID;
                        OLNObj.MemberID = update.MemberId;
                        OLNObj.NoticeTypeId = update.EventId;
                        OLNObj.Subject = update.Subject;
                        OLNObj.NoticeDetail = update.Notice;
                        OLNObj.MinistryId = update.MinistryId;
                        OLNObj.DemandId = update.DemandNo;
                        OLNObj.DepartmentID = update.DepartmentId;
                        //OLNObj.MemberSubmitDate = DateTime.Now;
                        //OLNObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        OLNObj.SubmitToVS = false;
                        OLNObj.AttachedFileName = update.FileName;
                        OLNObj.AttachedFilePath = update.FilePath;
                        pCtxt.OLNotices.Add(OLNObj);
                        pCtxt.SaveChanges();

                        update.Message = "Saved";
                    }
                }

                else if (update.PaperEntryType == "UpdateEntry")
                {
                    if (update.BtnCaption == "Submit")
                    {

                        //Get Notice Number
                        update.NoticeNumber = (string)DiariesContext.CheckNoticeNumber(update);

                        // Add record in tMemberNotice Table
                        if (update.ChkLocalLang == "Local")
                        {
                            NtcsObj.IsHindi = true;
                        }
                        else
                        {
                            NtcsObj.IsHindi = null;
                        }
                        NtcsObj.AssemblyID = update.AssemblyID;
                        NtcsObj.SessionID = update.SessionID;
                        NtcsObj.NoticeNumber = update.NoticeNumber;
                        NtcsObj.Notice = update.Notice;
                        NtcsObj.Subject = update.Subject;
                        NtcsObj.SubInCatchWord = update.Subject;
                        NtcsObj.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
                        NtcsObj.Priority = update.Priority;
                        NtcsObj.MinistryId = update.MinistryId;
                        NtcsObj.MemberId = update.MemberId;
                        NtcsObj.NoticeTypeID = update.EventId;
                        NtcsObj.NoticeDate = DateTime.Now.Date;
                        NtcsObj.NoticeTime = DateTime.Now.TimeOfDay;
                        NtcsObj.IsOnlineSubmitted = true;
                        NtcsObj.IsOnlineSubmittedDate = DateTime.Now;
                        pCtxt.tMemberNotices.Add(NtcsObj);
                        pCtxt.SaveChanges();

                        // Update record in OnlineNotice
                        OLNObj = pCtxt.OLNotices.Single(m => m.OnlineNoticeId == update.NoticeId);
                        pCtxt.OLNotices.Attach(OLNObj);
                        OLNObj.ManualNoticeId = NtcsObj.NoticeId;
                        OLNObj.NoticeNumber = update.NoticeNumber;
                        OLNObj.AssemblyId = update.AssemblyID;
                        OLNObj.SessionId = update.SessionID;
                        OLNObj.MemberID = update.MemberId;
                        OLNObj.NoticeTypeId = update.EventId;
                        OLNObj.Subject = update.Subject;
                        OLNObj.NoticeDetail = update.Notice;
                        OLNObj.MinistryId = update.MinistryId;
                        OLNObj.MemberSubmitDate = DateTime.Now;
                        OLNObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        if (update.ChkLocalLang == "Local")
                        {
                            OLNObj.IsHindi = true;
                        }
                        else
                        {
                            OLNObj.IsHindi = null;
                        }
                        OLNObj.SubmitToVS = true;
                        OLNObj.AttachedFileName = update.FileName;
                        OLNObj.AttachedFilePath = update.FilePath;
                        pCtxt.SaveChanges();

                        //Send NoticeId to Controller
                        update.NoticeId = OLNObj.OnlineNoticeId;
                        update.ConstituencyCode = DMdl.ConstituencyCode;
                        update.ConstituencyName = DMdl.ConstituencyName;
                        pCtxt.Close();

                        update.Message = "Submitted";
                    }

                    else if (update.BtnCaption == "Save")
                    {
                        // update record in OnlineNotice Table
                        OLNObj = pCtxt.OLNotices.Single(m => m.OnlineNoticeId == update.NoticeId);
                        pCtxt.OLNotices.Attach(OLNObj);
                        OLNObj.ManualNoticeId = NtcsObj.NoticeId;
                        OLNObj.NoticeNumber = update.NoticeNumber;
                        OLNObj.AssemblyId = update.AssemblyID;
                        OLNObj.SessionId = update.SessionID;
                        OLNObj.MemberID = update.MemberId;
                        OLNObj.NoticeTypeId = update.EventId;
                        OLNObj.Subject = update.Subject;
                        OLNObj.NoticeDetail = update.Notice;
                        OLNObj.MinistryId = update.MinistryId;
                        OLNObj.DemandId = update.DemandNo;
                        OLNObj.DepartmentID = update.DepartmentId;
                        if (update.ChkLocalLang == "Local")
                        {
                            OLNObj.IsHindi = true;
                        }
                        else
                        {
                            OLNObj.IsHindi = null;
                        }
                        //OLNObj.MemberSubmitDate = DateTime.Now;
                        //OLNObj.MemberSubmitTime = DateTime.Now.TimeOfDay;
                        OLNObj.SubmitToVS = false;
                        OLNObj.AttachedFileName = update.FileName;
                        OLNObj.AttachedFilePath = update.FilePath;
                        pCtxt.SaveChanges();

                        update.Message = "Updated";

                    }
                }
            }
            catch (Exception ex)
            {
                update.Message = ex.Message;
            }
            return update;
        }
        public static object GetNoticeDetailById(object param)
        {

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();

            var record = (from Ntcs in DiaCtx.OLNotices
                          join evt in DiaCtx.objmEvent on Ntcs.NoticeTypeId equals evt.EventId
                          where Ntcs.OnlineNoticeId == Obj.NoticeId
                          select new DiaryModel
                          {
                              NoticeId = Ntcs.OnlineNoticeId,
                              AssemblyID = Ntcs.AssemblyId,
                              SessionID = Ntcs.SessionId,
                              Subject = Ntcs.Subject,
                              MinistryId = Ntcs.MinistryId,
                              DateForBind = Ntcs.MemberSubmitDate,
                              EventName = evt.EventName,
                              IsSubmitToVS = Ntcs.SubmitToVS,
                              AttachFilePath = Ntcs.AttachedFilePath,
                              AttachFileName = Ntcs.AttachedFileName,
                              FileName = Ntcs.OriMemFileName,
                              FilePath = Ntcs.OriMemFilePath,
                              NoticeNumber = Ntcs.NoticeNumber
                          }).FirstOrDefault();

            var MinMisList = "";
            if (record.MinistryId != null && record.MinistryId != 0)
            {
                MinMisList = (from Min in DiaCtx.objmMinistry
                              where Min.MinistryID == record.MinistryId
                              select Min.MinisterName).FirstOrDefault();

            }
            Obj2 = record;
            Obj2.MinisterName = MinMisList.ToString().Trim();
            DiaCtx.Close();

            return Obj2;
        }
        public static object GetNoticeByIdForEdit(object param)
        {
            DiaryModel model = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            UserContext Uctx = new UserContext();

            var signpath = (from U in Uctx.mUsers
                            where U.UserId == model.UserId
                            select U.SignaturePath).FirstOrDefault();



            var query = (from Ntcs in DiaCtx.OLNotices
                         where (Ntcs.OnlineNoticeId == model.NoticeId)
                         select new DiaryModel
                         {
                             AssemblyID = Ntcs.AssemblyId,
                             SessionID = Ntcs.SessionId,
                             NoticeId = Ntcs.OnlineNoticeId,
                             Subject = Ntcs.Subject,
                             MemberId = Ntcs.MemberID,
                             MinistryId = Ntcs.MinistryId,
                             IsHindi = Ntcs.IsHindi,
                             EventId = Ntcs.NoticeTypeId,
                             FilePath = Ntcs.AttachedFilePath,
                             FileName = Ntcs.AttachedFileName,
                             Notice = Ntcs.NoticeDetail,
                             DemandNo = Ntcs.DemandId,
                             DepartmentId = Ntcs.DepartmentID
                         }).FirstOrDefault();

            model = query;

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 4
                           select Evnt).ToList();
            model.eventList = EvtList;

            /*Get All MinistryMinister*/
            var MinMisList = (from Min in DiaCtx.objmMinistry
                              join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
                              orderby Min.OrderID
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
                              }).ToList();
            model.memMinList = MinMisList;
            /*Get All Demand*/
            var DemandList = (from Demand in DiaCtx.tCutMotionDemand
                              select Demand).ToList();
            var DeptList = (from dept in DiaCtx.objmDepartment
                            select dept).ToList();
            tCutMotionDemand obj = new tCutMotionDemand();
            obj.DemandName = "Select Demand";
            obj.DemandNo = 0;
            DemandList.Add(obj);
            model.DemandList = DemandList;
            model.DeptList = DeptList;
            model.SignFilePath = Convert.ToString(signpath);

            return model;
        }
        static object UpdateNoticeFile(object param)
        {
            DiariesContext DCtxt = new DiariesContext();
            DiaryModel ObjD = param as DiaryModel;
            OnlineNotices OLNObj = new OnlineNotices();
            try
            {
                OLNObj = DCtxt.OLNotices.Single(d => d.OnlineNoticeId == ObjD.NoticeId);
                //Update in Online Question Table
                DCtxt.OLNotices.Attach(OLNObj);
                OLNObj.OriMemFilePath = ObjD.FilePath;
                OLNObj.OriMemFileName = ObjD.FileName;
                DCtxt.SaveChanges();
                DCtxt.Close();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Updated";

        }

        static object SubmitNoticeById(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel ObjD = param as DiaryModel;
            OnlineNotices OLNts = new OnlineNotices();
            tMemberNotice NtsObj = new tMemberNotice();
            DiaryModel DMdl = new DiaryModel();
            var Dname = "";
            var FL = (from SS in pCtxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();

            try
            { // Add record in tMemberNotice Table
                OLNts = pCtxt.OLNotices.Single(d => d.OnlineNoticeId == ObjD.NoticeId);

                ObjD.MemberId = OLNts.MemberID;
                ObjD.EventId = OLNts.NoticeTypeId;

                //Get Constituency Number
                DMdl = (DiaryModel)LegislationFixationContext.GetConstByMemberId(ObjD);

                //Get Notice Number
                ObjD.NoticeNumber = (string)DiariesContext.CheckNoticeNumber(ObjD);

                if (OLNts.DemandId != null || OLNts.DemandId != 0)
                {
                    Dname = (from SS in pCtxt.tCutMotionDemand
                             where SS.DemandNo == OLNts.DemandId
                             select SS.DemandName).FirstOrDefault();
                }

                if (OLNts.IsHindi == true)
                {
                    NtsObj.IsHindi = true;
                }
                NtsObj.AssemblyID = OLNts.AssemblyId;
                NtsObj.SessionID = OLNts.SessionId;
                NtsObj.NoticeNumber = ObjD.NoticeNumber;
                NtsObj.Notice = OLNts.NoticeDetail;
                NtsObj.Subject = OLNts.Subject;
                NtsObj.SubInCatchWord = OLNts.Subject;
                NtsObj.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
                NtsObj.MinistryId = OLNts.MinistryId;
                NtsObj.MemberId = OLNts.MemberID;
                NtsObj.NoticeTypeID = OLNts.NoticeTypeId;
                NtsObj.IsOnlineSubmitted = true;
                NtsObj.IsOnlineSubmittedDate = DateTime.Now;

                NtsObj.NoticeDate = DateTime.Today.Date;
                NtsObj.NoticeTime = DateTime.Now.TimeOfDay;
                if (OLNts.NoticeTypeId == 39)
                {
                    NtsObj.TypistRequired = true;
                    NtsObj.NoticeStatus = (int)NoticeStatusEnum.NoticeToTypist;
                }
                if (OLNts.NoticeTypeId != 39)
                {
                    NtsObj.TypistRequired = null;
                }
                if (OLNts.DemandId != null || OLNts.DemandId != 0)
                {
                    NtsObj.DemandID = OLNts.DemandId;
                }
                pCtxt.tMemberNotices.Add(NtsObj);
                pCtxt.SaveChanges();

                //Update Status in Online Notice 
                OLNts.SubmitToVS = true;
                OLNts.ManualNoticeId = NtsObj.NoticeId;
                OLNts.NoticeNumber = ObjD.NoticeNumber;
                OLNts.MemberSubmitDate = DateTime.Now;
                OLNts.MemberSubmitTime = DateTime.Now.TimeOfDay;
                pCtxt.SaveChanges();
                pCtxt.Close();


                //Send Field Value to Generate Pdf
                ObjD.SaveFilePath = FL;
                ObjD.Message = "Success";
                ObjD.NoticeId = OLNts.OnlineNoticeId;

                ObjD.ConstituencyCode = DMdl.ConstituencyCode;
                ObjD.ConstituencyName = DMdl.ConstituencyName;
                ObjD.Notice = OLNts.NoticeDetail;
                ObjD.Subject = OLNts.Subject;
                var currdate = Convert.ToDateTime(NtsObj.IsOnlineSubmittedDate).ToString("dd/MM/yyyy");
                var currTime = Convert.ToDateTime(DateTime.Now).ToString("hh:mm tt");

                ObjD.PDfDate = currdate;
                ObjD.PDfTime = currTime;
                if (OLNts.DemandId != null || OLNts.DemandId != 0)
                {
                    ObjD.DemandNo = OLNts.DemandId;
                    ObjD.DemandName = Dname;
                }


            }
            catch (Exception ex)
            {
                ObjD.Message = ex.Message;
            }

            return ObjD;
        }



        static object GetMemberMinisterList()
        {
            DiaryModel model = new DiaryModel();
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettingContext settingContext = new SiteSettingContext();

            var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            int AId = Convert.ToInt16(AssemblyCode);

            var MinMisList = (from Min in DiaCtx.objmMinistry
                              join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
                              where Min.AssemblyID == AId
                              orderby Min.OrderID
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
                              }).ToList();
            model.memMinList = MinMisList;

            return model;

        }

        static object GetDynamicMainMenuByUserId(object param)

        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();
            ModuleContext MCtx = new ModuleContext();


            List<mUserModules> LUM = new List<mUserModules>();
            //Get Main Menu Dynamically
            var MenuList = (from M in MCtx.tUserAccessRequest
                            where M.UserID == model.UserID && M.IsAcceptedDate != null
                            select new { M.AccessID, M.ActionControlId, M.SubAccessByUser, M.AccessOrder, M.MemberId }
                    ).ToList();
            //-------Procedure based start-----
            //SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-S5ELBLU\MSSQLSERVER2014;Initial Catalog=HPMS_IntranetApp;Persist Security Info=True;User ID=sa;Password=123");
            //SqlCommand command = new SqlCommand("select MajorProcessID, MajorProcessName from mMajorProcess ;", con);
            //if (con.State == ConnectionState.Closed)
            //    con.Open();
            //SqlDataReader reader = command.ExecuteReader();
            //if (reader.HasRows)
            //{
            //    List<KeyValuePair<string, string>> methodParameter = new List<KeyValuePair<string, string>>();

            //    while (reader.Read())
            //    {
            //        methodParameter.Add(new KeyValuePair<string,string>("@PaperCategoryTypeId", "1"));
            //    }
            //}
            //-------Procedure end-----
            if (MenuList != null)
            {
                foreach (var item in MenuList)
                {
                    if (item.AccessID != 74)
                    {


                        var MenuName = (from UA in MCtx.tUserAccessActions
                                        join UM in MCtx.mUserModules on UA.ModuleId equals UM.ModuleId
                                        where UA.UserAccessActionsId == item.AccessID
                                        select new { UM.ModuleId, UM.ModuleName, UM.ModuleOrder, UA.AccessActionOrder }).FirstOrDefault();


                        mUserModules Umodule = new mUserModules();
                        Umodule.ModuleId = MenuName.ModuleId;
                        Umodule.ModuleName = MenuName.ModuleName;
                        Umodule.ActionIds = item.ActionControlId;
                        Umodule.SubUserTypeName = item.SubAccessByUser;//this is use for SubAcess
                        Umodule.ModuleOrder = MenuName.AccessActionOrder;
                        Umodule.MemberId = item.MemberId;
                        LUM.Add(Umodule);
                    }
                }
            }
            model.MenuList = LUM.OrderBy(x => x.ModuleOrder).ToList();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionID && m.AssemblyID == model.AssemblyID select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyID select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;

            model.mAssemblyList = (from A in pCtxt.mAssemblies
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSessions
                                 where S.AssemblyID == model.AssemblyID
                                 select S).ToList();

            return model;
        }

        static object GetSubMenuByMenuIdHindi(object param)
        {
            mUserSubModules model = param as mUserSubModules;
            ModuleContext MCtx = new ModuleContext();
            List<mUserSubModules> USMList = new List<mUserSubModules>();
            //Get Sub Menu Dynamically
            var SubMenuList = (from M in MCtx.mUserSubModules
                               join UM in MCtx.mUserModules on M.ModuleId equals UM.ModuleId
                               where M.ModuleId == model.ModuleId
                               select new { M.SubModuleId, M.SubModuleNameLocal, UM.ModuleNameLocal, UM.ModuleId }).ToList();
            if (SubMenuList != null)
            {
                foreach (var item in SubMenuList)
                {
                    mUserSubModules USM = new mUserSubModules();
                    USM.SubModuleId = item.SubModuleId;
                    USM.SubModuleNameLocal = item.SubModuleNameLocal;
                    USM.ModuleId = item.ModuleId;
                    USM.ModuleNameLocal = item.ModuleNameLocal;
                    USMList.Add(USM);
                }
            }
            model.AccessList = USMList;
            return model;
        }
        static object GetSubMenuByMenuId(object param)
        {
            mUserSubModules model = param as mUserSubModules;
            ModuleContext MCtx = new ModuleContext();
            List<mUserSubModules> USMList = new List<mUserSubModules>();

            //Get Sub Menu Dynamically
            var QueryResultUserAction = (from t in MCtx.tUserAccessActions
                                         where t.ModuleId == model.ModuleId
           && t.UserTypeID == model.UserTypeID
           && t.SubUserTypeID == model.SubUserTypeID
                                         select t.UserAccessActionsId).FirstOrDefault();
            //var QueryResult = (from t in MCtx.tUserAccessRequest where t.AccessID == QueryResultUserAction select t.SubAccessByUser).FirstOrDefault();
            var QueryResult = (from t in MCtx.tUserAccessRequest
                               where t.AccessID == QueryResultUserAction
                               orderby t.IsAcceptedDate descending
                               select t.SubAccessByUser).FirstOrDefault();

            if (QueryResult != null && QueryResult != "")
            {
                if (!string.IsNullOrEmpty(QueryResult))
                {
                    QueryResult = QueryResult.TrimEnd(',');
                }
                List<string> StrTagIds = QueryResult.Split(',').ToList();
                List<int> TagIds = new List<int>();
                foreach (var item in StrTagIds)
                {
                    int num;
                    bool isNum = Int32.TryParse(item, out num);
                    if (isNum)
                    {
                        TagIds.Add(int.Parse(item));
                    }

                }

                //TagIds = QueryResult.Split(',').Select(int.Parse).ToList();

                var SubMenuList = (from M in MCtx.mUserSubModules
                                   join UM in MCtx.mUserModules on M.ModuleId equals UM.ModuleId
                                   where M.ModuleId == model.ModuleId
                                   && TagIds.Contains(M.SubModuleId)
                                   select new { M.SubModuleId, M.SubModuleName, M.SubModuleOrder, UM.ModuleName, UM.ModuleId }).ToList();
                if (SubMenuList != null)
                {
                    foreach (var item in SubMenuList)
                    {
                        mUserSubModules USM = new mUserSubModules();
                        USM.SubModuleId = item.SubModuleId;
                        USM.SubModuleName = item.SubModuleName;
                        USM.ModuleId = item.ModuleId;
                        USM.ModuleName = item.ModuleName;
                        USM.SubModuleOrder = item.SubModuleOrder;
                        USMList.Add(USM);
                    }
                }
                model.AccessList = USMList.OrderBy(x => x.SubModuleOrder).ToList();
            }
            else
            {
                var SubMenuList = (from M in MCtx.mUserSubModules
                                   join UM in MCtx.mUserModules on M.ModuleId equals UM.ModuleId
                                   where M.ModuleId == model.ModuleId && M.Isactive == true
                                   select new { M.SubModuleId, M.SubModuleName, M.SubModuleOrder, UM.ModuleName, UM.ModuleId }).ToList();
                if (SubMenuList != null)
                {
                    foreach (var item in SubMenuList)
                    {
                        mUserSubModules USM = new mUserSubModules();
                        USM.SubModuleId = item.SubModuleId;
                        USM.SubModuleName = item.SubModuleName;
                        USM.ModuleId = item.ModuleId;
                        USM.ModuleName = item.ModuleName;
                        USM.SubModuleOrder = item.SubModuleOrder;
                        USMList.Add(USM);
                    }
                }
                model.AccessList = USMList.OrderBy(x => x.SubModuleOrder).ToList();
            }

            return model;
        }

        static object GetTotalOnlineSubmissionCnt(object param)
        {
            mUserModules obj = param as mUserModules;
            NoticeContext pCtxt = new NoticeContext();
            ModuleContext MCtx = new ModuleContext();

            int Cnt = 0;
            //Get Sub Menu Dynamically

            var SubMenuList = (from M in MCtx.mUserSubModules
                               where M.ModuleId == obj.ModuleId
                               select new { M.SubModuleId, M.SubModuleName }).ToList();

            foreach (var item in SubMenuList)
            {
                if (item.SubModuleName == "Starred Questions")
                {
                    var result = (DiaryModel)GetTotalOnlineStarredCnt(obj.MemberId);
                    Cnt += result.ResultCount;
                }
                else if (item.SubModuleName == "Unstarred Questions")
                {
                    var result = (DiaryModel)GetTotalUnstarredCnt(obj.MemberId);
                    Cnt += result.ResultCount;
                }
                else if (item.SubModuleName == "Notices")
                {
                    var result = (DiaryModel)GetTotalNoticesCnt(obj.MemberId);
                    Cnt += result.ResultCount;
                }

            }
            obj.TotalCount = Cnt;
            return obj;
        }
        static int GetTotalTourCnt(object param)
        {
            int id = (int)param;
            ATourContext ACtx = new ATourContext();
            DateTime datetime = DateTime.Now.Date;
            //Get Count for Tour Programme
            var TodayTour = (from AT in ACtx.tMemberTour
                             where (AT.MemberId == id && AT.IsPublished == true)
                             && (AT.TourFromDateTime >= datetime && AT.TourToDateTime >= datetime)
                             select AT.TourId).Count();

            var UpComingTour = (from AT in ACtx.tMemberTour
                                where (AT.MemberId == id && AT.IsPublished == true)
                                && (AT.TourFromDateTime > datetime || AT.TourToDateTime > datetime)
                                select AT.TourId).Count();

            var PreviousTour = (from AT in ACtx.tMemberTour
                                where (AT.MemberId == id && AT.IsPublished == true)
                                && (AT.TourFromDateTime < datetime || AT.TourToDateTime < datetime)
                                select AT.TourId).Count();

            var UnpublishedTour = (from AT in ACtx.tMemberTour
                                   where (AT.MemberId == id && AT.IsPublished == false)
                                   && (AT.TourFromDateTime <= datetime || AT.TourToDateTime >= datetime)
                                   select AT.TourId).Count();

            //model.AllMemberTourCount = TodayTour + UpComingTour + PreviousTour;
            //model.TodayTourCount = TodayTour;
            //model.UpcomingTourCount = UpComingTour;

            return TodayTour + UpComingTour + PreviousTour + UnpublishedTour;
        }
        static int GetTotalGalleryCnt(object param)
        {

            return 0;
        }


        static object GetTotalOnlineStarredCnt(object param)
        {
            DiaryModel DM = param as DiaryModel;
            NoticeContext pCtxt = new NoticeContext();
            // Get Count for Question
            var OLSCnt = (from questions in pCtxt.OnlineQuestions
                          where questions.MemberID == DM.MemberId
                          && questions.AssemblyID == DM.AssemblyID
                          && questions.SessionID == DM.SessionID
                          && questions.QuestionType == (int)QuestionType.StartedQuestion
                          select questions.QuestionID).Count();

            DM.ResultCount = OLSCnt;
            return DM;
        }
        static object GetTotalUnstarredCnt(object param)
        {
            DiaryModel DM = param as DiaryModel;
            NoticeContext pCtxt = new NoticeContext();

            var OLUCnt = (from questions in pCtxt.OnlineQuestions
                          where questions.MemberID == DM.MemberId
                          && questions.AssemblyID == DM.AssemblyID
                          && questions.SessionID == DM.SessionID
                          && questions.QuestionType == (int)QuestionType.UnstaredQuestion
                          select questions.QuestionID).Count();

            DM.ResultCount = OLUCnt;
            return DM;
        }
        static object GetTotalNoticesCnt(object param)
        {
            DiaryModel DM = param as DiaryModel;
            NoticeContext pCtxt = new NoticeContext();

            var OLNCnt = (from Ntcs in pCtxt.OLNotices
                          where Ntcs.MemberID == DM.MemberId
                          && Ntcs.AssemblyId == DM.AssemblyID
                          && Ntcs.SessionId == DM.SessionID
                          select Ntcs.OnlineNoticeId).Count();

            DM.ResultCount = OLNCnt;
            return DM;
        }


        static object GetSearchDataByType(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel DM = param as DiaryModel;
            OnlineMemberQmodel model = new OnlineMemberQmodel();



            if (DM.PaperEntryType == "Starred Questions")
            {
                var query = (from questions in pCtxt.OnlineQuestions
                             where (questions.QuestionType == (int)QuestionType.StartedQuestion)
                             && (DM.Subject == "" || questions.Subject.Contains(DM.Subject))
                             && (DM.MinistryId == 0 || questions.MinistryId == DM.MinistryId)
                             && (DM.DiaryNumber == "" || questions.DiaryNumber == DM.DiaryNumber)
                             && (questions.MemberID == DM.MemberId)
                             && (questions.AssemblyID == DM.AssemblyID)
                             && (questions.SessionID == DM.SessionID)
                             && (DM.FromDate_DateTime == null || questions.MemberSubmitDate.Value.Year >= DM.FromDate_DateTime.Value.Year && questions.MemberSubmitDate.Value.Month >= DM.FromDate_DateTime.Value.Month && questions.MemberSubmitDate.Value.Day >= DM.FromDate_DateTime.Value.Day)
                             && (DM.ToDate_DateTime == null || questions.MemberSubmitDate.Value.Year <= DM.ToDate_DateTime.Value.Year && questions.MemberSubmitDate.Value.Month <= DM.ToDate_DateTime.Value.Month && questions.MemberSubmitDate.Value.Day <= DM.ToDate_DateTime.Value.Day)

                             select new OnlineMemberQmodel
                             {
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.MemberSubmitDate,
                             }).ToList();

                model.ResultCount = query.Count();
                model.tQuestionModel = query;
            }
            else if (DM.PaperEntryType == "Unstarred Questions")
            {
                var query = (from questions in pCtxt.OnlineQuestions
                             where (questions.QuestionType == (int)QuestionType.UnstaredQuestion)
                             && (DM.Subject == "" || questions.Subject.Contains(DM.Subject))
                             && (DM.MinistryId == 0 || questions.MinistryId == DM.MinistryId)
                             && (DM.DiaryNumber == "" || questions.DiaryNumber == DM.DiaryNumber)
                             && (questions.MemberID == DM.MemberId)
                             && (questions.AssemblyID == DM.AssemblyID)
                             && (questions.SessionID == DM.SessionID)
                             && (DM.FromDate_DateTime == null || questions.MemberSubmitDate.Value.Year >= DM.FromDate_DateTime.Value.Year && questions.MemberSubmitDate.Value.Month >= DM.FromDate_DateTime.Value.Month && questions.MemberSubmitDate.Value.Day >= DM.FromDate_DateTime.Value.Day)
                             && (DM.ToDate_DateTime == null || questions.MemberSubmitDate.Value.Year <= DM.ToDate_DateTime.Value.Year && questions.MemberSubmitDate.Value.Month <= DM.ToDate_DateTime.Value.Month && questions.MemberSubmitDate.Value.Day <= DM.ToDate_DateTime.Value.Day)

                             select new OnlineMemberQmodel
                             {
                                 QuestionID = questions.QuestionID,
                                 DiaryNumber = questions.DiaryNumber,
                                 Subject = questions.Subject,
                                 NoticeDate = questions.MemberSubmitDate,
                             }).ToList();

                model.ResultCount = query.Count();
                model.tQuestionModel = query;
            }
            else if (DM.PaperEntryType == "Notices")
            {
                var query = (from Ntcs in pCtxt.OLNotices
                             where Ntcs.MemberID == DM.MemberId
                             && (DM.Subject == "" || Ntcs.Subject.Contains(DM.Subject))
                             && (DM.MinistryId == 0 || Ntcs.MinistryId == DM.MinistryId)
                             && (DM.DiaryNumber == "" || Ntcs.NoticeNumber == DM.DiaryNumber)
                             && (Ntcs.AssemblyId == DM.AssemblyID)
                             && (Ntcs.SessionId == DM.SessionID)
                             && (DM.FromDate_DateTime == null || Ntcs.MemberSubmitDate.Value.Year >= DM.FromDate_DateTime.Value.Year && Ntcs.MemberSubmitDate.Value.Month >= DM.FromDate_DateTime.Value.Month && Ntcs.MemberSubmitDate.Value.Day >= DM.FromDate_DateTime.Value.Day)
                             && (DM.ToDate_DateTime == null || Ntcs.MemberSubmitDate.Value.Year <= DM.ToDate_DateTime.Value.Year && Ntcs.MemberSubmitDate.Value.Month <= DM.ToDate_DateTime.Value.Month && Ntcs.MemberSubmitDate.Value.Day <= DM.ToDate_DateTime.Value.Day)

                             select new OnlineMemberQmodel
                             {
                                 NoticeId = Ntcs.OnlineNoticeId,
                                 NoticeNumber = Ntcs.NoticeNumber,
                                 Subject = Ntcs.Subject,
                                 NoticeDate = Ntcs.MemberSubmitDate,

                             }).ToList();
                model.ResultCount = query.Count();
                model.tQuestionModel = query;
            }

            return model;
        }

        static object SaveUpdateSignature(object param)
        {
            DiaryModel DiaryObj = param as DiaryModel;
            UserContext Uctx = new UserContext();
            mUsers UserObj = new mUsers();
            try
            {
                UserObj = Uctx.mUsers.Single(m => m.UserId == DiaryObj.UserId);
                Uctx.mUsers.Attach(UserObj);
                UserObj.SignaturePath = DiaryObj.SignFilePath;
                UserObj.Mobile = "122";
                UserObj.MailID = "sdsd@fdfgdf.com";
                UserObj.OTPValue = "fgdgrtr";
                //UserObj.AssemblyPassword = "Ab1@SD";
                Uctx.SaveChanges();
                Uctx.Close();
                return "Success";
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            {
                return dbEx.Message;
            }

        }

        static object GetSignPathByUserId(object param)
        {
            DiaryModel DiaryObj = param as DiaryModel;
            UserContext Uctx = new UserContext();
            mUsers UserObj = new mUsers();
            try
            {
                var SignPath = (from u in Uctx.mUsers
                                where u.UserId == DiaryObj.UserId
                                select u.SignaturePath).FirstOrDefault();
                Uctx.Close();
                if (SignPath == null)
                {
                    SignPath = string.Empty;
                }
                return SignPath;
            }
            catch (Exception Ex)
            {
                return Ex.Message;
            }
        }

        public static object GetStarredAndUnstarredCnt(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionID && m.AssemblyID == model.AssemblyID select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyID select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;

            model.mAssemblyList = (from A in pCtxt.mAssemblies
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSessions
                                 where S.AssemblyID == model.AssemblyID
                                 select S).ToList();
            if (model.RoleName == "VSTranslator")
            {

                var StarredCnt = (from Q in pCtxt.tQuestions
                                  where Q.AssemblyID == model.AssemblyID
                                  && Q.SessionID == model.SessionID
                                  && Q.QuestionType == (int)QuestionType.StartedQuestion
                                  && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                   && Q.IsFixed == true
                                  //&& Q.IsFinalApproved == null
                                  select Q).ToList().Count();

                var UnstarredCnt = (from Q in pCtxt.tQuestions
                                    where Q.AssemblyID == model.AssemblyID
                                    && Q.SessionID == model.SessionID
                                    && Q.QuestionType == (int)QuestionType.UnstaredQuestion
                                    && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                     && Q.IsFixed == true
                                    //&& Q.IsFinalApproved == null
                                    select Q).ToList().Count();

                model.TotalStaredReceived = StarredCnt;
                model.TotalUnStaredReceived = UnstarredCnt;
            }
            else if (model.RoleName == "Legislative1")
            {



                var StarredCnt = (from Q in pCtxt.tQuestions
                                  where Q.AssemblyID == model.AssemblyID
                                  && Q.SessionID == model.SessionID
                                  && Q.QuestionType == (int)QuestionType.StartedQuestion
                                  && Q.IsFinalApproved == null
                                    && Q.IsProofReading == true
                                  //&& Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                  select Q).ToList().Count();

                var UnstarredCnt = (from Q in pCtxt.tQuestions
                                    where Q.AssemblyID == model.AssemblyID
                                    && Q.SessionID == model.SessionID
                                     && Q.IsFinalApproved == null
                                        && Q.IsProofReading == true
                                    && Q.QuestionType == (int)QuestionType.UnstaredQuestion
                                    //&& Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                    select Q).ToList().Count();

                model.TotalStaredReceived = StarredCnt;
                model.TotalUnStaredReceived = UnstarredCnt;
            }
            else if (model.RoleName == "VS_Secretary")
            {



                var StarredCnt = (from Q in pCtxt.tQuestions
                                  where Q.AssemblyID == model.AssemblyID
                                  && Q.SessionID == model.SessionID
                                  && Q.QuestionType == (int)QuestionType.StartedQuestion
                                  //&& Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                  select Q).ToList().Count();

                var UnstarredCnt = (from Q in pCtxt.tQuestions
                                    where Q.AssemblyID == model.AssemblyID
                                    && Q.SessionID == model.SessionID
                                    && Q.QuestionType == (int)QuestionType.UnstaredQuestion
                                    //&& Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                    select Q).ToList().Count();

                model.TotalStaredReceived = StarredCnt;
                model.TotalUnStaredReceived = UnstarredCnt;
            }

            return model;

        }

        public static object GetStarredQuestionsForChanger(object param)
        {
            TypistQuestionModel model = param as TypistQuestionModel;
            NoticeContext pCtxt = new NoticeContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            if (model.RoleName == "VSTranslator")
            {
                if (model.DataStatus == "CQ")
                {
                    var StarredQ = (from Q in pCtxt.tQuestions
                                    where Q.AssemblyID == model.AssemblyID
                                    && Q.SessionID == model.SessionID
                                    && (model.SessionDateId == 0 || Q.SessionDateId == model.SessionDateId)
                                    && Q.QuestionType == (int)QuestionType.StartedQuestion
                                    && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                    && Q.IsFixed == true
                                    && Q.DeActivateFlag == null

                                    select new TypistQuestionModel
                                    {
                                        QuestionID = Q.QuestionID,
                                        DiaryNumber = Q.DiaryNumber,
                                        QuesNumber = Q.QuestionNumber,
                                        Subject = Q.Subject,
                                        NoticeDate = Q.NoticeRecievedDate,
                                        NoticeTime = Q.NoticeRecievedTime,
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                        Flag = Q.DeActivateFlag,
                                    }).ToList();

                    foreach (var item in StarredQ)
                    {

                        if (string.IsNullOrEmpty(model.CurrentQidsForlock))
                        {
                            model.CurrentQidsForlock = item.QuestionID.ToString();
                        }
                        else
                        {
                            model.CurrentQidsForlock = model.CurrentQidsForlock + "," + item.QuestionID.ToString();
                        }


                    }

                    if (model.SessionDateId != 0)
                    {
                        var CQfilepath = (from SD in pCtxt.mSessionDates
                                          where SD.AssemblyId == model.AssemblyID
                                          && SD.SessionId == model.SessionID
                                          && (SD.Id == model.SessionDateId)
                                          && (SD.ApprovedBySecrt == true)
                                          select SD.StartQListPath).FirstOrDefault();
                        model.NoticePath = CQfilepath;

                    }

                    model.StarredApprovedTrans = (from M in pCtxt.mSessionDates
                                                  where M.Id == model.SessionDateId
                                                  select M.ApprovedByTrans).FirstOrDefault();
                    model.ResultCount = StarredQ.Count();
                    model.tQuestionModel = StarredQ;

                }
                else if (model.DataStatus == "PQ")
                {
                    var StarredQ = (from Q in pCtxt.tQuestions
                                    where Q.AssemblyID == model.AssemblyID
                                    && Q.SessionID == model.SessionID
                                    && (model.SessionDateId == 0 || Q.SessionDateId == model.SessionDateId)
                                    && Q.QuestionType == (int)QuestionType.StartedQuestion
                                    && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                    && Q.IsFixed == true
                                    && Q.DeActivateFlag == "RF"
                                    select new TypistQuestionModel
                                    {
                                        QuestionID = Q.QuestionID,
                                        DiaryNumber = Q.DiaryNumber,
                                        QuesNumber = Q.QuestionNumber,
                                        Subject = Q.Subject,
                                        NoticeDate = Q.NoticeRecievedDate,
                                        NoticeTime = Q.NoticeRecievedTime,
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                        Flag = Q.DeActivateFlag,
                                    }).ToList();

                    foreach (var item in StarredQ)
                    {

                        if (string.IsNullOrEmpty(model.PostponeQidsForlock))
                        {
                            model.PostponeQidsForlock = item.QuestionID.ToString();
                        }
                        else
                        {
                            model.PostponeQidsForlock = model.PostponeQidsForlock + "," + item.QuestionID.ToString();
                        }


                    }

                    if (model.SessionDateId != 0)
                    {
                        var PQfilepath = (from SD in pCtxt.mSessionDates
                                          where SD.AssemblyId == model.AssemblyID
                                          && SD.SessionId == model.SessionID
                                          && (SD.Id == model.SessionDateId)
                                          && (SD.SPPApprovedBySecrt == true)
                                          select SD.SPPQListPath).FirstOrDefault();
                        model.NoticePath = PQfilepath;

                    }
                    model.StarredPQApprovedTrans = (from M in pCtxt.mSessionDates
                                                    where M.Id == model.SessionDateId
                                                    select M.SPPApprovedByTrans).FirstOrDefault();
                    model.ResultCount = StarredQ.Count();
                    model.tQuestionModel = StarredQ;
                }



            }
            else if (model.RoleName == "Legislative1")
            {
                var StarredQ = (from Q in pCtxt.tQuestions
                                where Q.AssemblyID == model.AssemblyID
                                && Q.SessionID == model.SessionID
                                && Q.QuestionType == (int)QuestionType.StartedQuestion
                                && Q.IsFinalApproved == null
                                && Q.IsProofReading == true

                                select new TypistQuestionModel
                                {
                                    QuestionID = Q.QuestionID,
                                    DiaryNumber = Q.DiaryNumber,
                                    QuesNumber = Q.QuestionNumber,
                                    Subject = Q.Subject,
                                    NoticeDate = Q.NoticeRecievedDate,
                                    NoticeTime = Q.NoticeRecievedTime,
                                    MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    Flag = Q.DeActivateFlag,
                                    //ProofReaderName = Q.ProofReaderUserId,

                                }).ToList();

                model.ResultCount = StarredQ.Count();
                model.tQuestionModel = StarredQ;
            }
            else if (model.RoleName == "Admin")
            {
                var StarredQ = (from Q in pCtxt.tQuestions
                                where Q.AssemblyID == model.AssemblyID
                                && Q.SessionID == model.SessionID
                                && Q.QuestionType == (int)QuestionType.StartedQuestion

                                select new TypistQuestionModel
                                {
                                    QuestionID = Q.QuestionID,
                                    DiaryNumber = Q.DiaryNumber,
                                    QuesNumber = Q.QuestionNumber,
                                    Subject = Q.Subject,
                                    NoticeDate = Q.NoticeRecievedDate,
                                    NoticeTime = Q.NoticeRecievedTime,
                                    MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    Flag = Q.DeActivateFlag,
                                    //ProofReaderName = Q.ProofReaderUserId,

                                }).ToList();

                model.ResultCount = StarredQ.Count();
                model.tQuestionModel = StarredQ;
            }
            else if (model.RoleName == "VS_Secretary")
            {
                var StarredQ = (from Q in pCtxt.tQuestions
                                where Q.AssemblyID == model.AssemblyID
                                && Q.SessionID == model.SessionID
                                && Q.QuestionType == (int)QuestionType.StartedQuestion

                                select new TypistQuestionModel
                                {
                                    QuestionID = Q.QuestionID,
                                    DiaryNumber = Q.DiaryNumber,
                                    QuesNumber = Q.QuestionNumber,
                                    Subject = Q.Subject,
                                    NoticeDate = Q.NoticeRecievedDate,
                                    NoticeTime = Q.NoticeRecievedTime,
                                    MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    Flag = Q.DeActivateFlag,
                                    //ProofReaderName = Q.ProofReaderUserId,

                                }).ToList();

                model.ResultCount = StarredQ.Count();
                model.tQuestionModel = StarredQ;
            }
            else if (model.RoleName == "AdminPdfCorner")
            {
                if (model.DataStatus == "CQ")
                {
                    var StarredQ = (from Q in pCtxt.tQuestions
                                    where Q.AssemblyID == model.AssemblyID
                                    && Q.SessionID == model.SessionID
                                    && (model.SessionDateId == 0 || Q.SessionDateId == model.SessionDateId)
                                    && Q.QuestionType == (int)QuestionType.StartedQuestion
                                    && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                    && Q.IsFixed == true
                                    && Q.DeActivateFlag == null

                                    select new TypistQuestionModel
                                    {
                                        QuestionID = Q.QuestionID,
                                        DiaryNumber = Q.DiaryNumber,
                                        QuesNumber = Q.QuestionNumber,
                                        Subject = Q.Subject,
                                        NoticeDate = Q.NoticeRecievedDate,
                                        NoticeTime = Q.NoticeRecievedTime,
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                        Flag = Q.DeActivateFlag,
                                    }).ToList();

                    foreach (var item in StarredQ)
                    {

                        if (string.IsNullOrEmpty(model.CurrentQidsForlock))
                        {
                            model.CurrentQidsForlock = item.QuestionID.ToString();
                        }
                        else
                        {
                            model.CurrentQidsForlock = model.CurrentQidsForlock + "," + item.QuestionID.ToString();
                        }


                    }

                    if (model.SessionDateId != 0)
                    {
                        var CQfilepath = (from SD in pCtxt.mSessionDates
                                          where SD.AssemblyId == model.AssemblyID
                                          && SD.SessionId == model.SessionID
                                          && (SD.Id == model.SessionDateId)
                                          && (SD.ApprovedBySecrt == true)
                                          select SD.StartQListPath).FirstOrDefault();
                        model.NoticePath = CQfilepath;

                    }

                    model.StarredApprovedTrans = (from M in pCtxt.mSessionDates
                                                  where M.Id == model.SessionDateId
                                                  select M.ApprovedByTrans).FirstOrDefault();
                    model.ResultCount = StarredQ.Count();
                    model.tQuestionModel = StarredQ;

                }
                else if (model.DataStatus == "PQ")
                {
                    var StarredQ = (from Q in pCtxt.tQuestions
                                    where Q.AssemblyID == model.AssemblyID
                                    && Q.SessionID == model.SessionID
                                    && (model.SessionDateId == 0 || Q.SessionDateId == model.SessionDateId)
                                    && Q.QuestionType == (int)QuestionType.StartedQuestion
                                    && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                    && Q.IsFixed == true
                                    && Q.DeActivateFlag == "RF"
                                    select new TypistQuestionModel
                                    {
                                        QuestionID = Q.QuestionID,
                                        DiaryNumber = Q.DiaryNumber,
                                        QuesNumber = Q.QuestionNumber,
                                        Subject = Q.Subject,
                                        NoticeDate = Q.NoticeRecievedDate,
                                        NoticeTime = Q.NoticeRecievedTime,
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                        Flag = Q.DeActivateFlag,
                                    }).ToList();

                    foreach (var item in StarredQ)
                    {

                        if (string.IsNullOrEmpty(model.PostponeQidsForlock))
                        {
                            model.PostponeQidsForlock = item.QuestionID.ToString();
                        }
                        else
                        {
                            model.PostponeQidsForlock = model.PostponeQidsForlock + "," + item.QuestionID.ToString();
                        }


                    }

                    if (model.SessionDateId != 0)
                    {
                        var PQfilepath = (from SD in pCtxt.mSessionDates
                                          where SD.AssemblyId == model.AssemblyID
                                          && SD.SessionId == model.SessionID
                                          && (SD.Id == model.SessionDateId)
                                          && (SD.SPPApprovedBySecrt == true)
                                          select SD.SPPQListPath).FirstOrDefault();
                        model.NoticePath = PQfilepath;

                    }
                    model.StarredPQApprovedTrans = (from M in pCtxt.mSessionDates
                                                    where M.Id == model.SessionDateId
                                                    select M.SPPApprovedByTrans).FirstOrDefault();
                    model.ResultCount = StarredQ.Count();
                    model.tQuestionModel = StarredQ;
                }


            }




            return model;
        }

        public static object GetUnStarredQuestionsForChanger(object param)
        {
            TypistQuestionModel model = param as TypistQuestionModel;
            NoticeContext pCtxt = new NoticeContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            if (model.RoleName == "VSTranslator")
            {
                if (model.DataStatus == "CQ")
                {
                    var UnStarredQ = (from Q in pCtxt.tQuestions
                                      where Q.AssemblyID == model.AssemblyID
                                      && Q.SessionID == model.SessionID
                                      && (model.SessionDateId == 0 || Q.SessionDateId == model.SessionDateId)
                                      && Q.QuestionType == (int)QuestionType.UnstaredQuestion
                                      && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                      && Q.IsFixed == true
                                      && Q.DeActivateFlag == null

                                      select new TypistQuestionModel
                                      {
                                          QuestionID = Q.QuestionID,
                                          DiaryNumber = Q.DiaryNumber,
                                          QuesNumber = Q.QuestionNumber,
                                          Subject = Q.Subject,
                                          NoticeDate = Q.NoticeRecievedDate,
                                          NoticeTime = Q.NoticeRecievedTime,
                                          MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                          Flag = Q.DeActivateFlag,

                                      }).ToList();

                    foreach (var item in UnStarredQ)
                    {

                        if (string.IsNullOrEmpty(model.CurrentQidsForlock))
                        {
                            model.CurrentQidsForlock = item.QuestionID.ToString();
                        }
                        else
                        {
                            model.CurrentQidsForlock = model.CurrentQidsForlock + "," + item.QuestionID.ToString();
                        }


                    }
                    if (model.SessionDateId != 0)
                    {
                        var CQfilepath = (from SD in pCtxt.mSessionDates
                                          where SD.AssemblyId == model.AssemblyID
                                          && SD.SessionId == model.SessionID
                                          && (SD.Id == model.SessionDateId)
                                          && (SD.UnstarredApprovedBySecrt == true)
                                          select SD.UnStartQListPath).FirstOrDefault();
                        model.NoticePath = CQfilepath;

                    }

                    model.UnstarredApprovedTrans = (from M in pCtxt.mSessionDates
                                                    where M.Id == model.SessionDateId
                                                    select M.UnstarredApprovedByTrans).FirstOrDefault();

                    model.ResultCount = UnStarredQ.Count();
                    model.tQuestionModel = UnStarredQ;
                }
                else if (model.DataStatus == "PQ")
                {
                    var UnStarredQ = (from Q in pCtxt.tQuestions
                                      where Q.AssemblyID == model.AssemblyID
                                      && Q.SessionID == model.SessionID
                                      && (model.SessionDateId == 0 || Q.SessionDateId == model.SessionDateId)
                                      && Q.QuestionType == (int)QuestionType.UnstaredQuestion
                                      && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                      && Q.IsFixed == true
                                      && Q.DeActivateFlag == "RF"

                                      select new TypistQuestionModel
                                      {
                                          QuestionID = Q.QuestionID,
                                          DiaryNumber = Q.DiaryNumber,
                                          QuesNumber = Q.QuestionNumber,
                                          Subject = Q.Subject,
                                          NoticeDate = Q.NoticeRecievedDate,
                                          NoticeTime = Q.NoticeRecievedTime,
                                          MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                          Flag = Q.DeActivateFlag,
                                      }).ToList();

                    foreach (var item in UnStarredQ)
                    {

                        if (string.IsNullOrEmpty(model.PostponeQidsForlock))
                        {
                            model.PostponeQidsForlock = item.QuestionID.ToString();
                        }
                        else
                        {
                            model.PostponeQidsForlock = model.PostponeQidsForlock + "," + item.QuestionID.ToString();
                        }


                    }

                    if (model.SessionDateId != 0)
                    {
                        var PQfilepath = (from SD in pCtxt.mSessionDates
                                          where SD.AssemblyId == model.AssemblyID
                                          && SD.SessionId == model.SessionID
                                          && (SD.Id == model.SessionDateId)
                                          && (SD.UPPApprovedBySecrt == true)
                                          select SD.UPPQListPath).FirstOrDefault();
                        model.NoticePath = PQfilepath;

                    }
                    model.UnstarredPQApprovedTrans = (from M in pCtxt.mSessionDates
                                                      where M.Id == model.SessionDateId
                                                      select M.UPPApprovedByTrans).FirstOrDefault();
                    model.ResultCount = UnStarredQ.Count();
                    model.tQuestionModel = UnStarredQ;
                }
            }
            else if (model.RoleName == "Legislative1")
            {
                var UnStarredQ = (from Q in pCtxt.tQuestions
                                  where Q.AssemblyID == model.AssemblyID
                                  && Q.SessionID == model.SessionID
                                  && Q.QuestionType == (int)QuestionType.UnstaredQuestion
                                   && Q.IsFinalApproved == null
                                   && Q.IsProofReading == true
                                  select new TypistQuestionModel
                                  {
                                      QuestionID = Q.QuestionID,
                                      DiaryNumber = Q.DiaryNumber,
                                      QuesNumber = Q.QuestionNumber,
                                      Subject = Q.Subject,
                                      NoticeDate = Q.NoticeRecievedDate,
                                      NoticeTime = Q.NoticeRecievedTime,
                                      MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                      Flag = Q.DeActivateFlag,

                                  }).ToList();

                model.ResultCount = UnStarredQ.Count();
                model.tQuestionModel = UnStarredQ;
            }
            else if (model.RoleName == "Admin")
            {
                var UnStarredQ = (from Q in pCtxt.tQuestions
                                  where Q.AssemblyID == model.AssemblyID
                                  && Q.SessionID == model.SessionID
                                  && Q.QuestionType == (int)QuestionType.UnstaredQuestion

                                  select new TypistQuestionModel
                                  {
                                      QuestionID = Q.QuestionID,
                                      DiaryNumber = Q.DiaryNumber,
                                      QuesNumber = Q.QuestionNumber,
                                      Subject = Q.Subject,
                                      NoticeDate = Q.NoticeRecievedDate,
                                      NoticeTime = Q.NoticeRecievedTime,
                                      MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                      Flag = Q.DeActivateFlag,
                                      //ProofReaderName = Q.ProofReaderUserId,

                                  }).ToList();

                model.ResultCount = UnStarredQ.Count();
                model.tQuestionModel = UnStarredQ;
            }
            else if (model.RoleName == "VS_Secretary")
            {
                var UnStarredQ = (from Q in pCtxt.tQuestions
                                  where Q.AssemblyID == model.AssemblyID
                                  && Q.SessionID == model.SessionID
                                  && Q.QuestionType == (int)QuestionType.UnstaredQuestion

                                  select new TypistQuestionModel
                                  {
                                      QuestionID = Q.QuestionID,
                                      DiaryNumber = Q.DiaryNumber,
                                      QuesNumber = Q.QuestionNumber,
                                      Subject = Q.Subject,
                                      NoticeDate = Q.NoticeRecievedDate,
                                      NoticeTime = Q.NoticeRecievedTime,
                                      MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                      Flag = Q.DeActivateFlag,

                                  }).ToList();

                model.ResultCount = UnStarredQ.Count();
                model.tQuestionModel = UnStarredQ;
            }
            else if (model.RoleName == "AdminPdfCorner")
            {
                if (model.DataStatus == "CQ")
                {
                    var UnStarredQ = (from Q in pCtxt.tQuestions
                                      where Q.AssemblyID == model.AssemblyID
                                      && Q.SessionID == model.SessionID
                                      && (model.SessionDateId == 0 || Q.SessionDateId == model.SessionDateId)
                                      && Q.QuestionType == (int)QuestionType.UnstaredQuestion
                                      && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                      && Q.IsFixed == true
                                      && Q.DeActivateFlag == null

                                      select new TypistQuestionModel
                                      {
                                          QuestionID = Q.QuestionID,
                                          DiaryNumber = Q.DiaryNumber,
                                          QuesNumber = Q.QuestionNumber,
                                          Subject = Q.Subject,
                                          NoticeDate = Q.NoticeRecievedDate,
                                          NoticeTime = Q.NoticeRecievedTime,
                                          MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                          Flag = Q.DeActivateFlag,

                                      }).ToList();

                    foreach (var item in UnStarredQ)
                    {

                        if (string.IsNullOrEmpty(model.CurrentQidsForlock))
                        {
                            model.CurrentQidsForlock = item.QuestionID.ToString();
                        }
                        else
                        {
                            model.CurrentQidsForlock = model.CurrentQidsForlock + "," + item.QuestionID.ToString();
                        }


                    }
                    if (model.SessionDateId != 0)
                    {
                        var CQfilepath = (from SD in pCtxt.mSessionDates
                                          where SD.AssemblyId == model.AssemblyID
                                          && SD.SessionId == model.SessionID
                                          && (SD.Id == model.SessionDateId)
                                          && (SD.UnstarredApprovedBySecrt == true)
                                          select SD.UnStartQListPath).FirstOrDefault();
                        model.NoticePath = CQfilepath;

                    }

                    model.UnstarredApprovedTrans = (from M in pCtxt.mSessionDates
                                                    where M.Id == model.SessionDateId
                                                    select M.UnstarredApprovedByTrans).FirstOrDefault();

                    model.ResultCount = UnStarredQ.Count();
                    model.tQuestionModel = UnStarredQ;
                }
                else if (model.DataStatus == "PQ")
                {
                    var UnStarredQ = (from Q in pCtxt.tQuestions
                                      where Q.AssemblyID == model.AssemblyID
                                      && Q.SessionID == model.SessionID
                                      && (model.SessionDateId == 0 || Q.SessionDateId == model.SessionDateId)
                                      && Q.QuestionType == (int)QuestionType.UnstaredQuestion
                                      && Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                      && Q.IsFixed == true
                                      && Q.DeActivateFlag == "RF"

                                      select new TypistQuestionModel
                                      {
                                          QuestionID = Q.QuestionID,
                                          DiaryNumber = Q.DiaryNumber,
                                          QuesNumber = Q.QuestionNumber,
                                          Subject = Q.Subject,
                                          NoticeDate = Q.NoticeRecievedDate,
                                          NoticeTime = Q.NoticeRecievedTime,
                                          MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                          Flag = Q.DeActivateFlag,
                                      }).ToList();

                    foreach (var item in UnStarredQ)
                    {

                        if (string.IsNullOrEmpty(model.PostponeQidsForlock))
                        {
                            model.PostponeQidsForlock = item.QuestionID.ToString();
                        }
                        else
                        {
                            model.PostponeQidsForlock = model.PostponeQidsForlock + "," + item.QuestionID.ToString();
                        }


                    }

                    if (model.SessionDateId != 0)
                    {
                        var PQfilepath = (from SD in pCtxt.mSessionDates
                                          where SD.AssemblyId == model.AssemblyID
                                          && SD.SessionId == model.SessionID
                                          && (SD.Id == model.SessionDateId)
                                          && (SD.UPPApprovedBySecrt == true)
                                          select SD.UPPQListPath).FirstOrDefault();
                        model.NoticePath = PQfilepath;

                    }
                    model.UnstarredPQApprovedTrans = (from M in pCtxt.mSessionDates
                                                      where M.Id == model.SessionDateId
                                                      select M.UPPApprovedByTrans).FirstOrDefault();
                    model.ResultCount = UnStarredQ.Count();
                    model.tQuestionModel = UnStarredQ;
                }
            }
            return model;
        }


        static object UpdatemDepartmentPdfPath(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            string msg = "";
            var Val = (from Dep in pCtxt.mDepartmentPdfPath
                       where Dep.deptId == model.DepartmentId
                        && Dep.AssemblyID == model.AssemblyID
                        && Dep.SessionID == model.SessionID
                       select Dep.deptId).SingleOrDefault();

            if (Val == null)
            {
                mDepartmentPdfPath obj = new mDepartmentPdfPath();
                obj.AssemblyID = model.AssemblyID;
                obj.SessionID = model.SessionID;
                obj.deptId = model.DepartmentId;
                obj.deptname = model.DepartmentName;
                obj.StarredBeforeFixPath = model.FilePath;
                obj.SLastUpdatedDate = model.Updatedate;
                pCtxt.mDepartmentPdfPath.Add(obj);
                pCtxt.SaveChanges();

                return msg;
            }
            else
            {
                mDepartmentPdfPath obj = pCtxt.mDepartmentPdfPath.Single(m => m.deptId == model.DepartmentId && m.AssemblyID == model.AssemblyID && m.SessionID == model.SessionID);
                obj.AssemblyID = model.AssemblyID;
                obj.SessionID = model.SessionID;
                obj.deptId = model.DepartmentId;
                obj.deptname = model.DepartmentName;
                obj.StarredBeforeFixPath = model.FilePath;
                obj.SLastUpdatedDate = model.Updatedate;
                pCtxt.SaveChanges();
            }



            return msg;


        }

        static object UpdatemDepartmentPdfUnstarred(object param)
        {
            //tMemberNotice model = param as tMemberNotice;
            //NoticeContext pCtxt = new NoticeContext();
            //mDepartment obj = pCtxt.mDepartments.Single(m => m.deptId == model.DepartmentId);
            //obj.UnStarredBeforeFixPath = model.FilePath;
            //obj.UnSLastUpdatedDate = model.Updatedate;
            //pCtxt.SaveChanges();
            //string msg = "";

            //return msg;
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            string msg = "";

            var Val = (from Dep in pCtxt.mDepartmentPdfPath
                       where Dep.deptId == model.DepartmentId
                        && Dep.AssemblyID == model.AssemblyID
                        && Dep.SessionID == model.SessionID
                       select Dep.deptId).SingleOrDefault();

            if (Val == null)
            {
                mDepartmentPdfPath obj = new mDepartmentPdfPath();
                obj.AssemblyID = model.AssemblyID;
                obj.SessionID = model.SessionID;
                obj.deptId = model.DepartmentId;
                obj.deptname = model.DepartmentName;
                obj.UnSLastUpdatedDate = model.Updatedate;
                obj.UnStarredBeforeFixPath = model.FilePath;
                pCtxt.mDepartmentPdfPath.Add(obj);
                pCtxt.SaveChanges();

                return msg;
            }
            else
            {
                mDepartmentPdfPath obj = pCtxt.mDepartmentPdfPath.Single(m => m.deptId == model.DepartmentId && m.AssemblyID == model.AssemblyID && m.SessionID == model.SessionID);
                obj.AssemblyID = model.AssemblyID;
                obj.SessionID = model.SessionID;
                obj.deptId = model.DepartmentId;
                obj.deptname = model.DepartmentName;
                obj.UnStarredBeforeFixPath = model.FilePath;
                obj.UnSLastUpdatedDate = model.Updatedate;
                pCtxt.SaveChanges();
            }



            return msg;

        }



        static tMemberNotice GetDiaryNoForPdfDiaryWiseStarred(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 1 && questions.QuestionStatus == 3 && questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID
                         select new QuestionModelCustom
                         {

                             DiaryNumber = questions.DiaryNumber,

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }


        static tMemberNotice UnstarredGetDiaryNoForPdfDiaryWiseStarred(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 2 && questions.QuestionStatus == 3 && questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID
                         select new QuestionModelCustom
                         {

                             DiaryNumber = questions.DiaryNumber,

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }

        static tMemberNotice GetAllDetailsByDiaryNo(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 1 && questions.DiaryNumber == model.DiaryNo && questions.QuestionStatus == 3 && questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,

                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),
                         }).ToList();


            foreach (var val in query)
            {
                if (val.IsClubbed != null)
                {
                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tQuestions
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                            ConstituencyName = (from t in pCtxt.mConstituency
                                                                join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                select t.ConstituencyName).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemName))
                            {
                                val.CMemName = item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                            else
                            {
                                val.CMemName = val.CMemName + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                            }


                        }
                    }

                    //if (val.IsHindi == true)
                    //{
                    //    model.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault();
                    //}
                }
                if (val.IsHindi == true)
                {

                    val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                    val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                                                  join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                  where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                  select t.ConstituencyName_Local).FirstOrDefault();
                    val.MinistryNameLocal = (from M in pCtxt.mMinistry
                                             where M.MinistryID == val.MinisterID
                                             select M.MinistryNameLocal).FirstOrDefault();
                    if (val.IsClubbed != null)
                    {
                        string s = val.RefMemCode;
                        if (s != null && s != "")
                        {
                            string[] values = s.Split(',');

                            for (int i = 0; i < values.Length; i++)
                            {
                                int MemId = int.Parse(values[i]);
                                var item = (from questions in pCtxt.tQuestions
                                            select new QuestionModelCustom
                                            {
                                                MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.NameLocal)).FirstOrDefault(),
                                                ConstituencyName = (from t in pCtxt.mConstituency
                                                                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                    select t.ConstituencyName_Local).FirstOrDefault(),
                                            }).FirstOrDefault();

                                if (string.IsNullOrEmpty(val.CMemNameHindi))
                                {
                                    val.CMemNameHindi = item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                                else
                                {
                                    val.CMemNameHindi = val.CMemNameHindi + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                                }


                            }
                        }
                    }
                }
            }

            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }


        static tMemberNotice UnstarredGetAllDetailsByDiaryNo(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = (from questions in pCtxt.tQuestions
                         where questions.QuestionType == 2 && questions.DiaryNumber == model.DiaryNo && questions.QuestionStatus == 3 && questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,

                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),
                         }).ToList();


            foreach (var val in query)
            {
                if (val.IsClubbed != null)
                {
                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tQuestions
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                            ConstituencyName = (from t in pCtxt.mConstituency
                                                                join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                select t.ConstituencyName).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemName))
                            {
                                val.CMemName = item.MemberName + "(" + item.ConstituencyName + ")";
                            }
                            else
                            {
                                val.CMemName = val.CMemName + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                            }


                        }
                    }

                    //if (val.IsHindi == true)
                    //{
                    //    model.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault();
                    //}
                }
                if (val.IsHindi == true)
                {

                    val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                    val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                                                  join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                  where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                  select t.ConstituencyName_Local).FirstOrDefault();
                    val.MinistryNameLocal = (from M in pCtxt.mMinistry
                                             where M.MinistryID == val.MinisterID
                                             select M.MinistryNameLocal).FirstOrDefault();
                    if (val.IsClubbed != null)
                    {
                        string s = val.RefMemCode;
                        if (s != null && s != "")
                        {
                            string[] values = s.Split(',');

                            for (int i = 0; i < values.Length; i++)
                            {
                                int MemId = int.Parse(values[i]);
                                var item = (from questions in pCtxt.tQuestions
                                            select new QuestionModelCustom
                                            {
                                                MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.NameLocal)).FirstOrDefault(),
                                                ConstituencyName = (from t in pCtxt.mConstituency
                                                                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                                    select t.ConstituencyName_Local).FirstOrDefault(),
                                            }).FirstOrDefault();

                                if (string.IsNullOrEmpty(val.CMemNameHindi))
                                {
                                    val.CMemNameHindi = item.MemberName + "(" + item.ConstituencyName + ")";
                                }
                                else
                                {
                                    val.CMemNameHindi = val.CMemNameHindi + "," + item.MemberName + "(" + item.ConstituencyName + ")";
                                }


                            }
                        }
                    }
                }
            }

            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }


        static object EditPendingNoticeDetials(object param)
        {
            tPaperLaidV mdl = param as tPaperLaidV;
            NoticeContext paperLaidCntxtDB = new NoticeContext();
            var fileInfo = (from model in paperLaidCntxtDB.tPaperLaidTemps where model.PaperLaidId == mdl.PaperLaidId select model).SingleOrDefault();

            mdl.FileName = fileInfo.FileName;
            mdl.DocFileName = fileInfo.DocFileName;

            mdl.DocFilePath = fileInfo.FilePath + fileInfo.DocFileName;

            return mdl;
        }

        public static object BackToTypistById(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;
            tQuestion QuesObj = new tQuestion();

            try
            {
                QuesObj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
                qCtxDB.tQuestions.Attach(QuesObj);
                QuesObj.IsTypistFreeze = null;
                qCtxDB.SaveChanges();
                qCtxDB.Close();

                return "Question Sent Back to Typist !";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }

        public static object BackToPRByQuestionId(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            DiaryModel update = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();

            try
            {
                QuesObj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionId);
                qCtxDB.tQuestions.Attach(QuesObj);
                QuesObj.IsContentFreeze = null;
                QuesObj.IsProofReading = null;
                QuesObj.IsProofReadingDate = null;
                QuesObj.QuestionStatus = (int)Questionstatus.QuestionAssignTypist;
                QuesObj.ProofReaderUserId = null;
                qCtxDB.SaveChanges();
                qCtxDB.Close();

                return "Question Sent Back to Proof Reader !";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }


        static object CheckOTP(object param)
        {

            NoticeContext pCtxt = new NoticeContext();
            DiaryModel updateSQ = param as DiaryModel;
            String Exist = "Ok";
            //string OTPExist = "True";
            var query1 = (from tQ in pCtxt.tOTPDetails
                          where (tQ.OTP == updateSQ.EnterOTP) && (tQ.OTPid == updateSQ.OTPID)
                          select tQ).ToList().Count();
            // return query1;
            if (query1 == 0)
            {
                string OTPExist = "NotExist";
                return OTPExist.ToString();
            }
            else if (query1 == 1)
            {
                string OTPExist = "Exist";
                return OTPExist.ToString();
            }
            return Exist.ToString();
        }

        public static object ApproveGeneratedPDFByDate(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel DiaryObj = param as DiaryModel;
            NoticeContext pCtxt = new NoticeContext();
            try
            {


                mSessionDate SDateObj = (from SD in DiaCtx.objSessDate
                                         where SD.Id == DiaryObj.SessionDateId
                                         select SD).FirstOrDefault();
                DiaCtx.objSessDate.Attach(SDateObj);


                if (DiaryObj.PaperEntryType == "Starred")
                {
                    if (DiaryObj.UserName == "VSTranslator")
                    {
                        if (DiaryObj.ViewType == "CQ")
                        {
                            SDateObj.ApprovedByTrans = true;
                        }
                        else if (DiaryObj.ViewType == "PQ")
                        {
                            SDateObj.SPPApprovedByTrans = true;
                        }

                    }
                    else if (DiaryObj.UserName == "VS_Secretary")
                    {
                        if (DiaryObj.ViewType == "All")
                        {
                            SDateObj.ApprovedBySecrt = true;
                            SDateObj.SPPApprovedBySecrt = true;
                        }
                        else if (DiaryObj.ViewType == "CQ")
                        {
                            SDateObj.ApprovedBySecrt = true;

                            tAssemblyFiles obj = new tAssemblyFiles();
                            obj.AssemblyId = SDateObj.AssemblyId;
                            obj.SessionId = SDateObj.SessionId;
                            obj.SessionDateId = SDateObj.Id;
                            obj.TypeofDocumentId = 4;
                            obj.Description = "Starred Question";
                            obj.UploadFile = "/" + SDateObj.StartQListPath;
                            obj.CreatedBy = DiaryObj.UserName;
                            obj.CreatedDate = DiaryObj.IsPostPoneDate;
                            obj.Status = true;
                            obj.ModifiedDate = DiaryObj.IsPostPoneDate;
                            obj.ModifiedBy = DiaryObj.UserName;
                            pCtxt.tAssemblyFiles.Add(obj);
                            pCtxt.SaveChanges();


                            string SourceFile = SDateObj.StartQListPath;
                            string CompathFile = DiaryObj.CutMotionFilePath + SDateObj.StartQListPath;

                            String[] parts = SourceFile.Split("/"[0]);
                            string path1 = System.IO.Path.Combine(DiaryObj.CutMotionFilePath + "/Notices/", parts[4]);
                            System.IO.File.Copy(CompathFile, path1, true);

                            mNotice objNotice = new mNotice();
                            objNotice.NoticeTitle = "Starred Questions," + SDateObj.SessionDate.ToLongDateString();
                            objNotice.Attachments = parts[4];
                            objNotice.Status = 1;
                            objNotice.CategoryID = 370;
                            objNotice.CreatedBy = new Guid(DiaryObj.UserGuid);
                            objNotice.CreatedDate = DiaryObj.DateEntry;
                            objNotice.ModifiedBy = new Guid(DiaryObj.UserGuid);
                            objNotice.ModifiedDate = DiaryObj.DateEntry;
                            objNotice.ModifiedBy = new Guid(DiaryObj.UserGuid);
                            objNotice.ModifiedDate = DiaryObj.DateEntry;
                            objNotice.StartPublish = DiaryObj.DateEntry;
                            objNotice.StopPublish = DiaryObj.DateEntry;
                            pCtxt.tNoticeBoard.Add(objNotice);
                            pCtxt.SaveChanges();

                        }
                        else if (DiaryObj.ViewType == "PQ")
                        {
                            SDateObj.SPPApprovedBySecrt = true;
                            tAssemblyFiles obj = new tAssemblyFiles();
                            obj.AssemblyId = SDateObj.AssemblyId;
                            obj.SessionId = SDateObj.SessionId;
                            obj.SessionDateId = SDateObj.Id;
                            obj.TypeofDocumentId = 9;
                            obj.Description = "Posponed Starred Questions";
                            obj.UploadFile = "/" + SDateObj.SPPQListPath;
                            obj.CreatedBy = DiaryObj.UserName;
                            obj.CreatedDate = DiaryObj.IsPostPoneDate;
                            obj.Status = true;
                            obj.ModifiedDate = DiaryObj.IsPostPoneDate;
                            obj.ModifiedBy = DiaryObj.UserName;
                            pCtxt.tAssemblyFiles.Add(obj);
                            pCtxt.SaveChanges();
                        }


                    }

                }
                else if (DiaryObj.PaperEntryType == "Unstarred")
                {
                    if (DiaryObj.UserName == "VSTranslator")
                    {
                        if (DiaryObj.ViewType == "CQ")
                        {
                            SDateObj.UnstarredApprovedByTrans = true;
                        }
                        else if (DiaryObj.ViewType == "PQ")
                        {
                            SDateObj.UPPApprovedByTrans = true;
                        }

                    }
                    else if (DiaryObj.UserName == "VS_Secretary")
                    {
                        if (DiaryObj.ViewType == "All")
                        {
                            SDateObj.UnstarredApprovedBySecrt = true;
                            SDateObj.UPPApprovedBySecrt = true;
                        }
                        else if (DiaryObj.ViewType == "CQ")
                        {
                            SDateObj.UnstarredApprovedBySecrt = true;

                            tAssemblyFiles obj = new tAssemblyFiles();
                            obj.AssemblyId = SDateObj.AssemblyId;
                            obj.SessionId = SDateObj.SessionId;
                            obj.SessionDateId = SDateObj.Id;
                            obj.TypeofDocumentId = 5;
                            obj.Description = "UnStarred Question";
                            obj.UploadFile = "/" + SDateObj.UnStartQListPath;
                            obj.CreatedBy = DiaryObj.UserName;
                            obj.CreatedDate = DiaryObj.IsPostPoneDate;
                            obj.Status = true;
                            obj.ModifiedDate = DiaryObj.IsPostPoneDate;
                            obj.ModifiedBy = DiaryObj.UserName;
                            pCtxt.tAssemblyFiles.Add(obj);
                            pCtxt.SaveChanges();

                            string SourceFile = SDateObj.UnStartQListPath;
                            string CompathFile = DiaryObj.CutMotionFilePath + SDateObj.UnStartQListPath;
                            String[] parts = SourceFile.Split("/"[0]);
                            string path1 = System.IO.Path.Combine(DiaryObj.CutMotionFilePath + "/Notices/", parts[4]);
                            System.IO.File.Copy(CompathFile, path1, true);

                            mNotice objNotice = new mNotice();
                            objNotice.NoticeTitle = "UnStarred Questions," + SDateObj.SessionDate.ToLongDateString();
                            objNotice.Attachments = parts[4];
                            objNotice.Status = 1;
                            objNotice.CategoryID = 369;
                            objNotice.CreatedBy = new Guid(DiaryObj.UserGuid);
                            objNotice.CreatedDate = DiaryObj.DateEntry;
                            objNotice.ModifiedBy = new Guid(DiaryObj.UserGuid);
                            objNotice.ModifiedDate = DiaryObj.DateEntry;
                            objNotice.ModifiedBy = new Guid(DiaryObj.UserGuid);
                            objNotice.ModifiedDate = DiaryObj.DateEntry;
                            objNotice.StartPublish = DiaryObj.DateEntry;
                            objNotice.StopPublish = DiaryObj.DateEntry;
                            pCtxt.tNoticeBoard.Add(objNotice);
                            pCtxt.SaveChanges();
                        }
                        else if (DiaryObj.ViewType == "PQ")
                        {
                            SDateObj.UPPApprovedBySecrt = true;
                            tAssemblyFiles obj = new tAssemblyFiles();
                            obj.AssemblyId = SDateObj.AssemblyId;
                            obj.SessionId = SDateObj.SessionId;
                            obj.SessionDateId = SDateObj.Id;
                            obj.TypeofDocumentId = 17;
                            obj.Description = "Posponed UnStarred Questions";
                            obj.UploadFile = "/" + SDateObj.UPPQListPath;
                            obj.CreatedBy = DiaryObj.UserName;
                            obj.CreatedDate = DiaryObj.IsPostPoneDate;
                            obj.Status = true;
                            obj.ModifiedDate = DiaryObj.IsPostPoneDate;
                            obj.ModifiedBy = DiaryObj.UserName;
                            pCtxt.tAssemblyFiles.Add(obj);
                            pCtxt.SaveChanges();
                        }

                    }
                }
                DiaCtx.SaveChanges();
                DiaCtx.Close();
                DiaryObj.Message = "Approved Successfully !";
            }
            catch (Exception ex)
            {
                DiaryObj.Message = ex.Message;
            }

            return DiaryObj.Message;


        }

        public static object BackToTranByDate(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel DiaryObj = param as DiaryModel;

            try
            {
                mSessionDate SDateObj = (from SD in DiaCtx.objSessDate
                                         where SD.Id == DiaryObj.SessionDateId
                                         select SD).FirstOrDefault();

                DiaCtx.objSessDate.Attach(SDateObj);
                if (DiaryObj.PaperEntryType == "Starred")
                {
                    if (DiaryObj.ViewType == "All")
                    {
                        SDateObj.ApprovedByTrans = false;
                        SDateObj.SPPApprovedByTrans = false;
                    }
                    else if (DiaryObj.ViewType == "CQ")
                    {
                        SDateObj.ApprovedByTrans = false;
                    }
                    else if (DiaryObj.ViewType == "PQ")
                    {
                        SDateObj.SPPApprovedByTrans = false;
                    }
                }
                else if (DiaryObj.PaperEntryType == "Unstarred")
                {
                    if (DiaryObj.ViewType == "All")
                    {
                        SDateObj.UnstarredApprovedByTrans = false;
                        SDateObj.UPPApprovedByTrans = false;
                    }
                    else if (DiaryObj.ViewType == "CQ")
                    {
                        SDateObj.UnstarredApprovedByTrans = false;
                    }
                    else if (DiaryObj.ViewType == "PQ")
                    {
                        SDateObj.UPPApprovedByTrans = false;
                    }
                }
                DiaCtx.SaveChanges();
                DiaCtx.Close();
                DiaryObj.Message = "Returned Successfully !";
            }
            catch (Exception ex)
            {
                DiaryObj.Message = ex.Message;
            }

            return DiaryObj.Message;

        }




        static object GetDataByDate(object param)
        {
            try
            {
                //tQuestionModel model = param as tQuestionModel;
                AssignQuestionModel model = param as AssignQuestionModel;
                using (NoticeContext context = new NoticeContext())
                {

                    var List = (from DQuest in context.tQuestions
                                where EntityFunctions.TruncateTime(DQuest.NoticeRecievedDate) >= EntityFunctions.TruncateTime(model.DateFrom) && EntityFunctions.TruncateTime(DQuest.NoticeRecievedDate) <= EntityFunctions.TruncateTime(model.DateTo) && DQuest.QuestionStatus == 3 && DQuest.QuestionType == 1
                                orderby DQuest.QuestionID ascending
                                //   select DQuest).ToList();
                                select new AssignQuestionModel
                                {
                                    QuestionID = DQuest.QuestionID,
                                    DiaryNumber = DQuest.DiaryNumber,
                                    Subject = DQuest.Subject,
                                    DepartmentName = (from mc in context.mDepartments where mc.deptId == DQuest.DepartmentID select mc.deptname).FirstOrDefault(),
                                    MinisterName = (from mc in context.mMinistry where mc.MinistryID == DQuest.MinistryId select mc.MinisterName).FirstOrDefault(),
                                    MemberName = (from mc in context.mMembers where mc.MemberCode == DQuest.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    MainQ = DQuest.MainQuestion,
                                }).ToList().OrderBy(m => m.DepartmentName);


                    var results = List.ToList();
                    model.UserList = results;

                    return model;

                }


            }
            catch (Exception)
            {
                return null;
                throw;
            }

        }



        static object GetDataByUnstarredDate(object param)
        {
            try
            {
                //tQuestionModel model = param as tQuestionModel;
                AssignQuestionModel model = param as AssignQuestionModel;
                using (NoticeContext context = new NoticeContext())
                {

                    var List = (from DQuest in context.tQuestions
                                where EntityFunctions.TruncateTime(DQuest.NoticeRecievedDate) >= EntityFunctions.TruncateTime(model.DateFrom) && EntityFunctions.TruncateTime(DQuest.NoticeRecievedDate) <= EntityFunctions.TruncateTime(model.DateTo) && DQuest.QuestionStatus == 3 && DQuest.QuestionType == 2
                                orderby DQuest.QuestionID ascending
                                //   select DQuest).ToList();
                                select new AssignQuestionModel
                                {
                                    QuestionID = DQuest.QuestionID,
                                    DiaryNumber = DQuest.DiaryNumber,
                                    Subject = DQuest.Subject,
                                    DepartmentName = (from mc in context.mDepartments where mc.deptId == DQuest.DepartmentID select mc.deptname).FirstOrDefault(),
                                    MinisterName = (from mc in context.mMinistry where mc.MinistryID == DQuest.MinistryId select mc.MinisterName).FirstOrDefault(),
                                    MemberName = (from mc in context.mMembers where mc.MemberCode == DQuest.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    MainQ = DQuest.MainQuestion,
                                }).ToList().OrderBy(m => m.DepartmentName);


                    var results = List.ToList();
                    model.UserList = results;

                    return model;

                }


            }
            catch (Exception)
            {
                return null;
                throw;
            }

        }

        //Regarding Notices Assigning

        static object Istyoistrequired(object param)
        {

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var query = (from Event in DiaCtx.objmEvent
                         where (Event.EventId == Obj.EventId)
                         select Event.IsTyipstReqired).FirstOrDefault();
            Obj.TypistNeede = query.ToString();
            return Obj;
        }


        static object GetNoticesUserCode(object param)
        {
            NoticesAssignModel model = param as NoticesAssignModel;
            UserContext Ustxt = new UserContext();
            var query = (from User in Ustxt.mUsers
                         join UserRole in Ustxt.tUserRoles on User.UserId equals UserRole.UserID
                         join R in Ustxt.mRoles on UserRole.Roleid equals R.RoleId
                         //join E in Ustxt.mEmployee on new { UserName1 = User.UserName, DeptId1 = User.DeptId } equals new { UserName1 = E.empcd, DeptId1 = E.deptid }
                         where R.RoleName == model.RoleName
                         orderby User.UserName ascending
                         select new NoticesAssignModel
                         {
                             UserName = User.UserName,
                             TypistUserId = User.UserId,
                             UserId = User.UserId,
                             UId = User.UserId
                         }).ToList();

            var results = query.ToList();
            model.UserList = results;

            return model;
        }
        static object GetNoticesUserforAssigned(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            NoticesAssignModel model = param as NoticesAssignModel;
            UserContext Ustxt = new UserContext();
            var query = (from User in Ustxt.mUsers
                         join UserRole in Ustxt.tUserRoles on User.UserId equals UserRole.UserID
                         join R in Ustxt.mRoles on UserRole.Roleid equals R.RoleId
                         //join E in Ustxt.mEmployee on new { UserName1 = User.UserName, DeptId1 = User.DeptId } equals new { UserName1 = E.empcd, DeptId1 = E.deptid }
                         where R.RoleName == model.RoleName
                         // && (from tquest in Ustxt.tQuestions where User.UserId == new Guid(tquest.TypistUserId) select tquest.TypistUserId).Contains(User.UserId.ToString())

                         select new NoticesAssignModel
                         {
                             TypsitUserName = User.UserName,
                             TypistUserId = User.UserId,
                             // Count = (from tquest in Ustxt.tQuestions where tquest.TypistUserId== User.UserId  .Count() 

                         }).ToList();

            List<NoticesAssignModel> returnToCaller = new List<NoticesAssignModel>();



            foreach (var val in query)
            {

                string Id = val.TypistUserId.ToString();
                model.TypistUserId = val.TypistUserId;
                var item = (from Notice in Ustxt.tMemberNotice
                            where Notice.TypistUserIdNotice == Id
                            && Notice.AssemblyID == model.AssemblyID
                            && Notice.SessionID == model.SessionID
                            select new NoticesAssignModel
                            {
                                NoticeId = Notice.NoticeId,
                                NoticeNumber = Notice.NoticeNumber,
                                Subject = Notice.Subject,
                                NoticeDate = Notice.NoticeDate,

                            }).ToList().Count();
                model.Count = item;
                if (item != 0)
                {

                    NoticesAssignModel tempModel = new NoticesAssignModel();
                    tempModel = val;
                    tempModel.TypsitUserName = val.TypsitUserName + " (" + model.Count + ")";
                    tempModel.TypistUserId = model.TypistUserId;
                    returnToCaller.Add(tempModel);
                    //model.tMemberNoticeModel = returnToCaller;
                }

            }

            //add UnassignQuestion with Count

            NoticesAssignModel AQM = new NoticesAssignModel();
            AQM.AssemblyID = model.AssemblyID;
            AQM.SessionID = model.SessionID;
            AQM = (NoticesAssignModel)GetPendingNoticesForAssign(AQM);
            AQM.TypistUserId = new Guid();
            AQM.TypsitUserName = "Unassigned Notices (" + AQM.ResultCount + ")";
            returnToCaller.Add(AQM);
            model.tMemberNoticeModel = returnToCaller;

            // var results = query.ToList();


            return model;
        }

        public static object GetPendingNoticesForAssign(object param)
        {
            NoticesAssignModel model = param as NoticesAssignModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from Notice in pCtxt.tMemberNotices
                         where (Notice.NoticeStatus == (int)NoticeStatusEnum.NoticeToTypist) && (Notice.TypistUserIdNotice == null)
                         && Notice.AssemblyID == model.AssemblyID
                         && Notice.SessionID == model.SessionID
                         orderby Notice.NoticeId descending
                         select new NoticesAssignModel
                         {
                             NoticeId = Notice.NoticeId,
                             NoticeNumber = Notice.NoticeNumber,
                             Subject = Notice.Subject,
                             NoticeDate = Notice.NoticeDate,
                             OriDiaryFileName = Notice.OriDiaryFileName,
                             OriDiaryFilePath = Notice.OriDiaryFilePath,
                             //   Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notice.MemberId select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();


            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }

        public static object GetNoticesAssignList(object param)
        {
            NoticesAssignModel model = param as NoticesAssignModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from Notice in pCtxt.tMemberNotices
                             //where (questions.QuestionType == model.QuestionTypeId) && (questions.DiaryNumber != null) && (questions.QuestionLayingDate == null) && (questions.TypistUserId == model.QuestionValue) && (questions.IsContentFreeze == null) && (questions.OriDiaryFileName != null) && (questions.OriDiaryFilePath != null)
                         where (Notice.TypistUserIdNotice == model.QuestionValue) && (Notice.OriDiaryFileName != null) && (Notice.OriDiaryFilePath != null)
                         && Notice.AssemblyID == model.AssemblyID
                         && Notice.SessionID == model.SessionID
                         orderby Notice.NoticeId descending
                         select new NoticesAssignModel
                         {
                             NoticeId = Notice.NoticeId,
                             NoticeNumber = Notice.NoticeNumber,
                             Subject = Notice.Subject,
                             NoticeDate = Notice.NoticeDate,
                             OriDiaryFileName = Notice.OriDiaryFileName,
                             OriDiaryFilePath = Notice.OriDiaryFilePath,

                             //   Stats = questions.IsNoticeFreeze,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notice.MemberId select mc.Name).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            //model.tQuestion = results;
            model.tQuestionModel = results;

            return model;
        }

        static object AssignNotices(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;

            try
            {
                string s = update.QuestionValue;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int NoticeId = int.Parse(values[i]);
                        tMemberNotice obj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == NoticeId);

                        if (obj.OriDiaryFileName != null || obj.OriDiaryFilePath != null)
                        {
                            obj.TypistUserIdNotice = update.TypistUserId;
                            obj.IsAssignTypsitDate = DateTime.Now;
                            ///  obj.NoticeStatus = (int)NoticeStatusEnum.NoticeAssignTypist;
                            qCtxDB.SaveChanges();
                        }


                    }

                }

                return "Notice Assigned Successfully ! ";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }


        static object RoleBackNotices(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;


            string s = update.QuestionValue;
            if (s != null && s != "")
            {
                string[] values = s.Split(',');

                for (int i = 0; i < values.Length; i++)
                {
                    int NoticeId = int.Parse(values[i]);
                    tMemberNotice obj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == NoticeId);
                    obj.TypistUserIdNotice = null;
                    qCtxDB.SaveChanges();
                }

            }




            return null;
        }

        static object GetCountForNotrices(object param)
        {
            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionID && m.AssemblyID == model.AssemblyID select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyID select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;

            model.mAssemblyList = (from A in pCtxt.mAssemblies
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSessions
                                 where S.AssemblyID == model.AssemblyID
                                 select S).ToList();


            var NoticeCOunt = (from Notice in pCtxt.tMemberNotices
                               where (Notice.TypistRequired == true) && (Notice.TypistUserIdNotice == model.UId)
                              && Notice.AssemblyID == model.AssemblyID
                              && Notice.SessionID == model.SessionID
                               select new TypistQuestionModel
                               {
                                   NoticeId = Notice.NoticeId,
                                   NoticeNumber = Notice.NoticeNumber,
                                   Subject = Notice.Subject,
                                   NoticeDate = Notice.NoticeDate,
                                   MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notice.MemberId select mc.Name).FirstOrDefault()
                               }).ToList().Count();


            var NoticeProofCOunt = (from Notice in pCtxt.tMemberNotices
                                    where (Notice.TypistRequired == true) && (Notice.TypistFreezed == true)
                                   && Notice.AssemblyID == model.AssemblyID
                                   && Notice.SessionID == model.SessionID
                                    select new TypistQuestionModel
                                    {
                                        NoticeId = Notice.NoticeId,
                                        NoticeNumber = Notice.NoticeNumber,
                                        Subject = Notice.Subject,
                                        NoticeDate = Notice.NoticeDate,
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notice.MemberId select mc.Name).FirstOrDefault()
                                    }).ToList().Count();

            model.TotalNoticesReceived = NoticeCOunt;
            model.TotalNoticesProofRed = NoticeProofCOunt;

            return model;
        }


        public static object GetNoticesforTypist(object param)
        {
            TypistQuestionModel model = param as TypistQuestionModel;

            NoticeContext pCtxt = new NoticeContext();
            SiteSettingContext settingContext = new SiteSettingContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            if (model.RoleName == "Vidhan Sabha Typist")
            {
                var query = (from N in pCtxt.tMemberNotices
                             where (N.TypistRequired == true) && (N.TypistUserIdNotice == model.UId)
                             && N.AssemblyID == model.AssemblyID
                             && N.SessionID == model.SessionID
                             orderby N.NoticeId ascending
                             select new TypistQuestionModel
                             {
                                 NoticeId = N.NoticeId,
                                 NoticeNumber = N.NoticeNumber,
                                 Subject = N.Subject,
                                 NoticeDate = N.NoticeDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == N.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 TypingBy = N.TypistUserIdNotice,
                                 ProofReaderName = N.ProofReaderUserId
                             }).ToList();
                List<TypistQuestionModel> returnToCaller = new List<TypistQuestionModel>();

                foreach (var val in query.ToList())
                {
                    TypistQuestionModel tempModel = new TypistQuestionModel();
                    tempModel = val;
                    if (val.ProofReaderName != null)
                    {
                        tempModel.UserName = getName(val.ProofReaderName);
                    }

                    if (val.TypingBy != null)
                    {
                        tempModel.TypingBy = getName(val.TypingBy);
                    }
                    returnToCaller.Add(tempModel);
                }
                //int totalRecords = query.Count();
                //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                // model.ResultCount = totalRecords;
                //model.tQuestionModel = returnToCaller.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                //model.tQuestionModel = query;
                model.tQuestionModel = returnToCaller.ToList();
            }
            else if (model.RoleName == "Vidhan Sabha Proof Reader")
            {
                var query = (from N in pCtxt.tMemberNotices
                             where (N.TypistRequired == true) && (N.TypistFreezed == true)
                             && N.AssemblyID == model.AssemblyID
                             && N.SessionID == model.SessionID
                             orderby N.NoticeId ascending
                             select new TypistQuestionModel
                             {
                                 NoticeId = N.NoticeId,
                                 NoticeNumber = N.NoticeNumber,
                                 Subject = N.Subject,
                                 NoticeDate = N.NoticeDate,
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == N.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 TypingBy = N.TypistUserIdNotice,
                                 ProofReaderName = N.ProofReaderUserId,
                             }).ToList();
                List<TypistQuestionModel> returnToCaller = new List<TypistQuestionModel>();

                foreach (var val in query.ToList())
                {
                    TypistQuestionModel tempModel = new TypistQuestionModel();
                    tempModel = val;
                    if (val.ProofReaderName != null)
                    {
                        tempModel.UserName = getName(val.ProofReaderName);
                    }

                    if (val.TypingBy != null)
                    {
                        tempModel.TypingBy = getName(val.TypingBy);
                    }
                    returnToCaller.Add(tempModel);
                }
                //int totalRecords = query.Count();
                //var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                // model.ResultCount = totalRecords;
                //model.tQuestionModel = returnToCaller.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                //model.tQuestionModel = query;
                model.tQuestionModel = returnToCaller.ToList();
            }

            return model;
        }

        static string GetAttachmentNotices(object param)
        {
            NoticeContext db = new NoticeContext();

            int NoticeId = Convert.ToInt32(param);


            var query = (from Nst in db.tMemberNotices
                         where Nst.NoticeId == NoticeId
                         select new
                         {
                             Nst.OriDiaryFilePath,
                             Nst.OriDiaryFileName
                         }).FirstOrDefault();

            // For Accessing File Path 
            var FAP = (from SS in db.SiteSettings
                       where SS.SettingName == "SecureFileAccessingUrlPath"
                       select SS.SettingValue).FirstOrDefault();



            return FAP + "/" + query.OriDiaryFilePath + query.OriDiaryFileName;

            //return query.ToList();
        }


        static tMemberNotice GetNoticeTypistDetails(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from N in pCtxt.tMemberNotices
                         where (N.NoticeId == model.NoticeId)
                         select new QuestionModelCustom
                         {
                             NoticeId = N.NoticeId,
                             NoticeNumber = N.NoticeNumber,
                             Subject = N.Subject,
                             MemberId = N.MemberId,
                             Notice = N.Notice,
                             NoticeDate = N.NoticeDate,
                             NoticeTime = N.NoticeTime,
                             MinisterID = N.MinistryId,
                             DepartmentId = N.DepartmentId,
                             // EventId= (from mc in pCtxt.mEvents where mc.EventId == N.NoticeTypeID select mc.EventId).FirstOrDefault(),
                             EventId = N.NoticeTypeID,
                             IsTypistFreeze = N.TypistFreezed,
                             IsProofReading = N.IsProofReading,

                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == N.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }

        static object UpdateNoticesTypist(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice obj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == update.NoticeId);
            obj.Notice = update.Notice;
            obj.Subject = update.Subject;
            obj.SubInCatchWord = update.Subject;
            obj.MinistryId = update.MinistryId;
            obj.TypistFreezed = true;
            obj.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
            obj.DepartmentId = update.DepartmentId;
            obj.IsProofReading = true;
            obj.IsProofReadingDate = DateTime.Now;
            obj.ProofReaderUserId = update.UId;
            qCtxDB.SaveChanges();
            obj.Message = "Update Sucessfully";
            return obj;
        }

        static object SentToProofReader(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice obj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == update.NoticeId);
            obj.Notice = update.Notice;
            obj.Subject = update.Subject;
            obj.SubInCatchWord = update.Subject;
            obj.MinistryId = update.MinistryId;
            obj.TypistFreezed = true;
            //  obj.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
            obj.DepartmentId = update.DepartmentId;
            qCtxDB.SaveChanges();
            obj.Message = "Update Sucessfully";
            return obj;
        }


        static object StayUpdateNotice(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;
            tMemberNotice obj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == update.NoticeId);
            obj.Notice = update.Notice;
            obj.Subject = update.Subject;
            obj.SubInCatchWord = update.Subject;
            obj.MinistryId = update.MinistryId;
            obj.DepartmentId = update.DepartmentId;
            qCtxDB.SaveChanges();
            obj.Message = "Update Sucessfully";

            return obj;

        }

        static object NoticeBackToTypist(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;
            tMemberNotice obj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == update.NoticeId);
            obj.TypistFreezed = null;
            qCtxDB.SaveChanges();
            obj.Message = "Update Sucessfully";

            return obj;

        }


        public static object GetNoticesforCutMotipn(object param)
        {
            CutMotionModel model = param as CutMotionModel;
            NoticeContext pCtxt = new NoticeContext();
            SiteSettingContext settingContext = new SiteSettingContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();
                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            var query = (from N in pCtxt.tMemberNotices
                         where (N.TypistRequired == true) && (N.TypistUserIdNotice == model.UId)
                         && N.AssemblyID == model.AssemblyID
                         && N.SessionID == model.SessionID
                         orderby N.NoticeId ascending
                         select new CutMotionModel
                         {
                             NoticeId = N.NoticeId,
                             NoticeNumber = N.NoticeNumber,
                             Subject = N.Subject,
                             NoticeDate = N.NoticeDate,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == N.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             TypingBy = N.TypistUserIdNotice
                         }).ToList();

            model.tQuestionModel = query;

            return model;
        }


        static object GetDataForcutmotion(object param)
        {
            try
            {

                CutMotionModel model = param as CutMotionModel;
                using (NoticeContext context = new NoticeContext())
                {

                    var List = (from DQuest in context.tMemberNotices
                                where DQuest.IsBracketCM == null && DQuest.NoticeTypeID == model.EventId && DQuest.DemandID == model.CMDemandId && DQuest.AssemblyID == model.AssemblyID && DQuest.SessionID == model.SessionID
                                orderby DQuest.NoticeId ascending
                                select new CutMotionModel
                                {
                                    NoticeId = DQuest.NoticeId,
                                    NoticeNumber = DQuest.NoticeNumber,
                                    Subject = DQuest.Subject,
                                    DepartmentName = (from mc in context.mDepartments where mc.deptId == DQuest.DepartmentId select mc.deptname).FirstOrDefault(),
                                    MinisterName = (from mc in context.mMinistry where mc.MinistryID == DQuest.MinistryId select mc.MinisterName).FirstOrDefault(),
                                    MemberName = (from mc in context.mMembers where mc.MemberCode == DQuest.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    Notice = DQuest.Notice,
                                    NoticeDate = DQuest.NoticeDate,
                                    NoticeTime = DQuest.NoticeTime
                                }).ToList();


                    var results = List.ToList();
                    model.UserList = results;

                    return model;

                }


            }
            catch (Exception)
            {
                return null;
                throw;
            }

        }
        static object GetDataByIdcutmotion(object param)
        {
            try
            {

                CutMotionModel model = param as CutMotionModel;
                using (NoticeContext context = new NoticeContext())
                {

                    model = (from DQuest in context.tMemberNotices
                             where DQuest.NoticeId == model.NoticeId
                             orderby DQuest.NoticeId ascending
                             select new CutMotionModel
                             {
                                 NoticeId = DQuest.NoticeId,
                                 NoticeNumber = DQuest.NoticeNumber,
                                 Subject = DQuest.Subject,
                                 DepartmentName = (from mc in context.mDepartments where mc.deptId == DQuest.DepartmentId select mc.deptname).FirstOrDefault(),
                                 MinisterName = (from mc in context.mMinistry where mc.MinistryID == DQuest.MinistryId select mc.MinisterName).FirstOrDefault(),
                                 MemberName = (from mc in context.mMembers where mc.MemberCode == DQuest.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 Notice = DQuest.Notice,
                                 NoticeDate = DQuest.NoticeDate,
                                 NoticeTime = DQuest.NoticeTime
                             }).FirstOrDefault();


                    // var results = List.ToList();
                    //model.UserList = results;

                    return model;

                }


            }
            catch (Exception)
            {
                return null;
                throw;
            }

        }

        public static object GetCutMotionsForClubWithBracket(object param)
        {
            CutMotionModel model = param as CutMotionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from CutM in pCtxt.tMemberNotices
                         where (CutM.AssemblyID == model.AssemblyID) && (CutM.SessionID == model.SessionID) && (CutM.BracketedWithNoticeIdNo != null) && (CutM.IsCMClubbed == null) && (CutM.MergeDiaryNoCM != null) && (CutM.NoticeTypeID == 74 || CutM.NoticeTypeID == 75 || CutM.NoticeTypeID == 76)
                         select new CutMotionModel
                         {
                             NoticeId = CutM.NoticeId,
                             NoticeNumber = CutM.NoticeNumber,
                             Subject = CutM.Subject,
                             NoticeDate = CutM.NoticeDate,
                             ClubbedDNo = CutM.MergeDiaryNoCM,
                             CMDemandId = CutM.DemandID,
                             EventName = (from E in pCtxt.mEvents where E.EventId == CutM.NoticeTypeID select E.EventName).FirstOrDefault(),
                             DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == CutM.DemandID select mc.DemandName).FirstOrDefault(),
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == CutM.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            model.ResultCount = totalRecords;
            model.UserList = query;

            return model;
        }


        static tMemberNotice GetCutMotionDetailsForMerging(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query1 = (from Cut in pCtxt.tMemberNotices
                          where (Cut.NoticeId == model.NoticeId)
                          select new QuestionModelCustom
                          {
                              BrackWithQIds = Cut.BracketedWithNoticeIdNo,
                              MainDiaryNo = Cut.NoticeNumber,
                              IsClubbed = Cut.IsCMClubbed,
                              ReferenceMemberCode = Cut.ReferenceCMMemberCode,
                          }).FirstOrDefault();



            if (query1.ReferenceMemberCode != null)
            {
                string s = query1.ReferenceMemberCode;
                if (s != null && s != "")
                {
                    string[] values1 = s.Split(',').Distinct().ToArray();

                    for (int i = 0; i < values1.Length; i++)
                    {
                        int MemId = int.Parse(values1[i]);
                        var item = (from questions in pCtxt.tQuestions
                                    select new QuestionModelCustom
                                    {
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                        ConstituencyName = (from t in pCtxt.mConstituency
                                                            join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                            where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                            select t.ConstituencyName).FirstOrDefault(),
                                    }).FirstOrDefault();

                        if (string.IsNullOrEmpty(query1.CMemName))
                        {
                            query1.CMemName = item.MemberName;
                        }
                        else
                        {
                            query1.CMemName = query1.CMemName + "," + item.MemberName;
                        }

                        model.CMemName = query1.CMemName;
                    }

                    model.MainDiaryNo = query1.MainDiaryNo;
                    string[] values = query1.BrackWithQIds.Split(',');

                    foreach (string val in values)
                    {
                        int questValue = Convert.ToInt32(val);


                        var query = (from questions in pCtxt.tMemberNotices
                                     where (questions.NoticeId == questValue)
                                     select new QuestionModelCustom
                                     {
                                         NoticeId = questions.NoticeId,
                                         Subject = questions.Subject,
                                         MemberId = questions.MemberId,
                                         Notice = questions.Notice,
                                         NoticeDate = questions.NoticeDate,
                                         NoticeTypeId = questions.NoticeTypeID,
                                         NoticeTime = questions.NoticeTime,
                                         // ReferenceMemberCode = questions.ReferenceCMMemberCode,
                                         //MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberId select (mc.Prefix + " " + mc.Name)).Distinct().FirstOrDefault(),
                                         NoticeNumber = questions.NoticeNumber,
                                         SubjectCatchWords = questions.SubInCatchWord,
                                         MinistryId = questions.MinistryId.Value,

                                     }).SingleOrDefault();


                        model.tQuestionModel.Add(query);
                    }


                }
            }

            return model;
        }


        public static object SaveMergeCutmotionsEntry(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice NtceObj = (tMemberNotice)param;
            string msg = NtceObj.NoticeNumber;
            try
            {
                tMemberNotice QuesObj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == NtceObj.NoticeId);
                QuesObj.Subject = NtceObj.Subject;
                QuesObj.Notice = NtceObj.Notice;
                QuesObj.IsCMClubbed = true;
                QuesObj.NoticeStatus = (int)NoticeStatusEnum.NoticeFreeze;
                //QuesObj.ReferencedNumber = NtceObj.QuesIds;
                //QuesObj.ReferenceMemberCode = NtceObj.MemIds;
                //QuesObj.IsContentFreeze = true;
                //QuesObj.TypistUserId = NtceObj.TypistUserId;
                QuesObj.IsCMClubbedDate = DateTime.Now;
                // QuesObj.SubInCatchWord = NtceObj.SubInCatchWord;
                // QuesObj.NoticeRecievedDate = DateTime.Now;
                //QuesObj.IsProofReading = true;
                //QuesObj.IsProofReadingDate = DateTime.Now;
                //QuesObj.QuestionStatus = (int)Questionstatus.QuestionFreeze;
                //QuesObj.ProofReaderUserId = NtceObj.RoleName;
                //QuesObj.MergeDiaryNoCM = NtceObj.Msg;

                // update Sequence of Member code according to MergeDiaryNo 
                // int CurrentMC = QuesObj.MemberID;
                // QuesObj.ReferenceMemberCode = "";
                // QuesObj.ReferenceMemberCode = CurrentMC.ToString();

                //string[] SDN = NtceObj.Msg.Split(',');

                //for (int q = 0; q < SDN.Length; q++)
                //{
                //    string DN = SDN[q].ToString().Trim();


                //    var MId = (from Q in qCtxDB.tQuestions
                //               where Q.DiaryNumber == DN
                //               select Q.MemberID).FirstOrDefault();
                //    QuesObj.ReferenceMemberCode = QuesObj.ReferenceMemberCode + "," + MId;
                //}


                qCtxDB.SaveChanges();

                //if (QuesObj.QuestionType == (int)QuestionType.StartedQuestion)
                //{
                //    if (QuesObj.IsFinalApproved == true)
                //    {
                //        msg = "SF_" + QuesObj.DiaryNumber;
                //    }
                //    else
                //    {
                //        msg = "SU_" + QuesObj.DiaryNumber;
                //    }

                //}
                //else if (QuesObj.QuestionType == (int)QuestionType.UnstaredQuestion)
                //{
                //    if (QuesObj.IsFinalApproved == true)
                //    {
                //        msg = "UF_" + QuesObj.DiaryNumber;
                //    }
                //    else
                //    {
                //        msg = "UU_" + QuesObj.DiaryNumber;
                //    }
                //}
            }



            catch (Exception ex)
            {
                msg = ex.Message;
            }


            return msg;
        }


        public static object GetCutMotionsClubbed(object param)
        {
            CutMotionModel model = param as CutMotionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from CutM in pCtxt.tMemberNotices
                         where (CutM.AssemblyID == model.AssemblyID) && (CutM.SessionID == model.SessionID) && (CutM.BracketedWithNoticeIdNo != null) && (CutM.IsCMClubbed != null) && (CutM.MergeDiaryNoCM != null) && (CutM.NoticeTypeID == 74 || CutM.NoticeTypeID == 75 || CutM.NoticeTypeID == 76)
                         select new CutMotionModel
                         {
                             NoticeId = CutM.NoticeId,
                             NoticeNumber = CutM.NoticeNumber,
                             Subject = CutM.Subject,
                             NoticeDate = CutM.NoticeDate,
                             ClubbedDNo = CutM.MergeDiaryNoCM,
                             //DemandNo = CutM.DemandID,
                             EventName = (from E in pCtxt.mEvents where E.EventId == CutM.NoticeTypeID select E.EventName).FirstOrDefault(),
                             DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == CutM.DemandID select mc.DemandName).FirstOrDefault(),
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == CutM.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            model.ResultCount = totalRecords;
            model.UserList = query;

            return model;
        }



        static tMemberNotice GetclubbedCutMotions(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query1 = (from Cut in pCtxt.tMemberNotices
                          where (Cut.NoticeId == model.NoticeId)
                          select new QuestionModelCustom

                          {
                              NoticeId = Cut.NoticeId,
                              Subject = Cut.Subject,
                              Notice = Cut.Notice,
                              NoticeNumber = Cut.NoticeNumber,
                              BrackWithQIds = Cut.MergeDiaryNoCM,
                              MainDiaryNo = Cut.NoticeNumber,
                              IsClubbed = Cut.IsCMClubbed,
                              ReferenceMemberCode = Cut.ReferenceCMMemberCode,
                              NoticeDate = Cut.NoticeDate,
                              CutMotionFilePath = Cut.CutMotionFilePath,
                              CutMotionFileName = Cut.CutMotionFileName,
                              EventName = (from E in pCtxt.mEvents where E.EventId == Cut.NoticeTypeID select E.EventName).FirstOrDefault(),
                              DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == Cut.DemandID select mc.DemandName).FirstOrDefault(),
                              MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Cut.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                          }).FirstOrDefault();

            if (query1.ReferenceMemberCode != null)
            {
                string s = query1.ReferenceMemberCode;
                if (s != null && s != "")
                {
                    string[] values1 = s.Split(',').Distinct().ToArray();

                    for (int i = 0; i < values1.Length; i++)
                    {
                        int MemId = int.Parse(values1[i]);
                        var item = (from questions in pCtxt.tQuestions
                                    select new QuestionModelCustom
                                    {
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                        ConstituencyName = (from t in pCtxt.mConstituency
                                                            join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                                            where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                                            select t.ConstituencyName).FirstOrDefault(),
                                    }).FirstOrDefault();

                        if (string.IsNullOrEmpty(query1.CMemName))
                        {
                            query1.CMemName = item.MemberName;
                        }
                        else
                        {
                            query1.CMemName = query1.CMemName + "," + item.MemberName;
                        }

                        model.CMemName = query1.CMemName;
                        model.NoticeId = query1.NoticeId;
                        model.Subject = query1.Subject;
                        model.Notice = query1.Notice;
                        model.MainDiaryNo = query1.NoticeNumber;
                        model.EventName = query1.EventName;
                        model.DemandName = query1.DemandName;
                        model.BrackWithQIds = query1.BrackWithQIds;
                        model.CutMotionFilePath = query1.CutMotionFilePath;
                        model.CutMotionFileName = query1.CutMotionFileName;

                    }

                    //model.MainDiaryNo = query1.MainDiaryNo;
                    //string[] values = query1.BrackWithQIds.Split(',');

                    //foreach (string val in values)
                    //{
                    //    int questValue = Convert.ToInt32(val);


                    //    var query = (from questions in pCtxt.tMemberNotices
                    //                 where (questions.NoticeId == questValue)
                    //                 select new QuestionModelCustom
                    //                 {
                    //                     NoticeId = questions.NoticeId,
                    //                     Subject = questions.Subject,
                    //                     MemberId = questions.MemberId,
                    //                     Notice = questions.Notice,
                    //                     NoticeDate = questions.NoticeDate,
                    //                     NoticeTypeId = questions.NoticeTypeID,
                    //                     NoticeTime = questions.NoticeTime,
                    //                     // ReferenceMemberCode = questions.ReferenceCMMemberCode,
                    //                     //MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberId select (mc.Prefix + " " + mc.Name)).Distinct().FirstOrDefault(),
                    //                     NoticeNumber = questions.NoticeNumber,
                    //                     SubjectCatchWords = questions.SubInCatchWord,
                    //                     MinistryId = questions.MinistryId.Value,

                    //                 }).SingleOrDefault();


                    //    model.tQuestionModel.Add(query);
                    //}
                    var FAP = (from SS in pCtxt.SiteSettings
                               where SS.SettingName == "SecureFileAccessingUrlPath"
                               select SS.SettingValue).FirstOrDefault();
                    model.CompletePath = FAP + "/" + model.CutMotionFilePath + model.CutMotionFileName;

                }
            }

            return model;
        }

        public static tMemberNotice GetAllDetailsForCutmotionPdfpdf(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            // mMemberAssembly MemAssem = param as mMemberAssembly;
            var query = new List<QuestionModelCustom>();
            query = (from questions in pCtxt.tMemberNotices
                     where questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID && questions.IsCMClubbed == true && questions.NoticeId == model.NoticeId
                     orderby questions.NoticeId ascending
                     select new QuestionModelCustom
                     {
                         NoticeId = questions.NoticeId,
                         Subject = questions.Subject,
                         MemberId = questions.MemberId,
                         Notice = questions.Notice,
                         NoticeDate = questions.NoticeDate,
                         // EventId = questions.EventId,
                         NoticeTime = questions.NoticeTime,
                         NoticeNumber = questions.NoticeNumber,
                         MinisterID = questions.MinistryId,
                         DepartmentId = questions.DepartmentId,
                         IsClubbed = questions.IsCMClubbed,
                         RefMemCode = questions.ReferenceCMMemberCode,
                         MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                         CMDemandId = questions.DemandID,
                         EventName = (from E in pCtxt.mEvents where E.EventId == questions.NoticeTypeID select E.EventNameLocal).FirstOrDefault(),
                         DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == questions.DemandID select mc.DemandName_Local).FirstOrDefault(),
                         QFixDate = questions.SubmittedDate,
                         //SessionDateLocal = (from Session in pCtxt.mSessionDates
                         //                    where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                         //                    select Session.SessionDateLocal).FirstOrDefault(),
                         //SessionDateId = (from Session in pCtxt.mSessionDates
                         //                 where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                         //                 select Session.Id).FirstOrDefault(),

                         //MinistryName = (from M in pCtxt.mMinistry
                         //                where M.MinistryID == questions.MinistryId
                         //                select M.MinistryName).FirstOrDefault(),

                         //SessionDateEnglish = (from Session in pCtxt.mSessionDates
                         //                      where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                         //                      select Session.SessionDate_Local).FirstOrDefault(),


                     }).ToList();


            foreach (var val in query)
            {
                model.DemandName = val.DemandName;
                model.EventName = val.EventName;
                model.CMDemandId = val.CMDemandId;
                model.SubmittedDate = val.QFixDate;
                if (val.IsClubbed == null)
                {
                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',').Distinct().ToArray();

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tMemberNotices
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                            //ConstituencyName = (from t in pCtxt.mConstituency
                                            //                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                            //                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                            //                    select t.ConstituencyName).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemName))
                            {
                                val.CMemName = item.MemberName;
                            }
                            else
                            {
                                val.CMemName = val.CMemName + "," + item.MemberName;
                            }


                        }
                    }


                }


                //val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                //val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                //                              join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                //                              where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                //                              select t.ConstituencyName_Local).FirstOrDefault();
                //val.MinistryNameLocal = (from M in pCtxt.mMinistry
                //                         where M.MinistryID == val.MinisterID
                //                         select M.MinistryNameLocal).FirstOrDefault();
                if (val.IsClubbed != null)
                {
                    string s = val.RefMemCode;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',').Distinct().ToArray();

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item = (from questions in pCtxt.tMemberNotices
                                        select new QuestionModelCustom
                                        {
                                            MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == MemId select (mc.NameLocal)).FirstOrDefault(),
                                            //ConstituencyName = (from t in pCtxt.mConstituency
                                            //                    join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                            //                    where f.MemberID == MemId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                            //                    select t.ConstituencyName_Local).FirstOrDefault(),
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.CMemNameHindi))
                            {
                                val.CMemNameHindi = item.MemberName;
                            }
                            else
                            {
                                val.CMemNameHindi = val.CMemNameHindi + "," + item.MemberName;
                            }


                        }

                    }


                }

            }

            // get Assembly Name and Session Name by Assembly Id and Session Id This is added on 23/01/2015 by Sunil

            //var AssemblyName = (from A in pCtxt.mAssemblies
            //                    where A.AssemblyCode == model.AssemblyID
            //                    select A.AssemblyName).FirstOrDefault();
            //var SName = (from S in pCtxt.mSessions
            //             where S.AssemblyID == model.AssemblyID
            //             && S.SessionCode == model.SessionID
            //             select new { S.SessionName, S.SessionNameLocal }).FirstOrDefault();
            // For Storing File 
            var FL = (from SS in pCtxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();

            // For Accessing File
            //var FAP = (from SS in pCtxt.SiteSettings
            //           where SS.SettingName == "SecureFileAccessingUrlPath"
            //           select SS.SettingValue).FirstOrDefault();


            //var last = query.Last();
            //var First = query.First();

            // model.StampTypeHindi = last.IsHindi;
            //  model.HeaderHindi = First.IsHindi;



            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;


            //model.SessionName = SName.SessionNameLocal;
            //model.AssesmblyName = AssemblyName;

            model.FilePath = FL;
            // model.DocFilePath = FL;

            return model;
        }


        public static object UpdateFilePathandName(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice NtceObj = (tMemberNotice)param;
            string msg = NtceObj.NoticeNumber;
            try
            {
                tMemberNotice QuesObj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == NtceObj.NoticeId);
                QuesObj.CutMotionFileName = NtceObj.FileName;
                QuesObj.CutMotionFilePath = NtceObj.FilePath;
                qCtxDB.SaveChanges();

            }



            catch (Exception ex)
            {
                msg = ex.Message;
            }


            return msg;
        }

        public static object GetListtTransfered(object param)
        {

            DeptRegisterModel model = param as DeptRegisterModel;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from Print in pCtxt.mchangeDepartmentAuditTrail
                         join dC in pCtxt.tQuestions on Print.DiaryNumber equals dC.DiaryNumber
                         where (Print.AadharId == model.AadharId && Print.ChangedDepartmentID == model.DepartmentId)
                         // orderby questions.IsAssignDate descending
                         select new DeptRegisterModel
                         {
                             DiaryNumber = Print.DiaryNumber,
                             AadharId = Print.AadharId,
                             AadharName = Print.UserName,
                             ChangeDateTime = Print.ChangedDate,
                             QuestionType = Print.QuestionType,
                             ChangedDepartment = Print.ChangedDepartmentID,
                             ChangedDepartmentName = (from mc in pCtxt.mDepartments where mc.deptId == Print.DepartmentID select mc.deptname).FirstOrDefault(),
                             OldDepartmentName = (from mc in pCtxt.mDepartments where mc.deptId == Print.ChangedDepartmentID select mc.deptname).FirstOrDefault(),
                             RecievedOnVS = dC.SubmittedDate
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.UserList = results;

            return model;
        }


        public static tMemberNotice GetDetailsNoPdf(object param)
        {

            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();
            var query = new List<QuestionModelCustom>();
            query = (from questions in pCtxt.tMemberNotices
                     where questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID && questions.NoticeId == model.NoticeId
                     orderby questions.NoticeId ascending
                     select new QuestionModelCustom
                     {
                         NoticeId = questions.NoticeId,
                         Subject = questions.Subject,
                         MemberId = questions.MemberId,
                         Notice = questions.Notice,
                         NoticeDate = questions.NoticeDate,
                         NoticeTime = questions.NoticeTime,
                         NoticeNumber = questions.NoticeNumber,
                         MinisterID = questions.MinistryId,
                         DepartmentId = questions.DepartmentId,
                         //IsClubbed = questions.IsCMClubbed,
                         //RefMemCode = questions.ReferenceCMMemberCode,
                         MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                         //CMDemandId = questions.DemandID,
                         //EventName = (from E in pCtxt.mEvents where E.EventId == questions.NoticeTypeID select E.EventNameLocal).FirstOrDefault(),
                         //DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == questions.DemandID select mc.DemandName_Local).FirstOrDefault(),
                         QFixDate = questions.SubmittedDate,
                         IsHindi = questions.IsHindi,
                         //SessionDateLocal = (from Session in pCtxt.mSessionDates
                         //                    where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                         //                    select Session.SessionDateLocal).FirstOrDefault(),
                         //SessionDateId = (from Session in pCtxt.mSessionDates
                         //                 where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                         //                 select Session.Id).FirstOrDefault(),

                         //MinistryName = (from M in pCtxt.mMinistry
                         //                where M.MinistryID == questions.MinistryId
                         //                select M.MinistryName).FirstOrDefault(),

                         //SessionDateEnglish = (from Session in pCtxt.mSessionDates
                         //                      where Session.SessionId == model.SessionID && Session.AssemblyId == model.AssemblyID && Session.SessionDate == model.NoticeDate
                         //                      select Session.SessionDate_Local).FirstOrDefault(),


                     }).ToList();


            foreach (var val in query)
            {


                val.MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault();
                val.ConstituencyName = (from t in pCtxt.mConstituency
                                        join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                        where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                        select t.ConstituencyName).FirstOrDefault();


                val.MemberNameLocal = (from mc in pCtxt.mMembers where mc.MemberCode == val.MemberId select (mc.NameLocal)).FirstOrDefault();
                val.ConstituencyName_Local = (from t in pCtxt.mConstituency
                                              join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                              where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                              select t.ConstituencyName_Local).FirstOrDefault();
                val.ConstituencyID = (from t in pCtxt.mConstituency
                                      join f in pCtxt.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                      where f.MemberID == val.MemberId && t.AssemblyID == model.AssemblyID && f.AssemblyID == model.AssemblyID
                                      select t.ConstituencyCode).FirstOrDefault();


            }


            var FL = (from SS in pCtxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();


            int totalRecords = query.Count();
            var results = query.ToList();
            model.TotalQCount = totalRecords;
            model.tQuestionModel = results;
            model.FilePath = FL;

            return model;
        }
        static tMemberNotice GetDataByQuestionIdForFixing(object param)
        {

            tMemberNotice model = param as tMemberNotice;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.tQuestions
                         join dC in pCtxt.mSessionDates on questions.SessionDateId equals dC.Id
                         where questions.QuestionID == model.QuestionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             AssemblyCode = questions.AssemblyID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             EventId = questions.EventId,
                             NoticeTime = questions.NoticeRecievedTime,
                             IsContentFreeze = questions.IsContentFreeze,
                             DiaryNumber = questions.DiaryNumber,
                             MinisterID = questions.MinistryId,
                             DepartmentId = questions.DepartmentID,
                             Checked = questions.IsProofReading.HasValue,
                             IsTypistFreeze = questions.IsTypistFreeze,
                             ConstituencyName = questions.ConstituencyName,
                             IsBracketed = questions.IsBracket,
                             BracketedDNo = questions.BracketedWithDNo,
                             IsClubbed = questions.IsClubbed,
                             RefMemCode = questions.ReferenceMemberCode,
                             IsHindi = questions.IsHindi,
                             DesireLayingDate = questions.IsFixedDate,
                             SubmittedDate = questions.SubmittedDate,
                             SessionDate = dC.SessionDate,
                             DepartmentName = (from dept in pCtxt.mDepartments where dept.deptId == questions.DepartmentID select dept.deptname).FirstOrDefault(),
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             MinistryName = (from M in pCtxt.mMinistry
                                             where M.MinistryID == questions.MinistryId
                                             select M.MinistryName).FirstOrDefault(),


                         }).ToList();
            var results = query.ToList();
            model.tQuestionModel = results;
            return model;
        }
        public static object LocktransUnstarred(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();

            DiaryModel Obj = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();
            int Qtype = 0;
            try
            {

                string[] Ids = Obj.QuesIds.Split(',');

                for (int i = 0; i < Ids.Length; i++)
                {

                    int id = Convert.ToInt16(Ids[i]);
                    QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                    QuesObj.IsLocktranslator = true;
                    DiaCtx.SaveChanges();

                }

                //Obj.Message = Qtype + "Fixed Question(s) Finally Approved !";

                DiaCtx.Close();

            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;
        }

        public static object UnLocktransUnstarred(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();

            DiaryModel Obj = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();
            int Qtype = 0;
            try
            {

                string[] Ids = Obj.QuesIds.Split(',');

                for (int i = 0; i < Ids.Length; i++)
                {

                    int id = Convert.ToInt16(Ids[i]);
                    QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                    QuesObj.IsLocktranslator = null;
                    DiaCtx.SaveChanges();

                }

                //Obj.Message = Qtype + "Fixed Question(s) Finally Approved !";

                DiaCtx.Close();

            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;
        }
        static object GetDataForNoticeBrackt(object param)
        {
            try
            {

                CutMotionModel model = param as CutMotionModel;
                using (NoticeContext context = new NoticeContext())
                {

                    var List = (from DQuest in context.tMemberNotices
                                where DQuest.IsBracketCM == null && DQuest.NoticeTypeID == model.EventId && DQuest.DepartmentId == model.DepartmentId && DQuest.AssemblyID == model.AssemblyID && DQuest.SessionID == model.SessionID
                                orderby DQuest.NoticeId ascending
                                select new CutMotionModel
                                {
                                    NoticeId = DQuest.NoticeId,
                                    NoticeNumber = DQuest.NoticeNumber,
                                    Subject = DQuest.Subject,
                                    DepartmentName = (from mc in context.mDepartments where mc.deptId == DQuest.DepartmentId select mc.deptname).FirstOrDefault(),
                                    MinisterName = (from mc in context.mMinistry where mc.MinistryID == DQuest.MinistryId select mc.MinisterName).FirstOrDefault(),
                                    MemberName = (from mc in context.mMembers where mc.MemberCode == DQuest.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    Notice = DQuest.Notice,
                                    NoticeDate = DQuest.NoticeDate,
                                    NoticeTime = DQuest.NoticeTime
                                }).ToList();


                    var results = List.ToList();
                    model.UserList = results;

                    return model;

                }


            }
            catch (Exception)
            {
                return null;
                throw;
            }

        }

        public static object GetNoticesForClubWithBracket(object param)
        {
            CutMotionModel model = param as CutMotionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from CutM in pCtxt.tMemberNotices
                         where (CutM.AssemblyID == model.AssemblyID) && (CutM.SessionID == model.SessionID) && (CutM.BracketedWithNoticeIdNo != null) && (CutM.IsCMClubbed == null) && (CutM.MergeDiaryNoCM != null) && (CutM.NoticeTypeID != 74) && (CutM.NoticeTypeID != 75) && (CutM.NoticeTypeID != 76)
                         select new CutMotionModel
                         {
                             NoticeId = CutM.NoticeId,
                             NoticeNumber = CutM.NoticeNumber,
                             Subject = CutM.Subject,
                             NoticeDate = CutM.NoticeDate,
                             ClubbedDNo = CutM.MergeDiaryNoCM,
                             CMDemandId = CutM.DemandID,
                             EventName = (from E in pCtxt.mEvents where E.EventId == CutM.NoticeTypeID select E.EventName).FirstOrDefault(),
                             DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == CutM.DemandID select mc.DemandName).FirstOrDefault(),
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == CutM.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            model.ResultCount = totalRecords;
            model.UserList = query;

            return model;
        }
        public static object GetNoticesClubbed(object param)
        {
            CutMotionModel model = param as CutMotionModel;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from CutM in pCtxt.tMemberNotices
                         where (CutM.AssemblyID == model.AssemblyID) && (CutM.SessionID == model.SessionID) && (CutM.BracketedWithNoticeIdNo != null) && (CutM.IsCMClubbed != null) && (CutM.MergeDiaryNoCM != null) && (CutM.NoticeTypeID != 74) && (CutM.NoticeTypeID != 75) && (CutM.NoticeTypeID != 76)
                         select new CutMotionModel
                         {
                             NoticeId = CutM.NoticeId,
                             NoticeNumber = CutM.NoticeNumber,
                             Subject = CutM.Subject,
                             NoticeDate = CutM.NoticeDate,
                             ClubbedDNo = CutM.MergeDiaryNoCM,
                             //DemandNo = CutM.DemandID,
                             EventName = (from E in pCtxt.mEvents where E.EventId == CutM.NoticeTypeID select E.EventName).FirstOrDefault(),
                             DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == CutM.DemandID select mc.DemandName).FirstOrDefault(),
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == CutM.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                         }).ToList();

            int totalRecords = query.Count();
            model.Noticeclubbed = totalRecords;
            model.UserList = query;

            return model;
        }

        public static object SaveMergeNoticeEntry(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice NtceObj = (tMemberNotice)param;
            string msg = NtceObj.NoticeNumber;
            try
            {
                tMemberNotice QuesObj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == NtceObj.NoticeId);
                QuesObj.Subject = NtceObj.Subject;
                QuesObj.Notice = NtceObj.Notice;
                QuesObj.IsCMClubbed = true;
                QuesObj.NoticeStatus = (int)NoticeStatusEnum.NoticeFreeze;
                QuesObj.IsCMClubbedDate = DateTime.Now;
                qCtxDB.SaveChanges();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
            }
            NtceObj.Message = msg;

            NtceObj.countClub = (from CutM in pCtxt.tMemberNotices
                                 where (CutM.AssemblyID == NtceObj.AssemblyID) && (CutM.SessionID == NtceObj.SessionID) && (CutM.BracketedWithNoticeIdNo != null) && (CutM.IsCMClubbed == null) && (CutM.MergeDiaryNoCM != null) && (CutM.NoticeTypeID != 74) && (CutM.NoticeTypeID != 75) && (CutM.NoticeTypeID != 76)
                                 select new CutMotionModel
                                 {
                                     NoticeId = CutM.NoticeId,
                                     NoticeNumber = CutM.NoticeNumber,
                                     Subject = CutM.Subject,
                                     NoticeDate = CutM.NoticeDate,
                                     ClubbedDNo = CutM.MergeDiaryNoCM,
                                     //DemandNo = CutM.DemandID,
                                     EventName = (from E in pCtxt.mEvents where E.EventId == CutM.NoticeTypeID select E.EventName).FirstOrDefault(),
                                     DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == CutM.DemandID select mc.DemandName).FirstOrDefault(),
                                     MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == CutM.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                                 }).ToList().Count();

            NtceObj.countClubbed = (from CutM in pCtxt.tMemberNotices
                                    where (CutM.AssemblyID == NtceObj.AssemblyID) && (CutM.SessionID == NtceObj.SessionID) && (CutM.BracketedWithNoticeIdNo != null) && (CutM.IsCMClubbed != null) && (CutM.MergeDiaryNoCM != null) && (CutM.NoticeTypeID != 74) && (CutM.NoticeTypeID != 75) && (CutM.NoticeTypeID != 76)
                                    select new CutMotionModel
                                    {
                                        NoticeId = CutM.NoticeId,
                                        NoticeNumber = CutM.NoticeNumber,
                                        Subject = CutM.Subject,
                                        NoticeDate = CutM.NoticeDate,
                                        ClubbedDNo = CutM.MergeDiaryNoCM,
                                        //DemandNo = CutM.DemandID,
                                        EventName = (from E in pCtxt.mEvents where E.EventId == CutM.NoticeTypeID select E.EventName).FirstOrDefault(),
                                        DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == CutM.DemandID select mc.DemandName).FirstOrDefault(),
                                        MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == CutM.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                                    }).ToList().Count();

            return NtceObj;
        }

        public static object GetMembers(object param)
        {
            NoticeContext pCtxt = new NoticeContext();
            tMemberNotice Obj = param as tMemberNotice;
            var Memlist = (from M in pCtxt.mMembers
                           join MA in pCtxt.mMemberAssembly on M.MemberCode equals MA.MemberID
                           join Const in pCtxt.mConstituency on MA.ConstituencyCode equals Const.ConstituencyCode
                           where (Const.AssemblyID == Obj.AssemblyID)
                               && (MA.AssemblyID == Obj.AssemblyID)
                               && !(from ExcepMinis in pCtxt.mMinistry where ExcepMinis.AssemblyID == Obj.AssemblyID select ExcepMinis.MemberCode).Contains(M.MemberCode)
                               && M.Active == true
                           orderby M.Name
                           select new DiaryModel
                           {
                               MemberId = MA.MemberID,
                               MemberName = M.Name,
                               ConstituencyName = Const.ConstituencyName,
                               ConstituencyCode = Const.ConstituencyCode
                           }).ToList();

            //foreach (var item in Memlist)
            //{
            //    item.MemberName = item.MemberName + " (" + item.ConstituencyName + "-" + item.ConstituencyCode.ToString() + ")";
            //}
            Obj.NewmemberList = Memlist;
            return Obj;
        }
        /////added by dharmendra
        static object GetMemberNameByIds(object param)
        {
            mUsers parameter = param as mUsers;
            string userNames = "";
            List<int> TagIds = parameter.UserName.Split(',').Select(int.Parse).ToList();
            MemberContext db = new MemberContext();
            //var query = db.mMembers.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
            var data = db.mMembers.Where(t => TagIds.Contains(t.MemberCode)).ToList();
            return data;
        }


        static object UpdateDiaryQuestions(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;
            tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
            obj.Subject = update.Subject;
            obj.MainQuestion = update.MainQuestion;
            obj.MinistryId = update.MinistryId;
            obj.DepartmentID = update.DepartmentID;
            obj.ConstituencyName = update.ConstituencyName;
            obj.ConstituencyNo = update.ConstituencyNo;
            obj.MemberID = update.MemberID;
            obj.QuestionType = update.QuestionType;
            qCtxDB.SaveChanges();
            return null;
        }

        static object UpdateDiaryNotices(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tMemberNotice update = param as tMemberNotice;
            tMemberNotice obj = qCtxDB.tMemberNotice.Single(m => m.NoticeId == update.NoticeId);
            obj.Subject = update.Subject;
            obj.MinistryId = update.MinistryId;
            obj.DepartmentId = update.DepartmentId;
            obj.NoticeTypeID = update.EventId;
            if (update.EventId == 39)
            {
                obj.TypistRequired = true;
            }
            if (update.EventId != 39)
            {
                obj.TypistRequired = null;
            }
            obj.MemberId = update.MemberId.Value;
            qCtxDB.SaveChanges();
            return null;
        }

        static object CheckQStatus(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel updateSQ = param as tQuestionModel;

            String Exist = "NotChange";
            var query = (from tQ in qCtxDB.tQuestions
                         where (tQ.QuestionID == updateSQ.QuestionID) && (tQ.QuestionNumber != null) && (tQ.IsFinalApproved == null)
                         select tQ).ToList().Count();
            if (query == 1)
            {

                return Exist.ToString();
            }
            if (query == 0)
            {
                string NotExist = "Change";

                return NotExist.ToString();
            }

            return Exist.ToString();
        }

        static object GetDQuestionNo(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion model = param as tQuestion;
            //tQuestionModel updateSQ = param as tQuestionModel;

            var query = (from questions in qCtxDB.tQuestions
                             //join dC in qCtxDB.mSessionDates on questions.SessionDateId equals dC.Id
                         where questions.DiaryNumber == model.DiaryNumber
                         select new tQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             QuestionNumber = questions.QuestionNumber,
                             IsFixedDate = questions.IsFixedTodayDate,
                             MemberN = (from mc in qCtxDB.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             QuestionTypeId = questions.QuestionType,
                         }).ToList();

            int totalRecords = query.Count();
            model.ResultCount = totalRecords;
            model.tQuestionModel = query;
            return model;
        }


        static object UpdatefixtoUnfix(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion model = param as tQuestion;
            var query = (from e in qCtxDB.tQuestions where e.DiaryNumber == model.DiaryNumber select e).SingleOrDefault();

            tQuestion obj = qCtxDB.tQuestions.Single(m => m.DiaryNumber == model.BracketedWithQNo);
            obj.IsFixed = query.IsFixed;
            obj.IsFixedDate = query.IsFixedDate;
            obj.SessionDateId = query.SessionDateId;
            obj.IsFixedTodayDate = query.IsFixedTodayDate;
            obj.QuestionNumber = query.QuestionNumber;
            qCtxDB.SaveChanges();

            tQuestion objnew = qCtxDB.tQuestions.Single(m => m.DiaryNumber == model.DiaryNumber);
            objnew.IsFixed = null;
            objnew.IsFixedDate = null;
            objnew.SessionDateId = null;
            objnew.IsFixedTodayDate = null;
            objnew.QuestionNumber = null;
            qCtxDB.SaveChanges();
            return model;
        }


        static object AssignNodalOffice(object param)
        {

            NoticeContext qCtxDB = new NoticeContext();
            tQuestion update = param as tQuestion;

            try
            {
                string s = update.QuestionValue;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int OfficeId = int.Parse(values[i]);
                        mOffice obj = qCtxDB.mOffice.Single(m => m.OfficeId == OfficeId);
                        obj.IsNodalOffice = true;
                        qCtxDB.SaveChanges();

                    }

                }

                return "Nodal Office created Successfully ! ";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }


        static object DeleteNodalOffice(object param)
        {

            NoticeContext qCtxDB = new NoticeContext();
            tQuestion update = param as tQuestion;

            try
            {
                string s = update.QuestionValue;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int OfficeId = int.Parse(values[i]);
                        mOffice obj = qCtxDB.mOffice.Single(m => m.OfficeId == OfficeId);
                        obj.IsNodalOffice = null;
                        qCtxDB.SaveChanges();
                    }

                }

                return "Nodal Office Removed Successfully ! ";
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }


        public static object GetIntroducedBills(object param)
        {

            OnlineMemberQmodel model = param as OnlineMemberQmodel;
            NoticeContext pCtxt = new NoticeContext();

            var query = (from Bills in pCtxt.tBillRegister
                         join PL in pCtxt.tPaperLaidVs on Bills.PaperLaidId equals PL.PaperLaidId
                         join PTemp in pCtxt.tPaperLaidTemps on Bills.PaperLaidId equals PTemp.PaperLaidId
                         where (Bills.AssemblyId == model.AssemblyID)
                         && (Bills.SessionId == model.SessionID)
                         && (PTemp.PaperLaidTempId == PL.DeptActivePaperId)
                         && (PL.IsLaid == null || PL.IsLaid == false)
                         orderby Bills.BillId ascending
                         select new OnlineMemberQmodel
                         {
                             BillName = Bills.BillName,
                             BillNo = Bills.BillNumber,
                             EvidhaRNo = PTemp.evidhanReferenceNumber,
                             DeptSubmittedDate = PTemp.DeptSubmittedDate,
                             BillsFilePath = PTemp.SignedFilePath,
                         }).ToList();

            model.ResultCount = query.Count();
            model.tQuestionModel = query;
            return model;
        }
        static object GetDQuestions(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            // tQuestion model = param as tQuestion;
            tQuestionModel model = param as tQuestionModel;

            var query = (from questions in qCtxDB.tQuestions
                         orderby questions.QuestionID
                         //join dC in qCtxDB.mSessionDates on questions.SessionDateId equals dC.Id
                         where questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                         select new tQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,

                             QuestionNumber = questions.QuestionNumber,
                             IsFixedDate = questions.IsFixedDate,

                             MainQuestion = questions.MainQuestion,
                             NoticeRecievedDate = questions.NoticeRecievedDate,
                             displayTime = questions.NoticeRecievedTime,
                             MemberN = (from mc in qCtxDB.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             QuestionTypeId = questions.QuestionType,
                             ConstituencyName = questions.ConstituencyName,
                             DepartmentN = (from mc in qCtxDB.mDepartment where mc.deptId == questions.DepartmentID select mc.deptname).FirstOrDefault(),
                         }).ToList();

            return query.ToList();
        }

        public static object GetDQuestionsByTypeId(object param)
        {

            QuestionsContext qCtxDB = new QuestionsContext();
            // tQuestion model = param as tQuestion;
            tQuestionModel model = param as tQuestionModel;
            int QTypeId = Convert.ToInt16(model.QuestionType);
            var query = (from questions in qCtxDB.tQuestions
                         orderby questions.QuestionID
                         //join dC in qCtxDB.mSessionDates on questions.SessionDateId equals dC.Id
                         where questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                         && questions.QuestionType == QTypeId
                         select new tQuestionModel
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,

                             QuestionNumber = questions.QuestionNumber,
                             IsFixedDate = questions.IsFixedDate,

                             MainQuestion = questions.MainQuestion,
                             NoticeRecievedDate = questions.NoticeRecievedDate,
                             displayTime = questions.NoticeRecievedTime,
                             MemberN = (from mc in qCtxDB.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             QuestionTypeId = questions.QuestionType,
                             ConstituencyName = questions.ConstituencyName,
                             DepartmentN = (from mc in qCtxDB.mDepartment where mc.deptId == questions.DepartmentID select mc.deptname).FirstOrDefault(),
                         }).ToList();

            return query.ToList();
        }











        public static object GetDNotices(object param)
        {

            NoticeContext qCtxDB = new NoticeContext();
            // tQuestion model = param as tQuestion;
            tMemberNoticeModel model = param as tMemberNoticeModel;


            var query = (from questions in qCtxDB.tMemberNotices
                             //join dC in qCtxDB.mSessionDates on questions.SessionDateId equals dC.Id
                             //join tps in qCtxDB.tPaperLaidVs on questions.PaperLaidId equals tps.PaperLaidId
                         where questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID
                         orderby questions.NoticeTypeID
                         select new tMemberNoticeModel
                         {
                             NoticeTypeID = questions.NoticeTypeID,
                             NoticeLocal = (from mc in qCtxDB.mEvents where mc.EventId == questions.NoticeTypeID select mc.EventName).FirstOrDefault(),
                             NoticeNumber = questions.NoticeNumber,
                             Subject = questions.Subject,

                             NoticeDate = questions.NoticeDate,
                             NoticeTime = questions.NoticeTime,
                             MemberName = (from mc in qCtxDB.mMembers where mc.MemberCode == questions.MemberId select mc.Name).FirstOrDefault(),

                             LaidOnDate = (from mc in qCtxDB.tPaperLaidVs
                                           join tps in qCtxDB.AdminLOB on mc.LOBRecordId equals tps.DraftRecordId
                                           where mc.PaperLaidId == questions.PaperLaidId
                                           && tps.IsLaid == true
                                           select tps.SessionDate).FirstOrDefault(),
                             //  LaidOnDate = (from mc in qCtxDB.AdminLOB where mc.DraftRecordId == tps.LOBRecordId select mc.SessionDate).FirstOrDefault(),

                             DepartmentName = (from mc in qCtxDB.mDepartments where mc.deptId == questions.DepartmentId select mc.deptname).FirstOrDefault(),
                         }).ToList();

            return query.ToList();
        }

        public static object GetNoticesByTypeId(object param)
        {

            NoticeContext qCtxDB = new NoticeContext();
            // tQuestion model = param as tQuestion;
            tMemberNoticeModel model = param as tMemberNoticeModel;

            var query = (from questions in qCtxDB.tMemberNotices
                             //join dC in qCtxDB.mSessionDates on questions.SessionDateId equals dC.Id
                         where questions.AssemblyID == model.AssemblyID && questions.SessionID == model.SessionID
                         && questions.NoticeTypeID == model.NoticeTypeID
                         orderby questions.NoticeTypeID
                         select new tMemberNoticeModel
                         {
                             NoticeTypeID = questions.NoticeTypeID,
                             NoticeLocal = (from mc in qCtxDB.mEvents where mc.EventId == questions.NoticeTypeID select mc.EventName).FirstOrDefault(),
                             NoticeNumber = questions.NoticeNumber,
                             Subject = questions.Subject,

                             NoticeDate = questions.NoticeDate,
                             NoticeTime = questions.NoticeTime,
                             MemberName = (from mc in qCtxDB.mMembers where mc.MemberCode == questions.MemberId select mc.Name).FirstOrDefault(),

                             LaidOnDate = (from mc in qCtxDB.tPaperLaidVs
                                           join tps in qCtxDB.AdminLOB on mc.LOBRecordId equals tps.DraftRecordId
                                           where mc.PaperLaidId == questions.PaperLaidId
                                           && tps.IsLaid == true
                                           select tps.SessionDate).FirstOrDefault(),
                             DepartmentName = (from mc in qCtxDB.mDepartments where mc.deptId == questions.DepartmentId select mc.deptname).FirstOrDefault(),
                         }).ToList();

            return query.ToList();
        }
    }
}

