﻿using SBL.DAL;
using SBL.DomainModel.Models.Category;
using SBL.DomainModel.Models.Speech;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.ASpeech
{
    class ASpeechContext : DBBase<ASpeechContext>
    {
        public ASpeechContext()
            : base("eVidhan")
        {
        }
        public DbSet<tSpeech> mSpeech { get; set; }
        public virtual DbSet<Category> Category { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {

                    case "AddSpeechCategory": { return AddSpeechCategory(param.Parameter); }

                    case "GetAllSpeechCategory": { return GetAllSpeechCategory(param.Parameter); }

                    case "GetSpeechCategoryById": { return GetSpeechCategoryById(param.Parameter); }

                    case "UpdateSpeechCategory": { return UpdateSpeechCategory(param.Parameter); }

                    case "DeleteSpeechCategory": { return DeleteSpeechCategory(param.Parameter); }

                    //Code for Speech CRUDS
                    case "AddSpeech": { return AddSpeech(param.Parameter); }

                    case "GetAllSpeech": { return GetAllSpeech(param.Parameter); }

                    case "GetSpeechById": { return GetSpeechById(param.Parameter); }

                    case "UpdateSpeech": { return UpdateSpeech(param.Parameter); }

                    case "DeleteSpeech": { return DeleteSpeech(param.Parameter); }

                    case "IsSpeechCategoryChildExist": { return IsSpeechCategoryChildExist(param.Parameter); }



                }
            }

            return null;
        }

        private static object IsSpeechCategoryChildExist(object p)
        {
            try
            {
                using (ASpeechContext ctx = new ASpeechContext())
                {
                    Category data = (Category)p;

                    var isExist = (from a in ctx.mSpeech where a.CategoryID == data.CategoryID select a).Count() > 0;

                    return isExist;
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }



        #region SpeechCode

        private static object DeleteSpeech(object p)
        {
            try
            {
                tSpeech DataTodelete = p as tSpeech;
                int id = DataTodelete.SpeechID;
                using (ASpeechContext db = new ASpeechContext())
                {
                    var SpeechToDelete = db.mSpeech.SingleOrDefault(a => a.SpeechID == id);
                    db.mSpeech.Remove(SpeechToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

            return true;
        }

        private static object UpdateSpeech(object p)
        {
            try
            {
                tSpeech Updatespeech = p as tSpeech;
                using (ASpeechContext db = new ASpeechContext())
                {
                    var data = (from a in db.mSpeech where Updatespeech.SpeechID == a.SpeechID select a).FirstOrDefault();


                    data.ModifiedDate = DateTime.Now;
                    data.SpeechDescription = Updatespeech.SpeechDescription;
                    data.LocalSpeechTitle = Updatespeech.LocalSpeechTitle;
                    data.LocalSpeechDescription = Updatespeech.LocalSpeechDescription;
                    data.SpeechTitle = Updatespeech.SpeechTitle;
                    data.CategoryID = Updatespeech.CategoryID;
                    data.Status = Updatespeech.Status;
                    data.SpeechType = Updatespeech.SpeechType;
                    data.FileName = Updatespeech.FileName;
                    data.FilePath = Updatespeech.FilePath;
                    data.FileName1 = Updatespeech.FileName1;
                    data.FilePath1 = Updatespeech.FilePath1;
                    db.mSpeech.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return Updatespeech;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private static object GetSpeechById(object p)
        {
            try
            {
                using (ASpeechContext db = new ASpeechContext())
                {
                    tSpeech speechToEdit = p as tSpeech;

                    var data = db.mSpeech.SingleOrDefault(o => o.SpeechID == speechToEdit.SpeechID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        private static object GetAllSpeech(object p)
        {
            try
            {
                using (ASpeechContext db = new ASpeechContext())
                {
                    var data = (from a in db.mSpeech join category in db.Category on a.CategoryID equals category.CategoryID 
                                select new 
                                { SpeechId = a.SpeechID, 
                                    Speech_Description = a.SpeechDescription, 
                                    Local_SpeechDescription = a.LocalSpeechDescription, 
                                    Date_Of_Event = a.CreatedDate, 
                                    Speech_Title = a.SpeechTitle, 
                                    Local_SpeechTitle = a.LocalSpeechTitle, 
                                    CategoryTypeName = category.CategoryName, 
                                    FileName = a.FileName,
                                    FilePath = a.FilePath,
                                    FileName1 = a.FileName1,
                                    FilePath1 = a.FilePath1,
                                    Status = a.Status,Hits = a.Hits }).ToList();//db.mNews.ToList();

                    List<tSpeech> result = new List<tSpeech>();

                    foreach (var a in data)
                    {
                        tSpeech partdata = new tSpeech();
                        partdata.SpeechID = a.SpeechId;
                        partdata.SpeechDescription = a.Speech_Description!=null?a.Speech_Description.Length > 100 ? a.Speech_Description.Substring(0, 100) + "..." : a.Speech_Description:null;
                        partdata.LocalSpeechDescription = a.Local_SpeechDescription!=null?a.Local_SpeechDescription.Length > 100 ? a.Local_SpeechDescription.Substring(0, 100) + "..." : a.Local_SpeechDescription:null;
                        partdata.SpeechTitle = a.Speech_Title;
                        partdata.StartPublish = a.Date_Of_Event;
                        partdata.LocalSpeechTitle = a.Local_SpeechTitle;
                        partdata.CategoryName = a.CategoryTypeName;
                        partdata.Status = a.Status;
                        partdata.Hits = a.Hits;
                        partdata.FileName = a.FileName;
                        partdata.FilePath = a.FilePath;
                        partdata.FileName1 = a.FileName1;
                        partdata.FilePath1 = a.FilePath1;
                        result.Add(partdata);
                    }

                    return result;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return null;
        }

        private static object AddSpeech(object p)
        {
            try
            {
                using (ASpeechContext newsContext = new ASpeechContext())
                {
                    tSpeech news = p as tSpeech;
                    news.CreatedDate = DateTime.Now;
                    news.StartPublish = DateTime.Now;
                    news.ModifiedDate = DateTime.Now;
                    news.StopPublish = DateTime.Now;
                    newsContext.mSpeech.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        #endregion


        #region SpeechCategory

        private static object DeleteSpeechCategory(object p)
        {
            try
            {
                Category DataTodelete = p as Category;
                int id = DataTodelete.CategoryID;
                using (ASpeechContext db = new ASpeechContext())
                {
                    var NewsToDelete = db.Category.SingleOrDefault(a => a.CategoryID == id);
                    db.Category.Remove(NewsToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return true;
        }

        private static object UpdateSpeechCategory(object p)
        {
            try
            {
                Category UpdateNews = p as Category;
                using (ASpeechContext db = new ASpeechContext())
                {
                    UpdateNews.CategoryType = 4;
                    UpdateNews.CreatedDate = DateTime.Now;
                    UpdateNews.ModifiedDate = DateTime.Now;
                    UpdateNews.StartPublish = DateTime.Now;
                    UpdateNews.StopPublish = DateTime.Now;
                    db.Category.Attach(UpdateNews);
                    db.Entry(UpdateNews).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateNews;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static object GetSpeechCategoryById(object p)
        {
            try
            {
                using (ASpeechContext db = new ASpeechContext())
                {
                    var category = p as Category;

                    var data = (from value in db.Category where value.CategoryID == category.CategoryID select value).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static object AddSpeechCategory(object p)
        {
            try
            {
                using (ASpeechContext newsContext = new ASpeechContext())
                {
                    Category news = p as Category;
                    news.CategoryType = 4;
                    news.CreatedDate = DateTime.Now;
                    news.ModifiedDate = DateTime.Now;
                    news.StartPublish = DateTime.Now;
                    news.StopPublish = DateTime.Now;
                    newsContext.Category.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        private static object GetAllSpeechCategory(object p)
        {
            try
            {
                using (ASpeechContext db = new ASpeechContext())
                {

                    var data = (from value in db.Category where value.CategoryType == 4 && value.Status == 1 select value).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
