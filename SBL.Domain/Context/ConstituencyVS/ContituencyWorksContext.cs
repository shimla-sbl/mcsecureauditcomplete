﻿using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.ConstituencyVS;
using System.Data.Entity;
using SBL.Service.Common;

namespace SBL.Domain.Context.ConstituencyVS
{
  public class ContituencyWorksContext:DBBase<ContituencyWorksContext>
   {
 
      public ContituencyWorksContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<workType> workType { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateWorks": { CreateWorks(param.Parameter); break; }
                case "UpdateWorks": { return UpdateWorks(param.Parameter); }
                case "DeleteWorks": { return DeleteWorks(param.Parameter); }
                case "GetAllWorks": { return GetAllWorks(); }
                case "GetWorksById": { return GetWorksById(param.Parameter); }

            }
            return null;
        }

       static void CreateWorks(object param)
        {
            try
            {
                using (ContituencyWorksContext db = new ContituencyWorksContext())
                {
                    workType model = param as workType;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.workType.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
}

        static object UpdateWorks(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ContituencyWorksContext db = new ContituencyWorksContext())
            {
                workType model = param as workType;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;
                db.workType.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllWorks();
        }
        static object DeleteWorks(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ContituencyWorksContext db = new ContituencyWorksContext())
            {
                workType parameter = param as workType;
                workType stateToRemove = db.workType.SingleOrDefault(a => a.workID == parameter.workID);
                if (stateToRemove != null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllWorks();
        }

        static List<workType> GetAllWorks()
        {
            ContituencyWorksContext db = new ContituencyWorksContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.workType
                         orderby a.workID descending
                         where a.IsDeleted == false
                         select a).ToList();

            return query;
        }

        static workType GetWorksById(object param)
        {
            workType parameter = param as workType;
            ContituencyWorksContext db = new ContituencyWorksContext();
            var query = db.workType.SingleOrDefault(a => a.workID == parameter.workID);
            return query;
        }


   }
}