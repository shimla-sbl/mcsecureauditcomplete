﻿using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.ConstituencyVS;
using System.Data.Entity;
using SBL.Service.Common;

namespace SBL.Domain.Context.ConstituencyVS
{
    public class ContituencySchemeEnumsContext : DBBase<ContituencySchemeEnumsContext>
    {
        public ContituencySchemeEnumsContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mProgramme> mProgramme { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateProgramme": { CreateProgramme(param.Parameter); break; }
                case "UpdateProgramme": { return UpdateProgramme(param.Parameter); }
                case "DeleteProgramme": { return DeleteProgramme(param.Parameter); }
                case "GetAllProgramme": { return GetAllProgramme(); }
                case "GetProgrammeById": { return GetProgrammeById(param.Parameter); }

            }
            return null;
        }

        static void CreateProgramme(object param)
        {
            try
            {
                using (ContituencySchemeEnumsContext db = new ContituencySchemeEnumsContext())
                {
                    mProgramme model = param as mProgramme;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mProgramme.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateProgramme(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ContituencySchemeEnumsContext db = new ContituencySchemeEnumsContext())
            {
                mProgramme model = param as mProgramme;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;
                db.mProgramme.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllProgramme();
        }


        static object DeleteProgramme(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ContituencySchemeEnumsContext db = new ContituencySchemeEnumsContext())
            {
                mProgramme parameter = param as mProgramme;
                mProgramme stateToRemove = db.mProgramme.SingleOrDefault(a => a.ProgramID == parameter.ProgramID);
                if (stateToRemove != null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllProgramme();
        }

        static List<mProgramme> GetAllProgramme()
        {
            ContituencySchemeEnumsContext db = new ContituencySchemeEnumsContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.mProgramme
                         orderby a.ProgramID descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mProgramme GetProgrammeById(object param)
        {
            mProgramme parameter = param as mProgramme;
            ContituencySchemeEnumsContext db = new ContituencySchemeEnumsContext();
            var query = db.mProgramme.SingleOrDefault(a => a.ProgramID == parameter.ProgramID);
            return query;
        }

    }
}