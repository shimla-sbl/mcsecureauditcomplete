﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.Constituency;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.ConstituencyVS;
using SBL.DomainModel.Models.CutMotionDemand;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SBL.DomainModel.Models.SubDivision;
using SBL.DomainModel.Models.mSchemeType;
using SBL.Domain.Context.RecipientGroups;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.ComplexModel.LinqKitSource;
using System.Data.Entity.Infrastructure;
using System.Data;
using SBL.DomainModel.ComplexModel.ErrorLog;

namespace SBL.Domain.Context.ConstituencyVS
{
    public class ConstituencyContext : DBBase<ConstituencyContext>
    {
        public ConstituencyContext()
            : base("eVidhan")
        {
            this.Database.CommandTimeout = 180;
            this.SetCommandTimeOut(180);
        }

        public void SetCommandTimeOut(int Timeout)
        {
            var objectContext = (this as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = Timeout;
        }
        #region tables

        public virtual DbSet<Demands> Demands { get; set; }

        public DbSet<DistrictModel> Districts { get; set; }

        public DbSet<SchemeMastersVS> SchemeMastersVS { get; set; }

        public DbSet<SchemeMastersConstituency> SchemeMastersConstituency { get; set; }

        public DbSet<mConstituency> mConstituency { get; set; }

        public DbSet<SiteSettings> SiteSettings { get; set; }

        public DbSet<mDepartment> mDepartment { get; set; }

        public DbSet<mPanchayat> mPanchayat { get; set; }

        public DbSet<ConstituencyWorkProgressImages> ConstituencyWorkProgressImages { get; set; }

        public DbSet<ConstituencySchemesBasicDetails> ConstituencySchemesBasicDetails { get; set; }

        public DbSet<ConstituencyWorkProgress> ConstituencyWorkProgress { get; set; }

        public DbSet<mProgramme> mProgramme { get; set; }

        public DbSet<workNature> workNature { get; set; }

        public DbSet<mworkStatus> mworkStatus { get; set; }

        public DbSet<workType> workType { get; set; }

        public DbSet<tConstituencyPanchayat> tConstituencyPanchayat { get; set; }

        public DbSet<mControllingAuthority> mControllingAuthority { get; set; }

        public DbSet<mOffice> mOffice { get; set; }

        public DbSet<tDepartmentOfficeMapped> tDepartmentOfficeMapped { get; set; }

        public DbSet<mUsers> mUsers { get; set; }

        public DbSet<ForwardGrievance> ForwardGrievance { get; set; }

        public DbSet<AdhaarDetails> AdhaarDetails { get; set; }

        public DbSet<tCutMotionDemand> tCutMotionDemand { get; set; }

        public DbSet<mMember> Member { get; set; }

        public DbSet<workTrans> workTrans { get; set; }

        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public DbSet<mSubDivision> mSubDivision { get; set; }
        public DbSet<mSchemeType> mSchemeType { get; set; }

        public virtual DbSet<tOfficeHierarchy> tOfficeHierarchy { get; set; } // Madhur

        #endregion tables

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "GetAllDemand": { return GetAllDemand(); }
                case "getAllDistrict": { return getAllDistrict(); }
                case "getAllList": { return getAllList(); }
                case "getWorkDepartment": { return getWorkDepartment(); }
                case "getAllDepartment": { return getAllDepartment(); }
                case "getAllPanchayat": { return getAllPanchayat(); }
                case "gelFilteredVSSchemeList": { return gelFilteredVSSchemeList(param.Parameter); }
                case "saveConstituencytoMyList": { return saveConstituencytoMyList(param.Parameter); }
                case "deleteConstituencyfromMyList": { return deleteConstituencyfromMyList(param.Parameter); }
                case "gelFilteredMySchemeList": { return gelFilteredMySchemeList(param.Parameter); }
                case "gelFilteredCommisionerSchemeList":
                    { return gelFilteredCommisionerSchemeList(param.Parameter); }
                case "gelFilteredMySchemeListAll": { return gelFilteredMySchemeListAll(param.Parameter); }
                case "getWorkDetailsByID": { return getWorkDetailsByID(param.Parameter); }
                case "updateWorksbyMembers": { return updateWorksbyMembers(param.Parameter); }
                case "CreateNewWorkbyMembers": { return CreateNewWorkbyMembers(param.Parameter); }
                case "UpdateImagesbyOfficer": { return UpdateImagesbyOfficer(param.Parameter); }
                case "getAllProgressImages": { return getAllProgressImages(param.Parameter); }
                case "UpdateProgressbyOfficer": { return UpdateProgressbyOfficer(param.Parameter); }
                case "viewAllProgressByMember": { return viewAllProgressByMember(param.Parameter); }
                case "getAllProgramme": { return getAllProgramme(); }
                case "getAllWorkNature": { return getAllWorkNature(); }
                case "getAllWorkStatus": { return getAllWorkStatus(); }
                case "getAllWorkType": { return getAllWorkType(); }
                case "getAllControllingAuthority": { return getAllControllingAuthority(param.Parameter); }
                case "GetExecutiveOffice": { return GetExecutiveOffice(param.Parameter); }
                case "getConstituencyDistrict": { return getConstituencyDistrict(param.Parameter); }
                //case "getConstituencyDistrict_ByConID": { return getConstituencyDistrict_ByConID(param.Parameter); }
                case "getConstituencyPanchayat": { return getConstituencyPanchayat(param.Parameter); }
                case "memberDepartment": { return memberDepartment(param.Parameter); }
                case "getAllOfficerDepartment": { return getAllOfficerDepartment(param.Parameter); }
                case "getDistrictConstituency": { return getDistrictConstituency(param.Parameter); }
                case "getBasicDetailsOFWorks": { return getBasicDetailsOFWorks(param.Parameter); }
                case "updateWorkTrans": { return updateWorkTrans(param.Parameter); }
                case "getLatestUpdate": { return getLatestUpdate(param.Parameter); }
                case "getworkDetailsByUpdateID": { return getworkDetailsByUpdateID(param.Parameter); }
                case "getAllLatestUpdate": { return getAllLatestUpdate(param.Parameter); }
                case "gelFilteredMySchemeListForOfficer": { return gelFilteredMySchemeListForOfficer(param.Parameter); }
                case "getExecutingDepartmentandOffice": { return getExecutingDepartmentandOffice(param.Parameter); }
                case "UpdateExecutingDetails": { return UpdateExecutingDetails(param.Parameter); }
                case "getWorkDetailsByIDForMember": { return getWorkDetailsByIDForMember(param.Parameter); }
                case "GetConstituencyCodebyMemberCodeAndAssemblyCode": { return GetConstituencyCodebyMemberCodeAndAssemblyCode(param.Parameter); }
                case "get_subdevision": { return get_subdevision(param.Parameter); }
                case "get_SchemeType": { return get_SchemeType(); }
                case "CheckWorkCode_ByCID": { return CheckWorkCode_ByCID(param.Parameter); }
                case "GetConstituencyNameByID": { return GetConstituencyNameByID(param.Parameter); }
                case "get_subdevision_SDM": { return get_subdevision_SDM(param.Parameter); }
                case "getMemberCodeByConstituency": { return getMemberCodeByConstituency(param.Parameter); }
                case "GetConstituencyCode_ByConID": { return GetConstituencyCode_ByConID(param.Parameter); }
                case "GetConstituencyCode":
                    { return GetConstituencyCode(param.Parameter); }
                    
                case "getConstituencyByDistrictID": { return getConstituencyByDistrictID(param.Parameter); }
                case "GetAllOfficemappedByMemberCode": { return GetAllOfficemappedByMemberCode(param.Parameter); }
                case "GetConstituencyByAssemblyId": { return GetConstituencyByAssemblyId(); }
                case "GetAllDistricts": { return GetAllDistricts(); }
                case "GetConstituencyByDistrictId": { return GetConstituencyByDistrictId(param.Parameter); }
                case "getConstituencyForMLA_SDM": { return getConstituencyForMLA_SDM(param.Parameter); }

                case "GetAllMlaListforWorks":
                    {
                        return GetAllMlaListforWorks(param.Parameter);
                    }
                case "GetAllSubOfficeList":
                    {
                        return GetAllSubOfficeList(param.Parameter);
                    }

                case "StatusSectioned":
                    {
                        return StatusSectioned(param.Parameter);
                    }
                case "gelFilteredMySchemeListByType":
                    {
                        return gelFilteredMySchemeListByType(param.Parameter);
                    }
                case "gelFilteredMySchemeListByConstId":
                    {
                        return gelFilteredMySchemeListByConstId(param.Parameter);
                    }
                    

                case "gelFilteredMySchemeListByTypeCount":
                    {
                        return gelFilteredMySchemeListByTypeCount(param.Parameter);
                    }
                case "gelFilteredMySchemeListByType1Count":
                    {
                        return gelFilteredMySchemeListByType1Count(param.Parameter);
                    }

                case "gelFilteredMySchemeListByType2Count":
                    {
                        return gelFilteredMySchemeListByType2Count(param.Parameter);
                    }
                case "gelFilteredMySchemeListByTypeSDMCount":
                    {
                        return gelFilteredMySchemeListByTypeSDMCount(param.Parameter);
                    }

                case "gelFilteredMySchemeListByType1":
                    {
                        return gelFilteredMySchemeListByType1(param.Parameter);
                    }

                case "gelFilteredMySchemeListByType2":
                    {
                        return gelFilteredMySchemeListByType2(param.Parameter);
                    }
                case "gelFilteredMySchemeListBySubDivision":
                    {
                        return gelFilteredMySchemeListBySubDivision(param.Parameter);
                    }

                case "getAllDepartmentBase":
                    {
                        return getAllDepartmentBase(param.Parameter);
                    }
                case "GetDistrictForDC":
                    {
                        return GetDistrictForDC(param.Parameter);
                    }
                case "getSubdivisionForUser":
                    {
                        return getSubdivisionForUser(param.Parameter);
                    }
                case "getSubdivisionForSDM":
                    {
                        return getSubdivisionForSDM(param.Parameter);
                    }

            }
            return null;
        }

        #region generic function

        public static string[] GetConstituencyCodebyMemberCodeAndAssemblyCode(object param)
        {
            string[] str = param as string[];
            string[] strOutput = new string[2];
            int AssemblyCode = Convert.ToInt32(str[0]);
            int MemberCode = Convert.ToInt32(str[1]);
            int ConstituencyCode = 0;
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var ConsCode = (from mMemberAss in context.mMemberAssembly
                                where mMemberAss.AssemblyID == AssemblyCode
                                 && mMemberAss.MemberID == MemberCode
                                select mMemberAss.ConstituencyCode).FirstOrDefault();
                ConstituencyCode = (int)(ConsCode);
                var ConsName = (from mMemberConst in context.mConstituency
                                where mMemberConst.AssemblyID == AssemblyCode
                                 && mMemberConst.ConstituencyCode == ConstituencyCode
                                select mMemberConst.ConstituencyName).FirstOrDefault();

                strOutput[0] = Convert.ToString((int)(ConsCode));
                strOutput[1] = Convert.ToString((string)(ConsName));
            }

            return strOutput;
        }

        public static string[] GetConstituencyCode_ByConID(object param)
        {
            string[] str = param as string[];
            string[] strOutput = new string[2];
            int AssemblyCode = Convert.ToInt32(str[0]);
            int ConID = 0;
            if (str[1] == "" || str[1] == null)
            {
                str[1] = "0";
            }
            else
            {
                ConID = Convert.ToInt32(str[1]);

            }

            //int ConID = Convert.ToInt32(str[1]);

            using (ConstituencyContext context = new ConstituencyContext())
            {
               

                var ConsName = (from con in context.mConstituency
                                where con.AssemblyID == AssemblyCode
                                 && con.ConstituencyID == ConID
                                select con.ConstituencyName).FirstOrDefault();

                strOutput[0] = Convert.ToString((int)(ConID));
                strOutput[1] = Convert.ToString((string)(ConsName));
            }

            return strOutput;
        }

        public static string[] GetConstituencyCode(object param)
        {
            string[] str = param as string[];
            string[] strOutput = new string[2];
            int AssemblyCode = Convert.ToInt32(str[0]);
            int ConID = Convert.ToInt32(str[1]);


            using (ConstituencyContext context = new ConstituencyContext())
            {


                var ConsName = (from con in context.mConstituency
                                where con.ConstituencyCode == ConID
                                select con.ConstituencyName).FirstOrDefault();
               

                strOutput[0] = Convert.ToString((int)(ConID));
                strOutput[1] = Convert.ToString((string)(ConsName));
            }

            return strOutput;
        }


        public static object getConstituencyByDistrictID(object param)
        {
            var district = Convert.ToUInt16(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);

                return (from listc in context.mConstituency
                        where listc.DistrictCode == district && listc.AssemblyID == assemblyID
                        select new fillListGenricInt
                        {
                            value = listc.ConstituencyCode,
                            Text = "Ward No." + listc.ConstituencyCode + ": " + listc.ConstituencyName
                        }).ToList();
            }
        }

        public static object GetConstituencyNameByID(object param)
        {
            WorkDetails obj = param as WorkDetails;
            if (obj.ConId_ForSDM_ == "")
            {
                obj.ConId_ForSDM_ = "0";
            }
            ConstituencyContext context = new ConstituencyContext();
            if (obj.ConId_ForSDM_ != "0")
            {
                List<long> ID = obj.ConId_ForSDM_.Split(',').Select(Int64.Parse).ToList();

                return (from mMemberConst in context.mConstituency
                        where mMemberConst.AssemblyID == obj.AssemblyId
                        && ID.Contains(mMemberConst.ConstituencyCode)
                        select new fillListGenricInt
                        {
                            value = mMemberConst.ConstituencyCode,
                            Text = "Ward No." + mMemberConst.ConstituencyCode + ": " + mMemberConst.ConstituencyName
                        }).ToList();

            }
            else
            {
                return (from mMemberConst in context.mConstituency
                        where mMemberConst.AssemblyID == obj.AssemblyId
                        //  && ID.Contains(mMemberConst.ConstituencyCode)
                        select new fillListGenricInt
                        {
                            value = mMemberConst.ConstituencyCode,
                            Text = "Ward No." + mMemberConst.ConstituencyCode + ": " + mMemberConst.ConstituencyName
                        }).ToList();

            }


        }
        public static object GetAllDemand()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.tCutMotionDemand
                        where list.DemandNo != null
                        orderby list.DemandName ascending
                        select new fillListGenricInt
                        {
                            value = list.DemandNo.HasValue ? list.DemandNo.Value : 0,
                            Text = list.DemandName
                        }
                        ).ToList();
            }
        }

        public static object getAllPanchayat()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var query = (from panchayat in context.mPanchayat
                             orderby panchayat.PanchayatName ascending
                             select new fillListGenricInt
                             {
                                 value = panchayat.PanchayatID,
                                 Text = panchayat.PanchayatName
                             }
                        ).ToList();
                return query;
            }
        }

        public static object getAllvsWorksList()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.Demands

                        select new fillListGenricInt
                        {
                            value = list.DemandNo,
                            Text = list.demandDesc
                        }
                        ).ToList();
            }
        }

        public static object getAllDistrict()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.Districts
                        orderby list.DistrictName ascending
                        select new fillListGenricInt
                        {
                            value = list.DistrictCode.Value,
                            Text = list.DistrictName
                        }
                        ).OrderBy(a => a.Text).ToList();
            }
        }

        public static object getAllDepartment()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.mDepartment
                        orderby new { list.RowNumber, list.deptname }
                        select new fillListGenric
                        {
                            value = list.deptId,
                            Text = list.deptname
                        }
                        ).OrderBy(a => a.Text).ToList();
            }
        }


        public static object getWorkDepartment()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {

                var result = from p in context.ConstituencySchemesBasicDetails
                             group p by p.ExecutingDepartmentId into pg
                             // join *after* group
                             join bp in context.mDepartment on pg.FirstOrDefault().ExecutingDepartmentId equals bp.deptId

                             select new fillListGenric
                             {
                                 value = bp.deptId,
                                 Text = bp.deptname
                             };




                //return (from list in context.mDepartment
                //        orderby new { list.RowNumber, list.deptname }
                //        select new fillListGenric
                //        {
                //            value = list.deptId,
                //            Text = list.deptname
                //        }
                //        ).OrderBy(a => a.Text).ToList();
                return result.ToList();
            }
        }




        public static object getAllDepartmentBase(object param)
        {
            // MlaDiary val = param as MlaDiary;
            string ofcid = Convert.ToString(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.mDepartment
                        where list.deptId == ofcid
                        orderby new { list.RowNumber, list.deptname }
                        select new fillListGenric
                        {
                            value = list.deptId,
                            Text = list.deptname
                        }
                        ).OrderBy(a => a.Text).ToList();
            }
        }



        public static object getAllOfficerDepartment(object param)
        {
            int OfficeID = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.mUsers
                        join aadhar in context.AdhaarDetails
                        on list.AadarId equals aadhar.AdhaarID
                        where list.OfficeId == OfficeID
                        select new fillListGenric
                        {
                            value = aadhar.AdhaarID,
                            Text = aadhar.Name
                        }
                        ).Distinct().ToList();
            }
        }

        public static object memberDepartment(object param)
        {
            string[] arr = param as string[];

            using (ConstituencyContext context = new ConstituencyContext())
            {
                if (arr[0] == "")
                {
                    arr[0] = "0";
                }
                int memberCode = Convert.ToInt32(arr[0]);
                int AssemblyCode = Convert.ToInt32(arr[1]);
                if (memberCode == 0)
                {
                    return (from list in context.mDepartment
                            join mapped in context.tDepartmentOfficeMapped
                            on list.deptId equals mapped.DeptId
                            // where mapped.MemberCode == memberCode && mapped.AssemblyCode == AssemblyCode
                            where mapped.AssemblyCode == AssemblyCode
                            orderby new { list.RowNumber, list.deptname }
                            select new fillListGenric
                            {
                                value = list.deptId,
                                Text = list.deptname
                            }
                           ).Distinct().OrderBy(a => a.Text).ToList();
                }
                else
                {
                    return (from list in context.mDepartment
                            join mapped in context.tDepartmentOfficeMapped
                            on list.deptId equals mapped.DeptId
                            where mapped.MemberCode == memberCode && mapped.AssemblyCode == AssemblyCode
                            orderby new { list.RowNumber, list.deptname }
                            select new fillListGenric
                            {
                                value = list.deptId,
                                Text = list.deptname
                            }
                            ).Distinct().OrderBy(a => a.Text).ToList();
                }
            }
        }

        public static object getAllWorkStatus()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.mworkStatus
                        orderby list.workStatusCode ascending
                        select new fillListGenricLong
                        {
                            value = list.workStatusCode,
                            Text = list.workStatus
                        }
                        ).OrderBy(a => a.Text).ToList();
            }
        }

        public static object getAllList()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.SchemeMastersVS
                        where list.DistrictId != -1
                        select list).ToList();
            }
        }

        public static object getAllMyWorkList(long constituencyID, bool isMember, string aadharID, params object[] param)
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {


                if (constituencyID != 0)
                {
                    return (from sc in context.SchemeMastersConstituency
                            join deatil in context.ConstituencySchemesBasicDetails on sc.schemeConstituencyID equals deatil.mySchemeID
                            join d in context.mDepartment on deatil.ExecutingDepartmentId equals d.deptId  
 join ofc in context.mOffice on deatil.ExecutingOfficeID equals ofc.OfficeId  
 
                            where sc.constituencyID == constituencyID
                            select sc).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
                }
                else
                {
                    if (string.IsNullOrEmpty(aadharID) && isMember)
                    {
                        return (from sc in context.SchemeMastersConstituency
                            join deatil in context.ConstituencySchemesBasicDetails on sc.schemeConstituencyID equals deatil.mySchemeID
                            join d in context.mDepartment on deatil.ExecutingDepartmentId equals d.deptId  
 join ofc in context.mOffice on deatil.ExecutingOfficeID equals ofc.OfficeId  


                                select sc).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
                    }
                    else
                    {

                        //List<long> workList = (from wlist in context.ForwardGrievance
                        //                       where wlist.OfficerCode == aadharID
                        //                       && wlist.Type == "Work"
                        //                       select wlist.GrvCode).Cast<long>().ToList();

                        //return (from list in context.SchemeMastersConstituency
                        //        where workList.Contains(list.schemeConstituencyID)
                        //        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();

                        var muser = (from user in context.mUsers
                                     join office in context.mOffice
                                     on user.OfficeId equals office.OfficeId
                                     where user.AadarId == aadharID
                                     select new
                                     {
                                         deptID = user.DeptId,
                                         officeCode = office.OfficeId
                                     }
                                   ).FirstOrDefault();


                        if (muser == null)
                        {


                           

                           
                           
                           
                           

                            return (from sc in context.SchemeMastersConstituency
                                    join deatil in context.ConstituencySchemesBasicDetails on sc.schemeConstituencyID equals deatil.mySchemeID
                                    join d in context.mDepartment on deatil.ExecutingDepartmentId equals d.deptId
                                    join ofc in context.mOffice on deatil.ExecutingOfficeID equals ofc.OfficeId
                                 

                                    select sc).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();

                        }
                        else
                        {
                            //var workList = (from work in context.ConstituencySchemesBasicDetails

                            //                where work.ExecutingDepartmentId == muser.deptID
                            //                && work.ExecutingOfficeID == muser.officeCode
                            //                select work.mySchemeID).ToList();

                            return (from sc in context.SchemeMastersConstituency
                                    join deatil in context.ConstituencySchemesBasicDetails on sc.schemeConstituencyID equals deatil.mySchemeID
                                    join d in context.mDepartment on deatil.ExecutingDepartmentId equals d.deptId
                                    join ofc in context.mOffice on deatil.ExecutingOfficeID equals ofc.OfficeId
                                   
                                    where deatil.ExecutingDepartmentId == muser.deptID
                                            && deatil.ExecutingOfficeID == muser.officeCode


                                   
                                    select sc).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();

                        }




                    }
                }
            }
        }

        //public static object getAllMyWorkList(long constituencyID, bool isMember, string aadharID)
        //{
        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {


        //        if (constituencyID != 0)
        //        {
        //            return (from list in context.SchemeMastersConstituency
        //                    where list.constituencyID == constituencyID
        //                    select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
        //        }
        //        else
        //        {
        //            if (string.IsNullOrEmpty(aadharID) && isMember)
        //            {
        //                return (from list in context.SchemeMastersConstituency
        //                        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
        //            }
        //            else
        //            {

        //                //List<long> workList = (from wlist in context.ForwardGrievance
        //                //                       where wlist.OfficerCode == aadharID
        //                //                       && wlist.Type == "Work"
        //                //                       select wlist.GrvCode).Cast<long>().ToList();

        //                //return (from list in context.SchemeMastersConstituency
        //                //        where workList.Contains(list.schemeConstituencyID)
        //                //        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();

        //                var muser = (from user in context.mUsers
        //                             join office in context.mOffice
        //                             on user.OfficeId equals office.OfficeId
        //                             where user.AadarId == aadharID
        //                             select new
        //                             {
        //                                 deptID = user.DeptId,
        //                                 officeCode = office.OfficeId
        //                             }
        //                           ).FirstOrDefault();

        //                //var workList = (from work in context.ConstituencySchemesBasicDetails

        //                //                where work.ExecutingDepartmentId == muser.deptID
        //                //                && work.ExecutingOfficeID == muser.officeCode
        //                //                select work.mySchemeID).ToList();

        //                //new changed
        //                var workList = (from work in context.ConstituencySchemesBasicDetails

        //                                where work.ExecutingDepartmentId == muser.deptID
        //                                // && work.ExecutingOfficeID == muser.officeCode
        //                                select work.mySchemeID).ToList();

        //                return (from list in context.SchemeMastersConstituency
        //                        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();

        //                //return (from list in context.SchemeMastersConstituency
        //                //        where workList.Contains(list.schemeConstituencyID)
        //                //        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
        //            }
        //        }
        //    }
        //}

        public static object getAllMyWork_SDM(string SubdivisionCode)
        {
            //string[] SubdivisionIds = SubdivisionCode.Split(',');
            // SubdivisionIds = SubdivisionCode.Split(',');
            var ids = SubdivisionCode.TrimEnd(',');
            if (ids.Contains(","))
            {
                //for(int i=0;i<SubdivisionIds.Length ; i++)
                // {
                ConstituencyContext context = new ConstituencyContext();
                //  Int32 temp = Convert.ToInt32(SubdivisionIds[i]);
                // //var tlist1 = (from list in context.SchemeMastersConstituency select list);
                return (from list in context.SchemeMastersConstituency
                        where ids.Contains(list.SubdevisionId.ToString())
                        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
            }
            else
            {

                ConstituencyContext context = new ConstituencyContext();
                //  Int32 temp = Convert.ToInt32(SubdivisionIds[i]);
                // //var tlist1 = (from list in context.SchemeMastersConstituency select list);
                int subid = Convert.ToInt16(ids);
                return (from list in context.SchemeMastersConstituency
                        where list.SubdevisionId == subid //ids.Contains(list.SubdevisionId.ToString())
                        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
            }

        }

        //for(int i=0;i<ids.Length ; i++)
        //{
        //ConstituencyContext context = new ConstituencyContext();
        ////  Int32 temp = Convert.ToInt32(SubdivisionIds[i]);
        //var tlist1 = (from list in context.SchemeMastersConstituency select list);
        //return (from list in context.SchemeMastersConstituency
        //        where ids.Contains(list.SubdevisionId.ToString())
        //        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();

        // }



        // }
        public static object getAllProgramme()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.mProgramme
                        where list.IsDeleted == false
                        orderby list.programmeName ascending
                        select new fillListGenricInt
                        {
                            value = list.ProgramID,
                            Text = list.programmeName
                        }
                        ).Distinct().OrderBy(a => a.Text).ToList();
            }
        }

        public static object getAllWorkNature()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.workNature
                        orderby list.WorkNatureName ascending
                        select new fillListGenricLong
                        {
                            value = list.WorkNatureCode,
                            Text = list.WorkNatureName
                        }
                        ).ToList();
            }
        }

        public static object getAllWorkType()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.workType
                        orderby list.WorkTypeName ascending
                        select new fillListGenricLong
                        {
                            value = list.WorkTypeCode,
                            Text = list.WorkTypeName
                        }
                        ).Distinct().OrderBy(a => a.Text).ToList();
            }
        }

        public static object getAllControllingAuthority(object param)
        {
            if (param == "")
            {
                param = "0";
            }
            long districtID = Convert.ToInt64(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                if (districtID != 0 && districtID != null)
                {
                    return (from list in context.mControllingAuthority
                            where list.districtCode == districtID || list.districtCode == 0
                            orderby list.ControllingAuthorityName ascending
                            select new fillListGenric
                            {
                                value = list.ControllingAuthorityID,
                                Text = list.ControllingAuthorityName
                            }
                            ).Distinct().ToList();
                }
                else
                {
                    return (from list in context.mControllingAuthority
                            orderby list.ControllingAuthorityName ascending
                            select new fillListGenric
                            {
                                value = list.ControllingAuthorityID,
                                Text = list.ControllingAuthorityName
                            }
                   ).Distinct().OrderBy(a => a.Text).ToList();
                }
            }
        }

        public static object GetExecutiveOffice(object param)
        {
            string[] arr = param as string[];
            string deptID = arr[0];
            if (arr[1] == "")
            {
                arr[1] = "0";
            }
            int memberCode = Convert.ToInt32(arr[1]);
            if (memberCode == 0)
            {
                using (ConstituencyContext context = new ConstituencyContext())
                {
                    return (from list in context.mOffice
                            join map in context.tDepartmentOfficeMapped
                            on list.OfficeId equals map.OfficeId
                            where list.deptid == deptID
                            // && map.MemberCode == memberCode
                            orderby list.officename ascending
                            select new fillListGenricInt
                            {
                                value = list.OfficeId,
                                Text = list.officename
                            }
                            ).Distinct().ToList();
                }
            }
            else
            {
                using (ConstituencyContext context = new ConstituencyContext())
                {
                    return (from list in context.mOffice
                            join map in context.tDepartmentOfficeMapped
                            on list.OfficeId equals map.OfficeId
                            where list.deptid == deptID
                            && map.MemberCode == memberCode
                            orderby list.officename ascending
                            select new fillListGenricInt
                            {
                                value = list.OfficeId,
                                Text = list.officename
                            }
                            ).Distinct().ToList();
                }
            }
        }






        public static string getExecutiveDepartmentName(object param)
        {
            string deptID = param as string;
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.mDepartment
                        where list.deptId == deptID

                        select list.deptabbr).FirstOrDefault();
            }
        }

        public static string getExecutiveOfficeName(object param)
        {
            int OfficeID = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.mOffice
                        where list.OfficeId == OfficeID
                        select list.officename).FirstOrDefault();
            }
        }

        //public static object getConstituencyDistrict(object param)
        //{
        //    if (param == "")
        //    {
        //        param = "0";
        //    }
        //    if (param.ToString().Contains(','))
        //    {
        //        List<long> checkedCid = param.ToString().Split(',').Select(Int64.Parse).ToList();
        //        using (ConstituencyContext context = new ConstituencyContext())
        //        {
        //            var currentAssembly = (from site in context.SiteSettings
        //                                   where site.SettingName == "Assembly"
        //                                   select site.SettingValue).FirstOrDefault();
        //            int assemblyID = Convert.ToInt32(currentAssembly);
        //            var con = (from list in context.mConstituency
        //                       //  where list.ConstituencyCode == ConstituteID && list.AssemblyID == assemblyID
        //                       where (checkedCid.Contains(list.ConstituencyCode) && list.AssemblyID == assemblyID)
        //                       select list.DistrictCode).FirstOrDefault();
        //            return (from list in context.Districts
        //                    where list.DistrictCode == con
        //                    select new fillListGenricInt
        //                    {
        //                        value = list.DistrictCode.Value,
        //                        Text = list.DistrictName
        //                    }
        //                     ).ToList();
        //        }
        //    }
        //    else
        //    {
        //        int ConstituteID = Convert.ToInt32(param.ToString());
        //        if (ConstituteID != 0)
        //        {
        //            using (ConstituencyContext context = new ConstituencyContext())
        //            {
        //                var currentAssembly = (from site in context.SiteSettings
        //                                       where site.SettingName == "Assembly"
        //                                       select site.SettingValue).FirstOrDefault();
        //                int assemblyID = Convert.ToInt32(currentAssembly);
        //                var con = (from list in context.mConstituency
        //                           where list.ConstituencyCode == ConstituteID && list.AssemblyID == assemblyID
        //                           select list.DistrictCode).FirstOrDefault();
        //                return (from list in context.Districts
        //                        where list.DistrictCode == con
        //                        select new fillListGenricInt
        //                        {
        //                            value = list.DistrictCode.Value,
        //                            Text = list.DistrictName
        //                        }
        //                         ).ToList();
        //            }
        //        }
        //        else
        //        {
        //            using (ConstituencyContext context = new ConstituencyContext())
        //            {
        //                var currentAssembly = (from site in context.SiteSettings
        //                                       where site.SettingName == "Assembly"
        //                                       select site.SettingValue).FirstOrDefault();
        //                int assemblyID = Convert.ToInt32(currentAssembly);
        //                var con = (from list in context.mConstituency
        //                           where list.AssemblyID == assemblyID
        //                           select list.DistrictCode).ToList();
        //                return (from list in context.Districts
        //                        where con.Contains(list.DistrictCode)
        //                        select new fillListGenricInt
        //                        {
        //                            value = list.DistrictCode.Value,
        //                            Text = list.DistrictName
        //                        }
        //                         ).ToList();
        //            }

        //        }
        //    }
        //}

        public static object getConstituencyDistrict(object param)
        {
            if (param == "")
            {
                param = "0";
            }
            if (param.ToString().Contains(','))
            {
                List<long> checkedCid = param.ToString().Split(',').Select(Int64.Parse).ToList();
                using (ConstituencyContext context = new ConstituencyContext())
                {
                    var currentAssembly = (from site in context.SiteSettings
                                           where site.SettingName == "Assembly"
                                           select site.SettingValue).FirstOrDefault();
                    int assemblyID = Convert.ToInt32(currentAssembly);

                    List<int> ConstituteCode = (from listC in context.mConstituency
                                                where checkedCid.Contains(listC.ConstituencyID)
                                                select listC.ConstituencyCode).ToList();

                    var con = (from list in context.mConstituency
                                   //  where list.ConstituencyCode == ConstituteID && list.AssemblyID == assemblyID
                               where (ConstituteCode.Contains(list.ConstituencyCode) && list.AssemblyID == assemblyID)
                               select list.DistrictCode).FirstOrDefault();
                    return (from list in context.Districts
                            where list.DistrictCode == con
                            select new fillListGenricInt
                            {
                                value = list.DistrictCode.Value,
                                Text = list.DistrictName
                            }
                             ).ToList();
                }
            }
            else
            {
                int ConstituteID = Convert.ToInt32(param.ToString());
                if (ConstituteID != 0)
                {
                    using (ConstituencyContext context = new ConstituencyContext())
                    {
                        var currentAssembly = (from site in context.SiteSettings
                                               where site.SettingName == "Assembly"
                                               select site.SettingValue).FirstOrDefault();
                        int assemblyID = Convert.ToInt32(currentAssembly);

                        //int constcode = Convert.ToInt32(param.ToString());
                        //var constituteCode = (from listc in context.mConstituency
                        //                      where listc.ConstituencyID == ConstituteID
                        //                      select listc.ConstituencyCode).FirstOrDefault();
                        //Done by Ajay
                        var constituteCode = (from listc in context.mConstituency
                                              where listc.ConstituencyCode == ConstituteID
                                              select listc.ConstituencyCode).FirstOrDefault();

                        var con = (from list in context.mConstituency
                                   where list.ConstituencyCode == constituteCode && list.AssemblyID == assemblyID // comparing with cons code now 
                                   select list.DistrictCode).FirstOrDefault();
                        return (from list in context.Districts
                                where list.DistrictCode == con
                                select new fillListGenricInt
                                {
                                    value = list.DistrictCode.Value,
                                    Text = list.DistrictName
                                }
                                 ).ToList();
                    }
                }
                else
                {
                    using (ConstituencyContext context = new ConstituencyContext())
                    {
                        var currentAssembly = (from site in context.SiteSettings
                                               where site.SettingName == "Assembly"
                                               select site.SettingValue).FirstOrDefault();
                        int assemblyID = Convert.ToInt32(currentAssembly);
                        var con = (from list in context.mConstituency
                                   where list.AssemblyID == assemblyID
                                   select list.DistrictCode).ToList();
                        return (from list in context.Districts
                                where con.Contains(list.DistrictCode)
                                select new fillListGenricInt
                                {
                                    value = list.DistrictCode.Value,
                                    Text = list.DistrictName
                                }
                                 ).ToList();
                    }

                }
            }
        }
        public static object getDistrictConstituency(object param)
        {
            int DistrictCode = Convert.ToInt32(param.ToString());
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);
                if (DistrictCode != 0 && DistrictCode != null)
                {
                    return (from list in context.mConstituency
                            where list.DistrictCode == DistrictCode && list.AssemblyID == assemblyID
                            select new fillListGenricInt
                            {
                                Text = list.ConstituencyName,
                                value = list.ConstituencyCode
                            }
                               ).OrderBy(a => a.Text).ToList();
                }
                else
                {
                    return (from list in context.mConstituency
                            where list.AssemblyID == assemblyID
                            select new fillListGenricInt
                            {
                                Text = list.ConstituencyName,
                                value = list.ConstituencyCode
                            }
                              ).OrderBy(a => a.Text).ToList();
                }
            }
        }

        //        public static object getConstituencyPanchayat(object param)
        //        {
        //            if (param == "")
        //            {
        //                param = "0";
        //            }
        //            if (param.ToString().Contains(','))
        //            {
        //                List<long> checkedCid = param.ToString().Split(',').Select(Int64.Parse).ToList();
        //                using (ConstituencyContext context = new ConstituencyContext())
        //                {
        //                    return (from list in context.tConstituencyPanchayat
        //                            join panchayat in context.mPanchayat
        //                            on list.PanchayatCode equals panchayat.PanchayatCode
        //                             where checkedCid.Contains(list.ConstituencyCode)

        ////where list.ConstituencyCode == ConstituteID
        //                            orderby panchayat.PanchayatName ascending
        //                            select new fillListGenricInt
        //                            {
        //                                value = panchayat.PanchayatCode,
        //                                Text = panchayat.PanchayatName
        //                            }
        //                                ).OrderBy(a => a.Text).ToList();
        //                }
        //            }
        //            else
        //            {
        //                //int ConstituteID = Convert.ToInt32(param.ToString());
        //                using (ConstituencyContext context = new ConstituencyContext())
        //                {
        //                    //changed by robin ...showing panchayat according to consitituency ConstituteID
        //                    int ConstCode = Convert.ToInt32(param.ToString());
        //                    var ConstituteID = (from listC in context.mConstituency
        //                                        where listC.ConstituencyID == ConstCode
        //                                   select listC.ConstituencyCode).FirstOrDefault();
        //                    if (ConstituteID != 0 && ConstituteID != null)
        //                    {
        //                        return (from list in context.tConstituencyPanchayat
        //                                join panchayat in context.mPanchayat
        //                                on list.PanchayatCode equals panchayat.PanchayatCode

        //                                where list.ConstituencyCode == ConstituteID
        //                                orderby panchayat.PanchayatName ascending
        //                                select new fillListGenricInt
        //                                {
        //                                    value = panchayat.PanchayatCode,
        //                                    Text = panchayat.PanchayatName
        //                                }
        //                                 ).OrderBy(a => a.Text).ToList();
        //                    }
        //                    else
        //                    {
        //                        return (from list in context.tConstituencyPanchayat
        //                                join panchayat in context.mPanchayat
        //                                on list.PanchayatCode equals panchayat.PanchayatCode
        //                                orderby panchayat.PanchayatName ascending
        //                                select new fillListGenricInt
        //                                {
        //                                    value = panchayat.PanchayatCode,
        //                                    Text = panchayat.PanchayatName
        //                                }
        //                         ).OrderBy(a => a.Text).ToList();
        //                    }
        //                }
        //            }
        //        }


        public static object getConstituencyForMLA_SDM(object param)
        {
            if (param == "")
            {
                param = "0";
            }
            if (param.ToString().Contains(','))
            {
                List<long> checkedCid = param.ToString().Split(',').Select(Int64.Parse).ToList();

                using (ConstituencyContext context = new ConstituencyContext())
                {
                    //List<int> ConstituteCode = (from listC in context.mConstituency
                    //                            where checkedCid.Contains(listC.ConstituencyID)
                    //                            select new fillListGenricInt
                    //                            {
                    //                                value = listC.ConstituencyCode,
                    //                                Text = listC.ConstituencyName
                    //                            }).ToList();

                    return (from list in context.mConstituency

                            where 
                            //list.AssemblyID == 13 && 
                            checkedCid.Contains(list.ConstituencyCode)

                            //where list.ConstituencyCode == ConstituteID
                            orderby list.ConstituencyName ascending
                            select new fillListGenricInt
                            {
                                value = list.ConstituencyCode,
                                Text = "Ward No." + list.ConstituencyCode + ":" + list.ConstituencyName




                            }
                                ).OrderBy(a => a.Text).ToList();
                }
            }
            else
            {
                using (ConstituencyContext context = new ConstituencyContext())
                {
                    // changed by robin ...showing panchayat according to consitituency constituteid
                    int constcode = Convert.ToInt32(param.ToString());


                    //var ConstituteID = Convert.ToInt32(param.ToString());

                    if (constcode != 0 && constcode != null)
                    {
                        return (from list in context.mConstituency
                                where
                                //list.AssemblyID == 13 &&
                                list.ConstituencyCode == constcode // comparing with code
                                orderby list.ConstituencyName ascending
                                select new fillListGenricInt
                                {
                                    value = list.ConstituencyCode,
                                    Text = "Ward No." + list.ConstituencyCode + ":" + list.ConstituencyName
                                }
                                 ).OrderBy(a => a.Text).ToList();
                    }
                    else
                    {
                        return (from list in context.mConstituency
                                //where list.AssemblyID == 13
                                orderby list.ConstituencyName ascending
                                select new fillListGenricInt
                                {
                                    value = list.ConstituencyCode,
                                    Text = "Ward No." + list.ConstituencyCode + ":" + list.ConstituencyName
                                }
                                ).OrderBy(a => a.Text).ToList();
                    }
                }
            }
        }

        public static object getSubdivisionForUser(object param)
        {
            if (param == "")
            {
                param = "0";
            }

            using (ConstituencyContext context = new ConstituencyContext())
            {
                int DistrictCode = Convert.ToInt32(param.ToString());
                if (DistrictCode != 0 && DistrictCode != null)
                {
                    return (from list in context.mSubDivision
                            where list.DistrictCode == DistrictCode // comparing with code
                            orderby list.SubDivisionName ascending
                            select new fillListGenricInt
                            {
                                value = list.mSubDivisionId,
                                Text = list.SubDivisionName
                            }
                             ).OrderBy(a => a.Text).ToList();
                }
                else
                {
                    return (from list in context.mSubDivision
                                //where list.DistrictCode == constcode // comparing with code
                            orderby list.SubDivisionName ascending
                            select new fillListGenricInt
                            {
                                value = list.mSubDivisionId,
                                Text = list.SubDivisionName
                            }
                             ).OrderBy(a => a.Text).ToList();
                }
            }
        }

        public static object getSubdivisionForSDM(object param)
        {
            if (param == "")
            {
                param = "0";
            }

            using (ConstituencyContext context = new ConstituencyContext())
            {
                int constcode = Convert.ToInt32(param.ToString());
                if (constcode != 0 && constcode != null)
                {
                    return (from list in context.mSubDivision
                            where list.mSubDivisionId == constcode // comparing with code
                            orderby list.SubDivisionName ascending
                            select new fillListGenricInt
                            {
                                value = list.mSubDivisionId,
                                Text = list.SubDivisionName
                            }
                             ).OrderBy(a => a.Text).ToList();
                }
                else
                {
                    return (from list in context.mSubDivision
                                //where list.DistrictCode == constcode // comparing with code
                            orderby list.SubDivisionName ascending
                            select new fillListGenricInt
                            {
                                value = list.mSubDivisionId,
                                Text = list.SubDivisionName
                            }
                             ).OrderBy(a => a.Text).ToList();
                }
            }
        }



        public static object getConstituencyPanchayat(object param)
        {
            if (param == "")
            {
                param = "0";
            }
            if (param.ToString().Contains(','))
            {
                List<long> checkedCid = param.ToString().Split(',').Select(Int64.Parse).ToList();

                using (ConstituencyContext context = new ConstituencyContext())
                {
                    List<int> ConstituteCode = (from listC in context.mConstituency
                                                where checkedCid.Contains(listC.ConstituencyID)
                                                select listC.ConstituencyCode).ToList();

                    return (from list in context.tConstituencyPanchayat
                            join panchayat in context.mPanchayat
                            on list.PanchayatCode equals panchayat.PanchayatCode
                            where ConstituteCode.Contains(list.ConstituencyCode)

                            //where list.ConstituencyCode == ConstituteID
                            orderby panchayat.PanchayatName ascending
                            select new fillListGenricInt
                            {
                                value = panchayat.PanchayatCode,
                                Text = panchayat.PanchayatName
                            }
                                ).OrderBy(a => a.Text).ToList();
                }
            }
            else
            {
                using (ConstituencyContext context = new ConstituencyContext())
                {
                    // changed by robin ...showing panchayat according to consitituency constituteid
                    int constcode = Convert.ToInt32(param.ToString());


                    var constituteid = (from listc in context.mConstituency
                                        where listc.ConstituencyID == constcode
                                        select listc.ConstituencyCode).FirstOrDefault();//returning code



                    //var ConstituteID = Convert.ToInt32(param.ToString());

                    if (constituteid != 0 && constituteid != null)
                    {
                        return (from list in context.tConstituencyPanchayat
                                join panchayat in context.mPanchayat
                                on list.PanchayatCode equals panchayat.PanchayatCode

                                where list.ConstituencyCode == constituteid // comparing with code
                                orderby panchayat.PanchayatName ascending
                                select new fillListGenricInt
                                {
                                    value = panchayat.PanchayatCode,
                                    Text = panchayat.PanchayatName
                                }
                                 ).OrderBy(a => a.Text).ToList();
                    }
                    else
                    {
                        return (from list in context.tConstituencyPanchayat
                                join panchayat in context.mPanchayat
                                on list.PanchayatCode equals panchayat.PanchayatCode
                                orderby panchayat.PanchayatName ascending
                                select new fillListGenricInt
                                {
                                    value = panchayat.PanchayatCode,
                                    Text = panchayat.PanchayatName
                                }
                         ).OrderBy(a => a.Text).ToList();
                    }
                }
            }
        }
        public static object getExecutingDepartmentandOffice(object param)
        {
            long mySchemeID = Convert.ToInt64(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var works = (from w in context.ConstituencySchemesBasicDetails
                             where w.mySchemeID == mySchemeID
                             select w).FirstOrDefault();

                context.Close();
                if (works != null)
                {
                    return new string[] { works.ExecutingDepartmentId, works.ExecutingOfficeID.ToString() };
                }
                else
                {
                }
                return new string[] { "0", "0" };
            }
        }

        public static object getBasicDetailsOFWorks(object param)
        {
            long schemeID = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var works = (from list in context.SchemeMastersConstituency

                             join wstaus in context.mworkStatus
                             on list.workStatus.Value equals wstaus.workStatusCode

                                into c
                             from cp in c.DefaultIfEmpty()

                             where list.schemeConstituencyID == schemeID
                             select new
                             {
                                 work = list,

                                 wstaus = cp
                             }
                             ).FirstOrDefault();
                if (works != null)
                {
                    workBasicView view = new workBasicView()
                    {
                        financialYear = works.work != null ? works.work.financialYear : "",
                        workCode = works.work.WorkCode,
                        workName = string.IsNullOrEmpty(works.work.workName) ? works.work.WorkNameLocal : works.work.workName,
                        workStatus = works.wstaus != null ? works.wstaus.workStatus : "",
                    };
                    return view;
                }
                return new workBasicView();
            }
        }

        public static void UpdateTrans(workTrans workTrans)
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                context.workTrans.Add(workTrans);
                context.SaveChanges();
                context.Close();
            }
        }

        public static void inserWorkTrans(workTrans workTrans)
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                context.workTrans.Add(workTrans);
                context.SaveChanges();
                context.Close();
            }
        }

        public static object updateWorkTrans(object param)
        {
            long autoID = Convert.ToInt64(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var works = (from w in context.workTrans
                             where w.autoID == autoID
                             select w).FirstOrDefault();
                context.Entry(works).State = EntityState.Modified;
                context.SaveChanges();
                context.Close();
            }
            return null;
        }

        public static object getLatestUpdate(object param)
        {
            string[] arr = param as string[];
            int page = string.IsNullOrEmpty(arr[1]) ? 0 : Convert.ToInt32(arr[1]) - 1;
            long memberCode = Convert.ToInt32(arr[0]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var result = (from list in context.workTrans

                              join musers in context.mUsers
                               on list.userID equals musers.UserId

                              join aadhar in context.AdhaarDetails
                              on musers.AadarId equals aadhar.AdhaarID

                              join scheme in context.SchemeMastersConstituency
                               on list.schemeID equals scheme.schemeConstituencyID

                              where scheme.constituencyID == memberCode
                              && list.isMember == false

                              orderby list.SubmitTime descending

                              select new
                              {
                                  imagePath = aadhar.Photo,
                                  memberCode = list.memberCode,
                                  OfficerName = aadhar.Name,
                                  Remarks = list.Remarks,
                                  schemeID = list.schemeID,
                                  workName = scheme.workName.Length > 0 ? scheme.workName : scheme.WorkNameLocal,
                                  submitDate = list.SubmitTime,
                                  autoid = list.autoID,
                                  read = list.read
                              }).ToList();
                List<workTrans> List = new List<workTrans>();
                foreach (var item in result)
                {
                    List.Add(new workTrans()
                    {
                        workName = item.workName,
                        imagePath = item.imagePath,
                        memberCode = item.memberCode,
                        OfficerName = item.OfficerName,
                        Remarks = item.Remarks,
                        SubmitTimeString = item.submitDate.ToString("dd-MM-yyyy hh:mm tt"),
                        autoID = item.autoid,
                        read = item.read,
                        schemeID = item.schemeID
                    });
                }
                int skipRecords = page * 5;
                return List.OrderByDescending(a => a.SubmitTime).Skip(skipRecords).Take(5).ToList();
            }
        }

        public static object getAllLatestUpdate(object param)
        {
            long memberCode = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var result = (from list in context.workTrans

                              join musers in context.mUsers
                               on list.userID equals musers.UserId

                              join aadhar in context.AdhaarDetails
                              on musers.AadarId equals aadhar.AdhaarID

                              join scheme in context.SchemeMastersConstituency
                               on list.schemeID equals scheme.schemeConstituencyID

                              where scheme.memberCode == memberCode
                              && list.isMember == false

                              orderby list.SubmitTime descending

                              select new
                              {
                                  imagePath = aadhar.Photo,
                                  memberCode = list.memberCode,
                                  OfficerName = aadhar.Name,
                                  Remarks = list.Remarks,
                                  schemeID = list.schemeID,
                                  workName = scheme.workName.Length > 0 ? scheme.workName : scheme.WorkNameLocal,
                                  submitDate = list.SubmitTime,
                                  autoid = list.autoID,
                                  read = list.read
                              }).ToList();
                List<workTrans> List = new List<workTrans>();
                foreach (var item in result)
                {
                    List.Add(new workTrans()
                    {
                        workName = item.workName,
                        imagePath = item.imagePath,
                        memberCode = item.memberCode,
                        OfficerName = item.OfficerName,
                        Remarks = item.Remarks,
                        SubmitTimeString = item.submitDate.ToString("dd-MM-yyyy hh:mm tt"),
                        autoID = item.autoid,
                        read = item.read,
                        schemeID = item.schemeID
                    });
                }
                return List;
            }
        }

        //getAllLatestUpdate

        public static object getworkDetailsByUpdateID(object param)
        {
            long autoID = Convert.ToInt64(param);

            using (ConstituencyContext context = new ConstituencyContext())
            {
                try
                {
                    var works = (from w in context.workTrans
                                 where w.autoID == autoID
                                 select w).FirstOrDefault();
                    works.read = true;
                    context.Entry(works).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();

                    WorkDetails details = (WorkDetails)getWorkDetailsByID(works.schemeID);
                    ErrorLog.WriteToLog(null, "Scheme ID : " + works.schemeID);
                    return details;
                }
                catch (Exception ex)
                {
                    ErrorLog.WriteToLog(ex, "Mathod Name getworkDetailsByUpdateID  ");
                    throw;
                }
            }
        }

        #endregion generic function

        #region searchFor mapping

        public static object gelFilteredVSSchemeList(object param)
        {
            string[] arr = param as string[];
            string District = arr[0], ControllingAuthority = arr[2], FinancialYear = arr[3];
            long constituencyID = Convert.ToInt32(arr[4]);
            long DistrictID = Convert.ToInt32(arr[5]);
            long Demand = Convert.ToInt64(arr[1]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);
                List<vsSchemeList> list = new List<vsSchemeList>();
                List<myShemeList> mylist = new List<myShemeList>();
                schemeMapping scheme = new schemeMapping()
                {
                    ControllingAuthorityID = ControllingAuthority,
                    FinancialYear = FinancialYear,
                    eConstituencyID = constituencyID,
                    vsConstituencyID = constituencyID,
                    vsSchemeList = list,
                    mySchemeList = mylist,
                    constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == constituencyID select cons.ConstituencyName).FirstOrDefault()
                };

                List<SchemeMastersConstituency> myresult = (List<SchemeMastersConstituency>)getAllMyWorkList(constituencyID, true, null);
                myresult = myresult.Where(a => a.ControllingAuthorityId == ControllingAuthority).ToList();

                if (Demand != 0)
                {
                    myresult = myresult.Where(a => a.DemandCode == Demand).ToList();
                }
                if (FinancialYear != null && !string.IsNullOrEmpty(FinancialYear))
                {
                    myresult = myresult.Where(a => a.financialYear == FinancialYear).ToList();
                }
                foreach (SchemeMastersConstituency item in myresult)
                {
                    mylist.Add(new myShemeList()
                    {
                        workCode = item.WorkCode,
                        workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                        mySchemeID = item.schemeConstituencyID,
                        sanctionedAmount = item.SanctionedAmount,
                        EstimatedAmount = item.AmountEstimate
                    });
                }
                List<SchemeMastersVS> result = (List<SchemeMastersVS>)getAllList();
                result = result.Where(a => a.DistrictId == DistrictID).ToList();
                if (Demand != 0)
                {
                    result = result.Where(a => a.DemandCode == Demand).ToList();
                }
                if (FinancialYear != null && !string.IsNullOrEmpty(FinancialYear))
                {
                    result = result.Where(a => a.financialYear == FinancialYear).ToList();
                }
                foreach (SchemeMastersVS item in result)
                {
                    list.Add(new vsSchemeList()
                    {
                        workCode = item.WorkCode,
                        workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                        vsSchemeID = item.schemeVSID,
                        sanctionedAmount = item.SanctionedAmount,
                        EstimatedAmount = item.AmountEstimate,
                        picked = mylist.FindIndex(a => a.workCode == item.WorkCode) >= 0 ? true : false
                    });
                }

                return scheme;
            }
        }

        #endregion searchFor mapping

        #region mapping works

        public static object saveConstituencytoMyList(object param)
        {
            string[] arr = param as string[];
            List<long> checkedID = arr[0].Split(',').Select(Int64.Parse).ToList();
            long constituencyID = Convert.ToInt64(arr[1]);
            string userid = arr[2];
            int memberCode = Convert.ToInt32(arr[3]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var schemeList = (from list in context.SchemeMastersVS
                                  where checkedID.Contains(list.schemeVSID)
                                  select list).ToList();
                foreach (var item in schemeList)
                {
                    SchemeMastersConstituency con = new SchemeMastersConstituency()
                    {
                        AmountEstimate = item.AmountEstimate,
                        ControllingAuthorityId = item.ControllingAuthorityId,
                        createdBY = userid,
                        createdDate = DateTime.Now,
                        DemandCode = 0,
                        DepartmentIdOwner = item.DepartmentIdOwner,
                        DistrictId = item.DistrictId,
                        financialYear = item.financialYear,
                        financialYearID = item.financialYearID,
                        modifiedBY = null,
                        ModifiedDate = DateTime.Now,
                        ProgrammeName = item.ProgrammeName,
                        SanctionedAmount = item.SanctionedAmount,
                        SanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate : new DateTime(Convert.ToInt32(item.financialYear.Split('-')[0]), 4, 1),
                        constituencyID = constituencyID,
                        TotalAmountReleased = 0,
                        WorkCode = item.WorkCode,
                        workName = item.workName,
                        WorkNameLocal = item.WorkNameLocal,
                        workStatus = 1,
                        memberCode = memberCode
                    };
                    context.SchemeMastersConstituency.Add(con);
                    Console.Write(context.GetValidationErrors().ToList());
                    context.SaveChanges();
                }
                context.SaveChanges();
                return null;
            }
        }

        public static object deleteConstituencyfromMyList(object param)
        {
            List<long> checkedID = param.ToString().Split(',').Select(Int64.Parse).ToList();
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var schemeList = (from list in context.SchemeMastersConstituency
                                  where checkedID.Contains(list.schemeConstituencyID)
                                  select list).ToList();
                foreach (var item in schemeList)
                {
                    context.Entry(item).State = EntityState.Deleted;
                    context.SaveChanges();
                }
                context.Close();
                return null;
            }
        }

        #endregion mapping works

        #region my constituency Works

        //public static object gelFilteredMySchemeList(object param)
        //{
        //    string[] arr = param as string[];
        //    string program = arr[1], ControllingAuthority = arr[3], FinancialYear = arr[4], agency = arr[5], mOwnerDepartment = arr[6], Panchayat = arr[7], WorkType = arr[8];
        //    long constituencyID = Convert.ToInt32(arr[0]);
        //    long districtCode = Convert.ToInt32(arr[10]);
        //    long demandCode = Convert.ToInt64(arr[2]);
        //    long workstatus = Convert.ToInt64(arr[9]);
        //    string aadharID = arr[11];
        //    bool member = Convert.ToBoolean(arr[12]);
        //    string cFinancialYear = arr[13];
        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {
        //        var currentAssembly = (from site in context.SiteSettings
        //                               where site.SettingName == "Assembly"
        //                               select site.SettingValue).FirstOrDefault();
        //        int assemblyID = Convert.ToInt32(currentAssembly);
        //        List<vsSchemeList> list = new List<vsSchemeList>();
        //        List<myShemeList> mylist = new List<myShemeList>();
        //        schemeMapping scheme = new schemeMapping()
        //        {
        //            ControllingAuthorityID = ControllingAuthority,
        //            FinancialYear = FinancialYear,
        //            eConstituencyID = constituencyID,
        //            vsConstituencyID = constituencyID,
        //            vsSchemeList = list,
        //            mySchemeList = mylist,
        //            constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == constituencyID select cons.ConstituencyName).FirstOrDefault()
        //        };

        //        List<SchemeMastersConstituency> myresult = (List<SchemeMastersConstituency>)getAllMyWorkList(constituencyID, member, aadharID);

        //        myresult = myresult.Where(a => a.workStatus == workstatus && a.ControllingAuthorityId == ControllingAuthority).ToList();
        //        if (demandCode != 0)
        //        {
        //            myresult = myresult.Where(a => a.DemandCode == demandCode).ToList();
        //        }
        //        if (districtCode != 0)
        //        {
        //            myresult = myresult.Where(a => a.DistrictId == districtCode).ToList();
        //        }

        //        foreach (SchemeMastersConstituency item in myresult)
        //        {
        //            var workDetails = (from cons in context.ConstituencySchemesBasicDetails
        //                               where cons.mySchemeID == item.schemeConstituencyID
        //                               select cons).FirstOrDefault();
        //            var physicalDetails = (from pcons in context.ConstituencyWorkProgress
        //                                   where pcons.schemeCode == item.schemeConstituencyID
        //                                   orderby pcons.submitDate descending
        //                                   select pcons).FirstOrDefault();
        //            var imageCount = (from pcons in context.ConstituencyWorkProgressImages
        //                              where pcons.schemeCode == item.schemeConstituencyID
        //                              select pcons).Count();

        //            mylist.Add(new myShemeList()
        //            {
        //                workCode = item.WorkCode,
        //                workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
        //                mySchemeID = item.schemeConstituencyID,
        //                sanctionedAmount = item.SanctionedAmount,
        //                EstimatedAmount = item.AmountEstimate,
        //                PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
        //                programName = item.ProgrammeName,
        //                sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
        //                districtID = item.DistrictId,
        //                ExecutiveDepartment = workDetails != null ? getExecutiveDepartmentName(workDetails.ExecutingDepartmentId) : "",
        //                ExecutiveOffice = workDetails != null ? getExecutiveOfficeName(workDetails.ExecutingOfficeID) : "",
        //                physicalProgress = physicalDetails != null ? physicalDetails.toalProgress : 0,
        //                financialProgress = physicalDetails != null ? physicalDetails.toalExpenditure : 0,
        //                financialYear = item.financialYear,

        //                CompletionDate = item.EstimatedCompletionDate,
        //                imageCount = imageCount,
        //                CompletionFinancialYear = workDetails != null ? item.financialYear : "",
        //                workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
        //                workStatusCode = item.workStatus
        //            });
        //        }
        //        if (agency != "0")
        //        {
        //            scheme.mySchemeList = scheme.mySchemeList.Where(a => a.ExecutiveDepartment == agency).ToList();
        //        }
        //        if (mOwnerDepartment != "0")
        //        {
        //            scheme.mySchemeList = scheme.mySchemeList.Where(a => a.oWnerDepartment == mOwnerDepartment).ToList();
        //        }
        //        if (FinancialYear != "0")
        //        {
        //            scheme.mySchemeList = scheme.mySchemeList.Where(a => a.financialYear == FinancialYear).ToList();
        //        }
        //        if (workstatus == 3)
        //        {
        //            scheme.mySchemeList = scheme.mySchemeList.Where(a => a.CompletionFinancialYear == cFinancialYear).ToList();
        //        }
        //        //if (!member)
        //        //{
        //        //    List<long> workList = (from wlist in context.ForwardGrievance
        //        //                           where wlist.OfficerCode == aadharID
        //        //                           && wlist.Type == "Work"
        //        //                           select wlist.GrvCode).Cast<long>().ToList();
        //        //    scheme.mySchemeList = scheme.mySchemeList.Where(a => workList.Contains(a.mySchemeID)).Select(a => a).ToList();
        //        //}
        //        return scheme;
        //    }
        //}


        public static object gelFilteredMySchemeListByType(object param)
        {

            string[] arr = param as string[];
            string agency = arr[0];
            long workstatus = Convert.ToInt64(arr[1]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                if (agency != "0")
                    predicate = predicate.And(q => q.ExecutiveDepartment == agency);
                if (workstatus != 0)
                    predicate = predicate.And(q => q.workStatusCode == workstatus);
              
                    predicate = predicate.And(q => q.workStatusCode == workstatus);

                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID


                               select new myShemeList
                               {
                                   WardName = (from cons in context.mConstituency
                                               where cons.ConstituencyCode == item.constituencyID
                                               select
"Ward " + cons.ConstituencyCode + "-  " + cons.ConstituencyName).FirstOrDefault(),
                                   workCode = item.WorkCode != null ? item.WorkCode : "",
                                   workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                                   mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,

                                   sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,

                                   EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                                   Panchayat = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatCode).FirstOrDefault(),
                                   PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                                   programName = item.ProgrammeName != null ? item.ProgrammeName : "",
                                   //sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                                   districtID = item.DistrictId != null ? item.DistrictId : 0,

                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveDepartmentName = (from d in context.mDepartment where d.deptId == M.ExecutingDepartmentId select d.deptname).FirstOrDefault(),

                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   oWnerDepartment = M.Owner_deptid != null ? M.Owner_deptid : "",
                                   //ExecutiveOfficeName = M.ExecutingOfficeID != null ? getExecutiveOfficeName(M.ExecutingOfficeID) : "",
                                   ExecutiveOfficeName = (from o in context.mOffice where o.OfficeId == M.ExecutingOfficeID select o.officename).FirstOrDefault(),

                                   //physicalProgress = (from o in context.ConstituencyWorkProgress where o.schemeCode == item.schemeConstituencyID select o.toalProgress).FirstOrDefault(),
                                   physicalProgress = (from o in context.ConstituencyWorkProgress
                                                       where o.schemeCode == item.schemeConstituencyID
                                                       orderby o.submitDate descending
                                                       select o.toalProgress).FirstOrDefault(),

                                   financialProgress = (from o in context.ConstituencyWorkProgress
                                                        where o.schemeCode == item.schemeConstituencyID
                                                        orderby o.submitDate descending
                                                        select o.toalExpenditure).FirstOrDefault(),

                                   financialYear = item.financialYear != null ? item.financialYear : "",

                                   CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",

                                   imageCount = (from pcons in context.ConstituencyWorkProgressImages where pcons.schemeCode == item.schemeConstituencyID select pcons.autoID).Count(),
                                   CompletionFinancialYear = item.financialYear,
                                   workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                                   subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,

                                   WorkType = item.workType != null ? item.workType : "0",

                                   ProgrammeName = (from pcons in context.mProgramme
                                                    where pcons.programmeCode.ToString() == item.ProgrammeName

                                                    select pcons.programmeName).FirstOrDefault()







                               }).AsExpandable().Where(predicate).OrderByDescending(m => m.mySchemeID).ToList();



                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }


        public static object gelFilteredMySchemeListByConstId(object param)
        {

            string[] arr = param as string[];
            long agency = Convert.ToInt64(arr[0]);
            long workstatus = Convert.ToInt64(arr[1]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                if (agency != 0)
                    predicate = predicate.And(q => q.constituencyID == agency);
                if (workstatus != 0)
                    predicate = predicate.And(q => q.workStatusCode == workstatus);

            

                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID
                                

                               select new myShemeList
                               {
                                   WardName = (from cons in context.mConstituency
                                               where cons.ConstituencyCode == item.constituencyID
                                               select
"Ward " + cons.ConstituencyCode + "-  " + cons.ConstituencyName).FirstOrDefault(),
                                   constituencyID =item.constituencyID,
                                   workCode = item.WorkCode != null ? item.WorkCode : "",
                                   workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                                   mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,

                                   sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,

                                   EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                                   Panchayat = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatCode).FirstOrDefault(),
                                   PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                                   programName = item.ProgrammeName != null ? item.ProgrammeName : "",
                                   //sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                                   districtID = item.DistrictId != null ? item.DistrictId : 0,

                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveDepartmentName = (from d in context.mDepartment where d.deptId == M.ExecutingDepartmentId select d.deptname).FirstOrDefault(),

                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   oWnerDepartment = M.Owner_deptid != null ? M.Owner_deptid : "",
                                   //ExecutiveOfficeName = M.ExecutingOfficeID != null ? getExecutiveOfficeName(M.ExecutingOfficeID) : "",
                                   ExecutiveOfficeName = (from o in context.mOffice where o.OfficeId == M.ExecutingOfficeID select o.officename).FirstOrDefault(),

                                   //physicalProgress = (from o in context.ConstituencyWorkProgress where o.schemeCode == item.schemeConstituencyID select o.toalProgress).FirstOrDefault(),
                                   physicalProgress = (from o in context.ConstituencyWorkProgress
                                                       where o.schemeCode == item.schemeConstituencyID
                                                       orderby o.submitDate descending
                                                       select o.toalProgress).FirstOrDefault(),

                                   financialProgress = (from o in context.ConstituencyWorkProgress
                                                        where o.schemeCode == item.schemeConstituencyID
                                                        orderby o.submitDate descending
                                                        select o.toalExpenditure).FirstOrDefault(),

                                   financialYear = item.financialYear != null ? item.financialYear : "",

                                   CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",

                                   imageCount = (from pcons in context.ConstituencyWorkProgressImages where pcons.schemeCode == item.schemeConstituencyID select pcons.autoID).Count(),
                                   CompletionFinancialYear = item.financialYear,
                                   workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                               
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                                   subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,

                                   WorkType = item.workType != null ? item.workType : "0",

                                   ProgrammeName = (from pcons in context.mProgramme
                                                    where pcons.programmeCode.ToString() == item.ProgrammeName

                                                    select pcons.programmeName).FirstOrDefault()







                               }).AsExpandable().Where(predicate).OrderByDescending(m => m.mySchemeID).ToList();



                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }

        public static object gelFilteredMySchemeListByType1(object param)
        {

            string[] arr = param as string[];
            // string agency = arr[0];
            long workstatus = Convert.ToInt64(arr[0]);
            int Distt = Convert.ToInt32(arr[1]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                //if (agency != "0")
                //    predicate = predicate.And(q => q.ExecutiveDepartment == agency);
                if (workstatus != 0)
                    predicate = predicate.And(q => q.workStatusCode == workstatus);
                if (Distt != 0 && Distt != null)
                    predicate = predicate.And(q => q.districtID == Distt);
                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID

                               select new myShemeList
                               {
                                   workCode = item.WorkCode != null ? item.WorkCode : "",
                                   workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                                   mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,

                                   sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,

                                   EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                                   Panchayat = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatCode).FirstOrDefault(),
                                   PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                                   programName = item.ProgrammeName != null ? item.ProgrammeName : "",
                                   //sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                                   districtID = item.DistrictId != null ? item.DistrictId : 0,

                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveDepartmentName = (from d in context.mDepartment where d.deptId == M.ExecutingDepartmentId select d.deptname).FirstOrDefault(),

                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   oWnerDepartment = M.Owner_deptid != null ? M.Owner_deptid : "",
                                   //ExecutiveOfficeName = M.ExecutingOfficeID != null ? getExecutiveOfficeName(M.ExecutingOfficeID) : "",
                                   ExecutiveOfficeName = (from o in context.mOffice where o.OfficeId == M.ExecutingOfficeID select o.officename).FirstOrDefault(),
                                   physicalProgress = (from o in context.ConstituencyWorkProgress
                                                       where o.schemeCode == item.schemeConstituencyID
                                                       orderby o.submitDate descending
                                                       select o.toalProgress).FirstOrDefault(),

                                   financialProgress = (from o in context.ConstituencyWorkProgress
                                                        where o.schemeCode == item.schemeConstituencyID
                                                        orderby o.submitDate descending
                                                        select o.toalExpenditure).FirstOrDefault(),

                                   financialYear = item.financialYear != null ? item.financialYear : "",

                                   CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",

                                   imageCount = (from pcons in context.ConstituencyWorkProgressImages where pcons.schemeCode == item.schemeConstituencyID select pcons.autoID).Count(),
                                   CompletionFinancialYear = item.financialYear,
                                   workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                                   subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,

                                   WorkType = item.workType != null ? item.workType : "0",

                               }).AsExpandable().Where(predicate).OrderByDescending(m => m.mySchemeID).ToList();

                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }

        public static object gelFilteredMySchemeListBySubDivision(object param)
        {

            string[] arr = param as string[];
            // string agency = arr[0];
            long workstatus = Convert.ToInt64(arr[0]);
            int Subdivision = Convert.ToInt32(arr[1]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                //if (agency != "0")
                //    predicate = predicate.And(q => q.ExecutiveDepartment == agency);
                if (workstatus != 0)
                    predicate = predicate.And(q => q.workStatusCode == workstatus);
                if (Subdivision != 0 && Subdivision != null)
                    predicate = predicate.And(q => q.subdevisionId == Subdivision);
                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID

                               select new myShemeList
                               {
                                   workCode = item.WorkCode != null ? item.WorkCode : "",
                                   workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                                   mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,
                                   // subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,
                                   sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,

                                   EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                                   Panchayat = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatCode).FirstOrDefault(),
                                   PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                                   programName = item.ProgrammeName != null ? item.ProgrammeName : "",
                                   //sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                                   districtID = item.DistrictId != null ? item.DistrictId : 0,

                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveDepartmentName = (from d in context.mDepartment where d.deptId == M.ExecutingDepartmentId select d.deptname).FirstOrDefault(),

                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   oWnerDepartment = M.Owner_deptid != null ? M.Owner_deptid : "",
                                   //ExecutiveOfficeName = M.ExecutingOfficeID != null ? getExecutiveOfficeName(M.ExecutingOfficeID) : "",
                                   ExecutiveOfficeName = (from o in context.mOffice where o.OfficeId == M.ExecutingOfficeID select o.officename).FirstOrDefault(),
                                   physicalProgress = (from o in context.ConstituencyWorkProgress
                                                       where o.schemeCode == item.schemeConstituencyID
                                                       orderby o.submitDate descending
                                                       select o.toalProgress).FirstOrDefault(),

                                   financialProgress = (from o in context.ConstituencyWorkProgress
                                                        where o.schemeCode == item.schemeConstituencyID
                                                        orderby o.submitDate descending
                                                        select o.toalExpenditure).FirstOrDefault(),

                                   financialYear = item.financialYear != null ? item.financialYear : "",

                                   CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",

                                   imageCount = (from pcons in context.ConstituencyWorkProgressImages where pcons.schemeCode == item.schemeConstituencyID select pcons.autoID).Count(),
                                   CompletionFinancialYear = item.financialYear,
                                   workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                                   subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,

                                   WorkType = item.workType != null ? item.workType : "0",

                               }).AsExpandable().Where(predicate).OrderByDescending(m => m.mySchemeID).ToList();

                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }

        public static object gelFilteredMySchemeListByType2(object param)
        {

            string[] arr = param as string[];
            // string agency = arr[0];
            long workstatus = Convert.ToInt64(arr[0]);
            int Constituency = Convert.ToInt32(arr[1]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                //if (agency != "0")
                //    predicate = predicate.And(q => q.ExecutiveDepartment == agency);
                if (workstatus != 0)
                    predicate = predicate.And(q => q.workStatusCode == workstatus);
                if (Constituency != 0 && Constituency != null)
                    predicate = predicate.And(q => q.constituencyID == Constituency);
                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID

                               select new myShemeList
                               {
                                   constituencyID = item.constituencyID != null ? item.constituencyID : 0,
                                   workCode = item.WorkCode != null ? item.WorkCode : "",
                                   workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                                   mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,

                                   sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,

                                   EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                                   Panchayat = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatCode).FirstOrDefault(),
                                   PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                                   programName = item.ProgrammeName != null ? item.ProgrammeName : "",
                                   //sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                                   districtID = item.DistrictId != null ? item.DistrictId : 0,

                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveDepartmentName = (from d in context.mDepartment where d.deptId == M.ExecutingDepartmentId select d.deptname).FirstOrDefault(),

                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   oWnerDepartment = M.Owner_deptid != null ? M.Owner_deptid : "",
                                   //ExecutiveOfficeName = M.ExecutingOfficeID != null ? getExecutiveOfficeName(M.ExecutingOfficeID) : "",
                                   ExecutiveOfficeName = (from o in context.mOffice where o.OfficeId == M.ExecutingOfficeID select o.officename).FirstOrDefault(),
                                   physicalProgress = (from o in context.ConstituencyWorkProgress
                                                       where o.schemeCode == item.schemeConstituencyID
                                                       orderby o.submitDate descending
                                                       select o.toalProgress).FirstOrDefault(),

                                   financialProgress = (from o in context.ConstituencyWorkProgress
                                                        where o.schemeCode == item.schemeConstituencyID
                                                        orderby o.submitDate descending
                                                        select o.toalExpenditure).FirstOrDefault(),

                                   financialYear = item.financialYear != null ? item.financialYear : "",

                                   CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",

                                   imageCount = (from pcons in context.ConstituencyWorkProgressImages where pcons.schemeCode == item.schemeConstituencyID select pcons.autoID).Count(),
                                   CompletionFinancialYear = item.financialYear,
                                   workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                                   subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,

                                   WorkType = item.workType != null ? item.workType : "0",

                               }).AsExpandable().Where(predicate).OrderByDescending(m => m.mySchemeID).ToList();

                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }

        public static object gelFilteredMySchemeListByTypeCount(object param)
        {

            string[] arr = param as string[];
            string agency = arr[0];

            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                if (agency != "0" && agency!="")
                    predicate = predicate.And(q => q.ExecutiveDepartment == agency);

                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID

                               select new myShemeList
                               {

                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   //workStatusCode = item.workStatus,
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                               }).AsExpandable().Where(predicate).ToList();

                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }


        public static object gelConstitFilteredMySchemeListByTypeCount(object param)
        {

            string[] arr = param as string[];
            Int64 Consttituency = Convert .ToInt64 (arr[0]);

            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                if (Consttituency != 0)
                    predicate = predicate.And(q => q.constituencyID == Consttituency);

                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID

                               select new myShemeList
                               {

                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   //workStatusCode = item.workStatus,
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                               }).AsExpandable().Where(predicate).ToList();

                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }
        public static object gelFilteredMySchemeListByType1Count(object param)
        {

            string[] arr = param as string[];
            int distt = Convert.ToInt32(arr[0]);

            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                if (distt != 0 && distt != null)
                    predicate = predicate.And(q => q.districtID == distt);

                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID

                               select new myShemeList
                               {
                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   districtID = item.DistrictId,
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                               }).AsExpandable().Where(predicate).ToList();

                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }

        public static object gelFilteredMySchemeListByTypeSDMCount(object param)
        {

            string[] arr = param as string[];
            int SubDivisionId = Convert.ToInt32(arr[0]);

            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                if (SubDivisionId != 0 && SubDivisionId != null)
                    predicate = predicate.And(q => q.subdevisionId == SubDivisionId);

                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID

                               select new myShemeList
                               {
                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   districtID = item.DistrictId,
                                   subdevisionId = item.SubdevisionId,
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                               }).AsExpandable().Where(predicate).ToList();

                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }

        public static object gelFilteredMySchemeListByType2Count(object param)
        {

            string[] arr = param as string[];
            int Const = Convert.ToInt32(arr[0]);

            using (ConstituencyContext context = new ConstituencyContext())
            {
                schemeMapping scheme = new schemeMapping();
                var predicate = PredicateBuilder.True<myShemeList>();

                if (Const != 0 && Const != null)
                    predicate = predicate.And(q => q.constituencyID == Const);


                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID

                               select new myShemeList
                               {
                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   districtID = item.DistrictId,
                                   constituencyID = item.constituencyID != null ? item.constituencyID : 0,
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                               }).AsExpandable().Where(predicate).ToList();

                scheme.mySchemeList = mResult.ToList();
                return scheme;
            }
        }

        public static object gelFilteredMySchemeListAll(object param)
        {

            string[] arr = param as string[];
            string program = arr[1], ControllingAuthority = arr[3], FinancialYear = arr[4], agency = arr[5], mOwnerDepartment = arr[6],
            Panchayat = arr[7], WorkType = arr[8];

            long constituencyID = Convert.ToInt32(arr[0]);
            long districtCode = Convert.ToInt32(arr[10]);

            long workstatus = Convert.ToInt64(arr[9]);
            string aadharID = arr[11];
            bool member = Convert.ToBoolean(arr[12]);
            string cFinancialYear = arr[13];
            string sanctionedDate = arr[14];
            string SectionAmount = arr[16];
            string IsDpr = arr[17];
            string IsImages = arr[18];
            string MLAfeb = arr[19];
            long Office = Convert.ToInt64(arr[20]);
            int SubDivision = Convert.ToInt16(arr[21]);

            using (ConstituencyContext context = new ConstituencyContext())
            {
                // var currentAssembly = (from site in context.SiteSettings
                //   where site.SettingName == "Assembly"
                //  select site.SettingValue).FirstOrDefault();
                // int assemblyID = Convert.ToInt32(currentAssembly);
                //List<vsSchemeList> list = new List<vsSchemeList>();
                //List<myShemeList> mylist = new List<myShemeList>();
                schemeMapping scheme = new schemeMapping();
                //{
                //    ControllingAuthorityID = ControllingAuthority,
                //    FinancialYear = FinancialYear,
                //    eConstituencyID = constituencyID,
                //    vsConstituencyID = constituencyID,
                //    vsSchemeList = list,
                //    mySchemeList = mylist,
                //    constituenctName = (from cons in context.mConstituency where cons.AssemblyID == 13 && cons.ConstituencyCode == constituencyID select cons.ConstituencyName).FirstOrDefault()
                //};
                //List<SchemeMastersConstituency> myresult = null;
                //myresult = (List<SchemeMastersConstituency>)getAllWorkList();
                //myresult = myresult.ToList();

                var predicate = PredicateBuilder.True<myShemeList>();

                if (constituencyID != 0)
                    predicate = predicate.And(q => q.constituencyID == constituencyID);

                if (agency != "0")
                    predicate = predicate.And(q => q.ExecutiveDepartment == agency);
                if (workstatus != 0)
                    predicate = predicate.And(q => q.workStatusCode == workstatus);

                //if (Office != 0 && Office != null)
                //{
                //    predicate = predicate.And(a => a.ExecutiveOfficeId == Office);
                //}
                if (mOwnerDepartment != "0")
                {
                    predicate = predicate.And(a => a.oWnerDepartment == mOwnerDepartment);
                }
                if (FinancialYear != "0")
                {
                    predicate = predicate.And(a => a.financialYear == FinancialYear);
                }

                if (SubDivision != 0 && SubDivision != null)
                {
                    predicate = predicate.And(a => a.subdevisionId == SubDivision);
                }
                //if (workstatus == 3 && cFinancialYear != "0")
                //{
                //    // scheme.mySchemeList = scheme.mySchemeList.Where(a => a.workStatus == workstatus).ToList();
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.CompletionFinancialYear == cFinancialYear).ToList();
                //}
                if (Panchayat != "0")
                {
                    Int64 pnccode = Convert.ToInt64(Panchayat);
                    predicate = predicate.And(a => a.Panchayat == pnccode);
                }

                if (IsImages != "0")
                {
                    if (IsImages == "1")
                    {
                        predicate = predicate.And(a => a.imageCount != 0);
                    }
                    else
                    {
                        predicate = predicate.And(a => a.imageCount == 0);
                    }
                }

                if (workstatus != 0)
                {
                    predicate = predicate.And(a => a.workStatusCode == workstatus);
                }
                if (ControllingAuthority != "0" && ControllingAuthority != "" && ControllingAuthority != null)
                {
                    predicate = predicate.And(a => a.ControllingAuthorityId == ControllingAuthority);
                }

                if (districtCode != 0)
                {
                    predicate = predicate.And(a => a.districtID == districtCode);
                }
                if (WorkType != "0" && WorkType != null && WorkType != "" && MLAfeb == "0")
                {
                    predicate = predicate.And(a => a.WorkType == WorkType);
                }
                if (MLAfeb != "0")
                {
                    predicate = predicate.And(a => a.programName == MLAfeb);
                }
                if (program != "0")
                {
                    predicate = predicate.And(a => a.programName == program);
                }
                if (IsDpr != "0")
                {
                    if (IsDpr == "1")
                    {
                        predicate = predicate.And(a => a.dprFileName != null);
                    }
                    else
                    {
                        predicate = predicate.And(a => a.dprFileName == null);
                    }
                }

                if (SectionAmount != "0")
                {
                    switch (SectionAmount)
                    {
                        case "0-1 LL":
                            predicate = predicate.And(a => a.sanctionedAmount >= 0 && a.sanctionedAmount <= 100000);

                            break;

                        case "1-2 LL":
                            predicate = predicate.And(a => a.sanctionedAmount >= 100000 && a.sanctionedAmount <= 200000);
                            break;

                        case "2-5 LL":
                            predicate = predicate.And(a => a.sanctionedAmount >= 200000 && a.sanctionedAmount <= 500000);
                            break;
                        case "5-10 LL":
                            predicate = predicate.And(a => a.sanctionedAmount >= 500000 && a.sanctionedAmount <= 1000000);
                            break;
                        case "10-25 LL":
                            predicate = predicate.And(a => a.sanctionedAmount >= 1000000 && a.sanctionedAmount <= 2500000);
                            break;

                        case "25-50 LL":
                            predicate = predicate.And(a => a.sanctionedAmount >= 2500000 && a.sanctionedAmount <= 5000000);
                            break;
                        case "50-1 LC":
                            predicate = predicate.And(a => a.sanctionedAmount >= 5000000 && a.sanctionedAmount <= 10000000);
                            break;
                        case "1-2 CC":
                            predicate = predicate.And(a => a.sanctionedAmount >= 10000000 && a.sanctionedAmount <= 20000000);
                            break;
                        case "2-5 CC":
                            predicate = predicate.And(a => a.sanctionedAmount >= 20000000 && a.sanctionedAmount <= 50000000);
                            break;
                        case "5 +":
                            predicate = predicate.And(a => a.sanctionedAmount >= 50000000);
                            break;
                        default:
                            break;
                    }
                }

                var mResult = (from item in context.SchemeMastersConstituency
                               join M in context.ConstituencySchemesBasicDetails on item.schemeConstituencyID equals M.mySchemeID
                               //join Ma in context.ConstituencyWorkProgress on item.schemeConstituencyID equals Ma.schemeCode
                               //join Img in context.ConstituencyWorkProgressImages on item.schemeConstituencyID equals Img.schemeCode

                               select new myShemeList
                               {
                                   workCode = item.WorkCode != null ? item.WorkCode : "",
                                   workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                                   mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,
                                   constituencyID = item.constituencyID != null ? item.constituencyID : 0,
                                   sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,

                                   EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                                   Panchayat = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatCode).FirstOrDefault(),

                                   PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                                   programName = item.ProgrammeName != null ? item.ProgrammeName : "",
                                   //sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                                   districtID = item.DistrictId != null ? item.DistrictId : 0,

                                   ExecutiveDepartment = M.ExecutingDepartmentId != null ? M.ExecutingDepartmentId : "",   // by me 
                                   ExecutiveDepartmentName = (from d in context.mDepartment where d.deptId == M.ExecutingDepartmentId select d.deptname).FirstOrDefault(),

                                   ExecutiveOfficeId = M.ExecutingOfficeID,
                                   oWnerDepartment = M.Owner_deptid != null ? M.Owner_deptid : "",
                                   //ExecutiveOfficeName = M.ExecutingOfficeID != null ? getExecutiveOfficeName(M.ExecutingOfficeID) : "",
                                   ExecutiveOfficeName = (from o in context.mOffice where o.OfficeId == M.ExecutingOfficeID select o.officename).FirstOrDefault(),

                                   physicalProgress = (from o in context.ConstituencyWorkProgress
                                                       where o.schemeCode == item.schemeConstituencyID
                                                       orderby o.submitDate descending
                                                       select o.toalProgress).FirstOrDefault(),

                                   financialProgress = (from o in context.ConstituencyWorkProgress
                                                        where o.schemeCode == item.schemeConstituencyID
                                                        orderby o.submitDate descending
                                                        select o.toalExpenditure).FirstOrDefault(),

                                   financialYear = item.financialYear != null ? item.financialYear : "",

                                   CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",

                                   imageCount = (from pcons in context.ConstituencyWorkProgressImages where pcons.schemeCode == item.schemeConstituencyID select pcons.autoID).Count(),
                                   CompletionFinancialYear = item.financialYear,
                                   workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                                   workStatusCode = item.workStatus != null ? item.workStatus : 0,
                                   subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,
                                   dprFileName = item.dprFileName,
                                   ControllingAuthorityId = item.ControllingAuthorityId,
                                   WorkType = item.workType != null ? item.workType : "0",

                               }).AsExpandable().Where(predicate).OrderByDescending(m => m.mySchemeID).ToList();



                scheme.mySchemeList = mResult.ToList();

                //if (agency != "0")
                //{
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.ExecutiveDepartment == agency).ToList();
                //}
                //if (Office != "0" && Office != null)
                //{
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.ExecutiveOffice == Office).ToList();
                //}
                //if (mOwnerDepartment != "0")
                //{
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.oWnerDepartment == mOwnerDepartment).ToList();
                //}
                //if (FinancialYear != "0")
                //{
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.financialYear == FinancialYear).ToList();
                //}
                //if (workstatus == 3 && cFinancialYear != "0")
                //{
                //    // scheme.mySchemeList = scheme.mySchemeList.Where(a => a.workStatus == workstatus).ToList();
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.CompletionFinancialYear == cFinancialYear).ToList();
                //}
                //if (Panchayat != "0")
                //{
                //    Int64 pnccode = Convert.ToInt64(Panchayat);
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.Panchayat == pnccode).ToList();
                //}

                //if (IsImages != "0")
                //{
                //    if (IsImages == "1")
                //    {
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.imageCount != 0).ToList();
                //    }
                //    else
                //    {
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.imageCount == 0).ToList();
                //    }
                //}
                //switch (SectionAmount)
                //{
                //    case "0-1 LL":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 0 && a.sanctionedAmount <= 100000).ToList();

                //        break;

                //    case "1-2 LL":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 100000 && a.sanctionedAmount <= 200000).ToList();
                //        break;

                //    case "2-5 LL":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 200000 && a.sanctionedAmount <= 500000).ToList();
                //        break;
                //    case "5-10 LL":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 500000 && a.sanctionedAmount <= 1000000).ToList();
                //        break;
                //    case "10-25 LL":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 1000000 && a.sanctionedAmount <= 2500000).ToList();
                //        break;

                //    case "25-50 LL":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 2500000 && a.sanctionedAmount <= 5000000).ToList();
                //        break;
                //    case "50-1 LC":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 5000000 && a.sanctionedAmount <= 10000000).ToList();
                //        break;
                //    case "1-2 CC":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 10000000 && a.sanctionedAmount <= 20000000).ToList();
                //        break;
                //    case "2-5 CC":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 20000000 && a.sanctionedAmount <= 50000000).ToList();
                //        break;
                //    case "5 +":
                //        scheme.mySchemeList = scheme.mySchemeList.Where(a => a.sanctionedAmount >= 50000000).ToList();
                //        break;
                //    default:
                //        break;
                //}
                //scheme.mySchemeList= scheme.mySchemeList.Take(10).ToList();
                return scheme;
            }
        }

        public static object getAllWorkList(long constituencyID, bool isMember, string aadharID)
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {


                if (constituencyID != 0)
                {
                    return (from list in context.SchemeMastersConstituency
                            where list.constituencyID == constituencyID
                            select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
                }
                else
                {
                    if (string.IsNullOrEmpty(aadharID) && isMember)
                    {
                        return (from list in context.SchemeMastersConstituency
                                select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
                    }
                    else
                    {

                        //List<long> workList = (from wlist in context.ForwardGrievance
                        //                       where wlist.OfficerCode == aadharID
                        //                       && wlist.Type == "Work"
                        //                       select wlist.GrvCode).Cast<long>().ToList();

                        //return (from list in context.SchemeMastersConstituency
                        //        where workList.Contains(list.schemeConstituencyID)
                        //        select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();

                        //var muser = (from user in context.mUsers
                        //             join office in context.mOffice
                        //             on user.OfficeId equals office.OfficeId
                        //             where user.AadarId == aadharID
                        //             select new
                        //             {
                        //                 deptID = user.DeptId,
                        //                 officeCode = office.OfficeId
                        //             }
                        //           ).FirstOrDefault();

                        var workList = (from work in context.ConstituencySchemesBasicDetails

                                            //where work.ExecutingDepartmentId == muser.deptID
                                            //&& work.ExecutingOfficeID == muser.officeCode
                                        select work.mySchemeID).ToList();

                        return (from list in context.SchemeMastersConstituency
                                where workList.Contains(list.schemeConstituencyID)
                                select list).OrderByDescending(a => a.ModifiedDate).ThenByDescending(a => a.createdDate).ToList();
                    }
                }
            }
        }
        public static object gelFilteredCommisionerSchemeList(object param)
        {


            string[] arr = param as string[];
            string program = arr[1], ControllingAuthority = arr[3], FinancialYear = arr[4], agency = arr[5], mOwnerDepartment = arr[6],
                Panchayat = arr[7], WorkType = arr[8]; // OfficeId = arr[16];

            long constituencyID = Convert.ToInt32(arr[0]);
            long districtCode = Convert.ToInt32(arr[10]);
            //  long demandCode = Convert.ToInt64(arr[2]);
            long workstatus = Convert.ToInt64(arr[9]);
            string aadharID = arr[11];
            bool member = Convert.ToBoolean(arr[12]);
            string cFinancialYear = arr[13];
            string sanctionedDate = arr[14];
            //Int32 SubdivisionCode = Convert.ToInt32(arr[15]);
            string SubdivisionCode = arr[15];
            // List<string> SubdivisionIds = new List<string>();
            //string[] SubdivisionIds = param as string[];
            //SubdivisionIds = SubdivisionCode.Split(',');
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);
                List<vsSchemeList> list = new List<vsSchemeList>();
                List<myShemeList> mylist = new List<myShemeList>();
                schemeMapping scheme = new schemeMapping();

                if (constituencyID == 0)
                {

                    scheme.ControllingAuthorityID = ControllingAuthority;
                    scheme.FinancialYear = FinancialYear;
                    scheme.eConstituencyID = constituencyID;
                    scheme.vsConstituencyID = constituencyID;
                    scheme.vsSchemeList = list;
                    scheme.mySchemeList = mylist;

                    scheme.constituenctName = (from cons in context.mConstituency select cons.ConstituencyName).FirstOrDefault();


                }
                else
                {
                    scheme.ControllingAuthorityID = ControllingAuthority;
                    scheme.FinancialYear = FinancialYear;
                    scheme.eConstituencyID = constituencyID;
                    scheme.vsConstituencyID = constituencyID;
                    scheme.vsSchemeList = list;
                    scheme.mySchemeList = mylist;

                    scheme.constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == constituencyID select cons.ConstituencyName).FirstOrDefault();
                }
                member = true;
                List<SchemeMastersConstituency> myresult = (List<SchemeMastersConstituency>)getAllMyWorkList(constituencyID, member, null);

                //  myresult = myresult.Where(a => a.workStatus == workstatus && a.ControllingAuthorityId == ControllingAuthority).ToList();
                myresult = myresult.ToList();
                if (workstatus != 0)
                {
                    myresult = myresult.Where(a => a.workStatus == workstatus).ToList();
                }
                if (ControllingAuthority != "0" && ControllingAuthority != "" && ControllingAuthority != null)
                {
                    myresult = myresult.Where(a => a.ControllingAuthorityId == ControllingAuthority).ToList();
                }
                //if (demandCode != 0);
                //{
                //    myresult = myresult.Where(a => a.DemandCode == demandCode).ToList();
                //}
                if (districtCode != 0)
                {
                    myresult = myresult.Where(a => a.DistrictId == districtCode).ToList();
                }
                if (WorkType != "0" && WorkType != null && WorkType != "")
                {
                    myresult = myresult.Where(a => a.workType == WorkType).ToList();
                }
                if (program != "0")
                {
                    myresult = myresult.Where(a => a.ProgrammeName == program).ToList();
                }
                if (sanctionedDate != "0" && sanctionedDate != "" && sanctionedDate != null)
                {
                    //DateTime ScDate = Convert.ToDateTime(sanctionedDate);
                    string Date = sanctionedDate;
                    Int16 dd = Convert.ToInt16(sanctionedDate.Substring(0, 2));
                    Int16 mm = Convert.ToInt16(sanctionedDate.Substring(3, 2));
                    Int16 yyyy = Convert.ToInt16(sanctionedDate.Substring(6, 4));
                    // DateTime dtDate = new DateTime(2018, 2, 12);
                    DateTime dtDate = new DateTime(yyyy, mm, dd);
                    //  model.EnclosureDate = dtDate;
                    //  myresult = myresult.Where(a => a.SanctionedDate.Value.ToString("dd-MM-yyyy") == sanctionedDate).ToList();
                    myresult = myresult.Where(a => a.SanctionedDate.Value.ToString("dd-MM-yyyy") == dtDate.ToString("dd-MM-yyyy")).ToList();
                }
                foreach (SchemeMastersConstituency item in myresult)
                {
                    var workDetails = (from cons in context.ConstituencySchemesBasicDetails
                                       where cons.mySchemeID == item.schemeConstituencyID
                                       select cons).FirstOrDefault();
                    var physicalDetails = (from pcons in context.ConstituencyWorkProgress
                                           where pcons.schemeCode == item.schemeConstituencyID
                                           orderby pcons.submitDate descending
                                           select pcons).FirstOrDefault();
                    var imageCount = (from pcons in context.ConstituencyWorkProgressImages
                                      where pcons.schemeCode == item.schemeConstituencyID
                                      select pcons).Count();
                    var ConsName = (from pcons in context.mConstituency
                                    where pcons.ConstituencyCode == item.constituencyID
                                    select pcons).FirstOrDefault();
                    int projectid = Convert.ToInt32(item.ProgrammeName);
                    var ProjectName = (from pcons in context.mProgramme
                                       where pcons.programmeCode == projectid
                                       select pcons).FirstOrDefault();

                    mylist.Add(new myShemeList()
                    {
                        workCode = item.WorkCode != null ? item.WorkCode : "",
                        workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                        mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,

                        sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,

                        EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                        Panchayat = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatCode).FirstOrDefault(),
                        PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                        programName = item.ProgrammeName != null ? item.ProgrammeName : "",
                        ProgrammeName = ProjectName.programmeName != null ? ProjectName.programmeName : "",
                        sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                        districtID = item.DistrictId != null ? item.DistrictId : 0,
                        WardName = "Ward " + ConsName.ConstituencyCode + "-  " + ConsName.ConstituencyName,
                        ExecutiveDepartmentName = workDetails != null ? getExecutiveDepartmentName(workDetails.ExecutingDepartmentId) : "",   // by  

                        ExecutiveDepartment = workDetails != null ? workDetails.ExecutingDepartmentId : "",
                        // workDetails.ExecutingDepartmentId != null ? workDetails.ExecutingDepartmentId : "",

                        // ExecutiveOffice = Convert.ToString(workDetails.ExecutingOfficeID),
                        ExecutiveOffice = workDetails != null ? Convert.ToString(workDetails.ExecutingOfficeID) : "",
                        oWnerDepartment = workDetails != null ? workDetails.Owner_deptid : "",
                        ExecutiveOfficeName = workDetails != null ? getExecutiveOfficeName(workDetails.ExecutingOfficeID) : "",
                        physicalProgress = physicalDetails != null ? physicalDetails.toalProgress : 0,
                        financialProgress = physicalDetails != null ? physicalDetails.toalExpenditure : 0,
                        financialYear = item.financialYear != null ? item.financialYear : "",

                        CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",

                        imageCount = imageCount != null ? imageCount : 0,
                        CompletionFinancialYear = workDetails != null ? item.financialYear : "",
                        workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                        workStatusCode = item.workStatus != null ? item.workStatus : 0,
                        subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,
                        // ProgrammeName = item.ProgrammeName != null ? item.ProgrammeName:"0",
                        WorkType = item.workType != null ? item.workType : "0",
                        // WorkType = item.work
                    });
                }

                //if (OfficeId != "0")
                //{
                //    //int offc = Convert.ToInt16(OfficeId);
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.ExecutiveOffice == OfficeId).ToList();
                //}

                if (agency != "0")
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.ExecutiveDepartment == agency).ToList();
                }
                if (mOwnerDepartment != "0")
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.oWnerDepartment == mOwnerDepartment).ToList();
                }
                if (FinancialYear != "0")
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.financialYear == FinancialYear).ToList();
                }
                if (workstatus == 3)
                {
                    // scheme.mySchemeList = scheme.mySchemeList.Where(a => a.workStatus == workstatus).ToList();
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.CompletionFinancialYear == cFinancialYear).ToList();
                }
                if (Panchayat != "0")
                {
                    Int64 pnccode = Convert.ToInt64(Panchayat);
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.Panchayat == pnccode).ToList();
                }
                //if (!member)
                //{
                //    List<long> workList = (from wlist in context.ForwardGrievance
                //                           where wlist.OfficerCode == aadharID
                //                           && wlist.Type == "Work"
                //                           select wlist.GrvCode).Cast<long>().ToList();
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => workList.Contains(a.mySchemeID)).Select(a => a).ToList();
                //}
                return scheme;
            }
        }
        public static object gelFilteredMySchemeList1(object param)
        {


            string[] arr = param as string[];
            string program = arr[1], ControllingAuthority = arr[3], FinancialYear = arr[4], agency = arr[5], mOwnerDepartment = arr[6],
                Panchayat = arr[7], WorkType = arr[8]; // OfficeId = arr[16];

            long constituencyID = Convert.ToInt32(arr[0]);
            long districtCode = Convert.ToInt32(arr[10]);
            //  long demandCode = Convert.ToInt64(arr[2]);
            long workstatus = Convert.ToInt64(arr[9]);
            string aadharID = arr[11];
            bool member = Convert.ToBoolean(arr[12]);
            string cFinancialYear = arr[13];
            string sanctionedDate = arr[14];
            //Int32 SubdivisionCode = Convert.ToInt32(arr[15]);
            string SubdivisionCode = arr[15];
            // List<string> SubdivisionIds = new List<string>();
            //string[] SubdivisionIds = param as string[];
            //SubdivisionIds = SubdivisionCode.Split(',');
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);
                List<vsSchemeList> list = new List<vsSchemeList>();
                List<myShemeList> mylist = new List<myShemeList>();
                schemeMapping scheme = new schemeMapping();

                if (constituencyID == 0)
                {

                    scheme.ControllingAuthorityID = ControllingAuthority;
                    scheme.FinancialYear = FinancialYear;
                    scheme.eConstituencyID = constituencyID;
                    scheme.vsConstituencyID = constituencyID;
                    scheme.vsSchemeList = list;
                    scheme.mySchemeList = mylist;

                    scheme.constituenctName = "All";
                }
                else
                {
                    scheme.ControllingAuthorityID = ControllingAuthority;
                    scheme.FinancialYear = FinancialYear;
                    scheme.eConstituencyID = constituencyID;
                    scheme.vsConstituencyID = constituencyID;
                    scheme.vsSchemeList = list;
                    scheme.mySchemeList = mylist;

                    scheme.constituenctName = (from cons in context.mConstituency where  cons.ConstituencyCode  == constituencyID select cons.ConstituencyName).FirstOrDefault();
                    
                }

                List<SchemeMastersConstituency> myresult = null;
                if (SubdivisionCode != "0")
                {

                    myresult = (List<SchemeMastersConstituency>)getAllMyWork_SDM(SubdivisionCode);
                }
                else
                {
                    myresult = (List<SchemeMastersConstituency>)getAllMyWorkList(constituencyID, member, aadharID, workstatus, ControllingAuthority, districtCode, WorkType, program, sanctionedDate);
                }
                //  myresult = myresult.Where(a => a.workStatus == workstatus && a.ControllingAuthorityId == ControllingAuthority).ToList();
                myresult = myresult.ToList();
                if (workstatus != 0)
                {
                    myresult = myresult.Where(a => a.workStatus == workstatus).ToList();
                }
                if (ControllingAuthority != "0" && ControllingAuthority != "" && ControllingAuthority != null)
                {
                    myresult = myresult.Where(a => a.ControllingAuthorityId == ControllingAuthority).ToList();
                }
                //if (demandCode != 0);
                //{
                //    myresult = myresult.Where(a => a.DemandCode == demandCode).ToList();
                //}
                if (districtCode != 0)
                {
                    myresult = myresult.Where(a => a.DistrictId == districtCode).ToList();
                }
                if (WorkType != "0" && WorkType != null && WorkType != "")
                {
                    myresult = myresult.Where(a => a.workType == WorkType).ToList();
                }
                if (program != "0")
                {
                    myresult = myresult.Where(a => a.ProgrammeName == program).ToList();
                }
                if (sanctionedDate != "0" && sanctionedDate != "" && sanctionedDate != null)
                {
                    //DateTime ScDate = Convert.ToDateTime(sanctionedDate);
                    string Date = sanctionedDate;
                    Int16 dd = Convert.ToInt16(sanctionedDate.Substring(0, 2));
                    Int16 mm = Convert.ToInt16(sanctionedDate.Substring(3, 2));
                    Int16 yyyy = Convert.ToInt16(sanctionedDate.Substring(6, 4));
                    // DateTime dtDate = new DateTime(2018, 2, 12);
                    DateTime dtDate = new DateTime(yyyy, mm, dd);
                    //  model.EnclosureDate = dtDate;
                    //  myresult = myresult.Where(a => a.SanctionedDate.Value.ToString("dd-MM-yyyy") == sanctionedDate).ToList();
                    myresult = myresult.Where(a => a.SanctionedDate.Value.ToString("dd-MM-yyyy") == dtDate.ToString("dd-MM-yyyy")).ToList();
                }
                foreach (SchemeMastersConstituency item in myresult)
                {
                    var workDetails = (from cons in context.ConstituencySchemesBasicDetails
                                       where cons.mySchemeID == item.schemeConstituencyID
                                       select cons).FirstOrDefault();
                    var physicalDetails = (from pcons in context.ConstituencyWorkProgress
                                           where pcons.schemeCode == item.schemeConstituencyID
                                           orderby pcons.submitDate descending
                                           select pcons).FirstOrDefault();
                    var imageCount = (from pcons in context.ConstituencyWorkProgressImages
                                      where pcons.schemeCode == item.schemeConstituencyID
                                      select pcons).Count();
                    var ConsName = (from pcons in context.mConstituency
                                    where pcons.ConstituencyCode == item.constituencyID
                                    select pcons).FirstOrDefault();
                    int projectid = Convert.ToInt32(item.ProgrammeName);
                    var ProjectName = (from pcons in context.mProgramme
                                       where pcons.programmeCode == projectid
                                       select pcons).FirstOrDefault();

                    mylist.Add(new myShemeList()
                    {
                        workCode = item.WorkCode != null ? item.WorkCode : "",
                        workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                        mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,

                        sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,

                        EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                        Panchayat = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatCode).FirstOrDefault(),
                        PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                        programName = item.ProgrammeName != null ? item.ProgrammeName : "",
                        ProgrammeName = ProjectName.programmeName != null ? ProjectName.programmeName : "",
                        sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                        districtID = item.DistrictId != null ? item.DistrictId : 0,
                        WardName = ConsName.ConstituencyName,
                        ExecutiveDepartmentName = workDetails != null ? getExecutiveDepartmentName(workDetails.ExecutingDepartmentId) : "",   // by me 

                        ExecutiveDepartment = workDetails != null ? workDetails.ExecutingDepartmentId : "",
                        // workDetails.ExecutingDepartmentId != null ? workDetails.ExecutingDepartmentId : "",

                        // ExecutiveOffice = Convert.ToString(workDetails.ExecutingOfficeID),
                        ExecutiveOffice = workDetails != null ? Convert.ToString(workDetails.ExecutingOfficeID) : "",
                        oWnerDepartment = workDetails != null ? workDetails.Owner_deptid : "",
                        ExecutiveOfficeName = workDetails != null ? getExecutiveOfficeName(workDetails.ExecutingOfficeID) : "",
                        physicalProgress = physicalDetails != null ? physicalDetails.toalProgress : 0,
                        financialProgress = physicalDetails != null ? physicalDetails.toalExpenditure : 0,
                        financialYear = item.financialYear != null ? item.financialYear : "",

                        CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",

                        imageCount = imageCount != null ? imageCount : 0,
                        CompletionFinancialYear = workDetails != null ? item.financialYear : "",
                        workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                        workStatusCode = item.workStatus != null ? item.workStatus : 0,
                        subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,
                        // ProgrammeName = item.ProgrammeName != null ? item.ProgrammeName:"0",
                        WorkType = item.workType != null ? item.workType : "0",
                        // WorkType = item.work
                    });
                }

                //if (OfficeId != "0")
                //{
                //    //int offc = Convert.ToInt16(OfficeId);
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.ExecutiveOffice == OfficeId).ToList();
                //}

                if (agency != "0")
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.ExecutiveDepartment == agency).ToList();
                }
                if (mOwnerDepartment != "0")
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.oWnerDepartment == mOwnerDepartment).ToList();
                }
                if (FinancialYear != "0")
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.financialYear == FinancialYear).ToList();
                }
                if (workstatus == 3)
                {
                    // scheme.mySchemeList = scheme.mySchemeList.Where(a => a.workStatus == workstatus).ToList();
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.CompletionFinancialYear == cFinancialYear).ToList();
                }
                if (Panchayat != "0")
                {
                    Int64 pnccode = Convert.ToInt64(Panchayat);
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.Panchayat == pnccode).ToList();
                }
                //if (!member)
                //{
                //    List<long> workList = (from wlist in context.ForwardGrievance
                //                           where wlist.OfficerCode == aadharID
                //                           && wlist.Type == "Work"
                //                           select wlist.GrvCode).Cast<long>().ToList();
                //    scheme.mySchemeList = scheme.mySchemeList.Where(a => workList.Contains(a.mySchemeID)).Select(a => a).ToList();
                //}
                return scheme;
            }
        }


        public static object gelFilteredMySchemeList(object param)
        {
            string[] arr = param as string[];
            string program = arr[1], ControllingAuthority = arr[3], FinancialYear = arr[4], agency = arr[5], mOwnerDepartment = arr[6],
                Panchayat = arr[7], WorkType = arr[8]; // OfficeId = arr[16];

            long constituencyID = Convert.ToInt32(arr[0]);
            long districtCode = Convert.ToInt32(arr[10]);
            //  long demandCode = Convert.ToInt64(arr[2]);
            long workstatus = Convert.ToInt64(arr[9]);
            string aadharID = arr[11];
            bool member = Convert.ToBoolean(arr[12]);
            string cFinancialYear = arr[13];
            string sanctionedDate = arr[14];
            //Int32 SubdivisionCode = Convert.ToInt32(arr[15]);
            string SubdivisionCode = arr[15];
            DateTime ? dtDate=null ;
            if (sanctionedDate != "0" && sanctionedDate != "" && sanctionedDate != null)
            {
                //DateTime ScDate = Convert.ToDateTime(sanctionedDate);
                string Date = sanctionedDate;
                Int16 dd = Convert.ToInt16(sanctionedDate.Substring(0, 2));
                Int16 mm = Convert.ToInt16(sanctionedDate.Substring(3, 2));
                Int16 yyyy = Convert.ToInt16(sanctionedDate.Substring(6, 4));
                // DateTime dtDate = new DateTime(2018, 2, 12);
                 dtDate = new DateTime(yyyy, mm, dd);
                //  model.EnclosureDate = dtDate;
                //  myresult = myresult.Where(a => a.SanctionedDate.Value.ToString("dd-MM-yyyy") == sanctionedDate).ToList();
               // myresult = myresult.Where(a => a.SanctionedDate.Value.ToString("dd-MM-yyyy") == dtDate.ToString("dd-MM-yyyy")).ToList();
            }

            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@workstatus", Convert.ToString(workstatus)));
            methodParameter.Add(new KeyValuePair<string, string>("@ControllingAuthority", Convert.ToString(ControllingAuthority)));
                     methodParameter.Add(new KeyValuePair<string, string>("@WorkType", Convert.ToString(WorkType)));
            methodParameter.Add(new KeyValuePair<string, string>("@program", Convert.ToString(program)));
            methodParameter.Add(new KeyValuePair<string, string>("@sanctionedDate", Convert.ToString(dtDate)));
            methodParameter.Add(new KeyValuePair<string, string>("@agency", Convert.ToString(agency)));
            methodParameter.Add(new KeyValuePair<string, string>("@mOwnerDepartment", Convert.ToString(mOwnerDepartment)));
            methodParameter.Add(new KeyValuePair<string, string>("@FinancialYear", Convert.ToString(FinancialYear)));
            methodParameter.Add(new KeyValuePair<string, string>("@constituencyID", Convert.ToString(constituencyID)));
            DataSet dataSetUser = new DataSet();
            dataSetUser = SBL.eLegislator.HPMS.ServiceAdaptor.ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "GetFilterWorks", methodParameter);
            List<vsSchemeList> list = new List<vsSchemeList>();
            List<myShemeList> mylist = new List<myShemeList>();
            schemeMapping scheme = new schemeMapping();

            scheme.vsSchemeList = list;
            scheme.mySchemeList = mylist;

            foreach (DataRow dr in dataSetUser.Tables[0].Rows)
            {

                mylist.Add(new myShemeList()
                {
                    workCode =Convert.ToString( dr["WorkCode"]),
                    workName = Convert.ToString(dr["workName"]),
                    mySchemeID = Convert.ToInt64(dr["mySchemeID"]),

                    sanctionedAmount = Convert.ToDouble(dr["sanctionedAmount"]),

                    EstimatedAmount = Convert.ToDouble(dr["EstimatedAmount"]),
                    Panchayat = Convert.ToInt64(dr["Panchayat"]),
                    PanchayatName = Convert.ToString(dr["PanchayatName"]),
                    programName = Convert.ToString(dr["programName"]),
                    ProgrammeName = Convert.ToString(dr["ProgrammeName"]),
                    sanctionedDate = Convert.ToString(dr["sanctionedDate"]),
             
                    districtID = Convert.ToInt64(dr["districtID"]),
                    WardName = Convert.ToString (dr["WardName"]),
                    ExecutiveDepartmentName = Convert.ToString(dr["ExecutiveDepartmentName"]),

                    ExecutiveDepartment = Convert.ToString(dr["ExecutiveDepartment"]),
                    // workDetails.ExecutingDepartmentId != null ? workDetails.ExecutingDepartmentId : "",

                    // ExecutiveOffice = Convert.ToString(workDetails.ExecutingOfficeID),
                    ExecutiveOffice = Convert.ToString(dr["ExecutiveOffice"]),
                    oWnerDepartment = Convert.ToString(dr["oWnerDepartment"]),
                    ExecutiveOfficeName = Convert.ToString(dr["ExecutiveOfficeName"]),
                    physicalProgress = Convert.ToDouble(dr["physicalProgress"]),
                    financialProgress = Convert.ToDouble(dr["financialProgress"]),
                    financialYear = Convert.ToString(dr["financialYear"]),

                    CompletionDate = Convert.ToString(dr["CompletionDate"]),

                    imageCount = Convert.ToInt32(dr["imageCount"]),
                    CompletionFinancialYear = Convert.ToString(dr["CompletionFinancialYear"]),
                    workStatus = Convert.ToString(dr["workStatus"]),
                    workStatusCode = Convert.ToInt64(dr["workStatusCode"]),
                    subdevisionId = Convert.ToInt32(dr["subdevisionId"]),
                    // ProgrammeName = item.ProgrammeName != null ? item.ProgrammeName:"0",
                    WorkType = Convert.ToString(dr["WorkType"]),
                    // WorkType = item.work
                });

            }
            return scheme;
        }

        //public static object getWorkDetailsByID(object param)
        //{
        //    long schemeID = Convert.ToInt32(param);
        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {

        //        var scheme = (from list in context.SchemeMastersConstituency
        //                      where list.schemeConstituencyID == schemeID
        //                      select list).FirstOrDefault();
        //        var currentAssembly = (from site in context.SiteSettings
        //                               where site.SettingName == "Assembly"
        //                               select site.SettingValue).FirstOrDefault();
        //        int assemblyID = Convert.ToInt32(currentAssembly);
        //        WorkDetails details = new WorkDetails()
        //        {
        //            constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == scheme.constituencyID select cons.ConstituencyName).FirstOrDefault(),
        //            ControllingAuthorityID = scheme.ControllingAuthorityId,
        //            districtID = scheme.DistrictId,
        //            eConstituencyID = scheme.constituencyID,
        //            EstimatedAmount = scheme.AmountEstimate,
        //            FinancialYear = scheme.financialYear,
        //            mySchemeID = scheme.schemeConstituencyID,
        //            Owner_deptid = scheme.DepartmentIdOwner,
        //            // Owner_deptName = "",
        //            Owner_deptName = scheme.DepartmentIdOwner,
        //            Panchayat = scheme.Panchayat,
        //            programName = scheme.ProgrammeName,
        //            sanctionedAmount = scheme.SanctionedAmount,
        //            sanctionedDate = scheme.SanctionedDate.HasValue ? scheme.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",
        //            workCode = scheme.WorkCode,
        //            // workName = scheme.workName,
        //            workName = string.IsNullOrEmpty(scheme.workName) ? scheme.WorkNameLocal : scheme.workName,
        //            WorkNatureID = scheme.workNature,
        //            WorkTypeID = scheme.workType,
        //            workStatus = scheme.workStatus.Value,
        //            revisedAmount = scheme.revisedAmount.HasValue ? scheme.revisedAmount : scheme.SanctionedAmount,
        //            Demand = scheme.DemandCode,
        //            EstimatedCompletionDate = scheme.EstimatedCompletionDate,
        //            memberCode = scheme.memberCode,
        //            dprFile = scheme.dprFileName,
        //            subdevisionId = scheme.SubdevisionId,

        //            Mode = "Save"
        //        };
        //        var workDetails = (from list in context.ConstituencySchemesBasicDetails
        //                           where list.mySchemeID == schemeID
        //                           select list).FirstOrDefault();
        //        if (workDetails != null)
        //        {
        //            details.Owner_deptid = workDetails.Owner_deptid;
        //            details.Owner_deptName = workDetails.Owner_deptName;
        //            details.programName = workDetails.programName;
        //            details.ExecutingDepartmentId = workDetails.ExecutingDepartmentId;
        //            details.ExecutingDepartmentName = string.IsNullOrEmpty(workDetails.ExecutingDepartmentId) ? "" : getExecutiveDepartmentName(workDetails.ExecutingDepartmentId);
        //            details.ExecutingOfficeID = workDetails.ExecutingOfficeID;
        //            details.ExecutingOfficeName = workDetails.ExecutingOfficeID.HasValue ? getExecutiveOfficeName(workDetails.ExecutingOfficeID) : "";
        //            details.Panchayat = workDetails.Panchayat;

        //            details.PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == workDetails.Panchayat select pan.PanchayatName).FirstOrDefault();
        //            details.ContractorName = workDetails.ContractorName;
        //            details.ContractorAddress = workDetails.ContractorAddress;
        //            details.completionDate = workDetails.completionDate;
        //            details.schemeTypeId = workDetails.SchemeTypeId;

        //            details.Mode = "Update";
        //        }

        //        var physicalDetails = (from list in context.ConstituencyWorkProgress
        //                               where list.schemeCode == schemeID
        //                               orderby list.submitDate descending
        //                               select list).FirstOrDefault();
        //        if (physicalDetails != null)
        //        {

        //            details.TotalPhysicalProgress = physicalDetails.toalProgress;
        //            details.TotalExpenditureAmount = physicalDetails.toalExpenditure;
        //        }
        //        return details;
        //    }
        //}

        //public static object getWorkDetailsByID(object param)
        //{
        //    long schemeID = Convert.ToInt32(param);
        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {
        //        var scheme = (from list in context.SchemeMastersConstituency
        //                      where list.schemeConstituencyID == schemeID
        //                      select list).FirstOrDefault();
        //        var currentAssembly = (from site in context.SiteSettings
        //                               where site.SettingName == "Assembly"
        //                               select site.SettingValue).FirstOrDefault();
        //        int assemblyID = Convert.ToInt32(currentAssembly);
        //        WorkDetails details = new WorkDetails()
        //        {
        //            constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == scheme.constituencyID select cons.ConstituencyName).FirstOrDefault(),
        //            ControllingAuthorityID = scheme.ControllingAuthorityId,
        //            districtID = scheme.DistrictId,
        //            eConstituencyID = scheme.constituencyID,
        //            EstimatedAmount = scheme.AmountEstimate,
        //            FinancialYear = scheme.financialYear,
        //            mySchemeID = scheme.schemeConstituencyID,
        //            Owner_deptid = scheme.DepartmentIdOwner,
        //           // Owner_deptName = "",
        //            Owner_deptName = scheme.DepartmentIdOwner,
        //            Panchayat = scheme.Panchayat,
        //            programName = scheme.ProgrammeName,
        //            sanctionedAmount = scheme.SanctionedAmount,
        //            sanctionedDate = scheme.SanctionedDate.HasValue ? scheme.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",
        //            workCode = scheme.WorkCode,
        //            // workName = scheme.workName,
        //            workName = string.IsNullOrEmpty(scheme.workName) ? scheme.WorkNameLocal : scheme.workName,
        //            WorkNatureID = scheme.workNature,
        //            WorkTypeID = scheme.workType,
        //            workStatus = scheme.workStatus.Value,
        //            revisedAmount = scheme.revisedAmount.HasValue ? scheme.revisedAmount : scheme.SanctionedAmount,
        //            Demand = scheme.DemandCode,
        //            EstimatedCompletionDate = scheme.EstimatedCompletionDate,
        //            memberCode = scheme.memberCode,
        //            dprFile = scheme.dprFileName,


        //            Mode = "Save"
        //        };
        //        var workDetails = (from list in context.ConstituencySchemesBasicDetails
        //                           where list.mySchemeID == schemeID
        //                           select list).FirstOrDefault();
        //        if (workDetails != null)
        //        {
        //            details.Owner_deptid = workDetails.Owner_deptid;
        //            details.Owner_deptName = workDetails.Owner_deptName;
        //            details.programName = workDetails.programName;
        //            details.ExecutingDepartmentId = workDetails.ExecutingDepartmentId;
        //            details.ExecutingDepartmentName = string.IsNullOrEmpty(workDetails.ExecutingDepartmentId) ? "" : getExecutiveDepartmentName(workDetails.ExecutingDepartmentId);
        //            details.ExecutingOfficeID = workDetails.ExecutingOfficeID;
        //            details.ExecutingOfficeName = workDetails.ExecutingOfficeID.HasValue ? getExecutiveOfficeName(workDetails.ExecutingOfficeID) : "";
        //            details.Panchayat = workDetails.Panchayat;

        //            details.PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == workDetails.Panchayat select pan.PanchayatName).FirstOrDefault();
        //            details.ContractorName = workDetails.ContractorName;
        //            details.ContractorAddress = workDetails.ContractorAddress;
        //            details.completionDate = workDetails.completionDate;
        //            details.Mode = "Update";
        //        }

        //        var physicalDetails = (from list in context.ConstituencyWorkProgress
        //                               where list.schemeCode == schemeID
        //                               orderby list.submitDate descending
        //                               select list).FirstOrDefault();
        //        if (physicalDetails != null)
        //        {

        //            details.TotalPhysicalProgress = physicalDetails.toalProgress;
        //            details.TotalExpenditureAmount = physicalDetails.toalExpenditure;
        //        }
        //        return details;
        //    }
        //}


        //public static object getWorkDetailsByIDForMember(object param)
        //{
        //    long schemeID = Convert.ToInt32(param);
        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {
        //        var scheme = (from list in context.SchemeMastersConstituency
        //                      where list.schemeConstituencyID == schemeID
        //                      select list).FirstOrDefault();
        //        var currentAssembly = (from site in context.SiteSettings
        //                               where site.SettingName == "Assembly"
        //                               select site.SettingValue).FirstOrDefault();
        //        int assemblyID = Convert.ToInt32(currentAssembly);
        //        WorkDetails details = new WorkDetails()
        //        {
        //            constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == scheme.constituencyID select cons.ConstituencyName).FirstOrDefault(),
        //            ControllingAuthorityID = scheme.ControllingAuthorityId,
        //            ControllingAuthorityName = (from con in context.mControllingAuthority where con.ControllingAuthorityID == scheme.ControllingAuthorityId select con.ControllingAuthorityName).FirstOrDefault(),
        //            districtID = scheme.DistrictId,
        //            districtName = (from con in context.Districts where con.DistrictCode == scheme.DistrictId select con.DistrictName).FirstOrDefault(),
        //            eConstituencyID = scheme.constituencyID,
        //            EstimatedAmount = scheme.AmountEstimate,
        //            FinancialYear = scheme.financialYear,
        //            mySchemeID = scheme.schemeConstituencyID,
        //            Owner_deptid = scheme.DepartmentIdOwner,
        //            //Owner_deptName = "",
        //            Owner_deptName = scheme.DepartmentIdOwner,
        //            Panchayat = scheme.Panchayat,
        //            programName = scheme.ProgrammeName,
        //            sanctionedAmount = scheme.SanctionedAmount,
        //            sanctionedDate = scheme.SanctionedDate.HasValue ? scheme.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",
        //            workCode = scheme.WorkCode,
        //            workName = scheme.workName,
        //            WorkNatureID = scheme.workNature,
        //            WorkTypeID = scheme.workType,
        //            workStatus = scheme.workStatus.Value,
        //            revisedAmount = scheme.revisedAmount.HasValue ? scheme.revisedAmount : scheme.SanctionedAmount,
        //            Demand = scheme.DemandCode,
        //            EstimatedCompletionDate = scheme.EstimatedCompletionDate,
        //            memberCode = scheme.memberCode,
        //            subdevisionId = scheme.SubdevisionId,

        //            Mode = "Save"
        //        };
        //        var workDetails = (from list in context.ConstituencySchemesBasicDetails
        //                           where list.mySchemeID == schemeID
        //                           select list).FirstOrDefault();
        //        if (workDetails != null)
        //        {
        //            details.Owner_deptid = workDetails.Owner_deptid;
        //            details.Owner_deptName = workDetails.Owner_deptName;
        //            details.programName = workDetails.programName;
        //            details.ExecutingDepartmentId = workDetails.ExecutingDepartmentId;
        //            details.ExecutingOfficeID = workDetails.ExecutingOfficeID;
        //            details.Panchayat = workDetails.Panchayat;
        //            details.schemeTypeId = workDetails.SchemeTypeId;
        //            details.PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == workDetails.Panchayat select pan.PanchayatName).FirstOrDefault();

        //            details.Mode = "Update";
        //        }

        //        var physicalDetails = (from list in context.ConstituencyWorkProgress
        //                               where list.schemeCode == schemeID
        //                               orderby list.submitDate descending
        //                               select list).FirstOrDefault();
        //        if (physicalDetails != null)
        //        {

        //            details.TotalPhysicalProgress = physicalDetails.toalProgress;


        //            details.TotalExpenditureAmount = physicalDetails.toalExpenditure;
        //        }
        //        return details;
        //    }
        //}

        ///change by Madhur-
        public static object getWorkDetailsByID(object param)
        {
            long schemeID = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {

                var scheme = (from list in context.SchemeMastersConstituency
                              where list.schemeConstituencyID == schemeID
                              select list).FirstOrDefault();
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);
                WorkDetails details = new WorkDetails()
                {
                    constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == scheme.constituencyID select cons.ConstituencyName).FirstOrDefault(),
                    ControllingAuthorityID = scheme.ControllingAuthorityId,
                    districtID = scheme.DistrictId,
                    eConstituencyID = scheme.constituencyID,
                    EstimatedAmount = scheme.AmountEstimate,
                    FinancialYear = scheme.financialYear,
                    mySchemeID = scheme.schemeConstituencyID,
                    Owner_deptid = scheme.DepartmentIdOwner,
                    completionDate = (from cons in context.ConstituencySchemesBasicDetails where cons.mySchemeID == schemeID select cons.completionDate).FirstOrDefault(),
                    // Owner_deptName = "",
                    Owner_deptName = scheme.DepartmentIdOwner,
                    Panchayat = scheme.Panchayat,
                    programName = scheme.ProgrammeName,
                    sanctionedAmount = scheme.SanctionedAmount,
                    sanctionedDate = scheme.SanctionedDate.HasValue ? scheme.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",
                    workCode = scheme.WorkCode,
                    // workName = scheme.workName,
                    workName = string.IsNullOrEmpty(scheme.workName) ? scheme.WorkNameLocal : scheme.workName,
                    WorkNatureID = scheme.workNature,
                    WorkTypeID = scheme.workType,
                    workStatus = scheme.workStatus.Value,
                    revisedAmount = scheme.revisedAmount.HasValue ? scheme.revisedAmount : scheme.SanctionedAmount,
                    Demand = scheme.DemandCode,
                    EstimatedCompletionDate = scheme.EstimatedCompletionDate,
                    memberCode = scheme.memberCode,
                    dprFile = scheme.dprFileName,
                    featuresfile = scheme.SalientFeatures,
                    subdevisionId = scheme.SubdevisionId,

                    Mode = "Save"
                };
                var workDetails = (from list in context.ConstituencySchemesBasicDetails
                                   where list.mySchemeID == schemeID
                                   select list).FirstOrDefault();
                if (workDetails != null)
                {
                    details.Owner_deptid = workDetails.Owner_deptid;
                    details.Owner_deptName = workDetails.Owner_deptName;
                    details.programName = workDetails.programName;
                    details.ExecutingDepartmentId = workDetails.ExecutingDepartmentId;
                    details.ExecutingDepartmentName = string.IsNullOrEmpty(workDetails.ExecutingDepartmentId) ? "" : getExecutiveDepartmentName(workDetails.ExecutingDepartmentId);
                    details.ExecutingOfficeID = workDetails.ExecutingOfficeID;
                    details.ExecutingOfficeName = workDetails.ExecutingOfficeID.HasValue ? getExecutiveOfficeName(workDetails.ExecutingOfficeID) : "";
                    details.Panchayat = workDetails.Panchayat;

                    details.PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == workDetails.Panchayat select pan.PanchayatName).FirstOrDefault();
                    details.ContractorName = workDetails.ContractorName;
                    details.ContractorAddress = workDetails.ContractorAddress;
                    //  scheme.EstimatedCompletionDate = workDetails.completionDate;
                    details.schemeTypeId = workDetails.SchemeTypeId;
                    //  details.EstimatedAmount = workDetails.EstimatedAmount;
                    details.EstimatedAmount = scheme.AmountEstimate;

                    details.Mode = "Update";
                }

                var physicalDetails = (from list in context.ConstituencyWorkProgress
                                       where list.schemeCode == schemeID
                                       orderby list.submitDate descending
                                       select list).FirstOrDefault();
                if (physicalDetails != null)
                {

                    details.TotalPhysicalProgress = physicalDetails.toalProgress;
                    details.TotalExpenditureAmount = physicalDetails.toalExpenditure;
                }
                return details;
            }
        }

        ///change by Madhur-

        public static object getWorkDetailsByIDForMember(object param)
        {
            long schemeID = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var scheme = (from list in context.SchemeMastersConstituency
                              where list.schemeConstituencyID == schemeID
                              select list).FirstOrDefault();
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);
                WorkDetails details = new WorkDetails()
                {
                    constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == scheme.constituencyID select cons.ConstituencyName).FirstOrDefault(),
                    ControllingAuthorityID = scheme.ControllingAuthorityId,
                    ControllingAuthorityName = (from con in context.mControllingAuthority where con.ControllingAuthorityID == scheme.ControllingAuthorityId select con.ControllingAuthorityName).FirstOrDefault(),
                    districtID = scheme.DistrictId,
                    districtName = (from con in context.Districts where con.DistrictCode == scheme.DistrictId select con.DistrictName).FirstOrDefault(),
                    eConstituencyID = scheme.constituencyID,
                    EstimatedAmount = scheme.AmountEstimate,
                    FinancialYear = scheme.financialYear,
                    mySchemeID = scheme.schemeConstituencyID,
                    Owner_deptid = scheme.DepartmentIdOwner,
                    //Owner_deptName = "",
                    Owner_deptName = scheme.DepartmentIdOwner,
                    Panchayat = scheme.Panchayat,
                    programName = scheme.ProgrammeName,
                    sanctionedAmount = scheme.SanctionedAmount,
                    sanctionedDate = scheme.SanctionedDate.HasValue ? scheme.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",
                    workCode = scheme.WorkCode,
                    workName = scheme.workName,
                    WorkNatureID = scheme.workNature,
                    WorkTypeID = scheme.workType,
                    workStatus = scheme.workStatus.Value,
                    revisedAmount = scheme.revisedAmount.HasValue ? scheme.revisedAmount : scheme.SanctionedAmount,
                    Demand = scheme.DemandCode,
                    EstimatedCompletionDate = scheme.EstimatedCompletionDate,
                    memberCode = scheme.memberCode,
                    subdevisionId = scheme.SubdevisionId,

                    Mode = "Save"
                };
                var workDetails = (from list in context.ConstituencySchemesBasicDetails
                                   where list.mySchemeID == schemeID
                                   select list).FirstOrDefault();
                if (workDetails != null)
                {
                    details.Owner_deptid = workDetails.Owner_deptid;
                    details.Owner_deptName = workDetails.Owner_deptName;
                    details.programName = workDetails.programName;
                    details.ExecutingDepartmentId = workDetails.ExecutingDepartmentId;
                    details.ExecutingOfficeID = workDetails.ExecutingOfficeID;
                    details.Panchayat = workDetails.Panchayat;
                    details.schemeTypeId = workDetails.SchemeTypeId;
                    details.PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == workDetails.Panchayat select pan.PanchayatName).FirstOrDefault();

                    details.Mode = "Update";
                }

                var physicalDetails = (from list in context.ConstituencyWorkProgress
                                       where list.schemeCode == schemeID
                                       orderby list.submitDate descending
                                       select list).FirstOrDefault();
                if (physicalDetails != null)
                {

                    details.TotalPhysicalProgress = physicalDetails.toalProgress;


                    details.TotalExpenditureAmount = physicalDetails.toalExpenditure;
                }
                return details;
            }
        }

        //public static object getWorkDetailsByIDForMember(object param)
        //{
        //    long schemeID = Convert.ToInt32(param);
        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {
        //        var scheme = (from list in context.SchemeMastersConstituency
        //                      where list.schemeConstituencyID == schemeID
        //                      select list).FirstOrDefault();
        //        var currentAssembly = (from site in context.SiteSettings
        //                               where site.SettingName == "Assembly"
        //                               select site.SettingValue).FirstOrDefault();
        //        int assemblyID = Convert.ToInt32(currentAssembly);
        //        WorkDetails details = new WorkDetails()
        //        {
        //            constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == scheme.constituencyID select cons.ConstituencyName).FirstOrDefault(),
        //            ControllingAuthorityID = scheme.ControllingAuthorityId,
        //            ControllingAuthorityName = (from con in context.mControllingAuthority where con.ControllingAuthorityID == scheme.ControllingAuthorityId select con.ControllingAuthorityName).FirstOrDefault(),
        //            districtID = scheme.DistrictId,
        //            districtName = (from con in context.Districts where con.DistrictCode == scheme.DistrictId select con.DistrictName).FirstOrDefault(),
        //            eConstituencyID = scheme.constituencyID,
        //            EstimatedAmount = scheme.AmountEstimate,
        //            FinancialYear = scheme.financialYear,
        //            mySchemeID = scheme.schemeConstituencyID,
        //            Owner_deptid = scheme.DepartmentIdOwner,
        //            //Owner_deptName = "",
        //            Owner_deptName = scheme.DepartmentIdOwner,
        //            Panchayat = scheme.Panchayat,
        //            programName = scheme.ProgrammeName,
        //            sanctionedAmount = scheme.SanctionedAmount,
        //            sanctionedDate = scheme.SanctionedDate.HasValue ? scheme.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",
        //            workCode = scheme.WorkCode,
        //            workName = scheme.workName,
        //            WorkNatureID = scheme.workNature,
        //            WorkTypeID = scheme.workType,
        //            workStatus = scheme.workStatus.Value,
        //            revisedAmount = scheme.revisedAmount.HasValue ? scheme.revisedAmount : scheme.SanctionedAmount,
        //            Demand = scheme.DemandCode,
        //            EstimatedCompletionDate = scheme.EstimatedCompletionDate,
        //            memberCode = scheme.memberCode,

        //            Mode = "Save"
        //        };
        //        var workDetails = (from list in context.ConstituencySchemesBasicDetails
        //                           where list.mySchemeID == schemeID
        //                           select list).FirstOrDefault();
        //        if (workDetails != null)
        //        {
        //            details.Owner_deptid = workDetails.Owner_deptid;
        //            details.Owner_deptName = workDetails.Owner_deptName;
        //            details.programName = workDetails.programName;
        //            details.ExecutingDepartmentId = workDetails.ExecutingDepartmentId;
        //            details.ExecutingOfficeID = workDetails.ExecutingOfficeID;
        //            details.Panchayat = workDetails.Panchayat;

        //            details.PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == workDetails.Panchayat select pan.PanchayatName).FirstOrDefault();

        //            details.Mode = "Update";
        //        }

        //        var physicalDetails = (from list in context.ConstituencyWorkProgress
        //                               where list.schemeCode == schemeID
        //                               orderby list.submitDate descending
        //                               select list).FirstOrDefault();
        //        if (physicalDetails != null)
        //        {

        //            details.TotalPhysicalProgress = physicalDetails.toalProgress;


        //            details.TotalExpenditureAmount = physicalDetails.toalExpenditure;
        //        }
        //        return details;
        //    }
        //}

        //public static object updateWorksbyMembers(object param)
        //{
        //    WorkDetails work = param as WorkDetails;
        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {
        //        ConstituencySchemesBasicDetails details = new ConstituencySchemesBasicDetails()
        //        {
        //            mySchemeID = work.mySchemeID,
        //            Owner_deptid = work.Owner_deptid,
        //            Owner_deptName = work.Owner_deptName,
        //            programName = work.programName,
        //            ExecutingDepartmentId = work.ExecutingDepartmentId,
        //            ExecutingAgencyID = work.ExecutingDepartmentId,
        //            ExecutingOfficeID = work.ExecutingOfficeID,
        //            Panchayat = work.Panchayat
        //        };
        //        if (work.Mode == "Save")
        //        {
        //            context.ConstituencySchemesBasicDetails.Add(details);
        //            context.SaveChanges();
        //        }
        //        else
        //        {
        //            var workDetails = (from list in context.ConstituencySchemesBasicDetails
        //                               where list.mySchemeID == work.mySchemeID
        //                               select list).FirstOrDefault();

        //            workDetails.Owner_deptid = work.Owner_deptid;
        //            workDetails.Owner_deptName = work.Owner_deptName;
        //            workDetails.programName = work.programName;
        //            workDetails.ExecutingDepartmentId = work.ExecutingDepartmentId;
        //            workDetails.ExecutingAgencyID = work.ExecutingOfficeID.Value.ToString();
        //            workDetails.Panchayat = work.Panchayat;
        //            workDetails.ExecutingOfficeID = work.ExecutingOfficeID;
        //            workDetails.ContractorAddress = work.ContractorAddress;
        //            workDetails.ContractorName = work.ContractorName;
        //            workDetails.completionDate = work.completionDate;
        //            workDetails.completionFinancialYear = GetFinancialYear(work.completionDate);
        //            context.Entry(workDetails).State = EntityState.Modified;
        //            context.SaveChanges();
        //        }
        //        var works = (from list in context.SchemeMastersConstituency
        //                     where list.schemeConstituencyID == work.mySchemeID
        //                     select list).FirstOrDefault();

        //        works.ControllingAuthorityId = work.ControllingAuthorityID;
        //        works.financialYear = work.FinancialYear;
        //        works.modifiedBY = work.userid;
        //        works.ModifiedDate = DateTime.Now;
        //        works.Panchayat = work.Panchayat;
        //        works.ProgrammeName = work.programName;
        //        works.SanctionedAmount = work.sanctionedAmount;
        //        works.revisedAmount = work.revisedAmount;
        //        if (work.sanctionedDate != null && (!string.IsNullOrEmpty(work.sanctionedDate)))
        //        {
        //            try
        //            {
        //                works.SanctionedDate = Convert.ToDateTime(work.sanctionedDate);
        //            }
        //            catch
        //            {
        //            }
        //        }
        //        works.workNature = work.WorkNatureID;
        //        works.workStatus = work.workStatus;
        //        works.workType = work.WorkTypeID;
        //        works.DemandCode = work.Demand;
        //        works.EstimatedCompletionDate = work.EstimatedCompletionDate;
        //        works.memberCode = work.memberCode;

        //        works.dprFileName = string.IsNullOrEmpty(work.dprFile) ? works.dprFileName : work.dprFile;


        //        context.Entry(works).State = EntityState.Modified;
        //        context.SaveChanges();
        //        workTrans workt = new workTrans();
        //        workt.memberCode = work.memberCode.HasValue ? work.memberCode.Value : 0;
        //        workt.read = false;
        //        workt.Remarks = "Work Update";
        //        workt.schemeID = work.mySchemeID;
        //        workt.SubmitTime = DateTime.Now;
        //        workt.isMember = work.isMember;
        //        workt.userID = new Guid(work.userid);
        //        inserWorkTrans(workt);

        //        context.Close();
        //        return null;
        //    }
        //}


        //public static object updateWorksbyMembers(object param)
        //{
        //    WorkDetails work = param as WorkDetails;
        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {
        //        ConstituencySchemesBasicDetails details = new ConstituencySchemesBasicDetails()
        //        {
        //            mySchemeID = work.mySchemeID,
        //            Owner_deptid = work.Owner_deptid,
        //            Owner_deptName = work.Owner_deptName,
        //            programName = work.programName,
        //            ExecutingDepartmentId = work.ExecutingDepartmentId,
        //            ExecutingAgencyID = work.ExecutingDepartmentId,
        //            ExecutingOfficeID = work.ExecutingOfficeID,
        //            Panchayat = work.Panchayat,
        //            SchemeTypeId = work.schemeTypeId,
        //        };
        //        if (work.Mode == "Save")
        //        {
        //            context.ConstituencySchemesBasicDetails.Add(details);
        //            context.SaveChanges();
        //        }
        //        else
        //        {
        //            var workDetails = (from list in context.ConstituencySchemesBasicDetails
        //                               where list.mySchemeID == work.mySchemeID
        //                               select list).FirstOrDefault();

        //            workDetails.Owner_deptid = work.Owner_deptid;
        //            workDetails.Owner_deptName = work.Owner_deptName;
        //            workDetails.programName = work.programName;
        //            workDetails.ExecutingDepartmentId = work.ExecutingDepartmentId;
        //            workDetails.ExecutingAgencyID = work.ExecutingOfficeID.Value.ToString();
        //            workDetails.Panchayat = work.Panchayat;
        //            workDetails.ExecutingOfficeID = work.ExecutingOfficeID;
        //            workDetails.ContractorAddress = work.ContractorAddress;
        //            workDetails.ContractorName = work.ContractorName;
        //            workDetails.completionDate = work.completionDate;
        //            workDetails.completionFinancialYear = GetFinancialYear(work.completionDate);
        //            workDetails.SchemeTypeId = work.schemeTypeId;
        //            context.Entry(workDetails).State = EntityState.Modified;
        //            context.SaveChanges();
        //        }
        //        var works = (from list in context.SchemeMastersConstituency
        //                     where list.schemeConstituencyID == work.mySchemeID
        //                     select list).FirstOrDefault();

        //        works.ControllingAuthorityId = work.ControllingAuthorityID;
        //        works.financialYear = work.FinancialYear;
        //        works.modifiedBY = work.userid;
        //        works.ModifiedDate = DateTime.Now;
        //        works.Panchayat = work.Panchayat;
        //        works.ProgrammeName = work.programName;
        //        works.SanctionedAmount = work.sanctionedAmount;
        //        works.revisedAmount = work.revisedAmount;
        //        works.SubdevisionId = work.subdevisionId;
        //        works.AssemblyId = work.AssemblyId;
        //        works.StateCode = work.StateCode;
        //        works.workName = work.workName;
        //        if (work.sanctionedDate != null && (!string.IsNullOrEmpty(work.sanctionedDate)))
        //        {
        //            try
        //            {
        //                works.SanctionedDate = Convert.ToDateTime(work.sanctionedDate);
        //            }
        //            catch
        //            {
        //            }
        //        }
        //        works.workNature = work.WorkNatureID;
        //        works.workStatus = work.workStatus;
        //        works.workType = work.WorkTypeID;
        //        works.DemandCode = work.Demand;
        //        works.EstimatedCompletionDate = work.EstimatedCompletionDate;
        //        works.memberCode = work.memberCode;

        //        works.dprFileName = string.IsNullOrEmpty(work.dprFile) ? works.dprFileName : work.dprFile;


        //        context.Entry(works).State = EntityState.Modified;
        //        context.SaveChanges();
        //        workTrans workt = new workTrans();
        //        workt.memberCode = work.memberCode.HasValue ? work.memberCode.Value : 0;
        //        workt.read = false;
        //        workt.Remarks = "Work Update";
        //        workt.schemeID = work.mySchemeID;
        //        workt.SubmitTime = DateTime.Now;
        //        workt.isMember = work.isMember;
        //        workt.userID = new Guid(work.userid);
        //        inserWorkTrans(workt);

        //        context.Close();
        //        return null;
        //    }
        //}

        public static object updateWorksbyMembers
            (object param)
        {
            WorkDetails work = param as WorkDetails;
            using (ConstituencyContext context = new ConstituencyContext())
            {
                ConstituencySchemesBasicDetails details = new ConstituencySchemesBasicDetails()
                {
                    mySchemeID = work.mySchemeID,
                    Owner_deptid = work.Owner_deptid,
                    Owner_deptName = work.Owner_deptName,
                    programName = work.programName,
                    ExecutingDepartmentId = work.ExecutingDepartmentId,
                    ExecutingAgencyID = work.ExecutingDepartmentId,
                    ExecutingOfficeID = work.ExecutingOfficeID,
                    Panchayat = work.Panchayat,
                    EstimatedAmount = work.EstimatedAmount,
                    SchemeTypeId = work.schemeTypeId,

                };
                if (work.Mode == "Save")
                {
                    context.ConstituencySchemesBasicDetails.Add(details);
                    context.SaveChanges();
                }
                else
                {
                    var workDetails = (from list in context.ConstituencySchemesBasicDetails
                                       where list.mySchemeID == work.mySchemeID
                                       select list).FirstOrDefault();

                    workDetails.Owner_deptid = work.Owner_deptid;
                    workDetails.Owner_deptName = work.Owner_deptName;
                    workDetails.programName = work.programName;
                    workDetails.ExecutingDepartmentId = work.ExecutingDepartmentId;
                    workDetails.ExecutingAgencyID = work.ExecutingOfficeID.Value.ToString();
                    workDetails.Panchayat = work.Panchayat;
                    workDetails.ExecutingOfficeID = work.ExecutingOfficeID;
                    workDetails.ContractorAddress = work.ContractorAddress;
                    workDetails.ContractorName = work.ContractorName;
                    workDetails.completionDate = work.completionDate;
                    if (work.completionDate != null && (!string.IsNullOrEmpty(work.completionDate)))
                    {
                        try
                        {
                            workDetails.CompletedDate = Convert.ToDateTime(work.completionDate);
                        }
                        catch
                        {
                            Console.WriteLine(String.Format("Hey, {0} is not a valid DateTime!", work.completionDate));
                        }
                    }
                    workDetails.completionFinancialYear = GetFinancialYear(work.completionDate);
                    workDetails.SchemeTypeId = work.schemeTypeId;
                    workDetails.EstimatedAmount = work.EstimatedAmount;
                    context.Entry(workDetails).State = EntityState.Modified;
                    context.SaveChanges();
                }
                var works = (from list in context.SchemeMastersConstituency
                             where list.schemeConstituencyID == work.mySchemeID
                             select list).FirstOrDefault();

                works.ControllingAuthorityId = work.ControllingAuthorityID;
                works.financialYear = work.FinancialYear;
                works.modifiedBY = work.userid;
                works.ModifiedDate = DateTime.Now;
                works.Panchayat = work.Panchayat;
                works.ProgrammeName = work.programName;
                works.SanctionedAmount = work.sanctionedAmount;
                works.revisedAmount = work.revisedAmount;
                works.SubdevisionId = work.subdevisionId;
                works.AssemblyId = work.AssemblyId;
                works.StateCode = work.StateCode;
                works.workName = work.workName;

                if (work.sanctionedDate != null && (!string.IsNullOrEmpty(work.sanctionedDate)))
                {
                    try
                    {
                        works.SanctionedDate = Convert.ToDateTime(work.sanctionedDate);
                    }
                    catch
                    {
                    }
                }
                works.workNature = work.WorkNatureID;
                works.workStatus = work.workStatus;
                works.workType = work.WorkTypeID;
                works.WorkCode = work.workCode;
                works.DemandCode = work.Demand;
                works.EstimatedCompletionDate = work.EstimatedCompletionDate;
                works.memberCode = work.memberCode;
                works.constituencyID = work.eConstituencyID.Value;
                works.dprFileName = string.IsNullOrEmpty(work.dprFile) ? works.dprFileName : work.dprFile;
                works.SalientFeatures = string.IsNullOrEmpty(work.featuresfile) ? works.SalientFeatures : work.featuresfile;
                works.AmountEstimate = work.EstimatedAmount;
                works.DepartmentIdOwner = work.Owner_deptid;
                works.financialYearID = work.FinancialYearID;
                context.Entry(works).State = EntityState.Modified;
                context.SaveChanges();
                workTrans workt = new workTrans();
                workt.memberCode = work.memberCode.HasValue ? work.memberCode.Value : 0;
                workt.read = false;
                workt.Remarks = "Work Update";
                workt.schemeID = work.mySchemeID;
                workt.SubmitTime = DateTime.Now;
                workt.isMember = work.isMember;
                workt.userID = new Guid(work.userid);
                inserWorkTrans(workt);

                context.Close();
                return null;
            }
        }



        public static string GetFinancialYear(string cdate)
        {
            string finyear = "";
            if (string.IsNullOrEmpty(cdate))
            {
                return finyear;
            }
            DateTime dt = new DateTime(Convert.ToInt32(cdate.Split('-')[2]), Convert.ToInt32(cdate.Split('-')[1]), Convert.ToInt32(cdate.Split('-')[0]));
            int m = dt.Month;
            int y = dt.Year;
            if (m > 3)
            {
                finyear = y.ToString() + "-" + Convert.ToString((y + 1));
                //get last  two digits (eg: 10 from 2010);
            }
            else
            {
                finyear = Convert.ToString((y - 1)) + "-" + y.ToString();
            }
            return finyear;
        }
        //public static object CreateNewWorkbyMembers(object param)
        //{
        //    WorkDetails work = param as WorkDetails;

        //   //For Sanctioned Date
        //    int day = (Convert.ToInt32(work.sanctionedDate.Substring(0, 2)));
        //    int month = (Convert.ToInt32(work.sanctionedDate.Substring(3, 2)));
        //    int year = (Convert.ToInt32(work.sanctionedDate.Substring(6, 4)));
        //    DateTime dtsanctionedDateSelected = new DateTime(year, month, day);


        //    using (ConstituencyContext context = new ConstituencyContext())
        //    {
        //        SchemeMastersConstituency con = new SchemeMastersConstituency()
        //        {
        //            AmountEstimate = work.EstimatedAmount,
        //            ControllingAuthorityId = work.ControllingAuthorityID,
        //            createdBY = work.userid,
        //            createdDate = DateTime.Now,
        //            DemandCode = 0,
        //            DepartmentIdOwner = work.Owner_deptid,
        //            DistrictId = work.districtID.Value,
        //            financialYear = work.FinancialYear,
        //            financialYearID = work.FinancialYearID,
        //            modifiedBY = null,
        //            ModifiedDate = DateTime.Now,
        //            ProgrammeName = work.programName,
        //            SanctionedAmount = work.sanctionedAmount,
        //            SanctionedDate = dtsanctionedDateSelected,
        //            constituencyID = work.eConstituencyID.Value,
        //            TotalAmountReleased = 0,
        //            WorkCode = work.workCode,
        //            workName = work.workName,
        //            WorkNameLocal = work.workName,
        //            dprFileName = work.dprFile,
        //            workStatus=work.workStatus,
        //            EstimatedCompletionDate = work.EstimatedCompletionDate,
        //            Panchayat=work.Panchayat,
        //            workType=work.WorkType,
        //            workNature=work.WorkNatureID
        //        };

        //        context.SchemeMastersConstituency.Add(con);
        //        context.SaveChanges();
        //        //con.WorkCode = con.financialYear.Split('-')[0] + "-" + con.DepartmentIdOwner + "-" + con.schemeConstituencyID;
        //        work.mySchemeID =  con.schemeConstituencyID;
        //        work.Panchayat = con.Panchayat;
        //        ConstituencySchemesBasicDetails details = new ConstituencySchemesBasicDetails()
        //        {
        //            mySchemeID = work.mySchemeID,
        //            Owner_deptid = work.Owner_deptName,
        //            Owner_deptName = work.Owner_deptName,
        //            programName = work.programName,
        //            ExecutingDepartmentId = work.ExecutingDepartmentId,
        //            ExecutingAgencyID = work.ExecutingDepartmentId,
        //            ExecutingOfficeID=work.ExecutingOfficeID,
        //            Panchayat = work.Panchayat
        //        };


        //        //ConstituencySchemesBasicDetails details = new ConstituencySchemesBasicDetails()
        //        //{
        //        //    mySchemeID = work.mySchemeID,
        //        //    Owner_deptid = work.Owner_deptName,
        //        //    Owner_deptName = work.Owner_deptName,
        //        //    programName = work.programName,
        //        //    ExecutingDepartmentId = work.ExecutingDepartmentId,
        //        //    ExecutingAgencyID = work.ExecutingDepartmentId,
        //        //    Panchayat = work.Panchayat
        //        //};
        //        context.ConstituencySchemesBasicDetails.Add(details);
        //        context.SaveChanges();
        //        context.Close();
        //        return null;
        //    }
        //}

        public static object CreateNewWorkbyMembers(object param)
        {
            WorkDetails work = param as WorkDetails;

            //For Sanctioned Date
            DateTime dtsanctionedDateSelected = DateTime.Now;
            if (work.sanctionedDate != null)
            {
                int day = (Convert.ToInt32(work.sanctionedDate.Substring(0, 2)));
                int month = (Convert.ToInt32(work.sanctionedDate.Substring(3, 2)));
                int year = (Convert.ToInt32(work.sanctionedDate.Substring(6, 4)));
                dtsanctionedDateSelected = new DateTime(year, month, day);
            }


            using (ConstituencyContext context = new ConstituencyContext())
            {
                SchemeMastersConstituency con = new SchemeMastersConstituency()
                {
                    AmountEstimate = work.EstimatedAmount,
                    ControllingAuthorityId = work.ControllingAuthorityID,
                    createdBY = work.userid,
                    createdDate = DateTime.Now,
                    DemandCode = work.Demand,
                    DepartmentIdOwner = work.Owner_deptid,
                    DistrictId = work.districtID.Value,
                    financialYear = work.FinancialYear,
                    financialYearID = work.FinancialYearID,
                    modifiedBY = null,
                    ModifiedDate = DateTime.Now,
                    ProgrammeName = work.programName,
                    SanctionedAmount = work.sanctionedAmount,
                    SanctionedDate = dtsanctionedDateSelected,
                    constituencyID = work.eConstituencyID.Value,
                    TotalAmountReleased = 0,
                    WorkCode = work.workCode.Trim(),
                    workName = work.workName,
                    WorkNameLocal = work.workName,
                    dprFileName = work.dprFile,
                    SalientFeatures = work.featuresfile,
                    workStatus = work.workStatus,
                    EstimatedCompletionDate = work.EstimatedCompletionDate,
                    Panchayat = work.Panchayat,
                    workType = work.WorkTypeID,// work.WorkType,
                    workNature = work.WorkNatureID,
                    SubdevisionId = work.subdevisionId,
                    AssemblyId = work.AssemblyId,
                    StateCode = work.StateCode,
                };

                context.SchemeMastersConstituency.Add(con);
                context.SaveChanges();
                work.mySchemeID = con.schemeConstituencyID;
                work.Panchayat = con.Panchayat;
                ConstituencySchemesBasicDetails details = new ConstituencySchemesBasicDetails()
                {
                    SchemeTypeId = work.schemeTypeId,
                    mySchemeID = work.mySchemeID,
                    Owner_deptid = work.Owner_deptid, // work.Owner_deptName,
                    Owner_deptName = work.Owner_deptName,
                    programName = work.programName,
                    ExecutingDepartmentId = work.ExecutingDepartmentId,
                    ExecutingAgencyID = work.ExecutingDepartmentId,
                    ExecutingOfficeID = work.ExecutingOfficeID,
                    Panchayat = work.Panchayat,
                    ContractorName = work.ContractorName,//added by robin as new reqirement for 2 cases completed and progress
                    ContractorAddress = work.ContractorAddress,//added by robin as new reqirement
                    completionDate = work.completionDate//added by robin as new reqirement for completed case
                };

                context.ConstituencySchemesBasicDetails.Add(details);
                context.SaveChanges();
                context.Close();
                return null;
            }
        }
        public static object UpdateImagesbyOfficer(object param)
        {
            ConstituencyWorkProgressImages images = param as ConstituencyWorkProgressImages;
            using (ConstituencyContext context = new ConstituencyContext())
            {
                context.ConstituencyWorkProgressImages.Add(images);
                context.SaveChanges();
                context.Close();
                workTrans workt = new workTrans();
                workt.memberCode = 0;
                workt.read = false;
                workt.Remarks = "Image Update";
                workt.schemeID = images.schemeCode;
                workt.SubmitTime = DateTime.Now;
                workt.isMember = false;
                workt.userID = new Guid(images.createdBY);
                inserWorkTrans(workt);
                return null;
            }
        }

        public static object getAllProgressImages(object param)
        {
            long schemeID = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.ConstituencyWorkProgressImages
                        where list.schemeCode == schemeID
                        // orderby list.UploadedDate descending
                        orderby list.autoID descending
                        select list).ToList();
            }
        }

        public static object UpdateProgressbyOfficer(object param)
        {
            ConstituencyWorkProgress images = param as ConstituencyWorkProgress;
            using (ConstituencyContext context = new ConstituencyContext())
            {

                images.toalProgress = images.toalProgress;
                //images.totalfundreleased = images.totalfundreleased;
                images.toalExpenditure = images.toalExpenditure;
                images.RemarkProgress = images.RemarkProgress;
                context.ConstituencyWorkProgress.Add(images);
                context.SaveChanges();
                context.Close();
                workTrans workt = new workTrans();
                workt.memberCode = 0;
                workt.read = false;
                workt.Remarks = "Physical and Financial Progress Update";
                workt.schemeID = images.schemeCode;
                workt.SubmitTime = DateTime.Now;
                workt.isMember = false;
                workt.userID = new Guid(images.createdBY);
                inserWorkTrans(workt);
                return null;
            }
        }

        public static object viewAllProgressByMember(object param)
        {
            long schemeID = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.ConstituencyWorkProgress
                        where list.schemeCode == schemeID
                        orderby list.submitDate descending
                        select list).ToList();
            }
        }

        public static object UpdateExecutingDetails(object param)
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                ConstituencySchemesBasicDetails details = param as ConstituencySchemesBasicDetails;
                if (details.Mode.ToLower() == "all")
                {
                    long[] schemeIDs = details.schemeList.Split(',').Select(long.Parse).ToArray();
                    foreach (long item in schemeIDs)
                    {
                        var check = (from list in context.ConstituencySchemesBasicDetails
                                     where list.mySchemeID == item
                                     select list).FirstOrDefault();
                        if (check != null)
                        {
                            check.ExecutingOfficeID = details.ExecutingOfficeID;
                            check.ExecutingAgencyID = details.ExecutingAgencyID;
                            check.ExecutingDepartmentId = details.ExecutingDepartmentId;
                            context.Entry(check).State = EntityState.Modified;
                            context.SaveChanges();
                        }
                        else
                        {
                            ConstituencySchemesBasicDetails cdetails = new ConstituencySchemesBasicDetails()
                            {
                                mySchemeID = item,
                                Owner_deptid = details.Owner_deptid,
                                Owner_deptName = details.Owner_deptName,
                                programName = details.programName,
                                ExecutingDepartmentId = details.ExecutingDepartmentId,
                                ExecutingAgencyID = details.ExecutingDepartmentId,
                                ExecutingOfficeID = details.ExecutingOfficeID,
                                Panchayat = details.Panchayat
                            };

                            context.ConstituencySchemesBasicDetails.Add(cdetails);
                            context.SaveChanges();
                        }
                    }
                }
                else
                {
                    long schemeID = Convert.ToInt32(details.mySchemeID);
                    var check = (from list in context.ConstituencySchemesBasicDetails
                                 where list.mySchemeID == schemeID
                                 select list).FirstOrDefault();
                    if (check != null)
                    {
                        check.ExecutingOfficeID = details.ExecutingOfficeID;
                        check.ExecutingAgencyID = details.ExecutingAgencyID;
                        check.ExecutingDepartmentId = details.ExecutingDepartmentId;
                        context.Entry(check).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                    else
                    {
                        ConstituencySchemesBasicDetails cdetails = new ConstituencySchemesBasicDetails()
                        {
                            mySchemeID = schemeID,
                            Owner_deptid = details.Owner_deptid,
                            Owner_deptName = details.Owner_deptName,
                            programName = details.programName,
                            ExecutingDepartmentId = details.ExecutingDepartmentId,
                            ExecutingAgencyID = details.ExecutingDepartmentId,
                            ExecutingOfficeID = details.ExecutingOfficeID,
                            Panchayat = details.Panchayat
                        };
                        context.ConstituencySchemesBasicDetails.Add(cdetails);
                        context.SaveChanges();
                    }
                }
                return null;
            }
        }


        public static object get_subdevision(object param)
        {
            int ConstituteID = Convert.ToInt32(param.ToString());
            using (ConstituencyContext context = new ConstituencyContext())
            {
                if (ConstituteID == 0)
                {
                    return (from list in context.mSubDivision

                            orderby list.ConstituencyID ascending
                            select new fillListGenricInt
                            {
                                value = list.mSubDivisionId,
                                Text = list.SubDivisionName
                            }
                    ).ToList();
                }
                else
                {

                    return (from list in context.mSubDivision
                            where list.ConstituencyID == ConstituteID
                            orderby list.ConstituencyID ascending
                            select new fillListGenricInt
                            {
                                value = list.mSubDivisionId,
                                Text = list.SubDivisionName
                            }
                    ).ToList();
                }


            }
        }

        public static object get_subdevision_SDM(object param)
        {
            List<long> Subdivision = param.ToString().Split(',').Select(Int64.Parse).ToList();
            //  int Subdivision = Convert.ToInt32(param.ToString());
            using (ConstituencyContext context = new ConstituencyContext())
            {
                return (from list in context.mSubDivision
                        where Subdivision.Contains(list.mSubDivisionId)
                        // where list.ConstituencyID == ConstituteID
                        orderby list.ConstituencyID ascending
                        select new fillListGenricInt
                        {
                            value = list.mSubDivisionId,
                            Text = list.SubDivisionName
                        }
                         ).ToList();

            }
        }
        public static object get_SchemeType()
        {

            using (ConstituencyContext context = new ConstituencyContext())
            {

                return (from list in context.mSchemeType
                        orderby list.mSchemeId ascending
                        select new fillListGenricInt
                        {
                            value = list.mSchemeId,
                            Text = list.SchemeName
                        }
                         ).ToList();
            }
        }

        public static string CheckWorkCode_ByCID(object param)
        {
            WorkDetails mod = param as WorkDetails;
            ConstituencyContext context = new ConstituencyContext();
            var data_ = (from data in context.SchemeMastersConstituency
                         where data.WorkCode == mod.workCode
                         // && data.WorkCode == mod.workCode
                         // where data.ControllingAuthorityId == mod.ControllingAuthorityID
                         // && data.WorkCode == mod.workCode
                         select data.WorkCode).FirstOrDefault();
            if (data_ == null || data_ == "")
            {
                data_ = "N";
            }
            return data_;
        }
        public static string Chk_workexist(object param)
        {
            WorkDetails mod = param as WorkDetails;
            ConstituencyContext context = new ConstituencyContext();
            var data_ = (from data in context.SchemeMastersConstituency
                         where data.WorkCode == mod.workCode
                         // && data.WorkCode == mod.workCode
                         // where data.ControllingAuthorityId == mod.ControllingAuthorityID
                         // && data.WorkCode == mod.workCode
                         select data.WorkCode).FirstOrDefault();
            if (data_ == null || data_ == "")
            {
                data_ = "N";
            }
            return data_;
        }

        public static string getMemberCodeByConstituency(object param)
        {
            WorkDetails mod = param as WorkDetails;
            ConstituencyContext context = new ConstituencyContext();
            var data1 = (from data in context.mMemberAssembly
                         where data.ConstituencyCode == mod.eConstituencyID
                         // && data.WorkCode == mod.workCode
                         // where data.ControllingAuthorityId == mod.ControllingAuthorityID
                         && data.AssemblyID == mod.AssemblyId
                         select data.MemberID).FirstOrDefault();

            return data1.ToString();
        }

        #endregion my constituency Works

        #region Officer Works

        public static int StatusSectioned(object param)
        {
            int[] arr = param as int[];
            int sttaus = arr[0];
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var count = (from u in context.SchemeMastersConstituency where u.workStatus == sttaus select u).Count();
                return count;
            }
        }




        public static object gelFilteredMySchemeListForOfficer(object param)
        {
            string[] arr = param as string[];
            string program = arr[1], ControllingAuthority = arr[3], FinancialYear = arr[4], agency = arr[5], mOwnerDepartment = arr[6], Panchayat = arr[7], WorkType = arr[8];
            long constituencyID = Convert.ToInt32(arr[0]);
            long districtCode = Convert.ToInt32(arr[10]);
            // long demandCode = Convert.ToInt64(arr[2]);
            long workstatus = Convert.ToInt64(arr[9]);
            string aadharID = arr[11];
            bool member = Convert.ToBoolean(arr[12]);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);
                List<vsSchemeList> list = new List<vsSchemeList>();
                List<myShemeList> mylist = new List<myShemeList>();
                schemeMapping scheme = new schemeMapping()
                {
                    ControllingAuthorityID = ControllingAuthority,
                    FinancialYear = FinancialYear,
                    eConstituencyID = constituencyID,
                    vsConstituencyID = constituencyID,
                    vsSchemeList = list,
                    mySchemeList = mylist,
                    constituenctName = (from cons in context.mConstituency where cons.AssemblyID == assemblyID && cons.ConstituencyCode == constituencyID select cons.ConstituencyName).FirstOrDefault()
                };

                List<SchemeMastersConstituency> myresult = (List<SchemeMastersConstituency>)getAllMyWorkList(constituencyID, false, null);
                myresult = myresult.Where(a => a.financialYear == FinancialYear && a.ControllingAuthorityId == ControllingAuthority).ToList();
                //if (demandCode != 0)
                //{
                //    myresult = myresult.Where(a => a.DemandCode == demandCode).ToList();
                //}
                if (districtCode != 0)
                {
                    myresult = myresult.Where(a => a.DistrictId == districtCode).ToList();
                }

                foreach (SchemeMastersConstituency item in myresult)
                {
                    var workDetails = (from cons in context.ConstituencySchemesBasicDetails
                                       where cons.mySchemeID == item.schemeConstituencyID
                                       select cons).FirstOrDefault();
                    var physicalDetails = (from pcons in context.ConstituencyWorkProgress
                                           where pcons.schemeCode == item.schemeConstituencyID
                                           orderby pcons.submitDate descending
                                           select pcons).FirstOrDefault();
                    var imageCount = (from pcons in context.ConstituencyWorkProgressImages
                                      where pcons.schemeCode == item.schemeConstituencyID
                                      select pcons).Count();

                    mylist.Add(new myShemeList()
                    {
                        workCode = item.WorkCode != null ? item.WorkCode : "",
                        workName = string.IsNullOrEmpty(item.workName) ? item.WorkNameLocal : item.workName,
                        mySchemeID = item.schemeConstituencyID != null ? item.schemeConstituencyID : 0,
                        sanctionedAmount = item.SanctionedAmount != null ? item.SanctionedAmount : 0,
                        EstimatedAmount = item.AmountEstimate != null ? item.AmountEstimate : 0,
                        PanchayatName = (from pan in context.mPanchayat where pan.PanchayatCode == item.Panchayat select pan.PanchayatName).FirstOrDefault(),
                        programName = item.ProgrammeName,
                        sanctionedDate = item.SanctionedDate.HasValue ? item.SanctionedDate.Value.ToString("dd-MM-yyyy") : "",// item.SanctionedDate.Value.ToString("dd-MM-yyyy"),
                        districtID = item.DistrictId != null ? item.DistrictId : 0,

                        ExecutiveDepartment = workDetails != null ? getExecutiveDepartmentName(workDetails.ExecutingDepartmentId) : "",
                        ExecutiveOffice = workDetails != null ? getExecutiveOfficeName(workDetails.ExecutingOfficeID) : "",
                        physicalProgress = physicalDetails != null ? physicalDetails.toalProgress : 0,
                        financialProgress = physicalDetails != null ? physicalDetails.toalExpenditure : 0,
                        financialYear = item.financialYear != null ? item.financialYear : "",
                        CompletionDate = item.EstimatedCompletionDate != null ? item.EstimatedCompletionDate : "",
                        imageCount = imageCount != null ? imageCount : 0,
                        workStatus = (from status in context.mworkStatus where status.workStatusCode == item.workStatus select status.workStatus).FirstOrDefault(),
                        workStatusCode = item.workStatus,
                        subdevisionId = item.SubdevisionId != null ? item.SubdevisionId : 0,
                    });
                }
                if (agency != "0")
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.ExecutiveDepartment == agency).ToList();
                }
                if (mOwnerDepartment != "0")
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.oWnerDepartment == mOwnerDepartment).ToList();
                }
                if (workstatus != 0)
                {
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => a.workStatusCode == workstatus).ToList();
                }
                if (!member)
                {
                    var workList = (from wlist in context.ForwardGrievance
                                    where wlist.AadarId == aadharID
                                    select wlist.GrvCode).ToList();
                    scheme.mySchemeList = scheme.mySchemeList.Where(a => workList.Contains(a.workCode)).Select(a => a).ToList();
                }
                return scheme;
            }
        }

        #endregion Officer Works

        public static object GetAllOfficemappedByMemberCode(object param)
        {

            RecipientGroupsContext context = new RecipientGroupsContext();

            ConstituencyContext tcontext = new ConstituencyContext();
            var currentAssembly = (from site in tcontext.SiteSettings
                                   where site.SettingName == "Assembly"
                                   select site.SettingValue).FirstOrDefault();
            int assemblyID = Convert.ToInt32(currentAssembly);
            List<MlaDiary> lst = new List<MlaDiary>();
            MlaDiary mdl = new MlaDiary();
            mdl.MappedMemberCode = Convert.ToString(param);

            int MemberCode = Convert.ToInt16(mdl.MappedMemberCode);
            //   int district = Convert.ToInt16(model.DistId);
            var _query = (from mo in context.mOffice
                          join m in context.tDepartmentOfficeMapped on mo.OfficeId equals m.OfficeId

                          join td in context.objmDepartment on m.DeptId equals td.deptId
                          where mo.officename != null && m.MemberCode == MemberCode  // && m.ConstituencyCode == model.ConstituencyCode
                          && m.AssemblyCode == assemblyID //&& mo.distcd == district  && mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
                          // select mo).OrderBy(a => a.officename).ToList();
                          select new
                          {
                              OfficeId = mo.OfficeId,
                              officename = mo.officename + " ( " + td.deptname + " ) ",
                          }).ToList().OrderBy(a => a.officename);
            List<SBL.DomainModel.Models.Office.mOffice> lst1 = new List<SBL.DomainModel.Models.Office.mOffice>();
            foreach (var item1 in _query)
            {
                SBL.DomainModel.Models.Office.mOffice mdl1 = new SBL.DomainModel.Models.Office.mOffice();
                mdl1.OfficeId = item1.OfficeId;
                mdl1.officename = item1.officename;
                lst1.Add(mdl1);
            }

            return lst1;
            // }


        }
        public static object GetConstituencyByAssemblyId()
        {
            //  var district = Convert.ToUInt16(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);

                return (from listc in context.mConstituency
                        where listc.AssemblyID == assemblyID
                        select new fillListGenricInt
                        {
                            value = listc.ConstituencyCode,
                            Text = listc.ConstituencyName
                        }).ToList();
            }
        }
        public static object GetAllDistricts()
        {
            //  var district = Convert.ToUInt16(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {


                return (from listc in context.Districts
                        where listc.IsActive == true
                        select new fillListGenricInt
                        {
                            value = listc.DistrictCode.Value,
                            Text = listc.DistrictName
                        }).ToList();
            }
        }

        public static object GetDistrictForDC(object param)
        {
            int ofcid = Convert.ToInt32(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {


                return (from listc in context.Districts
                        where listc.IsActive == true && listc.DistrictCode == ofcid

                        select new fillListGenricInt
                        {
                            value = listc.DistrictCode.Value,
                            Text = listc.DistrictName
                        }).ToList();
            }
        }
        public static object GetConstituencyByDistrictId(object param)
        {
            var district = Convert.ToUInt16(param);
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var currentAssembly = (from site in context.SiteSettings
                                       where site.SettingName == "Assembly"
                                       select site.SettingValue).FirstOrDefault();
                int assemblyID = Convert.ToInt32(currentAssembly);

                return (from listc in context.mConstituency
                        where listc.AssemblyID == assemblyID && listc.DistrictCode == district
                        select new fillListGenricInt
                        {
                            value = listc.ConstituencyCode,
                            Text = "Ward No." + listc.ConstituencyCode + ":" + listc.ConstituencyName



                        }).ToList();
            }
        }

        public static object GetAllMlaListforWorks(object param)
        {
            ConstituencyContext tcontext = new ConstituencyContext();
            var currentAssembly = (from site in tcontext.SiteSettings
                                   where site.SettingName == "Assembly"
                                   select site.SettingValue).FirstOrDefault();
            //int assemblyID = Convert.ToInt32(currentAssembly);
            int AssemblyId = Convert.ToInt32(currentAssembly);
            try
            {
                using (ConstituencyContext db = new ConstituencyContext())
                {
                    //var data = (from p in db.Member
                    //            join b in db.mMemberAssembly on p.MemberCode equals b.MemberID
                    //            //orderby b.SubOfficeId ascending
                    //            where b.AssemblyID == AssemblyId && b.Active == true
                    //            select p).ToList();
                    // GenreLst.AddRange(data.SelectMany(x => x.Split(',')).Distinct());

                    //List<MlaDiary> lst = new List<MlaDiary>();
                    //foreach (var item in data)
                    //{
                    //    MlaDiary mdl = new MlaDiary();
                    //    mdl.MemberCode = item.MemberCode;
                    //    mdl.MappedMemberName = item.Name;
                    //    lst.Add(mdl);
                    //}
                    return (from p in db.Member
                            join b in db.mMemberAssembly on p.MemberCode equals b.MemberID
                            where b.AssemblyID == AssemblyId && b.Active == true
                            select new fillListGenricInt
                            {
                                value = p.MemberCode,
                                Text = p.Name
                            }).ToList();
                    //return lst;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static object GetAllSubOfficeList(object param)
        {
            MlaDiary val = param as MlaDiary;
            Int64 ofcid = Convert.ToInt64(param);
            // int ofcid = Convert.ToInt32(param);

            try
            {

                using (ConstituencyContext db = new ConstituencyContext())
                {


                    if (ofcid == 0)
                    {
                        return (from p in db.mOffice
                                join b in db.tOfficeHierarchy on p.OfficeId equals b.SubOfficeId
                                orderby b.SubOfficeId ascending
                                select new fillListGenricInt
                                {
                                    value = p.OfficeId,
                                    Text = p.officename
                                }).ToList();

                    }
                    else
                    {
                        return (from p in db.mOffice
                                join b in db.tOfficeHierarchy on p.OfficeId equals b.SubOfficeId
                                orderby b.SubOfficeId ascending
                                where b.OfficeId == ofcid
                                select new fillListGenricInt
                                {
                                    value = p.OfficeId,
                                    Text = p.officename
                                }).ToList();

                    }
                    //var data = (from p in db.moffice
                    //            join b in db.tOfficeHierarchy on p.OfficeId equals b.SubOfficeId
                    //            orderby b.SubOfficeId ascending
                    //            where b.OfficeId == ofcid
                    //            select p).ToList();
                    // return data;



                    //List<MlaDiary> lst = new List<MlaDiary>();
                    //foreach (var item in data)
                    //{
                    //    MlaDiary mdl = new MlaDiary();
                    //    mdl.OfficeId = item.OfficeId;
                    //    mdl.officename = item.officename;
                    //    lst.Add(mdl);
                    //}
                    //   return lst;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



    }
}