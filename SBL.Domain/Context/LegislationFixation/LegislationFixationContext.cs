﻿namespace SBL.Domain.Context.LegislationFixation
{
    #region Namespace Reffrences
    using EvoPdf;
    using SBL.DAL;
    using SBL.Domain.Context.Diaries;
    using SBL.Domain.Context.Notice;
    using SBL.Domain.Context.Session;
    using SBL.Domain.Context.SiteSetting;
    using SBL.Domain.Context.User;
    using SBL.DomainModel;
    using SBL.DomainModel.ComplexModel;
    using SBL.DomainModel.Models.Bill;
    using SBL.DomainModel.Models.Committee;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Diaries;
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Event;
    using SBL.DomainModel.Models.LOB;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Notice;
    using SBL.DomainModel.Models.PaperLaid;
    using SBL.DomainModel.Models.Question;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.SiteSetting;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using SBL.DomainModel.Models.CutMotionDemand;
    using SBL.DomainModel.Models.Secretory;
    using SBL.Domain.Context.Ministry;

    #endregion

    class LegislationFixationContext : DBBase<LegislationFixationContext>
    {
        public LegislationFixationContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<tPaperLaidV> tPaperLaidVS { get; set; }
        public virtual DbSet<tBillRegister> tBillRegisters { get; set; }
        public virtual DbSet<tQuestion> tQuestions { get; set; }
        public virtual DbSet<tSubQuestion> tSubQuestions { get; set; }
        public virtual DbSet<tMemberNotice> tMemberNotices { get; set; }
        public virtual DbSet<tCommittee> tCommittees { get; set; }
        public virtual DbSet<mEvent> mEvents { get; set; }
        public virtual DbSet<mPaperCategoryType> mPaperCategoryTypes { get; set; }
        public virtual DbSet<mMinistry> mMinistry { get; set; }
        public virtual DbSet<mDepartment> mDepartment { get; set; }
        public virtual DbSet<mMember> mMember { get; set; }
        public virtual DbSet<tCutMotionDemand> tCutMotionDemand { get; set; }
        public virtual DbSet<tCancelQuestionAuditTrial> tCancelQuestionAuditTrial { get; set; }
        public virtual DbSet<mchangeDepartmentAuditTrail> mchangeDepartmentAuditTrail { get; set; }


        //updated by nitin

        public DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }
        public virtual DbSet<AdminLOB> AdminLOB { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                #region Paper Laid Summary For All Paper Category Type

                case "GetStarredQuestionSentCount":
                    {
                        return GetStarredQuestionSentCount(param.Parameter);
                    }

                case "GetStarredQuestionPendingCount":
                    {
                        return GetStarredQuestionPendingCount(param.Parameter);
                    }

                case "GetStarredQuestionPaperRecievedCount":
                    {
                        return GetStarredQuestionPaperRecievedCount(param.Parameter);
                    }

                case "GetStarredQuestionPaperCurrentLobCount":
                    {
                        return GetStarredQuestionPaperCurrentLobCount(param.Parameter);
                    }
                case "GetStarredQuestionPaperLaidInHomeCount":
                    {
                        return GetStarredQuestionPaperLaidInHomeCount(param.Parameter);
                    }

                case "GetUnStarredQuestionPaperRecievedCount":
                    {
                        return GetUnStarredQuestionPaperRecievedCount(param.Parameter);
                    }

                case "GetUnStarredQuestionSentCount":
                    {
                        return GetUnStarredQuestionSentCount(param.Parameter);
                    }
                case "GetUnStarredQuestionPendingCount":
                    {
                        return GetUnStarredQuestionPendingCount(param.Parameter);
                    }
                case "GetUnStarredQuestionPaperCurrentLobCount":
                    {
                        return GetUnStarredQuestionPaperCurrentLobCount(param.Parameter);
                    }
                case "GetUnStarredQuestionPaperLaidInHomeCount":
                    {
                        return GetUnStarredQuestionPaperLaidInHomeCount(param.Parameter);
                    }
                case "GetNoticePaperRecievedCount":
                    {
                        return GetNoticePaperRecievedCount(param.Parameter);
                    }


                case "GetNoticeSentCount":
                    {
                        return GetNoticeSentCount(param.Parameter);
                    }
                case "GetNoticePendingCount":
                    {
                        return GetNoticePendingCount(param.Parameter);
                    }

                case "GetNoticePaperCurrentLobCount":
                    {
                        return GetNoticePaperCurrentLobCount(param.Parameter);
                    }
                case "GetNoticePaperLaidInHomeCount":
                    {
                        return GetNoticePaperLaidInHomeCount(param.Parameter);
                    }
                case "GetBillsPaperRecievedCount":
                    {
                        return GetBillsPaperRecievedCount(param.Parameter);
                    }
                case "GetBillsPaperCurrentLobCount":
                    {
                        return GetBillsPaperCurrentLobCount(param.Parameter);
                    }
                case "GetBillsPaperLaidInHomeCount":
                    {
                        return GetBillsPaperLaidInHomeCount(param.Parameter);
                    }


                case "GetCommittePaperRecievedCount":
                    {
                        return GetCommittePaperRecievedCount(param.Parameter);
                    }
                case "GetCommittePaperCurrentLobCount":
                    {
                        return GetCommittePaperCurrentLobCount(param.Parameter);
                    }
                case "GetCommittePaperLaidInHomeCount":
                    {
                        return GetCommittePaperLaidInHomeCount(param.Parameter);
                    }

                case "GetPaperToLayRecievedCount":
                    {
                        return GetOtherPaperToLayRecievedCount(param.Parameter);
                    }

                case "GetPaperToLayCurrentLobCount":
                    {
                        return GetOtherPaperToLayCurrentLobCount(param.Parameter);
                    }

                case "GetPaperToLayLaidInHomeCount":
                    {
                        return GetOtherPaperToLayLaidInHomeCount(param.Parameter);
                    }
                case "GetNoticeAssignPendingCount":
                    {
                        return GetNoticeAssignPendingCount(param.Parameter);
                    }
                case "UpdatetransferQuestions":
                    {
                        return UpdatetransferQuestions(param.Parameter);
                    }

                case "RejectedList":
                    {
                        return RejectedList(param.Parameter);
                    }
                case "UnRejectedList":
                    {
                        return UnRejectedList(param.Parameter);
                    }



                //case "GetStarredQuestionSentList":
                //    {
                //        return GetStarredQuestionSentList();
                //    }

                //case "GetStarredQuestionPaperRecievedList":
                //    {
                //        return GetStarredQuestionPaperRecievedList();
                //    }

                //case "GetStarredQuestionPaperCurrentLobList":
                //    {
                //        return GetStarredQuestionPaperCurrentLobList();f
                //    }
                //case "GetStarredQuestionPaperLaidInHomeList":
                //    {
                //        return GetStarredQuestionPaperLaidInHomeList();
                //    }

                //case "GetUnStarredQuestionSentList":
                //    {
                //        return GetUnStarredQuestionSentList();
                //    }

                //case "GetUnStarredQuestionPaperRecievedList":
                //    {
                //        return GetUnStarredQuestionPaperRecievedList();
                //    }
                //case "GetUnStarredQuestionPaperCurrentLobList":
                //    {
                //        return GetUnStarredQuestionPaperCurrentLobList();
                //    }
                //case "GetUnStarredQuestionPaperLaidInHomeList":
                //    {
                //        return GetUnStarredQuestionPaperLaidInHomeList();
                //    }

                //case "GetNoticeSentList":
                //    {
                //        return GetNoticeSentList();
                //    }
                //case "GetNoticePaperRecievedList":
                //    {
                //        return GetNoticePaperRecievedList();
                //    }
                //case "GetNoticePaperCurrentLobList":
                //    {
                //        return GetNoticePaperCurrentLobList();
                //    }
                //case "GetNoticePaperLaidInHomeList":
                //    {
                //        return GetNoticePaperLaidInHomeList();
                //    }
                //case "GetBillsPaperRecievedList":
                //    {
                //        return GetBillsPaperRecievedList();
                //    }
                //case "GetBillsPaperCurrentLobList":
                //    {
                //        return GetBillsPaperCurrentLobList();
                //    }
                //case "GetBillsPaperLaidInHomeList":
                //    {
                //        return GetBillsPaperLaidInHomeList();
                //    }

                //case "GetCommittePaperRecievedList":
                //    {
                //        return GetCommittePaperRecievedList();
                //    }
                //case "GetCommittePaperCurrentLobList":
                //    {
                //        return GetCommittePaperCurrentLobList();
                //    }
                //case "GetCommittePaperLaidInHomeList":
                //    {
                //        return GetCommittePaperLaidInHomeList();
                //    }

                //case "GetPaperToLayRecievedList":
                //    {
                //        return GetOtherPaperToLayRecievedList();
                //    }

                //case "GetPaperToLayCurrentLobList":
                //    {
                //        return GetOtherPaperToLayCurrentLobList();
                //    }

                //case "GetPaperToLayLaidInHomeList":
                //    {
                //        return GetOtherPaperToLayLaidInHomeList();
                //    }

                #endregion

                #region Detailed Paper Laid Summary For A Paper Category Type By Minister/Department/Status
                case "GetPaperLaidSummaryForPaperCategory":
                    {
                        return GetPaperLaidSummaryForPaperCategory(param.Parameter);
                    }
                case "GetCompletePaperLaidDetailsByMinistry":
                    {
                        return GetCompletePaperLaidDetailsByMinistry(param.Parameter);
                    }

                case "GetCompletePaperLaidDetailsByPaperLaid":
                    {
                        return GetCompletePaperLaidDetailsByPaperLaid(param.Parameter);
                    }

                case "GetCompletePaperLaidDetailsByDepartment":
                    {
                        return GetCompletePaperLaidDetailsByDepartment(param.Parameter);
                    }
                case "GetCompletePaperLaidDetailsByStatus":
                    {
                        return GetCompletePaperLaidDetailsByStatus(param.Parameter);
                    }

                case "GetSessionAndSessionDateforPostpone":
                    {
                        return GetSessionAndSessionDateforPostpone(param.Parameter);
                    }

                case "PostponeQuestionbyId":
                    {
                        return PostponeQuestionbyId(param.Parameter);
                    }
                case "GetNoticeEditDiary":
                    {
                        return GetNoticeEditDiary(param.Parameter);
                    }

                #endregion

                #region other methods

                case "GetPapercategoryDetailsById":
                    {
                        return GetPapercategoryDetailsById(param.Parameter);
                    }
                case "GetPaperLaidDetailsById":
                    {
                        return GetPaperLaidDetailsById(param.Parameter);
                    }
                case "GetQuestionDetailsById":
                    {
                        return GetQuestionDetailsById(param.Parameter);
                    }
                case "GetNoticeDetailsById":
                    {
                        return GetNoticeDetailsById(param.Parameter);
                    }
                case "GetDepartmentById":
                    {
                        return GetDepartmentById(param.Parameter);
                    }

                case "GetMinistryById":
                    {
                        return GetMinistryById(param.Parameter);
                    }

                case "UpdateDepartmentwisePriority":
                    {
                        return UpdateDepartmentwisePriority(param.Parameter);
                    }
                case "UpdateMinistrywisePriority":
                    {
                        return UpdateMinistrywisePriority(param.Parameter);
                    }
                case "UpdatePaperLaidPriority":
                    {
                        return UpdatePaperLaidPriority(param.Parameter);
                    }

                #endregion

                #region Added By Nitin
                case "GetUpdatedOtherPaperLaidCounters":
                    {
                        return GetUpdatedOtherPaperLaidCounters(param.Parameter);
                    }
                case "GetOtherPaperPendingToLayListCount":
                    {
                        return GetOtherPaperPendingToLayListCount(param.Parameter);
                    }
                case "GetStarredQuestionPaperPendingToLayListCount":
                    {
                        return GetStarredQuestionPaperPendingToLayListCount(param.Parameter);
                    }
                case "GetUnStarredQuestionPaperPendingToLayListCount":
                    {
                        return GetUnStarredQuestionPaperPendingToLayListCount(param.Parameter);
                    }

                case "GetOtherPaperLaidPaperCategoryType":
                    {
                        return GetOtherPaperLaidPaperCategoryType(param.Parameter);
                    }
                case "GetUpdatedOtherPaperList":
                    {
                        return GetUpdatedOtherPaperList(param.Parameter);
                    }
                case "ShowUpdatedOtherPaperLaidDetailByID":
                    {
                        return ShowUpdatedOtherPaperLaidDetailByID(param.Parameter);
                    }
                case "GetDetailsByPaperLaidId":
                    {
                        return GetDetailsByPaperLaidId(param.Parameter);
                    }
                case "UpdateLOBTempIdByPaperLiadID":
                    {
                        return UpdateLOBTempIdByPaperLiadID(param.Parameter);
                    }

                case "GetNoticePaperPendingToLayListCount":
                    {
                        return GetNoticePaperPendingToLayListCount(param.Parameter);
                    }
                case "GetBillsPaperPendingToLayListCount":
                    {
                        return GetBillsPaperPendingToLayListCount(param.Parameter);
                    }
                case "GetCommittePaperPendingToLayListCount":
                    {
                        return GetCommittePaperPendingToLayListCount(param.Parameter);
                    }
                #endregion
                #region "Method added By Sunil"
                case "GetMemberNameByID":
                    {
                        return GetMemberNameByID(param.Parameter);
                    }

                case "GetQuestionByIdForEdit":
                    {
                        return GetQuestionByIdForEdit(param.Parameter);
                    }

                case "GetNoticeByIdForEdit":
                    {
                        return GetNoticeByIdForEdit(param.Parameter);
                    }
                case "GetDepartmentByMinister":
                    {
                        return GetDepartmentByMinister(param.Parameter);
                    }
                case "GetConstByMemberId":
                    {
                        return GetConstByMemberId(param.Parameter);
                    }
                case "SaveQuestion":
                    {
                        return SaveQuestion(param.Parameter);
                    }
                case "SaveNotice":
                    {
                        return SaveNotice(param.Parameter);
                    }
                case "GetQuestionDetailById":
                    {
                        return GetQuestionDetailById(param.Parameter);
                    }
                case "GetNoticeDetailById":
                    {
                        return GetNoticeDetailById(param.Parameter);
                    }

                case "CheckQuestionNo":
                    {
                        return CheckQuestionNo(param.Parameter);
                    }
                case "GetAllDiariedRecord":
                    {
                        return GetAllDiariedRecord(param.Parameter);
                    }
                case "GetQuesByIdForRejection":
                    {
                        return GetQuesByIdForRejection(param.Parameter);
                    }
                case "GetRejectRule":
                    {
                        return GetRejectRule(param.Parameter);

                    }

                case "RejectQuestionById":
                    {
                        return RejectQuestionById(param.Parameter);
                    }
                case "GetRulesforRejectedQuestionById":
                    {
                        return GetRulesforRejectedQuestionById(param.Parameter);
                    }
                case "ChangeQuestionTypeById":
                    {
                        return ChangeQuestionTypeById(param.Parameter);
                    }

                case "GetLegislationDashboardItemsCounter":
                    {
                        return GetLegislationDashboardItemsCounter(param.Parameter);
                    }
                case "GetEmpNameByEid":
                    {
                        return GetEmpNameByEid(param.Parameter);
                    }
                case "GetStarredFixedQuesCount":
                    {
                        return GetStarredFixedQuesCount(param.Parameter);
                    }
                case "GetStarredUnFixedQuesCount":
                    {
                        return GetStarredUnFixedQuesCount(param.Parameter);
                    }
                case "GetUnstarredFixedQuesCount":
                    {
                        return GetUnstarredFixedQuesCount(param.Parameter);
                    }
                case "GetUnstarredUnFixedQuesCount":
                    {
                        return GetUnstarredUnFixedQuesCount(param.Parameter);
                    }
                case "GetMemoCount":
                    {
                        return GetMemoCount(param.Parameter);
                    }


                case "GetStarredUnfixedQuestions":
                    {
                        return GetStarredUnfixedQuestions(param.Parameter);
                    }
                case "GetAllSessionDate":
                    {
                        return GetAllSessionDate(param.Parameter);
                    }
                case "SearchBySessionDate":
                    {
                        return SearchBySessionDate(param.Parameter);
                    }
                case "GetUnstarredUnfixedQuestions":
                    {
                        return GetUnstarredUnfixedQuestions(param.Parameter);
                    }
                case "GetRotationalMiniterName":
                    {
                        return GetRotationalMiniterName(param.Parameter);
                    }
                case "GetResetDataByQuestionId":
                    {
                        return GetResetDataByQuestionId(param.Parameter);
                    }
                case "GetFileNamePath":
                    {
                        return GetFileNamePath(param.Parameter);
                    }
                case "FixingQuestionById":
                    {
                        return FixingQuestionById(param.Parameter);
                    }
                case "GetDeptSubmittedDate":
                    {
                        return GetDeptSubmittedDate(param.Parameter);
                    }
                case "GetStarredfixedQuestions":
                    {

                        return GetStarredfixedQuestions(param.Parameter);
                    }
                case "GetUnstarredfixedQuestions":
                    {
                        return GetUnstarredfixedQuestions(param.Parameter);
                    }
                case "SearchBySessionDateForFix":
                    {
                        return SearchBySessionDateForFix(param.Parameter);
                    }
                case "ApproveFixingQuestion":
                    {
                        return ApproveFixingQuestion(param.Parameter);
                    }
                case "SearchPostPoneBySessionDateForFix":
                    {
                        return SearchPostPoneBySessionDateForFix(param.Parameter);
                    }
                case "GetSessionDateforPostpone":
                    {
                        return GetSessionDateforPostpone(param.Parameter);
                    }

                case "GetQuestionForPostpone":
                    {
                        return GetQuestionForPostpone(param.Parameter);
                    }

                case "ReFixPostPoneQuestionbyId":
                    {
                        return ReFixPostPoneQuestionbyId(param.Parameter);
                    }

                case "GetFixedDateBySDtId":
                    {
                        return GetFixedDateBySDtId(param.Parameter);
                    }

                case "GetAllPostPoneQues":
                    {
                        return GetAllPostPoneQues(param.Parameter);
                    }

                case "GetMinisterSubmittedDate":
                    {
                        return GetMinisterSubmittedDate(param.Parameter);
                    }
                case "GetLastQuestionNumber":
                    {
                        return GetLastQuestionNumber();
                    }
                case "AssignPrioritybyId":
                    {
                        return AssignPrioritybyId(param.Parameter);
                    }
                case "GetStarredRejectedCount":
                    {
                        return GetStarredRejectedCount(param.Parameter);
                    }
                case "GetUnstarredRejectedCount":
                    {
                        return GetUnstarredRejectedCount(param.Parameter);
                    }

                case "GetStarredToUnstarredCount":
                    {
                        return GetStarredToUnstarredCount(param.Parameter);
                    }

                case "GetStarredRejectedList":
                    {
                        return GetStarredRejectedList(param.Parameter);
                    }
                case "GetUnstarredRejectedList":
                    {
                        return GetUnstarredRejectedList(param.Parameter);
                    }

                case "GetStarredToUnstarredList":
                    {
                        return GetStarredToUnstarredList(param.Parameter);
                    }

                case "GetAttachmentByNoticeId":
                    {
                        return GetAttachmentByNoticeId(param.Parameter);
                    }

                case "GetConstNameLocalByNo":
                    {

                        return GetConstNameLocalByNo(param.Parameter);

                    }
                case "GetAllDetailByPaperType":
                    {
                        return GetAllDetailByPaperType(param.Parameter);
                    }
                case "GetAssemblyList":
                    {
                        return GetAssemblyList();
                    }
                case "GetSessionLsitbyAssemblyId":
                    {
                        return GetSessionLsitbyAssemblyId(param.Parameter);
                    }
                case "GetStarredQuestionsForTypeChange":
                    {
                        return GetStarredQuestionsForTypeChange(param.Parameter);
                    }
                case "MoveToUnstarredByIds":
                    {
                        return MoveToUnstarredByIds(param.Parameter);
                    }
                case "BackToStarredByQuesId":
                    {
                        return BackToStarredByQuesId(param.Parameter);
                    }
                case "GetQuestionsForBracket":
                    {
                        return GetQuestionsForBracket(param.Parameter);
                    }
                case "CheckBracktingByDiaryNumber":
                    {
                        return CheckBracktingByDiaryNumber(param.Parameter);
                    }
                case "BracketQuestionByDiaryNumber":
                    {
                        return BracketQuestionByDiaryNumber(param.Parameter);
                    }
                case "RemoveBracketingByQuestionId":
                    {
                        return RemoveBracketingByQuestionId(param.Parameter);
                    }
                case "GetChangedNotice":
                    {
                        return GetChangedNotice(param.Parameter);
                    }
                case "GetRuleNameByRuleId":
                    {
                        return GetRuleNameByRuleId(param.Parameter);
                    }
                case "GetNoticeForTypeChange":
                    {
                        return GetNoticeForTypeChange(param.Parameter);
                    }
                case "ChangeNoticeRuleById":
                    {
                        return ChangeNoticeRuleById(param.Parameter);
                    }
                case "GetChangedRuleNoticeCount":
                    {
                        return GetChangedRuleNoticeCount(param.Parameter);
                    }
                case "GetFixedQuesPDFByDate":
                    {
                        return GetFixedQuesPDFByDate(param.Parameter);
                    }
                case "AdmitRejectedQuesById":
                    {
                        return AdmitRejectedQuesById(param.Parameter);
                    }
                case "GetSearchEventDemands":
                    {
                        return GetSearchEventDemands(param.Parameter);
                    }
                case "BracketCutMotionByNoticeNumber":
                    {
                        return BracketCutMotionByNoticeNumber(param.Parameter);
                    }

                case "CheckBracktingByNoticeNumber":
                    {
                        return CheckBracktingByNoticeNumber(param.Parameter);
                    }

                case "GetFixedQuestion":
                    {
                        return GetFixedQuestion(param.Parameter);
                    }
                case "GetAllQDetailById":
                    {
                        return GetAllQDetailById(param.Parameter);
                    }
                case "UpdateDeptChangeQuestions":
                    {
                        return UpdateDeptChangeQuestions(param.Parameter);
                    }
                case "GetSentNotices":
                    {
                        return GetSentNotices(param.Parameter);
                    }
                case "GetAllNoticeDetailById":
                    {
                        return GetAllNoticeDetailById(param.Parameter);
                    }
                case "UpdateDeptChangeNotices":
                    {
                        return UpdateDeptChangeNotices(param.Parameter);
                    }
                case "QuestionFixedSessionDateCommasep":
                    {
                        return QuestionFixedSessionDateCommasep(param.Parameter);
                    }
                case "GetAcknowledgeStarredQuestion":
                    {
                        return GetAcknowledgeStarredQuestion(param.Parameter);
                    }
                case "GetAcknowledgeUnStarredQuestion":
                    {
                        return GetAcknowledgeUnStarredQuestion(param.Parameter);
                    }
                case "GetQuestionsForExcess":
                    {
                        return GetQuestionsForExcess(param.Parameter);
                    }

                case "ExcessingQuestions":
                    {
                        return ExcessingQuestions(param.Parameter);
                    }
                case "GetExcessedQuestions":
                    {
                        return GetExcessedQuestions(param.Parameter);
                    }
                case "RemoveExcessingQuestions":
                    {
                        return RemoveExcessingQuestions(param.Parameter);
                    }
                case "GetCountQuestionsForExcess":
                    {
                        return GetCountQuestionsForExcess(param.Parameter);
                    }
                case "GetCountExcessedQuestions":
                    {
                        return GetCountExcessedQuestions(param.Parameter);
                    }
                case "GetUnStarredCountQuestionsForExcess":
                    {
                        return GetUnStarredCountQuestionsForExcess(param.Parameter);
                    }
                case "GetCountUnStarredExcessedQuestions":
                    {
                        return GetCountUnStarredExcessedQuestions(param.Parameter);
                    }
                case "GetQuestionsForWithdrawn":
                    {
                        return GetQuestionsForWithdrawn(param.Parameter);
                    }
                case "WithdranwQuestions":
                    {
                        return WithdranwQuestions(param.Parameter);
                    }
                case "GetListOfWithdrawnQuest":
                    {
                        return GetListOfWithdrawnQuest(param.Parameter);
                    }

                case "GetStarredforWithdrawnCount":
                    {
                        return GetStarredforWithdrawnCount(param.Parameter);
                    }
                case "GetStarredListWithdrawnCount":
                    {
                        return GetStarredListWithdrawnCount(param.Parameter);
                    }
                case "GetUnstarredforWithdrawnCount":
                    {
                        return GetUnstarredforWithdrawnCount(param.Parameter);
                    }
                case "GetUnstarredListWithdrawnCount":
                    {
                        return GetUnstarredListWithdrawnCount(param.Parameter);
                    }
                case "GetRulesDepartment":
                    {
                        return GetRulesDepartment(param.Parameter);
                    }
                case "BracktingForNoticeNumber":
                    {
                        return BracktingForNoticeNumber(param.Parameter);
                    }
                case "BracketNoticeByNoticeNumber":
                    {
                        return BracketNoticeByNoticeNumber(param.Parameter);
                    }
                case "GetNoticesForPostpone":
                    {
                        return GetNoticesForPostpone(param.Parameter);
                    }
                case "PostponeNoticesbyId":
                    {
                        return PostponeNoticesbyId(param.Parameter);
                    }

                case "SearchPostPoneBySessionFixNotice":
                    {
                        return SearchPostPoneBySessionFixNotice(param.Parameter);
                    }

                case "ReFixPostPoneNoticebyId":
                    {
                        return ReFixPostPoneNoticebyId(param.Parameter);
                    }

                #endregion
                #region Update LOB Papers
                case "GetLOBUpdatedPaperBySessionDate":
                    { return GetLOBUpdatedPaperBySessionDate(param.Parameter); }
                #endregion


                case "GetStarredQuestionListForCancel":
                    { return GetStarredQuestionListForCancel(param.Parameter); }
                case "GetCancelledQuestionList":
                    { return GetCancelledQuestionList(param.Parameter); }
            }
            return null;
        }

        #region Other Methods

        static object UpdatePaperLaidPriority(object param)
        {
            tPaperLaidV parameter = param as tPaperLaidV;
            long PaperlaidId = parameter.PaperLaidId;
            string Prioritytype = parameter.MinistryName;
            int Priority = parameter.PaperlaidPriority;



            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {

                    var paperlaidToUpdate = db.tPaperLaidVS.SingleOrDefault(q => q.PaperLaidId == parameter.PaperLaidId);

                    switch (Prioritytype)
                    {
                        case "PaperLaid": paperlaidToUpdate.PaperlaidPriority = Priority; break;
                        case "Ministry": paperlaidToUpdate.MinistrywisePriority = Priority; break;
                        case "Department": paperlaidToUpdate.DepartmentwisePriority = Priority; break;
                    }
                    db.tPaperLaidVS.Attach(paperlaidToUpdate);
                    db.Entry(paperlaidToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                    return null;
                }
            }
            catch
            {
                throw;
            }

        }

        static object UpdateDepartmentwisePriority(object param)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    List<PaperLaidPriority> priority = param as List<PaperLaidPriority>;
                    priority = priority.Where(a => a.FilteredBy == 1).ToList();
                    var PaperLaids = db.tPaperLaidVS.ToList();
                    foreach (var Department in PaperLaids)
                    {
                        foreach (var item in priority)
                        {
                            if (Department.PaperLaidId == item.PaperlaidId)
                            {
                                var DepartmaentToUpdate = db.tPaperLaidVS.SingleOrDefault(b => b.PaperLaidId == item.PaperlaidId);
                                DepartmaentToUpdate.DepartmentwisePriority = item.Priority;
                                db.tPaperLaidVS.Attach(DepartmaentToUpdate);
                                db.Entry(DepartmaentToUpdate).State = EntityState.Modified;
                            }

                        }
                    }
                    db.SaveChanges();
                    db.Close();
                    return null;
                }
            }
            catch
            {
                throw;
            }
        }

        static object UpdateMinistrywisePriority(object param)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    List<PaperLaidPriority> priority = param as List<PaperLaidPriority>;
                    priority = priority.Where(a => a.FilteredBy == 2).ToList();
                    var PaperLaids = db.tPaperLaidVS.ToList();
                    foreach (var ministry in PaperLaids)
                    {
                        foreach (var item in priority)
                        {
                            if (ministry.PaperLaidId == item.PaperlaidId)
                            {
                                var MinistryToUpdate = db.tPaperLaidVS.SingleOrDefault(b => b.PaperLaidId == item.PaperlaidId);
                                MinistryToUpdate.MinistrywisePriority = item.Priority;
                                db.tPaperLaidVS.Attach(MinistryToUpdate);
                                db.Entry(MinistryToUpdate).State = EntityState.Modified;
                            }

                        }
                    }
                    db.SaveChanges();
                    db.Close();
                    return null;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetPapercategoryDetailsById(object param)
        {
            mPaperCategoryType paper = param as mPaperCategoryType;
            int Id = paper.PaperCategoryTypeId;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from P in db.mPaperCategoryTypes
                                  where P.PaperCategoryTypeId == Id
                                  select P).SingleOrDefault();

                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetPaperLaidDetailsById(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;
            long Id = paper.PaperLaidId;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from P in db.tPaperLaidVS
                                  where P.PaperLaidId == Id
                                  select P).SingleOrDefault();
                    var result1 = (from P in db.tBillRegisters
                                   where P.PaperLaidId == Id
                                   select P).SingleOrDefault();
                    result.BOTStatus = result1.Status;
                    result.BussinessType = (from E in db.mEvents where E.EventId == result.EventId select E.EventName).SingleOrDefault();                            

                    return result;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Paper Laid Summary For All Paper Category Types

        #region Qusetion

        //pending count
        static int GetStarredQuestionPendingCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tQuestion>)GetStarredQuestionPendingList();
                //return result.ToList().Count();
                var result = (tQuestion)GetStarredQuestionPendingList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionPendingList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from Q in db.tQuestions
                                  where ((Q.QuestionStatus == (int)Questionstatus.QuestionPending) || (Q.QuestionStatus == (int)Questionstatus.QuestionAssignTypist)
                                  || (Q.QuestionStatus == (int)Questionstatus.QuestionFreeze) || (Q.QuestionStatus == (int)Questionstatus.QuestionProofReading))
                                  //&& (Q.DiaryNumber != null)
                                  && (Q.AssemblyID == model.AssemblyCode)
                                  && (Q.SessionID == model.SessionCode)// for question
                                  && (Q.QuestionType == (int)QuestionType.StartedQuestion)// for starred
                                  && (Q.DeActivate == false || Q.DeActivate == null)
                                  && (Q.IsRejected == null || Q.IsRejected == false)
                                  //&& (Q.IsClubbed == false || Q.IsClubbed == null)
                                  //&& (Q.IsContentFreeze == true)
                                  orderby Q.QuestionID
                                  select Q).ToList();

                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }
        //sent count
        static int GetStarredQuestionSentCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tQuestion>)GetStarredQuestionSentList();
                //return result.ToList().Count();
                var result = (tQuestion)GetStarredQuestionSentList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionSentList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from Q in db.tQuestions
                                  where (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                                   && (Q.AssemblyID == model.AssemblyCode)
                                  && (Q.SessionID == model.SessionCode)
                                  && (Q.QuestionType == (int)QuestionType.StartedQuestion)// for starred
                                  orderby Q.QuestionID
                                  select Q).ToList();

                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }

        //Recieved count
        static int GetStarredQuestionPaperRecievedCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tQuestion>)GetStarredQuestionPaperRecievedList();
                //return result.ToList().Count();
                var result = (tQuestion)GetStarredQuestionPaperRecievedList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionPaperRecievedList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null  // for question
                                        && Q.QuestionType == 1// for starred
                                        && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                        && PLT.DeptSubmittedBy != null
                                        && PLT.DeptSubmittedDate != null
                                        && PLT.MinisterSubmittedDate != null
                                        && PL.AssemblyId == model.AssemblyCode
                                        && PL.SessionId == model.SessionCode
                                  select Q).ToList();

                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }


        //Current LOB  count
        static int GetStarredQuestionPaperCurrentLobCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetStarredQuestionPaperCurrentLobList();
                //return result.ToList().Count();
                var result = (tQuestion)GetStarredQuestionPaperCurrentLobList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionPaperCurrentLobList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for starred
                                    && PL.AssemblyId == model.AssemblyCode
                                    && PL.SessionId == model.SessionCode
                                  select Q).ToList();
                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }

        //Laid in home count
        static int GetStarredQuestionPaperLaidInHomeCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            tQuestion QObj = new tQuestion();
            try
            {
                //var result = (List<tPaperLaidV>)GetStarredQuestionPaperLaidInHomeList();
                //return result.ToList().Count();
                var result = (tQuestion)GetStarredQuestionPaperLaidInHomeList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionPaperLaidInHomeList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for starred
                                    && PL.AssemblyId == model.AssemblyCode
                                    && PL.SessionId == model.SessionCode
                                  select Q).ToList();
                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }

        //Pending To Lay list
        static object GetStarredQuestionPaperPendingToLayList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {

            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for starred
                                    && PL.AssemblyId == model.AssemblyCode
                                    && PL.SessionId == model.SessionCode
                                  select Q).ToList();
                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }


        //pending count
        static int GetUnStarredQuestionPendingCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tQuestion>)GetUnStarredQuestionPendingList();
                //return result.ToList().Count();
                var result = (tQuestion)GetUnStarredQuestionPendingList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }
        static object GetUnStarredQuestionPendingList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from Q in db.tQuestions
                                  where ((Q.QuestionStatus == (int)Questionstatus.QuestionPending) || (Q.QuestionStatus == (int)Questionstatus.QuestionAssignTypist)
                                  || (Q.QuestionStatus == (int)Questionstatus.QuestionFreeze) || (Q.QuestionStatus == (int)Questionstatus.QuestionProofReading))
                                  //&& (Q.DiaryNumber != null)
                                  && (Q.AssemblyID == model.AssemblyCode)
                                  && (Q.SessionID == model.SessionCode)// for question
                                  && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)// for unStarred
                                  && (Q.IsRejected == null || Q.IsRejected == false)
                                  //&& (Q.IsClubbed == false || Q.IsClubbed == null)
                                  //&& (Q.IsContentFreeze == true)
                                  orderby Q.QuestionID
                                  select Q).ToList();

                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }

        //sent count
        static int GetUnStarredQuestionSentCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tQuestion>)GetUnStarredQuestionSentList();
                //return result.ToList().Count();
                var result = (tQuestion)GetUnStarredQuestionSentList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetUnStarredQuestionSentList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from Q in db.tQuestions
                                  where (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                                   && (Q.AssemblyID == model.AssemblyCode)
                                  && (Q.SessionID == model.SessionCode)
                                  && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)// for Unstarred
                                  orderby Q.QuestionID
                                  select Q).ToList();

                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }


        //Recieved count
        static int GetUnStarredQuestionPaperRecievedCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tQuestion>)GetUnStarredQuestionPaperRecievedList();
                //return result.ToList().Count();
                var result = (tQuestion)GetUnStarredQuestionPaperRecievedList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetUnStarredQuestionPaperRecievedList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null // for question
                                     && Q.QuestionType == 2// for unstarred
                                     && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                     && PLT.DeptSubmittedBy != null
                                     && PLT.DeptSubmittedDate != null
                                     && PLT.MinisterSubmittedDate != null
                                     && PL.AssemblyId == model.AssemblyCode
                                     && PL.SessionId == model.SessionCode
                                  select Q).ToList();

                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;

                }
            }
            catch
            {
                throw;
            }
        }

        //Current LOB  count
        static int GetUnStarredQuestionPaperCurrentLobCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetUnStarredQuestionPaperCurrentLobList();
                //return result.ToList().Count();
                var result = (tQuestion)GetUnStarredQuestionPaperCurrentLobList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetUnStarredQuestionPaperCurrentLobList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                     && PL.AssemblyId == model.AssemblyCode
                                     && PL.SessionId == model.SessionCode
                                  select Q).ToList();
                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }


        //Laid in Home
        static int GetUnStarredQuestionPaperLaidInHomeCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetUnStarredQuestionPaperLaidInHomeList();
                //return result.ToList().Count();
                var result = (tQuestion)GetUnStarredQuestionPaperLaidInHomeList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetUnStarredQuestionPaperLaidInHomeList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                     && PL.AssemblyId == model.AssemblyCode
                                     && PL.SessionId == model.SessionCode
                                  select Q).ToList();
                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }

        //Pending To Lay
        static object GetUnStarredQuestionPaperPendingToLayList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tQuestion QObj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                     && PL.AssemblyId == model.AssemblyCode
                                     && PL.SessionId == model.SessionCode
                                  select Q).ToList();
                    QObj.ResultCount = result.Count();
                    QObj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return QObj;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Notice
        static int GetNoticePaperRecievedCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tMemberNotice>)GetNoticePaperRecievedList();
                //return result.ToList().Count();
                var result = (tMemberNotice)GetNoticePaperRecievedList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePaperRecievedList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tMemberNotice NObj = new tMemberNotice();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId == null
                                  && E.PaperCategoryTypeId == 4
                                  && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                  && PLT.DeptSubmittedBy != null
                                  && PLT.DeptSubmittedDate != null
                                  && PLT.MinisterSubmittedDate != null
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  // for notice
                                  select new { MN, PL.Title, MN.NoticeNumber, Min.MinistryName, Min.MinisterName }).ToList();
                    List<tMemberNotice> paperlaidlist = new List<tMemberNotice>();
                    foreach (var item in result)
                    {
                        tMemberNotice model = new tMemberNotice();
                        model = item.MN;
                        model.RuleNo = (from e in db.mEvents where e.EventId == item.MN.NoticeTypeID select e.RuleNo).FirstOrDefault();
                        model.NoticeNumber = item.NoticeNumber;
                        model.MinistryName = item.MinistryName;
                        model.MinisterName = item.MinistryName;
                        paperlaidlist.Add(model);
                    }

                    NObj.ResultCount = paperlaidlist.Count();
                    NObj.memberNoticeList = paperlaidlist.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return NObj;

                }
            }
            catch
            {
                throw;
            }
        }
        static int GetNoticeSentCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                var result = (tMemberNotice)GetNoticeSentList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticeSentList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tMemberNotice NObj = new tMemberNotice();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from MN in db.tMemberNotices
                                  where MN.NoticeNumber != null
                                  && (MN.NoticeStatus == (int)NoticeStatusEnum.NoticeSent)
                                  && (MN.AssemblyID == model.AssemblyCode)
                                  && (MN.SessionID == model.SessionCode)
                                  && (MN.IsSubmitted == true)
                                  // for noticepending
                                  select MN).ToList();


                    foreach (var item in result)
                    {
                        item.RuleNo = (from e in db.mEvents where e.EventId == item.NoticeTypeID select e.RuleNo).FirstOrDefault();

                    }


                    NObj.ResultCount = result.Count();
                    NObj.memberNoticeList = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return NObj;
                }
            }
            catch
            {
                throw;
            }

        }


        static int GetNoticePendingCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tMemberNotice>)GetNoticePendingList();
                //return result.ToList().Count();
                var result = (tMemberNotice)GetNoticePendingList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePendingList(int PageSize, int PageIndex, int AssemId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tMemberNotice NObj = new tMemberNotice();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from MN in db.tMemberNotices
                                  where MN.NoticeNumber != null
                                  && ((MN.NoticeStatus == (int)NoticeStatusEnum.NoticePending) || (MN.NoticeStatus == (int)NoticeStatusEnum.NoticeFreeze))
                                  && (MN.AssemblyID == model.AssemblyCode)
                                  && (MN.SessionID == model.SessionCode)
                                  // for noticepending
                                  select MN).ToList();

                    foreach (var item in result)
                    {
                        item.RuleNo = (from e in db.mEvents where e.EventId == item.NoticeTypeID select e.RuleNo).FirstOrDefault();

                    }


                    NObj.ResultCount = result.Count();
                    NObj.memberNoticeList = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return NObj;
                }
            }
            catch
            {
                throw;
            }
        }

        //Total List(Pending and Sent)
        static object GetAllStarredQuestionList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }

            tQuestion Obj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var pending = (from Q in db.tQuestions
                                   where ((Q.QuestionStatus == (int)Questionstatus.QuestionPending) || (Q.QuestionStatus == (int)Questionstatus.QuestionAssignTypist)
                                  || (Q.QuestionStatus == (int)Questionstatus.QuestionFreeze) || (Q.QuestionStatus == (int)Questionstatus.QuestionProofReading))
                                   //&& (Q.DiaryNumber != null)
                                   && (Q.AssemblyID == model.AssemblyCode)
                                   && (Q.SessionID == model.SessionCode)// for question
                                   && (Q.QuestionType == (int)QuestionType.StartedQuestion)// for starred
                                   orderby Q.QuestionID
                                   select Q).ToList();
                    var sent = (from Q in db.tQuestions
                                where (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                                && (Q.IsSubmitted == true)
                                && (Q.AssemblyID == model.AssemblyCode)
                                && (Q.SessionID == model.SessionCode)// for question
                                && (Q.QuestionType == (int)QuestionType.StartedQuestion)// for starred
                                orderby Q.QuestionID
                                select Q).ToList().ToList();

                    var result = new List<tQuestion>();
                    foreach (var item in pending)
                    {
                        item.IsPending = true;
                        result.Add(item);
                    }
                    foreach (var item in sent)
                    {
                        item.IsPending = false;
                        result.Add(item);
                    }



                    Obj.ResultCount = result.Count();
                    Obj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return Obj;
                }
            }
            catch
            {
                throw;
            }
        }
        //Total List(Pending and Sent)
        static object GetAllUnStarredQuestionList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();

            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }


            tQuestion Obj = new tQuestion();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var pending = (from Q in db.tQuestions
                                   where ((Q.QuestionStatus == (int)Questionstatus.QuestionPending) || (Q.QuestionStatus == (int)Questionstatus.QuestionAssignTypist)
                                   || (Q.QuestionStatus == (int)Questionstatus.QuestionProofReading) || (Q.QuestionStatus == (int)Questionstatus.QuestionFreeze))
                                   //&& (Q.DiaryNumber != null)
                                   && (Q.AssemblyID == model.AssemblyCode)
                                   && (Q.SessionID == model.SessionCode)// for question
                                  && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)// for Unstarred
                                   orderby Q.QuestionID
                                   select Q).ToList();
                    var sent = (from Q in db.tQuestions
                                where (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                                && (Q.IsSubmitted == true)
                                && (Q.AssemblyID == model.AssemblyCode)
                                && (Q.SessionID == model.SessionCode)// for question
                                && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)// for Unstarred
                                orderby Q.QuestionID
                                select Q).ToList().ToList();

                    var result = new List<tQuestion>();
                    foreach (var item in pending)
                    {
                        item.IsPending = true;
                        result.Add(item);
                    }
                    foreach (var item in sent)
                    {
                        item.IsPending = false;
                        result.Add(item);
                    }

                    Obj.ResultCount = result.Count();
                    Obj.QuestionListForTrasData = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return Obj;

                }
            }
            catch
            {
                throw;
            }
        }
        //Total List(Pending and Sent)
        static object GetAllNoticeList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }

            tMemberNotice NObj = new tMemberNotice();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var pending = (from MN in db.tMemberNotices
                                   where MN.NoticeNumber != null
                                   && ((MN.NoticeStatus == (int)NoticeStatusEnum.NoticePending) || (MN.NoticeStatus == (int)NoticeStatusEnum.NoticeFreeze))
                                   && (MN.AssemblyID == model.AssemblyCode)
                                   && (MN.SessionID == model.SessionCode)
                                   // for noticepending
                                   select MN).ToList();
                    var sent = (from MN in db.tMemberNotices
                                where MN.NoticeNumber != null
                                && (MN.NoticeStatus == (int)NoticeStatusEnum.NoticeSent)
                                && (MN.IsSubmitted == true)
                                && (MN.AssemblyID == model.AssemblyCode)
                                && (MN.SessionID == model.SessionCode)
                                // for noticepending
                                select MN).ToList();

                    var result = new List<tMemberNotice>();
                    foreach (var item in pending)
                    {
                        item.IsPending = true;
                        item.RuleNo = (from e in db.mEvents where e.EventId == item.NoticeTypeID select e.RuleNo).FirstOrDefault();
                        result.Add(item);
                    }
                    foreach (var item in sent)
                    {
                        item.IsPending = false;
                        item.RuleNo = (from e in db.mEvents where e.EventId == item.NoticeTypeID select e.RuleNo).FirstOrDefault();
                        result.Add(item);
                    }
                    NObj.ResultCount = result.Count();
                    NObj.memberNoticeList = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return NObj;
                }
            }
            catch
            {
                throw;
            }
        }

        static int GetNoticePaperCurrentLobCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetNoticePaperCurrentLobList();
                //return result.ToList().Count();
                var result = (tMemberNotice)GetNoticePaperCurrentLobList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePaperCurrentLobList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tMemberNotice NObj = new tMemberNotice();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 4 // for notice
                                  && PL.AssemblyId == model.AssemblyCode
                                  && PL.SessionId == model.SessionCode
                                  select MN).ToList();
                    foreach (var item in result)
                    {
                        item.RuleNo = (from e in db.mEvents where e.EventId == item.NoticeTypeID select e.RuleNo).FirstOrDefault();

                    }
                    NObj.ResultCount = result.Count();
                    NObj.memberNoticeList = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return NObj;
                }
            }
            catch
            {
                throw;
            }
        }


        static int GetNoticePaperLaidInHomeCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetNoticePaperLaidInHomeList();
                //return result.ToList().Count();
                var result = (tMemberNotice)GetNoticePaperLaidInHomeList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePaperLaidInHomeList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tMemberNotice NObj = new tMemberNotice();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 4 // for notice
                                  && PL.AssemblyId == model.AssemblyCode
                                  && PL.SessionId == model.SessionCode
                                  select MN).ToList();
                    NObj.ResultCount = result.Count();
                    NObj.memberNoticeList = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return NObj;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePaperPendingToLayList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemblyId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tMemberNotice NObj = new tMemberNotice();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && E.PaperCategoryTypeId == 4 // for notice
                                  && PL.AssemblyId == model.AssemblyCode
                                  && PL.SessionId == model.SessionCode
                                  select MN).ToList();

                    foreach (var item in result)
                    {
                        item.RuleNo = (from e in db.mEvents where e.EventId == item.NoticeTypeID select e.RuleNo).FirstOrDefault();

                    }

                    NObj.ResultCount = result.Count();
                    NObj.memberNoticeList = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return NObj;
                }
            }
            catch
            {
                throw;
            }
        }


        #endregion

        #region Bills
        static int GetBillsPaperRecievedCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetBillsPaperRecievedList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetBillsPaperRecievedList(1, 1, DM.AssemblyID, DM.SessionID, DM.AskedBy);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }
        static object GetBillsPaperRecievedList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {

            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {



                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                  join dept in db.mDepartment on B.DepartmentId equals dept.deptId
                                  where (PL.LOBRecordId == null)
                                  && (E.PaperCategoryTypeId == 3)
                                  && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                  && PLT.DeptSubmittedBy != null
                                  && PLT.DeptSubmittedDate != null
                                  // && PLT.MinisterSubmittedBy != null
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  //  && B.Status == 1
                                  select new { PL, PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName, dept.deptname ,E.EventName}).ToList();

                    List<tPaperLaidV> paperlaidlist = new List<tPaperLaidV>();

                    foreach (var item in result)
                    {
                        tPaperLaidV model = new tPaperLaidV();
                        model = item.PL;
                        model.MinistryName = item.MinistryName;
                        model.MinisterName = item.deptname;
                        model.BillNumber = item.BillNumber;
                        model.BussinessType = item.EventName;
                        //model.BOTStatus = item.Status;
                        paperlaidlist.Add(model);

                    }

                    PLObj.ResultCount = paperlaidlist.Count();
                    PLObj.ListtPaperLaidV = paperlaidlist.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }
        static int GetMemoCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            SiteSettings SSMdl = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                SSMdl.AssemblyCode = DM.AssemblyID;
                SSMdl.SessionCode = DM.SessionID;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {



                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                  join dept in db.mDepartment on B.DepartmentId equals dept.deptId
                                  where (PL.LOBRecordId == null)
                                  && (E.PaperCategoryTypeId == 3)
                                  && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                  && PLT.DeptSubmittedBy != null
                                  && PLT.DeptSubmittedDate != null
                                  // && PLT.MinisterSubmittedBy != null
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  //  && B.Status == 1
                                  select new { PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName, dept.deptname }).ToList();

                    return result.Count;
                }
            }
            catch
            {
                throw;
            }
        }



        static object GetBillsPaperRecievedList(int PageSize, int PageIndex, int AssemblyId, int SessionId, string UserId)
        {

            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    List<tPaperLaidV> paperlaidlist = new List<tPaperLaidV>();

                    if (UserId.ToUpper() == "4B29A0BD-C553-4070-A760-66C279B7419B")
                    {

                        var result = (from PL in db.tPaperLaidVS
                                      join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                      join E in db.mEvents on PL.EventId equals E.EventId
                                      join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                      join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                      join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                      join dept in db.mDepartment on B.DepartmentId equals dept.deptId
                                      where (PL.LOBRecordId == null)
                                      && (E.PaperCategoryTypeId == 3)
                                      && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                      && PLT.DeptSubmittedBy != null
                                      && PLT.DeptSubmittedDate != null
                                      // && PLT.MinisterSubmittedBy != null
                                      && PL.AssemblyId == SSMdl.AssemblyCode
                                      && PL.SessionId == SSMdl.SessionCode
                                      //&& B.Status == 1
                                      select new { PL, PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName, dept.deptname, B.Status ,E.EventName}).ToList();
                        foreach (var item in result)
                        {
                            tPaperLaidV model = new tPaperLaidV();
                            model = item.PL;
                            model.MinistryName = item.MinistryName;
                            switch (item.Status)
                            {
                                case 0:
                                    model.MinisterName = "Sent By: " + item.deptname;
                                    break;

                                case 1:
                                    model.MinisterName = "Approved By Additional Commissioner and Send to Commissioner.";
                                    break;

                                case 2:
                                    model.MinisterName = "Approved By Commissioner and Send to Mayor.";
                                    break;

                                case 3:
                                    model.MinisterName = "Approved By Mayor and Send Back to Commissioner";
                                    break;

                                case 4:
                                    model.MinisterName = "Approved By Commissioner and Send back to Additional Commissioner";
                                    break;

                                case 5:
                                    model.MinisterName = "Approved By Additional Commissioner and Send to Meeting Assistance";
                                    break;

                                case 6:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner";
                                    break;

                                case 7:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner";
                                    break;

                                case 8:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send to Mayor";
                                    break;

                                case 9:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner";
                                    break;

                                case 10:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner";
                                    break;

                                case 11:
                                    model.MinisterName = "Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house";
                                    break;

                                default:
                                    break;

                            }



                            model.BillNumber = item.BillNumber;
                            model.BOTStatus = item.Status;
                            model.BussinessType = item.EventName;
                            paperlaidlist.Add(model);

                        }

                        PLObj.ResultCount = paperlaidlist.Count();
                        PLObj.ListtPaperLaidV = paperlaidlist.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                        return PLObj;
                    }
                    else
                    {


                        var result = (from PL in db.tPaperLaidVS
                                      join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                      join E in db.mEvents on PL.EventId equals E.EventId
                                      join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                      join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                      join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                      join dept in db.mDepartment on B.DepartmentId equals dept.deptId
                                      where (PL.LOBRecordId == null)
                                      && (E.PaperCategoryTypeId == 3)
                                      && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                      && PLT.DeptSubmittedBy != null
                                      && PLT.DeptSubmittedDate != null
                                      // && PLT.MinisterSubmittedBy != null
                                      && PL.AssemblyId == SSMdl.AssemblyCode
                                      && PL.SessionId == SSMdl.SessionCode
                                      //&& ( B.Status == 0 || B.Status ==2) 
                                      select new { PL, PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName, dept.deptname, B.Status, E.EventName }).ToList();
                        foreach (var item in result)
                        {
                            tPaperLaidV model = new tPaperLaidV();
                            model = item.PL;

                            switch (item.Status)
                            {
                                case 0:
                                    model.MinisterName = "Sent By: " + item.deptname;
                                    break;

                                case 1:
                                    model.MinisterName = "Approved By Additional Commissioner and Send to Commissioner.";
                                    break;

                                case 2:
                                    model.MinisterName = "Approved By Commissioner and Send to Mayor.";
                                    break;

                                case 3:
                                    model.MinisterName = "Approved By Mayor and Send Back to Commissioner";
                                    break;

                                case 4:
                                    model.MinisterName = "Approved By Commissioner and Send back to Additional Commissioner";
                                    break;

                                case 5:
                                    model.MinisterName = "Approved By Additional Commissioner and Send to Meeting Assistance";
                                    break;

                                case 6:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner";
                                    break;

                                case 7:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner";
                                    break;

                                case 8:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send to Mayor";
                                    break;

                                case 9:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner";
                                    break;

                                case 10:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner";
                                    break;

                                case 11:
                                    model.MinisterName = "Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house";
                                    break;

                                default:
                                    break;

                            }



                            model.BillNumber = item.BillNumber;
                            model.BOTStatus = item.Status;
                            model.BussinessType = item.EventName;
                            paperlaidlist.Add(model);

                        }

                        PLObj.ResultCount = paperlaidlist.Count();
                        PLObj.ListtPaperLaidV = paperlaidlist.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                        return PLObj;
                    }



                }
            }
            catch
            {
                throw;
            }
        }


        static object GetBillsPaperRecievedList(int PageSize, int PageIndex, int AssemblyId, int SessionId, string UserId, string UserType)
        {

            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    List<tPaperLaidV> paperlaidlist = new List<tPaperLaidV>();

                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                  join dept in db.mDepartment on B.DepartmentId equals dept.deptId
                                  where (PL.LOBRecordId == null)
                                  && (E.PaperCategoryTypeId == 3)
                                  && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                  && PLT.DeptSubmittedBy != null
                                  && PLT.DeptSubmittedDate != null
                                  // && PLT.MinisterSubmittedBy != null
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode

                                  //&& B.Status == 1
                                  select new { PL, PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName, dept.deptname, B.Status, E.EventName }).ToList();




                    //if (UserId.ToUpper() == "4B29A0BD-C553-4070-A760-66C279B7419B")
                    //{

                    //     result = (from dt in result where (dt.Status ==2 || dt.Status == 8) select dt).ToList();
                        
                    //}


                    //else if (UserId.ToLower()== "c3481e2b-f20c-452e-b5a5-37a0a3d16f08")
                    //{

                    //    result = (from dt in result where (dt.Status == 0 || dt.Status == 4 || dt.Status == 6 || dt.Status == 10) select dt).ToList();


                        

                    //}

                    //else if (UserId.ToLower() == "ed8dc044-81ed-4587-94e0-cd33047da89e")
                    //{
                    //    result = (from dt in result where (dt.Status == 1 || dt.Status == 3 || dt.Status == 7 || dt.Status == 9) select dt).ToList();


                    

                    //}

                 


                    foreach (var item in result)
                        {
                            tPaperLaidV model = new tPaperLaidV();
                            model = item.PL;
                            model.MinistryName = item.MinistryName;
                            switch (item.Status)
                            {
                                case 0:
                                    model.MinisterName = "Sent By: " + item.deptname;
                                    break;

                                case 1:
                                    model.MinisterName = "Approved By Additional Commissioner and Send to Commissioner.";
                                    break;

                                case 2:
                                    model.MinisterName = "Approved By Commissioner and Send to Mayor.";
                                    break;

                                case 3:
                                    model.MinisterName = "Approved By Mayor and Send Back to Commissioner";
                                    break;

                                case 4:
                                    model.MinisterName = "Approved By Commissioner and Send back to Additional Commissioner";
                                    break;

                                case 5:
                                    model.MinisterName = "Approved By Additional Commissioner and Send to Meeting Assistance";
                                    break;

                                case 6:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner";
                                    break;

                                case 7:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner";
                                    break;

                                case 8:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send to Mayor";
                                    break;

                                case 9:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner";
                                    break;

                                case 10:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner";
                                    break;

                                case 11:
                                    model.MinisterName = "Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house";
                                    break;

                                default:
                                    break;

                            }



                            model.BillNumber = item.BillNumber;
                            model.BOTStatus = item.Status;
                            model.BussinessType = item.EventName;
                            paperlaidlist.Add(model);

                        }

                        PLObj.ResultCount = paperlaidlist.Count();
                        PLObj.ListtPaperLaidV = paperlaidlist.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                        return PLObj;
                    
                



                }
            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperPendingList(int PageSize, int PageIndex, int AssemblyId, int SessionId, string UserId, string UserType)
        {

            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    List<tPaperLaidV> paperlaidlist = new List<tPaperLaidV>();

                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                  join dept in db.mDepartment on B.DepartmentId equals dept.deptId
                                  where (PL.LOBRecordId == null)
                                  && (E.PaperCategoryTypeId == 3)
                                  && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                  && PLT.DeptSubmittedBy != null
                                  && PLT.DeptSubmittedDate != null
                                  // && PLT.MinisterSubmittedBy != null
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode

                                  //&& B.Status == 1
                                  select new { PL, PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName, dept.deptname, B.Status, E.EventName }).ToList();




                    if (UserId.ToUpper() == "4B29A0BD-C553-4070-A760-66C279B7419B")
                    {

                        result = (from dt in result where (dt.Status == 2 || dt.Status == 8) select dt).ToList();

                    }


                    else if (UserId.ToLower() == "c3481e2b-f20c-452e-b5a5-37a0a3d16f08")
                    {

                        result = (from dt in result where (dt.Status == 0 || dt.Status == 4 || dt.Status == 6 || dt.Status == 10) select dt).ToList();




                    }

                    else if (UserId.ToLower() == "ed8dc044-81ed-4587-94e0-cd33047da89e")
                    {
                        result = (from dt in result where (dt.Status == 1 || dt.Status == 3 || dt.Status == 7 || dt.Status == 9) select dt).ToList();




                    }




                    foreach (var item in result)
                    {
                        tPaperLaidV model = new tPaperLaidV();
                        model = item.PL;
                        model.MinistryName = item.MinistryName;
                        switch (item.Status)
                        {
                            case 0:
                                model.MinisterName = "Sent By: " + item.deptname;
                                break;

                            case 1:
                                model.MinisterName = "Approved By Additional Commissioner and Send to Commissioner.";
                                break;

                            case 2:
                                model.MinisterName = "Approved By Commissioner and Send to Mayor.";
                                break;

                            case 3:
                                model.MinisterName = "Approved By Mayor and Send Back to Commissioner";
                                break;

                            case 4:
                                model.MinisterName = "Approved By Commissioner and Send back to Additional Commissioner";
                                break;

                            case 5:
                                model.MinisterName = "Approved By Additional Commissioner and Send to Meeting Assistance";
                                break;

                            case 6:
                                model.MinisterName = "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner";
                                break;

                            case 7:
                                model.MinisterName = "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner";
                                break;

                            case 8:
                                model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send to Mayor";
                                break;

                            case 9:
                                model.MinisterName = "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner";
                                break;

                            case 10:
                                model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner";
                                break;

                            case 11:
                                model.MinisterName = "Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house";
                                break;

                            default:
                                break;

                        }



                        model.BillNumber = item.BillNumber;
                        model.BOTStatus = item.Status;
                        model.BussinessType = item.EventName;
                        paperlaidlist.Add(model);

                    }

                    PLObj.ResultCount = paperlaidlist.Count();
                    PLObj.ListtPaperLaidV = paperlaidlist.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;





                }
            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperRecievedListTobLaid(int PageSize, int PageIndex, int AssemblyId, int SessionId, string UserId, string UserType)
        {

            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    List<tPaperLaidV> paperlaidlist = new List<tPaperLaidV>();

                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                  join dept in db.mDepartment on B.DepartmentId equals dept.deptId
                                  where (PL.LOBRecordId == null)
                                  && (E.PaperCategoryTypeId == 3)
                                  && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                  && PLT.DeptSubmittedBy != null
                                  && PLT.DeptSubmittedDate != null
                                  // && PLT.MinisterSubmittedBy != null
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  && B.Status >10
                                  //&& B.Status == 1
                                  select new { PL, PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName, dept.deptname, B.Status, E.EventName }).ToList();




                   

                 


                    foreach (var item in result)
                        {
                            tPaperLaidV model = new tPaperLaidV();
                            model = item.PL;
                            model.MinistryName = item.MinistryName;
                            switch (item.Status)
                            {
                                case 0:
                                    model.MinisterName = "Sent By: " + item.deptname;
                                    break;

                                case 1:
                                    model.MinisterName = "Approved By Additional Commissioner and Send to Commissioner.";
                                    break;

                                case 2:
                                    model.MinisterName = "Approved By Commissioner and Send to Mayor.";
                                    break;

                                case 3:
                                    model.MinisterName = "Approved By Mayor and Send Back to Commissioner";
                                    break;

                                case 4:
                                    model.MinisterName = "Approved By Commissioner and Send back to Additional Commissioner";
                                    break;

                                case 5:
                                    model.MinisterName = "Approved By Additional Commissioner and Send to Meeting Assistance";
                                    break;

                                case 6:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner";
                                    break;

                                case 7:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner";
                                    break;

                                case 8:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send to Mayor";
                                    break;

                                case 9:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner";
                                    break;

                                case 10:
                                    model.MinisterName = "Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner";
                                    break;

                                case 11:
                                    model.MinisterName = "Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house";
                                    break;

                                default:
                                    break;

                            }



                            model.BillNumber = item.BillNumber;
                            model.BOTStatus = item.Status;
                            model.BussinessType = item.EventName;
                            paperlaidlist.Add(model);

                        }

                        PLObj.ResultCount = paperlaidlist.Count();
                        PLObj.ListtPaperLaidV = paperlaidlist.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                        return PLObj;
                    
                



                }
            }
            catch
            {
                throw;
            }
        }




        static int GetBillsPaperCurrentLobCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetBillsPaperCurrentLobList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetBillsPaperCurrentLobList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;

            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperCurrentLobList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 3 // for bills
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();

                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }

        }

        static int GetBillsPaperLaidInHomeCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetBillsPaperLaidInHomeList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetBillsPaperLaidInHomeList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperLaidInHomeList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 3 // for bills
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();

                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperPendingToLayList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && E.PaperCategoryTypeId == 3 // for bills
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();
                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Other Paper Types
        static int GetOtherPaperToLayRecievedCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetOtherPaperToLayRecievedList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetOtherPaperToLayRecievedList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }


        static object GetOtherPaperToLayRecievedList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join p in db.tPaperLaidTemp on PL.PaperLaidId equals p.PaperLaidId
                                  join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                  where PL.LOBRecordId == null && PC.PaperCategoryTypeId == 5
                                        && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                        && PLT.DeptSubmittedBy != null
                                        && PLT.DeptSubmittedDate != null
                                        && PLT.MinisterSubmittedBy != null// for paper lay 
                                        && PL.AssemblyId == SSMdl.AssemblyCode
                                        && PL.SessionId == SSMdl.SessionCode
                                  select new { PL, PL.Title, Min.MinisterName, Min.MinistryName }).ToList();
                    List<tPaperLaidV> paperlaidlist = new List<tPaperLaidV>();
                    foreach (var item in result)
                    {
                        tPaperLaidV model = new tPaperLaidV();
                        model = item.PL;
                        model.MinistryName = item.MinistryName;
                        model.MinisterName = item.MinisterName;
                        //model.BillNumber = item.BillNumber;
                        paperlaidlist.Add(model);

                    }

                    PLObj.ResultCount = paperlaidlist.Count();
                    PLObj.ListtPaperLaidV = paperlaidlist.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }


        static int GetOtherPaperToLayCurrentLobCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetOtherPaperToLayCurrentLobList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetOtherPaperToLayCurrentLobList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetOtherPaperToLayCurrentLobList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();

                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }



        static int GetOtherPaperToLayLaidInHomeCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetOtherPaperToLayLaidInHomeList();
                //return result.ToList().Count();

                var result = (tPaperLaidV)GetOtherPaperToLayLaidInHomeList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetOtherPaperToLayLaidInHomeList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LaidDate != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();

                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetOtherPaperPendingToLayList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();
                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Committee

        static int GetCommittePaperRecievedCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetCommittePaperRecievedList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetCommittePaperRecievedList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetCommittePaperRecievedList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {

                    var result = (from b in db.tPaperLaidVS
                                  join PLT in db.tPaperLaidTemp on b.PaperLaidId equals PLT.PaperLaidId
                                  join e in db.mEvents on b.EventId equals e.EventId
                                  join pc in db.mPaperCategoryTypes on e.PaperCategoryTypeId equals pc.PaperCategoryTypeId
                                  join c in db.tCommittees on b.CommitteeId equals c.CommitteeId
                                  join Min in db.mMinistry on b.MinistryId equals Min.MinistryID
                                  where b.LOBRecordId == null
                                  && e.PaperCategoryTypeId == 3
                                  && PLT.PaperLaidTempId == b.DeptActivePaperId
                                  && PLT.DeptSubmittedBy != null
                                  && PLT.DeptSubmittedDate != null
                                  && PLT.MinisterSubmittedDate != null
                                  && b.AssemblyId == SSMdl.AssemblyCode
                                  && b.SessionId == SSMdl.SessionCode
                                  select new { b, b.Title, Min.MinisterName, Min.MinistryName }).ToList();
                    List<tPaperLaidV> paperlaidlist1 = new List<tPaperLaidV>();
                    foreach (var item in result)
                    {
                        tPaperLaidV model1 = new tPaperLaidV();
                        model1 = item.b;
                        model1.MinisterName = item.MinisterName;
                        model1.MinistryName = item.MinistryName;
                        paperlaidlist1.Add(model1);
                    }

                    PLObj.ResultCount = paperlaidlist1.Count();
                    PLObj.ListtPaperLaidV = paperlaidlist1.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;

                }
            }
            catch
            {
                throw;
            }
        }


        static int GetCommittePaperCurrentLobCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetCommittePaperCurrentLobList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetCommittePaperCurrentLobList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetCommittePaperCurrentLobList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 3 // for committee
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();

                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }


        }

        static int GetCommittePaperLaidInHomeCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetCommittePaperLaidInHomeList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetCommittePaperLaidInHomeList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetCommittePaperLaidInHomeList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LaidDate != null && PC.PaperCategoryTypeId == 3 // for committee
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();

                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetCommittePaperPendingToLayList(int PageSize, int PageIndex, int AssemblyId, int SessionId)
        {
            SiteSettings SSMdl = new SiteSettings();
            if (AssemblyId != 0 && SessionId != 0)
            {
                SSMdl.AssemblyCode = AssemblyId;
                SSMdl.SessionCode = SessionId;
            }
            else
            {
                SSMdl = (SiteSettings)GetAllSiteSettings();
            }
            tPaperLaidV PLObj = new tPaperLaidV();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && PC.PaperCategoryTypeId == 3 // for committee
                                  && PL.AssemblyId == SSMdl.AssemblyCode
                                  && PL.SessionId == SSMdl.SessionCode
                                  select PL).ToList();
                    PLObj.ResultCount = result.Count();
                    PLObj.ListtPaperLaidV = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return PLObj;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion





        #endregion

        #region Paper Laid Summary For A Paper Category Type By Minister/Department/Status :Not Required

        #region Qusetion


        static List<PaperLaidByPaperCategoryType> GetStarredQuestionPaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for unstarred
                                  select PL).ToList();

                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetStarredQuestionPaperCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        static List<PaperLaidByPaperCategoryType> GetStarredQuestionPaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetUnStarredQuestionPaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetUnStarredQuestionPaperCurrentLob(bool ByMinister)
        {

            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetUnStarredQuestionPaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Notice
        static List<PaperLaidByPaperCategoryType> GetNoticePaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetNoticePaperCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetNoticePaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Bills
        static List<PaperLaidByPaperCategoryType> GetBillsPaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 3// for bills
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetBillsPaperCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 3 // for bills
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetBillsPaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 3 // for bills
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Other Paper Types
        static List<PaperLaidByPaperCategoryType> GetPaperToLayRecieved(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId == null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetPaperToLayCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetPaperToLayLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LaidDate != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Committee

        static List<PaperLaidByPaperCategoryType> GetCommittePaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId == null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetCommittePaperCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DepartmentId, Id = group.Key.DeparmentName, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetCommittePaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LaidDate != null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Common
        static object GetPaperLaidSummaryForPaperCategory(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;

            int PaperCategoryTypeId = paper.EventId;
            bool IsStarredQuestion = paper.AssemblyId == 0 ? false : true;
            bool IsMinistryLevel = paper.SessionId == 0 ? false : true;

            List<PaperLaidByPaperCategoryType> result = new List<PaperLaidByPaperCategoryType>();

            if (PaperCategoryTypeId == 1)
            {
                if (IsStarredQuestion)
                {
                    foreach (var item in GetStarredQuestionPaperRecieved(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    foreach (var item in GetStarredQuestionPaperCurrentLob(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    foreach (var item in GetStarredQuestionPaperLaidInHome(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    return result;

                }
                else
                {

                    foreach (var item in GetUnStarredQuestionPaperRecieved(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    foreach (var item in GetUnStarredQuestionPaperCurrentLob(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    foreach (var item in GetUnStarredQuestionPaperLaidInHome(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    return result;

                }
            }
            if (PaperCategoryTypeId == 3)
            {
                foreach (var item in GetBillsPaperRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetBillsPaperCurrentLob(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetBillsPaperLaidInHome(IsMinistryLevel))
                {
                    result.Add(item);
                }
                return result;
            }
            if (PaperCategoryTypeId == 3)
            {

                foreach (var item in GetCommittePaperRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetCommittePaperCurrentLob(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetCommittePaperLaidInHome(IsMinistryLevel))
                {
                    result.Add(item);
                }
                return result;
            }
            if (PaperCategoryTypeId == 4)
            {
                foreach (var item in GetNoticePaperRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetNoticePaperRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetNoticePaperLaidInHome(IsMinistryLevel))
                {
                    result.Add(item);
                }
                return result;
            }
            if (PaperCategoryTypeId == 5)
            {
                foreach (var item in GetPaperToLayRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetPaperToLayCurrentLob(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetPaperToLayLaidInHome(IsMinistryLevel))
                {
                    result.Add(item);
                }
                return result;
            }


            return null;
        }

        static object GetCompletePaperLaidDetailsByMinistry(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;
            var MinistryId = paper.MinistryId;
            var PaperCategoryId = paper.PaperCategoryTypeId;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  where PL.MinistryId == MinistryId && E.PaperCategoryTypeId == PaperCategoryId
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetCompletePaperLaidDetailsByPaperLaid(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;

            var PaperCategoryId = paper.PaperCategoryTypeId;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  where E.PaperCategoryTypeId == PaperCategoryId
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetCompletePaperLaidDetailsByDepartment(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;
            var DepartmentId = paper.DepartmentId;
            var PaperCategoryId = paper.PaperCategoryTypeId;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  where PL.DepartmentId == DepartmentId && E.PaperCategoryTypeId == PaperCategoryId
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetCompletePaperLaidDetailsByStatus(object param)
        {
            tPaperLaidV parameter = param as tPaperLaidV;
            int PaperCategoryTypeId = parameter.PaperCategoryTypeId;
            bool IsStarred = parameter.QuestionID == 0 ? false : true;
            string Status = parameter.Title;
            var result = new List<tPaperLaidV>();
            switch (Status)
            {
                case "All"://for pending+sent
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetAllStarredQuestionList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                            else
                                return GetAllUnStarredQuestionList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetAllNoticeList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        break;
                    }
                case "PendingList":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPendingList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                            else
                                return GetUnStarredQuestionPendingList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePendingList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        break;
                    }
                case "Sent":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionSentList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                            else
                                return GetUnStarredQuestionSentList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticeSentList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        break;
                    }
                case "Recieved":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPaperRecievedList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                            else
                                return GetUnStarredQuestionPaperRecievedList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 2)
                        {
                            return GetBillsPaperRecievedList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId, parameter.UserID.ToString(),parameter.UserName);
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetBillsPaperRecievedList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId, parameter.UserID.ToString(), parameter.UserName);
                            //return GetCommittePaperRecievedList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePaperRecievedList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 5)
                        {
                            return GetOtherPaperToLayRecievedList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        break;
                    }
                case "LaidInHome":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPaperLaidInHomeList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                            else
                                return GetUnStarredQuestionPaperLaidInHomeList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetBillsPaperLaidInHomeList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetCommittePaperLaidInHomeList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePaperLaidInHomeList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 5)
                        {
                            return GetOtherPaperToLayLaidInHomeList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        break;
                    }
                case "CurrentLob":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPaperCurrentLobList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                            else
                                return GetUnStarredQuestionPaperCurrentLobList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetBillsPaperCurrentLobList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetCommittePaperCurrentLobList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePaperCurrentLobList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 5)
                        {
                            return GetOtherPaperToLayCurrentLobList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        break;
                    }
                case "Pending":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPaperPendingToLayList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                            else
                                return GetUnStarredQuestionPaperPendingToLayList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetBillsPaperPendingToLayList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetCommittePaperPendingToLayList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePaperPendingToLayList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        else if (PaperCategoryTypeId == 5)
                        {
                            return GetOtherPaperPendingToLayList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId);
                        }
                        break;
                    }


                case "Laid":
                    return GetBillsPaperRecievedListTobLaid(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId, parameter.UserID.ToString(), parameter.UserName);
                case "MemoPending":
                    return GetBillsPaperPendingList(parameter.PAGE_SIZE, parameter.PageIndex, parameter.AssemblyId, parameter.SessionId, parameter.UserID.ToString(), parameter.UserName);

                default: return null;
            }
            return null;
        }

        static object GetQuestionDetailsById(object param)
        {
            tQuestion parameter = param as tQuestion;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    return db.tQuestions.SingleOrDefault(a => a.QuestionID == parameter.QuestionID);
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticeDetailsById(object param)
        {
            tMemberNotice parameter = param as tMemberNotice;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    return db.tMemberNotices.SingleOrDefault(a => a.NoticeId == parameter.NoticeId);
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetMinistryById(object param)
        {
            tQuestion parameter = param as tQuestion;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    return db.mMinistry.SingleOrDefault(a => a.MinistryID == parameter.MinistryId);
                }
            }
            catch
            {
                throw;
            }
        }


        static object GetDepartmentById(object param)
        {
            mDepartment parameter = param as mDepartment;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    return db.mDepartment.SingleOrDefault(a => a.deptId == parameter.deptId);
                }
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllDetailByPaperType(object param)
        {
            tPaperLaidV PLV = param as tPaperLaidV;
            List<tPaperLaidV> paperlaidlist = new List<tPaperLaidV>();
            //SiteSettings SSM = new SiteSettings();
            //SSM = (SiteSettings)GetAllSiteSettings();
            LegislationFixationContext db = new LegislationFixationContext();

            if (PLV.PaperTypeName == "Bill")
            {

                var Receve = (from PL in db.tPaperLaidVS
                              join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                              join E in db.mEvents on PL.EventId equals E.EventId
                              join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                              join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                              join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                              where (PL.LOBRecordId == null)
                              && (E.PaperCategoryTypeId == 3)
                              && PLT.PaperLaidTempId == PL.DeptActivePaperId
                              && PLT.DeptSubmittedBy != null
                              && PLT.DeptSubmittedDate != null
                              && PLT.MinisterSubmittedBy != null
                              && PL.AssemblyId == PLV.AssemblyId
                              && PL.SessionId == PLV.SessionId
                              select new { PL, PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName }).ToList();


                foreach (var item in Receve)
                {
                    tPaperLaidV model = new tPaperLaidV();
                    model = item.PL;
                    model.MinistryName = item.MinistryName;
                    model.MinisterName = item.MinisterName;
                    model.BillNumber = item.BillNumber;
                    paperlaidlist.Add(model);

                }

                var UC = (from PL in db.tPaperLaidVS
                          join E in db.mEvents on PL.EventId equals E.EventId
                          join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                          join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                          join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                          where PL.LOBRecordId != null && E.PaperCategoryTypeId == 3 // for bills
                          && PL.AssemblyId == PLV.AssemblyId
                          && PL.SessionId == PLV.SessionId
                          //select PL).ToList();
                          select new { PL, PL.Title, B.BillNumber, Min.MinisterName, Min.MinistryName }).ToList();

                foreach (var item in UC)
                {
                    tPaperLaidV model = new tPaperLaidV();
                    model = item.PL;
                    model.MinistryName = item.MinistryName;
                    model.MinisterName = item.MinisterName;
                    model.BillNumber = item.BillNumber;
                    paperlaidlist.Add(model);

                }

                PLV.ListtPaperLaidV = paperlaidlist;

                return PLV.ListtPaperLaidV;
            }

            else if (PLV.PaperTypeName == "Committee")
            {
                var CReceive = (from b in db.tPaperLaidVS
                                join PLT in db.tPaperLaidTemp on b.PaperLaidId equals PLT.PaperLaidId
                                join e in db.mEvents on b.EventId equals e.EventId
                                join pc in db.mPaperCategoryTypes on e.PaperCategoryTypeId equals pc.PaperCategoryTypeId
                                join c in db.tCommittees on b.CommitteeId equals c.CommitteeId
                                join Min in db.mMinistry on b.MinistryId equals Min.MinistryID
                                where b.LOBRecordId == null
                                && e.PaperCategoryTypeId == 3
                                && PLT.PaperLaidTempId == b.DeptActivePaperId
                                && PLT.DeptSubmittedBy != null
                                && PLT.DeptSubmittedDate != null
                                && PLT.MinisterSubmittedDate != null
                                && b.AssemblyId == PLV.AssemblyId
                                && b.SessionId == PLV.SessionId
                                select new { b, b.Title, Min.MinisterName, Min.MinistryName }).ToList();

                foreach (var item in CReceive)
                {
                    tPaperLaidV model1 = new tPaperLaidV();
                    model1 = item.b;
                    model1.MinisterName = item.MinisterName;
                    model1.MinistryName = item.MinistryName;
                    paperlaidlist.Add(model1);
                }

                var ComUp = (from PL in db.tPaperLaidVS
                             join E in db.mEvents on PL.EventId equals E.EventId
                             join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                             join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                             join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                             where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 3 // for committee
                             && PL.AssemblyId == PLV.AssemblyId
                             && PL.SessionId == PLV.SessionId
                             select new { PL, PL.Title, Min.MinisterName, Min.MinistryName }).ToList();

                foreach (var item in ComUp)
                {
                    tPaperLaidV model1 = new tPaperLaidV();
                    model1 = item.PL;
                    model1.MinisterName = item.MinisterName;
                    model1.MinistryName = item.MinistryName;
                    paperlaidlist.Add(model1);
                }

                PLV.ListtPaperLaidV = paperlaidlist;

                return PLV.ListtPaperLaidV;
            }

            else if (PLV.PaperTypeName == "OtherPaper")
            {
                var OPReceve = (from PL in db.tPaperLaidVS
                                join PLT in db.tPaperLaidTemp on PL.PaperLaidId equals PLT.PaperLaidId
                                join E in db.mEvents on PL.EventId equals E.EventId
                                join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                join p in db.tPaperLaidTemp on PL.PaperLaidId equals p.PaperLaidId
                                join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                                where PL.LOBRecordId == null && PC.PaperCategoryTypeId == 5
                                      && PLT.PaperLaidTempId == PL.DeptActivePaperId
                                      && PLT.DeptSubmittedBy != null
                                      && PLT.DeptSubmittedDate != null
                                      && PLT.MinisterSubmittedBy != null// for paper lay 
                                       && PL.AssemblyId == PLV.AssemblyId
                                        && PL.SessionId == PLV.SessionId
                                select new { PL, PL.Title, Min.MinisterName, Min.MinistryName }).ToList();

                foreach (var item in OPReceve)
                {
                    tPaperLaidV model = new tPaperLaidV();
                    model = item.PL;
                    model.MinistryName = item.MinistryName;
                    model.MinisterName = item.MinisterName;
                    paperlaidlist.Add(model);

                }

                var OPUC = (from PL in db.tPaperLaidVS
                            join E in db.mEvents on PL.EventId equals E.EventId
                            join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                            join Min in db.mMinistry on PL.MinistryId equals Min.MinistryID
                            where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                             && PL.AssemblyId == PLV.AssemblyId
                             && PL.SessionId == PLV.SessionId
                            select new { PL, PL.Title, Min.MinisterName, Min.MinistryName }).ToList();

                foreach (var item in OPUC)
                {
                    tPaperLaidV model = new tPaperLaidV();
                    model = item.PL;
                    model.MinistryName = item.MinistryName;
                    model.MinisterName = item.MinisterName;
                    paperlaidlist.Add(model);

                }

                PLV.ListtPaperLaidV = paperlaidlist;

                return PLV.ListtPaperLaidV;

            }






            return null;
        }

        #endregion


        #endregion


        #region added by nitin



        static int GetOtherPaperPendingToLayListCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetOtherPaperPendingToLayList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetOtherPaperPendingToLayList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }
        static int GetCommittePaperPendingToLayListCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetCommittePaperPendingToLayList(1,1);
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetCommittePaperPendingToLayList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }





        static int GetStarredQuestionPaperPendingToLayListCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tQuestion>)GetStarredQuestionPaperPendingToLayList();
                //return result.ToList().Count();
                var result = (tQuestion)GetStarredQuestionPaperPendingToLayList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static int GetBillsPaperPendingToLayListCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetBillsPaperPendingToLayList();
                //return result.ToList().Count();
                var result = (tPaperLaidV)GetBillsPaperPendingToLayList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;

            }
            catch
            {
                throw;
            }
        }
        static int GetNoticePaperPendingToLayListCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetNoticePaperPendingToLayList();
                //return result.ToList().Count();
                var result = (tMemberNotice)GetNoticePaperPendingToLayList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }


        static int GetUnStarredQuestionPaperPendingToLayListCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tPaperLaidV>)GetUnStarredQuestionPaperPendingToLayList();
                //return result.ToList().Count();

                var result = (tQuestion)GetUnStarredQuestionPaperPendingToLayList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static List<mEvent> GetOtherPaperLaidPaperCategoryType(object param)
        {
            LegislationFixationContext db = new LegislationFixationContext();

            mEvent model = param as mEvent;


            var query = (from dist in db.mEvents
                         where dist.PaperCategoryTypeId == 5
                         select dist);

            //string searchText = (model.EventName != null && model.EventName != "") ? model.EventName.ToLower() : "";
            //if (searchText != "")
            //{
            //    query = query.Where(a =>
            //        a.EventName != null && a.EventName.ToLower().Contains(searchText));

            //}

            return query.ToList();
        }
        static object GetUpdatedOtherPaperLaidCounters(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            model.TotalOtherpaperLaidUpdated = GetUpdatedOtherPaperList(model).ResultCount;
            return model;
        }
        static tPaperLaidV GetUpdatedOtherPaperList(object param)
        {
            LegislationFixationContext db = new LegislationFixationContext();
            tPaperLaidV model = param as tPaperLaidV;
            int paperCategoryId = 5;
            int? EventId = model.PaperCategoryTypeId;
            if (model.PageIndex == 0)
            {
                model.PageIndex = 1;
            }
            if (model.PAGE_SIZE == 0)
            {
                model.PAGE_SIZE = 20;
            }



            //          var query = (from  paperLaidTemp in pCtxt.tPaperLaidTemp 
            //                       join paperLaid in pCtxt.tPaperLaidV on paperLaidTemp.PaperLaidId equals paperLaid.PaperLaidId
            //                       where  paperLaid.DepartmentId == model.DepartmentId
            //&& paperLaidTemp.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
            //&& paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
            //&& paperLaid.DesireLayingDate > DateTime.Now && paperLaid.LaidDate == null && paperLaid.LOBRecordId != null
            //&& paperLaid.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId



            var query = (from m in db.tPaperLaidVS
                         join c in db.mEvents on m.EventId equals c.EventId
                         join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID

                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         where m.DeptActivePaperId == t.PaperLaidTempId
                         && t.DeptSubmittedBy != null
                         && t.DeptSubmittedDate != null
                         && t.MinisterSubmittedBy != null
                         && t.MinisterSubmittedDate != null
                         && m.DesireLayingDate > DateTime.Now
                         && m.LaidDate == null
                         && m.LOBRecordId != null
                         && m.MinisterActivePaperId == t.PaperLaidTempId
                         && c.PaperCategoryTypeId == paperCategoryId
                         && m.LOBPaperTempId < m.MinisterActivePaperId
                         select new PaperMovementModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             EventId = c.EventId,
                             Title = m.Title,
                             DeparmentName = m.DeparmentName,
                             MinisterName = ministery.MinistryName,
                             PaperTypeID = (int)m.PaperLaidId,
                             actualFilePath = t.FilePath + t.FileName,
                             PaperCategoryTypeId = c.PaperCategoryTypeId,
                             BusinessType = c.EventName,
                             version = t.Version,
                             FilePath = t.FileName,
                             DeptSubmittedDate = t.DeptSubmittedDate,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryTypes join e in db.mEvents on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault()

                         }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.DepartmentSubmitList = results;


            return model;
        }
        static object ShowUpdatedOtherPaperLaidDetailByID(object param)
        {
            LegislationFixationContext db = new LegislationFixationContext();
            PaperMovementModel model = param as PaperMovementModel;

            var query = (from tPaper in db.tPaperLaidVS
                         where model.PaperLaidId == tPaper.PaperLaidId
                         join mEvents in db.mEvents on tPaper.EventId equals mEvents.EventId
                         join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                         join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                         join tPaperTemp in db.tPaperLaidTemp on tPaper.DeptActivePaperId equals tPaperTemp.PaperLaidTempId
                         join mPaperCategory in db.mPaperCategoryTypes on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                         select new PaperMovementModel
                         {

                             EventName = mEvents.EventName,
                             MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
                             DeparmentName = mDept.deptname,
                             PaperLaidId = tPaper.PaperLaidId,
                             actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
                             ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                             FilePath = tPaperTemp.FilePath,
                             Title = tPaper.Title,
                             Description = tPaper.Description,
                             Remark = tPaper.Remark,
                             PaperCategoryTypeId = mEvents.PaperCategoryTypeId,
                             PaperCategoryTypeName = mPaperCategory.Name,
                             PaperSubmittedDate = tPaperTemp.DeptSubmittedDate

                         }).SingleOrDefault();
            model = query;
            model.SubmittedFileListByPaperLaidId = (from tPaperTemp in db.tPaperLaidTemp
                                                    where tPaperTemp.PaperLaidId == query.PaperLaidId && tPaperTemp.DeptSubmittedBy != null && tPaperTemp.DeptSubmittedDate
                            != null
                                                    select tPaperTemp).ToList();


            return model;
        }
        static object GetDetailsByPaperLaidId(object param)
        {
            LegislationFixationContext db = new LegislationFixationContext();
            PaperMovementModel model = param as PaperMovementModel;
            var query = (from mdl in db.tPaperLaidVS
                         where mdl.PaperLaidId == model.PaperLaidId
                         join mdlTemp in db.tPaperLaidTemp on mdl.DeptActivePaperId equals mdlTemp.PaperLaidTempId
                         join mdlAdminLOB in db.AdminLOB on mdl.LOBRecordId equals mdlAdminLOB.Id
                         select new PaperMovementModel
                         {
                             PaperLaidId = model.PaperLaidId,
                             MinisterActivePaperId = mdl.MinisterActivePaperId,
                             DeptActivePaperId = mdl.DeptActivePaperId,
                             actualFilePath = mdlTemp.SignedFilePath,
                             FilePath = mdlAdminLOB.PDFLocation
                         }).SingleOrDefault();
            return query;

        }

        static object UpdateLOBTempIdByPaperLiadID(object param)
        {
            PaperMovementModel model = param as PaperMovementModel;
            LegislationFixationContext db = new LegislationFixationContext();
            var query = (from mdl in db.tPaperLaidVS where model.PaperLaidId == mdl.PaperLaidId select mdl).SingleOrDefault();
            query.LOBPaperTempId = Convert.ToInt32(model.MinisterActivePaperId);
            db.SaveChanges();
            db.Close();
            return model;
        }


        #endregion

        #region "This is method for Edit of Question And Notice Copied from Diary context By Sunil"
        static object GetMemberNameByID(object param)
        {
            mMember parameter = param as mMember;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    return db.mMember.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
                }
            }

            catch
            {
                throw;
            }
        }
        public static object GetQuestionByIdForEdit(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;
            DiaryModel Obj2 = new DiaryModel();
            var record = (from Ques in DiaCtx.objQuestion
                          where Ques.QuestionID == Obj.QuestionId
                          select new DiaryModel
                          {
                              AssemblyID = Ques.AssemblyID,
                              SessionID = Ques.SessionID,
                              QuestionId = Ques.QuestionID,
                              QuestionTypeId = Ques.QuestionType,
                              DiaryNumber = Ques.DiaryNumber,
                              MemberId = Ques.MemberID,
                              ConstituencyName = Ques.ConstituencyName,
                              EventId = Ques.EventId,
                              CatchWordSubject = Ques.SubInCatchWord,
                              Subject = Ques.Subject,
                              MinistryId = Ques.MinistryId,
                              DepartmentId = Ques.DepartmentID,
                              MainQuestion = Ques.MainQuestion,
                              SessionDateId = Ques.SessionDateId,
                              QuestionNumber = Ques.QuestionNumber,
                              IsContentFreeze = Ques.IsContentFreeze,
                              IsDetailFreeze = Ques.IsQuestionDetailFreez,
                              IsQuestionFreeze = Ques.IsQuestionFreeze,
                              DateForBind = Ques.NoticeRecievedDate,
                              IsInitialApprove = Ques.IsInitialApproved,
                              QuestionStatus = Ques.QuestionStatus,
                              IsTranslation = Ques.IsTranslation,
                              FileName = Ques.OriDiaryFileName,
                              FilePath = Ques.OriDiaryFilePath,
                              IsQuestionInPart = Ques.IsQuestionInPart,
                              IsHindi = Ques.IsHindi,

                          }).FirstOrDefault();
            Obj2 = record;

            /*Get All Session date according to session id */
            var SDt = (from SD in SCtx.mSessDate
                       where SD.SessionId == record.SessionID
                       select SD).ToList();

            foreach (var item in SDt)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }
            Obj2.SessDateList = SDt;



            /*Get All Event or Category*/

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 1
                           select Evnt).ToList();
            Obj2.eventList = EvtList;

            /*Get All MinistryMinister*/
            var MinMisList = (from Min in DiaCtx.objmMinistry
                              join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
                              orderby Min.OrderID
                              where minis.AssemblyID == record.AssemblyID && Min.IsActive == true && Min.AssemblyID == record.AssemblyID
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
                              }).ToList();
            Obj2.memMinList = MinMisList;

            /*Get All Department*/
            if (record.MinistryId != null && record.MinistryId != 0)
            {
                var DeptList = (from minsDept in DiaCtx.objMinisDepart
                                join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
                                where minsDept.MinistryID == record.MinistryId
                                orderby dept.deptname
                                select new DiaryModel
                                {
                                    DepartmentId = dept.deptId,
                                    DepartmentName = dept.deptname

                                }).ToList();

                Obj2.DiaryList = DeptList;
            }
            /*Get  Member Name*/

            if (record.MemberId != 0)
            {
                var Member = (from Mem in DiaCtx.objmMember
                              where Mem.MemberCode == record.MemberId
                              select Mem).FirstOrDefault();

                Obj2.MemberName = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
            }

            DiaCtx.Close();


            return Obj2;

        }

        public static object GetNoticeByIdForEdit(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();

            var q = (from assemblyid in DiaCtx.mSiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);

            var PndList = (from Ntces in DiaCtx.ObjNotices
                           where Ntces.NoticeId == Obj.NoticeId
                           select new DiaryModel
                           {
                               NoticeNumber = Ntces.NoticeNumber,
                               MemId = Ntces.MemberId,
                               EventId = Ntces.NoticeTypeID,
                               CatchWordSubject = Ntces.SubInCatchWord,
                               Subject = Ntces.Subject,
                               MinistryId = Ntces.MinistryId,
                               DepartmentId = Ntces.DepartmentId,
                               DateForBind = Ntces.NoticeDate,
                               TimeForBind = Ntces.NoticeTime,
                               NoticeId = Ntces.NoticeId,
                               Notice = Ntces.Notice,
                               NoticeStatus = Ntces.NoticeStatus,
                               DemandNo = Ntces.DemandID,
                               AttachFileName = Ntces.CutMotionFileName,
                               AttachFilePath = Ntces.CutMotionFilePath,
                               ManualNoticeNumber = Ntces.ManualNoticeNumber,
                           }).FirstOrDefault();
            Obj2 = PndList;

            var FL = (from SS in pCtxt.SiteSettings
                      where SS.SettingName == "FileAccessingUrlPath"
                      select SS.SettingValue).FirstOrDefault();

            Obj2.AttachFileName = FL + "/" + PndList.AttachFilePath + PndList.AttachFileName;
            ///*Get All Session date according to session id */
            //var SDt = (from SD in SCtx.mSessDate
            //           where SD.SessionId == Obj.SessionID
            //           select SD).ToList();

            //foreach (var item in SDt)
            //{
            //    item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            //}
            //Obj2.SessDateList = SDt;



            /*Get All Event or Category*/

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 4
                           select Evnt).ToList();
            Obj2.eventList = EvtList;

            /*Get All MinistryMinister*/
            var MinMisList = (from Min in DiaCtx.objmMinistry
                              join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
                              orderby Min.OrderID
                              where minis.AssemblyID == aid && Min.IsActive == true && Min.AssemblyID == aid
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
                              }).ToList();
            Obj2.memMinList = MinMisList;

            /*Get All Department*/
            if (PndList.MinistryId != null && PndList.MinistryId != 0)
            {
                var DeptList = (from minsDept in DiaCtx.objMinisDepart
                                join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
                                where minsDept.MinistryID == PndList.MinistryId
                                select new DiaryModel
                                {
                                    DepartmentId = dept.deptId,
                                    DepartmentName = dept.deptname

                                }).ToList();

                Obj2.DiaryList = DeptList;
            }
            /*Get All Member*/
            if (PndList.MemId != 0 && PndList.MemId != null)
            {
                var Member = (from Mem in DiaCtx.objmMember
                              where Mem.MemberCode == PndList.MemId
                              select Mem).FirstOrDefault();

                Obj2.MemberName = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
            }
            /*Get All Demand*/
            var DemandList = (from Demand in DiaCtx.tCutMotionDemand
                              select Demand).ToList();
            Obj2.DemandList = DemandList;
            DiaCtx.Close();


            return Obj2;
        }

        public static object GetDepartmentByMinister(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            var q = (from assemblyid in DiaCtx.mSiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);
            var DeptList = (from minsDept in DiaCtx.objMinisDepart
                            join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
                            //	join SDept in DiaCtx.SecDept on dept.deptId equals SDept.DepartmentID
                            where minsDept.MinistryID == Obj.MinistryId && minsDept.AssemblyID == aid && minsDept.IsDeleted == null
                            orderby dept.deptname
                            select new DiaryModel
                            {
                                DepartmentId = dept.deptId,
                                DepartmentName = dept.deptname
                            }).Distinct().OrderBy(x => x.DepartmentName).ToList();
            //}).Distinct().ToList();
            DiaCtx.Close();

            return DeptList;

        }

        public static object GetConstByMemberId(object param)
        {
            if (null == param)
            {
                return null;
            }

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            var Constituency = (from C in DiaCtx.ObjConst
                                join MA in DiaCtx.ObjMemAssem on C.ConstituencyCode equals MA.ConstituencyCode
                                where MA.MemberID == Obj.MemberId
                                && C.AssemblyID == Obj.AssemblyID
                                && MA.AssemblyID == Obj.AssemblyID
                                select new DiaryModel
                                {
                                    ConstituencyCode = C.ConstituencyCode,
                                    ConstituencyName = C.ConstituencyName

                                }).FirstOrDefault();
            return Constituency;
        }

        public static object SaveQuestion(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tQuestion Obj2 = new tQuestion();
            string msg = "";
            Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == Obj.QuestionId);
            DiaCtx.objQuestion.Attach(Obj2);
           
            try
            {

                if (Obj.ViewType == "QInbox")
                {
                    if (Obj.BtnCaption == "Save")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.SessionDateId = Obj.SessionDateId;
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionPending;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        if (Obj.IsQuestionInPart == true)
                        {
                            Obj2.IsQuestionInPart = true;
                        }
                        else
                        {
                            Obj2.IsQuestionInPart = false;
                        }
                        if (Obj.IsHindi == true)
                        {
                            Obj2.IsHindi = true;
                        }
                        else
                        {
                            Obj2.IsHindi = false;
                        }
                        DiaCtx.SaveChanges();
                        msg = "Successfully Saved !";
                    }
                    else if (Obj.BtnCaption == "Send")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.SessionDateId = Obj.SessionDateId;
                        Obj2.IsQuestionFreeze = true;
                        Obj2.IsQuestionFreezeDate = DateTime.Now;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        if (Obj.IsQuestionInPart == true)
                        {
                            Obj2.IsQuestionInPart = true;
                        }
                        else
                        {
                            Obj2.IsQuestionInPart = false;
                        }
                        if (Obj.IsHindi == true)
                        {
                            Obj2.IsHindi = true;
                        }
                        else
                        {
                            Obj2.IsHindi = false;
                        }
                        //Obj2.QuestionStatus = (int)Questionstatus.QuestionSent;
                        //Obj2.IsSubmitted = true;
                        //Obj2.SubmittedDate = DateTime.Now;
                        //Obj2.SubmittedBy = Obj.SubmittedBy;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Sent !";
                    }

                }
                else if (Obj.ViewType == "QDraft")
                {
                    if (Obj.BtnCaption == "Save")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.SessionDateId = Obj.SessionDateId;
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionPending;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        if (Obj.IsQuestionInPart == true)
                        {
                            Obj2.IsQuestionInPart = true;
                        }
                        else
                        {
                            Obj2.IsQuestionInPart = false;
                        }
                        if (Obj.IsHindi == true)
                        {
                            Obj2.IsHindi = true;
                        }
                        else
                        {
                            Obj2.IsHindi = false;
                        }
                        DiaCtx.SaveChanges();
                        msg = "Successfully Saved !";
                    }
                    else if (Obj.BtnCaption == "Send")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.SessionDateId = Obj.SessionDateId;
                        Obj2.IsQuestionFreeze = true;
                        Obj2.IsQuestionFreezeDate = DateTime.Now;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        if (Obj.IsQuestionInPart == true)
                        {
                            Obj2.IsQuestionInPart = true;
                        }
                        else
                        {
                            Obj2.IsQuestionInPart = false;
                        }

                        if (Obj.IsHindi == true)
                        {
                            Obj2.IsHindi = true;
                        }
                        else
                        {
                            Obj2.IsHindi = false;
                        }
                        //Obj2.QuestionStatus = (int)Questionstatus.QuestionSent;
                        //Obj2.IsSubmitted = true;
                        //Obj2.SubmittedDate = DateTime.Now;
                        //Obj2.SubmittedBy = Obj.SubmittedBy;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Freeze !";
                    }

                    //CancelledQuestionSave
                    else if (Obj.BtnCaption == "CancelledQuestionSave")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.IsTranslation = true;
                        Obj2.IsFixed = true;
                        Obj2.IsFixedDate = Obj.FixSessionDate;
                        Obj2.SessionDateId = Obj.FixSessionDateId;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        if (Obj.IsQuestionInPart == true)
                        {
                            Obj2.IsQuestionInPart = true;
                        }
                        else
                        {
                            Obj2.IsQuestionInPart = false;
                        }

                        if (Obj.IsHindi == true)
                        {
                            Obj2.IsHindi = true;
                        }
                        else
                        {
                            Obj2.IsHindi = false;
                        }
                        //Obj2.QuestionStatus = (int)Questionstatus.QuestionPending;
                        DiaCtx.SaveChanges();
                        msg = "Detail Saved Successfully !";
                    }
                    else if (Obj.BtnCaption == "SaveBefore")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.IsTranslation = true;
                        if (Obj.IsQuestionInPart == true)
                        {
                            Obj2.IsQuestionInPart = true;
                        }
                        else
                        {
                            Obj2.IsQuestionInPart = false;
                        }

                        if (Obj.IsHindi == true)
                        {
                            Obj2.IsHindi = true;
                        }
                        else
                        {
                            Obj2.IsHindi = false;
                        }
                        //Obj2.QuestionStatus = (int)Questionstatus.QuestionPending;
                        DiaCtx.SaveChanges();
                        msg = "Detail Saved Successfully !";
                    }
                    else if (Obj.BtnCaption == "FreezeBefore")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionFreeze;
                        Obj2.IsQuestionDetailFreez = true;
                        Obj2.IsQuestionDetailFreezDate = DateTime.Now;
                        if (Obj.IsQuestionInPart == true)
                        {
                            Obj2.IsQuestionInPart = true;
                        }
                        else
                        {
                            Obj2.IsQuestionInPart = false;
                        }
                        if (Obj.IsHindi == true)
                        {
                            Obj2.IsHindi = true;
                        }
                        else
                        {
                            Obj2.IsHindi = false;
                        }
                        DiaCtx.SaveChanges();
                        var result = SkipQuestionProcessSteps.SkippingQuesProcessingStepsAfterFreezings(Obj.QuestionId);
                        msg = "Detail Freezed Successfully !";

                        // Save Question For Tracking in TQuestionAuditTrial
                        TQuestionAuditTrial QuesObj = new TQuestionAuditTrial();
                        QuesObj.MainQuesionId = Obj.QuestionId;
                        QuesObj.EventId = Obj.EventId;
                        QuesObj.MinistryId = Obj.MinistryId;
                        QuesObj.DepartmentID = Obj.DepartmentId;
                        QuesObj.MemberID = Obj.MemberId;
                        QuesObj.Subject = Obj.Subject;
                        QuesObj.MainQuestion = Obj.MainQuestion;
                        QuesObj.ConstituencyName = Obj.ConstituencyName;
                        QuesObj.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        QuesObj.QuestionStatus = (int)Questionstatus.QuestionFreeze;
                        QuesObj.IsQuestionDetailFreez = true;
                        QuesObj.IsQuestionDetailFreezDate = DateTime.Now;
                        QuesObj.RecordBy = "Legislation";
                        if (Obj.IsQuestionInPart == true)
                        {
                            QuesObj.IsQuestionInPart = true;
                        }
                        else
                        {
                            QuesObj.IsQuestionInPart = false;
                        }
                        if (Obj.IsHindi == true)
                        {
                            QuesObj.IsHindi = true;
                        }
                        else
                        {
                            QuesObj.IsHindi = false;
                        }

                        DiaCtx.tQuestionAuditTrial.Add(QuesObj);
                        DiaCtx.SaveChanges();

                    }
                    //else if (Obj.BtnCaption == "ApproveBefore")
                    //{
                    //    Obj2.IsInitialApproved = true;
                    //    Obj2.IsInitialApprovedDate = DateTime.Now;
                    //    DiaCtx.SaveChanges();
                    //    msg = "Approved Successfully !";
                    //}
                    //else if (Obj.BtnCaption == "UnFreezeBefore")
                    //{
                    //    Obj2.IsQuestionDetailFreez = null;
                    //    Obj2.IsQuestionDetailFreezDate = null;
                    //    DiaCtx.SaveChanges();
                    //    msg = "Detail Unfreezed Successfully !";
                    //}
                    else if (Obj.BtnCaption == "ApproveAfter")
                    {
                        //Obj2.IsFinalApproved = true;
                        //Obj2.IsFinalApprovedDate = DateTime.Now;
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionSent;
                        Obj2.IsSubmitted = true;
                        Obj2.SubmittedDate = DateTime.Now;
                        Obj2.SubmittedBy = Obj.SubmittedBy;
                        DiaCtx.SaveChanges();
                        var result = SkipQuestionProcessSteps.SkipQuesProcessingStep7LegiFixing(Obj.QuestionId,Obj.SessionDateId);
                        msg = Obj2.SubmittedDate + "_" + "Approved Successfully !";
                        //msg = "Approved Successfully !";
                    }
                    else if (Obj.BtnCaption == "UnFreezeAfter")
                    {
                        //Obj2.IsQuestionFreeze = null;
                        //Obj2.IsQuestionFreezeDate = null;
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionProofReading;
                        DiaCtx.SaveChanges();
                        msg = "Unfreezed Successfully !";
                    }
                    else if (Obj.BtnCaption == "ChangeContent")
                    {
                        //Obj2.IsQuestionFreeze = null;
                        //Obj2.IsQuestionFreezeDate = null;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        DiaCtx.SaveChanges();
                        msg = "Content Changed Successfully !";
                    }



                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            DiaCtx.Close();

            return msg;

        }

        public static object SaveNotice(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tMemberNotice Obj2 = new tMemberNotice();
            string msg = "";
            Obj2 = DiaCtx.ObjNotices.Single(m => m.NoticeId == Obj.NoticeId);
            DiaCtx.ObjNotices.Attach(Obj2);
            try
            {

                if (Obj.ViewType == "NInbox")
                {
                    if (Obj.BtnCaption == "Save")
                    {
                        Obj2.NoticeTypeID = Obj.EventId;

                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Saved !";
                    }
                    else if (Obj.BtnCaption == "Send")
                    {
                        Obj2.NoticeTypeID = Obj.EventId;

                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        Obj2.IsNoticeFreeze = true;
                        Obj2.IsNoticeFreezeDate = DateTime.Now;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticeSent;
                        Obj2.IsSubmitted = true;
                        Obj2.SubmittedDate = DateTime.Now;
                        Obj2.SubmittedBy = Obj.SubmittedBy;
                        Obj2.Actice = true;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Sent !";
                    }

                }
                else if (Obj.ViewType == "NDraft")
                {
                    if (Obj.BtnCaption == "Save")
                    {
                        Obj2.NoticeTypeID = Obj.EventId;
                        if (Obj2.NoticeTypeID == 74 || Obj2.NoticeTypeID == 75 || Obj2.NoticeTypeID == 76)
                        {
                            Obj2.DemandID = Obj.DemandNo;
                        }
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        Obj2.ManualNoticeNumber = Obj.ManualNoticeNumber;
                        if (Obj2.NoticeStatus != 3)
                            Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Saved !";
                    }
                    else if (Obj.BtnCaption == "Send")
                    {
                        Obj2.NoticeTypeID = Obj.EventId;
                        if (Obj2.NoticeTypeID == 74 || Obj2.NoticeTypeID == 75 || Obj2.NoticeTypeID == 76)
                        {
                            Obj2.DemandID = Obj.DemandNo;
                        }
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        Obj2.IsNoticeFreeze = true;
                        Obj2.IsNoticeFreezeDate = DateTime.Now;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticeFreeze;
                        Obj2.Actice = true;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Freeze !";
                    }
                    else if (Obj.BtnCaption == "ApproveAfter")
                    {
                        Obj2.IsSubmitted = true;
                        Obj2.SubmittedDate = DateTime.Now;
                        Obj2.SubmittedBy = Obj.SubmittedBy;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticeSent;
                        Obj2.NoticeTypeID = Obj.EventId;
                        //for update
                        if (Obj2.NoticeTypeID == 74 || Obj2.NoticeTypeID == 75 || Obj2.NoticeTypeID == 76)
                        {
                            Obj2.DemandID = Obj.DemandNo;
                            Obj2.Actice = true;
                        }
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        DiaCtx.SaveChanges();
                        msg = "Approved Successfully !";
                    }
                    else if (Obj.BtnCaption == "UnFreezeAfter")
                    {
                        //Obj2.IsNoticeFreeze = null;
                        //Obj2.IsNoticeFreezeDate = null;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
                        DiaCtx.SaveChanges();
                        msg = "Unfreezed Successfully !";
                    }

                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            DiaCtx.Close();

            return msg;

        }

        public static object GetQuestionDetailById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();

            var record = (from Ques in DiaCtx.objQuestion
                          join sdt in DiaCtx.objSessDate on Ques.SessionDateId equals sdt.Id
                          join evt in DiaCtx.objmEvent on Ques.EventId equals evt.EventId
                          join Mem in DiaCtx.objmMember on Ques.MemberID equals Mem.MemberCode

                          where Ques.QuestionID == Obj.QuestionId
                          select new DiaryModel
                          {
                              QuestionId = Ques.QuestionID,
                              QuestionTypeId = Ques.QuestionType,
                              DiaryNumber = Ques.DiaryNumber,
                              QuestionNumber = Ques.QuestionNumber,
                              MemberName = Mem.Name,
                              ConstituencyName = Ques.ConstituencyName,
                              EventName = evt.EventName,
                              CatchWordSubject = Ques.SubInCatchWord,
                              Subject = Ques.Subject,
                              MinistryId = Ques.MinistryId,
                              DepartmentId = Ques.DepartmentID,
                              MainQuestion = Ques.MainQuestion,
                              NoticeDate = sdt.SessionDate,

                          }).FirstOrDefault();

            var MinMisList = "";
            var Dept = "";
            if (record.MinistryId != null && record.MinistryId != 0)
            {
                MinMisList = (from Min in DiaCtx.objmMinistry
                              where Min.MinistryID == record.MinistryId
                              select Min.MinisterName).FirstOrDefault();

            }


            if (record.DepartmentId != null && record.DepartmentId != "")
            {
                Dept = (from dept in DiaCtx.objmDepartment
                        where dept.deptId == record.DepartmentId
                        select dept.deptname).FirstOrDefault();
            }

            Obj2 = record;
            Obj2.MinisterName = MinMisList.ToString().Trim();
            Obj2.DepartmentName = Dept.ToString().Trim();
            DiaCtx.Close();

            return Obj2;
        }
        public static object GetNoticeDetailById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();

            var record = (from Ntces in DiaCtx.ObjNotices
                          where Ntces.NoticeId == Obj.NoticeId
                          join evt in DiaCtx.objmEvent on Ntces.NoticeTypeID equals evt.EventId
                          join Mem in DiaCtx.objmMember on Ntces.MemberId equals Mem.MemberCode
                          select new DiaryModel
                          {
                              NoticeNumber = Ntces.NoticeNumber,
                              MemberName = Mem.Name,
                              EventName = evt.EventName,
                              CatchWordSubject = Ntces.SubInCatchWord,
                              Subject = Ntces.Subject,
                              MinistryId = Ntces.MinistryId,
                              DepartmentId = Ntces.DepartmentId,
                              DateForBind = Ntces.NoticeDate,
                              TimeForBind = Ntces.NoticeTime,
                              NoticeId = Ntces.NoticeId,
                              Notice = Ntces.Notice,
                          }).FirstOrDefault();
            var MinMisList = "";
            var Dept = "";
            if (record.MinistryId != null && record.MinistryId != 0)
            {
                MinMisList = (from Min in DiaCtx.objmMinistry
                              where Min.MinistryID == record.MinistryId
                              select Min.MinisterName).FirstOrDefault();

            }


            if (record.DepartmentId != null && record.DepartmentId != "")
            {
                Dept = (from dept in DiaCtx.objmDepartment
                        where dept.deptId == record.DepartmentId
                        select dept.deptname).FirstOrDefault();
            }

            Obj2 = record;
            Obj2.MinisterName = MinMisList.ToString().Trim();
            Obj2.DepartmentName = Dept.ToString().Trim();
            DiaCtx.Close();


            return Obj2;
        }

        public static object CheckQuestionNo(object param)
        {

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel updateSQ = param as DiaryModel;

            var query = (from tQ in DiaCtx.objQuestion
                         where tQ.QuestionNumber == updateSQ.QuestionNumber
                         && (tQ.AssemblyID == updateSQ.AssemblyID)
                         && (tQ.QuestionType == updateSQ.QuestionTypeId)
                         select tQ).ToList().Count();

            return query.ToString();
        }


        public static object GetAllDiariedRecord(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            DiaryModel Obj2 = new DiaryModel();

            if (Obj.PaperEntryType == "Starred")
            {
                IEnumerable<DiaryModel> starredDiaried = (from Ques in DiaCtx.objQuestion
                                                          join mem in DiaCtx.objmMember on Ques.MemberID equals mem.MemberCode
                                                          where (Ques.AssemblyID == Obj.AssemblyID)
                                                          && (Ques.SessionID == Obj.SessionID)
                                                          && (Ques.QuestionType == (int)QuestionType.StartedQuestion)
                                                          //&& (Ques.DiaryNumber != null)
                                                          && (Ques.QuestionStatus != 0)
                                                          orderby Ques.QuestionID
                                                          select new DiaryModel
                                                          {
                                                              DiaryNumber = Ques.DiaryNumber,
                                                              MemberId = Ques.MemberID,
                                                              //AskedBy = mem.Name,
                                                              DateForBind = Ques.NoticeRecievedDate,
                                                              QuestionId = Ques.QuestionID,
                                                          }).ToList();

                foreach (var item in starredDiaried)
                {
                    if (item.MemberId != 0)
                    {
                        var Member = (from Mem in DiaCtx.objmMember
                                      where Mem.MemberCode == item.MemberId
                                      select Mem).FirstOrDefault();

                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
                    }
                }
                var results = starredDiaried.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();
                Obj2.DiaryList = results.ToList();
                Obj2.ResultCount = starredDiaried.ToList().Count();
                DiaCtx.Close();
                return Obj2;
            }
            else if (Obj.PaperEntryType == "Unstarred")
            {
                IEnumerable<DiaryModel> UnstarredDiaried = (from Ques in DiaCtx.objQuestion
                                                            join mem in DiaCtx.objmMember on Ques.MemberID equals mem.MemberCode
                                                            where (Ques.AssemblyID == Obj.AssemblyID)
                                                            && (Ques.SessionID == Obj.SessionID)
                                                            && (Ques.QuestionType == (int)QuestionType.UnstaredQuestion)
                                                             //&& (Ques.DiaryNumber != null)
                                                             && (Ques.QuestionStatus != 0)
                                                            orderby Ques.QuestionID
                                                            select new DiaryModel
                                                            {
                                                                DiaryNumber = Ques.DiaryNumber,
                                                                MemberId = Ques.MemberID,
                                                                //AskedBy = mem.Name,
                                                                DateForBind = Ques.NoticeRecievedDate,
                                                                QuestionId = Ques.QuestionID,
                                                            }).ToList();
                foreach (var item in UnstarredDiaried)
                {
                    if (item.MemberId != 0)
                    {
                        var Member = (from Mem in DiaCtx.objmMember
                                      where Mem.MemberCode == item.MemberId
                                      select Mem).FirstOrDefault();

                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
                    }
                }

                var results = UnstarredDiaried.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();
                Obj2.DiaryList = results.ToList();
                Obj2.ResultCount = UnstarredDiaried.ToList().Count();
                DiaCtx.Close();
                return Obj2;
            }
            else if (Obj.PaperEntryType == "Notice")
            {
                IEnumerable<DiaryModel> NoticeDiaried = (from Ntces in DiaCtx.ObjNotices
                                                         join mem in DiaCtx.objmMember on Ntces.MemberId equals mem.MemberCode
                                                         where (Ntces.AssemblyID == Obj.AssemblyID)
                                                         && (Ntces.SessionID == Obj.SessionID)
                                                         && (Ntces.NoticeNumber != null)
                                                          && (Ntces.NoticeStatus != 0)
                                                         orderby Ntces.NoticeId
                                                         select new DiaryModel
                                                         {
                                                             NoticeNumber = Ntces.NoticeNumber,
                                                             MemId = Ntces.MemberId,
                                                             //AskedBy = mem.Name,
                                                             DateForBind = Ntces.NoticeDate,
                                                             NoticeId = Ntces.NoticeId,
                                                         }).ToList();

                foreach (var item in NoticeDiaried)
                {
                    if (item.MemId != 0 && item.MemId != null)
                    {
                        var Member = (from Mem in DiaCtx.objmMember
                                      where Mem.MemberCode == item.MemId
                                      select Mem).FirstOrDefault();

                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
                    }
                }


                var results = NoticeDiaried.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();
                Obj2.DiaryList = results.ToList();
                Obj2.ResultCount = NoticeDiaried.ToList().Count();
                DiaCtx.Close();
                return Obj2;
            }
            return null;
        }

        public static object GetQuesByIdForRejection(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var record = (from Ques in DiaCtx.objQuestion
                              //join sdt in DiaCtx.objSessDate on Ques.SessionDateId equals sdt.Id
                              //join evt in DiaCtx.objmEvent on Ques.EventId equals evt.EventId
                              //join Mem in DiaCtx.objmMember on Ques.MemberID equals Mem.MemberCode

                          where Ques.QuestionID == Obj.QuestionId
                          select new DiaryModel
                          {
                              QuestionId = Ques.QuestionID,
                              DiaryNumber = Ques.DiaryNumber,
                              Subject = Ques.Subject,
                              DateForBind = Ques.NoticeRecievedDate

                          }).FirstOrDefault();

            return record;

        }

        public static object GetRejectRule(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();

            var record = (from Ques in DiaCtx.ObjQuesRule
                          select Ques).ToList();


            return record;
        }

        public static object RejectQuestionById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tQuestion Obj2 = new tQuestion();
            tQuestionRuleRegister RRQ = new tQuestionRuleRegister();
            string msg = "";
            try
            {
                Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == Obj.QuestionId);
                DiaCtx.objQuestion.Attach(Obj2);
                Obj2.IsRejected = true;
                Obj2.IsRejectedDate = DateTime.Now;
                Obj2.RejectRemarks = Obj.RejectRemark;
                //Obj2.QuestionStatus = (int)Questionstatus.QuestionRejected;
                DiaCtx.SaveChanges();

                string[] ids = Obj.RIds.Split(',');

                for (int i = 0; i < ids.Length; i++)
                {
                    RRQ.RulesId = Convert.ToInt16(ids[i]);
                    RRQ.QuestionId = Obj.QuestionId;
                    DiaCtx.ObjRuleRegister.Add(RRQ);
                    DiaCtx.SaveChanges();
                }

                msg = "Success";
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return msg;
        }
        public static object AdmitRejectedQuesById(object param)
        {

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Dmdl = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();


            try
            {
                QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == Dmdl.QuestionId);
                DiaCtx.objQuestion.Attach(QuesObj);

                // Again Admit Rejected Question
                QuesObj.IsRejected = null;
                QuesObj.IsRejectedDate = null;
                QuesObj.RejectRemarks = null;

                if (QuesObj.QuestionType == (int)QuestionType.StartedQuestion)
                {
                    Dmdl.Message = "S_Question Readmited Successfully !";
                }
                else if (QuesObj.QuestionType == (int)QuestionType.UnstaredQuestion)
                {
                    Dmdl.Message = "U_Question Readmited Successfully !";
                }

                DiaCtx.SaveChanges();


                //Get Updated Count
                var UCnt = (from Q in DiaCtx.objQuestion
                            where (Q.AssemblyID == QuesObj.AssemblyID)
                            && (Q.SessionID == QuesObj.SessionID)
                            && (Q.QuestionType == QuesObj.QuestionType)
                             && (Q.IsRejected == true)
                            select Q).ToList().Count();

                Dmdl.STotalCnt = UCnt;
                DiaCtx.Close();
            }
            catch (Exception ex)
            {
                Dmdl.Message = ex.Message;
            }

            return Dmdl;
        }

        public static object GetRulesforRejectedQuestionById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            int Qid = (int)param;
            var ruleLst = (from QR in DiaCtx.ObjQuesRule
                           join Qrt in DiaCtx.ObjRuleRegister on QR.QuestionRuleId equals Qrt.RulesId
                           where Qrt.QuestionId == Qid
                           select new DiaryModel
                           {
                               RuleId = QR.QuestionRuleId,
                               RuleName = QR.Rules

                           }).ToList();

            return ruleLst;
        }

        public static object ChangeQuestionTypeById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tQuestion Obj2 = new tQuestion();
            string msg = "";
            try
            {
                Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == Obj.QuestionId);
                DiaCtx.objQuestion.Attach(Obj2);

                if (Obj2.QuestionType == (int)QuestionType.StartedQuestion)
                {
                    Obj2.QuestionType = (int)QuestionType.UnstaredQuestion;
                    Obj2.OldQuestionType = (int)QuestionType.StartedQuestion;
                }


                Obj2.IsTypeChange = true;
                Obj2.IsTypeChangeDate = DateTime.Now;
                Obj2.ChangeTypeRemarks = Obj.ChangeTypeRemarks;
                //Obj2.QuestionStatus = (int)Questionstatus.QuestionFreeze;
                DiaCtx.SaveChanges();

                //msg = "Success";
                msg = "Type Changed to Unstarred Sucessfully !";

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return msg;
        }

        public static object MoveToUnstarredByIds(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tQuestion Obj2 = new tQuestion();
            tQuestion MergeDiary = new tQuestion();
            int AssemId = 0;
            int SessId = 0;
            try
            {
                string[] Ids = Obj.QuesIds.Split(',');
                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);
                    Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                    AssemId = Obj2.AssemblyID;
                    SessId = Obj2.SessionID;
                    DiaCtx.objQuestion.Attach(Obj2);

                    if (Obj2.IsBracket != true)
                    {
                        // Change All Brackted Question's Type
                        if (Obj2.MergeDiaryNo != null && Obj2.MergeDiaryNo != "")
                        {
                            string[] MergerDiaryNum = Obj2.MergeDiaryNo.Split();
                            for (int j = 0; j < MergerDiaryNum.Length; j++)
                            {
                                string MDN = MergerDiaryNum[j].ToString().Trim();
                                MergeDiary = DiaCtx.objQuestion.Single(m => m.DiaryNumber == MDN);
                                DiaCtx.objQuestion.Attach(MergeDiary);

                                MergeDiary.QuestionType = (int)QuestionType.UnstaredQuestion;
                                MergeDiary.OldQuestionType = (int)QuestionType.StartedQuestion;
                                MergeDiary.IsTypeChange = true;
                                MergeDiary.IsTypeChangeDate = DateTime.Now;
                                MergeDiary.ChangeTypeRemarks = Obj.ChangeTypeRemarks;
                            }

                        }

                        // Change parent Question's Type
                        Obj2.QuestionType = (int)QuestionType.UnstaredQuestion;
                        Obj2.OldQuestionType = (int)QuestionType.StartedQuestion;
                        Obj2.IsTypeChange = true;
                        Obj2.IsTypeChangeDate = DateTime.Now;
                        Obj2.ChangeTypeRemarks = Obj.ChangeTypeRemarks;
                        DiaCtx.SaveChanges();

                    }
                    else
                    {
                        Obj.Message = "U_Type Can't be change, Because Diary Number " + Obj2.DiaryNumber + " is Brackted with Diary Number " + Obj2.BracketedWithDNo;
                        break;
                    }



                }

                //Get Updated Count
                if (Obj.Message == null || Obj.Message == "")
                {
                    Obj.Message = "S_Type Changed to Unstarred Sucessfully !";
                }

                var UCnt = (from Q in DiaCtx.objQuestion
                            where
                             (Q.AssemblyID == AssemId)
                            && (Q.SessionID == SessId)
                            && (Q.OldQuestionType == (int)QuestionType.StartedQuestion)
                            && (Q.IsTypeChange == true)
                            select Q).ToList().Count();


                Obj.STotalCnt = UCnt;

                DiaCtx.Close();
            }

            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj;

        }

        public static object GetLegislationDashboardItemsCounter(object param)
        {
            DiaryModel model = param as DiaryModel;

            //Starred Questions Count
            //model.SPending = GetStarredQuestionPendingCount();
            var SPResult = (tQuestion)GetStarredQuestionPendingList(1, 1, model.AssemblyID, model.SessionID);
            var SSResult = (tQuestion)GetStarredQuestionSentList(1, 1, model.AssemblyID, model.SessionID);
            model.SPending = SPResult.ResultCount;
            model.SNewlyDiaried = GetStarredNewlyDiariedCount();
            model.STotalCnt = SPResult.ResultCount + SSResult.ResultCount;


            //UnStarred Questions Count
            var USPResult = (tQuestion)GetUnStarredQuestionPendingList(1, 1, model.AssemblyID, model.SessionID);
            var USSResult = (tQuestion)GetUnStarredQuestionSentList(1, 1, model.AssemblyID, model.SessionID);
            //model.UPending = GetUnStarredQuestionPendingCount();
            model.UPending = USPResult.ResultCount;
            model.UNewlyDiaried = GetUnStarredNewlyDiariedCount();
            model.UTotalCnt = USPResult.ResultCount + USSResult.ResultCount;


            //Notices
            var result = (tMemberNotice)GetNoticePendingList(1, 1, model.AssemblyID, model.SessionID);
            var result1 = (tMemberNotice)GetNoticeSentList(1, 1, model.AssemblyID, model.SessionID);

            //model.NPending = GetNoticePendingCount();
            model.NPending = result.ResultCount;
            model.NNewlyDiaried = GetNoticeNewlyDiariedCount();
            model.NTotalCnt = result.ResultCount + result1.ResultCount;

            //Bills
            //model.BPending = GetBillsPaperRecievedCount();
            var BRResult = (tPaperLaidV)GetBillsPaperRecievedList(1, 1, model.AssemblyID, model.SessionID);
            model.BPending = BRResult.ResultCount;

            //Committees
            //model.CPending = GetCommittePaperRecievedCount();
            var CRResult = (tPaperLaidV)GetCommittePaperRecievedList(1, 1, model.AssemblyID, model.SessionID);
            model.CPending = CRResult.ResultCount;
            //Others Papers
            var OPRResult = (tPaperLaidV)GetOtherPaperToLayRecievedList(1, 1, model.AssemblyID, model.SessionID);
            //model.OPPending = GetOtherPaperToLayRecievedCount();
            model.OPPending = OPRResult.ResultCount;

            return model;
        }

        public static int GetStarredNewlyDiariedCount()
        {
            SiteSettings model = new SiteSettings();
            model = (SiteSettings)GetAllSiteSettings();
            DiariesContext DiaCtx = new DiariesContext();
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.QuestionStatus == (int)Questionstatus.QuestionPending)
                          //&& (Q.DiaryNumber != null)
                          && (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == (int)QuestionType.StartedQuestion)
                          select Q).ToList().Count();

            return Convert.ToInt16(result);
        }
        public static int GetUnStarredNewlyDiariedCount()
        {
            SiteSettings model = new SiteSettings();
            model = (SiteSettings)GetAllSiteSettings();

            DiariesContext DiaCtx = new DiariesContext();
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.QuestionStatus == (int)Questionstatus.QuestionPending)
                          //&& (Q.DiaryNumber != null)
                          && (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)
                          select Q).ToList().Count();

            return Convert.ToInt16(result);
        }
        public static int GetNoticeNewlyDiariedCount()
        {
            SiteSettings model = new SiteSettings();
            model = (SiteSettings)GetAllSiteSettings();
            DiariesContext DiaCtx = new DiariesContext();
            var result = (from N in DiaCtx.ObjNotices
                          where (N.NoticeStatus == (int)NoticeStatusEnum.NoticePending)
                          && (N.NoticeNumber != null)
                          && (N.AssemblyID == model.AssemblyCode)
                          && (N.SessionID == model.SessionCode)
                          select N).ToList().Count();

            return Convert.ToInt16(result);
        }

        public static object GetEmpNameByEid(object param)
        {
            string EmpCd = param as string;

            UserContext Ustxt = new UserContext();

            var Name = (from User in Ustxt.mUsers
                            //join E in Ustxt.mEmployee on new { UserName1 = User.UserName, DeptId1 = User.DeptId } equals new { UserName1 = E.empcd, DeptId1 = E.deptid }
                        where User.UserId == new Guid(EmpCd)
                        select new AssignQuestionModel
                        {
                            //UserName = E.empfname + " " + E.empmname + " " + E.emplname,
                            UserName = User.UserName,
                        }).FirstOrDefault();



            return Name.UserName.ToString().Trim();

        }

        public static object GetFileNamePath(object param)
        {
            long? PLID = (long?)param;
            using (LegislationFixationContext db = new LegislationFixationContext())
            {
                var data = (from q in db.tPaperLaidVS
                            where (q.PaperLaidId == PLID)
                            join temp in db.tPaperLaidTemp on q.DeptActivePaperId equals temp.PaperLaidTempId
                            select new DiaryModel
                            {
                                FilePath = temp.SignedFilePath,
                                version = temp.Version
                            }).FirstOrDefault();

                return data;
            }
        }

        public static object GetDeptSubmittedDate(object param)
        {
            long? PLID = (long?)param;
            using (LegislationFixationContext db = new LegislationFixationContext())
            {
                var result = (from p in db.tPaperLaidVS
                              where (p.PaperLaidId == PLID)
                              join t in db.tPaperLaidTemp on p.DeptActivePaperId equals t.PaperLaidTempId
                              select new DiaryModel
                              {
                                  DeptSubmittedDate = t.DeptSubmittedDate

                              }).FirstOrDefault();
                return result;

            }


        }
        public static object GetMinisterSubmittedDate(object param)
        {
            long PLID = (long)param;
            using (LegislationFixationContext db = new LegislationFixationContext())
            {
                var result = (from p in db.tPaperLaidVS
                              where (p.PaperLaidId == PLID)
                              join tpt in db.tPaperLaidTemp on p.DeptActivePaperId equals tpt.PaperLaidTempId
                              select new DiaryModel
                              {
                                  MinisterSubmittedDate = tpt.MinisterSubmittedDate
                              }).FirstOrDefault();
                return result;
            }

        }




        #region Starred and Unstarred Fixed Question Related Created On 02-07-2014

        public static object GetStarredFixedQuesCount(object param)
        {

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel DM = param as DiaryModel;
            SiteSettings siteSettingMod = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                siteSettingMod.AssemblyCode = DM.AssemblyID;
                siteSettingMod.SessionCode = DM.SessionID;
            }
            else
            {
                siteSettingMod = (SiteSettings)GetAllSiteSettings();
            }

            var FixCount = (from Q in DiaCtx.objQuestion
                            where (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                            && (Q.AssemblyID == siteSettingMod.AssemblyCode)
                            && (Q.SessionID == siteSettingMod.SessionCode)
                            && (Q.IsFixed == true)
                            && (Q.QuestionType == (int)QuestionType.StartedQuestion)
                            select Q).ToList().Count();
            return FixCount;
        }
        public static object GetStarredUnFixedQuesCount(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel DM = param as DiaryModel;
            SiteSettings siteSettingMod = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                siteSettingMod.AssemblyCode = DM.AssemblyID;
                siteSettingMod.SessionCode = DM.SessionID;
            }
            else
            {
                siteSettingMod = (SiteSettings)GetAllSiteSettings();
            }
            var UnFixCount = (from Q in DiaCtx.objQuestion
                              where (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                                && (Q.AssemblyID == siteSettingMod.AssemblyCode)
                            && (Q.SessionID == siteSettingMod.SessionCode)
                              && (Q.IsFixed == false || Q.IsFixed == null)
                              && (Q.IsPostpone == false)
                              && (Q.QuestionType == (int)QuestionType.StartedQuestion)
                              select Q).ToList().Count();
            return UnFixCount;
        }
        public static object GetUnstarredFixedQuesCount(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel DM = param as DiaryModel;
            SiteSettings siteSettingMod = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                siteSettingMod.AssemblyCode = DM.AssemblyID;
                siteSettingMod.SessionCode = DM.SessionID;
            }
            else
            {
                siteSettingMod = (SiteSettings)GetAllSiteSettings();
            }
            var FixCount = (from Q in DiaCtx.objQuestion
                            where (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                              && (Q.AssemblyID == siteSettingMod.AssemblyCode)
                            && (Q.SessionID == siteSettingMod.SessionCode)
                            && (Q.IsFixed == true)
                            && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)
                            select Q).ToList().Count();
            return FixCount;
        }
        public static object GetUnstarredUnFixedQuesCount(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel DM = param as DiaryModel;
            SiteSettings siteSettingMod = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                siteSettingMod.AssemblyCode = DM.AssemblyID;
                siteSettingMod.SessionCode = DM.SessionID;
            }
            else
            {
                siteSettingMod = (SiteSettings)GetAllSiteSettings();
            }
            var UnFixCount = (from Q in DiaCtx.objQuestion
                              where (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                                && (Q.AssemblyID == siteSettingMod.AssemblyCode)
                            && (Q.SessionID == siteSettingMod.SessionCode)
                              && (Q.IsFixed == false || Q.IsFixed == null)
                               && (Q.IsPostpone == false)
                              && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)
                              select Q).ToList().Count();
            return UnFixCount;
        }

        public static object GetStarredUnfixedQuestions(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = new DiaryModel();
            SiteSettings model = new SiteSettings();

            DiaryModel DryModel = param as DiaryModel;



            model = (SiteSettings)GetAllSiteSettings();
            if (DryModel.AssemblyID != 0 && DryModel.SessionID != 0)
            {
                model.AssemblyCode = DryModel.AssemblyID;
                model.SessionCode = DryModel.SessionID;
            }

            //	Obj.SessDateList = (List<mSessionDate>)GetAllSessionDate(param);
            Obj.SessDateList = (List<mSessionDate>)GetSessionDateNotFixed(param);
            Obj.SUnfixCount = (int)GetStarredUnFixedQuesCount(param);

            Obj.memberList = (List<DiaryModel>)GetMemberList(model.AssemblyCode);

            /// Check Last Number In Current Session if it is 0 or null then Last Number in Previous Session 

            var LastNumber = (from Q in DiaCtx.objQuestion
                              where Q.AssemblyID == model.AssemblyCode
                              && Q.SessionID == model.SessionCode
                              && Q.QuestionType == 1
                              && Q.DeActivateFlag != "RF"
                              select Q.QuestionNumber).Max();

            if (LastNumber != null && LastNumber != 0)
            {
                Obj.QuestionNumber = LastNumber;
            }
            else
            {
                Obj.QuestionNumber = model.PreSLQNum;
            }

            return Obj;
        }
        public static object GetStarredfixedQuestions(object param)
        {
            try
            {

                DiaryModel Obj = new DiaryModel();
                Obj.SessDateList = (List<mSessionDate>)GetAllSessionDate(param);
                Obj.SfixCount = Convert.ToInt32(GetStarredFixedQuesCount(param));
                return Obj;
            }
            catch
            {
                return null;
            }

        }

        public static object GetUnstarredUnfixedQuestions(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            SiteSettings model = new SiteSettings();


            model = (SiteSettings)GetAllSiteSettings();
            if (Obj.AssemblyID != 0 && Obj.SessionID != 0)
            {
                model.AssemblyCode = Obj.AssemblyID;
                model.SessionCode = Obj.SessionID;
            }

            //Obj.SessDateList = (List<mSessionDate>)GetAllSessionDate(param);
            Obj.SessDateList = (List<mSessionDate>)GetSessionDateNotFixUn(param);


            Obj.SUnfixCount = (int)GetUnstarredUnFixedQuesCount(param);
            /// Check Last Number In Current Session if it is 0 or null then Last Number in Previous Session 
            var LastNumber = (from Q in DiaCtx.objQuestion
                              where Q.AssemblyID == model.AssemblyCode
                              && Q.SessionID == model.SessionCode
                              && Q.QuestionType == 2
                              && Q.DeActivateFlag != "RF"
                              select Q.QuestionNumber).Max();

            if (LastNumber != null && LastNumber != 0)
            {
                Obj.QuestionNumber = LastNumber;
            }
            else
            {
                Obj.QuestionNumber = model.PreSLQNumUn;
            }
            return Obj;
        }

        public static object GetLastQuestionNumber()
        {
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            model = (SiteSettings)GetAllSiteSettings();
            var LastNumber = (from Q in DiaCtx.objQuestion
                              where Q.AssemblyID == model.AssemblyCode
                              && Q.SessionID == model.SessionCode
                              select Q.QuestionNumber).Max();
            return LastNumber;
        }
        public static object GetUnstarredfixedQuestions(object param)
        {
            DiaryModel Obj = new DiaryModel();
            Obj.SessDateList = (List<mSessionDate>)GetAllSessionDate(param);
            Obj.SfixCount = (int)GetUnstarredFixedQuesCount(param);
            return Obj;
        }

        public static object GetAllSessionDate(object param)
        {
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;
            /*Get All Session date according to session id */
            var SDt = (from SD in SCtx.mSessDate
                       where (SD.SessionId == Obj.SessionID)
                       && (SD.AssemblyId == Obj.AssemblyID)
                       select SD).OrderBy(d => d.SessionDate).ToList();

            foreach (var item in SDt)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }

            return SDt;
        }

        public static object GetSessionDateNotFixUn(object param)
        {
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;
            /*Get All Session date according to session id */
            var SDt = (from SD in SCtx.mSessDate
                       where (SD.SessionId == Obj.SessionID)
                       && (SD.AssemblyId == Obj.AssemblyID)
                        && (SD.UnstarredApprovedBySecrt != true)
                       select SD).OrderBy(d => d.SessionDate).ToList();

            foreach (var item in SDt)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }

            return SDt;
        }

        public static object GetSessionDateNotFixed(object param)
        {
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;
            /*Get All Session date according to session id */
            var SDt = (from SD in SCtx.mSessDate
                       where (SD.SessionId == Obj.SessionID)
                       && (SD.AssemblyId == Obj.AssemblyID)
                       && (SD.ApprovedBySecrt != true)
                       select SD).OrderBy(d => d.SessionDate).ToList();

            foreach (var item in SDt)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }

            return SDt;
        }

        public static object GetRotationalMiniterName(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            string BindMinisName = "";

            if (Obj.SessionDateId != 0)
            {
                var MinisIds = (from RM in DiaCtx.tRotationMinister
                                where (RM.SessionDateId == Obj.SessionDateId)
                                select RM.MinistryId).FirstOrDefault();

                MinisIds = MinisIds.ToString().Trim();


                string[] StrId = MinisIds.Split(',');


                for (int i = 0; i < StrId.Length; i++)
                { // Convert String Id into int Id
                    int Id = Convert.ToInt16(StrId[i]);

                    //Get Minister Name By Id
                    var MinisName = (from M in DiaCtx.objmMinistry
                                     where M.MinistryID == Id
                                     select M.MinistryName).FirstOrDefault();
                    if (MinisName != null)
                    {
                        BindMinisName = BindMinisName + MinisName.ToString().Trim() + ", ";
                    }

                }

                Obj.MinisterName = BindMinisName.Substring(0, BindMinisName.Length - 2);
            }
            return Obj.MinisterName;
        }

        public static object SearchBySessionDate(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            List<DiaryModel> ListForBind = new List<DiaryModel>();
            if (Obj.SessionDateId != 0)
            {
                var MinisIds = (from RM in DiaCtx.tRotationMinister
                                where (RM.SessionDateId == Obj.SessionDateId)
                                select RM.MinistryId).FirstOrDefault();
                if (MinisIds != null)
                {
                    MinisIds = MinisIds.ToString().Trim();

                    // Convert String Id into int Id
                    string[] StrId = MinisIds.Split(',');

                    for (int i = 0; i < StrId.Length; i++)
                    {
                        int Id = Convert.ToInt16(StrId[i]);

                        var QList = (from Q in DiaCtx.objQuestion
                                     where Q.MinistryId == Id
                                     && (Q.QuestionType == Obj.QuestionTypeId)
                                     && (Q.QuestionStatus == Obj.QuestionStatus)
                                     && (Q.AssemblyID == Obj.AssemblyID)
                                     && (Q.SessionID == Obj.SessionID)
                                     && (Q.IsFixed == false || Q.IsFixed == null)
                                      && (Q.IsPostpone == false)
                                     orderby Q.QuestionID
                                     select new DiaryModel
                                     {
                                         QuestionId = Q.QuestionID,
                                         Subject = Q.Subject,
                                         DiaryNumber = Q.DiaryNumber,
                                         MinistryId = Q.MinistryId,
                                         DepartmentId = Q.DepartmentID,
                                         MemberId = Q.MemberID,
                                         DateForBind = Q.NoticeRecievedDate,
                                         TimeForBind = Q.NoticeRecievedTime,
                                         IsBracket = Q.IsBracket,
                                         IsHindi = Q.IsHindi,

                                     }).ToList();

                        foreach (var item in QList)
                        {
                            if (item.MemberId != 0)
                            {
                                var Member = (from Mem in DiaCtx.objmMember
                                              where Mem.MemberCode == item.MemberId
                                              select Mem).FirstOrDefault();

                                if (item.IsHindi == true)
                                {
                                    item.AskedBy = Member.NameLocal.ToString().Trim();

                                }
                                else
                                {
                                    item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();

                                }
                            }

                            if (item.MinistryId != 0 && item.DepartmentId != null)
                            {
                                var MinistryName = (from M in DiaCtx.objmMinistry
                                                    where M.MinistryID == item.MinistryId
                                                    select M).FirstOrDefault();

                                if (item.IsHindi == true)
                                {
                                    item.MinistryName = MinistryName.MinistryNameLocal.ToString().Trim();
                                }
                                else
                                {
                                    item.MinistryName = MinistryName.MinistryName.ToString().Trim();
                                }
                            }

                            if (item.DepartmentId != "" && item.DepartmentId != null)
                            {
                                var DeptName = (from D in DiaCtx.objmDepartment
                                                where D.deptId == item.DepartmentId
                                                select D).FirstOrDefault();

                                if (item.IsHindi == true)
                                {
                                    item.DepartmentName = DeptName.deptnameLocal.ToString().Trim();
                                }
                                else
                                {
                                    item.DepartmentName = DeptName.deptname.ToString().Trim();
                                }
                            }
                        }





                        ListForBind.AddRange(QList);
                        Obj.DiaryList = ListForBind;

                    }
                }
                return Obj.DiaryList;
            }

            else
            {
                var QList = (from Q in DiaCtx.objQuestion
                             where (Q.QuestionType == Obj.QuestionTypeId)
                             && (Q.QuestionStatus == Obj.QuestionStatus)
                             && (Q.AssemblyID == Obj.AssemblyID)
                             && (Q.SessionID == Obj.SessionID)
                              && (Q.IsFixed == false || Q.IsFixed == null)
                               && (Q.IsPostpone == false)
                             orderby Q.QuestionID
                             select new DiaryModel
                             {

                                 QuestionId = Q.QuestionID,
                                 Subject = Q.Subject,
                                 DiaryNumber = Q.DiaryNumber,
                                 MinistryId = Q.MinistryId,
                                 DepartmentId = Q.DepartmentID,
                                 MemberId = Q.MemberID,
                                 DateForBind = Q.NoticeRecievedDate,
                                 TimeForBind = Q.NoticeRecievedTime,
                                 IsBracket = Q.IsBracket,
                                 IsHindi = Q.IsHindi,

                             }).ToList();


                foreach (var item in QList)
                {
                    if (item.MemberId != 0)
                    {
                        var Member = (from Mem in DiaCtx.objmMember
                                      where Mem.MemberCode == item.MemberId
                                      select Mem).FirstOrDefault();

                        if (item.IsHindi == true)
                        {
                            item.AskedBy = Member.NameLocal.ToString().Trim();

                        }
                        else
                        {
                            item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();

                        }
                    }

                    if (item.MinistryId != 0 && item.DepartmentId != null)
                    {
                        var MinistryName = (from M in DiaCtx.objmMinistry
                                            where M.MinistryID == item.MinistryId
                                            select M).FirstOrDefault();

                        if (item.IsHindi == true)
                        {
                            item.MinistryName = MinistryName.MinistryNameLocal.ToString().Trim();
                        }
                        else
                        {
                            item.MinistryName = MinistryName.MinistryName.ToString().Trim();
                        }
                    }

                    if (item.DepartmentId != "" && item.DepartmentId != null)
                    {
                        var DeptName = (from D in DiaCtx.objmDepartment
                                        where D.deptId == item.DepartmentId
                                        select D).FirstOrDefault();


                        if (item.IsHindi == true)
                        {
                            item.DepartmentName = DeptName.deptnameLocal.ToString().Trim();
                        }
                        else
                        {
                            item.DepartmentName = DeptName.deptname.ToString().Trim();
                        }
                    }


                }

                ListForBind.AddRange(QList);
            }


            Obj.DiaryList = ListForBind;


            return Obj.DiaryList;
        }

        public static object SearchBySessionDateForFix(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var QList = (from Q in DiaCtx.objQuestion
                         where (Q.QuestionType == Obj.QuestionTypeId)
                         && (Q.AssemblyID == Obj.AssemblyID)
                         && (Q.SessionID == Obj.SessionID)
                         && (Q.IsFixed == true)
                         && ((Obj.SessionDateId == 0) || (Q.SessionDateId == Obj.SessionDateId))
                          && (Q.QuestionStatus == Obj.QuestionStatus)
                         orderby Q.QuestionID
                         select new DiaryModel
                         {
                             QuestionId = Q.QuestionID,
                             Subject = Q.Subject,
                             DiaryNumber = Q.DiaryNumber,
                             MinistryId = Q.MinistryId,
                             DepartmentId = Q.DepartmentID,
                             MemberId = Q.MemberID,
                             DateForBind = Q.NoticeRecievedDate,
                             TimeForBind = Q.NoticeRecievedTime,
                             QuestionNumber = Q.QuestionNumber,
                             IsFinalApprove = Q.IsFinalApproved,
                             IsPostPoneDate = Q.IsPostponeDate,
                             Flag = Q.DeActivateFlag,
                             IsHindi = Q.IsHindi

                         }).ToList();

            foreach (var item in QList)
            {
                if (item.MemberId != 0)
                {
                    var Member = (from Mem in DiaCtx.objmMember
                                  where Mem.MemberCode == item.MemberId
                                  select Mem).FirstOrDefault();
                    if (item.IsHindi == true)
                    {
                        item.AskedBy = Member.NameLocal.ToString().Trim();

                    }
                    else
                    {
                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();

                    }
                }

                if (item.MinistryId != 0 && item.DepartmentId != null)
                {
                    var MinistryName = (from M in DiaCtx.objmMinistry
                                        where M.MinistryID == item.MinistryId
                                        select M).FirstOrDefault();
                    if (item.IsHindi == true)
                    {
                        item.MinistryName = MinistryName.MinistryNameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.MinistryName = MinistryName.MinistryName.ToString().Trim();
                    }
                }

                if (item.DepartmentId != "" && item.DepartmentId != null)
                {
                    var DeptName = (from D in DiaCtx.objmDepartment
                                    where D.deptId == item.DepartmentId
                                    select D).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.DepartmentName = DeptName.deptnameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.DepartmentName = DeptName.deptname.ToString().Trim();
                    }

                }
            }

            Obj.DiaryList = QList;

            return Obj.DiaryList;
        }

        public static object GetResetDataByQuestionId(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var Resetdata = (from At in DiaCtx.tQuestionAuditTrial
                             where (At.MainQuesionId == Obj.QuestionId)
                             && (At.RecordBy == "ProofReader")
                             select new DiaryModel
                             {
                                 Subject = At.Subject,
                                 MainQuestion = At.MainQuestion,
                                 MinistryId = At.MinistryId,
                                 DepartmentId = At.DepartmentID,
                                 IsQuestionInPart = At.IsQuestionInPart,
                                 IsHindi = At.IsHindi,

                             }).FirstOrDefault();
            return Resetdata;
        }

        static object GetQuestionOrderNumBySId(int AId, int SId, int Qtype, int? SDId)
        {
            DiariesContext DiaCtx = new DiariesContext();

            var QN = (from Q in DiaCtx.objQuestion
                      where Q.AssemblyID == AId
                      && Q.SessionID == SId
                      && Q.QuestionType == Qtype
                      && Q.SessionDateId == SDId
                      select Q.QuestionOrderNum).Max();

            if (QN != null)
            {
                QN = QN + 1;
            }
            else
            {
                QN = 1;
            }

            return QN;

        }

        public static object FixingQuestionById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();

            DiaryModel Obj = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();
            //SiteSettings siteSettingMod = (SiteSettings)GetAllSiteSettings();

            try
            {

                string[] Ids = Obj.QuesIds.Split(',');
                string[] FixNum = Obj.FixNums.Split(',');
                ////string[] ManualNum = Obj.ManualFixNo.Split(',');
                if (Ids.Length == FixNum.Length)
                {
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        int id = Convert.ToInt16(Ids[i]);
                        int QNum = Convert.ToInt16(FixNum[i]);

                        QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                        QuesObj.IsFixed = true;
                        QuesObj.IsFixedDate = (DateTime)GetFixedDateBySDtId(Obj.SessionDateId);
                        QuesObj.IsFixedTodayDate = DateTime.Now;
                        QuesObj.SessionDateId = Obj.SessionDateId;
                        QuesObj.QuestionNumber = QNum;
                        //QuesObj.ManualQuestionNumber = ManualNum[i];
                        //QuesObj.QuestionOrderNum = (i + 1);
                        //DiaCtx.SaveChanges();

                        //get Order number and update
                        QuesObj.QuestionOrderNum = (int)GetQuestionOrderNumBySId(QuesObj.AssemblyID, QuesObj.SessionID, QuesObj.QuestionType, Obj.SessionDateId);
                        DiaCtx.SaveChanges();
                        var result = SkipQuestionProcessSteps.SkippingQuesProcessingStepsAfterFixing(id);
                        Obj.Message = "Question(s) Fixed Successfully with Question Number ";
                    }
                }

                else
                {
                    Obj.Message = "Question Number not assigned Properly !";
                }


                // Get Question number
                //int? QuesNum = (from Q in DiaCtx.objQuestion
                //                where (Q.AssemblyID == siteSettingMod.AssemblyCode)
                //                    && (Q.SessionID == siteSettingMod.SessionCode)
                //                orderby Q.QuestionID descending
                //                select Q.QuestionNumber
                //               ).FirstOrDefault();

                //QuesNum = QuesNum + 1;

                //for (int i = 0; i < Ids.Length; i++)
                //{
                //    int id = Convert.ToInt16(Ids[i]);

                //    QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                //    QuesObj.IsFixed = true;
                //    QuesObj.IsFixedDate = DateTime.Now;
                //    QuesObj.SessionDateId = Obj.SessionDateId;
                //    QuesObj.QuestionNumber = QuesNum;
                //    DiaCtx.SaveChanges();
                //}

                DiaCtx.Close();

            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;

        }
        public static object ApproveFixingQuestion(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();

            DiaryModel Obj = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();
            int Qtype = 0;
            try
            {

                string[] Ids = Obj.QuesIds.Split(',');

                for (int i = 0; i < Ids.Length; i++)
                {

                    int id = Convert.ToInt16(Ids[i]);
                    Obj.Message = " Question Number " + Ids[i] + " Approved and Email/Sms Sent.";
                    QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                    //Check Question Type
                    Qtype = QuesObj.QuestionType;
                    QuesObj.IsFinalApproved = true;
                    QuesObj.IsFinalApprovedDate = DateTime.Now;
                    DiaCtx.SaveChanges();

                }

                //Obj.Message = Qtype + "Fixed Question(s) Finally Approved !";

                DiaCtx.Close();

            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;
        }

        public static object GetAllPostPoneQues(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var QList = (from Q in DiaCtx.objQuestion
                         where (Q.QuestionType == Obj.QuestionTypeId)
                         && (Q.AssemblyID == Obj.AssemblyID)
                         && (Q.SessionID == Obj.SessionID)
                         && (Q.IsFinalApproved == true)
                         && (Q.SessionDateId == Obj.SessionDateId)
                         && (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                         orderby Q.QuestionID
                         select new DiaryModel
                         {
                             QuestionId = Q.QuestionID,
                             Subject = Q.Subject,
                             DiaryNumber = Q.DiaryNumber,
                             MinistryId = Q.MinistryId,
                             DepartmentId = Q.DepartmentID,
                             MemberId = Q.MemberID,
                             DateForBind = Q.IsPostponeDate,
                             QuestionNumber = Q.QuestionNumber,
                             IsPostPone = Q.IsPostpone,
                             IsHindi = Q.IsHindi

                         }).ToList();

            foreach (var item in QList)
            {
                if (item.MemberId != 0)
                {
                    var Member = (from Mem in DiaCtx.objmMember
                                  where Mem.MemberCode == item.MemberId
                                  select Mem).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.AskedBy = Member.NameLocal.ToString().Trim();

                    }
                    else
                    {
                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();

                    }
                }

                if (item.MinistryId != 0 && item.DepartmentId != null)
                {
                    var MinistryName = (from M in DiaCtx.objmMinistry
                                        where M.MinistryID == item.MinistryId
                                        select M).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.MinistryName = MinistryName.MinistryNameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.MinistryName = MinistryName.MinistryName.ToString().Trim();
                    }
                }

                if (item.DepartmentId != "" && item.DepartmentId != null)
                {
                    var DeptName = (from D in DiaCtx.objmDepartment
                                    where D.deptId == item.DepartmentId
                                    select D).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.DepartmentName = DeptName.deptnameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.DepartmentName = DeptName.deptname.ToString().Trim();
                    }
                }
            }

            Obj.DiaryList = QList;

            return Obj.DiaryList;

        }

        public static object GetSessionDateforPostpone(object param)
        {
            DiaryModel Obj = param as DiaryModel;
            //SiteSettings model = new SiteSettings();
            //model = (SiteSettings)GetAllSiteSettings();
            //Obj.AssemblyID = model.AssemblyCode;
            //Obj.SessionID = model.SessionCode;
            Obj.SessDateList = (List<mSessionDate>)GetAllSessionDate(param);
            return Obj;
        }

        public static object GetQuestionForPostpone(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var QList = (from Q in DiaCtx.objQuestion
                         where (Q.QuestionType == Obj.QuestionTypeId)
                         && (Q.AssemblyID == Obj.AssemblyID)
                         && (Q.SessionID == Obj.SessionID)
                         && (Q.IsFinalApproved == true)
                         && (Q.SessionDateId == Obj.SessionDateId)
                         && (Q.QuestionStatus == (int)Questionstatus.QuestionSent)
                         orderby Q.QuestionID
                         select new DiaryModel
                         {
                             QuestionId = Q.QuestionID,
                             Subject = Q.Subject,
                             DiaryNumber = Q.DiaryNumber,
                             MinistryId = Q.MinistryId,
                             DepartmentId = Q.DepartmentID,
                             MemberId = Q.MemberID,
                             DateForBind = Q.IsPostponeDate,
                             QuestionNumber = Q.QuestionNumber,
                             IsPostPone = Q.IsPostpone,
                             IsHindi = Q.IsHindi

                         }).ToList();

            foreach (var item in QList)
            {
                if (item.MemberId != 0)
                {
                    var Member = (from Mem in DiaCtx.objmMember
                                  where Mem.MemberCode == item.MemberId
                                  select Mem).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.AskedBy = Member.NameLocal.ToString().Trim();

                    }
                    else
                    {
                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();

                    }

                }

                if (item.MinistryId != 0 && item.MinistryId != 85 && item.DepartmentId != null)
                {
                    var MinistryName = (from M in DiaCtx.objmMinistry
                                        where M.MinistryID == item.MinistryId
                                        select M).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.MinistryName = MinistryName.MinistryNameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.MinistryName = MinistryName.MinistryName.ToString().Trim();
                    }
                }

                if (item.DepartmentId != "" && item.DepartmentId != null)
                {
                    var DeptName = (from D in DiaCtx.objmDepartment
                                    where D.deptId == item.DepartmentId
                                    select D).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.DepartmentName = DeptName.deptnameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.DepartmentName = DeptName.deptname.ToString().Trim();
                    }
                }
            }

            Obj.DiaryList = QList;

            return Obj.DiaryList;
        }



        public static object PostponeQuestionbyId(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();

            DiaryModel Obj = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();

            DateTime ppdate = new DateTime();
            if (Obj.SessionDateId != 0)
            {
                ppdate = (DateTime)GetFixedDateBySDtId(Obj.SessionDateId);
            }
            try
            {

                string[] Ids = Obj.QuesIds.Split(',');

                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);

                    QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                    DiaCtx.objQuestion.Attach(QuesObj);
                    QuesObj.IsPostpone = true;
                    QuesObj.IsPostponeDate = ppdate;
                    QuesObj.IsFixed = false;
                    QuesObj.IsFixedTodayDate = null;
                    QuesObj.IsFinalApproved = null;
                    QuesObj.IsFinalApprovedDate = null;

                    ///Use DeactiveFlag For Postpone Status
                    ///
                    QuesObj.DeActivateFlag = "P";
                    QuesObj.StatusPQ = "P";
                    DiaCtx.SaveChanges();

                }

                Obj.Message = "Question(s) of Selected Fixing Date has been Postponed Sucessfully !";

                DiaCtx.Close();

            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;

        }

        public static object GetSessionAndSessionDateforPostpone(object param)
        {
            DiaryModel Obj = param as DiaryModel;
            SessionContext SCtx = new SessionContext();
            //SiteSettings model = new SiteSettings();

            //model = (SiteSettings)GetAllSiteSettings();

            Obj.SessList = (from S in SCtx.mSession
                            orderby S.SessionCode descending
                            where S.AssemblyID == Obj.AssemblyID
                            select S).ToList();

            //Obj.AssemblyID = model.AssemblyCode;
            //Obj.SessionID = model.SessionCode;
            Obj.SessDateList = (List<mSessionDate>)GetAllSessionDate(param);

            return Obj;

        }



        public static object SearchPostPoneBySessionDateForFix(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            DateTime ppdate = new DateTime();
            if (Obj.SessionDateId != 0)
            {
                ppdate = (DateTime)GetSessionDateById(Obj);
            }
            var QList = (from Q in DiaCtx.objQuestion
                         where (Q.QuestionType == Obj.QuestionTypeId)
                         && (Q.AssemblyID == Obj.AssemblyID)
                         && (Q.SessionID == Obj.SessionID)
                         && (Q.IsPostpone == true)
                         && ((Obj.SessionDateId == 0) || (Q.IsPostponeDate == ppdate))
                         //&& (Q.QuestionStatus == Obj.QuestionStatus)
                         orderby Q.QuestionID
                         select new DiaryModel
                         {
                             QuestionId = Q.QuestionID,
                             Subject = Q.Subject,
                             DiaryNumber = Q.DiaryNumber,
                             MinistryId = Q.MinistryId,
                             DepartmentId = Q.DepartmentID,
                             MemberId = Q.MemberID,
                             DateForBind = Q.IsPostponeDate,
                             QuestionNumber = Q.QuestionNumber,
                             IsPostPone = Q.IsPostpone,
                             SessionID = Q.SessionID,
                             IsHindi = Q.IsHindi,
                             IsFixed = Q.IsFixed,
                             DeActivateFlag = Q.DeActivateFlag,
                         }).ToList();

            foreach (var item in QList)
            {
                if (item.MemberId != 0)
                {
                    var Member = (from Mem in DiaCtx.objmMember
                                  where Mem.MemberCode == item.MemberId
                                  select Mem).FirstOrDefault();
                    if (item.IsHindi == true)
                    {
                        item.AskedBy = Member.NameLocal.ToString().Trim();

                    }
                    else
                    {
                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();

                    }


                }

                if (item.MinistryId != 0 && item.DepartmentId != null)
                {
                    var MinistryName = (from M in DiaCtx.objmMinistry
                                        where M.MinistryID == item.MinistryId
                                        select M).FirstOrDefault();
                    if (item.IsHindi == true)
                    {
                        item.MinistryName = MinistryName.MinistryNameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.MinistryName = MinistryName.MinistryName.ToString().Trim();
                    }

                }

                if (item.DepartmentId != "" && item.DepartmentId != null)
                {
                    var DeptName = (from D in DiaCtx.objmDepartment
                                    where D.deptId == item.DepartmentId
                                    select D).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.DepartmentName = DeptName.deptnameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.DepartmentName = DeptName.deptname.ToString().Trim();
                    }
                }
            }

            Obj.DiaryList = QList;

            return Obj;


        }

        public static object ReFixPostPoneQuestionbyId(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();

            DiaryModel Obj = param as DiaryModel;

            ///Get Curretnt Assembly And Session
            SiteSettings model = new SiteSettings();

            model = (SiteSettings)GetAllSiteSettings();

            tQuestion QuesObj = new tQuestion();
            tQuestion ForCopyQuesObj = new tQuestion();
            var FL = (from SS in DiaCtx.mSiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();
            try
            {

                string[] Ids = Obj.QuesIds.Split(',');

                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);

                    ///Change Question Status in PostPoned Session 
                    QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == id && m.SessionID == Obj.SessionID);
                    DiaCtx.objQuestion.Attach(QuesObj);
                    //if (Obj.SessionID != model.SessionCode)
                    //{
                    //QuesObj.IsPostpone = false;
                    //QuesObj.IsPostponeDate = null;

                    ///Use DeactiveFlag For Postpone ReFix Status
                    ///
                    QuesObj.DeActivateFlag = "RF";
                    DiaCtx.SaveChanges();


                    ///Copy Question in Current Session

                    ForCopyQuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == id && m.SessionID == Obj.SessionID);
                    DiaCtx.objQuestion.Attach(ForCopyQuesObj);
                    ForCopyQuesObj.SessionID = model.SessionCode;
                    ForCopyQuesObj.IsPostpone = false;
                    ForCopyQuesObj.IsFixed = true;
                    ForCopyQuesObj.IsFixedDate = (DateTime)GetFixedDateBySDtId(Obj.SessionDateId);
                    ForCopyQuesObj.IsFixedTodayDate = DateTime.Now;
                    ForCopyQuesObj.SessionDateId = Obj.SessionDateId;
                    ForCopyQuesObj.DeActivateFlag = "RF";
                    ForCopyQuesObj.PaperLaidId = null;
                    ForCopyQuesObj.QuestionSequence = null;
                    ForCopyQuesObj.StatusPQ = null;
                    ForCopyQuesObj.CurentStatusPQ = "RF";
                    ForCopyQuesObj.IsLocktranslator = null;
                    DiaCtx.objQuestion.Add(ForCopyQuesObj);
                    DiaCtx.SaveChanges();
                    Obj.AssemblyID = ForCopyQuesObj.AssemblyID;
                    Obj.SessionID = ForCopyQuesObj.SessionID;
                    Obj.QuestionTypeId = ForCopyQuesObj.QuestionType;
                    Obj.SaveFilePath = FL;

                    //}
                    //else if (Obj.SessionID == model.SessionCode)
                    //{
                    //    QuesObj.IsPostpone = false;
                    //    //QuesObj.IsPostponeDate = null;
                    //    QuesObj.IsFixed = true;
                    //    QuesObj.IsFixedDate = (DateTime)GetFixedDateBySDtId(Obj.SessionDateId);
                    //    QuesObj.IsFixedTodayDate = DateTime.Now;
                    //    QuesObj.SessionDateId = Obj.SessionDateId;
                    //    QuesObj.DeActivateFlag = "RF";
                    //    DiaCtx.SaveChanges();

                    //}

                }

                Obj.Message = "PostPoned Question(s) Re-Fixed Successfully For Selected Fixing Date !";
                DiaCtx.Close();

            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;
        }

        public static object AssignPrioritybyId(object param)
        {
            LegislationFixationContext LFCtx = new LegislationFixationContext();

            DiaryModel Obj = param as DiaryModel;
            tPaperLaidV PLObj = new tPaperLaidV();

            //SiteSettings siteSettingMod = (SiteSettings)GetAllSiteSettings();

            try
            {

                string[] Ids = Obj.QuesIds.Split(',');
                string[] FixNum = Obj.FixNums.Split(',');

                if (Ids.Length == FixNum.Length)
                {
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        int id = Convert.ToInt16(Ids[i]);
                        int PNum = Convert.ToInt16(FixNum[i]);

                        PLObj = LFCtx.tPaperLaidVS.Single(m => m.PaperLaidId == id);
                        PLObj.PaperlaidPriority = PNum;
                        LFCtx.SaveChanges();

                        Obj.Message = "Priority Updated !";
                    }
                }

                else
                {
                    Obj.Message = "Priority not assigned Properly !";
                }
                LFCtx.Close();
            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;

        }

        public static object BackToStarredByQuesId(object param)
        {

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Dmdl = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();
            tQuestion MergeDiary = new tQuestion();
            int AssemId = 0;
            int SessId = 0;

            try
            {
                QuesObj = DiaCtx.objQuestion.Single(m => m.QuestionID == Dmdl.QuestionId);
                AssemId = QuesObj.AssemblyID;
                SessId = QuesObj.SessionID;
                DiaCtx.objQuestion.Attach(QuesObj);

                if (QuesObj.IsBracket != true)
                {

                    // Change All Brackted Question's Type
                    if (QuesObj.MergeDiaryNo != null && QuesObj.MergeDiaryNo != "")
                    {
                        string[] MergerDiaryNum = QuesObj.MergeDiaryNo.Split();
                        for (int j = 0; j < MergerDiaryNum.Length; j++)
                        {
                            string MDN = MergerDiaryNum[j].ToString().Trim();
                            MergeDiary = DiaCtx.objQuestion.Single(m => m.DiaryNumber == MDN);
                            DiaCtx.objQuestion.Attach(MergeDiary);

                            MergeDiary.QuestionType = (int)QuestionType.StartedQuestion;
                            MergeDiary.OldQuestionType = 0;
                            MergeDiary.IsTypeChange = null;
                            MergeDiary.IsTypeChangeDate = null;
                            MergeDiary.ChangeTypeRemarks = null;
                        }

                    }

                    // Change Parent Question type
                    if (QuesObj.QuestionType == (int)QuestionType.UnstaredQuestion)
                    {
                        QuesObj.QuestionType = (int)QuestionType.StartedQuestion;
                        QuesObj.OldQuestionType = 0;
                    }
                    QuesObj.IsTypeChange = null;
                    QuesObj.IsTypeChangeDate = null;
                    QuesObj.ChangeTypeRemarks = null;

                    Dmdl.Message = "S_Type Changed to Starred Sucessfully !";
                    DiaCtx.SaveChanges();
                }
                else
                {
                    Dmdl.Message = "U_Type Can't be change, Because Diary Number " + QuesObj.DiaryNumber + " is Brackted with Diary Number " + QuesObj.BracketedWithDNo;
                }
                //Get Updated Count
                var UCnt = (from Q in DiaCtx.objQuestion
                            where
                             (Q.AssemblyID == AssemId)
                            && (Q.SessionID == SessId)
                            && (Q.OldQuestionType == (int)QuestionType.StartedQuestion)
                            && (Q.IsTypeChange == true)
                            select Q).ToList().Count();

                Dmdl.STotalCnt = UCnt;
                DiaCtx.Close();
            }
            catch (Exception ex)
            {
                Dmdl.Message = ex.Message;
            }

            return Dmdl;
        }



        public static object GetQuestionsForBracket(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == DM.QuestionTypeId)
                          && (Q.IsBracket == null || Q.IsBracket == false)
                          && (Q.MergeDiaryNo == null || Q.MergeDiaryNo == "")
                          && (Q.QuestionNumber == null)
                          && (Q.IsProofReading == true)
                          select Q).ToList();
            return result;
        }

        public static object CheckBracktingByDiaryNumber(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Dmdl = param as DiaryModel;
            tQuestion QuesObj = new tQuestion();
            tQuestion Obj2 = new tQuestion();
            int DMatchCount = 0;
            string DiaryNum = "";
            try
            {

                QuesObj = (from Q in DiaCtx.objQuestion
                           where Q.DiaryNumber == Dmdl.DiaryNumber
                           && Q.QuestionType == Dmdl.QuestionTypeId
                           select Q).SingleOrDefault();

                if (QuesObj != null)
                {
                    string[] Ids = Dmdl.QuesIds.Split(',');
                    for (int i = 0; i < Ids.Length; i++)
                    {
                        int id = Convert.ToInt16(Ids[i]);
                        Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                        if (Obj2.DepartmentID != QuesObj.DepartmentID)
                        {
                            DMatchCount = DMatchCount + 1;
                            DiaryNum = Obj2.DiaryNumber;
                            break;
                        }
                    }
                    if (QuesObj.QuestionID > Obj2.QuestionID)
                    {
                        Dmdl.Message = "Question can be only Bracket with Before received Question.";
                    }
                    else if (QuesObj.AssemblyID != Obj2.AssemblyID)
                    {
                        Dmdl.Message = "Question can not be Bracket in different Assembly.";
                    }
                    else if (QuesObj.SessionID != Obj2.SessionID)
                    {
                        Dmdl.Message = "Question can not be Bracket in different Session.";
                    }
                    else if (DMatchCount > 0)
                    {
                        Dmdl.Message = "Department should be same for bracketing. Department of " + DiaryNum + " is different from " + Dmdl.DiaryNumber;
                    }
                    else if (QuesObj.IsBracket == true)
                    {
                        Dmdl.Message = "This Diary Number is already Bracket with Diary Number " + QuesObj.BracketedWithDNo;
                    }

                    else
                    {
                        Dmdl.Message = "Not Bracket";
                    }

                }
                else
                {
                    Dmdl.Message = "Invalid Diary Number";
                }
                DiaCtx.Close();
            }
            catch (Exception ex)
            {
                Dmdl.Message = ex.Message;
            }

            return Dmdl;
        }

        public static object BracketQuestionByDiaryNumber(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tQuestion Obj2 = new tQuestion();
            tQuestion UParent = new tQuestion();
            int AssemId = 0;
            int SessId = 0;
            try
            {


                string[] Ids = Obj.QuesIds.Split(',');
                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);
                    Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                    AssemId = Obj2.AssemblyID;
                    SessId = Obj2.SessionID;
                    DiaCtx.objQuestion.Attach(Obj2);

                    //Do Bracketing
                    Obj2.IsBracket = true;
                    Obj2.BracketedWithDNo = Obj.DiaryNumber;

                    // Update Parent Question Status
                    UParent = DiaCtx.objQuestion.Single(m => m.DiaryNumber == Obj.DiaryNumber && m.QuestionType == Obj.QuestionTypeId);
                    DiaCtx.objQuestion.Attach(UParent);

                    //Update MeregeDiaryNo
                    if (UParent.MergeDiaryNo == null || UParent.MergeDiaryNo == "")
                    {
                        UParent.MergeDiaryNo = Obj2.DiaryNumber;
                    }
                    else
                    {
                        UParent.MergeDiaryNo = UParent.MergeDiaryNo + ',' + Obj2.DiaryNumber;
                    }
                    //Update BracketQuestionID
                    if (UParent.BracketedWithQNo == null || UParent.BracketedWithQNo == "")
                    {
                        UParent.BracketedWithQNo = Convert.ToString(UParent.QuestionID) + ',' + Obj2.QuestionID;
                    }
                    else
                    {
                        UParent.BracketedWithQNo = UParent.BracketedWithQNo + ',' + Obj2.QuestionID;
                    }
                    //Update ReferenceNumber
                    if (UParent.ReferencedNumber == null || UParent.ReferencedNumber == "")
                    {
                        UParent.ReferencedNumber = Convert.ToString(UParent.QuestionID) + ',' + Obj2.QuestionID;
                    }
                    else
                    {
                        UParent.ReferencedNumber = UParent.ReferencedNumber + ',' + Obj2.QuestionID;
                    }
                    //Update ReferenceMember
                    if (UParent.ReferenceMemberCode == null || UParent.ReferenceMemberCode == "")
                    {
                        if (UParent.MemberID != Obj2.MemberID)
                        {
                            UParent.ReferenceMemberCode = Convert.ToString(UParent.MemberID) + ',' + Obj2.MemberID;
                        }
                        else
                        {
                            UParent.ReferenceMemberCode = Convert.ToString(UParent.MemberID);
                        }
                    }
                    else
                    {
                        string[] mid = UParent.ReferenceMemberCode.Split(',');
                        var matchCount = 0;
                        foreach (var m in mid)
                        {
                            if (Convert.ToInt16(m) == Obj2.MemberID)
                            {
                                matchCount = matchCount + 1;
                            }
                        }

                        if (matchCount == 0)
                        {
                            UParent.ReferenceMemberCode = Convert.ToString(UParent.ReferenceMemberCode) + ',' + Obj2.MemberID;
                        }

                    }

                    DiaCtx.SaveChanges();
                }

                //Get Updated Bracket Count
                var UCnt = (from Q in DiaCtx.objQuestion
                            where
                             (Q.AssemblyID == AssemId)
                            && (Q.SessionID == SessId)
                            && (Q.QuestionType == Obj.QuestionTypeId)
                            && (Q.IsBracket == true)
                            select Q).ToList().Count();

                //Get Updated ForClubbing Count
                var UClubbCnt = (from Q in DiaCtx.objQuestion
                                 where (Q.QuestionType == Obj.QuestionTypeId)
                                 && (Q.AssemblyID == AssemId)
                                && (Q.SessionID == SessId)
                                && (Q.BracketedWithQNo != null)
                                && (Q.MergeDiaryNo != null)
                                && (Q.IsClubbed == null)
                                 select Q).ToList().Count();


                Obj.Message = "Questions are sucessfully bracketd with DiaryNumber " + Obj.DiaryNumber + " !";
                Obj.STotalCnt = UCnt;
                Obj.CTotalCnt = UClubbCnt;


                DiaCtx.Close();
            }

            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj;
        }

        public static object RemoveBracketingByQuestionId(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tQuestion Obj2 = new tQuestion();
            tQuestion UParent = new tQuestion();

            try
            {
                Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == Obj.QuestionId);
                Obj.AssemblyID = Obj2.AssemblyID;
                Obj.SessionID = Obj2.SessionID;
                Obj.QuestionTypeId = Obj2.QuestionType;

                DiaCtx.objQuestion.Attach(Obj2);

                // Update Parent Question Status
                UParent = DiaCtx.objQuestion.Single(m => m.DiaryNumber == Obj2.BracketedWithDNo && m.QuestionType == Obj.QuestionTypeId);
                DiaCtx.objQuestion.Attach(UParent);

                //Update MeregeDiaryNo
                if (UParent.MergeDiaryNo != null)
                {
                    string[] MDN = UParent.MergeDiaryNo.Split(',');
                    UParent.MergeDiaryNo = null;
                    foreach (var m in MDN)
                    {
                        if (m.Trim() != Obj2.DiaryNumber.Trim())
                        {
                            if (UParent.MergeDiaryNo == null)
                            {
                                UParent.MergeDiaryNo = m.Trim();
                            }
                            else
                            {
                                UParent.MergeDiaryNo = UParent.MergeDiaryNo + ',' + m.Trim();
                            }
                        }
                    }
                }

                //Update BracketQuestionID
                if (UParent.BracketedWithQNo != null)
                {
                    string[] BQN = UParent.BracketedWithQNo.Split(',');
                    UParent.BracketedWithQNo = null;
                    foreach (var m in BQN)
                    {
                        if (Convert.ToInt16(m.Trim()) != Obj2.QuestionID)
                        {
                            if (UParent.BracketedWithQNo == null)
                            {
                                UParent.BracketedWithQNo = m.Trim();
                            }
                            else
                            {
                                UParent.BracketedWithQNo = UParent.BracketedWithQNo + ',' + m.Trim();
                            }
                        }
                    }
                }

                //Update ReferenceNumber
                if (UParent.ReferencedNumber != null)
                {
                    string[] RN = UParent.ReferencedNumber.Split(',');
                    UParent.ReferencedNumber = null;
                    foreach (var m in RN)
                    {
                        if (Convert.ToInt16(m.Trim()) != Obj2.QuestionID)
                        {
                            if (UParent.ReferencedNumber == null)
                            {
                                UParent.ReferencedNumber = m.Trim();
                            }
                            else
                            {
                                UParent.ReferencedNumber = UParent.ReferencedNumber + ',' + m.Trim();
                            }
                        }
                    }
                }

                //Update ReferenceMember
                if (UParent.ReferenceMemberCode != null)
                {
                    string[] RMC = UParent.ReferenceMemberCode.Split(',');
                    UParent.ReferenceMemberCode = null;
                    foreach (var m in RMC)
                    {
                        if (Convert.ToInt16(m.Trim()) != Obj2.MemberID)
                        {
                            if (UParent.ReferenceMemberCode == null)
                            {
                                UParent.ReferenceMemberCode = m.Trim();
                            }
                            else
                            {
                                UParent.ReferenceMemberCode = UParent.ReferenceMemberCode + ',' + m.Trim();
                            }
                        }
                    }
                }

                //Remove Bracketing
                Obj2.IsBracket = null;
                Obj2.BracketedWithDNo = null;

                DiaCtx.SaveChanges();


                //Get Updated Bracket Count
                var UCnt = (from Q in DiaCtx.objQuestion
                            where
                             (Q.AssemblyID == Obj.AssemblyID)
                            && (Q.SessionID == Obj.SessionID)
                            && (Q.QuestionType == Obj.QuestionTypeId)
                            && (Q.IsBracket == true)
                            select Q).ToList().Count();

                //Get Updated ForClubbing Count
                var UClubbCnt = (from Q in DiaCtx.objQuestion
                                 where (Q.QuestionType == Obj.QuestionTypeId)
                                 && (Q.AssemblyID == Obj.AssemblyID)
                                && (Q.SessionID == Obj.SessionID)
                                && (Q.BracketedWithQNo != null)
                                && (Q.MergeDiaryNo != null)
                                && (Q.IsClubbed == null)
                                 select Q).ToList().Count();

                Obj.Message = "Diary Number " + Obj2.DiaryNumber + " is removed from Bracketing. This was bracketed with Diary Number " + UParent.DiaryNumber + " !";
                Obj.STotalCnt = UCnt;
                Obj.CTotalCnt = UClubbCnt;

                DiaCtx.Close();
            }

            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj;
        }
        public static object GetChangedNotice(object param)
        {

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel objDiary = param as DiaryModel;
            tMemberNotice ObjNotice = new tMemberNotice();
            SiteSettings model = new SiteSettings();
            if (objDiary.AssemblyID != 0 && objDiary.SessionID != 0)
            {
                model.AssemblyCode = objDiary.AssemblyID;
                model.SessionCode = objDiary.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from N in DiaCtx.ObjNotices
                          where N.AssemblyID == model.AssemblyCode
                          && (N.SessionID == model.SessionCode)
                          && (N.OldNoticeTypeID != 0)
                          && (N.IsTypeChange == true)

                          select N).ToList();

            var evntList = (from E in DiaCtx.objmEvent
                            where E.PaperCategoryTypeId == 4
                            select E).ToList();

            ObjNotice.memberNoticeList = result;
            ObjNotice.eventList = evntList;
            mEvent Evnt = new mEvent();
            Evnt.EventId = 0;
            Evnt.RuleNo = "Select Rule";
            ObjNotice.eventList.Add(Evnt);
            return ObjNotice;
        }
        public static string GetRuleNameByRuleId(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            int RuleId = (int)param;
            var result = (from E in DiaCtx.objmEvent
                          where E.EventId == RuleId
                          select E.RuleNo).SingleOrDefault();
            return result;
        }

        public static object GetNoticeForTypeChange(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel objDiary = param as DiaryModel;
            tMemberNotice ObjNotice = new tMemberNotice();
            SiteSettings model = new SiteSettings();
            if (objDiary.AssemblyID != 0 && objDiary.SessionID != 0)
            {
                model.AssemblyCode = objDiary.AssemblyID;
                model.SessionCode = objDiary.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from N in DiaCtx.ObjNotices
                          where N.AssemblyID == model.AssemblyCode
                          && (N.SessionID == model.SessionCode)
                          && (N.IsTypeChange == false)
                          select N).ToList();

            var evntList = (from E in DiaCtx.objmEvent
                            where E.PaperCategoryTypeId == 4
                            select E).ToList();

            ObjNotice.memberNoticeList = result;
            ObjNotice.eventList = evntList;

            mEvent Evnt = new mEvent();
            Evnt.EventId = 0;
            Evnt.RuleNo = "Select Rule";
            ObjNotice.eventList.Add(Evnt);
            return ObjNotice;
        }

        public static object ChangeNoticeRuleById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tMemberNotice ObjNotice = new tMemberNotice();
            try
            {
                string[] Ids = Obj.RIds.Split(',');
                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);
                    ObjNotice = DiaCtx.ObjNotices.Single(m => m.NoticeId == id);
                    Obj.AssemblyID = ObjNotice.AssemblyID;
                    Obj.SessionID = ObjNotice.SessionID;
                    DiaCtx.ObjNotices.Attach(ObjNotice);


                    if (ObjNotice.OriNoticeTypeID == 0)
                    {
                        //capture Original Rule Id 
                        ObjNotice.OriNoticeTypeID = ObjNotice.NoticeTypeID;
                        ObjNotice.OldNoticeTypeID = ObjNotice.NoticeTypeID;
                        //Change Rule Id 
                        ObjNotice.NoticeTypeID = Obj.RuleId;
                        ObjNotice.ChangeTypeRemarks = "Notice Rule Changed";
                        ObjNotice.IsTypeChange = true;
                        ObjNotice.TypeChangeDate = DateTime.Now;
                    }
                    else if (ObjNotice.OriNoticeTypeID != 0)
                    {
                        if (ObjNotice.OriNoticeTypeID == Obj.RuleId)
                        {
                            //clear all changes when it is original rule Id
                            ObjNotice.OriNoticeTypeID = 0;
                            ObjNotice.OldNoticeTypeID = 0;
                            ObjNotice.NoticeTypeID = Obj.RuleId;
                            ObjNotice.ChangeTypeRemarks = null;
                            ObjNotice.IsTypeChange = false;
                            ObjNotice.TypeChangeDate = null;
                        }
                        else
                        {

                            ObjNotice.OldNoticeTypeID = ObjNotice.NoticeTypeID;
                            //Change Rule Id 
                            ObjNotice.NoticeTypeID = Obj.RuleId;
                            ObjNotice.TypeChangeDate = DateTime.Now;
                        }
                    }


                    DiaCtx.SaveChanges();
                }

                //Get Notice Change Cnt
                var CCnt = (from N in DiaCtx.ObjNotices
                            where
                             (N.AssemblyID == Obj.AssemblyID)
                            && (N.SessionID == Obj.SessionID)
                            && (N.IsTypeChange == true)
                            && (N.OldNoticeTypeID != 0)
                            select N).ToList().Count();

                Obj.Message = "Rule Number Changed Successfully !";
                Obj.STotalCnt = CCnt;


                DiaCtx.Close();
            }

            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj;
        }

        public static int GetChangedRuleNoticeCount(object param)
        {
            var obj = (tMemberNotice)GetChangedNotice(param);
            return obj.memberNoticeList.Count();
        }

        public static object GetFixedQuesPDFByDate(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            if (Obj.QuestionTypeId == (int)QuestionType.StartedQuestion)
            {
                var SPDF = (from SD in DiaCtx.objSessDate
                            where (SD.AssemblyId == Obj.AssemblyID)
                            && (SD.SessionId == Obj.SessionID)
                            && (SD.Id == Obj.SessionDateId)
                            && (SD.ApprovedByTrans == true)
                            select new { SD.StartQListPath, SD.ApprovedBySecrt }).FirstOrDefault();
                var SPPPDF = (from SD in DiaCtx.objSessDate
                              where (SD.AssemblyId == Obj.AssemblyID)
                              && (SD.SessionId == Obj.SessionID)
                              && (SD.Id == Obj.SessionDateId)
                              && (SD.SPPApprovedByTrans == true)
                              select new { SD.SPPQListPath, SD.SPPApprovedBySecrt }).FirstOrDefault();

                if (SPDF != null)
                {
                    Obj.FilePath = Convert.ToString(SPDF.StartQListPath);
                    Obj.CQPDFApprdBySert = SPDF.ApprovedBySecrt;
                }

                if (SPPPDF != null)
                {

                    Obj.AttachFilePath = Convert.ToString(SPPPDF.SPPQListPath);
                    Obj.PQPDFApprdBySert = SPPPDF.SPPApprovedBySecrt;
                }
            }
            else if (Obj.QuestionTypeId == (int)QuestionType.UnstaredQuestion)
            {
                var UPDF = (from SD in DiaCtx.objSessDate
                            where (SD.AssemblyId == Obj.AssemblyID)
                            && (SD.SessionId == Obj.SessionID)
                            && (SD.Id == Obj.SessionDateId)
                            && (SD.UnstarredApprovedByTrans == true)
                            select new { SD.UnStartQListPath, SD.UnstarredApprovedBySecrt }).FirstOrDefault();
                var UPPPDF = (from SD in DiaCtx.objSessDate
                              where (SD.AssemblyId == Obj.AssemblyID)
                              && (SD.SessionId == Obj.SessionID)
                              && (SD.Id == Obj.SessionDateId)
                              && (SD.UPPApprovedByTrans == true)
                              select new { SD.UPPQListPath, SD.UPPApprovedBySecrt }).FirstOrDefault();
                if (UPDF != null)
                {

                    Obj.FilePath = Convert.ToString(UPDF.UnStartQListPath);
                    Obj.CQPDFApprdBySert = UPDF.UnstarredApprovedBySecrt;
                }
                if (UPPPDF != null)
                {
                    Obj.AttachFilePath = Convert.ToString(UPPPDF.UPPQListPath);
                    Obj.PQPDFApprdBySert = UPPPDF.UPPApprovedBySecrt;
                }
            }



            return Obj;
        }




        public static object GetAllSiteSettings()
        {
            SiteSettingContext SiteCtx = new SiteSettingContext();
            SiteSettings model = new SiteSettings();

            string AssemblyCode = (from mdl in SiteCtx.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            string SessionCode = (from mdl in SiteCtx.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();
            string LastQNum = (from mdl in SiteCtx.SiteSettings where mdl.SettingName == "LastQuestionNumber" select mdl.SettingValue).SingleOrDefault();
            string LastQNumUn = (from mdl in SiteCtx.SiteSettings where mdl.SettingName == "LastQuestionNumberUnstarred" select mdl.SettingValue).SingleOrDefault();
            model.SessionCode = Convert.ToInt32(SessionCode);
            model.AssemblyCode = Convert.ToInt32(AssemblyCode);
            model.PreSLQNum = Convert.ToInt32(LastQNum);
            model.PreSLQNumUn = Convert.ToInt32(LastQNumUn);
            return model;
        }

        public static object GetSessionDateById(DiaryModel Obj)
        {
            SessionContext SCtx = new SessionContext();

            /*Get Session date according to session date id */
            var SDt = (from SD in SCtx.mSessDate
                       where (SD.SessionId == Obj.SessionID)
                       && (SD.AssemblyId == Obj.AssemblyID)
                       && (SD.Id == Obj.SessionDateId)
                       select SD.SessionDate).FirstOrDefault();

            return Convert.ToDateTime(SDt);
        }


        public static object GetFixedDateBySDtId(object param)
        {
            SessionContext SCtx = new SessionContext();
            SiteSettings model = new SiteSettings();
            int Sid = (int)param;

            //model = (SiteSettings)GetAllSiteSettings();
            //var SDt = (from SD in SCtx.mSessDate
            //           where (SD.SessionId == model.SessionCode)
            //           && (SD.AssemblyId == model.AssemblyCode)
            //           && (SD.Id == Sid)
            //           select SD.SessionDate).FirstOrDefault();

            var SDt = (from SD in SCtx.mSessDate
                       where (SD.Id == Sid)
                       select SD.SessionDate).FirstOrDefault();

            return Convert.ToDateTime(SDt);
        }

        //public static object GetFixedDateBySDtIdForPostPone(object param)
        //{
        //    SessionContext SCtx = new SessionContext();
        //    SiteSettings model = new SiteSettings();
        //    DiaryModel ObjDiary = param as DiaryModel;

        //    if (ObjDiary.AssemblyID == 0 && ObjDiary.SessionID == 0)
        //    {
        //        model = (SiteSettings)GetAllSiteSettings();
        //    }
        //    else
        //    {
        //        model.AssemblyCode = ObjDiary.AssemblyID;
        //        model.SessionCode = ObjDiary.SessionID;
        //    }
        //    var SDt = (from SD in SCtx.mSessDate
        //               where (SD.SessionId == model.SessionCode)
        //               && (SD.AssemblyId == model.AssemblyCode)
        //               && (SD.Id == ObjDiary.SessionDateId)
        //               select SD.SessionDate).FirstOrDefault();

        //    return Convert.ToDateTime(SDt);
        //}

        public static object GetMemberList(int AssemblyId)
        {
            DiariesContext DiaCtx = new DiariesContext();
            var Memlist = (from M in DiaCtx.objmMember
                           join MA in DiaCtx.ObjMemAssem on M.MemberCode equals MA.MemberID
                           //join Const in DiaCtx.ObjConst on MA.ConstituencyCode equals Const.ConstituencyCode
                           where //(Const.AssemblyID == AssemblyId) &&
                               (MA.AssemblyID == AssemblyId)
                               && !(from ExcepMinis in DiaCtx.objmMinistry where ExcepMinis.AssemblyID == AssemblyId select ExcepMinis.MemberCode).Contains(M.MemberCode)
                           orderby M.Name
                           select new DiaryModel
                           {
                               MemberId = MA.MemberID,
                               MemberName = M.Name,
                               //ConstituencyName = Const.ConstituencyName,
                               //ConstituencyCode = Const.ConstituencyCode
                           }).ToList();

            //foreach (var item in Memlist)
            //{
            //    item.MemberName = item.MemberName + " (" + item.ConstituencyName + "-" + item.ConstituencyCode.ToString() + ")";
            //}

            return Memlist;
        }

        #endregion



        #region "Rejected And Starred To Unstarred Question List created on 28-07-2014"

        static int GetStarredRejectedCount(object param)
        {

            var obj = (List<tQuestion>)GetStarredRejectedList(param);
            return obj.Count();
        }

        static int GetUnstarredRejectedCount(object param)
        {
            var obj = (List<tQuestion>)GetUnstarredRejectedList(param);
            return obj.Count();
        }

        static int GetStarredToUnstarredCount(object param)
        {
            var obj = (List<tQuestion>)GetStarredToUnstarredList(param);
            return obj.Count();
        }

        static object GetStarredRejectedList(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();

            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {

                model = (SiteSettings)GetAllSiteSettings();
            }

            var result = (from Q in DiaCtx.objQuestion
                          where //(Q.QuestionStatus == (int)Questionstatus.QuestionProofReading)
                                //&& (Q.DiaryNumber != null)
                          (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == (int)QuestionType.StartedQuestion)
                          && (Q.IsRejected == true)

                          select Q).ToList();



            return result;

        }

        static object GetUnstarredRejectedList(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();

            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }


            var result = (from Q in DiaCtx.objQuestion
                          where //(Q.QuestionStatus == (int)Questionstatus.QuestionProofReading)
                                //&& (Q.DiaryNumber != null)
                         (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)
                          && (Q.IsRejected == true)

                          select Q).ToList();



            return result;

        }

        static object GetStarredToUnstarredList(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where
                           (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.OldQuestionType == (int)QuestionType.StartedQuestion)
                          && (Q.IsTypeChange == true)
                          //&& ((Q.QuestionStatus == (int)Questionstatus.QuestionFreeze) || (Q.QuestionStatus == (int)Questionstatus.QuestionSent))
                          select Q).ToList();



            return result;

        }

        static object GetStarredQuestionsForTypeChange(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == (int)QuestionType.StartedQuestion)
                          //((Q.QuestionStatus == (int)Questionstatus.QuestionFreeze) || (Q.QuestionStatus == (int)Questionstatus.QuestionSent))
                          select Q).ToList();
            return result;

        }

        static object GetAttachmentByNoticeId(object param)
        {
            DiariesContext db = new DiariesContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            int NoticeId = Convert.ToInt32(param);


            var query = (from Ntces in db.ObjNotices
                         where Ntces.NoticeId == NoticeId
                         select new
                         {
                             Ntces.OriDiaryFilePath,
                             Ntces.OriDiaryFileName
                         }).FirstOrDefault();

            // For Accessing File Path 
            var FAP = (from SS in settingContext.SiteSettings
                       where SS.SettingName == "SecureFileAccessingUrlPath"
                       select SS.SettingValue).FirstOrDefault();



            return FAP + "/" + query.OriDiaryFilePath + query.OriDiaryFileName;

        }


        static object GetConstNameLocalByNo(object param)
        {
            if (null == param)
            {
                return null;
            }

            DiariesContext DiaCtx = new DiariesContext();
            string CN = param as string;
            int ConstNo = Convert.ToInt16(CN);
            var Constituency = (from C in DiaCtx.ObjConst
                                where C.ConstituencyCode == ConstNo
                                select new DiaryModel
                                {
                                    ConstituencyCode = C.ConstituencyCode,
                                    ConstituencyName = C.ConstituencyName_Local

                                }).FirstOrDefault();
            return Constituency;

        }

        static object GetAssemblyList()
        {
            NoticeContext pCtxt = new NoticeContext();
            var AssemList = (from A in pCtxt.mAssemblies
                             select A).ToList();

            return AssemList;
        }

        static object GetSessionLsitbyAssemblyId(object param)
        {
            DiaryModel DM = param as DiaryModel;
            SiteSettings model = new SiteSettings();

            if (DM.AssemblyID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
            }
            else
            {

                model = (SiteSettings)GetAllSiteSettings();
            }

            NoticeContext pCtxt = new NoticeContext();

            var SessList = (from S in pCtxt.mSessions
                            where S.AssemblyID == model.AssemblyCode
                            select S).OrderByDescending(x=>x.StartDate).ToList();
            return SessList;
        }


        #endregion

        #endregion

        #region Updated LOB PApers

        static object GetLOBUpdatedPaperBySessionDate(object param)
        {
            //  LegislationFixation
            return null;
        }
        #endregion


        static int GetNoticeAssignPendingCount(object param)
        {
            DiaryModel DM = param as DiaryModel;
            try
            {
                //var result = (List<tMemberNotice>)GetNoticePendingList();
                //return result.ToList().Count();
                var result = (tMemberNotice)GetNoticeAssignList(1, 1, DM.AssemblyID, DM.SessionID);
                return result.ResultCount;
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticeAssignList(int PageSize, int PageIndex, int AssemId, int SessionId)
        {
            SiteSettings model = new SiteSettings();
            if (AssemId != 0 && SessionId != 0)
            {
                model.AssemblyCode = AssemId;
                model.SessionCode = SessionId;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            tMemberNotice NObj = new tMemberNotice();
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    //var result = (from MN in db.tMemberNotices
                    //              where MN.NoticeNumber != null
                    //              && (MN.TypistRequired == true)
                    //              && (MN.NoticeStatus == (int)NoticeStatusEnum.NoticeToTypist ||
                    //              MN.NoticeStatus == (int)NoticeStatusEnum.NoticeAssignTypist ||
                    //              MN.NoticeStatus == (int)NoticeStatusEnum.NoticeFreeze ||
                    //              MN.NoticeStatus == (int)NoticeStatusEnum.NoticePublished ||
                    //              MN.NoticeStatus == (int)NoticeStatusEnum.NoticeSent)
                    //              && (MN.AssemblyID == model.AssemblyCode)
                    //              && (MN.SessionID == model.SessionCode)
                    //              select MN).ToList();
                    var result = (from MN in db.tMemberNotices
                                  where MN.NoticeNumber != null
                                  && (MN.TypistRequired == true)
                                  && (MN.TypistUserIdNotice == null)
                                  && (MN.AssemblyID == model.AssemblyCode)
                                  && (MN.SessionID == model.SessionCode)
                                  select MN).ToList();
                    foreach (var item in result)
                    {
                        item.RuleNo = (from e in db.mEvents where e.EventId == item.NoticeTypeID select e.RuleNo).FirstOrDefault();

                    }


                    NObj.ResultCount = result.Count();
                    NObj.memberNoticeList = result.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
                    return NObj;
                }
            }
            catch
            {
                throw;
            }
        }


        //shashi


        static object GetStarredQuestionListForCancel(object param)
        {
            //tQuestion Obj = param as tQuestion;
            TransferQuestionModel Obj = param as TransferQuestionModel;

            int istar = (int)Obj.QuestionStatus;
            DateTime dt = DateTime.Today;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {

                    var res = (from Q in db.tQuestions
                               where Q.QuestionNumber != null && Q.IsFixed != null && Q.IsFixedDate != null
                               && (Q.AssemblyID == Obj.AssemblyID)
                               && (Q.SessionID == Obj.SessionID)// for question
                               && (Q.QuestionType == istar)// for starred
                                                           //&& Q.IsFixedDate < dt 
                               orderby Q.QuestionID
                               select new TransferQuestionModel
                               {
                                   DepartmentName = (from mc in db.mDepartment where mc.deptId == Q.DepartmentID select mc.deptname).FirstOrDefault(),
                                   CommonNumberForAll = Q.DiaryNumber,
                                   QuestionNumber = Q.QuestionNumber,
                                   MinistryName = (from M in db.mMinistry
                                                   where M.MinistryID == Q.MinistryId
                                                   select M.MinistryName).FirstOrDefault(),
                                   ItemId = Q.QuestionID,
                                   Subject = Q.Subject,
                                   AskedBy = (from mc in db.mMember where mc.MemberCode == Q.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                   // IsPending = Q.IsPending,
                                   IsContentFreeze = Q.IsContentFreeze,
                                   IsDetailFreeze = Q.IsQuestionDetailFreez,
                                   IsQuestionFreeze = Q.IsQuestionFreeze,
                                   IsInitialApprove = Q.IsInitialApproved,
                                   IsRejected = Q.IsRejected,
                                   IsTypeChanged = Q.IsTypeChange,
                                   IsPaperLaid = true,
                                   QuesStage = Q.QuestionStatus,
                                   IsFinalApprove = Q.IsFinalApproved,
                                   Priority = Q.Priority,
                                   IsTranslation = Q.IsTranslation,
                                   IsClubbed = Q.IsClubbed,
                                   IsBracketed = Q.IsBracket,
                                   PaperCategoryTypeId = Q.QuestionType,
                                   IsFixedDate = Q.IsFixedDate

                               }).ToList();

                    Obj.tQuestionModel = res;
                    return Obj;
                }
            }
            catch
            {
                throw;
            }
        }
        static object GetCancelledQuestionList(object param)
        {
            tQuestion Obj = param as tQuestion;
            int istar = (int)Obj.QuestionStatus;
            DateTime dt = DateTime.Today;
            try
            {
                using (LegislationFixationContext db = new LegislationFixationContext())
                {
                    var res = (from Q in db.tQuestions
                                   //join t in db.tCancelQuestionAuditTrial on Q.QuestionID equals t.QuestionID into T_Leftjoin
                                   //from t in T_Leftjoin.DefaultIfEmpty()
                               where Q.QuestionNumber != null && Q.DiaryNumber != null && Q.IsFixed == null && Q.IsFixedDate == null
                               && Q.IsFinalApproved == null && Q.IsFinalApprovedDate == null && Q.NoticeID == 9
                               && (Q.AssemblyID == Obj.AssemblyID)
                               && (Q.SessionID == Obj.SessionID)// for question
                               && (Q.QuestionType == istar)// for starred                      
                               orderby Q.QuestionID
                               select Q
                               ).ToList();

                    List<tQuestion> Listques = new List<tQuestion>();
                    foreach (var item in res)
                    {
                        tQuestion model = new tQuestion();
                        model = item;
                        model.IsFixedDate = item.IsFixedDate;
                        model.IsFixedDate = (from audit in db.tCancelQuestionAuditTrial
                                             where audit.QuestionID == item.QuestionID
                                             orderby audit.SNo descending
                                             select
                                                       audit.IsFixedDate
                                  ).FirstOrDefault();
                        Listques.Add(model);
                    }

                    return Listques;
                }
            }
            catch
            {
                throw;
            }
        }

        //End
        public static object GetSearchEventDemands(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            CutMotionModel Obj = param as CutMotionModel;
            CutMotionModel Obj2 = new CutMotionModel();
            //var PndList = (from Ntces in DiaCtx.ObjNotices
            //               where Ntces.NoticeId == Obj.NoticeId
            //               select new DiaryModel
            //               {
            //                   NoticeNumber = Ntces.NoticeNumber,
            //                   MemId = Ntces.MemberId,
            //                   EventId = Ntces.NoticeTypeID,
            //                   CatchWordSubject = Ntces.SubInCatchWord,
            //                   Subject = Ntces.Subject,
            //                   MinistryId = Ntces.MinistryId,
            //                   DepartmentId = Ntces.DepartmentId,
            //                   DateForBind = Ntces.NoticeDate,
            //                   TimeForBind = Ntces.NoticeTime,
            //                   NoticeId = Ntces.NoticeId,
            //                   Notice = Ntces.Notice,
            //                   NoticeStatus = Ntces.NoticeStatus,
            //                   CMDemandId = Ntces.DemandID
            //               }).FirstOrDefault();
            //Obj2 = PndList;

            ///*Get All Session date according to session id */
            //var SDt = (from SD in SCtx.mSessDate
            //           where SD.SessionId == Obj.SessionID
            //           select SD).ToList();

            //foreach (var item in SDt)
            //{
            //    item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            //}
            //Obj2.SessDateList = SDt;



            /*Get All Event or Category*/

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 4
                           orderby Evnt.EventName
                           select Evnt).ToList();
            Obj2.eventList = EvtList;

            /*Get All MinistryMinister*/
            //var MinMisList = (from Min in DiaCtx.objmMinistry
            //                  join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
            //                  orderby Min.OrderID
            //                  select new mMinisteryMinisterModel
            //                  {
            //                      MinistryID = minis.MinistryID,
            //                      MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
            //                  }).ToList();
            //Obj2.memMinList = MinMisList;

            /*Get All Department*/
            //if (PndList.MinistryId != null && PndList.MinistryId != 0)
            //{
            //    var DeptList = (from minsDept in DiaCtx.objMinisDepart
            //                    join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
            //                    where minsDept.MinistryID == PndList.MinistryId
            //                    select new DiaryModel
            //                    {
            //                        DepartmentId = dept.deptId,
            //                        DepartmentName = dept.deptname

            //                    }).ToList();

            //    Obj2.DiaryList = DeptList;
            //}
            /*Get All Member*/
            //if (PndList.MemId != 0 && PndList.MemId != null)
            //{
            //    var Member = (from Mem in DiaCtx.objmMember
            //                  where Mem.MemberCode == PndList.MemId
            //                  select Mem).FirstOrDefault();

            //    Obj2.MemberName = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
            //}
            /*Get All Demand*/
            var DemandList = (from Demand in DiaCtx.tCutMotionDemand
                              select Demand).ToList();
            Obj2.DemandList = DemandList;
            DiaCtx.Close();


            return Obj2;
        }


        public static object CheckBracktingByNoticeNumber(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Dmdl = param as DiaryModel;
            tMemberNotice QuesObj = new tMemberNotice();
            tMemberNotice Obj2 = new tMemberNotice();
            int DMatchCount = 0;
            string DiaryNum = "";
            try
            {

                QuesObj = (from Q in DiaCtx.ObjNotices
                           where Q.NoticeNumber == Dmdl.NoticeNumber
                           select Q).SingleOrDefault();

                if (QuesObj != null)
                {
                    //string[] Ids = Dmdl.QuesIds.Split(',');
                    //for (int i = 0; i < Ids.Length; i++)
                    //{
                    //    int id = Convert.ToInt16(Ids[i]);
                    //    Obj2 = DiaCtx.ObjNotices.Single(m => m.NoticeId == id);
                    //    if (Obj2.DepartmentId != QuesObj.DepartmentId)
                    //    {
                    //        DMatchCount = DMatchCount + 1;
                    //        DiaryNum = Obj2.NoticeNumber;
                    //        break;
                    //    }
                    //}
                    //if (QuesObj.NoticeId > Obj2.NoticeId)
                    //{
                    //    Dmdl.Message = "Cut Motion can be only Bracket with Before received Cut Motion.";
                    //}
                    //else if (QuesObj.AssemblyID != Obj2.AssemblyID)
                    //if (QuesObj.AssemblyID != Obj2.AssemblyID)
                    //{
                    //    Dmdl.Message = "Cut Motion can not be Bracket in different Assembly.";
                    //}
                    //else if (QuesObj.SessionID != Obj2.SessionID)
                    //{
                    //    Dmdl.Message = "Cut Motion can not be Bracket in different Session.";
                    //}
                    //else if (DMatchCount > 0)
                    //{
                    //    Dmdl.Message = "Department should be same for bracketing. Department of " + DiaryNum + " is different from " + Dmdl.DiaryNumber;
                    //}
                    if (QuesObj.IsBracketCM == true)
                    {
                        Dmdl.Message = "This Diary Number is already Bracket with Diary Number " + QuesObj.BracketedWithNoticeNo;
                    }

                    else
                    {
                        Dmdl.Message = "Not Bracket";
                    }

                }
                else
                {
                    Dmdl.Message = "Invalid Diary Number";
                }
                DiaCtx.Close();
            }
            catch (Exception ex)
            {
                Dmdl.Message = ex.Message;
            }

            return Dmdl;
        }


        public static object BracketCutMotionByNoticeNumber(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tMemberNotice Obj2 = new tMemberNotice();
            tMemberNotice UParent = new tMemberNotice();
            int AssemId = 0;
            int SessId = 0;
            try
            {


                string[] Ids = Obj.QuesIds.Split(',');
                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);
                    Obj2 = DiaCtx.ObjNotices.Single(m => m.NoticeId == id);
                    AssemId = Obj2.AssemblyID;
                    SessId = Obj2.SessionID;
                    DiaCtx.ObjNotices.Attach(Obj2);

                    //Do Bracketing
                    Obj2.IsBracketCM = true;
                    Obj2.BracketedWithNoticeNo = Obj.NoticeNumber;

                    // Update Parent Question Status
                    UParent = DiaCtx.ObjNotices.Single(m => m.NoticeNumber == Obj.NoticeNumber);
                    DiaCtx.ObjNotices.Attach(UParent);

                    //Update MeregeDiaryNo
                    if (UParent.MergeDiaryNoCM == null || UParent.MergeDiaryNoCM == "")
                    {
                        UParent.MergeDiaryNoCM = Obj2.NoticeNumber;
                    }
                    else
                    {
                        UParent.MergeDiaryNoCM = UParent.MergeDiaryNoCM + ',' + Obj2.NoticeNumber;
                    }
                    //Update BracketQuestionID
                    if (UParent.BracketedWithNoticeIdNo == null || UParent.BracketedWithNoticeIdNo == "")
                    {
                        UParent.BracketedWithNoticeIdNo = Convert.ToString(UParent.NoticeId) + ',' + Obj2.NoticeId;
                    }
                    else
                    {
                        UParent.BracketedWithNoticeIdNo = UParent.BracketedWithNoticeIdNo + ',' + Obj2.NoticeId;
                    }
                    //Update ReferenceNumber
                    if (UParent.ReferencedNumberCm == null || UParent.ReferencedNumberCm == "")
                    {
                        UParent.ReferencedNumberCm = Convert.ToString(UParent.NoticeId) + ',' + Obj2.NoticeId;
                    }
                    else
                    {
                        UParent.ReferencedNumberCm = UParent.ReferencedNumberCm + ',' + Obj2.NoticeId;
                    }
                    //Update ReferenceMember
                    if (UParent.ReferenceCMMemberCode == null || UParent.ReferenceCMMemberCode == "")
                    {
                        if (UParent.MemberId != Obj2.MemberId)
                        {
                            UParent.ReferenceCMMemberCode = Convert.ToString(UParent.MemberId) + ',' + Obj2.MemberId;
                        }
                        else
                        {
                            UParent.ReferenceCMMemberCode = Convert.ToString(UParent.MemberId);
                        }
                    }
                    else
                    {
                        string[] mid = UParent.ReferenceCMMemberCode.Split(',');
                        var matchCount = 0;
                        foreach (var m in mid)
                        {
                            if (Convert.ToInt16(m) == Obj2.MemberId)
                            {
                                matchCount = matchCount + 1;
                            }
                        }

                        if (matchCount == 0)
                        {
                            UParent.ReferenceCMMemberCode = Convert.ToString(UParent.ReferenceCMMemberCode) + ',' + Obj2.MemberId;
                        }

                    }

                    DiaCtx.SaveChanges();
                }

                //Get Updated Bracket Count
                var UCnt = (from Q in DiaCtx.ObjNotices
                            where
                             (Q.AssemblyID == AssemId)
                            && (Q.SessionID == SessId)
                            && (Q.IsBracketCM == true)
                            select Q).ToList().Count();

                //Get Updated ForClubbing Count
                var UClubbCnt = (from Q in DiaCtx.ObjNotices
                                 where
                                (Q.AssemblyID == AssemId)
                                && (Q.SessionID == SessId)
                                && (Q.BracketedWithNoticeNo != null)
                                && (Q.MergeDiaryNoCM != null)
                                && (Q.IsCMClubbed == null)
                                 select Q).ToList().Count();


                Obj.Message = "Cut Motion are sucessfully bracketd with NoticeNumber " + Obj.NoticeNumber + " !";
                Obj.STotalCnt = UCnt;
                Obj.CTotalCnt = UClubbCnt;


                DiaCtx.Close();
            }

            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj;
        }

        public static object GetFixedQuestion(object param)
        {
            ChangeDeptModule model = param as ChangeDeptModule;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from questions in pCtxt.tQuestions
                         where (questions.AssemblyID == model.AssemblyID) && (questions.SessionID == model.SessionID) && (questions.IsSubmitted == true)
                         select new ChangeDeptModule
                         {
                             QuestionID = questions.QuestionID,
                             QuestionTypeId = questions.QuestionType,
                             DiaryNumber = questions.DiaryNumber,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DepartmentId = questions.DepartmentID,
                             DepartmentName = (from mc in pCtxt.mDepartments where mc.deptId == questions.DepartmentID select mc.deptname).FirstOrDefault(),
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }


        public static object GetAllQDetailById(object param)
        {

            ChangeDeptModule model = param as ChangeDeptModule;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where (questions.QuestionID == model.QuestionID)
                         select new ChangeDeptModule
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.QuestionNumber,
                             Subject = questions.Subject,
                             MemberId = questions.MemberID,
                             MainQuestion = questions.MainQuestion,
                             NoticeDate = questions.NoticeRecievedDate,
                             DiaryNumber = questions.DiaryNumber,
                             DepartmentId = questions.DepartmentID,
                             AssemblyID = questions.AssemblyID,
                             SessionID = questions.SessionID,
                             QuestionTypeId = questions.QuestionType,
                             MinistryId = questions.MinistryId,
                             MinisterName = (from mc in pCtxt.mMinistry
                                             where (mc.MinistryID == questions.MinistryId && mc.AssemblyID == questions.AssemblyID)
                                             select mc.MinisterName).FirstOrDefault(),
                             SecAadharId = (from mc in pCtxt.mUsers
                                            where (mc.IsSecretoryId == true && mc.DeptId.Contains(questions.DepartmentID))
                                            select mc.AadarId).FirstOrDefault(),
                             SecAadharName = (from mc in pCtxt.AdhaarDetails
                                              join PLT in pCtxt.mUsers on mc.AdhaarID equals PLT.AadarId
                                              where (PLT.IsSecretoryId == true && PLT.DeptId.Contains(questions.DepartmentID))
                                              select mc.Name).FirstOrDefault(),
                             //SecAadharId = (from mc in pCtxt.SecDept
                             //             join PLT in pCtxt.mSecretory on mc.SecretoryID  equals PLT.SecretoryID
                             //             where (mc.DepartmentID == questions.DepartmentID && mc.IsActive == true)
                             //             select PLT.AadhaarId).FirstOrDefault(),
                             //SecAadharName = (from mc in pCtxt.SecDept
                             //               join PLT in pCtxt.mSecretory on mc.SecretoryID equals PLT.SecretoryID
                             //               where (mc.DepartmentID == questions.DepartmentID && mc.IsActive == true)
                             //               select PLT.SecretoryName).FirstOrDefault(),
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DepartmentName = (from mc in pCtxt.mDepartments where mc.deptId == questions.DepartmentID select mc.deptname).FirstOrDefault(),
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }
        static object UpdateDeptChangeQuestions(object param)
        {

            LegislationFixationContext qCtxDB = new LegislationFixationContext();
            tQuestion update = param as tQuestion;
            mchangeDepartmentAuditTrail QuesObj = new mchangeDepartmentAuditTrail();
            //    NoticeContext pCtxt = new NoticeContext();
            tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
            obj.DepartmentID = update.DepartmentID;
            obj.OldDepartmentId = update.OldDepartmentId;
            obj.OldMinistryId = update.OldMinistryId;
            obj.MinistryId = update.MinistryId;
            qCtxDB.SaveChanges();
            string msg = "";

            try
            {

                QuesObj.AssemblyID = update.AssemblyID;
                QuesObj.SessionID = update.SessionID;
                QuesObj.QuestionType = update.QuestionType;
                QuesObj.DepartmentID = update.DepartmentID;
                QuesObj.ChangedDepartmentID = update.OldDepartmentId;
                QuesObj.DiaryNumber = update.DiaryNumber;
                QuesObj.AadharId = update.FileName;
                QuesObj.UserName = "Commissioner (Department)";
                QuesObj.TransferByName = "Commissioner (Department)";
                QuesObj.TransferByAadharId = update.FileName;
                QuesObj.ChangedDate = DateTime.Now;
                qCtxDB.mchangeDepartmentAuditTrail.Add(QuesObj);
                qCtxDB.SaveChanges();
                qCtxDB.Close();
            }


            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return msg;
        }

        public static object GetSentNotices(object param)
        {
            ChangeDeptModule model = param as ChangeDeptModule;
            NoticeContext pCtxt = new NoticeContext();
            var query = (from Notice in pCtxt.tMemberNotices
                         where (Notice.AssemblyID == model.AssemblyID) && (Notice.SessionID == model.SessionID) && (Notice.IsSubmitted == true)
                         select new ChangeDeptModule
                         {
                             NoticeId = Notice.NoticeId,
                             NoticeNumber = Notice.NoticeNumber,
                             Subject = Notice.Subject,
                             NoticeDate = Notice.NoticeDate,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notice.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DepartmentId = Notice.DepartmentId,
                             DepartmentName = (from mc in pCtxt.mDepartments where mc.deptId == Notice.DepartmentId select mc.deptname).FirstOrDefault(),
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }

        public static object GetAllNoticeDetailById(object param)
        {

            ChangeDeptModule model = param as ChangeDeptModule;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from Notice in pCtxt.tMemberNotices
                         where (Notice.NoticeId == model.NoticeId)
                         select new ChangeDeptModule
                         {
                             NoticeId = Notice.NoticeId,
                             Subject = Notice.Subject,
                             MemberId = Notice.MemberId,
                             MainQuestion = Notice.Notice,
                             NoticeDate = Notice.NoticeDate,
                             DiaryNumber = Notice.NoticeNumber,
                             DepartmentId = Notice.DepartmentId,
                             AssemblyID = Notice.AssemblyID,
                             SessionID = Notice.SessionID,
                             MinistryId = Notice.MinistryId,
                             MinisterName = (from mc in pCtxt.mMinistry
                                             where (mc.MinistryID == Notice.MinistryId && mc.AssemblyID == Notice.AssemblyID)
                                             select mc.MinisterName).FirstOrDefault(),
                             SecAadharId = (from mc in pCtxt.mUsers
                                            where (mc.IsSecretoryId == true && mc.DeptId.Contains(Notice.DepartmentId))
                                            select mc.AadarId).FirstOrDefault(),
                             SecAadharName = (from mc in pCtxt.AdhaarDetails
                                              join PLT in pCtxt.mUsers on mc.AdhaarID equals PLT.AadarId
                                              where (PLT.IsSecretoryId == true && PLT.DeptId.Contains(Notice.DepartmentId))
                                              select mc.Name).FirstOrDefault(),
                             //SecAadharId = (from mc in pCtxt.SecDept
                             //             join PLT in pCtxt.mSecretory on mc.SecretoryID  equals PLT.SecretoryID
                             //             where (mc.DepartmentID == questions.DepartmentID && mc.IsActive == true)
                             //             select PLT.AadhaarId).FirstOrDefault(),
                             //SecAadharName = (from mc in pCtxt.SecDept
                             //               join PLT in pCtxt.mSecretory on mc.SecretoryID equals PLT.SecretoryID
                             //               where (mc.DepartmentID == questions.DepartmentID && mc.IsActive == true)
                             //               select PLT.SecretoryName).FirstOrDefault(),
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == Notice.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DepartmentName = (from mc in pCtxt.mDepartments where mc.deptId == Notice.DepartmentId select mc.deptname).FirstOrDefault(),
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();

            model.tQuestionModel = results;

            return model;
        }

        static object UpdateDeptChangeNotices(object param)
        {

            LegislationFixationContext qCtxDB = new LegislationFixationContext();
            tMemberNotice update = param as tMemberNotice;
            mchangeDepartmentAuditTrail QuesObj = new mchangeDepartmentAuditTrail();
            tMemberNotice obj = qCtxDB.tMemberNotices.Single(m => m.NoticeId == update.NoticeId);
            obj.DepartmentName = (from mc in qCtxDB.mDepartment where mc.deptId == update.DepartmentId select mc.deptname).FirstOrDefault();
            obj.MinisterName = (from mc in qCtxDB.mMinistry
                                where (mc.MinistryID == update.MinistryId && mc.AssemblyID == update.AssemblyID)
                                select mc.MinistryName).FirstOrDefault();
            obj.DepartmentId = update.DepartmentId;
            obj.OldDepartmentId = update.OldDepartmentId;
            obj.OldMinistryId = update.OldMinistryId;
            obj.MinistryId = update.MinistryId;
            qCtxDB.SaveChanges();
            string msg = "";

            try
            {

                QuesObj.AssemblyID = update.AssemblyID;
                QuesObj.SessionID = update.SessionID;
                QuesObj.DepartmentID = update.DepartmentId;
                QuesObj.ChangedDepartmentID = update.OldDepartmentId;
                QuesObj.DiaryNumber = update.NoticeNumber;
                QuesObj.NoticeNumber = update.NoticeNumber;
                QuesObj.AadharId = update.FileName;
                QuesObj.UserName = "Commissioner (Department)";
                QuesObj.TransferByName = "Commissioner (Department)";
                QuesObj.TransferByAadharId = update.FileName;
                QuesObj.ChangedDate = DateTime.Now;
                qCtxDB.mchangeDepartmentAuditTrail.Add(QuesObj);
                qCtxDB.SaveChanges();
                qCtxDB.Close();
            }


            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return msg;
        }

        public static object QuestionFixedSessionDateCommasep(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var CQList = (from Q in DiaCtx.objQuestion
                          where (Q.QuestionType == Obj.QuestionTypeId)
                          && (Q.AssemblyID == Obj.AssemblyID)
                          && (Q.SessionID == Obj.SessionID)
                          && (Q.IsFixed == true)
                          && ((Obj.SessionDateId == 0) || (Q.SessionDateId == Obj.SessionDateId))
                           && (Q.QuestionStatus == Obj.QuestionStatus)
                            && (Q.DeActivateFlag == null)
                          orderby Q.QuestionID
                          select new DiaryModel
                          {
                              QuestionId = Q.QuestionID,
                              Subject = Q.Subject,
                              DiaryNumber = Q.DiaryNumber,
                              MinistryId = Q.MinistryId,
                              DepartmentId = Q.DepartmentID,
                              MemberId = Q.MemberID,
                              DateForBind = Q.NoticeRecievedDate,
                              TimeForBind = Q.NoticeRecievedTime,
                              QuestionNumber = Q.QuestionNumber,
                              IsFinalApprove = Q.IsFinalApproved,
                              IsPostPoneDate = Q.IsPostponeDate,
                              Flag = Q.DeActivateFlag,
                              IsHindi = Q.IsHindi

                          }).ToList();


            foreach (var item in CQList)
            {

                if (string.IsNullOrEmpty(Obj.CurrentQuesIds))
                {
                    Obj.CurrentQuesIds = item.QuestionId.ToString();
                }
                else
                {
                    Obj.CurrentQuesIds = Obj.CurrentQuesIds + "," + item.QuestionId.ToString();
                }


            }

            var PQList = (from Q in DiaCtx.objQuestion
                          where (Q.QuestionType == Obj.QuestionTypeId)
                          && (Q.AssemblyID == Obj.AssemblyID)
                          && (Q.SessionID == Obj.SessionID)
                          && (Q.IsFixed == true)
                          && ((Obj.SessionDateId == 0) || (Q.SessionDateId == Obj.SessionDateId))
                           && (Q.QuestionStatus == Obj.QuestionStatus)
                           && (Q.DeActivateFlag == "RF")
                          orderby Q.QuestionID
                          select new DiaryModel
                          {
                              QuestionId = Q.QuestionID,
                              Subject = Q.Subject,
                              DiaryNumber = Q.DiaryNumber,
                              MinistryId = Q.MinistryId,
                              DepartmentId = Q.DepartmentID,
                              MemberId = Q.MemberID,
                              DateForBind = Q.NoticeRecievedDate,
                              TimeForBind = Q.NoticeRecievedTime,
                              QuestionNumber = Q.QuestionNumber,
                              IsFinalApprove = Q.IsFinalApproved,
                              IsPostPoneDate = Q.IsPostponeDate,
                              Flag = Q.DeActivateFlag,
                              IsHindi = Q.IsHindi

                          }).ToList();

            foreach (var item in PQList)
            {

                if (string.IsNullOrEmpty(Obj.PostPoneQuesIds))
                {
                    Obj.PostPoneQuesIds = item.QuestionId.ToString();
                }
                else
                {
                    Obj.PostPoneQuesIds = Obj.PostPoneQuesIds + "," + item.QuestionId.ToString();
                }


            }
            if (Obj.SessionDateId != 0)
            {
                mSessionDate SDateObj = (from SD in DiaCtx.objSessDate
                                         where SD.Id == Obj.SessionDateId
                                         select SD).FirstOrDefault();
                Obj.SessionDate = Convert.ToDateTime(SDateObj.SessionDate).ToString("dd/MM/yyyy");
            }
            return Obj;
        }


        static tMemberNotice GetAcknowledgeStarredQuestion(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where questions.IsAcknowledgmentDate != null && questions.QuestionType == 1 && questions.AssemblyID == model.AssemblyCode
                         && questions.SessionID == model.SessionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             MainQuestion = questions.MainQuestion,
                             IsHindi = questions.IsHindi,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             MemberNameLocal = (from mch in pCtxt.mMembers where mch.MemberCode == questions.MemberID select mch.NameLocal).FirstOrDefault(),
                             IsAcknowledgmentDate = questions.IsAcknowledgmentDate,
                             DepartmentId = questions.DepartmentID
                         }).ToList();

            int totalRecords = query.Count();
            model.ResultCount = totalRecords;
            model.tQuestionModel = query;

            return model;

        }
        static tMemberNotice GetAcknowledgeUnStarredQuestion(object param)
        {
            tMemberNotice model = param as tMemberNotice;

            NoticeContext pCtxt = new NoticeContext();

            var query = (from questions in pCtxt.tQuestions
                         where questions.IsAcknowledgmentDate != null && questions.QuestionType == 2 && questions.AssemblyID == model.AssemblyCode
                         && questions.SessionID == model.SessionID
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             DiaryNumber = questions.DiaryNumber,
                             Subject = questions.Subject,
                             NoticeDate = questions.NoticeRecievedDate,
                             MainQuestion = questions.MainQuestion,
                             IsHindi = questions.IsHindi,
                             MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             MemberNameLocal = (from mch in pCtxt.mMembers where mch.MemberCode == questions.MemberID select mch.NameLocal).FirstOrDefault(),
                             DepartmentId = questions.DepartmentID,
                         }).ToList();

            int totalRecords = query.Count();
            model.ResultCount = totalRecords;
            model.tQuestionModel = query;

            return model;

        }
        //for Excess Question

        public static object GetQuestionsForExcess(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == DM.QuestionTypeId)
                          && (Q.IsBracket == null || Q.IsBracket == false)
                          && (Q.MergeDiaryNo == null || Q.MergeDiaryNo == "")
                          && (Q.SessionDateId == null)
                          && (Q.IsRejected == null)
                          && (Q.IsExcessQuestion == null)
                          select Q).ToList();
            return result;
        }



        public static object ExcessingQuestions(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Dmdl = param as DiaryModel;
            //tQuestion QuesObj = new tQuestion();
            tQuestion Obj2 = new tQuestion();
            try
            {
                string[] Ids = Dmdl.QuesIds.Split(',');
                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);
                    Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                    Obj2.IsExcessQuestion = true;
                    DiaCtx.SaveChanges();
                    Obj2.Message = "Question(s) Excessed Successfully";
                }
                if (Dmdl.QuestionTypeId == 1)
                {
                    Dmdl.StarredCountExess = (from Q in DiaCtx.objQuestion
                                              where (Q.AssemblyID == Dmdl.AssemblyID)
                                && (Q.SessionID == Dmdl.SessionID)
                                && (Q.QuestionType == Dmdl.QuestionTypeId)
                                && (Q.IsBracket == null || Q.IsBracket == false)
                                && (Q.MergeDiaryNo == null || Q.MergeDiaryNo == "")
                                && (Q.SessionDateId == null)
                                && (Q.IsRejected == null)
                                && (Q.IsExcessQuestion == null)
                                              select Q).ToList().Count();

                    Dmdl.StarredCountExessed = (from Q in DiaCtx.objQuestion
                                                where (Q.AssemblyID == Dmdl.AssemblyID)
                                  && (Q.SessionID == Dmdl.SessionID)
                                  && (Q.QuestionType == Dmdl.QuestionTypeId)
                                  && (Q.IsExcessQuestion == true)

                                                select Q).ToList().Count();
                }
                else if (Dmdl.QuestionTypeId == 2)
                {
                    Dmdl.UnStarredCountExess = (from Q in DiaCtx.objQuestion
                                                where (Q.AssemblyID == Dmdl.AssemblyID)
                                  && (Q.SessionID == Dmdl.SessionID)
                                  && (Q.QuestionType == Dmdl.QuestionTypeId)
                                  && (Q.IsBracket == null || Q.IsBracket == false)
                                  && (Q.MergeDiaryNo == null || Q.MergeDiaryNo == "")
                                  && (Q.SessionDateId == null)
                                  && (Q.IsRejected == null)
                                  && (Q.IsExcessQuestion == null)
                                                select Q).ToList().Count();

                    Dmdl.UnStarredCountExessed = (from Q in DiaCtx.objQuestion
                                                  where (Q.AssemblyID == Dmdl.AssemblyID)
                                    && (Q.SessionID == Dmdl.SessionID)
                                    && (Q.QuestionType == Dmdl.QuestionTypeId)
                                    && (Q.IsExcessQuestion == true)

                                                  select Q).ToList().Count();
                }

            }
            catch (Exception ex)
            {
                Dmdl.Message = ex.Message;
            }

            return Dmdl;
        }

        public static object GetExcessedQuestions(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == DM.QuestionTypeId)
                          && (Q.IsExcessQuestion == true)

                          select Q).ToList();
            return result;
        }

        public static object RemoveExcessingQuestions(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Dmdl = param as DiaryModel;
            //tQuestion QuesObj = new tQuestion();
            tQuestion Obj2 = new tQuestion();
            try
            {
                string[] Ids = Dmdl.QuesIds.Split(',');
                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);
                    Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                    Obj2.IsExcessQuestion = null;
                    DiaCtx.SaveChanges();
                    Obj2.Message = "Question(s) Excessed Successfully";
                }
                if (Dmdl.QuestionTypeId == 1)
                {
                    Dmdl.StarredCountExess = (from Q in DiaCtx.objQuestion
                                              where (Q.AssemblyID == Dmdl.AssemblyID)
                                && (Q.SessionID == Dmdl.SessionID)
                                && (Q.QuestionType == Dmdl.QuestionTypeId)
                                && (Q.IsBracket == null || Q.IsBracket == false)
                                && (Q.MergeDiaryNo == null || Q.MergeDiaryNo == "")
                                && (Q.SessionDateId == null)
                                && (Q.IsRejected == null)
                                && (Q.IsExcessQuestion == null)
                                              select Q).ToList().Count();

                    Dmdl.StarredCountExessed = (from Q in DiaCtx.objQuestion
                                                where (Q.AssemblyID == Dmdl.AssemblyID)
                                  && (Q.SessionID == Dmdl.SessionID)
                                  && (Q.QuestionType == Dmdl.QuestionTypeId)
                                  && (Q.IsExcessQuestion == true)

                                                select Q).ToList().Count();
                }
                else if (Dmdl.QuestionTypeId == 2)
                {
                    Dmdl.UnStarredCountExess = (from Q in DiaCtx.objQuestion
                                                where (Q.AssemblyID == Dmdl.AssemblyID)
                                  && (Q.SessionID == Dmdl.SessionID)
                                  && (Q.QuestionType == Dmdl.QuestionTypeId)
                                  && (Q.IsBracket == null || Q.IsBracket == false)
                                  && (Q.MergeDiaryNo == null || Q.MergeDiaryNo == "")
                                  && (Q.SessionDateId == null)
                                  && (Q.IsRejected == null)
                                  && (Q.IsExcessQuestion == null)
                                                select Q).ToList().Count();

                    Dmdl.UnStarredCountExessed = (from Q in DiaCtx.objQuestion
                                                  where (Q.AssemblyID == Dmdl.AssemblyID)
                                    && (Q.SessionID == Dmdl.SessionID)
                                    && (Q.QuestionType == Dmdl.QuestionTypeId)
                                    && (Q.IsExcessQuestion == true)

                                                  select Q).ToList().Count();
                }
            }
            catch (Exception ex)
            {
                Dmdl.Message = ex.Message;
            }

            return Dmdl;
        }

        public static object GetCountQuestionsForExcess(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == 1)
                          && (Q.IsBracket == null || Q.IsBracket == false)
                          && (Q.MergeDiaryNo == null || Q.MergeDiaryNo == "")
                          && (Q.SessionDateId == null)
                          && (Q.IsRejected == null)
                          && (Q.IsExcessQuestion == null)
                          select Q).ToList().Count();
            return result;
        }
        public static object GetCountExcessedQuestions(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == 1)
                          && (Q.IsExcessQuestion == true)

                          select Q).ToList().Count();
            return result;
        }

        public static object GetUnStarredCountQuestionsForExcess(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == 2)
                          && (Q.IsBracket == null || Q.IsBracket == false)
                          && (Q.MergeDiaryNo == null || Q.MergeDiaryNo == "")
                          && (Q.SessionDateId == null)
                          && (Q.IsRejected == null)
                          && (Q.IsExcessQuestion == null)
                          select Q).ToList().Count();
            return result;
        }
        public static object GetCountUnStarredExcessedQuestions(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == 2)
                          && (Q.IsExcessQuestion == true)

                          select Q).ToList().Count();
            return result;
        }

        //WithDrawn By Member

        public static object GetQuestionsForWithdrawn(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == DM.QuestionTypeId)
                          && (Q.IsBracket == null || Q.IsBracket == false)
                          && (Q.IsRejected == null)
                          && (Q.IsWithdrawbyMember == null)
                          select Q).ToList();
            return result;
        }

        public static object WithdranwQuestions(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Dmdl = param as DiaryModel;
            tQuestion Obj2 = new tQuestion();
            try
            {

                int id = Convert.ToInt16(Dmdl.QuesIds);
                Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == id);
                Obj2.IsWithdrawbyMember = true;
                Obj2.WithdrawbyMemberDateTime = DateTime.Now;
                DiaCtx.SaveChanges();
                Obj2.Message = "Question(s) Withdrawn Successfully";

                if (Dmdl.QuestionTypeId == 1)
                {
                    Dmdl.StarredCountForWithdrawn = (from Q in DiaCtx.objQuestion
                                                     where (Q.AssemblyID == Dmdl.AssemblyID)
                          && (Q.SessionID == Dmdl.SessionID)
                          && (Q.QuestionType == Dmdl.QuestionTypeId)
                          && (Q.IsBracket == null || Q.IsBracket == false)
                          && (Q.IsRejected == null)
                          && (Q.IsWithdrawbyMember == null)
                                                     select Q).ToList().Count();


                    Dmdl.StarredWithdrawnCount = (from Q in DiaCtx.objQuestion
                                                  where (Q.AssemblyID == Dmdl.AssemblyID)
                                    && (Q.SessionID == Dmdl.SessionID)
                                    && (Q.QuestionType == Dmdl.QuestionTypeId)
                                     && (Q.IsWithdrawbyMember == true)

                                                  select Q).ToList().Count();
                }
                else if (Dmdl.QuestionTypeId == 2)
                {
                    Dmdl.UnstarredCountForWithdrawn = (from Q in DiaCtx.objQuestion
                                                       where (Q.AssemblyID == Dmdl.AssemblyID)
                                 && (Q.SessionID == Dmdl.SessionID)
                                 && (Q.QuestionType == Dmdl.QuestionTypeId)
                                 && (Q.IsBracket == null || Q.IsBracket == false)
                                 && (Q.IsRejected == null)
                                 && (Q.IsWithdrawbyMember == null)
                                                       select Q).ToList().Count();

                    Dmdl.UntarredWithdrawnCount = (from Q in DiaCtx.objQuestion
                                                   where (Q.AssemblyID == Dmdl.AssemblyID)
                                   && (Q.SessionID == Dmdl.SessionID)
                                   && (Q.QuestionType == Dmdl.QuestionTypeId)
                                    && (Q.IsWithdrawbyMember == true)

                                                   select Q).ToList().Count();
                }

            }
            catch (Exception ex)
            {
                Dmdl.Message = ex.Message;
            }

            return Dmdl;
        }
        public static object GetListOfWithdrawnQuest(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == DM.QuestionTypeId)
                          && (Q.IsWithdrawbyMember == true)
                          select Q).ToList();
            return result;
        }
        public static object GetStarredforWithdrawnCount(object param)
        {

            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == 1)
                          && (Q.IsBracket == null || Q.IsBracket == false)
                          && (Q.IsRejected == null)
                          && (Q.IsWithdrawbyMember == null)
                          select Q).ToList().Count();
            return result;
        }
        public static object GetStarredListWithdrawnCount(object param)
        {

            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == 1)
                          && (Q.IsWithdrawbyMember == true)
                          select Q).ToList().Count();
            return result;
        }




        public static object GetUnstarredforWithdrawnCount(object param)
        {

            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == 2)
                          && (Q.IsBracket == null || Q.IsBracket == false)
                          && (Q.IsRejected == null)
                          && (Q.IsWithdrawbyMember == null)
                          select Q).ToList().Count();
            return result;
        }
        public static object GetUnstarredListWithdrawnCount(object param)
        {

            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();
            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {
                model = (SiteSettings)GetAllSiteSettings();
            }
            var result = (from Q in DiaCtx.objQuestion
                          where (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == 2)
                          && (Q.IsWithdrawbyMember == true)
                          select Q).ToList().Count();
            return result;
        }


        public static object GetRulesDepartment(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            CutMotionModel Obj = param as CutMotionModel;
            CutMotionModel Obj2 = new CutMotionModel();
            MinisteryContext DBContext = new MinisteryContext();
            mMinistry obj = param as mMinistry;
            /*Get All Event or Category*/

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 4
                           orderby Evnt.EventName
                           select Evnt).ToList();
            Obj2.eventList = EvtList;

            /*Get All Department*/

            var departmentList = (from ministryDept in DBContext.mMinistryDepartments
                                  join dept in DBContext.mDepartments on ministryDept.DeptID equals dept.deptId
                                  join SDept in DBContext.SecDept on dept.deptId equals SDept.DepartmentID
                                  orderby dept.deptname ascending
                                  select new tMemberNotice
                                  {
                                      DepartmentId = dept.deptId,
                                      DepartmentName = dept.deptname

                                  }).Distinct().ToList();
            Obj2.DepartmentLt = departmentList;

            return Obj2;
        }
        public static object BracktingForNoticeNumber(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Dmdl = param as DiaryModel;
            tMemberNotice QuesObj = new tMemberNotice();
            tMemberNotice Obj2 = new tMemberNotice();
            int DMatchCount = 0;
            string DiaryNum = "";
            try
            {

                QuesObj = (from Q in DiaCtx.ObjNotices
                           where Q.NoticeNumber == Dmdl.NoticeNumber
                           select Q).SingleOrDefault();

                if (QuesObj != null)
                {
                    //string[] Ids = Dmdl.QuesIds.Split(',');
                    //for (int i = 0; i < Ids.Length; i++)
                    //{
                    //    int id = Convert.ToInt16(Ids[i]);
                    //    Obj2 = DiaCtx.ObjNotices.Single(m => m.NoticeId == id);
                    //    if (Obj2.DepartmentId != QuesObj.DepartmentId)
                    //    {
                    //        DMatchCount = DMatchCount + 1;
                    //        DiaryNum = Obj2.NoticeNumber;
                    //        break;
                    //    }
                    //}
                    //if (QuesObj.NoticeId > Obj2.NoticeId)
                    //{
                    //    Dmdl.Message = "Cut Motion can be only Bracket with Before received Cut Motion.";
                    //}
                    //else if (QuesObj.AssemblyID != Obj2.AssemblyID)
                    //if (QuesObj.AssemblyID != Obj2.AssemblyID)
                    //{
                    //    Dmdl.Message = "Cut Motion can not be Bracket in different Assembly.";
                    //}
                    //else if (QuesObj.SessionID != Obj2.SessionID)
                    //{
                    //    Dmdl.Message = "Cut Motion can not be Bracket in different Session.";
                    //}
                    //else if (DMatchCount > 0)
                    //{
                    //    Dmdl.Message = "Department should be same for bracketing. Department of " + DiaryNum + " is different from " + Dmdl.DiaryNumber;
                    //}
                    if (QuesObj.IsBracketCM == true)
                    {
                        Dmdl.Message = "This Diary Number is already Bracket with Diary Number " + QuesObj.BracketedWithNoticeNo;
                    }

                    else
                    {
                        Dmdl.Message = "Not Bracket";
                    }

                }
                else
                {
                    Dmdl.Message = "Invalid Diary Number";
                }
                DiaCtx.Close();
            }
            catch (Exception ex)
            {
                Dmdl.Message = ex.Message;
            }

            return Dmdl;
        }


        public static object BracketNoticeByNoticeNumber(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel Obj = param as DiaryModel;
            tMemberNotice Obj2 = new tMemberNotice();
            tMemberNotice UParent = new tMemberNotice();
            int AssemId = 0;
            int SessId = 0;
            try
            {


                string[] Ids = Obj.QuesIds.Split(',');
                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);
                    Obj2 = DiaCtx.ObjNotices.Single(m => m.NoticeId == id);
                    AssemId = Obj2.AssemblyID;
                    SessId = Obj2.SessionID;
                    DiaCtx.ObjNotices.Attach(Obj2);

                    //Do Bracketing
                    Obj2.IsBracketCM = true;
                    Obj2.BracketedWithNoticeNo = Obj.NoticeNumber;

                    // Update Parent Question Status
                    UParent = DiaCtx.ObjNotices.Single(m => m.NoticeNumber == Obj.NoticeNumber);
                    DiaCtx.ObjNotices.Attach(UParent);

                    //Update MeregeDiaryNo
                    if (UParent.MergeDiaryNoCM == null || UParent.MergeDiaryNoCM == "")
                    {
                        UParent.MergeDiaryNoCM = Obj2.NoticeNumber;
                    }
                    else
                    {
                        UParent.MergeDiaryNoCM = UParent.MergeDiaryNoCM + ',' + Obj2.NoticeNumber;
                    }
                    //Update BracketQuestionID
                    if (UParent.BracketedWithNoticeIdNo == null || UParent.BracketedWithNoticeIdNo == "")
                    {
                        UParent.BracketedWithNoticeIdNo = Convert.ToString(UParent.NoticeId) + ',' + Obj2.NoticeId;
                    }
                    else
                    {
                        UParent.BracketedWithNoticeIdNo = UParent.BracketedWithNoticeIdNo + ',' + Obj2.NoticeId;
                    }
                    //Update ReferenceNumber
                    if (UParent.ReferencedNumberCm == null || UParent.ReferencedNumberCm == "")
                    {
                        UParent.ReferencedNumberCm = Convert.ToString(UParent.NoticeId) + ',' + Obj2.NoticeId;
                    }
                    else
                    {
                        UParent.ReferencedNumberCm = UParent.ReferencedNumberCm + ',' + Obj2.NoticeId;
                    }
                    //Update ReferenceMember
                    if (UParent.ReferenceCMMemberCode == null || UParent.ReferenceCMMemberCode == "")
                    {
                        if (UParent.MemberId != Obj2.MemberId)
                        {
                            UParent.ReferenceCMMemberCode = Convert.ToString(UParent.MemberId) + ',' + Obj2.MemberId;
                        }
                        else
                        {
                            UParent.ReferenceCMMemberCode = Convert.ToString(UParent.MemberId);
                        }
                    }
                    else
                    {
                        string[] mid = UParent.ReferenceCMMemberCode.Split(',');
                        var matchCount = 0;
                        foreach (var m in mid)
                        {
                            if (Convert.ToInt16(m) == Obj2.MemberId)
                            {
                                matchCount = matchCount + 1;
                            }
                        }

                        if (matchCount == 0)
                        {
                            UParent.ReferenceCMMemberCode = Convert.ToString(UParent.ReferenceCMMemberCode) + ',' + Obj2.MemberId;
                        }

                    }

                    DiaCtx.SaveChanges();
                }

                //Get Updated Bracket Count
                var UCnt = (from Q in DiaCtx.ObjNotices
                            where
                             (Q.AssemblyID == AssemId)
                            && (Q.SessionID == SessId)
                            && (Q.IsBracketCM == true)
                            select Q).ToList().Count();

                //Get Updated ForClubbing Count
                var UClubbCnt = (from Q in DiaCtx.ObjNotices
                                 where
                                (Q.AssemblyID == AssemId)
                                && (Q.SessionID == SessId)
                                && (Q.BracketedWithNoticeNo != null)
                                && (Q.MergeDiaryNoCM != null)
                                && (Q.IsCMClubbed == null)
                                 select Q).ToList().Count();


                Obj.Message = "Notices are sucessfully bracketd with NoticeNumber " + Obj.NoticeNumber + " !";
                Obj.STotalCnt = UCnt;
                Obj.CTotalCnt = UClubbCnt;


                DiaCtx.Close();
            }

            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }
            Obj.countClub = (from CutM in pCtxt.tMemberNotices
                             where (CutM.AssemblyID == Obj2.AssemblyID) && (CutM.SessionID == Obj2.SessionID) && (CutM.BracketedWithNoticeIdNo != null) && (CutM.IsCMClubbed == null) && (CutM.MergeDiaryNoCM != null) && (CutM.NoticeTypeID != 74) && (CutM.NoticeTypeID != 75) && (CutM.NoticeTypeID != 76)
                             select new CutMotionModel
                             {
                                 NoticeId = CutM.NoticeId,
                                 NoticeNumber = CutM.NoticeNumber,
                                 Subject = CutM.Subject,
                                 NoticeDate = CutM.NoticeDate,
                                 ClubbedDNo = CutM.MergeDiaryNoCM,
                                 //DemandNo = CutM.DemandID,
                                 EventName = (from E in pCtxt.mEvents where E.EventId == CutM.NoticeTypeID select E.EventName).FirstOrDefault(),
                                 DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == CutM.DemandID select mc.DemandName).FirstOrDefault(),
                                 MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == CutM.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                             }).ToList().Count();

            Obj.countClubbed = (from CutM in pCtxt.tMemberNotices
                                where (CutM.AssemblyID == Obj2.AssemblyID) && (CutM.SessionID == Obj2.SessionID) && (CutM.BracketedWithNoticeIdNo != null) && (CutM.IsCMClubbed != null) && (CutM.MergeDiaryNoCM != null) && (CutM.NoticeTypeID != 74) && (CutM.NoticeTypeID != 75) && (CutM.NoticeTypeID != 76)
                                select new CutMotionModel
                                {
                                    NoticeId = CutM.NoticeId,
                                    NoticeNumber = CutM.NoticeNumber,
                                    Subject = CutM.Subject,
                                    NoticeDate = CutM.NoticeDate,
                                    ClubbedDNo = CutM.MergeDiaryNoCM,
                                    //DemandNo = CutM.DemandID,
                                    EventName = (from E in pCtxt.mEvents where E.EventId == CutM.NoticeTypeID select E.EventName).FirstOrDefault(),
                                    DemandName = (from mc in pCtxt.tCutMotionDemand where mc.DemandNo == CutM.DemandID select mc.DemandName).FirstOrDefault(),
                                    MemberName = (from mc in pCtxt.mMembers where mc.MemberCode == CutM.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault()
                                }).ToList().Count();

            //   return NtceObj;
            return Obj;
        }

        public static object GetNoticeEditDiary(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            NoticeContext pCtxt = new NoticeContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();

            var PndList = (from Ntces in DiaCtx.ObjNotices
                           where Ntces.NoticeId == Obj.NoticeId
                           select new DiaryModel
                           {
                               NoticeNumber = Ntces.NoticeNumber,
                               MemId = Ntces.MemberId,
                               MemberId = Ntces.MemberId.Value,
                               EventId = Ntces.NoticeTypeID,
                               CatchWordSubject = Ntces.SubInCatchWord,
                               Subject = Ntces.Subject,
                               MinistryId = Ntces.MinistryId,
                               DepartmentId = Ntces.DepartmentId,
                               DateForBind = Ntces.NoticeDate,
                               TimeForBind = Ntces.NoticeTime,
                               NoticeId = Ntces.NoticeId,
                               Notice = Ntces.Notice,
                               NoticeStatus = Ntces.NoticeStatus,
                               DemandNo = Ntces.DemandID,
                               AttachFileName = Ntces.CutMotionFileName,
                               AttachFilePath = Ntces.CutMotionFilePath,

                           }).FirstOrDefault();
            Obj2 = PndList;

            var FL = (from SS in pCtxt.SiteSettings
                      where SS.SettingName == "FileAccessingUrlPath"
                      select SS.SettingValue).FirstOrDefault();

            Obj2.AttachFileName = FL + "/" + PndList.AttachFilePath + PndList.AttachFileName;

            var Memlist = (from M in DiaCtx.objmMember
                           join MA in DiaCtx.ObjMemAssem on M.MemberCode equals MA.MemberID
                           join Const in DiaCtx.ObjConst on MA.ConstituencyCode equals Const.ConstituencyCode
                           where (Const.AssemblyID == Obj.AssemblyID)
                               && (MA.AssemblyID == Obj.AssemblyID)
                               && !(from ExcepMinis in DiaCtx.objmMinistry where ExcepMinis.AssemblyID == Obj.AssemblyID select ExcepMinis.MemberCode).Contains(M.MemberCode)
                           orderby M.Name
                           select new DiaryModel
                           {
                               MemberId = MA.MemberID,
                               MemberName = M.Name,
                               ConstituencyName = Const.ConstituencyName,
                               ConstituencyCode = Const.ConstituencyCode
                           }).ToList();

            foreach (var item in Memlist)
            {
                item.MemberName = item.MemberName + " (" + item.ConstituencyName + "-" + item.ConstituencyCode.ToString() + ")";
            }
            Obj2.memberList = Memlist;
            /*Get All Event or Category*/

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 4
                           select Evnt).ToList();
            Obj2.eventList = EvtList;

            /*Get All MinistryMinister*/
            var MinMisList = (from Min in DiaCtx.objmMinistry
                              join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
                              orderby Min.OrderID
                              where minis.AssemblyID == Obj.AssemblyID && Min.IsActive == true && Min.AssemblyID == Obj.AssemblyID
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
                              }).ToList();
            Obj2.memMinList = MinMisList;

            /*Get All Department*/
            if (PndList.MinistryId != null && PndList.MinistryId != 0)
            {
                var DeptList = (from minsDept in DiaCtx.objMinisDepart
                                join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
                                where minsDept.MinistryID == PndList.MinistryId
                                select new DiaryModel
                                {
                                    DepartmentId = dept.deptId,
                                    DepartmentName = dept.deptname

                                }).ToList();

                Obj2.DiaryList = DeptList;
            }
            /*Get All Member*/
            if (PndList.MemId != 0 && PndList.MemId != null)
            {
                var Member = (from Mem in DiaCtx.objmMember
                              where Mem.MemberCode == PndList.MemId
                              select Mem).FirstOrDefault();

                Obj2.MemberName = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
            }
            /*Get All Demand*/
            var DemandList = (from Demand in DiaCtx.tCutMotionDemand
                              select Demand).ToList();
            Obj2.DemandList = DemandList;
            DiaCtx.Close();


            return Obj2;
        }
        static object UpdatetransferQuestions(object param)
        {
            LegislationFixationContext qCtxDB = new LegislationFixationContext();
            TransferQuestionModel model = param as TransferQuestionModel;
            tCancelQuestionAuditTrial QuesObj = new tCancelQuestionAuditTrial();
            string msg = "";
            model = (from Q in qCtxDB.tQuestions
                     where (Q.AssemblyID == model.AssemblyID)
                     && (Q.SessionID == model.SessionID)
                     && (Q.DiaryNumber == model.DiaryNumber)
                     && (Q.QuestionNumber == model.QuestionNumber)
                     select new TransferQuestionModel
                     {
                         AssemblyID = Q.AssemblyID,
                         SessionID = Q.SessionID,
                         DiaryNumber = Q.DiaryNumber,
                         QuestionNumber = Q.QuestionNumber,
                         QuestionID = Q.QuestionID,
                         IsFixedDate = Q.IsFixedDate,
                         IsNoticeFreezeDate = Q.IsFinalApprovedDate
                     }).FirstOrDefault();

            tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionNumber == model.QuestionNumber && m.AssemblyID == model.AssemblyID && m.SessionID == model.SessionID && m.DiaryNumber == model.DiaryNumber);
            obj.IsFixed = null;
            obj.IsFixedDate = null;
            obj.IsFinalApproved = null;
            obj.IsFinalApprovedDate = null;
            obj.NoticeID = 9;
            qCtxDB.SaveChanges();
            try
            {

                QuesObj.QuestionID = model.QuestionID;
                QuesObj.QuestionNumber = model.QuestionNumber;
                QuesObj.DiaryNumber = model.DiaryNumber;
                QuesObj.IsFixedDate = model.IsFixedDate;
                QuesObj.IsFinalApprovedDate = model.IsNoticeFreezeDate;
                QuesObj.CancelDate = DateTime.Now;
                qCtxDB.tCancelQuestionAuditTrial.Add(QuesObj);
                qCtxDB.SaveChanges();
                qCtxDB.Close();
            }


            catch (Exception ex)
            {
                msg = ex.Message;
            }

            return null;
        }



        public static object GetNoticesForPostpone(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var QList = (from Q in DiaCtx.ObjNotices
                         join evt in DiaCtx.objmEvent on Q.NoticeTypeID equals evt.EventId
                         where (Q.AssemblyID == Obj.AssemblyID)
                              && (Q.SessionID == Obj.SessionID)
                         && (Q.NoticeStatus == (int)NoticeStatusEnum.NoticeSent)
                         orderby Q.NoticeId
                         select new DiaryModel
                         {
                             NoticeId = Q.NoticeId,
                             Subject = Q.Subject,
                             NoticeNumber = Q.NoticeNumber,
                             MinistryId = Q.MinistryId,
                             DepartmentId = Q.DepartmentId,
                             MemId = Q.MemberId,
                             DateForBind = Q.IsPostponeDate,
                             IsPostPone = Q.IsPostpone,
                             EventName = evt.EventName
                         }).ToList();

            foreach (var item in QList)
            {
                if (item.MemId != 0)
                {
                    var Member = (from Mem in DiaCtx.objmMember
                                  where Mem.MemberCode == item.MemId
                                  select Mem).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.AskedBy = Member.NameLocal.ToString().Trim();

                    }
                    else
                    {
                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();

                    }

                }

                if (item.MinistryId != 0 && item.MinistryId != 85 && item.DepartmentId != null)
                {
                    var MinistryName = (from M in DiaCtx.objmMinistry
                                        where M.MinistryID == item.MinistryId
                                        select M).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.MinistryName = MinistryName.MinistryNameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.MinistryName = MinistryName.MinistryName.ToString().Trim();
                    }
                }

                if (item.DepartmentId != "" && item.DepartmentId != null)
                {
                    var DeptName = (from D in DiaCtx.objmDepartment
                                    where D.deptId == item.DepartmentId
                                    select D).FirstOrDefault();

                    if (item.IsHindi == true)
                    {
                        item.DepartmentName = DeptName.deptnameLocal.ToString().Trim();
                    }
                    else
                    {
                        item.DepartmentName = DeptName.deptname.ToString().Trim();
                    }
                }
            }

            Obj.DiaryList = QList;

            return Obj.DiaryList;
        }

        public static object PostponeNoticesbyId(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tMemberNotice QuesObj = new tMemberNotice();

            try
            {

                string[] Ids = Obj.QuesIds.Split(',');

                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);

                    QuesObj = DiaCtx.ObjNotices.Single(m => m.NoticeId == id);
                    DiaCtx.ObjNotices.Attach(QuesObj);
                    QuesObj.IsPostpone = true;
                    QuesObj.IsNoticeFreezeDate = null;
                    QuesObj.IsNoticeFreeze = null;
                    QuesObj.NoticeStatus = 1;
                    QuesObj.IsSubmitted = null;
                    QuesObj.SubmittedDate = null;
                    ///Use DeactiveFlag For Postpone Status
                    ///
                    QuesObj.DeActivateFlag = "P";
                    DiaCtx.SaveChanges();

                }

                Obj.Message = "Notices(s) has been Postponed Sucessfully !";

                DiaCtx.Close();

            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;

        }



        public static object SearchPostPoneBySessionFixNotice(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;

            var QList = (from Q in DiaCtx.ObjNotices
                         join evt in DiaCtx.objmEvent on Q.NoticeTypeID equals evt.EventId
                         where
                          (Q.AssemblyID == Obj.AssemblyID)
                         && (Q.SessionID == Obj.SessionID)
                         && (Q.IsPostpone == true)
                         orderby Q.NoticeId
                         select new DiaryModel
                         {
                             NoticeId = Q.NoticeId,
                             Subject = Q.Subject,
                             DiaryNumber = Q.NoticeNumber,
                             MinistryId = Q.MinistryId,
                             DepartmentId = Q.DepartmentId,
                             MemId = Q.MemberId,
                             DateForBind = Q.IsPostponeDate,
                             EventName = evt.EventName,
                             IsPostPone = Q.IsPostpone,
                             SessionID = Q.SessionID,
                             DeActivateFlag = Q.DeActivateFlag,
                         }).ToList();

            foreach (var item in QList)
            {
                if (item.MemId != 0)
                {
                    var Member = (from Mem in DiaCtx.objmMember
                                  where Mem.MemberCode == item.MemId
                                  select Mem).FirstOrDefault();
                    item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
                }

                if (item.MinistryId != 0 && item.DepartmentId != null)
                {
                    var MinistryName = (from M in DiaCtx.objmMinistry
                                        where M.MinistryID == item.MinistryId
                                        select M).FirstOrDefault();

                    item.MinistryName = MinistryName.MinistryName.ToString().Trim();


                }

                if (item.DepartmentId != "" && item.DepartmentId != null)
                {
                    var DeptName = (from D in DiaCtx.objmDepartment
                                    where D.deptId == item.DepartmentId
                                    select D).FirstOrDefault();
                    item.DepartmentName = DeptName.deptname.ToString().Trim();

                }
            }

            Obj.DiaryList = QList;

            return Obj;


        }

        public static object ReFixPostPoneNoticebyId(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();

            DiaryModel Obj = param as DiaryModel;

            ///Get Curretnt Assembly And Session
            SiteSettings model = new SiteSettings();

            model = (SiteSettings)GetAllSiteSettings();

            tMemberNotice QuesObj = new tMemberNotice();
            tMemberNotice ForCopyQuesObj = new tMemberNotice();
            var FL = (from SS in DiaCtx.mSiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();
            try
            {

                string[] Ids = Obj.QuesIds.Split(',');

                for (int i = 0; i < Ids.Length; i++)
                {
                    int id = Convert.ToInt16(Ids[i]);

                    ///Change Question Status in PostPoned Session 
                    QuesObj = DiaCtx.ObjNotices.Single(m => m.NoticeId == id && m.SessionID == Obj.SessionID);
                    DiaCtx.ObjNotices.Attach(QuesObj);
                    if (Obj.SessionID != model.SessionCode)
                    {
                        //QuesObj.IsPostpone = false;
                        //QuesObj.IsPostponeDate = null;

                        ///Use DeactiveFlag For Postpone ReFix Status
                        ///
                        QuesObj.DeActivateFlag = "RF";
                        DiaCtx.SaveChanges();


                        ///Copy Question in Current Session

                        ForCopyQuesObj = DiaCtx.ObjNotices.Single(m => m.NoticeId == id && m.SessionID == Obj.SessionID);
                        DiaCtx.ObjNotices.Attach(ForCopyQuesObj);
                        ForCopyQuesObj.SessionID = model.SessionCode;
                        ForCopyQuesObj.IsPostpone = false;
                        //ForCopyQuesObj.IsFixed = true;
                        //ForCopyQuesObj.IsFixedDate = (DateTime)GetFixedDateBySDtId(Obj.SessionDateId);
                        //ForCopyQuesObj.IsFixedTodayDate = DateTime.Now;
                        //ForCopyQuesObj.SessionDateId = Obj.SessionDateId;
                        ForCopyQuesObj.DeActivateFlag = "RF";
                        ForCopyQuesObj.PaperLaidId = null;
                        ForCopyQuesObj.QuestionSequence = null;
                        DiaCtx.ObjNotices.Add(ForCopyQuesObj);
                        DiaCtx.SaveChanges();
                        Obj.AssemblyID = ForCopyQuesObj.AssemblyID;
                        Obj.SessionID = ForCopyQuesObj.SessionID;
                        // Obj.QuestionTypeId = ForCopyQuesObj.QuestionType;
                        Obj.SaveFilePath = FL;

                    }
                    else if (Obj.SessionID == model.SessionCode)
                    {
                        QuesObj.IsPostpone = false;
                        //QuesObj.IsPostponeDate = null;
                        //QuesObj.IsFixed = true;
                        //QuesObj.IsFixedDate = (DateTime)GetFixedDateBySDtId(Obj.SessionDateId);
                        //QuesObj.IsFixedTodayDate = DateTime.Now;
                        //QuesObj.SessionDateId = Obj.SessionDateId;
                        QuesObj.DeActivateFlag = "RF";
                        DiaCtx.SaveChanges();

                    }

                }

                Obj.Message = "PostPoned Notices(s) Re-Fixed Successfully For Selected Current Session !";
                DiaCtx.Close();

            }
            catch (Exception ex)
            {
                Obj.Message = ex.Message;
            }

            return Obj.Message;
        }


        static object RejectedList(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();

            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {

                model = (SiteSettings)GetAllSiteSettings();
            }

            var result = (from Q in DiaCtx.objQuestion
                          join t in DiaCtx.objmMember on Q.MemberID equals t.MemberCode
                          join M in DiaCtx.objmMinistry on Q.MinistryId equals M.MinistryID
                          where
                          (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == (int)QuestionType.StartedQuestion)
                          && (Q.IsRejected == true)
                          select Q).ToList();
            return result;

        }

        static object UnRejectedList(object param)
        {
            DiaryModel DM = param as DiaryModel;
            DiariesContext DiaCtx = new DiariesContext();
            SiteSettings model = new SiteSettings();

            if (DM.AssemblyID != 0 && DM.SessionID != 0)
            {
                model.AssemblyCode = DM.AssemblyID;
                model.SessionCode = DM.SessionID;
            }
            else
            {

                model = (SiteSettings)GetAllSiteSettings();
            }

            var result = (from Q in DiaCtx.objQuestion
                          join t in DiaCtx.objmMember on Q.MemberID equals t.MemberCode
                          join M in DiaCtx.objmMinistry on Q.MinistryId equals M.MinistryID
                          where
                          (Q.AssemblyID == model.AssemblyCode)
                          && (Q.SessionID == model.SessionCode)
                          && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)
                          && (Q.IsRejected == true)

                          select Q).ToList();



            return result;

        }

    }

}

