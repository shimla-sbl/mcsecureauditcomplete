﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SBL.DAL;
using SBL.DomainModel.Models.FooterPublicData;

using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

namespace SBL.Domain.Context.FooterPublicDatas
{
    
        public class FooterPublicDataContext : DBBase<FooterPublicDataContext>
    {

        public FooterPublicDataContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<FooterPublicData> FooterPublicData { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateFooterPublicData": { CreateFooterPublicData(param.Parameter); break; }

                case "UpdateFooterPublicData": { return UpdateFooterPublicData(param.Parameter); }
                case "DeleteFooterPublicData": { return DeleteFooterPublicData(param.Parameter); }
                case "GetAllFooterPublicData": { return GetAllFooterPublicData(); }
                case "GetFooterPublicDataById": { return GetFooterPublicDataById(param.Parameter); }
                case "CheckId":
                    {
                        return CheckId(param.Parameter);
                    }
            }
            return null;
        }


        static void CreateFooterPublicData(object param)
        {
            try
            {
                using (FooterPublicDataContext db = new FooterPublicDataContext())
                {
                    FooterPublicData model = param as FooterPublicData;

                    db.FooterPublicData.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateFooterPublicData(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (FooterPublicDataContext db = new FooterPublicDataContext())
            {
                FooterPublicData model = param as FooterPublicData;

                db.FooterPublicData.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllFooterPublicData();
        }

        static object DeleteFooterPublicData(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (FooterPublicDataContext db = new FooterPublicDataContext())
            {
                FooterPublicData parameter = param as FooterPublicData;
                FooterPublicData stateToRemove = db.FooterPublicData.SingleOrDefault(a => a.ID == parameter.ID);
                //if (stateToRemove != null)
                //{
                //    stateToRemove.IsDeleted = true;
                //}
                db.FooterPublicData.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllFooterPublicData();
        }

        static List<FooterPublicData> GetAllFooterPublicData()
        {
            FooterPublicDataContext db = new FooterPublicDataContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.FooterPublicData
                         orderby a.ID descending

                         select a).ToList();

            return query;
        }

        static FooterPublicData GetFooterPublicDataById(object param)
        {
            FooterPublicData parameter = param as FooterPublicData;
            FooterPublicDataContext db = new FooterPublicDataContext();
            var query = db.FooterPublicData.SingleOrDefault(a => a.ID == parameter.ID);
            return query;
        }

        static object CheckId(object param)
        {

            FooterPublicDataContext qCtxDB = new FooterPublicDataContext();
            FooterPublicData updateSQ = param as FooterPublicData;

            var query = (from tQ in qCtxDB.FooterPublicData
                         where tQ.TitleID == updateSQ.TitleID
                         select tQ).ToList().Count();

            return query.ToString();
        }

    }
}
