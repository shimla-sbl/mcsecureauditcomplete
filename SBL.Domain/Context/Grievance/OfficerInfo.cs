﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Grievance
{
    [Serializable]
    public class OfficerInfo
    {

        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string DeptId { get; set; }
        public string AadarId { get; set; }
    }
}
