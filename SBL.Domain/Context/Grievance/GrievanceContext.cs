﻿using SBL.DAL;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserModule;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;

namespace SBL.Domain.Context.Grievance
{
    public class GrievanceContext : DBBase<GrievanceContext>
    {
        public GrievanceContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<tGrievanceReply> tGrievanceReply { get; set; }
        public DbSet<ForwardGrievance> ForwardGrievance { get; set; }
        public DbSet<tMemberGrievances> tMemberGrievances { get; set; }
        public DbSet<AdhaarDetails> AdhaarDetails { get; set; }
        public DbSet<mUserInformation> mUserInformation { get; set; }
        public virtual DbSet<mMember> mMembers { get; set; }

        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<tUserAccessRequest> tUserAccessRequest { get; set; }
        public DbSet<mOffice> mOffice { get; set; }
        public DbSet<tDepartmentOfficeMapped> tDepartmentOfficeMapped { get; set; }
        public DbSet<tGrievanceOfficerDeleted> tGrievanceOfficerDeleted { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateMemberGrievances":
                    {
                        return CreateMemberGrievances(param.Parameter);
                    }
                case "UpdateMemberGrievances":
                    {
                        return UpdateMemberGrievances(param.Parameter);
                    }
                case "DeleteMemberGrievances":
                    {
                        return DeleteMemberGrievances(param.Parameter);
                    }
                case "GetAllOfficeGrievances":
                    {
                        return GetAllOfficeGrievances(param.Parameter);
                    }
                case "GetAllMemberGrievances":
                    {
                        return GetAllMemberGrievances(param.Parameter);
                    }
                case "GetMemberGrievancesByGrvCode":
                    {
                        return GetMemberGrievancesByGrvCode(param.Parameter);
                    }
                case "GetMemberGrievancesWithInfo":
                    {
                        return GetMemberGrievancesWithInfo(param.Parameter);
                    }
                case "CreateForwardGrievance":
                    {
                        return CreateForwardGrievance(param.Parameter);
                    }
                case "UpdateForwardGrievance":
                    {
                        return UpdateForwardGrievance(param.Parameter);
                    }
                case "DeleteForwardGrievance":
                    {
                        return DeleteForwardGrievance(param.Parameter);
                    }



                case "GetAllForwardGrievance":
                    {
                        return GetAllForwardGrievance(param.Parameter);
                    }
                case "GeForwardGrievanceById":
                    {
                        return GeForwardGrievanceById(param.Parameter);
                    }
                case "CreateGrievanceReply":
                    {
                        return CreateGrievanceReply(param.Parameter);
                    }

                case "UpdateGrievanceReply":
                    {
                        return UpdateGrievanceReply(param.Parameter);
                    }
                case "DeleteGrievanceReply":
                    {
                        return DeleteGrievanceReply(param.Parameter);
                    }
                case "GetAllGrievanceReply":
                    {
                        return GetAllGrievanceReply(param.Parameter);
                    }
                case "GetGrievanceReplyById":
                    {
                        return GetGrievanceReplyById(param.Parameter);
                    }

                case "CreateUserInformation":
                    {
                        return CreateUserInformation(param.Parameter);
                    }

                case "UpdateUserInformation":
                    {
                        return UpdateUserInformation(param.Parameter);
                    }
                case "DeleteUserInformation":
                    {
                        return DeleteUserInformation(param.Parameter);
                    }
                case "GetAllUserInformation":
                    {
                        return GetAllUserInformation(param.Parameter);
                    }
                case "GetUserInformationById":
                    {
                        return GetUserInformationById(param.Parameter);
                    }
                case "GetAllFieldOfficer":
                    {
                        return GetAllFieldOfficer(param.Parameter);
                    }
                case "GetFieldOfficerByGrvCode":
                    {
                        return GetFieldOfficerByGrvCode(param.Parameter);
                    }
                case "GetFieldOfficerByMemberCode":
                    {
                        return GetFieldOfficerByMemberCode(param.Parameter);
                    }

                case "GetAllFieldOfficerByMemberCode":
                    {
                        return GetAllFieldOfficerByMemberCode(param.Parameter);
                    }
                case "AddGrievanceOfficerDeleted":
                    {
                        return AddGrievanceOfficerDeleted(param.Parameter);
                    }
                case "GetOfficerByAdhaarID":
                    {
                        return GetOfficerByAdhaarID(param.Parameter);
                    }
                case "DeleteGrievanceOfficer":
                    {
                        return DeleteGrievanceOfficer(param.Parameter);
                    }
                case "GetForwardGrievanceByGRVCode":
                    {
                        return GetForwardGrievanceByGRVCode(param.Parameter);
                    }
                case "GetUserByAadharId":
                    {
                        return GetUserByAadharId(param.Parameter);
                    }
                case "GetOfficeUser":
                    {
                        return GetOfficeUser(param.Parameter);


                    }
                case "GetMemberByOfficeUser":
                    {
                        return GetMemberByOfficeUser(param.Parameter);


                    }
                    
                    
            }
            return null;
        }

        public static object GetUserByAadharId(object param)
        {
            string AadarId = param as string;

            GrievanceContext db = new GrievanceContext();
            var data = (from U in db.mUsers
                        where U.AadarId == AadarId
                        select U).FirstOrDefault();

            return data;

        }
        #region Member Grievances

        public static object CreateMemberGrievances(object param)
        {
            try
            {
                tMemberGrievances model = param as tMemberGrievances;
                using (GrievanceContext db = new GrievanceContext())
                {


                    db.tMemberGrievances.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.GuID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateMemberGrievances(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    tMemberGrievances model = param as tMemberGrievances;
                    db.tMemberGrievances.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteMemberGrievances(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    tMemberGrievances parameter = param as tMemberGrievances;
                    tMemberGrievances stateToRemove = db.tMemberGrievances.SingleOrDefault(a => a.GuID == parameter.GuID);
                    db.tMemberGrievances.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllOfficeGrievances(object param)
        {

            try
            {
                List<tMemberGrievances> result = new List<tMemberGrievances>();
                ForwardGrievance obj = param as ForwardGrievance;
                if (obj.SubDivisionId == "" || obj.SubDivisionId == null)
                {
                    using (GrievanceContext db = new GrievanceContext())
                    {
                        var data = (from MG in db.tMemberGrievances
                                    join FG in db.ForwardGrievance on MG.GrvCode equals FG.GrvCode
                                    join UI in db.mUserInformation on MG.UserCode equals UI.UserCode
                                    where FG.OfficeId == obj.OfficeId && FG.DeptId == obj.DeptId && (MG.StatusId == obj.status)//|| MG.StatusId == 2 || MG.StatusId == 4)//FG.OfficerCode == AadharId
                                    orderby MG.GuID descending
                                    select new
                                     {
                                         GuID = MG.GuID,
                                         GrvCode = MG.GrvCode,
                                         MemberCode = MG.MemberCode,
                                         StatusModifiedDate = MG.StatusModifiedDate,
                                         InformationContent = MG.InformationContent,
                                         SubmittedDate = MG.SubmittedDate,
                                         UserCode = MG.UserCode,
                                         InfoTypeId = MG.InfoTypeId,
                                         StatusId = MG.StatusId,
                                         FirstName = UI.FirstName,
                                         LastName = UI.LastName,
                                         MobileNumber = UI.MobileNumber,
                                         Address = UI.Address,
                                         AadhaarId = UI.AadhaarId,
                                         Constituency = UI.Constituency,
                                         Panchayat = UI.Panchayat,
                                         District = UI.District,
                                         WardNumber = UI.WardNumber
                                     }).ToList();

                        foreach (var item in data)
                        {
                            tMemberGrievances MGObj = new tMemberGrievances();
                            MGObj.GuID = item.GuID;
                            MGObj.GrvCode = item.GrvCode;
                            MGObj.StatusModifiedDate = item.StatusModifiedDate;
                            MGObj.InformationContent = item.InformationContent;
                            MGObj.SubmittedDate = item.SubmittedDate;
                            MGObj.MemberCode = item.MemberCode;
                            MGObj.UserCode = item.UserCode;
                            MGObj.InfoTypeId = item.InfoTypeId;
                            MGObj.StatusId = item.StatusId;
                            MGObj.FirstName = item.FirstName;
                            MGObj.LastName = item.LastName;
                            MGObj.MobileNumber = item.MobileNumber;
                            MGObj.Address = item.Address;
                            MGObj.AadhaarId = item.AadhaarId;
                            MGObj.Constituency = item.Constituency;
                            MGObj.Panchayat = item.Panchayat;
                            MGObj.District = item.District;
                            MGObj.WardNumber = item.WardNumber;
                            result.Add(MGObj);
                        }


                        return result;
                    }
                }
                else
                {
                    using (GrievanceContext db = new GrievanceContext())
                    {
                        var data = (from MG in db.tMemberGrievances
                                    join FG in db.ForwardGrievance on MG.GrvCode equals FG.GrvCode
                                    join UI in db.mUserInformation on MG.UserCode equals UI.UserCode
                                  //  where FG.OfficeId == obj.OfficeId && FG.DeptId == obj.DeptId && (MG.StatusId == obj.status) && UI.SubDivisionId==obj.SubDivisionId  //|| MG.StatusId == 2 || MG.StatusId == 4)//FG.OfficerCode == AadharId

                                    where MG.StatusId == obj.status && UI.SubDivisionId == obj.SubDivisionId  //|| MG.StatusId == 2 || MG.StatusId == 4)//FG.OfficerCode == AadharId
                                    orderby MG.GuID descending
                                    select new
                                    {
                                        GuID = MG.GuID,
                                        GrvCode = MG.GrvCode,
                                        MemberCode = MG.MemberCode,
                                        StatusModifiedDate = MG.StatusModifiedDate,
                                        InformationContent = MG.InformationContent,
                                        SubmittedDate = MG.SubmittedDate,
                                        UserCode = MG.UserCode,
                                        InfoTypeId = MG.InfoTypeId,
                                        StatusId = MG.StatusId,
                                        FirstName = UI.FirstName,
                                        LastName = UI.LastName,
                                        MobileNumber = UI.MobileNumber,
                                        Address = UI.Address,
                                        AadhaarId = UI.AadhaarId,
                                        Constituency = UI.Constituency,
                                        Panchayat = UI.Panchayat,
                                        District = UI.District,
                                        WardNumber = UI.WardNumber
                                    }).ToList();

                        foreach (var item in data)
                        {
                            tMemberGrievances MGObj = new tMemberGrievances();
                            MGObj.GuID = item.GuID;
                            MGObj.GrvCode = item.GrvCode;
                            MGObj.StatusModifiedDate = item.StatusModifiedDate;
                            MGObj.InformationContent = item.InformationContent;
                            MGObj.SubmittedDate = item.SubmittedDate;
                            MGObj.MemberCode = item.MemberCode;
                            MGObj.UserCode = item.UserCode;
                            MGObj.InfoTypeId = item.InfoTypeId;
                            MGObj.StatusId = item.StatusId;
                            MGObj.FirstName = item.FirstName;
                            MGObj.LastName = item.LastName;
                            MGObj.MobileNumber = item.MobileNumber;
                            MGObj.Address = item.Address;
                            MGObj.AadhaarId = item.AadhaarId;
                            MGObj.Constituency = item.Constituency;
                            MGObj.Panchayat = item.Panchayat;
                            MGObj.District = item.District;
                            MGObj.WardNumber = item.WardNumber;
                            result.Add(MGObj);
                        }


                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetMemberGrievancesByGrvCode(object param)
        {
            tMemberGrievances parameter = param as tMemberGrievances;

            GrievanceContext db = new GrievanceContext();
            tMemberGrievances result = new tMemberGrievances();
            var data = (from MG in db.tMemberGrievances
                        join UI in db.mUserInformation on MG.UserCode equals UI.UserCode
                        join AD in db.mMembers on MG.MemberCode equals AD.MemberCode
                        where MG.GrvCode == parameter.GrvCode
                        select new
                        {

                            MG,
                            Prefix = AD.Prefix,
                            Name = AD.Name,
                            FirstName = UI.FirstName,
                            LastName = UI.LastName,
                            Mobile = UI.MobileNumber,
                            Address = UI.Address

                        }
                        ).FirstOrDefault();
            if (data != null)
            {
                result = data.MG;
                result.MemberName = data.Prefix + " " + data.Name;
                result.UserName = data.FirstName + " " + (data.LastName != null ? data.LastName : "");
                //result.MobileNumber = data.Mobile;
                result.Address = data.Address;
            }
            return result;

        }


        public static object GetMemberGrievancesWithInfo(object param)
        {
            tMemberGrievances parameter = param as tMemberGrievances;

            GrievanceContext db = new GrievanceContext();
            tMemberGrievances result = new tMemberGrievances();
            var data = (from MG in db.tMemberGrievances
                        join UI in db.mUserInformation on MG.UserCode equals UI.UserCode
                        join FG in db.ForwardGrievance on MG.GrvCode equals FG.GrvCode
                        join ME in db.mMembers on FG.MemberCode equals ME.MemberCode
                        where MG.GrvCode == parameter.GrvCode && FG.GrvCode == parameter.GrvCode && FG.OfficerCode == parameter.OfficerCode
                        select new
                        {
                            MG,
                            FirstName = UI.FirstName,
                            LastName = UI.LastName,
                            Prefix = ME.Prefix,
                            MemberName = ME.Name
                        }).OrderBy(a => a.MG.GuID).FirstOrDefault();

            if (data != null)
            {
                result = data.MG;
                result.UserName = data.FirstName + " " + (data.LastName != null ? data.LastName : "");
                result.MemberName = data.Prefix + ". " + data.MemberName;
            }
            return result;

        }

        #endregion


        #region Forward Grievances

        public static object CreateForwardGrievance(object param)
        {
            try
            {
                ForwardGrievance model = param as ForwardGrievance;
                using (GrievanceContext db = new GrievanceContext())
                {


                    db.ForwardGrievance.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.ForwardId;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateForwardGrievance(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    ForwardGrievance model = param as ForwardGrievance;
                    db.ForwardGrievance.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteForwardGrievance(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    ForwardGrievance parameter = param as ForwardGrievance;
                    ForwardGrievance stateToRemove = db.ForwardGrievance.SingleOrDefault(a => a.ForwardId == parameter.ForwardId);
                    db.ForwardGrievance.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllForwardGrievance(object param)
        {

            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    var data = (from FG in db.ForwardGrievance
                                orderby FG.ForwardId descending
                                select FG).ToList();


                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GeForwardGrievanceById(object param)
        {
            ForwardGrievance parameter = param as ForwardGrievance;

            GrievanceContext db = new GrievanceContext();
            var data = (from FG in db.ForwardGrievance
                        where FG.ForwardId == parameter.ForwardId
                        select FG).OrderByDescending(a => a.ForwardId).FirstOrDefault();

            return data;

        }

        #endregion


        #region   Grievance Reply

        public static object CreateGrievanceReply(object param)
        {
            try
            {
                tGrievanceReply model = param as tGrievanceReply;
                using (GrievanceContext db = new GrievanceContext())
                {


                    db.tGrievanceReply.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.GrievanceReplyID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateGrievanceReply(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    tGrievanceReply model = param as tGrievanceReply;
                    db.tGrievanceReply.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteGrievanceReply(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    tGrievanceReply parameter = param as tGrievanceReply;
                    tGrievanceReply stateToRemove = db.tGrievanceReply.SingleOrDefault(a => a.GrievanceReplyID == parameter.GrievanceReplyID);
                    db.tGrievanceReply.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllGrievanceReply(object param)
        {

            try
            {
                string GrvCode = param as string;
                using (GrievanceContext db = new GrievanceContext())
                {
                    var data = (from GR in db.tGrievanceReply
                                join AD in db.AdhaarDetails on GR.OfficerCode equals AD.AdhaarID
                                join MU in db.mUsers on GR.OfficerCode equals MU.AadarId
                                where GR.GrvCode == GrvCode
                                orderby GR.GrievanceReplyID descending
                                select new { GR, Name = AD.Name, Mobile = MU.MobileNo }).ToList();

                    List<tGrievanceReply> result = new List<tGrievanceReply>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            tGrievanceReply Reply = new tGrievanceReply();
                            Reply = item.GR;
                            Reply.OfficerName = item.Name;
                            Reply.MobileNumber = item.Mobile;
                            result.Add(Reply);

                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetGrievanceReplyById(object param)
        {
            tGrievanceReply parameter = param as tGrievanceReply;

            GrievanceContext db = new GrievanceContext();
            var data = (from GR in db.tGrievanceReply
                        where GR.GrievanceReplyID == parameter.GrievanceReplyID
                        select GR).OrderByDescending(a => a.GrievanceReplyID).FirstOrDefault();

            return data;

        }

        #endregion

        #region   User Information

        public static object CreateUserInformation(object param)
        {
            try
            {
                mUserInformation model = param as mUserInformation;
                using (GrievanceContext db = new GrievanceContext())
                {


                    db.mUserInformation.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.UserId;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateUserInformation(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    mUserInformation model = param as mUserInformation;
                    db.mUserInformation.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteUserInformation(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    mUserInformation parameter = param as mUserInformation;
                    mUserInformation stateToRemove = db.mUserInformation.SingleOrDefault(a => a.UserId == parameter.UserId);
                    db.mUserInformation.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllUserInformation(object param)
        {

            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    var data = (from UI in db.mUserInformation
                                orderby UI.UserId descending
                                select UI).ToList();


                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetUserInformationById(object param)
        {
            mUserInformation parameter = param as mUserInformation;

            GrievanceContext db = new GrievanceContext();
            var data = (from UI in db.mUserInformation
                        where UI.UserId == parameter.UserId
                        select UI).OrderByDescending(a => a.UserId).FirstOrDefault();

            return data;

        }

        #endregion

        #region Assign Grievances

        public static object GetAllMemberGrievances(object param)
        {

            try
            {
                int MemberCode = Convert.ToInt16(param);
                List<tMemberGrievances> result = new List<tMemberGrievances>();
                using (GrievanceContext db = new GrievanceContext())
                {
                    var data = (from MG in db.tMemberGrievances
                                join UI in db.mUserInformation on MG.UserCode equals UI.UserCode
                                where MG.MemberCode == MemberCode // && (MG.StatusId == 2 || MG.StatusId == 3)
                                orderby MG.GuID descending
                                select new
                                {
                                    GuID = MG.GuID,
                                    GrvCode = MG.GrvCode,
                                    MemberCode = MG.MemberCode,
                                    SubmittedDate = MG.SubmittedDate,
                                    StatusModifiedDate = MG.StatusModifiedDate,
                                    InformationContent = MG.InformationContent,
                                    UserCode = MG.UserCode,
                                    InfoTypeId = MG.InfoTypeId,
                                    StatusId = MG.StatusId,
                                    FirstName = UI.FirstName,
                                    LastName = UI.LastName,
                                    MobileNumber = UI.MobileNumber,
                                    Address = UI.Address,
                                    AadhaarId = UI.AadhaarId,
                                    Constituency = UI.Constituency,
                                    Panchayat = UI.Panchayat,
                                    District = UI.District,
                                    WardNumber = UI.WardNumber
                                }).ToList();
                    foreach (var item in data)
                    {
                        tMemberGrievances obj = new tMemberGrievances();
                        obj.GuID = item.GuID;
                        obj.GrvCode = item.GrvCode;
                        obj.InformationContent = item.InformationContent;
                        obj.MemberCode = item.MemberCode;
                        obj.SubmittedDate = item.SubmittedDate;
                        obj.StatusModifiedDate = item.StatusModifiedDate;
                        obj.UserCode = item.UserCode;
                        obj.InfoTypeId = item.InfoTypeId;
                        obj.StatusId = item.StatusId;
                        obj.FirstName = item.FirstName;
                        obj.LastName = item.LastName;
                        obj.MobileNumber = item.MobileNumber;
                        obj.Address = item.Address;
                        obj.AadhaarId = item.AadhaarId;
                        obj.Constituency = item.Constituency;
                        obj.Panchayat = item.Panchayat;
                        obj.District = item.District;
                        obj.WardNumber = item.WardNumber;
                        result.Add(obj);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetForwardGrievanceByGRVCode(object param)
        {

            try
            {
                ForwardGrievance Parameter = param as ForwardGrievance;
                using (GrievanceContext db = new GrievanceContext())
                {
                    var data = (from FG in db.ForwardGrievance
                                where FG.MemberCode == Parameter.MemberCode && FG.GrvCode == Parameter.GrvCode
                                select FG).FirstOrDefault();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetAllFieldOfficer(object param)
        {

            try
            {
                ForwardGrievance Parameter = param as ForwardGrievance;
                List<ForwardGrievance> OfficerList = new List<ForwardGrievance>();
                using (GrievanceContext db = new GrievanceContext())
                {
                    var myst = (from OD in db.tGrievanceOfficerDeleted
                                where OD.MemberCode == Parameter.MemberCode
                                select new
                                {
                                    AadharId = OD.AdharId
                                }).ToList();
                    List<string> StringList = new List<string>();
                    if (myst != null && myst.Count() > 0)
                    {
                        foreach (var item in myst)
                        {
                            StringList.Add(item.AadharId);
                        }
                    }
                    string[] array = StringList.ToArray();

                    var data = (from U in db.mUsers
                                join AD in db.AdhaarDetails on U.AadarId equals AD.AdhaarID
                                join DO in db.tDepartmentOfficeMapped on U.OfficeId equals DO.OfficeId
                                where DO.MemberCode == Parameter.MemberCode && array.Contains(U.AadarId)

                                select new
                                {
                                    UserId = U.UserId,
                                    Name = AD.Name,
                                    DeptId = U.DeptId,
                                    AadarId = U.AadarId,
                                    Designation = U.Designation,
                                    Mobile = U.MobileNo,
                                    email = U.EmailId
                                }).Distinct().ToList();

                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            ForwardGrievance obj = new ForwardGrievance();
                            obj.AadarId = item.AadarId;
                            obj.Name = item.Name;
                            obj.DeptId = item.DeptId;
                            obj.UserId = item.UserId;
                            obj.Designation = item.Designation;
                            obj.Mobile = item.Mobile;
                            obj.Email = item.email;

                            var ForwardGrievance = (from FG in db.ForwardGrievance
                                                    where FG.MemberCode == Parameter.MemberCode && FG.GrvCode == Parameter.GrvCode && FG.OfficerCode == obj.AadarId
                                                    select FG).FirstOrDefault();
                            if (ForwardGrievance != null)
                            {
                                obj.ForwardId = ForwardGrievance.ForwardId;
                                obj.OfficerCode = ForwardGrievance.OfficerCode;
                            }
                            OfficerList.Add(obj);
                        }
                    }
                    return OfficerList.OrderBy(m => m.Name).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllFieldOfficerByMemberCode(object param)
        {

            try
            {
                ForwardGrievance Parameter = param as ForwardGrievance;
                List<ForwardGrievance> OfficerList = new List<ForwardGrievance>();
                using (GrievanceContext db = new GrievanceContext())
                {

                    //var myst = (from OD in db.tGrievanceOfficerDeleted
                    //            where OD.MemberCode == Parameter.MemberCode
                    //            select new
                    //            {
                    //                AadharId = OD.AdharId
                    //            }).ToList();
                    //List<string> StringList = new List<string>();
                    //if (myst != null && myst.Count() > 0)
                    //{
                    //    foreach (var item in myst)
                    //    {
                    //        StringList.Add(item.AadharId);
                    //    }
                    //}
                    //string[] array = StringList.ToArray();

                    var data = (from U in db.mUsers
                                join AD in db.AdhaarDetails on U.AadarId equals AD.AdhaarID
                                join DO in db.tDepartmentOfficeMapped on U.OfficeId equals DO.OfficeId
                                join DPT in db.mDepartment on U.DeptId equals DPT.deptId into DPTS
                                from DPTSS in DPTS.DefaultIfEmpty()
                                join OFC in db.mOffice on U.OfficeId equals OFC.OfficeId into OFCS
                                from OFCSS in OFCS.DefaultIfEmpty()
                                where DO.MemberCode == Parameter.MemberCode //&& !array.Contains(U.AadarId)  // && (MG.StatusId == 2 || MG.StatusId == 3)

                                select new
                                {
                                    UserId = U.UserId,
                                    Name = AD.Name,
                                    DeptId = U.DeptId,
                                    DeptName = DPTSS.deptname,
                                    AadarId = U.AadarId,
                                    Designation = U.Designation,
                                    Mobile = U.MobileNo,
                                    email = U.EmailId,
                                    OfficeName = OFCSS.officename,
                                    OfficeId = OFCSS.OfficeId,
                                }).Distinct().ToList();

                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            ForwardGrievance obj = new ForwardGrievance();
                            obj.AadarId = item.AadarId;
                            obj.Name = item.Name;
                            obj.DeptId = item.DeptId;
                            obj.DeptName = item.DeptName;
                            obj.UserId = item.UserId;
                            obj.Designation = item.Designation;
                            obj.Mobile = item.Mobile;
                            obj.Email = item.email;
                            obj.Officename = item.OfficeName;
                            obj.OfficeId = item.OfficeId;
                            var GrievanceOfficerDeleted = (from GO in db.tGrievanceOfficerDeleted
                                                           where GO.MemberCode == Parameter.MemberCode && GO.AdharId == obj.AadarId
                                                           select GO).FirstOrDefault();
                            if (GrievanceOfficerDeleted != null)
                            {
                                obj.ForwardId = GrievanceOfficerDeleted.Id;
                                obj.OfficerCode = GrievanceOfficerDeleted.AdharId;
                            }
                            OfficerList.Add(obj);
                        }
                    }
                    return OfficerList.OrderBy(m => m.Name).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetFieldOfficerByMemberCode(object param)
        {

            try
            {
                ForwardGrievance Parameter = param as ForwardGrievance;
                List<ForwardGrievance> OfficerList = new List<ForwardGrievance>();
                using (GrievanceContext db = new GrievanceContext())
                {

                    //                    Select AD.Name,O.officename,FG.ForwardId,FG.GrvCode,FG.MemberCode,FG.OfficerCode,FG.ForwardDate,FG.Type from mUsers U
                    //inner join AdhaarDetails AD on U.AadarId= AD.AdhaarID
                    //inner join ForwardGrievance FG on U.AadarId= FG.OfficerCode
                    //inner join mOffice O on U.OfficeId =O.OfficeId
                    //where FG.MemberCode='19' and FG.GrvCode='2015/133'

                    var data = (from FG in db.ForwardGrievance
                                join DPT in db.mDepartment on FG.DeptId equals DPT.deptId into DPTS
                                from DPTSS in DPTS.DefaultIfEmpty()
                                join O in db.mOffice on FG.OfficeId equals O.OfficeId
                                where FG.MemberCode == Parameter.MemberCode && FG.GrvCode == Parameter.GrvCode
                                orderby FG.ForwardId descending
                                select new
                                {
                                    FG,
                                    Officename = O.officename,
                                    Department = DPTSS.deptname
                                }).ToList();

                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            ForwardGrievance obj = new ForwardGrievance();
                            obj = item.FG;
                            obj.Officename = item.Officename;
                            obj.DeptName = item.Department;
                            OfficerList.Add(obj);
                        }
                    }

                    return OfficerList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetFieldOfficerByGrvCode(object param)
        {

            try
            {
                ForwardGrievance Parameter = param as ForwardGrievance;
                List<ForwardGrievance> OfficerList = new List<ForwardGrievance>();
                using (GrievanceContext db = new GrievanceContext())
                {

                    //                    Select AD.Name,O.officename,FG.ForwardId,FG.GrvCode,FG.MemberCode,FG.OfficerCode,FG.ForwardDate,FG.Type from mUsers U
                    //inner join AdhaarDetails AD on U.AadarId= AD.AdhaarID
                    //inner join ForwardGrievance FG on U.AadarId= FG.OfficerCode
                    //inner join mOffice O on U.OfficeId =O.OfficeId
                    //where FG.MemberCode='19' and FG.GrvCode='2015/133'

                    var data = (from FG in db.ForwardGrievance
                                join DPT in db.mDepartment on FG.DeptId equals DPT.deptId into DPTS
                                from DPTSS in DPTS.DefaultIfEmpty()
                                join O in db.mOffice on FG.OfficeId equals O.OfficeId
                                where FG.GrvCode == Parameter.GrvCode
                                orderby FG.ForwardId descending
                                select new
                                {
                                    FG,
                                    Officename = O.officename,
                                    Department = DPTSS.deptname
                                }).ToList();

                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            ForwardGrievance obj = new ForwardGrievance();
                            obj = item.FG;
                            obj.Officename = item.Officename;
                            obj.DeptName = item.Department;
                            OfficerList.Add(obj);
                        }
                    }

                    return OfficerList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static object GetOfficerByAdhaarID(object param)
        {
            AdhaarDetails parameter = param as AdhaarDetails;

            GrievanceContext db = new GrievanceContext();
            var data = (from FG in db.AdhaarDetails
                        where FG.AdhaarID == parameter.AdhaarID
                        select FG).OrderByDescending(a => a.AdhaarID).FirstOrDefault();

            return data;

        }

        #endregion

        #region GrievanceOfficerDeleted
        public static object AddGrievanceOfficerDeleted(object param)
        {
            try
            {
                tGrievanceOfficerDeleted model = param as tGrievanceOfficerDeleted;
                using (GrievanceContext db = new GrievanceContext())
                {
                    db.tGrievanceOfficerDeleted.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.Id;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
        public static object DeleteGrievanceOfficer(object param)
        {
            try
            {
                using (GrievanceContext db = new GrievanceContext())
                {
                    tGrievanceOfficerDeleted parameter = param as tGrievanceOfficerDeleted;
                    tGrievanceOfficerDeleted stateToRemove = db.tGrievanceOfficerDeleted.SingleOrDefault(a => a.MemberCode == parameter.MemberCode && a.AdharId == parameter.AdharId);
                    db.tGrievanceOfficerDeleted.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }


        private static object GetOfficeUser(object param)
        {
            using (GrievanceContext context = new GrievanceContext())
            {
                string AId = param.ToString();
                var obj = (from Officer in context.tGrievanceOfficerDeleted
                           where Officer.AdharId == AId
                           select Officer.OfficeId).FirstOrDefault();
                return obj;
            }
        }
        private static object GetMemberByOfficeUser(object param)
        {
            string[] str = new string[2];  

            using (GrievanceContext context = new GrievanceContext())
            {
                string AId = param.ToString();
                var obj = (from Officer in context.tGrievanceOfficerDeleted
                           where Officer.AdharId == AId
                           select Officer.MemberCode).FirstOrDefault();                
                return obj;

            }
        }

        #endregion
    }
}
