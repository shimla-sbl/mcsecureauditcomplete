﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Grievance
{
    [Table("tMemberGrievances")]
    [Serializable]
    public class tMemberGrievances
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 GuID { get; set; }

        public string GrvCode { get; set; }

        public int MemberCode { get; set; }

        public string UserCode { get; set; }

        public string Latitude { get; set; }

        public string Longitude { get; set; }

        public int InfoTypeId { get; set; }

        public int StatusId { get; set; }

        public DateTime SubmittedDate { get; set; }

        public DateTime StatusModifiedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public string ImageName { get; set; }

        public string PdfName { get; set; }

        public string InformationContent { get; set; }

        // public string IsForwarded { get; set; }
        public string Location { get; set; }

        public string DisposedBy { get; set; }

        public string DiposedUserId { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public string UserName { get; set; }

        [NotMapped]
        public string MemberName { get; set; }

        [NotMapped]
        public string OfficerCode { get; set; }

        [NotMapped]
        public string Address { get; set; }

        [NotMapped]
        public List<tMemberGrievances> MemberGrievance { get; set; }
        [NotMapped]
        public string FirstName { get; set; }
        [NotMapped]
        public string LastName { get; set; }
        [NotMapped]
        public string MobileNumber { get; set; }
        [NotMapped]
        public string AadhaarId { get; set; }
        [NotMapped]
        public string Constituency { get; set; }
        [NotMapped]
        public string Panchayat { get; set; }
        [NotMapped]
        public string District { get; set; }
        [NotMapped]
        public string WardNumber { get; set; }

        public bool IsDeleted { get; set; }
        
    }
}
