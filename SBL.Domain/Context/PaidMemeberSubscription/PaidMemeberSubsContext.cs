﻿using SBL.DAL;
using SBL.DomainModel.Models.PaidMemeberSubscription;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.PaidMemeberSubscription
{
   public class PaidMemeberSubsContext: DBBase<PaidMemeberSubsContext>
    {
        public PaidMemeberSubsContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<PaidMemeberSubs> PaidMemeberSubs { get; set; }


        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreatePaidMemeberSubs":
                    {
                        return CreatePaidMemeberSubs(param.Parameter);
                    }
                case "UpdatePaidMemeberSubs":
                    {
                        return UpdatePaidMemeberSubs(param.Parameter);
                    }
                case "DeletePaidMemeberSubs":
                    {
                        return DeletePaidMemeberSubs(param.Parameter);
                    }
                case "GetAllPaidMemeberSubsDetails":
                    {
                        return GetAllPaidMemeberSubsDetails(param.Parameter);
                    }
                case "GetPaidMemeberSubsById":
                    {
                        return GetPaidMemeberSubsById(param.Parameter);
                    }
                case "SearchPaidMemeberSubs":
                    {
                        return SearchPaidMemeberSubs(param.Parameter);
                    }
            }

            return null;
        }

        public static object CreatePaidMemeberSubs(object param)
        {
            try
            {
                using (PaidMemeberSubsContext db = new PaidMemeberSubsContext())
                {
                    PaidMemeberSubs model = param as PaidMemeberSubs;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.PaidMemeberSubs.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdatePaidMemeberSubs(object param)
        {
            try
            {
                using (PaidMemeberSubsContext db = new PaidMemeberSubsContext())
                {
                    PaidMemeberSubs model = param as PaidMemeberSubs;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.PaidMemeberSubs.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeletePaidMemeberSubs(object param)
        {
            try
            {
                using (PaidMemeberSubsContext db = new PaidMemeberSubsContext())
                {
                    PaidMemeberSubs parameter = param as PaidMemeberSubs;
                    PaidMemeberSubs stateToRemove = db.PaidMemeberSubs.SingleOrDefault(a => a.PaidMemeberSubsID == parameter.PaidMemeberSubsID);
                    db.PaidMemeberSubs.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllPaidMemeberSubsDetails(object param)
        {

            PaidMemeberSubsContext db = new PaidMemeberSubsContext();

            var data = (from PC in db.PaidMemeberSubs
                        orderby PC.NameOfSubscriber
                        select PC).ToList();


            return data;
        }


        public static object SearchPaidMemeberSubs(object param)
        {
            
            string Data = param as string;
            string[] Result = Data.Split(',');
            string SubscriberName = Result[0];
            string Designation = Result[1];
            string StartDateFrom = Result[2];
            string StartDateTo = Result[3];
            string EndDateFrom = Result[4];
            string EndDateTo = Result[5];
            DateTime SFromDate = new DateTime();
            DateTime SToDate = new DateTime();

            DateTime EFromDate = new DateTime();
            DateTime EToDate = new DateTime();


            DateTime.TryParse(StartDateFrom, out SFromDate);
            DateTime.TryParse(StartDateTo, out SToDate);

            DateTime.TryParse(EndDateFrom, out EFromDate);
            DateTime.TryParse(EndDateTo, out EToDate);

            PaidMemeberSubsContext db = new PaidMemeberSubsContext();

            var data = (from PM in db.PaidMemeberSubs
                        orderby PM.NameOfSubscriber
                        where PM.NameOfSubscriber.Contains(SubscriberName.Trim()) || PM.Designation.Contains(Designation.Trim())
                        || (PM.SubsStartDate >= SFromDate && PM.SubsStartDate <= SToDate)
                        || (PM.SubsEndDate >= EFromDate && PM.SubsEndDate <= EToDate)
                        select PM).ToList();


            return data;
        }


        public static object GetPaidMemeberSubsById(object param)
        {
            PaidMemeberSubs parameter = param as PaidMemeberSubs;

            PaidMemeberSubsContext db = new PaidMemeberSubsContext();

            var query = db.PaidMemeberSubs.SingleOrDefault(a => a.PaidMemeberSubsID == parameter.PaidMemeberSubsID);

            return query;
        }
    }
}
