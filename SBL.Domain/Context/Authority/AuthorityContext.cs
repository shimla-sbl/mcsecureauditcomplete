﻿using SBL.DAL;
using SBL.DomainModel.Models.Authority;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Secretory;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Authority
{
    public class AuthorityContext : DBBase<AuthorityContext>
    {

        public AuthorityContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mAuthority> mAuthority { get; set; }
        public virtual DbSet<SiteSettings> SiteSettings { get; set; }
        public DbSet<mSecretory> mSecretory { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateAuthority": { return CreateAuthority(param.Parameter); break; }

                case "UpdateAuthority": { return UpdateAuthority(param.Parameter); }
                case "DeleteAuthority": { return DeleteAuthority(param.Parameter); }
                case "GetAllAuthority": { return GetAllAuthority(); }
                case "GetAuthorityById": { return GetAuthorityById(param.Parameter); }
                case "GetAllSecretory": { return GetAllSecretory(); }
            }
            return null;
        }


        static object CreateAuthority(object param)
        {
            try
            {
                using (AuthorityContext db = new AuthorityContext())
                {
                    mAuthority model = param as mAuthority;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mAuthority.Add(model);
                    db.SaveChanges();
                    db.Close();
                    return model.AuthorityId;
                }

            }
            catch
            {
                throw;
            }
        }



        static List<mSecretory> GetAllSecretory()
        {
            try
            {
                using (AuthorityContext db = new AuthorityContext())
                {
                    var data = (from a in db.mSecretory
                                orderby a.SecretoryName
                                where a.IsDeleted == null
                                select a).ToList();
                    return data;

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        static object UpdateAuthority(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (AuthorityContext db = new AuthorityContext())
            {
                mAuthority model = param as mAuthority;
                model.CreationDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mAuthority.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllAuthority();
        }

        static object DeleteAuthority(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (AuthorityContext db = new AuthorityContext())
            {
                mAuthority parameter = param as mAuthority;
                mAuthority stateToRemove = db.mAuthority.SingleOrDefault(a => a.AuthorityId == parameter.AuthorityId);
                if(stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
               // db.mAuthority.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllAuthority();
        }

        static List<mAuthority> GetAllAuthority()
        {
            AuthorityContext db = new AuthorityContext();

            //var query = db.mAuthority.ToList();
            var query = (from a in db.mAuthority
                         //orderby a.AuthorityId descending
                         where a.IsDeleted==null
                         select a).ToList();
            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            //return query;
            List<mAuthority> list = new List<mAuthority>();
            foreach (var item in query)
            {
                
                item.ImageAcessurl = siteSettingData.SettingValue + item.FilePath + "/Thumb/" + item.FileName;


                list.Add(item);

            }
            return list.OrderByDescending(c => c.AuthorityId).ToList();



            
        }

        static mAuthority GetAuthorityById(object param)
        {
            mAuthority parameter = param as mAuthority;
            AuthorityContext db = new AuthorityContext();
            var query = db.mAuthority.SingleOrDefault(a => a.AuthorityId == parameter.AuthorityId);
            return query;
        }


    }
}
