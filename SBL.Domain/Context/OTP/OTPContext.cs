﻿using SBL.DAL;
using SBL.DomainModel.Models.OTP;
using SBL.Service.Common;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.OTP
{
   public class OTPContext : DBBase<OTPContext>
    {  public OTPContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<tOTPDetails> tOTPDetails { get; set; }

       public static object Execute(ServiceParameter param)
       {
           if (null == param)
            {
                return null;
            }
           switch (param.Method)
           {
               case "AddOTP":
                   {
                       return AddOTP(param.Parameter);
                   }
               case "GetOTPByID":
                   {
                       return GetOTPByID(param.Parameter);
                   }
           }

           return null;
       }
       static object AddOTP(object param)
       {
           //  BillsContext obj = new BillsContext();
           //  tOTPDetailsVS model = param as tOTPDetailsVS;
           OTPContext otpCtxt = new OTPContext();
           tOTPDetails model = param as tOTPDetails;
           var vals = 0;
           var id = 0;
           //var query = CheckExistingOTP(model);
           //if (query != null)
           //{
           //    return query;
           //}
           using (var obj = new OTPContext())
           {

               model.CreationDate = System.DateTime.Now;
               model.IsActive = true;
               obj.tOTPDetails.Add(model);
               vals = obj.SaveChanges();
               obj.Close();
               id = model.OTPid;

               //obj.tBillRegisterVs.Add(model);

           }
           model.OTPid = id;
           return model;
       }
       static object CheckExistingOTP(object param)
       {
           OTPContext otpCtxt = new OTPContext();
           tOTPDetails model = param as tOTPDetails;
           var query = (from mdl in otpCtxt.tOTPDetails
                        where mdl.MobileNo == model.MobileNo && mdl.UserName == model.UserName
                        select mdl).FirstOrDefault();
           return query;
       }
       static object GetOTPByID(object param)
       {
           OTPContext otpCtxt = new OTPContext();
           tOTPDetails model = param as tOTPDetails;
           var query = (from mdl in otpCtxt.tOTPDetails
                        where mdl.OTP == model.OTP && mdl.MobileNo == model.MobileNo && mdl.UserName == model.UserName
                        select mdl).SingleOrDefault();
           return query;
       }
    }
}
