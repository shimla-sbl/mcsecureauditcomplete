﻿using SBL.Service.Common;
using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SBL.Domain.Context.Department;
using SBL.Domain.Context.Session;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Notice;
using System.Text.RegularExpressions;
using SBL.DomainModel.ComplexModel;
using System.Data;
using SBL.DomainModel.Models.Enums;

namespace SBL.Domain.Context.OtherPaperLaid
{
	public class OtherPaperLaidContext : DBBase<OtherPaperLaidContext>
	{
		public OtherPaperLaidContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

		public DbSet<mMember> mMember { get; set; }
		public DbSet<mAssembly> mAssembly { get; set; }
		public DbSet<mDepartment> mDepartment { get; set; }
		public DbSet<mSession> mSession { get; set; }

		//Pending List
		public DbSet<mPaperCategoryType> mPaperCategoryType { get; set; }
		public DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }
		public DbSet<mEvent> mEvent { get; set; }
		public DbSet<tPaperLaidV> tPaperLaidV { get; set; }

		//PaperLaidEntry
		public DbSet<mDocumentType> mDocumentTypes { get; set; }
		public DbSet<mSessionDate> mSessionDates { get; set; }
		public DbSet<mBillType> mBillTypes { get; set; }

		public DbSet<tQuestion> tQuestions { get; set; }
		public DbSet<tQuestionType> tQuestionTypes { get; set; }
		public DbSet<mNoticeType> mNoticeTypes { get; set; }
		public DbSet<tMemberNotice> tMemberNotice { get; set; }
		public DbSet<mMinistry> mMinistry { get; set; }
		public DbSet<mMinistryDepartment> mMInistryDepartment { get; set; }
		public DbSet<tBillRegister> tBillRegister { get; set; }

		public static object Execute(ServiceParameter param)
		{

			if (null == param)
			{
				return null;
			}
			switch (param.Method)
			{
				case "GetMinisterMinistryName":
					{
						return GetMinisterMinistryName(param.Parameter);
					}
				case "GetPaperLaidDepartmentDetails":
					{
						return GetPaperLaidDepartmentDetails(param.Parameter);
					}
				case "GetAllPaperCategoryType":
					{
						return GetAllPaperCategoryType(param.Parameter);
					}
				case "GetPendingDepartmentPaperLaidList":
					{
						return GetPendingDepartmentPaperLaidList(param.Parameter);
					}
				case "GetPaperLaidByEventId":
					{
						return GetPaperLaidByEventId(param.Parameter);
					}
				case "GetPaperLaidTempById":
					{
						return GetPaperLaidTempById(param.Parameter);
					}
				case "UpdateLOBRecordIdIntPaperLaidVS":
					{
						return UpdateLOBRecordIdIntPaperLaidVS(param.Parameter);
					}
				//                    updated by nitin
				case "GetPaperLaidCategoryDetails":
					{
						return GetPaperLaidCategoryDetails(param.Parameter);
					}
				case "GetDocumentType":
					{
						return GetDocumentType(param.Parameter);
					}
				case "GetSessionDates":
					{
						return GetSessionDates(param.Parameter);
					}
				case "GetQuestionNumber":
					{
						return GetQuestionNumber(param.Parameter);
					}
				case "GetNotices":
					{
						return GetNotices(param.Parameter);
					}
				case "SubmittPaperLaidEntry":
					{
						return SubmittPaperLaidEntry(param.Parameter);
					}
				case "GetFileSaveDetails":
					{
						return GetFileSaveDetails(param.Parameter);
					}
				case "SaveDetailsInTempPaperLaid":
					{
						return SaveDetailsInTempPaperLaid(param.Parameter);
					}
				case "UpdateDeptActiveId":
					{
						return UpdateDeptActiveId(param.Parameter);

					}
				case "GetQuestionDetails":
					{
						return GetQuestionDetails(param.Parameter);
					}
				case "UpdateQuestionPaperLaidId":
					{
						return UpdateQuestionPaperLaidId(param.Parameter);
					}
				case "GetNoticeDetails":
					{
						return GetNoticeDetails(param.Parameter);
					}
				case "CheckBillNUmber":
					{
						return CheckBillNUmber(param.Parameter);
					}
				case "UpdateNoticePaperLaidId":
					{
						return UpdateNoticePaperLaidId(param.Parameter);
					}
				case "InsertBillDetails":
					{
						return InsertBillDetails(param.Parameter);
					}
				case "SubmitPendingDepartmentPaperLaid":
					{
						return SubmitPendingDepartmentPaperLaid(param.Parameter);
					}
				case "EditPendingPaperLaidDetials":
					{
						return EditPendingPaperLaidDetials(param.Parameter);
					}
				case "UpdatePaperLaidEntry":
					{
						return UpdatePaperLaidEntry(param.Parameter);
					}

				case "GetExisitngFileDetails":
					{
						return GetExisitngFileDetails(param.Parameter);
					}

				case "UpdateDetailsInTempPaperLaid":
					{
						return UpdateDetailsInTempPaperLaid(param.Parameter);
					}
				case "GetSubmittedPaperLaidList":
					{
						return GetSubmittedPaperLaidList(param.Parameter);
					}
				case "DeletePaperLaidByID":
					{
						return DeletePaperLaidByID(param.Parameter);
					}
				case "UpdatePaperLaidFileByID":
					{
						return UpdatePaperLaidFileByID(param.Parameter);
					}
				case "ShowPaperLaidDetailByID":
					{
						return ShowPaperLaidDetailByID(param.Parameter);
					}
				case "GetSubmittedPaperLaidByID":
					{
						return GetSubmittedPaperLaidByID(param.Parameter);
					}
				case "GetFileVersion":
					{
						return GetFileVersion(param.Parameter);
					}
				case "GetPaperLaidById":
					{
						return GetPaperLaidById(param.Parameter);
					}
				case "GetDepartmentNameMinistryDetailsByMinisterID":
					{
						return GetDepartmentNameMinistryDetailsByMinisterID(param.Parameter);
					}
				case "GetPendingQuestionsByType":
					{
						return GetPendingQuestionsByType(param.Parameter);
					}
				case "GetSubmittedQuestionsByType":
					{
						return GetSubmittedQuestionsByType(param.Parameter);
					}
				case "GetLaidInHouseQuestionsByType":
					{
						return GetLaidInHouseQuestionsByType(param.Parameter);
					}
				case "GetPendingForSubQuestionsByType":
					{
						return GetPendingForSubQuestionsByType(param.Parameter);
					}
				case "GetPendingToLayQuestionsByType":
					{
						return GetPendingToLayQuestionsByType(param.Parameter);
					}
				case "GetCountForQuestionTypes":
					{
						return GetCountForQuestionTypes(param.Parameter);
					}
				case "GetQuetionEventID":
					{
						return GetQuetionEventID(param.Parameter);
					}
				case "UpdateMinisterActivePaperId":
					{
						return UpdateMinisterActivePaperId(param.Parameter);
					}
				case "GetQuestionByPaperLaidID":
					{
						return GetQuestionByPaperLaidID(param.Parameter);
					}
				case "GetPaperLaidByPaperLaidId":
					{
						return GetPaperLaidByPaperLaidId(param.Parameter);
					}
				#region Other PaperLaid

				case "GetOtherPaperLaidPendingsDetails":
					{
						return GetOtherPaperLaidPendingsDetails(param.Parameter);
					}
				case "GetCountForOtherPaperLaidTypes":
					{
						return GetCountForOtherPaperLaidTypes(param.Parameter);
					}
				case "GetOtherPaperLaidPaperCategoryType":
					{
						return GetOtherPaperLaidPaperCategoryType(param.Parameter);
					}
				case "UpdateOtherPaperLaidEntry":
					{
						return UpdateOtherPaperLaidEntry(param.Parameter);
					}
				case "GetExisitngFileDetailsForOtherPaperLaid":
					{
						return GetExisitngFileDetailsForOtherPaperLaid(param.Parameter);
					}
				case "UpdateDetailsInTempOtherPaperLaid":
					{
						return UpdateDetailsInTempOtherPaperLaid(param.Parameter);
					}
				case "SubmittOtherPaperLaidEntry":
					{
						return SubmittOtherPaperLaidEntry(param.Parameter);
					}
				case "GetFileSaveDetailsForOtherPaperLaid":
					{
						return GetFileSaveDetailsForOtherPaperLaid(param.Parameter);
					}
				case "SaveDetailsInTempOtherPaperLaid":
					{
						return SaveDetailsInTempOtherPaperLaid(param.Parameter);
					}
				case "SubmitPendingDepartmentOtherPaperLaid":
					{
						return SubmitPendingDepartmentOtherPaperLaid(param.Parameter);
					}
				case "EditPendingOtherPaperLaidDetials":
					{
						return EditPendingOtherPaperLaidDetials(param.Parameter);
					}
				case "UpdateOthePaperLaidDeptActiveId":
					{
						return UpdateOthePaperLaidDeptActiveId(param.Parameter);
					}
				case "GetAllEventsByOtherPaperLaid":
					{
						return GetAllEventsByOtherPaperLaid(param.Parameter);
					}
				case "GetSubmittedOtherPaperLaidList":
					{
						return GetSubmittedOtherPaperLaidList(param.Parameter);
					}
				case "ShowOtherPaperLaidDetailByID":
					{
						return ShowOtherPaperLaidDetailByID(param.Parameter);
					}
				case "GetOtherPaperLaidFileVersion":
					{
						return GetOtherPaperLaidFileVersion(param.Parameter);
					}
				case "UpdateOtherPaperLaidFileByID":
					{
						return UpdateOtherPaperLaidFileByID(param.Parameter);
					}
				case "UpcomingLOBByOtherpaperLaidId":
					{
						return UpcomingLOBByOtherpaperLaidId(param.Parameter);
					}
				case "GetOtherPaperLaidCounters":
					{
						return GetOtherPaperLaidCounters(param.Parameter);
					}
				case "GetPendingToLayOtherPaperLaidByType":
					{
						return GetPendingToLayOtherPaperLaidByType(param.Parameter);
					}
				case "GetOtherPaperLaidInHouseByType":
					{
						return GetOtherPaperLaidInHouseByType(param.Parameter);
					}
				case "GetOtherPaperLaidAllList":
					{
						return GetOtherPaperLaidAllList(param.Parameter);
					}
				case "GetSubmittedOtherPaperLaidListCommittee":
					{
						return GetSubmittedOtherPaperLaidListCommittee(param.Parameter);
					}
				case "ShowOtherPaperLaidDetailByIDCommittee":
					{
						return ShowOtherPaperLaidDetailByIDCommittee(param.Parameter);
					}
				case "GetOtherPaperLaidFileVersionCommittee":
					{
						return GetOtherPaperLaidFileVersionCommittee(param.Parameter);
					}

				//
				#endregion

			} return null;
		}

		static tPaperLaidV GetOtherPaperLaidAllList(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel model = param as PaperMovementModel;
			int paperCategoryId = 5;
			int? EventId = model.PaperCategoryTypeId;

			string csv = model.DepartmentId;
			IEnumerable<string> ids = csv.Split(',').Select(str => str);

			var pending = (from m in db.tPaperLaidV
						   join c in db.mEvent on m.EventId equals c.EventId
						   join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						   join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						   where t.DeptSubmittedDate == null
						   && c.PaperCategoryTypeId == paperCategoryId
						   && t.DeptSubmittedBy == null
						   && ids.Contains(m.DepartmentId)
						   && m.AssemblyId == model.AssemblyId
						   && m.SessionId == model.SessionId
						   select new PaperMovementModel
						   {
							   DeptActivePaperId = m.DeptActivePaperId,
							   PaperLaidId = m.PaperLaidId,
							   EventName = c.EventName,
							   EventId = c.EventId,
							   Title = m.Title,
							   DeparmentName = m.DeparmentName,
							   MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							   PaperTypeID = (int)m.PaperLaidId,
							   actualFilePath = t.FilePath + t.FileName,
							   PaperCategoryTypeId = c.PaperCategoryTypeId,
							   BusinessType = c.EventName,
							   version = t.Version,
							   DeptSubmittedBy = t.DeptSubmittedBy,
							   PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							   DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						   }).OrderBy(m => m.PaperLaidId).ToList();


			var submit = (from m in db.tPaperLaidV
						  join c in db.mEvent on m.EventId equals c.EventId
						  join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						  join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						  where m.DeptActivePaperId == t.PaperLaidTempId
						   && c.PaperCategoryTypeId == paperCategoryId
						   && ids.Contains(m.DepartmentId)
							&& m.AssemblyId == model.AssemblyId
						   && m.SessionId == model.SessionId
							&& t.DeptSubmittedBy != null
							&& t.DeptSubmittedDate != null
						  select new PaperMovementModel
						  {
							  DeptActivePaperId = m.DeptActivePaperId,
							  PaperLaidId = m.PaperLaidId,
							  EventName = c.EventName,
							  EventId = c.EventId,
							  Title = m.Title,
							  DeparmentName = m.DeparmentName,
							  MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							  PaperTypeID = (int)m.PaperLaidId,
							  actualFilePath = t.FilePath + t.FileName,
							  PaperCategoryTypeId = c.PaperCategoryTypeId,
							  BusinessType = c.EventName,
							  version = t.Version,
							  FilePath = t.FileName,
							  DeptSubmittedDate = t.DeptSubmittedDate,
							  DeptSubmittedBy = t.DeptSubmittedBy,
							  PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							  DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						  }).OrderBy(m => m.PaperLaidId);
			int pendingCount = pending.Count();
			int submitCount = submit.Count();
			model.PaperMovementCollection = pending;



			//foreach (var pendingList in pending)
			//{
			//    if (pendingList.DesireLayingDate != null)
			//    {

			//        var curr_Desidate = Convert.ToDateTime(pendingList.DesireLayingDate).ToString("dd");
			//        var curr_Desimonth = Convert.ToDateTime(pendingList.DesireLayingDate).ToString("MM");
			//        var curr_Desiyear = Convert.ToDateTime(pendingList.DesireLayingDate).ToString("yyyy");
			//        pendingList.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
			//    }
			//    else
			//    {

			//        var curr_Desidate = Convert.ToDateTime(pendingList.DesireLayingDate).ToString("dd");
			//        var curr_Desimonth = Convert.ToDateTime(pendingList.DesireLayingDate).ToString("MM");
			//        var curr_Desiyear = Convert.ToDateTime(pendingList.DesireLayingDate).ToString("yyyy");
			//        pendingList.DesiredLayingDate = "";
			//    }
			//    model.PaperMovementCollection.Add(pendingList);
			//}


			foreach (var submitList in submit)
			{
				var curr_date = Convert.ToDateTime(submitList.DeptSubmittedDate).ToString("dd");
				var curr_month = Convert.ToDateTime(submitList.DeptSubmittedDate).ToString("MM");
				var curr_year = Convert.ToDateTime(submitList.DeptSubmittedDate).ToString("yyyy");
				submitList.DepartmentSubmittedDate = Convert.ToDateTime(submitList.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
				// submitList.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;


				var curr_Desidate = Convert.ToDateTime(submitList.DesireLayingDate).ToString("dd");
				var curr_Desimonth = Convert.ToDateTime(submitList.DesireLayingDate).ToString("MM");
				var curr_Desiyear = Convert.ToDateTime(submitList.DesireLayingDate).ToString("yyyy");
				submitList.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;

				model.PaperMovementCollection.Add(submitList);
			}



			model.PaperMovementCollection = model.PaperMovementCollection.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
			tPaperLaidV tPaper = new tPaperLaidV();
			tPaper.ResultCount = pendingCount + submitCount;
			tPaper.DepartmentSubmitList = model.PaperMovementCollection;

			return tPaper;
		}

		static tPaperLaidV GetPaperLaidByPaperLaidId(object param)
		{
			tPaperLaidV paperLaid = param as tPaperLaidV;
			OtherPaperLaidContext plCntxt = new OtherPaperLaidContext();
			paperLaid = (from mdl in plCntxt.tPaperLaidV where mdl.PaperLaidId == paperLaid.PaperLaidId select mdl).FirstOrDefault();

			return paperLaid;
		}

		static object GetQuestionByPaperLaidID(object param)
		{
			OtherPaperLaidContext DBContext = new OtherPaperLaidContext();
			tQuestion questionModel = param as tQuestion;

			var query = (from questions in DBContext.tQuestions
						 where questions.PaperLaidId == questionModel.PaperLaidId && questions.DepartmentID == questionModel.DepartmentID
						 select questions).FirstOrDefault();

			return query;
		}

		public static object UpdateMinisterActivePaperId(object param)
		{
			string obj = param as string;
			string[] obja = obj.Split(',');

			foreach (var item in obja)
			{
				using (var obj1 = new OtherPaperLaidContext())
				{
					long id = Convert.ToInt16(item);

					var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
										where PaperLaidModel.PaperLaidId == id
										select PaperLaidModel).FirstOrDefault();
					if (PaperLaidObj != null)
					{
						PaperLaidObj.MinisterActivePaperId = PaperLaidObj.DeptActivePaperId;
					}
					obj1.SaveChanges();
					obj1.Close();
				}
			}
			string meg = "";
			return meg;
		}

		static tPaperLaidV GetDepartmentNameMinistryDetailsByMinisterID(object param)
		{
			tPaperLaidV paperLaid = param as tPaperLaidV;
			OtherPaperLaidContext plCntxt = new OtherPaperLaidContext();
			var mMinistry = (from mdl in plCntxt.mMinistry where mdl.MinistryID == paperLaid.MinistryId select mdl).SingleOrDefault();
			paperLaid.MinistryName = mMinistry.MinistryName;
			paperLaid.MinistryNameLocal = mMinistry.MinistryNameLocal;
			var DepartmentName = (from mod in plCntxt.mDepartment where paperLaid.DepartmentId == mod.deptId select mod.deptname).SingleOrDefault();
			paperLaid.DeparmentName = DepartmentName;
			return paperLaid;
		}

		static object GetQuetionEventID(object param)
		{
			mEvent mEvents = param as mEvent;
			OtherPaperLaidContext qxt = new OtherPaperLaidContext();
			int paperCategoryTypeId = (from mdl in qxt.mPaperCategoryType
									   where mdl.Name.Contains("ques")
									   select mdl.PaperCategoryTypeId).SingleOrDefault();

			mEvents = (from mdl in qxt.mEvent
					   where mdl.PaperCategoryTypeId == paperCategoryTypeId && mdl.EventName.Contains("hour")
					   select mdl).SingleOrDefault();
			return mEvents;
		}

		static tPaperLaidV GetPaperLaidById(object param)
		{
			tPaperLaidV paperLaid = param as tPaperLaidV;
			OtherPaperLaidContext plCntxt = new OtherPaperLaidContext();
			paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV where mdl.PaperLaidId == paperLaid.PaperLaidId select mdl).ToList();

			return paperLaid;
		}

		static object GetMinisterMinistryName(object param)
		{
			string DeptId = param.ToString();
			OtherPaperLaidContext ctxt = new OtherPaperLaidContext();

			var query = (from mdl in ctxt.mMInistryDepartment
						 where mdl.DeptID == DeptId
						 join mdl1 in ctxt.mMinistry on mdl.MinistryID equals mdl1.MinistryID
						 select new mMinisteryMinisterModel
						 {
							 MinistryID = mdl1.MinistryID
						 }).SingleOrDefault();
			return query;

		}

		static tPaperLaidV GetPaperLaidByEventId(object param)
		{
			tPaperLaidV paperLaid = param as tPaperLaidV;
			OtherPaperLaidContext plCntxt = new OtherPaperLaidContext();
			paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV where mdl.EventId == paperLaid.EventId && mdl.MinisterActivePaperId != null && mdl.DeptActivePaperId != null && mdl.LOBRecordId == null select mdl).ToList();

			return paperLaid;
		}

		static tPaperLaidTemp GetPaperLaidTempById(object param)
		{
			tPaperLaidTemp paperLaid = param as tPaperLaidTemp;
			OtherPaperLaidContext plCntxt = new OtherPaperLaidContext();
			var result = (from mdl in plCntxt.tPaperLaidTemp where mdl.PaperLaidTempId == paperLaid.PaperLaidTempId select mdl).FirstOrDefault();
			paperLaid = result;
			return paperLaid;
		}

		static string UpdateLOBRecordIdIntPaperLaidVS(object param)
		{
			tPaperLaidV paperLaid = param as tPaperLaidV;
			OtherPaperLaidContext plCntxt = new OtherPaperLaidContext();

			using (var obj = new OtherPaperLaidContext())
			{
				var PaperLaidObj = obj.tPaperLaidV.Where(m => m.PaperLaidId == paperLaid.PaperLaidId).FirstOrDefault();
				if (PaperLaidObj != null)
				{
					PaperLaidObj.LOBRecordId = paperLaid.LOBRecordId;
				}
				obj.SaveChanges();
				obj.Close();
			}
			return null;
		}

		static List<mPaperCategoryType> GetAllPaperCategoryType(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();

			mPaperCategoryType model = param as mPaperCategoryType;
			var query = (from dist in db.mPaperCategoryType
						 select dist);

			string searchText = (model.Name != null && model.Name != "") ? model.Name.ToLower() : "";
			if (searchText != "")
			{
				query = query.Where(a =>
					a.Name != null && a.Name.ToLower().Contains(searchText));

			}
			return query.ToList();
		}

		static object GetPaperLaidDepartmentDetails(object param)
		{
			tPaperLaidV model = new tPaperLaidV();
			DepartmentContext departmentContext = new DepartmentContext();
			SessionContext SessionContext = new SessionContext();
			return model;
		}

		// Paper pending list

		static List<PaperMovementModel> GetPendingDepartmentPaperLaidList(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel model = param as PaperMovementModel;

			int? EventId = null;
			int? PaperCategoryTypeId = null;
			if (model.EventId != 0 && model.PaperCategoryTypeId != 0)
			{
				EventId = model.EventId;
			}
			else if (model.PaperCategoryTypeId != 0)
			{
				PaperCategoryTypeId = model.PaperCategoryTypeId;
			}

			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.CommitteeId == null && t.DeptSubmittedDate == null
						 && t.DeptSubmittedBy == null
						 && (!EventId.HasValue || m.EventId == model.EventId)
						 select new PaperMovementModel
						 {
							 DeptActivePaperId = m.DeptActivePaperId,
							 PaperLaidId = m.PaperLaidId,
							 EventName = c.EventName,
							 Title = m.Title,
							 DeparmentName = m.DeparmentName,
							 MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							 PaperTypeID = (int)m.PaperLaidId,
							 actualFilePath = t.FilePath + t.FileName,
							 PaperCategoryTypeId = c.PaperCategoryTypeId,
							 BusinessType = c.EventName,
							 version = t.Version,
							 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							 DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						 }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			foreach (var item in query)
			{
				switch (item.PaperCategoryTypeId)
				{
					case 1:
						item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

						break;
					case 2:
						item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
						break;
					case 4:
						item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
						break;
					default:
						break;
				}
			}

			if (PaperCategoryTypeId != null)
			{
				var filterQuery = query.Where(foo => foo.PaperCategoryTypeId == PaperCategoryTypeId);
				return filterQuery.ToList();
			}
			return query.ToList();
		}

		// Nitin code
		static object GetSessionDates(object param)
		{
			OtherPaperLaidContext plCntxt = new OtherPaperLaidContext();
			mSessionDate sessionDates = new mSessionDate();
			tPaperLaidV paperLaid = param as tPaperLaidV;
			paperLaid.mSessionDates = (from mdl2 in plCntxt.mSessionDates where mdl2.SessionId == paperLaid.SessionId && mdl2.AssemblyId == paperLaid.AssemblyCode select mdl2).ToList().ToList();
			foreach (var item in paperLaid.mSessionDates)
			{
				item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

			}
			mSessionDate obj = new mSessionDate();
			obj.Id = 0;
			obj.DisplayDate = "Desired Date to be Lay";
			paperLaid.mSessionDates.Add(obj);

			return paperLaid.mSessionDates;

		}

		static object GetDocumentType(object param)
		{
			OtherPaperLaidContext plCntxt = new OtherPaperLaidContext();
			mDocumentType documents = new mDocumentType();
			mSessionDate sessionDates = new mSessionDate();
			tPaperLaidV paperLaid = param as tPaperLaidV;
			paperLaid.mDocumentTypes = (from mdl in plCntxt.mDocumentTypes select mdl).OrderBy(m => m.DocumentType).ToList();
			mDocumentType obj = new mDocumentType();
			obj.DocumentTypeID = 0;
			obj.DocumentType = "Paper Type";
			paperLaid.mDocumentTypes.Add(obj);
			return paperLaid.mDocumentTypes;

		}

		static object GetPaperLaidCategoryDetails(object param)
		{
			int[] ids = (int[])param;





			int id = Convert.ToInt32(ids[0]);
			int noticeTypeId = ids[1];
			OtherPaperLaidContext cnxt = new OtherPaperLaidContext();
			tPaperLaidV model = new tPaperLaidV();

			string categoryName = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == id select mdl.Name).SingleOrDefault();

			if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
			{
				model.tQuestioType = (from mdl in cnxt.tQuestionTypes select mdl).ToList();
				tQuestionType obj = new tQuestionType();
				obj.QuestionTypeName = "select Question Type";
				obj.QuestionTypeId = 0;
				model.tQuestioType.Add(obj);
				model.categoryType = "question";
			}
			else if (categoryName.IndexOf("notice", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
			{
				model.tMemberNotice = (from mdl in cnxt.tMemberNotice where mdl.NoticeTypeID == noticeTypeId select mdl).ToList();
				tMemberNotice ob = new tMemberNotice();
				ob.NoticeId = 0;
				ob.NoticeNumber = "Select Notice Number";
				model.tMemberNotice.Add(ob);
				model.categoryType = "notice";
			}
			else if (categoryName.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
			{
				model.mBillTypes = (from mdl in cnxt.mBillTypes select mdl).ToList();
				model.categoryType = "bill";
			}
			else
			{ model.categoryType = "other"; }
			return model;
		}
		static object GetFileSaveDetails(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;
			OtherPaperLaidContext cnxt = new OtherPaperLaidContext();
			int? catedoryId = (from mdl in cnxt.mEvent
							   where mdl.EventId == model.EventId
							   select mdl.PaperCategoryTypeId).SingleOrDefault();
			string respectivePaper = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == catedoryId select mdl.Name).SingleOrDefault();

			var FileUpdate = (from paperLaidTemp in cnxt.tPaperLaidTemp
							  where paperLaidTemp.PaperLaidId == model.PaperLaidId
								  && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
							  select paperLaidTemp).FirstOrDefault();
			if (FileUpdate != null)
			{
				model.Count = Convert.ToInt32(FileUpdate.Version);
			}
			else
			{
				int count = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
				int newVersion = count + 1;
				model.Count = newVersion;
			}

			model.RespectivePaper = respectivePaper;

			return model;
		}

		static tPaperLaidV GetQuestionNumber(object param)
		{
			tPaperLaidV mod = param as tPaperLaidV;
			// int questionId = Convert.ToInt32(param);
			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			tPaperLaidV paperMdel = new tPaperLaidV();
			paperMdel.tQuestion = (from mdl in pCtxt.tQuestions
								   where mdl.QuestionType == mod.QuestionTypeId && mdl.DepartmentID == mod.DepartmentId
									   && mdl.PaperLaidId == null
								   select mdl).ToList();
			return paperMdel;
		}

		static object GetQuestionDetails(object param)
		{
			tQuestion tQuestions = new tQuestion();
			int questionNumber = Convert.ToInt32(param);
			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			tPaperLaidV paperMdel = new tPaperLaidV();
			tQuestions = (from mdl in pCtxt.tQuestions where mdl.QuestionNumber == questionNumber select mdl).SingleOrDefault();
			return tQuestions;
		}
		static object GetNoticeDetails(object param)
		{
			string noticeNumber = param.ToString();
			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			tMemberNotice model = new tMemberNotice();
			model = (from mdl in pCtxt.tMemberNotice where mdl.NoticeNumber == noticeNumber select mdl).SingleOrDefault();
			model.Subject = Regex.Replace(model.Subject, @"<[^>]+>|&nbsp;", "").Trim();
			model.Notice = Regex.Replace(model.Notice, @"<[^>]+>|&nbsp;", "").Trim();
			string[] date = model.NoticeDate.ToString().Split(' ');
			model.DateNotice = date[0];
			return model;
		}
		static object CheckBillNUmber(object param)
		{
			string billNumber = param.ToString();
			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			tBillRegister model = new tBillRegister();
			model = (from mdl in pCtxt.tBillRegister where mdl.BillNumber == billNumber select mdl).SingleOrDefault();
			return model;
		}


		static object GetNotices(object param)
		{
			int noticeId = Convert.ToInt32(param);
			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			tPaperLaidV paperMdel = new tPaperLaidV();
			paperMdel.tMemberNotice = (from mdl in pCtxt.tMemberNotice where mdl.NoticeTypeID == noticeId && mdl.PaperLaidId == null select mdl).ToList();
			return paperMdel;
		}
		static object SubmittPaperLaidEntry(object param)
		{
			if (null == param)
			{
				return null;
			}

			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidV submitPaperEntry = param as tPaperLaidV;

			if (submitPaperEntry.PaperLaidId == 0)
			{
				paperLaidCntxtDB.tPaperLaidV.Add(submitPaperEntry);
			}
			else
			{
				paperLaidCntxtDB.tPaperLaidV.Attach(submitPaperEntry);
				paperLaidCntxtDB.Entry(submitPaperEntry).State = EntityState.Modified;
			}
			paperLaidCntxtDB.SaveChanges();
			paperLaidCntxtDB.Close();
			return submitPaperEntry;

		}
		static object SaveDetailsInTempPaperLaid(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;

			var query = (from paperLaidTemp in paperLaidCntxtDB.tPaperLaidTemp
						 where paperLaidTemp.PaperLaidId == submitPaperTemp.PaperLaidId
						 && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
						 select paperLaidTemp).SingleOrDefault();

			if (query != null)
			{
				paperLaidCntxtDB.tPaperLaidTemp.Attach(query);
				query.FileName = submitPaperTemp.FileName;
				query.FilePath = submitPaperTemp.FilePath;
			}
			else
			{
				submitPaperTemp.DeptSubmittedDate = null;
				paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
			}

			paperLaidCntxtDB.SaveChanges();
			paperLaidCntxtDB.Close();
			return submitPaperTemp;
		}

		static object UpdateDeptActiveId(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
			var tPaperLaid = (from mdl in paperLaidCntxtDB.tPaperLaidV where mdl.PaperLaidId == submitPaperTemp.PaperLaidId select mdl).SingleOrDefault();
			if (tPaperLaid != null)
			{
				tPaperLaid.DeptActivePaperId = submitPaperTemp.PaperLaidTempId;
				paperLaidCntxtDB.SaveChanges();
				paperLaidCntxtDB.Close();

			}
			return tPaperLaid;

		}
		static object UpdateQuestionPaperLaidId(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();

			tQuestion updatePaperLaid = param as tQuestion;
			var tQuestion = (from mdl in paperLaidCntxtDB.tQuestions where mdl.QuestionID == updatePaperLaid.QuestionID select mdl).SingleOrDefault();
			if (tQuestion != null)
			{
				tQuestion.PaperLaidId = updatePaperLaid.PaperLaidId;
				paperLaidCntxtDB.SaveChanges();
				paperLaidCntxtDB.Close();

			}
			return tQuestion;

		}

		static object UpdateNoticePaperLaidId(object param)
		{
			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();

			tMemberNotice updatePaperLaid = param as tMemberNotice;
			var tMemberNotice = (from mdl in paperLaidCntxtDB.tMemberNotice where mdl.NoticeId == updatePaperLaid.NoticeId select mdl).SingleOrDefault();
			if (tMemberNotice != null)
			{
				tMemberNotice.PaperLaidId = updatePaperLaid.PaperLaidId;
				paperLaidCntxtDB.SaveChanges();
				paperLaidCntxtDB.Close();
			}
			return tMemberNotice;

		}
		static object InsertBillDetails(object param)
		{
			if (null == param)
			{ return null; }
			tBillRegister bills = new tBillRegister();
			tPaperLaidV mdl = param as tPaperLaidV;
			bills.PaperLaidId = (int)mdl.PaperLaidId;
			bills.BillDate = Convert.ToDateTime(mdl.BillDate);
			bills.BillNumber = mdl.BillNUmberValue + " of " + mdl.BillNumberYear;
			bills.AssemblyId = mdl.AssemblyCode;
			bills.SessionId = mdl.SessionCode;
			bills.MinisterId = mdl.MinistryId;
			bills.DepartmentId = mdl.DepartmentId;
			bills.BillTypeId = mdl.BillTypeId;
			bills.IsBillAct = false;
			bills.IsBillReturn = false;
			bills.IsBillSigned = false;
			bills.IsCirculation = false;
			bills.IsClauseConsideration = false;
			bills.IsElicitingOpinion = false;
			bills.IsFinancialBillRule1 = false;
			bills.IsFinancialBillRule2 = false;
			bills.IsForwordedToMinister = false;
			bills.IsIntentioned = false;
			bills.IsIntentionedToMinister = false;
			bills.IsIntentionReturn = false;
			bills.IsIntentionSigned = false;
			bills.IsMoneyBill = false;
			bills.IsRecommendedArticle = false;
			bills.IsReferSelectCommitte = false;
			bills.IsSubmitted = false;
			bills.IsThirdReading = false;
			bills.IsWithdrawal = false;
			bills.ShortTitle = mdl.Description;
			using (OtherPaperLaidContext pCtxt = new OtherPaperLaidContext())
			{
				bills = pCtxt.tBillRegister.Add(bills);
				pCtxt.SaveChanges();
				pCtxt.Close();
			}
			return bills;
		}
		static object SubmitPendingDepartmentPaperLaid(object param)
		{
			tPaperLaidTemp mdl = param as tPaperLaidTemp;
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();

			var tPaperLaidTemps = (from md in paperLaidCntxtDB.tPaperLaidTemp
								   where md.PaperLaidId == mdl.PaperLaidId && md.Version ==
									   (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == mdl.PaperLaidId select bdMax.Version).Max()
								   select md).SingleOrDefault();
			//var tPaperLaidTemps = (from model in paperLaidCntxtDB.tPaperLaidTemp
			//                       where model.PaperLaidId == mdl.PaperLaidId && model.PaperLaidTempId == query.PaperLaidTempId
			//                       select model).SingleOrDefault();

			if (tPaperLaidTemps != null)
			{
				tPaperLaidTemps.DeptSubmittedDate = mdl.DeptSubmittedDate;
				tPaperLaidTemps.DeptSubmittedBy = mdl.DeptSubmittedBy;
				paperLaidCntxtDB.SaveChanges();
				paperLaidCntxtDB.Close();
			}


			return tPaperLaidTemps;
		}

		static object EditPendingPaperLaidDetials(object param)
		{
			tPaperLaidV mdl = param as tPaperLaidV;
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			mdl = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == mdl.PaperLaidId select model).SingleOrDefault();
			var fileInfo = (from model in paperLaidCntxtDB.tPaperLaidTemp
							where model.PaperLaidId == mdl.PaperLaidId
							where model.DeptSubmittedBy == null && model.DeptSubmittedDate == null
							select model).SingleOrDefault();
			mdl.FileName = fileInfo.FileName;
			mdl.FilePath = fileInfo.FilePath + fileInfo.FileName;
			return mdl;
		}


		static object UpdatePaperLaidEntry(object param)
		{
			tPaperLaidV updatedDetails = param as tPaperLaidV;
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			var existingDetails = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == updatedDetails.PaperLaidId select model).SingleOrDefault();

			if (existingDetails != null)
			{
				existingDetails.EventId = updatedDetails.EventId;
				existingDetails.MinistryId = updatedDetails.MinistryId;
				existingDetails.DepartmentId = updatedDetails.DepartmentId;
				existingDetails.SessionId = updatedDetails.SessionCode;
				existingDetails.PaperTypeID = updatedDetails.PaperTypeID;
				existingDetails.Title = updatedDetails.Title;
				existingDetails.Description = updatedDetails.Description;
				existingDetails.Remark = updatedDetails.Remark;
				existingDetails.PaperLaidId = updatedDetails.PaperLaidId;
				paperLaidCntxtDB.SaveChanges();
				paperLaidCntxtDB.Close();
			}
			return existingDetails;
		}
		static object UpdateDetailsInTempPaperLaid(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
			var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidTempId == submitPaperTemp.PaperLaidTempId select mdl).SingleOrDefault();
			if (existingTPaperLaidTemp != null)
			{
				existingTPaperLaidTemp.FileName = submitPaperTemp.FileName;
				//    existingTPaperLaidTemp.FilePath = submitPaperTemp.FilePath;
			}

			paperLaidCntxtDB.SaveChanges();
			paperLaidCntxtDB.Close();
			return submitPaperTemp;
		}
		static object GetExisitngFileDetails(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidV details = param as tPaperLaidV;

			var existingTPaperLaidTemp = (from md in paperLaidCntxtDB.tPaperLaidTemp
										  where md.PaperLaidId == details.PaperLaidId && md.Version ==
											  (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == details.PaperLaidId select bdMax.Version).Max()
										  select md).SingleOrDefault();

			//var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidId == details.PaperLaidId select mdl).SingleOrDefault();
			if (existingTPaperLaidTemp != null)
			{

				return existingTPaperLaidTemp;
			}

			return null;
		}
		// Paper pending list
		static List<PaperMovementModel> GetSubmittedPaperLaidList(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel model = param as PaperMovementModel;

			int? EventId = null;
			int? PaperCategoryTypeId = null;
			if (model.EventId != 0 && model.PaperCategoryTypeId != 0)
			{
				EventId = model.EventId;
			}
			else if (model.PaperCategoryTypeId != 0)
			{
				PaperCategoryTypeId = model.PaperCategoryTypeId;
			}

			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.DeptActivePaperId == t.PaperLaidTempId
							 //&& t.DeptSubmittedDate != null
							 //&& t.DeptSubmittedBy != null
						 && (!EventId.HasValue || m.EventId == model.EventId)
						 //&& (!PaperCategoryTypeId.HasValue || m.EventId



						 select new PaperMovementModel
						 {
							 DeptActivePaperId = m.DeptActivePaperId,
							 PaperLaidId = m.PaperLaidId,
							 EventName = c.EventName,
							 Title = m.Title,
							 DeparmentName = m.DeparmentName,
							 MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							 PaperTypeID = (int)m.PaperLaidId,
							 actualFilePath = t.FilePath + t.FileName,
							 PaperCategoryTypeId = c.PaperCategoryTypeId,
							 BusinessType = c.EventName,
							 version = t.Version,
							 DeptSubmittedBy = t.DeptSubmittedBy,
							 DeptSubmittedDate = t.DeptSubmittedDate,
							 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							 DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						 }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			foreach (var item in query)
			{
				switch (item.PaperCategoryTypeId)
				{
					case 1:
						item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

						break;
					case 2:
						item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
						break;
					case 4:
						item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
						break;
					default:
						break;
				}
			}

			if (PaperCategoryTypeId != null)
			{
				var filterQuery = query.Where(foo => foo.PaperCategoryTypeId == PaperCategoryTypeId);
				return filterQuery.ToList();
			}

			return query.ToList();
		}

		static object DeletePaperLaidByID(object param)
		{
			tPaperLaidV tPaperLaid = param as tPaperLaidV;
			OtherPaperLaidContext db = new OtherPaperLaidContext();

			//remove from tPaperLaidV

			var query = (from mdl in db.tPaperLaidV where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
			db.tPaperLaidV.Remove(query);


			//Remove from tPaperLaidTemp

			var query1 = (from mdl in db.tPaperLaidTemp where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
			db.tPaperLaidTemp.Remove(query1);

			//Remove From Bill if exist

			var query2 = (from mdl in db.tBillRegister where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
			if (query2 != null)
			{
				db.tBillRegister.Remove(query2);
			}

			//Remove Id form tQuestion if exist

			var query3 = (from mdl in db.tQuestions where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
			if (query3 != null)
			{
				query3.PaperLaidId = null;
			}
			//Remove Id form tMemberNotice if exist

			var query4 = (from mdl in db.tMemberNotice where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
			if (query4 != null)
			{
				query4.PaperLaidId = null;
			}
			db.SaveChanges();
			db.Close();
			return null;
		}
		static object UpdatePaperLaidFileByID(object param)
		{
			tPaperLaidTemp model = param as tPaperLaidTemp;

			OtherPaperLaidContext db = new OtherPaperLaidContext();
			if (model == null)
			{
				return null;
			}
			model.DeptSubmittedDate = null;
			model.DeptSubmittedBy = null;
			model = db.tPaperLaidTemp.Add(model);
			model.PaperLaidTempId = model.PaperLaidTempId;
			db.SaveChanges();
			db.Close();
			return model;
		}

		static object ShowPaperLaidDetailByID(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel model = param as PaperMovementModel;

			var query = (from tPaper in db.tPaperLaidV
						 where model.PaperLaidId == tPaper.PaperLaidId
						 join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
						 join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
						 join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
						 join tPaperTemp in db.tPaperLaidTemp on tPaper.DeptActivePaperId equals tPaperTemp.PaperLaidTempId
						 join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
						 select new PaperMovementModel
						 {
							 EventName = mEvents.EventName,
							 MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
							 DeparmentName = mDept.deptname,
							 PaperLaidId = tPaper.PaperLaidId,
							 actualFilePath = tPaperTemp.FileName,
							 ReplyPathFileLocation = tPaperTemp.FilePath + tPaperTemp.FileName,
							 ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
							 Title = tPaper.Title,
							 Description = tPaper.Description,
							 Remark = tPaper.Remark,
							 PaperCategoryTypeId = mEvents.PaperCategoryTypeId,
							 PaperCategoryTypeName = mPaperCategory.Name,
							 PaperSubmittedDate = tPaperTemp.DeptSubmittedDate
						 }).SingleOrDefault();
			var categoryName = query.PaperCategoryTypeName;
			if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
			{
				int? Number = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId select mdl.QuestionNumber).SingleOrDefault();

				//New
				var questionDet = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId select mdl).SingleOrDefault();
				if (questionDet != null)
				{
					query.MemberName = (from member in db.mMember
										where member.MemberCode == questionDet.MemberCode
										select member.Name).FirstOrDefault();
				}
				query.Number = Number.ToString();
			}
			//else if (categoryName.IndexOf("notice", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
			//{
			//    query.Number = (from mdl in db.tMemberNotice where mdl.PaperLaidId == model.PaperLaidId select mdl.NoticeNumber).SingleOrDefault();
			//}
			//else if (categoryName.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
			//{
			//    query.Number = (from mdl in db.tBillRegister where mdl.PaperLaidId == model.PaperLaidId select mdl.BillNumber).SingleOrDefault();
			//}
			//else
			//{ query.Number = ""; }
			return query;
		}

		static object GetFileVersion(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel paper = param as PaperMovementModel;
			tPaperLaidV model = new tPaperLaidV();

			model.Count = (from tPaperTemp in db.tPaperLaidTemp
						   where tPaperTemp.PaperLaidId == paper.PaperLaidId
						   select tPaperTemp.PaperLaidId).Count();
			model.Count = model.Count + 1;
			return model;
		}



		static object GetSubmittedPaperLaidByID(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();

			tPaperLaidV model = param as tPaperLaidV;

			model.DepartmentSubmitList = (from tPaper in db.tPaperLaidV
										  where model.PaperLaidId == tPaper.PaperLaidId
										  join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
										  join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
										  join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
										  //join mSessionDates in db.mSessionDates on tPaper.DesireLayingDateId equals mSessionDates.Id
										  //join mDocumentTypes in db.mDocumentTypes on tPaper.PaperTypeID equals mDocumentTypes.DocumentTypeID
										  join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
										  join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
										  select new PaperMovementModel
										  {
											  DeptSubmittedDate = tPaperTemp.DeptSubmittedDate,
											  PaperTypeName = mPaperCategory.Name,
											  actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
											  FilePath = tPaperTemp.FilePath,
											  DeptSubmittedBy = tPaperTemp.DeptSubmittedBy,
											  PaperLaidId = model.PaperLaidId,
											  DeptActivePaperId = tPaper.DeptActivePaperId,
											  version = tPaperTemp.Version
										  }).ToList();
			int i = 0;
			foreach (var mod in model.DepartmentSubmitList)
			{
				if (i == 0)
				{
					model.FilePath = mod.FilePath;
					model.PaperLaidId = mod.PaperLaidId;
					i++;
				}
			}
			return model;
		}

		#region Newly Added (Ram)

		static tPaperLaidV GetPendingQuestionsByType(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;

			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			//var query = (from mdl in pCtxt.tQuestions
			//             where mdl.QuestionType == model.QuestionTypeId && mdl.DepartmentID == model.DepartmentId
			//                 && mdl.PaperLaidId == null
			//             select mdl).ToList();

			var query = (from questions in pCtxt.tQuestions
						 where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
							 && questions.PaperLaidId == null
						 select new QuestionModelCustom
						 {
							 QuestionID = questions.QuestionID,
							 QuestionNumber = questions.QuestionNumber,
							 Subject = questions.Subject,
							 PaperLaidId = questions.PaperLaidId,
							 Version = null,
							 DeptActivePaperId = null,
							 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
						 }).ToList();

			int totalRecords = query.Count();
			var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			model.ResultCount = totalRecords;
			//model.tQuestion = results;
			model.tQuestionModel = results;

			return model;
		}

		static tPaperLaidV GetSubmittedQuestionsByType(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;

			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			//var query = (from questions in pCtxt.tQuestions
			//             join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
			//             join paperLaidVS in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaidVS.PaperLaidId
			//             where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
			//                 && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null
			//                 && paperLaidTemp.DeptSubmittedDate != null
			//                 && paperLaidVS.DeptActivePaperId==paperLaidTemp.PaperLaidTempId
			//             select questions).ToList();

			var query = (from questions in pCtxt.tQuestions
						 join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
						 join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
						 where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
							 && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null
							 && paperLaidTemp.DeptSubmittedDate != null
							 && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
						 select new QuestionModelCustom
						 {
							 QuestionID = questions.QuestionID,
							 QuestionNumber = questions.QuestionNumber,
							 Subject = paperLaid.Title,
							 PaperLaidId = questions.PaperLaidId,
							 Version = paperLaidTemp.Version,
							 DeptActivePaperId = paperLaid.DeptActivePaperId,
							 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
						 }).ToList();

			int totalRecords = query.Count();
			var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			model.ResultCount = totalRecords;
			model.tQuestionModel = results;

			return model;
		}

		//static tPaperLaidV GetPendingForSubQuestionsByType(object param)
		//{
		//    OtherPaperLaidContext db = new OtherPaperLaidContext();
		//    tPaperLaidV model = param as tPaperLaidV;

		//    //int? EventId = null;
		//    //int? PaperCategoryTypeId = null;
		//    //if (model.EventId != 0 && model.PaperCategoryTypeId != 0)
		//    //{
		//    //    EventId = model.EventId;
		//    //}
		//    //else if (model.PaperCategoryTypeId != 0)
		//    //{
		//    //    PaperCategoryTypeId = model.PaperCategoryTypeId;
		//    //}

		//    var query = (from m in db.tPaperLaidV
		//                 join c in db.mEvent on m.EventId equals c.EventId
		//                 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
		//                 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
		//                 join questions in db.tQuestions on model.QuestionTypeId equals questions.QuestionType
		//                 where m.CommitteeId == null && t.DeptSubmittedDate == null
		//                 && t.DeptSubmittedBy == null 
		//                 //&& c.PaperCategoryTypeId == model.PaperCategoryTypeId && (!EventId.HasValue || m.EventId == model.EventId)
		//                 select new PaperMovementModel
		//                 {
		//                     DeptActivePaperId = m.DeptActivePaperId,
		//                     PaperLaidId = m.PaperLaidId,
		//                     EventName = c.EventName,
		//                     Title = m.Title,
		//                     DeparmentName = m.DeparmentName,
		//                     MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
		//                     PaperTypeID = (int)m.PaperLaidId,
		//                     actualFilePath = t.FilePath + t.FileName,
		//                     PaperCategoryTypeId = c.PaperCategoryTypeId,
		//                     BusinessType = c.EventName,
		//                     version = t.Version,
		//                     QuestionNumber = questions.QuestionNumber,
		//                     PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault()
		//                 }).OrderBy(m => m.PaperLaidId).ToList();

		//    int totalRecords = query.Count();
		//    var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

		//    model.ResultCount = totalRecords;
		//    model.DepartmentPendingList = results;

		//    return model;
		//}


		static tPaperLaidV GetPendingForSubQuestionsByType(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;

			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			var query = (from questions in pCtxt.tQuestions
						 join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
						 join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
						 where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
							 && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy == null
							 && paperLaidTemp.DeptSubmittedDate == null
						 select new QuestionModelCustom
						 {
							 QuestionID = questions.QuestionID,
							 QuestionNumber = questions.QuestionNumber,
							 //Subject = questions.Subject,
							 Subject = paperLaid.Title,
							 PaperLaidId = questions.PaperLaidId,
							 Version = paperLaidTemp.Version,
							 DeptActivePaperId = paperLaid.DeptActivePaperId,
							 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault()
						 }).ToList();

			int totalRecords = query.Count();
			var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			model.ResultCount = totalRecords;
			model.tQuestionModel = results;

			return model;
		}


		//Get all the questions Which are laid in house.
		//TODO : Change the way it is working at the moment (As we are dealing with questions here we should be able to pass and get instance of Question model)
		static tPaperLaidV GetLaidInHouseQuestionsByType(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;

			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			var query = (from questions in pCtxt.tQuestions
						 join paperLaidVS in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaidVS.PaperLaidId
						 join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
						 where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
							 && questions.PaperLaidId != null && paperLaidVS.LOBRecordId != null && paperLaidVS.LaidDate != null
							 && paperLaidVS.DeptActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidVS.MinisterActivePaperId != null

						 select questions).ToList();

			int totalRecords = query.Count();
			var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			model.ResultCount = totalRecords;
			model.tQuestion = results;

			return model;
		}

		static tPaperLaidV GetPendingToLayQuestionsByType(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;
			OtherPaperLaidContext pCtxt = new OtherPaperLaidContext();
			var query = (from questions in pCtxt.tQuestions
						 join paperLaidC in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaidC.PaperLaidId

						 where questions.QuestionType == model.QuestionTypeId && questions.DepartmentID == model.DepartmentId
							 && questions.PaperLaidId != null && paperLaidC.LOBRecordId != null && paperLaidC.LaidDate == null

						 select questions).ToList();

			int totalRecords = query.Count();
			var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			model.ResultCount = totalRecords;
			model.tQuestion = results;

			return model;
		}

		static object GetCountForQuestionTypes(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;
			model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;
			model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
			model.TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
			model.TotalStaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
			model.TotalStaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
			return model;
		}
		#endregion

		#region OtherPaperLaid

		static object GetCountForOtherPaperLaidTypes(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			//  PaperMovementModel model = param as PaperMovementModel;
			tPaperLaidV mod = param as tPaperLaidV;
			int paperCategoryId = 5;
			string csv = mod.DepartmentId;
			IEnumerable<string> ids = csv.Split(',').Select(str => str);


			int count = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where t.DeptSubmittedDate == null
						 && c.PaperCategoryTypeId == paperCategoryId
						 && t.DeptSubmittedBy == null
						 && t.MinisterSubmittedBy == null
						 && t.MinisterSubmittedDate == null
						 && ids.Contains(m.DepartmentId)
						 && m.AssemblyId == mod.AssemblyId
						 && m.SessionId == mod.SessionId
						&& m.DeptActivePaperId == t.PaperLaidTempId
						 select new PaperMovementModel
						 {
						 }).Count();






			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.DeptActivePaperId == t.PaperLaidTempId
						 && c.PaperCategoryTypeId == paperCategoryId
						 && ids.Contains(m.DepartmentId)
						 && m.AssemblyId == mod.AssemblyId
						 && m.SessionId == mod.SessionId
						&& t.DeptSubmittedBy != null
						&& t.DeptSubmittedDate != null
						 //  && t.MinisterSubmittedBy != null
						 //&& t.MinisterSubmittedDate != null
						 select new PaperMovementModel
						 {
						 }).Count();


			db.Close();
			mod.OtherPaperCount = count;
			mod.Count = query;
			return mod;
		}


		static List<OtherPaperDraftCustom> GetOtherPaperLaidPendingsDetails(object param)
		{


			OtherPaperLaidContext db = new OtherPaperLaidContext();
			OtherPaperDraftCustom model = param as OtherPaperDraftCustom;
			int paperCategoryId = 5;
			//int? EventId = model.PaperCategoryTypeId; 
			int? EventId = model.PaperCategoryTypeId;

			//            (from tPaperLaidVS m
			//join .mEvent c on m.EventId equals c.EventId
			//join  mMinistry ministery on m.MinistryId equals ministery.MinistryID
			//join tPaperLaidTemp t on m.PaperLaidId equals t.PaperLaidId
			//where c.PaperCategoryTypeId=5 && t.DeptSubmittedBy == null && t.DeptSubmittedDate == NULL


			string csv = model.DepartmentId;
			IEnumerable<string> ids = csv.Split(',').Select(str => str);
			int? QNumber = 0;
			int? Bname = 0;
			if (model.PaperTypeID != 0 && model.PaperTypeID != null)
			{
				QNumber = model.PaperTypeID;
			}
			if (model.EventId != 0 && model.EventId != null)
			{
				Bname = model.EventId;
			}
			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.DeptActivePaperId == t.PaperLaidTempId

						 && c.PaperCategoryTypeId == paperCategoryId
						 && t.DeptSubmittedBy == null
						 && ids.Contains(m.DepartmentId)
						 && m.AssemblyId == model.AssemblyId
						 && m.SessionId == model.SessionId
						 && (QNumber == 0 || QNumber == m.PaperLaidId)
						   && (QNumber == 0 || QNumber == c.EventId)
						 select new OtherPaperDraftCustom
						 {
							 DeptActivePaperId = m.DeptActivePaperId,
							 PaperLaidId = m.PaperLaidId,
							 EventName = c.EventName,
							 EventId = c.EventId,
							 Title = m.Title,
							 evidhanReferenceNumber = t.evidhanReferenceNumber,
							 DeparmentName = m.DeparmentName,
							 MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							 PaperTypeID = (int)m.PaperLaidId,
							 actualFilePath = t.FilePath + t.FileName,
							 PaperCategoryTypeId = c.PaperCategoryTypeId,
							 BusinessType = c.EventName,
							 version = t.Version,
							 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							 DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						 }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			if (EventId != null && EventId != 0)
			{

				var filterQuery = query.Where(foo => foo.EventId == EventId);
				return filterQuery.ToList();
			}

			foreach (var item in query)
			{
				if (item.DesireLayingDate != null)
				{

					var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
					var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
					var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
					item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
				}
				else
				{
					var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
					var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
					var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
					item.DesiredLayingDate = "";
				}
			}

			return query.ToList();
		}
		static List<mEvent> GetOtherPaperLaidPaperCategoryType(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();

			mEvent model = param as mEvent;


			var query = (from dist in db.mEvent
						 where dist.PaperCategoryTypeId == 5
						 select dist);

			//string searchText = (model.EventName != null && model.EventName != "") ? model.EventName.ToLower() : "";
			//if (searchText != "")
			//{
			//    query = query.Where(a =>
			//        a.EventName != null && a.EventName.ToLower().Contains(searchText));

			//}

			return query.ToList();
		}
		static object UpdateOtherPaperLaidEntry(object param)
		{
			tPaperLaidV updatedDetails = param as tPaperLaidV;
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			var existingDetails = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == updatedDetails.PaperLaidId select model).SingleOrDefault();

			if (existingDetails != null)
			{
				existingDetails.EventId = updatedDetails.EventId;
				existingDetails.MinistryId = updatedDetails.MinistryId;
				existingDetails.DepartmentId = updatedDetails.DepartmentId;
				existingDetails.SessionId = updatedDetails.SessionId;
				existingDetails.PaperTypeID = updatedDetails.PaperTypeID;
				existingDetails.Title = updatedDetails.Title;
				existingDetails.Description = updatedDetails.Description;
				existingDetails.Remark = updatedDetails.Remark;
				existingDetails.PaperLaidId = updatedDetails.PaperLaidId;
				existingDetails.DesireLayingDateId = updatedDetails.DesireLayingDateId;
				paperLaidCntxtDB.SaveChanges();
				paperLaidCntxtDB.Close();
			}
			return existingDetails;
		}
		static object GetExisitngFileDetailsForOtherPaperLaid(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidV details = param as tPaperLaidV;

			var existingTPaperLaidTemp = (from md in paperLaidCntxtDB.tPaperLaidTemp
										  where md.PaperLaidId == details.PaperLaidId && md.Version ==
											  (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == details.PaperLaidId select bdMax.Version).Max()
										  select md).SingleOrDefault();

			//var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidId == details.PaperLaidId select mdl).SingleOrDefault();
			if (existingTPaperLaidTemp != null)
			{

				return existingTPaperLaidTemp;
			}

			return null;
		}
		static object UpdateDetailsInTempOtherPaperLaid(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
			var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidTempId == submitPaperTemp.PaperLaidTempId select mdl).SingleOrDefault();
			if (existingTPaperLaidTemp != null)
			{
				existingTPaperLaidTemp.FileName = submitPaperTemp.FileName;
				existingTPaperLaidTemp.DeptSubmittedBy = submitPaperTemp.DeptSubmittedBy;
				existingTPaperLaidTemp.DeptSubmittedDate = submitPaperTemp.DeptSubmittedDate;

				//    existingTPaperLaidTemp.FilePath = submitPaperTemp.FilePath;
			}

			paperLaidCntxtDB.SaveChanges();
			paperLaidCntxtDB.Close();
			return submitPaperTemp;
		}
		static object SubmittOtherPaperLaidEntry(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidV submitPaperEntry = param as tPaperLaidV;
			paperLaidCntxtDB.tPaperLaidV.Add(submitPaperEntry);
			paperLaidCntxtDB.SaveChanges();
			paperLaidCntxtDB.Close();
			return submitPaperEntry;

		}
		static object GetFileSaveDetailsForOtherPaperLaid(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;
			OtherPaperLaidContext cnxt = new OtherPaperLaidContext();
			int? catedoryId = (from mdl in cnxt.mEvent
							   where mdl.EventId == model.EventId
							   select mdl.PaperCategoryTypeId).SingleOrDefault();
			string respectivePaper = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == catedoryId select mdl.Name).SingleOrDefault();
			int count = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
			int newVersion = count + 1;
			model.RespectivePaper = respectivePaper;
			model.Count = newVersion;
			return model;
		}
		static object SaveDetailsInTempOtherPaperLaid(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
			//submitPaperTemp.DeptSubmittedDate = DateTime.Now;


			paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
			var RefNumber = submitPaperTemp.AssemblyId + "/" + submitPaperTemp.SessionId + "/" + submitPaperTemp.PaperLaidId + "/" + "v" + submitPaperTemp.Version;
			submitPaperTemp.evidhanReferenceNumber = RefNumber;
			paperLaidCntxtDB.SaveChanges();
			paperLaidCntxtDB.Close();
			return submitPaperTemp;
		}
		static object SubmitPendingDepartmentOtherPaperLaid(object param)
		{
			tPaperLaidTemp mdl = param as tPaperLaidTemp;
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();

			var tPaperLaidTemps = (from md in paperLaidCntxtDB.tPaperLaidTemp
								   where md.PaperLaidId == mdl.PaperLaidId && md.Version ==
									   (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == mdl.PaperLaidId select bdMax.Version).Max()
								   select md).SingleOrDefault();
			//var tPaperLaidTemps = (from model in paperLaidCntxtDB.tPaperLaidTemp
			//                       where model.PaperLaidId == mdl.PaperLaidId && model.PaperLaidTempId == query.PaperLaidTempId
			//                       select model).SingleOrDefault();

			if (tPaperLaidTemps != null)
			{
				tPaperLaidTemps.DeptSubmittedDate = mdl.DeptSubmittedDate;
				tPaperLaidTemps.DeptSubmittedBy = mdl.DeptSubmittedBy;
				paperLaidCntxtDB.SaveChanges();
				paperLaidCntxtDB.Close();
			}


			return tPaperLaidTemps;
		}
		static object EditPendingOtherPaperLaidDetials(object param)
		{
			tPaperLaidV mdl = param as tPaperLaidV;
			tPaperLaidTemp tempModel = new tPaperLaidTemp();

			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			mdl = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == mdl.PaperLaidId select model).FirstOrDefault();
			tempModel = (from mdl1 in paperLaidCntxtDB.tPaperLaidTemp
						 where mdl1.PaperLaidId == mdl.PaperLaidId && mdl1.Version ==
							 (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == mdl.PaperLaidId select bdMax.Version).Max()
						 select mdl1).FirstOrDefault();
			mdl.actualFilePath = tempModel.FilePath + tempModel.FileName;
			mdl.FileVersion = tempModel.Version;
			mdl.FileName = tempModel.FileName;
			mdl.DocFileName = tempModel.DocFileName;

			return mdl;
		}
		static object UpdateOthePaperLaidDeptActiveId(object param)
		{

			if (null == param)
			{
				return null;
			}
			OtherPaperLaidContext paperLaidCntxtDB = new OtherPaperLaidContext();
			tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
			var tPaperLaid = (from mdl in paperLaidCntxtDB.tPaperLaidV where mdl.PaperLaidId == submitPaperTemp.PaperLaidId select mdl).SingleOrDefault();
			if (tPaperLaid != null)
			{
				tPaperLaid.DeptActivePaperId = submitPaperTemp.PaperLaidTempId;
				paperLaidCntxtDB.SaveChanges();
				paperLaidCntxtDB.Close();

			}
			return tPaperLaid;

		}
		static object GetAllEventsByOtherPaperLaid(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();

			var mEvents = (from mdl in db.mEvent where mdl.PaperCategoryTypeId == 5 select mdl).OrderBy(m => m.EventName).ToList();
			return mEvents;
		}
		static List<PaperMovementModel> GetSubmittedOtherPaperLaidList(object param)
		{



			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel model = param as PaperMovementModel;
			int paperCategoryId = 5;
			int? EventId = model.PaperCategoryTypeId;

			//            (from tPaperLaidVS m
			//join .mEvent c on m.EventId equals c.EventId
			//join  mMinistry ministery on m.MinistryId equals ministery.MinistryID
			//join tPaperLaidTemp t on m.PaperLaidId equals t.PaperLaidId
			//where c.PaperCategoryTypeId=5 && t.DeptSubmittedBy == null && t.DeptSubmittedDate == NULL

			string csv = model.DepartmentId;
			IEnumerable<string> ids = csv.Split(',').Select(str => str);
			int? ONumber = 0;
			int? Bname = 0;
			if (model.QuestionNumber != 0 && model.QuestionNumber != null)
			{
				ONumber = model.QuestionNumber;
			}

			if (model.EventId != 0 && model.EventId != null)
			{
				Bname = model.EventId;
			}
			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.DeptActivePaperId == t.PaperLaidTempId
					  && c.PaperCategoryTypeId == paperCategoryId
					  && ids.Contains(m.DepartmentId)
					  && m.AssemblyId == model.AssemblyId
					   && m.SessionId == model.SessionId
					   && (ONumber == 0 || ONumber == m.PaperLaidId)
					   && (Bname == 0 || Bname == c.EventId)
					   && t.DeptSubmittedBy != null
					   && t.DeptSubmittedDate != null


						 select new PaperMovementModel
						 {
							 DeptActivePaperId = m.DeptActivePaperId,
							 PaperLaidId = m.PaperLaidId,
							 EventName = c.EventName,
							 EventId = c.EventId,
							 Title = m.Title,
							 DeparmentName = m.DeparmentName,
							 MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							 PaperTypeID = (int)m.PaperLaidId,
							 actualFilePath = t.FilePath + t.FileName,
							 PaperCategoryTypeId = c.PaperCategoryTypeId,
							 BusinessType = c.EventName,
							 version = t.Version,
							 evidhanReferenceNumber = t.evidhanReferenceNumber,
							 FilePath = t.FileName,
							 Status = (int)QuestionDashboardStatus.RepliesSent,
							 DeptSubmittedDate = t.DeptSubmittedDate,
							 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							 DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						 }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			if (EventId != null && EventId != 0)
			{

				var filterQuery = query.Where(foo => foo.EventId == EventId);
				return filterQuery.ToList();
			}

			foreach (var item in query)
			{
				var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
				var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
				var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
				item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
				// item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;
				if (item.DesiredLayingDate != null)
				{

					var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
					var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
					var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
					item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;

				}
				else
				{
					var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
					var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
					var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
					item.DesiredLayingDate = "";
				}
				//var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
				//var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
				//var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
				//item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
			}

			return query.ToList();
		}
		static object ShowOtherPaperLaidDetailByID(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel model = param as PaperMovementModel;

			var query = (from tPaper in db.tPaperLaidV
						 where model.PaperLaidId == tPaper.PaperLaidId
						 join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
						 join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
						 join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
						 join tPaperTemp in db.tPaperLaidTemp on tPaper.DeptActivePaperId equals tPaperTemp.PaperLaidTempId
						 join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
						 select new PaperMovementModel
						 {

							 EventName = mEvents.EventName,
							 MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
							 DeparmentName = mDept.deptname,
							 PaperLaidId = tPaper.PaperLaidId,
							 actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
							 ReplyPathFileLocation = tPaperTemp.SignedFilePath,
							 ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
							 FilePath = tPaperTemp.FilePath,
							 Title = tPaper.Title,
							 DocFileName = tPaperTemp.DocFileName,
							 PaperLaidTempId = tPaperTemp.PaperLaidTempId,
							 Description = tPaper.Description,
							 Remark = tPaper.Remark,
							 PaperCategoryTypeId = mEvents.PaperCategoryTypeId,
							 PaperCategoryTypeName = mPaperCategory.Name,
							 PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
							 DesiredDateId = tPaper.DesireLayingDateId,
							 FileVersion = tPaperTemp.Version
						 }).SingleOrDefault();
			if (query.DesiredDateId != 0)
			{
				var DesiredDateModel = (from mSessionDates in db.mSessionDates where query.DesiredDateId == mSessionDates.Id select mSessionDates).FirstOrDefault();
				if (DesiredDateModel == null)
				{
					query.DesireLayingDate = null;
				}
				else
				{
					query.DesireLayingDate = DesiredDateModel.SessionDate;
				}
			}
			model = query;
			model.SubmittedFileListByPaperLaidId = (from tPaperTemp in db.tPaperLaidTemp
													where tPaperTemp.PaperLaidId == query.PaperLaidId && tPaperTemp.DeptSubmittedBy != null && tPaperTemp.DeptSubmittedDate
							!= null
													select tPaperTemp).ToList();


			return model;
		}
		static object GetOtherPaperLaidFileVersion(object param)
		{

			OtherPaperLaidContext db = new OtherPaperLaidContext();
			//PaperMovementModel paper = param as PaperMovementModel;
			tPaperLaidV model = param as tPaperLaidV;
			//tPaperLaidV model = new tPaperLaidV();


			//var FileUpdate = (from tPaperTemp in cnxt.tPaperLaidTemp
			//                  where tPaperTemp.PaperLaidId == model.PaperLaidId
			//               select tPaperTemp.PaperLaidId).Count();

			var FileUpdate = (from paperLaidTemp in db.tPaperLaidTemp
							  where paperLaidTemp.PaperLaidId == model.PaperLaidId
								  && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
							  select paperLaidTemp).FirstOrDefault();


			if (FileUpdate != null)
			{
				if (FileUpdate.Version != null)
				{
					model.Count = Convert.ToInt32(FileUpdate.Version);
				}
				else
				{
					model.Count = 1;
				}
			}
			else
			{
				int count = (from mdl in db.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
				int newVersion = count + 1;
				model.Count = newVersion;
			}


			if (FileUpdate != null)
			{
				if (FileUpdate.DocFileVersion != null)
				{
					model.MainDocCount = Convert.ToInt32(FileUpdate.DocFileVersion);
				}
				else
				{
					model.MainDocCount = 1;
				}
			}
			else
			{
				int MainDocCount = (from mdl in db.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
				int newVersionDoc = MainDocCount + 1;
				model.MainDocCount = newVersionDoc;
			}


			return model;

			//OtherPaperLaidContext db = new OtherPaperLaidContext();
			//PaperMovementModel paper = param as PaperMovementModel;
			//tPaperLaidV model = new tPaperLaidV();

			//model.Count = (from tPaperTemp in db.tPaperLaidTemp
			//               where tPaperTemp.PaperLaidId == paper.PaperLaidId
			//               select tPaperTemp.PaperLaidId).Count();
			//model.Count = model.Count + 1;
			//return model;
		}
		static object UpdateOtherPaperLaidFileByID(object param)
		{
			tPaperLaidTemp model = param as tPaperLaidTemp;

			OtherPaperLaidContext db = new OtherPaperLaidContext();
			if (model == null)
			{
				return null;
			}

			db.tPaperLaidTemp.Add(model);
			db.SaveChanges();

			tPaperLaidV tPaper = new tPaperLaidV();
			tPaper = (from mdl in db.tPaperLaidV where mdl.PaperLaidId == model.PaperLaidId select mdl).SingleOrDefault();
			tPaper.DeptActivePaperId = model.PaperLaidTempId;


			var RefNumber = model.AssemblyId + "/" + model.SessionId + "/" + model.PaperLaidId + "/" + "v" + model.Version;
			model.evidhanReferenceNumber = RefNumber;
			db.SaveChanges();
			db.Close();
			return model;
		}
		static object GetOtherPaperLaidCounters(object param)
		{
			tPaperLaidV model = param as tPaperLaidV;
			model.TotalOtherPaperLaidUpComingLOB = UpcomingLOBByOtherpaperLaidId(model).ResultCount;
			model.TotalOtherPaperLaidInTheHouse = GetOtherPaperLaidInHouseByType(model).ResultCount;
			model.TotalOtherPaperPendingToLay = GetPendingToLayOtherPaperLaidByType(model).ResultCount;

			return model;
		}

		static tPaperLaidV GetPendingToLayOtherPaperLaidByType(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			tPaperLaidV model = param as tPaperLaidV;
			int paperCategoryId = 5;
			int? EventId = model.PaperCategoryTypeId;

			if (model.PageIndex == 0)
			{
				model.PageIndex = 1;
			}
			if (model.PAGE_SIZE == 0)
			{
				model.PAGE_SIZE = 20;
			}

			string csv = model.DepartmentId;
			IEnumerable<string> ids = csv.Split(',').Select(str => str);
			int? ONumber = 0;
			if (model.QuestionNumber != 0 && model.QuestionNumber != null)
			{
				ONumber = model.QuestionNumber;
			}
			int? Bname = 0;
			if (model.EventId != 0 && model.EventId != null)
			{
				Bname = model.EventId;
			}
			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.DeptActivePaperId == t.PaperLaidTempId
						 && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
						 && m.LaidDate == null && m.LOBRecordId != null
						 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null
						 && m.MinisterActivePaperId == t.PaperLaidTempId
						 && c.PaperCategoryTypeId == paperCategoryId
						  && ids.Contains(m.DepartmentId)
						  && m.AssemblyId == model.AssemblyId
						  && m.SessionId == model.SessionId
						  && (ONumber == 0 || ONumber == m.PaperLaidId)
						   && (Bname == 0 || Bname == c.EventId)
						 select new PaperMovementModel
						 {
							 DeptActivePaperId = m.DeptActivePaperId,
							 PaperLaidId = m.PaperLaidId,
							 EventName = c.EventName,
							 EventId = c.EventId,
							 Title = m.Title,
							 DeparmentName = m.DeparmentName,
							 MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							 PaperTypeID = (int)m.PaperLaidId,
							 actualFilePath = t.FilePath + t.FileName,
							 PaperCategoryTypeId = c.PaperCategoryTypeId,
							 BusinessType = c.EventName,
							 version = t.Version,
							 FilePath = t.FileName,
							 DeptSubmittedDate = t.DeptSubmittedDate,
							 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							 DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						 }).OrderBy(m => m.PaperLaidId).ToList();

			int totalRecords = query.Count();
			var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			model.ResultCount = totalRecords;
			model.DepartmentSubmitList = results;

			foreach (var item in query)
			{
				var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
				var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
				var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
				item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;

				var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
				var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
				var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
				item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
			}

			return model;


		}
		static tPaperLaidV GetOtherPaperLaidInHouseByType(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			tPaperLaidV model = param as tPaperLaidV;
			int paperCategoryId = 5;
			int? EventId = model.PaperCategoryTypeId;
			if (model.PageIndex == 0)
			{
				model.PageIndex = 1;
			}
			if (model.PAGE_SIZE == 0)
			{
				model.PAGE_SIZE = 20;
			}

			string csv = model.DepartmentId;
			IEnumerable<string> ids = csv.Split(',').Select(str => str);

			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.DeptActivePaperId == t.PaperLaidTempId && t.DeptSubmittedBy != null && t.DeptSubmittedDate != null
						 && t.MinisterSubmittedBy != null && t.MinisterSubmittedDate != null && m.DesireLayingDate > DateTime.Now && m.LaidDate != null && m.LOBRecordId != null
						 && m.MinisterActivePaperId == t.PaperLaidTempId && c.PaperCategoryTypeId == paperCategoryId
						 && ids.Contains(m.DepartmentId)


						 select new PaperMovementModel
						 {
							 DeptActivePaperId = m.DeptActivePaperId,
							 PaperLaidId = m.PaperLaidId,
							 EventName = c.EventName,
							 EventId = c.EventId,
							 Title = m.Title,
							 DeparmentName = m.DeparmentName,
							 MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							 PaperTypeID = (int)m.PaperLaidId,
							 actualFilePath = t.FilePath + t.FileName,
							 PaperCategoryTypeId = c.PaperCategoryTypeId,
							 BusinessType = c.EventName,
							 version = t.Version,
							 FilePath = t.FileName,
							 DeptSubmittedDate = t.DeptSubmittedDate,
							 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							 DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						 }).OrderBy(m => m.PaperLaidId).ToList();

			int totalRecords = query.Count();
			var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			model.ResultCount = totalRecords;
			model.DepartmentSubmitList = results;


			return model;
		}


		static tPaperLaidV UpcomingLOBByOtherpaperLaidId(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			tPaperLaidV model = param as tPaperLaidV;
			int paperCategoryId = 5;
			int? EventId = model.PaperCategoryTypeId;
			if (model.PageIndex == 0)
			{
				model.PageIndex = 1;
			}
			if (model.PAGE_SIZE == 0)
			{
				model.PAGE_SIZE = 20;
			}

			string csv = model.DepartmentId;
			IEnumerable<string> ids = csv.Split(',').Select(str => str);
			int? ONumber = 0;
			int? Bname = 0;
			if (model.QuestionNumber != 0 && model.QuestionNumber != null)
			{
				ONumber = model.QuestionNumber;
			}

			if (model.EventId != 0 && model.EventId != null)
			{
				Bname = model.EventId;
			}

			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID

						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.DeptActivePaperId == t.PaperLaidTempId
						 && t.DeptSubmittedBy != null
						 && t.DeptSubmittedDate != null
						 && t.MinisterSubmittedBy != null
						 && t.MinisterSubmittedDate != null
						 && m.DesireLayingDate > DateTime.Now
						 && m.LaidDate == null
						 && m.LOBRecordId != null
						 && m.MinisterActivePaperId == t.PaperLaidTempId
						 && c.PaperCategoryTypeId == paperCategoryId
						 && ids.Contains(m.DepartmentId)
						 && m.AssemblyId == model.AssemblyId
						&& m.SessionId == model.SessionId
						&& (ONumber == 0 || ONumber == m.PaperLaidId)
						  && (Bname == 0 || Bname == c.EventId)
						 select new PaperMovementModel
						 {
							 DeptActivePaperId = m.DeptActivePaperId,
							 PaperLaidId = m.PaperLaidId,
							 EventName = c.EventName,
							 EventId = c.EventId,
							 Title = m.Title,
							 DeparmentName = m.DeparmentName,
							 MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
							 PaperTypeID = (int)m.PaperLaidId,
							 actualFilePath = t.FilePath + t.FileName,
							 PaperCategoryTypeId = c.PaperCategoryTypeId,
							 BusinessType = c.EventName,
							 version = t.Version,
							 FilePath = t.FileName,
							 DeptSubmittedDate = t.DeptSubmittedDate,
							 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
							 DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
						 }).OrderBy(m => m.PaperLaidId).ToList();

			int totalRecords = query.Count();
			var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			model.ResultCount = totalRecords;
			model.DepartmentSubmitList = results;


			return model;
		}

		#endregion

		static List<PaperMovementModel> GetSubmittedOtherPaperLaidListCommittee(object param)
		{

			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel model = param as PaperMovementModel;
			int paperCategoryId = 3;  //For Committee
			int? EventId = model.PaperCategoryTypeId;



			string csv = model.DepartmentId;
			IEnumerable<string> ids = csv.Split(',').Select(str => str);
			int? ONumber = 0;
			int? Bname = 0;
			if (model.QuestionNumber != 0 && model.QuestionNumber != null)
			{
				ONumber = model.QuestionNumber;
			}

			if (model.EventId != 0 && model.EventId != null)
			{
				Bname = model.EventId;
			}
			var query = (from m in db.tPaperLaidV
						 join c in db.mEvent on m.EventId equals c.EventId
						 // join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
						 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
						 where m.CommitteeActivePaeperId == t.PaperLaidTempId &&
					 c.PaperCategoryTypeId == paperCategoryId
					  && ids.Contains(m.DepartmentId)
					  && m.AssemblyId == model.AssemblyId
					   && m.SessionId == model.SessionId
					   && (ONumber == 0 || ONumber == m.PaperLaidId)
					   && (Bname == 0 || Bname == c.EventId)
						 && t.CommtSubmittedBy != null
						  && t.CommtSubmittedDate != null
						 select new PaperMovementModel
	 {
		 DeptActivePaperId = m.DeptActivePaperId,
		 PaperLaidId = m.PaperLaidId,
		 EventName = c.EventName,
		 EventId = c.EventId,
		 Title = m.Title,
		 DeparmentName = m.DeparmentName,
		 MinisterName = "",//ministery.MinisterName + ", " + ministery.MinistryName,
		 PaperTypeID = (int)m.PaperLaidId,
		 actualFilePath = t.FilePath + t.FileName,
		 PaperCategoryTypeId = c.PaperCategoryTypeId,
		 BusinessType = c.EventName,
		 version = t.Version,
		 evidhanReferenceNumber = t.evidhanReferenceNumber,
		 FilePath = t.FileName,
		 Status = (int)QuestionDashboardStatus.RepliesSent,
		 DeptSubmittedDate = t.DeptSubmittedDate,
		 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
		 DesireLayingDate = m.DesireLayingDateId == 0 ? (DateTime?)null : (from sd in db.mSessionDates where sd.Id == m.DesireLayingDateId select sd.SessionDate).FirstOrDefault()
	 }).OrderBy(m => m.PaperLaidId).ToList();  //.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

			if (EventId != null && EventId != 0)
			{

				var filterQuery = query.Where(foo => foo.EventId == EventId);
				return filterQuery.ToList();
			}

			foreach (var item in query)
			{
				var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
				var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
				var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
				item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
				// item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;
				if (item.DesiredLayingDate != null)
				{

					var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
					var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
					var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
					item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;

				}
				else
				{
					var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
					var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
					var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
					item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
				}
				//var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
				//var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
				//var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
				//item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
			}

			List<PaperMovementModel> mdlList = new List<PaperMovementModel>();
			foreach (var item in query)
			{
				PaperMovementModel m = new PaperMovementModel();
				m.evidhanReferenceNumber = item.evidhanReferenceNumber;
				m.DeptActivePaperId = item.DeptActivePaperId;
				m.PaperLaidId = item.PaperLaidId;
				m.EventName = item.EventName;
				m.EventId = item.EventId;
				m.Title = item.Title;
				m.DeparmentName = item.DeparmentName;
				m.MinisterName = item.MinisterName;
				m.PaperTypeID = item.PaperTypeID;
				m.actualFilePath = item.actualFilePath;
				m.PaperCategoryTypeId = item.PaperCategoryTypeId;
				m.BusinessType = item.BusinessType;
				m.version = item.version;
				m.FilePath = item.FilePath;
				m.Status = item.Status;
				m.DeptSubmittedDate = item.DeptSubmittedDate;
				m.PaperCategoryTypeName = item.PaperCategoryTypeName;
				m.DesireLayingDate = item.DesireLayingDate;
				mdlList.Add(m);


			}


			return mdlList;// query.ToList();
		}

		static object ShowOtherPaperLaidDetailByIDCommittee(object param)
		{
			OtherPaperLaidContext db = new OtherPaperLaidContext();
			PaperMovementModel model = param as PaperMovementModel;

			var query = (from tPaper in db.tPaperLaidV
						 join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
						 //join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
						 join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
						 join tPaperTemp in db.tPaperLaidTemp on tPaper.CommitteeActivePaeperId equals tPaperTemp.PaperLaidTempId
						 join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
						 where model.PaperLaidId == tPaper.PaperLaidId
						 select new PaperMovementModel
						 {

							 EventName = mEvents.EventName,
							 MinisterName = "", //mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
							 DeparmentName = mDept.deptname,
							 PaperLaidId = tPaper.PaperLaidId,
							 actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
							 ReplyPathFileLocation = tPaperTemp.SignedFilePath,
							 ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
							 FilePath = tPaperTemp.FilePath,
							 Title = tPaper.Title,
							 DocFileName = tPaperTemp.DocFileName,
							 PaperLaidTempId = tPaperTemp.PaperLaidTempId,
							 Description = tPaper.Description,
							 Remark = tPaper.Remark,
							 PaperCategoryTypeId = mEvents.PaperCategoryTypeId,
							 PaperCategoryTypeName = mPaperCategory.Name,
							 PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
							 DesiredDateId = tPaper.DesireLayingDateId,
							 FileVersion = tPaperTemp.Version
						 }).SingleOrDefault();
			if (query.DesiredDateId != 0)
			{
				var DesiredDateModel = (from mSessionDates in db.mSessionDates where query.DesiredDateId == mSessionDates.Id select mSessionDates).FirstOrDefault();
				if (DesiredDateModel == null)
				{
					query.DesireLayingDate = null;
				}
				else
				{
					query.DesireLayingDate = DesiredDateModel.SessionDate;
				}
			}
			model = query;
			model.SubmittedFileListByPaperLaidId = (from tPaperTemp in db.tPaperLaidTemp
													where tPaperTemp.PaperLaidId == query.PaperLaidId && tPaperTemp.CommtSubmittedBy != null && tPaperTemp.CommtSubmittedDate != null
													select tPaperTemp).ToList();


			return model;
		}

		static object GetOtherPaperLaidFileVersionCommittee(object param)
		{

			OtherPaperLaidContext db = new OtherPaperLaidContext();

			tPaperLaidV model = param as tPaperLaidV;


			 
			int count = (from mdl in db.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
			int newVersion = count + 1;
			model.Count = newVersion;
			 

			return model;


		}
	}
}
