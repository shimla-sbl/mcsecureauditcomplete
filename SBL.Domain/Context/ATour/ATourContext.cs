﻿using SBL.DAL;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.Tour;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
 

namespace SBL.Domain.Context.ATour
{
	public class ATourContext : DBBase<ATourContext>
	{
		public ATourContext()
			: base("eVidhan")
		{
		}
		public DbSet<tMemberTour> tMemberTour { get; set; }
		public virtual DbSet<mMember> Member { get; set; }
		public DbSet<RecipientGroup> RecipientGroups { get; set; }

		public static object Execute(ServiceParameter param)
		{
			if (param != null)
			{
				switch (param.Method)
				{
					case "AddTour": { return AddTour(param.Parameter); }
					case "GetAllTour": { return GetAllTour(param.Parameter); }
					case "GetTodayTour": { return GetTodayTour(param.Parameter); }
					case "GetUpcomingTour": { return GetUpcomingTour(param.Parameter); }
					case "GetAllMemberTour": { return GetAllMemberTour(param.Parameter); }
					case "GetTodayMemberTour": { return GetTodayMemberTour(param.Parameter); }
					case "GetUpcomingMemberTour": { return GetUpcomingMemberTour(param.Parameter); }
					case "GetTourById": { return GetTourById(param.Parameter); }
					case "UpdateTour": { return UpdateTour(param.Parameter); }
					case "DeleteTour": { return DeleteTour(param.Parameter); }
					case "Publishtour": { return Publishtour(param.Parameter); }
					case "GetContactGroupsByGroupIDs": { return GetContactGroupsByGroupIDs(param.Parameter); }
					case "GetUnpublishedMemberTour": { return GetUnpublishedMemberTour(param.Parameter); }
					case "GetPreviousMemberTour": { return GetPreviousMemberTour(param.Parameter); }
					case "GetAllUpdatedTourCounts": { return GetAllUpdatedTourCounts(param.Parameter); }
					 
				}
			}

			return null;
		}

	 

		#region TourCode

		private static object GetAllUpdatedTourCounts(object p)
		{
			tMemberTour tour = p as tMemberTour;

			try
			{
				ATourContext db = new ATourContext();



				DateTime datetime = DateTime.Now.Date;

				var TodayTour = (from AT in db.tMemberTour
								 where (AT.MemberId == tour.MemberId)
								 && (AT.TourFromDateTime == datetime && AT.TourToDateTime == datetime)
								 && AT.IsPublished == true
								 select AT.TourId).Count();

				var UpComingTour = (from AT in db.tMemberTour
									where (AT.MemberId == tour.MemberId)
									&& (AT.TourFromDateTime > datetime || AT.TourToDateTime > datetime)
									&& AT.IsPublished == true
									select AT.TourId).Count();

				var PreviousTour = (from AT in db.tMemberTour
									where (AT.MemberId == tour.MemberId)
									&& (AT.TourFromDateTime < datetime || AT.TourToDateTime < datetime)
									&& AT.IsPublished == true
									select AT.TourId).Count();

				var UnpublisedTour = (from AT in db.tMemberTour
									  where (AT.MemberId == tour.MemberId)
									  && AT.IsPublished == false
									  select AT.TourId).Count();

				tour.TodayToursUpdatedCnt = TodayTour;

				tour.UpcomingToursUpdatedCnt = UpComingTour;

				tour.PreviousToursUpdatedCnt = PreviousTour;

				tour.UnPublishedToursUpdatedCnt = UnpublisedTour;

				return tour;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object DeleteTour(object p)
		{
			try
			{
				tMemberTour DataTodelete = p as tMemberTour;
				int id = DataTodelete.TourId;
				using (ATourContext db = new ATourContext())
				{
					var TourToDelete = db.tMemberTour.SingleOrDefault(a => a.TourId == id);
					db.tMemberTour.Remove(TourToDelete);
					db.SaveChanges();
					db.Close();
				}

				return "Tour deleted successfully";
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object Publishtour(object p)
		{
			try
			{
				List<tMemberTour> tourList = p as List<tMemberTour>;

				using (ATourContext db = new ATourContext())
				{

					foreach (tMemberTour tour in tourList)
					{
						var data = (from a in db.tMemberTour
									where a.TourId == tour.TourId
									select a).FirstOrDefault();

						data.IsPublished = true;

						db.Entry(data).State = EntityState.Modified;

						db.SaveChanges();
					}

					db.Close();
				}

				return "Tours published successfully";
			}
			catch (Exception ex)
			{
				return "Tours publishing failed";
			}
		}

		private static object UpdateTour(object p)
		{
			try
			{
				tMemberTour UpdateTour = p as tMemberTour;
				using (ATourContext db = new ATourContext())
				{
					var data = (from a in db.tMemberTour where UpdateTour.TourId == a.TourId select a).FirstOrDefault();

					data.ModifiedDate = DateTime.Now;
					data.TourDescrption = UpdateTour.TourDescrption;
					data.LocalTourTitle = UpdateTour.LocalTourTitle;
					data.LocalTourDescription = UpdateTour.LocalTourDescription;
					data.TourTitle = UpdateTour.TourTitle;
					data.TourToDateTime = UpdateTour.TourToDateTime;
					data.TourFromDateTime = UpdateTour.TourFromDateTime;
					//data.TourLocation = UpdateTour.TourLocation;
					data.Attachement = UpdateTour.Attachement;
					data.AliasAttachment = UpdateTour.AliasAttachment;
					data.ModifiedBy = UpdateTour.ModifiedBy;
					data.mode = UpdateTour.mode;
					data.Purpose = UpdateTour.Purpose;
					data.Arrival = UpdateTour.Arrival;
					data.Departure = UpdateTour.Departure;
					data.ArrivalTime = UpdateTour.ArrivalTime;
					data.DepartureTime = UpdateTour.DepartureTime;
					db.tMemberTour.Attach(data);
					db.Entry(data).State = EntityState.Modified;
					db.SaveChanges();

					db.Close();
				}
				return UpdateTour;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetTourById(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					tMemberTour TourToEdit = p as tMemberTour;

					var data = db.tMemberTour.SingleOrDefault(o => o.TourId == TourToEdit.TourId);
					return data;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetAllTour(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					var data = (from a in db.tMemberTour
								join member in db.Member
								on a.MemberId equals member.MemberCode
								select new
								{
									Tour = a,
									MemberName = member.Prefix + " " + member.Name
								}).ToList();

					List<tMemberTour> result = new List<tMemberTour>();

					foreach (var a in data)
					{
						tMemberTour partdata = new tMemberTour();
						partdata = a.Tour;
						partdata.MemberName = a.MemberName;

						result.Add(partdata);
					}

					return result;

				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetTodayTour(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					DateTime datetime = DateTime.Now.Date;
					var data = (from a in db.tMemberTour
								join member in db.Member
								on a.MemberId equals member.MemberCode
								where (a.TourFromDateTime <= datetime && a.TourToDateTime >= datetime)
								&& a.IsPublished == true
								select new
								{
									Tour = a,
									MemberName = member.Prefix + " " + member.Name
								}).ToList().OrderByDescending(x => x.Tour.TourFromDateTime);


					List<tMemberTour> result = new List<tMemberTour>();

					foreach (var a in data)
					{
						tMemberTour partdata = new tMemberTour();
						partdata = a.Tour;
						partdata.MemberName = a.MemberName;

						result.Add(partdata);
					}

					return result;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetUpcomingTour(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					DateTime datetime = DateTime.Now.Date;
					var data = (from a in db.tMemberTour
								join member in db.Member
								on a.MemberId equals member.MemberCode
								where (a.TourFromDateTime > datetime || a.TourToDateTime > datetime)
								&& a.IsPublished == true
								select new
								{
									Tour = a,
									MemberName = member.Prefix + " " + member.Name
								}).ToList().OrderByDescending(x => x.Tour.TourFromDateTime);


					List<tMemberTour> result = new List<tMemberTour>();

					foreach (var a in data)
					{
						tMemberTour partdata = new tMemberTour();
						partdata = a.Tour;
						partdata.MemberName = a.MemberName;

						result.Add(partdata);
					}

					return result;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetAllMemberTour(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					tMemberTour membertour = p as tMemberTour;
					var data = (from a in db.tMemberTour
								join member in db.Member
								on a.MemberId equals member.MemberCode
								where a.MemberId == membertour.MemberId
								select new
								{
									Tour = a,
									MemberName = member.Prefix + " " + member.Name
								}).ToList().OrderBy(x => x.Tour.TourFromDateTime);

					List<tMemberTour> result = new List<tMemberTour>();

					foreach (var a in data)
					{
						tMemberTour partdata = new tMemberTour();
						partdata = a.Tour;
						partdata.MemberName = a.MemberName;

						result.Add(partdata);
					}

					return result;

				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetPreviousMemberTour(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					tMemberTour membertour = p as tMemberTour;
					DateTime datetime = DateTime.Now.Date;
					var data = (from a in db.tMemberTour
								join member in db.Member
								on a.MemberId equals member.MemberCode
								where (a.TourFromDateTime < datetime)
								&& a.MemberId == membertour.MemberId && a.IsPublished == true
								select new
								{
									Tour = a,
									MemberName = member.Prefix + " " + member.Name
								}).ToList().OrderBy(x => x.Tour.TourFromDateTime);

					List<tMemberTour> result = new List<tMemberTour>();

					foreach (var a in data)
					{
						tMemberTour partdata = new tMemberTour();
						partdata = a.Tour;
						partdata.MemberName = a.MemberName;

						result.Add(partdata);
					}

					return result;

				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetTodayMemberTour(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					tMemberTour membertour = p as tMemberTour;
					DateTime datetime = DateTime.Now.Date;
					var data = (from a in db.tMemberTour
								join member in db.Member
								on a.MemberId equals member.MemberCode
								where (a.TourFromDateTime == datetime)
								&& a.MemberId == membertour.MemberId && a.IsPublished == true
								select new
								{
									Tour = a,
									MemberName = member.Prefix + " " + member.Name
								}).ToList().OrderBy(x => x.Tour.TourFromDateTime);


					List<tMemberTour> result = new List<tMemberTour>();

					foreach (var a in data)
					{
						tMemberTour partdata = new tMemberTour();
						partdata = a.Tour;
						partdata.MemberName = a.MemberName;

						result.Add(partdata);
					}

					return result;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetUpcomingMemberTour(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					tMemberTour memberTour = p as tMemberTour;
					DateTime datetime = DateTime.Now.Date;
					var data = (from a in db.tMemberTour
								join member in db.Member
								on a.MemberId equals member.MemberCode
								where (a.TourFromDateTime > datetime) && (a.MemberId == memberTour.MemberId)
								&& a.IsPublished == true
								select new
								{
									Tour = a,
									MemberName = member.Prefix + " " + member.Name
								}).ToList().OrderBy(x => x.Tour.TourFromDateTime);


					List<tMemberTour> result = new List<tMemberTour>();

					foreach (var a in data)
					{
						tMemberTour partdata = new tMemberTour();
						partdata = a.Tour;
						partdata.MemberName = a.MemberName;

						result.Add(partdata);
					}

					return result;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object AddTour(object p)
		{
			try
			{
				using (ATourContext newsContext = new ATourContext())
				{
					tMemberTour news = p as tMemberTour;

					news.CreatedDate = DateTime.Now;
					news.ModifiedDate = DateTime.Now;

					newsContext.tMemberTour.Add(news);
					newsContext.SaveChanges();

					newsContext.Close();
				}

				return null;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		private static object GetUnpublishedMemberTour(object p)
		{
			try
			{
				using (ATourContext db = new ATourContext())
				{
					tMemberTour memberTour = p as tMemberTour;
					DateTime datetime = DateTime.Now.Date;
					var data = (from a in db.tMemberTour
								join member in db.Member
								on a.MemberId equals member.MemberCode
								where (a.MemberId == memberTour.MemberId)
								&& a.IsPublished == false
								select new
								{
									Tour = a,
									MemberName = member.Prefix + " " + member.Name
								}).ToList().OrderBy(x => x.Tour.TourFromDateTime);

					List<tMemberTour> result = new List<tMemberTour>();

					foreach (var a in data)
					{
						tMemberTour partdata = new tMemberTour();
						partdata = a.Tour;
						partdata.MemberName = a.MemberName;

						result.Add(partdata);
					}

					return result;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private static object GetContactGroupsByGroupIDs(object param)
		{
			RecipientGroupMember multipleIDs = param as RecipientGroupMember;

			string[] values = multipleIDs.AssociatedContactGroups.Split(',');

			List<int> groupIds = new List<int>();

			for (int i = 0; i < values.Length; i++)
			{
				groupIds.Add(int.Parse(values[i]));
			}

			using (ATourContext context = new ATourContext())
			{
				var query = (from recipientGroups in context.RecipientGroups
							 where recipientGroups.IsActive == true
							 && groupIds.Contains(recipientGroups.GroupID)
							 select recipientGroups);
				return query.ToList();
			}
		}

		#endregion
	}
}
