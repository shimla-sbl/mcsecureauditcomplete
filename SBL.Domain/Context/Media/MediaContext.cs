﻿namespace SBL.Domain.Context.Media
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SBL.DAL;
    using System.Data.Entity;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Media;

    using SBL.DomainModel.ComplexModel.MediaComplexModel;
    using SBL.Domain.Context.Assembly;
    using SBL.Domain.Context.Session;
    using SBL.Domain.Context.SiteSetting;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Session;
    using SBL.DomainModel.Models.SiteSetting;

    using System.Data;

    #endregion

    public class MediaContext : DBBase<MediaContext>
    {
        public MediaContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<JournalistsForPass> JournalistsForPass { get; set; }
        public DbSet<Journalist> Journalists { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<mDepartment> Departments { get; set; }
        public DbSet<tMediaYears> tMediaYear { get; set; }
        public virtual DbSet<SiteSettings> mSiteSettings { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mSession> mSession { get; set; }
        //public DbSet<PassRequest> passRequest { get; set; }


        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                #region DashBoard
                case "GetDashBoardValue": { return GetDashBoardValue(param.Parameter); }

                #endregion

                #region Journalists
                case "GetAllJournalists": { return GetAllJournalists(); }
                case "AddJournalists": { return AddJournalists(param.Parameter); break; }
                case "GetJournalistsById": { return GetJournalistsById(param.Parameter); }
                case "UpdateJournalists": { return UpdateJournalists(param.Parameter); }
                case "DeleteJournalists": { DeleteJournalists(param.Parameter); break; }
                case "ChangeJournlistStatus": { ChangeJournlistStatus(param.Parameter); break; }
                case "GetJournalistsCommittieeMembers": { return GetJournalistsCommittieeMembers(param.Parameter); }
                case "AddToJournalistForYear": { AddToJournalistForYear(param.Parameter); break; }
                case "RemoveFromJournalistCommitteeList": { RemoveFromJournalistCommitteeList(param.Parameter); break; }

                case "GetAllAcreditedJournalists": { return GetAllAcreditedJournalists(param.Parameter); }
                case "GetAllAcreditedJournalists1": { return GetAllAcreditedJournalists1(param.Parameter); }
                case "GetAllNonAcreditedJournalists": { return GetAllNonAcreditedJournalists(param.Parameter); }
                case "GetAllNonAcreditedJournalists1": { return GetAllNonAcreditedJournalists1(param.Parameter); }
                case "GetMediaByYear": { return GetMediaByYear(param.Parameter); }
                case "AddJournalistToCurrentSession": { return AddJournalistToCurrentSession(param.Parameter); }
                case "GetRequestedMediaByYear": { return GetRequestedMediaByYear(param.Parameter); }
                case "GetJournalistsForPassByID": { return GetJournalistsForPassByID(param.Parameter); }
                case "GetAllJournalistsForPassByIdSession": { return GetAllJournalistsForPassByIdSession(param.Parameter); }

                #endregion

                #region Organizations
                case "GetAllOrganizations": { return GetAllOrganizations(); }
                case "AddOrganization": { AddOrganization(param.Parameter); break; }
                case "GetOrganizationById": { return GetOrganizationById(param.Parameter); }
                case "UpdateOrganization": { return UpdateOrganization(param.Parameter); }
                case "DeleteOrganization": { DeleteOrganization(param.Parameter); break; }
                case "ChangeOrganizationStatus": { ChangeOrganizationStatus(param.Parameter); break; }

                #endregion

                #region PassRequest
                //case "AddPassrequest": { AddPassrequest(param.Parameter); break; }
                case "GetJournalistsForPassByIdCurrentYear":
                    {
                        return GetJournalistsForPassByIdCurrentYear(param.Parameter);
                    }
                case "GetJournalistsForPassByIdCurrentSession":
                    {
                        return GetJournalistsForPassByIdCurrentSession(param.Parameter);
                    }

                case "SubmitJournalistToCurrentYear":
                    {
                        return SubmitJournalistToCurrentYear(param.Parameter);
                    }
                case "GetAllJournalistsForPassByIdYear":
                    {
                        return GetAllJournalistsForPassByIdYear(param.Parameter);
                    }
                case "SubmitPassRequest":
                    {
                        return SubmitPassRequest(param.Parameter);
                    }
                case "GetJournalistsForPassByIdandYear":
                    {
                        return GetJournalistsForPassByIdandYear(param.Parameter);
                    }
                case "GetJournalistsForPassByIdandSession":
                    {
                        return GetJournalistsForPassByIdandSession(param.Parameter);
                    }
                case "RemoveJournalistFormCurrentYear":
                    {
                        return RemoveJournalistFormCurrentYear(param.Parameter);
                    }
                case "GetJournalistsByAadharId":
                    {
                        return GetJournalistsByAadharId(param.Parameter);
                    }



                #endregion
            }
            return null;
        }


        #region DashBoard AssemblyList and SessionList
        static object GetDashBoardValue(object param)
        {

            //tPaperLaidV model = param as tPaperLaidV;
            MediaModel model = param as MediaModel;
            MediaContext pCtxt = new MediaContext();

            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();
            if (model.AssemblyId == 0 && model.SessionId == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionId = Convert.ToInt16(SessionCode);
                model.AssemblyId = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionId && m.AssemblyID == model.AssemblyId select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyId select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssemblyName = AssemblyName;


            model.mAssemblyList = (from A in pCtxt.mAssembly
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSession
                                 where S.AssemblyID == model.AssemblyId
                                 select S).ToList();
            return model;


        }
        #endregion

        #region Journalists

        static void ChangeJournlistStatus(object param)
        {
            Journalist Journalist = param as Journalist;
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    var JournalistToUpdate = db.Journalists.SingleOrDefault(a => a.JournalistID == Journalist.JournalistID);

                    bool IsActive = (JournalistToUpdate.IsActive) ? false : true;
                    JournalistToUpdate.IsActive = IsActive;
                    db.Journalists.Attach(JournalistToUpdate);
                    db.Entry(JournalistToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }

        }

        static object GetAllJournalists()
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {

                    var data = db.Journalists.Include("Organization").OrderBy(a => a.Name);
                    return data.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetAllAcreditedJournalists(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    var data = db.Journalists.Include("Organization").OrderBy(a => a.JournalistID).Where(a => a.IsAcredited == true && (a.IsDeleted == null || a.IsDeleted == false)).ToList();
                    //var status = false;
                    // int sessionCode = (int)param;
                    JournalistsForPass model = param as JournalistsForPass;
                    foreach (var item in data)
                    {
                        var Journalist = GetJournalistsForPassByIdCurrentSessonValue(item.JournalistID, (int)model.SessionCode, (int)model.AssemblyCode) as List<JournalistsForPass>;
                        if (Journalist.Count > 0)
                        {
                            item.IsRequested = true;
                            //if (Journalist.Count > 0) { status = true; }
                        }
                        else
                        {
                            item.IsRequested = false;
                        }


                    }
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }


        static object GetAllAcreditedJournalists1(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    var data = db.Journalists.Include("Organization").OrderByDescending(a => a.JournalistID).Where(a => a.IsAcredited == true && (a.IsDeleted == null || a.IsDeleted == false)).ToList();
                    var siteSettingData = (from a in db.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
                    // int sessionCode = (int)param;
                    //List<Journalist> result = new List<Journalist>();
                    JournalistsForPass model = param as JournalistsForPass;
                    foreach (var item in data)
                    {
                        //Journalist partdata = new Journalist();


                        item.ImageAcessurl = siteSettingData.SettingValue + item.Photo + "/Thumb/" + item.FileName;
                        //result.Add(partdata);

                        var Journalist = GetJournalistsForPassByIdCurrentSessonValue(item.JournalistID, (int)model.SessionCode, (int)model.AssemblyCode) as List<JournalistsForPass>;
                        if (Journalist.Count > 0)
                        {
                            item.IsRequested = true;

                        }
                        else
                        {
                            item.IsRequested = false;
                        }


                    }
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }






        static object GetAllNonAcreditedJournalists(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {

                    var data = db.Journalists.Include("Organization").OrderBy(a => a.JournalistID).Where(a => a.IsAcredited == false && (a.IsDeleted == null || a.IsDeleted == false)).ToList();
                    //return data.ToList();
                    JournalistsForPass model = param as JournalistsForPass;
                    //int sessionCode = (int)param;
                    foreach (var item in data)
                    {
                        var Journalist = GetJournalistsForPassByIdCurrentSessonValue(item.JournalistID, (int)model.SessionCode, (int)model.AssemblyCode) as List<JournalistsForPass>;
                        if (Journalist.Count > 0)
                        {
                            item.IsRequested = true;
                            //if (Journalist.Count > 0) { status = true; }
                        }
                        else
                        {
                            item.IsRequested = false;
                        }


                    }
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }



        static object GetAllNonAcreditedJournalists1(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    var data = db.Journalists.Include("Organization").OrderByDescending(a => a.JournalistID).Where(a => a.IsAcredited == false && (a.IsDeleted == null || a.IsDeleted == false)).ToList();
                    var siteSettingData = (from a in db.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
                    JournalistsForPass model = param as JournalistsForPass;
                    //int sessionCode = (int)param;
                    //List<Journalist> result = new List<Journalist>();
                    foreach (var item in data)
                    {
                        //Journalist partdata = new Journalist();


                        item.ImageAcessurl = siteSettingData.SettingValue + item.Photo + "/Thumb/" + item.FileName;
                        //result.Add(partdata);

                        var Journalist = GetJournalistsForPassByIdCurrentSessonValue(item.JournalistID, (int)model.SessionCode, (int)model.AssemblyCode) as List<JournalistsForPass>;
                        if (Journalist.Count > 0)
                        {
                            item.IsRequested = true;
                            //if (Journalist.Count > 0) { status = true; }
                        }
                        else
                        {
                            item.IsRequested = false;
                        }

                    }
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }





        static object GetJournalistsCommittieeMembers(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    Journalist Journalist = param as Journalist;

                    return null;
                }
            }
            catch
            {
                throw;
            }
        }

        static object AddJournalists(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    Journalist Journalist = param as Journalist;
                    db.Journalists.Add(Journalist);
                    db.SaveChanges();
                    db.Close();
                    return Journalist.JournalistID;
                }

            }
            catch
            {
                throw;
            }
        }

        static object GetJournalistsById(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    Journalist JournalistToEdit = param as Journalist;
                    var data = db.Journalists.Include("Organization").SingleOrDefault(o => o.JournalistID == JournalistToEdit.JournalistID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }




        static object GetJournalistsByAadharId(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    Journalist JournalistToEdit = param as Journalist;
                    var data = db.Journalists.Include("Organization").FirstOrDefault(o => o.UID == JournalistToEdit.UID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static object UpdateJournalists(object param)
        {
            try
            {
                Journalist UpdateJournalist = param as Journalist;
                using (MediaContext db = new MediaContext())
                {

                    // UpdateJournalist.FileName = updateJournalist.FileName;
                    //  UpdateJournalist.Photo = updateJournalist.Photo;
                    // UpdateJournalist.ThumbName = updateJournalist.ThumbName;

                    db.Journalists.Attach(UpdateJournalist);
                    db.Entry(UpdateJournalist).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateJournalist;
            }
            catch
            {
                throw;
            }
        }

        static void DeleteJournalists(object param)
        {
            try
            {
                //Journalist DataTodelete = param as Journalist;
                //int id = DataTodelete.JournalistID;
                //using (MediaContext db = new MediaContext())
                //{


                //    var JournalistToDelete = db.Journalists.SingleOrDefault(a => a.JournalistID == id);
                //    db.Journalists.Remove(JournalistToDelete);
                //    db.SaveChanges();
                //    db.Close();
                //}

                Journalist DataTodelete = param as Journalist;
                int id = DataTodelete.JournalistID;
                using (var obj = new MediaContext())
                {
                    var PaperLaidObj = obj.Journalists.Where(m => m.JournalistID == id).FirstOrDefault();
                    if (PaperLaidObj != null)
                    {
                        PaperLaidObj.IsDeleted = true;

                    }
                    obj.SaveChanges();
                    obj.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static void AddToJournalistForYear(object param)
        {
            List<JournalistsForPass> JournalistsToProcess = param as List<JournalistsForPass>;
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    foreach (var item in JournalistsToProcess)
                    {
                        var JournalistItem = new JournalistsForPass { IsCommitteMember = false, JournalistId = item.JournalistId, Year = item.Year };

                        if (db.JournalistsForPass.Where(a => a.JournalistId != JournalistItem.JournalistId && a.Year != JournalistItem.Year).Count() > 0)
                        {
                            db.JournalistsForPass.Add(JournalistItem);
                        }

                    }
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {

                throw;
            }
        }

        static void RemoveFromJournalistCommitteeList(object param)
        {
            Journalist JournalistToProcess = param as Journalist;
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    var JournalistToUpdate = db.JournalistsForPass.SingleOrDefault(a => a.JournalistId == JournalistToProcess.JournalistID);
                    //  JournalistToUpdate.IsCommitteMember = false;
                    db.JournalistsForPass.Attach(JournalistToUpdate);
                    db.Entry(JournalistToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        //Not Using Now
        static void RemoveFromSelectedJournalistCommitteeList(object param)
        {
            List<Journalist> JournalistsToProcess = param as List<Journalist>;
            try
            {
                Journalist Journalist = param as Journalist;
                using (MediaContext db = new MediaContext())
                {
                    foreach (var item in JournalistsToProcess)
                    {
                        var JournalistToUpdate = db.JournalistsForPass.SingleOrDefault(a => a.JournalistId == item.JournalistID);
                        JournalistToUpdate.IsCommitteMember = false;
                        db.JournalistsForPass.Attach(JournalistToUpdate);
                        db.Entry(JournalistToUpdate).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }



        public static object GetMediaByYear1(object param)
        {
            tMediaYears model = (tMediaYears)param;
            MediaContext db = new MediaContext();

            var result = (from mediaYear in db.tMediaYear where mediaYear.Year == model.Year select mediaYear).ToList();
            return result;
        }

        public static object GetMediaByYear(object param)
        {
            JournalistsForPass model = (JournalistsForPass)param;
            MediaContext db = new MediaContext();

            var result = (from journalistsForPass in db.JournalistsForPass
                          join journalist in db.Journalists on journalistsForPass.JournalistId equals journalist.JournalistID
                          where journalistsForPass.SessionCode == model.SessionCode && journalistsForPass.AssemblyCode == model.AssemblyCode
                          select journalist).Include("Organization").OrderBy(a => a.Name).ToList();

            foreach (var item in result)
            {
                var jForYear = GetAllJournalistsForPassByIdSession(new JournalistsForPass { JournalistId = item.JournalistID, SessionCode = model.SessionCode, AssemblyCode = model.AssemblyCode }) as JournalistsForPass;
                if (jForYear != null)
                {
                    item.RequestId = jForYear.JournalistsForPassId;
                }
                else
                {
                    item.RequestId = 0;
                }

                var journalistIsRequested = GetJournalistsForPassByIdandSession(new JournalistsForPass { JournalistId = item.JournalistID, SessionCode = model.SessionCode, AssemblyCode = model.AssemblyCode, IsRequested = true }) as List<JournalistsForPass>;
                if (journalistIsRequested != null)
                {
                    if (journalistIsRequested.Count > 0) { item.IsRequested = true; }
                }
                else { item.IsRequested = false; }
            }

            return result;
        }

        public static object GetRequestedMediaByYear(object param)
        {
            JournalistsForPass model = (JournalistsForPass)param;
            MediaContext db = new MediaContext();

            var result = (from journalistsForPass in db.JournalistsForPass
                          join journalist in db.Journalists on journalistsForPass.JournalistId equals journalist.JournalistID
                          where journalistsForPass.SessionCode == model.SessionCode && journalistsForPass.AssemblyCode == model.AssemblyCode && journalistsForPass.IsRequested == true
                          && journalist.IsActive == true
                          select journalist).Include("Organization").OrderBy(a => a.Name).ToList();

            //GetAllJournalistsForPassByIdYear
            foreach (var item in result)
            {
                var resultReturned = GetAllJournalistsForPassByIdSession(new JournalistsForPass { JournalistId = item.JournalistID, SessionCode = model.SessionCode, AssemblyCode = model.AssemblyCode }) as JournalistsForPass;

                bool status = false;
                if (resultReturned != null)
                {
                    if (resultReturned.AssemblyCode != null && resultReturned.SessionCode != null)
                    {
                        status = model.AssemblyCode == resultReturned.AssemblyCode;
                        status = model.SessionCode == resultReturned.SessionCode;
                    }
                }
                item.IsInCurrentSession = resultReturned.IsJournalistIdForApproval;
            }

            return result;
        }



        static object GetJournalistsForPassByIdandYear(object param)
        {
            JournalistsForPass model = (JournalistsForPass)param;
            MediaContext db = new MediaContext();

            var Result = (from jornalistforpass in db.JournalistsForPass where jornalistforpass.JournalistId == model.JournalistId && jornalistforpass.Year == model.Year && jornalistforpass.IsRequested == true select jornalistforpass).ToList();
            return Result;
        }
        static object GetJournalistsForPassByIdandSession(object param)
        {
            JournalistsForPass model = (JournalistsForPass)param;
            MediaContext db = new MediaContext();

            var Result = (from jornalistforpass in db.JournalistsForPass where jornalistforpass.JournalistId == model.JournalistId && jornalistforpass.SessionCode == model.SessionCode && jornalistforpass.AssemblyCode == model.AssemblyCode && jornalistforpass.IsRequested == true select jornalistforpass).ToList();
            return Result;
        }
        #endregion

        #region Organizations
        static void ChangeOrganizationStatus(object param)
        {
            Organization Organization = param as Organization;
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    var OrganizationToUpdate = db.Organizations.SingleOrDefault(a => a.OrganizationID == Organization.OrganizationID);

                    bool IsActive = (OrganizationToUpdate.IsActive) ? false : true;
                    OrganizationToUpdate.IsActive = IsActive;
                    db.Organizations.Attach(OrganizationToUpdate);
                    db.Entry(OrganizationToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }

        }
        static object GetAllOrganizations()
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    //Organization Organization = param as Organization;
                    var data = db.Organizations.OrderBy(a => a.Name).ToList();
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }
        static object GetAllActiveOrganizations(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    Organization Organization = param as Organization;
                    var data = db.Organizations.Where(o => o.IsActive == true).OrderBy(q => q.Name).ToList();
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }
        static void AddOrganization(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    Organization Organization = param as Organization;
                    Organization.IsActive = true;
                    db.Organizations.Add(Organization);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }

        }
        static object GetOrganizationById(object param)
        {
            try
            {
                using (MediaContext db = new MediaContext())
                {
                    Organization OrganizationToEdit = param as Organization;

                    var data = db.Organizations.SingleOrDefault(o => o.OrganizationID == OrganizationToEdit.OrganizationID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }
        static object UpdateOrganization(object param)
        {
            try
            {
                Organization UpdateOrganization = param as Organization;
                using (MediaContext db = new MediaContext())
                {
                    db.Organizations.Attach(UpdateOrganization);
                    db.Entry(UpdateOrganization).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateOrganization;
            }
            catch
            {
                throw;
            }
        }
        static void DeleteOrganization(object param)
        {
            try
            {
                Organization DataTodelete = param as Organization;
                int id = DataTodelete.OrganizationID;
                using (MediaContext db = new MediaContext())
                {
                    var OrganisationToDelete = db.Organizations.SingleOrDefault(a => a.OrganizationID == id);
                    db.Organizations.Remove(OrganisationToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }



        #endregion


        static object GetJournalistsForPassByIdCurrentYear(object param)
        {
            MediaContext db = new MediaContext();
            int Id = (int)param;
            string year = Convert.ToString(DateTime.Now.Year);

            var Result = (from jornalistforpass in db.JournalistsForPass where jornalistforpass.JournalistId == Id && jornalistforpass.Year == year select jornalistforpass).ToList();
            return Result;
        }

        static object GetJournalistsForPassByIdCurrentSession(object param)
        {
            MediaContext db = new MediaContext();
            JournalistsForPass model = param as JournalistsForPass;
            int Id = model.JournalistId;
            int sessionID = (int)model.SessionCode;
            var Result = (from jornalistforpass in db.JournalistsForPass where jornalistforpass.JournalistId == Id && jornalistforpass.SessionCode == sessionID select jornalistforpass).ToList();
            return Result;
        }
        static object GetJournalistsForPassByIdCurrentSessonValue(int JournalistId, int sessionCode, int AssemblyCode)
        {
            MediaContext db = new MediaContext();
            int Id = (int)JournalistId;
            int sessionID = (int)sessionCode;
            int AssemblyID = (int)AssemblyCode;
            var Result = (from jornalistforpass in db.JournalistsForPass where jornalistforpass.JournalistId == Id && jornalistforpass.SessionCode == sessionID && jornalistforpass.AssemblyCode == AssemblyID select jornalistforpass).ToList();
            return Result;
        }
        static object GetJournalistsForPassByID(object param)
        {
            MediaContext db = new MediaContext();
            JournalistsForPass model = (JournalistsForPass)param;

            var Result = (from jornalistforpass in db.JournalistsForPass where jornalistforpass.JournalistsForPassId == model.JournalistsForPassId && jornalistforpass.AssemblyCode == model.AssemblyCode && jornalistforpass.SessionCode == model.SessionCode select jornalistforpass).ToList();
            return Result;
        }

        static object GetAllJournalistsForPassByIdYear(object param)
        {
            MediaContext db = new MediaContext();
            JournalistsForPass model = (JournalistsForPass)param;

            var Result = (from jornalistforpass in db.JournalistsForPass where jornalistforpass.JournalistId == model.JournalistId && jornalistforpass.Year == model.Year select jornalistforpass).FirstOrDefault();
            return Result;
        }
        static object GetAllJournalistsForPassByIdSession(object param)
        {
            MediaContext db = new MediaContext();
            JournalistsForPass model = (JournalistsForPass)param;

            var Result = (from jornalistforpass in db.JournalistsForPass where jornalistforpass.JournalistId == model.JournalistId && jornalistforpass.SessionCode == model.SessionCode && jornalistforpass.AssemblyCode == model.AssemblyCode select jornalistforpass).FirstOrDefault();
            return Result;
        }

        static object SubmitJournalistToCurrentYear(object param)
        {
            MediaContext db = new MediaContext();
            JournalistsForPass model = (JournalistsForPass)param;
            db.JournalistsForPass.Add(model);
            db.SaveChanges();
            return null;
        }


        static object SubmitPassRequest(object param)
        {
            JournalistsForPass model = (JournalistsForPass)param;
            using (var obj = new MediaContext())
            {
                var PaperLaidObj = obj.JournalistsForPass.Where(m => m.JournalistsForPassId == model.JournalistsForPassId).FirstOrDefault();
                if (PaperLaidObj != null)
                {
                    PaperLaidObj.IsRequested = true;
                    PaperLaidObj.RequestedDate = DateTime.Now;
                }
                obj.SaveChanges();
                obj.Close();
            }

            return null;
        }


        static object RemoveJournalistFormCurrentYear(object param)
        {
            JournalistsForPass model = (JournalistsForPass)param;
            MediaContext db = new MediaContext();
            db.JournalistsForPass.Remove((from journalist in db.JournalistsForPass where journalist.Year == model.Year && journalist.JournalistsForPassId == model.JournalistsForPassId select journalist).FirstOrDefault());
            db.SaveChanges();
            return null;
        }

        static object AddJournalistToCurrentSession(object param)
        {
            JournalistsForPass model = (JournalistsForPass)param;
            MediaContext db = new MediaContext();
            db.JournalistsForPass.Attach(model);
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
            return model;
        }

    }
}
