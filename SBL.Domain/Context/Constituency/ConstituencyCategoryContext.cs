﻿using SBL.DAL;
using SBL.DomainModel.Models.Constituency;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Constituency
{
    public class ConstituencyCategoryContext : DBBase<ConstituencyCategoryContext>
    {

        public ConstituencyCategoryContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mConstituencyReservedCategory> mConstituencyReservedCategory { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateConstituencyCategory": { CreateConstituencyCategory(param.Parameter); break; }

                case "UpdateConstituencyCategory": { return UpdateConstituencyCategory(param.Parameter); }
                case "DeleteConstituencyCategory": { return DeleteConstituencyCategory(param.Parameter); }
                case "GetAllConstituencyCategory": { return GetAllConstituencyCategory(); }
                case "GetConstituencyCategoryById": { return GetConstituencyCategoryById(param.Parameter); }
                
            }
            return null;
        }


        static void CreateConstituencyCategory(object param)
        {
            try
            {
                using (ConstituencyCategoryContext db = new ConstituencyCategoryContext())
                {
                    mConstituencyReservedCategory model = param as mConstituencyReservedCategory;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mConstituencyReservedCategory.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateConstituencyCategory(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ConstituencyCategoryContext db = new ConstituencyCategoryContext())
            {
                mConstituencyReservedCategory model = param as mConstituencyReservedCategory;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mConstituencyReservedCategory.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllConstituencyCategory();
        }

        static object DeleteConstituencyCategory(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ConstituencyCategoryContext db = new ConstituencyCategoryContext())
            {
                mConstituencyReservedCategory parameter = param as mConstituencyReservedCategory;
                mConstituencyReservedCategory stateToRemove = db.mConstituencyReservedCategory.SingleOrDefault(a => a.ConstituencyCategoryId == parameter.ConstituencyCategoryId);
                if (stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllConstituencyCategory();
        }

        static List<mConstituencyReservedCategory> GetAllConstituencyCategory()
        {
            ConstituencyCategoryContext db = new ConstituencyCategoryContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.mConstituencyReservedCategory
                         orderby a.ConstituencyCategoryId descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mConstituencyReservedCategory GetConstituencyCategoryById(object param)
        {
            mConstituencyReservedCategory parameter = param as mConstituencyReservedCategory;
            ConstituencyCategoryContext db = new ConstituencyCategoryContext();
            var query = db.mConstituencyReservedCategory.SingleOrDefault(a => a.ConstituencyCategoryId == parameter.ConstituencyCategoryId);
            return query;
        }


    }
}
