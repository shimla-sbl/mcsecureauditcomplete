﻿namespace SBL.Domain.Context.Constituency
{
    #region Namespace reffrence block.

    using SBL.DAL;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.ConstituencyVS;
    using SBL.DomainModel.Models.District;
    using SBL.DomainModel.Models.Member;
    using SBL.Service.Common;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.DomainModel.Models.SubDivision;
    using SBL.DomainModel.Models.References;

    #endregion Namespace reffrence block.

    public class ConstituencyContext : DBBase<ConstituencyContext>
    {
        #region Constructor Block

        public ConstituencyContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        #endregion Constructor Block

        #region Data Table Definition Block

        public DbSet<mConstituency> mConstituency { get; set; }

        public DbSet<mAssembly> mAssemblies { get; set; }

        public DbSet<DistrictModel> Districts { get; set; }

        public DbSet<mPanchayat> mPanchayat { get; set; }

        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }

        public DbSet<mProgramme> mProgramme { get; set; }

        public DbSet<mConstituencyReservedCategory> mConstituencyReservedCategory { get; set; }
      
        public DbSet<SiteSettings> SiteSettings { get; set; }
        public DbSet<mSubDivision> mSubDivision { get; set; }
        public DbSet<mUsers> mUsers { get; set; }
        #endregion Data Table Definition Block

        #region Execute Block.

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateConstituency": { CreateConstituency(param.Parameter); break; }
                case "UpdateConstituency": { return UpdateConstituency(param.Parameter); }
                case "DeleteConstituency": { return DeleteConstituency(param.Parameter); }
                case "GetAllConstituency": { return GetAllConstituency(); }
                case "GetConstituencyById": { return GetConstituencyById(param.Parameter); }
                case "GetSessionCodeByIdforUpdate": { return GetSessionCodeByIdforUpdate(param.Parameter); }
                case "GetConstituencyCodeById": { return GetConstituencyCodeById(param.Parameter); }
                case "GetAssemblyDataBasedOnId": { return GetAssemblyDataBasedOnId(param.Parameter); }
                case "GetDDlAssemblyBasedOnId": { return GetDDlAssemblyBasedOnId(); }
                case "GetAllAssembly": { return GetAllAssembly(); }
                case "GetAllDistrict": { return GetAllDistrict(); }
                case "GetAllConstituencyCategory": { return GetAllConstituencyCategory(); }
                case "GetAllLGDistrict": { return GetAllLGDistrict(); }
                case "GetAllAssemblyIDs": { return GetAllAssemblyIDs(param.Parameter); }
                case "IsConstituencyIdChildExist": { return IsConstituencyIdChildExist(param.Parameter); }
                case "CheckId":
                    {
                        return CheckId(param.Parameter);
                    }
                case "CopyAssemblyData": { CopyAssemblyData(param.Parameter); break; }
                case "CheckIdd":
                    {
                        return CheckIdd(param.Parameter);
                    }
                case "GetAllDistrict_By_Assembly": { return GetAllDistrict_By_Assembly(); }
                case "getConstituency_ByDistrict": { return getConstituency_ByDistrict(param.Parameter); }
                case "getSubdivision_ByConstituency": { return getSubdivision_ByConstituency(param.Parameter); }
                case "getAllPanchayat": { return getAllPanchayat(); }
                case "getConstituencyNameByid": { return getConstituencyNameByid(param.Parameter); }
                case "getDivisionNameByid": { return getDivisionNameByid(param.Parameter); }

                #region panchayat Block

                case "GetAllPanchayats": { return GetAllPanchayats(param.Parameter); }

                #endregion panchayat Block

                case "GetBeneficiarySchemesList":
                    {
                        return GetBeneficiarySchemesList(param.Parameter);
                    }
                case "Get_ConstituencyCode_ById": { return Get_ConstituencyCode_ById(param.Parameter); }
                case "GetBlockListByConstId": { return GetBlockListByConstId(param.Parameter); }
                case "GetPanchayatByBlockId": { return GetPanchayatByBlockId(param.Parameter); }
                case "Save_Benificiary_Details": { return Save_Benificiary_Details(param.Parameter); }
                case "GetBenificiaryDetailsList": { return GetBenificiaryDetailsList(param.Parameter); }
                case "GetActionTypes": { return GetActionTypes(param.Parameter); }
                case "GetBenificiaryDetailsForEdit": { return GetBenificiaryDetailsForEdit(param.Parameter); }
                case "Update_Benificiary_Details": { return Update_Benificiary_Details(param.Parameter); }
                case "_Get_ConstituencyCode": { return _Get_ConstituencyCode(param.Parameter); }

                case "GetHelplineSectorList": { return GetHelplineSectorList(param.Parameter); }
                case "Save_Helpline_Details": { return Save_Helpline_Details(param.Parameter); }
                case "GetHelpLineDetailsForEdit": { return GetHelpLineDetailsForEdit(param.Parameter); }
                case "Update_Helpline_Details": { return Update_Helpline_Details(param.Parameter); }

            }
            return null;
        }

        #endregion Execute Block.

        #region Method Definition & Body.

        #region panchayat Block

        private static object GetAllPanchayats(object param)
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var query = (from panchayat in context.mPanchayat
                             orderby panchayat.PanchayatCode ascending
                             select panchayat);
                return query.ToList();
            }
        }

        #endregion panchayat Block

        #endregion Method Definition & Body.

        private static object IsConstituencyIdChildExist(object param)
        {
            try
            {
                using (ConstituencyContext db = new ConstituencyContext())
                {
                    mConstituency model = (mConstituency)param;
                    var Res = (from e in db.mMemberAssembly
                               where (e.ConstituencyCode == model.ConstituencyCode)
                               select e).Count();

                    if (Res == 0)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                //return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        static object CheckId(object param)
        {

            ConstituencyContext qCtxDB = new ConstituencyContext();
            mConstituency updateSQ = param as mConstituency;

            var query = (from tQ in qCtxDB.mConstituency
                         where tQ.ConstituencyCode == updateSQ.ConstituencyCode && tQ.AssemblyID == updateSQ.AssemblyID
                         select tQ).ToList().Count();

            return query.ToString();
        }

        static object CheckIdd(object param)
        {

            ConstituencyContext qCtxDB = new ConstituencyContext();
            mConstituency updateSQ = param as mConstituency;

            var query = (from tQ in qCtxDB.mConstituency
                         where tQ.AssemblyID == updateSQ.AssemblyID
                         select tQ).ToList().Count();

            return query.ToString();
        }



        private static void CopyAssemblyData(object param)
        {
            try
            {
                using (ConstituencyContext db = new ConstituencyContext())
                {
                    mConstituency constitu = param as mConstituency;

                    var query = (from p in db.mConstituency

                                 where p.IsDeleted == null && p.AssemblyID == constitu.AssemblyID
                                 select p).ToList();
                    foreach (var item in query)
                    {


                        item.AssemblyID = constitu.toAssemblyID;
                        item.CreatedBy = constitu.CreatedBy;
                        item.ModifiedBy = constitu.ModifiedBy;

                        item.CreatedDate = DateTime.Now;
                        item.ModifiedDate = DateTime.Now;
                        //list.Add(item);
                        db.mConstituency.Add(item);
                        db.SaveChanges();

                    }
                    db.Close();

                }
            }
            catch
            {
                throw;
            }
        }



        private static int GetSessionCodeByIdforUpdate(object param)
        {
            try
            {
                ConstituencyContext db = new ConstituencyContext();
                int id = Convert.ToInt32(param);
                var query1 = db.mConstituency.SingleOrDefault(a => a.ConstituencyID == id);
                return query1.ConstituencyCode;
                //ConstituencyContext db = new ConstituencyContext();
                //mConstituency model = param as mConstituency;
                //var query = (from m in db.mConstituency
                //             where m.AssemblyID == model.AssemblyID && m.ConstituencyID==m.ConstituencyID
                //             select m.ConstituencyCode).ToList();
                ////return query;
                ////ConstituencyContext db = new ConstituencyContext();
                ////mConstituency model = param as mConstituency;
                ////var query = (from m in db.mConstituency
                ////             where m.AssemblyID == model.AssemblyID
                ////             select m.ConstituencyCode).ToList();
                //if (query.Count > 0)
                //{
                //    var data = query.Max();
                //    return data;

                //}
                //else
                //{
                //    return 0;

                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static int GetConstituencyCodeById(object param)
        {
            // mConstituency parameter = param as mConstituency;
            try
            {
                ConstituencyContext db = new ConstituencyContext();
                int id = Convert.ToInt32(param);

                var query1 = db.mConstituency.Where(a => a.AssemblyID == id).ToList();

                if (query1.Count > 0)
                {
                    var query = query1.Max(q => q.ConstituencyCode);

                    return query;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static List<mAssembly> GetDDlAssemblyBasedOnId()
        {
            try
            {
                DBManager db = new DBManager();
                var data = from e in db.mAssemblies select e;
                return data.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static object GetAssemblyDataBasedOnId(object param)
        {
            try
            {
                ConstituencyContext db = new ConstituencyContext();
                int id = Convert.ToInt32(param);
                var data = db.mConstituency.Where(a => a.AssemblyID == id).ToList();
                return data;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void CreateConstituency(object param)
        {
            try
            {
                using (ConstituencyContext db = new ConstituencyContext())
                {
                    mConstituency constitu = param as mConstituency;
                    constitu.CreatedDate = DateTime.Now;
                    constitu.ModifiedDate = DateTime.Now;
                    db.mConstituency.Add(constitu);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        private static object UpdateConstituency(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ConstituencyContext db = new ConstituencyContext())
            {
                mConstituency model = param as mConstituency;
                model.CreatedDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                db.mConstituency.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllAssemblyIDs(param);
        }

        private static object DeleteConstituency(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ConstituencyContext db = new ConstituencyContext())
            {
                mConstituency parameter = param as mConstituency;
                mConstituency constituencyToRemove = db.mConstituency.SingleOrDefault(a => a.ConstituencyID == parameter.ConstituencyID);
                if (constituencyToRemove != null)
                {
                    constituencyToRemove.IsDeleted = true;
                }
                // db.mConstituency.Remove(constituencyToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllAssemblyIDs(param);
        }

        private static List<mConstituency> GetAllConstituency()
        {
            ConstituencyContext db = new ConstituencyContext();
            var query = (from p in db.mConstituency
                         orderby p.ConstituencyID descending
                         where p.IsDeleted == null
                         select p);
            return query.ToList();
        }

        private static mConstituency GetConstituencyById(object param)
        {
            mConstituency parameter = param as mConstituency;
            ConstituencyContext db = new ConstituencyContext();
            var query = db.mConstituency.SingleOrDefault(a => a.ConstituencyID == parameter.ConstituencyID);
            return query;
        }

        //static int GetConstituencyCodeById(object param)
        //{
        //    // mConstituency parameter = param as mConstituency;
        //    try
        //    {
        //        DBManager db = new DBManager();
        //        int id = Convert.ToInt32(param);

        //        var data=(from a in db.mAssemblies
        //                  join c in db.mConstituency on a.

        //        var query1 = db.mConstituency.Where(a => a.AssemblyID == id).ToList();

        //        if (query1.Count > 0)
        //        {
        //            var query = query1.LastOrDefault();

        //            return query.ConstituencyCode;

        //        }
        //        else
        //        {
        //            return 0;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        #region Assembly

        private static List<mAssembly> GetAllAssembly()
        {
            DBManager db = new DBManager();
            var query = (from p in db.mAssemblies
                         orderby p.AssemblyName ascending
                         select p);
            return query.ToList();
        }

        private static object GetAllAssemblyIDs(object param)
        {
            ConstituencyContext db = new ConstituencyContext();
            mConstituency model = param as mConstituency;
            var data = (from consti in db.mConstituency
                        join assembly in db.mAssemblies on consti.AssemblyID equals assembly.AssemblyCode into joinAssemblyName
                        from assem in joinAssemblyName.DefaultIfEmpty()
                        join district in db.Districts on consti.DistrictCode equals district.DistrictCode into joinDistrictName
                        from distr in joinDistrictName.DefaultIfEmpty()
                        join cat in db.mConstituencyReservedCategory on consti.ReservedCategory equals cat.ConstituencyCategoryId into joincatName
                        from categ in joincatName.DefaultIfEmpty()
                        where consti.IsDeleted == null
                        select new
                        {
                            consti,
                            assem.AssemblyName,
                            distr.DistrictCode,
                            categ.ConstituencyCategoryCode
                        }).ToList();
            //var data1=
            List<mConstituency> list = new List<mConstituency>();
            foreach (var item in data)
            {
                item.consti.GetAssemblyName = item.AssemblyName;
                item.consti.GetDistrictName = item.DistrictCode.ToString();
                item.consti.GetReservedCategoryName = item.ConstituencyCategoryCode;

                list.Add(item.consti);
            }
            var data1 = (from a in db.mConstituency
                         // where a.PanchayatID
                         orderby a.ConstituencyID descending
                         select a).Count();

            int totalRecords = data1;
            model.ResultCount = totalRecords;
            var results = list.OrderByDescending(m => m.ConstituencyID).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            model.ConstituencyList = results;
            return model;
            //return list.OrderByDescending(c => c.ConstituencyID).ToList();
        }

        private static List<DistrictModel> GetAllDistrict()
        {
            try
            {
                DBManager db = new DBManager();
                var query = (from p in db.Districts
                             orderby p.DistrictCode ascending
                             where p.IsDeleted == null
                             select p);
                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static List<mConstituencyReservedCategory> GetAllConstituencyCategory()
        {
            try
            {
                DBManager db = new DBManager();
                var query = (from p in db.mConstituencyReservedCategory
                             orderby p.ConstituencyCategoryId ascending
                             where p.IsDeleted == null
                             select p);
                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static List<DistrictModel> GetAllLGDistrict()
        {
            DBManager db = new DBManager();
            var query = (from p in db.Districts
                         select p);
            return query.ToList();
        }


        #endregion Assembly

        public static List<mConstituency> getConstituency_ByDistrict(object param)
        {


            if (param == null || param.ToString() == "")
            {
                param = "0";
            }
            int DistrictCode = Convert.ToInt32(param.ToString());
            ConstituencyContext context = new ConstituencyContext();
            var currentAssembly = (from site in context.SiteSettings
                                   where site.SettingName == "Assembly"
                                   select site.SettingValue).FirstOrDefault();
            int assemblyID = Convert.ToInt32(currentAssembly);

            // if (DistrictCode != 0 && DistrictCode != null)
            // {
            var qquery = (from list in context.mConstituency
                          where list.DistrictCode == DistrictCode && list.AssemblyID == assemblyID
                          select list
                          ).ToList();
            // }
            // else
            // {
            //var qquery = (from list in context.mConstituency
            //        where list.AssemblyID == assemblyID
            //        select list
            //          ).ToList();
            // }
            return qquery;

        }


        public static List<mSubDivision> getSubdivision_ByConstituency(object param)
        {
            if (param == null || param == "")
            {
                param = "0";
            }
            List<mSubDivision> Clist = new List<mSubDivision>();
            mUsers obj = new mUsers();
            string IDs = param.ToString();
            //obj.ConIds = IDs.Split(',').ToList();
            string[] allParts = IDs.Split(',');
            for (int j = 0; j <= allParts.Length - 1; j++)
            {
                string ConsId = allParts[j];
                //if (ConsId == "")
                //{

                //}
                if (allParts[j].Contains("HP"))
                {
                }
                else
                {
                    int ConId = Convert.ToInt32(allParts[j]);

                    ConstituencyContext context = new ConstituencyContext();
                    var query = (from list in context.mSubDivision
                                 where list.ConstituencyID == ConId
                                 orderby list.ConstituencyID ascending
                                 select list).ToList();
                    foreach (var val in query.ToList())
                    {
                        mSubDivision tempModel = new mSubDivision();
                        tempModel = val;
                        if (val.mSubDivisionId != null)
                        {
                            tempModel.mSubDivisionId = val.mSubDivisionId;
                        }

                        if (val.SubDivisionName != null)
                        {
                            tempModel.SubDivisionName = val.SubDivisionName;
                        }


                        Clist.Add(tempModel);
                    }
                }

            }

            //string[] a = obj.Split(',');
            //foreach (var item in a)
            //{



            //    int Id = Convert.ToInt32(item);
            //    var Res = (from e in db.tConstituencyPanchayat where e.ConstituencyPanchayatID == Id select e).SingleOrDefault();
            //    db.tConstituencyPanchayat.Remove(Res);
            //    db.SaveChanges();
            //}

            return Clist;
        }
        public static List<mPanchayat> getAllPanchayat()
        {
            using (ConstituencyContext context = new ConstituencyContext())
            {
                var query = (from panchayat in context.mPanchayat
                             orderby panchayat.PanchayatName ascending
                             select panchayat
                        ).ToList();
                return query;
            }
        }
        static object getConstituencyNameByid(object param)
        {

            ConstituencyContext Context = new ConstituencyContext();
            mUsers model = param as mUsers;
            ConstituencyContext context = new ConstituencyContext();
            var currentAssembly = (from site in context.SiteSettings
                                   where site.SettingName == "Assembly"
                                   select site.SettingValue).FirstOrDefault();
            int assemblyID = Convert.ToInt32(currentAssembly);
            string ConcName = "";
            foreach (var sr in model.ConIds)
            {
                ConcName += (from s in Context.mConstituency where s.ConstituencyCode.ToString() == sr && s.AssemblyID == assemblyID select s.ConstituencyName).SingleOrDefault() + ",";
            }

            ConcName = ConcName.TrimEnd(',');
            return ConcName;

        }
        static object getDivisionNameByid(object param)
        {

            ConstituencyContext Context = new ConstituencyContext();
            mUsers model = param as mUsers;
            ConstituencyContext context = new ConstituencyContext();
            //var currentAssembly = (from site in context.SiteSettings
            //                       where site.SettingName == "Assembly"
            //                       select site.SettingValue).FirstOrDefault();
            //int assemblyID = Convert.ToInt32(currentAssembly);
            string SubDName = "";
            foreach (var sr in model.SubdivisionIds)
            {
                SubDName += (from s in Context.mSubDivision where s.mSubDivisionId.ToString() == sr select s.SubDivisionName).SingleOrDefault() + ",";
            }

            SubDName = SubDName.TrimEnd(',');
            return SubDName;

        }
        private static List<DistrictModel> GetAllDistrict_By_Assembly()
        {
            try
            {
                DBManager db = new DBManager();
                ConstituencyContext context = new ConstituencyContext();
                var currentState = (from site in context.SiteSettings
                                    where site.SettingName == "StateCode"
                                    select site.SettingValue).FirstOrDefault();
                int StateCode = Convert.ToInt32(currentState);
                var query = (from p in db.Districts
                             orderby p.DistrictCode ascending
                             where p.IsDeleted == null && p.StateCode == StateCode
                             select p);
                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //New Methods for Benificiary FeedBack--Constituency
        public static object GetBeneficiarySchemesList(object param)
        {
            try
            {
                using (DBManager db = new DBManager())
                {
                    //int CommitteeId = Convert.ToInt32(param.ToString());
                    tBeneficiarySchemes Cmodel = param as tBeneficiarySchemes;
                    List<tBeneficiarySchemes> s = new List<tBeneficiarySchemes>();

                    var query = (from Schemes in db.tBeneficiarySchemes
                                 where Schemes.IsActive == true
                                 select Schemes
                        ).ToList();
                    return query;

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static object GetBlockListByConstId(object param)
        {

            int ConstituencyCode = Convert.ToInt32(param.ToString());

            try
            {
                using (DBManager db = new DBManager())
                {

                    List<mBlock> s = new List<mBlock>();
                    var query = (from Block in db.mBlock
                                 where Block.ConstituencyCode == ConstituencyCode
                                 select Block
                        ).ToList();
                    return query;

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static object GetPanchayatByBlockId(object param)
        {
            int BlockCode = Convert.ToInt32(param.ToString());

            try
            {
                using (DBManager db = new DBManager())
                {
                    List<mPanchayat> querynew = new List<mPanchayat>();

                    var query = (from Block in db.tConstituencyPanchayat
                                 where Block.BlockCode == BlockCode
                                 // && Block.IsActive == true
                                 select Block
                        ).ToList();
                    foreach (var item in query)
                    {
                        var querylist = (from Panchayat in db.mPanchayat
                                         where Panchayat.PanchayatCode == item.PanchayatCode
                                         select Panchayat
                         ).ToList();
                        querynew.AddRange(querylist);
                    }
                    return querynew;

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static object GetActionTypes(object param)
        {
            try
            {
                using (DBManager db = new DBManager())
                {
                    var query = (from Actions in db.mTypeOfAction
                                 select Actions
                        ).ToList();
                    return query;

                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static object Save_Benificiary_Details(object param)
        {
            try
            {
                tBeneficearyFeedback item = param as tBeneficearyFeedback;
                tBeneficearyFeedback obj = new tBeneficearyFeedback();
                using (DBManager ctx = new DBManager())
                {
                    obj.BeneficearySchemeTypeId = item.BeneficearySchemeTypeId;
                    obj.BlockCode = item.BlockCode;
                    obj.PanchayatCode = item.PanchayatCode;
                    obj.ConstituencyCode = item.ConstituencyCode;
                    obj.BeneficearyName = item.BeneficearyName;
                    obj.BeneficearyAdress = item.BeneficearyAdress;
                    obj.BeneficearyMobile = item.BeneficearyMobile;
                    obj.Sex = item.Sex;
                    obj.BenefitDate = item.BenefitDate;
                    obj.BeneficearyComponent = item.BeneficearyComponent;
                    obj.BeneficearyAmount = item.BeneficearyAmount;
                    obj.FeedbackDate = item.FeedbackDate;
                    obj.FeedBackDetails = item.FeedBackDetails;
                    obj.ActionDate = item.ActionDate;
                    obj.ActionTypeCode = item.ActionTypeCode;
                    obj.ActionDetails = item.ActionDetails;
                    obj.CreatedDate = item.CreatedDate;
                    ctx.tBeneficearyFeedback.Add(obj);
                    ctx.SaveChanges();
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }
        private static object GetBenificiaryDetailsList(object param)
        {
            int ConstituencyId = Convert.ToInt32(param.ToString());

            {
                using (DBManager context = new DBManager())
                {
                    var list = (from a in context.tBeneficearyFeedback
                                where a.ConstituencyCode == ConstituencyId
                                select new
                                {
                                    Id = a.Id,
                                    BeneficearySchemeTypeId = a.BeneficearySchemeTypeId,
                                    BeneficearyName = a.BeneficearyName,
                                    BeneficearyAdress = a.BeneficearyAdress,
                                    BeneficearyMobile = a.BeneficearyMobile,
                                    Sex = a.Sex,
                                    BenefitDate = a.BenefitDate,
                                    BeneficearyComponent = a.BeneficearyComponent,
                                    BeneficearyAmount = a.BeneficearyAmount,
                                    BlockCode = a.BlockCode,
                                    PanchayatCode = a.PanchayatCode,
                                    ConstituencyCode = a.ConstituencyCode,
                                    FeedbackDate = a.FeedbackDate,
                                    FeedBackDetails = a.FeedBackDetails,
                                    ActionDate = a.ActionDate,
                                    ActionTypeCode = a.ActionTypeCode,
                                    ActionDetails = a.ActionDetails,
                                }).OrderByDescending(a => a.Id).ToList();
                    //   .OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<tBeneficearyFeedback> mdl = new List<tBeneficearyFeedback>();
                    foreach (var a in list)
                    {
                        tBeneficearyFeedback m = new tBeneficearyFeedback();
                        m.Id = a.Id;

                        m.BeneficearySchemeTypeId = a.BeneficearySchemeTypeId;
                        m.BeneficearySchemeTypeName = (from mc in context.tBeneficiarySchemes where mc.Id == a.BeneficearySchemeTypeId select mc.SchemeName).FirstOrDefault();

                        m.BeneficearyName = a.BeneficearyName;
                        m.BeneficearyAdress = a.BeneficearyAdress;
                        m.BeneficearyMobile = a.BeneficearyMobile;
                        m.Sex = a.Sex;

                        m.BenefitDate = a.BenefitDate;

                        m.BeneficearyComponent = a.BeneficearyComponent;
                        m.BeneficearyAmount = a.BeneficearyAmount;

                        m.BlockCode = a.BlockCode;
                        m.BlockName = (from mc in context.mBlock where mc.BlockCode == a.BlockCode select mc.BlockName).FirstOrDefault();

                        m.PanchayatCode = a.PanchayatCode;
                        m.PanchayatName = (from mc in context.mPanchayat where mc.PanchayatCode == a.PanchayatCode select mc.PanchayatName).FirstOrDefault();


                        m.ConstituencyCode = a.ConstituencyCode;
                        m.ConstituencyName = (from mc in context.mConstituency where mc.ConstituencyCode == a.ConstituencyCode select mc.ConstituencyName).FirstOrDefault();


                        m.FeedbackDate = a.FeedbackDate;

                        m.FeedBackDetails = a.FeedBackDetails;
                        m.ActionDate = a.ActionDate;

                        m.ActionTypeCode = a.ActionTypeCode;
                        m.ActionTypeName = (from mc in context.mTypeOfAction where mc.ActionCode == a.ActionTypeCode select mc.ActionName).FirstOrDefault();


                        m.ActionDetails = a.ActionDetails;
                        mdl.Add(m);
                    }
                    return mdl;
                }
            }
        }

        private static object GetBenificiaryDetailsForEdit(object param)
        {
            int Id = Convert.ToInt32(param.ToString());

            {
                using (DBManager context = new DBManager())
                {
                    var list = (from a in context.tBeneficearyFeedback
                                where a.Id == Id
                                select new
                                {
                                    Id = a.Id,
                                    BeneficearySchemeTypeId = a.BeneficearySchemeTypeId,
                                    BeneficearyName = a.BeneficearyName,
                                    BeneficearyAdress = a.BeneficearyAdress,
                                    BeneficearyMobile = a.BeneficearyMobile,
                                    Sex = a.Sex,
                                    BenefitDate = a.BenefitDate,
                                    BeneficearyComponent = a.BeneficearyComponent,
                                    BeneficearyAmount = a.BeneficearyAmount,
                                    BlockCode = a.BlockCode,
                                    PanchayatCode = a.PanchayatCode,
                                    ConstituencyCode = a.ConstituencyCode,
                                    FeedbackDate = a.FeedbackDate,
                                    FeedBackDetails = a.FeedBackDetails,
                                    ActionDate = a.ActionDate,
                                    ActionTypeCode = a.ActionTypeCode,
                                    ActionDetails = a.ActionDetails,
                                }).OrderByDescending(a => a.Id).ToList();
                    //   .OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<tBeneficearyFeedback> mdl = new List<tBeneficearyFeedback>();
                    foreach (var a in list)
                    {
                        tBeneficearyFeedback m = new tBeneficearyFeedback();
                        m.Id = a.Id;

                        m.BeneficearySchemeTypeId = a.BeneficearySchemeTypeId;
                        m.BeneficearySchemeTypeName = (from mc in context.tBeneficiarySchemes where mc.Id == a.BeneficearySchemeTypeId select mc.SchemeName).FirstOrDefault();

                        m.BeneficearyName = a.BeneficearyName;
                        m.BeneficearyAdress = a.BeneficearyAdress;
                        m.BeneficearyMobile = a.BeneficearyMobile;
                        m.Sex = a.Sex;

                        m.BenefitDate = a.BenefitDate;

                        m.BeneficearyComponent = a.BeneficearyComponent;
                        m.BeneficearyAmount = a.BeneficearyAmount;

                        m.BlockCode = a.BlockCode;
                        m.BlockName = (from mc in context.mBlock where mc.BlockCode == a.BlockCode select mc.BlockName).FirstOrDefault();

                        m.PanchayatCode = a.PanchayatCode;
                        m.PanchayatName = (from mc in context.mPanchayat where mc.PanchayatCode == a.PanchayatCode select mc.PanchayatName).FirstOrDefault();


                        m.ConstituencyCode = a.ConstituencyCode;
                        m.ConstituencyName = (from mc in context.mConstituency where mc.ConstituencyCode == a.ConstituencyCode select mc.ConstituencyName).FirstOrDefault();


                        m.FeedbackDate = a.FeedbackDate;

                        m.FeedBackDetails = a.FeedBackDetails;
                        m.ActionDate = a.ActionDate;

                        m.ActionTypeCode = a.ActionTypeCode;
                        m.ActionTypeName = (from mc in context.mTypeOfAction where mc.ActionCode == a.ActionTypeCode select mc.ActionName).FirstOrDefault();


                        m.ActionDetails = a.ActionDetails;
                        mdl.Add(m);
                    }
                    return mdl;
                }
            }
        }


        public static object Update_Benificiary_Details(object param)
        {
            try
            {
                using (DBManager context = new DBManager())
                {

                    SBL.DomainModel.Models.Constituency.tBeneficearyFeedback model = new SBL.DomainModel.Models.Constituency.tBeneficearyFeedback();
                    tBeneficearyFeedback item = param as tBeneficearyFeedback;
                    model.Id = item.Id;
                    var obj = (from attachments in context.tBeneficearyFeedback where attachments.Id == model.Id select attachments).FirstOrDefault();
                    obj.BeneficearySchemeTypeId = item.BeneficearySchemeTypeId;
                    obj.BlockCode = item.BlockCode;
                    obj.PanchayatCode = item.PanchayatCode;
                    obj.ConstituencyCode = item.ConstituencyCode;
                    obj.BeneficearyName = item.BeneficearyName;
                    obj.BeneficearyAdress = item.BeneficearyAdress;
                    obj.Sex = item.Sex;
                    obj.BeneficearyMobile = item.BeneficearyMobile;
                    obj.BenefitDate = item.BenefitDate;
                    obj.BeneficearyComponent = item.BeneficearyComponent;
                    obj.BeneficearyAmount = item.BeneficearyAmount;
                    obj.FeedbackDate = item.FeedbackDate;
                    obj.FeedBackDetails = item.FeedBackDetails;
                    obj.ActionDate = item.ActionDate;
                    obj.ActionTypeCode = item.ActionTypeCode;
                    obj.ActionDetails = item.ActionDetails;
                    obj.ModifiedDate = item.ModifiedDate;
                    context.Entry(obj).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                    return 0;
                }
            }
            catch
            {
                return 1; //Error
            }
        }

        static object Get_ConstituencyCode_ById(object param)
        {

            ConstituencyContext Context = new ConstituencyContext();
            string Cons_IDs = param.ToString();
            ConstituencyContext context = new ConstituencyContext();
            var currentAssembly = (from site in context.SiteSettings
                                   where site.SettingName == "Assembly"
                                   select site.SettingValue).FirstOrDefault();
            int assemblyID = Convert.ToInt32(currentAssembly);
            int Conc_Code = 0;
            Conc_Code = (from s in Context.mConstituency where s.ConstituencyID.ToString() == Cons_IDs && s.AssemblyID == assemblyID select s.ConstituencyCode).SingleOrDefault();
            return Conc_Code;
        }


        public static object GetHelplineSectorList(object param)
        {
            try
            {
                using (DBManager db = new DBManager())
                {
                    //int CommitteeId = Convert.ToInt32(param.ToString());
                    tSectors Cmodel = param as tSectors;
                    List<tBeneficiarySchemes> s = new List<tBeneficiarySchemes>();

                    var query = (from Sector in db.Sectors
                                 where Sector.Active == true
                                 select Sector
                        ).ToList();
                    return query;

                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        public static object Save_Helpline_Details(object param)
        {
            try
            {
                tMemberHelpline item = param as tMemberHelpline;
                tMemberHelpline obj = new tMemberHelpline();
                using (DBManager ctx = new DBManager())
                {
                    obj.SectorId = item.SectorId;
                    obj.BlockCode = item.BlockCode;
                    obj.PanchayatCode = item.PanchayatCode;
                    obj.ConstituencyCode = item.ConstituencyCode;
                    obj.ApplicantName = item.ApplicantName;
                    obj.ApplicantAddress = item.ApplicantAddress;
                    obj.ApplicantMobile = item.ApplicantMobile;
                    obj.Sex = item.Sex;

                    obj.CallingDate = item.CallingDate;
                    obj.CallDetails = item.CallDetails;

                    obj.ActionDate = item.ActionDate;
                    obj.ActionTypeCode = item.ActionTypeCode;
                    obj.ActionDetails = item.ActionDetails;
                    obj.CreatedDate = item.CreatedDate;
                    ctx.tMemberHelpline.Add(obj);
                    ctx.SaveChanges();
                    return 0;  //Success
                }
            }
            catch
            {
                return 1; //Error
            }
        }


        static object _Get_ConstituencyCode(object param)
        {
            ConstituencyContext Context = new ConstituencyContext();
            mMemberAssembly item = param as mMemberAssembly;
            int membercode = item.MemberID;
            ConstituencyContext context = new ConstituencyContext();
            int Conc_Code = 0;
            Conc_Code = (from s in Context.mMemberAssembly
                         where s.AssemblyID == item.AssemblyID
                         && s.MemberID == item.MemberID
                         select s.ConstituencyCode).SingleOrDefault();
            return Conc_Code;
        }

        private static object GetHelpLineDetailsForEdit(object param)
        {
            int Id = Convert.ToInt32(param.ToString());

            {
                using (DBManager context = new DBManager())
                {
                    var list = (from a in context.tMemberHelpline
                                where a.Id == Id
                                select new
                                {
                                    Id = a.Id,
                                    SectorId = a.SectorId,
                                    ApplicantName = a.ApplicantName,
                                    ApplicantAddress = a.ApplicantAddress,
                                    ApplicantMobile = a.ApplicantMobile,
                                    Sex = a.Sex,

                                    BlockCode = a.BlockCode,
                                    PanchayatCode = a.PanchayatCode,
                                    ConstituencyCode = a.ConstituencyCode,
                                    CallingDate = a.CallingDate,
                                    CallDetails = a.CallDetails,
                                    ActionDate = a.ActionDate,
                                    ActionTypeCode = a.ActionTypeCode,
                                    ActionDetails = a.ActionDetails,
                                }).OrderByDescending(a => a.Id).ToList();
                    //   .OrderBy(a => a.CommitteeName).ThenByDescending(a => a.CreatedDate).ToList();

                    List<tMemberHelpline> mdl = new List<tMemberHelpline>();
                    foreach (var a in list)
                    {
                        tMemberHelpline m = new tMemberHelpline();
                        m.Id = a.Id;

                        m.SectorId = a.SectorId;
                        m.SectorName = (from mc in context.Sectors where mc.Id == a.Id select mc.Title).FirstOrDefault();

                        m.ApplicantName = a.ApplicantName;
                        m.ApplicantAddress = a.ApplicantAddress;
                        m.ApplicantMobile = a.ApplicantMobile;
                        m.Sex = a.Sex;


                        m.BlockCode = a.BlockCode;
                        m.BlockName = (from mc in context.mBlock where mc.BlockCode == a.BlockCode select mc.BlockName).FirstOrDefault();

                        m.PanchayatCode = a.PanchayatCode;
                        m.PanchayatName = (from mc in context.mPanchayat where mc.PanchayatCode == a.PanchayatCode select mc.PanchayatName).FirstOrDefault();


                        m.ConstituencyCode = a.ConstituencyCode;
                        m.ConstituencyName = (from mc in context.mConstituency where mc.ConstituencyCode == a.ConstituencyCode select mc.ConstituencyName).FirstOrDefault();


                        m.CallDetails = a.CallDetails;

                        m.CallingDate = a.CallingDate;
                        m.ActionDate = a.ActionDate;

                        m.ActionTypeCode = a.ActionTypeCode;
                        m.ActionTypeName = (from mc in context.mTypeOfAction where mc.ActionCode == a.ActionTypeCode select mc.ActionName).FirstOrDefault();


                        m.ActionDetails = a.ActionDetails;
                        mdl.Add(m);
                    }
                    return mdl;
                }
            }
        }
        public static object Update_Helpline_Details(object param)
        {
            try
            {
                using (DBManager context = new DBManager())
                {

                    SBL.DomainModel.Models.Constituency.tMemberHelpline model = new SBL.DomainModel.Models.Constituency.tMemberHelpline();
                    tMemberHelpline item = param as tMemberHelpline;
                    model.Id = item.Id;
                    var obj = (from attachments in context.tMemberHelpline where attachments.Id == model.Id select attachments).FirstOrDefault();
                    obj.SectorId = item.SectorId;
                    obj.BlockCode = item.BlockCode;
                    obj.PanchayatCode = item.PanchayatCode;
                    obj.ConstituencyCode = item.ConstituencyCode;
                    obj.ApplicantName = item.ApplicantName;
                    obj.ApplicantAddress = item.ApplicantAddress;
                    obj.ApplicantMobile = item.ApplicantMobile;
                    obj.Sex = item.Sex;
                    obj.CallingDate = item.CallingDate;
                    obj.CallDetails = item.CallDetails;
                    obj.ActionDate = item.ActionDate;
                    obj.ActionTypeCode = item.ActionTypeCode;
                    obj.ActionDetails = item.ActionDetails;
                    context.Entry(obj).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                    return 0;
                }
            }
            catch
            {
                return 1; //Error
            }
        }

    }
}