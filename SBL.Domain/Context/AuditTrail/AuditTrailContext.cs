﻿using System;
using System.Linq;
using SBL.DAL;
using SBL.Service.Common;
using System.Data.Entity;
using SBL.DomainModel.Models.AuditTrail;

namespace SBL.Domain.Context.AuditTrail
{
    public class AuditTrailContext : DBBase<AuditTrailContext>
    {
        public AuditTrailContext() : base("eVidhan") { }

        public DbSet<Audit> Audits { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "AddAudit": { return AddAudit(param.Parameter); }
                    case "GetAuditList": { return GetAuditList(); }                   
                }
            }
            return null;

        }
        static object AddAudit(object param)
        {
            try
            {
                using (AuditTrailContext db = new AuditTrailContext())
                {
                    Audit audit = param as Audit;
                    db.Audits.Add(audit);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        static object GetAuditList()
        {
            try
            {
                using (AuditTrailContext db = new AuditTrailContext())
                {
                    return db.Audits.OrderByDescending(a=>a.LogID).Take(500).ToList();
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

       
    } 

}
