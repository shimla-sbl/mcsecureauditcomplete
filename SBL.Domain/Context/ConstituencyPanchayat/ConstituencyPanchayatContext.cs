﻿using SBL.DAL;
using SBL.DomainModel.Models.Constituency;
using SBL.Service.Common;
//using SBL.eLegistrator.HouseController.Web.Areas.ConstituencyPanchyat.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SBL.Domain.Context.ConstituencyPanchayat
{
   public class tConstituencyPanchayatContext :DBBase<tConstituencyPanchayatContext>
    {
       public tConstituencyPanchayatContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<tConstituencyPanchayat> ConstituencyPanch { get; set; }
        public DbSet<mPanchayat> mPanchayat { get; set; }



        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreatePanchayat": { CreatePanchayat(param.Parameter); break; }
                case "UpdatePanchayat": { return UpdatePanchayat(param.Parameter); }
                case "DeletePanchayat": { return DeletePanchayat(param.Parameter); }
                case "GetAllPanchayats": { return GetAllPanchayats(); }
                case "GetPanchayatById": { return GetPanchayatById(param.Parameter); }
                //case "GetAllActivePanchayats": { return GetAllActivePanchayats(); }



            }
            return null;
        }
        static List<mPanchayat> GetAllPanchayats()
        {
            tConstituencyPanchayatContext db = new tConstituencyPanchayatContext();
            var data = db.mPanchayat.ToList();
            return data;

        }

        static void CreatePanchayat(object param)
        {
            try
            {
                using (tConstituencyPanchayatContext db = new tConstituencyPanchayatContext())
                {
                    tConstituencyPanchayat model = param as tConstituencyPanchayat;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.ConstituencyPanch.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdatePanchayat(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (tConstituencyPanchayatContext db = new tConstituencyPanchayatContext())
            {
                tConstituencyPanchayat model = param as tConstituencyPanchayat;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.ConstituencyPanch.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllPanchayats();
        }

        static object DeletePanchayat(object param)
        {
            try 
	{
	            using(tConstituencyPanchayatContext db= new tConstituencyPanchayatContext())
                {
                    tConstituencyPanchayat model = param as tConstituencyPanchayat;
                    tConstituencyPanchayat constToDelete = db.ConstituencyPanch.SingleOrDefault(c => c.ConstituencyPanchayatID == model.ConstituencyPanchayatID);
                    db.ConstituencyPanch.Remove(constToDelete);
                        db.SaveChanges();
                    db.Close();

                }
                return GetAllPanchayats();
		
	}
	catch (Exception ex)
	{
		
		throw ex;
	}
        }

        static tConstituencyPanchayat GetPanchayatById(object param)
        {
            tConstituencyPanchayat parameter = param as tConstituencyPanchayat;
            tConstituencyPanchayatContext db = new tConstituencyPanchayatContext();
            var query = db.ConstituencyPanch.SingleOrDefault(a => a.ConstituencyPanchayatID == parameter.ConstituencyPanchayatID);
            return query;
        }



       

    }
}

    

