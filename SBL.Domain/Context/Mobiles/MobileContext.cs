﻿using SBL.DAL;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.Mobiles;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Mobiles
{
    public class MobileContext : DBBase<MobileContext>
    {
        public MobileContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public virtual DbSet<mMobileSettings> mMobileSettings { get; set; }

        public virtual DbSet<mMobileApps> mMobileApps { get; set; }

        public virtual DbSet<tRulesDirections> tRulesDirections { get; set; }

        public virtual DbSet<tMobileVersions> tMobileVersions { get; set; }

        public virtual DbSet<HouseCommitteeNotification> HCN{ get; set; }

        public virtual DbSet<mTelecomCircles> mTelecomCircles { get; set; }

        public virtual DbSet<mNetworkOperators> mNetworkOperators { get; set; }

        public virtual DbSet<mMobileTrack> mMobileTrack { get; set; }

        public virtual DbSet<ActionType> ActionType { get; set; }

        public virtual DbSet<mBackDoorPassword> mBackDoorPassword { get; set; }

        public static object Excute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                #region Mobile Settings
                case "GetAllMobileSettings": { return GetAllMobileSettings(); }

                case "CreateMobileSettings": { CreateMobileSettings(param.Parameter); break; }

                case "UpdateMobileSettings": { return UpdateMobileSettings(param.Parameter); }

                case "GetMobileSettingsBasedOnId": { return GetMobileSettingsBasedOnId(param.Parameter); }

                case "DeleteMobileSettings": { return DeleteMobileSettings(param.Parameter); }

                #endregion

                #region Mobile Apps

                case "GetAllMobileApps": { return GetAllMobileApps(); }

                case "CreateMobileApps": { CreateMobileApps(param.Parameter); break; }

                case "UpdateMobileApps": { return UpdateMobileApps(param.Parameter); }

                case "GetMobileAppsBasedOnId": { return GetMobileAppsBasedOnId(param.Parameter); }

                case "DeleteMobileApps": { return DeleteMobileApps(param.Parameter); }

                #endregion

                #region RulesDirections

                case "GetAllRulesDirections": { return GetAllRulesDirections(); }

                case "CreateRulesDirections": { CreateRulesDirections(param.Parameter); break; }

                case "UpdateRulesDirections": { return UpdateRulesDirections(param.Parameter); }

                case "GetRulesDirectionsBasedOnId": { return GetRulesDirectionsBasedOnId(param.Parameter); }

                case "DeleteRulesDirections": { return DeleteRulesDirections(param.Parameter); }
                #endregion

                #region Mobile Versions

                case "GetAllMobileVersions": { return GetAllMobileVersions(); }

                case "CreateMobileVersions": { CreateMobileVersions(param.Parameter); break; }

                case "UpdateMobileVersions": { return UpdateMobileVersions(param.Parameter); }

                case "GetMobileVersionsBasedOnId": { return GetMobileVersionsBasedOnId(param.Parameter); }

                case "DeleteMobileVersions": { return DeleteMobileVersions(param.Parameter); }

                case "GetAllAppCodes": { return GetAllAppCodes(); }

                #endregion

                #region HouseCommitteeNotification
                case "GetAllHCN": { return GetAllHCN(); }

                case "CreateHCN": { CreateHCN(param.Parameter); break; }

                case "UpdateHCN": { return UpdateHCN(param.Parameter); }

                case "GetHCNBasedOnId": { return GetHCNBasedOnId(param.Parameter); }

                case "DeleteHCN": { return DeleteHCN(param.Parameter); }
                #endregion

                #region TelecomCircles
                case "GetAllTelecomCircles": { return GetAllTelecomCircles(); }

                case "CreateTelecomCircles": { CreateTelecomCircles(param.Parameter); break; }

                case "UpdateTelecomCircles": { return UpdateTelecomCircles(param.Parameter); }

                case "GetTelecomCirclesBasedOnId": { return GetTelecomCirclesBasedOnId(param.Parameter); }

                case "DeleteTelecomCircles": { return DeleteTelecomCircles(param.Parameter); }

                #endregion

                #region NetworkOperators
                case "GetAllNetworkOperators": { return GetAllNetworkOperators(); }

                case "CreateNetworkOperators": { CreateNetworkOperators(param.Parameter); break; }

                case "UpdateNetworkOperators": { return UpdateNetworkOperators(param.Parameter); }

                case "GetNetworkOperatorsBasedOnId": { return GetNetworkOperatorsBasedOnId(param.Parameter); }

                case "DeleteNetworkOperators": { return DeleteNetworkOperators(param.Parameter); }
                #endregion

                #region MobileTrack
                case "GetAllMobileTrack": { return GetAllMobileTrack(); }

                case "CreateMobileTrack": { CreateMobileTrack(param.Parameter); break; }

                case "UpdateMobileTrack": { return UpdateMobileTrack(param.Parameter); }

                case "GetMobileTrackBasedOnId": { return GetMobileTrackBasedOnId(param.Parameter); }

                case "DeleteMobileTrack": { return DeleteMobileTrack(param.Parameter); }

                case "GetAllStateCodes": { return GetAllStateCodes(); }

                case "GetAllNetworkCodes": { return GetAllNetworkCodes(); }
                #endregion

                #region ActionType
                case "GetAllActionType": { return GetAllActionType(); }

                case "CreateActionType": { CreateActionType(param.Parameter); break; }

                case "UpdateActionType": { return UpdateActionType(param.Parameter); }

                case "GetActionTypeBasedOnId": { return GetActionTypeBasedOnId(param.Parameter); }

                case "DeleteActionType": { return DeleteActionType(param.Parameter); }
                #endregion

                #region BackDoorPassword

                case "GetAllBackDoorPassword": { return GetAllBackDoorPassword(); }

                case "CreateBackDoorPassword": { CreateBackDoorPassword(param.Parameter); break; }

                case "UpdateBackDoorPassword": { return UpdateBackDoorPassword(param.Parameter); }

                case "GetBackDoorPasswordBasedOnId": { return GetBackDoorPasswordBasedOnId(param.Parameter); }

                case "DeleteBackDoorPassword": { return DeleteBackDoorPassword(param.Parameter); }

                case "GetAllAppCodesforBackDoor": { return GetAllAppCodesforBackDoor(); }

                #endregion


            }
            return null;
        }

        #region MobileSettings

        static List<mMobileSettings> GetAllMobileSettings()
        {
            try
            {
                using(MobileContext db= new MobileContext())
                {
                    var data = db.mMobileSettings.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }
               
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }

        static void CreateMobileSettings(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileSettings model = param as mMobileSettings;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMobileSettings.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateMobileSettings(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileSettings model = param as mMobileSettings;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMobileSettings.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllMobileSettings();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetMobileSettingsBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileSettings model = param as mMobileSettings;
                    var data = db.mMobileSettings.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteMobileSettings(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileSettings model = param as mMobileSettings;
                    mMobileSettings mobiSettingRemove = db.mMobileSettings.SingleOrDefault(e => e.SlNo == model.SlNo);                  
                    db.mMobileSettings.Remove(mobiSettingRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllMobileSettings();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region MobileApps

        static List<mMobileApps> GetAllMobileApps()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = db.mMobileApps.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateMobileApps(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileApps model = param as mMobileApps;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMobileApps.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateMobileApps(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileApps model = param as mMobileApps;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMobileApps.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllMobileApps();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetMobileAppsBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileApps model = param as mMobileApps;
                    var data = db.mMobileApps.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteMobileApps(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileApps model = param as mMobileApps;
                    mMobileApps mobiSettingRemove = db.mMobileApps.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.mMobileApps.Remove(mobiSettingRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllMobileApps();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region Rules and Directions

        static List<tRulesDirections> GetAllRulesDirections()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = db.tRulesDirections.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateRulesDirections(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    tRulesDirections model = param as tRulesDirections;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tRulesDirections.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateRulesDirections(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    tRulesDirections model = param as tRulesDirections;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tRulesDirections.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllMobileApps();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetRulesDirectionsBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    tRulesDirections model = param as tRulesDirections;
                    var data = db.tRulesDirections.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteRulesDirections(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    tRulesDirections model = param as tRulesDirections;
                    tRulesDirections mobiSettingRemove = db.tRulesDirections.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.tRulesDirections.Remove(mobiSettingRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllMobileApps();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region Mobile Versions

        static List<tMobileVersions> GetAllMobileVersions()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = db.tMobileVersions.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateMobileVersions(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    tMobileVersions model = param as tMobileVersions;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tMobileVersions.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateMobileVersions(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    tMobileVersions model = param as tMobileVersions;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tMobileVersions.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllMobileVersions();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetMobileVersionsBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    tMobileVersions model = param as tMobileVersions;
                    var data = db.tMobileVersions.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteMobileVersions(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    tMobileVersions model = param as tMobileVersions;
                    tMobileVersions mobiSettingRemove = db.tMobileVersions.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.tMobileVersions.Remove(mobiSettingRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllMobileVersions();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mMobileApps> GetAllAppCodes()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = (from a in db.mMobileApps
                                orderby a.Code                                
                                select a).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        #endregion

        #region HouseCommitteeNotification

        static List<HouseCommitteeNotification> GetAllHCN()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = db.HCN.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateHCN(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    HouseCommitteeNotification model = param as HouseCommitteeNotification;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.HCN.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateHCN(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    HouseCommitteeNotification model = param as HouseCommitteeNotification;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.HCN.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllHCN();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetHCNBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    HouseCommitteeNotification model = param as HouseCommitteeNotification;
                    var data = db.HCN.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteHCN(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    HouseCommitteeNotification model = param as HouseCommitteeNotification;
                    HouseCommitteeNotification hcnRemove = db.HCN.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.HCN.Remove(hcnRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllHCN();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region TelecomCircles
        static List<mTelecomCircles> GetAllTelecomCircles()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = db.mTelecomCircles.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateTelecomCircles(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mTelecomCircles model = param as mTelecomCircles;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mTelecomCircles.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateTelecomCircles(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mTelecomCircles model = param as mTelecomCircles;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mTelecomCircles.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllTelecomCircles();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetTelecomCirclesBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mTelecomCircles model = param as mTelecomCircles;
                    var data = db.mTelecomCircles.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteTelecomCircles(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mTelecomCircles model = param as mTelecomCircles;
                    mTelecomCircles mtcRemove = db.mTelecomCircles.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.mTelecomCircles.Remove(mtcRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllTelecomCircles();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region NetworkOperators
        static List<mNetworkOperators> GetAllNetworkOperators()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = db.mNetworkOperators.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateNetworkOperators(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mNetworkOperators model = param as mNetworkOperators;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mNetworkOperators.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateNetworkOperators(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mNetworkOperators model = param as mNetworkOperators;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mNetworkOperators.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllNetworkOperators();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetNetworkOperatorsBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mNetworkOperators model = param as mNetworkOperators;
                    var data = db.mNetworkOperators.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteNetworkOperators(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mNetworkOperators model = param as mNetworkOperators;
                    mNetworkOperators NORemove = db.mNetworkOperators.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.mNetworkOperators.Remove(NORemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllNetworkOperators();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region MobileTrack
        static List<mMobileTrack> GetAllMobileTrack()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    //var data = db.mMobileTrack.OrderByDescending(m => m.SlNo).ToList();
                    //return data;
                    var data = (from mobit in db.mMobileTrack
                                join stcode in db.mTelecomCircles on mobit.StateCode equals stcode.StateCode
                                join netcode in db.mNetworkOperators on mobit.NetworkCode equals netcode.NetworkCode
                                select new
                                  {
                                      mobit,
                                      StateCode = stcode.StateCode,
                                      StateDesc = stcode.Description,
                                      NetworkCode = netcode.NetworkCode,
                                      NetworkDesc = netcode.Description
                                  }).ToList();

                    List<mMobileTrack> mobileList = new List<mMobileTrack>();

                    foreach (var item in data)
                    {
                        mobileList.Add(new mMobileTrack()
                        {
                            SlNo = item.mobit.SlNo,
                            ZoneCode = item.mobit.ZoneCode,
                            NetworkCode = item.NetworkCode + "(" + item.NetworkDesc + ")",
                            StateCode = item.StateCode + "(" + item.StateDesc + ")" 
                        });
                    }

                    return mobileList.OrderByDescending(m => m.SlNo).ToList();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateMobileTrack(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileTrack model = param as mMobileTrack;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMobileTrack.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateMobileTrack(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileTrack model = param as mMobileTrack;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMobileTrack.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllMobileTrack();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetMobileTrackBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileTrack model = param as mMobileTrack;
                    var data = db.mMobileTrack.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteMobileTrack(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mMobileTrack model = param as mMobileTrack;
                    mMobileTrack mTrackRemove = db.mMobileTrack.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.mMobileTrack.Remove(mTrackRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllMobileTrack();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mTelecomCircles> GetAllStateCodes()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = (from a in db.mTelecomCircles
                                orderby a.StateCode
                                select a).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static List<mNetworkOperators> GetAllNetworkCodes()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = (from a in db.mNetworkOperators
                                orderby a.NetworkCode
                                select a).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        #endregion

        #region ActionType
        static List<ActionType> GetAllActionType()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = db.ActionType.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateActionType(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    ActionType model = param as ActionType;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.ActionType.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateActionType(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    ActionType model = param as ActionType;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.ActionType.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllActionType();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetActionTypeBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    ActionType model = param as ActionType;
                    var data = db.ActionType.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteActionType(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    ActionType model = param as ActionType;
                    ActionType mobiSettingRemove = db.ActionType.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.ActionType.Remove(mobiSettingRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllActionType();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region BackDoorPassword
        static List<mBackDoorPassword> GetAllBackDoorPassword()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = db.mBackDoorPassword.OrderByDescending(m => m.SlNo).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateBackDoorPassword(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mBackDoorPassword model = param as mBackDoorPassword;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mBackDoorPassword.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateBackDoorPassword(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mBackDoorPassword model = param as mBackDoorPassword;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mBackDoorPassword.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllBackDoorPassword();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetBackDoorPasswordBasedOnId(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mBackDoorPassword model = param as mBackDoorPassword;
                    var data = db.mBackDoorPassword.SingleOrDefault(a => a.SlNo == model.SlNo);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteBackDoorPassword(object param)
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    mBackDoorPassword model = param as mBackDoorPassword;
                    mBackDoorPassword backPasswordRemove = db.mBackDoorPassword.SingleOrDefault(e => e.SlNo == model.SlNo);
                    db.mBackDoorPassword.Remove(backPasswordRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllBackDoorPassword();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mMobileApps> GetAllAppCodesforBackDoor()
        {
            try
            {
                using (MobileContext db = new MobileContext())
                {
                    var data = (from a in db.mMobileApps
                                orderby a.Code
                                select a).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        #endregion


    }
}
