﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Question;
using SBL.Service.Common;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.Notice;

namespace SBL.Domain.Context.QuestionListPDF
{
    public class QuestionListPDFContext : DBBase<QuestionListPDFContext>
    {
        public QuestionListPDFContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mAssembly> mAssembly { get; set; }

        public DbSet<mSession> mSession { get; set; }

        public DbSet<SiteSettings> mSiteSettings { get; set; }

        public DbSet<mMember> mMembers { get; set; }

        public DbSet<tQuestion> tQuestion { get; set; }

        public DbSet<tQuestionType> tQuestionType { get; set; }
        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public DbSet<SBL.DomainModel.Models.Constituency.mConstituency> mConstituency { get; set; }
        public virtual DbSet<mMinistry> mMinistry { get; set; }
        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetAllQuestionList":
                    {
                        return GetAllQuestionList(param.Parameter);
                    }
                case "GetQuestionTypes":
                    {
                        return GetQuestionTypes(param.Parameter);
                    }
                case "GetAllMembers":
                    {
                        return GetAllMembers(param.Parameter);
                    }
                case "GetAllAssemblies":
                    {
                        return GetAllAssemblies(param.Parameter);
                    }
                case "GetAllSessions":
                    {
                        return GetAllSessions(param.Parameter);
                    }
                case "GetCurrentAssembly":
                    {
                        return GetCurrentAssembly(param.Parameter);
                    }
                case "GetCurrentSession":
                    {
                        return GetCurrentSession(param.Parameter);
                    }
                case "GetMembers":
                    {
                        return GetMembers(param.Parameter);
                    }
                    
            }
            return null;
        }

        public static object GetAllAssemblies(object param)
        {
            QuestionListPDFContext db = new QuestionListPDFContext();

            var data = (from assembly in db.mAssembly
                        select assembly).ToList();
            return data;

        }

        public static object GetAllSessions(object param)
        {
            QuestionListPDFContext db = new QuestionListPDFContext();

            var data = (from session in db.mSession
                        select session).ToList();
            return data;
        }

        public static object GetCurrentAssembly(object param)
        {
            QuestionListPDFContext db = new QuestionListPDFContext();

            var data = (from siteSettings in db.mSiteSettings
                        where siteSettings.SettingName == "Assembly"
                        select siteSettings.SettingValue).FirstOrDefault();
            return data;
        }

        public static object GetCurrentSession(object param)
        {
            QuestionListPDFContext db = new QuestionListPDFContext();

            var data = (from siteSettings in db.mSiteSettings
                        where siteSettings.SettingName == "Session"
                        select siteSettings.SettingValue).FirstOrDefault();
            return data;
        }

        public static object GetAllQuestionList(object param)
        {
            QuestionListPDFContext db = new QuestionListPDFContext();
            QuestionListPDFViewModel model = param as QuestionListPDFViewModel;

            var data = (from question in db.tQuestion
                        join member in db.mMembers
                        on question.MemberID equals member.MemberCode
                        where question.AssemblyID == model.AssemblyCode && question.SessionID == model.SessionID &&
                        question.NoticeRecievedDate != null && question.NoticeRecievedDate <= model.NoticeRecievedDate &&
                        question.QuestionType == model.QuestionType &&
                        member.MemberCode == model.MemberCode
                        
                        select new QuestionListPDFViewModel
                        {
                            DiaryNumber = question.DiaryNumber,
                            NoticeRecievedDate = question.NoticeRecievedDate,
                            NoticeRecievedTime = question.NoticeRecievedTime,
                            Name = member.Name,
                            Subject = question.Subject,
                            QuestionNumber = question.QuestionNumber,
                            MainQuestion = question.MainQuestion,
                            IsFixedDate = question.IsFixedDate
                        }).ToList();

            return data; 
        }

        public static object GetQuestionTypes(object param)
        {
            QuestionListPDFContext db = new QuestionListPDFContext();

            var data = (from QuestionTypes in db.tQuestionType
                        select new tQuestionTypeModel
                        {
                            QuestionTypeId = QuestionTypes.QuestionTypeId,
                            QuestionTypeName = QuestionTypes.QuestionTypeName
                        }).ToList();
            
            return data;
        }

        public static object GetAllMembers(object param)
        {
            QuestionListPDFContext db = new QuestionListPDFContext();

            var data = (from members in db.mMembers
                        select members).ToList();
            return data;
        }


        //public static object GetMembers(object param)
        //{
        //    QuestionListPDFContext pCtxt = new QuestionListPDFContext();
        //    QuestionListPDFViewModel Obj = param as QuestionListPDFViewModel;
        //    var Memlist = (from M in pCtxt.mMembers
        //                   join MA in pCtxt.mMemberAssembly on M.MemberCode equals MA.MemberID
        //                   join Const in pCtxt.mConstituency on MA.ConstituencyCode equals Const.ConstituencyCode
        //                   where (Const.AssemblyID == Obj.AID)
        //                       && (MA.AssemblyID == Obj.AID)
        //                       && !(from ExcepMinis in pCtxt.mMinistry where ExcepMinis.AssemblyID == Obj.AID select ExcepMinis.MemberCode).Contains(M.MemberCode)
        //                       && M.Active == true
        //                   orderby M.Name
        //                   select new DiaryModel
        //                   {
        //                       MemberId = MA.MemberID,
        //                       MemberName = M.Name,
        //                       ConstituencyName = Const.ConstituencyName,
        //                       ConstituencyCode = Const.ConstituencyCode
        //                   }).ToList();

        //    Obj.NewmemberList = Memlist;
        //    return Obj;
        //}

        public static object GetMembers(object param)
        {
            QuestionListPDFContext pCtxt = new QuestionListPDFContext();
            tMemberNotice Obj = param as tMemberNotice;
            var Memlist = (from M in pCtxt.mMembers
                           join MA in pCtxt.mMemberAssembly on M.MemberCode equals MA.MemberID
                           join Const in pCtxt.mConstituency on MA.ConstituencyCode equals Const.ConstituencyCode
                           where (Const.AssemblyID == Obj.AssemblyID)
                               && (MA.AssemblyID == Obj.AssemblyID)
                               && !(from ExcepMinis in pCtxt.mMinistry where ExcepMinis.AssemblyID == Obj.AssemblyID select ExcepMinis.MemberCode).Contains(M.MemberCode)
                               && M.Active == true
                           orderby M.Name
                           select new DiaryModel
                           {
                               MemberId = MA.MemberID,
                               MemberName = M.Name,
                               ConstituencyName = Const.ConstituencyName,
                               ConstituencyCode = Const.ConstituencyCode
                           }).ToList();
          
            Obj.NewmemberList = Memlist;
            return Obj;
        }
    }
}
