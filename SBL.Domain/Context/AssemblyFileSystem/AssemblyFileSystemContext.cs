﻿using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.AssemblyFileSystem
{
    public class AssemblyFileSystemContext : DBBase<AssemblyFileSystemContext>
    {
        public AssemblyFileSystemContext() : base("eVidhan") { }

        public DbSet<tAssemblyFiles> tAssemblyFiles { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mSession> mSession { get; set; }
        public DbSet<mSessionDate> mSessionDate { get; set; }
        public DbSet<mAssemblyTypeofDocuments> mAssemblyTypeofDocuments { get; set; }
        public DbSet<SiteSettings> SiteSettings { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "GetAllAssemblyFiles": { return GetAllAssemblyFiles(param.Parameter); }

                    case "AddAssemblyFile": { return AddAssemblyFile(param.Parameter); }

                    case "UpdateAssemblyFile": { return UpdateAssemblyFile(param.Parameter); }

                    case "GetAssemblyFileById": { return GetAssemblyFileById(param.Parameter); }

                    case "DeleteAssemblyFileById": { return DeleteAssemblyFileById(param.Parameter); }

                    case "GetAssemblyTypeofDocumentLst": { return GetAssemblyTypeofDocumentLst(param.Parameter); }

                    case "GetAssemblyTypeofDocumentLstForDepartment": { return GetAssemblyTypeofDocumentLstForDepartment(param.Parameter); }

                    case "GetAllAssemblyFilesSearch": { return GetAllAssemblyFilesSearch(param.Parameter); }

                    case "SearchGetAssemblyTypeofDocumentLst": { return SearchGetAssemblyTypeofDocumentLst(param.Parameter); }


                }
            }
            return null;
        }



        private static object AddAssemblyFile(object p)
        {
            try
            {
                using (AssemblyFileSystemContext db = new AssemblyFileSystemContext())
                {
                    tAssemblyFiles data = p as tAssemblyFiles;

                    data.ModifiedDate = DateTime.Now;
                    data.CreatedDate = DateTime.Now;
                    data.Status = true;

                    db.tAssemblyFiles.Add(data);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            //throw new NotImplementedException();
        }

        private static object UpdateAssemblyFile(object p)
        {

            try
            {
                using (AssemblyFileSystemContext db = new AssemblyFileSystemContext())
                {
                    tAssemblyFiles updateresult = p as tAssemblyFiles;
                    var actualResult = (from assemblyfile in db.tAssemblyFiles
                                        where
                                            assemblyfile.AssemblyFileId == updateresult.AssemblyFileId
                                        select assemblyfile).FirstOrDefault();

                    actualResult.AssemblyId = updateresult.AssemblyId;
                    actualResult.SessionDateId = updateresult.SessionDateId;
                    actualResult.SessionId = updateresult.SessionId;
                    actualResult.TypeofDocumentId = updateresult.TypeofDocumentId;
                    actualResult.UploadFile = updateresult.UploadFile;
                    actualResult.Status = updateresult.Status;
                    actualResult.ModifiedDate = DateTime.Now;
                    actualResult.ModifiedBy = updateresult.ModifiedBy;
                    actualResult.Description = updateresult.Description;

                    db.tAssemblyFiles.Attach(actualResult);
                    db.Entry(actualResult).State = EntityState.Modified;
                    db.SaveChanges();


                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return true;
          //  throw new NotImplementedException();
        }

        private static object DeleteAssemblyFileById(object p)
        {
            try
            {
                using (AssemblyFileSystemContext db = new AssemblyFileSystemContext())
                {
                    tAssemblyFiles id = p as tAssemblyFiles;

                    var result = (from assemblyfile in db.tAssemblyFiles
                                  where
                                      assemblyfile.AssemblyFileId == id.AssemblyFileId
                                  select assemblyfile).FirstOrDefault();

                    db.tAssemblyFiles.Remove(result);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static object GetAllAssemblyFiles(object p)
        {
            try
            {
                using (AssemblyFileSystemContext db = new AssemblyFileSystemContext())
                {
                    var relaresult = (from asblyFiles in db.tAssemblyFiles
                                      join assembly in db.mAssembly on asblyFiles.AssemblyId equals assembly.AssemblyCode
                                      join session in db.mSession on
                                         new { SessionCode = asblyFiles.SessionId, assemblyId = asblyFiles.AssemblyId } equals new { SessionCode = session.SessionCode, assemblyId = session.AssemblyID }
                                      join sessiondate
                                          in db.mSessionDate on asblyFiles.SessionDateId equals sessiondate.Id into nullsessiondate
                                      from sessdate in nullsessiondate.DefaultIfEmpty()
                                      join asmblydoctype in db.mAssemblyTypeofDocuments on asblyFiles.TypeofDocumentId
                                      equals asmblydoctype.TypeofDocumentId
                                      orderby asblyFiles.AssemblyFileId descending
                                      select
                                          new
                                          {
                                              assemblyFile = asblyFiles,
                                              assemblyName = assembly.AssemblyName,
                                              sessionName = session.SessionName,
                                              sessiondateName = sessdate != null ? sessdate.SessionDate : default(DateTime),
                                              docTypeName = asmblydoctype.TypeofDocumentName
                                          }).ToList();


                    List<tAssemblyFiles> assemblyFileList = new List<tAssemblyFiles>();

                    var filecessSetings = (from a in db.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();


                    foreach (var item in relaresult)
                    {
                        tAssemblyFiles assemblyFile = new tAssemblyFiles();
                        assemblyFile = item.assemblyFile;
                        assemblyFile.FileAcessPath = filecessSetings.SettingValue;
                        //assemblyFile.UploadFile = filecessSetings.SettingValue + item.assemblyFile.UploadFile;
                        assemblyFile.AssemblyName = item.assemblyName;
                        assemblyFile.SessionName = item.sessionName;
                        assemblyFile.SessionDate = item.sessiondateName.ToString("dd/MM/yyyy") == "01/01/0001" ? "" : item.sessiondateName.ToString("dd/MM/yyyy");
                        assemblyFile.AssemblyDocTypeName = item.docTypeName;
                        assemblyFileList.Add(assemblyFile);
                    }

                    return assemblyFileList;
                    //return (from assFiles in db.tAssemblyFiles select assFiles).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }





        private static object GetAllAssemblyFilesSearch(object param)
        {
            try
            {
                using (AssemblyFileSystemContext db = new AssemblyFileSystemContext())
                {
                    var id = Convert.ToInt32(param);
                    var relaresult = (from asblyFiles in db.tAssemblyFiles
                                      join assembly in db.mAssembly on asblyFiles.AssemblyId equals assembly.AssemblyCode
                                      join session in db.mSession on
                                         new { SessionCode = asblyFiles.SessionId, assemblyId = asblyFiles.AssemblyId } equals new { SessionCode = session.SessionCode, assemblyId = session.AssemblyID }
                                      join sessiondate
                                          in db.mSessionDate on asblyFiles.SessionDateId equals sessiondate.Id into nullsessiondate
                                      from sessdate in nullsessiondate.DefaultIfEmpty()
                                      join asmblydoctype in db.mAssemblyTypeofDocuments on asblyFiles.TypeofDocumentId
                                      equals asmblydoctype.TypeofDocumentId
                                      where asblyFiles.TypeofDocumentId == id
                                      orderby asblyFiles.AssemblyFileId descending
                                      select
                                          new
                                          {
                                              assemblyFile = asblyFiles,
                                              assemblyName = assembly.AssemblyName,
                                              sessionName = session.SessionName,
                                              sessiondateName = sessdate != null ? sessdate.SessionDate : default(DateTime),
                                              docTypeName = asmblydoctype.TypeofDocumentName
                                          }).ToList();


                    List<tAssemblyFiles> assemblyFileList = new List<tAssemblyFiles>();

                    var filecessSetings = (from a in db.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();


                    foreach (var item in relaresult)
                    {
                        tAssemblyFiles assemblyFile = new tAssemblyFiles();
                        assemblyFile = item.assemblyFile;
                        assemblyFile.FileAcessPath = filecessSetings.SettingValue;
                        //assemblyFile.UploadFile = filecessSetings.SettingValue + item.assemblyFile.UploadFile;
                        assemblyFile.AssemblyName = item.assemblyName;
                        assemblyFile.SessionName = item.sessionName;
                        assemblyFile.SessionDate = item.sessiondateName.ToString("dd/MM/yyyy") == "01/01/0001" ? "" : item.sessiondateName.ToString("dd/MM/yyyy");
                        assemblyFile.AssemblyDocTypeName = item.docTypeName;
                        assemblyFileList.Add(assemblyFile);
                    }

                    return assemblyFileList;
                    //return (from assFiles in db.tAssemblyFiles select assFiles).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }





        private static object GetAssemblyFileById(object p)
        {
            try
            {
                using (AssemblyFileSystemContext db = new AssemblyFileSystemContext())
                {
                    tAssemblyFiles id = p as tAssemblyFiles;
                    var filecessSetings = (from a in db.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
                    var result = (from assemblyfile in db.tAssemblyFiles
                                  where
                                      assemblyfile.AssemblyFileId == id.AssemblyFileId
                                  select assemblyfile).FirstOrDefault();
                    result.FileAcessPath = filecessSetings.SettingValue;
                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static object GetAssemblyTypeofDocumentLst(object p)
        {
            try
            {
                using (AssemblyFileSystemContext db = new AssemblyFileSystemContext())
                {
                    var result = (from assemblyfile in db.mAssemblyTypeofDocuments

                                  select assemblyfile).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static object GetAssemblyTypeofDocumentLstForDepartment(object p)
        {
            try
            {
                using (AssemblyFileSystemContext db = new AssemblyFileSystemContext())
                {
                    var result = (from assemblyfile in db.mAssemblyTypeofDocuments
                                  where assemblyfile.TypeofDocumentId == 1 || assemblyfile.TypeofDocumentId == 2 || assemblyfile.TypeofDocumentId == 3
                                  select assemblyfile).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        static object SearchGetAssemblyTypeofDocumentLst(object param)
        {
            DBManager db = new DBManager();
            //var id = Convert.ToInt32(param);
            var pname = Convert.ToString(param);
            var data = (from m in db.mAssemblyTypeofDocuments
                        where m.IsDeleted == null && m.TypeofDocumentName == pname && m.TypeofDocumentName != null
                        select m.TypeofDocumentId).FirstOrDefault();
            return data;

        }



    }
}
