﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.Domain.Context.Assembly;
using SBL.Domain.Context.Session;
using SBL.Domain.Context.SiteSetting;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.PrintingPress;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.StaffManagement;
using SBL.DomainModel.Models.FormTR2;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.SiteSetting;
using System.Data;

namespace SBL.Domain.Context.TR2
{
    public class TR2Contex : DBBase<TR2Contex>
    {
        public TR2Contex() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public virtual DbSet<mStaff> mStaff { get; set; }
        public virtual DbSet<mAccountFields> mAccountFields { get; set; }
        public virtual DbSet<tEmployeeAccountDetails> tEmployeeAccountDetails { get; set; }
        public virtual DbSet<mBudget> mBudget { get; set; }
        public DbSet<SiteSettings> SiteSettings { get; set; }
        public virtual DbSet<tRebate> tRebate { get; set; }
        public virtual DbSet<TR2LoanDetail> TR2LoanDetail { get; set; }
        public virtual DbSet<Form16> Form16 { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {

                case "GetStaff":
                    {
                        return GetStaff(param.Parameter);
                    }
                case "GetAllAccountFields":
                    {
                        return GetAllAccountFields(param.Parameter);
                    }
                case "SaveHPTR2Form":
                    {
                        return SaveHPTR2Form(param.Parameter);
                    }
                case "UpdateHPTR2Form":
                    {
                        return UpdateHPTR2Form(param.Parameter);
                    }

                case "GetBudgetCode":
                    {
                        return GetBudgetCode(param.Parameter);
                    }
                case "GetHPTR2Data":
                    {
                        return GetHPTR2Data(param.Parameter);
                    }
                case "GetSumforReport":
                    {
                        return GetSumforReport(param.Parameter);
                    }
                case "GetTR2DetailByAccountFieldId":
                    {
                        return GetTR2DetailByAccountFieldId(param.Parameter);
                    }
                case "GetTR2ListByAccountFieldId":
                    {
                        return GetTR2ListByAccountFieldId(param.Parameter);
                    }
                case "GetTR2DetailByAccountFieldIdAndMonthAndEmpId":
                    {
                        return GetTR2DetailByAccountFieldIdAndMonthAndEmpId(param.Parameter);
                    }
                case "GetNetAmount":
                    {
                        return GetNetAmount(param.Parameter);
                    }
                case "GetDaData":
                    {
                        return GetDaData(param.Parameter);
                    }
                case "GetRebete":
                    {
                        return GetRebete(param.Parameter);
                    }
                case "GetRevateCountAmount":
                    {
                        return GetRevateCountAmount(param.Parameter);
                    }

                case "SaveRebate":
                    {
                        return SaveRebate(param.Parameter);
                    }
                case "DeleteRebate":
                    {
                        return DeleteRebate(param.Parameter);
                    }
                case "SaveLoanDetails":
                    {
                        return SaveLoanDetails(param.Parameter);
                    }
                case "SaveForm16":
                    {
                        return SaveForm16(param.Parameter);
                    }
                case "GetForm16":
                    {
                        return GetForm16(param.Parameter);
                    }
                case "GetAllForm16":
                    {
                        return GetAllForm16(param.Parameter);
                    }
                case "GetForm16ById":
                    {
                        return GetForm16ById(param.Parameter);
                    }
                case "GetPayData":
                    {
                        return GetPayData(param.Parameter);
                    }
                case "GetLoanDetailByForm16Id":
                    {
                        return GetLoanDetailByForm16Id(param.Parameter);
                    }
                case "GetRebeteByForm16Id":
                    {
                        return GetRebeteByForm16Id(param.Parameter);
                    }
                case "GetQ1Report":
                    {
                        return GetQ1Report(param.Parameter);
                    }
                case "GetSiteSettingd":
                    {
                        return GetSiteSettingd(param.Parameter);
                    }
                    
            }

            return null;
        }


        public static object GetStaff(object param)
        {

            //   mStaff model = param as mStaff;
            Tr2Model newmodel = new Tr2Model();
            using (var cnxt = new TR2Contex())
            {
                var data = (from mdl in cnxt.mStaff where mdl.isActive == true select mdl).OrderBy(z => z.StaffName).ToList();

                return data;
            }

        }



        public static object GetDaData(object param)
        {

            try
            {
                tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

                int Year = model.Nww.Item1;
                int Month = model.Nww.Item2;
                using (TR2Contex db = new TR2Contex())
                {
                    var data = (from EA in db.tEmployeeAccountDetails
                                orderby EA.AccountFieldId
                                where EA.EmpId == model.EmpId && EA.AccountYear == Year && EA.AccountMonth == Month
                                select EA).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }


        public static object GetPayData(object param)
        {

            try
            {
                tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

                int Year = model.Nww.Item1;
                int Month = model.Nww.Item2;
                using (TR2Contex db = new TR2Contex())
                {
                    var data1 = (from EA in db.tEmployeeAccountDetails
                                 orderby EA.AccountFieldId
                                 where EA.EmpId == model.EmpId && EA.AccountYear == Year && EA.AccountMonth == Month && EA.FromDate == EA.FromDate
                                 select EA).ToList();

                    if (data1 != null)
                    {
                        var data = (from EA in db.tEmployeeAccountDetails
                                    orderby EA.AccountFieldId
                                    where EA.EmpId == model.EmpId && EA.AccountYear == Year && EA.AccountMonth == Month
                                    select EA).ToList();
                        return data;
                    }

                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public static object GetBudgetCode(object param)
        {

            tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

            TR2Contex Tr2Cxt = new TR2Contex();
            var query = (from Account in Tr2Cxt.mBudget
                         where Account.BudgetID == model.BID
                         //  orderby questions.QuestionNumber ascending
                         select new Tr2Model
                         {

                             TreasuryCD = Account.TreasuryCode,
                             DDOCD = Account.DDOCode,
                             MajorHead = Account.MajorHead,
                             SubmajorHead = Account.SubMajorHead,
                             MinorHead = Account.MinorHead,
                             BudgetCode = Account.BudgetCode,
                             Plan = Account.Plan,
                             Gazett = Account.Gazetted,
                             DemandNo = Account.DemandNo,
                             ObjectCode = Account.ObjectCode,
                             VotedCharged = Account.VotedCharged

                         }).FirstOrDefault();

            model.TreasuryCD = query.TreasuryCD;
            model.DDOCD = query.DDOCD;
            model.MajorHead = query.MajorHead;
            model.SubmajorHead = query.SubmajorHead;
            model.MinorHead = query.MinorHead;
            model.BudgetCode = query.BudgetCode;
            model.Plan = query.Plan;
            model.Gazett = query.Gazett;
            model.DemandNo = query.DemandNo;
            model.ObjectCode = query.ObjectCode;
            model.VotedCharged = query.VotedCharged;

            var FL = (from SS in Tr2Cxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();
            var FAP = (from SS in Tr2Cxt.SiteSettings
                       where SS.SettingName == "SecureFileAccessingUrlPath"
                       select SS.SettingValue).FirstOrDefault();

            model.FilePath = FL;
            model.GetFilePath = FAP;


            return model;
        }


        public static object GetSumforReport(object param)
        {

            tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

            TR2Contex Tr2Cxt = new TR2Contex();
            int[] DesignationIds = Array.ConvertAll(model.MstaffIds.ToArray(), s => int.Parse(s));

            var ResultData = (from Account in Tr2Cxt.tEmployeeAccountDetails
                              where Account.finanacialYear == model.finanacialYear && Account.AccountMonth == model.AccountMonth && (DesignationIds).Contains(Account.EmpId)
                              select Account).ToList();

            model.BasicPay = ResultData.Where(m => m.AccountFieldId == 2).Sum(m => m.AccountFieldValue);
            model.SpecialPay = ResultData.Where(m => m.AccountFieldId == 3).Sum(m => m.AccountFieldValue);
            model.DearnessAllowns = ResultData.Where(m => m.AccountFieldId == 4).Sum(m => m.AccountFieldValue);
            model.CompenseAllowns = ResultData.Where(m => m.AccountFieldId == 5).Sum(m => m.AccountFieldValue);
            model.HRAAllowns = ResultData.Where(m => m.AccountFieldId == 6).Sum(m => m.AccountFieldValue);
            model.CapitalAllowns = ResultData.Where(m => m.AccountFieldId == 7).Sum(m => m.AccountFieldValue);
            model.ConveyaceAllowns = ResultData.Where(m => m.AccountFieldId == 8).Sum(m => m.AccountFieldValue);
            model.WashingAllowns = ResultData.Where(m => m.AccountFieldId == 9).Sum(m => m.AccountFieldValue);
            model.GP = ResultData.Where(m => m.AccountFieldId == 10).Sum(m => m.AccountFieldValue);
            model.SectPay = ResultData.Where(m => m.AccountFieldId == 11).Sum(m => m.AccountFieldValue);

            model.GrossTotalHead = model.BasicPay + model.SpecialPay + model.DearnessAllowns + model.CompenseAllowns + model.HRAAllowns + model.CapitalAllowns + model.ConveyaceAllowns + model.WashingAllowns + model.GP + model.SectPay;

            model.GPFSubs = ResultData.Where(m => m.AccountFieldId == 17).Sum(m => m.AccountFieldValue);
            model.GPFAdvans = ResultData.Where(m => m.AccountFieldId == 18).Sum(m => m.AccountFieldValue);
            model.HBA = ResultData.Where(m => m.AccountFieldId == 19).Sum(m => m.AccountFieldValue);
            model.HBAInterest = ResultData.Where(m => m.AccountFieldId == 20).Sum(m => m.AccountFieldValue);
            model.NCarScooterAdvance = ResultData.Where(m => m.AccountFieldId == 21).Sum(m => m.AccountFieldValue);
            model.MCScooterInterest = ResultData.Where(m => m.AccountFieldId == 22).Sum(m => m.AccountFieldValue);
            model.WarmclothAdvance = ResultData.Where(m => m.AccountFieldId == 23).Sum(m => m.AccountFieldValue);
            model.WarmclothInterest = ResultData.Where(m => m.AccountFieldId == 24).Sum(m => m.AccountFieldValue);
            model.LTCTAAdvance = ResultData.Where(m => m.AccountFieldId == 26).Sum(m => m.AccountFieldValue);
            model.FestivalAdvance = ResultData.Where(m => m.AccountFieldId == 25).Sum(m => m.AccountFieldValue);
            model.MiscelRecov = ResultData.Where(m => m.AccountFieldId == 27).Sum(m => m.AccountFieldValue);

            model.TotalDeducA = model.GPFSubs + model.GPFAdvans + model.HBA + model.HBAInterest + model.NCarScooterAdvance + model.MCScooterInterest + model.WarmclothAdvance + model.WarmclothInterest + model.LTCTAAdvance + model.FestivalAdvance + model.MiscelRecov;

            model.InsurFund = ResultData.Where(m => m.AccountFieldId == 31).Sum(m => m.AccountFieldValue);
            model.SavingFund = ResultData.Where(m => m.AccountFieldId == 30).Sum(m => m.AccountFieldValue);
            model.HouseRent = ResultData.Where(m => m.AccountFieldId == 32).Sum(m => m.AccountFieldValue);
            model.PLI = ResultData.Where(m => m.AccountFieldId == 33).Sum(m => m.AccountFieldValue);
            model.LIC = ResultData.Where(m => m.AccountFieldId == 34).Sum(m => m.AccountFieldValue);
            model.IncomeTax = ResultData.Where(m => m.AccountFieldId == 35).Sum(m => m.AccountFieldValue);
            model.Surcharge = ResultData.Where(m => m.AccountFieldId == 36).Sum(m => m.AccountFieldValue);
            model.OtherBT = ResultData.Where(m => m.AccountFieldId == 37).Sum(m => m.AccountFieldValue);

            model.TotalDeducB = model.InsurFund + model.SavingFund + model.HouseRent + model.PLI + model.LIC + model.IncomeTax + model.Surcharge + model.OtherBT;

            model.BalanceAmount = model.GrossTotalHead - model.TotalDeducA;

            model.NetAmount = model.BalanceAmount - model.TotalDeducB;



            return model;
        }

        #region Account Fields
        public static object GetAllAccountFields(object param)
        {

            try
            {
                using (TR2Contex db = new TR2Contex())
                {
                    var data = (from AF in db.mAccountFields
                                orderby AF.Value
                                select AF).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


        #region EmployeeAccountDetails
        public static object SaveHPTR2Form(object param)
        {
            try
            {
                using (TR2Contex db = new TR2Contex())
                {
                    tEmployeeAccountDetails model = param as tEmployeeAccountDetails;
                    db.tEmployeeAccountDetails.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }

        }
        public static object UpdateHPTR2Form(object param)
        {
            try
            {
                using (TR2Contex db = new TR2Contex())
                {
                    tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

                    db.tEmployeeAccountDetails.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }
        public static object GetHPTR2Data(object param)
        {

            try
            {
                tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

                using (TR2Contex db = new TR2Contex())
                {
                    var data = (from EA in db.tEmployeeAccountDetails
                                orderby EA.AccountFieldId
                                where EA.EmpId == model.EmpId && EA.finanacialYear == model.finanacialYear && EA.AccountMonth == model.AccountMonth
                                select EA).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public static object GetTR2DetailByAccountFieldId(object param)
        {

            try
            {
                tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

                using (TR2Contex db = new TR2Contex())
                {
                    var data = (from EA in db.tEmployeeAccountDetails
                                where EA.Id == model.Id && EA.AccountFieldId == model.AccountFieldId //&& EA.EmpId == model.EmpId
                                select EA).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetTR2DetailByAccountFieldIdAndMonthAndEmpId(object param)
        {

            try
            {
                tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

                using (TR2Contex db = new TR2Contex())
                {
                    var data = (from EA in db.tEmployeeAccountDetails
                                where EA.EmpId == model.EmpId && EA.AccountFieldId == model.AccountFieldId && EA.finanacialYear == model.finanacialYear
                                && EA.AccountMonth == model.AccountMonth
                                select EA).FirstOrDefault();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetTR2ListByAccountFieldId(object param)
        {

            try
            {
                tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

                using (TR2Contex db = new TR2Contex())
                {
                    var data = (from EA in db.tEmployeeAccountDetails
                                where EA.Id > model.Id && EA.AccountFieldId == model.AccountFieldId && EA.EmpId == model.EmpId
                                select EA).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




        public static object GetQ1Report(object param)
        {

            tEmployeeAccountDetails model = param as tEmployeeAccountDetails;
            int Year = model.Nww.Item1;
            int Month = model.Nww.Item2;
            TR2Contex Tr2Cxt = new TR2Contex();
            //int[] DesignationIds = Array.ConvertAll(model.MstaffIds.ToArray(), s => int.Parse(s));
            //int[] MonthsIds = Array.ConvertAll(model.MonthsQuater.ToArray(), s => int.Parse(s));

            var ResultData = (from Account in Tr2Cxt.tEmployeeAccountDetails
                              orderby Account.AccountFieldId
                              where Account.EmpId == model.EmpId && Account.AccountYear == Year && Account.AccountMonth == Month
                              select Account).ToList();

            //var ResultData = (from Account in Tr2Cxt.tEmployeeAccountDetails
            //                  where Account.finanacialYear == model.finanacialYear && (MonthsIds).Contains(Account.AccountMonth.Value) && (DesignationIds).Contains(Account.EmpId)
            //                  select Account).ToList();

            //model.BasicPay = ResultData.Where(m => m.AccountFieldId == 2).Sum(m => m.AccountFieldValue);
            //model.SpecialPay = ResultData.Where(m => m.AccountFieldId == 3).Sum(m => m.AccountFieldValue);
            //model.DearnessAllowns = ResultData.Where(m => m.AccountFieldId == 4).Sum(m => m.AccountFieldValue);
            //model.CompenseAllowns = ResultData.Where(m => m.AccountFieldId == 5).Sum(m => m.AccountFieldValue);
            //model.HRAAllowns = ResultData.Where(m => m.AccountFieldId == 6).Sum(m => m.AccountFieldValue);
            //model.CapitalAllowns = ResultData.Where(m => m.AccountFieldId == 7).Sum(m => m.AccountFieldValue);
            //model.ConveyaceAllowns = ResultData.Where(m => m.AccountFieldId == 8).Sum(m => m.AccountFieldValue);
            //model.WashingAllowns = ResultData.Where(m => m.AccountFieldId == 9).Sum(m => m.AccountFieldValue);
            //model.GP = ResultData.Where(m => m.AccountFieldId == 10).Sum(m => m.AccountFieldValue);
            //model.SectPay = ResultData.Where(m => m.AccountFieldId == 11).Sum(m => m.AccountFieldValue);

            //model.GrossTotalHead = model.BasicPay + model.SpecialPay + model.DearnessAllowns + model.CompenseAllowns + model.HRAAllowns + model.CapitalAllowns + model.ConveyaceAllowns + model.WashingAllowns + model.GP + model.SectPay;

            //model.GPFSubs = ResultData.Where(m => m.AccountFieldId == 17).Sum(m => m.AccountFieldValue);
            //model.GPFAdvans = ResultData.Where(m => m.AccountFieldId == 18).Sum(m => m.AccountFieldValue);
            //model.HBA = ResultData.Where(m => m.AccountFieldId == 19).Sum(m => m.AccountFieldValue);
            //model.HBAInterest = ResultData.Where(m => m.AccountFieldId == 20).Sum(m => m.AccountFieldValue);
            //model.NCarScooterAdvance = ResultData.Where(m => m.AccountFieldId == 21).Sum(m => m.AccountFieldValue);
            //model.MCScooterInterest = ResultData.Where(m => m.AccountFieldId == 22).Sum(m => m.AccountFieldValue);
            //model.WarmclothAdvance = ResultData.Where(m => m.AccountFieldId == 23).Sum(m => m.AccountFieldValue);
            //model.WarmclothInterest = ResultData.Where(m => m.AccountFieldId == 24).Sum(m => m.AccountFieldValue);
            //model.LTCTAAdvance = ResultData.Where(m => m.AccountFieldId == 26).Sum(m => m.AccountFieldValue);
            //model.FestivalAdvance = ResultData.Where(m => m.AccountFieldId == 25).Sum(m => m.AccountFieldValue);
            //model.MiscelRecov = ResultData.Where(m => m.AccountFieldId == 27).Sum(m => m.AccountFieldValue);

            //model.TotalDeducA = model.GPFSubs + model.GPFAdvans + model.HBA + model.HBAInterest + model.NCarScooterAdvance + model.MCScooterInterest + model.WarmclothAdvance + model.WarmclothInterest + model.LTCTAAdvance + model.FestivalAdvance + model.MiscelRecov;

            //model.InsurFund = ResultData.Where(m => m.AccountFieldId == 31).Sum(m => m.AccountFieldValue);
            //model.SavingFund = ResultData.Where(m => m.AccountFieldId == 30).Sum(m => m.AccountFieldValue);
            //model.HouseRent = ResultData.Where(m => m.AccountFieldId == 32).Sum(m => m.AccountFieldValue);
            //model.PLI = ResultData.Where(m => m.AccountFieldId == 33).Sum(m => m.AccountFieldValue);
            //model.LIC = ResultData.Where(m => m.AccountFieldId == 34).Sum(m => m.AccountFieldValue);
            //model.IncomeTax = ResultData.Where(m => m.AccountFieldId == 35).Sum(m => m.AccountFieldValue);
            //model.Surcharge = ResultData.Where(m => m.AccountFieldId == 36).Sum(m => m.AccountFieldValue);
            //model.OtherBT = ResultData.Where(m => m.AccountFieldId == 37).Sum(m => m.AccountFieldValue);

            //model.TotalDeducB = model.InsurFund + model.SavingFund + model.HouseRent + model.PLI + model.LIC + model.IncomeTax + model.Surcharge + model.OtherBT;

            //model.BalanceAmount = model.GrossTotalHead - model.TotalDeducA;

            //model.NetAmount = model.BalanceAmount - model.TotalDeducB;

            var FL = (from SS in Tr2Cxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();
            var FAP = (from SS in Tr2Cxt.SiteSettings
                       where SS.SettingName == "SecureFileAccessingUrlPath"
                       select SS.SettingValue).FirstOrDefault();

            model.FilePath = FL;
            model.GetFilePath = FAP;

            return ResultData;
        }



        public static object GetSiteSettingd(object param)
        {
            tEmployeeAccountDetails model = param as tEmployeeAccountDetails;

            TR2Contex Tr2Cxt = new TR2Contex();
            var FL = (from SS in Tr2Cxt.SiteSettings
                      where SS.SettingName == "FileLocation"
                      select SS.SettingValue).FirstOrDefault();
            var FAP = (from SS in Tr2Cxt.SiteSettings
                       where SS.SettingName == "SecureFileAccessingUrlPath"
                       select SS.SettingValue).FirstOrDefault();

            model.FilePath = FL;
            model.GetFilePath = FAP;


            return model;
        }
        #endregion

        #region FORM-16
        public static object GetAllForm16(object param)
        {

            try
            {
                using (TR2Contex db = new TR2Contex())
                {
                    var data = (from AF in db.Form16
                                join ST in db.mStaff on AF.EmpId equals ST.StaffID
                                orderby AF.ID
                                select new
                                {
                                    Form16 = AF,
                                    StaffName = ST.StaffName,
                                    Designation = ST.Designation
                                }).ToList();

                    List<Form16> result = new List<Form16>();
                    if (data != null)
                    {
                        foreach (var item in data)
                        {
                            Form16 obj = new Form16();
                            obj = item.Form16;
                            obj.StaffName = item.StaffName;
                            obj.Designation = item.Designation;

                            result.Add(obj);
                        }


                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetNetAmount(object param)
        {

            try
            {
                tEmployeeAccountDetails model = param as tEmployeeAccountDetails;


                using (TR2Contex db = new TR2Contex())
                {
                    var AccountFieldIdList = (from AF in db.mAccountFields
                                              where AF.Type == "DUES"
                                              select new { Value = AF.Value }).ToList();
                    if (AccountFieldIdList != null)
                    {
                        int[] AccountFieldIds = new int[AccountFieldIdList.Count];
                        int i = 0;
                        foreach (var obj in AccountFieldIdList)
                        {
                            AccountFieldIds[i++] = obj.Value;
                        }

                        var data = (from EA in db.tEmployeeAccountDetails
                                    where EA.EmpId == model.EmpId && EA.finanacialYear == model.finanacialYear
                                    && (AccountFieldIds).Contains(EA.AccountFieldId)
                                    select EA).ToList().Sum(m => m.AccountFieldValue);
                        return data;
                    }
                    else
                    {
                        return 0;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }
        public static object SaveLoanDetails(object param)
        {
            try
            {
                using (TR2Contex db = new TR2Contex())
                {
                    TR2LoanDetail model = param as TR2LoanDetail;
                    db.TR2LoanDetail.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }

        }
        public static object SaveForm16(object param)
        {
            try
            {
                Form16 model = param as Form16;
                using (TR2Contex db = new TR2Contex())
                {

                    db.Form16.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.ID;
            }
            catch
            {
                throw;
            }

        }

        public static object GetForm16(object param)
        {
            try
            {
                Form16 model = param as Form16;
                using (TR2Contex db = new TR2Contex())
                {

                    var data = (from FO in db.Form16
                                where FO.EmpId == model.EmpId && FO.FinancialYear == model.FinancialYear
                                select FO).FirstOrDefault();
                    return data;
                }


            }
            catch
            {
                throw;
            }

        }
        public static object GetForm16ById(object param)
        {
            try
            {
                Form16 model = param as Form16;
                using (TR2Contex db = new TR2Contex())
                {

                    var data = (from FO in db.Form16
                                join ST in db.mStaff on FO.EmpId equals ST.StaffID
                                where FO.ID == model.ID
                                select new
                                {
                                    Form16 = FO,
                                    StaffName = ST.StaffName,
                                    Designation = ST.Designation
                                }).FirstOrDefault();

                    Form16 obj = new Form16();
                    if (data != null)
                    {
                        obj = data.Form16;
                        obj.Designation = obj.Designation;
                        obj.StaffName = obj.StaffName;
                    }
                    return obj;
                }


            }
            catch
            {
                throw;
            }

        }

        public static object GetLoanDetailByForm16Id(object param)
        {
            try
            {
                Form16 model = param as Form16;
                using (TR2Contex db = new TR2Contex())
                {

                    var data = (from FO in db.TR2LoanDetail
                                where FO.Form16Id == model.ID 
                                select FO).ToList();
                    return data;
                }


            }
            catch
            {
                throw;
            }

        }
        #endregion

        #region Rebete
        public static object GetRebete(object param)
        {

            try
            {
                tRebate model = param as tRebate;


                using (TR2Contex db = new TR2Contex())
                {

                    var rebate = (from AF in db.tRebate
                                  where AF.EmployeeID == model.EmployeeID && AF.FinancialYear == model.FinancialYear
                                  select AF).FirstOrDefault();
                    return rebate;



                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public static object SaveRebate(object param)
        {
            try
            {
                using (TR2Contex db = new TR2Contex())
                {
                    tRebate model = param as tRebate;
                    db.tRebate.Add(model);
                    db.SaveChanges();

                    db.Close();
                    return model;
                }

                return null;
            }
            catch
            {
                throw;
            }

        }
        public static object DeleteRebate(object param)
        {
            try
            {
                using (TR2Contex db = new TR2Contex())
                {
                    tRebate model = param as tRebate;

                    tRebate obj = db.tRebate.Single(m => m.Id == model.Id);
                    db.tRebate.Remove(obj);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }

        }


        public static object GetRebeteByForm16Id(object param)
        {

            try
            {
                tRebate model = param as tRebate;


                using (TR2Contex db = new TR2Contex())
                {

                    var rebate = (from AF in db.tRebate
                                  where AF.Form16Id == model.Form16Id// && AF.FinancialYear == model.FinancialYear
                                  select AF).FirstOrDefault();
                    return rebate;



                }
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }
        public static object GetRevateCountAmount(object param)
        {

            try
            {
                tRebate model = param as tRebate;


                using (TR2Contex db = new TR2Contex())
                {


                    var data = (from EA in db.tEmployeeAccountDetails
                                where EA.EmpId == model.EmployeeID && EA.finanacialYear == model.FinancialYear
                                && EA.AccountFieldId == model.AccountFID
                                select EA).ToList().Sum(m => m.AccountFieldValue);
                    return data;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        #endregion

    }


}