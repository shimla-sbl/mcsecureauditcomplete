﻿using SBL.DAL;
using SBL.Domain.Context.Assembly;
using SBL.Domain.Context.Ministry;
using SBL.DomainModel;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.ComplexModel.LinqKitSource;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Data.Entity.SqlServer;
using SBL.DomainModel.Models.Diaries;

namespace SBL.Domain.Context.Questions
{
    internal class QuestionsContext : DBBase<QuestionsContext>
    {
        public QuestionsContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<mEvent> Events { get; set; }

        public DbSet<ReporterDescriptionsLog> tReporterDescriptionLogs { get; set; }

        public DbSet<tStarredQuestionsHistory> tStarredQuestionsHistory { get; set; }

        public DbSet<tUnStarredQuestionsHistory> tUnStarredQuestionsHistory { get; set; }

        public DbSet<tQuestion> tQuestions { get; set; }

        public DbSet<tSubQuestion> tSubQuestions { get; set; }

        public DbSet<tQuestionType> tQuestionType { get; set; }

        public DbSet<mConstituency> mConstituency { get; set; }

        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }

        public DbSet<mMember> mMember { get; set; }

        public DbSet<mAssembly> mAssembly { get; set; }

        public DbSet<mDepartment> mDepartment { get; set; }

        public DbSet<mMinistryDepartment> mMinistryDepartments { get; set; }

        public DbSet<mSession> mSession { get; set; }
        public DbSet<mSessionDate> mSessionDate { get; set; }

        public DbSet<SiteSettings> mSiteSettings { get; set; }

        public DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }

        public DbSet<mMinistry> mMinistries { get; set; }

        public DbSet<mQuestionRules> mQuestionRules { get; set; }

        public DbSet<mNoticeType> mNoticeType { get; set; }

        public DbSet<tPaperLaidV> tPaperLaidV { get; set; }

        public DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }

        public DbSet<tMemberNotice> tMemberNotice { get; set; }

        public DbSet<mchangeDepartmentAuditTrail> mchangeDepartmentAuditTrail { get; set; }

        public DbSet<tBillRegister> tBillRegister { get; set; }
        public DbSet<mBills> mbills { get; set; }
        public DbSet<tAuditTrial> tAuditTrial { get; set; }
        public DbSet<tMailStatus> tMailStatus { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "GetQuestionSearchData":
                    {
                        return GetQuestionSearchData(param.Parameter);
                    }
                case "GetPendingQuestion":
                    {
                        return GetPendingQuestion(param.Parameter);
                    }
                case "GetPendingQuestionCount":
                    {
                        return GetPendingQuestionCount(param.Parameter);
                    }
                case "SubmitById":
                    {
                        return SubmitById(param.Parameter);
                    }
                case "DeleteQues":
                    {
                        return DeleteQues(param.Parameter);
                    }
                case "GetSubmitQuestion":
                    {
                        return GetSubmitQuestion(param.Parameter);
                    }
                case "GetSubmitQuestionCount":
                    {
                        return GetSubmitQuestionCount(param.Parameter);
                    }
                case "GetConstByMemberId":
                    {
                        return GetConstByMemberId(param.Parameter);
                    }
                case "GetQuestionID":
                    {
                        return GetQuestionID(param.Parameter);
                    }
                case "InsertNGetQid":
                    {
                        return InsertNGetQuestionID(param.Parameter);
                    }
                case "UpdateQuestionsbyVS":
                    {
                        return UpdateQuestionsbyVS(param.Parameter);
                    }
                case "CheckQuestionNo":
                    {
                        return CheckQuestionNo(param.Parameter);
                    }
                case "GetQuesForEditById":
                    {
                        return GetQuesForEditById(param.Parameter);
                    }
                case "GetDepartmentByMinistery":
                    {
                        return GetDepartmentByMinistery(param.Parameter);
                    }
                case "GetQuestionDetailsByID":
                    return GetQuestionDetailsByID(param.Parameter);

                case "GetQuestionDetailsDepartmentByID":

                    return GetQuestionDetailsDepartmentByID(param.Parameter);

                case "GetQuestionDetailsByDiaryNumber":
                    return GetQuestionDetailsByDiaryNumber(param.Parameter);

                case "GetQuestionDetailsDiaryNumber":
                    return GetQuestionDetailsDiaryNumber(param.Parameter);

                case "GetQuestionBySessionDate":
                    {
                        return GetQuestionBySessionDate(param.Parameter);
                    }
                case "GetQuestionBySessionDateWithPaper":
                    {
                        return GetQuestionBySessionDateWithPaper(param.Parameter);
                    }
                case "GetQuestionTypeByTypeId":
                    {
                        return GetQuestionTypeByTypeId(param.Parameter);
                    }
                case "GetAttachment":
                    {
                        return GetAttachment(param.Parameter);
                    }
                //Added by Badri
                case "GetmQuestionRules":
                    {
                        return GetmQuestionRules();
                    }
                case "SaveQuestionRule":
                    {
                        return SaveQuestionRule(param.Parameter);
                    }
                case "EditQuestionRules":
                    {
                        return EditQuestionRules(param.Parameter);
                    }
                case "UpdateQuestionRule":
                    {
                        return UpdateQuestionRule(param.Parameter);
                    }
                case "UpdatePaperLaid":
                    {
                        UpdatePaperLaid(param.Parameter);
                        break;
                    }

                case "DeleteQuestionRule":
                    {
                        return DeleteQuestionRule(param.Parameter);
                    }
                case "GetQuestionBySessionDateAndLaidType":
                    {
                        return GetQuestionBySessionDateAndLaidType(param.Parameter);
                    }
                case "GetDraftAndApprovedQuestionBySessionDateAndLaidType":
                    {
                        return GetDraftAndApprovedQuestionBySessionDateAndLaidType(param.Parameter);
                    }

                case "GetAssQuestionFromID":
                    {
                        return GetAssQuestionFromID(param.Parameter);
                    }
                case "GetDraftPDFQuestionFromID":
                    {
                        return GetDraftPDFQuestionFromID(param.Parameter);
                    }

                case "GetQuestionBySessionDateAndLatestFile":
                    {
                        return GetQuestionBySessionDateAndLatestFile(param.Parameter);
                    }
                case "GetOtherpaperPaperType":
                    {
                        return GetOtherpaperPaperType(param.Parameter);
                    }
                case "GetAssOtherfileFromID":
                    {
                        return GetAssOtherfileFromID(param.Parameter);
                    }

                //**********************************
                case "GetQuestionSubjects":
                    {
                        return GetQuestionSubjects();
                    }
                case "GetQuestionSubjects_key":
                    {
                        return GetQuestionSubjects_key(param.Parameter);
                    }
                case "GetDebatesSubjects":
                    {
                        return GetDebatesSubjects();
                    }
                case "GetQuestionTypes":
                    {
                        return GetQuestionTypes();
                    }
                case "GetSearchedQusetions":
                    {
                        return GetSearchedQuestions(param.Parameter);
                    }
                case "GetSearchedQuestionsHistory":
                    {
                        return GetSearchedQuestionsHistory(param.Parameter);
                    }
                case "GetQuestionFilePathByQuestionID":
                    {
                        return GetQuestionFilePathByQuestionID(param.Parameter);
                    }
                case "GetSearchedDebates":
                    {
                        return GetSearchedDebates(param.Parameter);
                    }
                case "StartedQuestioncount": { return StartedQuestioncount(param.Parameter); }

                case "StartedQuestioncountForAll": { return StartedQuestioncountForAll(param.Parameter); }
                case "getNoticeListByMemberCode": { return getNoticeListByMemberCode(param.Parameter); }
                case "getNoticeListForAll": { return getNoticeListForAll(param.Parameter); }
                case "getQuestionListByMemberCode": { return getQuestionListByMemberCode(param.Parameter); }
                case "getQuestionListForAll": { return getQuestionListForAll(param.Parameter); }
                case "getQuestionByID": { return getQuestionByID(param.Parameter); }
                case "GetAssQuestionDocFromID": { return GetAssQuestionDocFromID(param.Parameter); }
                case "GetOtherFileSDocFromID": { return GetOtherFileSDocFromID(param.Parameter); }

                case "GetHistoryDetailsDiaryNumber":
                    return GetHistoryDetailsDiaryNumber(param.Parameter);

                case "CompleteStatusReport": { return CompleteStatusReport(param.Parameter); }

                case "DepartmentWiseQuestionNoticeReport": { return DepartmentWiseQuestionNoticeReport(param.Parameter); }

                case "GetQuestionSubmittedListByMemberId": { return GetQuestionSubmittedListByMemberId(param.Parameter); }
                case "GetQuestionPendingListByMemberId": { return GetQuestionPendingListByMemberId(param.Parameter); }

                    //For eMail
                case "GetQuestionBySessionDateForEmail": { return GetQuestionBySessionDateForEmail(param.Parameter); }
                case "GetQDetailsBySessionDatenMemId": { return GetQDetailsBySessionDatenMemId(param.Parameter); }
                case "GeteMailByMemberId": { return GeteMailByMemberId(param.Parameter); }
                case "Addemailstatus": { return Addemailstatus(param.Parameter); }

            }

            return null;
        }

        //Added by Badri

        private static object DeleteQuestionRule(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (QuestionsContext db = new QuestionsContext())
            {
                // mNoticeType parameter = param as mNoticeType;
                int Id = Convert.ToInt32(param);
                mQuestionRules DelQuestions = (from e in db.mQuestionRules where e.QuestionRuleId == Id select e).SingleOrDefault();
                if (DelQuestions != null)
                {
                    DelQuestions.IsDeleted = true;
                }
                //  db.mQuestionRules.Remove(DelQuestions);
                db.SaveChanges();
                db.Close();
            }
            return GetmQuestionRules();
        }

        private static object UpdateQuestionRule(object param)
        {
            QuestionsContext db = new QuestionsContext();
            mQuestionRules model = param as mQuestionRules;
            model.CreatedDate = DateTime.Now;
            model.ModifiedDate = DateTime.Now;
            db.mQuestionRules.Attach(model);
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
            db.Close();
            return GetmQuestionRules();
        }

        private static void UpdatePaperLaid(object param)
        {
            QuestionsContext db = new QuestionsContext();

            tPaperLaidV model = param as tPaperLaidV;
            model = (from m in db.tPaperLaidV where m.PaperLaidId == model.PaperLaidId select m).SingleOrDefault();
            if (model.IsLaid == true)
            {
                model.IsLaid = false;
            }
            else
            {
                model.IsLaid = true;
            }
            db.tPaperLaidV.Attach(model);
            db.Entry(model).State = EntityState.Modified;
            db.SaveChanges();
            updatedQuestionsHistoryTable(model.PaperLaidId, true);
            db.Close();
            // return GetmQuestionRules();
        }

        private static void updatedQuestionsHistoryTable(long paperLaidID, bool paperLaid)
        {
            using (QuestionsContext db = new QuestionsContext())
            {
                var tQuestion = (from question in db.tQuestions
                                 where question.PaperLaidId == paperLaidID
                                 select question
                                   ).FirstOrDefault();
                if (tQuestion != null)
                {
                    if (paperLaid)
                    {
                        if (!(checkQuestionsHistoryTable(tQuestion.QuestionNumber.Value.ToString(), tQuestion.SessionID, tQuestion.AssemblyID, tQuestion.QuestionType) > 0))
                        {
                            if (tQuestion.QuestionType == 1)
                            {
                                tStarredQuestionsHistory sQuestion = new tStarredQuestionsHistory();
                                sQuestion.AssemblyID = tQuestion.AssemblyID;
                                sQuestion.SessionID = tQuestion.SessionID;
                                sQuestion.AttachmentName = tQuestion.QuestionNumber + ".pdf";
                                sQuestion.EventID = "3";
                                sQuestion.QuestionNo = tQuestion.QuestionNumber.Value.ToString();
                                sQuestion.OriginalDescription = tQuestion.Subject;
                                sQuestion.EditedDescription = tQuestion.Subject;
                                sQuestion.QuestionDetails = tQuestion.MainQuestion;
                                sQuestion.IsLaid = true;
                                sQuestion.LoggedUSer = "admin";
                                sQuestion.LoggedTime = DateTime.Now;
                                sQuestion.SessionDate = (from tSessDt in db.mSessionDate
                                                         where tSessDt.Id == tQuestion.SessionDateId
                                                         select tSessDt.SessionDate).FirstOrDefault();
                                sQuestion.AskedBy = tQuestion.IsClubbed.HasValue ? (tQuestion.IsClubbed.Value ? tQuestion.ReferenceMemberCode : tQuestion.MemberID.ToString()) : tQuestion.MemberID.ToString();
                                sQuestion.MinisterId = (from tMin in db.mMinsitryMinister
                                                        where tMin.MinsitryMinistersID == tQuestion.MinistryId
                                                        select tMin.MinistryID).FirstOrDefault();
                                sQuestion.DepartmentId = (from mDept in db.mMinistryDepartments
                                                          where mDept.MinistryID == tQuestion.MinistryId
                                                          &&
                                                          mDept.DeptID == tQuestion.DepartmentID
                                                          &&
                                                          mDept.IsDeleted == null
                                                          select mDept.MinistryDepartmentsID
                                                          ).FirstOrDefault();

                                sQuestion.IsOldData = 1;
                                sQuestion.IsPostponed = tQuestion.DeActivateFlag == "RF" ? true : false;
                                sQuestion.IsFreeze = true;
                                db.tStarredQuestionsHistory.Add(sQuestion);

                            }
                            else
                            {
                                tUnStarredQuestionsHistory unQuestion = new tUnStarredQuestionsHistory();
                                unQuestion.AssemblyID = tQuestion.AssemblyID;
                                unQuestion.SessionID = tQuestion.SessionID;
                                unQuestion.AttachmentName = tQuestion.QuestionNumber + ".pdf";
                                unQuestion.EventID = "3";
                                unQuestion.QuestionNo = tQuestion.QuestionNumber.Value.ToString();
                                unQuestion.OriginalDescription = tQuestion.Subject;
                                unQuestion.EditedDescription = tQuestion.Subject;
                                unQuestion.QuestionDetails = tQuestion.MainQuestion;
                                unQuestion.IsLaid = true;
                                unQuestion.LoggedUSer = "admin";
                                unQuestion.LoggedTime = DateTime.Now;
                                unQuestion.SessionDate = (from tSessDt in db.mSessionDate
                                                          where tSessDt.Id == tQuestion.SessionDateId
                                                          select tSessDt.SessionDate).FirstOrDefault();
                                unQuestion.AskedBy = tQuestion.IsClubbed.HasValue ? (tQuestion.IsClubbed.Value ? tQuestion.ReferenceMemberCode : tQuestion.MemberID.ToString()) : tQuestion.MemberID.ToString();
                                unQuestion.MinisterId = (from tMin in db.mMinsitryMinister
                                                         where tMin.MinsitryMinistersID == tQuestion.MinistryId
                                                         select tMin.MinistryID).FirstOrDefault();
                                unQuestion.DepartmentId = (from mDept in db.mMinistryDepartments
                                                           where mDept.MinistryID == tQuestion.MinistryId
                                                           &&
                                                           mDept.DeptID == tQuestion.DepartmentID
                                                           &&
                                                           mDept.IsDeleted == null
                                                           select mDept.MinistryDepartmentsID
                                                          ).FirstOrDefault();

                                unQuestion.IsOldData = 1;
                                unQuestion.IsPostponed = tQuestion.DeActivateFlag == "RF" ? true : false;
                                unQuestion.IsFreeze = true;
                                db.tUnStarredQuestionsHistory.Add(unQuestion);

                            }
                        }
                        else
                        {
                            
                           deleteQuestionsHistoryTable(tQuestion.QuestionNumber.Value.ToString(), tQuestion.SessionID, tQuestion.AssemblyID, tQuestion.QuestionType);
                            
                        }
                    }
                    
                }
                else
                {
                    checkBillsAndUpdate(paperLaidID, paperLaid);
                }
                db.SaveChanges();
                db.Close();
            }
            
        }

        private static int checkQuestionsHistoryTable(string questionNumber, int session, int assembly, int qType)
        {
            if (qType == 1)
            {
                using (QuestionsContext db = new QuestionsContext())
                {
                    return (from sQuestion in db.tStarredQuestionsHistory
                            where sQuestion.SessionID == session
                            &&
                            sQuestion.AssemblyID == assembly
                            &&
                            sQuestion.QuestionNo == questionNumber
                            select sQuestion
                                    ).Count();
                }
            }
            else
            {
                using (QuestionsContext db = new QuestionsContext())
                {
                    return (from sQuestion in db.tUnStarredQuestionsHistory
                            where sQuestion.SessionID == session
                            &&
                            sQuestion.AssemblyID == assembly
                            &&
                            sQuestion.QuestionNo == questionNumber
                            select sQuestion
                                    ).Count();
                }
            }
        }

        private static void deleteQuestionsHistoryTable(string questionNumber, int session, int assembly, int qType)
        {
            if (qType == 1)
            {
                using (QuestionsContext db = new QuestionsContext())
                {
                    var sQuestions = (from sQuestion in db.tStarredQuestionsHistory
                                      where sQuestion.SessionID == session
                                      &&
                                      sQuestion.AssemblyID == assembly
                                      &&
                                      sQuestion.QuestionNo == questionNumber
                                      select sQuestion
                                    ).FirstOrDefault();
                    db.Entry(sQuestions).State = EntityState.Deleted;
                    db.SaveChanges();
                    db.Close();
                }
            }
            else
            {
                using (QuestionsContext db = new QuestionsContext())
                {
                    var unQuestions = (from sQuestion in db.tUnStarredQuestionsHistory
                                       where sQuestion.SessionID == session
                                       &&
                                       sQuestion.AssemblyID == assembly
                                       &&
                                       sQuestion.QuestionNo == questionNumber
                                       select sQuestion
                                    ).FirstOrDefault();
                    db.Entry(unQuestions).State = EntityState.Deleted;
                    db.SaveChanges();
                    db.Close();
                }
            }
        }

        private static void checkBillsAndUpdate(long paperLaidID, bool status)
        {
            using (QuestionsContext context = new QuestionsContext())
            {
                var result = (from bill in context.tBillRegister
                              join mbill in context.mbills
                              on bill.BillNumber equals mbill.BillNo
                              where bill.PaperLaidId == paperLaidID
                              select mbill
                                ).FirstOrDefault();
                if (result != null)
                {
                    result.IsLaid = status;
                    context.Entry(result).State = EntityState.Modified;
                    context.SaveChanges();
                }

            }
        }

        private static object EditQuestionRules(object param)
        {
            QuestionsContext db = new QuestionsContext();
            int Id = Convert.ToInt32(param);
            var Res = (from e in db.mQuestionRules where e.QuestionRuleId == Id select e).FirstOrDefault();
            return Res;
        }

        private static object SaveQuestionRule(object param)
        {
            QuestionsContext db = new QuestionsContext();
            mQuestionRules model = param as mQuestionRules;
            model.CreatedDate = DateTime.Now;
            model.ModifiedDate = DateTime.Now;
            db.mQuestionRules.Add(model);
            db.SaveChanges();
            return model;
        }

        private static object GetmQuestionRules()
        {
            //QuestionsContext db = new QuestionsContext();
            //var query =(from e in db.tQuestionRules select e
            using (QuestionsContext context = new QuestionsContext())
            {
                //var query = from e in context.mQuestionRules select e;
                var query = (from e in context.mQuestionRules
                             orderby e.QuestionRuleId descending
                             where e.IsDeleted == null
                             select e);
                return query.ToList();
            }
        }

        //**********************************

        public static object GetQuestionDetailsDiaryNumber(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestionModel questionModel = param as tQuestionModel;
            var query = (from tQ in db.tQuestions
                         where (tQ.DiaryNumber == questionModel.DiaryNumber)
                         select new tQuestionModel
                         {
                             IsHindi = tQ.IsHindi,
                             PaperLaidId = tQ.PaperLaidId,
                             QuestionID = tQ.QuestionID,
                             SubmittedBy = tQ.SubmittedBy,
                             AssemblyId = tQ.AssemblyID,
                             SessionId = tQ.SessionID,
                             MemberId = tQ.MemberID,
                             MinisterId = tQ.MinisterID,
                             StatusId = tQ.MQStatus,
                             DepartmentId = tQ.DepartmentID,
                             QtypeId = tQ.QuestionType,
                             ConstituencyName = tQ.ConstituencyName,
                             ConstituencyNo = tQ.ConstituencyNo,
                             Subject = tQ.Subject,
                             MainQuestion = tQ.MainQuestion,
                             QuestionPriority = tQ.Priority,
                             BracketedWithDNo = tQ.BracketedWithDNo,
                             DateforReply = tQ.DeptDateOfReply,
                             Priority = tQ.Priority,
                             QuestionNumber = tQ.QuestionNumber,
                             Date = tQ.IsFixedDate,
                             IsSubmitted = tQ.IsSubmitted,
                             DiaryNumber = tQ.DiaryNumber,
                             MinisterN = (from minister in db.mMinsitryMinister
                                          where minister.MinistryID == tQ.MinistryId
                                          select minister.MinisterName).FirstOrDefault(),
                             MinisterN_Hindi = "",
                             MemberN = (from member in db.mMember
                                        where member.MemberCode == tQ.MemberID
                                        select member.Name).FirstOrDefault(),
                             MemberN_Hindi = "",
                             CDiaryNumber = "",
                             DepartmentN = (from dept in db.mDepartment
                                            where dept.deptId == tQ.DepartmentID
                                            select dept.deptname).FirstOrDefault(),
                             ReferenceMemberCode = tQ.ReferenceMemberCode,
                             IsClubbed = tQ.IsClubbed,
                             MergeDiaryNo = tQ.MergeDiaryNo,
                             IsFinalApproved = tQ.IsFinalApproved,
                             IsBracket = tQ.IsBracket
                         }).ToList();
            questionModel.objQuestList1 = query;

            List<tQuestionModel> returnToCaller = new List<tQuestionModel>();

            foreach (var val in query)
            {
                string s = val.ReferenceMemberCode;
                string MDiary = val.MergeDiaryNo;

                if (MDiary != null && MDiary != "")
                {
                    string[] values = MDiary.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        string MDiaryNum = (values[i]);

                        var MenuList = (from dept in db.tQuestions where dept.DiaryNumber == MDiaryNum select dept).ToList();

                        // tempModel.DiaryNumList = (from dept in db.tQuestions where dept.DiaryNumber == MDiaryNum select dept).ToList();

                        foreach (var item in MenuList)
                        {
                            tQuestionModel tempModel = new tQuestionModel();
                            tempModel.DiaryNumber = item.DiaryNumber;

                            returnToCaller.Add(tempModel);
                        }
                    }
                }

                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item = (from questions in db.tQuestions
                                    select new QuestionModelCustom
                                    {
                                        MemberName = (from mc in db.mMember where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    }).FirstOrDefault();

                        if (string.IsNullOrEmpty(val.RMemberName))
                        {
                            val.RMemberName = item.MemberName;
                        }
                        else
                        {
                            val.RMemberName = val.RMemberName + "," + item.MemberName;
                        }
                    }
                }

                questionModel.DiaryNumList = returnToCaller;
            }

            return questionModel;
        }

        public static object GetQuestionDetailsByDiaryNumber(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestionModel questionModel = param as tQuestionModel;
            var query = (from tQ in db.tQuestions
                         where (tQ.DiaryNumber == questionModel.DiaryNumber)
                         select new tQuestionModel
                         {
                             IsHindi = tQ.IsHindi,
                             PaperLaidId = tQ.PaperLaidId,
                             QuestionID = tQ.QuestionID,
                             SubmittedBy = tQ.SubmittedBy,
                             AssemblyId = tQ.AssemblyID,
                             SessionId = tQ.SessionID,
                             MemberId = tQ.MemberID,
                             MinisterId = tQ.MinisterID,
                             StatusId = tQ.MQStatus,
                             DepartmentId = tQ.DepartmentID,
                             SubmittedDate = tQ.SubmittedDate,
                             QtypeId = tQ.QuestionType,
                             ConstituencyName = tQ.ConstituencyName,
                             ConstituencyNo = tQ.ConstituencyNo,
                             Subject = tQ.Subject,

                             MainQuestion = tQ.MainQuestion,
                             QuestionPriority = tQ.Priority,
                             DateforReply = tQ.DeptDateOfReply,
                             Priority = tQ.Priority,
                             QuestionNumber = tQ.QuestionNumber,
                             Date = tQ.IsFixedDate,
                             IsAcknowledgmentDate = tQ.IsAcknowledgmentDate,
                             IsSubmitted = tQ.IsSubmitted,
                             DiaryNumber = tQ.DiaryNumber,
                             MinisterN = (from minister in db.mMinsitryMinister
                                          where minister.MinistryID == tQ.MinistryId
                                          select minister.MinisterName).FirstOrDefault(),
                             MinisterN_Hindi = "",
                             MemberN = (from member in db.mMember
                                        where member.MemberCode == tQ.MemberID
                                        select member.Name).FirstOrDefault(),
                             MemberN_Hindi = "",
                             CDiaryNumber = "",
                             DepartmentN = (from dept in db.mDepartment
                                            where dept.deptId == tQ.DepartmentID
                                            select dept.deptname).FirstOrDefault(),
                             ReferenceMemberCode = tQ.ReferenceMemberCode,
                             IsClubbed = tQ.IsClubbed,
                             MergeDiaryNo = tQ.MergeDiaryNo,
                             IsFinalApproved = tQ.IsFinalApproved,
                             IsBracket = tQ.IsBracket
                         }).ToList();
            questionModel.objQuestList1 = query;

            List<tQuestionModel> returnToCaller = new List<tQuestionModel>();

            foreach (var val in query)
            {
                var SubmittedDateString = Convert.ToDateTime(val.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                var AcknowledgmentDateString = Convert.ToDateTime(val.IsAcknowledgmentDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                string s = val.ReferenceMemberCode;
                string MDiary = val.MergeDiaryNo;

                if (MDiary != null && MDiary != "")
                {
                    string[] values = MDiary.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        string MDiaryNum = (values[i]);

                        var MenuList = (from dept in db.tQuestions where dept.DiaryNumber == MDiaryNum select dept).ToList();

                        // tempModel.DiaryNumList = (from dept in db.tQuestions where dept.DiaryNumber == MDiaryNum select dept).ToList();

                        foreach (var item in MenuList)
                        {
                            tQuestionModel tempModel = new tQuestionModel();
                            tempModel.DiaryNumber = item.DiaryNumber;

                            returnToCaller.Add(tempModel);
                        }
                    }
                }

                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item = (from questions in db.tQuestions
                                    select new QuestionModelCustom
                                    {
                                        MemberName = (from mc in db.mMember where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    }).FirstOrDefault();

                        if (string.IsNullOrEmpty(val.RMemberName))
                        {
                            val.RMemberName = item.MemberName;
                        }
                        else
                        {
                            val.RMemberName = val.RMemberName + "," + item.MemberName;
                        }
                    }
                }

                questionModel.DiaryNumList = returnToCaller;
                questionModel.SubmittedDateString = SubmittedDateString;
                questionModel.AcknowledgmentDateString = AcknowledgmentDateString;
            }

            return questionModel;
        }

        public static object GetQuestionDetailsDepartmentByID(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestionModel questionModel = param as tQuestionModel;
            var query = (from tQ in db.tQuestions
                         where (tQ.QuestionID == questionModel.QuestionID)
                         select new tQuestionModel
                         {
                             IsHindi = tQ.IsHindi,
                             PaperLaidId = tQ.PaperLaidId,
                             QuestionID = tQ.QuestionID,
                             SubmittedBy = tQ.SubmittedBy,
                             AssemblyId = tQ.AssemblyID,
                             SessionId = tQ.SessionID,
                             MemberId = tQ.MemberID,
                             MinisterId = tQ.MinisterID,
                             StatusId = tQ.MQStatus,
                             DepartmentId = tQ.DepartmentID,
                             SubmittedDate = tQ.SubmittedDate,
                             QtypeId = tQ.QuestionType,
                             ConstituencyName = tQ.ConstituencyName,
                             ConstituencyNo = tQ.ConstituencyNo,
                             Subject = tQ.Subject,
                             IsAcknowledgmentDate = tQ.IsAcknowledgmentDate,
                             MainQuestion = tQ.MainQuestion,
                             QuestionPriority = tQ.Priority,
                             DateforReply = tQ.DeptDateOfReply,
                             Priority = tQ.Priority,
                             QuestionNumber = tQ.QuestionNumber,
                             Date = tQ.IsFixedDate,
                             IsSubmitted = tQ.IsSubmitted,
                             DiaryNumber = tQ.DiaryNumber,
                             MinisterN = (from minister in db.mMinsitryMinister
                                          where minister.MinistryID == tQ.MinistryId
                                          select minister.MinisterName).FirstOrDefault(),
                             MinisterN_Hindi = "",
                             MemberN = (from member in db.mMember
                                        where member.MemberCode == tQ.MemberID
                                        select member.Name).FirstOrDefault(),
                             MemberN_Hindi = "",
                             CDiaryNumber = "",
                             DepartmentN = (from dept in db.mDepartment
                                            where dept.deptId == tQ.DepartmentID
                                            select dept.deptname).FirstOrDefault(),
                             ReferenceMemberCode = tQ.ReferenceMemberCode,
                             IsClubbed = tQ.IsClubbed,
                             MergeDiaryNo = tQ.MergeDiaryNo,
                             IsFinalApproved = tQ.IsFinalApproved,
                             IsBracket = tQ.IsBracket
                         }).ToList();
            questionModel.objQuestList1 = query;

            List<tQuestionModel> returnToCaller = new List<tQuestionModel>();

            foreach (var val in query)
            {
                var SubmittedDateString = Convert.ToDateTime(val.SubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                var AcknowledgmentDateString = Convert.ToDateTime(val.IsAcknowledgmentDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                string s = val.ReferenceMemberCode;
                string MDiary = val.MergeDiaryNo;

                if (MDiary != null && MDiary != "")
                {
                    string[] values = MDiary.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        string MDiaryNum = (values[i]);

                        var MenuList = (from dept in db.tQuestions where dept.DiaryNumber == MDiaryNum select dept).ToList();

                        // tempModel.DiaryNumList = (from dept in db.tQuestions where dept.DiaryNumber == MDiaryNum select dept).ToList();

                        foreach (var item in MenuList)
                        {
                            tQuestionModel tempModel = new tQuestionModel();
                            tempModel.DiaryNumber = item.DiaryNumber;

                            returnToCaller.Add(tempModel);
                        }
                    }
                }

                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item = (from questions in db.tQuestions
                                    select new QuestionModelCustom
                                    {
                                        MemberName = (from mc in db.mMember where mc.MemberCode == MemId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                    }).FirstOrDefault();

                        if (string.IsNullOrEmpty(val.RMemberName))
                        {
                            val.RMemberName = item.MemberName;
                        }
                        else
                        {
                            val.RMemberName = val.RMemberName + "," + item.MemberName;
                        }
                    }
                }

                questionModel.DiaryNumList = returnToCaller;
                questionModel.SubmittedDateString = SubmittedDateString;
                questionModel.AcknowledgmentDateString = AcknowledgmentDateString;
            }

            return questionModel;
        }

        private static object GetQuestionDetailsByID(object param)
        {
            try
            {
                QuestionsContext questionContext = new QuestionsContext();

                tQuestionModel questionModel = param as tQuestionModel;
                var query = (from tQ in questionContext.tQuestions
                             where (tQ.QuestionID == questionModel.QuestionID)
                             select new tQuestionModel
                             {
                                 IsHindi = tQ.IsHindi,
                                 PaperLaidId = tQ.PaperLaidId,
                                 QuestionID = tQ.QuestionID,
                                 SubmittedBy = tQ.SubmittedBy,
                                 AssemblyId = tQ.AssemblyID,
                                 SessionId = tQ.SessionID,
                                 MemberId = tQ.MemberID,
                                 MinisterId = tQ.MinisterID,
                                 StatusId = tQ.MQStatus,
                                 DepartmentId = tQ.DepartmentID,
                                 QtypeId = tQ.QuestionType,
                                 ConstituencyName = tQ.ConstituencyName,
                                 ConstituencyNo = tQ.ConstituencyNo,
                                 Subject = tQ.Subject,
                                 MainQuestion = tQ.MainQuestion,
                                 QuestionPriority = tQ.Priority,
                                 DateforReply = tQ.DeptDateOfReply,
                                 Priority = tQ.Priority,
                                 QuestionNumber = tQ.QuestionNumber,
                                 Date = tQ.IsFixedDate,
                                 IsSubmitted = tQ.IsSubmitted,
                                 DiaryNumber = tQ.DiaryNumber,

                                 MinisterN = (from minister in questionContext.mMinsitryMinister
                                              where minister.MinistryID == tQ.MinistryId
                                              select minister.MinisterName).FirstOrDefault(),
                                 MinisterN_Hindi = "",
                                 MemberN = (from member in questionContext.mMember
                                            where member.MemberCode == tQ.MemberID
                                            select member.Name).FirstOrDefault(),
                                 MemberN_Hindi = "",
                                 DepartmentN = (from dept in questionContext.mDepartment
                                                where dept.deptId == tQ.DepartmentID
                                                select dept.deptname).FirstOrDefault()
                             }).FirstOrDefault();

                return query;
            }
            catch (Exception ex)
            {
                //string errorMessage = ex.Message;
                //errorMessage = AppendNewLine(errorMessage);
                //errorMessage = errorMessage + ex.Source;

                //string path = @"C:\\errorLog.txt";
                //if (!File.Exists(path))
                //{
                //    File.Create(path);
                //    TextWriter tw = new StreamWriter(path);
                //    tw.WriteLine(errorMessage);
                //    tw.Close();
                //}
                //else if (File.Exists(path))
                //{
                //    TextWriter tw = new StreamWriter(path);
                //    tw.WriteLine(errorMessage);
                //    tw.Close();
                //}
                return null;
            }
        }

        private static string AppendNewLine(string Input)
        {
            return Input + "\r\n\r\n";
        }

        private static object GetDepartmentByMinistery(object param)
        {
            MinisteryContext DBContext = new MinisteryContext();

            if (param == null)
            {
                return null;
            }

            mMinistry obj = param as mMinistry;
            var departmentList = (from ministryDept in DBContext.mMinistryDepartments
                                  join dept in DBContext.mDepartments on ministryDept.DeptID equals dept.deptId
                                  where ministryDept.MinistryID == obj.MinistryID
                                  select new tMemberNotice
                                  {
                                      DepartmentId = dept.deptId,
                                      DepartmentName = dept.deptname
                                  }).ToList();

            return departmentList;
        }

        private static object GetQuestionSearchData(object param)
        {
            tQuestionModel qs = param as tQuestionModel;
            QuestionsContext questionContext = new QuestionsContext();
            AssemblyContext assemblyContext = new AssemblyContext();

            qs.tQuestionType = (from qt in questionContext.tQuestionType
                                select qt).ToList();

            qs.mAssembly = (from a in assemblyContext.mAssemblies
                            orderby a.AssemblyID descending
                            select a).ToList();
            mAssembly obj = new mAssembly();
            obj.AssemblyID = 0;
            obj.AssemblyName = "Select Assembly";
            qs.mAssembly.Add(obj);

            tQuestionType obj3 = new tQuestionType();
            obj3.QuestionTypeId = 0;
            obj3.QuestionTypeName = "Select Question Type";
            qs.tQuestionType.Add(obj3);

            return qs;
        }

        private static object GetPendingQuestion(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel obj = param as tQuestionModel;

            int? Questtype = null;
            int? QuestionNum = null;
            if (obj.QuestionType != null)
            {
                Questtype = Convert.ToInt32(obj.QuestionType);
            }
            if (obj.QuestionNumber != 0)
            {
                QuestionNum = obj.QuestionNumber;
            }
            var Qquery = (from tQ in qCtxDB.tQuestions
                          join QType in qCtxDB.tQuestionType on tQ.QuestionType equals QType.QuestionTypeId
                          join Dept in qCtxDB.mDepartment on tQ.DepartmentID equals Dept.deptId
                          join MM in qCtxDB.mMember on tQ.MemberID equals MM.MemberCode
                          where (tQ.IsSubmitted == null)
                           && (!QuestionNum.HasValue || tQ.QuestionNumber == QuestionNum)
                         && (!obj.IsFixedDate.HasValue || tQ.IsFixedDate.Value.Year == obj.IsFixedDate.Value.Year && tQ.IsFixedDate.Value.Month == obj.IsFixedDate.Value.Month && tQ.IsFixedDate.Value.Day == obj.IsFixedDate.Value.Day)
                         && (!Questtype.HasValue || tQ.QuestionType == Questtype)
                          orderby tQ.QuestionID ascending
                          select new tQuestionModel
                          {
                              QuestionType = QType.QuestionTypeName,
                              QuestionNumber = tQ.QuestionNumber,
                              Subject = tQ.Subject,
                              MemberN = MM.Name,
                              QuestionID = tQ.QuestionID,
                              SessionId = tQ.SessionID,
                              DepartmentN = Dept.deptname,
                              MinisterId = tQ.MinisterID,
                              DepartmentId = tQ.DepartmentID,
                              QuestionFixDate = tQ.IsFixedDate,
                              IsSubmitted = tQ.IsSubmitted,
                              TableName = "QOnline",
                              MinisterName = (from mn in qCtxDB.mMinsitryMinister where mn.MinistryID == tQ.MinisterID select mn.MinisterName).FirstOrDefault()
                          }).Skip((obj.PageIndex - 1) * obj.PAGE_SIZE).Take(obj.PAGE_SIZE).ToList();

            List<tQuestionModel> asList = Qquery.ToList();
            //return asList;

            //foreach (var item in asList)
            //{
            //    if (item.MinisterId != 0)
            //    {
            //        if (item.IsHindi == true)
            //        {
            //            var MinNameH = (from M in qCtxDB.mMember
            //                            where M.MemberID == item.MinisterId
            //                            select M.Name_Hindi).FirstOrDefault();
            //            item.MinisterN = MinNameH;
            //        }
            //        else
            //        {
            //            var MinName = (from M in qCtxDB.mMember
            //                           where M.MemberID == item.MinisterId
            //                           select M.Name).FirstOrDefault();
            //            item.MinisterN = MinName;
            //        }
            //    }
            //    if (item.DepartmentId != "" && item.DepartmentId != null)
            //    {
            //        if (item.IsHindi == true)
            //        {
            //            var DepNameH = (from D in qCtxDB.mDepartment
            //                            where D.deptId == item.DepartmentId
            //                            select D.deptname_hindi).FirstOrDefault();
            //            item.DepartmentN = DepNameH;
            //        }
            //        else
            //        {
            //            var DepName = (from D in qCtxDB.mDepartment
            //                           where D.deptId == item.DepartmentId
            //                           select D.deptname).FirstOrDefault();
            //            item.DepartmentN = DepName;
            //        }
            //    }
            //    if (item.SessionId != 0)
            //    {
            //        if (item.IsHindi == true)
            //        {
            //            var SessionNameH = (from S in qCtxDB.mSession
            //                                where S.Id == item.SessionId
            //                                select S.Name_Local).FirstOrDefault();
            //            item.SessionN = SessionNameH;
            //        }
            //        else
            //        {
            //            var SessionName = (from S in qCtxDB.mSession
            //                               where S.Id == item.SessionId
            //                               select S.Name).FirstOrDefault();
            //            item.SessionN = SessionName;
            //        }
            //    }

            //}

            return asList;
        }

        private static object GetPendingQuestionCount(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel obj = param as tQuestionModel;
            //int? AssemblyId = null; int? SessionId = null; int? MemberId = null; int? MinisterId = null; int? Qtype = null;
            //int? StatusId = null;

            //if (obj.AssemblyId != 0)
            //{
            //    AssemblyId = obj.AssemblyId;
            //}
            //if (obj.SessionId != 0)
            //{
            //    SessionId = obj.SessionId;
            //}
            //if (obj.MemberId != 0)
            //{
            //    MemberId = obj.MemberId;
            //}
            //if (obj.MinisterId != 0)
            //{
            //    MinisterId = obj.MinisterId;
            //}
            //if (obj.QtypeId != 0)
            //{
            //    Qtype = obj.QtypeId;
            //}
            //if (obj.StatusId != 0)
            //{
            //    StatusId = obj.StatusId;
            //}

            var Qquery = (from tQ in qCtxDB.tQuestions
                          //join Assem in qCtxDB.mAssembly on tQ.AssemblyID equals Assem.AssemblyID
                          //join S in qCtxDB.mSession on tQ.SessionID equals S.SessionCode
                          //join M in qCtxDB.mMember on tQ.MemberID equals M.MemberID
                          //join QType in qCtxDB.tQuestionType on tQ.QuestionType equals QType.QuestionTypeId
                          //join MM in qCtxDB.mMember on tQ.MinisterID equals MM.MemberID
                          //join Dept in qCtxDB.mDepartment on tQ.DepartmentID equals Dept.deptId
                          where (tQ.IsSubmitted == null)
                          //where (!AssemblyId.HasValue || tQ.AssemblyID == AssemblyId)
                          //&& (!SessionId.HasValue || tQ.SessionID == SessionId)
                          //&& (obj.DepartmentId == null || obj.DepartmentId == string.Empty || tQ.DepartmentID == obj.DepartmentId)
                          //&& (!MemberId.HasValue || tQ.MemberID == MemberId)
                          //&& (!Qtype.HasValue || tQ.QuestionType == Qtype)
                          //&& (!StatusId.HasValue || tQ.MQStatus == StatusId)
                          //&& (!MinisterId.HasValue || tQ.MinisterID == MinisterId)
                          //&& (!obj.Date.HasValue || tQ.QuestionDate.Value.Year == obj.Date.Value.Year && tQ.QuestionDate.Value.Month == obj.Date.Value.Month && tQ.QuestionDate.Value.Day == obj.Date.Value.Day)
                          //orderby tQ.QuestionDate descending
                          select new tQuestionModel
                          {
                              //QuestionType = QType.QuestionTypeName,
                              //QuestionNo = Convert.ToString(tQ.QuestionNumber),
                              //Subject = tQ.Subject,
                              //MemberN = M.Name,
                              //MinisterN = MM.Name,
                              //AssemblyN = Assem.AssemblyName,
                              //QuestionID = tQ.QuestionID,
                              //SessionId = tQ.SessionID,
                              //DepartmentN = Dept.deptname,
                              //MinisterId = tQ.MinisterID,
                              //DepartmentId = tQ.DepartmentID,
                              //QuestionFixDate = tQ.IsFixedDate,
                              //IsSubmitted = tQ.IsSubmitted,
                              //TableName = "QOnline"
                          }).Count();
            //  List<tQuestionModel> asList = Qquery.ToList();

            return Qquery.ToString();
        }

        private static object SubmitById(object param)
        {
            QuestionsContext Qctx = new QuestionsContext();
            tQuestionModel tqm = param as tQuestionModel;
            tQuestion tqo = Qctx.tQuestions.Single(m => m.QuestionID == tqm.QuestionID);
            try
            {
                tqo.IsSubmitted = true;
                tqo.QuestionDate = DateTime.Now.Date;
                Qctx.SaveChanges();
                tqm.Message = "Question Submitted !";
            }
            catch (Exception ex)
            {
                tqm.Message = ex.Message;
            }

            return tqm;
        }

        private static object GetSubmitQuestion(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel obj = param as tQuestionModel;
            //int? Qtype = null; int? QuestionNo = null;

            int? Questtype = null;
            int? QuestionNum = null;
            if (obj.QuestionType != null)
            {
                Questtype = Convert.ToInt32(obj.QuestionType);
            }
            if (obj.QuestionNumber != 0)
            {
                QuestionNum = obj.QuestionNumber;
            }
            //if (obj.MemberId != 0)
            //{
            //    MemberId = obj.MemberId;
            //}
            //if (obj.MinisterId != 0)
            //{
            //    MinisterId = obj.MinisterId;
            //}
            //if (obj.QtypeId != 0)
            //{
            //    Qtype = obj.QtypeId;
            //}
            //if (obj.StatusId != 0)
            //{
            //    StatusId = obj.StatusId;
            //}

            var Qquery = (from tQ in qCtxDB.tQuestions
                          join MM in qCtxDB.mMember on tQ.MemberID equals MM.MemberCode
                          join QType in qCtxDB.tQuestionType on tQ.QuestionType equals QType.QuestionTypeId
                          join Dept in qCtxDB.mDepartment on tQ.DepartmentID equals Dept.deptId
                          where (tQ.IsSubmitted == true)
                         && (!QuestionNum.HasValue || tQ.QuestionNumber == QuestionNum)
                         && (!obj.IsFixedDate.HasValue || tQ.IsFixedDate.Value.Year == obj.IsFixedDate.Value.Year && tQ.IsFixedDate.Value.Month == obj.IsFixedDate.Value.Month && tQ.IsFixedDate.Value.Day == obj.IsFixedDate.Value.Day)
                         && (!Questtype.HasValue || tQ.QuestionType == Questtype)
                          orderby tQ.QuestionID ascending
                          select new tQuestionModel
                          {
                              QuestionType = QType.QuestionTypeName,
                              QuestionNumber = tQ.QuestionNumber,
                              Subject = tQ.Subject,
                              MemberN = MM.Name,
                              QuestionID = tQ.QuestionID,
                              SessionId = tQ.SessionID,
                              DepartmentN = Dept.deptname,
                              MinisterId = tQ.MinisterID,
                              DepartmentId = tQ.DepartmentID,
                              QuestionFixDate = tQ.IsFixedDate,
                              IsSubmitted = tQ.IsSubmitted,
                              TableName = "QOnline",
                              MinisterName = (from mn in qCtxDB.mMinsitryMinister where mn.MinistryID == tQ.MinisterID select mn.MinisterName).FirstOrDefault()
                          }).Skip((obj.PageIndex - 1) * obj.PAGE_SIZE).Take(obj.PAGE_SIZE).ToList();

            return Qquery;
            //List<tQuestionModel> asList = Qquery.ToList();
            //return asList;

            //foreach (var item in asList)
            //{
            //    if (item.MinisterId != 0)
            //    {
            //        if (item.IsHindi == true)
            //        {
            //            var MinNameH = (from M in qCtxDB.mMember
            //                            where M.MemberID == item.MinisterId
            //                            select M.Name_Hindi).FirstOrDefault();
            //            item.MinisterN = MinNameH;
            //        }
            //        else
            //        {
            //            var MinName = (from M in qCtxDB.mMember
            //                           where M.MemberID == item.MinisterId
            //                           select M.Name).FirstOrDefault();
            //            item.MinisterN = MinName;
            //        }
            //    }
            //    if (item.DepartmentId != "" && item.DepartmentId != null)
            //    {
            //        if (item.IsHindi == true)
            //        {
            //            var DepNameH = (from D in qCtxDB.mDepartment
            //                            where D.deptId == item.DepartmentId
            //                            select D.deptname_hindi).FirstOrDefault();
            //            item.DepartmentN = DepNameH;
            //        }
            //        else
            //        {
            //            var DepName = (from D in qCtxDB.mDepartment
            //                           where D.deptId == item.DepartmentId
            //                           select D.deptname).FirstOrDefault();
            //            item.DepartmentN = DepName;
            //        }
            //    }
            //    if (item.SessionId != 0)
            //    {
            //        if (item.IsHindi == true)
            //        {
            //            var SessionNameH = (from S in qCtxDB.mSession
            //                                where S.Id == item.SessionId
            //                                select S.Name_Local).FirstOrDefault();
            //            item.SessionN = SessionNameH;
            //        }
            //        else
            //        {
            //            var SessionName = (from S in qCtxDB.mSession
            //                               where S.Id == item.SessionId
            //                               select S.Name).FirstOrDefault();
            //            item.SessionN = SessionName;
            //        }
            //    }

            //}
        }

        /// <summary>
        /// This method is use for getting Submitted question by memberId (Created By Shubham Giri)
        /// </summary>
        /// <param name="param">Member Id</param>
        /// <returns>Question List</returns>
        ///
        private static object GetQuestionSubmittedListByMemberId(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel model = param as tQuestionModel;

            var Qquery = (from tQ in qCtxDB.tQuestions
                          join MM in qCtxDB.mMember on tQ.MemberID equals MM.MemberCode
                          join QType in qCtxDB.tQuestionType on tQ.QuestionType equals QType.QuestionTypeId
                          join Dept in qCtxDB.mDepartment on tQ.DepartmentID equals Dept.deptId
                          where tQ.IsSubmitted == true && tQ.MemberID == model.MemberId && tQ.AssemblyID == model.AssemblyId
                          && tQ.SessionID == model.SessionId
                          orderby tQ.QuestionID ascending
                          select new tQuestionModel
                          {
                              QuestionType = QType.QuestionTypeName,
                              QuestionNumber = tQ.QuestionNumber,
                              Subject = tQ.Subject,
                              MemberN = MM.Name,
                              QuestionID = tQ.QuestionID,
                              SessionId = tQ.SessionID,
                              DepartmentN = Dept.deptname,
                              MinisterId = tQ.MinisterID,
                              DepartmentId = tQ.DepartmentID,
                              QuestionFixDate = tQ.IsFixedDate,
                              IsSubmitted = tQ.IsSubmitted,
                              MainQuestion = tQ.MainQuestion,
                              DiaryNumber = tQ.DiaryNumber,
                              NoticeReceivedDate = tQ.NoticeRecievedDate,
                              NoticeRecievedTime = tQ.NoticeRecievedTime,
                              MinisterName = (from mn in qCtxDB.mMinsitryMinister where mn.MinistryID == tQ.MinistryId select mn.MinisterName).FirstOrDefault()
                          }).ToList();

            return Qquery;
        }

        /// <summary>
        /// This method is use for getting Pending question by memberId (Created By Shubham Giri)
        /// </summary>
        /// <param name="param">Member Id</param>
        /// <returns>Question List</returns>
        ///
        private static object GetQuestionPendingListByMemberId(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();

            tQuestionModel model = param as tQuestionModel;

            var Qquery = (from tQ in qCtxDB.tQuestions
                          join MM in qCtxDB.mMember on tQ.MemberID equals MM.MemberCode
                          join QType in qCtxDB.tQuestionType on tQ.QuestionType equals QType.QuestionTypeId
                          join Dept in qCtxDB.mDepartment on tQ.DepartmentID equals Dept.deptId
                          where tQ.IsSubmitted == null && tQ.MemberID == model.MemberId && tQ.AssemblyID == model.AssemblyId
                           && tQ.SessionID == model.SessionId
                          orderby tQ.QuestionID ascending
                          select new tQuestionModel
                          {
                              QuestionType = QType.QuestionTypeName,
                              QuestionNumber = tQ.QuestionNumber,
                              Subject = tQ.Subject,
                              MemberN = MM.Name,
                              QuestionID = tQ.QuestionID,
                              SessionId = tQ.SessionID,
                              DepartmentN = Dept.deptname,
                              MinisterId = tQ.MinisterID,
                              DepartmentId = tQ.DepartmentID,
                              QuestionFixDate = tQ.IsFixedDate,
                              IsSubmitted = tQ.IsSubmitted,
                              MainQuestion = tQ.MainQuestion,
                              DiaryNumber = tQ.DiaryNumber,
                              NoticeReceivedDate = tQ.NoticeRecievedDate,
                              NoticeRecievedTime = tQ.NoticeRecievedTime,
                              MinisterName = (from mn in qCtxDB.mMinsitryMinister where mn.MinistryID == tQ.MinistryId select mn.MinisterName).FirstOrDefault()
                          }).ToList();

            return Qquery;
        }

        private static object GetSubmitQuestionCount(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel obj = param as tQuestionModel;
            //int? AssemblyId = null; int? SessionId = null; int? MemberId = null; int? MinisterId = null; int? Qtype = null;
            //int? StatusId = null;

            //if (obj.AssemblyId != 0)
            //{
            //    AssemblyId = obj.AssemblyId;
            //}
            //if (obj.SessionId != 0)
            //{
            //    SessionId = obj.SessionId;
            //}
            //if (obj.MemberId != 0)
            //{
            //    MemberId = obj.MemberId;
            //}
            //if (obj.MinisterId != 0)
            //{
            //    MinisterId = obj.MinisterId;
            //}
            //if (obj.QtypeId != 0)
            //{
            //    Qtype = obj.QtypeId;
            //}
            //if (obj.StatusId != 0)
            //{
            //    StatusId = obj.StatusId;
            //}

            int Qquery = (from tQ in qCtxDB.tQuestions

                          where (tQ.IsSubmitted == true)
                          //&& (!SessionId.HasValue || tQ.SessionID == SessionId)
                          //&& (obj.DepartmentId == null || obj.DepartmentId == string.Empty || tQ.DepartmentID == obj.DepartmentId)
                          //&& (!MemberId.HasValue || tQ.MemberID == MemberId)
                          //&& (!Qtype.HasValue || tQ.QuestionType == Qtype)
                          //&& (!StatusId.HasValue || tQ.MQStatus == StatusId)
                          //&& (!MinisterId.HasValue || tQ.MinisterID == MinisterId)
                          //&& (!obj.Date.HasValue || tQ.QuestionDate.Value.Year == obj.Date.Value.Year && tQ.QuestionDate.Value.Month == obj.Date.Value.Month && tQ.QuestionDate.Value.Day == obj.Date.Value.Day)
                          //orderby tQ.QuestionDate descending

                          select new tQuestionModel
                          {
                              //QuestionType = QType.QuestionTypeName,
                              //QuestionNo = Convert.ToString(tQ.QuestionNumber),
                              //Subject = tQ.Subject,
                              //MemberN = M.Name,
                              //MinisterN = MM.Name,
                              //AssemblyN = Assem.AssemblyName,
                              //QuestionID = tQ.QuestionID,
                              //SessionId = tQ.SessionID,
                              //DepartmentN = Dept.deptname,
                              //MinisterId = tQ.MinisterID,
                              //DepartmentId = tQ.DepartmentID,
                              //QuestionFixDate = tQ.IsFixedDate,
                              //IsSubmitted = tQ.IsSubmitted,
                              //TableName = "QOnline"
                          }).Count();

            return Qquery.ToString();
        }

        private static object GetQuestionID(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel updateSQ = param as tQuestionModel;

            var queery = (from tQ in qCtxDB.tQuestions
                          where tQ.AssemblyID == updateSQ.AssemblyId && tQ.QuestionType == updateSQ.QtypeId
                          select tQ.QuestionNumber).Max();

            return queery.ToString();
        }

        private static object InsertNGetQuestionID(object param)
        {
            if (null == param)
            {
                return null;
            }

            MinisteryContext ministryContext = new MinisteryContext();
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion Qm = new tQuestion();
            tQuestion qInsertQues = param as tQuestion;
            var CheckCount = (from m in qCtxDB.tQuestions
                              where m.MemberID == qInsertQues.MemberID && m.QuestionDate == qInsertQues.QuestionDate && m.QuestionType == qInsertQues.QuestionType
                              select m).Count();
            if (qInsertQues.MinisterID != 0)
            {
                var resultMinistry = (from ministry in ministryContext.mMinsitryMinisteries
                                      where ministry.MinistryID == qInsertQues.MinisterID
                                      select ministry).FirstOrDefault();
                if (resultMinistry != null)
                {
                    qInsertQues.MemberCode = resultMinistry.MemberCode;
                }
            }

            if (qInsertQues.QuestionType == 1 && CheckCount >= 2)
            {
                Qm.Message = "Only 2 Starred Question allowed in a day";
            }
            else if (qInsertQues.QuestionType == 2 && CheckCount >= 3)
            {
                Qm.Message = "Only 3 Unstarred Question allowed in a day";
            }
            else if (qInsertQues.QuestionType == 3 && CheckCount >= 1)
            {
                Qm.Message = "Only 1 Short Notice allowed in a day";
            }
            else
            {
                qCtxDB.tQuestions.Add(qInsertQues);
                qCtxDB.SaveChanges();

                var getQid = (from m in qCtxDB.tQuestions
                              where m.QuestionID == qInsertQues.QuestionID
                              select m).FirstOrDefault();

                Qm = (tQuestion)getQid;
                Qm.Message = "InsertSubQuestion";
            }

            return Qm;
        }

        private static object UpdateQuestionsbyVS(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestion update = param as tQuestion;

            tQuestion obj = qCtxDB.tQuestions.Single(m => m.QuestionID == update.QuestionID);
            obj.AssemblyID = update.AssemblyID;
            obj.SessionID = update.SessionID;
            obj.MemberID = update.MemberID;
            obj.MinisterID = update.MinisterID;
            obj.QuestionType = update.QuestionType;
            obj.DepartmentID = update.DepartmentID;
            obj.ConstituencyName = update.ConstituencyName;
            obj.ConstituencyNo = update.ConstituencyNo;
            obj.Subject = update.Subject;
            obj.MainQuestion = update.MainQuestion;
            obj.IsFixedDate = update.IsFixedDate;
            obj.ReferenceMemberCode = update.ReferenceMemberCode;
            qCtxDB.SaveChanges();

            return null;
        }

        private static object CheckQuestionNo(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel updateSQ = param as tQuestionModel;

            var query = (from tQ in qCtxDB.tQuestions
                         where tQ.QuestionNumber == updateSQ.QuestionNumber
                         select tQ).ToList().Count();

            return query.ToString();
        }

        private static object GetConstByMemberId(object param)
        {
            if (null == param)
            {
                return null;
            }

            QuestionsContext qCtxDB = new QuestionsContext();
            mMemberAssembly MemAssem = param as mMemberAssembly;
            var Constituency = (from t in qCtxDB.mConstituency
                                join f in qCtxDB.mMemberAssembly on t.ConstituencyCode equals f.ConstituencyCode
                                where f.MemberID == MemAssem.MemberID && t.AssemblyID == MemAssem.AssemblyID && f.AssemblyID == MemAssem.AssemblyID
                                select t).ToList();
            return Constituency;
        }

        private static object DeleteQues(object param)
        {
            QuestionsContext Qctx = new QuestionsContext();
            tQuestionModel tqm = param as tQuestionModel;
            tQuestion tqo = Qctx.tQuestions.Single(m => m.QuestionID == tqm.QuestionID);
            Qctx.tQuestions.Remove(tqo);
            Qctx.SaveChanges();
            tqm.Message = "Question Deleted !";
            //try
            //{
            //    Qctx.SaveChanges();
            //    tqm.Message = "Question Deletes !";
            //}

            //catch (Exception ex)
            //{
            //    tqm.Message = ex.Message;
            //}

            return tqm;
        }

        private static object GetQuesForEditById(object param)
        {
            QuestionsContext qCtxDB = new QuestionsContext();
            tQuestionModel obj = param as tQuestionModel;
            tQuestionModel tqm = new tQuestionModel();

            tqm.tQuestionType = (from qt in qCtxDB.tQuestionType
                                 select qt).ToList();

            tqm.mMemberModel = (from m in qCtxDB.mMember
                                select m).ToList();

            tqm.mDepartment = (from d in qCtxDB.mDepartment
                               select d).ToList();
            tqm.objQuestList = (from tQ in qCtxDB.tQuestions
                                where (tQ.QuestionID == obj.QuestionID)
                                select new tQuestionModel
                                {
                                    IsHindi = tQ.IsHindi,
                                    QuestionID = tQ.QuestionID,
                                    SubmittedBy = tQ.SubmittedBy,
                                    AssemblyId = tQ.AssemblyID,
                                    SessionId = tQ.SessionID,
                                    MemberId = tQ.MemberID,
                                    MinisterId = tQ.MinisterID,
                                    StatusId = tQ.MQStatus,
                                    DepartmentId = tQ.DepartmentID,
                                    QtypeId = tQ.QuestionType,
                                    ConstituencyName = tQ.ConstituencyName,
                                    ConstituencyNo = tQ.ConstituencyNo,
                                    Subject = tQ.Subject,
                                    MainQuestion = tQ.MainQuestion,
                                    DateforReply = tQ.DeptDateOfReply,
                                    QuestionNumber = tQ.QuestionNumber,
                                    Date = tQ.IsFixedDate,
                                    IsSubmitted = tQ.IsSubmitted,
                                    QuestionTypeId = tQ.QuestionType,
                                    IsMerge = tQ.IsMerge,
                                    gggg = tQ.ReferenceMemberCode
                                }).ToList();

            return tqm;
        }

        private static object GetQuestionBySessionDate(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion Question = (tQuestion)param;
            var LOB = (from Questions in db.tQuestions
                       where Questions.QuestionType == Question.QuestionType && Questions.IsFixedDate == Question.IsFixedDate && (Questions.DeActivate == false || Questions.DeActivate == null)
                       orderby Questions.QuestionNumber
                       select Questions).ToList();

            return LOB;
        }

        private static object GetQuestionBySessionDateAndLaidType(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion question = (tQuestion)param;
            var LOB = (from Questions in db.tQuestions
                       join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId
                       join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                       where Questions.QuestionType == question.QuestionType && Questions.IsFixedDate == question.IsFixedDate
                       && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                       && paperLaidTemp.DeptSubmittedDate != null
                       && Questions.IsFinalApproved == true
                       orderby Questions.QuestionNumber
                       select new QuestionModelCustom
                       {
                           PaperLaidId = paperLaidVs.PaperLaidId,
                           QuestionID = Questions.QuestionID,
                           QuestionNumber = Questions.QuestionNumber,
                           FileName = paperLaidTemp.SignedFilePath,
                           IsLaid = paperLaidVs.IsLaid,
                           Subject = Questions.Subject,
                       }).ToList();

            return LOB;
        }

        private static object GetDraftAndApprovedQuestionBySessionDateAndLaidType(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion question = (tQuestion)param;
            var LOB = (from Questions in db.tQuestions
                       join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId into pvs
                       from paperLaidVs in pvs.DefaultIfEmpty()
                       join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.DeptActivePaperId equals paperLaidTemp.PaperLaidTempId into pvt
                       from paperLaidTemp in pvt.DefaultIfEmpty()
                       join mDeparment in db.mDepartment on Questions.DepartmentID equals mDeparment.deptId into dvt
                       from mDeparment in dvt.DefaultIfEmpty()
                       where Questions.QuestionType == question.QuestionType && Questions.IsFixedDate == question.IsFixedDate
                       && (paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId || paperLaidVs.DeptActivePaperId == null)
                       && (paperLaidTemp.DeptSubmittedDate != null || paperLaidTemp.DeptSubmittedDate == null)
                       && Questions.IsFinalApproved == true
                       orderby Questions.QuestionNumber

                       select new QuestionModelCustom
                       {
                           PaperLaidId = paperLaidVs.PaperLaidId,
                           QuestionID = Questions.QuestionID,
                           QuestionNumber = Questions.QuestionNumber,
                           FileName = paperLaidTemp.SignedFilePath,
                           SfilePath = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                           IsLaid = paperLaidVs.IsLaid,
                           Subject = Questions.Subject,
                           QFixDate = question.IsFixedDate,
                           QSubmitedDate = paperLaidTemp.DeptSubmittedDate,
                           DepartmentName = mDeparment.deptname,
                           // DepartmentSubmittedDate=paperLaidTemp.,
                       }).ToList().Distinct();

            return LOB.GroupBy(a => a.QuestionID).Select(p => p.First()).ToList();
        }

        private static object GetQuestionBySessionDateAndLatestFile(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion question = (tQuestion)param;
            var LOB = (from Questions in db.tQuestions
                       join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId
                       join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                       join mDeparment in db.mDepartment on Questions.DepartmentID equals mDeparment.deptId
                       where Questions.QuestionType == question.QuestionType && Questions.IsFixedDate == question.IsFixedDate
                       && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                       && paperLaidTemp.DeptSubmittedDate != null
                       && Questions.IsFinalApproved == true
                       orderby Questions.QuestionNumber
                       select new QuestionModelCustom
                       {
                           PaperLaidId = paperLaidVs.PaperLaidId,
                           QuestionID = Questions.QuestionID,
                           QuestionNumber = Questions.QuestionNumber,
                           FileName = paperLaidTemp.SignedFilePath,
                           DocFilePath = paperLaidTemp.FilePath + paperLaidTemp.DocFileName,
                           IsLaid = paperLaidVs.IsLaid,
                           Subject = Questions.Subject,
                           Version = paperLaidTemp.Version,
                           DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                           DepartmentName = mDeparment.deptname,
                           LastVarifiedVersion=(from tQ in  db.tAuditTrial where tQ.paperLaidID==paperLaidVs.PaperLaidId orderby tQ.DateTime descending select tQ.documentReferenceNumber).FirstOrDefault(),
                           LastVarifiedVersionDate = (from tQ in db.tAuditTrial where tQ.paperLaidID == paperLaidVs.PaperLaidId orderby tQ.DateTime descending select tQ.DateTime).FirstOrDefault()
                       }).ToList();

            return LOB;
        }

        private static object GetOtherpaperPaperType(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tPaperLaidV question = (tPaperLaidV)param;
            var LOB = (from paperLaidVs in db.tPaperLaidV
                       //join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId
                       join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.PaperLaidId equals paperLaidTemp.PaperLaidId
                       where (paperLaidVs.PaperTypeID == 5 || paperLaidVs.PaperTypeID == 2 || paperLaidVs.PaperTypeID == 4) && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidTemp.DeptSubmittedDate != null && paperLaidVs.AssemblyId == question.AssemblyId && paperLaidVs.SessionId == question.SessionId
                       orderby paperLaidTemp.DeptSubmittedDate
                       select new QuestionModelCustom
                       {
                           PaperLaidId = paperLaidTemp.PaperLaidTempId,
                           //QuestionID = Questions.QuestionID,
                           //QuestionNumber = Questions.QuestionNumber,
                           FileName = paperLaidTemp.SignedFilePath,
                           IsLaid = paperLaidVs.IsLaid,
                           Subject = paperLaidVs.Title,
                           Version = paperLaidTemp.Version,
                           DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                           DepartmentName = paperLaidVs.DeparmentName
                       }).ToList();

            return LOB;
        }

        private static string GetAssQuestionFromID(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion question = (tQuestion)param;
            return (from Questions in db.tQuestions
                    join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId
                    join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.DeptActivePaperId equals paperLaidTemp.PaperLaidTempId
                    where Questions.QuestionID == question.QuestionID && (Questions.DeActivate == false || Questions.DeActivate == null)
                    select paperLaidTemp.SignedFilePath).FirstOrDefault();
        }

        private static string GetAssQuestionDocFromID(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion question = (tQuestion)param;
            return (from Questions in db.tQuestions
                    join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId
                    join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.DeptActivePaperId equals paperLaidTemp.PaperLaidTempId
                    where Questions.QuestionID == question.QuestionID && (Questions.DeActivate == false || Questions.DeActivate == null)
                    select paperLaidTemp.FilePath + paperLaidTemp.DocFileName).FirstOrDefault();
        }

        private static string GetDraftPDFQuestionFromID(object param)
        {
            //QuestionsContext db = new QuestionsContext();
            //tQuestion question = (tQuestion)param;
            //return (from Questions in db.tQuestions
            //        join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId into pv
            //        from paperLaidVs in pv.DefaultIfEmpty()
            //        join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.MinisterActivePaperId equals paperLaidTemp.PaperLaidTempId into tp
            //        from paperLaidTemp in tp.DefaultIfEmpty()
            //        where Questions.QuestionID == question.QuestionID && (Questions.DeActivate == false || Questions.DeActivate == null)
            //        select paperLaidTemp.FilePath + paperLaidTemp.FileName).FirstOrDefault();

            QuestionsContext db = new QuestionsContext();
            tQuestion question = (tQuestion)param;
            return (from Questions in db.tQuestions
                    join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId
                    join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.DeptActivePaperId equals paperLaidTemp.PaperLaidTempId
                    where Questions.QuestionID == question.QuestionID && (Questions.DeActivate == false || Questions.DeActivate == null)
                    select paperLaidTemp.FilePath + paperLaidTemp.FileName).FirstOrDefault();
        }

        private static string GetAssOtherfileFromID(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tPaperLaidTemp question = (tPaperLaidTemp)param;
            var reg = (from paperLaidTemp in db.tPaperLaidTemp
                       where paperLaidTemp.PaperLaidTempId == question.PaperLaidTempId
                       select paperLaidTemp.SignedFilePath).FirstOrDefault();
            return reg;
        }

        private static string GetOtherFileSDocFromID(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tPaperLaidTemp question = (tPaperLaidTemp)param;
            var reg = (from paperLaidTemp in db.tPaperLaidTemp
                       where paperLaidTemp.PaperLaidTempId == question.PaperLaidTempId
                       select paperLaidTemp.FilePath + paperLaidTemp.DocFileName).FirstOrDefault();
            return reg;
        }

        private static object GetQuestionBySessionDateWithPaper(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion Question = (tQuestion)param;
            var LOB = (from Questions in db.tQuestions
                       join paperLaidVs in db.tPaperLaidV on Questions.PaperLaidId equals paperLaidVs.PaperLaidId
                       join paperLaidTemp in db.tPaperLaidTemp on paperLaidVs.DeptActivePaperId equals paperLaidTemp.PaperLaidTempId
                       where Questions.QuestionType == Question.QuestionType
                       && Questions.IsFixedDate == Question.IsFixedDate
                       && (Questions.DeActivate == false || Questions.DeActivate == null)
                       //&& Question.IsFinalApproved==true
                       orderby Questions.QuestionNumber

                       select new QuestionModelCustom
                       {
                           QuestionID = Questions.QuestionID,
                           QuestionNumber = Questions.QuestionNumber,
                           FileName = paperLaidTemp.SignedFilePath,
                           DocFilePath = paperLaidTemp.FilePath + paperLaidTemp.DocFileName
                       }).ToList();

            return LOB;
        }

        private static object GetQuestionTypeByTypeId(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestionType Question = (tQuestionType)param;
            var LOB = (from Questions in db.tQuestionType
                       where Questions.QuestionTypeId == Question.QuestionTypeId
                       select Questions).FirstOrDefault();

            return LOB;
        }

        private static string GetAttachment(object param)
        {
            QuestionsContext db = new QuestionsContext();

            int QuestionId = Convert.ToInt32(param);

            var query = (from Qst in db.tQuestions
                         where Qst.QuestionID == QuestionId
                         select new
                         {
                             Qst.OriDiaryFilePath,
                             Qst.OriDiaryFileName
                         }).FirstOrDefault();

            // For Accessing File Path
            var FAP = (from SS in db.mSiteSettings
                       where SS.SettingName == "SecureFileAccessingUrlPath"
                       select SS.SettingValue).FirstOrDefault();

            return FAP + "/" + query.OriDiaryFilePath + query.OriDiaryFileName;

            //return query.ToList();
        }

        //**************************************************

        private static SearchModel GetQuestionFilePathByQuestionID(object param)
        {
            QuestionsContext db = new QuestionsContext();
            var model = param as SearchModel;
            //int QuestionId = Convert.ToInt32(param);
            var query = (from Questions in db.tQuestions
                         where Questions.QuestionID == model.ID
                         select new SearchModel
                         {
                             FilePath = Questions.AnswerAttachLocation
                         }).FirstOrDefault();

            return query;
        }

        private static object GetQuestionSubjects()
        {
            QuestionsContext db = new QuestionsContext();
            var qSubjects = (from Questions in db.tQuestions
                             orderby Questions.Subject
                             select Questions.Subject).Distinct().ToList();
            return qSubjects;
        }

        private static object GetQuestionSubjects_key(object param)
        {
            var subjectKey = param as string;
            QuestionsContext db = new QuestionsContext();
            var qSubjects = (from Questions in db.tQuestions
                             where (Questions.Subject.ToUpper().StartsWith(subjectKey.Trim().ToUpper()))
                             orderby Questions.Subject
                             select Questions.Subject).Distinct().Take(10).ToList();
            return qSubjects;
        }

        private static object GetDebatesSubjects()
        {
            QuestionsContext db = new QuestionsContext();
            var qSubjects = (from Debates in db.tReporterDescriptionLogs
                             orderby Debates.Subject
                             select Debates.Subject).Distinct().ToList();
            return qSubjects;
        }

        private static object GetQuestionTypes()
        {
            QuestionsContext db = new QuestionsContext();
            var query = db.tQuestionType.ToList();
            return query;
        }

        public static object GetSearchedQuestions(object param)
        {
            var model = param as SearchModel;
            QuestionsContext db = new QuestionsContext();
            var predicate = PredicateBuilder.True<SearchModel>();
            if (model.AssemblyID != 0)
                predicate = predicate.And(q => q.AssemblyID == model.AssemblyID);
            if (model.SessionId != 0)
                predicate = predicate.And(q => q.SessionId == model.SessionId);
            if (model.DepartmentID != null)
                predicate = predicate.And(q => q.DepartmentID == model.DepartmentID);
            if (model.MinsitryMinistersID != 0)
                predicate = predicate.And(q => q.MinsitryMinistersID == model.MinsitryMinistersID);
            if (model.MemberID != 0)
                predicate = predicate.And(q => q.MemberID == model.MemberID);
            if (model.QuestionType != 0)
                predicate = predicate.And(q => q.QuestionType == model.QuestionType);
            if (model.FromDate != null)
                predicate = predicate.And(q => q.Date >= model.FromDate);
            if (model.ToDate != null)
                predicate = predicate.And(q => q.Date <= model.ToDate);
            //if (model.FromDate != null && model.ToDate == null)
            //    predicate = predicate.And(q => q.Date >= model.FromDate);
            //if (model.ToDate != null && model.FromDate == null)
            //    predicate = predicate.And(q => q.Date <= model.ToDate);
            //if (model.ToDate != null && model.FromDate != null)
            //    predicate = predicate.And(q => q.Date >= model.FromDate && q.Date <= model.ToDate);
            if (model.Subject != null)
                predicate = predicate.And(q => q.Subject.ToUpper().Contains(model.Subject.Trim().ToUpper()));

            var qResults = (from Questions in db.tQuestions
                            select new SearchModel
                            {
                                ID = Questions.QuestionID,
                                AssemblyID = Questions.AssemblyID,
                                SessionId = Questions.SessionID,
                                DepartmentID = Questions.DepartmentID,
                                MemberID = Questions.MemberID,
                                FilePath = Questions.AnswerAttachLocation,
                                MinsitryMinistersID = (from ministers in db.mMinsitryMinister
                                                       where ministers.MinistryID == Questions.MinistryId
                                                       select ministers.MinsitryMinistersID).FirstOrDefault(),
                                AssemblyName = (from Assemblies in db.mAssembly
                                                where Assemblies.AssemblyID == Questions.AssemblyID
                                                select Assemblies.AssemblyName).FirstOrDefault(),
                                AssemblyCode = Questions.AssemblyID,
                                SessionName = (from Sessions in db.mSession
                                               where Sessions.SessionCode == Questions.SessionID
                                               select Sessions.SessionName).FirstOrDefault(),
                                SessionCode = Questions.SessionID,
                                Date = Questions.SubmittedDate,
                                Subject = Questions.Subject,
                                QuestionType = Questions.QuestionType,
                                MemberName = (from Members in db.mMember
                                              where Members.MemberCode == Questions.MemberID
                                              select Members.Name).FirstOrDefault(),
                                MinisterName = (from Ministers in db.mMinsitryMinister
                                                where Ministers.MinistryID == Questions.MinistryId
                                                select Ministers.MinisterName).FirstOrDefault(),
                                MinistryName = (from Ministries in db.mMinistries
                                                where Ministries.MinistryID == Questions.MinistryId
                                                select Ministries.MinistryName).FirstOrDefault(),
                                DepartmentName = (from Departments in db.mDepartment
                                                  where Departments.deptId == Questions.DepartmentID
                                                  select Departments.deptname).FirstOrDefault()
                            }).AsExpandable().Where(predicate).ToList();

            model.objList = qResults.ToList();

            return model;
        }

        public static object GetSearchedDebates(object param)
        {
            var model = param as SearchModel;
            QuestionsContext db = new QuestionsContext();
            var predicate = PredicateBuilder.True<SearchModel>();

            if (model.SessionId != 0)
                predicate = predicate.And(q => q.SessionId == model.SessionId);
            if (model.MemberID != 0)
                predicate = predicate.And(q => q.MemberID == model.MemberID);
            if (model.FromDate != null)
                predicate = predicate.And(q => q.Date >= model.FromDate);
            if (model.ToDate != null)
                predicate = predicate.And(q => q.Date <= model.ToDate);
            if (model.Subject != null)
                predicate = predicate.And(q => q.Subject.ToUpper().Contains(model.Subject.Trim().ToUpper()));
            if (model.mEventId != 0)
                predicate = predicate.And(q => q.mEventId == model.mEventId);
            if (model.FromAssemblyID != 0 && model.ToAssemblyID == 0)
                predicate = predicate.And(q => q.AssemblyID == model.FromAssemblyID);
            if (model.ToAssemblyID != 0 && model.FromAssemblyID == 0)
                predicate = predicate.And(q => q.AssemblyID == model.ToAssemblyID);
            if (model.FromAssemblyID != 0 && model.ToAssemblyID != 0)
                predicate = predicate.And(q => (q.AssemblyID >= model.FromAssemblyID && q.AssemblyID <= model.ToAssemblyID));
            predicate = predicate.And(q => (q.IsFreeze == true));

            var qResults = (from Debates in db.tReporterDescriptionLogs
                            orderby Debates.LOBDate, Debates.Subject, Debates.MemberId, Debates.mEventId
                            select new SearchModel
                            {
                                ID = Debates.Id,
                                AssemblyID = Debates.AssemblyId,
                                SessionId = Debates.SessionId,
                                AssemblyName = (from Assemblies in db.mAssembly
                                                where Assemblies.AssemblyCode == Debates.AssemblyId
                                                select Assemblies.AssemblyName).FirstOrDefault(),
                                AssemblyCode = Debates.AssemblyId,
                                SessionName = (from Sessions in db.mSession
                                               where Sessions.SessionCode == Debates.SessionId
                                               select Sessions.SessionName).FirstOrDefault(),
                                SessionCode = Debates.SessionId,
                                mEventName = (from Events in db.Events
                                              where Events.EventId == Debates.mEventId
                                              select Events.EventName).FirstOrDefault(),
                                mEventId = Debates.mEventId,
                                Subject = Debates.Subject,
                                Date = Debates.LOBDate,
                                MemberID = Debates.MemberId,
                                MemberName = (from Members in db.mMember
                                              where Members.MemberCode == Debates.MemberId
                                              select Members.Name).FirstOrDefault(),
                                PageNos = Debates.EditedVersionPageNumber,
                                QuestionNo = Debates.RefNo,
                                QuestionType = Debates.QuestionType,
                                IsFreeze = Debates.IsFreeze
                            }).AsExpandable().Where(predicate).ToList();

            model.objList = qResults.ToList();

            List<SearchModel> ListDebates = new List<SearchModel>();
            //SearchModel temp = new SearchModel();
            string pageNos = "";
            bool flag = false;
            int count = 0;
            SearchModel test = new SearchModel();
            foreach (var item in qResults)
            {
                ++count;

                if (test.Date == item.Date && test.Subject == item.Subject && test.mEventId == item.mEventId && test.MemberID == item.MemberID)
                {
                    flag = true;
                }
                else
                {
                    if (count != 1)
                    {
                        ListDebates.Add(test);
                        pageNos = "";
                        flag = false;
                        test = new SearchModel();
                    }
                    if (item.Subject == "education")
                    {
                        int k = 0;
                    }
                    if (item.mEventId == 3)
                    {
                        int k = 0;
                    }
                }

                test.ID = item.ID;
                test.AssemblyCode = item.AssemblyCode;
                test.SessionCode = item.SessionCode;
                test.mEventId = item.mEventId;
                test.mEventName = item.mEventName;
                test.Subject = item.Subject;
                test.Date = item.Date;
                test.MemberID = item.MemberID;
                test.MemberName = item.MemberName;
                test.QuestionNo = item.QuestionNo;
                test.QuestionType = item.QuestionType;

                //test.PageNos = item.PageNos;
                //if (flag)
                {
                    if (!string.IsNullOrEmpty(item.PageNos))
                    {
                        if (!flag)
                            pageNos = "";
                        string[] pageList = (item.PageNos.Contains("-")) ? item.PageNos.Split('-') : null;
                        if (pageList != null)
                        {
                            int pageStart = (!string.IsNullOrEmpty(pageList[0])) ? int.Parse(pageList[0]) : -1;
                            int pageEnd = (!string.IsNullOrEmpty(pageList[1])) ? int.Parse(pageList[1]) : -1;

                            if (pageNos != "")
                                pageNos += ",";
                            for (int i = pageStart; ; i++)
                            {
                                pageNos += (i < 10) ? ("0" + i.ToString()) : i.ToString();
                                if (i == pageEnd)
                                {
                                    break;
                                }
                                else
                                {
                                    pageNos += ",";
                                }
                            }
                        }
                        else
                        {
                            int page = (!string.IsNullOrEmpty(item.PageNos)) ? int.Parse(item.PageNos) : -1;
                            if (pageNos != "")
                                pageNos += ",";
                            pageNos += (page < 10) ? ("0" + page.ToString()) : page.ToString();
                        }
                        test.PageNos = pageNos;
                    }
                    if (count == qResults.Count)
                    {
                        ListDebates.Add(test);
                    }
                }

                //if (count == qResults.Count)
                //{
                //    ListDebates.Add(test);
                //}
            }
            model.objList = ListDebates.ToList();
            return model;
        }

        public static object GetSearchedQuestionsHistory(object param)
        {
            QuestionsContext db = new QuestionsContext();
            var model = param as SearchModel;

            var predicate = PredicateBuilder.True<SearchModel>();
            if (model.AssemblyID != 0)
                predicate = predicate.And(q => q.AssemblyID == model.AssemblyID);
            if (model.SessionId != 0)
                predicate = predicate.And(q => q.SessionId == model.SessionId);
            if (model.Department_Id != null)
                predicate = predicate.And(q => q.Department_Id == model.Department_Id);
            if (model.MinsitryMinistersID != 0)
                predicate = predicate.And(q => q.MinsitryMinistersID == model.MinsitryMinistersID);
            if (model.MemberID != 0)
                predicate = predicate.And(q => q.MemberID == model.MemberID);
            if (model.FromDate != null)
                predicate = predicate.And(q => q.Date >= model.FromDate);
            if (model.ToDate != null)
                predicate = predicate.And(q => q.Date <= model.ToDate);
            if (model.Subject != null)
                predicate = predicate.And(q => q.Subject.ToUpper().Contains(model.Subject.Trim().ToUpper()));
            //Subject = ((starredQuestions.EditedDescription.Trim() != null && starredQuestions.EditedDescription.Trim() != "") ? starredQuestions.EditedDescription.Substring(starredQuestions.EditedDescription.IndexOf("("), starredQuestions.EditedDescription.IndexOf("-")) : ""),
            if (model.QuestionType == 1)
            {
                var qResults = (from starredQuestions in db.tStarredQuestionsHistory
                                select new SearchModel
                                {
                                    ID = starredQuestions.id,
                                    AssemblyID = starredQuestions.AssemblyID,
                                    SessionId = starredQuestions.SessionID,
                                    Department_Id = starredQuestions.DepartmentId,
                                    MinsitryMinistersID = starredQuestions.MinisterId,
                                    MemberID = starredQuestions.MinisterId,
                                    FilePath = starredQuestions.AttachmentName,
                                    AssemblyName = (from Assemblies in db.mAssembly
                                                    where Assemblies.AssemblyID == starredQuestions.AssemblyID
                                                    select Assemblies.AssemblyName).FirstOrDefault(),
                                    AssemblyCode = starredQuestions.AssemblyID,
                                    SessionName = (from Sessions in db.mSession
                                                   where Sessions.SessionCode == starredQuestions.SessionID
                                                   select Sessions.SessionName).FirstOrDefault(),
                                    SessionCode = starredQuestions.SessionID,
                                    Date = starredQuestions.SessionDate,
                                    QuestionType = 1,
                                    QuestionNo = starredQuestions.QuestionNo,
                                    Subject = (starredQuestions.EditedDescription.Contains("(") && starredQuestions.EditedDescription.Contains(") -")) ? starredQuestions.EditedDescription.Substring(starredQuestions.EditedDescription.IndexOf("(") + 1, (starredQuestions.EditedDescription.IndexOf(") -") - 4)) : starredQuestions.EditedDescription,
                                    MemberName = (from Members in db.mMember
                                                  where Members.MemberCode == starredQuestions.MinisterId
                                                  select Members.Name).FirstOrDefault(),
                                    MinisterName = (from Ministers in db.mMinsitryMinister
                                                    where Ministers.MinsitryMinistersID == starredQuestions.MinisterId
                                                    select Ministers.MinisterName).FirstOrDefault(),
                                    RefPageNo = starredQuestions.RefPageNo,
                                    DepartmentName = (from Departments in db.mDepartment
                                                      where Departments.deptId == (from mMinistryDepartments in db.mMinistryDepartments
                                                                                   where mMinistryDepartments.MinistryDepartmentsID == starredQuestions.DepartmentId
                                                                                   select mMinistryDepartments.DeptID).FirstOrDefault()
                                                      select Departments.deptname).FirstOrDefault()
                                }).AsExpandable().Where(predicate).ToList();
                //var pageNo= (db.tReporterDescriptionLogs.GroupBy(l=> new{l.RefNo}).Select(g=>new{g.Key.RefNo, ReporterDescriptionsLog=string.Join("|",g.Select(i=>i.EditedVersionPageNumber)) }));

                model.objList = qResults;
            }
            else if (model.QuestionType == 2)
            {
                var qResults = (from unstarredQuestions in db.tUnStarredQuestionsHistory
                                select new SearchModel
                                {
                                    ID = unstarredQuestions.id,
                                    AssemblyID = unstarredQuestions.AssemblyID,
                                    SessionId = unstarredQuestions.SessionID,
                                    Department_Id = unstarredQuestions.DepartmentId,
                                    MinsitryMinistersID = unstarredQuestions.MinisterId,
                                    MemberID = unstarredQuestions.MinisterId,
                                    FilePath = unstarredQuestions.AttachmentName,
                                    AssemblyName = (from Assemblies in db.mAssembly
                                                    where Assemblies.AssemblyID == unstarredQuestions.AssemblyID
                                                    select Assemblies.AssemblyName).FirstOrDefault(),
                                    AssemblyCode = unstarredQuestions.AssemblyID,
                                    SessionName = (from Sessions in db.mSession
                                                   where Sessions.SessionCode == unstarredQuestions.SessionID
                                                   select Sessions.SessionName).FirstOrDefault(),
                                    SessionCode = unstarredQuestions.SessionID,
                                    Date = unstarredQuestions.SessionDate,
                                    QuestionType = 2,
                                    QuestionNo = unstarredQuestions.QuestionNo,
                                    Subject = (unstarredQuestions.EditedDescription.Contains("(") && unstarredQuestions.EditedDescription.Contains(") -")) ? unstarredQuestions.EditedDescription.Substring(unstarredQuestions.EditedDescription.IndexOf("(") + 1, (unstarredQuestions.EditedDescription.IndexOf(") -") - 4)) : unstarredQuestions.EditedDescription,
                                    MemberName = (from Members in db.mMember
                                                  where Members.MemberCode == unstarredQuestions.MinisterId
                                                  select Members.Name).FirstOrDefault(),
                                    MinisterName = (from Ministers in db.mMinsitryMinister
                                                    where Ministers.MinsitryMinistersID == unstarredQuestions.MinisterId
                                                    select Ministers.MinisterName).FirstOrDefault(),
                                    RefPageNo = unstarredQuestions.RefPageNo,
                                    DepartmentName = (from Departments in db.mDepartment
                                                      where Departments.deptId == (from mMinistryDepartments in db.mMinistryDepartments
                                                                                   where mMinistryDepartments.MinistryDepartmentsID == unstarredQuestions.DepartmentId
                                                                                   select mMinistryDepartments.DeptID).FirstOrDefault()
                                                      select Departments.deptname).FirstOrDefault()
                                }).AsExpandable().Where(predicate).ToList();
                model.objList = qResults;
            }
            else
            {
                //var qResults = (from unstarrdQuestions in db.tUnStarredQuestionsHistory join starredQuestions in db.tStarredQuestionsHistory
                //                select new SearchModel{});
                var sqResults = (from starredQuestions in db.tStarredQuestionsHistory
                                 select new SearchModel
                                 {
                                     ID = starredQuestions.id,
                                     AssemblyID = starredQuestions.AssemblyID,
                                     SessionId = starredQuestions.SessionID,
                                     Department_Id = starredQuestions.DepartmentId,
                                     MinsitryMinistersID = starredQuestions.MinisterId,
                                     MemberID = starredQuestions.MinisterId,
                                     FilePath = starredQuestions.AttachmentName,
                                     AssemblyName = (from Assemblies in db.mAssembly
                                                     where Assemblies.AssemblyID == starredQuestions.AssemblyID
                                                     select Assemblies.AssemblyName).FirstOrDefault(),
                                     AssemblyCode = starredQuestions.AssemblyID,
                                     SessionName = (from Sessions in db.mSession
                                                    where Sessions.SessionCode == starredQuestions.SessionID
                                                    select Sessions.SessionName).FirstOrDefault(),
                                     SessionCode = starredQuestions.SessionID,
                                     Date = starredQuestions.SessionDate,
                                     QuestionType = 1,
                                     QuestionNo = starredQuestions.QuestionNo,
                                     Subject = (starredQuestions.EditedDescription.Contains("(") && starredQuestions.EditedDescription.Contains(") -")) ? starredQuestions.EditedDescription.Substring(starredQuestions.EditedDescription.IndexOf("(") + 1, (starredQuestions.EditedDescription.IndexOf(") -") - 4)) : starredQuestions.EditedDescription,
                                     MemberName = (from Members in db.mMember
                                                   where Members.MemberCode == starredQuestions.MinisterId
                                                   select Members.Name).FirstOrDefault(),
                                     MinisterName = (from Ministers in db.mMinsitryMinister
                                                     where Ministers.MinsitryMinistersID == starredQuestions.MinisterId
                                                     select Ministers.MinisterName).FirstOrDefault(),
                                     RefPageNo = starredQuestions.RefPageNo,
                                     DepartmentName = (from Departments in db.mDepartment
                                                       where Departments.deptId == (from mMinistryDepartments in db.mMinistryDepartments
                                                                                    where mMinistryDepartments.MinistryDepartmentsID == starredQuestions.DepartmentId
                                                                                    select mMinistryDepartments.DeptID).FirstOrDefault()
                                                       select Departments.deptname).FirstOrDefault()
                                 }).AsExpandable().Where(predicate).ToList();
                var uqResults = (from unstarredQuestions in db.tUnStarredQuestionsHistory
                                 select new SearchModel
                                 {
                                     ID = unstarredQuestions.id,
                                     AssemblyID = unstarredQuestions.AssemblyID,
                                     SessionId = unstarredQuestions.SessionID,
                                     Department_Id = unstarredQuestions.DepartmentId,
                                     MinsitryMinistersID = unstarredQuestions.MinisterId,
                                     MemberID = unstarredQuestions.MinisterId,
                                     FilePath = unstarredQuestions.AttachmentName,
                                     AssemblyName = (from Assemblies in db.mAssembly
                                                     where Assemblies.AssemblyID == unstarredQuestions.AssemblyID
                                                     select Assemblies.AssemblyName).FirstOrDefault(),
                                     AssemblyCode = unstarredQuestions.AssemblyID,
                                     SessionName = (from Sessions in db.mSession
                                                    where Sessions.SessionCode == unstarredQuestions.SessionID
                                                    select Sessions.SessionName).FirstOrDefault(),
                                     SessionCode = unstarredQuestions.SessionID,
                                     Date = unstarredQuestions.SessionDate,
                                     QuestionType = 2,
                                     QuestionNo = unstarredQuestions.QuestionNo,
                                     Subject = (unstarredQuestions.EditedDescription.Contains("(") && unstarredQuestions.EditedDescription.Contains(") -")) ? unstarredQuestions.EditedDescription.Substring(unstarredQuestions.EditedDescription.IndexOf("(") + 1, (unstarredQuestions.EditedDescription.IndexOf(") -") - 4)) : unstarredQuestions.EditedDescription,
                                     MemberName = (from Members in db.mMember
                                                   where Members.MemberCode == unstarredQuestions.MinisterId
                                                   select Members.Name).FirstOrDefault(),
                                     MinisterName = (from Ministers in db.mMinsitryMinister
                                                     where Ministers.MinsitryMinistersID == unstarredQuestions.MinisterId
                                                     select Ministers.MinisterName).FirstOrDefault(),
                                     RefPageNo = unstarredQuestions.RefPageNo,
                                     DepartmentName = (from Departments in db.mDepartment
                                                       where Departments.deptId == (from mMinistryDepartments in db.mMinistryDepartments
                                                                                    where mMinistryDepartments.MinistryDepartmentsID == unstarredQuestions.DepartmentId
                                                                                    select mMinistryDepartments.DeptID).FirstOrDefault()
                                                       select Departments.deptname).FirstOrDefault()
                                 }).AsExpandable().Where(predicate).ToList();
                //var Questions = sqResults.Union(uqResults);
                model.objList = sqResults.Union(uqResults).ToList();
            }

            List<SearchModel> ListQuestions = new List<SearchModel>();

            foreach (var item in model.objList)
            {
                string pageNos = "";
                int pCount = 0;
                var pageno = (from debates in db.tReporterDescriptionLogs
                              where debates.RefNo == item.QuestionNo
                              select debates.EditedVersionPageNumber).ToList();
                foreach (var pno in pageno)
                {
                    ++pCount;
                    pageNos += pno.ToString();
                    if (pCount != (pageno.Count()))
                    {
                        pageNos += ",";
                    }
                }
                string[] pgSplit = pageNos.Split(',');
                pageNos = "";
                for (int j = 0; j < pgSplit.Count(); j++)
                {
                    if (!string.IsNullOrEmpty(pgSplit[j]))
                    {
                        string[] pageList = (pgSplit[j].Contains("-")) ? pgSplit[j].Split('-') : null;
                        if (pageList != null)
                        {
                            int pageStart = (!string.IsNullOrEmpty(pageList[0])) ? int.Parse(pageList[0]) : -1;
                            int pageEnd = (!string.IsNullOrEmpty(pageList[1])) ? int.Parse(pageList[1]) : -1;

                            if (pageNos != "")
                                pageNos += ",";
                            if (pageEnd > 9)
                            {
                                bool b = false;
                            }
                            for (int i = pageStart; ; i++)
                            {
                                pageNos += (i < 10) ? ("0" + i.ToString()) : i.ToString();
                                if (i == pageEnd)
                                {
                                    break;
                                }
                                else
                                {
                                    pageNos += ",";
                                }
                            }
                        }
                        else
                        {
                            //pageNos = item.PageNos;
                            if (pageNos != "")
                                pageNos += ",";
                            pageNos += pgSplit[j];
                        }
                        //test.PageNos = pageNos;
                    }
                }

                if (pageNos != "")
                {
                    bool a = false;
                }
                item.RefPageNo = pageNos;
                ListQuestions.Add(item);
            }
            model.objList = ListQuestions;

            return model;
        }

        #region memberiwse report added by durgesh

        public static object StartedQuestioncount(object param)
        {
            string[] arr = param as string[];
            int memberCode = Convert.ToInt32(arr[0]);
            int assemblyCode = Convert.ToInt32(arr[1]);
            int SessionCode = Convert.ToInt32(arr[2]);

            using (QuestionsContext context = new QuestionsContext())
            {
                List<memberWiseQuestionReport> ws = new List<memberWiseQuestionReport>();
                var Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/") && list.MemberID == memberCode && list.QuestionType == 1
                                select list
                                        );
                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "Starred  Question";
                oq.OnlineCount = Question.Where(a => a.IsOnlineSubmitted == true).Count();
                List<memberWiseQuestionReportList> Online = new List<memberWiseQuestionReportList>();
                //foreach (var item in Question.Where(a => a.IsOnlineSubmitted == true).ToList())
                //{
                //    // Online.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeRecievedDate.HasValue ? item.NoticeRecievedDate.ToString() : "-", Subject = item.Subject });
                //}
                oq.OnlineCountList = Online;

                oq.offlineCount = Question.Where(a => a.IsOnlineSubmitted == false).Count();
                List<memberWiseQuestionReportList> offline = new List<memberWiseQuestionReportList>();
                //foreach (var item in Question.Where(a => a.IsOnlineSubmitted == false).ToList())
                //{
                //    //offline.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeRecievedDate.HasValue ? item.NoticeRecievedDate.ToString() : "-", Subject = item.Subject });
                //}
                oq.offlineCountList = offline;

                oq.postpondCount = Question.Where(a => a.IsPostpone == true).Count();
                List<memberWiseQuestionReportList> postpond = new List<memberWiseQuestionReportList>();
                //foreach (var item in Question.Where(a => a.DeActivateFlag == "RF").ToList())
                //{
                //    //postpond.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.IsPostponeDate.HasValue ? item.IsPostponeDate.ToString() : "-", Subject = item.Subject });
                //}
                oq.postpondCounttList = postpond;
                oq.totalCount = oq.OnlineCount + oq.offlineCount + oq.postpondCount;
                oq.Type = "Starred";
                ws.Add(oq);
                ws.Add((memberWiseQuestionReport)unStartedQuestioncount(param));

                var noticeList = (from list in context.Events

                                  where list.PaperCategoryTypeId == 4
                                  select new
                                  {
                                      list.RuleNo,
                                      list.EventId
                                  }
                                    ).Distinct().ToList();
                foreach (var nitem in noticeList)
                {
                    ws.Add((memberWiseQuestionReport)Noticescount(new string[] { memberCode.ToString(), assemblyCode.ToString(), SessionCode.ToString(), nitem.RuleNo.ToString(), nitem.EventId.ToString() }));
                }
                context.Close();
                return ws;
            }
        }

        public static object unStartedQuestioncount(object param)
        {
            string[] arr = param as string[];
            int memberCode = Convert.ToInt32(arr[0]);
            int assemblyCode = Convert.ToInt32(arr[1]);
            int SessionCode = Convert.ToInt32(arr[2]);

            using (QuestionsContext context = new QuestionsContext())
            {
                var Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/") && list.MemberID == memberCode && list.QuestionType == 2
                                select list
                                        );
                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "UnStarred  Question";
                oq.OnlineCount = Question.Where(a => a.IsOnlineSubmitted == true).Count();
                List<memberWiseQuestionReportList> Online = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.IsOnlineSubmitted == true).ToList())
                {
                    Online.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeRecievedDate.HasValue ? item.NoticeRecievedDate.ToString() : "-", Subject = item.Subject });
                }
                oq.OnlineCountList = Online;

                oq.offlineCount = Question.Where(a => a.IsOnlineSubmitted == false).Count();
                List<memberWiseQuestionReportList> offline = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.IsOnlineSubmitted == false).ToList())
                {
                    offline.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeRecievedDate.HasValue ? item.NoticeRecievedDate.ToString() : "-", Subject = item.Subject });
                }
                oq.offlineCountList = offline;

                oq.postpondCount = Question.Where(a => a.IsPostpone == true).Count();
                List<memberWiseQuestionReportList> postpond = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.DeActivateFlag == "RF").ToList())
                {
                    postpond.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.IsPostponeDate.HasValue ? item.IsPostponeDate.ToString() : "-", Subject = item.Subject });
                }
                oq.postpondCounttList = postpond;
                oq.totalCount = oq.OnlineCount + oq.offlineCount + oq.postpondCount;
                oq.Type = "UnStarred";

                context.Close();
                return oq;
            }
        }

        public static object Noticescount(object param)
        {
            string[] arr = param as string[];
            int memberCode = Convert.ToInt32(arr[0]);
            int assemblyCode = Convert.ToInt32(arr[1]);
            int SessionCode = Convert.ToInt32(arr[2]);
            string NoticeTypeID = arr[3];
            string NoticeNAme = arr[4];
            using (QuestionsContext context = new QuestionsContext())
            {
                var eventList = (from list in context.Events
                                 where list.RuleNo == NoticeTypeID
                                 select list.EventId).FirstOrDefault();

                var Notice = (from list in context.tMemberNotice
                              where list.AssemblyID == assemblyCode && list.SessionID == SessionCode &&  list.MemberId == memberCode && list.NoticeTypeID == eventList
                              select list
                                        );
                var result = Notice.ToList();
                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "Under Rule " + NoticeTypeID;
                oq.OnlineCount = Notice.Where(a => a.IsOnlineSubmitted == true && a.NoticeNumber.Contains(assemblyCode + "/" + SessionCode)).Count();
                List<memberWiseQuestionReportList> Online = new List<memberWiseQuestionReportList>();
                foreach (var item in Notice.Where(a => a.IsOnlineSubmitted == true ).ToList())
                {
                    Online.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeDate.ToString(), Subject = item.Subject });
                }
                oq.OnlineCountList = Online;

                oq.offlineCount = Notice.Where(a => a.IsOnlineSubmitted == false && a.NoticeNumber.Contains(assemblyCode + "/" + SessionCode)).Count();
                List<memberWiseQuestionReportList> offline = new List<memberWiseQuestionReportList>();
                foreach (var item in Notice.Where(a => a.IsOnlineSubmitted == false).ToList())
                {
                    offline.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeDate.ToString(), Subject = item.Subject });
                }
                oq.offlineCountList = offline;
                oq.postpondCount = Notice.Where(a => a.DeActivateFlag == "RF").Count();
                List<memberWiseQuestionReportList> postpond = new List<memberWiseQuestionReportList>();
                foreach (var item in Notice.Where(a => a.DeActivateFlag == "RF").ToList())
                {
                    postpond.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.IsPostponeDate.HasValue ? item.IsPostponeDate.ToString() : "-", Subject = item.Subject });
                }
                oq.postpondCounttList = postpond;
                oq.totalCount = oq.OnlineCount + oq.offlineCount;
                oq.Type = "Notice";
                oq.ruleNO = NoticeTypeID;
                context.Close();
                return oq;
            }
        }

        public static object getNoticeListByMemberCode(object param)
        {
            string[] arr = param as string[];
            int memberCode = Convert.ToInt32(arr[0]);
            int assemblyCode = Convert.ToInt32(arr[1]);
            int SessionCode = Convert.ToInt32(arr[2]);
            string Qtype = arr[3];
            string ruleNo = arr[4];
            using (QuestionsContext context = new QuestionsContext())
            {
                var eventList = (from list in context.Events
                                 where list.RuleNo == ruleNo
                                 select list.EventId).FirstOrDefault();

                var Notice = (from list in context.tMemberNotice
                              where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberId == memberCode && list.NoticeTypeID == eventList
                              select list
                                        );
                if (Qtype == "Online") { Notice = Notice.Where(a => a.IsOnlineSubmitted == true); }
                else if (Qtype == "Manual") { Notice = Notice.Where(a => a.IsOnlineSubmitted == false); }
                else if (Qtype == "notAdmitted")
                {
                    Notice = Notice.Where(a => a.FixedDate != null);
                }
                else if (Qtype == "admitted")
                {
                    Notice = Notice.Where(a => a.FixedDate == null);
                }
                else if (Qtype == "changeType")
                {
                    Notice = Notice.Where(a => a.TypeChangeDate != null);
                }
                else if (Qtype == "breacked")
                {
                    Notice = Notice.Where(a => a.IsBracketCM == true);
                }
                else if (Qtype == "clubbed")
                {
                    Notice = Notice.Where(a => a.IsCMClubbed == true);
                }
                else if (Qtype == "raisedInHouse")
                {
                    Notice = (from list in context.tPaperLaidV
                              join n in Notice
                              on list.PaperLaidId equals n.PaperLaidId
                              where
                             list.IsLaid == true
                              select n);
                }
                else if (Qtype == "withdrawByMember")
                {
                    Notice = Notice.Where(a => a.IsWithdrawbyMember == true);
                }
                else if (Qtype == "excessQuestion")
                {
                    Notice = Notice.Where(a => a.IsExcessQuestion == true);
                }
                else if (Qtype == "timeBarred")
                {
                    Notice = Notice.Where(a => a.IsTimeBarred == true);
                }

                List<memberWiseQuestionList> questionList = new List<memberWiseQuestionList>();
                foreach (var item in Notice.ToList())
                {
                    string Path = string.Empty;
                    questionList.Add(new memberWiseQuestionList()
                    {
                        ConstituencyName = item.ConstituencyName,
                        DepartmentID = (from dep in context.mDepartment
                                        where dep.deptId == item.DepartmentId
                                        select dep.deptname).FirstOrDefault(),
                        DiaryNumber = item.DiaryNo,
                        NoticeRecievedDate = Convert.ToDateTime(item.NoticeDate).ToString("dd-MM-yyyy"),
                        PaperLaidId = item.PaperLaidId.HasValue ? item.PaperLaidId : 0,
                        QuestionID = item.QuestionID,
                        noticeNumber = item.NoticeNumber,
                        noticeType = (from events in context.Events
                                      where events.EventId == item.NoticeTypeID
                                      select events.EventName).FirstOrDefault(),
                        subject = item.Subject,
                        Minister = (from Min in context.mMinistries
                                    join minis in context.mMinsitryMinister on Min.MinistryID equals minis.MinistryID
                                    where minis.MinistryID == item.MinistryId
                                    select minis.MinisterName).FirstOrDefault(),

                        filePath = (from SS in context.mSiteSettings
                                    where SS.SettingName == "SecureFileAccessingUrlPath"
                                    select SS.SettingValue).FirstOrDefault() + "/" + item.OriDiaryFilePath + item.OriDiaryFileName
                    });
                }
                context.Close();
                return questionList;
            }
        }

        public static object getNoticeListForAll(object param)
        {
            string[] arr = param as string[];

            int assemblyCode = Convert.ToInt32(arr[0]);
            int SessionCode = Convert.ToInt32(arr[1]);
            string Qtype = arr[2];
            string ruleNo = arr[3];
            using (QuestionsContext context = new QuestionsContext())
            {
                var eventList = (from list in context.Events
                                 where list.RuleNo == ruleNo
                                 select list.EventId).FirstOrDefault();

                var Notice = (from list in context.tMemberNotice
                              where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.NoticeTypeID == eventList
                              select list
                                        );
                if (Qtype == "Online") { Notice = Notice.Where(a => a.IsOnlineSubmitted == true && a.NoticeNumber.Contains(assemblyCode + "/" + SessionCode)); }
                else if (Qtype == "Manual") { Notice = Notice.Where(a => a.IsOnlineSubmitted == false && a.NoticeNumber.Contains(assemblyCode + "/" + SessionCode)); }
                else if (Qtype == "Postpone") { Notice = Notice.Where(a => a.DeActivateFlag == "Rf"); }

                List<memberWiseQuestionList> questionList = new List<memberWiseQuestionList>();
                foreach (var item in Notice.ToList())
                {
                    var time = TimeSpan.Parse(Convert.ToString(item.NoticeTime));

                    DateTime mtime = DateTime.Today + time;
                    String result = mtime.ToString("hh:mm tt");

                   // var output = time.ToString(@"hh\:mm\:ss");
                    questionList.Add(new memberWiseQuestionList()
                    {
                        ConstituencyName = item.ConstituencyName,
                        DepartmentID = (from dep in context.mDepartment
                                        where dep.deptId == item.DepartmentId
                                        select dep.deptname).FirstOrDefault(),
                        DiaryNumber = item.DiaryNo,
                        NoticeRecievedDate = Convert.ToDateTime(item.NoticeDate).ToString("dd-MM-yyyy"),
                        PaperLaidId = item.PaperLaidId.HasValue ? item.PaperLaidId : 0,
                        QuestionID = item.QuestionID,
                        noticeNumber = item.NoticeNumber,
                        ReceivedTime = Convert.ToString(result),
                        noticeType = (from events in context.Events
                                      where events.EventId == item.NoticeTypeID
                                      select events.EventName).FirstOrDefault(),
                        subject = item.Subject,
                        Minister = (from Min in context.mMinistries
                                    join minis in context.mMinsitryMinister on Min.MinistryID equals minis.MinistryID
                                    where minis.MinistryID == item.MinistryId
                                    select minis.MinisterName).FirstOrDefault(),
                        filePath = (from SS in context.mSiteSettings
                                    where SS.SettingName == "SecureFileAccessingUrlPath"
                                    select SS.SettingValue).FirstOrDefault() + "/" + item.OriDiaryFilePath + item.OriDiaryFileName,
                        MemberName = (from Mem in context.mMember
                                      where Mem.MemberCode == item.MemberId
                                      select Mem.Name).FirstOrDefault()
                    });
                }
                context.Close();
                return questionList;
            }
        }

        public static object getQuestionListByMemberCode(object param)
        {
            string[] arr = param as string[];
            List<tQuestion> Question = null;
            int memberCode = Convert.ToInt32(arr[0]);
            int assemblyCode = Convert.ToInt32(arr[1]);
            int SessionCode = Convert.ToInt32(arr[2]);
            string type = arr[3];
            string online = arr[4];
            int qtype = 0;
            if (type == "Starred")
            {
                qtype = 1;
            }
            else
            {
                qtype = 2;
            }
            using (QuestionsContext context = new QuestionsContext())
            {
                if (online == "Online")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype && list.IsOnlineSubmitted == true
                                select list
                                              ).ToList();
                }
                else if (online == "Manual")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype && list.IsOnlineSubmitted == false
                                select list
                                             ).ToList();
                }
                else if (online == "Postpone")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype && list.DeActivateFlag == "RF"
                                select list
                                             ).ToList();
                }
                else if (online == "Total")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                select list
                                             ).ToList();
                }
                else if (online == "totalCount")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                select list
                                             ).ToList();
                }
                else if (online == "notAdmitted")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                              && list.IsFixed == false
                                select list
                                             ).ToList();
                }
                else if (online == "admitted")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                 && list.IsFixed == true
                                select list
                                             ).ToList();
                }
                else if (online == "changeType")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                 && list.IsTypeChange == false
                                select list
                                             ).ToList();
                }
                else if (online == "breacked")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                 && list.IsBracket == false
                                select list
                                             ).ToList();
                }
                else if (online == "clubbed")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                 && list.IsClubbed == false
                                select list
                                            ).ToList();
                }
                else if (online == "raisedInHouse")
                {
                    Question = (from list in context.tQuestions
                                join tpaper in context.tPaperLaidV
                                on list.PaperLaidId equals tpaper.PaperLaidId
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                               && tpaper.IsLaid == true
                                select list
                                              ).ToList();
                }
                else if (online == "withdrawByMember")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                 && list.IsWithdrawbyMember == true
                                select list
                                             ).ToList();
                }
                else if (online == "excessQuestion")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                  && list.IsWithdrawbyMember == true
                                select list
                                             ).ToList();
                }
                else if (online == "timeBarred")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.MemberID == memberCode && list.QuestionType == qtype
                                  && list.IsTimeBarred == true
                                select list
                                             ).ToList();
                }
                List<memberWiseQuestionList> questionList = new List<memberWiseQuestionList>();
                foreach (var item in Question)
                {
                    questionList.Add(new memberWiseQuestionList()
                    {
                        ConstituencyName = item.ConstituencyName,
                        DepartmentID = (from dep in context.mDepartment
                                        where dep.deptId == item.DepartmentID
                                        select dep.deptname).FirstOrDefault(),
                        DiaryNumber = item.DiaryNumber,
                        NoticeRecievedDate = item.NoticeRecievedDate.HasValue ? Convert.ToDateTime(item.NoticeRecievedDate).ToString("dd-MM-yyyy") : "-",
                        PaperLaidId = item.PaperLaidId.HasValue ? item.PaperLaidId : 0,
                        QuestionID = item.QuestionID,
                        QuestionNumber = item.QuestionNumber.HasValue ? item.QuestionNumber : 0,
                        subject = item.Subject,
                        Minister = (from Min in context.mMinistries
                                    join minis in context.mMinsitryMinister on Min.MinistryID equals minis.MinistryID
                                    where minis.MinistryID == item.MinistryId
                                    select minis.MinisterName).FirstOrDefault()
                    });
                }
                context.Close();
                return questionList;
            }
        }

        public static object getQuestionListForAll(object param)
        {
            string[] arr = param as string[];
            List<tQuestion> Question = null;

            int assemblyCode = Convert.ToInt32(arr[0]);
            int SessionCode = Convert.ToInt32(arr[1]);
            string type = arr[2];
            string online = arr[3];
            int qtype = 0;
            if (type == "Starred")
            {
                qtype = 1;
            }
            else
            {
                qtype = 2;
            }
            using (QuestionsContext context = new QuestionsContext())
            {
                if (online == "Online")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype && list.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/") && list.IsOnlineSubmitted == true
                                select list
                                              ).ToList();
                }
                else if (online == "Manual")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype && list.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/") && list.IsOnlineSubmitted == false
                                select list
                                             ).ToList();
                }
                else if (online == "Postpone")
                {
                    Question = (from list in context.tQuestions
                               // where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype && list.DeActivateFlag == "RF"
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype && list.CurentStatusPQ == "RF"
                                select list
                                             ).ToList();
                }
                else if (online == "Total")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/") && list.QuestionType == qtype
                                select list
                                             ).ToList();
                }
                else if (online == "totalCount")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                select list
                                             ).ToList();
                }
                else if (online == "notAdmitted")
                {
                    //Question = (from list in context.tQuestions
                    //            where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                    //          && list.IsFixed == false
                    //            select list
                    //                         ).ToList();
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                && list.IsRejected == true
                                select list
                               ).ToList();
                }
                else if (online == "admitted")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                 && list.IsFixed == true && list.IsFinalApproved == true && list.QuestionNumber != null && list.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/") 
                                select list
                                             ).ToList();
                }
                else if (online == "changeType")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                 && list.IsTypeChange == true
                                select list
                                             ).ToList();
                }
                else if (online == "breacked")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                 && list.IsBracket == true
                                select list
                                             ).ToList();
                }
                else if (online == "clubbed")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                 && list.IsClubbed == true
                                select list
                                            ).ToList();
                }
                else if (online == "raisedInHouse")
                {
                    Question = (from list in context.tQuestions
                                join tpaper in context.tPaperLaidV
                                on list.PaperLaidId equals tpaper.PaperLaidId
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                               && tpaper.IsLaid == true
                                select list
                                              ).ToList();
                }
                else if (online == "withdrawByMember")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                 && list.IsWithdrawbyMember == true
                                select list
                                             ).ToList();
                }
                else if (online == "excessQuestion")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                  && list.IsWithdrawbyMember == true
                                select list
                                             ).ToList();
                }
                else if (online == "timeBarred")
                {
                    Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == qtype
                                  && list.IsTimeBarred == true
                                select list
                                             ).ToList();
                }
                List<memberWiseQuestionList> questionList = new List<memberWiseQuestionList>();
                foreach (var item in Question)
                {
                    var time = TimeSpan.Parse(Convert.ToString(item.NoticeRecievedTime));
                    DateTime mtime = DateTime.Today + time;
                    String result = mtime.ToString("hh:mm tt");


                    //var time = TimeSpan.Parse(Convert.ToString(item.NoticeRecievedTime));
                    //var output = time.ToString(@"hh\:mm\:ss");
                    questionList.Add(new memberWiseQuestionList()
                    {
                        ConstituencyName = item.ConstituencyName,
                        DepartmentID = (from dep in context.mDepartment
                                        where dep.deptId == item.DepartmentID
                                        select dep.deptname).FirstOrDefault(),
                        DiaryNumber = item.DiaryNumber,
                        NoticeRecievedDate = item.NoticeRecievedDate.HasValue ? Convert.ToDateTime(item.NoticeRecievedDate).ToString("dd-MM-yyyy") : "-",
                        PaperLaidId = item.PaperLaidId.HasValue ? item.PaperLaidId : 0,
                        QuestionID = item.QuestionID,
                        QuestionNumber = item.QuestionNumber.HasValue ? item.QuestionNumber : 0,
                        subject = item.Subject,
                        ReceivedTime = Convert.ToString(result),
                        Minister = (from Min in context.mMinistries
                                    join minis in context.mMinsitryMinister on Min.MinistryID equals minis.MinistryID
                                    where minis.MinistryID == item.MinistryId
                                    select minis.MinisterName).FirstOrDefault(),

                        MemberName = (from Mem in context.mMember
                                      where Mem.MemberCode == item.MemberID
                                      select Mem.Name).FirstOrDefault()
                    });
                }
                context.Close();
                return questionList;
            }
        }

        public static object getQuestionByID(object param)
        {
            long QuestionID = Convert.ToInt64(param.ToString());
            using (QuestionsContext context = new QuestionsContext())
            {
                var item = (from list in context.tQuestions
                            where list.QuestionID == QuestionID
                            select list
                                            ).FirstOrDefault();

                memberWiseQuestionList questionList = new memberWiseQuestionList()

                {
                    ConstituencyName = item.ConstituencyName,
                    DepartmentID = (from dep in context.mDepartment
                                    where dep.deptId == item.DepartmentID
                                    select dep.deptname).FirstOrDefault(),
                    DiaryNumber = item.DiaryNumber,
                    NoticeRecievedDate = item.NoticeRecievedDate.HasValue ? Convert.ToDateTime(item.NoticeRecievedDate).ToString("dd-MM-yyyy") : "-",
                    PaperLaidId = item.PaperLaidId.HasValue ? item.PaperLaidId : 0,
                    QuestionID = item.QuestionID,
                    QuestionNumber = item.QuestionNumber.HasValue ? item.QuestionNumber : 0,
                    subject = item.Subject,
                    Minister = (from Min in context.mMinistries
                                join minis in context.mMinsitryMinister on Min.MinistryID equals minis.MinistryID
                                where minis.MinistryID == item.MinistryId
                                select minis.MinisterName).FirstOrDefault(),
                    MainQuestion = item.MainQuestion,
                    MainAnswer = item.MainAnswer
                };
                context.Close();
                return questionList;
            }
        }

        #endregion memberiwse report added by durgesh

        #region Allmember report added by durgesh

        public static object StartedQuestioncountForAll(object param)
        {
            string[] arr = param as string[];

            int assemblyCode = Convert.ToInt32(arr[0]);
            int SessionCode = Convert.ToInt32(arr[1]);

            using (QuestionsContext context = new QuestionsContext())
            {
                List<memberWiseQuestionReport> ws = new List<memberWiseQuestionReport>();
                var Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == 1
                                select list
                                        );
                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "Starred  Question";
               // oq.OnlineCount = Question.Where(a => a.IsOnlineSubmitted == true).Count();
                oq.OnlineCount = Question.Where(a => a.IsOnlineSubmitted == true && a.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/")).Count();
                List<memberWiseQuestionReportList> Online = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.IsOnlineSubmitted == true).ToList())
                {
                    Online.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeRecievedDate.HasValue ? item.NoticeRecievedDate.ToString() : "-", Subject = item.Subject });
                }
                oq.OnlineCountList = Online;

                oq.offlineCount = Question.Where(a => a.IsOnlineSubmitted == false && a.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/")).Count();
                List<memberWiseQuestionReportList> offline = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.IsOnlineSubmitted == false).ToList())
                {
                    offline.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeRecievedDate.HasValue ? item.NoticeRecievedDate.ToString() : "-", Subject = item.Subject });
                }
                oq.offlineCountList = offline;
                oq.postpondCount = Question.Where(a => a.CurentStatusPQ == "RF").Count();
                //oq.postpondCount = Question.Where(a => a.IsPostpone == true).Count();
                List<memberWiseQuestionReportList> postpond = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.CurentStatusPQ == "RF").ToList())
                {
                    postpond.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.IsPostponeDate.HasValue ? item.IsPostponeDate.ToString() : "-", Subject = item.Subject });
                }
                oq.postpondCounttList = postpond;
               // oq.totalCount = oq.OnlineCount + oq.offlineCount + oq.postpondCount;
                oq.totalCount = oq.OnlineCount + oq.offlineCount ;
                oq.Type = "Starred";
                ws.Add(oq);
                ws.Add((memberWiseQuestionReport)unStartedQuestioncountForAll(param));

                var noticeList = (from list in context.Events
                                  orderby list.ruleOrderNumber
                                  where list.PaperCategoryTypeId == 4
                                  select new
                                  {
                                      list.RuleNo,
                                      list.EventId,
                                      list.ruleOrderNumber
                                  }
                                    ).ToList();
                foreach (var nitem in noticeList)
                {
                    ws.Add((memberWiseQuestionReport)NoticescountForAll(new string[] { assemblyCode.ToString(), SessionCode.ToString(), nitem.RuleNo.ToString(), nitem.EventId.ToString() }));
                }
                context.Close();
                return ws;
            }
        }

        public static object unStartedQuestioncountForAll(object param)
        {
            string[] arr = param as string[];

            int assemblyCode = Convert.ToInt32(arr[0]);
            int SessionCode = Convert.ToInt32(arr[1]);

            using (QuestionsContext context = new QuestionsContext())
            {
                var Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == 2
                                select list
                                        );
                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "UnStarred  Question";
                oq.OnlineCount = Question.Where(a => a.IsOnlineSubmitted == true && a.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/")).Count();
                List<memberWiseQuestionReportList> Online = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.IsOnlineSubmitted == true).ToList())
                {
                    Online.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeRecievedDate.HasValue ? item.NoticeRecievedDate.ToString() : "-", Subject = item.Subject });
                }
                oq.OnlineCountList = Online;

                oq.offlineCount = Question.Where(a => a.IsOnlineSubmitted == false && a.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/")).Count();
                List<memberWiseQuestionReportList> offline = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.IsOnlineSubmitted == false).ToList())
                {
                    offline.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeRecievedDate.HasValue ? item.NoticeRecievedDate.ToString() : "-", Subject = item.Subject });
                }
                oq.offlineCountList = offline;
                oq.postpondCount = Question.Where(a => a.CurentStatusPQ == "RF").Count();            
                List<memberWiseQuestionReportList> postpond = new List<memberWiseQuestionReportList>();
                foreach (var item in Question.Where(a => a.CurentStatusPQ == "RF").ToList())
                {
                    postpond.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.IsPostponeDate.HasValue ? item.IsPostponeDate.ToString() : "-", Subject = item.Subject });
                }
                oq.postpondCounttList = postpond;
                oq.totalCount = oq.OnlineCount + oq.offlineCount;
                oq.Type = "UnStarred";

                context.Close();
                return oq;
            }
        }

        public static object NoticescountForAll(object param)
        {
            string[] arr = param as string[];

            int assemblyCode = Convert.ToInt32(arr[0]);
            int SessionCode = Convert.ToInt32(arr[1]);
            string NoticeTypeID = arr[2];
            string NoticeNAme = arr[3];
            using (QuestionsContext context = new QuestionsContext())
            {
                var eventList = (from list in context.Events
                                 where list.RuleNo == NoticeTypeID

                                 select list.EventId).FirstOrDefault();

                var Notice = (from list in context.tMemberNotice
                              where list.AssemblyID == assemblyCode && list.SessionID == SessionCode  && list.NoticeTypeID == eventList
                              select list
                                        );
                var result = Notice.ToList();
                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "Under Rule " + NoticeTypeID;
                oq.OnlineCount = Notice.Where(a => a.IsOnlineSubmitted == true && a.NoticeNumber.Contains(assemblyCode + "/" + SessionCode)).Count();
                List<memberWiseQuestionReportList> Online = new List<memberWiseQuestionReportList>();
                foreach (var item in Notice.Where(a => a.IsOnlineSubmitted == true ).ToList())
                {
                    Online.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeDate.ToString(), Subject = item.Subject });
                }
                oq.OnlineCountList = Online;

                oq.offlineCount = Notice.Where(a => a.IsOnlineSubmitted == false && a.NoticeNumber.Contains(assemblyCode + "/" + SessionCode)).Count();
                List<memberWiseQuestionReportList> offline = new List<memberWiseQuestionReportList>();
                foreach (var item in Notice.Where(a => a.IsOnlineSubmitted == false).ToList())
                {
                    offline.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.NoticeDate.ToString(), Subject = item.Subject });
                }
                oq.offlineCountList = offline;

                oq.postpondCount = Notice.Where(a => a.DeActivateFlag == "RF").Count();
                List<memberWiseQuestionReportList> postpond = new List<memberWiseQuestionReportList>();
                foreach (var item in Notice.Where(a => a.DeActivateFlag == "RF").ToList())
                {
                    postpond.Add(new memberWiseQuestionReportList() { QuestionID = item.QuestionID, Date = item.IsPostponeDate.HasValue ? item.IsPostponeDate.ToString() : "-", Subject = item.Subject });
                }
                oq.postpondCounttList = postpond;

                oq.totalCount = oq.OnlineCount + oq.offlineCount ;
                oq.Type = "Notice";
                oq.ruleNO = NoticeTypeID;
                context.Close();
                return oq;
            }
        }

        #endregion Allmember report added by durgesh

        #region Complete Status Report

        public static object CompleteStatusReport(object param)
        {
            string[] arr = param as string[];
            int memberCode = Convert.ToInt32(arr[0]);
            int assemblyCode = Convert.ToInt32(arr[1]);
            int SessionCode = Convert.ToInt32(arr[2]);

            using (QuestionsContext context = new QuestionsContext())
            {
                List<memberWiseQuestionReport> ws = new List<memberWiseQuestionReport>();

                var DatesId = (from list in context.mSessionDate
                               where list.AssemblyId == assemblyCode && list.SessionId == SessionCode
                               select list.Id);

                var allIds = string.Join(",", DatesId);
          
                var Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == 1
                                select list
                                        );
                if (memberCode != 0)
                {
                    Question = Question.Where(a => a.MemberID == memberCode);
                }

                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "Starred  Question";
               // oq.totalCount = Question.Count();
                oq.totalCount = Question.Where(a => a.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/")).Count();           
                	string[] values = allIds.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int Did = int.Parse(values[i]);
                        var item = (from list in context.tQuestions
                                    //where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.StatusPQ == null && list.SessionDateId == Did && list.QuestionType == 1 && list.IsFinalApproved == true
                                    where list.AssemblyID == assemblyCode && list.SessionID == SessionCode  && list.SessionDateId == Did && list.QuestionType == 1 && list.IsFinalApproved == true && list.QuestionNumber!=null && list.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/")
                                    select list).Count();


                        if (oq.admitted == null)
                        {
                            oq.admitted = item;
                        }
                        else
                        {
                            oq.admitted = oq.admitted + item;
                        }

                    }

               
                //oq.admitted = Question.Where(a => a.IsFixed == true).Count();
                oq.notAdmitted = Question.Where(a => a.IsRejected == true).Count();
                oq.changeType = Question.Where(a => a.IsTypeChange == true).Count();
                oq.breacked = Question.Where(a => a.IsBracket == true).Count();
                oq.clubbed = Question.Where(a => a.IsClubbed == true).Count();
                oq.raisedInHouse = (from tpaperlaid in context.tPaperLaidV
                                    join que in Question on tpaperlaid.PaperLaidId equals que.PaperLaidId
                                    where tpaperlaid.IsLaid == true
                                    select tpaperlaid
                                        ).Count();
                oq.withdrawByMember = Question.Where(a => a.IsWithdrawbyMember == true).Count(); 
                oq.excessQuestion = Question.Where(a => a.IsExcessQuestion == true).Count();
                oq.timeBarred = Question.Where(a => a.IsTimeBarred == true).Count(); ;
                oq.Type = "Starred";
                ws.Add(oq);
                ws.Add((memberWiseQuestionReport)unStartedCompleteQuestioncount(param));
                var noticeList = (from list in context.Events

                                  where list.PaperCategoryTypeId == 4
                                  select new
                                  {
                                      list.RuleNo,
                                      list.EventId
                                  }
                                    ).Distinct().ToList();
                foreach (var nitem in noticeList)
                {
                    ws.Add((memberWiseQuestionReport)NoticesCompletecount(new string[] { memberCode.ToString(), assemblyCode.ToString(), SessionCode.ToString(), nitem.RuleNo.ToString(), nitem.EventId.ToString() }));
                }
                context.Close();
                return ws;
            }
        }

        public static memberWiseQuestionReport unStartedCompleteQuestioncount(object param)
        {
            string[] arr = param as string[];
            int memberCode = Convert.ToInt32(arr[0]);
            int assemblyCode = Convert.ToInt32(arr[1]);
            int SessionCode = Convert.ToInt32(arr[2]);

            using (QuestionsContext context = new QuestionsContext())
            {

                var DatesId = (from list in context.mSessionDate
                               where list.AssemblyId == assemblyCode && list.SessionId == SessionCode
                               select list.Id);

                var allIds = string.Join(",", DatesId);

                var Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == 2
                                select list
                                        );
                if (memberCode != 0)
                {
                    Question = Question.Where(a => a.MemberID == memberCode);
                }
                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "UnStarred  Question";

                oq.totalCount = Question.Where(a => a.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/")).Count();
                string[] values = allIds.Split(',');

                for (int i = 0; i < values.Length; i++)
                {
                    int Did = int.Parse(values[i]);
                    var item = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.SessionDateId == Did && list.QuestionType == 2 && list.IsFinalApproved == true && list.QuestionNumber != null && list.DiaryNumber.Contains(assemblyCode + "/" + SessionCode + "/")
                                //where list.AssemblyID == assemblyCode 
                                //&& list.SessionID == SessionCode 
                                //&& list.StatusPQ == null 
                                //&& list.SessionDateId == Did 
                                //&& list.QuestionType == 2
                                //&& list.IsFinalApproved==true
                                select list).Count();


                    if (oq.admitted == null)
                    {
                        oq.admitted = item;
                    }
                    else
                    {
                        oq.admitted = oq.admitted + item;
                    }

                }


                //oq.admitted = Question.Where(a => a.IsFixed == true).Count();
                oq.notAdmitted = Question.Where(a => a.IsRejected == true).Count();
              //  oq.totalCount = Question.Count();
              //  oq.notAdmitted = Question.Where(a => a.IsFixed == false).Count();
              //  oq.admitted = Question.Where(a => a.IsFixed == true).Count();
                oq.changeType = Question.Where(a => a.IsTypeChange == true).Count();
                oq.breacked = Question.Where(a => a.IsBracket == true).Count();
                oq.clubbed = Question.Where(a => a.IsClubbed == true).Count();
                oq.raisedInHouse = (from tpaperlaid in context.tPaperLaidV
                                    join que in Question on tpaperlaid.PaperLaidId equals que.PaperLaidId
                                    where tpaperlaid.IsLaid == true
                                    select tpaperlaid
                              ).Count();
                oq.withdrawByMember = Question.Where(a => a.IsWithdrawbyMember == true).Count();
                oq.excessQuestion = Question.Where(a => a.IsExcessQuestion == true).Count();
                oq.timeBarred = Question.Where(a => a.IsTimeBarred == true).Count();
                oq.Type = "UnStarred";

                context.Close();
                return oq;
            }
        }

        public static memberWiseQuestionReport NoticesCompletecount(object param)
        {
            string[] arr = param as string[];
            int memberCode = Convert.ToInt32(arr[0]);
            int assemblyCode = Convert.ToInt32(arr[1]);
            int SessionCode = Convert.ToInt32(arr[2]);
            string NoticeTypeID = arr[3];
            string NoticeNAme = arr[4];
            using (QuestionsContext context = new QuestionsContext())
            {
                var eventList = (from list in context.Events
                                 where list.RuleNo == NoticeTypeID
                                 select list.EventId).FirstOrDefault();

                var Notice = (from list in context.tMemberNotice
                              where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.NoticeTypeID == eventList
                              select list
                                        );
                if (memberCode != 0)
                {
                    Notice = Notice.Where(a => a.MemberId == memberCode);
                }
                var result = Notice.ToList();
                memberWiseQuestionReport oq = new memberWiseQuestionReport();
                oq.ModuleName = "Under Rule " + NoticeTypeID;
                oq.totalCount = Notice.Count();
                //oq.notAdmitted = Notice.Where(a => a.FixedDate == null).Count();
                oq.notAdmitted = 0;
                oq.admitted = Notice.Where(a => a.FixedDate != null).Count();
                oq.changeType = Notice.Where(a => a.IsTypeChange == true).Count();
                oq.breacked = Notice.Where(a => a.IsBracketCM == true).Count();
                oq.clubbed = Notice.Where(a => a.IsCMClubbed == true).Count();
                oq.raisedInHouse = (from tpaperlaid in context.tPaperLaidV
                                    join que in Notice on tpaperlaid.PaperLaidId equals que.PaperLaidId
                                    where tpaperlaid.IsLaid == true
                                    select tpaperlaid
                                     ).Count();
                oq.withdrawByMember = 0;
                oq.excessQuestion = 0;
                oq.timeBarred = 0;
                oq.Type = "Notice";
                oq.ruleNO = NoticeTypeID;
                context.Close();
                return oq;
            }
        }

        #endregion Complete Status Report

        public static object GetHistoryDetailsDiaryNumber(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestionModel questionModel = param as tQuestionModel;

            var query = (from tQ in db.mchangeDepartmentAuditTrail
                         where (tQ.DiaryNumber == questionModel.DiaryNumber)

                         select new tQuestionModel
                         {
                             DepartmentN = (from dept in db.mDepartment
                                            where dept.deptId == tQ.DepartmentID
                                            select dept.deptname).FirstOrDefault(),

                             ChangedDepName = (from dept in db.mDepartment
                                               where dept.deptId == tQ.ChangedDepartmentID
                                               select dept.deptname).FirstOrDefault(),
                             DepartmentId = tQ.DepartmentID,
                             ChangedDepartmentID = tQ.ChangedDepartmentID,
                             UserName = tQ.UserName,
                             ChangedDate = tQ.ChangedDate
                         }).ToList();

            questionModel.objQuestList1 = query;
            foreach (var item in query)
            {
                item.ChangedDateString = Convert.ToDateTime(item.ChangedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
            }
            return questionModel;
        }

        #region QuestionNotice Report

        public static object DepartmentWiseQuestionNoticeReport(object param)
        {
            string[] arr = param as string[];
            string DepartmentId = Convert.ToString(arr[0]);
            int MemberID = Convert.ToInt32(arr[1]);
            string SessionDate = Convert.ToString(arr[2]);

            int SessionDateId = 0;
            if (SessionDate != "Select Session Date")
            {
                string[] arrDate = SessionDate.Split('/');
                //now use array to get specific date object
                string day = arrDate[0].ToString();
                string month = arrDate[1].ToString();
                string year = arrDate[2].ToString();

                SessionDateId = Convert.ToInt32(day);
            }

            int assemblyCode = Convert.ToInt32(arr[3]);
            int SessionCode = Convert.ToInt32(arr[4]);

            using (QuestionsContext context = new QuestionsContext())
            {
                List<DepartmentWiseQuestionNoticeReport> ws = new List<DepartmentWiseQuestionNoticeReport>();

                var Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == 1

                                select list
                                        );
                if (DepartmentId != "0")
                {
                    Question = Question.Where(a => a.DepartmentID == DepartmentId);
                }
                if (MemberID != 0)
                {
                    Question = Question.Where(a => a.MemberID == MemberID);
                }
                if (SessionDate != "Select Session Date")
                {
                    Question = Question.Where(a => a.SessionDateId == SessionDateId);
                }

                DepartmentWiseQuestionNoticeReport oq = new DepartmentWiseQuestionNoticeReport();
                oq.ModuleName = "Starred  Question";
                oq.totalCount = Question.Count();
                oq.Acknowledged = Question.Where(a => a.IsAcknowledgmentDate != null).Count();
                oq.notAcknowledged = Question.Where(a => a.IsAcknowledgmentDate == null).Count();
                oq.Accepted = Question.Where(a => a.IsAcknowledgmentDate == null).Count();
                oq.notAccepted = Question.Where(a => a.IsFixed == true).Count();
                oq.Fixed = Question.Where(a => a.IsFinalApproved == true).Count();
                oq.ReplyRecieved = (from tpaperlaid in context.tPaperLaidTemp
                                    join que in Question on tpaperlaid.PaperLaidId equals que.PaperLaidId
                                    where tpaperlaid.DeptSubmittedDate != null
                                    select tpaperlaid
                                        ).Count();

                oq.ReplyNotRecieved = Question.Where(a => a.PaperLaidId != null).Count();
                oq.QuestionSent = Question.Where(a => a.PaperLaidId == null).Count();
                oq.withdrawByMember = 0;
                oq.excessQuestion = 0;
                oq.timeBarred = 0;
                oq.Type = "Starred";
                ws.Add(oq);
                ws.Add((DepartmentWiseQuestionNoticeReport)unStartedQuestionNoticecount(param));
                var noticeList = (from list in context.Events

                                  where list.PaperCategoryTypeId == 4
                                  select new
                                  {
                                      list.RuleNo,
                                      list.EventId
                                  }
                                    ).Distinct().ToList();
                foreach (var nitem in noticeList)
                {
                    ws.Add((DepartmentWiseQuestionNoticeReport)NoticesDepcount(new string[] { DepartmentId.ToString(), MemberID.ToString(), SessionDate.ToString(), assemblyCode.ToString(), SessionCode.ToString(), nitem.RuleNo.ToString(), nitem.EventId.ToString() }));
                }
                context.Close();
                return ws;
            }
        }

        public static DepartmentWiseQuestionNoticeReport unStartedQuestionNoticecount(object param)
        {
            string[] arr = param as string[];
            string DepartmentId = Convert.ToString(arr[0]);
            int MemberID = Convert.ToInt32(arr[1]);
            string SessionDate = Convert.ToString(arr[2]);

            int SessionDateId = 0;
            if (SessionDate != "Select Session Date")
            {
                string[] arrDate = SessionDate.Split('/');
                //now use array to get specific date object
                string day = arrDate[0].ToString();
                string month = arrDate[1].ToString();
                string year = arrDate[2].ToString();

                SessionDateId = Convert.ToInt32(day);
            }

            int assemblyCode = Convert.ToInt32(arr[3]);
            int SessionCode = Convert.ToInt32(arr[4]);

            using (QuestionsContext context = new QuestionsContext())
            {
                var Question = (from list in context.tQuestions
                                where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.QuestionType == 2
                                select list
                                        );
                if (DepartmentId != "0")
                {
                    Question = Question.Where(a => a.DepartmentID == DepartmentId);
                }

                if (MemberID != 0)
                {
                    Question = Question.Where(a => a.MemberID == MemberID);
                }
                if (SessionDate != "Select Session Date")
                {
                    Question = Question.Where(a => a.SessionDateId == SessionDateId);
                }

                DepartmentWiseQuestionNoticeReport oq = new DepartmentWiseQuestionNoticeReport();
                oq.ModuleName = "UnStarred  Question";
                oq.totalCount = Question.Count();
                oq.Acknowledged = Question.Where(a => a.IsAcknowledgmentDate != null).Count();
                oq.notAcknowledged = Question.Where(a => a.IsAcknowledgmentDate == null).Count();
                oq.Accepted = Question.Where(a => a.IsAcknowledgmentDate == null).Count();
                oq.notAccepted = Question.Where(a => a.IsFixed == true).Count();
                oq.Fixed = Question.Where(a => a.IsFinalApproved == true).Count();
                oq.changeType = Question.Where(a => a.IsTypeChange == true).Count();
                oq.ReplyRecieved = (from tpaperlaid in context.tPaperLaidTemp
                                    join que in Question on tpaperlaid.PaperLaidId equals que.PaperLaidId
                                    where tpaperlaid.DeptSubmittedDate != null
                                    select tpaperlaid
                                      ).Count();

                oq.ReplyNotRecieved = Question.Where(a => a.PaperLaidId != null).Count();
                oq.QuestionSent = Question.Where(a => a.PaperLaidId == null).Count();
                oq.withdrawByMember = 0;
                oq.excessQuestion = 0;
                oq.timeBarred = 0;
                oq.Type = "UnStarred";

                context.Close();
                return oq;
            }
        }

        public static DepartmentWiseQuestionNoticeReport NoticesDepcount(object param)
        {
            string[] arr = param as string[];
            string DepartmentId = Convert.ToString(arr[0]);
            int MemberID = Convert.ToInt32(arr[1]);
            string SessionDate = Convert.ToString(arr[2]);
            int SessionDateId = 0;
            if (SessionDate != "Select Session Date")
            {
                string[] arrDate = SessionDate.Split('/');
                //now use array to get specific date object
                string day = arrDate[0].ToString();
                string month = arrDate[1].ToString();
                string year = arrDate[2].ToString();

                SessionDateId = Convert.ToInt32(day);
            }
            int assemblyCode = Convert.ToInt32(arr[3]);
            int SessionCode = Convert.ToInt32(arr[4]);
            string NoticeTypeID = arr[5];
            string NoticeNAme = arr[6];
            using (QuestionsContext context = new QuestionsContext())
            {
                var eventList = (from list in context.Events
                                 where list.RuleNo == NoticeTypeID
                                 select list.EventId).FirstOrDefault();

                var Notice = (from list in context.tMemberNotice
                              where list.AssemblyID == assemblyCode && list.SessionID == SessionCode && list.NoticeTypeID == eventList
                              select list
                                        );
                if (DepartmentId != "")
                {
                    Notice = Notice.Where(a => a.DepartmentId == DepartmentId);
                }

                if (MemberID != 0)
                {
                    Notice = Notice.Where(a => a.MemberId == MemberID);
                }

                //if (SessionDate != "0")
                //{
                //    Notice = Notice.Where(a => a.SessionDateId == SessionDateId);
                //}
                var result = Notice.ToList();
                DepartmentWiseQuestionNoticeReport oq = new DepartmentWiseQuestionNoticeReport();
                oq.ModuleName = "Under Rule " + NoticeTypeID;
                oq.totalCount = Notice.Count();
                oq.Acknowledged = Notice.Where(a => a.IsAcknowledgmentDate != null).Count();
                oq.notAcknowledged = Notice.Where(a => a.IsAcknowledgmentDate == null).Count();
                oq.Accepted = Notice.Where(a => a.IsAcknowledgmentDate == null).Count();

                oq.changeType = Notice.Where(a => a.IsTypeChange == true).Count();
                oq.ReplyRecieved = (from tpaperlaid in context.tPaperLaidTemp
                                    join que in Notice on tpaperlaid.PaperLaidId equals que.PaperLaidId
                                    where tpaperlaid.DeptSubmittedDate != null
                                    select tpaperlaid
                                       ).Count();

                oq.ReplyNotRecieved = Notice.Where(a => a.PaperLaidId != null).Count();
                oq.QuestionSent = Notice.Where(a => a.PaperLaidId == null).Count();
                oq.withdrawByMember = 0;
                oq.excessQuestion = 0;
                oq.timeBarred = 0;
                oq.Type = "Notice";
                oq.ruleNO = NoticeTypeID;
                context.Close();
                return oq;
            }
        }

        #endregion QuestionNotice Report



        ///For Sending email get list of member and their questions
        ///
        private static object GetQuestionBySessionDateForEmail(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion question = (tQuestion)param;
            if (question.Designation == "Member")
            {
                var LOB = (from Questions in db.tQuestions
                           where Questions.IsFixedDate == question.IsFixedDate
                           && Questions.AssemblyID == question.AssemblyID
                           && Questions.SessionID == question.SessionID
                               // && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                               // && paperLaidTemp.DeptSubmittedDate != null
                           && Questions.IsFinalApproved == true
                           orderby Questions.QuestionNumber
                           select new QuestionModelCustom
                           {
                               // PaperLaidId = paperLaidVs.PaperLaidId,
                               QuestionID = Questions.QuestionID,
                               QuestionNumber = Questions.QuestionNumber,
                               QuestionTypeId = Questions.QuestionType,
                               MemberId = Questions.MemberID,
                               MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                               AssemblyCode = Questions.AssemblyID,
                               SessionCode = Questions.SessionID,
                               SessionDate = Questions.IsFixedDate,
                               SessionDateId = Questions.SessionDateId,
                               EmailSentDate = (from mc in db.tMailStatus
                                                where mc.MemberID == Questions.MemberID
                                                    && mc.AssemblyID == Questions.AssemblyID
                                                    && mc.SessionID == Questions.SessionID
                                                    && mc.SessionDateId == Questions.SessionDateId
                                                select (mc.EmailSentDate)).FirstOrDefault(),
                               // MemberName=Questions.MemberN,
                               //FileName = paperLaidTemp.SignedFilePath,
                               //DocFilePath = paperLaidTemp.FilePath + paperLaidTemp.DocFileName,
                               //IsLaid = paperLaidVs.IsLaid,
                               Subject = Questions.Subject,
                               // Version = paperLaidTemp.Version,
                               // DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                               DepartmentName = (from Departments in db.mDepartment
                                                 where Departments.deptId == Questions.DepartmentID
                                                 select Departments.deptname).FirstOrDefault(),
                               ConstituencyName = Questions.ConstituencyName,
                               //LastVarifiedVersion = (from tQ in db.tAuditTrial where tQ.paperLaidID == paperLaidVs.PaperLaidId orderby tQ.DateTime descending select tQ.documentReferenceNumber).FirstOrDefault(),
                               //LastVarifiedVersionDate = (from tQ in db.tAuditTrial where tQ.paperLaidID == paperLaidVs.PaperLaidId orderby tQ.DateTime descending select tQ.DateTime).FirstOrDefault()
                           }).ToList();
                return LOB;
            }
            else
            {
                var LOB = (from Questions in db.tQuestions
                           where Questions.IsFixedDate == question.IsFixedDate
                           && Questions.AssemblyID == question.AssemblyID
                           && Questions.SessionID == question.SessionID
                           && Questions.MinistryId != null
                               // && paperLaidVs.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                               // && paperLaidTemp.DeptSubmittedDate != null
                           && Questions.IsFinalApproved == true
                           orderby Questions.QuestionNumber
                           select new QuestionModelCustom
                           {
                               // PaperLaidId = paperLaidVs.PaperLaidId,
                               QuestionID = Questions.QuestionID,
                               QuestionNumber = Questions.QuestionNumber,
                               QuestionTypeId = Questions.QuestionType,
                              
                               MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                               MinisterName = (from mc in db.mMinistries where mc.MinistryID == Questions.MinistryId && mc.AssemblyID == question.AssemblyID select (mc.MinisterName)).FirstOrDefault(),
                               MinisterID = (from mc in db.mMinistries where mc.MinistryID == Questions.MinistryId && mc.AssemblyID == question.AssemblyID select (mc.MemberCode)).FirstOrDefault(),
                               //MemberId = Questions.MinisterID,
                               //ConstituencyID = (from mc in db.mMemberAssembly where mc.MemberID == MinisterID && mc.Active == true && mc.AssemblyID == question.AssemblyID select (mc.ConstituencyCode)).FirstOrDefault(),
                               //ConstituencyName = (from mc in db.mConstituency where mc.ConstituencyCode == ConstituencyID && mc.AssemblyID == question.AssemblyID select (mc.ConstituencyName)).FirstOrDefault(),

                               AssemblyCode = Questions.AssemblyID,
                               SessionCode = Questions.SessionID,
                               SessionDate = Questions.IsFixedDate,
                               SessionDateId = Questions.SessionDateId,
                               EmailSentDate = (from mc in db.tMailStatus
                                                join mbill in db.mMinistries
                                               on mc.MemberID equals mbill.MemberCode
                                                where mbill.MinistryID == Questions.MinistryId
                                                    && mc.AssemblyID == Questions.AssemblyID
                                                    && mc.SessionID == Questions.SessionID
                                                    && mc.SessionDateId == Questions.SessionDateId
                                                select (mc.EmailSentDate)).FirstOrDefault(),
                               // MemberName=Questions.MemberN,
                               //FileName = paperLaidTemp.SignedFilePath,
                               //DocFilePath = paperLaidTemp.FilePath + paperLaidTemp.DocFileName,
                               //IsLaid = paperLaidVs.IsLaid,
                               Subject = Questions.Subject,
                               // Version = paperLaidTemp.Version,
                               // DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                               DepartmentName = (from Departments in db.mDepartment
                                                 where Departments.deptId == Questions.DepartmentID
                                                 select Departments.deptname).FirstOrDefault(),
                               //ConstituencyName = Questions.ConstituencyName,

                               //LastVarifiedVersion = (from tQ in db.tAuditTrial where tQ.paperLaidID == paperLaidVs.PaperLaidId orderby tQ.DateTime descending select tQ.documentReferenceNumber).FirstOrDefault(),
                               //LastVarifiedVersionDate = (from tQ in db.tAuditTrial where tQ.paperLaidID == paperLaidVs.PaperLaidId orderby tQ.DateTime descending select tQ.DateTime).FirstOrDefault()
                           }).ToList();
                return LOB;
            }

        }
        private static object GetQDetailsBySessionDatenMemId(object param)
        {
            QuestionsContext db = new QuestionsContext();
            tQuestion Question = (tQuestion)param;
            var LOB = (from Questions in db.tQuestions
                       where Questions.IsFixedDate == Question.IsFixedDate
                           // && (Questions.DeActivate == false || Questions.DeActivate == null)
                       && Questions.MemberID == Question.MemberID
                       && Questions.AssemblyID == Question.AssemblyID
                       && Questions.SessionID == Questions.SessionID
                       orderby Questions.QuestionType ascending

                       select new QuestionModelCustom
                       {
                           // PaperLaidId = paperLaidVs.PaperLaidId,
                           QuestionID = Questions.QuestionID,
                           QuestionNumber = Questions.QuestionNumber,
                           QuestionTypeId = Questions.QuestionType,
                           MemberId = Questions.MemberID,
                           MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                           AssemblyCode = Questions.AssemblyID,
                           SessionCode = Questions.SessionID,
                           SessionDate = Questions.IsFixedDate,
                           SessionDateId = Questions.SessionDateId,
                           // MemberName=Questions.MemberN,
                           //FileName = paperLaidTemp.SignedFilePath,
                           //DocFilePath = paperLaidTemp.FilePath + paperLaidTemp.DocFileName,
                           //IsLaid = paperLaidVs.IsLaid,
                           Subject = Questions.Subject,
                           MainQuestion = Questions.MainQuestion,
                           // Version = paperLaidTemp.Version,
                           // DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                           DepartmentName = (from Departments in db.mDepartment
                                             where Departments.deptId == Questions.DepartmentID
                                             select Departments.deptname).FirstOrDefault(),
                           MinisterName = (from mini in db.mMinsitryMinister
                                           where mini.MinistryID == Questions.MinistryId
                                           select mini.MinisterName).FirstOrDefault(),
                           ConstituencyName = Questions.ConstituencyName,
                           //LastVarifiedVersion = (from tQ in db.tAuditTrial where tQ.paperLaidID == paperLaidVs.PaperLaidId orderby tQ.DateTime descending select tQ.documentReferenceNumber).FirstOrDefault(),
                           //LastVarifiedVersionDate = (from tQ in db.tAuditTrial where tQ.paperLaidID == paperLaidVs.PaperLaidId orderby tQ.DateTime descending select tQ.DateTime).FirstOrDefault()
                       }).ToList();

            return LOB;
        }


        private static object GeteMailByMemberId(object param)
        {
            if (null == param)
            {
                return null;
            }

            QuestionsContext qCtxDB = new QuestionsContext();
            mMember MemAssem = param as mMember;
            var Email = (from t in qCtxDB.mMember
                         where t.MemberCode == MemAssem.MemberID
                         select t.Email).FirstOrDefault();

            return Email;
        }
        static object Addemailstatus(object param)
        {

            try
            {
                tMailStatus notice = param as tMailStatus;
                using (QuestionsContext db = new QuestionsContext())
                {
                    notice.AssemblyID = notice.AssemblyID;
                    notice.SessionID = notice.SessionID;
                    notice.SessionDate = notice.SessionDate;
                    notice.SessionDateId = notice.SessionDateId;
                    notice.MemberID = notice.MemberID;
                    notice.RecEmailId = notice.RecEmailId;
                    //  notice.QuestionNumber = notice.QuestionNumber;
                    notice.EmailSentDate = DateTime.Now;
                    notice.Type = notice.Type;
                    db.tMailStatus.Add(notice);

                    db.SaveChanges();

                    db.Close();


                }
                return notice;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;




        }
    }
}