﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.Domain.Context.Assembly;
using SBL.Domain.Context.Session;
using SBL.Domain.Context.SiteSetting;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.PrintingPress;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.User;

namespace SBL.Domain.Context.PrintingPress
{
    public class PrintingPressContex : DBBase<PrintingPressContex>
    {
        public PrintingPressContex() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public virtual DbSet<mAssembly> mAssemblies { get; set; }
        public virtual DbSet<mSession> mSessions { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<mPaperCategoryType> mPaperCategoryType { get; set; }
        public DbSet<tPrintingPress> tPrintingPress { get; set; }
        public DbSet<tPrintingTemp> tPrintingTemp { get; set; }
        public DbSet<AdhaarDetails> AdhaarDetails { get; set; }
        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<tStatusPrintingPress> tStatusPrintingPress { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetAssemblySessionList":
                    {
                        return GetAssemblySessionList(param.Parameter);
                    }
                case "GetDepartment":
                    {
                        return GetDepartment(param.Parameter);
                    }
                case "GetDepartmentByDeptId":
                    {
                        return GetDepartmentByDeptId(param.Parameter);
                    }
                case "GetPaperType":
                    {
                        return GetPaperType(param.Parameter);
                    }
                case "GetListtprint":
                    {
                        return GetListtprint(param.Parameter);
                    }
                case "GetListtprintByDeptId":
                    {
                        return GetListtprintByDeptId(param.Parameter);
                    }
                case "UpdatetPrintTemp":
                    {
                        return UpdatetPrintTemp(param.Parameter);
                    }
                case "GetDispatchList":
                    {
                        return GetDispatchList(param.Parameter);

                    }
                case "GetFiles":
                    {
                        return GetFiles(param.Parameter);

                    }
                case "GetCountForPrinting":
                    {
                        return GetCountForPrinting(param.Parameter);

                    }
                case "GetDetails":
                    {
                        return GetDetails(param.Parameter);

                    }
                case "GetAadharDetails":
                    {
                        return GetAadharDetails(param.Parameter);

                    }
                case "UpdatetAcknowledge":
                    {
                        return UpdatetAcknowledge(param.Parameter);

                    }
                case "NewEntry":
                    {
                        return NewEntry(param.Parameter);

                    }
                case "NewInsertEntry":
                    {
                        return NewInsertEntry(param.Parameter);

                    }
                case "GetDepartmentwithselect":
                    {
                        return GetDepartmentwithselect(param.Parameter);

                    }
                case "GetCompleteprintList":
                    {
                        return GetCompleteprintList(param.Parameter);

                    }
                case "GetStatus":
                    {
                        return GetStatus(param.Parameter);

                    }
                case "GetListtDispatchDept":
                    {
                        return GetListtDispatchDept(param.Parameter);

                    }
                case "GetCountFordept":
                    {
                        return GetCountFordept(param.Parameter);

                    }
                case "GetRecieptCountFordept":
                    {
                        return GetRecieptCountFordept(param.Parameter);

                    }
                case "GetDispatchCountFordept":
                    {
                        return GetDispatchCountFordept(param.Parameter);

                    }
                case "GetdatabyRefNo":
                    {
                        return GetdatabyRefNo(param.Parameter);

                    }

            }

            return null;
        }

        public static object GetAssemblySessionList(object param)
        {
            PrintingPressModel model = param as PrintingPressModel;
            PrintingPressContex pCtxt = new PrintingPressContex();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();

            if (model.AssemblyID == 0 && model.SessionID == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionID = Convert.ToInt16(SessionCode);
                model.AssemblyID = Convert.ToInt16(AssemblyCode);
            }

            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionID && m.AssemblyID == model.AssemblyID select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyID select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;

            model.mAssemblyList = (from A in pCtxt.mAssemblies
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSessions
                                 where S.AssemblyID == model.AssemblyID
                                 select S).ToList();

            return model;
        }

        //static List<mDepartment> GetDepartment(object param)
        public static object GetDepartment(object param)
        {

            mDepartment model = param as mDepartment;
            PrintingPressModel newmodel = new PrintingPressModel();
            using (var cnxt = new PrintingPressContex())
            {
                //var data = (from mdl in cnxt.mDepartment select mdl).OrderBy(i => i.deptname.ToLower()).ToList();

                var data = (from mdl in cnxt.mDepartment where mdl.deptname.Contains(">") select mdl).OrderBy(z => z.deptname).ToList();
                var data2 = (from mdl in cnxt.mDepartment where !mdl.deptname.Contains(">") select mdl).OrderBy(z => z.deptname).ToList();
                newmodel.mDepartment = data2.ToList<mDepartment>();
                foreach (var session in data)
                {
                    newmodel.mDepartment.Add(new mDepartment()
                    {
                        deptname = session.deptname.Replace(">",""),
                        deptId = session.deptId
                    });
                }

                //newmodel.mDepartment.Add(new mDepartment()
                //{
                //    deptname = "ChiefSecretary",
                //    deptId = "HPCSEC1"
                //});
                //foreach (var session in data2)
                //{
                //    newmodel.mDepartment.Add(new mDepartment()
                //    {
                //        deptname = session.deptname,
                //        deptId = session.deptId
                //    });
                //}

                var retdata = newmodel.mDepartment.OrderBy(z => z.deptname).ToList();

                return retdata;
            }

        }

        public static object GetDepartmentByDeptId(object param)
        {

            string DeptIds = param as string;
            string[] DeptArray = DeptIds.Split(',');
            PrintingPressModel newmodel = new PrintingPressModel();
            using (var cnxt = new PrintingPressContex())
            {
                //var data = (from mdl in cnxt.mDepartment select mdl).OrderBy(i => i.deptname.ToLower()).ToList();

                var data = (from mdl in cnxt.mDepartment where mdl.deptname.Contains(">") & (DeptArray).Contains(mdl.deptId) select mdl).OrderBy(z => z.deptname).ToList();
                var data2 = (from mdl in cnxt.mDepartment where !mdl.deptname.Contains(">") & (DeptArray).Contains(mdl.deptId) select mdl).OrderBy(z => z.deptname).ToList();
                newmodel.mDepartment = data2.ToList<mDepartment>();
                foreach (var session in data)
                {
                    newmodel.mDepartment.Add(new mDepartment()
                    {
                        deptname = session.deptname,
                        deptId = session.deptId
                    });
                }
                //foreach (var session in data2)
                //{
                //    newmodel.mDepartment.Add(new mDepartment()
                //    {
                //        deptname = session.deptname,
                //        deptId = session.deptId
                //    });
                //}

                var retdata = newmodel.mDepartment;

                return retdata;
            }

        }

        public static object GetDepartmentwithselect(object param)
        {

            mDepartment model = param as mDepartment;
            PrintingPressModel newmodel = new PrintingPressModel();
            using (var cnxt = new PrintingPressContex())
            {
                //var data = (from mdl in cnxt.mDepartment select mdl).OrderBy(i => i.deptname.ToLower()).ToList();

                var data = (from mdl in cnxt.mDepartment where mdl.deptname.Contains(">") select mdl).OrderBy(z => z.deptname).ToList();
                var data2 = (from mdl in cnxt.mDepartment where !mdl.deptname.Contains(">") select mdl).OrderBy(z => z.deptname).ToList();
                var newe = (from mdl in cnxt.mDepartment where mdl.deptname == "same" select mdl).ToList();
                newmodel.mDepartment = newe.ToList<mDepartment>();

                newmodel.mDepartment.Add(new mDepartment()
                {
                    deptname = "Select Department",
                    deptId = "0"
                });

                foreach (var session in data2)
                {
                    newmodel.mDepartment.Add(new mDepartment()
                    {
                        deptname = session.deptname,
                        deptId = session.deptId
                    });
                }
                foreach (var session in data)
                {
                    newmodel.mDepartment.Add(new mDepartment()
                    {
                        deptname = session.deptname,
                        deptId = session.deptId
                    });
                }

                var retdata = newmodel.mDepartment;
                // newmodel.mDepartment.Add(list);
                //newmodel.mDepartment = data2.ToList<mDepartment>();


                //foreach (var session in data)
                //{
                //    newmodel.mDepartment.Add(new mDepartment()
                //    {
                //        deptname = session.deptname,
                //        deptId = session.deptId
                //    });
                //}
                return retdata;
            }

        }

        static List<mPaperCategoryType> GetPaperType(object param)
        {

            mPaperCategoryType model = param as mPaperCategoryType;

            using (var cnxt = new PrintingPressContex())
            {
                var data = (from mdl in cnxt.mPaperCategoryType select mdl).OrderBy(z => z.Name).ToList();
                return data;
            }

        }

        static List<tStatusPrintingPress> GetStatus(object param)
        {

            tStatusPrintingPress model = param as tStatusPrintingPress;

            using (var cnxt = new PrintingPressContex())
            {
                var data = (from mdl in cnxt.tStatusPrintingPress select mdl).OrderBy(z => z.StatusId).ToList();
                return data;
            }

        }


        public static object GetListtprint(object param)
        {

            tPrintingPress model = param as tPrintingPress;
            PrintingPressContex pCtxt = new PrintingPressContex();
            var query = (from Print in pCtxt.tPrintingPress
                         where (Print.CreatedprintPress == true)
                         // orderby questions.IsAssignDate descending
                         select new PrintingPressModel
                         {
                             PrintSeqenceId = Print.PrintSeqenceId,
                             Title = Print.Title,
                             DeparmentName = Print.DeparmentName,
                             DepartmentId = Print.DepartmentId,
                             //  Description=Print.Description,
                             AssemblyId = Print.AssemblyId,
                             SessionId = Print.SessionId,
                             PaperTypeName = (from mc in pCtxt.mPaperCategoryType where mc.PaperCategoryTypeId == Print.PaperTypeID select mc.Name).FirstOrDefault(),
                             RecievedDate = Print.RecievedDate,
                             AdhaarID = Print.AdhaarID,
                             Acceptance = Print.Acceptance,
                             Acknowledge = Print.Acknowledge,
                             SubmitDate = Print.SubmitDate,
                             Resend = Print.Resend,
                             RefNo = Print.RefNo,
                             StatusName = (from mc in pCtxt.tStatusPrintingPress where mc.StatusId == Print.Status select mc.StatusName).FirstOrDefault(),
                             StatusId = Print.Status
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.PrintingPressModel = results;

            return model;
        }


        public static object GetListtDispatchDept(object param)
        {

            tPrintingPress model = param as tPrintingPress;
            PrintingPressContex pCtxt = new PrintingPressContex();
            var query = (from Print in pCtxt.tPrintingPress
                         where (Print.CreatedprintPress == false)
                         // orderby questions.IsAssignDate descending
                         select new PrintingPressModel
                         {
                             PrintSeqenceId = Print.PrintSeqenceId,
                             Title = Print.Title,
                             DeparmentName = Print.DeparmentName,
                             DepartmentId = Print.DepartmentId,
                             //  Description=Print.Description,
                             AssemblyId = Print.AssemblyId,
                             SessionId = Print.SessionId,
                             PaperTypeName = (from mc in pCtxt.mPaperCategoryType where mc.PaperCategoryTypeId == Print.PaperTypeID select mc.Name).FirstOrDefault(),
                             RecievedDate = Print.RecievedDate,
                             AdhaarID = Print.AdhaarID,
                             Acceptance = Print.Acceptance,
                             Acknowledge = Print.Acknowledge,
                             SubmitDate = Print.SubmitDate,
                             Resend = Print.Resend,
                             RefNo = Print.RefNo,
                             StatusName = (from mc in pCtxt.tStatusPrintingPress where mc.StatusId == Print.Status select mc.StatusName).FirstOrDefault(),
                             StatusId = Print.Status
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.PrintingPressModel = results;

            return model;
        }

        public static object GetCompleteprintList(object param)
        {

            tPrintingPress model = param as tPrintingPress;
            PrintingPressContex pCtxt = new PrintingPressContex();
            var query = (from Print in pCtxt.tPrintingPress
                         where (Print.CreatedprintPress == true)
                         // orderby questions.IsAssignDate descending
                         select new PrintingPressModel
                         {
                             PrintSeqenceId = Print.PrintSeqenceId,
                             Title = Print.Title,
                             DeparmentName = Print.DeparmentName,
                             DepartmentId = Print.DepartmentId,
                             //  Description=Print.Description,
                             AssemblyId = Print.AssemblyId,
                             SessionId = Print.SessionId,
                             PaperTypeName = (from mc in pCtxt.mPaperCategoryType where mc.PaperCategoryTypeId == Print.PaperTypeID select mc.Name).FirstOrDefault(),
                             RecievedDate = Print.RecievedDate,
                             AdhaarID = Print.AdhaarID,
                             Acceptance = Print.Acceptance,
                             Acknowledge = Print.Acknowledge,
                             SubmitDate = Print.SubmitDate,
                             Resend = Print.Resend,
                             RefNo = Print.RefNo,
                             StatusId = Print.Status,
                             StatusName = (from mc in pCtxt.tStatusPrintingPress where mc.StatusId == Print.Status select mc.StatusName).FirstOrDefault(),
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.PrintingPressModel = results;

            return model;
        }

        public static object GetListtprintByDeptId(object param)
        {

            tPrintingPress model = param as tPrintingPress;
            PrintingPressContex pCtxt = new PrintingPressContex();

            int? PaperTypeID = null;

            if (model.PaperTypeID != 0)
            {
                PaperTypeID = model.PaperTypeID;
            }

            if (model.DepartmentId == Convert.ToString(0))
            {
                var query = (from Print in pCtxt.tPrintingPress
                             where (Print.CreatedprintPress == false)
                             // orderby questions.IsAssignDate descending
                             select new PrintingPressModel
                             {
                                 PrintSeqenceId = Print.PrintSeqenceId,
                                 Title = Print.Title,
                                 DeparmentName = Print.DeparmentName,
                                 DepartmentId = Print.DepartmentId,
                                 //Description = Print.Description,
                                 AssemblyId = Print.AssemblyId,
                                 SessionId = Print.SessionId,
                                 PaperTypeName = (from mc in pCtxt.mPaperCategoryType where mc.PaperCategoryTypeId == Print.PaperTypeID select mc.Name).FirstOrDefault(),
                                 RecievedDate = Print.RecievedDate,
                                 AdhaarID = Print.AdhaarID,
                                 Acceptance = Print.Acceptance,
                                 Acknowledge = Print.Acknowledge,
                                 SubmitDate = Print.SubmitDate,
                                 RefNo = Print.RefNo,
                                 StatusId = Print.Status,
                                 StatusName = (from mc in pCtxt.tStatusPrintingPress where mc.StatusId == Print.Status select mc.StatusName).FirstOrDefault(),
                             }).ToList();

                int totalRecords = query.Count();
                var results = query.ToList();
                model.PrintingPressModel = results;

                return model;
            }
            else
            {

                var query = (from Print in pCtxt.tPrintingPress
                             where (Print.CreatedprintPress == false && (Print.DepartmentId == null || Print.DepartmentId == string.Empty || Print.DepartmentId == model.DepartmentId) && (!PaperTypeID.HasValue || Print.PaperTypeID == PaperTypeID))
                             select new PrintingPressModel
                             {
                                 PrintSeqenceId = Print.PrintSeqenceId,
                                 Title = Print.Title,
                                 DeparmentName = Print.DeparmentName,
                                 DepartmentId = Print.DepartmentId,
                                 // Description = Print.Description,
                                 PaperTypeName = (from mc in pCtxt.mPaperCategoryType where mc.PaperCategoryTypeId == Print.PaperTypeID select mc.Name).FirstOrDefault(),
                                 RecievedDate = Print.RecievedDate,
                                 AdhaarID = Print.AdhaarID,
                                 Acceptance = Print.Acceptance,
                                 Acknowledge = Print.Acknowledge,
                                 SubmitDate = Print.SubmitDate,
                                 RefNo = Print.RefNo,
                                 StatusId = Print.Status,
                                 StatusName = (from mc in pCtxt.tStatusPrintingPress where mc.StatusId == Print.Status select mc.StatusName).FirstOrDefault(),
                             }).ToList();

                int totalRecords = query.Count();
                var results = query.ToList();
                model.PrintingPressModel = results;
            }
            return model;
        }

        static object UpdatetPrintTemp(object param)
        {

            tPrintingTemp model = param as tPrintingTemp;
            tPrintingPress modelupdate = new tPrintingPress();
            PrintingPressContex pCtxt = new PrintingPressContex();
            tPrintingTemp obj = pCtxt.tPrintingTemp.Single(m => m.PrintSeqenceId == model.PrintSeqenceId);

            obj.FileName = model.FileName;
            obj.FilePath = model.FilePath;
            obj.DocFileName = model.DocFileName;
            obj.DocFilePath = model.DocFilePath;
            obj.SubmittedDate = model.SubmittedDate;
            obj.SubmittedBy = model.SubmittedBy;
            pCtxt.SaveChanges();
            string msg = "";

            try
            {
                tPrintingPress objPrint = pCtxt.tPrintingPress.Single(m => m.PrintSeqenceId == model.PrintSeqenceId);
                objPrint.Dispatch = true;
                objPrint.DepartmentId = model.DepartmentId;
                objPrint.PaperTypeID = model.PaperCategoryTypeId;
                objPrint.Title = model.Title;
                objPrint.SubmitDate = model.SubmittedDate;
                pCtxt.SaveChanges();
                pCtxt.Close();
            }


            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }

        public static object GetDispatchList(object param)
        {

            tPrintingPress model = param as tPrintingPress;
            PrintingPressContex pCtxt = new PrintingPressContex();
            var query = (from Print in pCtxt.tPrintingPress
                         where (Print.CreatedprintPress == false)
                         // orderby questions.IsAssignDate descendinge
                         select new PrintingPressModel
                         {
                             PrintSeqenceId = Print.PrintSeqenceId,
                             Title = Print.Title,
                             DeparmentName = Print.DeparmentName,
                             DepartmentId = Print.DepartmentId,
                             //Description = Print.Description,
                             AssemblyId = Print.AssemblyId,
                             SessionId = Print.SessionId,
                             PaperTypeName = (from mc in pCtxt.mPaperCategoryType where mc.PaperCategoryTypeId == Print.PaperTypeID select mc.Name).FirstOrDefault(),
                             RecievedDate = Print.RecievedDate,
                             AdhaarID = Print.AdhaarID,
                             Acceptance = Print.Acceptance,
                             Acknowledge = Print.Acknowledge,
                             SubmitDate = Print.SubmitDate,
                             Resend = Print.Resend,
                             RefNo = Print.RefNo,
                             StatusId = Print.Status,
                             StatusName = (from mc in pCtxt.tStatusPrintingPress where mc.StatusId == Print.Status select mc.StatusName).FirstOrDefault(),
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.PrintingPressModel = results;

            return model;
        }

        public static object GetFiles(object param)
        {

            tPrintingTemp model = param as tPrintingTemp;
            PrintingPressContex pCtxt = new PrintingPressContex();
            var query = (from Print in pCtxt.tPrintingPress
                         join Rtemp in pCtxt.tPrintingTemp on Print.PrintSeqenceId equals Rtemp.PrintSeqenceId
                         where (Print.PrintSeqenceId == model.PrintSeqenceId)
                         select new PrintingPressModel
                         {
                             PrintSeqenceId = Print.PrintSeqenceId,
                             FileName = Rtemp.FileName,
                             DocFileName = Rtemp.DocFileName,
                             Title = Print.Title,
                             AssemblyId = Print.AssemblyId,
                             SessionId = Print.SessionId,
                             DepartmentId = Print.DepartmentId,
                             DeparmentName = Print.DeparmentName,
                             PaperTypeID = Print.PaperTypeID,
                             Acknowledge = Print.Acknowledge,
                             RecievedDate = Print.RecievedDate,
                             SubmitDate = Print.SubmitDate,
                             RefNo = Print.RefNo,
                             StatusId = Print.Status,
                             StatusName = (from mc in pCtxt.tStatusPrintingPress where mc.StatusId == Print.Status select mc.StatusName).FirstOrDefault(),
                             Remark = Print.Remarks,
                             AdhaarID = Print.AdhaarID
                         }).ToList();
            int totalRecords = query.Count();
            var results = query.ToList();
            model.PrintingPressModel = results;

            return model;
        }

        static object GetCountForPrinting(object param)
        {
            PrintingPressModel model = param as PrintingPressModel;
            PrintingPressContex pCtxt = new PrintingPressContex();
            var ReciptCnt = (from C in pCtxt.tPrintingPress
                             where (C.CreatedprintPress == false)

                             select new PrintingPressModel
                             {

                             }).ToList().Count();
            model.TotalReciptCount = ReciptCnt;
            var Dispatchcount = (from C in pCtxt.tPrintingPress
                                 where (C.CreatedprintPress == true)
                                 select new PrintingPressModel
                                 {

                                 }).ToList().Count();

            model.TotalDispatchCount = Dispatchcount;
            return model;
        }

        static object GetRecieptCountFordept(object param)
        {
            try
            {
                tPaperLaidV mod = param as tPaperLaidV;
                PrintingPressContex db = new PrintingPressContex();

                int count = (from m in db.tPrintingPress

                             where m.CreatedprintPress == true

                             select new PrintingPressModel
                             {
                             }).Count();
                mod.ReceiptCount = count;
                return mod;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        static object GetDispatchCountFordept(object param)
        {
            try
            {
                tPaperLaidV mod = param as tPaperLaidV;
                PrintingPressContex db = new PrintingPressContex();

                int count = (from m in db.tPrintingPress

                             where m.CreatedprintPress == false

                             select new PrintingPressModel
                             {
                             }).Count();
                mod.DispatchCount = count;
                return mod;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        static object GetCountFordept(object param)
        {
            PrintingPressModel model = param as PrintingPressModel;
            PrintingPressContex pCtxt = new PrintingPressContex();
            var ReciptCnt = (from C in pCtxt.tPrintingPress
                             where (C.CreatedprintPress == true)

                             select new PrintingPressModel
                             {

                             }).ToList().Count();
            model.TotalReciptCount = ReciptCnt;
            var Dispatchcount = (from C in pCtxt.tPrintingPress
                                 where (C.CreatedprintPress == false)
                                 select new PrintingPressModel
                                 {

                                 }).ToList().Count();

            model.TotalDispatchCount = Dispatchcount;
            return model;
        }
        public static object GetDetails(object param)
        {

            tPrintingTemp model = param as tPrintingTemp;
            PrintingPressContex pCtxt = new PrintingPressContex();
            var query = (from Print in pCtxt.tPrintingPress
                         join Rtemp in pCtxt.tPrintingTemp on Print.PrintSeqenceId equals Rtemp.PrintSeqenceId
                         where (Print.PrintSeqenceId == model.PrintSeqenceId)
                         // orderby questions.IsAssignDate descending
                         select new PrintingPressModel
                         {
                             PrintSeqenceId = Print.PrintSeqenceId,
                             FileName = Rtemp.FileName,
                             DocFileName = Rtemp.DocFileName,
                             Title = Print.Title,
                             AssemblyId = Print.AssemblyId,
                             SessionId = Print.SessionId,
                             DepartmentId = Print.DepartmentId,
                             DeparmentName = Print.DeparmentName,
                             PaperTypeID = Print.PaperTypeID,
                             Acknowledge = Print.Acknowledge,
                             RecievedDate = Print.RecievedDate,
                             SubmitDate = Print.SubmitDate,
                             RefNo = Print.RefNo,
                             StatusName = (from mc in pCtxt.tStatusPrintingPress where mc.StatusId == Print.Status select mc.StatusName).FirstOrDefault(),
                             StatusId = Print.Status,
                             Remark = Print.Remarks,
                             Dispatch = Print.Dispatch
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.PrintingPressModel = results;

            return model;
        }

        static object GetAadharDetails(object param)
        {
            PrintingPressModel model = param as PrintingPressModel;
            PrintingPressContex Ustxt = new PrintingPressContex();
            var query = (from User in Ustxt.AdhaarDetails
                         where (User.AdhaarID == model.AdhaarID)
                         //select User).ToList();
                         select new PrintingPressModel
                         {

                             AdhaarID = User.AdhaarID,
                             AdhaarName = User.Name,
                             FatherName = User.FatherName,
                             Gender = User.Gender,
                             Address = User.Address,
                             DOB = User.DOB,
                             Email = User.Email,
                             District = User.District,
                             PinCode = User.PinCode,
                             Photo = User.Photo,
                             MobileNo = User.MobileNo,

                         }).ToList();


            var results = query.ToList();
            model.objList = results;

            return model;
        }

        static object UpdatetAcknowledge(object param)
        {
            string msg = "";
            tPrintingPress model = param as tPrintingPress;
            PrintingPressContex pCtxt = new PrintingPressContex();

            tPrintingPress objPrint = pCtxt.tPrintingPress.Single(m => m.PrintSeqenceId == model.PrintSeqenceId);
            objPrint.Acknowledge = model.Acknowledge;
            pCtxt.SaveChanges();
            pCtxt.Close();

            return msg;
        }

        static object NewInsertEntry(object param)
        {

            tPrintingTemp model = param as tPrintingTemp;
            PrintingPressContex pCtxt = new PrintingPressContex();
            string msg = "";

            try
            {
                tPrintingPress objPrint = new tPrintingPress();
                objPrint.Title = model.Title;
                objPrint.AssemblyId = model.AssemblyId;
                objPrint.SessionId = model.SessionId;
                objPrint.DepartmentId = model.DepartmentId;
                objPrint.DeparmentName = model.DeparmentName;
                objPrint.PaperTypeID = model.PaperCategoryTypeId;
                objPrint.Dispatch = true;
                objPrint.RefNo = model.RefNo;
                objPrint.Remarks = model.Remark;
                objPrint.Status = model.StatusID;
                objPrint.SubmitDate = model.SubmittedDate;
                if (model.SubmittedBy != "")
                {
                    objPrint.AdhaarID = model.SubmittedBy;
                    objPrint.CreatedprintPress = false;
                }
                else
                {
                    objPrint.CreatedprintPress = true;
                    objPrint.AdhaarID = "Printing Press";
                }

                pCtxt.tPrintingPress.Add(objPrint);
                pCtxt.SaveChanges();
                var PNewid = objPrint.PrintSeqenceId;
                model.PrintSeqenceId = objPrint.PrintSeqenceId;
                tPrintingTemp obj = new tPrintingTemp();
                obj.PrintSeqenceId = PNewid;
                pCtxt.tPrintingTemp.Add(obj);
                pCtxt.SaveChanges();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return model;
        }


        static object NewEntry(object param)
        {

            tPrintingTemp model = param as tPrintingTemp;
            PrintingPressContex pCtxt = new PrintingPressContex();
            string msg = "";

            try
            {
                //tPrintingPress objPrint = pCtxt.tPrintingPress.Single(m => m.PrintSeqenceId == model.PrintSeqenceId);
                //objPrint.Dispatch = true;
                //objPrint.Title = model.Title;
                //objPrint.AssemblyId = model.AssemblyId;
                //objPrint.SessionId = model.SessionId;
                //objPrint.DepartmentId = model.DepartmentId;
                //objPrint.DeparmentName = model.DeparmentName;
                //objPrint.PaperTypeID = model.PaperCategoryTypeId;
                //objPrint.SubmitDate = model.SubmittedDate;
                //objPrint.CreatedprintPress = true;
                //pCtxt.SaveChanges();
                tPrintingTemp obj = pCtxt.tPrintingTemp.Single(m => m.PrintSeqenceId == model.PrintSeqenceId);
                obj.FileName = model.FileName;
                obj.FilePath = model.FilePath;
                obj.DocFileName = model.DocFileName;
                obj.DocFilePath = model.DocFilePath;
                obj.SubmittedDate = model.SubmittedDate;
                obj.SubmittedBy = model.SubmittedBy;
                pCtxt.SaveChanges();
            }

            catch (Exception ex)
            {
                msg = ex.Message;
            }
            return msg;
        }


        static object GetdatabyRefNo(object param)
        {
            tPrintingPress model = param as tPrintingPress;
            PrintingPressContex pCtxt = new PrintingPressContex();
            //var query = (from C in pCtxt.tPrintingPress
            //             where (C.RefNo == model.RefNo )
            //             orderby C.SubmitDate descending
            //                 select new 
            //                 {
            //                     DepartmentId = C.DepartmentId,
            //                     PaperTypeID=C.PaperTypeID,
            //                     Title=C.Title,
            //                     Remark=C.Remarks,

            //                 }).First();
            var check = (from C in pCtxt.tPrintingPress
                         where C.RefNo == model.RefNo
                         select C).Count();
            if (check != 0)
            {
                var query = (from C in pCtxt.tPrintingPress
                             where C.RefNo == model.RefNo
                             orderby C.SubmitDate descending
                             select C).First();
                var results = query;
                return query;
            }

            // model.PrintingPressModel = results;
            return null;
        }


    }


}