﻿using SBL.Service.Common;
using SBL.DAL;
using System.Data.Entity;
using System.Linq;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.PaperMovement;

namespace SBL.Domain.Context.PaperMovement
{
    public class PaperMovementDetailsContaxt : DBBase<PaperMovementDetailsContaxt>
    {
        public PaperMovementDetailsContaxt() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mPaperCategoryType> mPaperCategoryType { get; set; }
        public DbSet<AdhaarDetails> mAdhaarDetails { get; set; }      
        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<mEmployee> mEmployee { get; set; }
        public DbSet<PaperMovementDetail> mPaperMovementDetail { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "GetMovementDetailsByIds":
                    {
                        return GetMovementDetailsByIds(param.Parameter);
                    }
            }
            return null;
        }

        static object GetMovementDetailsByIds(object param)
        {
            MovementDetailsCustom paperLaidMovemnt = param as MovementDetailsCustom;
            PaperMovementDetailsContaxt plmCntxt = new PaperMovementDetailsContaxt();            
            var MovementDetails = (from mPapMovDetails in plmCntxt.mPaperMovementDetail                        
                         where mPapMovDetails.DepartmentId == paperLaidMovemnt.DepartmentId
                               && mPapMovDetails.PaperCategoryTypeId == paperLaidMovemnt.PaperCategoryTypeId                               
                               && mPapMovDetails.AssemblyId == paperLaidMovemnt.AssemblyId
                               && mPapMovDetails.SessionId == paperLaidMovemnt.SessionId
                               && mPapMovDetails.Number == paperLaidMovemnt.Number
                               select new MovementDetailsCustom
                               {
                                 AadarId=mPapMovDetails.AadarId,
                                 MovementUser=mPapMovDetails.MovementUser,
                                 MovementDate=mPapMovDetails.MovementDate,
                                 EmpCode=mPapMovDetails.EmpCode,
                                 DepartmentId = mPapMovDetails.DepartmentId

                               }).ToList();

            return MovementDetails;

        }




    }
}
