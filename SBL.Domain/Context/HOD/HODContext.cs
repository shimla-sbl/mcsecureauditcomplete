﻿using System;
using System.Data;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.HOD;

namespace SBL.Domain.Context.HOD
{
    public class HODContext : DBBase<HODContext>
    {
        public HODContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mHOD> mHOD { get; set; }

        public DbSet<mDepartment> mDepartment { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateHOD":
                    {
                        return CreateHOD(param.Parameter);
                    }
                case "UpdateHOD":
                    {
                        return UpdateHOD(param.Parameter);
                    }
                case "UpdateHODByAdharId":
                    {
                        return UpdateHODByAdharId(param.Parameter);
                    }
                    
                case "DeleteHOD":
                    {
                        return DeleteHOD(param.Parameter);
                    }
                case "GetAllDepartment":
                    {
                        return GetAllDepartment(param.Parameter);
                    }
                case "GetAllHODDetails":
                    {
                        return GetAllHODDetails(param.Parameter);
                    }
                case "GetHODById":
                    {
                        return GetHODById(param.Parameter);
                    }
                case "CheckHodExist":
                    {
                        return CheckHodExist(param.Parameter);
                    }
                case "GetHodByAdharid":
                    {
                        return GetHodByAdharid(param.Parameter);
                    }
                case "GetAllHODList":
                    {
                        return GetAllHODList(param.Parameter);
                    }
                    
                    
            } 
            
            return null;
        }

        public static object CreateHOD(object param)
        {
            try
            {
                using (HODContext db = new HODContext())
                {
                    mHOD model = param as mHOD;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    //model.DeptID = "HPD0005";
                    db.mHOD.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateHOD(object param)
        {
            try
            {
                using (HODContext db = new HODContext())
                {
                    mHOD model = param as mHOD;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mHOD.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }
        public static object UpdateHODByAdharId(object param)
        {
            try
            {
                using (HODContext db = new HODContext())
                {
                    mHOD model = param as mHOD;
                    mHOD modelTo = new mHOD();
                    modelTo = (from hod in db.mHOD where hod.AadhaarId == model.AadhaarId select hod).SingleOrDefault();
                    modelTo.HODName = model.HODName;
                    modelTo.DesignationDescription = model.DesignationDescription;
                    modelTo.Email = model.Email;
                    modelTo.DeptID = model.DeptID;
                    modelTo.Mobile = model.Mobile;
                    model.ModifiedWhen = DateTime.Now;
                    db.mHOD.Attach(modelTo);
                    db.Entry(modelTo).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }
        public static object DeleteHOD(object param)
        {
             try
             {
                 using (HODContext db = new HODContext())
                 {
                     mHOD parameter = param as mHOD;
                     mHOD stateToRemove = db.mHOD.SingleOrDefault(a => a.HODID == parameter.HODID);
                     if(stateToRemove!=null)
                     {
                         stateToRemove.IsDeleted = true;
                     }
                     //db.mHOD.Remove(stateToRemove);
                     db.SaveChanges();
                     db.Close();
                 }

                 return null;
             }
             catch
             {
                 throw;
             }
        }
        public static object GetAllHODList(object param)
        {
            HODContext db = new HODContext();

            var data = (from hod in db.mHOD select hod).OrderBy(a => a.HODName);
            return data.ToList();
        }
        public static object GetAllHODDetails(object param)
        {
            HODContext db = new HODContext();

            var data = (from hod in db.mHOD
                        join m in db.mDepartment on hod.DeptID equals m.deptId into joindeptname
                        from n in joindeptname.DefaultIfEmpty()
                        where hod.IsDeleted==null
                        select new
                        {
                            hod,
                            n.deptname,
                        }).ToList();

            List<mHOD> list = new List<mHOD>();
            foreach (var item in data)
            {
                item.hod.GetDepartmentName = item.deptname;
                list.Add(item.hod);

            }
            return list.OrderByDescending(c => c.HODID).ToList();
        }

        public static object GetAllDepartment(object param)
        {
            HODContext db = new HODContext();

             var data = (from m in db.mDepartment
                        orderby m.deptname ascending
                         where m.IsDeleted == null
                        select m).ToList();
            return data;

        }

        public static mHOD GetHODById(object param)
        {
            mHOD parameter = param as mHOD;

            HODContext db = new HODContext();

            var query = db.mHOD.SingleOrDefault(a => a.HODID == parameter.HODID);
            
            return query;
        }
        private static object CheckHodExist(object p)
        {
            //SecretoryContext SecCxt = new SecretoryContext();
            mHOD model = p as mHOD;
            using (var ctx = new HODContext())
            {
                var isexist = (from usrdschash in ctx.mHOD where usrdschash.AadhaarId == model.AadhaarId select usrdschash).Count() > 0;

                return isexist;
            }
        }
        public static object GetHodByAdharid(object param)
        {
            HODContext db = new HODContext();
            mHOD model = param as mHOD;
            var query = (from mdl in db.mHOD where mdl.AadhaarId == model.AadhaarId select mdl).SingleOrDefault();
            return query;
        }
      
    }
}
