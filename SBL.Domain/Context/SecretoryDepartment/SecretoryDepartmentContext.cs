﻿namespace SBL.Domain.Context.SecretoryDepartment
{
    using System.Linq;
    using SBL.DAL;
    using SBL.DomainModel.ComplexModel;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Secretory;
    using System.Data.Entity;
    using System.Collections.Generic;
    using SBL.DomainModel.Models.Assembly;
    using System;
    using System.Data;

    public class SecretoryDepartmentContext : DBBase<SecretoryDepartmentContext>
    {
        public SecretoryDepartmentContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mSecretoryDepartment> mSecretoryDepartment { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<mSecretory> mSecretory { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetSecretoryDepartmentBySecID":
                    {
                        return GetSecretoryDepartmentBySecID(param.Parameter);
                    }
                case "GetSecretoryDepartmentByEmpCodeAndSecDeptId":
                    {
                        return GetSecretoryDepartmentByEmpCodeAndSecDeptId(param.Parameter);
                    }
                case "GetmSecretoryDepartmentBySecID":
                    {
                        return GetmSecretoryDepartmentBySecID(param.Parameter);
                    }
                #region ForSecretoryDepartment
                case "GetAllSecretoryDepartment": { return GetAllSecretoryDepartment(); }
                case "CreateSecretoryDepartment": { CreateSecretoryDepartment(param.Parameter); break; }
                case "UpdateSecretoryDepartment": { return UpdateSecretoryDepartment(param.Parameter); }
                case "DeleteSecretoryDepartment": { return DeleteSecretoryDepartment(param.Parameter); }
                case "GetSecDeptBasedOnId": { return GetSecDeptBasedOnId(param.Parameter); }
                case "GetAllSecretories": { return GetAllSecretories(); }
                case "GetAllAssemblies": { return GetAllAssemblies(); }
                case "GetAllDepartments": { return GetAllDepartments(); }
                #endregion



            }
            return null;
        }


        public static object GetSecretoryDepartmentBySecID(object param)
        {
            SecretoryDepartmentContext SecCxt = new SecretoryDepartmentContext();
            mSecretoryDepartment model = param as mSecretoryDepartment;
            var query = (from dept in SecCxt.mDepartment
                         join mdl in SecCxt.mSecretoryDepartment on dept.deptId equals mdl.DepartmentID
                         where mdl.SecretoryID == model.SecretoryID
                         select new SecretoryDepartmentModel
                         {
                             DepartmentID = dept.deptId,
                             DepartmentName = dept.deptname
                         }).OrderBy(m => m.DepartmentName).ToList();
            return query;
        }

        public static object GetSecretoryDepartmentByEmpCodeAndSecDeptId(object param)
        {
            SecretoryDepartmentContext SecCxt = new SecretoryDepartmentContext();
            mSecretory model = param as mSecretory;
            string DeptIds = "";

            var secretary = (from sect in SecCxt.mSecretory where sect.DeptID == model.DeptID && sect.EmpCode == model.EmpCode select sect.SecretoryID).FirstOrDefault();

            if (secretary != null)
            {
                var query = (from dept in SecCxt.mDepartment
                             join mdl in SecCxt.mSecretoryDepartment on dept.deptId equals mdl.DepartmentID
                             where mdl.SecretoryID == secretary
                             select new SecretoryDepartmentModel
                             {
                                 DepartmentID = dept.deptId,
                                 DepartmentName = dept.deptname
                             }).OrderBy(m => m.DepartmentName).ToList();

                if (query != null)
                {
                    foreach (var item in query)
                    {
                        DeptIds += item.DepartmentID + ",";
                    }
                }
            }

            if (DeptIds != "")
            {
                return DeptIds.Substring(0, (DeptIds.Length - 1));
            }
            return DeptIds;
        }


        static List<mSecretoryDepartment> GetAllSecretoryDepartment()
        {
            try
            {
                using (DBManager db = new DBManager())
                {
                    var Data = (from SecDept in db.mSecretoryDepartment
                                join SecAssem in db.mAssemblies on SecDept.AssemblyID equals SecAssem.AssemblyCode into JoinAssemblyName
                                from assem in JoinAssemblyName.DefaultIfEmpty()
                                join dept in db.mDepartments on SecDept.DepartmentID equals dept.deptId into joindeptName
                                from deptment in joindeptName.DefaultIfEmpty()
                                join Secretary in db.mSecretory on SecDept.SecretoryID equals Secretary.SecretoryID into joinsecName
                                from SecName in joinsecName.DefaultIfEmpty()
                                where SecDept.IsDeleted==null
                                select new
                                {
                                    SecDept,
                                    assem.AssemblyName,
                                    deptment.deptname,
                                    SecName.SecretoryName
                                }).ToList();

                    List<mSecretoryDepartment> list = new List<mSecretoryDepartment>();
                    foreach (var item in Data)
                    {
                        item.SecDept.GetAssemblyName = item.AssemblyName;
                        item.SecDept.GetDeptName = item.deptname;
                        item.SecDept.GetSecretoryName = item.SecretoryName;
                        list.Add(item.SecDept);
                    }

                    return list.OrderByDescending(s=>s.SecretoryDepartmentID).ToList();



                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static List<mSecretory> GetAllSecretories()
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    var data = (from s in db.mSecretory
                                orderby s.SecretoryName ascending
                                where s.IsDeleted == null
                                select s).ToList();
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mAssembly> GetAllAssemblies()
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    var data = (from a in db.mAssembly
                                orderby a.AssemblyName ascending
                                select a).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mDepartment> GetAllDepartments()
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    var data = (from d in db.mDepartment
                                orderby d.deptname ascending
                                where d.IsDeleted == null
                                select d).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static void CreateSecretoryDepartment(object param)
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    mSecretoryDepartment model = param as mSecretoryDepartment;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mSecretoryDepartment.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static object UpdateSecretoryDepartment(object param)
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    mSecretoryDepartment model = param as mSecretoryDepartment;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mSecretoryDepartment.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllSecretoryDepartment();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeleteSecretoryDepartment(object param)
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    mSecretoryDepartment model = param as mSecretoryDepartment;
                    var data = db.mSecretoryDepartment.SingleOrDefault(q => q.SecretoryDepartmentID == model.SecretoryDepartmentID);
                    if(data!=null)
                    {
                        data.IsDeleted = true;
                    }
                   // db.mSecretoryDepartment.Remove(data);
                    db.SaveChanges();
                    db.Close();

                }

                return GetAllSecretoryDepartment();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetSecDeptBasedOnId(object param)
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    mSecretoryDepartment model = param as mSecretoryDepartment;
                    var data = db.mSecretoryDepartment.SingleOrDefault(w => w.SecretoryDepartmentID == model.SecretoryDepartmentID);
                    return data;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #region UpdateSecDept

        private static void DelSecretoryDepartment(object param)
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    mSecretoryDepartment model = param as mSecretoryDepartment;
                    var data = db.mSecretoryDepartment.SingleOrDefault(q => q.SecretoryDepartmentID == model.SecretoryDepartmentID);
                    db.mSecretoryDepartment.Remove(data);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static object GetmSecretoryDepartmentBySecID(object param)
        {
            try
            {
                using (SecretoryDepartmentContext db = new SecretoryDepartmentContext())
                {
                    mSecretoryDepartment model = param as mSecretoryDepartment;

                    var data = (from sectDept in db.mSecretoryDepartment where sectDept.SecretoryID == model.SecretoryID select sectDept).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion UpdateSecDept

    }
}

