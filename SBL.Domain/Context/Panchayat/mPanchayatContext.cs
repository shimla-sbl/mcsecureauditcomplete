﻿using SBL.DAL;
using SBL.DomainModel.Models.Constituency;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Panchayat
{
    public class mPanchayatContext : DBBase<mPanchayatContext>
    {
        public mPanchayatContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public virtual DbSet<mPanchayat> mPanchayat { get; set; }
		public virtual DbSet<tConstituencyPanchayat> tConstituencyPanchayat { get; set; }
        public static object Excute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "GetAllPanchayat": { return GetAllPanchayat(param.Parameter); }

                case "CreatePanchayat": { CreatePanchayat(param.Parameter); break; }

                case "UpdatePanchayat": { return UpdatePanchayat(param.Parameter); }

                case "GetPanchayatBasedOnId": { return GetPanchayatBasedOnId(param.Parameter); }

                case "DeletePanchayat": { return DeletePanchayat(param.Parameter); }
               
            }
            return null;
        }

        static object GetAllPanchayat(object param)
        {
            try
            {
                using (mPanchayatContext db = new mPanchayatContext())
                {
                    
                    mPanchayat model = param as mPanchayat;
                    //string searchText = model.PanchayatName;
                    var data = (from a in db.mPanchayat
                                orderby a.PanchayatID descending
                                where a.IsDeleted ==null
                                select a).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                    var data1 = (from a in db.mPanchayat
                                // where a.PanchayatID
                                 orderby a.PanchayatID descending
                                 where a.IsDeleted == null
                                 select a).Count();
                    //var query = db.mPanchayat.ToList();
                    //if (searchText != null && searchText != "")
                    //{
                    //    query = (from p in query
                    //            where (
                    //            (searchText != null) && (p.PanchayatName.Contains(searchText)
                    //            || p.PanchayatNameLocal.Contains(searchText)
                    //            || p.PanchayatCode.Contains(searchText)))
                    //            select p).ToList();
                    //}
                    //var asd = query;
                    //model.PanchayatList = asd;
                    int totalRecords = data1;
                    model.ResultCount = totalRecords;
                    var results = data.ToList();
                    model.PanchayatList = results;
                    return model;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreatePanchayat(object param)
        {
            try
            {
                using (mPanchayatContext db = new mPanchayatContext())
                {
                    mPanchayat model = param as mPanchayat;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mPanchayat.Add(model);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static object UpdatePanchayat(object param)
        {
            try
            {
                using (mPanchayatContext db = new mPanchayatContext())
                {
                    mPanchayat model = param as mPanchayat;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mPanchayat.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllPanchayat(param);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetPanchayatBasedOnId(object param)
        {
            try
            {
                using (mPanchayatContext db = new mPanchayatContext())
                {
                    mPanchayat model = param as mPanchayat;
                    var data = db.mPanchayat.SingleOrDefault(a => a.PanchayatID == model.PanchayatID);
                    return data;
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object DeletePanchayat(object param)
        {
            try
            {
                using (mPanchayatContext db = new mPanchayatContext())
                {
                    mPanchayat model = param as mPanchayat;
                    mPanchayat panchRemove = db.mPanchayat.SingleOrDefault(e => e.PanchayatID == model.PanchayatID);
                    if(panchRemove!=null)
                    {
                        panchRemove.IsDeleted = true;
                    }
                    //db.mPanchayat.Remove(panchRemove);
                    db.SaveChanges();
                    db.Close();

                }
                return GetAllPanchayat(param);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
