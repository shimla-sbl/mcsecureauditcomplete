﻿using SBL.DAL;
using SBL.DomainModel.Models.States;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Constituency;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Panchayat
{
    
     public class PanchayatVillageContext : DBBase<PanchayatVillageContext>
    {


         public PanchayatVillageContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        
        public DbSet<DistrictModel> Districts { get; set; }
        public DbSet<mStates> mStates { get; set; }
        public DbSet<mPanchayat> mPanchayat { get; set; }
        public DbSet<mVillage> mVillage { get; set; }
        public DbSet<tPanchayatVillage> tPanchayatVillage { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreatePanchayatVillage": { CreatePanchayatVillage(param.Parameter); break; }

                case "UpdatePanchayatVillage": { return UpdatePanchayatVillage(param.Parameter); }
                case "DeletePanchayatVillage": { return DeletePanchayatVillage(param.Parameter); }
                case "GetAllPanchayatVillage": { return GetAllPanchayatVillage(); }
                case "GetAllPanchayatVillageSearch": { return GetAllPanchayatVillageSearch(param.Parameter); }
                case "GetPanchayatVillageById": { return GetPanchayatVillageById(param.Parameter); }
                case "GetAllState": { return GetAllState(); }
                case "GetAllDistrict": { return GetAllDistrict(); }
                case "GetAllPanchayat": { return GetAllPanchayat(); }
                case "SearchPanchayat": { return SearchPanchayat(param.Parameter); }
                case "GetAllVillage": { return GetAllVillage(); }

                
            }
            return null;
        }

        

        static void CreatePanchayatVillage(object param)
        {
            try
            {
                using (PanchayatVillageContext db = new PanchayatVillageContext())
                {
                    tPanchayatVillage model = param as tPanchayatVillage;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tPanchayatVillage.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdatePanchayatVillage(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (PanchayatVillageContext db = new PanchayatVillageContext())
            {
                tPanchayatVillage model = param as tPanchayatVillage;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.tPanchayatVillage.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllPanchayatVillage();
        }

        static object DeletePanchayatVillage(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (PanchayatVillageContext db = new PanchayatVillageContext())
            {
                tPanchayatVillage parameter = param as tPanchayatVillage;
                tPanchayatVillage stateToRemove = db.tPanchayatVillage.SingleOrDefault(a => a.PanchayatVillageID == parameter.PanchayatVillageID);
                if (stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
                
                db.SaveChanges();
                db.Close();
            }
            return GetAllPanchayatVillage();
        }

        

        static List<tPanchayatVillage> GetAllPanchayatVillage()
        
        {
            PanchayatVillageContext db = new PanchayatVillageContext();
            //tPanchayatVillage model = param as tPanchayatVillage;
            var data = (from sect in db.tPanchayatVillage
                        join m in db.mStates on sect.StateCode equals m.mStateID into joinStateAbbreviation
                        from n in joinStateAbbreviation.DefaultIfEmpty()
                        join m1 in db.Districts on sect.DistrictCode equals m1.DistrictCode into joinDistrictAbbreviation
                        from n1 in joinDistrictAbbreviation.DefaultIfEmpty()
                        join m2 in db.mPanchayat on sect.PanchayatCode equals m2.PanchayatCode into joinPanchayatAbbreviation
                        from n2 in joinPanchayatAbbreviation.DefaultIfEmpty()
                        join m3 in db.mVillage on sect.VillageCode equals m3.VillageCode into joinVillageAbbreviation
                        from n3 in joinVillageAbbreviation.DefaultIfEmpty()


                        where sect.IsDeleted == null
                        select new
                        {
                            sect,
                            n.StateName,
                            n1.DistrictName,
                            n2.PanchayatName,
                            n3.VillageName,


                        }).ToList();

            List<tPanchayatVillage> list = new List<tPanchayatVillage>();
            foreach (var item in data)
            {
                item.sect.GetStateName = item.StateName;
                item.sect.GetDistrictName = item.DistrictName;
                item.sect.GetPanchayatName = item.PanchayatName;
                item.sect.GetVillageName = item.VillageName;

                list.Add(item.sect);

            }
            return list.OrderByDescending(c => c.PanchayatVillageID).ToList();
        }


        static List<tPanchayatVillage> GetAllPanchayatVillageSearch(object param)
        {
            PanchayatVillageContext db = new PanchayatVillageContext();
            //tPanchayatVillage model = param as tPanchayatVillage;
            var id = Convert.ToInt32(param);
            var data = (from sect in db.tPanchayatVillage
                        join m in db.mStates on sect.StateCode equals m.mStateID into joinStateAbbreviation
                        from n in joinStateAbbreviation.DefaultIfEmpty()
                        join m1 in db.Districts on sect.DistrictCode equals m1.DistrictCode into joinDistrictAbbreviation
                        from n1 in joinDistrictAbbreviation.DefaultIfEmpty()
                        join m2 in db.mPanchayat on sect.PanchayatCode equals m2.PanchayatCode into joinPanchayatAbbreviation
                        from n2 in joinPanchayatAbbreviation.DefaultIfEmpty()
                        join m3 in db.mVillage on sect.VillageCode equals m3.VillageCode into joinVillageAbbreviation
                        from n3 in joinVillageAbbreviation.DefaultIfEmpty()


                        where sect.IsDeleted == null && sect.PanchayatCode == id
                        select new
                        {
                            sect,
                            n.StateName,
                            n1.DistrictName,
                            n2.PanchayatName,
                            n3.VillageName,


                        }).ToList();

            List<tPanchayatVillage> list = new List<tPanchayatVillage>();
            foreach (var item in data)
            {
                item.sect.GetStateName = item.StateName;
                item.sect.GetDistrictName = item.DistrictName;
                item.sect.GetPanchayatName = item.PanchayatName;
                item.sect.GetVillageName = item.VillageName;

                list.Add(item.sect);

            }
            return list.OrderByDescending(c => c.PanchayatVillageID).ToList();
        }



        static object SearchPanchayat(object param)
        {
            DBManager db = new DBManager();
            //var id = Convert.ToInt32(param);
            var pname = Convert.ToString(param);
            var data = (from m in db.mPanchayat
                       where m.IsDeleted == null && m.PanchayatName == pname && m.PanchayatName!=null
                       select m.PanchayatCode).FirstOrDefault();
            return data;
            
        }




        static tPanchayatVillage GetPanchayatVillageById(object param)
        {
            tPanchayatVillage parameter = param as tPanchayatVillage;
            PanchayatVillageContext db = new PanchayatVillageContext();
            var query = db.tPanchayatVillage.SingleOrDefault(a => a.PanchayatVillageID == parameter.PanchayatVillageID);
            return query;
        }

        static List<mStates> GetAllState()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mStates
                        orderby m.StateName ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;

        }

        static List<DistrictModel> GetAllDistrict()  
        {
            DBManager db = new DBManager();
            var data = (from m in db.Districts
                        orderby m.DistrictName ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;

        }

        static List<mPanchayat> GetAllPanchayat()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mPanchayat
                        orderby m.PanchayatName ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;

        }

        static List<mVillage> GetAllVillage()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mVillage
                        orderby m.VillageName ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;

        }



       
    }
}
