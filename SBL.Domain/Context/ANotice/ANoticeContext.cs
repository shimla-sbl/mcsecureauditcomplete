﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SBL.DAL;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.Category;
using SBL.Service.Common;
using System.Data;

namespace SBL.Domain.Context.ANotice
{
    public class ANoticeContext : DBBase<ANoticeContext>
    {
        public ANoticeContext()
            : base("eVidhan")
        {
        }
        public DbSet<mNotice> mNotice { get; set; }
        public virtual DbSet<Category> Category { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "AddNotice": { return AddNotice(param.Parameter); }
                    case "GetAllNotice": { return GetAllNotice(); }
                    case "GetAllNoticeLastWeek": { return GetAllNoticeLastWeek(); }
                    case "UpdateNotice": { return UpdateNotice(param.Parameter); }
                    case "DeleteNotice": { DeleteNotice(param.Parameter); break; }
                    case "GetNoticeById": { return GetNoticeById(param.Parameter); }
                    case "GetAllCategoryType": { return GetAllCategoryType(param.Parameter); }



                    case "AddNoticeCategory": { return AddNoticeCategory(param.Parameter); }
                    case "GetAllNoticeCategory": { return GetAllNoticeCategory(); }
                    case "UpdateNoticeCategory": { return UpdateNoticeCategory(param.Parameter); }
                    case "DeleteNoticeCategory": { DeleteNoticeCategory(param.Parameter); break; }
                    case "GetNoticeByIdCategory": { return GetNoticeByIdCategory(param.Parameter); }
                    case "IsNoticeCategoryChildExist": { return IsNoticeCategoryChildExist(param.Parameter); }

                }
            }
            return null;
        }

        private static object IsNoticeCategoryChildExist(object p)
        {
            try
            {
                using (ANoticeContext ctx = new ANoticeContext())
                {
                    Category data = (Category)p;

                    var isExist = (from a in ctx.mNotice where a.CategoryID == data.CategoryID select a).Count() > 0;

                    return isExist;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetAllCategoryType(object param)
        {
            try
            {
                using (ANoticeContext db = new ANoticeContext())
                {

                    var data = (from value in db.Category where (value.CategoryType == 1 || value.CategoryType == 2 || value.CategoryType == 5) && value.Status == 1
                                select value).OrderBy(m => m.CategoryName).ToList();

                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static void DeleteNotice(object param)
        {
            try
            {
                mNotice DataTodelete = param as mNotice;
                int id = DataTodelete.NoticeID;
                using (ANoticeContext db = new ANoticeContext())
                {
                    var NewsToDelete = db.mNotice.SingleOrDefault(a => a.NoticeID == id);
                    db.mNotice.Remove(NewsToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticeById(object param)
        {
            try
            {
                using (ANoticeContext db = new ANoticeContext())
                {
                    mNotice noticeToEdit = param as mNotice;

                    var data = db.mNotice.SingleOrDefault(o => o.NoticeID == noticeToEdit.NoticeID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static object AddNotice(object param)
        {

            try
            {
                mNotice notice = param as mNotice;
                using (ANoticeContext db = new ANoticeContext())
                {

                    notice.CreatedDate = DateTime.Now;
                    //notice.StartPublish = DateTime.Now;
                    //notice.StopPublish = DateTime.Now;
                    notice.ModifiedDate = DateTime.Now;
                   
                    db.mNotice.Add(notice);
                    db.SaveChanges();
                    if (!string.IsNullOrEmpty(notice.Attachments))
                    {
                        //notice.Attachments = notice.NoticeID + "_" + notice.Attachments;
                        notice.Attachments = notice.Attachments;
                        db.mNotice.Attach(notice);
                        db.Entry(notice).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    db.Close();


                }

                return string.IsNullOrEmpty(notice.Attachments) ? "" : notice.Attachments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;




        }

        static object GetAllNotice()
        {
            try
            {
                using (ANoticeContext db = new ANoticeContext())
                {
                    var data = (from a in db.mNotice join category in db.Category on a.CategoryID equals category.CategoryID orderby a.NoticeID descending select new { Notice = a, CategoryTypeName = category.CategoryName }).ToList();//db.mNews.ToList();

                    List<mNotice> result = new List<mNotice>();

                    foreach (var a in data)
                    {
                        mNotice partdata = new mNotice();
                        partdata = a.Notice;
                        partdata.CategoryName = a.CategoryTypeName;
                        result.Add(partdata);
                    }

                    return result;
                    //var notice = db.mNotice.ToList();
                    //return notice;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object GetAllNoticeLastWeek()
        {
            try
            {
                using (ANoticeContext db = new ANoticeContext())
                {
                    var data = (from a in db.mNotice join category in db.Category on a.CategoryID equals category.CategoryID orderby a.NoticeID descending select new { Notice = a, CategoryTypeName = category.CategoryName }).ToList().Take(7);//db.mNews.ToList();

                    List<mNotice> result = new List<mNotice>();

                    foreach (var a in data)
                    {
                        mNotice partdata = new mNotice();
                        partdata = a.Notice;
                        partdata.CategoryName = a.CategoryTypeName;
                        result.Add(partdata);
                    }

                    return result;
                    //var notice = db.mNotice.ToList();
                    //return notice;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object UpdateNotice(object param)
        {
            try
            {
                mNotice UpdateNotice = param as mNotice;
                using (ANoticeContext db = new ANoticeContext())
                {
                    var data = (from a in db.mNotice where UpdateNotice.NoticeID == a.NoticeID select a).FirstOrDefault();
                    data.ModifiedDate = DateTime.Now;
                    
                    data.AssemblyId = UpdateNotice.AssemblyId;
                    data.SessionId = UpdateNotice.SessionId;
                    data.Status = UpdateNotice.Status;
                    data.NoticeTitle = UpdateNotice.NoticeTitle;
                    data.NoticeDescription = UpdateNotice.NoticeDescription;
                    data.CategoryID = UpdateNotice.CategoryID;
                    data.Attachments = UpdateNotice.Attachments;
                    data.LocalNoticeDescription = UpdateNotice.LocalNoticeDescription;
                    data.LocalNoticeTitle = UpdateNotice.LocalNoticeTitle;
                    data.deptId = UpdateNotice.deptId;
                    //data.ModifiedBy = DateTime.Now;
                    db.mNotice.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateNotice;
            }
            catch
            {
                throw;
            }
        }



        static object GetAllNoticeCategory()
        {
            try
            {
                using (ANoticeContext db = new ANoticeContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from value in db.Category where (value.CategoryType == 1 || value.CategoryType == 2 || value.CategoryType == 5) select value).OrderByDescending(a => a.CategoryID).ToList();
                    // var data = db.Category.ToList();
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static object AddNoticeCategory(object param)
        {
            try
            {
                using (ANoticeContext newsContext = new ANoticeContext())
                {
                    Category news = param as Category;
                    news.CategoryType = 2;
                    news.CreatedDate = DateTime.Now;
                    news.ModifiedDate = DateTime.Now;
                    news.StartPublish = DateTime.Now;
                    news.StopPublish = DateTime.Now;
                    newsContext.Category.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object UpdateNoticeCategory(object param)
        {

            try
            {
                Category UpdateNews = param as Category;
                using (ANoticeContext db = new ANoticeContext())
                {
                    UpdateNews.CategoryType = 2;
                    UpdateNews.CreatedDate = DateTime.Now;
                    UpdateNews.ModifiedDate = DateTime.Now;
                    UpdateNews.StartPublish = DateTime.Now;
                    UpdateNews.StopPublish = DateTime.Now;
                    db.Category.Attach(UpdateNews);
                    db.Entry(UpdateNews).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateNews;
            }
            catch
            {
                throw;
            }
        }

        static void DeleteNoticeCategory(object param)
        {
            try
            {
                Category DataTodelete = param as Category;
                int id = DataTodelete.CategoryID;
                using (ANoticeContext db = new ANoticeContext())
                {
                    var NewsToDelete = db.Category.SingleOrDefault(a => a.CategoryID == id);
                    db.Category.Remove(NewsToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticeByIdCategory(object param)
        {
            try
            {
                using (ANoticeContext db = new ANoticeContext())
                {
                    Category NewsToEdit = param as Category;

                    var data = db.Category.SingleOrDefault(o => o.CategoryID == NewsToEdit.CategoryID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

    }
}
