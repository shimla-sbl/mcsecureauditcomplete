﻿#region Namespace reffrences
using SBL.Service.Common;
using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Notice;
using SBL.Domain.Context.paperLaid;
using System.Globalization;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.ComplexModel;
using System.Data;
#endregion

namespace SBL.Domain.Context.BillPaperLaid
{
    public class BillPaperLaidContext : DBBase<BillPaperLaidContext>
    {
        public BillPaperLaidContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mMember> mMember { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<mSession> mSession { get; set; }

        //Pending List
        public DbSet<mPaperCategoryType> mPaperCategoryType { get; set; }
        public DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }
        public DbSet<mEvent> mEvent { get; set; }
        public DbSet<tPaperLaidV> tPaperLaidV { get; set; }

        //PaperLaidEntry
        public DbSet<mDocumentType> mDocumentTypes { get; set; }
        public DbSet<mSessionDate> mSessionDates { get; set; }
        public DbSet<mBillType> mBillTypes { get; set; }
        public DbSet<mBills> mBills { get; set; }
        public DbSet<tQuestion> tQuestions { get; set; }
        public DbSet<tQuestionType> tQuestionTypes { get; set; }
        public DbSet<mNoticeType> mNoticeTypes { get; set; }
        public DbSet<tMemberNotice> tMemberNotice { get; set; }
        public DbSet<mMinistry> mMinistry { get; set; }
        public DbSet<mMinistryDepartment> mMInistryDepartment { get; set; }
        public DbSet<tBillRegister> tBillRegister { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                #region Srinivasa Bills code

                case "UpdatePaperLaidEntry":
                    {
                        return UpdatePaperLaidEntry(param.Parameter);
                    }

                case "InsertBillDetails":
                    {
                        return InsertBillDetails(param.Parameter);
                    }
                case "GetExisitngFileDetails":
                    {
                        return GetExisitngFileDetails(param.Parameter);
                    }
                case "GetPaperLaidTemp":
                    {
                        return GetPaperLaidTemp(param.Parameter);
                    }
                case "GetPaperLaidVs":
                    {
                        return GetPaperLaidVs(param.Parameter);
                    }
                    
                case "UpdateDetailsInTempPaperLaid":
                    {
                        return UpdateDetailsInTempPaperLaid(param.Parameter);
                    }

                case "SubmittPaperLaidEntry":
                    {
                        return SubmittPaperLaidEntry(param.Parameter);
                    }

                case "BillsSubmittPaperLaidEntry":
                    {
                        return BillsSubmittPaperLaidEntry(param.Parameter);
                    }

                    
                case "GetFileSaveDetails":
                    {
                        return GetFileSaveDetails(param.Parameter);
                    }

                case "SaveDetailsInTempPaperLaid":
                    {
                        return SaveDetailsInTempPaperLaid(param.Parameter);
                    }

                case "GetDepartmentNameMinistryDetailsByMinisterID":
                    {
                        return GetDepartmentNameMinistryDetailsByMinisterID(param.Parameter);
                    }
                case "GetBillTypes":
                    {
                        return GetBillTypes(param.Parameter);
                    }
                case "GetsentBill":
                    {
                        return GetsentBill(param.Parameter);
                    }

                case "ShowSentBillById":
                    {
                        return ShowSentBillById(param.Parameter);
                    }
                case "GetDocumentType":
                    {
                        return GetDocumentType(param.Parameter);
                    }

                case "GetAmendmentBills":
                    {
                        return GetAmendmentBills(param.Parameter);
                    }

                case "GetSessionDates":
                    {
                        return GetSessionDates(param.Parameter);
                    }

                case "CheckBillNUmber":
                    {
                        return CheckBillNUmber(param.Parameter);
                    }

                case "GetDraftsBill":
                    {
                        return GetDraftsBill(param.Parameter);
                    }
                case "GetpendingsBill":
                    {
                        return GetpendingsBill(param.Parameter);
                    }
                    
                case "ShowDraftBillById":
                    {
                        return ShowDraftBillById(param.Parameter);
                    }

                case "GetBillSentByID":
                    {
                        return GetBillSentByID(param.Parameter);
                    }
                case "GetBillFileVersion":
                    {
                        return GetBillFileVersion(param.Parameter);
                    }

                case "GetSubmittedPaperLaidByID":
                    {
                        return GetSubmittedPaperLaidByID(param.Parameter);
                    }
                case "UpdateBillPaperLaidFileByID":
                    {
                        return UpdateBillPaperLaidFileByID(param.Parameter);
                    }
                case "UpdateBillDetails":
                    {
                        return UpdateBillDetails(param.Parameter);
                    }
                case "GetCountForBillsandNotice":
                    {
                        return GetCountForBillsandNotice(param.Parameter);
                    }
                case "GetBillULOBList":
                    {
                        return GetBillULOBList(param.Parameter);
                    }
                case "GetBillLIHList":
                    {
                        return GetBillLIHList(param.Parameter);
                    }
                case "GetBillPLIHList":
                    {
                        return GetBillPLIHList(param.Parameter);
                    }
                case "UpdatePaperLaidEntryForBilReplace":
                    {
                        return UpdatePaperLaidEntryForBilReplace(param.Parameter);
                    }
                case "GetAllCurrentBillsList":
                    {
                        return GetAllCurrentBillsList(param.Parameter);
                    }
                case "GetDepartmentByid":
                    {
                        return GetDepartmentByid(param.Parameter);
                    }
                case "UpdateDetailsInTempPaperLaidNotices":
                    {
                        return UpdateDetailsInTempPaperLaidNotices(param.Parameter);
                    }
                case "CheckBillNUmberExist":
                    {
                        return CheckBillNUmberExist(param.Parameter);
                    }
                case "GetBillInfoByBillNo":
                    {
                        return GetBillInfoByBillNo(param.Parameter);
                    }
                    
                case "InsertmBillsRecord":
                    {
                        InsertmBillsRecord(param.Parameter); break;
                    }
                case "UpdatemBillsRecord":
                    {
                        UpdatemBillsRecord(param.Parameter); break;
                    }
                case "GetEventNameByID":
                    {
                        return GetEventNameByID(param.Parameter);
                    }

                case "BillsSaveDetailsInTempPaperLaid":
                    {
                        return BillsSaveDetailsInTempPaperLaid(param.Parameter);
                    }
            }
                #endregion


            return null;
        }



        static object BillsSaveDetailsInTempPaperLaid(object param)
        {

            if (null == param)
            {
                return null;
            }
            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
            //submitPaperTemp.DeptSubmittedDate =  DateTime.Now;
            //submitPaperTemp.DeptSubmittedBy = submitPaperTemp.DeptSubmittedBy;
            paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;
        }

        static List<mDepartment> GetDepartmentByid(object param)
        {

            mDepartment model = param as mDepartment;
            //tPaperLaidV model = param as tPaperLaidV;
            string csv = model.deptId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            using (var cnxt = new BillPaperLaidContext())
            {
                var data = (from mdl in cnxt.mDepartment where ids.Contains(mdl.deptId) select mdl).ToList();
                return data;
            }

        }

        static tPaperLaidV GetAllCurrentBillsList(object param)
        {
            tPaperLaidV data = (tPaperLaidV)param;
            List<tPaperLaidV> billsList = new List<tPaperLaidV>();


            // 5 and 6 ids are related to bills in Events.
            using (var dbcontext = new BillPaperLaidContext())
            {

                string csv = data.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);

                var BillsDrafts = (from a in dbcontext.tPaperLaidV
                                   join b in dbcontext.tPaperLaidTemp
                                       on a.PaperLaidId equals b.PaperLaidId
                                   join c in dbcontext.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                   where a.AssemblyId == data.AssemblyId
                                       && a.SessionId == data.SessionId
                                       && a.DeptActivePaperId == b.PaperLaidTempId
                                        && b.DeptSubmittedBy == null
                                        && b.DeptSubmittedDate == null

                                        && ids.Contains(a.DepartmentId)

                                   select new
                                   {
                                       paperLaidId = a.PaperLaidId,
                                       MinistryId = a.MinistryId,
                                       DepartmentId = a.DepartmentId,
                                       DeparmentName = a.DeparmentName,
                                       FileName = b.FileName,
                                       FilePath = b.FilePath,
                                       tpaperLaidTempId = b.PaperLaidTempId,
                                       FileVersion = b.Version,
                                       Title = a.Title,
                                       BillNumber = c.BillNumber,
                                       MinistryName = (from m in dbcontext.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
                                       MemberName = (from mbr in dbcontext.mMember
                                                     join minstry in dbcontext.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                                     where minstry.MinistryID == a.MinistryId
                                                     select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault(),
                                       DesireLayingDateId = a.DesireLayingDateId,
                                       PaperSent = b.FilePath + b.FileName
                                   }).ToList();

                var BillsSent = (from a in dbcontext.tPaperLaidV
                                 join b in dbcontext.tPaperLaidTemp
                                     on a.PaperLaidId equals b.PaperLaidId
                                 join c in dbcontext.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                 where a.AssemblyId == data.AssemblyId
                                     && a.SessionId == data.SessionId && (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
                                     && b.DeptSubmittedBy != null
                                     && b.DeptSubmittedDate != null
                                     && a.DeptActivePaperId == b.PaperLaidTempId

                                      && ids.Contains(a.DepartmentId)
                                 select new
                                 {
                                     paperLaidId = a.PaperLaidId,
                                     MinistryId = a.MinistryId,
                                     DepartmentId = a.DepartmentId,
                                     DeparmentName = a.DeparmentName,
                                     FileName = b.FileName,
                                     FilePath = b.FilePath,
                                     tpaperLaidTempId = b.PaperLaidTempId,
                                     FileVersion = b.Version,
                                     Title = a.Title,
                                     BillSentToMinisterDate = b.DeptSubmittedDate,
                                     BillNumber = c.BillNumber,
                                     MinistryName = (from m in dbcontext.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
                                     MemberName = (from mbr in dbcontext.mMember
                                                   join minstry in dbcontext.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                                   where minstry.MinistryID == a.MinistryId
                                                   select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault(),
                                     DesireLayingDateId = a.DesireLayingDateId,
                                     PaperSent = b.FilePath + b.FileName
                                 }).ToList();

                if (null != BillsDrafts)
                {
                    billsList.AddRange(BillsDrafts.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        Status = (int)QuestionDashboardStatus.DraftReplies,
                        MemberName = a.MemberName,
                        NoticeNumber = a.BillNumber,
                        PaperSent = a.PaperSent,
                        BOTStatus = (int)QuestionDashboardStatus.DraftReplies,
                        DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in dbcontext.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).SingleOrDefault()
                    }).ToList());
                }

                if (null != BillsSent)
                {
                    billsList.AddRange(BillsSent.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        DepartmentSubmitDate = a.BillSentToMinisterDate,
                        Status = (int)QuestionDashboardStatus.DraftReplies,
                        MemberName = a.MemberName,
                        NoticeNumber = a.BillNumber,
                        PaperSent = a.PaperSent,
                        DeptSubmittedDate = a.BillSentToMinisterDate,
                        BOTStatus = (int)QuestionDashboardStatus.RepliesSent,
                        DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in dbcontext.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).SingleOrDefault()

                    }).ToList());
                }

                data.ResultCount = billsList.Count;
                data.ListtPaperLaidV = billsList.Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();
                return data;
            }
        }
        //added code venkat for dynamic 
        private static tPaperLaidV GetBillPLIHList(object p)
        {
            using (var pCtxt = new BillPaperLaidContext())
            {
                // var currentdate = DateTime.Now;

                tPaperLaidV model = p as tPaperLaidV;
                var currentdate = DateTime.Now;
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                //added code venkat for dynamic 
                string BillNumber = "";
                string tiTle = "";
                int? MemberId = 0;
                if (model.BillNumber != "" && model.BillNumber != null)
                {
                    BillNumber = model.BillNumber;
                }
                if (model.Title != "" && model.Title != null)
                {
                    tiTle = model.Title;
                }
                if (model.MinistryId != 0 && model.MinistryId != null)
                {
                    MemberId = model.MinistryId;
                }
                //end--------------------------------
                var billPLIHCount = (from bill in pCtxt.tBillRegister
                                     join paperLaidC in pCtxt.tPaperLaidV on bill.PaperLaidId equals paperLaidC.PaperLaidId
                                     join paperLaidTemp in pCtxt.tPaperLaidTemp on bill.PaperLaidId equals paperLaidTemp.PaperLaidId
                                     where
                                         bill.PaperLaidId != null
                                         && paperLaidC.LaidDate == null
                                         && ((from m in pCtxt.mSessionDates where m.SessionId == paperLaidC.DesireLayingDateId select m.SessionDate).FirstOrDefault() > currentdate)
                                         && paperLaidC.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId
                                         && paperLaidC.LOBRecordId == null
                                         && paperLaidTemp.DeptSubmittedBy != null
                                         && paperLaidTemp.DeptSubmittedDate != null
                                         && paperLaidTemp.MinisterSubmittedBy != null
                                         && paperLaidTemp.MinisterSubmittedDate != null
                                          && ids.Contains(paperLaidC.DepartmentId)
                                         //added code venkat for dynamic 
                                          && (BillNumber == "" || BillNumber == bill.BillNumber)
                                        && (tiTle == "" || paperLaidC.Title == tiTle)
                                        //end------------------------------------------------
                                     select new
                                     {
                                         paperLaidId = paperLaidC.PaperLaidId,
                                         MinistryId = paperLaidC.MinistryId,
                                         DepartmentId = paperLaidC.DepartmentId,
                                         DeparmentName = paperLaidC.DeparmentName,
                                         FileName = paperLaidTemp.FileName,
                                         FilePath = paperLaidTemp.FilePath,
                                         tpaperLaidTempId = paperLaidTemp.PaperLaidTempId,
                                         FileVersion = paperLaidTemp.Version,
                                         Title = paperLaidC.Title,
                                         BillSentToMinisterDate = paperLaidTemp.DeptSubmittedDate,
                                         BillNumber = bill.BillNumber,
                                         MinistryName = (from m in pCtxt.mMinistry where m.MinistryID == paperLaidC.MinistryId select m.MinistryName).FirstOrDefault(),
                                         MemberName = (from mbr in pCtxt.mMember
                                                       join minstry in pCtxt.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                                       where minstry.MinistryID == paperLaidC.MinistryId
                                                       select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault()
                                     }).ToList();

                if (null != billPLIHCount)
                {
                    model.ResultCount = billPLIHCount.Count;
                    model.ListtPaperLaidV = billPLIHCount.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        DepartmentSubmitDate = a.BillSentToMinisterDate,
                        BillNumber = a.BillNumber,
                        MemberName = a.MemberName,
                        NoticeNumber = a.BillNumber
                    }).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                    return model;
                }
                else
                {
                    return model;
                }
            }
        }
        //added code venkat for dynamic 
        private static tPaperLaidV GetBillLIHList(object p)
        {
            using (var ctx = new BillPaperLaidContext())
            {
                tPaperLaidV model = p as tPaperLaidV;
                var currentdate = DateTime.Now;
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                //added code venkat for dynamic 
                string BillNumber = "";
                string tiTle = "";
                int? MemberId = 0;
                if (model.BillNumber != "" && model.BillNumber != null)
                {
                    BillNumber = model.BillNumber;
                }
                if (model.Title != "" && model.BillNumber != null)
                {
                    tiTle = model.Title;
                }
                if (model.MinistryId != 0 && model.MinistryId != null)
                {
                    MemberId = model.MinistryId;
                }
                //end-----------------------------------
                var billLIHCount = (from a in ctx.tPaperLaidV
                                    join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                    join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                    where c.PaperLaidId != null

                                    && a.LOBRecordId != null
                                    && a.DeptActivePaperId == b.PaperLaidTempId
                                    && a.MinisterActivePaperId != null
                                    && a.LaidDate != null
                                    && a.LaidDate <= currentdate
                                    && b.DeptSubmittedBy != null
                                    && b.DeptSubmittedDate != null
                                    && b.MinisterSubmittedBy != null
                                    && b.MinisterSubmittedDate != null
                                     && ids.Contains(a.DepartmentId)
                                        //added code venkat for dynamic 
                                    && (BillNumber == "" || BillNumber == c.BillNumber)
                                        && (tiTle == "" || a.Title == tiTle)
                                        //end------------------------------
                                    select new
                                    {
                                        paperLaidId = a.PaperLaidId,
                                        MinistryId = a.MinistryId,
                                        DepartmentId = a.DepartmentId,
                                        DeparmentName = a.DeparmentName,
                                        FileName = b.FileName,
                                        FilePath = b.FilePath,
                                        tpaperLaidTempId = b.PaperLaidTempId,
                                        FileVersion = b.Version,
                                        Title = a.Title,
                                        BillSentToMinisterDate = b.DeptSubmittedDate,
                                        BillNumber = c.BillNumber,
                                        MinistryName = (from m in ctx.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
                                        MemberName = (from mbr in ctx.mMember
                                                      join minstry in ctx.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                                      where minstry.MinistryID == a.MinistryId
                                                      select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault()
                                    }).ToList();

                if (null != billLIHCount)
                {
                    model.ResultCount = billLIHCount.Count;
                    model.ListtPaperLaidV = billLIHCount.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        DepartmentSubmitDate = a.BillSentToMinisterDate,
                        BillNumber = a.BillNumber,
                        MemberName = a.MemberName,
                        NoticeNumber = a.BillNumber
                    }).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                    return model;
                }
                else
                {
                    return model;
                }
            }
        }
        //added code venkat for dynamic 
        private static tPaperLaidV GetBillULOBList(object p)
        {
            tPaperLaidV model = p as tPaperLaidV;
            var currentdate = DateTime.Now;
            using (var ctx = new BillPaperLaidContext())
            {
                string csv = model.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                //added code venkat for dynamic 
                string BillNumber = "";
                string tiTle = "";
                int? MemberId = 0;
                if (model.BillNumber != "" && model.BillNumber != null)
                {
                    BillNumber = model.BillNumber;
                }
                if (model.Title != "" && model.Title != null)
                {
                    tiTle = model.Title;
                }
                if (model.MinistryId != 0 && model.MinistryId != null)
                {
                    MemberId = model.MinistryId;
                }
                //end-----------------------------------------
                var billULOBCount = (from a in ctx.tPaperLaidV
                                     join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                     join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                     where b.DeptSubmittedDate != null
                                     && b.DeptSubmittedBy != null
                                     && b.MinisterSubmittedBy != 0
                                     && b.MinisterSubmittedDate != null
                                     && ((from m in ctx.mSessionDates where m.SessionId == a.DesireLayingDateId select m.SessionDate).FirstOrDefault() >= currentdate)
                                     && a.LaidDate == null
                                     && a.LOBRecordId != null

                                      && ids.Contains(a.DepartmentId)
                                         //added code venkat for dynamic 
                                      && (BillNumber == "" || BillNumber == c.BillNumber)
                                        && (tiTle == "" || a.Title == tiTle)
                                        //end------------------------------------
                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         BillSentToMinisterDate = b.DeptSubmittedDate,
                                         BillNumber = c.BillNumber,
                                         MinistryName = (from m in ctx.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
                                         MemberName = (from mbr in ctx.mMember
                                                       join minstry in ctx.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                                       where minstry.MinistryID == a.MinistryId
                                                       select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault()
                                     }).ToList();
                if (null != billULOBCount)
                {
                    model.ResultCount = billULOBCount.Count;
                    model.ListtPaperLaidV = billULOBCount.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        DepartmentSubmitDate = a.BillSentToMinisterDate,
                        BillNumber = a.BillNumber,
                        MemberName = a.MemberName,
                        NoticeNumber = a.BillNumber

                    }).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                    return model;
                }
                else
                {
                    return model;
                }
            }

        }

        private static object GetCountForBillsandNotice(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            var currentdate = DateTime.Now;


            string csv = model.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);


            using (var ctx = new BillPaperLaidContext())
            {

                 var billPendingcount = (from a in ctx.tPaperLaidV
                                      join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                      join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                      where a.AssemblyId == model.AssemblyId
                                          && a.SessionId == model.SessionId
                                        && a.DeptActivePaperId == b.PaperLaidTempId
                                          && b.DeptSubmittedBy != null
                                          && b.DeptSubmittedDate != null
                                          && b.MinisterSubmittedBy == null
                                          && b.MinisterSubmittedDate == null
                                           && b.DeptSubmittedBy != null
                                          && b.DeptSubmittedDate != null
                                          && ids.Contains(a.DepartmentId)
                                          && b.FileName ==null

                                      select a).Count();
                 model.TotalBillPendingCount = billPendingcount;
            }
            using (var ctx = new BillPaperLaidContext())
            {
                var billdraftcount = (from a in ctx.tPaperLaidV
                                      join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                      join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                      where a.AssemblyId == model.AssemblyId
                                          && a.SessionId == model.SessionId
                                        && a.DeptActivePaperId == b.PaperLaidTempId
                                          && b.DeptSubmittedBy == null
                                          && b.DeptSubmittedDate == null
                                          && b.MinisterSubmittedBy == null
                                          && b.MinisterSubmittedDate == null
                                           && ids.Contains(a.DepartmentId)
                                         

                                      select a).Count();
                model.TotalBillDraftCount = billdraftcount;
            }

            using (var ctx = new BillPaperLaidContext())
            {
                var billsentcount = (from a in ctx.tPaperLaidV
                                     join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                     join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                     where a.AssemblyId == model.AssemblyId
                                         && a.SessionId == model.SessionId
                                         && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                         && b.DeptSubmittedBy != null
                                         && b.DeptSubmittedDate != null
                                         
                                         && a.DeptActivePaperId == b.PaperLaidTempId
                                         && ids.Contains(a.DepartmentId)
                                     select b).Count();
                model.TotalBillSentCount = billsentcount;
            }

            using (var ctx = new BillPaperLaidContext())
            {

                var billULOBCount = (from a in ctx.tPaperLaidV
                                     join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                     join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                     where b.DeptSubmittedDate != null && b.DeptSubmittedBy != null && b.MinisterSubmittedBy != 0
                                     && b.MinisterSubmittedDate != null && a.DesireLayingDate >= currentdate
                                     && a.LaidDate == null && a.LOBRecordId != null
                                     && ids.Contains(a.DepartmentId)
                                     && a.AssemblyId == model.AssemblyId
                                     && a.SessionId == model.SessionId
                                     select a).Count();
                model.BillUPLOBCount = billULOBCount;
            }


            using (var ctx = new BillPaperLaidContext())
            {
                //var currentdate = DateTime.Now;
                var billLIHCount = (from a in ctx.tPaperLaidV
                                    join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                    join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                    where c.PaperLaidId != null
                                    && ids.Contains(a.DepartmentId)
                                    && a.LOBRecordId != null && a.DeptActivePaperId == b.PaperLaidTempId
                                    && a.MinisterActivePaperId != null && a.LaidDate != null && a.LaidDate <= currentdate && b.DeptSubmittedBy != null && b.DeptSubmittedDate != null
                                    && b.MinisterSubmittedBy != null && b.MinisterSubmittedDate != null
                                    && a.AssemblyId == model.AssemblyId
                                     && a.SessionId == model.SessionId
                                    select a).Count();
                model.BillLIHCount = billLIHCount;
            }

            using (var pCtxt = new BillPaperLaidContext())
            {
                // var currentdate = DateTime.Now;

                var billPLIHCount = (from bill in pCtxt.tBillRegister
                                     join paperLaidC in pCtxt.tPaperLaidV on bill.PaperLaidId equals paperLaidC.PaperLaidId
                                     join paperLaidTemp in pCtxt.tPaperLaidTemp on bill.PaperLaidId equals paperLaidTemp.PaperLaidId
                                     where ids.Contains(bill.DepartmentId)
                                         && bill.PaperLaidId != null && paperLaidC.LaidDate == null && paperLaidC.DesireLayingDate > currentdate
                                         && paperLaidC.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidC.LOBRecordId == null
                                         && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
                                         && paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
                                         && paperLaidC.AssemblyId == model.AssemblyId
                                     && paperLaidC.SessionId == model.SessionId
                                     select paperLaidC).Count();
                model.BillPLIHCount = billPLIHCount;
            }

            model.TotalBillsCount = model.TotalBillSentCount + model.TotalBillDraftCount;


            ////------notice-----///

            using (var ctx = new BillPaperLaidContext())
            {

                var Inboxcount = (from a in ctx.tMemberNotice
                                  where a.AssemblyID == model.AssemblyId
                                      && a.SessionID == model.SessionId
                                      && a.PaperLaidId == null
                                      && a.DepartmentId != null
                                      && a.MinistryId != null
                                      && a.IsSubmitted == true
                                      && ids.Contains(a.DepartmentId)
                                  select a).Count();
                model.NoticeInboxCount = Inboxcount;

            }

            using (var ctx = new BillPaperLaidContext())
            {
                var noticedraftreplycount = (from a in ctx.tPaperLaidV
                                             join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                             join c in ctx.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                             where a.AssemblyId == model.AssemblyId
                                                 && a.SessionId == model.SessionId
                                                 && b.DeptSubmittedBy == null
                                                 && b.DeptSubmittedDate == null
                                                 && b.MinisterSubmittedDate==null
                                                 && ids.Contains(a.DepartmentId)
                                                 && a.DeptActivePaperId == b.PaperLaidTempId
                                                
                                             select a).Count();
                model.NoticeReplayDraft = noticedraftreplycount;
            }

            using (var ctx = new BillPaperLaidContext())
            {
                var noticesentReplycount = (from a in ctx.tPaperLaidV
                                            join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                            join c in ctx.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                            where a.AssemblyId == model.AssemblyId
                                                && a.SessionId == model.SessionId
                                                && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                && b.DeptSubmittedBy != null
                                                && b.DeptSubmittedDate != null
                                                && b.MinisterSubmittedDate !=null
                                                && a.DeptActivePaperId == b.PaperLaidTempId
                                                && ids.Contains(a.DepartmentId)
                                            select b).Count();
                model.NoticeReplaySent = noticesentReplycount;
            }

            using (var ctx = new BillPaperLaidContext())
            {
                //  var currentdate = DateTime.Now;
                var noticeULOBCount = (from a in ctx.tPaperLaidV
                                       join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                       join c in ctx.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                       where b.DeptSubmittedDate != null && b.DeptSubmittedBy != null && b.MinisterSubmittedBy != 0 && b.MinisterSubmittedDate != null && a.DesireLayingDate >= currentdate
                                       && a.LaidDate == null && a.LOBRecordId != null
                                       && ids.Contains(a.DepartmentId)
                                       && a.AssemblyId == model.AssemblyId
                                     && a.SessionId == model.SessionId
                                       select a).Count();
                model.NoticeUPLOBCount = noticeULOBCount;
            }


            using (var ctx = new BillPaperLaidContext())
            {
                // var currentdate = DateTime.Now;
                var noticeLIHCount = (from a in ctx.tPaperLaidV
                                      join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                      join c in ctx.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                      where c.PaperLaidId != null
                                      && ids.Contains(a.DepartmentId)
                                      && a.LOBRecordId != null && a.DeptActivePaperId == b.PaperLaidTempId
                                      && a.MinisterActivePaperId != null && a.LaidDate != null && a.LaidDate <= currentdate && b.DeptSubmittedBy != null && b.DeptSubmittedDate != null
                                      && b.MinisterSubmittedBy != null && b.MinisterSubmittedDate != null
                                      && a.AssemblyId == model.AssemblyId
                                     && a.SessionId == model.SessionId
                                      select a).Count();

                model.NoticeLIHCount = noticeLIHCount;
            }

            using (var pCtxt = new BillPaperLaidContext())
            {
                //var currentdate = DateTime.Now;

                var noticePLIHCount = (from notice in pCtxt.tMemberNotice
                                       join paperLaidC in pCtxt.tPaperLaidV on notice.PaperLaidId equals paperLaidC.PaperLaidId
                                       join paperLaidTemp in pCtxt.tPaperLaidTemp on notice.PaperLaidId equals paperLaidTemp.PaperLaidId
                                       where ids.Contains(notice.DepartmentId)
                                           && notice.PaperLaidId != null && paperLaidC.LaidDate == null && paperLaidC.DesireLayingDate > currentdate
                                           && paperLaidC.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidC.LOBRecordId == null
                                           && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
                                           && paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
                                           && paperLaidC.AssemblyId == model.AssemblyId
                                     && paperLaidC.SessionId == model.SessionId
                                       select paperLaidC).Count();
                model.NoticePLIHCount = noticePLIHCount;
            }

            model.TotalNoticesCount = model.NoticePLIHCount + model.NoticeLIHCount + model.NoticeUPLOBCount + model.NoticeReplaySent + model.NoticeReplayDraft + model.NoticeInboxCount;
            return model;
        }


        private static object UpdateBillDetails(object p)
        {
            tBillRegister model = (tBillRegister)p;
            using (var ctx = new BillPaperLaidContext())
            {
                var data = (from a in ctx.tBillRegister where a.PaperLaidId == model.PaperLaidId select a).FirstOrDefault();

                data.ShortTitle = model.ShortTitle;
                //data.BillNumber = model.BillNumber;
                data.BillTypeId = model.BillTypeId;
                data.MinisterId = model.MinisterId;
                data.BillDate = model.BillDate;
                data.PreviousBillId = model.BillId;
                data.IsAmendment = model.IsAmendment;
             
                ctx.SaveChanges();
                return data;

            }
        }

        //private static object UpdateBillDetails(object p)
        //{
        //    var data = (tBillRegister)p;
        //    using (var ctx = new BillPaperLaidContext())
        //    {
        //        var billdata = (from a in ctx.tBillRegister where a.PaperLaidId == data.PaperLaidId select a).FirstOrDefault();

        //        billdata.ShortTitle = data.ShortTitle;
        //        billdata.BillNumber = data.BillNumber;
        //        billdata.BillTypeId = data.BillTypeId;
        //        billdata.MinisterId = data.MinisterId;
        //        billdata.BillDate = data.BillDate;
        //        billdata.PreviousBillId = data.BillId;
        //        billdata.IsAmendment = data.IsAmendment;
        //        ctx.SaveChanges();

        //    }



        //    return data;


        //    // throw new NotImplementedException();
        //}




        #region BillPaperLaid SrinivasaCode

        /// <summary>
        /// Author:Srinivasa
        /// 
        /// Usage: This method is used for geting Drafts Bills from tpaperLaidVS and tpaperLaidTemp 
        /// 
        /// Dependencies: THis method is used by GetDraftBills method of PaperLaidDepartment Controller
        /// </summary>
        /// <param name="p">tpaperLaidV  </param>
        /// <returns>List of tpaperLaidV</returns>

        static object UpdateBillPaperLaidFileByID(object param)
        {
            tPaperLaidTemp model = param as tPaperLaidTemp;

            BillPaperLaidContext db = new BillPaperLaidContext();
            if (model == null)
            {
                return null;
            }
            var RefNumber = model.AssemblyId + "/" + model.SessionId + "/" + model.PaperLaidId + "/" + "V" + model.Version;
            model.evidhanReferenceNumber = RefNumber;
            model = db.tPaperLaidTemp.Add(model);
            db.SaveChanges();
            model.PaperLaidTempId = model.PaperLaidTempId;

            db.Close();
            return model;
        }
        private static tPaperLaidV ShowSentBillById(object p)
        {
            tPaperLaidV data = (tPaperLaidV)p;

            using (var ctx = new BillPaperLaidContext())
            {

                var paperLaidList = (from a in ctx.tPaperLaidV
                                     join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                     join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                     join f in ctx.mMinistry on a.MinistryId equals f.MinistryID
                                     where a.PaperLaidId == data.PaperLaidId && a.DeptActivePaperId == b.PaperLaidTempId
                                     select new
                                     {
                                         PaperLaidId = a.PaperLaidId,
                                         AssemblyId = a.AssemblyId,
                                         SessionId = a.SessionId,
                                         //MinistryId = a.MinistryId,
                                         MinistryName = f.MinisterName,
                                         //DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         PaperSent = b.SignedFilePath,
                                         Title = a.Title,
                                         DepartmentLayedDate = b.DeptSubmittedDate,
                                         //  DesireLayingDateId = a.DesireLayingDateId,
                                         Description = a.Description,
                                         DocFileName = b.DocFileName,
                                         ProvisionUnderWhich = a.ProvisionUnderWhich,
                                         Remark = a.Remark,
                                         PaperTypeId = a.PaperTypeID,
                                         BillTypeId = c.BillTypeId,
                                         BillNumber = c.BillNumber,
                                         BillDate = c.BillDate,
                                         //PaperTypeName = d.DocumentType,                                         
                                         DesireLayingDateId = a.DesireLayingDateId
                                     }).FirstOrDefault();
                if (null != paperLaidList)
                    return new tPaperLaidV
                    {
                        SessionId = paperLaidList.SessionId,
                        AssemblyId = paperLaidList.AssemblyId,
                        //MinistryId = paperLaidList.MinistryId,
                        MinistryName = paperLaidList.MinistryName,
                        //DepartmentId = paperLaidList.DepartmentId,
                        DeparmentName = paperLaidList.DeparmentName,
                        FileName = paperLaidList.FileName,
                        DocFileName = paperLaidList.DocFileName,
                        FilePath = paperLaidList.FilePath,
                        tpaperLaidTempId = paperLaidList.tpaperLaidTempId,
                        FileVersion = paperLaidList.FileVersion,
                        Title = paperLaidList.Title,
                       // actualFilePath = paperLaidList.FilePath + paperLaidList.FileName,
                        actualFilePath = paperLaidList.PaperSent,
                        // DesireLayingDateId = paperLaidList.DesireLayingDateId,
                        Description = paperLaidList.Description,
                        ProvisionUnderWhich = paperLaidList.ProvisionUnderWhich,
                        Remark = paperLaidList.Remark,
                        PaperTypeID = paperLaidList.PaperTypeId,
                        BillDate = paperLaidList.BillDate.ToString(),
                        BillNUmberValue = Convert.ToInt32(paperLaidList.BillNumber.Split(' ')[0]),
                        BillNumberYear = Convert.ToInt32(paperLaidList.BillNumber.Split(' ')[2]),
                        BillNumber = paperLaidList.BillNumber,
                        BillTypeId = paperLaidList.BillTypeId,
                        PaperLaidId = paperLaidList.PaperLaidId,
                        //PaperTypeName = paperLaidList.PaperTypeName,
                        DepartmentSubmitDate = paperLaidList.DepartmentLayedDate,
                        DesireLayingDate = paperLaidList.DesireLayingDateId == 0 ? (DateTime?)null : (from m in ctx.mSessionDates where m.Id == paperLaidList.DesireLayingDateId select m.SessionDate).SingleOrDefault()
                        //EventId = paperLaidList.EventId

                    };

                return null;
            }
        }


        //public static object GetsentBill(object p)
        //{
        //   tPaperLaidV data = (tPaperLaidV)p;
        //   BillPaperLaidContext dbcontext = new BillPaperLaidContext();

        //   string csv = data.DepartmentId;
        //   IEnumerable<string> ids = csv.Split(',').Select(str => str);
        //   //added code venkat for dynamic 
        //   string BillNumber = "";
        //   string tiTle = "";
        //   int? MemberId = 0;
        //   if (data.BillNumber != "" && data.BillNumber != null)
        //   {
        //       BillNumber = data.BillNumber;
        //   }
        //   if (data.Title != "" && data.Title != null)
        //   {
        //       tiTle = data.Title;
        //   }
        //   if (data.MemberId != 0 && data.MemberId != null)
        //   {
        //       MemberId = data.MemberId;
        //   }

           


        //    //Get Submitted Questions.
        //    var paperLaidList = (from a in dbcontext.tPaperLaidV
        //                         join b in dbcontext.tPaperLaidTemp
        //                             on a.PaperLaidId equals b.PaperLaidId
        //                         join c in dbcontext.tBillRegister on a.PaperLaidId equals c.PaperLaidId
        //                         where a.AssemblyId == data.AssemblyId
        //                             && a.SessionId == data.SessionId && (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
        //                             && b.DeptSubmittedBy != null
        //                             && b.DeptSubmittedDate != null
        //                             && a.DeptActivePaperId == b.PaperLaidTempId
        //                             //added code venkat for dynamic 
        //                             //&& (BillNumber == "" || BillNumber == c.BillNumber)
        //                             // && (tiTle == "" || a.Title == tiTle)
        //                             //end--------------------------------
        //                             //&& (MemberId == 0 || a.MemberId == MemberId)
        //                              && ids.Contains(a.DepartmentId)

        //                         select new tPaperLaidV
        //                         {
                                    
        //                             PaperLaidId = a.PaperLaidId,
        //                             MinistryId = a.MinistryId,
        //                             DepartmentId = a.DepartmentId,
        //                             DeparmentName = a.DeparmentName,
        //                             FileName = b.FileName,
        //                             FilePath = b.FilePath,
        //                             DeptSubmittedDate = b.DeptSubmittedDate,
        //                             tpaperLaidTempId = b.PaperLaidTempId,
        //                             FileVersion = b.Version,
        //                             Title = a.Title,
        //                             DepartmentSubmitDate = b.DeptSubmittedDate,
        //                             BillNumber = c.BillNumber,
        //                             MinistryName = (from m in dbcontext.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
        //                             MemberName = (from mbr in dbcontext.mMember
        //                                           join minstry in dbcontext.mMinistry on mbr.MemberCode equals minstry.MemberCode
        //                                           where minstry.MinistryID == a.MinistryId
        //                                           select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault(),
        //                             DesireLayingDateId = a.DesireLayingDateId,
        //                             PaperSent = b.FilePath + b.FileName
        //                         }).ToList();

        //    foreach (var item in paperLaidList)
        //    {

        //        item.DepartmentSubmittedDateNew = Convert.ToDateTime(item.DepartmentSubmitDate).ToString("dd/MM/yyyy hh:mm:ss tt");

        //    }

        //    return paperLaidList;
        //}


        //added code venkat for dynamic 
        private static tPaperLaidV GetsentBill(object p)
        {
            tPaperLaidV data = (tPaperLaidV)p;

            // 5 and 6 ids are related to bills in Events.

            using (var dbcontext = new BillPaperLaidContext())
            {
                string csv = data.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                //added code venkat for dynamic 
                string BillNumber = "";
                string tiTle = "";
                int? MemberId = 0;
                if (data.BillNumber != "" && data.BillNumber != null)
                {
                    BillNumber = data.BillNumber;
                }
                if (data.Title != "" && data.Title != null)
                {
                    tiTle = data.Title;
                }
                if (data.MemberId != 0 && data.MemberId != null)
                {
                    MemberId = data.MemberId;
                }
               //end--------------------------------
                var paperLaidList = (from a in dbcontext.tPaperLaidV
                                     join b in dbcontext.tPaperLaidTemp
                                         on a.PaperLaidId equals b.PaperLaidId
                                     join c in dbcontext.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                     join E in dbcontext.mEvent on a.EventId equals E.EventId
                                     where a.AssemblyId == data.AssemblyId
                                         && a.SessionId == data.SessionId && (a.DeptActivePaperId != null && a.DeptActivePaperId != 0)
                                         && b.DeptSubmittedBy != null
                                         && b.DeptSubmittedDate != null
                                         //&& a.DeptActivePaperId == b.PaperLaidTempId
                                         //added code venkat for dynamic 
                                       //&& (BillNumber == "" || BillNumber == c.BillNumber)
                                       // && (tiTle == "" || a.Title == tiTle)
                                        //end--------------------------------
                                         //&& (MemberId == 0 || a.MemberId == MemberId)
                                          && ids.Contains(a.DepartmentId)
                                     orderby b.PaperLaidId descending

                                     select new
                                     {
                                         BussinessType = E.EventName,
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         Status= c.Status,
                                         BillReturnRemarks =c.Status ==0 ? "Sent To Additional Commissioner" : c.Status ==1 ? "Approved By Additional Commissioner and Send to Commissioner. Remarks is:" + c.BillReturnRemarks : c.Status ==2 ? "Approved By Commissioner and Send to Mayor.Remarks is:" + c.BillReturnRemarks : c.Status == 3 ? "Approved By Mayor and Send back to Commissioner.Remarks is:" + c.BillReturnRemarks : c.Status == 4 ? "Approved By Commissioner and Send back to Additional Commissioner.Remarks is:" + c.BillReturnRemarks : c.Status == 5 ? "Approved By Additional Commissioner and Send to Meeting Assistance.Remarks is:" + c.BillReturnRemarks: c.Status == 6 ? "Committe Meeting Discussion Approved By Meeting Assistance and Send to Additional Commissioner.Remarks is:" + c.BillReturnRemarks : c.Status == 7 ? "Committe Meeting Discussion Approved By Additional Commissioner and Send to Commissioner.Remarks is:" + c.BillReturnRemarks : c.Status == 8 ? "Committe Meeting Discussion Approved By Commissioner and Send to Mayor.Remarks is:" + c.BillReturnRemarks : c.Status == 9 ? "Committe Meeting Discussion Approved By Mayor and Send Back to Commissioner.Remarks is:" + c.BillReturnRemarks : c.Status == 10 ? " Committe Meeting Discussion Approved By Commissioner and Send back to Additional Commissioner.Remarks is:" + c.BillReturnRemarks : c.Status == 11 ? " Committe Meeting Discussion Final Approved By Additional Commissioner and Send to Meeting Assistance for Memorandum to be laid in house.Remarks is:" + c.BillReturnRemarks : "",


                                        

                                          evidhanReferenceNumber = b.evidhanReferenceNumber,
                                         BillSentToMinisterDate = b.DeptSubmittedDate,
                                         BillNumber = c.BillNumber,
                                         MinistryName = (from m in dbcontext.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
                                         MemberName = (from mbr in dbcontext.mMember
                                                       join minstry in dbcontext.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                                       where minstry.MinistryID == a.MinistryId
                                                       select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault(),
                                         DesireLayingDateId = a.DesireLayingDateId,
                                         PaperSent = b.FilePath + b.FileName
                                     }).OrderByDescending(a=>a.paperLaidId).ToList();

                if (null != paperLaidList)
                {
                    data.ResultCount = paperLaidList.Count;
                    data.ListtPaperLaidV = paperLaidList.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        BussinessType=a.BussinessType,
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        evidhanReferenceNumber = a.evidhanReferenceNumber,
                        actualFilePath = a.FilePath + a.FileName,
                        DepartmentSubmitDate = a.BillSentToMinisterDate,
                        MemberName = a.MemberName,
                        NoticeNumber = a.BillNumber,
                        PaperSent = a.PaperSent,
                        DeptSubmittedDate = a.BillSentToMinisterDate,
                        Status = a.Status,
                        Remark=a.BillReturnRemarks,
                        DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in dbcontext.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).SingleOrDefault()
                    }).Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).OrderByDescending(a=>a.PaperLaidId).ToList();


                   

                    return data;
                }
                else
                {

                    return data;
                }
            }

        }

      

    private static tPaperLaidV GetpendingsBill(object p)
        {
            tPaperLaidV data = (tPaperLaidV)p;

            // 5 and 6 ids are related to bills in Events.

            using (var dbcontext = new BillPaperLaidContext())
            {

                string csv = data.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);

                var paperLaidList = (from a in dbcontext.tPaperLaidV
                                     join b in dbcontext.tPaperLaidTemp
                                         on a.PaperLaidId equals b.PaperLaidId
                                     join c in dbcontext.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                     where a.AssemblyId == data.AssemblyId
                                         && a.SessionId == data.SessionId
                                          && b.DeptSubmittedBy != null
                                          && b.DeptSubmittedDate != null
                                           && b.MinisterSubmittedBy == null
                                          && b.MinisterSubmittedDate == null
                                          && a.DeptActivePaperId == b.PaperLaidTempId
                                           && ids.Contains(a.DepartmentId)
                                            && b.FileName == null
                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         BillNumber = c.BillNumber,
                                         
                                         MinistryName = (from m in dbcontext.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
                                         MemberName = (from mbr in dbcontext.mMember
                                                       join minstry in dbcontext.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                                       where minstry.MinistryID == a.MinistryId
                                                       select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault(),
                                         DesireLayingDateId = a.DesireLayingDateId,
                                         PaperSent = b.FilePath + b.FileName
                                     }).ToList();

                if (null != paperLaidList)
                {
                    data.ResultCount = paperLaidList.Count;
                    data.ListtPaperLaidV = paperLaidList.AsEnumerable().Select(a => new tPaperLaidV
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        MemberName = a.MemberName,
                        NoticeNumber = a.BillNumber,
                        PaperSent = a.PaperSent,
                        Status = (int)QuestionDashboardStatus.DraftReplies,
                        DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in dbcontext.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).SingleOrDefault()
                    }).Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();

                    return data;
                }
                else
                {
                    return data;
                }
                // var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            }


            //  throw new NotImplementedException();
        }
        //added code venkat for dynamic 
        private static tPaperLaidV GetDraftsBill(object p)
        {
            tPaperLaidV data = (tPaperLaidV)p;

            // 5 and 6 ids are related to bills in Events.

            using (var dbcontext = new BillPaperLaidContext())
            {

                string csv = data.DepartmentId;
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                //added code venkat for dynamic 
                string BillNumber = "";
                string tiTle = "";
                int? MemberId = 0;
                if (data.BillNumber != "" && data.BillNumber != "null" && data.BillNumber != null)
                {
                    BillNumber = data.BillNumber;
                }
                if (data.Title != "" && data.Title != "null" && data.Title != null)
                {
                    tiTle = data.Title;
                }
                if (data.MemberId != 0 && data.MemberId != null)
                {
                    MemberId = data.MemberId;
                }
                //end---------------------------


                
                var paperLaidList = (from a in dbcontext.tPaperLaidV
                                     join b in dbcontext.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                     join c in dbcontext.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                     // join mbr1 in dbcontext.mMember on a.MemberId equals mbr1.MemberID
                                     //join minstry1 in dbcontext.mMinistry on mbr1.MemberCode equals minstry1.MemberCode
                                     where a.AssemblyId == data.AssemblyId
                                         && a.SessionId == data.SessionId
                                          && a.DeptActivePaperId == b.PaperLaidTempId
                                          && b.DeptSubmittedBy == null
                                          && b.DeptSubmittedDate == null
                                           && ids.Contains(a.DepartmentId)
                                         //added code venkat for dynamic 
                                          && (BillNumber == "" || BillNumber == c.BillNumber)
                                        && (tiTle == "" || a.Title == tiTle)
                                        && (MemberId == 0 || MemberId == a.MemberId)
                                        //end-------------------------------------------

                                     select new
                                     {
                                         paperLaidId = a.PaperLaidId,
                                         MinistryId = a.MinistryId,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         BillNumber = c.BillNumber,
                                         evidhanReferenceNumber = b.evidhanReferenceNumber,
                                         MinistryName = (from m in dbcontext.mMinistry where m.MinistryID == a.MinistryId select m.MinistryName).FirstOrDefault(),
                                         MemberName = (from mbr in dbcontext.mMember
                                                       join minstry in dbcontext.mMinistry on mbr.MemberCode equals minstry.MemberCode
                                                       where minstry.MinistryID == a.MinistryId
                                                       select (mbr.Prefix + " " + mbr.Name)).FirstOrDefault(),
                                         DesireLayingDateId = a.DesireLayingDateId,
                                         PaperSent = b.FilePath + b.FileName
                                     }).ToList();

                if (null != paperLaidList)
                {
                    data.ResultCount = paperLaidList.Count;
                    data.BillsDraftCustomModel = paperLaidList.AsEnumerable().Select(a => new BillsDraftCustomModel
                    {
                        PaperLaidId = a.paperLaidId,
                        MinistryId = a.MinistryId,
                        MinistryName = a.MinistryName,
                        DepartmentId = a.DepartmentId,
                        DeparmentName = a.DeparmentName,
                        FileName = a.FileName,
                        FilePath = a.FilePath,
                        tpaperLaidTempId = a.tpaperLaidTempId,
                        FileVersion = a.FileVersion,
                        Title = a.Title,
                        actualFilePath = a.FilePath + a.FileName,
                        MemberName = a.MemberName,
                        evidhanReferenceNumber = a.evidhanReferenceNumber,
                        NoticeNumber = a.BillNumber,
                        PaperSent = a.PaperSent,
                        Status = (int)QuestionDashboardStatus.DraftReplies,
                        DesireLayingDate = a.DesireLayingDateId == 0 ? (DateTime?)null : (from m in dbcontext.mSessionDates where m.Id == a.DesireLayingDateId select m.SessionDate).SingleOrDefault()
                    }).Skip((data.PageIndex - 1) * data.PAGE_SIZE).Take(data.PAGE_SIZE).ToList();

                    return data;
                }
                else
                {
                    return data;
                }
                // var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            }


            //  throw new NotImplementedException();
        }

        static tPaperLaidV ShowDraftBillById(object param)
        {
            tPaperLaidV data = (tPaperLaidV)param;

            using (var ctx = new BillPaperLaidContext())
            {

                var paperLaidList = (from a in ctx.tPaperLaidV
                                     join b in ctx.tPaperLaidTemp
                                         on a.PaperLaidId equals b.PaperLaidId
                                     join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                     where a.PaperLaidId == data.PaperLaidId && a.DeptActivePaperId == b.PaperLaidTempId
                                    
                                     select new
                                     {
                                         PaperLaidId = a.PaperLaidId,
                                         AssemblyId = a.AssemblyId,
                                         SessionId = a.SessionId,
                                         MinistryId = a.MinistryId,
                                         MinistryName = a.MinistryName,
                                         DepartmentId = a.DepartmentId,
                                         DeparmentName = a.DeparmentName,
                                         FileName = b.FileName,
                                         FilePath = b.FilePath,
                                         DocFileName = b.DocFileName,
                                         tpaperLaidTempId = b.PaperLaidTempId,
                                         FileVersion = b.Version,
                                         Title = a.Title,
                                         DesireLayingDateId = a.DesireLayingDateId,
                                         Description = a.Description,
                                         ProvisionUnderWhich = a.ProvisionUnderWhich,
                                         Remark = a.Remark,
                                         PaperTypeId = a.PaperTypeID,
                                         BillTypeId = c.BillTypeId,
                                         BillNumber = c.BillNumber,
                                         BillDate = c.BillDate,
                                         EventId = a.EventId,
                                         Bills = c.BillId
                                     }).OrderByDescending(x=>x.FileVersion).FirstOrDefault();

                return new tPaperLaidV
                {
                    SessionId = paperLaidList.SessionId,
                    AssemblyId = paperLaidList.AssemblyId,
                    MinistryId = paperLaidList.MinistryId,
                    //MinistryName = paperLaidList.MinistryName,
                    DepartmentId = paperLaidList.DepartmentId,
                    //DeparmentName = paperLaidList.DeparmentName,
                    FileName = paperLaidList.FileName,
                    FilePath = paperLaidList.FilePath,
                    tpaperLaidTempId = paperLaidList.tpaperLaidTempId,
                    FileVersion = paperLaidList.FileVersion,
                    Title = paperLaidList.Title,
                    actualFilePath = paperLaidList.FilePath + paperLaidList.FileName,
                    DocFileName = paperLaidList.DocFileName,
                    DesireLayingDateId = paperLaidList.DesireLayingDateId,
                    Description = paperLaidList.Description,
                    ProvisionUnderWhich = paperLaidList.ProvisionUnderWhich,
                    Remark = paperLaidList.Remark,
                    PaperTypeID = paperLaidList.PaperTypeId,
                    BillDate = paperLaidList.BillDate != null ? string.Format("{0:dd/MM/yyyy}", paperLaidList.BillDate) : null,
                    BillNUmberValue = Convert.ToInt32(paperLaidList.BillNumber.Split(' ')[0]),
                    BillNumberYear = Convert.ToInt32(paperLaidList.BillNumber.Split(' ')[2]),
                    BillTypeId = paperLaidList.BillTypeId,
                    PaperLaidId = paperLaidList.PaperLaidId,
                    EventId = paperLaidList.EventId,
                    BillId = paperLaidList.Bills

                };
            }

        }

        static tPaperLaidV GetDepartmentNameMinistryDetailsByMinisterID(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            BillPaperLaidContext plCntxt = new BillPaperLaidContext();
            var mMinistry = (from mdl in plCntxt.mMinistry where mdl.MinistryID == paperLaid.MinistryId select mdl).SingleOrDefault();
            paperLaid.MinistryName = mMinistry.MinistryName;
            paperLaid.MinistryNameLocal = mMinistry.MinistryNameLocal;
            var DepartmentName = (from mod in plCntxt.mDepartment where paperLaid.DepartmentId == mod.deptId select mod.deptname).SingleOrDefault();
            paperLaid.DeparmentName = DepartmentName;
            return paperLaid;

        }


        static object GetBillFileVersion(object param)
        {

            tPaperLaidV model = param as tPaperLaidV;
            BillPaperLaidContext cnxt = new BillPaperLaidContext();


            //var FileUpdate = (from tPaperTemp in cnxt.tPaperLaidTemp
            //                  where tPaperTemp.PaperLaidId == model.PaperLaidId
            //               select tPaperTemp.PaperLaidId).Count();

            var FileUpdate = (from paperLaidTemp in cnxt.tPaperLaidTemp
                              where paperLaidTemp.PaperLaidId == model.PaperLaidId
                                  && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
                              select paperLaidTemp).FirstOrDefault();


            if (FileUpdate != null)
            {
                if (FileUpdate.Version != null)
                {
                    model.Count = Convert.ToInt32(FileUpdate.Version);
                }
                else
                {
                    model.Count = 1;
                }
            }
            else
            {
                int count = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
                int newVersion = count + 1;
                model.Count = newVersion;
            }


            if (FileUpdate != null)
            {
                if (FileUpdate.DocFileVersion != null)
                {
                    model.MainDocCount = Convert.ToInt32(FileUpdate.DocFileVersion);
                }
                else
                {
                    model.MainDocCount = 1;
                }
            }
            else
            {
                int MainDocCount = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
                int newVersionDoc = MainDocCount + 1;
                model.MainDocCount = newVersionDoc;
            }


            return model;



            //tPaperLaidV paper = param as tPaperLaidV;
            ////tPaperLaidV model = new tPaperLaidV();
            //using (var db = new BillPaperLaidContext())
            //{

            //    paper.Count = (from tPaperTemp in db.tPaperLaidTemp
            //                   where tPaperTemp.PaperLaidId == paper.PaperLaidId
            //                   select tPaperTemp.PaperLaidId).Count();

            //    paper.MainDocCount = (from tPaperTemp in db.tPaperLaidTemp
            //                   where tPaperTemp.PaperLaidId == paper.PaperLaidId
            //                   select tPaperTemp.PaperLaidId).Count();

            //    paper.FileVersion = paper.Count;

            //    paper.DocFileVersion = paper.MainDocCount;


            //}
            //return paper;
        }


        static object BillsSubmittPaperLaidEntry(object param)
        {
            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidV submitPaperEntry = param as tPaperLaidV;
            paperLaidCntxtDB.tPaperLaidV.Add(submitPaperEntry);
            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            // submitPaperEntry.PaperLaidId = 
            return submitPaperEntry;

        }

        static object SubmittPaperLaidEntry(object param)
        {

            if (null == param)
            {
                return null;
            }

            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidV submitPaperEntry = param as tPaperLaidV;

            if (submitPaperEntry.PaperLaidId == 0)
            {
                paperLaidCntxtDB.tPaperLaidV.Add(submitPaperEntry);
            }
            else
            {
                var ActiveDeptID = (from tPL in paperLaidCntxtDB.tPaperLaidV where tPL.PaperLaidId == submitPaperEntry.PaperLaidId select tPL.DeptActivePaperId).SingleOrDefault();
                if (ActiveDeptID != null)
                {
                    submitPaperEntry.DeptActivePaperId = ActiveDeptID;
                }
                paperLaidCntxtDB.tPaperLaidV.Attach(submitPaperEntry);
                paperLaidCntxtDB.Entry(submitPaperEntry).State = EntityState.Modified;
            }
            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperEntry;



            //BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            //tPaperLaidV submitPaperEntry = param as tPaperLaidV;
            //paperLaidCntxtDB.tPaperLaidV.Add(submitPaperEntry);
            //paperLaidCntxtDB.SaveChanges();
            //paperLaidCntxtDB.Close();
            //// submitPaperEntry.PaperLaidId = 
            //return submitPaperEntry;

        }

        //static object SubmittPaperLaidEntry(object param)
        //{
        //    if (null == param)
        //    {
        //        return null;
        //    }

        //    BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
        //    tPaperLaidV submitPaperEntry = param as tPaperLaidV;

        //    if (submitPaperEntry.PaperLaidId == 0)
        //    {
        //        paperLaidCntxtDB.tPaperLaidV.Add(submitPaperEntry);
        //    }
        //    else
        //    {
        //        paperLaidCntxtDB.tPaperLaidV.Attach(submitPaperEntry);
        //        paperLaidCntxtDB.Entry(submitPaperEntry).State = EntityState.Modified;
        //    }
        //    paperLaidCntxtDB.SaveChanges();
        //    paperLaidCntxtDB.Close();
        //    return submitPaperEntry;

        //}
        static object GetFileSaveDetails(object param)
        {

            tPaperLaidV model = param as tPaperLaidV;
            BillPaperLaidContext cnxt = new BillPaperLaidContext();

            var FileUpdate = (from paperLaidTemp in cnxt.tPaperLaidTemp
                              where paperLaidTemp.PaperLaidId == model.PaperLaidId
                                  && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
                              select paperLaidTemp).FirstOrDefault();


            if (FileUpdate != null)
            {
                if (FileUpdate.Version != null)
                {
                    model.Count = Convert.ToInt32(FileUpdate.Version);
                }
                else
                {
                    model.Count = 1;
                }
                model.FileName = FileUpdate.FileName;
                model.FilePath = FileUpdate.FilePath;
                model.DocFileName = FileUpdate.DocFileName;
            }
            else
            {
                int count = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
                int newVersion = count + 1;
                model.Count = newVersion;
            }


            return model;




            //tPaperLaidV model = param as tPaperLaidV;
            //BillPaperLaidContext cnxt = new BillPaperLaidContext();
            //int? catedoryId = (from mdl in cnxt.mEvent
            //                   where mdl.EventId == model.EventId
            //                   select mdl.PaperCategoryTypeId).SingleOrDefault();
            //model.RespectivePaper = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == catedoryId select mdl.Name).SingleOrDefault();
            //model.Count = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();

            //return model;
        }


        //static object GetFileSaveDetails(object param)
        //{
        //    tPaperLaidV model = param as tPaperLaidV;
        //    BillPaperLaidContext cnxt = new BillPaperLaidContext();
        //    int? catedoryId = (from mdl in cnxt.mEvent
        //                       where mdl.EventId == model.EventId
        //                       select mdl.PaperCategoryTypeId).SingleOrDefault();
        //    string respectivePaper = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == catedoryId select mdl.Name).SingleOrDefault();

        //    var FileUpdate = (from paperLaidTemp in cnxt.tPaperLaidTemp
        //                      where paperLaidTemp.PaperLaidId == model.PaperLaidId
        //                          && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
        //                      select paperLaidTemp).FirstOrDefault();
        //    if (FileUpdate != null)
        //    {
        //        model.Count = Convert.ToInt32(FileUpdate.Version);
        //    }
        //    else
        //    {
        //        int count = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
        //        int newVersion = count + 1;
        //        model.Count = newVersion;
        //    }

        //    model.RespectivePaper = respectivePaper;

        //    return model;
        //}

        static object SaveDetailsInTempPaperLaid(object param)
        {

            if (null == param)
            {
                return null;
            }
            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;

            var query = (from paperLaidTemp in paperLaidCntxtDB.tPaperLaidTemp
                         where paperLaidTemp.PaperLaidId == submitPaperTemp.PaperLaidId
                         && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
                         select paperLaidTemp).SingleOrDefault();

            if (query != null)
            {
                paperLaidCntxtDB.tPaperLaidTemp.Attach(query);
                query.FileName = submitPaperTemp.FileName;
                query.FilePath = submitPaperTemp.FilePath;
                query.DocFileName = submitPaperTemp.DocFileName;
               
                query.Version = submitPaperTemp.Version;
                query.DocFileVersion = submitPaperTemp.DocFileVersion;
                submitPaperTemp.PaperLaidTempId = query.PaperLaidTempId;
                 submitPaperTemp.DeptSubmittedDate = null;
                 var RefNumber1 = submitPaperTemp.AssemblyId + "/" + submitPaperTemp.SessionId + "/" + submitPaperTemp.PaperLaidId + "/" + "V" + submitPaperTemp.Version;
                 submitPaperTemp.evidhanReferenceNumber = RefNumber1;
                 query.evidhanReferenceNumber = submitPaperTemp.evidhanReferenceNumber;

            }
            else
            {
                submitPaperTemp.DeptSubmittedDate = null;
                var RefNumber = submitPaperTemp.AssemblyId + "/" + submitPaperTemp.SessionId + "/" + submitPaperTemp.PaperLaidId + "/" + "V" + submitPaperTemp.Version;
                submitPaperTemp.evidhanReferenceNumber = RefNumber;
                paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
            }

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;


            //if (null == param)
            //{
            //    return null;
            //}
            //BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            //tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
            ////submitPaperTemp.DeptSubmittedDate =  DateTime.Now;
            ////submitPaperTemp.DeptSubmittedBy = submitPaperTemp.DeptSubmittedBy;
            //paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
            //paperLaidCntxtDB.SaveChanges();
            //paperLaidCntxtDB.Close();
            //return submitPaperTemp;
        }

        //static object SaveDetailsInTempPaperLaid(object param)
        //{

        //    if (null == param)
        //    {
        //        return null;
        //    }
        //    BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
        //    tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;

        //    var query = (from paperLaidTemp in paperLaidCntxtDB.tPaperLaidTemp
        //                 where paperLaidTemp.PaperLaidId == submitPaperTemp.PaperLaidId
        //                 && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
        //                 select paperLaidTemp).SingleOrDefault();

        //    if (query != null)
        //    {
        //        paperLaidCntxtDB.tPaperLaidTemp.Attach(query);
        //        query.FileName = submitPaperTemp.FileName;
        //        query.FilePath = submitPaperTemp.FilePath;
        //    }
        //    else
        //    {
        //        submitPaperTemp.DeptSubmittedDate = null;
        //        paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
        //    }

        //    paperLaidCntxtDB.SaveChanges();
        //    paperLaidCntxtDB.Close();
        //    return submitPaperTemp;
        //}

        static object InsertBillDetails(object param)
        {

            if (null == param)
            { return null; }
            tBillRegister bills = new tBillRegister();
            tPaperLaidV mdl = param as tPaperLaidV;
            bills.PaperLaidId = (int)mdl.PaperLaidId;
            string[] formats = { "dd/MM/yyyy","dd-MM-yyyy", "dd/M/yyyy","dd-M-yyyy", "d/M/yyyy","d-M-yyyy", "d/MM/yyyy","d-MM-yyyy","MM/dd/yyyy","MM-dd-yyyy","MM/dd/yy","MM-dd-yy",
                    "dd/MM/yy","dd-MM-yy", "dd/M/yy","dd-M-yy","d/M/yy","d-M-yy","d/MM/yy","d-MM-yy"};
            var datetstring = mdl.BillDate.Split(' ')[0];
            bills.BillDate = string.IsNullOrEmpty(mdl.BillDate) ? (DateTime?)null : DateTime.ParseExact(datetstring, formats, CultureInfo.InvariantCulture, DateTimeStyles.None); //DateTime.ParseExact(obj.BillDate, "d", provider);
            // bills.BillDate = Convert.ToDateTime(mdl.BillDate);            
            bills.BillNumber = mdl.BOTStatus + " of " + mdl.BillNumberYear;
            bills.AssemblyId = mdl.AssemblyId;
            bills.SessionId = mdl.SessionId;
            bills.MinisterId = mdl.MinistryId;
            bills.DepartmentId = mdl.DepartmentId;
            bills.BillTypeId = mdl.BillTypeId;
            bills.IsBillAct = false;
            bills.IsBillReturn = false;
            bills.IsBillSigned = false;
            bills.IsCirculation = false;
            bills.IsClauseConsideration = false;
            bills.IsElicitingOpinion = false;
            bills.IsFinancialBillRule1 = false;
            bills.IsFinancialBillRule2 = false;
            bills.IsForwordedToMinister = false;
            bills.IsIntentioned = false;
            bills.IsIntentionedToMinister = false;
            bills.IsIntentionReturn = false;
            bills.IsIntentionSigned = false;
            bills.IsMoneyBill = false;
            bills.IsRecommendedArticle = false;
            bills.IsReferSelectCommitte = false;
            bills.IsSubmitted = false;
            bills.IsThirdReading = false;
            bills.IsWithdrawal = false;
            bills.ShortTitle = mdl.Description;
            bills.BillNo = mdl.BillNUmberValue;
            bills.BillYear = mdl.BillNumberYear;
            bills.BillName = mdl.Title;
            bills.IsAmendment = mdl.IsAmendment;
            bills.PreviousBillId = mdl.BillId;
            using (BillPaperLaidContext pCtxt = new BillPaperLaidContext())
            {
                bills = pCtxt.tBillRegister.Add(bills);
                pCtxt.SaveChanges();
                pCtxt.Close();
            }
            return bills;
        }

        static object UpdatePaperLaidEntry(object param)
        {
            tPaperLaidV updatedDetails = param as tPaperLaidV;
            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            var existingDetails = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == updatedDetails.PaperLaidId select model).SingleOrDefault();

            if (existingDetails != null)
            {
                existingDetails.EventId = updatedDetails.EventId;
                existingDetails.MinistryId = updatedDetails.MinistryId;
                         existingDetails.SessionId = updatedDetails.SessionId;
                existingDetails.PaperTypeID = updatedDetails.PaperTypeID;
                existingDetails.Title = updatedDetails.Title;
                existingDetails.Description = updatedDetails.Description;
                existingDetails.Remark = updatedDetails.Remark;
                existingDetails.PaperLaidId = updatedDetails.PaperLaidId;
               
                existingDetails.ProvisionUnderWhich = updatedDetails.ProvisionUnderWhich;
                existingDetails.DesireLayingDateId = updatedDetails.DesireLayingDateId;
                existingDetails.ModifiedWhen = updatedDetails.ModifiedWhen;
               
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }
            return existingDetails;
        }


        static object UpdatePaperLaidEntryForBilReplace(object param)
        {
            tPaperLaidV updatedDetails = param as tPaperLaidV;
            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            var existingDetails = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == updatedDetails.PaperLaidId select model).SingleOrDefault();

            if (existingDetails != null)
            {

                existingDetails.DeptActivePaperId = updatedDetails.DeptActivePaperId;
                existingDetails.ModifiedBy = updatedDetails.ModifiedBy;
                existingDetails.ModifiedWhen = updatedDetails.ModifiedWhen;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }
            return existingDetails;
        }

        static object GetSubmittedPaperLaidByID(object param)
        {
            BillPaperLaidContext db = new BillPaperLaidContext();

            tPaperLaidV model = param as tPaperLaidV;

            model.DepartmentSubmitList = (from tPaper in db.tPaperLaidV
                                          where model.PaperLaidId == tPaper.PaperLaidId
                                          join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                                          join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                                          join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                                          //join mSessionDates in db.mSessionDates on tPaper.DesireLayingDateId equals mSessionDates.Id
                                          //join mDocumentTypes in db.mDocumentTypes on tPaper.PaperTypeID equals mDocumentTypes.DocumentTypeID
                                          join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
                                          join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                                          select new PaperMovementModel
                                          {
                                              DeptSubmittedDate = tPaperTemp.DeptSubmittedDate,
                                              PaperTypeName = mPaperCategory.Name,
                                              actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
                                              FilePath = tPaperTemp.FilePath,
                                              DeptSubmittedBy = tPaperTemp.DeptSubmittedBy,
                                              PaperLaidId = model.PaperLaidId,
                                              DeptActivePaperId = tPaper.DeptActivePaperId,
                                              version = tPaperTemp.Version,
                                              Description = tPaper.Description,
                                              Title = tPaper.Title,
                                              ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                                              Remark = tPaper.Remark,
                                              MinistryName = tPaper.MinistryName,
                                              PaperLaidTempId = tPaperTemp.PaperLaidTempId


                                          }).ToList();
            //int i = 0;
            //foreach (var mod in model.DepartmentSubmitList)
            //{
            //    if (i == 0)
            //    {
            //        model.FilePath = mod.FilePath;
            //        model.PaperLaidId = mod.PaperLaidId;

            //        i++;
            //    }
            //}

            db.Close();
            tPaperLaidV data = new tPaperLaidV();
            data.PaperLaidId = model.PaperLaidId;
            data = ShowSentBillById(data);

            data.DepartmentSubmitList = model.DepartmentSubmitList;

            //data.FilePath = model.FilePath;

            return data;
        }


        //static object GetExisitngFileDetails(object param)
        //{

        //    if (null == param)
        //    {
        //        return null;
        //    }
        //    BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
        //    tPaperLaidV details = param as tPaperLaidV;

        //    var existingTPaperLaidTemp = (from md in paperLaidCntxtDB.tPaperLaidTemp
        //                                  where md.PaperLaidId == details.PaperLaidId && md.Version ==
        //                                      (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == details.PaperLaidId select bdMax.Version).Max()
        //                                  select md).SingleOrDefault();

        //    //var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidId == details.PaperLaidId select mdl).SingleOrDefault();
        //    if (existingTPaperLaidTemp != null)
        //    {

        //        return existingTPaperLaidTemp;
        //    }

        //    return null;
        //}


        static object GetExisitngFileDetails(object param)
        {

            if (null == param)
            {
                return null;
            }

            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidV details = param as tPaperLaidV;

            var existingTPaperLaidTemp = (from md in paperLaidCntxtDB.tPaperLaidTemp
                                          where md.PaperLaidId == details.PaperLaidId && md.Version ==
                                              (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == details.PaperLaidId select bdMax.Version).Max()
                                          select md).SingleOrDefault();

            //var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidId == details.PaperLaidId select mdl).SingleOrDefault();
            if (existingTPaperLaidTemp != null)
            {

                return existingTPaperLaidTemp;
            }

            return null;
        
        }


        static object GetPaperLaidTemp(object param)
        {

            if (null == param)
            {
                return null;
            }

            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidV details = param as tPaperLaidV;

            //var existingTPaperLaidTemp = (from md in paperLaidCntxtDB.tPaperLaidTemp
            //                              where md.PaperLaidId == details.PaperLaidId && md.Version ==
            //                                  (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == details.PaperLaidId select bdMax.Version).Max()
            //                              select md).SingleOrDefault();

            var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidId == details.PaperLaidId select mdl).SingleOrDefault();
            if (existingTPaperLaidTemp != null)
            {

                return existingTPaperLaidTemp;
            }

            return null;
        }

        static object GetPaperLaidVs(object param)
        {

            if (null == param)
            {
                return null;
            }

            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidV details = param as tPaperLaidV;

            var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidV select mdl).SingleOrDefault();
            if (existingTPaperLaidTemp != null)
            {

                return existingTPaperLaidTemp;
            }

            return null;
        }

        

        static object GetBillSentByID(object param)
        {
            PaperLaidContext db = new PaperLaidContext();

            tPaperLaidV model = param as tPaperLaidV;

            model.DepartmentSubmitList = (from tPaper in db.tPaperLaidV
                                          where model.PaperLaidId == tPaper.PaperLaidId
                                          join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                                          join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                                          join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                                          //join mSessionDates in db.mSessionDates on tPaper.DesireLayingDateId equals mSessionDates.Id
                                          //join mDocumentTypes in db.mDocumentTypes on tPaper.PaperTypeID equals mDocumentTypes.DocumentTypeID
                                          join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
                                          join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                                          select new PaperMovementModel
                                          {
                                              DeptSubmittedDate = tPaperTemp.DeptSubmittedDate,
                                              PaperTypeName = mPaperCategory.Name,
                                              actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
                                              FilePath = tPaperTemp.FilePath,
                                              DeptSubmittedBy = tPaperTemp.DeptSubmittedBy,
                                              PaperLaidId = model.PaperLaidId,
                                              PaperLaidTempId = tPaperTemp.PaperLaidTempId,
                                              DeptActivePaperId = tPaper.DeptActivePaperId,
                                              version = tPaperTemp.Version
                                          }).ToList();
            int i = 0;
            foreach (var mod in model.DepartmentSubmitList)
            {
                if (i == 0)
                {
                    model.FilePath = mod.FilePath;
                    model.PaperLaidId = mod.PaperLaidId;
                    model.PaperLaidTempId = mod.PaperLaidTempId;
                    i++;
                }
            }
            return model;
        }

        static object UpdateDetailsInTempPaperLaid(object param)
        {

            if (null == param)
            {
                return null;
            }
            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
            var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidTempId == submitPaperTemp.PaperLaidTempId select mdl).SingleOrDefault();
            existingTPaperLaidTemp.DeptSubmittedDate = submitPaperTemp.DeptSubmittedDate;
            existingTPaperLaidTemp.DeptSubmittedBy = submitPaperTemp.DeptSubmittedBy;
          
            if (existingTPaperLaidTemp != null)
            {
                existingTPaperLaidTemp.FileName = submitPaperTemp.FileName;
                existingTPaperLaidTemp.DocFileName = submitPaperTemp.DocFileName;
                existingTPaperLaidTemp.Version = submitPaperTemp.Version;
                //    existingTPaperLaidTemp.FilePath = submitPaperTemp.FilePath;
            }

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;
        }

        static object UpdateDetailsInTempPaperLaidNotices(object param)
        {

            if (null == param)
            {
                return null;
            }
            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
            var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidTempId == submitPaperTemp.PaperLaidId select mdl).SingleOrDefault();
           
            //existingTPaperLaidTemp.DeptSubmittedDate = submitPaperTemp.DeptSubmittedDate;
            //existingTPaperLaidTemp.DeptSubmittedBy = submitPaperTemp.DeptSubmittedBy;

            if (existingTPaperLaidTemp != null)
            {
                //existingTPaperLaidTemp.FileName = submitPaperTemp.FileName;
                existingTPaperLaidTemp.DocFileName = submitPaperTemp.DocFileName;
                //    existingTPaperLaidTemp.FilePath = submitPaperTemp.FilePath;
            }

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;
        }

        static object GetSessionDates(object param)
        {
            BillPaperLaidContext plCntxt = new BillPaperLaidContext();
            mSessionDate sessionDates = new mSessionDate();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            paperLaid.mSessionDates = (from mdl2 in plCntxt.mSessionDates where mdl2.SessionId == paperLaid.SessionId && mdl2.AssemblyId == paperLaid.AssemblyId select mdl2).ToList();
            foreach (var item in paperLaid.mSessionDates)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }
            mSessionDate obj = new mSessionDate();
            obj.Id = 0;
            obj.DisplayDate = "Desired Date to be Lay";
            paperLaid.mSessionDates.Add(obj);

            return paperLaid.mSessionDates;

        }

        static object GetDocumentType(object param)
        {
            BillPaperLaidContext plCntxt = new BillPaperLaidContext();
            // mDocumentType documents = new mDocumentType();
            //mSessionDate sessionDates = new mSessionDate();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            paperLaid.mDocumentTypes = (from mdl in plCntxt.mDocumentTypes select mdl).ToList();
            mDocumentType obj = new mDocumentType();
            obj.DocumentTypeID = 0;
            obj.DocumentType = "Paper Type";
            paperLaid.mDocumentTypes.Add(obj);
            return paperLaid.mDocumentTypes;

        }

        static object GetAmendmentBills(object param)
        {
            BillPaperLaidContext plCntxt = new BillPaperLaidContext();
            // mDocumentType documents = new mDocumentType();
            //mSessionDate sessionDates = new mSessionDate();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            paperLaid.tBillAmendment = (from mdl in plCntxt.tBillRegister where mdl.IsAmendment == false select mdl).ToList();
            tBillRegister obj = new tBillRegister();
            obj.BillId = 0;
            obj.BillName = "previous Bills";
            paperLaid.tBillAmendment.Add(obj);
            return paperLaid.tBillAmendment;

        }

        static List<mBillType> GetBillTypes(object param)
        {
            using (var cnxt = new BillPaperLaidContext())
            {
                var data = (from mdl in cnxt.mBillTypes select mdl).OrderByDescending(x => x.BillTypeId).ToList();
                return data;
            }

        }

        static object CheckBillNUmber(object param)
        {
            string billNumber = param.ToString();
            BillPaperLaidContext pCtxt = new BillPaperLaidContext();
            tBillRegister model = new tBillRegister();
            model = (from mdl in pCtxt.tBillRegister where mdl.BillNumber == billNumber select mdl).FirstOrDefault();
            return model;
        }

        #endregion
        /// <summary>
        /// //////added by dharmendra
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        #region add record in mbills
        private static object GetBillInfoByBillNo(object p)
        {

            BillPaperLaidContext pCtxt = new BillPaperLaidContext();
            mBills model = p as mBills;
            // mUserSubModules update = p as mUserSubModules;

            var isexist = (from bills in pCtxt.mBills where bills.BillNo == model.BillNo select bills).SingleOrDefault();
            return isexist;

        }
        private static object CheckBillNUmberExist(object p)
        {

            BillPaperLaidContext pCtxt = new BillPaperLaidContext();
            mBills model = p as mBills;
            // mUserSubModules update = p as mUserSubModules;

            var isexist = (from bills in pCtxt.mBills where bills.BillNo == model.BillNo select bills).Count() > 0;
            return isexist;

        }

        static void InsertmBillsRecord(object param)
        {


            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            mBills submittedDetials = param as mBills;
            paperLaidCntxtDB.mBills.Add(submittedDetials);
            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            // submitPaperEntry.PaperLaidId = 
            // return submitPaperEntry;

        }
        static void UpdatemBillsRecord(object param)
        {
            mBills updatedDetails = param as mBills;
            BillPaperLaidContext paperLaidCntxtDB = new BillPaperLaidContext();
            var existingDetails = (from model in paperLaidCntxtDB.mBills where model.BillNo == updatedDetails.BillNo select model).SingleOrDefault();

            if (existingDetails != null)
            {
                if (updatedDetails.Type == "Introduce")
                {
                    existingDetails.IntroductionDate = DateTime.Now;
                    existingDetails.BillTitle = updatedDetails.BillTitle;
                    existingDetails.IntroductionFilePath = updatedDetails.IntroductionFilePath;
                }
                else if (updatedDetails.Type == "Passed")
                {
                    existingDetails.PassingDate = DateTime.Now;
                    existingDetails.BillTitle = updatedDetails.BillTitle;
                    existingDetails.PassingFilePath = updatedDetails.PassingFilePath;
                }
                else if (updatedDetails.Type == "Assent")
                {
                    existingDetails.AssesntDate = DateTime.Now;
                    existingDetails.BillTitle = updatedDetails.BillTitle;
                    existingDetails.AccentedFilePath = updatedDetails.AccentedFilePath;
                }

                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }

        }
        static object GetEventNameByID(object param)
        {
            mEvent model = param as mEvent;
            PaperLaidContext cnxt = new PaperLaidContext();
            var query = (from evn in cnxt.mEvent where evn.EventId == model.EventId select evn);
            // mEvent eventt = new mEvent();
            //eventt = query;
            return query;
        }

        #endregion
    }
}
