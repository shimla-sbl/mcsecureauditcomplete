﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.DomainModel.Models.testingEmp;
using SBL.Service.Common;

namespace SBL.Domain.Context.TestEmployee
{
    public  class EmployeeContext : DBBase<EmployeeContext>
    {   public EmployeeContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
    public DbSet<Emptest> firstdb { get; set; }
    public DbSet<Emptest> seconddb { get; set; }
    public DbSet<Emptest> thirddb { get; set; }

   
        


    public static object Savedata(object param)
    {
        Emptest dataemp = param as Emptest;
        using (EmployeeContext context = new EmployeeContext())
        {
            try
            {
                context.seconddb.Add(dataemp);
                context.SaveChanges();
                context.Close();
            }
            catch { }
        }
        return null;
    }

    public static object Getist(object param)
    {
        using (EmployeeContext context = new EmployeeContext())
        {
            
            var query = (from a in context.firstdb
                         select new
                         {
                             
                             empName = a.EmployeeName,
                             adrs=a.Address
                         }).ToList();
            List<Emptest> lst = new List<Emptest>();
            foreach (var item in query)
            {
                Emptest mdl = new Emptest();
              
                mdl.EmployeeName = item.empName.ToString();
                mdl.Address = item.empName;

                lst.Add(mdl);
            }
            return lst;
        }
    }
 

    }
}
