﻿namespace SBL.Domain.Context.Employee
{
    using SBL.DAL;
    using SBL.DomainModel.ComplexModel.UserComplexModel;
    using SBL.DomainModel.Models.Employee;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Secretory;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Adhaar;
    using System.Data.Entity;
    using System.Linq;

    using System;
    using System.Collections.Generic;
    using SBL.Service.Common;
    using System.Data;
    using System.Text;
    using System.Threading.Tasks;

    public class EmployeeContext : DBBase<EmployeeContext>
    {
        public EmployeeContext() : base("eVidhan") { }
        public DbSet<mEmployee> mEmployees { get; set; }
        public DbSet<mMember> mMember { get; set; }
        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<mSecretory> mSecretory { get; set; }
        public DbSet<AuthorisedEmployee> AuthEmployee { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<AdhaarDetails> adharDetails { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetEmployeeDetialsByEmpID":
                    {
                        return GetEmployeeDetialsByEmpID(param.Parameter);
                    }
                case "GetEmployeeFullDetailsDetails":
                    {
                        return GetEmployeeFullDetailsDetails(param.Parameter);
                    }
                case "SubmitAuthorizedEmployeeDetails":
                    {
                        return SubmitAuthorizedEmployeeDetails(param.Parameter);
                    }
                case "SubmitAuthorizedUser":
                    {
                        return SubmitAuthorizedUser(param.Parameter);
                    }
                    
                case "GetAuthorizedEmployeeDetails":
                    {
                        return GetAuthorizedEmployeeDetails(param.Parameter);
                    }
                case "CreateEmployee": { CreateEmployee(param.Parameter); break; }

                case "UpdEmployee": { return UpdEmployee(param.Parameter); }
                case "DelEmployee": { return DelEmployee(param.Parameter); }
                //case "GetAllEmployee": { return GetAllEmployee(param.Parameter); }
                case "GetEmployeeById": { return GetEmployeeById(param.Parameter); }
                case "GetAllDepartment": { return GetAllDepartment(); }

                case "GetAllEmployeeIDs": { return GetAllEmployeeIDs(param.Parameter); }
            } return null;
        }
        public static object GetEmployeeDetialsByEmpID(object param)
        {
            EmployeeContext empCtxt = new EmployeeContext();
            mEmployee model = param as mEmployee;
            mUsers user = new mUsers();
            string id = model.empcd;
            var query = (from mdl in empCtxt.mEmployees where id == mdl.empcd && mdl.deptid == model.deptid select mdl).SingleOrDefault();

            return query;

        }
        private static object GetEmployeeFullDetailsDetails(object parameter)
        {
            mUsers userdata = (mUsers)parameter;


            using (var ctx = new EmployeeContext())
            {

                var userdetails = (from user in ctx.mUsers
                                   //join emp in ctx.mEmployees on new { c = user.UserName, d = user.DeptId } equals new { c = emp.empcd, d = emp.deptid }
                                   join sec in ctx.mSecretory on user.SecretoryId equals sec.SecretoryID
                                   where user.UserName == userdata.UserName && user.DeptId == userdata.DeptId && user.UserId==userdata.UserId
                                   select new mUserModelClass
                                   {

                                       EmpOrMemberCode = user.UserName,
                                       DeptId = user.DeptId,
                                       //Name = emp.empfname + " " + emp.empmname + " " + emp.emplname,
                                      // Address = emp.emphmtown,
                                       SecretoryName = sec.SecretoryName,
                                       DepartmentIDs = user.DepartmentIDs,
                                       Mobile = user.MobileNo,
                                       EmailId = user.EmailId,
                                       AdhaarID = user.AadarId,
                                      // FatherName = emp.empfmhname,
                                       Photo = user.Photo,
                                       //Gender = emp.empgender,
                                       DOB = user.DOB,
                                       UserId = user.UserId
                                   }).FirstOrDefault();
              var adharuser = (from adhar in ctx.adharDetails where adhar.AdhaarID == userdetails.AdhaarID select adhar).FirstOrDefault();
              userdetails.Name = adharuser.Name;
              userdetails.Address = adharuser.Address;
              userdetails.FatherName = adharuser.FatherName;
              userdetails.Gender = adharuser.Gender;
                if (userdetails.Gender == "F")
                {
                    userdetails.Name = "Smt. " + userdetails.Name;
                    userdetails.Gender = "Female";
                }
                else if (userdetails.Gender == "M")
                {
                    userdetails.Name = "Shri. " + userdetails.Name;
                }
                return userdetails;
            }
        }
        public static object SubmitAuthorizedEmployeeDetails(object param)
        {
            AuthorisedEmployee employee = (AuthorisedEmployee)param;
            EmployeeContext empCtxt = new EmployeeContext();
            var query = (from authEmployee in empCtxt.AuthEmployee where employee.UserId == authEmployee.UserId select authEmployee).FirstOrDefault();
            if (query != null)
            {
                query.IsPermissions = employee.IsPermissions;
                query.ModifiedWhen = employee.ModifiedWhen;
                query.AssociatedDepts = employee.AssociatedDepts;
                query.ModifiedBy = employee.ModifiedBy;
                if (employee.isAuthorized)
                {
                    query.isAuthorized = employee.isAuthorized;
                }
            }
            else
            {
                empCtxt.AuthEmployee.Add(employee);
            }
            empCtxt.SaveChanges();
            empCtxt.Close();

            return employee;
        }
        public static object SubmitAuthorizedUser(object param)
        {
            AuthorisedEmployee employee = (AuthorisedEmployee)param;
            EmployeeContext empCtxt = new EmployeeContext();
            var query = (from authEmployee in empCtxt.AuthEmployee where employee.UserId == authEmployee.UserId select authEmployee).FirstOrDefault();
            if (query != null)
            {
                query.IsPermissions = employee.IsPermissions;
                query.ModifiedWhen = employee.ModifiedWhen;
                query.AssociatedDepts = employee.AssociatedDepts;
                query.ModifiedBy = employee.ModifiedBy;
                query.EmpAadhaarID = employee.EmpAadhaarID;
                query.SecAadhaarID = employee.SecAadhaarID;
                query.SecretaryId = employee.SecretaryId;
                query.isAuthorized = employee.isAuthorized;
                query.IsActive = employee.IsActive;
                query.IsPermissions = true;
                query.EmployeeSelectedDepts = employee.EmployeeSelectedDepts;
                if (employee.isAuthorized)
                {
                    query.isAuthorized = employee.isAuthorized;
                }
            }
            else
            {
                empCtxt.AuthEmployee.Add(employee);
            }
            empCtxt.SaveChanges();
            empCtxt.Close();

            return employee;
        }
        public static object GetAuthorizedEmployeeDetails(object param)
        {
            AuthorisedEmployee employee = (AuthorisedEmployee)param;
            EmployeeContext empCtxt = new EmployeeContext();
            var query = (from authEmployee in empCtxt.AuthEmployee where employee.UserId == authEmployee.UserId select authEmployee).FirstOrDefault();
            if (query != null)
            {
                return query;
            }
            return null;
        }




        static void CreateEmployee(object param)
        {
            try
            {
                using (EmployeeContext db = new EmployeeContext())
                {
                    mEmployee model = param as mEmployee;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mEmployees.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdEmployee(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (EmployeeContext db = new EmployeeContext())
            {
                mEmployee model = param as mEmployee;
                model.CreationDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mEmployees.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllEmployeeIDs(param);
        }

        static object DelEmployee(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (EmployeeContext db = new EmployeeContext())
            {
                mEmployee parameter = param as mEmployee;
                mEmployee stateToRemove = db.mEmployees.SingleOrDefault(a => a.Id == parameter.Id);
                if(stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mEmployees.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllEmployeeIDs(param);
        }





        //public static object GetAllEmployee(object param)
        //{
        //    try
        //    {
        //        EmployeeContext db = new EmployeeContext();
        //        mEmployee model = param as mEmployee;
        //        var data = (from a in db.mEmployees
        //                    orderby a.Id descending
        //                    select a).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
        //        var data1 = (from a in db.mEmployees
        //                     orderby a.Id descending
        //                     select a).Count();
        //        int totalRecords = data1;
        //        model.ResultCount = totalRecords;
        //        var results = data.ToList();
        //        model.EmployeeList = results;
        //        return model;

        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }

        //}










        static mEmployee GetEmployeeById(object param)
        {
            mEmployee parameter = param as mEmployee;
            EmployeeContext db = new EmployeeContext();
            var query = db.mEmployees.SingleOrDefault(a => a.Id == parameter.Id);
            return query;
        }






        static List<mDepartment> GetAllDepartment()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mDepartments
                        orderby m.deptname ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;

        }






        static object GetAllEmployeeIDs(object param)
        {
            EmployeeContext db = new EmployeeContext();
            mEmployee model = param as mEmployee;
            var data = (from emp in db.mEmployees
                        join m in db.mDepartment on emp.deptid equals m.deptId into joindeptname
                        from n in joindeptname.DefaultIfEmpty()
                        //join district in db.Districts on consti.DistrictCode equals district.DistrictId into joinDistrictName
                        //from distr in joinDistrictName.DefaultIfEmpty()
                        where emp.IsDeleted==null
                        select new
                        {
                            emp,
                            n.deptname,
                            //distr.DistrictCode

                        }).ToList();

            List<mEmployee> list = new List<mEmployee>();
            foreach (var item in data)
            {
                item.emp.GetDepartmentName = item.deptname;
                //item.consti.GetDistrictName = item.DistrictCode;
                list.Add(item.emp);

            }
            var data1 = (from a in db.mEmployees
                         orderby a.Id descending
                         select a).Count();
            int totalRecords = data1;
            model.ResultCount = totalRecords;
            var results = list.OrderByDescending(e => e.Id).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            model.EmployeeList = results;
            return model;

        }









    }
}
