﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.Domain.Context.Passes;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Member;
using SBL.Service.Common;
using SBL.DomainModel.Models.Department;
using System.Data.Entity.Validation;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.Diaries;


namespace SBL.Domain.Context.RecipientGroups
{
	public class RecipientGroupsContext : DBBase<RecipientGroupsContext>
	{
		#region Constructor Block
		public RecipientGroupsContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
		#endregion

		#region Data Table Definition Block
		public DbSet<RecipientGroup> RecipientGroups { get; set; }
		public DbSet<RecipientGroupMember> RecipientGroupMembers { get; set; }
		public DbSet<AuthorisedEmployee> AuthorisedEmployee { get; set; }
		public DbSet<mUsers> mUsers { get; set; }
		public DbSet<SiteSettings> mSiteSettings { get; set; }
		public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
		public DbSet<SBL.DomainModel.Models.Constituency.mPanchayat> mPanchayat { get; set; }
		public DbSet<SBL.DomainModel.Models.Constituency.mConstituency> mConstituency { get; set; }
		public DbSet<SBL.DomainModel.Models.Office.mOffice> mOffice { get; set; }
		public DbSet<mMember> mMember { get; set; }
		public DbSet<SBL.DomainModel.Models.MemberSMSQuota.mMemberSMSQuotaAddOn> mMemberSMSQuotaAddOn { get; set; }
		public DbSet<SBL.DomainModel.Models.Office.tDepartmentOfficeMapped> tDepartmentOfficeMapped { get; set; }
		public DbSet<SBL.DomainModel.Models.Secretory.mSecretory> mSecretory { get; set; }
		public DbSet<SBL.DomainModel.Models.Secretory.mSecretoryDepartment> mSecretoryDepartment { get; set; }
		public DbSet<SBL.DomainModel.Models.UserModule.tUserAccessRequest> tUserAccessRequest { get; set; }
		public DbSet<SBL.DomainModel.Models.Adhaar.AdhaarDetails> AdhaarDetails { get; set; }
		public DbSet<SBL.DomainModel.Models.HOD.mHOD> mHOD { get; set; }
        public DbSet<mDepartment> objmDepartment { get; set; }
        public DbSet<SBL.DomainModel.Models.Grievance.tGrievanceOfficerDeleted> tGrievanceOfficerDeleted { get; set; }
      
		#endregion

		#region Execute Block.
		public static object Execute(ServiceParameter param)
		{
			if (null == param)
			{
				return null;
			}
			switch (param.Method)
			{
				#region Recipient Group
				case "CreateNewContactGroup": { return CreateNewContactGroup(param.Parameter); }
				case "UpdateContactGroup": { return UpdateContactGroup(param.Parameter); }
				case "GetAllContactGroups": { return GetAllContactGroups(param.Parameter); }
				case "GetContactGroupsByCretedBy": { return GetContactGroupsByCretedBy(param.Parameter); }
				case "GetContactGroupsByCretedByType": { return GetContactGroupsByCretedByType(param.Parameter); }
				case "GetContactGroupByID": { return GetContactGroupByID(param.Parameter); }
				case "DeleteContactGroup": { return DeleteContactGroup(param.Parameter); }
				case "GetContactGroupsByUser": { return GetContactGroupsByUser(param.Parameter); }
				case "GetAuthorizedEmployees": { return GetAuthorizedEmployees(param.Parameter); }
				case "GetContactGroupsByGroupIDs": { return GetContactGroupsByGroupIDs(param.Parameter); }
				case "GetAllDepartment": { return GetAllDepartment(param.Parameter); }
				case "GetAllContactGroupsByMember": { return GetAllContactGroupsByMember(param.Parameter); }
				case "GetContactGroupByMember": { return GetContactGroupByMember(param.Parameter); }
                case "GetAllOffice_ByDeptDistrict": { return GetAllOffice_ByDeptDistrict(param.Parameter); }                    
				#endregion

				#region Recipient Group Member
				case "CreateNewContactGroupMember": { return CreateNewContactGroupMember(param.Parameter); }
				case "CreateMultipleContactGroupMember": { return CreateMultipleContactGroupMember(param.Parameter); }
				case "UpdateContactGroupMember": { return UpdateContactGroupMember(param.Parameter); }
				case "GetContactGroupMembersByGroupID": { return GetContactGroupMembersByGroupID(param.Parameter); }
				case "GetContactGroupMemberByID": { return GetContactGroupMemberByID(param.Parameter); }
				case "DeleteContactGroupMember": { return DeleteContactGroupMember(param.Parameter); }
				case "GetContactMembersByGroupID": { return GetContactMembersByGroupID(param.Parameter); }

				// Add New
				case "GetAllConstituencyByMember": { return GetAllConstituencyByMember(param.Parameter); }
				case "GetAllConstituencyGroupsByMember": { return GetAllConstituencyGroupsByMember(param.Parameter); }
				case "GetConstituencyGroupByMember": { return GetConstituencyGroupByMember(param.Parameter); }
				case "GetConstituenxyGroupListByGroupID": { return GetConstituenxyGroupListByGroupID(param.Parameter); }
				case "GetConstituencyGroupMemberByMemberID": { return GetConstituencyGroupMemberByMemberID(param.Parameter); }
				case "GetAllPanchayat": { return GetAllPanchayat(param.Parameter); }
				case "GePanchayatGroupByMember": { return GePanchayatGroupByMember(param.Parameter); }
				case "GetAllPanchayatGroupsByMember": { return GetAllPanchayatGroupsByMember(param.Parameter); }
				case "GetPanchayatGroupMembersByGroupID": { return GetPanchayatGroupMembersByGroupID(param.Parameter); }
				case "GetPanchayatGroupMemberByID": { return GetPanchayatGroupMemberByID(param.Parameter); }

				//Send sms  
				case "GetAllGroupDetails": { return GetAllGroupDetails(param.Parameter); }
				case "GetGroupMemberByID": { return GetGroupMemberByID(param.Parameter); }
				case "GetConstituencyNameByCode": { return GetConstituencyNameByCode(param.Parameter); }

				//getallmembers for all groups  
				case "GetAllMembersForAllGroup": { return GetAllMembersForAllGroup(param.Parameter); }

				//Constituency Office Group  
				case "GetAllConstituencyOfficeGroupsByMember": { return GetAllConstituencyOfficeGroupsByMember(param.Parameter); }
				case "GetConstituencyOfficeGroupByMember": { return GetConstituencyOfficeGroupByMember(param.Parameter); }
				case "GetAllOffice": { return GetAllOffice(param.Parameter); }

				case "GetConstituencyOfficeGroupMembersByGroupID": { return GetConstituencyOfficeGroupMembersByGroupID(param.Parameter); }
				case "GetContactOfficeGroupMemberByID": { return GetContactOfficeGroupMemberByID(param.Parameter); }

				//sms quota by member  

				case "GetMAXLimitByMember": { return GetMAXLimitByMember(param.Parameter); }
				case "GetExtendSMSLimitByMember": { return GetExtendSMSLimitByMember(param.Parameter); }

				// Office Mapping with department   
				case "GetOfficeByDeptId": { return GetOfficeByDeptId(param.Parameter); }
				case "AddOffice": { return AddOffice(param.Parameter); }
				case "GetMappedOfficeByDeptId": { return GetMappedOfficeByDeptId(param.Parameter); }
				case "DeleteOffice": { return DeleteOffice(param.Parameter); }
				case "GetAllMappedOfficeByMemberCode": { return GetAllMappedOfficeByMemberCode(param.Parameter); }

				//added by dharmendra 
				case "CheckGroupMemberExist": { return CheckGroupMemberExist(param.Parameter); }
				case "UpdateContactGroupMemberFromUserReg": { return UpdateContactGroupMemberFromUserReg(param.Parameter); }
				case "GetContactGroupMembersByNewFixed": { return GetContactGroupMembersByNewFixed(param.Parameter); }


				case "GetOwnGroups": { return GetOwnGroups(param.Parameter); }

                // added by Shubham 
                case "GetContactGroupMembersByAadharId": { return GetContactGroupMembersByAadharId(param.Parameter); }
                case "GetAllDepartmentByDepartmentCode": { return GetAllDepartmentByDepartmentCode(param.Parameter); }
                case "GetContactGroupMembersByAadharIdAndMobileNumber": { return GetContactGroupMembersByAadharIdAndMobileNumber(param.Parameter); }

                //by robin
                case "GetContactGroupMembersByDeptInHC": { return GetContactGroupMembersByDeptInHC(param.Parameter); }
                case "GetAllOfficemapped_ByDeptDistrict": { return GetAllOfficemapped_ByDeptDistrict(param.Parameter); }
                case "GetAllDepartment_maaped": { return GetAllDepartment_maaped(param.Parameter); }
                case "GetAllOfficemapped_ByDeptDstForDialog": { return GetAllOfficemapped_ByDeptDstForDialog(param.Parameter); }
                case "GetAllOfficemappedForSearch": { return GetAllOfficemappedForSearch(param.Parameter); }
				#endregion
			}
			return null;
		}
		#endregion

		#region Method Definition & Body.

		#region Recipient Group Methods
		static object CreateNewContactGroup(object param)
		{
			try
			{
				using (RecipientGroupsContext _context = new RecipientGroupsContext())
				{
					RecipientGroup RecipientGroupToCreate = param as RecipientGroup;
					_context.RecipientGroups.Add(RecipientGroupToCreate);
					_context.SaveChanges();
					_context.Close();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			return null;
		}

		static object UpdateContactGroup(object param)
		{
			RecipientGroup RecipientGroupToUpdate = param as RecipientGroup;
			try
			{
				using (RecipientGroupsContext _context = new RecipientGroupsContext())
				{
					_context.RecipientGroups.Attach(RecipientGroupToUpdate);
					_context.Entry(RecipientGroupToUpdate).State = EntityState.Modified;
					_context.SaveChanges();
					_context.Close();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			return RecipientGroupToUpdate;
		}

		static object GetAllContactGroups(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsActive == true && recipientGroup.IsGeneric == true //&&  recipientGroup.CreatedBy == RecipientGroup.CreatedBy
							 select recipientGroup).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}


		static object GetContactGroupsByCretedBy(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsActive == true && recipientGroup.IsGeneric == true
							 && ((RecipientGroup.CreatedBy == null || RecipientGroup.CreatedBy == "") || RecipientGroup.CreatedBy == RecipientGroup.CreatedBy)
							 select recipientGroup);
				return query.ToList();
			}
		}

		static object GetContactGroupsByCretedByType(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsActive == true && recipientGroup.CreatedBy == RecipientGroup.CreatedBy
							 && recipientGroup.GroupTypeID == RecipientGroup.GroupTypeID
							 select recipientGroup);
				return query.ToList();
			}
		}

		static object GetContactGroupByID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsActive == true && recipientGroup.GroupID == RecipientGroup.GroupID
							 select recipientGroup).FirstOrDefault();
				return query;
			}
		}

		static object DeleteContactGroup(object param)
		{
			try
			{
				using (RecipientGroupsContext _context = new RecipientGroupsContext())
				{
					RecipientGroup recipientGroup = param as RecipientGroup;

					RecipientGroupMember recipientGroupMembers = new RecipientGroupMember();

					var groupMembersList = (from groupMembers in _context.RecipientGroupMembers
											where groupMembers.GroupID == recipientGroup.GroupID
											select groupMembers).ToList();

					if (_context.Entry(recipientGroup).State == EntityState.Detached)
						_context.RecipientGroups.Attach(recipientGroup);

					_context.RecipientGroups.Remove(recipientGroup);

					foreach (var item in groupMembersList)
					{
						if (_context.Entry(item).State == EntityState.Detached)
							_context.RecipientGroupMembers.Attach(item);

						_context.RecipientGroupMembers.Remove(item);
					}

					_context.SaveChanges();

					_context.Close();
				}

				return null;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}
		}

		static object GetContactGroupsByUser(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsActive == true && recipientGroup.CreatedBy == RecipientGroup.CreatedBy
							 select recipientGroup).FirstOrDefault();
				return query;
			}
		}

		static object GetAuthorizedEmployees(object param)
		{
			RecipientGroup RecipientGroup = param as RecipientGroup;
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				var UserInfo = (from model in context.AuthorisedEmployee
								join Login in context.mUsers on model.UserId equals Login.UserId
								where model.IsActive == true
								&& model.UserId == RecipientGroup.UserID
								select model).ToList();

				return UserInfo;
			}

			//tPaperLaidV mdl = param as tPaperLaidV;
			//PaperLaidContext paperLaidCntxtDB = new PaperLaidContext();

			//var UserInfo = (from model in paperLaidCntxtDB.AuthorisedEmployee
			//                join Login in paperLaidCntxtDB.mUsers on model.UserId equals Login.UserId
			//                where model.IsActive == true
			//                && model.UserId == mdl.UserID
			//                select model).ToList();

			//return UserInfo;
		}

		static object GetContactGroupsByGroupIDs(object param)
		{
			RecipientGroupMember multipleIDs = param as RecipientGroupMember;
			List<int> groupIds = new List<int>();
			string deptId = multipleIDs.DepartmentCode;



			string[] values = multipleIDs.AssociatedContactGroups.Split(',');
			for (int i = 0; i < values.Length; i++)
			{
				groupIds.Add(int.Parse(values[i]));
			}

			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				var query = (from recipientGroups in context.RecipientGroups
							 where recipientGroups.IsActive == true
							 && groupIds.Contains(recipientGroups.GroupID)
							 && ((deptId == null || deptId == "") || deptId == recipientGroups.DepartmentCode)
							 select recipientGroups);
				return query.ToList();
			}
		}

		static object GetAllDepartment(object param)
		{
			try
			{
				DepartmentPassesContext db = new DepartmentPassesContext();
				mDepartmentPasses model = (mDepartmentPasses)param;
				//string csv = model.DepartmentID;
				//IEnumerable<string> ids = csv.Split(',').Select(str => str);
				var result = (from mdl in db.PassDepartment select mdl).ToList();
				return result;
			}
			catch (Exception ex)
			{
			}
			return null;
		}

        static object GetAllDepartment_maaped(object param)
		{
			try
			{
			DepartmentPassesContext	 db = new DepartmentPassesContext();
				//mDepartmentPasses model = (mDepartmentPasses)param;
                 MlaDiary model =  param as MlaDiary;
				//string csv = model.DepartmentID;
				//IEnumerable<string> ids = csv.Split(',').Select(str => str);
				var result = (from mdl in db.PassDepartment  join tdm  in db.tDepartmentOfficeMapped on mdl.deptId equals tdm.DeptId 
                              where tdm.AssemblyCode==model.AssemblyId && tdm.MemberCode==model.MlaCode //select mdl).ToList();
                              orderby new { mdl.RowNumber, mdl.deptname }
                             
                            //select new mDepartment
                           // {
                            //   deptId = mdl.deptId,
                            //   deptname = mdl.deptname
                            //}
                              select mdl).Distinct().OrderBy(a => a.deptId).ToList();
				return result;
			}
			catch (Exception ex)
			{
			}
			return null;
		}
		static object GetAllContactGroupsByMember(object param)
		{

			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsGeneric == true
							 select recipientGroup).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}

		static object GetContactGroupByMember(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.GroupID == RecipientGroup.GroupID
							 select recipientGroup).FirstOrDefault();

				return query;
			}
		}

		#endregion

		#region Recipient Group Member Methods
		static object CreateNewContactGroupMember(object param)
		{
			try
			{
				using (RecipientGroupsContext _context = new RecipientGroupsContext())
				{
					RecipientGroupMember RecipientGroupMemberToCreate = param as RecipientGroupMember;
					_context.RecipientGroupMembers.Add(RecipientGroupMemberToCreate);
					_context.SaveChanges();
					_context.Close();
				}
			}
            catch (DbEntityValidationException ex)
			{
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
			}
			return null;
		}

		static object CreateMultipleContactGroupMember(object param)
		{
			try
			{
				using (RecipientGroupsContext _context = new RecipientGroupsContext())
				{
					List<RecipientGroupMember> RecipientGroupMembersToCreate = param as List<RecipientGroupMember>;
					foreach (var item in RecipientGroupMembersToCreate)
					{
						_context.RecipientGroupMembers.Add(item);
					}
					_context.SaveChanges();
					_context.Close();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			return null;
		}

		static object UpdateContactGroupMember(object param)
		{
			RecipientGroupMember RecipientGroupMemberToUpdate = param as RecipientGroupMember;
			try
			{
				using (RecipientGroupsContext _context = new RecipientGroupsContext())
				{
                    _context.RecipientGroupMembers.Attach(RecipientGroupMemberToUpdate);
                    _context.Entry(RecipientGroupMemberToUpdate).State = EntityState.Modified;
                    _context.SaveChanges();
                    _context.Close();

				}
			}
			catch (Exception ex)
			{
				throw;
			}
			return RecipientGroupMemberToUpdate;
		}

		static object GetContactGroupMembersByGroupID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where 
							 //recipientGroupMember.IsActive == true &&
							 recipientGroupMember.GroupID == RecipientGroup.GroupID
							 select recipientGroupMember).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}

        static object GetContactGroupMembersByAadharId(object param)
        {
            using (RecipientGroupsContext context = new RecipientGroupsContext())
            {
                RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

                var query = (from recipientGroupMember in context.RecipientGroupMembers
                             where  recipientGroupMember.AadharId == RecipientGroup.AadharId
                             select recipientGroupMember).FirstOrDefault();
                return query;
            }
        }

        static object GetContactGroupMembersByAadharIdAndMobileNumber(object param)
        {
            using (RecipientGroupsContext context = new RecipientGroupsContext())
            {
                RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

                var query = (from recipientGroupMember in context.RecipientGroupMembers
                             where recipientGroupMember.AadharId == RecipientGroup.AadharId && recipientGroupMember.GroupID==RecipientGroup.GroupID
                             select recipientGroupMember).FirstOrDefault();
                return query;
            }
        }

		static object GetContactGroupMembersByNewFixed(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && (recipientGroupMember.GroupID == 117 || recipientGroupMember.GroupID == 118) && recipientGroupMember.DepartmentCode.Contains(RecipientGroup.DepartmentCode)
							 select recipientGroupMember).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}

        static object GetContactGroupMembersByDeptInHC(object param)
        {
            using (RecipientGroupsContext context = new RecipientGroupsContext())
            {
                RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

                var query = (from recipientGroupMember in context.RecipientGroupMembers
                             where recipientGroupMember.IsActive == true && (recipientGroupMember.GroupID == 165) && recipientGroupMember.CommitteeId.Contains(RecipientGroup.CommitteeId)
                             select recipientGroupMember).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
                return query.ToList();
            }
        }


		static object GetContactGroupMemberByID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where  recipientGroupMember.GroupMemberID == RecipientGroup.GroupMemberID //&& recipientGroupMember.DepartmentCode != "0"
							 select recipientGroupMember).FirstOrDefault();
				return query;
			}
		}


		static object DeleteContactGroupMember(object param)
		{
			try
			{
				using (RecipientGroupsContext _context = new RecipientGroupsContext())
				{
					RecipientGroupMember recipientGroupMember = param as RecipientGroupMember;



					if (_context.Entry(recipientGroupMember).State == EntityState.Detached)
						_context.RecipientGroupMembers.Attach(recipientGroupMember);

					_context.RecipientGroupMembers.Remove(recipientGroupMember);

					_context.SaveChanges();

					_context.Close();



					//RecipientGroupMember RecipientGroupMember = param as RecipientGroupMember;
					//var contactGroupMemberToDelete = GetContactGroupByID(RecipientGroupMember) as RecipientGroupMember;

					//if (_context.Entry(contactGroupMemberToDelete).State == EntityState.Detached)
					//    _context.RecipientGroupMembers.Attach(contactGroupMemberToDelete);

					//_context.RecipientGroupMembers.Remove(contactGroupMemberToDelete);
					//_context.SaveChanges();
					//_context.Close();
				}
			}
			catch (Exception ex)
			{
				return null;
				throw;
			}
			return null;
		}

		static object GetContactMembersByGroupID(object param)
		{
			RecipientGroupMember multipleIDs = param as RecipientGroupMember;
			List<int> groupIds = new List<int>();

			string[] values = multipleIDs.AssociatedContactGroups.Split(',');
			for (int i = 0; i < values.Length; i++)
			{
				groupIds.Add(int.Parse(values[i]));
			}

			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && groupIds.Contains(recipientGroupMember.GroupID)
							 select recipientGroupMember);
				return query.ToList();
			}
		}

		#endregion

		#endregion

		#region Constituency Group & Group Members
		//Shashi GetConstuency List
		static object GetAllConstituencyByMember(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.ConstituencyCode == RecipientGroup.ConstituencyCode && recipientGroup.DepartmentCode == "0" && recipientGroup.PancayatCode == null && (recipientGroup.IsConstituencyOffice == false || recipientGroup.IsConstituencyOffice == null)
							 select recipientGroup).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}

		static object GetAllConstituencyGroupsByMember(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;
				List<RecipientGroup> result = new List<RecipientGroup>();

				var query = (from recipientGroup in context.RecipientGroups
							 join K in context.mConstituency on recipientGroup.ConstituencyCode equals K.ConstituencyID into K_join //on new { PanchayatCode = recipientGroup.PancayatCode } equals new { PanchayatCode = recipientGroup.PanchayatCode } into K_join
							 from K in K_join.DefaultIfEmpty()
							 where recipientGroup.ConstituencyCode == RecipientGroup.ConstituencyCode && recipientGroup.DepartmentCode == "0" && recipientGroup.PancayatCode == null && (recipientGroup.IsConstituencyOffice == false || recipientGroup.IsConstituencyOffice == null)
							 select new
							 {
								 RG = recipientGroup,
								 CON = K
							 }).OrderBy(a => a.RG.Name).ToList();

				if (query != null && query.Count() > 0)
				{
					foreach (var item in query)
					{
						RecipientGroup obj = new RecipientGroup();
						obj = item.RG;
						obj.ConstituencyName = item.RG.Name + " (" + item.CON.ConstituencyName + ")";
						result.Add(obj);
					}
				}
				return result;
			}
		}

		static object GetConstituencyGroupByMember(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.GroupID == RecipientGroup.GroupID && recipientGroup.DepartmentCode == "0"
							 select recipientGroup).FirstOrDefault();

				return query;
			}
		}

		static object GetConstituenxyGroupListByGroupID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && recipientGroupMember.GroupID == RecipientGroup.GroupID && recipientGroupMember.DepartmentCode == "0"
							 select recipientGroupMember).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}

		static object GetConstituencyGroupMemberByMemberID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && recipientGroupMember.GroupMemberID == RecipientGroup.GroupMemberID && recipientGroupMember.DepartmentCode == "0"
							 select recipientGroupMember).FirstOrDefault();
				return query;
			}
		}


		//Panchayat
		static object GetAllPanchayat(object param)
		{
			try
			{
				Panchayat.mPanchayatContext db = new Panchayat.mPanchayatContext();
				SBL.DomainModel.Models.Constituency.mPanchayat model = (SBL.DomainModel.Models.Constituency.mPanchayat)param;

				var result = (from mdl in db.mPanchayat
							  join cl in db.tConstituencyPanchayat on mdl.PanchayatCode equals cl.PanchayatCode
							  select mdl).ToList();
				return result;
			}
			catch (Exception ex)
			{
			}
			return null;
		}

		static object GePanchayatGroupByMember(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.GroupID == RecipientGroup.GroupID && recipientGroup.DepartmentCode == "0" && recipientGroup.PancayatCode != null
							 select recipientGroup).FirstOrDefault();

				return query;
			}
		}


		static object GetAllPanchayatGroupsByMember(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				List<RecipientGroup> result = new List<RecipientGroup>();

				var query = (from recipientGroup in context.RecipientGroups
							 join K in context.mPanchayat on recipientGroup.PancayatCode equals K.PanchayatCode into K_join //on new { PanchayatCode = recipientGroup.PancayatCode } equals new { PanchayatCode = recipientGroup.PanchayatCode } into K_join
							 from K in K_join.DefaultIfEmpty()
							 where recipientGroup.ConstituencyCode == RecipientGroup.ConstituencyCode && recipientGroup.DepartmentCode == "0" && recipientGroup.PancayatCode != null
							 select new
							 {
								 RG = recipientGroup,
								 PAN = K
							 }).OrderBy(a => a.RG.RankingOrder).ThenBy(a => a.RG.Name).ToList();
				if (query != null && query.Count() > 0)
				{
					foreach (var item in query)
					{
						RecipientGroup obj = new RecipientGroup();
						obj = item.RG;
						obj.PanchayatName = item.RG.Name + " (" + item.PAN.PanchayatName + ")";
						result.Add(obj);
					}
				}
				return result;
			}
		}

		static object GetPanchayatGroupMembersByGroupID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && recipientGroupMember.GroupID == RecipientGroup.GroupID && recipientGroupMember.DepartmentCode == "0"
							 select recipientGroupMember).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}

		static object GetPanchayatGroupMemberByID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && recipientGroupMember.GroupMemberID == RecipientGroup.GroupMemberID && recipientGroupMember.DepartmentCode == "0"
							 select recipientGroupMember).FirstOrDefault();
				return query;
			}
		}


		//Send sms
		static object GetAllGroupDetails(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsActive == true && recipientGroup.ConstituencyCode == RecipientGroup.ConstituencyCode || recipientGroup.IsGeneric == true
							 select recipientGroup);
				return query.ToList();
			}
		}
		static object GetGroupMemberByID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && recipientGroupMember.GroupID == RecipientGroup.GroupID
							 select recipientGroupMember).OrderBy(a => a.Name).ToList();
				return query;
			}
		}

		static object GetConstituencyNameByCode(object param)
		{


			using (Member.MemberContext context = new Member.MemberContext())
			{
				int a = Convert.ToInt32(param);

				var query = (from d in context.mConstituency
							 where d.ConstituencyID == a
							 select d.ConstituencyName).SingleOrDefault();
				string Cname = Convert.ToString(query);
				return Cname;
			}
		}

		static object GetAllMembersForAllGroup(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from members in context.RecipientGroupMembers
							 join groups in context.RecipientGroups on members.GroupID equals groups.GroupID into gorup_leftjoin
							 from groups in gorup_leftjoin.DefaultIfEmpty()
							 where members.IsActive == true && (groups.ConstituencyCode == RecipientGroup.ConstituencyCode || groups.IsGeneric == true)
							 select members).OrderBy(a => a.Name).ToList();
				return query;
			}

		}
		#endregion

		#region Constituency Office
		static object GetAllOffice(object param)
		{
			try
			{
				RecipientGroupsContext context = new RecipientGroupsContext();
				string deptcode = Convert.ToString(param);
                List<string> deptlist = new List<string>();
                if (!string.IsNullOrEmpty(deptcode))
                {
                    deptlist = deptcode.Split(',').Distinct().ToList();
                }
				if (deptcode.Trim().Length > 0)
				{
					var query = (from m in context.mOffice
								 where deptlist.Contains(m.deptid)
								 select m).OrderBy(a => a.officename).ToList();
					return query;
				}
                else
                {
                    var query = (from m in context.mOffice  where m.officename != null
                                 select m).OrderBy(a => a.officename).ToList();
                    return query;

                    

                }


			}
			catch (Exception ex)
			{
			}
			return null;
		}

        static object GetAllOffice_ByDeptDistrict(object param)
		
        {
			try
			{
				RecipientGroupsContext context = new RecipientGroupsContext();
                RecipientGroup model = param as RecipientGroup;
				string DistrictCode = Convert.ToString(param);
                List<string> dislist = new List<string>();
                if (!string.IsNullOrEmpty(model.DistId) && !string.IsNullOrEmpty(model.DeptId))
                {
                    int district = Convert.ToInt16(model.DistId);
                    var query = (from m in context.mOffice  where m.officename != null && m.distcd == district && m.deptid ==model.DeptId
                                 select m).OrderBy(a => a.officename).ToList();
                    return query;
                }                
			}
			catch (Exception ex)
			{
			}
			return null;
		}

        static object GetAllOfficemapped_ByDeptDistrict(object param)
		
        {
			try
			{
				RecipientGroupsContext context = new RecipientGroupsContext();
                RecipientGroup model = param as RecipientGroup;
                //string DistrictCode = Convert.ToString(param);
                //List<string> dislist = new List<string>();
                if (model.ConstituencyCodeForSubdivision == "0")
                {
                    if (!string.IsNullOrEmpty(model.DistId))   //&& !string.IsNullOrEmpty(model.DeptId)
                    {
                        int district = Convert.ToInt16(model.DistId);
						//var _query =(from  m in context.tDepartmentOfficeMapped
						//             join mo in context.mOffice on m.OfficeId equals mo.OfficeId
						//             where mo.officename != null && m.MemberCode == model.MemberCode //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
						//             select mo).OrderBy(a => a.officename).ToList();
						//var _query = (from mo in context.mOffice                                      
						//              join m in context.tDepartmentOfficeMapped on mo.OfficeId equals m.OfficeId
						//              //join tg in context.tGrievanceOfficerDeleted on m.OfficeId equals tg.OfficeId 
						//              join td in context.objmDepartment on m.DeptId equals td.deptId 
						//              where mo.officename != null && m.MemberCode == model.MemberCode //&& m.ConstituencyCode == model.ConstituencyCode
						//              && m.AssemblyCode == model.AssId //&& mo.distcd == district  //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
						//            //  select mo).OrderBy(a => a.officename).ToList();
						//              select new
						//              {
						//                  OfficeId = mo.OfficeId,
						//                  officename = mo.officename + " [ " + td.deptname + " ] " ,
						//              }).ToList().OrderBy(a => a.officename);


						var _query = (from mo in context.mOffice
									  join td in context.objmDepartment on mo.deptid equals td.deptId
									  select new
									  {
										  OfficeId = mo.OfficeId,
										  officename = mo.officename + " [ " + td.deptname + " ] ",
									  }).ToList().OrderBy(a => a.officename);


						List<mOffice> lst = new List<mOffice>();
                        foreach (var item in _query)
                        {
                            SBL.DomainModel.Models.Office.mOffice mdl = new SBL.DomainModel.Models.Office.mOffice();
                            mdl.OfficeId = item.OfficeId;
                            mdl.officename = item.officename;
                            lst.Add(mdl);
                        }

                        return lst;
                    }
                }
                else //SDM Case
                {
                    if (!string.IsNullOrEmpty(model.DistId))   //&& !string.IsNullOrEmpty(model.DeptId)
                    {
                        int district = Convert.ToInt16(model.DistId);
                        //var _query =(from  m in context.tDepartmentOfficeMapped
                        //             join mo in context.mOffice on m.OfficeId equals mo.OfficeId
                        //             where mo.officename != null && m.MemberCode == model.MemberCode //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
                        //             select mo).OrderBy(a => a.officename).ToList();
                        var _query = (from mo in context.mOffice
                                      join m in context.tDepartmentOfficeMapped on mo.OfficeId equals m.OfficeId
                                     // join tg in context.tGrievanceOfficerDeleted on m.OfficeId equals tg.OfficeId 
                                      join td in context.objmDepartment on m.DeptId equals td.deptId
                                      where mo.officename != null && m.MemberCode == model.MemberCode  // && m.ConstituencyCode == model.ConstituencyCode
                                      && m.AssemblyCode == model.AssId && mo.distcd == district  //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
                                     // select mo).OrderBy(a => a.officename).ToList();
                                      select new
                                      {
                                          OfficeId = mo.OfficeId,
                                          officename = mo.officename + " ( " + td.deptname + " ) ",
                                      }).ToList().OrderBy(a => a.officename);
                        List<SBL.DomainModel.Models.Office.mOffice> lst = new List<SBL.DomainModel.Models.Office.mOffice>();
                        foreach (var item in _query)
                        {
                            SBL.DomainModel.Models.Office.mOffice mdl = new SBL.DomainModel.Models.Office.mOffice();
                            mdl.OfficeId = item.OfficeId;
                            mdl.officename = item.officename;
                            lst.Add(mdl);
                        }
                        return lst;
                    }
                }
			}
			catch (Exception ex)
			{
			}
			return null;
		}
        static object GetAllOfficemappedForSearch(object param)
        {
            try
            {
                RecipientGroupsContext context = new RecipientGroupsContext();
                RecipientGroup model = param as RecipientGroup;

                if (model.ConstituencyCodeForSubdivision == "0")
                {
                    if (model.MemberCode == null)
                    {
                        model.MemberCode = 0;
                    }
                    if (!string.IsNullOrEmpty(model.DistId))   //&& !string.IsNullOrEmpty(model.DeptId)
                    {
                        if ( model.MemberCode != 0)
                        {
                            var query = (from ma in context.mMember
                                         // join ma in context.ObjMemAssem on a.MemberCode equals ma.MemberID
                                         //join a in context.tDepartmentOfficeMapped on ma.MemberCode equals a.MemberCode
                                         //  join a in context.tGrievanceOfficerDeleted on ma.MemberCode equals a.MemberCode
                                         join ms in context.mMemberAssembly on ma.MemberCode equals ms.MemberID
                                         where ms.AssemblyID == model.AssId && ma.MemberCode == model.MemberCode
                                         select new
                                         {
                                             MemberCode = ma.MemberCode,
                                             MemberName = ma.Name,
                                         }).ToList().OrderBy(ma => ma.MemberCode);
                            List<MlaDiary> lst = new List<MlaDiary>();
                            foreach (var item in query)
                            {
                                MlaDiary mdl = new MlaDiary();
                                mdl.MappedMemberCode = item.MemberCode.ToString();
                                // mdl.MappedMemberName = item.MemberName;
                                // lst.Add(mdl);
                                //}
                                // return lst;
                                // }

                                model.MemberCode = Convert.ToInt16(mdl.MappedMemberCode);

                                int district = Convert.ToInt16(model.DistId);
                                var _query = (from mo in context.mOffice
                                              join m in context.tDepartmentOfficeMapped on mo.OfficeId equals m.OfficeId
                                              join td in context.objmDepartment on m.DeptId equals td.deptId
                                              where mo.officename != null && m.MemberCode == model.MemberCode //&& m.ConstituencyCode == model.ConstituencyCode
                                              && m.AssemblyCode == model.AssId && mo.distcd == district  //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode

                                              select new
                                              {
                                                  OfficeId = mo.OfficeId,
                                                  officename = mo.officename + " [ " + td.deptname + " ] ",
                                              }).ToList().OrderBy(a => a.officename);
                                List<mOffice> lst1 = new List<mOffice>();
                                foreach (var item1 in _query)
                                {
                                    SBL.DomainModel.Models.Office.mOffice mdl1 = new SBL.DomainModel.Models.Office.mOffice();
                                    mdl1.OfficeId = item1.OfficeId;
                                    mdl1.officename = item1.officename;
                                    lst1.Add(mdl1);
                                }
                                return lst1;
                            }



                        }
                        else
                        {
                            var query = (from ma in context.mMember
                                         // join ma in context.ObjMemAssem on a.MemberCode equals ma.MemberID
                                         join a in context.tDepartmentOfficeMapped on ma.MemberCode equals a.MemberCode
                                         // join a in context.tGrievanceOfficerDeleted on ma.MemberCode equals a.MemberCode
                                         join ms in context.mMemberAssembly on ma.MemberCode equals ms.MemberID
                                         where a.OfficeId == model.OfficeId && ms.AssemblyID == model.AssId   //&& a.AdharId == mlaobj.AdhrId 
                                         select new
                                         {
                                             MemberCode = ma.MemberCode,
                                             MemberName = ma.Name,
                                         }).ToList().OrderBy(ma => ma.MemberCode);
                            List<MlaDiary> lst = new List<MlaDiary>();
                            foreach (var item in query)
                            {
                                MlaDiary mdl = new MlaDiary();
                                mdl.MappedMemberCode = item.MemberCode.ToString();
                                //  mdl.MappedMemberName = item.MemberName;
                                //lst.Add(mdl);
                                // }
                                // return lst;

                                model.MemberCode = Convert.ToInt16(mdl.MappedMemberCode);

                                int district = Convert.ToInt16(model.DistId);
                                var _query = (from mo in context.mOffice
                                              join m in context.tDepartmentOfficeMapped on mo.OfficeId equals m.OfficeId
                                              join td in context.objmDepartment on m.DeptId equals td.deptId
                                              where mo.officename != null && m.MemberCode == model.MemberCode //&& m.ConstituencyCode == model.ConstituencyCode
                                              && m.AssemblyCode == model.AssId && mo.distcd == district  //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode

                                              select new
                                              {
                                                  OfficeId = mo.OfficeId,
                                                  officename = mo.officename + " [ " + td.deptname + " ] ",
                                              }).ToList().OrderBy(a => a.officename);
                                List<mOffice> lst1 = new List<mOffice>();
                                foreach (var item1 in _query)
                                {
                                    SBL.DomainModel.Models.Office.mOffice mdl1 = new SBL.DomainModel.Models.Office.mOffice();
                                    mdl1.OfficeId = item1.OfficeId;
                                    mdl1.officename = item1.officename;
                                    lst1.Add(mdl1);
                                }
                                return lst1;
                            }

                         
                        }
                    }
                }
                else //SDM Case
                {
                    if (!string.IsNullOrEmpty(model.DistId))   //&& !string.IsNullOrEmpty(model.DeptId)
                    {
                        var query = (from ma in context.mMember
                                     // join ma in context.ObjMemAssem on a.MemberCode equals ma.MemberID
                                     join a in context.tDepartmentOfficeMapped on ma.MemberCode equals a.MemberCode
                                     // join a in context.tGrievanceOfficerDeleted on ma.MemberCode equals a.MemberCode
                                     join ms in context.mMemberAssembly on ma.MemberCode equals ms.MemberID
                                     where a.OfficeId == model.OfficeId && ms.AssemblyID == model.AssId   //&& a.AdharId == mlaobj.AdhrId 
                                     select new
                                     {
                                         MemberCode = ma.MemberCode,
                                         MemberName = ma.Name,
                                     }).ToList().OrderBy(ma => ma.MemberCode);
                        List<MlaDiary> lst = new List<MlaDiary>();
                        foreach (var item in query)
                        {
                            MlaDiary mdl = new MlaDiary();
                            mdl.MappedMemberCode = item.MemberCode.ToString();
                            //   mdl.MappedMemberName = item.MemberName;
                            // lst.Add(mdl);
                            // }
                            model.MemberCode = Convert.ToInt16(mdl.MappedMemberCode);
                            int district = Convert.ToInt16(model.DistId);
                            var _query = (from mo in context.mOffice
                                          join m in context.tDepartmentOfficeMapped on mo.OfficeId equals m.OfficeId
                                          // join tg in context.tGrievanceOfficerDeleted on m.OfficeId equals tg.OfficeId 
                                          join td in context.objmDepartment on m.DeptId equals td.deptId
                                          where mo.officename != null && m.MemberCode == model.MemberCode  // && m.ConstituencyCode == model.ConstituencyCode
                                          && m.AssemblyCode == model.AssId && mo.distcd == district  //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
                                          // select mo).OrderBy(a => a.officename).ToList();
                                          select new
                                          {
                                              OfficeId = mo.OfficeId,
                                              officename = mo.officename + " ( " + td.deptname + " ) ",
                                          }).ToList().OrderBy(a => a.officename);
                            List<SBL.DomainModel.Models.Office.mOffice> lst1 = new List<SBL.DomainModel.Models.Office.mOffice>();
                            foreach (var item1 in _query)
                            {
                                SBL.DomainModel.Models.Office.mOffice mdl1 = new SBL.DomainModel.Models.Office.mOffice();
                                mdl1.OfficeId = item1.OfficeId;
                                mdl1.officename = item1.officename;
                                lst1.Add(mdl1);
                            }
                            return lst1;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static object GetAllOfficemapped_ByDeptDstForDialog(object param)
        {
            try
            {
                RecipientGroupsContext context = new RecipientGroupsContext();
                RecipientGroup model = param as RecipientGroup;
                string DistrictCode = Convert.ToString(param);
                List<string> dislist = new List<string>();
                if (model.ConstituencyCodeForSubdivision == "0")
                {
                    if (!string.IsNullOrEmpty(model.DistId))   //&& !string.IsNullOrEmpty(model.DeptId)
                    {
                        int district = Convert.ToInt16(model.DistId);
                        //var _query =(from  m in context.tDepartmentOfficeMapped
                        //             join mo in context.mOffice on m.OfficeId equals mo.OfficeId
                        //             where mo.officename != null && m.MemberCode == model.MemberCode //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
                        //             select mo).OrderBy(a => a.officename).ToList();
                        var _query = (from mo in context.mOffice
                                      join m in context.tDepartmentOfficeMapped on mo.OfficeId equals m.OfficeId
                                      //join tg in context.tGrievanceOfficerDeleted on m.OfficeId equals tg.OfficeId 
                                      join td in context.objmDepartment on m.DeptId equals td.deptId
                                      where mo.officename != null && m.MemberCode == model.MemberCode //&& m.ConstituencyCode == model.ConstituencyCode
                                      && m.AssemblyCode == model.AssId && mo.distcd == district  //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
                                      //  select mo).OrderBy(a => a.officename).ToList();
                                      select new fillListGenric1
                                      {
                                          value = mo.OfficeId.ToString(),
                                          Text = mo.officename + " [ " + td.deptname + " ] "
                                      }
                            ).Distinct().ToList();
                        //              select new
                        //              {
                        //                  OfficeId = mo.OfficeId,
                        //                  officename = mo.officename + " [ " + td.deptname + " ] ",
                        //              }).ToList().OrderBy(a => a.officename);
                        //List<mOffice> lst = new List<mOffice>();
                        //foreach (var item in _query)
                        //{
                        //    SBL.DomainModel.Models.Office.mOffice mdl = new SBL.DomainModel.Models.Office.mOffice();
                        //    mdl.OfficeId = item.OfficeId;
                        //    mdl.officename = item.officename;
                        //    lst.Add(mdl);
                        //}

                        return _query;
                    }
                }
                else //SDM Case
                {
                    if (!string.IsNullOrEmpty(model.DistId))   //&& !string.IsNullOrEmpty(model.DeptId)
                    {
                        int district = Convert.ToInt16(model.DistId);
                        //var _query =(from  m in context.tDepartmentOfficeMapped
                        //             join mo in context.mOffice on m.OfficeId equals mo.OfficeId
                        //             where mo.officename != null && m.MemberCode == model.MemberCode //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
                        //             select mo).OrderBy(a => a.officename).ToList();
                        var _query = (from mo in context.mOffice
                                      join m in context.tDepartmentOfficeMapped on mo.OfficeId equals m.OfficeId
                                      // join tg in context.tGrievanceOfficerDeleted on m.OfficeId equals tg.OfficeId 
                                      join td in context.objmDepartment on m.DeptId equals td.deptId
                                      where mo.officename != null && m.MemberCode == model.MemberCode  // && m.ConstituencyCode == model.ConstituencyCode
                                      && m.AssemblyCode == model.AssId && mo.distcd == district  //&& mo.distcd == district && m.DeptId == model.DeptId && m.MemberCode ==model.MemberCode
                                      // select mo).OrderBy(a => a.officename).ToList();
                                      select new fillListGenric1
                                      {
                                          value = mo.OfficeId.ToString(),
                                          Text = mo.officename + " [ " + td.deptname + " ] "
                                      }
                            ).Distinct().ToList();
                        //              select new
                        //              {
                        //                  OfficeId = mo.OfficeId,
                        //                  officename = mo.officename + " ( " + td.deptname + " ) ",
                        //              }).ToList().OrderBy(a => a.officename);
                        //List<SBL.DomainModel.Models.Office.mOffice> lst = new List<SBL.DomainModel.Models.Office.mOffice>();
                        //foreach (var item in _query)
                        //{
                        //    SBL.DomainModel.Models.Office.mOffice mdl = new SBL.DomainModel.Models.Office.mOffice();
                        //    mdl.OfficeId = item.OfficeId;
                        //    mdl.officename = item.officename;
                        //    lst.Add(mdl);
                        //}
                        return _query;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
     

		static object GetAllConstituencyOfficeGroupsByMember(object param)
		{

			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsActive == true && recipientGroup.DepartmentCode == "0" && recipientGroup.OfficeId == 0 && recipientGroup.IsConstituencyOffice == true
							 select recipientGroup).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}
		static object GetConstituencyOfficeGroupByMember(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.GroupID == RecipientGroup.GroupID && recipientGroup.DepartmentCode == "0" && recipientGroup.OfficeId == 0 && recipientGroup.IsConstituencyOffice == true
							 select recipientGroup).FirstOrDefault();

				return query;
			}
		}

		static object GetConstituencyOfficeGroupMembersByGroupID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && recipientGroupMember.GroupID == RecipientGroup.GroupID
							 select recipientGroupMember).OrderBy(a => a.RankingOrder).ThenBy(a => a.Name);
				return query.ToList();
			}
		}
		static object GetContactOfficeGroupMemberByID(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroupMember RecipientGroup = param as RecipientGroupMember;

				var query = (from recipientGroupMember in context.RecipientGroupMembers
							 where recipientGroupMember.IsActive == true && recipientGroupMember.GroupMemberID == RecipientGroup.GroupMemberID && recipientGroupMember.DepartmentCode != "0"
							 select recipientGroupMember).FirstOrDefault();
				return query;
			}
		}

		#endregion

		#region SMS Quota

		//Get Monthly SMS Quota of A Member
		static object GetMAXLimitByMember(object param)
		    {
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				mMember Member = param as mMember;
				var query = (from mem in context.mMember
							 where mem.AadhaarNo == Member.AadhaarNo
							 select new
							 {
								 SMSQuota = mem.MonthlySMSQuota
							 }
							   ).FirstOrDefault();

				if (Member.isAlive == true)
				{
					query = (from mem in context.mMember
							 where mem.AadhaarNo == Member.AadhaarNo
							 select new
							 {
								 SMSQuota = mem.MonthlySMSQuota
							 }
							   ).FirstOrDefault();
				}
				else
				{
					query = (from mem in context.mUsers
							 where mem.AadarId == Member.AadhaarNo
							 select new
							 {
								 SMSQuota = mem.MonthlySMSQuota
							 }
								).FirstOrDefault();
				}
				var smsquota = query.SMSQuota;
				return smsquota;


			}
		}
		static object GetExtendSMSLimitByMember(object param)
		{
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				mMember Member = param as mMember;
				var query = (from mem in context.mMemberSMSQuotaAddOn
							 where mem.MemberCode == Member.MemberCode && mem.AddonDate.Value.Month == Member.DistrictID
							 group mem by new { mem.MemberCode, mem.AddonDate.Value.Month } into g
							 select new
							 {
								 AddonSMSQuota = g.Sum(a => a.AddonValue)
							 }
							 ).FirstOrDefault();
				var smsquota = 0;
				if (query != null)
					smsquota = query.AddonSMSQuota;

				return smsquota;
			}




		}

		#endregion

		#region Office Mapping with Department

		static object GetOfficeByDeptId(object param)
		{
			string DeptID = Convert.ToString(param);
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{

				var query = (from moff in context.mOffice
							 where moff.deptid == DeptID
							 select moff
								 ).OrderBy(a => a.officename).ToList();

				return query;
			}
		}

		private static object AddOffice(object param)
		{
			SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model = param as SBL.DomainModel.Models.Office.tDepartmentOfficeMapped;

			string obj = model.Ids;
			string[] a = obj.Split(',');


			RecipientGroupsContext db = new RecipientGroupsContext();


			foreach (var item in a)
			{
				int Id = Convert.ToInt32(item);
				SBL.DomainModel.Models.Office.mOffice office = new DomainModel.Models.Office.mOffice();

				var res = (from Aobj in db.tDepartmentOfficeMapped
						   where Aobj.OfficeId == Id && Aobj.DeptId == model.DeptId && Aobj.ConstituencyCode==model.ConstituencyCode
						   select a).Count();
				if (res == 0)
				{

					SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model1 = new DomainModel.Models.Office.tDepartmentOfficeMapped();

					model1.OfficeId = Id;
					model1.AssemblyCode = model.AssemblyCode;
					model1.ConstituencyCode = model.ConstituencyCode;
					model1.DeptId = model.DeptId;
					model1.MemberCode = model.MemberCode;
					db.tDepartmentOfficeMapped.Add(model1);
					db.SaveChanges();

					//update Status IsMapping
					office = db.mOffice.Single(b => b.OfficeId == Id);
					office.IsMapped = true;
					db.SaveChanges();
				}
			}

			return model;
		}
		private static object DeleteOffice(object param)
		{
			SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model = param as SBL.DomainModel.Models.Office.tDepartmentOfficeMapped;

			string obj = model.Ids;
			string[] a = obj.Split(',');


			RecipientGroupsContext db = new RecipientGroupsContext();


			foreach (var item in a)
			{
                int Id = 0;
                if (item.Contains("Chk_"))
                {
                    var s = item.Remove(0, 4);
                     Id = Convert.ToInt32(s);
                }
                else
                {
                     Id = Convert.ToInt32(item);
                }
				SBL.DomainModel.Models.Office.mOffice office = new DomainModel.Models.Office.mOffice();
				SBL.DomainModel.Models.Office.tDepartmentOfficeMapped model1 = new DomainModel.Models.Office.tDepartmentOfficeMapped();

                model1 = db.tDepartmentOfficeMapped.Single(c => c.OfficeId == Id && c.ConstituencyCode == model.ConstituencyCode);
				db.tDepartmentOfficeMapped.Remove(model1);
				db.SaveChanges();

				//update Status IsMapping
				office = db.mOffice.Single(b => b.OfficeId == Id);
				office.IsMapped = false;
				db.SaveChanges();
			}

			return model;
		}

		static object GetMappedOfficeByDeptId(object param)
		{
			//string DeptID = Convert.ToString(param);

			SBL.DomainModel.Models.Office.tDepartmentOfficeMapped mdl = param as SBL.DomainModel.Models.Office.tDepartmentOfficeMapped;
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{

				var query = (from moff in context.mOffice
							 join a in context.tDepartmentOfficeMapped on moff.OfficeId equals a.OfficeId
							 where a.DeptId == mdl.DeptId && a.MemberCode == mdl.MemberCode && moff.IsMapped == true
							 select moff
								 ).OrderBy(a => a.officename).ToList();



				return query;
			}
		}
		static object GetAllMappedOfficeByMemberCode(object param)
		{
			//string DeptID = Convert.ToString(param);

			SBL.DomainModel.Models.Office.tDepartmentOfficeMapped mdl = param as SBL.DomainModel.Models.Office.tDepartmentOfficeMapped;
			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{

                //var query = (from moff in context.mOffice
                //             join a in context.tDepartmentOfficeMapped on moff.OfficeId equals a.OfficeId
                //             where a.MemberCode == mdl.MemberCode && moff.IsMapped == true
                //             select moff
                //                 ).OrderBy(a => a.officename).ToList();





				var query = (from moff in context.mOffice
							 
							 select moff
								 ).OrderBy(a => a.officename).ToList();


				return query;
			}
		}

        static object GetAllDepartmentByDepartmentCode(object param)
        {
            string[] StringArray = param.ToString().Split(',');
            StringArray = StringArray.Where(x => !string.IsNullOrEmpty(x)).ToArray();

           
            using (RecipientGroupsContext context = new RecipientGroupsContext())
            {

                var query = (from dept in context.objmDepartment
                             where (StringArray).Contains(dept.deptId)
                             select dept).ToList();
                var deptname = string.Join(",", query.Select(p => p.deptname));

                return deptname;
            }
        }
		#endregion

		#region Update Records from mUsers
		static object CheckGroupMemberExist(object p)
		{
			RecipientGroupMember RecipientGroup = p as RecipientGroupMember;
			using (var ctx = new RecipientGroupsContext())
			{
				var isexist = (from rc in ctx.RecipientGroupMembers where rc.AadharId == RecipientGroup.AadharId select rc).Count() > 0;

				return isexist;
			}
		}
		static object UpdateContactGroupMemberFromUserReg(object param)
		{
			RecipientGroupMember RecipientGroupMemberToUpdate = param as RecipientGroupMember;
			try
			{
				using (RecipientGroupsContext _context = new RecipientGroupsContext())
				{
                    var query = (from mdl in _context.RecipientGroupMembers where mdl.AadharId == RecipientGroupMemberToUpdate.AadharId select mdl).FirstOrDefault();
					//_context.Configuration.ValidateOnSaveEnabled = false;
					//_context.SaveChanges();
					query.GroupID = RecipientGroupMemberToUpdate.GroupID;
					query.Name = RecipientGroupMemberToUpdate.Name;
					query.Gender = RecipientGroupMemberToUpdate.Gender;
					query.Designation = RecipientGroupMemberToUpdate.Designation;
					query.Email = RecipientGroupMemberToUpdate.Email;
					query.MobileNo = RecipientGroupMemberToUpdate.MobileNo;
					query.Address = RecipientGroupMemberToUpdate.Address;
					query.DepartmentCode = RecipientGroupMemberToUpdate.DepartmentCode;
					query.RankingOrder = RecipientGroupMemberToUpdate.RankingOrder;
					query.IsActive = true;

					_context.RecipientGroupMembers.Attach(query);
					_context.Entry(query).State = EntityState.Modified;
					_context.SaveChanges();
					_context.Close();
				}
			}
			catch (Exception ex)
			{
				throw;
			}
			return RecipientGroupMemberToUpdate;
		}

		static object GetOwnGroups(object param)
		{

			using (RecipientGroupsContext context = new RecipientGroupsContext())
			{
				RecipientGroup RecipientGroup = param as RecipientGroup;

				var query = (from recipientGroup in context.RecipientGroups
							 where recipientGroup.IsPublic == false && recipientGroup.IsActive == false && recipientGroup.CreatedBy == RecipientGroup.CreatedBy
							 select recipientGroup).OrderBy(a => a.Name);
				return query.ToList();
			}
		}
		#endregion
	}
}
