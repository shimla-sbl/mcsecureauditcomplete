﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using SBL.DAL;
using System.Data.Entity;
using SBL.DomainModel.Models.PublishDocument;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Employee;

namespace SBL.Domain.Context.PublishDocument
{
    public class PublishDocumentContext : DBBase<PublishDocumentContext>
    {
        public PublishDocumentContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<PublishDocumentDetails> masterDocument { get; set; }
        public DbSet<AuditPublishDocument> auditDocument { get; set; }

        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<AuthorisedEmployee> AuthorisedEmployee { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetPublishDocumentList":
                    {
                        return GetPublishDocumentList(param.Parameter);
                    }
                case "AddPublishDocument":
                    {
                        return AddPublishDocument(param.Parameter);
                    }
                case "GetDocumentById":
                    {
                        return GetDocumentById(param.Parameter);
                    }
                case "UpdatePublishDocument":
                    {
                        return UpdatePublishDocument(param.Parameter);
                    }
                case "DeletePublisDocument":
                    {
                        return DeletePublisDocument(param.Parameter);
                    }
                case "GetMaxPublishID":
                    {
                        return GetMaxPublishID(param.Parameter);
                    }
                case "GetAuthorizedEmployees":
                    {
                        return GetAuthorizedEmployees(param.Parameter);
                    }
                case "GetOfficeIDByDpIdandPublisID":
                    {
                        return GetOfficeIDByDpIdandPublisID(param.Parameter);
                    }
                case "AddauditPublishDocument":
                    {
                        return AddauditPublishDocument(param.Parameter);
                    }
                case "GetMaxAuditPublishID":
                    {
                        return GetMaxAuditPublishID(param.Parameter);
                    }
                case "GetOfficeandDocumentCount":
                    {
                        return GetOfficeandDocumentCount(param.Parameter);
                    }

            }
            return null;
        }
        static object GetPublishDocumentList(object param)
        {
            try
            {
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {
                    PublishDocumentDetails model = param as PublishDocumentDetails;
                    // if (model.IsUserType == "5")
                    // {
                    var result = (from publish in _context.masterDocument
                                  orderby publish.PublishDocumentID descending
                                  select publish).ToList();

                    return result;
                    //}
                    //else
                    //{
                    //    var result = (from publish in _context.masterDocument where publish.IsUserId == model.IsUserId 
                    //                  orderby publish.PublishDocumentID descending
                    //                  select publish).ToList();
                    //    return result;
                    //}
                }

            }
            catch (Exception ee)
            {
                throw ee;
            }
            return null;
        }
        static object AddPublishDocument(object param)
        {
            try
            {
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {
                    PublishDocumentDetails PublishDocumentCreate = param as PublishDocumentDetails;
                    _context.masterDocument.Add(PublishDocumentCreate);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object AddauditPublishDocument(object param)
        {
            try
            {
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {
                    AuditPublishDocument PublishDocumentCreate = param as AuditPublishDocument;
                    _context.auditDocument.Add(PublishDocumentCreate);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
        public static object GetDocumentById(object param)
        {
            PublishDocumentDetails parameter = param as PublishDocumentDetails;

            PublishDocumentContext db = new PublishDocumentContext();

            var data = (from Document in db.masterDocument
                        where Document.PublishDocumentID == parameter.PublishDocumentID
                        select Document).FirstOrDefault();

            return data;

        }
        static object GetAuthorizedEmployees(object param)
        {
            try
            {
                PublishDocumentDetails mdl = param as PublishDocumentDetails;
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {
                    var UserInfo = (from model in _context.AuthorisedEmployee
                                    join Login in _context.mUsers on model.UserId equals Login.UserId
                                    where model.IsActive == true
                                    && model.UserId == mdl.IsUserId
                                    select model).ToList();

                    return UserInfo;
                }
            }
            catch (Exception ee)
            {
            }
            return null;
        }
        static object UpdatePublishDocument(object param)
        {
            PublishDocumentDetails ToUpdate = param as PublishDocumentDetails;
            try
            {
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {
                    _context.masterDocument.Attach(ToUpdate);
                    _context.Entry(ToUpdate).State = EntityState.Modified;
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ToUpdate;
        }
        public static object DeletePublisDocument(object param)
        {
            try
            {
                using (PublishDocumentContext db = new PublishDocumentContext())
                {
                    PublishDocumentDetails parameter = param as PublishDocumentDetails;
                    PublishDocumentDetails stateToRemove = db.masterDocument.SingleOrDefault(a => a.PublishDocumentID == parameter.PublishDocumentID);
                    db.masterDocument.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        static Int32 GetMaxPublishID(object param)
        {
            try
            {
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {

                    var masterMaxPublishID = (from materPasses in _context.masterDocument
                                              select materPasses).OrderByDescending(m => m.PublishDocumentID).Take(1).FirstOrDefault();
                    if (masterMaxPublishID != null)
                    {

                        return Convert.ToInt32(masterMaxPublishID.PublishDocumentID) + 1;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }
        static Int32 GetMaxAuditPublishID(object param)
        {
            try
            {
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {

                    var masterMaxPublishID = (from materPasses in _context.auditDocument
                                              select materPasses).OrderByDescending(m => m.Id).Take(1).FirstOrDefault();
                    if (masterMaxPublishID != null)
                    {

                        return Convert.ToInt32(masterMaxPublishID.Id) + 1;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }
        static Int32 GetOfficeIDByDpIdandPublisID(object param)
        {
            try
            {
                PublishDocumentDetails parameter = param as PublishDocumentDetails;
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {

                    var masterMaxPublishID = (from materPasses in _context.masterDocument
                                              where materPasses.PublishDocumentID == parameter.PublishDocumentID && materPasses.FromdepartmentId == parameter.FromdepartmentId
                                              select materPasses).OrderByDescending(m => m.PublishDocumentID).Take(1).FirstOrDefault();
                    if (masterMaxPublishID != null)
                    {

                        return masterMaxPublishID.FromOfficeId;
                    }
                    return 0;
                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }

        static object GetOfficeandDocumentCount(object param)
        {
            try
            {
                PublishDocumentDetails parameter = param as PublishDocumentDetails;
                using (PublishDocumentContext _context = new PublishDocumentContext())
                {

                    var masterMaxPublishID = (from materPasses in _context.masterDocument
                                              where materPasses.FromOfficeId == parameter.FromOfficeId && materPasses.DocumentType == parameter.DocumentType
                                              select materPasses).ToList();
                    return masterMaxPublishID;
                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }

    }
}



