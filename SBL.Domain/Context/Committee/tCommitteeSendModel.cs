﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Ministery;
using System.Web.Mvc;
using System.ComponentModel;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using Lib.Web.Mvc.JQuery.JqGrid;

namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
    public class tCommitteeSendModel
    {

        public tCommitteeSendModel()
        {
            this.AssemList = new List<mAssembly>();
            this.SessList = new List<mSession>();
            this.EventList = new List<mEvent>();
            this.CommList = new List<tCommittee>();
            this.MemList = new List<mMember>();
            this.DeptList = new List<mDepartment>();
            this.DocList = new List<mDocumentType>();
            memMinList = new List<mMinisteryMinisterModel>();


        }

        public tCommitteeSendModel(tCommitteeSendModel Obj)
        {
            this.PaperName = Obj.PaperName;
            this.FileName = Obj.FileName;
            this.FilePath = Obj.FilePath;
            this.SignedFilePath = Obj.SignedFilePath;
            this.paperLaidId = Obj.paperLaidId;
            this.PaperLaidIdTemp = Obj.PaperLaidIdTemp;
            this.EventName = Obj.EventName;
            this.CommitteeName = Obj.CommitteeName;
            this.ChairPersonName = Obj.ChairPersonName;
            this.MinisterName = Obj.MinisterName;
            this.DepartmentName = Obj.DepartmentName;
            this.Title = Obj.Title;
            this.version = Obj.version;
            this.Description = Obj.Description;
            this.Remark = Obj.Remark;
            this.ProUnder = Obj.ProUnder;
            this.SubmittedDate = Obj.SubmittedDate;
            this.DesireLayDate = Obj.DesireLayDate;
            this.Status = Obj.Status;
            this.PaperLaidIdForCheckBox = Convert.ToString(Obj.paperLaidId);
           
        }



        #region Column Name


        [JqGridColumnFormatter("$.PaperLaidIdForCheckBoxFormatter")]
         [DisplayName("Tick to send")]
        public string PaperLaidIdForCheckBox { get; set; }

       
        [Required(ErrorMessage = "Enter Title.")]
        [DisplayName("Subject")]
        public string Title { get; set; }

        [DisplayName("Business Type")]
        public string EventName { get; set; }

        [DisplayName("Paper")]
        [Required(ErrorMessage = "Please Select File.")]
        [JqGridColumnFormatter("$.SendPaperSentFormatter")]
        public string FileName { get; set; }


        [DisplayName("Sent On")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? SubmittedDate { get; set; }


        //[HiddenInput(DisplayValue = false)]
        [DisplayName("Desire Lying Date")]
        [Required(ErrorMessage = "Please Select Date.")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DesireLayDate { get; set; }
        #endregion


      
        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Committee.")]
        public int? CommitteeId { get; set; }


       
        [HiddenInput(DisplayValue = false)]
        public string CommitteeName { get; set; }

        //[DisplayName("Paper")]
        [HiddenInput(DisplayValue = false)]
        public string PaperName { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int? version { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }    

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Assembly")]
        public int AssemblyId { get; set; }

       

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Session")]
        //public int? AssemId { get; set; }
        public int SessionId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string AssemblyName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }



        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Category.")]
        public int EventId { get; set; }

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Chairperson.")]
        public int? MemberId { get; set; }

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Department.")]
        public string DeptId { get; set; }





        [HiddenInput(DisplayValue = false)]
        public string CommitteeSubmittedDate { get; set; }


        //     [UIHint("tinymce_jquery_partial"), AllowHtml]
        [Required(ErrorMessage = "Enter Description.")]
        [HiddenInput(DisplayValue = false)]
        public string Description { get; set; }


        //        [UIHint("tinymce_jquery_partial"), AllowHtml]
        [Required(ErrorMessage = "Enter Provision Under.")]
        [HiddenInput(DisplayValue = false)]
        public string ProUnder { get; set; }

        

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Select Paper Type.")]
        public int? DocumentTypeId { get; set; }



        //  [UIHint("tinymce_jquery_partial"), AllowHtml]
        [HiddenInput(DisplayValue = false)]
        public string Remark { get; set; }

        [HiddenInput(DisplayValue = false)]
        [Required(ErrorMessage = "Please Select Minister.")]
        public int MinistryId { get; set; }



        [HiddenInput(DisplayValue = false)]
        public string FilePath { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string SignedFilePath { get; set; }

        

        [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinistryNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DepartmentName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DepartmentNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string SubmittedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Message { get; set; }

     



        [HiddenInput(DisplayValue = false)]
        public int OldVersion { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int CurrentVersion { get; set; }




        [HiddenInput(DisplayValue = false)]
        public string ChairPersonName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinisterName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? paperLaidId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? PaperLaidIdTemp { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ActionType { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string BtnCaption { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int DesireLayDateId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DoctName { get; set; }

    

        [HiddenInput(DisplayValue = false)]
        public int ComDraftCount { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ComSentCount { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string UserDesignation { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CurrentUserName { get; set; }


        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mAssembly> AssemList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mSession> SessList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mEvent> EventList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<tCommittee> CommList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mSessionDate> SessDateList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mMember> MemList { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mDepartment> DeptList { get; set; }
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mDocumentType> DocList { get; set; }
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<mMinisteryMinisterModel> memMinList { get; set; }
        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<tCommitteeModel> ComModel { get; set; }

        [HiddenInput(DisplayValue = false)]
        public virtual ICollection<tCommitteeSendModel> SendComModel { get; set; }

        [HiddenInput(DisplayValue = false)]
        public PaperMovementContainerModel paperMovementModel { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? DeptActivePaperId { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string ForSave { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string LoginId { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public string HashKey { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }

        /*Variable for paging*/
        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int RowsPerPage { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int selectedPage { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int loopStart { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int loopEnd { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int PageIndex { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int PageSize { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int PageNumber { get; set; }



    }
}
