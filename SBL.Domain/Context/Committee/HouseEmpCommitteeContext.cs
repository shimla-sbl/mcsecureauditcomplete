﻿using SBL.DomainModel.Models.Committee;
using System.Data.Entity;
using SBL.Service.Common;
using SBL.DomainModel.Extension;
using SBL.DAL;
using SBL.DomainModel.Models.Member;

using System.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace SBL.Domain.Context.Committee
{
   public class HouseEmpCommitteeContext : DBBase<HouseEmpCommitteeContext>
   {
       public HouseEmpCommitteeContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
       public  DbSet<AuthEmployee> AuthEmployee { get; set; }
       public static object Execute(ServiceParameter param)
       {
           if (null == param)
           {
               return null;
           }

           switch (param.Method)
           {
               case "CreateHouseEmpCommittee": { CreateHouseEmpCommittee(param.Parameter); break; }
               case "UpdateHouseEmpCommittee": { return UpdateHouseEmpCommittee(param.Parameter); }
               case "DeleteHouseEmpCommittee": { return DeleteHouseEmpCommittee(param.Parameter); }
               case "GetAllHouseEmpCommittee": { return GetAllHouseEmpCommittee(); }
               case "GetHouseEmpCommitteeById": { return GetHouseEmpCommitteeById(param.Parameter); }

           }
           return null;
       }

       static void CreateHouseEmpCommittee(object param)
       {
           try
           {
               using (HouseEmpCommitteeContext db = new HouseEmpCommitteeContext())
               {
                   AuthEmployee model = param as AuthEmployee;
                   model.CreatedDate = DateTime.Now;
                   model.ModifiedDate = DateTime.Now;
                   db.AuthEmployee.Add(model);
                   db.SaveChanges();
                   db.Close();
               }

           }
           catch
           {
               throw;
           }
       }


       static object UpdateHouseEmpCommittee(object param)
       {
           if (null == param)
           {
               return null;
           }
           using (HouseEmpCommitteeContext db = new HouseEmpCommitteeContext())
           {
             
               AuthEmployee model = param as AuthEmployee;
               model.CreatedDate = DateTime.Now;
               model.ModifiedDate = DateTime.Now;
               db.AuthEmployee.Attach(model);
               db.Entry(model).State = EntityState.Modified;
               db.SaveChanges();
               db.Close();
           }
           return GetAllHouseEmpCommittee();
       }

       static object DeleteHouseEmpCommittee(object param)
       {
           if (null == param)
           {
               return null;
           }
           using (HouseEmpCommitteeContext db = new HouseEmpCommitteeContext())
           {
               AuthEmployee parameter = param as AuthEmployee;
               AuthEmployee stateToRemove = db.AuthEmployee.SingleOrDefault(a => a.ID == parameter.ID);
               if (stateToRemove != null)
               {
                   stateToRemove.IsDeleted = true;
               }
               //db.mStates.Remove(stateToRemove);
               db.SaveChanges();
               db.Close();
           }
           return GetAllHouseEmpCommittee();
       }

       static List<AuthEmployee> GetAllHouseEmpCommittee()
       {
           HouseEmpCommitteeContext db = new HouseEmpCommitteeContext();

           //var query = db.mStates.ToList();
           var query = (from a in db.AuthEmployee
                        orderby a.ID descending
                        where a.IsDeleted == false
                        select a).ToList();

           return query;
       }

       static AuthEmployee GetHouseEmpCommitteeById(object param)
       {
           AuthEmployee parameter = param as AuthEmployee;
           HouseEmpCommitteeContext db = new HouseEmpCommitteeContext();
           var query = db.AuthEmployee.SingleOrDefault(a => a.ID == parameter.ID);
           return query;
       }

   }


}
