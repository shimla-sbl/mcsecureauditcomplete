﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using SBL.DAL;
using System.Data.Entity;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Session;
using SBL.Domain.Context.Session;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.ComplexModel.LinqKitSource;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.SiteSetting;


namespace SBL.Domain.Context.Committee
{
	public class CommitteeContext : DBBase<CommitteeContext>
	{
		public CommitteeContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

		public DbSet<mDepartment> objmDepartment { get; set; }
		public DbSet<mEvent> objmEvent { get; set; }
		public DbSet<tCommittee> objtcommittee { get; set; }
		public DbSet<mMember> objmMember { get; set; }
		public DbSet<mDocumentType> objmDocument { get; set; }
		public DbSet<mMinistry> objmMinistry { get; set; }
		public DbSet<mMinsitryMinister> objmMinistryMinister { get; set; }
		public DbSet<tPaperLaidTemp> objtPaperLaidTemp { get; set; }
		public DbSet<tPaperLaidV> objtPaperLaidV { get; set; }
		public DbSet<mMinistryDepartment> objMinisDepart { get; set; }
		public DbSet<mSessionDate> objSessDate { get; set; }
		public DbSet<tCommitteeEmployee> objCommEmp { get; set; }
		public DbSet<tCommitteeMember> objCommMem { get; set; }
		public DbSet<mCommitteeType> mCommitteeType { get; set; }
		public DbSet<mAssembly> mAssembly { get; set; }
		public DbSet<tCommitteeReport> tcomrep { get; set; }
		public DbSet<tCommitteeReportType> tcomreptype { get; set; }
		public DbSet<mSession> mSession { get; set; }
		//public DbSet<mMember> mMember { get; set; }
		public DbSet<SBL.DomainModel.Models.eFile.eFileAttachment> eFilAttachment { get; set; }
		public DbSet<CommitteeTypePermission> CommitteeTypePermission { get; set; }//added by robin
		public DbSet<mUsers> mUsers { get; set; }//added by robin
		public DbSet<SBL.DomainModel.Models.eFile.CommitteeMembersUpload> CommitteeMembersUpload { get; set; }
		public DbSet<SBL.DomainModel.Models.Adhaar.AdhaarDetails> AdhaarDetails { get; set; }
		public DbSet<SBL.DomainModel.Models.Constituency.mConstituency> mConstituency { get; set; }
		public DbSet<SBL.DomainModel.Models.Member.mMemberAssembly> mMemberAssembly { get; set; }
        public DbSet<SBL.DomainModel.Models.Committee.AuthEmployee> mAuthEmployee { get; set; }
		//SBL.DomainModel.Models.Adhaar.AdhaarDetails
        public virtual DbSet<SiteSettings> SiteSettings { get; set; }
		public static object Execute(SBL.Service.Common.ServiceParameter param)
		{
			switch (param.Method)
			{
				case "GetCommittees":
					{
						return GetCommittees(param.Parameter);
					}
				case "GetCategory":
					{
						return GetCategory();
					}
				case "GetCommittee":
					{
						return GetCommittee(param.Parameter);
					}
				case "GetChairPerson":
					{
						return GetChairPerson();
					}
				case "GetDocumentType":
					{
						return GetDocumentType();
					}
				case "GetMinisterMinistry":
					{
						return GetMinisterMinistry();
					}
				case "SaveCommittee":
					{
						return SaveCommittee(param.Parameter);
					}
				case "GetPendings":
					{
						return GetPendings();
					}
				case "GetOldFile":
					{
						return GetOldFile(param.Parameter);
					}
				case "GetDepartmentByMinister":
					{
						return GetDepartmentByMinister(param.Parameter);
					}
				case "SubmitPending":
					{
						return SubmitPending(param.Parameter);
					}
				case "GetRecordForEdit":
					{
						return GetRecordForEdit(param.Parameter);
					}
				case "GetAllDepartment":
					{
						return GetAllDepartment();
					}
				case "GetSubmitted":
					{
						return GetSubmitted();
					}
				case "DeletePending":
					{
						return DeletePending(param.Parameter);
					}
				case "ViewDetails":
					{
						return ViewDetails(param.Parameter);

					}

				case "SendViewDetails":
					{
						return SendViewDetails(param.Parameter);

					}


				case "CommitteeChairPendingViewDetails":
					{
						return CommitteeChairPendingViewDetails(param.Parameter);
					}

				case "SearchByCategory":
					{
						return SearchByCategory(param.Parameter);
					}
				case "FileUpload":
					{
						return FileUploadPopUp(param.Parameter);
					}
				case "AddLatestFile":
					{
						return AddLatestFile(param.Parameter);
					}
				case "GetInitialCount":
					{
						return GetInitialCount(param.Parameter);
					}
				case "ViewDetailsForReplacePaper":
					{
						return ViewDetailsForReplacePaper(param.Parameter);
					}
				case "GetLatestVersion":
					{ return GetLatestVersion(param.Parameter); }
				case "GetAllList":
					{ return GetAllList(param.Parameter); }
				case "GetChairmanByCommId":
					{
						return GetChairmanByCommId(param.Parameter);
					}


				//Added By Uday

				case "SubmittedCommitteeChairViewDetails":
					{
						return SubmittedCommitteeChairViewDetails(param.Parameter);
					}

				case "NewGetDraft":
					{
						return NewGetDraft(param.Parameter);
					}


				case "SearchChairByCategory":
					{
						return SearchChairByCategory(param.Parameter);
					}

				case "CommitteeUpcomingLOB":
					{
						return CommitteeUpcomingLOB(param.Parameter);
					}


				case "CommitteeLaidinHouse":
					{
						return CommitteeLaidinHouse(param.Parameter);
					}

				case "CommitteePendingToLay":
					{
						return CommitteePendingToLay(param.Parameter);
					}

				case "GetInitialChairCount":
					{
						return GetInitialChairCount(param.Parameter);
					}

				case "GetAllChairList":
					{
						return GetAllChairList(param.Parameter);
					}

				case "SendingPendingForSubCommitteeByType":
					{
						return SendingPendingForSubCommitteeByType(param.Parameter);
					}

				case "GetCommitteePaperAttachmentByIds":
					{
						return GetCommitteePaperAttachmentByIds(param.Parameter);
					}


				case "SendingPendingForSubCommitteeChairByType":
					{
						return SendingPendingForSubCommitteeChairByType(param.Parameter);
					}

				case "InsertSignPathAttachmentCommittee":
					{
						return InsertSignPathAttachmentCommittee(param.Parameter);
					}

				case "UpdateDepartmentMinisterActivePaperIdCommittee":
					{
						return UpdateDepartmentMinisterActivePaperIdCommittee(param.Parameter);
					}

				case "ShowCommitteeMemberByID":
					{
						return ShowCommitteeMemberByID(param.Parameter);
					}
				//added by aiswarya

				case "GetSearchedCommitteeGetTitleList":
					{
						return GetSearchedCommitteeGetTitleList(param.Parameter);
					}

				case "GetAllPeriodList":
					{
						return GetAllPeriodList(param.Parameter);
					}
				case "GetAllReportNoList":
					{
						return GetAllReportNoList(param.Parameter);
					}

				case "GetAllChairmanList":
					{
						return GetAllChairmanList(param.Parameter);
					}
				case "GetAllReporttypeList":
					{
						return GetAllReporttypeList(param.Parameter);
					}
				case "GetReporttypeList":
					{
						return GetReporttypeList(param.Parameter);
					}
				//case "GetAllDateOfLaying":
				//    {
				//        return GetAllDateOfLaying(param.Parameter);
				//    }
				//case "GetAllAssemblyReverseList":
				//    {
				//        return GetAllAssemblyReverseList(param.Parameter);
				//    }
				//case "GetCommitteSubjects_key":
				//    {
				//        return GetCommitteSubjects_key(param.Parameter);
				//    }
				case "GetSearchedCommittee":
					{
						return GetSearchedCommittee(param.Parameter);
					}
				//case "GetTitleList":
				//    {
				//        return GetTitleList(param.Parameter);
				//    }
				case "GetReportNoList":
					{
						return GetReportNoList(param.Parameter);
					}
				case "GetAllPeriods":
					{
						return GetAllPeriods(param.Parameter);
					}
				case "GetAllMembersforCommittee":
					{
						return GetAllMembersforCommittee(param.Parameter);
					}

				//added by bhadri
				case "GetAllCommiteeType":
					{
						return GetAllCommiteeType();
					}
				case "CreateCommiteeType":
					{
						return CreateCommiteeType(param.Parameter);
					}
				case "CreateCommittee":
					{
						return CreateCommittee(param.Parameter);
					}
				case "GetCommitteById":
					{
						return GetCommitteById(param.Parameter);
					}
				case "UpdateCommittee":
					{
						return UpdateCommittee(param.Parameter);
					}
				case "GetChairman":
					{
						return GetChairman(param.Parameter);
					}
				case "GetCommitteeAssemlyComType":
					{
						return GetCommitteeAssemlyComType(param.Parameter);
					}

				//case"GetAlldateoflayingList":
				//    {
				//        return GetAlldateoflayingList(param.Parameter);
				//    }
				case "DeleteCommitteeType": { return DeleteCommitteeType(param.Parameter); }
				case "CommitteeExisit": { return CheckCommitteExisit(); }
				case "GetAllCommitteeMembers": { return GetAllCommitteeMembers(); }
				case "GetAllCommittee": { return GetAllCommittee(); }
				case "ShowCommitteMemberByCommitteeId": { return ShowCommitteMemberByCommitteeId(param.Parameter); }
				case "AddCommitteeMembers": { return AddCommitteeMembers(param.Parameter); }
				case "GetSubCommitteById": { return GetSubCommitteeById(param.Parameter); }
				case "UpdateSubCommittee": { return UpdateSubCommittee(param.Parameter); }
				case "DeleteSubCommittee": { return DeleteSubCommittee(param.Parameter); }
				case "GetCommitteeForPDF": { return GetCommitteeForPDF(param.Parameter); }
				case "GetCommitteeReply": { return GetCommitteeReply(param.Parameter); }

				//added by robin
				case "GetAllUsers": { return GetAllUsers(); }
				case "SaveNewCommitteType": { return SaveNewCommitteType(param.Parameter); }
				case "GetAllCommiteeTypeForPermission": { return GetAllCommiteeTypeForPermission(); }
				case "GetUserCommitteePermissionList": { return GetUserCommitteePermissionList(); }
				case "DeleteCommitteePermissionType": { return DeleteCommitteePermissionType(param.Parameter); }
				case "CheckNewCommitteType1": { return CheckNewCommitteType1(param.Parameter); }
				case "CheckNewCommitteType2": { return CheckNewCommitteType2(param.Parameter); }
				case "SaveNewCommitteTypeIsActive": { return SaveNewCommitteTypeIsActive(param.Parameter); }

                case "GetCommitteeAuthorizeList": { return GetCommitteeAuthorizeList(); }
                case "DataForEditCommAuth": { return DataForEditCommAuth(param.Parameter); }
                case "NewCommitteAuthorize": { return NewCommitteAuthorize(param.Parameter); }
                case "DeleteCommitteeAuth": { return DeleteCommitteeAuth(param.Parameter); }
                //Shashi    
				case "GetAllCommitteeByMemberPermission": { return GetAllCommitteeByMemberPermission(param.Parameter); }
				case "GetCommitteeDetailsByCommitteeId": { return GetCommitteeDetailsByCommitteeId(param.Parameter); }
				case "GetAllCommitteeDetByID": { return GetAllCommitteeDetByID(param.Parameter); }
				case "GetCommitteeMemberPDFListByCommitteeId": { return GetCommitteeMemberPDFListByCommitteeId(param.Parameter); }
				case "DeletCommitteeMemberByCommitteeID": { return DeletCommitteeMemberByCommitteeID(param.Parameter); }
				case "GetAllMembersByCommitteeIdPDF": { return GetAllMembersByCommitteeIdPDF(param.Parameter); }
				case "DeleteGeneratedMemberPDfByID": { return DeleteGeneratedMemberPDfByID(param.Parameter); }
                case "Get_ActiveCommittee": { return Get_ActiveCommittee(); }
				//DeleteGeneratedMemberPDfByID

			}

			return null;
		}




		#region "Committee"

		static object GetCommittees(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();

			//tCommittee model = param as tCommittee;

			var query = (from cmt in CommCtx.objtcommittee
						 //where (cmt.AssemblyID == model.AssemblyID)
						 where (cmt.AssemblyID == 12)
						 select new CommitteeSearchModel
						 //select new tCommittee
						 {
							 CommitteeId = cmt.CommitteeId,
							 CommitteeName = cmt.CommitteeName
						 }

						 );

			return query.ToList();
		}

		//static object GetCommitteeReply(object param)
		//{
		//    try
		//    {
		//        using (CommitteeContext context = new CommitteeContext())
		//        {
		//            List<SBL.DomainModel.Models.eFile.DepartmentReply> DeptReply = new List<DomainModel.Models.eFile.DepartmentReply>();
		//            List<SBL.DomainModel.Models.eFile.DepartmentReply> DeptReplys = new List<DomainModel.Models.eFile.DepartmentReply>();
		//            List<int> ids = new List<int>();
		//            int parentId=Convert.ToInt32(param);
		//            DeptReply = (from value in context.eFilAttachment
		//                         where value.ParentId == parentId //value.Type == "CommitteeToDept" &&
		//                          select new SBL.DomainModel.Models.eFile.DepartmentReply {CreatedDate=value.CreatedDate, Description=value.Description, eFileAttachmentId=value.eFileAttachmentId, 
		//                          Title=value.Title}).ToList();
		//            foreach (var item in DeptReply)
		//            {

		//            }
		//            return result;
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        string exception = ex.ToString();
		//    }
		//    return null;
		//}

		//static object GetCommitteeReply(object param)
		//{
		//    SBL.DomainModel.Models.eFile.DepartmentReply DeptReply = new DomainModel.Models.eFile.DepartmentReply();
		//    SBL.DomainModel.Models.eFile.DepartmentReply DeptReplys = new DomainModel.Models.eFile.DepartmentReply();
		//    using (CommitteeContext context = new CommitteeContext())
		//    {
		//        int ParentId = Convert.ToInt32(param);
		//        DeptReply = (from value in context.eFilAttachment
		//                     where value.ParentId == ParentId //value.Type == "CommitteeToDept" &&
		//                     select new SBL.DomainModel.Models.eFile.DepartmentReply
		//                     {
		//                         CreatedDate = value.CreatedDate,
		//                         Description = value.Description,
		//                         eFileAttachmentId = value.eFileAttachmentId,
		//                         Title = value.Title,
		//                         ParentId = value.ParentId 
		//                     }).FirstOrDefault();
		//        DeptReplys = DeptReply;                
		//        BuildChildNode(DeptReply);
		//        //DeptReply.ChildNode.Add(DeptReplys);
		//        return DeptReply;
		//    }

		//    //return null;
		//}


		static object GetCommitteeReply(object param)
		{
			List<SBL.DomainModel.Models.eFile.DepartmentReply> DeptReply = new List<DomainModel.Models.eFile.DepartmentReply>();
			using (CommitteeContext context = new CommitteeContext())
			{
				int ParentId = Convert.ToInt32(param);
				DeptReply = (from value in context.eFilAttachment
							 where value.ParentId == ParentId //value.Type == "CommitteeToDept" &&
							 select new SBL.DomainModel.Models.eFile.DepartmentReply
							 {
								 CreatedDate = value.CreatedDate,
								 Description = value.Description,
								 eFileAttachmentId = value.eFileAttachmentId,
								 Title = value.Title,
								 ParentId = value.ParentId
							 }).ToList();
				foreach (var item in DeptReply)
				{
					BuildChildNode(item);
				}

				return DeptReply;
			}

			//return null;
		}

		private static void BuildChildNode(SBL.DomainModel.Models.eFile.DepartmentReply DeptReply)
		{
			using (CommitteeContext context = new CommitteeContext())
			{
				if (DeptReply != null)
				{
					List<SBL.DomainModel.Models.eFile.DepartmentReply> childNode = (from value in context.eFilAttachment
																					where value.ParentId == DeptReply.eFileAttachmentId //value.Type == "CommitteeToDept" &&
																					select new SBL.DomainModel.Models.eFile.DepartmentReply
																					{
																						CreatedDate = value.CreatedDate,
																						Description = value.Description,
																						eFileAttachmentId = value.eFileAttachmentId,
																						Title = value.Title
																					}).ToList<SBL.DomainModel.Models.eFile.DepartmentReply>();
					if (childNode.Count > 0)
					{
						foreach (var child123Node in childNode)
						{
							BuildChildNode(child123Node);
							DeptReply.ChildNode.Add(child123Node);
						}
					}

				}
			}
		}





		static object GetCommitteeForPDF(object param)
		{
			try
			{
				using (CommitteeContext context = new CommitteeContext())
				{
					var result = (from emp in context.objCommMem
								  group emp by new { emp.CommitteeId, emp.IsReportChairMan, emp.MemberId } into empg
								  join mem in context.objmMember on empg.FirstOrDefault().MemberId equals mem.MemberID
								  join com in context.objtcommittee on empg.FirstOrDefault().CommitteeId equals com.CommitteeId
								  select new tCommitteeMemberList { CommitteeNameLocal = com.CommitteeNameLocal, CommitteMemberName = mem.NameLocal, IsChairMan = empg.FirstOrDefault().IsReportChairMan, CommitteeName = com.CommitteeName }).ToList();
					return result;
				}
			}
			catch (Exception ex)
			{
			}
			return null;
		}

		static object DeleteSubCommittee(object param)
		{
			try
			{
				using (CommitteeContext context = new CommitteeContext())
				{
					int CommitteeId = Convert.ToInt32(param);
					tCommittee committee = context.objtcommittee.SingleOrDefault(value => value.CommitteeId == CommitteeId);
					context.objtcommittee.Remove(committee);
					context.SaveChanges();
					return true;
				}
			}
			catch (Exception ex)
			{
				string exception = ex.ToString();
			}
			return false;
		}

		static object UpdateSubCommittee(object param)
		{
			try
			{
				using (CommitteeContext context = new CommitteeContext())
				{
					tCommittee model = param as tCommittee;
					context.Entry(model).State = EntityState.Modified;
					context.SaveChanges();
					context.Close();
				}

			}
			catch (Exception ex)
			{
				string exception = ex.ToString();
			}
			return null;
		}

		static object GetSubCommitteeById(object param)
		{
			try
			{
				using (CommitteeContext comcontext = new CommitteeContext())
				{

					int CommitteeId = Convert.ToInt32(param);
					var comList = (from data in comcontext.objtcommittee
								  // join assembly in comcontext.mAssembly
								   //on data.AssemblyID equals assembly.AssemblyID
								  // join comType in comcontext.mCommitteeType
								   //on data.CommitteeTypeId equals comType.CommitteeTypeId
								   where data.CommitteeId == CommitteeId
								   select data).FirstOrDefault();
					return comList;
				}

			}
			catch (Exception ex)
			{
				string exception = ex.ToString();
			}
			return null;
		}

		/// <summary>
		/// Add committee members
		/// </summary>
		/// <param name="param"></param>
		static object AddCommitteeMembers(object param)
		{

			CommitteeContext ComContext = new CommitteeContext();
			tCommitteeMember model = param as tCommitteeMember;

			//int identityKey = (from data in ComContext.objCommMem
			//				   where data.CommitteeId == model.CommitteeId && data.MemberId == model.MemberId
			//				   select data.IdKey).FirstOrDefault();
			//if (identityKey > 0)
			//{
			//	model.IdKey = identityKey;
			//	ComContext.Entry(model).State = EntityState.Modified;
			//	ComContext.SaveChanges();
			//	ComContext.Close();
			//}
			//else
			//{
			ComContext.objCommMem.Add(model);
			ComContext.SaveChanges();
			ComContext.Close();
			//}
			return true;
		}

		//static object EditSubCommittee(object param)
		//{

		//}

		static object ShowCommitteMemberByCommitteeId(object param)
		{
			using (CommitteeContext dbContext = new CommitteeContext())
			{
				string[] testvalue = param as string[];

				int Objvalue = Convert.ToInt32(testvalue[0]);
				int AID = Convert.ToInt32(testvalue[1]);

				var memberList = (from value in dbContext.objCommMem
								  join member in dbContext.objmMember on value.MemberId equals member.MemberID
								  join a in dbContext.mMemberAssembly on member.MemberCode equals a.MemberID
								  join c in dbContext.mConstituency on
									new { code = a.ConstituencyCode, Id = a.AssemblyID } equals new { code = c.ConstituencyCode, Id = c.AssemblyID.Value }
								  where value.CommitteeId == Objvalue && a.AssemblyID == AID
								  select new tCommitteeMemberList
								  {
									  CommitteeId = value.CommitteeId,
									  CommitteMemberName = member.Name + "( " + c.ConstituencyName + " )",
									  IsChairMan = value.IsReportChairMan,
									  //IdKey = value.IdKey,
									  MemberId = value.MemberId,
									  IsChairManComm = value.IsCommitteeChairMan,
									  StaffID = value.StaffID
								  }).ToList();
				return memberList;
			}
		}
		static object GetAllCommittee()
		{
			//List<CommitteesListView> list = new List<CommitteesListView>();
			using (CommitteeContext comcontext = new CommitteeContext())
			{
				var comList = (from data in comcontext.objtcommittee
							  // join assembly in comcontext.mAssembly on data.AssemblyID equals assembly.AssemblyID
							   //join comType in comcontext.mCommitteeType on data.CommitteeTypeId equals comType.CommitteeTypeId
							   where data.ParentId == 0
							   select new CommitteesListView
							   {
								 //  AssemblyName = assembly.AssemblyName,
								   CommitteeType = "",
								   CommitteeId = data.CommitteeId,
								   CommitteeName = data.CommitteeName,
								   DateOfCreation = data.DateOfCreation,
								   ParentId = data.ParentId,
                                  Abbreviation=data.Abbreviation,
								   CommitteeNameLocal = data.CommitteeNameLocal
							   }).OrderBy(a => a.CommitteeName).ToList();
				return comList;
			}
		}



		public static object GetAllCommitteeMembers(object param)
		{
			CommitteeContext ComContext = new CommitteeContext();
			var list = from value in ComContext.objCommMem where value.CommitteeId == 1 select value;
			return list;
		}

		static object GetAllCommitteeMembers()
		{
			using (CommitteeContext dbContext = new CommitteeContext())
			{
				var memberList = (from value in dbContext.objCommMem
								  join member in dbContext.objmMember
								  on value.MemberId equals member.MemberID
								  where value.CommitteeId == 1
								  select new tCommitteeMemberList
								  {
									  CommitteeId = value.CommitteeId,
									  CommitteMemberName = member.Name,
									  IsChairMan = value.IsReportChairMan,
									  IdKey = value.IdKey,
									  MemberId = value.MemberId
								  }).ToList();
				return memberList;
			}
			//return null;
		}

		static object CreateCommittee(object param)
		{
			using (CommitteeContext context = new CommitteeContext())
			{
                 
				tCommittee model = param as tCommittee;
                if (model.Mode == "Edit")
                {
                    tCommittee obj = context.objtcommittee.Single(m => m.CommitteeId == model.CommitteeId);               
                    obj.CommitteeName = model.CommitteeName;
                    obj.Remark = model.Remark;
                    obj.Abbreviation = model.Abbreviation;
                    obj.ParentId = 0;
                    obj.AutoSchedule = true;
                    obj.DateOfCreation = DateTime.Now;
                    obj.CommitteeYear = "0";
                    context.SaveChanges();                   
                }
                else
                {
                    model.AutoSchedule = true;
                    model.DateOfCreation = DateTime.Now;
                    model.ParentId = 0;
                    context.objtcommittee.Add(model);
                    context.SaveChanges();
                    context.Close();
                }
				
			}
			return null;
		}
		static object CheckCommitteExisit()
		{
			using (CommitteeContext ComContext = new CommitteeContext())
			{
				int count = (from value in ComContext.objCommMem where value.CommitteeId == 1 select value).Count();
				return count;
			}
		}

		static object ShowCommitteeMemberByID(object param)
		{
			tCommitteeMember cm = new tCommitteeMember();

			int UserName = Convert.ToInt32(param);

			CommitteeContext CommCtx = new CommitteeContext();

			var EvtList = (from Evnt in CommCtx.objCommMem

						   where Evnt.IsReportChairMan == true && Evnt.MemberId == UserName
						   select Evnt).ToList();


			return EvtList;


		}


		static object GetCategory()
		{
			CommitteeContext CommCtx = new CommitteeContext();
			var EvtList = (from Evnt in CommCtx.objmEvent
						   where Evnt.PaperCategoryTypeId == 3
						   select Evnt).ToList();
			CommCtx.Close();
			return EvtList;

		}






		static object GetCommittee(object param)
		{
            CommitteeContext CommCtx = new CommitteeContext();
            //var model = param as tCommittee;
            var query = (from cmt in CommCtx.objtcommittee

                         select cmt);
                 
            //var model = param as tCommitteeModel;
            //CommitteeContext CommCtx = new CommitteeContext();
            //var predicate = PredicateBuilder.True<tCommittee>();
            //if (model.AssemblyId != 0)
            //    predicate = predicate.And(q => q.AssemblyID == model.AssemblyId);
          
            //var query = (from cmt in CommCtx.objtcommittee
            //             select new tCommittee
            //             //{
            //             //    CommitteeId = cmt.CommitteeId,
            //             //    CommitteeName = cmt.CommitteeName,
                          
            //             //}

            //             ).AsExpandable().Where(predicate);

            return query.ToList();
			//return query.ToList();
		}



		static object GetCommitteeAssemlyComType(object param)
		{
			var model = param as tCommittee;
			CommitteeContext CommCtx = new CommitteeContext();
			var predicate = PredicateBuilder.True<CommitteeSearchModel>();
			if (model.AssemblyID != 0)
				predicate = predicate.And(q => q.AssemblyID == model.AssemblyID);
			if (model.CommitteeTypeId != 0)
				predicate = predicate.And(q => q.CommitteeTypeId == model.CommitteeTypeId);


			var query = (from cmt in CommCtx.objtcommittee
						 select new CommitteeSearchModel
						 {
							 CommitteeId = cmt.CommitteeId,
							 CommitteeName = cmt.CommitteeName,
							 AssemblyID = cmt.AssemblyID,
							 CommitteeTypeId = cmt.CommitteeTypeId
						 }

						 ).AsExpandable().Where(predicate);

			return query.ToList();
		}


		static object GetChairPerson()
		{
			CommitteeContext CommCtx = new CommitteeContext();
			var ChairPer = (from ChrPersn in CommCtx.objmMember
							select ChrPersn).ToList();
			CommCtx.Close();
			return ChairPer;

		}

		static object GetDocumentType()
		{
			CommitteeContext CommCtx = new CommitteeContext();
			var DocList = (from Doct in CommCtx.objmDocument
						   select Doct).ToList();
			CommCtx.Close();
			return DocList;

		}

		static object GetAllDepartment()
		{
			//CommitteeContext CommCtx = new CommitteeContext();
			//var DocList = (from Doct in CommCtx.objmDocument
			//               select Doct).ToList();

			List<tCommitteeModel> obj = new List<tCommitteeModel>();
			tCommitteeModel CM = new tCommitteeModel();
			CM.DeptId = "0";
			CM.DepartmentName = "Select Department";
			obj.Add(CM);
			return obj;

		}


		public static object GetInitialChairCount(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel ComObj = param as tCommitteeModel;

			int PndListCnt = (from Pap in CommCtx.objtPaperLaidV
							  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							  join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							  where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
							   && tempP.MinisterSubmittedDate == null
							  && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
							  orderby tempP.PaperLaidId
							  select new tCommitteeModel
							  {
							  }).ToList().Count;



			int SubListCnt = (from Pap in CommCtx.objtPaperLaidV
							  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							  where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
							  && tempP.MinisterSubmittedDate != null
							  && (Pap.CommitteeId != null)
							  && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
							  orderby tempP.PaperLaidId
							  select new tCommitteeModel
							  {
							  }).ToList().Count;


			int UpcomingLOB = (from Pap in CommCtx.objtPaperLaidV
							   join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							   join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							   join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							   join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							   join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							   join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							   where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
							   && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
							   && Pap.LOBRecordId != null
							   && Pap.LaidDate == null
							   && Pap.DesireLayingDate > DateTime.Now
							   && tempP.MinisterSubmittedDate != null
							   orderby tempP.PaperLaidId
							   select new tCommitteeModel
							   {
							   }).ToList().Count;

			int LaidintheHouse = (from Pap in CommCtx.objtPaperLaidV
								  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
								  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
								  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
								  join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
								  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
								  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
								  where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
								  && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
								  && Pap.LOBRecordId != null
								  && Pap.LaidDate != null
								  && Pap.DesireLayingDate <= DateTime.Now
								  && tempP.MinisterSubmittedDate != null
								  orderby tempP.PaperLaidId
								  select new tCommitteeModel
								  {
								  }).ToList().Count;

			int PendingToLay = (from Pap in CommCtx.objtPaperLaidV
								join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
								join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
								join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
								join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
								join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
								join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
								where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
								&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
								&& Pap.LOBRecordId == null
								&& Pap.LaidDate == null
								&& Pap.DesireLayingDate > DateTime.Now
								&& tempP.MinisterSubmittedDate != null
								orderby tempP.PaperLaidId
								select new tCommitteeModel
								{
								}).ToList().Count;


			ComObj.ComDraftCount = PndListCnt;
			ComObj.ComSentCount = SubListCnt;
			ComObj.ComUpcomingCount = UpcomingLOB;
			ComObj.ComLaidIntheHouseCount = LaidintheHouse;
			ComObj.ComPendingtoLayCount = PendingToLay;

			return ComObj;
		}




		public static object NewGetDraft(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;


			if (Obj.ActionType == "Pending")
			{
				IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
														join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
														join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
														join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
														join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
														join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
														where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
														&& (Pap.DeptActivePaperId == null)
														&& (Pap.CommitteeId != null)
														orderby tempP.PaperLaidId
														select new tCommitteeModel
														{
															FileName = tempP.FileName,
															FilePath = tempP.FilePath,
															paperLaidId = Pap.PaperLaidId,
															PaperName = "Committee",
															PaperLaidIdTemp = tempP.PaperLaidTempId,
															EventName = evnt.EventName,
															CommitteeName = comm.CommitteeName,
															MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
															DepartmentName = Dept.deptname,
															Title = Pap.Title,
															Description = Pap.Description,
															ProUnder = Pap.ProvisionUnderWhich,
															Remark = Pap.Remark,
															Status = (int)CommitteeStatus.Draft,
															version = tempP.Version,
															DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault()
														}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();




				Obj.ResultCount = PndList.Count();
				Obj.ComModel = PndList.ToList();
			}
			else if (Obj.ActionType == "Submitted")
			{
				IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
														join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
														join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
														join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
														join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
														join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
														where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
														&& (Pap.CommitteeId != null)
														&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
														orderby tempP.PaperLaidId
														select new tCommitteeModel
														{
															FileName = tempP.SignedFilePath,
															SignedFilePath = tempP.SignedFilePath,
															paperLaidId = Pap.PaperLaidId,
															PaperLaidIdTemp = tempP.PaperLaidTempId,
															EventName = evnt.EventName,
															CommitteeName = comm.CommitteeName,
															MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
															DepartmentName = Dept.deptname,
															version = tempP.Version,
															Status = (int)CommitteeStatus.Send,
															Title = Pap.Title,
															DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
															SubmittedDate = tempP.CommtSubmittedDate,
														}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

				Obj.ResultCount = PndList.Count();
				Obj.ComModel = PndList.ToList();

				foreach (var item in PndList)
				{
					Obj.SignedFilePath = item.FileName;
					Obj.Status = item.Status;
				}

			}

			CommCtx.Close();
			return Obj;

		}


		static object GetMinisterMinistry()
		{

			CommitteeContext CommCtx = new CommitteeContext();
			var MinMisList = (from Min in CommCtx.objmMinistry
							  join minis in CommCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
							  where minis.MinistryID != 12
							  select new mMinisteryMinisterModel
							  {
								  MinistryID = minis.MinistryID,
								  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
							  }).ToList();
			CommCtx.Close();

			return MinMisList;
		}

		static object GetOldFile(Object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel obj = param as tCommitteeModel;
			var finName = (from ptemp in CommCtx.objtPaperLaidTemp
						   where (ptemp.PaperLaidId == obj.paperLaidId && ptemp.PaperLaidTempId == obj.PaperLaidIdTemp)
						   select ptemp.FileName).FirstOrDefault();
			CommCtx.Close();
			return finName;
		}

		static object GetDepartmentByMinister(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;
			var DeptList = (from minsDept in CommCtx.objMinisDepart
							join dept in CommCtx.objmDepartment on minsDept.DeptID equals dept.deptId
							where minsDept.MinistryID == Obj.MinistryId
							select new tCommitteeModel
							{
								DeptId = dept.deptId,
								DepartmentName = dept.deptname

							}).ToList();
			CommCtx.Close();

			return DeptList;

		}

		static object SaveCommittee(object param)
		{
			tPaperLaidV PLMdl = new tPaperLaidV();
			tPaperLaidTemp PLTemp = new tPaperLaidTemp();
			var msg = "";
			try
			{
				CommitteeContext CommCtx = new CommitteeContext();

				tCommitteeModel comObj = param as tCommitteeModel;
				if (comObj.ActionType == "SaveUpdateEntry")
				{
					if (comObj.BtnCaption == "Update")
					{
						PLMdl = CommCtx.objtPaperLaidV.Single(m => m.PaperLaidId == comObj.paperLaidId);

						PLMdl.AssemblyId = comObj.AssemblyId;
						PLMdl.SessionId = comObj.SessionId;
						PLMdl.EventId = comObj.EventId;
						PLMdl.MinistryId = comObj.MinistryId;
						PLMdl.MinistryName = comObj.MinistryName;
						PLMdl.MinistryNameLocal = comObj.MinistryName;
						PLMdl.DepartmentId = comObj.DeptId;
						PLMdl.DeparmentName = comObj.DepartmentName;
						PLMdl.DeparmentNameLocal = comObj.DepartmentName;
						PLMdl.Title = comObj.Title;
						PLMdl.Description = comObj.Description;
						PLMdl.PaperTypeID = comObj.DocumentTypeId;
						PLMdl.DesireLayingDateId = comObj.DesireLayDateId;
						PLMdl.CommitteeId = comObj.CommitteeId;
						PLMdl.CommitteeChairmanMemberId = comObj.MemberId;
						PLMdl.Remark = comObj.Remark;
						PLMdl.ProvisionUnderWhich = comObj.ProUnder;
						CommCtx.SaveChanges();

						/*Update PaperLaidtemp*/
						PLTemp = CommCtx.objtPaperLaidTemp.Single(m => m.PaperLaidTempId == comObj.PaperLaidIdTemp);
						if (comObj.FilePath != null)
						{
							PLTemp.FilePath = comObj.FilePath;
							PLTemp.FileName = comObj.AssemblyId + "_" + comObj.SessionId + "_C_" + PLMdl.PaperLaidId + "_V1.pdf";
						}

						CommCtx.SaveChanges();
						CommCtx.Close();
						msg = "Update Successfully !";
					}
					else if (comObj.BtnCaption == "Save")
					{
						PLMdl.AssemblyId = comObj.AssemblyId;
						PLMdl.SessionId = comObj.SessionId;
						PLMdl.EventId = comObj.EventId;
						PLMdl.MinistryId = comObj.MinistryId;
						PLMdl.MinistryName = comObj.MinistryName;
						PLMdl.MinistryNameLocal = comObj.MinistryNameLocal;
						PLMdl.DepartmentId = comObj.DeptId;
						PLMdl.DeparmentName = comObj.DepartmentName;
						PLMdl.DeparmentNameLocal = comObj.DepartmentName;
						PLMdl.Title = comObj.Title;
						PLMdl.Description = comObj.Description;
						PLMdl.PaperTypeID = comObj.DocumentTypeId;
						PLMdl.DesireLayingDateId = comObj.DesireLayDateId;
						PLMdl.CommitteeId = comObj.CommitteeId;
						PLMdl.CommitteeChairmanMemberId = comObj.MemberId;
						PLMdl.Remark = comObj.Remark;
						PLMdl.ProvisionUnderWhich = comObj.ProUnder;
						CommCtx.objtPaperLaidV.Add(PLMdl);
						CommCtx.SaveChanges();

						/*insert into  PaperLaidtemp*/
						PLTemp.PaperLaidId = PLMdl.PaperLaidId;
						PLTemp.Version = 1;
						PLTemp.FilePath = comObj.FilePath;
						PLTemp.FileName = comObj.AssemblyId + "_" + comObj.SessionId + "_C_" + PLMdl.PaperLaidId + "_V1.pdf";
						CommCtx.objtPaperLaidTemp.Add(PLTemp);
						CommCtx.SaveChanges();

						//PLMdl.CommitteeActivePaeperId = PLTemp.PaperLaidTempId;
						CommCtx.SaveChanges();
						CommCtx.Close();

						msg = PLMdl.PaperLaidId + "_Saved Successfully !";
					}
				}

				else if (comObj.ActionType == "SendEntry")
				{
					if (comObj.BtnCaption == "Update")
					{
						tPaperLaidTemp Obj = new tPaperLaidTemp();
						tPaperLaidV Obj2 = new tPaperLaidV();

						Obj = CommCtx.objtPaperLaidTemp.Single(m => m.PaperLaidTempId == comObj.PaperLaidIdTemp);

						Obj.CommtSubmittedBy = comObj.SubmittedBy;
						Obj.CommtSubmittedDate = DateTime.Now;
						CommCtx.SaveChanges();

						Obj2 = CommCtx.objtPaperLaidV.Single(m => m.PaperLaidId == Obj.PaperLaidId);
						Obj2.CommitteeActivePaeperId = Obj.PaperLaidTempId;
						CommCtx.SaveChanges();

						CommCtx.Close();
						msg = PLMdl.PaperLaidId + "_Sent Successfully !";

					}
					else if (comObj.BtnCaption == "Save")
					{
						PLMdl.AssemblyId = comObj.AssemblyId;
						PLMdl.SessionId = comObj.SessionId;
						PLMdl.EventId = comObj.EventId;
						PLMdl.MinistryId = comObj.MinistryId;
						PLMdl.MinistryName = comObj.MinistryName;
						PLMdl.MinistryNameLocal = comObj.MinistryNameLocal;
						PLMdl.DepartmentId = comObj.DeptId;
						PLMdl.DeparmentName = comObj.DepartmentName;
						PLMdl.DeparmentNameLocal = comObj.DepartmentName;
						PLMdl.Title = comObj.Title;
						PLMdl.Description = comObj.Description;
						PLMdl.PaperTypeID = comObj.DocumentTypeId;
						PLMdl.DesireLayingDateId = comObj.DesireLayDateId;
						PLMdl.CommitteeId = comObj.CommitteeId;
						PLMdl.CommitteeChairmanMemberId = comObj.MemberId;
						PLMdl.Remark = comObj.Remark;
						PLMdl.ProvisionUnderWhich = comObj.ProUnder;
						CommCtx.objtPaperLaidV.Add(PLMdl);
						CommCtx.SaveChanges();

						/*insert into  PaperLaidtemp*/
						PLTemp.PaperLaidId = PLMdl.PaperLaidId;
						PLTemp.Version = 1;
						PLTemp.FilePath = comObj.FilePath;
						PLTemp.FileName = comObj.AssemblyId + "_" + comObj.SessionId + "_C_" + PLMdl.PaperLaidId + "_V1.pdf";
						PLTemp.CommtSubmittedBy = comObj.SubmittedBy;
						PLTemp.CommtSubmittedDate = DateTime.Now;
						CommCtx.objtPaperLaidTemp.Add(PLTemp);
						CommCtx.SaveChanges();

						//PLMdl.CommitteeActivePaeperId = PLTemp.PaperLaidTempId;

						//// Update CommitteeActivePaeper in PaperLaidVs table

						PLMdl.CommitteeActivePaeperId = PLTemp.PaperLaidTempId;
						CommCtx.SaveChanges();

						CommCtx.Close();

						msg = PLMdl.PaperLaidId + "_Sent Successfully !";
					}
				}





			}
			catch (Exception ex)
			{
				msg = ex.Message;
			}


			return msg;
		}



		public static object GetPendings()
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = new tCommitteeModel();

			var CategoryList = (from C in CommCtx.objmEvent
								where C.PaperCategoryTypeId == 3
								select C).ToList();
			int PndListCnt = (from Pap in CommCtx.objtPaperLaidV
							  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							  join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							  where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
							  && (Pap.DeptActivePaperId == null)
							  orderby tempP.PaperLaidId
							  select new tCommitteeModel
							  {
							  }).ToList().Count;

			//var PndList = (from Pap in CommCtx.objtPaperLaidV
			//               join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
			//               join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
			//               join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
			//               join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
			//               join minis in CommCtx.objmMinistryMinister on Pap.MinistryId equals minis.MinistryID
			//               join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
			//               where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
			//               //&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
			//               orderby tempP.PaperLaidId
			//               select new tCommitteeModel
			//               {
			//                   FileName = tempP.FileName,
			//                   FilePath = tempP.FilePath,
			//                   paperLaidId = tempP.PaperLaidId,
			//                   PaperLaidIdTemp = tempP.PaperLaidTempId,
			//                   EventName = evnt.EventName,
			//                   CommitteeName = comm.CommitteeName,
			//                   ChairPersonName = mem.Name,
			//                   MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
			//                   DepartmentName = Dept.deptname,
			//                   Title = Pap.Title,

			//               }).ToList();

			CommCtx.Close();
			//Obj.ComModel = PndList;
			Obj.EventList = CategoryList;
			Obj.ComDraftCount = PndListCnt;



			return Obj;
		}

		public static object SubmitPending(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel CM = param as tCommitteeModel;
			tPaperLaidTemp Obj = new tPaperLaidTemp();
			tPaperLaidV Obj2 = new tPaperLaidV();
			string msg = "";
			try
			{
				Obj = CommCtx.objtPaperLaidTemp.Single(m => m.PaperLaidTempId == CM.PaperLaidIdTemp);

				Obj.CommtSubmittedBy = CM.SubmittedBy;
				Obj.CommtSubmittedDate = DateTime.Now;
				CommCtx.SaveChanges();

				Obj2 = CommCtx.objtPaperLaidV.Single(m => m.PaperLaidId == Obj.PaperLaidId);
				Obj2.CommitteeActivePaeperId = Obj.PaperLaidTempId;
				CommCtx.SaveChanges();

				msg = "Success";

			}
			catch (Exception ex)
			{
				msg = ex.Message;
			}
			CommCtx.Close();
			return msg;
		}

		public static object GetRecordForEdit(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			SessionContext SCtx = new SessionContext();
			int Id = (int)param;
			tCommitteeModel Obj = new tCommitteeModel();

			/*Get Existing data */

			var PndList = (from tempP in CommCtx.objtPaperLaidTemp
						   join Pap in CommCtx.objtPaperLaidV on tempP.PaperLaidId equals Pap.PaperLaidId
						   join CHM in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals CHM.MemberCode
						   where tempP.PaperLaidTempId == Id
						   select new tCommitteeModel
						   {
							   AssemblyId = Pap.AssemblyId,
							   SessionId = Pap.SessionId,
							   paperLaidId = tempP.PaperLaidId,
							   PaperLaidIdTemp = tempP.PaperLaidTempId,
							   EventId = Pap.EventId,
							   CommitteeId = Pap.CommitteeId,
							   MemberId = Pap.CommitteeChairmanMemberId,
							   ChairPersonName = CHM.Name,
							   MinistryId = Pap.MinistryId,
							   DeptId = Pap.DepartmentId,
							   DesireLayDateId = Pap.DesireLayingDateId,
							   DocumentTypeId = Pap.PaperTypeID,
							   Title = Pap.Title,
							   Description = Pap.Description,
							   ProUnder = Pap.ProvisionUnderWhich,
							   Remark = Pap.Remark,
							   FilePath = tempP.FilePath,
							   FileName = tempP.FileName,
							   ActualFileName = tempP.FilePath + tempP.FileName,
							   version = tempP.Version

						   }).FirstOrDefault();
			Obj = PndList;

			/*Get All Session date according to session id */
			var SDt = (from SD in SCtx.mSessDate
					   where SD.SessionId == Obj.SessionId
					   select SD).ToList();

			foreach (var item in SDt)
			{
				item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

			}
			Obj.SessDateList = SDt;

			/*Get All Event or Category*/

			var EvtList = (from Evnt in CommCtx.objmEvent
						   where Evnt.PaperCategoryTypeId == 3
						   select Evnt).ToList();
			Obj.EventList = EvtList;

			/*Get All Committee*/
			var CmtList = (from cmt in CommCtx.objtcommittee
						   select cmt).ToList();
			Obj.CommList = CmtList;

			///*Get All Chairperson*/
			//var ChairPer = (from ChrPersn in CommCtx.objmMember
			//                select ChrPersn).ToList();
			//Obj.MemList = ChairPer;

			/*Get All DoctType*/
			var DocList = (from Doct in CommCtx.objmDocument
						   select Doct).ToList();
			Obj.DocList = DocList;
			/*Get All MinistryMinister*/
			var MinMisList = (from Min in CommCtx.objmMinistry
							  join minis in CommCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
							  select new mMinisteryMinisterModel
							  {
								  MinistryID = minis.MinistryID,
								  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
							  }).ToList();
			Obj.memMinList = MinMisList;



			/*Get All Department*/
			var DeptList = (from minsDept in CommCtx.objMinisDepart
							join dept in CommCtx.objmDepartment on minsDept.DeptID equals dept.deptId
							where minsDept.MinistryID == Obj.MinistryId
							select new tCommitteeModel
							{
								DeptId = dept.deptId,
								DepartmentName = dept.deptname

							}).ToList();
			CommCtx.Close();
			Obj.ComModel = DeptList;

			return Obj;
		}

		public static object GetSubmitted()
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = new tCommitteeModel();

			var CategoryList = (from C in CommCtx.objmEvent
								where C.PaperCategoryTypeId == 3
								select C).ToList();
			int SubListCnt = (from Pap in CommCtx.objtPaperLaidV
							  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							  join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							  where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
							  && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
							  orderby tempP.PaperLaidId
							  select new tCommitteeModel
							  {
							  }).ToList().Count;

			//var PndList = (from Pap in CommCtx.objtPaperLaidV
			//               join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
			//               join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
			//               join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
			//               join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
			//               join minis in CommCtx.objmMinistryMinister on Pap.MinistryId equals minis.MinistryID
			//               join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
			//               where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
			//               //&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
			//               orderby tempP.PaperLaidId
			//               select new tCommitteeModel
			//               {
			//                   FileName = tempP.FileName,
			//                   FilePath = tempP.FilePath,
			//                   paperLaidId = tempP.PaperLaidId,
			//                   PaperLaidIdTemp = tempP.PaperLaidTempId,
			//                   EventName = evnt.EventName,
			//                   CommitteeName = comm.CommitteeName,
			//                   ChairPersonName = mem.Name,
			//                   MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
			//                   DepartmentName = Dept.deptname,
			//                   SubmittedDate = tempP.DeptSubmittedDate,
			//                   version = tempP.Version,
			//                   Title = Pap.Title
			//               }).ToList();
			CommCtx.Close();
			Obj.EventList = CategoryList;
			//Obj.ComModel = PndList;
			Obj.ComSentCount = SubListCnt;

			return Obj;
		}

		public static object DeletePending(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			int Id = (int)param;
			tPaperLaidTemp ObjPtemp = new tPaperLaidTemp();
			tPaperLaidV ObjPVS = new tPaperLaidV();
			string Msg = "";
			try
			{
				/*Get Existing data */

				ObjPtemp = (from tempP in CommCtx.objtPaperLaidTemp
							where tempP.PaperLaidTempId == Id
							select tempP).FirstOrDefault();


				if (ObjPtemp != null)
				{

					ObjPVS = (from PV in CommCtx.objtPaperLaidV
							  where PV.PaperLaidId == ObjPtemp.PaperLaidId
							  select PV).FirstOrDefault();

					CommCtx.objtPaperLaidV.Remove(ObjPVS);
					CommCtx.objtPaperLaidTemp.Remove(ObjPtemp);
					CommCtx.SaveChanges();
					Msg = "Success";
				}
			}

			catch (Exception ex)
			{
				Msg = ex.Message;
			}

			CommCtx.Close();
			return Msg;
		}


		public static object ViewDetails(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			PaperMovementContainerModel Obj = param as PaperMovementContainerModel;


			Obj = (from Pap in CommCtx.objtPaperLaidV
				   join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
				   join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
				   join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
				   join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
				   join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
				   join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
				   join Doct in CommCtx.objmDocument on Pap.PaperTypeID equals Doct.DocumentTypeID
				   join Sdt in CommCtx.objSessDate on Pap.DesireLayingDateId equals Sdt.Id

				   where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
					&& (Pap.CommitteeActivePaeperId == null)
						   && (tempP.PaperLaidId == Obj.paperLaidId)


				   select new PaperMovementContainerModel
				   {
					   AssemblyId = Pap.AssemblyId,
					   SessionId = Pap.SessionId,
					   FileName = tempP.FileName,
					   FilePath = tempP.FilePath,
					   paperLaidId = tempP.PaperLaidId,
					   PaperLaidIdTemp = tempP.PaperLaidTempId,
					   EventName = evnt.EventName,
					   CommitteeName = comm.CommitteeName,
					   ChairPersonName = mem.Name,
					   MinisterName = minis.MinisterName + ", " + minis.MinistryName,
					   DepartmentName = Dept.deptname,
					   Title = Pap.Title,
					   Description = Pap.Description,
					   ProUnder = Pap.ProvisionUnderWhich,
					   Remark = Pap.Remark,
					   DoctName = Doct.DocumentType,
					   SubmittedDate = tempP.CommtSubmittedDate,
					   version = tempP.Version,
					   DesireLayDate = Sdt.SessionDate
				   }).FirstOrDefault();
			//}

			CommCtx.Close();

			return Obj;
		}


		public static object SendViewDetails(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			PaperMovementContainerModel Obj = param as PaperMovementContainerModel;

			//if (Obj.ActionType == "PendingDetails")
			//{

			//    Obj = (from Pap in CommCtx.objtPaperLaidV
			//           join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
			//           join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
			//           join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
			//           join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
			//           join minis in CommCtx.objmMinistryMinister on Pap.MinistryId equals minis.MinistryID
			//           join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
			//           join Doct in CommCtx.objmDocument on Pap.PaperTypeID equals Doct.DocumentTypeID
			//           join SeeD in CommCtx.objSessDate on Pap.SessionId equals SeeD.SessionId
			//           where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
			//           && (tempP.PaperLaidTempId == Obj.PaperLaidIdTemp)
			//           select new PaperMovementContainerModel
			//           {
			//               FileName = tempP.FileName,
			//               FilePath = tempP.FilePath,
			//               SubmittedDate = SeeD.SessionDate,
			//               paperLaidId = tempP.PaperLaidId,
			//               PaperLaidIdTemp = tempP.PaperLaidTempId,
			//               EventName = evnt.EventName,
			//               CommitteeName = comm.CommitteeName,
			//               ChairPersonName = mem.Name,
			//               MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
			//               DepartmentName = Dept.deptname,
			//               Title = Pap.Title,
			//               Description = Pap.Description,
			//               ProUnder = Pap.ProvisionUnderWhich,
			//               Remark = Pap.Remark,
			//               DoctName = Doct.DocumentType,
			//               version = tempP.Version
			//           }).FirstOrDefault();
			//}
			//else if (Obj.ActionType == "SubmitDetails")
			//{
			Obj = (from Pap in CommCtx.objtPaperLaidV
				   join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
				   join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
				   join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
				   join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
				   join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
				   join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
				   join Doct in CommCtx.objmDocument on Pap.PaperTypeID equals Doct.DocumentTypeID
				   join Sdt in CommCtx.objSessDate on Pap.DesireLayingDateId equals Sdt.Id
				   where //(tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
				   (Pap.PaperLaidId == Obj.paperLaidId)
					&& Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId

				   select new PaperMovementContainerModel
				   {
					   AssemblyId = Pap.AssemblyId,
					   SessionId = Pap.SessionId,
					   FileName = tempP.FileName,
					   FilePath = tempP.FilePath,
					   paperLaidId = tempP.PaperLaidId,
					   PaperLaidIdTemp = tempP.PaperLaidTempId,
					   EventName = evnt.EventName,
					   CommitteeName = comm.CommitteeName,
					   ChairPersonName = mem.Name,
					   MinisterName = minis.MinisterName + ", " + minis.MinistryName,
					   DepartmentName = Dept.deptname,
					   Title = Pap.Title,
					   Description = Pap.Description,
					   ProUnder = Pap.ProvisionUnderWhich,
					   Remark = Pap.Remark,
					   DoctName = Doct.DocumentType,
					   SubmittedDate = tempP.CommtSubmittedDate,
					   version = tempP.Version,
					   DesireLayDate = Sdt.SessionDate
				   }).FirstOrDefault();
			//}

			CommCtx.Close();

			return Obj;
		}

		public static object CommitteeChairPendingViewDetails(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			PaperMovementContainerModel Obj = param as PaperMovementContainerModel;

			//if (Obj.ActionType == "PendingDetails")
			//{

			//    Obj = (from Pap in CommCtx.objtPaperLaidV
			//           join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
			//           join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
			//           join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
			//           join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
			//           join minis in CommCtx.objmMinistryMinister on Pap.MinistryId equals minis.MinistryID
			//           join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
			//           join Doct in CommCtx.objmDocument on Pap.PaperTypeID equals Doct.DocumentTypeID
			//           join SeeD in CommCtx.objSessDate on Pap.SessionId equals SeeD.SessionId
			//           where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
			//           && (tempP.PaperLaidTempId == Obj.PaperLaidIdTemp)
			//           select new PaperMovementContainerModel
			//           {
			//               FileName = tempP.FileName,
			//               FilePath = tempP.FilePath,
			//               SubmittedDate = SeeD.SessionDate,
			//               paperLaidId = tempP.PaperLaidId,
			//               PaperLaidIdTemp = tempP.PaperLaidTempId,
			//               EventName = evnt.EventName,
			//               CommitteeName = comm.CommitteeName,
			//               ChairPersonName = mem.Name,
			//               MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
			//               DepartmentName = Dept.deptname,
			//               Title = Pap.Title,
			//               Description = Pap.Description,
			//               ProUnder = Pap.ProvisionUnderWhich,
			//               Remark = Pap.Remark,
			//               DoctName = Doct.DocumentType,
			//               version = tempP.Version
			//           }).FirstOrDefault();
			//}
			//else if (Obj.ActionType == "SubmitDetails")
			//{
			Obj = (from Pap in CommCtx.objtPaperLaidV
				   join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
				   join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
				   join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
				   join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
				   join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
				   join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
				   join Doct in CommCtx.objmDocument on Pap.PaperTypeID equals Doct.DocumentTypeID
				   join Sdt in CommCtx.objSessDate on Pap.DesireLayingDateId equals Sdt.Id
				   where //(tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
				   (Pap.PaperLaidId == Obj.paperLaidId)
				   && Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId
				   select new PaperMovementContainerModel
				   {
					   AssemblyId = Pap.AssemblyId,
					   SessionId = Pap.SessionId,
					   FileName = tempP.FileName,
					   FilePath = tempP.FilePath,
					   paperLaidId = tempP.PaperLaidId,
					   PaperLaidIdTemp = tempP.PaperLaidTempId,
					   EventName = evnt.EventName,
					   CommitteeName = comm.CommitteeName,
					   ChairPersonName = mem.Name,
					   MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
					   DepartmentName = Dept.deptname,
					   Title = Pap.Title,
					   Description = Pap.Description,
					   ProUnder = Pap.ProvisionUnderWhich,
					   Remark = Pap.Remark,
					   DoctName = Doct.DocumentType,
					   SubmittedDate = tempP.CommtSubmittedDate,
					   version = tempP.Version,
					   DesireLayDate = Sdt.SessionDate
				   }).FirstOrDefault();
			//}

			CommCtx.Close();

			return Obj;
		}

		public static object ViewDetailsForReplacePaper(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;

			Obj.ComModel = (from Pap in CommCtx.objtPaperLaidV
							join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							join Doct in CommCtx.objmDocument on Pap.PaperTypeID equals Doct.DocumentTypeID
							where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
							&& (tempP.PaperLaidId == Obj.paperLaidId)
							 && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
							select new tCommitteeModel
							{
								AssemblyId = Pap.AssemblyId,
								SessionId = Pap.SessionId,
								FileName = tempP.FileName,
								FilePath = tempP.FilePath,
								paperLaidId = tempP.PaperLaidId,
								PaperLaidIdTemp = tempP.PaperLaidTempId,
								EventName = evnt.EventName,
								CommitteeName = comm.CommitteeName,
								ChairPersonName = mem.Name,
								MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
								DepartmentName = Dept.deptname,
								Title = Pap.Title,
								Description = Pap.Description,
								ProUnder = Pap.ProvisionUnderWhich,
								Remark = Pap.Remark,
								DoctName = Doct.DocumentType,
								SubmittedDate = tempP.CommtSubmittedDate,
								version = tempP.Version
							}).ToList();

			int i = 0;
			foreach (var mod in Obj.ComModel)
			{
				if (i == 0)
				{
					Obj.FilePath = mod.FilePath;
					Obj.paperLaidId = mod.paperLaidId;
					Obj.AssemblyId = mod.AssemblyId;
					Obj.SessionId = mod.SessionId;
					Obj.version = mod.version;
					i++;
				}
			}
			return Obj;
		}
		public static object SearchByCategory(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;


			if (Obj.ActionType == "Pending")
			{
				IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
														join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
														join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
														join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
														join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
														join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
														where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
														&& (Pap.DeptActivePaperId == null)
														&& (Pap.CommitteeId != null)
														orderby tempP.PaperLaidId
														select new tCommitteeModel
														{
															FileName = tempP.FilePath + tempP.FileName,
															FilePath = tempP.FilePath,
															paperLaidId = Pap.PaperLaidId,
															PaperName = "Committee",
															PaperLaidIdTemp = tempP.PaperLaidTempId,
															EventName = evnt.EventName,
															CommitteeName = comm.CommitteeName,
															MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
															DepartmentName = Dept.deptname,
															Title = Pap.Title,
															Description = Pap.Description,
															ProUnder = Pap.ProvisionUnderWhich,
															Remark = Pap.Remark,
															Status = (int)CommitteeStatus.Draft,
															version = tempP.Version,
															DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault()
														}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();




				Obj.ResultCount = PndList.Count();
				Obj.ComModel = PndList.ToList();
			}
			else if (Obj.ActionType == "Submitted")
			{
				IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
														join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
														join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
														join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
														join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
														join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
														where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
														&& (Pap.CommitteeId != null)
														&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
														orderby tempP.PaperLaidId
														select new tCommitteeModel
														{
															FileName = tempP.FilePath + tempP.FileName,
															FilePath = tempP.FilePath,
															SignedFilePath = tempP.SignedFilePath,
															paperLaidId = Pap.PaperLaidId,
															PaperLaidIdTemp = tempP.PaperLaidTempId,
															EventName = evnt.EventName,
															CommitteeName = comm.CommitteeName,
															MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
															DepartmentName = Dept.deptname,
															version = tempP.Version,
															Status = (int)CommitteeStatus.Draft,
															Title = Pap.Title,
															DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
															SubmittedDate = tempP.CommtSubmittedDate,
														}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

				Obj.ResultCount = PndList.Count();
				Obj.ComModel = PndList.ToList();

				foreach (var item in PndList)
				{
					Obj.SignedFilePath = item.FilePath + item.FileName;
					Obj.Status = item.Status;
				}

			}

			CommCtx.Close();
			return Obj;

		}

		public static object FileUploadPopUp(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;

			var S = (from Pap in CommCtx.objtPaperLaidV
					 join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
					 where (tempP.PaperLaidId == Obj.paperLaidId)
					 select new tCommitteeModel
					 {
						 AssemblyId = Pap.AssemblyId,
						 SessionId = Pap.SessionId,
						 paperLaidId = tempP.PaperLaidId,
					 }).FirstOrDefault();


			var Allfile = (from Pap in CommCtx.objtPaperLaidV
						   join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
						   join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
						   where (tempP.PaperLaidId == Obj.paperLaidId)
						   orderby tempP.PaperLaidId
						   select new tCommitteeModel
						   {
							   FileName = tempP.FileName,
							   FilePath = tempP.FilePath,
							   paperLaidId = tempP.PaperLaidId,
							   PaperLaidIdTemp = tempP.PaperLaidTempId,
							   EventName = evnt.EventName,
							   SubmittedDate = tempP.CommtSubmittedDate,
							   version = tempP.Version,
						   }).ToList();

			CommCtx.Close();

			Obj.ComModel = Allfile;
			Obj.paperLaidId = S.paperLaidId;
			Obj.AssemblyId = S.AssemblyId;
			Obj.SessionId = S.SessionId;
			return Obj;
		}

		public static object GetLatestVersion(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel comObj = param as tCommitteeModel;
			int? V = (from temp in CommCtx.objtPaperLaidTemp
					  where (temp.PaperLaidId == comObj.paperLaidId)
					  orderby temp.Version descending
					  select temp.Version).FirstOrDefault();

			V = V + 1;

			return V;
		}
		public static object AddLatestFile(object param)
		{

			CommitteeContext CommCtx = new CommitteeContext();
			tPaperLaidV PLMdl = new tPaperLaidV();
			tPaperLaidTemp PLTemp = new tPaperLaidTemp();
			tCommitteeModel comObj = param as tCommitteeModel;

			var msg = "";
			try
			{
				//int? V = (from temp in CommCtx.objtPaperLaidTemp
				//          where (temp.PaperLaidId == comObj.paperLaidId)
				//          orderby temp.Version descending
				//          select temp.Version).FirstOrDefault();

				//V = V + 1;

				/*Insert in PaperLaidTemp */
				PLTemp.PaperLaidId = comObj.paperLaidId;
				PLTemp.Version = comObj.version;
				PLTemp.FilePath = comObj.FilePath;
				PLTemp.FileName = comObj.AssemblyId + "_" + comObj.SessionId + "_C_" + comObj.paperLaidId + "_V" + comObj.version + ".pdf";
				PLTemp.CommtSubmittedBy = comObj.SubmittedBy;
				PLTemp.CommtSubmittedDate = DateTime.Now;
				CommCtx.objtPaperLaidTemp.Add(PLTemp);
				CommCtx.SaveChanges();

				/*Add Active Id in PaperLaidVs*/
				PLMdl = CommCtx.objtPaperLaidV.Single(m => m.PaperLaidId == comObj.paperLaidId);
				PLMdl.CommitteeActivePaeperId = PLTemp.PaperLaidTempId;
				CommCtx.SaveChanges();

				msg = "Saved Successfully !";

				CommCtx.Close();

			}
			catch (Exception ex)
			{
				msg = ex.Message;
			}

			return msg;

		}
		#endregion


		#region NewAdded

		public static object GetPendingsChairPerson()
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = new tCommitteeModel();

			var CategoryList = (from C in CommCtx.objmEvent
								where C.PaperCategoryTypeId == 3
								select C).ToList();
			int PndListCnt = (from Pap in CommCtx.objtPaperLaidV
							  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							  join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							  where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
							  && (Pap.DeptActivePaperId == null)
							  orderby tempP.PaperLaidId
							  select new tCommitteeModel
							  {
							  }).ToList().Count;


			CommCtx.Close();
			Obj.EventList = CategoryList;
			Obj.ComDraftCount = PndListCnt;
			return Obj;
		}




		static List<tCommitteeModel> GetCommitteePaperAttachmentByIds(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			string obj = param as string;
			string[] obja = obj.Split(',');
			List<long> ids = new List<long>();
			foreach (string item in obja)
			{
				ids.Add(Convert.ToInt16(item));
			}

			var query = (from ministerModel in CommCtx.objtPaperLaidV
						 join ministryModel in CommCtx.objtPaperLaidTemp on ministerModel.PaperLaidId equals ministryModel.PaperLaidId
						 //where ministerModel.DeptActivePaperId == ministryModel.PaperLaidTempId
						 where ids.Contains(ministerModel.PaperLaidId)

						 select new tCommitteeModel
						 {
							 FilePath = ministryModel.FilePath,
							 FileName = ministryModel.FileName,
							 DeptActivePaperId = ministerModel.DeptActivePaperId,

						 });


			return query.ToList();
		}

		public static object SendingPendingForSubCommitteeByType(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeSendModel Obj = param as tCommitteeSendModel;


			if (Obj.ActionType == "Pending")
			{
				IEnumerable<tCommitteeSendModel> PndList = (from Pap in CommCtx.objtPaperLaidV
															join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
															join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
															join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
															join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
															join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
															join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
															where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
															&& (Obj.EventId == 0 || Pap.EventId == Obj.EventId)
															&& (Pap.DeptActivePaperId == null)
															orderby tempP.PaperLaidId
															select new tCommitteeSendModel
															{

																FileName = tempP.FileName,
																FilePath = tempP.FilePath,
																paperLaidId = Pap.PaperLaidId,
																PaperName = "Committee",
																PaperLaidIdTemp = tempP.PaperLaidTempId,
																EventName = evnt.EventName,
																CommitteeName = comm.CommitteeName,
																ChairPersonName = mem.Name,
																MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
																DepartmentName = Dept.deptname,
																Title = Pap.Title,
																Description = Pap.Description,
																ProUnder = Pap.ProvisionUnderWhich,
																Remark = Pap.Remark,
																Status = (int)CommitteeStatus.Draft,
																version = tempP.Version,
															}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();


				Obj.ResultCount = PndList.Count();
				Obj.SendComModel = PndList.ToList();

				foreach (var item in PndList)
				{
					Obj.paperLaidId = item.paperLaidId;
					Obj.AssemblyId = item.AssemblyId;
					Obj.SessionId = item.SessionId;
					Obj.Status = item.Status;
				}
			}
			else if (Obj.ActionType == "Submitted")
			{
				IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
														join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
														join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
														join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
														join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
														join minis in CommCtx.objmMinistryMinister on Pap.MinistryId equals minis.MinistryID
														join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
														where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
														&& (Obj.EventId == 0 || Pap.EventId == Obj.EventId)
														&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
														orderby tempP.PaperLaidId
														select new tCommitteeModel
														{
															FileName = tempP.FileName,
															FilePath = tempP.FilePath,
															paperLaidId = Pap.PaperLaidId,
															PaperLaidIdTemp = tempP.PaperLaidTempId,
															EventName = evnt.EventName,
															CommitteeName = comm.CommitteeName,
															ChairPersonName = mem.Name,
															MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
															DepartmentName = Dept.deptname,
															Title = Pap.Title,
															Status = (int)CommitteeStatus.Draft,
															SubmittedDate = tempP.CommtSubmittedDate,
														}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

				Obj.ResultCount = PndList.Count();
				Obj.ComModel = PndList.ToList();


			}

			CommCtx.Close();
			return Obj;

		}

		public static object SendingPendingForSubCommitteeChairByType(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeSendModel Obj = param as tCommitteeSendModel;





			if (Obj.ActionType == "Submitted")
			{
				IEnumerable<tCommitteeSendModel> PndList = (from Pap in CommCtx.objtPaperLaidV
															join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
															join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
															join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
															join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
															join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
															join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
															where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
															  && tempP.MinisterSubmittedDate != null
															&& (Obj.EventId == 0 || Pap.EventId == Obj.EventId)
														  && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
															orderby tempP.PaperLaidId
															select new tCommitteeSendModel
															{

																FileName = tempP.FilePath + tempP.FileName,
																FilePath = tempP.FilePath,
																paperLaidId = Pap.PaperLaidId,
																PaperName = "Committee",
																PaperLaidIdTemp = tempP.PaperLaidTempId,
																EventName = evnt.EventName,
																CommitteeName = comm.CommitteeName,
																ChairPersonName = mem.Name,
																MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
																DepartmentName = Dept.deptname,
																Title = Pap.Title,
																Description = Pap.Description,
																ProUnder = Pap.ProvisionUnderWhich,
																Remark = Pap.Remark,
																Status = (int)CommitteeStatus.Draft,
																version = tempP.Version,
															}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();


				Obj.ResultCount = PndList.Count();
				Obj.SendComModel = PndList.ToList();

				foreach (var item in PndList)
				{
					Obj.paperLaidId = item.paperLaidId;
					Obj.AssemblyId = item.AssemblyId;
					Obj.SessionId = item.SessionId;
					Obj.Status = item.Status;
					Obj.version = item.version;
				}
			}
			if (Obj.ActionType == "Pending")
			{
				IEnumerable<tCommitteeSendModel> PndList = (from Pap in CommCtx.objtPaperLaidV
															join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
															join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
															join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
															join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
															join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
															join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
															where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
															&& tempP.MinisterSubmittedDate == null
															&& (Obj.EventId == 0 || Pap.EventId == Obj.EventId)
															&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
															orderby tempP.PaperLaidId
															select new tCommitteeSendModel
															{
																FileName = tempP.FilePath + tempP.FileName,
																FilePath = tempP.FilePath,
																paperLaidId = Pap.PaperLaidId,
																PaperLaidIdTemp = tempP.PaperLaidTempId,
																EventName = evnt.EventName,
																CommitteeName = comm.CommitteeName,
																ChairPersonName = mem.Name,
																version = tempP.Version,
																MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
																DepartmentName = Dept.deptname,
																Title = Pap.Title,
																Status = (int)CommitteeStatus.Draft,
																SubmittedDate = tempP.CommtSubmittedDate,
															}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

				Obj.ResultCount = PndList.Count();
				Obj.SendComModel = PndList.ToList();
				foreach (var item in PndList)
				{
					Obj.paperLaidId = item.paperLaidId;
					Obj.AssemblyId = item.AssemblyId;
					Obj.SessionId = item.SessionId;
					Obj.Status = item.Status;
				}

			}

			CommCtx.Close();
			return Obj;

		}

		public static object SearchChairByCategory(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;


			if (Obj.ActionType == "Pending")
			{
				IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
														join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
														join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
														join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
														join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
														join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
														where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
														&& (Pap.DeptActivePaperId == null)
														&& (Pap.CommitteeId != null)
														orderby tempP.PaperLaidId
														select new tCommitteeModel
														{
															FileName = tempP.FileName,
															FilePath = tempP.FilePath,
															paperLaidId = Pap.PaperLaidId,
															PaperName = "Committee",
															PaperLaidIdTemp = tempP.PaperLaidTempId,
															EventName = evnt.EventName,
															CommitteeName = comm.CommitteeName,
															MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
															DepartmentName = Dept.deptname,
															Title = Pap.Title,
															Description = Pap.Description,
															ProUnder = Pap.ProvisionUnderWhich,
															Remark = Pap.Remark,
															Status = (int)CommitteeStatus.Draft,
															version = tempP.Version,
															DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault()
														}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();




				Obj.ResultCount = PndList.Count();
				Obj.ComModel = PndList.ToList();
			}
			else if (Obj.ActionType == "Submitted")
			{

				IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
														join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
														join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
														join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
														join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
														join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId


														where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
														&& tempP.MinisterSubmittedDate != null
														&& (Pap.CommitteeId != null)
														&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
														orderby tempP.PaperLaidId
														select new tCommitteeModel
														{
															FileName = tempP.SignedFilePath,
															SignedFilePath = tempP.SignedFilePath,
															paperLaidId = Pap.PaperLaidId,
															PaperLaidIdTemp = tempP.PaperLaidTempId,
															EventName = evnt.EventName,
															CommitteeName = comm.CommitteeName,
															MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
															DepartmentName = Dept.deptname,
															version = tempP.Version,
															Status = (int)CommitteeStatus.Send,
															Title = Pap.Title,
															DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
															SubmittedDate = tempP.CommtSubmittedDate,
														}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

				Obj.ResultCount = PndList.Count();
				Obj.ComModel = PndList.ToList();

				foreach (var item in PndList)
				{
					Obj.SignedFilePath = item.FileName;
					Obj.Status = item.Status;
				}

			}

			CommCtx.Close();
			return Obj;

		}

		public static object CommitteeUpcomingLOB(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;

			IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
													join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
													join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
													join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
													join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
													join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId

													where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
													&& tempP.MinisterSubmittedDate != null
													&& (Pap.CommitteeId != null)
													&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
													&& Pap.LOBRecordId != null
													&& Pap.LaidDate == null
													&& Pap.DesireLayingDate > DateTime.Now
													orderby tempP.PaperLaidId
													select new tCommitteeModel
													{
														FileName = tempP.SignedFilePath,
														SignedFilePath = tempP.SignedFilePath,
														paperLaidId = Pap.PaperLaidId,
														PaperLaidIdTemp = tempP.PaperLaidTempId,
														EventName = evnt.EventName,
														CommitteeName = comm.CommitteeName,
														MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
														DepartmentName = Dept.deptname,
														version = tempP.Version,
														Status = (int)CommitteeStatus.Send,
														Title = Pap.Title,
														DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
														SubmittedDate = tempP.CommtSubmittedDate,
													}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

			Obj.ResultCount = PndList.Count();
			Obj.ComModel = PndList.ToList();

			foreach (var item in PndList)
			{
				Obj.SignedFilePath = item.FileName;
				Obj.Status = item.Status;
			}



			CommCtx.Close();
			return Obj;

		}

		public static object CommitteeLaidinHouse(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;

			IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
													join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
													join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
													join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
													join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
													join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
													where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
													&& tempP.MinisterSubmittedDate != null
													&& (Pap.CommitteeId != null)
													&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
													&& Pap.LaidDate <= DateTime.Now
													&& Pap.LaidDate != null
													&& Pap.LOBRecordId != null
													orderby tempP.PaperLaidId
													select new tCommitteeModel
													{
														FileName = tempP.SignedFilePath,
														SignedFilePath = tempP.SignedFilePath,
														paperLaidId = Pap.PaperLaidId,
														PaperLaidIdTemp = tempP.PaperLaidTempId,
														EventName = evnt.EventName,
														CommitteeName = comm.CommitteeName,
														MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
														DepartmentName = Dept.deptname,
														version = tempP.Version,
														Status = (int)CommitteeStatus.Send,
														Title = Pap.Title,
														DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
														SubmittedDate = tempP.CommtSubmittedDate,
													}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

			Obj.ResultCount = PndList.Count();
			Obj.ComModel = PndList.ToList();

			foreach (var item in PndList)
			{
				Obj.SignedFilePath = item.FileName;
				Obj.Status = item.Status;
			}



			CommCtx.Close();
			return Obj;

		}

		public static object CommitteePendingToLay(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;

			IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
													join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
													join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
													join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
													join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
													join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
													where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
													&& tempP.MinisterSubmittedDate != null
													&& (Pap.CommitteeId != null)
													&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
													&& Pap.DesireLayingDate > DateTime.Now
													&& Pap.LOBRecordId == null
													&& Pap.LaidDate == null
													orderby tempP.PaperLaidId
													select new tCommitteeModel
													{
														FileName = tempP.SignedFilePath,
														SignedFilePath = tempP.SignedFilePath,
														paperLaidId = Pap.PaperLaidId,
														PaperLaidIdTemp = tempP.PaperLaidTempId,
														EventName = evnt.EventName,
														CommitteeName = comm.CommitteeName,
														MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
														DepartmentName = Dept.deptname,
														version = tempP.Version,
														Status = (int)CommitteeStatus.Send,
														Title = Pap.Title,
														DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
														SubmittedDate = tempP.CommtSubmittedDate,
													}).Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

			Obj.ResultCount = PndList.Count();
			Obj.ComModel = PndList.ToList();

			foreach (var item in PndList)
			{
				Obj.SignedFilePath = item.FileName;
				Obj.Status = item.Status;
			}



			CommCtx.Close();
			return Obj;

		}

		public static object GetAllChairList(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;
			List<tCommitteeModel> NewLsitForBind = new List<tCommitteeModel>();

			//Pending List
			IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
													join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
													join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
													join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
													join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
													join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
													where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
													&& tempP.MinisterSubmittedDate == null
													&& (Obj.EventId == 0 || Pap.EventId == Obj.EventId)
													&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
													orderby tempP.PaperLaidId
													select new tCommitteeModel
													{
														PaperName = "Committee",
														FileName = tempP.FilePath + tempP.FileName,
														FilePath = tempP.SignedFilePath,
														paperLaidId = Pap.PaperLaidId,
														PaperLaidIdTemp = tempP.PaperLaidTempId,
														EventName = evnt.EventName,
														CommitteeName = comm.CommitteeName,
														version = tempP.Version,
														MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
														DepartmentName = Dept.deptname,
														Title = Pap.Title,
														SubmittedDate = tempP.CommtSubmittedDate,
														Status = (int)CommitteeStatus.Draft,
														DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),

													}).ToList();

			//var Pndresults = PndList.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

			//Submitted List




			IEnumerable<tCommitteeModel> SubList = (from Pap in CommCtx.objtPaperLaidV
													join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
													join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
													join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
													join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
													join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
													where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
													&& tempP.MinisterSubmittedDate != null
													&& (Pap.CommitteeId != null)
													&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
													orderby tempP.PaperLaidId
													select new tCommitteeModel
													{
														PaperName = "Committee",
														FileName = tempP.SignedFilePath,
														FilePath = tempP.SignedFilePath,
														paperLaidId = Pap.PaperLaidId,
														PaperLaidIdTemp = tempP.PaperLaidTempId,
														EventName = evnt.EventName,
														CommitteeName = comm.CommitteeName,
														version = tempP.Version,
														MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
														DepartmentName = Dept.deptname,
														Title = Pap.Title,
														SubmittedDate = tempP.CommtSubmittedDate,
														Status = (int)CommitteeStatus.Send,
														DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),

													}).ToList();

			//var Subresults = PndList.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();


			NewLsitForBind.AddRange(PndList);
			NewLsitForBind.AddRange(SubList);

			Obj.ResultCount = PndList.Count() + SubList.Count();
			Obj.ComModel = NewLsitForBind.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

			CommCtx.Close();
			return Obj;

		}

		public static object SubmittedCommitteeChairViewDetails(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;


			Obj = (from Pap in CommCtx.objtPaperLaidV
				   join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
				   join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
				   join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
				   join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
				   join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
				   join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
				   join Doct in CommCtx.objmDocument on Pap.PaperTypeID equals Doct.DocumentTypeID
				   join Sdt in CommCtx.objSessDate on Pap.DesireLayingDateId equals Sdt.Id
				   where //(tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
				   (Pap.PaperLaidId == Obj.paperLaidId)
				   && Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId

				   select new tCommitteeModel
				   {
					   AssemblyId = Pap.AssemblyId,
					   SessionId = Pap.SessionId,
					   FileName = tempP.SignedFilePath,
					   FilePath = tempP.FilePath,
					   paperLaidId = tempP.PaperLaidId,
					   PaperLaidIdTemp = tempP.PaperLaidTempId,
					   EventName = evnt.EventName,
					   CommitteeName = comm.CommitteeName,
					   ChairPersonName = mem.Name,
					   MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
					   DepartmentName = Dept.deptname,
					   Title = Pap.Title,
					   Description = Pap.Description,
					   ProUnder = Pap.ProvisionUnderWhich,
					   Remark = Pap.Remark,
					   DoctName = Doct.DocumentType,
					   SubmittedDate = tempP.CommtSubmittedDate,
					   version = tempP.Version,
					   DesireLayDate = Sdt.SessionDate
				   }).FirstOrDefault();
			//}

			CommCtx.Close();

			return Obj;
		}

		#endregion


		//where (m.SessionId == s.SessionId && m.AssemblyId==s.AssemblyId)
		// SessionContext db = new SessionContext();
		// mSession model = param as mSession;
		// var query = (from dist in db.mSession
		// orderby dist.SessionCode descending
		//   where dist.AssemblyID == model.AssemblyID
		//  select dist);

		//  return query.ToList();




		public static object InsertSignPathAttachmentCommittee(object param)
		{
			tPaperLaidTemp pT = param as tPaperLaidTemp;
			string[] obja = pT.FilePath.Split(',');

			foreach (var item in obja)
			{
				using (var obj1 = new CommitteeContext())
				{
					long id = Convert.ToInt16(item);



					var PaperLaidObj = (from PaperLaidModel in obj1.objtPaperLaidV
										join ministryModel in obj1.objtPaperLaidTemp on PaperLaidModel.PaperLaidId equals ministryModel.PaperLaidId
										where PaperLaidModel.PaperLaidId == id
										&& PaperLaidModel.CommitteeActivePaeperId == ministryModel.PaperLaidTempId
										select ministryModel).FirstOrDefault();
					if (PaperLaidObj != null)
					{
						var filename = PaperLaidObj.FileName;
						string[] Afile = filename.Split('.');
						filename = Afile[0];
						var path = "/PaperLaid/" + pT.AssemblyId + "/" + pT.SessionId + "/Signed/" + filename + "_signed.pdf";
						PaperLaidObj.SignedFilePath = path;
						PaperLaidObj.MinisterSubmittedDate = pT.CommtSubmittedDate;

					}

					obj1.SaveChanges();
					obj1.Close();
				}
			}

			string meg = "";
			return meg;
		}

		public static object UpdateDepartmentMinisterActivePaperIdCommittee(object param)
		{
			string obj = param as string;
			string[] obja = obj.Split(',');

			foreach (var item in obja)
			{

				using (var obj1 = new CommitteeContext())
				{
					long id = Convert.ToInt16(item);

					var PaperLaidObj = (from PaperLaidModel in obj1.objtPaperLaidV
										where PaperLaidModel.PaperLaidId == id
										select PaperLaidModel).FirstOrDefault();

					var PaperLaidTempObj = (from PaperLaidModel in obj1.objtPaperLaidV
											join ministryModel in obj1.objtPaperLaidTemp on PaperLaidModel.PaperLaidId equals ministryModel.PaperLaidId
											where PaperLaidModel.PaperLaidId == id
											// && PaperLaidModel.CommitteeActivePaeperId == ministryModel.PaperLaidTempId
											select ministryModel).FirstOrDefault();

					if (PaperLaidObj != null)
					{
						//PaperLaidObj.DeptActivePaperId = PaperLaidTempObj.PaperLaidTempId;
						PaperLaidObj.CommitteeActivePaeperId = PaperLaidTempObj.PaperLaidTempId;
						PaperLaidObj.MinisterActivePaperId = PaperLaidTempObj.PaperLaidTempId;

					}
					//obj.tBillRegisterVs.Add(model);
					obj1.SaveChanges();
					obj1.Close();
				}
			}


			string meg = "";
			return meg;
		}


		public static object GetChairman(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;


			var ChairmanList = (from CMem in CommCtx.objCommMem
								join M in CommCtx.objmMember on CMem.MemberId equals M.MemberCode
								where CMem.IsReportChairMan == true
								select new tCommitteeModel()
								{
									ChairPersonName = M.Name,
									MemberId = M.MemberCode

								}).ToList();
			return ChairmanList;
		}


		public static object GetInitialCount(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel ComObj = param as tCommitteeModel;

			int PndListCnt = (from Pap in CommCtx.objtPaperLaidV
							  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							  join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							  where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
							  && (Pap.DeptActivePaperId == null)
							  orderby tempP.PaperLaidId
							  select new tCommitteeModel
							  {
							  }).ToList().Count;

			int SubListCnt = (from Pap in CommCtx.objtPaperLaidV
							  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							  join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							  where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
							  && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
							  orderby tempP.PaperLaidId
							  select new tCommitteeModel
							  {
							  }).ToList().Count;

			int UpcomingLOB = (from Pap in CommCtx.objtPaperLaidV
							   join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
							   join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
							   join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
							   join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
							   join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
							   join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
							   where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
							   && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
							   && Pap.LOBRecordId != null
							   && Pap.LaidDate == null
							   && Pap.DesireLayingDate > DateTime.Now
							   && tempP.MinisterSubmittedDate != null
							   orderby tempP.PaperLaidId
							   select new tCommitteeModel
							   {
							   }).ToList().Count;

			int LaidintheHouse = (from Pap in CommCtx.objtPaperLaidV
								  join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
								  join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
								  join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
								  join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
								  join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
								  join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
								  where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
								  && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
								  && Pap.LOBRecordId != null
								  && Pap.LaidDate != null
								  && Pap.DesireLayingDate <= DateTime.Now
								  && tempP.MinisterSubmittedDate != null
								  orderby tempP.PaperLaidId
								  select new tCommitteeModel
								  {
								  }).ToList().Count;

			int PendingToLay = (from Pap in CommCtx.objtPaperLaidV
								join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
								join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
								join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
								join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
								join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
								join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
								where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
								&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
								&& Pap.LOBRecordId == null
								&& Pap.LaidDate == null
								&& Pap.DesireLayingDate > DateTime.Now
								&& tempP.MinisterSubmittedDate != null
								orderby tempP.PaperLaidId
								select new tCommitteeModel
								{
								}).ToList().Count;


			ComObj.ComDraftCount = PndListCnt;
			ComObj.ComSentCount = SubListCnt;
			ComObj.ComUpcomingCount = UpcomingLOB;
			ComObj.ComLaidIntheHouseCount = LaidintheHouse;
			ComObj.ComPendingtoLayCount = PendingToLay;


			return ComObj;
		}

		public static object GetAllList(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;
			List<tCommitteeModel> NewLsitForBind = new List<tCommitteeModel>();

			//Pending List
			IEnumerable<tCommitteeModel> PndList = (from Pap in CommCtx.objtPaperLaidV
													join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
													join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
													join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
													join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
													join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
													join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
													where (tempP.CommtSubmittedBy == null || tempP.CommtSubmittedBy == "") && (tempP.CommtSubmittedDate == null)
													&& (Obj.EventId == 0 || Pap.EventId == Obj.EventId)
													//&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
													orderby tempP.PaperLaidId
													select new tCommitteeModel
													{
														PaperName = "Committee",
														FileName = tempP.FilePath + tempP.FileName,
														FilePath = tempP.FilePath,
														paperLaidId = Pap.PaperLaidId,
														PaperLaidIdTemp = tempP.PaperLaidTempId,
														EventName = evnt.EventName,
														CommitteeName = comm.CommitteeName,
														ChairPersonName = mem.Name,
														MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
														DepartmentName = Dept.deptname,
														Title = Pap.Title,
														version = tempP.Version,
														Description = Pap.Description,
														Status = (int)CommitteeStatus.Draft,
														Remark = Pap.Remark,
														ProUnder = Pap.ProvisionUnderWhich,
														DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
														//   SubmittedDate=tempP.CommtSubmittedDate
													}).ToList();

			//var Pndresults = PndList.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

			//Submitted List
			IEnumerable<tCommitteeModel> SubList = (from Pap in CommCtx.objtPaperLaidV
													join tempP in CommCtx.objtPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
													join evnt in CommCtx.objmEvent on Pap.EventId equals evnt.EventId
													join comm in CommCtx.objtcommittee on Pap.CommitteeId equals comm.CommitteeId
													join mem in CommCtx.objmMember on Pap.CommitteeChairmanMemberId equals mem.MemberCode
													join minis in CommCtx.objmMinistry on Pap.MinistryId equals minis.MinistryID
													join Dept in CommCtx.objmDepartment on Pap.DepartmentId equals Dept.deptId
													where (tempP.CommtSubmittedBy != null || tempP.CommtSubmittedBy != "") && (tempP.CommtSubmittedDate != null)
													&& (Obj.EventId == 0 || Pap.EventId == Obj.EventId)
													&& (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
													orderby tempP.PaperLaidId
													select new tCommitteeModel
													{
														PaperName = "Committee",
														FileName = tempP.FilePath + tempP.FileName,
														FilePath = tempP.FilePath,
														paperLaidId = Pap.PaperLaidId,
														PaperLaidIdTemp = tempP.PaperLaidTempId,
														EventName = evnt.EventName,
														CommitteeName = comm.CommitteeName,
														version = tempP.Version,
														ChairPersonName = mem.Name,
														MinisterName = minis.MinisterName,// + ", " + mm.MinistryName,
														DepartmentName = Dept.deptname,
														Title = Pap.Title,
														Status = (int)CommitteeStatus.Send,
														SubmittedDate = tempP.CommtSubmittedDate,
														DesireLayDate = Pap.DesireLayingDateId == null ? (DateTime?)null : (from sessionDates in CommCtx.objSessDate where sessionDates.Id == Pap.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),

													}).ToList();

			//var Subresults = PndList.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

			foreach (var item in SubList)
			{
				Obj.version = item.version;
				Obj.AssemblyCode = item.AssemblyCode;
				Obj.SessionCode = item.SessionCode;
				Obj.version = item.version;
				Obj.Status = item.Status;

			}

			NewLsitForBind.AddRange(PndList);
			NewLsitForBind.AddRange(SubList);

			Obj.ResultCount = PndList.Count() + SubList.Count();
			Obj.ComModel = NewLsitForBind.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();

			CommCtx.Close();
			return Obj;

		}
		//for Assembly 


		//public static object GetAllAssemblyReverseList(object param)
		//{


		//    CommitteeContext CommCtx = new CommitteeContext();
		//    tCommitteeReport obj = param as tCommitteeReport;

		//    var query = (from CMem in CommCtx.tcomrep
		//                 join M in CommCtx.objtcommittee on CMem.CommitteeId equals M.CommitteeId
		//                 where CMem.CommitteeId == obj.CommitteeId
		//                 join a in CommCtx.mAssembly on CMem.AssemblyId equals a.AssemblyCode
		//                 orderby a.AssemblyName descending
		//                 select new CommitteeSearchModel
		//                 {
		//                     AssemblyID = a.AssemblyCode,
		//                     AssemblyName = a.AssemblyName
		//                 }

		//                 ).Distinct().ToList();

		//    //var query = (from CMem in CommCtx.tcomrep
		//    //             join M in CommCtx.objtcommittee on CMem.CommitteeId equals M.CommitteeId
		//    //             where CMem.CommitteeId == obj.CommitteeId
		//    //             join a in CommCtx.mAssembly on CMem.AssemblyId equals a.AssemblyID
		//    //             orderby CMem.AssemblyId
		//    //             select a.AssemblyName).Distinct().ToList();


		//    return query;

		//}
		public static object GetAllPeriods(object param)
		{


			CommitteeContext CommCtx = new CommitteeContext();

			var TitleList = (from Doct in CommCtx.tcomrep
							 select Doct.Period).Distinct().ToList();










			return TitleList;
		}

		public static object GetSearchedCommitteeGetTitleList(object param)
		{


			CommitteeContext CommCtx = new CommitteeContext();

			var TitleList = (from Doct in CommCtx.tcomrep
							 select Doct.Title).Distinct().ToList();

			return TitleList;
		}



		public static object GetAllTitleList(object param)
		{


			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeReport obj = param as tCommitteeReport;
			var query = (from CMem in CommCtx.tcomrep
						 join M in CommCtx.objtcommittee on CMem.CommitteeId equals M.CommitteeId
						 where CMem.CommitteeId == obj.CommitteeId
						 orderby CMem.Title descending
						 select CMem.Title).Distinct().ToList();


			return query;

		}





		//for dateoflaying

		//public static object GetAllReportNoList(object param)
		//{


		//    CommitteeContext CommCtx = new CommitteeContext();
		//    tCommitteeReport obj = param as tCommitteeReport;
		//    var query = (from CMem in CommCtx.tcomrep
		//                 join M in CommCtx.objtcommittee on CMem.CommitteeId equals M.CommitteeId
		//                 where CMem.CommitteeId == obj.CommitteeId
		//                 orderby CMem.ReportNo descending
		//                 select CMem.ReportNo).ToList();


		//    return query;

		//}



		public static object GetAllReportNoList(object param)
		{


			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeReport obj = param as tCommitteeReport;
			var query = (from CMem in CommCtx.tcomrep
						 join M in CommCtx.objtcommittee on CMem.CommitteeId equals M.CommitteeId
						 where CMem.CommitteeId == obj.CommitteeId
						 orderby CMem.ReportNo descending
						 select new CommitteeSearchModel
						 {
							 ReportNo = CMem.ReportNo,
							 OriginalReportNo = CMem.OriginalReportNo
						 }

						 ).Distinct().ToList();


			return query;

		}

		public static List<CommitteeSearchModel> GetReportNoList(object param)
		{


			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeReport obj = param as tCommitteeReport;
			var query = (from CMem in CommCtx.tcomrep
						 orderby CMem.ReportNo descending
						 select new CommitteeSearchModel
						 {
							 ReportNo = CMem.ReportNo,
							 OriginalReportNo = CMem.OriginalReportNo
						 }

						 ).Distinct().ToList();


			return query;

		}

		//for period


		public static object GetAllPeriodList(object param)
		{


			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeReport obj = param as tCommitteeReport;
			var query = (from CMem in CommCtx.tcomrep
						 join M in CommCtx.objtcommittee on CMem.CommitteeId equals M.CommitteeId
						 where CMem.CommitteeId == obj.CommitteeId
						 orderby CMem.Period descending
						 select CMem.Period).ToList();


			return query;

		}

		//static object GetCommitteSubjects_key(object param)
		//{
		//    var subjectKey = param as string;
		//    CommitteeContext db = new CommitteeContext();
		//    var qSubjects = (from Questions in db.tcomrep
		//                     where (Questions.Subject.ToUpper().StartsWith(subjectKey.Trim().ToUpper()))
		//                     orderby Questions.Subject descending
		//                     select Questions.Subject).Distinct().Take(10).ToList();
		//    return qSubjects;
		//}

		public static object GetAllChairmanList(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();


			var name = (from CMem in CommCtx.objCommMem
						join M in CommCtx.objmMember on CMem.MemberId equals M.MemberCode
						where CMem.IsReportChairMan == true
						orderby CMem.MemberId
						select new CommitteeSearchModel()
						{
							ChairmanName = M.Name,
							MemberID = M.MemberCode

						}).Distinct().ToList();
			return name;
		}

		public static object GetAllReporttypeList(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeReport Obj = param as tCommitteeReport;



			var name = (from CMem in CommCtx.tcomrep
						join M in CommCtx.tcomreptype on CMem.ReportTypeId equals M.ReportTypeId


						where CMem.CommitteeId == Obj.CommitteeId
						orderby CMem.CommitteeId descending

						select M).Distinct().ToList();



			//join a in CommCtx.tcomreptype on CMem.ReportTypeId equals a.ReportTypeId orderby CMem.CommitteeId
			//   select a.ReportTypeName).ToList();




			return name;
		}







		public static object GetReporttypeList(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			var ReporttypeList = (from Doct in CommCtx.tcomreptype
								  select Doct).Distinct().ToList();






			//join a in CommCtx.tcomreptype on CMem.ReportTypeId equals a.ReportTypeId orderby CMem.CommitteeId
			//   select a.ReportTypeName).ToList();




			return ReporttypeList;
		}



		//static object GetAllDateOfLaying(object param)
		//{
		//    CommitteeContext CommCtx = new CommitteeContext();
		//    tCommitteeReport Obj = param as tCommitteeReport;
		//  /////////////////////////
		//    //where (d.CommitteeId == Obj.CommitteeId) 
		//    var DateList = (from d in CommCtx.tcomrep

		//                select d).ToList();




		//    //var DateList = (from m in CommCtx.tcomrep
		//    //                       where (m.CommitteeId == Obj.CommitteeId)
		//    //                       select m).ToList();
		//    List<tCommitteeReport> ListSession = new List<tCommitteeReport>();


		//    foreach (var item in DateList)
		//    {
		//        tCommitteeReport test = new tCommitteeReport();

		//        test.DateOfLaying = item.DateOfLaying;
		//        test.CommitteeId = item.CommitteeId;
		//        ListSession.Add(test);
		//    }


		//    return ListSession;
		//}

		//static object GetAllDateOfLaying(object param)
		//{
		//    CommitteeContext CommCtx = new CommitteeContext();
		//    tCommitteeReport obj = param as tCommitteeReport;
		//    var query =
		//        (from dt in
		//             (from CMem in CommCtx.tcomrep
		//              join M in CommCtx.objtcommittee on CMem.CommitteeId equals M.CommitteeId


		//              where CMem.CommitteeId == obj.CommitteeId
		//              orderby CMem.DateOfLaying.ToString("dd/MM/yyyy")
		//              select CMem.DateOfLaying).ToList()
		//         select string.Format("{0:MM/dd/yyyy}", dt)).ToList();

		//    return query;
		//}

		public static object GetSearchedCommittee(object param)
		{
			CommitteeContext db = new CommitteeContext();
			var model = param as CommitteeSearchModel;

			var predicate = PredicateBuilder.True<CommitteeSearchModel>();
			if (model.AssemblyID != 0)
				predicate = predicate.And(q => q.AssemblyCode == model.AssemblyID);
			if (model.CommitteeId != 0)
				predicate = predicate.And(q => q.CommitteeId == model.CommitteeId);
			if (model.ReportTypeName != null)
				predicate = predicate.And(q => q.ReportTypeName == model.ReportTypeName);
			if (model.MemberID != 0)
				predicate = predicate.And(q => q.ChairmanName == model.ChairmanName);
			if (model.MemberID != 0)
				predicate = predicate.And(q => q.MemberID == model.MemberID);
			if (model.ReportNo != null)
				predicate = predicate.And(q => q.ReportNo == model.ReportNo);
			if (model.Title != null)
				predicate = predicate.And(q => q.Title == model.Title);
			if (model.FromDate != null)
				predicate = predicate.And(q => q.DateOfCreation >= model.FromDate);
			if (model.ToDate != null)
				predicate = predicate.And(q => q.DateOfCreation <= model.ToDate);
			//if (model.Fromperiod!= null)
			//    predicate = predicate.And(q => q.Period >= model.Fromperiod);
			//if (model.Toperiod != null)
			//    predicate = predicate.And(q => q.Period <= model.Toperiod);
			if (model.SessionId != 0)
				predicate = predicate.And(q => q.SessionId == model.SessionId);

			//Subject = ((starredQuestions.EditedDescription.Trim() != null && starredQuestions.EditedDescription.Trim() != "") ? starredQuestions.EditedDescription.Substring(starredQuestions.EditedDescription.IndexOf("("), starredQuestions.EditedDescription.IndexOf("-")) : ""),
			//if (model != null)
			//    predicate = predicate.And(q => q.SessionCode == model.SessionCode);



			{
				var cResults = (from Crept in db.tcomrep
								select new CommitteeSearchModel
								{
									AssemblyID = (from Assemblies in db.mAssembly
												  join com in db.objtcommittee on Assemblies.AssemblyID equals com.AssemblyID

												  select Assemblies.AssemblyID).FirstOrDefault(),
									SessionId = (from Sessions in db.mSession
												 join com in db.objtcommittee on
													 Sessions.SessionCode equals com.SessionId
												 select Sessions.SessionID).FirstOrDefault(),
									CommitteeId = Crept.CommitteeId,
									CommitteeTypeId = (from committype in db.mCommitteeType
													   join Com in db.objtcommittee on committype.CommitteeTypeId equals Com.CommitteeTypeId
													   select committype
														   .CommitteeTypeId).FirstOrDefault(),

									AssemblyCode = (from Assemblies in db.mAssembly
													join com in db.objtcommittee on Assemblies.AssemblyID equals com.AssemblyID

													select Assemblies.AssemblyID).FirstOrDefault(),
									AssemblyName = (from Assemblies in db.mAssembly
													join com in db.objtcommittee on Assemblies.AssemblyID equals com.AssemblyID

													select Assemblies.AssemblyName).FirstOrDefault(),

									SessionName = (from Sessions in db.mSession
												   join com in db.objtcommittee on
													   Sessions.SessionCode equals com.SessionId
												   select Sessions.SessionName).FirstOrDefault(),

									CommitteeName = (from Comt in db.objtcommittee
													 where Comt.CommitteeId == Crept.CommitteeId
													 select Comt.CommitteeName).FirstOrDefault(),
									Title = Crept.Title,
									DateOfCreation = Crept.DateOfLaying,
									OriginalReportNo = Crept.OriginalReportNo,
									ReportNo = Crept.ReportNo,
									CommitteeTypeName = (from committype in db.mCommitteeType join Com in db.objtcommittee on committype.CommitteeTypeId equals Com.CommitteeTypeId select committype.CommitteeTypeName).FirstOrDefault(),
									MemberName = (from CMem in db.objCommMem
												  join M in db.objmMember on CMem.MemberId equals M.MemberCode
												  where CMem.CommitteeId == Crept.CommitteeId
												  select M.Name).FirstOrDefault(),
									ChairmanName = (from CMem in db.objCommMem
													join M in db.objmMember on CMem.MemberId equals M.MemberCode
													where CMem.CommitteeId == Crept.CommitteeId
													&& CMem.IsReportChairMan == true
													select M.Name).FirstOrDefault(),
									ReportTypeName = (from CMem in db.tcomrep
													  join M in db.tcomreptype on CMem.ReportTypeId equals M.ReportTypeId
													  select M.ReportTypeName).FirstOrDefault(),

									PDFPath = Crept.PDFPath,

								}).AsExpandable().Where(predicate).ToList();
				model.objList = cResults;
			}

			return model;
		}

		// MemberContext MCtx = new MemberContext();

		// mAssembly m = param as mAssembly;

		//var M = (from mem in MCtx.mMembers
		//join MemAssembly in MCtx.mMemberAssembly on mem.MemberCode equals MemAssembly.MemberID
		//where MemAssembly.AssemblyID == m.AssemblyID
		//select mem).ToList();
		//return M;
		// from CMem in db.objCommMem
		//              join M in db.objmMember on CMem.MemberId equals M.MemberCode
		//              where CMem.CommitteeId == Crept.CommitteeId
		//              select M.Name
		// GetAllMembersforCommittee

		public static object GetAllMembersforCommittee(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			CommitteeSearchModel obj = param as CommitteeSearchModel;

			var name = (from CMem in CommCtx.objCommMem
						join M in CommCtx.objmMember on CMem.MemberId equals M.MemberCode
						where CMem.CommitteeId == obj.CommitteeId
						orderby CMem.MemberId
						select M.Name).Distinct().ToList();
			return name;
		}

		public static object GetChairmanByCommId(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			tCommitteeModel Obj = param as tCommitteeModel;


			var name = (from CMem in CommCtx.objCommMem
						join M in CommCtx.objmMember on CMem.MemberId equals M.MemberCode
						where CMem.CommitteeId == Obj.CommitteeId
						&& CMem.IsReportChairMan == true
						select new tCommitteeModel()
						{
							ChairPersonName = M.Name,
							MemberId = M.MemberCode

						}).FirstOrDefault();
			return name;
		}

		#region "Common Method"

		public string ConvertSQLDate(DateTime? dt)
		{
			DateTime ConvtDate = Convert.ToDateTime(dt);
			string month = ConvtDate.Month.ToString();
			if (month.Length < 2)
			{
				month = "0" + month;
			}
			string day = ConvtDate.Day.ToString();
			string year = ConvtDate.Year.ToString();
			string Date = day + "/" + month + "/" + year;
			return Date;
		}

		public static DateTime? ConvertUIDate(string dt)
		{
			//dt = Sanitizer.GetSafeHtmlFragment(dt);
			string Day = dt.Substring(0, dt.IndexOf("/"));
			string Month = dt.Substring(dt.IndexOf("/") + 1, (dt.LastIndexOf("/") - dt.IndexOf("/") - 1));
			string Year = dt.Substring(dt.LastIndexOf("/") + 1, 4);
			DateTime ConvtDate = new DateTime(Convert.ToInt16(Year), Convert.ToInt16(Month), Convert.ToInt16(Day));

			return ConvtDate;
		}

		#endregion

		#region Added by Bhadri
		static object UpdateCommittee(object param)
		{
			if (null == param)
			{
				return null;
			}
			using (CommitteeContext db = new CommitteeContext())
			{
				mCommitteeType model = param as mCommitteeType;
				model.CreatedDate = DateTime.Now;
				model.ModifiedDate = DateTime.Now;
				db.mCommitteeType.Attach(model);
				db.Entry(model).State = EntityState.Modified;
				db.SaveChanges();
				db.Close();
			}
			return GetAllCommiteeType();
		}
		private static object GetCommitteById(object param)
		{
			int Id = Convert.ToInt32(param);
			CommitteeContext CommCtx = new CommitteeContext();
			var Res = (from e in CommCtx.mCommitteeType where e.CommitteeTypeId == Id select e).FirstOrDefault();
			CommCtx.Close();
			return Res;

		}

		private static object CreateCommiteeType(object param)
		{
			CommitteeContext db = new CommitteeContext();
			mCommitteeType model = param as mCommitteeType;
			model.CreatedDate = DateTime.Now;
			model.ModifiedDate = DateTime.Now;
			db.mCommitteeType.Add(model);
			db.SaveChanges();
			return model;
		}


		static List<mCommitteeType> GetAllCommiteeType()
		{
			CommitteeContext db = new CommitteeContext();
			var data = (from e in db.mCommitteeType
						orderby e.CommitteeTypeId descending
						select e);
			return data.ToList();

		}
		static object DeleteCommitteeType(object param)
		{
			if (null == param)
			{
				return null;
			}
			using (CommitteeContext db = new CommitteeContext())
			{
				mCommitteeType parameter = param as mCommitteeType;
				mCommitteeType committeeToRemove = db.mCommitteeType.SingleOrDefault(a => a.CommitteeTypeId == parameter.CommitteeTypeId);
				if (committeeToRemove != null)
				{
					committeeToRemove.IsDeleted = true;
				}
				//db.mCommitteeType.Remove(committeeToRemove);
				db.SaveChanges();
				db.Close();
			}
			return GetAllCommiteeType();
		}
		#endregion


		#region Added By Robin Committee type permission
		public static object SaveNewCommitteType(object param)
		{
			try
			{
				using (CommitteeContext context = new CommitteeContext())
				{
					CommitteeTypePermission model = param as CommitteeTypePermission;
					context.CommitteeTypePermission.Add(model);
					context.SaveChanges();
					context.Close();
				}

				return null;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static object GetAllUsers()
		{
			try
			{
				CommitteeContext CommitteContext = new CommitteeContext();
				var data = (from list in CommitteContext.mUsers
							join a in CommitteContext.AdhaarDetails on list.AadarId equals a.AdhaarID
							where list.IsActive == true
							select new
							{
								UserId = list.UserId,
								UserName = a.Name + "( " + a.AdhaarID + " )"
							}

							).ToList();

				List<mUsers> mdl = new List<mUsers>();
				foreach (var item in data)
				{
					mUsers d = new mUsers();
					d.UserId = item.UserId;
					d.UserName = item.UserName;
					mdl.Add(d);
				}

				return mdl;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static object GetAllCommiteeTypeForPermission()
		{
			try
			{
				CommitteeContext CommitteContext = new CommitteeContext();
				var data = (from list in CommitteContext.mCommitteeType
							select list).ToList();
				return data;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static object GetUserCommitteePermissionList()
		{
			try
			{
				CommitteeContext db = new CommitteeContext();
				var data = (from list in db.CommitteeTypePermission
							join user in db.mUsers on list.UserId equals user.UserId
							join aa in db.AdhaarDetails on user.AadarId equals aa.AdhaarID
							join comm in db.objtcommittee on list.CommitteeTypeId equals comm.CommitteeId
							where list.IsActive == true
							select new
							{
								CommitteeTypePermId = list.CommitteeTypePermId,
								UserId = list.UserId,
								CommitteeTypeId = list.CommitteeTypeId,
								Username = aa.Name + "(" + aa.AdhaarID + ")",
								CommitteeTypeName = comm.CommitteeName
							}).OrderBy(a => a.Username).ThenBy(a => a.CommitteeTypeName).ToList();
				List<CommitteeTypePermission> result = new List<CommitteeTypePermission>();
				if (data != null)
				{
					foreach (var x in data)
					{
						CommitteeTypePermission obj = new CommitteeTypePermission();
						obj.CommitteeTypePermId = x.CommitteeTypePermId;
						obj.UserId = x.UserId;
						obj.CommitteeTypeId = x.CommitteeTypeId;
						obj.Username = x.Username;
						obj.CommitteeTypeName = x.CommitteeTypeName;
						result.Add(obj);
					}
				}
				return result;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static object DeleteCommitteePermissionType(object param)
		{
			try
			{
				CommitteeContext context = new CommitteeContext();
				// CommitteeTypePermission id = param as CommitteeTypePermission;
				int a = Convert.ToInt16(param);
				var data = (from list in context.CommitteeTypePermission
							where list.CommitteeTypePermId == a
							select list).FirstOrDefault();
				data.IsActive = false;
				context.Entry(data).State = EntityState.Modified;
				context.SaveChanges();
				context.Close();
				return null;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static object CheckNewCommitteType1(object param)
		{
			try
			{
				CommitteeContext context = new CommitteeContext();
				CommitteeTypePermission checkdata = param as CommitteeTypePermission;
				//int a = Convert.ToInt16(param);
				var data = (from list in context.CommitteeTypePermission
							where list.UserId == checkdata.UserId && list.CommitteeTypeId == checkdata.CommitteeTypeId && list.IsActive == true
							select list).FirstOrDefault();
				return data;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static object CheckNewCommitteType2(object param)
		{
			try
			{
				CommitteeContext context = new CommitteeContext();
				CommitteeTypePermission checkdata = param as CommitteeTypePermission;
				//int a = Convert.ToInt16(param);
				var data = (from list in context.CommitteeTypePermission
							where list.UserId == checkdata.UserId && list.CommitteeTypeId == checkdata.CommitteeTypeId && list.IsActive == false
							select list).FirstOrDefault();
				return data;
			}
			catch (Exception)
			{

				throw;
			}
		}

		public static object SaveNewCommitteTypeIsActive(object param)
		{
			try
			{
				CommitteeContext context = new CommitteeContext();
				CommitteeTypePermission value = param as CommitteeTypePermission;
				//int a = Convert.ToInt16(param);
				var data = (from list in context.CommitteeTypePermission
							where list.UserId == value.UserId && list.CommitteeTypeId == value.CommitteeTypeId
							select list).FirstOrDefault();
				data.IsActive = true;
				context.Entry(data).State = EntityState.Modified;
				context.SaveChanges();
				context.Close();
				return null;
			}
			catch (Exception)
			{

				throw;
			}
		}
		#endregion

		#region Committee Module By Shashi
		// Committee Module By Shashi
        //static object GetAllCommitteeByMemberPermission(object param)
        //{
        //    Guid UserID = new Guid();
        //    Guid.TryParse(param as string, out UserID);
        //    using (CommitteeContext comcontext = new CommitteeContext())
        //    {

        //        var val = (from a in comcontext.CommitteeTypePermission
        //                   where a.UserId == UserID
        //                   select a.CommitteeTypeId).Count();

        //        if (val == 0)
        //        {
        //            var comList = (from data in comcontext.objtcommittee
        //                           join assembly in comcontext.mAssembly on data.AssemblyID equals assembly.AssemblyID
        //                           // join comType in comcontext.mCommitteeType on data.CommitteeTypeId equals comType.CommitteeTypeId

        //                           //join pdf in comcontext.CommitteeMembersUpload on data.CommitteeId equals pdf.CommitteeId
        //                           //into gorup_leftjoin
        //                           //from pdf in gorup_leftjoin.DefaultIfEmpty()

        //                           where data.ParentId == 0  //&& pdf.CommStatus == 1
        //                           select new CommitteesListView
        //                           {
        //                               AssemblyName = assembly.AssemblyName,
        //                               CommitteeType = "",
        //                               CommitteeId = data.CommitteeId,
        //                               CommitteeName = data.CommitteeName,
        //                               DateOfCreation = data.DateOfCreation,
        //                               ParentId = data.ParentId
        //                               //CommFilePath = data.CommFilePath
        //                           }).ToList();
        //            //List<tCommittee> mdl=new List<tCommittee> ();
        //            //foreach (var item in comList)
        //            return comList;
        //        }
        //        else
        //        {
        //            var perm = (from a in comcontext.CommitteeTypePermission
        //                        where a.UserId == UserID
        //                        select a.CommitteeTypeId).ToArray();
        //            var comList = (from data in comcontext.objtcommittee
        //                           join assembly in comcontext.mAssembly on data.AssemblyID equals assembly.AssemblyID
        //                           // join comType in comcontext.mCommitteeType on data.CommitteeTypeId equals comType.CommitteeTypeId

        //                           //join pdf in comcontext.CommitteeMembersUpload on data.CommitteeId equals pdf.CommitteeId
        //                           //into gorup_leftjoin
        //                           //from pdf in gorup_leftjoin.DefaultIfEmpty()

        //                           where data.ParentId == 0 && (perm).Contains(data.CommitteeId) //&& pdf.CommStatus == 1
        //                           select new CommitteesListView
        //                           {
        //                               AssemblyName = assembly.AssemblyName,
        //                               CommitteeType = "",
        //                               CommitteeId = data.CommitteeId,
        //                               CommitteeName = data.CommitteeName,
        //                               DateOfCreation = data.DateOfCreation,
        //                               ParentId = data.ParentId
        //                               //CommFilePath = data.CommFilePath
        //                           }).ToList();
        //            //List<tCommittee> mdl=new List<tCommittee> ();
        //            //foreach (var item in comList)
        //            return comList;
        //        }

				
        //    }
        //}
        static object GetAllCommitteeByMemberPermission(object param)
        {
            Guid UserID = new Guid();
            Guid.TryParse(param as string, out UserID);
            using (CommitteeContext comcontext = new CommitteeContext())
            {

                var q = (from assemblyid in comcontext.SiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);


                var val = (from a in comcontext.CommitteeTypePermission
                           where a.UserId == UserID
                           select a.CommitteeTypeId).Count();

                if (val == 0)
                {
                    var comList = (from data in comcontext.objtcommittee
                                   join assembly in comcontext.mAssembly on data.AssemblyID equals assembly.AssemblyID
                                   // join comType in comcontext.mCommitteeType on data.CommitteeTypeId equals comType.CommitteeTypeId

                                   //join pdf in comcontext.CommitteeMembersUpload on data.CommitteeId equals pdf.CommitteeId
                                   //into gorup_leftjoin
                                   //from pdf in gorup_leftjoin.DefaultIfEmpty()

                                   where data.ParentId == 0 && assembly.AssemblyCode == aid//&& pdf.CommStatus == 1
                                   select new CommitteesListView
                                   {
                                       AssemblyName = assembly.AssemblyName,
                                       CommitteeType = "",
                                       CommitteeId = data.CommitteeId,
                                       CommitteeName = data.CommitteeName,
                                       DateOfCreation = data.DateOfCreation,
                                       ParentId = data.ParentId
                                       //CommFilePath = data.CommFilePath
                                   }).ToList();
                    //List<tCommittee> mdl=new List<tCommittee> ();
                    //foreach (var item in comList)
                    return comList;
                }
                else
                {
                    var perm = (from a in comcontext.CommitteeTypePermission
                                where a.UserId == UserID
                                select a.CommitteeTypeId).ToArray();
                    var comList = (from data in comcontext.objtcommittee
                                   join assembly in comcontext.mAssembly on data.AssemblyID equals assembly.AssemblyID
                                   // join comType in comcontext.mCommitteeType on data.CommitteeTypeId equals comType.CommitteeTypeId

                                   //join pdf in comcontext.CommitteeMembersUpload on data.CommitteeId equals pdf.CommitteeId
                                   //into gorup_leftjoin
                                   //from pdf in gorup_leftjoin.DefaultIfEmpty()

                                   where data.ParentId == 0 && (perm).Contains(data.CommitteeId) //&& pdf.CommStatus == 1
                                   select new CommitteesListView
                                   {
                                       AssemblyName = assembly.AssemblyName,
                                       CommitteeType = "",
                                       CommitteeId = data.CommitteeId,
                                       CommitteeName = data.CommitteeName,
                                       DateOfCreation = data.DateOfCreation,
                                       ParentId = data.ParentId
                                       //CommFilePath = data.CommFilePath
                                   }).ToList();
                    //List<tCommittee> mdl=new List<tCommittee> ();
                    //foreach (var item in comList)
                    return comList;
                }


            }
        }
		static object GetAllCommitteeDetByID(object param)
		{
			int CommID = Convert.ToInt32(param);
			using (CommitteeContext comcontext = new CommitteeContext())
			{

				var comList = (from data in comcontext.objtcommittee
							   join assembly in comcontext.mAssembly on data.AssemblyID equals assembly.AssemblyID
							   join comType in comcontext.mCommitteeType on data.CommitteeTypeId equals comType.CommitteeTypeId
							   where data.ParentId == 0 && data.CommitteeId == CommID
							   select new CommitteesListView
							   {
								   AssemblyName = assembly.AssemblyName,
								   CommitteeType = comType.CommitteeTypeName,
								   CommitteeId = data.CommitteeId,
								   CommitteeName = data.CommitteeName,
								   DateOfCreation = data.DateOfCreation,
								   ParentId = data.ParentId
							   }).ToList();

				return comList;
			}
		}

		static object GetCommitteeDetailsByCommitteeId(object param)
		{
			int id = Convert.ToInt32(param);
			using (CommitteeContext comcontext = new CommitteeContext())
			{
				var query = (from a in comcontext.objtcommittee
							 where a.CommitteeId == id
							 select a).FirstOrDefault();

				return query;
			}

		}
		static object GetCommitteeMemberPDFListByCommitteeId(object param)
		{
			int id = Convert.ToInt32(param);
			using (CommitteeContext comcontext = new CommitteeContext())
			{
				var query = (from a in comcontext.CommitteeMembersUpload
							 where a.CommitteeId == id && a.CommStatus == 1
							 select a).FirstOrDefault();

				return query;
			}

		}
		static object DeletCommitteeMemberByCommitteeID(object param)
		{
			int id = Convert.ToInt32(param);
			using (CommitteeContext comcontext = new CommitteeContext())
			{
				var query = (from a in comcontext.objCommMem
							 where a.CommitteeId == id
							 select a).ToList().Count();

				if (query > 0)
				{
					tCommittee committee = comcontext.objtcommittee.SingleOrDefault(value => value.CommitteeId == id);
					comcontext.objtcommittee.Remove(committee);
					comcontext.SaveChanges();

					return 1;
				}

				return 0;
			}

		}

		public static object GetAllMembersByCommitteeIdPDF(object param)
		{
			CommitteeContext CommCtx = new CommitteeContext();
			int id = Convert.ToInt32(param as string);

			var query = (from CMem in CommCtx.objCommMem
						 join M in CommCtx.objmMember on CMem.MemberId equals M.MemberID
						 join c in CommCtx.objtcommittee on CMem.CommitteeId equals c.CommitteeId
						 where CMem.CommitteeId == id
						 select new
						 {
							 CommitteeId = CMem.CommitteeId,
							 CommitteeName = c.CommitteeNameLocal,
							 MemberName = M.NameLocal,
							 IsChairman = CMem.IsReportChairMan,

						 }).OrderBy(a => a.CommitteeId).OrderByDescending(a => a.IsChairman).ToList();

			List<tCommitteeMemberList> mdlList = new List<tCommitteeMemberList>();

			foreach (var item in query)
			{
				tCommitteeMemberList m = new tCommitteeMemberList();
				m.CommitteeId = item.CommitteeId;
				m.CommitteeNameLocal = item.CommitteeName;
				m.CommitteMemberName = item.MemberName;
				m.IsChairMan = item.IsChairman;
				mdlList.Add(m);

			}
			return mdlList;
		}

		static object DeleteGeneratedMemberPDfByID(object param)
		{
			try
			{
				int id = Convert.ToInt32(param);
				using (CommitteeContext comcontext = new CommitteeContext())
				{
					var itemremove = comcontext.CommitteeMembersUpload.SingleOrDefault(m => m.CommUploadID == id);
					comcontext.CommitteeMembersUpload.Remove(itemremove);
					comcontext.SaveChanges();

					return 1;
				}
			}
			catch
			{
				return 0;
			}

		}

        static object GetCommitteeAuthorizeList()
        {
            try
            {
                using (CommitteeContext comcontext = new CommitteeContext())
                {
                    var list = (from a in comcontext.mAuthEmployee
                                //where a.IsDeleted!=true
                                select a).ToList();
                    return list;
                }
            }
            catch
            {
                return 0;
            }

        }
        static object Get_ActiveCommittee()
        {
            try
            {
              //  int CommID = Convert.ToInt32(param);
                using (CommitteeContext comcontext = new CommitteeContext())
                {

                    var comList = (from data in comcontext.objtcommittee
                                  // join assembly in comcontext.mAssembly on data.AssemblyID equals assembly.AssemblyID
                                  // join comType in comcontext.mCommitteeType on data.CommitteeTypeId equals comType.CommitteeTypeId
                                   where data.IsActive==true
                                   select new CommitteesListView
                                   {
                                      CommitteeId = data.CommitteeId,
                                       CommitteeName = data.CommitteeName,
                                   }).ToList();

                    return comList;
                }
            }
            catch
            {
                return 0;
            }
        
        }
        static object DataForEditCommAuth(object param)
        {
            try
            {
                var id = Convert.ToInt16(param);
                using (CommitteeContext comcontext = new CommitteeContext())
                {
                    var data = (from a in comcontext.mAuthEmployee
                                where a.ID == id
                                select a).FirstOrDefault();
                    return data;
                }
            }
            catch
            {
                return 0;
            }

        }


        static object NewCommitteAuthorize(object param)
        {
            try
            {
                AuthEmployee obj = param as AuthEmployee;
                using (CommitteeContext comcontext = new CommitteeContext())
                {
                    var list = (from a in comcontext.mAuthEmployee
                                where a.ID == obj.ID
                                select a).FirstOrDefault();

                    if (list == null)
                    {
                        obj.CreatedDate = DateTime.Now;
                        comcontext.mAuthEmployee.Add(obj);
                        comcontext.SaveChanges();
                        comcontext.Close();
                    }
                    else
                    {
                        list.AadharId = obj.AadharId;
                        list.CreatedBy = obj.CreatedBy;
                        list.CreatedDate = obj.CreatedDate;
                        list.IsActive = obj.IsActive;
                        //list.IsDeleted = obj.IsDeleted;
                        list.ModifiedBy = obj.ModifiedBy;
                        list.ModifiedDate = obj.ModifiedDate;
                        list.Name = obj.Name;
                        list.Remark = obj.Remark;
                        comcontext.Entry(list).State = EntityState.Modified;
                        comcontext.SaveChanges();
                        comcontext.Close();
                    }

                    return null;
                }
            }
            catch
            {
                return 0;
            }

        }

        public static object DeleteCommitteeAuth(object param)
        {
            try
            {
                CommitteeContext context = new CommitteeContext();

                int a = Convert.ToInt16(param);
                var data = (from list in context.mAuthEmployee
                            where list.ID == a
                            select list).FirstOrDefault();
                //data.IsDeleted = true;
                //context.Entry(data).State = EntityState.Modified;
                context.mAuthEmployee.Remove(data);
                context.SaveChanges();
                context.Close();
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        // End
		#endregion

	}


}
