﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.StaffManagement;

namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
    [Table("mBranches")]
    public partial class mBranches
    {
        public mBranches()
		{
            this.BranchList = new List<mBranches>();
            this.EmpList = new List<mStaff>();
            this.DeptList = new List<mDepartment>();
            this.MemberList = new List<HouseComModel>();
            this.OtherThanList = new List<HouseComModel>();
            
		}

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int BranchId { get; set; }
        public int BranchCode { get; set; }
        public string BranchName { get; set; }
        public string BranchLocal { get; set; }
        public int BranchOrder { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string Abbreviation { get; set; }
        [NotMapped]
        public Guid UserId { get; set; }
        public virtual ICollection<mBranches> BranchList { get; set; }
        [NotMapped]
        public List<mStaff> EmpList { get; set; }
        [NotMapped]
        public string AadharID { get; set; }
        [NotMapped]
        public string StaffName { get; set; }

        [NotMapped]
        public string Remark { get; set; }
        [NotMapped]
        public int efileID { get; set; }
        [NotMapped]
        public string EmpName { get; set; }
        [NotMapped]
        public string Remarks { get; set; }
        [NotMapped]
        public DateTime? AssignDateTime { get; set; }
         [NotMapped]
        public virtual ICollection<mDepartment> DeptList { get; set; }
         public string deptId { get; set; }
         public string deptname { get; set; }
         [NotMapped]
         public string AssignfrmAadharId { get; set; }
         [NotMapped]
         public string AssignfrmName { get; set; }
         [NotMapped]
         public string AssignfrmDesignation { get; set; }
        
         [NotMapped]
         public string EmpDesignation { get; set; }
         [NotMapped]
         public List<string> DeptIds { get; set; }
        [NotMapped]
         public int AssemblyID { get; set; }
        [NotMapped]
        public int SessionID { get; set; }
        [NotMapped]
        public int MId { get; set; }
        [NotMapped]
        public string MemberName { get; set; }
        [NotMapped]
        public int ConstituencyCode { get; set; }
        [NotMapped]
        public string ConstituencyName { get; set; }
        [NotMapped]
        public virtual ICollection<HouseComModel> MemberList { get; set; }
        [NotMapped]
        public virtual ICollection<HouseComModel> OtherThanList { get; set; }
        [NotMapped]
        public List<string> MemberIds { get; set; }
        [NotMapped]
        public List<string> OthersIds { get; set; }
        [NotMapped]
        public bool Minister { get; set; }
        [NotMapped]
        public bool Secretary { get; set; }
        [NotMapped]
        public bool HOD { get; set; }
        [NotMapped]
        public string deptnameCC { get; set; }
        [NotMapped]
        public string MemberNameCC { get; set; }
        [NotMapped]
        public string OthersNameCC { get; set; }
        [NotMapped]
        public bool Approvefile { get; set; }
        [NotMapped]
        public bool SignDraftpaper { get; set; }
        [NotMapped]
        public string Type { get; set; }
    }
}
