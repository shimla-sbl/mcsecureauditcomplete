﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
   public class PaperMovementContainerModel
    {
        public int AssemblyId { get; set; }

        public int SessionId { get; set; }
        public string AssemblyName { get; set; }
        public string SessionName { get; set; }

        public int? CommitteeId { get; set; }

        public int EventId { get; set; }

        public int? MemberId { get; set; }

        public string DeptId { get; set; }


        public string Title { get; set; }


        public string Description { get; set; }


        public string ProUnder { get; set; }

        public DateTime? DesireLayDate { get; set; }

        public int? DocumentTypeId { get; set; }


        public string Remark { get; set; }

        public int MinistryId { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string MinistryName { get; set; }
        public string MinistryNameLocal { get; set; }
        public string DepartmentName { get; set; }
        public string DepartmentNameLocal { get; set; }
        public string SubmittedBy { get; set; }
        public string Message { get; set; }
        public string EventName { get; set; }
        public int? version { get; set; }
        public int OldVersion { get; set; }
        public int CurrentVersion { get; set; }
        public string CommitteeName { get; set; }
        public string ChairPersonName { get; set; }
        public string MinisterName { get; set; }
        public long? paperLaidId { get; set; }
        public long? PaperLaidIdTemp { get; set; }
        public string ActionType { get; set; }
        public string BtnCaption { get; set; }
        public int DesireLayDateId { get; set; }
        public string DoctName { get; set; }

        public DateTime? SubmittedDate { get; set; }
        public string Status { get; set; }

        public int ComDraftCount { get; set; }
        public int ComSentCount { get; set; }
        public string UserDesignation { get; set; }
        public string CurrentUserName { get; set; }
    }
}
