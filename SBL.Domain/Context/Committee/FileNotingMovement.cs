﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
    [Table("FileNotingMovement")]
    public partial class FileNotingMovement  
    {
        public FileNotingMovement()
		{
      
		}
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int eFileAttachmentId { get; set; }
        public int BranchId { get; set; }
        public string EmpAadharId { get; set; }
        public string EmpName { get; set; }
        public string EmpDesignation { get; set; }
        public string AssignfrmAadharId { get; set; }
        public string AssignfrmName { get; set; }
        public string AssignfrmDesignation { get; set; }
        public string Noting { get; set; }
        public DateTime? AssignDateTime { get; set; }
        public int FileId { get; set; }
        [NotMapped]
        public virtual ICollection<HouseComModel> HouseComModel { get; set; }
        [NotMapped]
        public string PDFPath { get; set; }
        public int DraftId { get; set; }
    }
}
