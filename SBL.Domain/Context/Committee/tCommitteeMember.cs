﻿namespace SBL.DomainModel.Models.Committee
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tCommitteeMembers")]
    [Serializable]
    public partial class tCommitteeMember
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKey { get; set; }

        public int MemberId { get; set; }

        public int CommitteeId { get; set; }

        public DateTime? JoinedDate { get; set; }

        public DateTime? RelievedDate { get; set; }


        public bool IsReportChairMan { get; set; }
        public bool IsCommitteeChairMan { get; set; }

		public int StaffID { get; set; }

        //9-1-2016 : Added by Sujeet as requirement from client
        public int? CommitteeReportId { get; set; }

    }

    [Serializable]
    public class tCommitteeMemberList
    {
        public int IdKey { get; set; }

        public int MemberId { get; set; }

        public int CommitteeId { get; set; }

        public DateTime? JoinedDate { get; set; }

        public DateTime? RelievedDate { get; set; }

        public bool IsChairMan { get; set; }

		public bool IsChairManComm { get; set; }

		public int StaffID { get; set; }

        public string CommitteMemberName { get; set; }

        public string Mode { get; set; }

        public string CommitteeName { get; set; }

        public string CommitteeNameLocal { get; set; }

		

    }
}
