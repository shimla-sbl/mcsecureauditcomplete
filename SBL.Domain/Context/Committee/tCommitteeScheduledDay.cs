﻿namespace SBL.DomainModel.Models.Committee
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tCommitteeScheduledDays")]
    [Serializable]
    public partial class tCommitteeScheduledDay
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdKey { get; set; }

        public int CommitteeId { get; set; }

        public int ScheduledDay { get; set; }

        public TimeSpan ScheduledTime { get; set; }

        public decimal ScheduledDuration { get; set; }
    }
}
