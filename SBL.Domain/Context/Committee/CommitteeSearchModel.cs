﻿using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Session;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
  [Serializable]
    public class CommitteeSearchModel 
    {
        public CommitteeSearchModel()
        {
            this.objList = new List<CommitteeSearchModel>();
        }
        public int? CommitteeId { get; set; }
        public string AssemblyName { get; set; }
        public string SessionName { get; set; }
        public int AssemblyID { get; set; }
        public int AssemblyCode { get; set; }
        public int SessionId { get; set; }
        public int SessionCode { get; set; }
        //public int Assembl { get; set; }
        public string CommitteeType { get; set; }
        public string MemberName { get; set; }
        public int MemberID { get; set; }
        public int ParentId { get; set; }
        public int chairmanId { get; set; }
        public string CommitteeName { get; set; }
        public string CommitteeTypeName { get; set; }
        public string ChairmanName { get; set; }
        public string Title { get; set; }
        public string Period { get; set; }
        public string ReportNo { get; set; }
        public string OriginalReportNo { get; set; }
        public string PDFPath { get; set; }
        public int CommitteeTypeId { get; set; }
     
        public List<tCommitteeReportType>CommitteeReportTypeList { get; set; }

        public string Subject { get; set; }

        public int ReportTypeId { get; set; }

        public DateTime?  Date { get; set; }

        public DateTime? FromDate { get; set; }

        public DateTime? ToDate { get; set; }
        public string Fromperiod { get; set; }

        public string Toperiod { get; set; }

        public DateTime? DateOfCreation { get; set; }

        //public List<mAssembly> PeriodList { get; set; }

        public List<CommitteeSearchModel> objList { get; set; }

        public List<mAssembly> AssemblyList { get; set; }

        public List<mSession> SessionList { get; set; }

        public List<mMember> MemberList { get; set; }

        public List<CommitteeSearchModel> ChairmanList { get; set; }

        public List<tCommittee> CommitteeList { get; set; }


        public List<mCommitteeType> CommitteeTypeList { get; set; }

        public List<string> SubjectList { get; set; }

        public List<string> AllTitleList { get; set; }

        public string ReportTypeName { get; set; }

        public List<CommitteeSearchModel> ReportNoList { get; set; }

        public List<CommitteeSearchModel> OriginalReportNoList { get; set; }

        public List<tCommitteeReportType> AllReporttypeList { get; set; }
    }
}


