﻿using SBL.DomainModel.Models.Session;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
    [Table("tCommitteeReports")]
    [Serializable]
    public class tCommitteeReport
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public Nullable<int> CommitteeId { get; set; }
        public Nullable<System.DateTime> DateOfLaying { get; set; }
        //public string Subject { get; set; }
        public Nullable<int> ReportTypeId { get; set; }
        public string Period { get; set; }
        public string Title { get; set; }

        public string ReportNo { get; set; }
        public string OriginalReportNo { get; set; }
        public string PDFPath { get; set; }
        public Nullable<bool> IsFreeze { get; set; }

		// By Aishwarys
		public string CommitteeFinYear  { get; set; }
		public string DeptmentID  { get; set; }
		public DateTime EntryDate   { get; set; }
		public bool IsOnline { get; set; }

        public int? AssemblyID { get; set; }
        public int? SessionID { get; set; }

        [NotMapped]
        public int? DateOfLayingId { get; set; }

        public long? PaperLaidId { get; set; }


    }


}
