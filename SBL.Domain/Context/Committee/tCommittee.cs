﻿namespace SBL.DomainModel.Models.Committee
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tCommittee")]
    [Serializable]
    public partial class tCommittee
    {
        public tCommittee()
        {
             tCommitteeCalendars = new HashSet<tCommitteeCalendar>();
           // this.tCommitteeCalendars = new List<tCommitteeCalendar>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommitteeId { get; set; }

        public int AssemblyID { get; set; }

        public int CommitteeTypeId { get; set; }

        public int? ParentId { get; set; }

        public int SessionId { get; set; }

        [Required]
        [StringLength(128)]
        public string CommitteeName { get; set; }

        public DateTime? DateOfCreation { get; set; }

        public bool IsActive { get; set; }

        public bool AutoSchedule { get; set; }

        public string Remark { get; set; }
        public string Abbreviation { get; set; }

        public string CommitteeNameLocal { get;set;}
		public string CommitteeYear { get; set; }
        [NotMapped]
        public string Mode { get; set; }
        [NotMapped]
        public virtual ICollection<tCommitteeCalendar> tCommitteeCalendars { get; set; }
    }

    [Serializable]
    public class CommitteesListView
    {
        public int CommitteeId { get; set; }

        public string AssemblyName { get; set; }

        public string CommitteeType { get; set; }

        public int? ParentId { get; set; }

        public string CommitteeName { get; set; }

        public DateTime? DateOfCreation { get; set; }

		public string CommFilePath { get; set; }
		public string CommitteeNameLocal { get; set; }
         [NotMapped]
        public string Abbreviation { get; set; }
    }
}
