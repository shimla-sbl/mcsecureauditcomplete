﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Committee
{
    [Serializable]
    [Table("tCommitteeEmployee")]
    public class tCommitteeEmployee
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CommEmpId { get; set; }
        public int CommitteeId { get; set; }
        public int EmpId { get; set; }
        public Boolean isActive { get; set; }

    }
}
