﻿using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.ROM;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Ministery;
//using SBL.DomainModel.Models.SiteSetting;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;



using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.SiteSetting;

namespace SBL.Domain.Context.RotationofMinisters
{
    class RotationofMinistersContext : DBBase<RotationofMinistersContext>
    {


        public RotationofMinistersContext() : base("eVidhan") { }

        public DbSet<tRotationMinister> tRotationMinister { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mSession> mSession { get; set; }
        public DbSet<mSessionDate> mSessionDate { get; set; }
        public DbSet<mMinistry> mMinistries { get; set; }
        public virtual DbSet<SiteSettings> SiteSettings { get; set; }
        public DbSet<mMinsitryMinister> mMinsitryMinisters { get; set; }


        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "GetAllRotationofMinisters": { return GetAllRotationofMinisters(param.Parameter); }

                    case "AddRotationofMinisters": { return AddRotationofMinisters(param.Parameter); }

                    case "UpdateRotationofMinisters": { return UpdateRotationofMinisters(param.Parameter); }

                    case "GetRotationofMinistersById": { return GetRotationofMinistersById(param.Parameter); }

                    case "DeleteRotationofMinistersById": { return DeleteRotationofMinistersById(param.Parameter); }
                    case "GetAllMinistery":
                        {
                            return GetAllMinistery(param.Parameter);
                        }
                    case "GetAllMinisteryMinisters":
                        {
                            return GetAllMinisteryMinisters(param.Parameter);
                        }

                }
            }
            return null;
        }



        private static object AddRotationofMinisters(object p)
        {
            try
            {
                using (RotationofMinistersContext db = new RotationofMinistersContext())
                {
                    tRotationMinister data = p as tRotationMinister;

                    data.ModifiedDate = DateTime.Now;
                    data.CreatedDate = DateTime.Now;
                    // data.Status = true;

                    db.tRotationMinister.Add(data);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        private static object UpdateRotationofMinisters(object p)
        {

            try
            {
                using (RotationofMinistersContext db = new RotationofMinistersContext())
                {
                    tRotationMinister updateresult = p as tRotationMinister;
                    var actualResult = (from rotmin in db.tRotationMinister
                                        where
                                            rotmin.Id == updateresult.Id
                                        select rotmin).FirstOrDefault();

                    actualResult.AssemblyId = updateresult.AssemblyId;
                    actualResult.SessionDateId = updateresult.SessionDateId;
                    actualResult.SessionId = updateresult.SessionId;
                    actualResult.MinistryId = updateresult.MinistryId;
                    actualResult.IsActive = updateresult.IsActive;
                    actualResult.ModifiedDate = DateTime.Now;
                    actualResult.ModifiedBy = updateresult.ModifiedBy;


                    db.tRotationMinister.Attach(actualResult);
                    db.Entry(actualResult).State = EntityState.Modified;
                    db.SaveChanges();


                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return true;

        }

        private static object DeleteRotationofMinistersById(object p)
        {
            try
            {
                using (RotationofMinistersContext db = new RotationofMinistersContext())
                {
                    tRotationMinister id = p as tRotationMinister;

                    var result = (from rotmin in db.tRotationMinister
                                  where
                                      rotmin.Id == id.Id
                                  select rotmin).FirstOrDefault();

                    db.tRotationMinister.Remove(result);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static object GetAllRotationofMinisters(object p)
        {
            try
            {
                using (RotationofMinistersContext db = new RotationofMinistersContext())
                {
                    var relaresult = (from asblyFiles in db.tRotationMinister


                                      //join assembly in db.mAssembly on asblyFiles.AssemblyId equals assembly.AssemblyCode
                                      //join session in db.mSession on
                                      //   new { SessionCode = asblyFiles.SessionId, assemblyId = asblyFiles.AssemblyId } equals new { SessionCode = session.SessionCode, assemblyId = session.AssemblyID }
                                      join sessiondate
                                          in db.mSessionDate on asblyFiles.SessionDateId equals sessiondate.Id into nullsessiondate
                                      from sessdate in nullsessiondate.DefaultIfEmpty()

                                      orderby asblyFiles.Id descending
                                      select
                                          new
                                          {
                                              assemblyFile = asblyFiles,
                                              //assemblyName = assembly.AssemblyName,
                                              //sessionName = session.SessionName,
                                              sessiondateName = sessdate != null ? sessdate.SessionDate : default(DateTime),


                                              //ministryname = min.MinistryName,

                                          }).ToList();


                    List<tRotationMinister> assemblyFileList = new List<tRotationMinister>();




                    foreach (var item in relaresult)
                    {
                        string BindMinisName = "";
                        tRotationMinister assemblyFile = new tRotationMinister();
                        assemblyFile = item.assemblyFile;

                        var MinisIds = item.assemblyFile.MinistryId.ToString().Trim();
                        string[] StrId = MinisIds.Split(',');

                        for (int i = 0; i < StrId.Length; i++)
                        { // Convert String Id into int Id
                            int Id = Convert.ToInt16(StrId[i]);
                            
                            //Get Minister Name By Id
                            var MinisName = (from M in db.mMinistries
                                             where M.MinistryID == Id
                                             select M.MinistryName).FirstOrDefault();

                            if (MinisName != null)
                            {
                                BindMinisName = BindMinisName + MinisName.ToString().Trim() + ", ";
                            }


                        }

                        assemblyFile.MinistryId = BindMinisName;
                        //assemblyFile.AssemblyName = item.assemblyName;
                        //assemblyFile.SessionName = item.sessionName;
                        assemblyFile.SessionDate = item.sessiondateName.ToString("dd/MM/yyyy") == "01/01/0001" ? "" : item.sessiondateName.ToString("dd/MM/yyyy");

                        //assemblyFile.MinisteryMinisterName = item.ministryname;

                        assemblyFileList.Add(assemblyFile);
                    }


                    return assemblyFileList;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object GetRotationofMinistersById(object p)
        {
            try
            {
                using (RotationofMinistersContext db = new RotationofMinistersContext())
                {
                    tRotationMinister id = p as tRotationMinister;

                    var result = (from rotmin in db.tRotationMinister
                                  where
                                      rotmin.Id == id.Id
                                  select rotmin).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }




        static List<mMinistry> GetAllMinistery(object param)
        {
            RotationofMinistersContext db = new RotationofMinistersContext();

            var query = (from dist in db.mMinistries
                         orderby dist.MinistryID descending
                         select dist).ToList();


            return query.ToList();
        }



        static List<mMinsitryMinister> GetAllMinisteryMinisters(object param)
        {
            RotationofMinistersContext db = new RotationofMinistersContext();
            List<mMinsitryMinister> newmodel = new List<mMinsitryMinister>();

            var q = (from assemblyid in db.SiteSettings
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int Aid = Convert.ToInt32(q);

            var query = (from minmis in db.mMinsitryMinisters
                         join min in db.mMinistries on minmis.MinistryID equals min.MinistryID
                         where min.MinistryName != "Chief Parliamentary Secretary" && min.AssemblyID==Aid
                         select new
                         {
                             minmis,
                             min,

                         }).ToList();
            foreach (var item in query)
            {
                mMinsitryMinister newmodels = new mMinsitryMinister();
                newmodels.MinsitryMinistersID = item.minmis.MinsitryMinistersID;
                newmodels.MinistryID = item.min.MinistryID;
                newmodels.MinistryName = item.min.MinistryName;
                newmodel.Add(newmodels);
            }

            return newmodel;
        }



    }
}
