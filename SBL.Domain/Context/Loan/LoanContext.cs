﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Loan;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.salaryhead;
using SBL.DomainModel.Models.SiteSetting;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;

namespace SBL.Domain.Context.Loan
{
    public class LoanContext : DBBase<LoanContext>
    {
        public LoanContext()
            : base("eVidhan")
        {
        }

        public DbSet<loanDetails> loanDetails { get; set; }

        public DbSet<loaneeDetails> loaneeDetails { get; set; }

        public DbSet<loanTransDetails> loanTransDetails { get; set; }

        public DbSet<LoanType> loanType { get; set; }

        public virtual DbSet<mMemberAccountsDetails> mMemberAccountsDetails { get; set; }

        public virtual DbSet<mConstituency> mConstituency { get; set; }

        public virtual DbSet<mMember> mMember { get; set; }

        public virtual DbSet<mDepartment> mDepartment { get; set; }

        public DbSet<mMemberAssembly> mMemberAssemblies { get; set; }

        public virtual DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }

        public virtual DbSet<SiteSettings> mSiteSettings { get; set; }

        public virtual DbSet<mAssembly> mAssembly { get; set; }

        public virtual DbSet<disbursementDetails> disbursementDetails { get; set; }

        public virtual DbSet<salaryheads> salaryHeads { get; set; }

        public static object checkLoanTypeByName(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                string loanName = param as string;
                var result = (from list in context.loanType
                              where list.loanName.ToLower() == loanName.ToLower()
                              select list).Count();
                return result;
            }
        }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "getLoneTypeList": { return getLoneTypeList(); }
                case "fillLoanTypeList": { return fillLoanTypeList(); }
                case "saveLoneType": { return saveLoanType(param.Parameter); }
                case "updateLoanType": { return updateLoanType(param.Parameter); }
                case "getLoanTypeByID": { return getLoanTypeByID(param.Parameter); }
                case "checkLoanTypeByName": { return checkLoanTypeByName(param.Parameter); }
                case "getDesignationList": { return getDesignationList(); }
                case "getCurrentMemberList": { return getCurrentMemberList(); }
                case "getXMemberList": { return getXMemberList(); }
                case "getLoaneeCodeList": { return getLoaneeCodeList(); }
                case "getLoanNumberList": { return getLoanNumberList(); }
                case "getLoanAccountList": { return getLoanAccountList(); }
                case "getMemberListByDesignation": { return getMemberListByDesignation(param.Parameter); }
                case "getCurrentMemberLoanList": { return getCurrentMemberLoanList(param.Parameter); }
                case "getMemberListByID": { return getMemberListByID(param.Parameter); }
                case "getXMemberLoanList": { return getXMemberLoanList(param.Parameter); }
                case "getMemberListByLoaneeID": { return getMemberListByLoaneeID(param.Parameter); }
                case "getMemberByLoaneeID": { return getMemberByLoaneeID(param.Parameter); }
                case "getMemberListByLoanNo": { return getMemberListByLoanNo(param.Parameter); }
                case "getMemberByLoanNo": { return getMemberByLoanNo(param.Parameter); }
                case "getMemberListByLoanAccountNo": { return getMemberListByLoanAccountNo(param.Parameter); }
                case "getMemberByLoanAccountNo": { return getMemberByLoanAccountNo(param.Parameter); }
                case "getLoaneeDetailsByID": { return getLoaneeDetailsByID(param.Parameter); }
                case "getActiveLoanList": { return getActiveLoanList(); }
                case "getLastLoaneeNo": { return getLastLoaneeNo(); }
                case "saveLoanee": { return saveLoanee(param.Parameter); }
                case "deleteLoanType": { return deleteLoanType(param.Parameter); }
                case "GetMemberAccountDetailsById": { return GetMemberAccountDetailsById(param.Parameter); }
                case "saveLoan": { return saveLoan(param.Parameter); }
                case "getDisburmentList": { return getDisburmentList(param.Parameter); }
                case "changeStatusofDisbursement": { return changeStatusofDisbursement(param.Parameter); }
                case "getLoanDetailsByLoanID": { return getLoanDetailsByLoanID(param.Parameter); }
                case "updateLoanInstallment": { return updateLoanInstallment(param.Parameter); }
                case "updateLoanInst": { return updateLoanInst(param.Parameter); }
                case "statementforLoan": { return statementforLoan(param.Parameter); }
                case "activeLoanList": { return activeLoanList(param.Parameter); }
                case "getDisburmentListByID": { return getDisburmentListByID(param.Parameter); }
                case "GetActiveLoanListForChallan": { return GetActiveLoanListForChallan(param.Parameter); }
                case "getchallanDetailsByLoanID": { return getchallanDetailsByLoanID(param.Parameter); }
                case "financialYearReport": { return financialYearReport(param.Parameter); }
                case "updateLoanDetails": { return updateLoanDetails(param.Parameter); }
                case "updateDisbursement": { return updateDisbursement(param.Parameter); }
                case "individualReportDateWise": { return individualReportDateWise(param.Parameter); }
                case "allReportDateWise": { return allReportDateWise(param.Parameter); }
                case "getLoanDetailsByInstAutoID": { return getLoanDetailsByInstAutoID(param.Parameter); }
                case "deleteLoanInst": { return deleteLoanInst(param.Parameter); }
                case "getDepartmentList": { return getDepartmentList(); }
            }
            return null;
        }

        #region member Details

        public static object GetMemberAccountDetailsById(object param)
        {
            using (LoanContext db = new LoanContext())
            {
                int memberCode = Convert.ToInt32(param);
                mMemberAccountsDetails _mDetails = new mMemberAccountsDetails();
                var mMember = (from _list in db.mMember
                               where _list.MemberCode == memberCode
                               select _list).FirstOrDefault();

                mMemberAssembly mAssemb = getLastAssembly(memberCode);

                var accountDetails = (from _accountList in db.mMemberAccountsDetails
                                      where _accountList.MemberCode == memberCode
                                      select _accountList).FirstOrDefault();
                if (mMember != null)
                {
                    _mDetails.MemberCode = mMember.MemberCode;
                    _mDetails.MemberDetail = mMember.Description;
                    _mDetails.MemberName = mMember.Name;
                    _mDetails.MobileNumber = mMember.Mobile;
                    _mDetails.PhotoCode = mMember.MemberCode;
                    _mDetails.MemberAddress = mMember.PermanentAddress;
                    _mDetails.constituencyCode = mAssemb.ConstituencyCode;
                    _mDetails.ConstituencyName = (from cons in db.mConstituency
                                                  where cons.ConstituencyCode == mAssemb.ConstituencyCode
                                                  select cons.ConstituencyName).FirstOrDefault();
                    _mDetails.MemberAddress = mMember.ShimlaAddress;
                }
                if (accountDetails != null)
                {
                    _mDetails.AccountNo = accountDetails.AccountNo;
                    _mDetails.AccountNoS = accountDetails.AccountNoS;
                    _mDetails.BankName = accountDetails.BankName;

                    _mDetails.EPABXNo = accountDetails.EPABXNo;
                    _mDetails.HBAAccountNo = accountDetails.HBAAccountNo;
                    _mDetails.IFSCCode = accountDetails.IFSCCode;
                    _mDetails.MCAAccountNo = accountDetails.MCAAccountNo;

                    _mDetails.NomineeAccountNo = accountDetails.NomineeAccountNo;
                    _mDetails.NomineeAccountNoS = accountDetails.NomineeAccountNoS;
                    _mDetails.NomineeActive = accountDetails.NomineeActive;
                    _mDetails.NomineeBankName = accountDetails.NomineeBankName;
                    _mDetails.NomineeIFSCCode = accountDetails.NomineeIFSCCode;
                    _mDetails.NomineeName = accountDetails.NomineeName;
                    _mDetails.NomineeRelationShip = accountDetails.NomineeRelationShip;
                }

                return _mDetails;
            }
        }

        public static object activeLoanList(object param)
        {
            int memberCode = Convert.ToInt32(param);
            using (LoanContext db = new LoanContext())
            {
                var loanList = (from loan in db.loanDetails
                                join loanee in db.loaneeDetails on loan.loaneeID equals loanee.loaneeID
                                join Loantype in db.loanType on loan.loanTypeID equals Loantype.loanID
                                where loanee.memberCode == memberCode
                                select new
                                {
                                    lonaDetails = loan,
                                    loanName = Loantype.loanName
                                }
                                    ).ToList();
                List<loanDetails> lDetailsList = new List<loanDetails>();
                foreach (var item in loanList)
                {
                    loanDetails lDetails = new loanDetails();
                    lDetails.loanAmount = item.lonaDetails.loanAmount;
                    lDetails.loanTypeName = item.loanName;
                    lDetails.loanStatus = item.lonaDetails.loanStatus;
                    lDetailsList.Add(lDetails);
                }

                return lDetailsList;
            }
        }

        #endregion member Details

        #region Loanee

        public static object getLastLoaneeNo()
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.loaneeDetails
                              orderby list.loaneeID descending
                              select list.loaneeID).FirstOrDefault();
                return result;
            }
        }

        public static object saveLoanee(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                try
                {
                    loaneeDetails _lDetails = param as loaneeDetails;
                    context.loaneeDetails.Add(_lDetails);
                    context.SaveChanges();
                    context.Close();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return null;
            }
        }

        #endregion Loanee

        #region Save and update Loan

        public static object saveLoan(object param)
        {
            loanDetails loan = null;
            using (LoanContext context = new LoanContext())
            {
                try
                {
                    loanDetails _lDetails = param as loanDetails;
                    context.loanDetails.Add(_lDetails);
                    context.SaveChanges();
                    int id = _lDetails.AutoID;
                    loan = (from result in context.loanDetails
                            where result.AutoID == id
                            select result
                                   ).FirstOrDefault();
                    loan.loanNumber = Int64.Parse(DateTime.Now.Year.ToString() + '0' + loan.loanTypeID + '0' + loan.loaneeID + '0' + loan.AutoID);
                    context.Entry(loan).State = EntityState.Modified;
                    context.SaveChanges();

                    foreach (var item in _lDetails.disbursementDetails)
                    {
                        item.loanNumber = loan.loanNumber;
                        context.disbursementDetails.Add(item);
                        context.SaveChanges();
                    }
                    context.Close();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return loan.loanNumber;
            }
        }

        public static object updateLoanDetails(object param)
        {
            loanDetails ldetails = param as loanDetails;
            using (LoanContext context = new LoanContext())
            {
                var result = (from res in context.loanDetails
                              where res.loanNumber == ldetails.loanNumber
                              select res).FirstOrDefault();
                result.loanAccountNumber = ldetails.loanAccountNumber;
                result.loanAmount = ldetails.loanAmount;
                result.loanInterestRate = ldetails.loanInterestRate;
                result.totalNoofEMI = ldetails.totalNoofEMI;
                result.lastEditBy = 1;
                result.lastEditDate = DateTime.Now;
                result.loanSanctionDate = ldetails.loanSanctionDate;
                result.loanTypeID = ldetails.loanTypeID;
                context.Entry(result).State = EntityState.Modified;
                context.SaveChanges();
                context.Close();
                return null;
            }
        }

        public static object updateDisbursement(object param)
        {
            disbursmentList lst = param as disbursmentList;
            List<disbursementDetails> list = lst.disbursmentListofMember;
            long loanNumber = lst.loanNumber;
            using (LoanContext context = new LoanContext())
            {
                var dlist = (from dislist in context.disbursementDetails
                             where dislist.loanNumber == loanNumber
                             select dislist
                                 ).ToList();
                foreach (var item in dlist)
                {
                    item.loanNumber = lst.loanNumber;
                    context.disbursementDetails.Remove(item);
                    context.SaveChanges();
                }
                foreach (var item in list)
                {
                    item.loanNumber = lst.loanNumber;
                    context.disbursementDetails.Add(item);
                    context.SaveChanges();
                }
                context.Close();
                return null;
            }
        }

        #endregion Save and update Loan

        #region Loan Type
        #region get Loan Type Details

        public static object getCurrentMemberList()
        {
            using (LoanContext context = new LoanContext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);

                var result = (from member in context.mMember
                              join massembly in context.mMemberAssemblies
                              on member.MemberCode equals massembly.MemberID
                              where massembly.AssemblyID == aid
                              orderby member.Name
                              select new fillCurrentMamber
                              {
                                  MemberName = member.Name,
                                  MemberCode = member.MemberCode
                              }
                                   );
                return result.ToList();
            }
        }

        public static object getDesignationList()
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.mMemberAssemblies
                              where list.Designation != null
                              select new FillDesignation
                              {
                                  designationID = list.DesignationID,
                                  designationName = list.Designation
                              }

                              ).Distinct().ToList();
                return result;
            }
        }

        public static object getLoanAccountList()
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.loanDetails
                              join member in context.loaneeDetails
                              on list.loaneeID equals member.loaneeID
                              select new fillLoanAccountList
                              {
                                  accountNumber = list.loanAccountNumber,
                                  MemberName = member.memberName
                              }).ToList();
                return result;
            }
        }

        public static object getLoaneeCodeList()
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.loaneeDetails
                              select new fillLoanee
                              {
                                  loaneeID = list.loaneeID,
                                  MemberName = list.memberName
                              }).ToList();
                return result;
            }
        }

        public static object getLoanNumberList()
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.loanDetails
                              join member in context.loaneeDetails
                              on list.loaneeID equals member.loaneeID
                              select new fillLoanee
                              {
                                  loaneeID = list.loanNumber,
                                  MemberName = member.memberName
                              });
                return result.ToList();
            }
        }

        public static object getLoanTypeByID(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                int loanID = Convert.ToInt32(param);
                var result = (from list in context.loanType
                              where list.loanID == loanID
                              select list)
                             .FirstOrDefault();
                return result;
            }
        }

        public static object getLoneTypeList()
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from loneList in context.loanType
                              select loneList).ToList();
                return result;
            }
        }

        public static object fillLoanTypeList()
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.loanType
                              where list.loanName != null
                              select new fillLoanType
                              {
                                  loanID = list.loanID,
                                  loanType = list.loanName,
                                  AccountHeadsName = list.accountHeadsName
                              }

                              ).Distinct().ToList();
                return result;
            }
        }

        #endregion get Loan Type Details

        #region delete Loan Type

        public static object deleteLoanType(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                int loanID = Convert.ToInt32(param);
                try
                {
                    var LoanTYpe = (from _loanType in context.loanType
                                    where _loanType.loanID == loanID
                                    select _loanType
                                      ).FirstOrDefault();

                    context.loanType.Remove(LoanTYpe);
                    context.SaveChanges();
                    context.Close();
                }
                catch (Exception ex)
                {
                    throw;
                }

                return null;
            }
        }

        #endregion delete Loan Type

        #region Loan master

        # region search loanMaster

        public static object getMemberByLoanAccountNo(object param)
        {
            string loanAccountNumber = param as string;

            using (LoanContext context = new LoanContext())
            {
                var result = (from _loanDetails in context.loanDetails
                              join _loaneeDetails in context.loaneeDetails
                              on _loanDetails.loaneeID equals _loaneeDetails.loaneeID
                              join _member in context.mMember
                              on _loaneeDetails.memberCode equals _member.MemberCode
                              where _loanDetails.loanAccountNumber == loanAccountNumber
                              select new loanSearchResult
                              {
                                  memberName = _member.Name,
                                  memberCode = _member.MemberCode,
                                  Address = _member.PermanentAddress,
                                  contactNumber = _member.Mobile,
                                  loaneeCode = _loanDetails.loaneeID,
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getMemberByLoaneeID(object param)
        {
            int loaneeCode = Convert.ToInt32(param);
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.loaneeDetails
                              join member in context.mMember
                              on list.memberCode equals member.MemberCode
                              where list.loaneeID == loaneeCode
                              select new loanSearchResult
                              {
                                  memberName = member.Name,
                                  memberCode = list.memberCode,
                                  Address = member.PermanentAddress,
                                  contactNumber = member.Mobile,
                                  loaneeCode = list.loaneeID,
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getMemberByLoanNo(object param)
        {
            int loanNo = Convert.ToInt32(param);
            using (LoanContext context = new LoanContext())
            {
                var result = (from _loanDetails in context.loanDetails
                              join _loaneeDetails in context.loaneeDetails
                              on _loanDetails.loaneeID equals _loaneeDetails.loaneeID
                              join _member in context.mMember
                              on _loaneeDetails.memberCode equals _member.MemberCode
                              where _loanDetails.loanNumber == loanNo
                              select new loanSearchResult
                              {
                                  memberName = _member.Name,
                                  memberCode = _member.MemberCode,
                                  Address = _member.PermanentAddress,
                                  contactNumber = _member.Mobile,
                                  loaneeCode = _loanDetails.loaneeID,
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getMemberListByDesignation(object param)
        {
            int designatioID = int.Parse(param.ToString());
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.mMember
                              join designation in context.mMemberAssemblies
                              on list.MemberCode equals designation.MemberID
                              where designation.DesignationID == designatioID
                              select new loanSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,
                                  designation = designation.Designation,
                                  Address = list.PermanentAddress,
                                  constituencyCode = designation.ConstituencyCode,
                                  constituencyName = (from con in context.mConstituency
                                                      where con.ConstituencyCode == designation.ConstituencyCode
                                                      select con.ConstituencyName).FirstOrDefault(),
                                  contactNumber = list.Mobile,
                                  loaneeCode = (from loanee in context.loaneeDetails
                                                where loanee.memberCode == list.MemberCode
                                                select loanee.loaneeID).FirstOrDefault(),
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getMemberListByID(object param)
        {
            int memID = Convert.ToInt32(param);
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.mMember

                              where list.MemberCode == memID

                              select new loanSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,

                                  Address = list.PermanentAddress,

                                  loaneeCode = (from loanee in context.loaneeDetails
                                                where loanee.memberCode == list.MemberCode
                                                select loanee.loaneeID).FirstOrDefault(),
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getCurrentMemberLoanList(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);
                var result = (from list in context.mMember
                              join designation in context.mMemberAssemblies
                              on list.MemberCode equals designation.MemberID
                              where designation.AssemblyID == aid

                              select new loanSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,
                                  designation = designation.Designation,
                                  Address = list.PermanentAddress,
                                  constituencyCode = designation.ConstituencyCode,
                                  constituencyName = (from con in context.mConstituency
                                                      where con.ConstituencyCode == designation.ConstituencyCode
                                                      select con.ConstituencyName).FirstOrDefault(),
                                  contactNumber = list.Mobile,
                                  loaneeCode = (from loanee in context.loaneeDetails
                                                where loanee.memberCode == list.MemberCode
                                                select loanee.loaneeID).FirstOrDefault(),
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getMemberListByLoanAccountNo(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from _loanDetails in context.loanDetails
                              join _loaneeDetails in context.loaneeDetails
                              on _loanDetails.loaneeID equals _loaneeDetails.loaneeID
                              join _member in context.mMember
                              on _loaneeDetails.memberCode equals _member.MemberCode

                              select new loanSearchResult
                              {
                                  memberName = _member.Name,
                                  memberCode = _member.MemberCode,
                                  Address = _member.PermanentAddress,
                                  contactNumber = _member.Mobile,
                                  loaneeCode = _loanDetails.loaneeID,
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static mMemberAssembly getLastAssembly(int? memberID)
        {
            using (LoanContext context = new LoanContext())
            {
                var assemblyID = (from assembly in context.mMemberAssemblies
                                  join mAssemblyOrder in context.mAssembly
                                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                  where assembly.MemberID == memberID
                                  orderby mAssemblyOrder.AssemblyID descending
                                  select assembly.AssemblyID
                            ).FirstOrDefault();
                var memberAssemblies = (from memberAssembly in context.mMemberAssemblies
                                        where memberAssembly.AssemblyID == assemblyID
                                        && memberAssembly.MemberID == memberID
                                        select memberAssembly
                                        ).FirstOrDefault();
                return memberAssemblies;
            }
        }

        public static object getMemberListByLoaneeID(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.loaneeDetails
                              join member in context.mMember
                              on list.memberCode equals member.MemberCode
                              select new loanSearchResult
                              {
                                  memberName = member.Name,
                                  memberCode = list.memberCode,
                                  Address = member.PermanentAddress,
                                  contactNumber = member.Mobile,
                                  loaneeCode = list.loaneeID,
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getMemberListByLoanNo(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from _loanDetails in context.loanDetails
                              join _loaneeDetails in context.loaneeDetails
                              on _loanDetails.loaneeID equals _loaneeDetails.loaneeID
                              join _member in context.mMember
                              on _loaneeDetails.memberCode equals _member.MemberCode
                              select new loanSearchResult
                              {
                                  memberName = _member.Name,
                                  memberCode = _member.MemberCode,
                                  Address = _member.PermanentAddress,
                                  contactNumber = _member.Mobile,
                                  loaneeCode = _loanDetails.loaneeID,
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getXMemberLoanList(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);

                var cMemberCodeList = (from Assembly in context.mMemberAssemblies
                                       where Assembly.AssemblyID == aid
                                       select Assembly.MemberID).ToList();

                var result = (from list in context.mMember
                              where !(cMemberCodeList.Contains(list.MemberCode))

                              select new loanSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,
                                  //designation = designation.Designation,
                                  //Address = list.PermanentAddress,
                                  // constituencyCode = designation.ConstituencyCode,
                                  // constituencyName = (from con in context.mConstituency
                                  // where con.ConstituencyCode == designation.ConstituencyCode
                                  // select con.ConstituencyName).FirstOrDefault(),
                                  contactNumber = list.Mobile,
                                  loaneeCode = (from loanee in context.loaneeDetails
                                                where loanee.memberCode == list.MemberCode
                                                select loanee.loaneeID).FirstOrDefault(),
                                  anyActiveLoan = 0,
                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getXMemberList()
        {
            using (LoanContext context = new LoanContext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);

                var cMemberCodeList = (from Assembly in context.mMemberAssemblies
                                       where Assembly.AssemblyID == aid
                                       select Assembly.MemberID).ToList();

                var result = (from member in context.mMember
                              where !(cMemberCodeList.Contains(member.MemberCode))
                              select new fillCurrentMamber
                              {
                                  MemberName = member.Name,
                                  MemberCode = member.MemberCode
                              }
                                   ).Distinct();
                return result.ToList();
            }
        }

        #endregion Loan master

        public static object getLoaneeDetailsByID(object param)
        {
            int memberID = Convert.ToInt32(param);
            using (LoanContext context = new LoanContext())
            {
                var result = (from _loanee in context.loaneeDetails
                              where _loanee.memberCode == memberID
                              select _loanee).FirstOrDefault();
                return result;
            }
        }

        #endregion Loan Type

        #region save and update loan type

        public static object saveLoanType(object param)
        {
            try
            {
                LoanType lType = param as LoanType;
                using (LoanContext context = new LoanContext())
                {
                    context.loanType.Add(lType);
                    context.SaveChanges();
                    salaryheads heads = new salaryheads();
                    heads.hType = "dr";
                    heads.LoanID = lType.loanID;
                    heads.perc = 0;
                    heads.salaryComponentId = 0;
                    heads.sHeadName = lType.loanName + "(p)";
                    heads.Status = true;
                    heads.hsubType = "a";
                    heads.amountType = "Fixed";
                    heads.LoanComponentType = "P";
                    heads.orderNo = (from order in context.salaryHeads
                                     where order.hType == "dr"
                                     orderby order.orderNo descending
                                     select order.orderNo
                                       ).First() + 1;
                    context.salaryHeads.Add(heads);
                    context.SaveChanges();
                    salaryheads iheads = new salaryheads();
                    iheads.hType = "dr";
                    iheads.LoanID = lType.loanID;
                    iheads.perc = 0;
                    iheads.hsubType = "a";
                    iheads.salaryComponentId = 0;
                    iheads.sHeadName = lType.loanName + "(i)";
                    iheads.Status = true;
                    iheads.amountType = "Fixed";
                    iheads.LoanComponentType = "I";
                    iheads.orderNo = (from order in context.salaryHeads
                                      where order.hType == "dr"
                                      orderby order.orderNo descending
                                      select order.orderNo
                                       ).First() + 1;
                    context.salaryHeads.Add(iheads);
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        public static object updateLoanType(object param)
        {
            try
            {
                LoanType lType = param as LoanType;
                using (LoanContext context = new LoanContext())
                {
                    context.Entry(lType).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        #endregion save and update loan type
        #endregion

        #region loan Recovery

        public static object getActiveLoanList()
        {
            using (
                LoanContext context = new LoanContext())
            {
                var result = (from _loanDetails in context.loanDetails
                              join _loaneeDetails in context.loaneeDetails
                              on _loanDetails.loaneeID equals _loaneeDetails.loaneeID
                              join _member in context.mMember
                              on _loaneeDetails.memberCode equals _member.MemberCode
                              join loanType in context.loanType
                              on _loanDetails.loanTypeID equals loanType.loanID
                              where _loanDetails.loanStatus == true

                              orderby _loanDetails.loanNumber descending
                              select new loanSearchResult
                              {
                                  memberName = _member.Name,
                                  memberCode = _member.MemberCode,
                                  Address = _member.PermanentAddress,
                                  contactNumber = _member.Mobile,
                                  loaneeCode = _loanDetails.loaneeID,
                                  anyActiveLoan = 0,
                                  loanCode = _loanDetails.loanNumber,
                                  LoanName = loanType.loanName,
                                  loanAmount = _loanDetails.loanAmount,
                                  totalEMI = _loanDetails.totalNoofEMI,
                                  intRate = _loanDetails.loanInterestRate,
                                  sanDate = _loanDetails.loanSanctionDate, //+ _loanDetails.releaseDate
                                  relDate = _loanDetails.releaseDate,

                              }
                                         ).Distinct().ToList();

                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    if (m != null)
                    {
                        item.designation = m.Designation;
                        item.constituencyCode = m.ConstituencyCode;
                        item.constituencyName = (from con in context.mConstituency
                                                 where con.ConstituencyCode == m.ConstituencyCode
                                                 select con.ConstituencyName).FirstOrDefault();
                    }
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                    var loantrans = (from tran in context.loanTransDetails
                                     where tran.loanNumber == item.loanCode
                                     where tran.transType == "dr"
                                     orderby new { tran.yearID, tran.monthID, tran.autoID } descending
                                     select tran).FirstOrDefault();
                    if (loantrans != null)
                    {
                        item.lastInstallment = "#" + loantrans.installmentNumber + " (P) " + loantrans.principalReceived + " (I) " + loantrans.interestReceived + " @ " + (loantrans.paymentDate.Split(' ').Length > 0 ? loantrans.paymentDate.Split(' ')[0] : loantrans.paymentDate);
                        item.outstarndingBalance = loantrans.closingBalance;
                        item.outstarndingIntereset = loantrans.outstandingInterest;

                    }


                    var result1 = (from _loan in context.loanDetails
                                   join _loanee in context.loaneeDetails
                                   on _loan.loaneeID equals _loanee.loaneeID
                                   where _loan.loanNumber == item.loanCode
                                   select new
                                   {
                                       // laon = _loan,
                                       loanAccountnumber = _loan.loanAccountNumber
                                       // laonee = _loanee
                                   }
                                    ).FirstOrDefault();

                    item.loanAccontNumber = result1.loanAccountnumber;

                }
                return result;
            }
        }



        public static object getLoanDetailsByLoanID(object param)
        {
            long loanNumber = Convert.ToInt64(param.ToString());
            using (LoanContext context = new LoanContext())
            {
                var result = (from _loan in context.loanDetails
                              join _loanee in context.loaneeDetails
                              on _loan.loaneeID equals _loanee.loaneeID
                              where _loan.loanNumber == loanNumber
                              select new
                              {
                                  laon = _loan,
                                  laonee = _loanee
                              }
                                ).FirstOrDefault();
                var loantrans = (from tran in context.loanTransDetails
                                 where tran.loanNumber == loanNumber
                                 orderby new { tran.yearID, tran.monthID, tran.transType, tran.autoID } descending
                                 select tran).FirstOrDefault();
                installmentDetails inst = new installmentDetails();
                inst.Address = result.laonee.address;
                inst.ConsCode = result.laonee.constituencyCode;
                inst.ConsName = result.laonee.constituencyName;
                mMemberAssembly m = getLastAssembly(result.laonee.memberCode);
                inst.Designation = m.Designation;
                inst.EMI = (from emi in context.loanTransDetails
                            where emi.loanNumber == loanNumber
                            && emi.transType == "dr"
                            select emi
                              ).Count() + 1;
                inst.pAmount = Math.Round(result.laon.loanAmount / result.laon.totalNoofEMI);
                if (loantrans != null)
                {
                    inst.lastInstallment = "#" + loantrans.installmentNumber + " (P) " + loantrans.principalReceived + " (I) " + loantrans.interestReceived + " @ " + (loantrans.paymentDate.Split(' ').Length > 0 ? loantrans.paymentDate.Split(' ')[0] : loantrans.paymentDate);
                    inst.outstarndingBalance = loantrans.closingBalance;
                    inst.outstarndingIntereset = loantrans.outstandingInterest;

                    if (loantrans.closingBalance > 0)
                    {
                        inst.iAmount = Math.Round((loantrans.closingBalance * result.laon.loanInterestRate) / 1200);
                    }
                    else
                    {
                        inst.iAmount = 0;
                    }
                }
                else
                {
                    inst.iAmount = 0;
                }

                inst.intRate = result.laon.loanInterestRate;
                inst.loanAccountNumber = result.laon.loanAccountNumber;
                inst.loanAmount = result.laon.loanAmount;
                inst.loanNumber = result.laon.loanNumber;
                inst.loantypeID = result.laon.loanTypeID;
                inst.loanType = (from name in context.loanType
                                 where name.loanID == result.laon.loanTypeID
                                 select name.loanName).FirstOrDefault();
                inst.memberCode = result.laonee.memberCode;
                inst.memberName = result.laonee.memberName;
                inst.mobileNumber = result.laonee.mobile;
                inst.noOFEMI = result.laon.totalNoofEMI;
                inst.paidInt = inst.iAmount;
                var ss = result.laon.loanSanctionDate.Split(' ')[0];
                // inst.sancationDate = string.Format("{0:dd-mm-yyyy}", DateTime.Parse(result.laon.loanSanctionDate.Split(' ')[0]));
                DateTime ToDateD = new DateTime(Convert.ToInt32(ss.Split('/')[2]), Convert.ToInt32(ss.Split('/')[1]), Convert.ToInt32(ss.Split('/')[0]));

                inst.sancationDate = ToDateD.ToString("dd/MM/yyyy");
                return inst;
            }
        }

        public static object getLoanDetailsByInstAutoID(object param)
        {
            long AutoID = Convert.ToInt64(param.ToString());
            using (LoanContext context = new LoanContext())
            {
                long loanNumber = (from loan in context.loanTransDetails
                                   where loan.autoID == AutoID
                                   select loan.loanNumber).FirstOrDefault();

                var result = (from _loan in context.loanDetails
                              join _loanee in context.loaneeDetails
                              on _loan.loaneeID equals _loanee.loaneeID
                              where _loan.loanNumber == loanNumber
                              select new
                              {
                                  laon = _loan,
                                  laonee = _loanee
                              }
                                ).FirstOrDefault();

                var loantrans = (from tran in context.loanTransDetails
                                 where tran.autoID == AutoID
                                 select tran).FirstOrDefault();

                installmentDetails inst = new installmentDetails();
                inst.PaymentDate = loantrans.paymentDate;
                inst.Address = result.laonee.address;
                inst.ConsCode = result.laonee.constituencyCode;
                inst.ConsName = result.laonee.constituencyName;
                mMemberAssembly m = getLastAssembly(result.laonee.memberCode);
                inst.Designation = m.Designation;
                if (loantrans != null)
                {
                    inst.lastInstallment = "#" + loantrans.installmentNumber + " (P) " + loantrans.principalReceived + " (I) " + loantrans.interestReceived + " @ " + (loantrans.paymentDate.Split(' ').Length > 0 ? loantrans.paymentDate.Split(' ')[0] : loantrans.paymentDate);
                    inst.outstarndingBalance = loantrans.closingBalance;
                    inst.outstarndingIntereset = loantrans.outstandingInterest;
                    inst.iAmount = loantrans.interestReceived;
                    inst.pAmount = loantrans.principalReceived;
                }
                inst.intRate = result.laon.loanInterestRate;
                inst.loanAccountNumber = result.laon.loanAccountNumber;
                inst.loanAmount = result.laon.loanAmount;
                inst.loanNumber = result.laon.loanNumber;
                inst.loantypeID = result.laon.loanTypeID;
                inst.loanType = (from name in context.loanType
                                 where name.loanID == result.laon.loanTypeID
                                 select name.loanName).FirstOrDefault();
                inst.memberCode = result.laonee.memberCode;
                inst.memberName = result.laonee.memberName;
                inst.mobileNumber = result.laonee.mobile;
                inst.noOFEMI = result.laon.totalNoofEMI;

                inst.EMI = loantrans.installmentNumber;
                inst.paidInt = loantrans.interestForMonth;
                var ss = result.laon.loanSanctionDate.Split(' ')[0];
                // inst.sancationDate = string.Format("{0:dd-mm-yyyy}", DateTime.Parse(result.laon.loanSanctionDate.Split(' ')[0]));
                DateTime ToDateD = new DateTime(Convert.ToInt32(ss.Split('/')[2]), Convert.ToInt32(ss.Split('/')[1]), Convert.ToInt32(ss.Split('/')[0]));

                inst.sancationDate = ToDateD.ToString("dd/MM/yyyy");
                return inst;
            }
        }

        public static object updateLoanInstallment(object param)
        {
            installmentDetails detail = param as installmentDetails;
            using (LoanContext context = new LoanContext())
            {
                var loantrans = (from tran in context.loanTransDetails
                                 where tran.loanNumber == detail.loanNumber
                                 orderby tran.autoID descending
                                 select tran).FirstOrDefault();
                loanTransDetails trans = new loanTransDetails();
                trans.closingBalance = loantrans.openingBalance - detail.pAmount;
                trans.DDOCode = detail.DDOCode;
                trans.disbursementAmount = 0;
                trans.installmentNumber = detail.EMI;
                trans.interestForMonth = detail.paidInt;
                trans.interestReceived = detail.iAmount;
                trans.loanAccontNumber = detail.loanAccountNumber;
                trans.loanNumber = detail.loanNumber;
                if (detail.PaymentDate != null)
                {
                    DateTime pDate = new DateTime();
                    DateTime.TryParse(detail.PaymentDate, out pDate);

                    trans.monthID = detail.PaymentDate.Split('/').Length > 0 ? (Convert.ToInt32(detail.PaymentDate.Split('/')[1]) <= 12 ? Convert.ToInt32(detail.PaymentDate.Split('/')[1]) : -1) : -1;
                    trans.yearID = detail.PaymentDate.Split('/').Length > 0 ? (Convert.ToInt32(detail.PaymentDate.Split('/')[2])) : -1;
                }

                trans.openingBalance = loantrans.closingBalance;
                trans.closingBalance = loantrans.closingBalance - detail.pAmount;
                trans.outstandingInterest = loantrans.outstandingInterest + (detail.paidInt - detail.iAmount);
                trans.payDate = DateTime.Now;
                trans.paymentCode = detail.PaymentCode;
                trans.paymentDate = detail.PaymentDate;
                trans.paymentMode = detail.PaymentMode;
                trans.principalReceived = detail.pAmount;
                trans.transType = "dr";
                trans.treasuryName = detail.treasuryName;
                if (detail.voucherDate != null)
                {
                    DateTime Date = new DateTime();
                    DateTime.TryParse(detail.voucherDate, out Date);
                    trans.voucherDate = Date;
                }
                trans.voucherNumber = detail.voucherNumber;
                //  trans.paymentDate = DateTime.Now.ToShortDateString();
                trans.intRate = detail.intRate;
                context.loanTransDetails.Add(trans);
                context.SaveChanges();
                context.Close();
                return null;
            }
        }

        public static object updateLoanInst(object param)
        {
            try
            {
                installmentDetails detail = param as installmentDetails;
                using (LoanContext context = new LoanContext())
                {
                    var loantrans = (from tran in context.loanTransDetails
                                     where tran.autoID == detail.AutoID
                                     //  && tran.transType == "dr"
                                     orderby tran.autoID descending
                                     select tran).FirstOrDefault();

                    var trans = (from tran in context.loanTransDetails
                                 where tran.autoID == detail.AutoID
                                 select tran).FirstOrDefault();
                    trans.DDOCode = detail.DDOCode;
                    trans.disbursementAmount = 0;
                    trans.installmentNumber = detail.EMI;
                    trans.interestForMonth = detail.paidInt;
                    trans.interestReceived = detail.iAmount;
                    trans.loanAccontNumber = detail.loanAccountNumber;
                    trans.loanNumber = detail.loanNumber;
                    if (detail.PaymentDate != null)
                    {
                        DateTime pDate = new DateTime();
                        DateTime.TryParse(detail.PaymentDate, out pDate);
                        trans.monthID = pDate.Month;
                        trans.yearID = pDate.Year;
                    }
                    if (loantrans != null)
                    {
                        //trans.openingBalance = loantrans.closingBalance;
                        // trans.closingBalance = loantrans.closingBalance - detail.pAmount;
                        // trans.outstandingInterest = loantrans.outstandingInterest + (detail.paidInt - detail.iAmount);
                    }

                    trans.payDate = DateTime.Now;
                    trans.paymentCode = detail.PaymentCode;
                    trans.paymentDate = detail.PaymentDate;
                    trans.paymentMode = detail.PaymentMode;
                    trans.principalReceived = detail.pAmount;
                    trans.transType = "dr";
                    trans.treasuryName = detail.treasuryName;
                    if (detail.voucherDate != null)
                    {
                        DateTime Date = new DateTime();
                        DateTime.TryParse(detail.voucherDate, out Date);
                        trans.voucherDate = Date;
                    }
                    trans.voucherNumber = detail.voucherNumber;
                    //  trans.paymentDate = DateTime.Now.ToShortDateString();
                    trans.intRate = detail.intRate;

                    context.Entry(trans).State = EntityState.Modified;
                    context.SaveChanges();
                    reCalculateLoan(loantrans.loanNumber);
                    context.Close();
                    return loantrans.loanNumber;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static object deleteLoanInst(object param)
        {
            try
            {
                long autoid = Convert.ToInt64(param);
                using (LoanContext context = new LoanContext())
                {
                    var loantrans = (from tran in context.loanTransDetails
                                     where tran.autoID == autoid
                                     select tran).FirstOrDefault();
                    context.Entry(loantrans).State = EntityState.Deleted;
                    context.SaveChanges();
                    reCalculateLoan(loantrans.loanNumber);
                    return loantrans.loanNumber;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static void updateLoan(int autoID)
        {
            using (LoanContext context = new LoanContext())
            {
                var loantrans = (from tran in context.loanTransDetails
                                 where tran.autoID < autoID
                                 //  && tran.transType == "dr"
                                 orderby tran.autoID descending
                                 select tran).FirstOrDefault();

                var trans = (from tran in context.loanTransDetails
                             where tran.autoID == autoID
                             select tran).FirstOrDefault();

                var result = (from _loan in context.loanDetails

                              where _loan.loanNumber == trans.loanNumber
                              select _loan
                          ).FirstOrDefault();

                trans.installmentNumber = trans.installmentNumber + 1;
                double paidInt = 0;

                if (loantrans != null)
                {
                    if (loantrans.closingBalance > 0)
                    {
                        paidInt = Math.Round((loantrans.closingBalance * result.loanInterestRate) / 1200);
                    }
                    else
                    {
                        paidInt = 0;
                    }
                }
                else
                {
                    paidInt = 0;
                }
                trans.interestForMonth = paidInt;
                trans.openingBalance = loantrans.closingBalance;
                trans.closingBalance = loantrans.closingBalance - trans.principalReceived;
                trans.outstandingInterest = loantrans.outstandingInterest + (paidInt - trans.interestReceived);
                trans.payDate = DateTime.Now;
                context.Entry(trans).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public static long reCalculateLoan(long loanNumber)
        {
            using (LoanContext context = new LoanContext())
            {
                correctDate(loanNumber);
                var loantrans = (from tran in context.loanTransDetails
                                 where tran.loanNumber == loanNumber && tran.paymentDate != null
                                 orderby new { tran.yearID, tran.monthID, tran.transType }
                                 select tran).ToList();
                int instno = 0;
                for (int i = 0; i < loantrans.Count; i++)
                {
                    double opBal = 0;
                    double outint = 0;
                    DateTime lastdate = DateTime.Now;
                    DateTime curdate = DateTime.Now;
                    if (i != 0)
                    {
                        opBal = Convert.ToInt32(loantrans[i - 1].closingBalance);
                        lastdate = Convert.ToDateTime(new DateTime(loantrans[i - 1].yearID, loantrans[i - 1].monthID, 1));             //loantrans[i - 1].paymentDate
                        outint = Convert.ToInt32(loantrans[i - 1].outstandingInterest);
                    }
                    double clBal = Convert.ToInt32(loantrans[i].closingBalance);
                    IFormatProvider culture = new CultureInfo("en-IN", true);
                    //    _list.Date = DateTime.ParseExact(item.paymentDate.ToString(), "MMM-yyyy", culture).ToString();
                    //curdate = DateTime.ParseExact(loantrans[i].paymentDate, "dd-MM-yyyy", culture);
                    double intformonth = Convert.ToInt32(loantrans[i].interestForMonth);
                    int intRate = Convert.ToInt32(loantrans[i].intRate);
                    var loanAccountNumber = loantrans[i].loanAccontNumber;
                    int loanIntRate = Convert.ToInt32(loantrans[i].intRate);
                    double prec = Convert.ToInt32(loantrans[i].principalReceived);
                    double irec = Convert.ToInt32(loantrans[i].interestReceived);
                    double disburmentAMount = Convert.ToInt32(loantrans[i].disbursementAmount);

                    if (disburmentAMount != 0)
                    {
                        loantrans[i].interestForMonth = 0;
                        loantrans[i].outstandingInterest = outint;

                    }
                    else
                    {
                        int monthID = loantrans[i].paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loantrans[i].paymentDate.Split('/')[1]) <= 12 ? Convert.ToInt32(loantrans[i].paymentDate.Split('/')[1]) : -1) : -1;
                        int yearID = loantrans[i].paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loantrans[i].paymentDate.Split('/')[2])) : -1;
                        instno++;
                        loantrans[i].installmentNumber = instno;
                        double nodays = ((yearID - loantrans[i - 1].yearID) * 12) + monthID - loantrans[i - 1].monthID;
                        if (nodays == 0)
                        {
                            nodays = 1;
                        }
                        double intofmonth = Math.Round(((opBal * loanIntRate) / 1200) * nodays);
                        loantrans[i].interestForMonth = intofmonth;

                        loantrans[i].outstandingInterest = outint + (intofmonth - irec);
                    }
                    loantrans[i].closingBalance = opBal + (disburmentAMount - prec);
                    loantrans[i].openingBalance = opBal;
                    context.Entry(loantrans[i]).State = EntityState.Modified;
                    context.SaveChanges();
                }


                return 0;
            }
        }

        public static void correctDate(long loannumber)
        {
            using (LoanContext context = new LoanContext())
            {
                var loantrans = (from tran in context.loanTransDetails
                                 where tran.loanNumber == loannumber
                                 orderby tran.paymentDate
                                 select tran).ToList();
                foreach (loanTransDetails loan in loantrans)
                {
                    if (!string.IsNullOrEmpty(loan.paymentDate))
                    {
                        //loan.paymentDate = loan.paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loan.paymentDate.Split('/')[2]) <= 12 ? new DateTime(Convert.ToInt32(loan.paymentDate.Split('/')[1]), Convert.ToInt32(loan.paymentDate.Split('/')[0]), Convert.ToInt32(loan.paymentDate.Split('/')[1])).ToString("dd/MM/yyyy") : loan.paymentDate) : loan.paymentDate;
                        loan.paymentDate = loan.paymentDate.Split(' ').Length > 0 ? loan.paymentDate.Split(' ')[0] : loan.paymentDate;
                        loan.monthID = loan.paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loan.paymentDate.Split('/')[1]) <= 12 ? Convert.ToInt32(loan.paymentDate.Split('/')[1]) : -1) : -1;
                        loan.yearID = loan.paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loan.paymentDate.Split('/')[2])) : -1;
                        context.Entry(loan).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                }
            }
        }

        #endregion

        #region disbursement

        public static object getDisburmentListByID(object param)
        {
            long loanNumber = Convert.ToInt64(param.ToString());
            using (LoanContext context = new LoanContext())
            {
                var dislist = (from des in context.disbursementDetails
                               where des.loanNumber == loanNumber
                               orderby des.disburmentDate
                               select des).ToList();
                return dislist;
            }
        }

        public static object getDisburmentList(object param)
        {
            string[] arr = param as string[];
            string ss = arr[0];
            string aa = arr[1];
            DateTime date1 = new DateTime(Convert.ToInt32(ss.Split('/')[2]), Convert.ToInt32(ss.Split('/')[0]), Convert.ToInt32(ss.Split('/')[1]));
            DateTime date2 = new DateTime(Convert.ToInt32(aa.Split('/')[2]), Convert.ToInt32(aa.Split('/')[0]), Convert.ToInt32(aa.Split('/')[1]));
            using (LoanContext context = new LoanContext())
            {
                var result = (from list in context.disbursementDetails
                              join loan in context.loanDetails
                              on list.loanNumber equals loan.loanNumber
                              join loanee in context.loaneeDetails
                              on loan.loaneeID equals loanee.loaneeID
                              join member in context.mMember
                              on loanee.memberCode equals member.MemberCode
                              where list.disburmentStatus == 0 && list.disburmentDate >= date1 && list.disburmentDate <= date2
                              orderby list.disburmentDate descending
                              select new
                              {
                                  dis = list,
                                  member = member,
                                  loan = loan
                              }
                                ).ToList();
                List<disbursmentList> dlist = new List<disbursmentList>();
                foreach (var item in result)
                {
                    disbursmentList _list = new disbursmentList();
                    _list.autoID = item.dis.disbursmentID;
                    _list.Name = item.member.Name;
                    _list.loanTypeID = item.loan.loanTypeID;
                    _list.loanTypeName = (from name in context.loanType
                                          where name.loanID == item.loan.loanTypeID
                                          select name.loanName).FirstOrDefault();
                    _list.loanNumber = item.dis.loanNumber;
                    mMemberAssembly m = getLastAssembly(item.member.MemberCode);
                    _list.designation = m.Designation;
                    _list.disburmenDate = string.Format("{0:dd-MM-yyyy}", item.dis.disburmentDate);
                    _list.disburmentAMount = item.dis.disburmentAmount;
                    _list.totalLoanAmount = item.loan.loanAmount;
                    _list.disbursmentListofMember = (from des in context.disbursementDetails
                                                     where des.loanNumber == item.dis.loanNumber
                                                     orderby des.disburmentDate
                                                     select des).ToList();
                    dlist.Add(_list);
                }
                return dlist;
            }
        }

        public static object changeStatusofDisbursement(object param)
        {
            string[] arr = param as string[];
            int id = Convert.ToInt32(arr[1]);
            int status = Convert.ToInt32(arr[0]);
            using (LoanContext context = new LoanContext())
            {
                var dis = (from list in context.disbursementDetails
                           where list.disbursmentID == id
                           select list).FirstOrDefault();
                dis.disburmentStatus = status;
                context.Entry(dis).State = EntityState.Modified;
                context.SaveChanges();

                var trans = (from transt in context.loanTransDetails
                             where transt.loanNumber == dis.loanNumber
                             orderby transt.autoID descending
                             select transt).ToList().Take(1);
                if (trans.Count() > 0)
                {
                    foreach (var item in trans)
                    {
                        loanTransDetails loan = new loanTransDetails();
                        loan = item;
                        loan.installmentNumber = 0;
                        loan.interestForMonth = 0;
                        loan.interestReceived = 0;
                        loan.loanNumber = dis.loanNumber;
                        loan.monthID = DateTime.Now.Month;
                        loan.yearID = DateTime.Now.Year;
                        loan.transType = "cr";
                        loan.openingBalance = loan.closingBalance;
                        loan.closingBalance = loan.closingBalance + dis.disburmentAmount;
                        loan.disbursementAmount = dis.disburmentAmount;
                        loan.outstandingInterest = 0;
                        loan.payDate = DateTime.Now;
                        loan.paymentMode = "Disbursement";
                        loan.principalReceived = 0;
                        loan.paymentDate = dis.disburmentDate.ToString("dd/MM/yyyy");
                        loan.monthID = loan.paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loan.paymentDate.Split('/')[1]) <= 12 ? Convert.ToInt32(loan.paymentDate.Split('/')[1]) : -1) : -1;
                        loan.yearID = loan.paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loan.paymentDate.Split('/')[2])) : -1;
                        context.loanTransDetails.Add(loan);
                        context.SaveChanges();
                    }
                }
                else
                {
                    loanTransDetails loan = new loanTransDetails();
                    loan.outstandingInterest = 0;
                    loan.payDate = DateTime.Now;
                    loan.paymentMode = "Disbursement";
                    loan.principalReceived = 0;
                    loan.disbursementAmount = dis.disburmentAmount;
                    loan.installmentNumber = 0;
                    loan.interestForMonth = 0;
                    loan.interestReceived = 0;
                    loan.loanNumber = dis.loanNumber;
                    loan.monthID = DateTime.Now.Month;
                    loan.yearID = DateTime.Now.Year;
                    loan.transType = "cr";
                    loan.openingBalance = loan.closingBalance;
                    loan.closingBalance = loan.closingBalance + dis.disburmentAmount;
                    loan.paymentDate = dis.disburmentDate.ToString("dd/MM/yyyy");
                    loan.monthID = loan.paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loan.paymentDate.Split('/')[1]) <= 12 ? Convert.ToInt32(loan.paymentDate.Split('/')[1]) : -1) : -1;
                    loan.yearID = loan.paymentDate.Split('/').Length > 0 ? (Convert.ToInt32(loan.paymentDate.Split('/')[2])) : -1;
                    context.loanTransDetails.Add(loan);

                    context.SaveChanges();
                }
                context.Close();
                return null;
            }
        }

        #endregion

        #region statement

        public static object statementforLoan(object param)
        {
            long loannumber = Convert.ToInt64(param);
            using (LoanContext context = new LoanContext())
            {
                var statment = (from list in context.loanTransDetails
                                where list.loanNumber == loannumber &&
                                list.transType == "dr"
                                orderby new { list.yearID, list.monthID, list.autoID }
                                select list).ToList();
                var loan = (from _loan in context.loanDetails
                            join loanee in context.loaneeDetails
                            on _loan.loaneeID equals loanee.loaneeID
                            where _loan.loanNumber == loannumber
                            select new
                            {
                                _loan = _loan,
                                _loanee = loanee
                            }
                            ).FirstOrDefault();
                LoanStatement lstatement = new LoanStatement();
                lstatement.LoaNNumber = loannumber.ToString();
                lstatement.EMI = loan._loan.totalNoofEMI.ToString();
                lstatement.LoanAmount = loan._loan.loanAmount.ToString();
                lstatement.memberName = loan._loanee.memberName;
                lstatement.senctionDate = loan._loan.loanSanctionDate;
                List<statementList> slist = new List<statementList>();
                foreach (var item in statment)
                {
                    statementList _list = new statementList();
                    _list.closingBalance = item.closingBalance;
                    //try
                    //{
                    //    IFormatProvider culture = new CultureInfo("en-IN", true);
                    //    _list.Date = DateTime.ParseExact(item.paymentDate.ToString(), "MMM-yyyy", culture).ToString();
                    //    // _list.Date = DateTime.Parse(item.paymentDate).ToString("dd-MM-yyyy");
                    //}
                    //catch
                    //{
                    try
                    {
                        DateTime date1 = new DateTime(Convert.ToInt32(item.paymentDate.Split('/')[2]), Convert.ToInt32(item.paymentDate.Split('/')[1]), Convert.ToInt32(item.paymentDate.Split('/')[0]));
                        _list.Date = date1.ToString("MMM-yyyy");
                    }
                    catch
                    {
                        _list.Date = item.paymentDate.Split(' ').Length > 0 ? item.paymentDate.Split(' ')[0] : item.paymentDate;
                    }
                    // }
                    _list.autoID = item.autoID;
                    _list.iAmount = item.interestReceived;
                    _list.instno = item.installmentNumber;
                    _list.interest = item.interestForMonth;
                    _list.intsRate = item.intRate;
                    _list.openingBalance = item.openingBalance;
                    _list.outstandingint = item.outstandingInterest;
                    _list.pAmount = item.principalReceived;
                    _list.PaymentMode = item.paymentMode + " - " + item.paymentCode;
                    _list.DDOCode = item.DDOCode;
                    _list.TreasuryName = item.treasuryName;
                    _list.Voucher = item.voucherDate.HasValue ? (DateTime.Parse(item.voucherDate.ToString()).ToString("dd-MM-yyyy").Equals("01-01-1900") ? "" : item.voucherNumber + " @ " + (item.voucherDate.HasValue ? DateTime.Parse(item.voucherDate.ToString()).ToString("dd-MM-yyyy") : "")) : "";
                    slist.Add(_list);
                }
                lstatement.statement = slist;
                return lstatement;
            }
        }

        #endregion

        #region challan

        public static object GetActiveLoanListForChallan(object param)
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from _loanDetails in context.loanDetails
                              join _loaneeDetails in context.loaneeDetails
                              on _loanDetails.loaneeID equals _loaneeDetails.loaneeID
                              join _member in context.mMember
                              on _loaneeDetails.memberCode equals _member.MemberCode
                              join loantype in context.loanType on
                              _loanDetails.loanTypeID equals loantype.loanID
                              join loanTrans in context.loanTransDetails
                              on _loanDetails.loanNumber equals loanTrans.loanNumber
                              where _loanDetails.loanStatus == true && loanTrans.transType == "dr"
                              && loanTrans.transType != "Salary"
                              select new loanSearchResult
                              {
                                  memberName = _member.Name,
                                  memberCode = _member.MemberCode,
                                  Address = _member.PermanentAddress,
                                  contactNumber = _member.Mobile,
                                  loaneeCode = _loanDetails.loaneeID,
                                  anyActiveLoan = 0,
                                  loanCode = _loanDetails.loanNumber,
                                  loanAmount = _loanDetails.loanAmount,
                                  LoanName = loantype.loanName,
                                  EMIIAmount = loanTrans.interestReceived,
                                  EMIPAmount = loanTrans.principalReceived,
                                  EMIAmount = loanTrans.interestReceived + loanTrans.principalReceived,
                                  EMIDate = loanTrans.payDate,
                                  installmentID = loanTrans.autoID
                              }
                                         ).Distinct().ToList();
                foreach (var item in result)
                {
                    //item.EMIDate = ;
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    item.designation = m.Designation;
                    item.constituencyCode = m.ConstituencyCode;
                    item.constituencyName = (from con in context.mConstituency
                                             where con.ConstituencyCode == m.ConstituencyCode
                                             select con.ConstituencyName).FirstOrDefault();
                    item.anyActiveLoan = (from loanbool in context.loanDetails
                                          select loanbool).Count(a => a.loaneeID == item.loaneeCode);
                }
                return result;
            }
        }

        public static object getchallanDetailsByLoanID(object param)
        {
            int installmentID = Convert.ToInt32(param);
            using (LoanContext context = new LoanContext())
            {
                var result = (from loantrans in context.loanTransDetails
                              join loandetails in context.loanDetails
                              on loantrans.loanNumber equals loandetails.loanNumber
                              join loanee in context.loaneeDetails
                              on loandetails.loaneeID equals loanee.loaneeID
                              join loanType in context.loanType
                              on loandetails.loanTypeID equals loanType.loanID
                              where loantrans.autoID == installmentID
                              select new
                              {
                                  loantrans = loantrans,
                                  loan = loandetails,
                                  loanee = loanee,
                                  loanType = loanType
                              }

                                  ).FirstOrDefault();
                loanChallanDetails details = new loanChallanDetails();
                details.pAmount = result.loantrans.principalReceived + ".00";
                details.iAmount = result.loantrans.interestReceived + ".00";
                details.Amount = (result.loantrans.interestReceived + result.loantrans.principalReceived) + ".00";
                details.AmountInWords = "";
                details.DDOCODE = result.loantrans.DDOCode;
                details.loanName = result.loanType.loanName;
                details.paymentMode = result.loantrans.paymentMode + " " + result.loantrans.paymentCode + " Dated : " + result.loantrans.paymentDate;
                List<string> codeList = new List<string>();
                List<string> codeTextList = new List<string>();
                codeList.Add(result.loanType.majorCode);
                codeList.Add(result.loanType.subMajorCode);
                codeList.Add(result.loanType.minorCode);
                codeList.Add(result.loanType.subCode);
                codeTextList.Add(result.loanType.majorCode);
                codeTextList.Add(result.loanType.subMajorCode);
                codeTextList.Add(result.loanType.minorCode);
                codeTextList.Add(result.loanType.subCode);
                details.CodesList = codeList;
                details.CodetextList = codeTextList;
                details.TreasuryCode = result.loantrans.treasuryName;
                details.Tenderedby = result.loanee.memberName + " ---- " + result.loanee.address;
                details.loanNumber = result.loantrans.loanNumber;
                return details;
            }
        }

        #endregion

        #region financial Year Report

        public static object financialYearReport(object param)
        {
            DateTime[] datearry = param as DateTime[];
            DateTime date1 = datearry[0];
            DateTime date2 = datearry[1];
            using (LoanContext db = new LoanContext())
            {
                //var result = (from ltype in context.loanType
                //              join lDetails in context.loanDetails
                //              on ltype.loanID equals lDetails.loanTypeID
                //              join ltrans in context.loanTransDetails
                //              on lDetails.loanNumber equals ltrans.loanNumber
                //              where ltrans.payDate >= date1 && ltrans.payDate <= date2
                //              group ltype by ltype.loanName into res
                //              select res).ToList();
                var result = (from ltype in db.loanType
                              join ldetails in db.loanDetails on new { LoanID = ltype.loanID } equals new { LoanID = ldetails.loanTypeID } into ldetails_join
                              from ldetails in ldetails_join.DefaultIfEmpty()
                              join ltrans in db.loanTransDetails on ldetails.loanNumber equals ltrans.loanNumber into ltrans_join

                              from ltrans in ltrans_join.DefaultIfEmpty()
                              where ltrans.payDate >= date1 && ltrans.payDate <= date2
                              group new { ltype, ltrans } by new
                              {
                                  ltype.loanName
                              } into g
                              orderby
                               g.Key.loanName
                              select new
                              {
                                  g.Key.loanName,
                                  Column1 = (double?)g.Sum(p => p.ltrans.principalReceived),
                                  Column2 = (double?)g.Sum(p => p.ltrans.interestReceived)
                              }).ToList();
                List<YearlyReport> List = new List<YearlyReport>();
                foreach (var item in result)
                {
                    YearlyReport _list = new YearlyReport();
                    _list.loanName = item.loanName;
                    _list.pAmount = (item.Column1.HasValue ? item.Column1 : 0) + ".00";
                    _list.iAmount = (item.Column2.HasValue ? item.Column2 : 0) + ".00";
                    _list.Amount = ((item.Column1.HasValue ? item.Column1 : 0) + (item.Column2.HasValue ? item.Column2 : 0)) + ".00";
                    List.Add(_list);
                }

                return List;
            }
        }

        public static object individualReportDateWise(object param)
        {
            string[] arrr = param as string[];
            long loanNUmber = Convert.ToInt64(arrr[0]);
            DateTime date1 = new DateTime(Convert.ToInt32(arrr[1].Split('/')[2]), Convert.ToInt32(arrr[1].Split('/')[0]), Convert.ToInt32(arrr[1].Split('/')[1]));
            DateTime date2 = new DateTime(Convert.ToInt32(arrr[2].Split('/')[2]), Convert.ToInt32(arrr[2].Split('/')[0]), Convert.ToInt32(arrr[2].Split('/')[1]));

            using (LoanContext db = new LoanContext())
            {
                var result = (from ltype in db.loanType
                              join ldetails in db.loanDetails on new { LoanID = ltype.loanID } equals new { LoanID = ldetails.loanTypeID } into ldetails_join
                              from ldetails in ldetails_join.DefaultIfEmpty()
                              join ltrans in db.loanTransDetails on ldetails.loanNumber equals ltrans.loanNumber into ltrans_join
                              from ltrans in ltrans_join.DefaultIfEmpty()
                              where ltrans.payDate >= date1 && ltrans.payDate <= date2 && ldetails.loanNumber == loanNUmber
                              group new { ltype, ltrans } by new
                              {
                                  ltype.loanName
                              } into g
                              orderby
                               g.Key.loanName
                              select new
                              {
                                  g.Key.loanName,
                                  Column1 = (double?)g.Sum(p => p.ltrans.principalReceived),
                                  Column2 = (double?)g.Sum(p => p.ltrans.interestReceived)
                              });
                List<YearlyReport> List = new List<YearlyReport>();
                foreach (var item in result)
                {
                    YearlyReport _list = new YearlyReport();
                    _list.loanName = item.loanName;
                    _list.pAmount = (item.Column1.HasValue ? item.Column1 : 0) + ".00";
                    _list.iAmount = (item.Column2.HasValue ? item.Column2 : 0) + ".00";
                    _list.Amount = ((item.Column1.HasValue ? item.Column1 : 0) + (item.Column2.HasValue ? item.Column2 : 0)) + ".00";
                    List.Add(_list);
                }
                otherLoanReports oreport = new otherLoanReports();
                var details = (from loan in db.loanDetails
                               join loanee in db.loaneeDetails
                               on loan.loaneeID equals loanee.loaneeID
                               join lType in db.loanType
                               on loan.loanTypeID equals lType.loanID
                               where loan.loanNumber == loanNUmber
                               select new
                               {
                                   loan = loan,
                                   loanee = loanee,
                                   loanType = lType
                               }
                                  ).FirstOrDefault();
                oreport.EMI = details.loan.totalNoofEMI.ToString();
                oreport.LoanAmount = details.loan.loanAmount.ToString();
                oreport.LoaNNumber = details.loan.loanNumber.ToString();
                oreport.memberName = details.loanee.memberName;
                oreport.senctionDate = details.loan.loanSanctionDate;
                oreport.YearlyReport = List;
                oreport.loanName = details.loanType.loanName;
                oreport.FromDate = date1.ToString("dd/MM/yyyy");
                oreport.toDate = date2.ToString("dd/MM/yyyy");

                return oreport;
            }
        }

        public static object allReportDateWise(object param)
        {
            string[] arrr = param as string[];

            DateTime date1 = new DateTime(Convert.ToInt32(arrr[0].Split('/')[2]), Convert.ToInt32(arrr[0].Split('/')[0]), Convert.ToInt32(arrr[0].Split('/')[1]));
            DateTime date2 = new DateTime(Convert.ToInt32(arrr[1].Split('/')[2]), Convert.ToInt32(arrr[1].Split('/')[0]), Convert.ToInt32(arrr[1].Split('/')[1]));

            using (LoanContext db = new LoanContext())
            {
                var result = (from ltype in db.loanType
                              join ldetails in db.loanDetails on new { LoanID = ltype.loanID } equals new { LoanID = ldetails.loanTypeID } into ldetails_join
                              from ldetails in ldetails_join.DefaultIfEmpty()
                              join ltrans in db.loanTransDetails on ldetails.loanNumber equals ltrans.loanNumber into ltrans_join
                              from ltrans in ltrans_join.DefaultIfEmpty()
                              where ltrans.payDate >= date1 && ltrans.payDate <= date2
                              group new { ltype, ltrans } by new
                              {
                                  ltype.loanName
                              } into g
                              orderby
                               g.Key.loanName
                              select new
                              {
                                  g.Key.loanName,
                                  Column1 = (double?)g.Sum(p => p.ltrans.principalReceived),
                                  Column2 = (double?)g.Sum(p => p.ltrans.interestReceived)
                              });
                List<YearlyReport> List = new List<YearlyReport>();
                foreach (var item in result)
                {
                    YearlyReport _list = new YearlyReport();
                    _list.loanName = item.loanName;
                    _list.pAmount = (item.Column1.HasValue ? item.Column1 : 0) + ".00";
                    _list.iAmount = (item.Column2.HasValue ? item.Column2 : 0) + ".00";
                    _list.Amount = ((item.Column1.HasValue ? item.Column1 : 0) + (item.Column2.HasValue ? item.Column2 : 0)) + ".00";
                    List.Add(_list);
                }
                return List;
            }
        }

        #endregion


        public static object getDepartmentList()
        {
            using (LoanContext context = new LoanContext())
            {
                var result = (from dep in context.mDepartment
                            
                              orderby dep.deptname
                              select new fillDepartment
                              {
                                  DepartmentName = dep.deptname,
                                  DepartrmentID = dep.deptId
                              }
                                   );
                return result.ToList();
            }
        }
    }
}