﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using SBL.DAL;
using SBL.Service.Common;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.RoadPermit;
using SBL.DomainModel.ComplexModel;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data;

namespace SBL.Domain.Context.RoadPermit
{
    public class RoadPermitContext : DBBase<RoadPermitContext>
    {
        public RoadPermitContext()
            : base("eVidhan")
        {

            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = false;
            //this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<mMember> mMembers { get; set; }

        public DbSet<mAssembly> mAssembly { get; set; }

        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }

    //    public DbSet<mDesignation> mDesignations { get; set; }

        public DbSet<mConstituency> mConstituency { get; set; }

        public DbSet<mPermit> mPermit { get; set; }

        public DbSet<mSealedRoads> mSealedRoads { get; set; }

        public DbSet<mRestrictedRoads> mRestrictedRoads { get; set; }

        public DbSet<tPermitSealedRoads> tPermitSealedRoads { get; set; }

        public DbSet<tPermitRestrictedRoads> tPermitRestrictedRoads { get; set; }

        public DbSet<mMemberDesignation> mMemberDesignations { get; set; }
        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetAllPermitDetails":
                    {
                        return GetAllPermitDetails(param.Parameter);
                    }
                case "GetRoadPermitById":
                    {
                        return GetRoadPermitById(param.Parameter);
                    }
                case "UpdateRoadPermit":
                    {
                        return UpdateRoadPermit(param.Parameter);
                    }
                case "UpdateRoadPermitNew":
                    {
                        return UpdateRoadPermitNew(param.Parameter);
                    }
                    
                case "GetNewRoadPermitCode":
                    {
                        return GetNewRoadPermitCode(param.Parameter);
                    }
                case "GetMemberListByDesignation":
                    {
                        return GetMemberListByDesignation(param.Parameter);
                    }
                case "GetAllMembers":
                    {
                        return GetAllMembers(param.Parameter);
                    }
                    
                case "GetAllDesignations":
                    {
                        return GetAllDesignations(param.Parameter);
                    }
                case "GetLastConstituencyByMember":
                    {
                        return GetLastConstituencyByMember(param.Parameter);
                    }
                case "GetAllConstituency":
                    {
                        return GetAllConstituency(param.Parameter);
                    }
                case "GetAllSealedRoads":
                    {
                        return GetAllSealedRoads(param.Parameter);
                    }
                case "GetAllRestrictedRoads":
                    {
                        return GetAllRestrictedRoads(param.Parameter);
                    }
                case "SaveRoadPermit":
                    {
                        return SaveRoadPermit(param.Parameter);
                    }
                case "GetAddressByMember":
                    {
                        return GetAddressByMember(param.Parameter);
                    }

                case "GetAllPermitSealedDetails":
                    {
                        return GetAllPermitSealedDetails(param.Parameter);
                    }
                case "SavePermitSealedRoads":
                    {
                        return SavePermitSealedRoads(param.Parameter);
                    }
                case "SavePermitRestrictedRoads":
                    {
                        return SavePermitRestrictedRoads(param.Parameter);
                    }
                case "GetAllPermitRestrictedDetails":
                    {
                        return GetAllPermitRestrictedDetails(param.Parameter);
                    }
                case "SearchRoadPermitDetails":
                    {
                        return SearchRoadPermitDetails(param.Parameter);
                    }
                case "GetAllIndivisualPermitDetails":
                    {
                        return GetAllIndivisualPermitDetails(param.Parameter);
                    }

                    
            }
            return null;
        }

        public static object GetAllPermitDetails(object param)
        {
            try
            {
                using (RoadPermitContext db = new RoadPermitContext())
                {
                    var data = (from PER in db.mPermit
                                join M in db.mMembers on PER.MemberCode equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                orderby
                                  PER.PermitNo descending
                                select new
                                {
                                    PER,
                                    MemeberName = M.Name
                                }).ToList();

                    List<mPermit> result = new List<mPermit>();
                    foreach (var a in data)
                    {
                        mPermit partdata = new mPermit();
                        partdata = a.PER;
                        partdata.MemberName = (a.MemeberName != null ? a.MemeberName : string.Empty);
                        result.Add(partdata);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static object GetAllIndivisualPermitDetails(object param)
        {
            RoadPermitViewModel model = param as RoadPermitViewModel;
            try
            {
                using (RoadPermitContext db = new RoadPermitContext())
                {
                    var data = (from PER in db.mPermit
                                join M in db.mMembers on PER.MemberCode equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                where PER.PermitNo == model.PermitNo
                                orderby
                                  PER.PermitNo descending
                                select new
                                {
                                    PER,
                                    MemeberName = M.Name
                                }).ToList();

                    List<mPermit> result = new List<mPermit>();
                    foreach (var a in data)
                    {
                        mPermit partdata = new mPermit();
                        partdata = a.PER;
                        partdata.MemberName = (a.MemeberName != null ? a.MemeberName : string.Empty);
                        result.Add(partdata);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static object UpdateRoadPermit(object param)
        {
            try
            {
                using (RoadPermitContext db = new RoadPermitContext())
                {
                    mPermit model = param as mPermit;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mPermit.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateRoadPermitNew(object param)
        {
            try
            {
                using (RoadPermitContext db = new RoadPermitContext())
                {
                    RoadPermitViewModel model = param as RoadPermitViewModel;
                    mPermit modelto = new mPermit();

                    modelto = (from sb in db.mPermit where sb.PermitNo == model.PermitNo select sb).SingleOrDefault();
                    modelto.AssemblyId = model.AssemblyId;
                    modelto.ConstituencyCode = model.ConstituencyCode;
                    modelto.DesignationCode = model.DesignationCode;
                    modelto.MemberCode = model.MemberCode ?? 0;
                    modelto.PermitCode = model.PermitCode;
                    modelto.VehicleNo = model.VehicleNo;
                    modelto.ValidFrom = new DateTime(Convert.ToInt32(model.ValidFrom.Split('/')[2]),
                    Convert.ToInt32(model.ValidFrom.Split('/')[1]), Convert.ToInt32(model.ValidFrom.Split('/')[0]));

                    modelto.ValidTo = new DateTime(Convert.ToInt32(model.ValidTo.Split('/')[2]),
                    Convert.ToInt32(model.ValidTo.Split('/')[1]), Convert.ToInt32(model.ValidTo.Split('/')[0]));
                    modelto.ModifiedDate = DateTime.Now;
                    db.mPermit.Attach(modelto);
                    db.Entry(modelto).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();

                }


                using (RoadPermitContext context = new RoadPermitContext())
                {
                    RoadPermitViewModel viewModel = param as RoadPermitViewModel;
                    tPermitRestrictedRoads road = new tPermitRestrictedRoads();

                    var vps = context.tPermitRestrictedRoads.Where(a => a.PermitNo == viewModel.PermitNo).ToList();
                    foreach (var vp in vps)
                        context.tPermitRestrictedRoads.Remove(vp);
                    context.SaveChanges();

                    foreach (mRestrictedRoads restrictedRoads in viewModel.RestrictedRoadsCol)
                    {
                        road.PermitCode = viewModel.PermitCode;
                        road.PermitNo = Convert.ToInt16(viewModel.PermitNo);
                        road.RoadCode = restrictedRoads.RoadCode;
                        context.tPermitRestrictedRoads.Add(road);
                        context.SaveChanges();
                    }
                    context.Close();
                }
                using (RoadPermitContext context = new RoadPermitContext())
                {
                    RoadPermitViewModel viewModel = param as RoadPermitViewModel;
                    tPermitSealedRoads road = new tPermitSealedRoads();

                    var vps = context.tPermitSealedRoads.Where(a => a.PermitNo == viewModel.PermitNo).ToList();
                    foreach (var vp in vps)
                        context.tPermitSealedRoads.Remove(vp);
                    context.SaveChanges();

                    foreach (mSealedRoads sealedRoads in viewModel.SealedRoadsCol)
                    {
                        road.PermitCode = viewModel.PermitCode;
                        road.PermitNo = Convert.ToInt16(viewModel.PermitNo);
                        road.RoadCode = sealedRoads.RoadCode;
                        context.tPermitSealedRoads.Add(road);
                        context.SaveChanges();
                    }
                    context.Close();
                }


                //using (RoadPermitContext context = new RoadPermitContext())
                //{
                //    RoadPermitViewModel viewModel = param as RoadPermitViewModel;
                //    foreach (mRestrictedRoads restrictedRoads in viewModel.RestrictedRoadsCol)
                //    {
                //        tPermitRestrictedRoads road = new tPermitRestrictedRoads();
                //        road = (from sb in context.tPermitRestrictedRoads where sb.PermitNo == viewModel.PermitNo select sb).SingleOrDefault();
                //        road.PermitCode = viewModel.PermitCode;
                //        road.PermitNo = Convert.ToInt16(viewModel.PermitNo);
                //        road.RoadCode = restrictedRoads.RoadCode;
                //        context.tPermitRestrictedRoads.Attach(road);
                //        context.SaveChanges();
                //    }
                //    context.Close();
                //}
                // using (RoadPermitContext db = new RoadPermitContext())
                // {
                //  //   RoadPermitViewModel viewModel = param as RoadPermitViewModel;
                // //    foreach (mSealedRoads sealedRoads in viewModel.SealedRoadsCol)
                ////     {
                //         tPermitSealedRoads road = new tPermitSealedRoads();
                //         db.tPermitSealedRoads.Attach(road);
                //         db.Entry(road).State = EntityState.Modified;
                //         //road.PermitCode = viewModel.PermitCode;
                //         //road.PermitNo = Convert.ToInt16(viewModel.PermitNo);
                //         //road.RoadCode = sealedRoads.RoadCode;
                //         //context.tPermitSealedRoads.Add(road);
                //         db.SaveChanges();
                //   //  }

                //     db.Close();
                // }

                return null;
            }
            catch
            {
                throw;
            }
        }
        public static object GetRoadPermitById(object param)
        {
            mPermit parameter = param as mPermit;

            RoadPermitContext db = new RoadPermitContext();

            var data = (from P in db.mPermit
                        join M in db.mMembers on P.MemberCode equals M.MemberCode into M_join
                        from M in M_join.DefaultIfEmpty()
                        join D in db.mMemberDesignations on new { Desigcode = P.DesignationCode } equals new { Desigcode = D.memDesigcode } into D_join
                        from D in D_join.DefaultIfEmpty()
                        join C in db.mConstituency on new { ConstituencyCode = P.ConstituencyCode } equals new { ConstituencyCode = C.ConstituencyCode } into C_join
                        from C in C_join.DefaultIfEmpty()
                        where
                          P.PermitNo == parameter.PermitNo &&
                          C.AssemblyID == P.AssemblyId
                        select new
                        {
                            P,
                            Name = M.Name,
                            Prefix = M.Prefix,
                            Address = M.ShimlaAddress,
                            Designame = D.memDesigname,
                            ConstituencyName = C.ConstituencyName
                        }).FirstOrDefault();



            mPermit partdata = new mPermit();
            partdata = data.P;
            partdata.MemberName = data.Name;
            partdata.Prefix = data.Prefix;
            partdata.Address = data.Address;
            partdata.Designation = data.Designame;
            partdata.ConstituencyName = data.ConstituencyName;

            return partdata;
        }

        public static object GetNewRoadPermitCode(object param)
        {
            string permitCode = string.Empty;

            try
            {
                RoadPermitContext context = new RoadPermitContext();
                int lastId = 0;

                var Value = (from P in context.mPermit
                             orderby P.PermitNo descending
                             select P.PermitNo).FirstOrDefault();

                int.TryParse(Value.ToString(), out lastId);

                permitCode = lastId + 1 + "/" + DateTime.Now.Year;

                return permitCode;
            }
            catch (Exception ex)
            {
                return permitCode;
            }
        }

        public static object GetAddressByMember(object param)
        {
            try
            {
                RoadPermitContext context = new RoadPermitContext();

                RoadPermitViewModel model = param as RoadPermitViewModel;

                var address = (from member in context.mMembers
                               where member.MemberCode == model.MemberCode
                               select member.PermanentAddress).FirstOrDefault();

                return address;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object GetAllConstituency(object param)
        {
            try
            {
                RoadPermitContext context = new RoadPermitContext();
                RoadPermitViewModel model = param as RoadPermitViewModel;

                var query = (from constituencies in context.mConstituency
                             where constituencies.AssemblyID == model.AssemblyId
                             orderby constituencies.ConstituencyID descending
                             select constituencies).OrderBy(m => m.ConstituencyName).ToList();

                return query;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static object GetLastConstituencyByMember(object param)
        {
            try
            {
                RoadPermitContext context = new RoadPermitContext();

                RoadPermitViewModel model = param as RoadPermitViewModel;

                var constituencyID = (from assembly in context.mMemberAssembly
                                      join mConstituencyOrder in context.mConstituency
                                      on assembly.ConstituencyCode equals mConstituencyOrder.ConstituencyCode
                                      where assembly.MemberID == model.MemberCode && assembly.AssemblyID == model.AssemblyId
                                      orderby mConstituencyOrder.ConstituencyCode descending
                                      select assembly.ConstituencyCode).FirstOrDefault();

                return constituencyID;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static object GetAllMembers(object param)
        {
            try
            {
                RoadPermitContext context = new RoadPermitContext();
                var GetAllMembers = (from members in context.mMembers
                                           select members).OrderBy(m=>m.Name).ToList();
                return GetAllMembers;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object GetMemberListByDesignation(object param)
        {
            try
            {
                RoadPermitViewModel model = param as RoadPermitViewModel;

                //int designationID = int.Parse(model.DesignationCode.ToString());

                
                RoadPermitContext context = new RoadPermitContext();

                if (model != null)
                {

                    var membersByDesg = (from members in context.mMembers
                                         join memberAssembly in context.mMemberAssembly
                                         on members.MemberCode equals memberAssembly.MemberID
                                         where memberAssembly.AssemblyID == model.AssemblyId && memberAssembly.DesignationID == model.DesignationCode
                                         // where memberAssembly.DesignationID == model.DesignationCode && memberAssembly.AssemblyID=model.AssemblyId
                                         select members).Distinct().ToList();

                    var validPermitMembers = (from members in context.mPermit
                                              where members.ValidFrom.Value.Year == DateTime.Now.Year
                                              select members).ToList();




                    var membersWithNoPermit = (from members in membersByDesg
                                               //where !(from validMembers in validPermitMembers
                                               //        where validMembers.MemberCode == members.MemberCode
                                               //        select validMembers.MemberCode)
                                               //        .Contains(members.MemberCode)
                                               select members).OrderBy(m => m.Name).ToList();


                    return membersWithNoPermit;
                }
                else
                {
                    var membersByDesg = (from members in context.mMembers
                                         join memberAssembly in context.mMemberAssembly
                                         on members.MemberCode equals memberAssembly.MemberID
                                         //where memberAssembly.AssemblyID == model.AssemblyId && memberAssembly.DesignationID == model.DesignationCode
                                         //  where memberAssembly.DesignationID == model.DesignationCode && memberAssembly.AssemblyID=model.AssemblyId
                                         select members).Distinct().ToList();

                    var validPermitMembers = (from members in context.mPermit
                                              where members.ValidFrom.Value.Year == DateTime.Now.Year
                                              select members).ToList();




                    var membersWithNoPermit = (from members in membersByDesg
                                               //where !(from validMembers in validPermitMembers
                                               //        where validMembers.MemberCode == members.MemberCode
                                               //        select validMembers.MemberCode)
                                               //        .Contains(members.MemberCode)
                                               select members).OrderBy(m => m.Name).ToList();


                    return membersWithNoPermit;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object GetAllSealedRoads(object param)
        {
            try
            {
                RoadPermitContext context = new RoadPermitContext();

                var query = (from sealedRoads in context.mSealedRoads
                             select sealedRoads).ToList();
                return query;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static object GetAllRestrictedRoads(object param)
        {
            try
            {
                RoadPermitContext context = new RoadPermitContext();

                var query = (from sealedRoads in context.mRestrictedRoads
                             select sealedRoads).ToList();
                return query;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object GetAllDesignations(object param)
        {
            try
            {
                RoadPermitContext context = new RoadPermitContext();

                var query = (from designations in context.mMemberDesignations
                             select designations).ToList();
                return query;
            }
            catch (Exception ex)
            {
                return null;
            }
        }





        public static object SaveRoadPermit(object param)
        {
            try
            {


                mPermit model = new mPermit();
                using (RoadPermitContext context = new RoadPermitContext())
                {
                    RoadPermitViewModel viewModel = param as RoadPermitViewModel;

                    model.DesignationCode = viewModel.DesignationCode;
                    model.ConstituencyCode = viewModel.ConstituencyCode;
                    model.AssemblyId = viewModel.AssemblyId;
                    model.PermitCode = viewModel.PermitCode;

                    model.MemberCode = viewModel.MemberCode ?? 0;

                    model.VehicleNo = viewModel.VehicleNo;

                    model.ValidFrom = new DateTime(Convert.ToInt32(viewModel.ValidFrom.Split('/')[2]),

                    Convert.ToInt32(viewModel.ValidFrom.Split('/')[1]), Convert.ToInt32(viewModel.ValidFrom.Split('/')[0]));

                    model.ValidTo = new DateTime(Convert.ToInt32(viewModel.ValidTo.Split('/')[2]),

                    Convert.ToInt32(viewModel.ValidTo.Split('/')[1]), Convert.ToInt32(viewModel.ValidTo.Split('/')[0]));

                    //model.ValidFrom = string.IsNullOrEmpty(viewModel.ValidFrom) ? DateTime.Now : DateTime.ParseExact(viewModel.ValidFrom, "dd/mm/yyyy", null);

                    //model.ValidTo = string.IsNullOrEmpty(viewModel.ValidTo) ? DateTime.Now : DateTime.ParseExact(viewModel.ValidTo, "dd/mm/yyyy", null);

                    model.CreationDate = DateTime.Now;

                    model.ModifiedDate = DateTime.Now;

                    context.mPermit.Add(model);

                    context.SaveChanges();

                    context.Close();


                }

                return model.PermitNo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        //  This is for searching road permit
        public static object SearchRoadPermitDetails(object param)
        {
            try
            {
                string Data = param as string;
                DateTime FYFrom = new DateTime(Convert.ToInt32(Data.Split('-')[0]), 4, 01);
                DateTime FYTo = new DateTime(Convert.ToInt32(Data.Split('-')[1]), 3, 31);

                using (RoadPermitContext db = new RoadPermitContext())
                {
                    var data = (from P in db.mPermit
                                join M in db.mMembers on P.MemberCode equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join D in db.mMemberDesignations on new { Desigcode = P.DesignationCode } equals new { Desigcode = D.memDesigcode } into D_join
                                from D in D_join.DefaultIfEmpty()
                                join C in db.mConstituency on new { ConstituencyCode = P.ConstituencyCode } equals new { ConstituencyCode = C.ConstituencyCode } into C_join
                                from C in C_join.DefaultIfEmpty()
                                where
                                  P.ValidFrom >= FYFrom && P.ValidTo <= FYTo && C.AssemblyID == P.AssemblyId
                                select new
                                {
                                    P,
                                    Name = M.Name,
                                    Prefix = M.Prefix,
                                    Address = M.ShimlaAddress,
                                    Designame = D.memDesigname,
                                    ConstituencyName = C.ConstituencyName
                                }).ToList();

                    List<mPermit> result = new List<mPermit>();
                    foreach (var a in data)
                    {
                        mPermit partdata = new mPermit();
                        partdata = a.P;
                        partdata.MemberName = a.Name;
                        partdata.Prefix = a.Prefix;
                        partdata.Address = a.Address;
                        partdata.Designation = a.Designame;
                        partdata.ConstituencyName = a.ConstituencyName;
                        result.Add(partdata);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Permit Sealed Roads
        public static object GetAllPermitSealedDetails(object param)
        {
            try
            {
                tPermitSealedRoads obj = param as tPermitSealedRoads;
                using (RoadPermitContext db = new RoadPermitContext())
                {
                    var data = (from SR in db.tPermitSealedRoads
                                join M in db.mSealedRoads on SR.RoadCode equals M.RoadCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                orderby SR.SlNo
                                where SR.PermitNo == obj.PermitNo
                                select new
                                {
                                    SR,
                                    Description = M.Description
                                }).ToList();

                    List<tPermitSealedRoads> result = new List<tPermitSealedRoads>();
                    foreach (var a in data)
                    {
                        tPermitSealedRoads partdata = new tPermitSealedRoads();
                        partdata = a.SR;
                        partdata.Description = (a.Description != null ? a.Description : string.Empty);
                        result.Add(partdata);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static object SavePermitSealedRoads(object param)
        {
            try
            {
                using (RoadPermitContext context = new RoadPermitContext())
                {
                    RoadPermitViewModel viewModel = param as RoadPermitViewModel;
                    foreach (mSealedRoads sealedRoads in viewModel.SealedRoadsCol)
                    {
                        tPermitSealedRoads road = new tPermitSealedRoads();

                        road.PermitCode = viewModel.PermitCode;
                        road.PermitNo = Convert.ToInt16(viewModel.PermitNo);
                        road.RoadCode = sealedRoads.RoadCode;
                        context.tPermitSealedRoads.Add(road);
                        context.SaveChanges();
                    }

                    context.Close();
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Permit Restricted Roads
        public static object SavePermitRestrictedRoads(object param)
        {
            try
            {
                using (RoadPermitContext context = new RoadPermitContext())
                {
                    RoadPermitViewModel viewModel = param as RoadPermitViewModel;
                    foreach (mRestrictedRoads restrictedRoads in viewModel.RestrictedRoadsCol)
                    {
                        tPermitRestrictedRoads road = new tPermitRestrictedRoads();

                        road.PermitCode = viewModel.PermitCode;
                        road.PermitNo = Convert.ToInt16(viewModel.PermitNo);
                        road.RoadCode = restrictedRoads.RoadCode;
                        context.tPermitRestrictedRoads.Add(road);
                        context.SaveChanges();
                    }
                    context.Close();
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }



        }

        public static object GetAllPermitRestrictedDetails(object param)
        {
            try
            {
                tPermitRestrictedRoads obj = param as tPermitRestrictedRoads;
                using (RoadPermitContext db = new RoadPermitContext())
                {
                    var data = (from RR in db.tPermitRestrictedRoads
                                join M in db.mRestrictedRoads on RR.RoadCode equals M.RoadCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                orderby RR.SlNo
                                where RR.PermitNo == obj.PermitNo
                                select new
                                {
                                    RR,
                                    Description = M.Description
                                }).ToList();

                    List<tPermitRestrictedRoads> result = new List<tPermitRestrictedRoads>();
                    foreach (var a in data)
                    {
                        tPermitRestrictedRoads partdata = new tPermitRestrictedRoads();
                        partdata = a.RR;
                        partdata.Description = (a.Description != null ? a.Description : string.Empty);
                        result.Add(partdata);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
