﻿using SBL.DAL;
using SBL.DomainModel.Models.Constituency;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Village
{
    public class VillageContext : DBBase<VillageContext>
    {
        public VillageContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public virtual DbSet<mVillage> mVillage { get; set; }
       public static object Excute(SBL.Service.Common.ServiceParameter param)
       {
           if (null == param)
           {
               return null;
           }
           switch(param.Method)
           {
               case "GetAllVillages": { return GetAllVillages(param.Parameter); }
               case "CreateVillage": { CreateVillage(param.Parameter); break; }
               case "UpdateVillage": { return UpdateVillage(param.Parameter); }
               case "GetVillageBasedOnId": { return GetVillageBasedOnId(param.Parameter); }
               case "DeleteVillage": { return DeleteVillage(param.Parameter); }
              
           }
           return null;
       }

       static object GetAllVillages(object param)
       {
           try
           {

               using (VillageContext db = new VillageContext())
               {
                   //    mVillage model = new mVillage();
                   //    var data = (from v in db.mVillage
                   //                orderby v.VillageID descending
                   //                select v).ToList();
                   //    return data;
                   //}
                   mVillage model = param as mVillage;
                   //string searchText = model.PanchayatName;
                   var data = (from a in db.mVillage
                               orderby a.VillageID descending
                               where a.IsDeleted==null
                               select a).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
                   var data1 = (from a in db.mVillage
                                // where a.PanchayatID
                                orderby a.VillageID descending
                                where a.IsDeleted==null
                                select a).Count();
                   int totalRecords = data1;
                   model.ResultCount = totalRecords;
                   var results = data.ToList();
                   model.VillageList = results;
                   return model;
               }

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }

       static void CreateVillage(object param)
       {
           try
           {
               using(VillageContext db= new VillageContext())
               {
                   mVillage model= param as mVillage;
                   model.CreatedWhen = DateTime.Now;
                   model.ModifiedWhen = DateTime.Now;
                   db.mVillage.Add(model);
                   db.SaveChanges();
                   db.Close();
               }
           }
           catch (Exception ex)
           {

               throw ex;
           }
       }

       static object UpdateVillage(object param)
       {
           try
           {
               using(VillageContext db= new VillageContext())
               {
                   mVillage model = param as mVillage;
                   model.CreatedWhen = DateTime.Now;
                   model.ModifiedWhen = DateTime.Now;
                   db.mVillage.Attach(model);
                   db.Entry(model).State = EntityState.Modified;
                   db.SaveChanges();
                   db.Close();

               }
               return GetAllVillages(param);

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }

       static mVillage GetVillageBasedOnId(object param)
       {
           try
           {
               using(VillageContext db= new VillageContext())
               {
                   mVillage model = param as mVillage;
                   var data = db.mVillage.SingleOrDefault(v => v.VillageID == model.VillageID);
                       return data;
               }

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }

       static object DeleteVillage(object param)
       {
           try
           {
               using(VillageContext db= new VillageContext())
               {
                   mVillage model = param as mVillage;
                   mVillage villToDelete = db.mVillage.SingleOrDefault(v => v.VillageID == model.VillageID);
                   if(villToDelete!=null)
                   {
                       villToDelete.IsDeleted = true;

                   }
                  // db.mVillage.Remove(villToDelete);
                   db.SaveChanges();
                   db.Close();

               }

               return GetAllVillages(param);

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }
    }
}
 