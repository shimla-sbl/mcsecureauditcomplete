﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SBL.DAL;
using SBL.DomainModel.Models.News;
using SBL.DomainModel.Models.SiteSetting;
using SBL.Service.Common;
using System.Data;
using SBL.DomainModel.Models.Category;

namespace SBL.Domain.Context.ANews
{
    public class ANewsContext : DBBase<ANewsContext>
    {
        public ANewsContext()
            : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mNews> mNews { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<SiteSettings> mSiteSettings { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "AddNews": { return AddNews(param.Parameter); }
                    case "GetNews": { return GetNews(); }
                    case "EditNews": { return EditNews(param.Parameter); }
                    case "UpdateNews": { return UpdateNews(param.Parameter); }
                    case "DeleteNews": { DeleteNews(param.Parameter); break; }
                    case "GetNewsById": { return GetNewsById(param.Parameter); }
                    case "GetAllCategoryType": { return GetAllCategoryType(param.Parameter); }


                    case "AddNewsCategory": { return AddNewsCategory(param.Parameter); }
                    case "GetNewsCategory": { return GetNewsCategory(); }
                    case "EditNewsCategory": { return EditNewsCategory(param.Parameter); }
                    case "UpdateNewsCategory": { return UpdateNewsCategory(param.Parameter); }
                    case "DeleteNewsCategory": { DeleteNewsCategory(param.Parameter); break; }
                    case "GetNewsByIdCategory": { return GetNewsByIdCategory(param.Parameter); }
                    case "IsNewsCategoryChildExist": { return IsNewsCategoryChildExist(param.Parameter); }


                }
            }
            return null;
        }
        static object GetAllCategoryType(object param)
        {
            try
            {
                using (ANewsContext db = new ANewsContext())
                {
                    //Category cc= param as Category;
                    //  Organization Organization = param as Organization;
                    var data = (from value in db.Category where (value.CategoryType == 3 || value.CategoryType == 4) && value.Status == 1 select value).ToList();
                    //var data = db.Category.Where(

                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetNews()
        {
            try
            {
                using (ANewsContext db = new ANewsContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from a in db.mNews join category in db.Category on a.CategoryID equals category.CategoryID select new { news = a, CategoryTypeName = category.CategoryName }).OrderByDescending(a => a.news.CreatedDate).ToList();//db.mNews.ToList();
                    var siteSettingData = (from a in db.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
                    List<mNews> result = new List<mNews>();

                    foreach (var a in data)
                    {
                        mNews partdata = new mNews();

                        partdata = a.news;
                        partdata.CategoryTypeName = a.CategoryTypeName;
                        partdata.ImageAcessurl = siteSettingData.SettingValue + a.news.Images + "/Thumb/";
                        result.Add(partdata);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static object AddNews(object param)
        {
            try
            {
                mNews news = param as mNews;
                using (ANewsContext newsContext = new ANewsContext())
                {

                    news.CreatedDate = DateTime.Now;
                    news.StartPublish = DateTime.Now;
                    news.ModifiedDate = DateTime.Now;
                    news.FinishPublish = DateTime.Now;
                    newsContext.mNews.Add(news);
                    newsContext.SaveChanges();

                    //if (!string.IsNullOrEmpty(news.FileName))
                    //{
                    //    news.FileName = news.LatestNewsID + "_" + news.FileName;
                    //    newsContext.mNews.Attach(news);
                    //    newsContext.Entry(news).State = EntityState.Modified;
                    //    newsContext.SaveChanges();
                    //}
                    newsContext.Close();


                }

                //return string.IsNullOrEmpty(news.FileName) ? "" : news.FileName;
                return news.LatestNewsID;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        static object GetNewsById(object param)
        {
            try
            {
                using (ANewsContext db = new ANewsContext())
                {
                    mNews NewsToEdit = param as mNews;
                    var siteSettingData = (from a in db.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
                    var data = db.mNews.SingleOrDefault(o => o.LatestNewsID == NewsToEdit.LatestNewsID);
                    
                    data.FileAcessPath = siteSettingData.SettingValue;

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        static object EditNews(object param)
        {
            try
            {
                using (ANewsContext newsContext = new ANewsContext())
                {
                    int newsID = Convert.ToInt32(param);
                    mNews notice = (from value in newsContext.mNews where value.LatestNewsID == newsID select value).FirstOrDefault();
                    return notice;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return null;
        }

        static object UpdateNews(object param)
        {

            try
            {
                mNews UpdateNews = param as mNews;
                using (ANewsContext db = new ANewsContext())
                {
                    var data = (from a in db.mNews where UpdateNews.LatestNewsID == a.LatestNewsID select a).FirstOrDefault();


                    data.ModifiedDate = DateTime.Now;
                    data.NewsDescription = UpdateNews.NewsDescription;
                    data.LocalNewsTitle = UpdateNews.LocalNewsTitle;
                    data.LocalNewsDescription = UpdateNews.LocalNewsDescription;
                    data.NewsTitle = UpdateNews.NewsTitle;
                    data.CategoryID = UpdateNews.CategoryID;
                    data.Status = UpdateNews.Status;
                    data.Images = UpdateNews.Images;
                    data.FileName = UpdateNews.FileName;
                    data.NewsEventDate = UpdateNews.NewsEventDate;
                    data.ThumbName = UpdateNews.ThumbName;

                    data.Images1 = UpdateNews.Images1;
                    data.FileName1 = UpdateNews.FileName1;
                    data.ThumbName1 = UpdateNews.ThumbName1;


                    db.mNews.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateNews;
            }
            catch
            {
                throw;
            }
        }

        static void DeleteNews(object param)
        {
            try
            {
                mNews DataTodelete = param as mNews;
                int id = DataTodelete.LatestNewsID;
                using (ANewsContext db = new ANewsContext())
                {
                    var NewsToDelete = db.mNews.SingleOrDefault(a => a.LatestNewsID == id);
                    db.mNews.Remove(NewsToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }






        static object GetNewsCategory()
        {
            try
            {
                using (ANewsContext db = new ANewsContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from value in db.Category where (value.CategoryType == 4 || value.CategoryType == 3) select value).OrderByDescending(a => a.CreatedDate).ToList();
                    // var data = db.Category.ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static object AddNewsCategory(object param)
        {
            try
            {
                using (ANewsContext newsContext = new ANewsContext())
                {
                    Category news = param as Category;
                    news.CategoryType = 1;
                    news.CreatedDate = DateTime.Now;
                    news.ModifiedDate = DateTime.Now;
                    news.StartPublish = DateTime.Now;
                    news.StopPublish = DateTime.Now;
                    newsContext.Category.Add(news);
                    newsContext.SaveChanges();
                    newsContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        static object EditNewsCategory(object param)
        {
            try
            {
                using (ANewsContext newsContext = new ANewsContext())
                {
                    int newsID = Convert.ToInt32(param);
                    Category notice = (from value in newsContext.Category where value.CategoryID == newsID select value).FirstOrDefault();
                    return notice;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }

        static object UpdateNewsCategory(object param)
        {

            try
            {
                Category UpdateNews = param as Category;
                using (ANewsContext db = new ANewsContext())
                {

                    var data = (from a in db.Category where UpdateNews.CategoryID == a.CategoryID select a).FirstOrDefault();
                    // UpdateNews.CategoryType = "News";
                    //// UpdateNews.CreatedDate = DateTime.Now;
                    // UpdateNews.ModifiedDate = DateTime.Now;
                    // data.StartPublish = UpdateNews.StartPublish;
                    data.CategoryDescription = UpdateNews.CategoryDescription;
                    data.LocalCategoryDescription = UpdateNews.LocalCategoryDescription;
                    data.LocalCategoryName = UpdateNews.LocalCategoryName;
                    data.CategoryName = UpdateNews.CategoryName;
                    data.Status = UpdateNews.Status;
                    data.CategoryID = UpdateNews.CategoryID;
                    db.Category.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
                return UpdateNews;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static void DeleteNewsCategory(object param)
        {
            try
            {
                Category DataTodelete = param as Category;
                int id = DataTodelete.CategoryID;
                using (ANewsContext db = new ANewsContext())
                {
                    var NewsToDelete = db.Category.SingleOrDefault(a => a.CategoryID == id);
                    db.Category.Remove(NewsToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static object GetNewsByIdCategory(object param)
        {
            try
            {
                using (ANewsContext db = new ANewsContext())
                {
                    Category NewsToEdit = param as Category;

                    var data = db.Category.SingleOrDefault(o => o.CategoryID == NewsToEdit.CategoryID);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        private static object IsNewsCategoryChildExist(object p)
        {
            try
            {
                using (ANewsContext ctx = new ANewsContext())
                {
                    Category data = (Category)p;

                    var isExist = (from a in ctx.mNews where a.CategoryID == data.CategoryID select a).Count() > 0;

                    return isExist;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}
