﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Pension;
using SBL.DomainModel.Models.SiteSetting;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.Pension
{
    public class pensionContext : DBBase<pensionContext>
    {
        public pensionContext()
            : base("eVidhan")
        {
        }

        public virtual DbSet<mMember> mMember { get; set; }
        public virtual DbSet<mMemberNominee> mMemberNominee { get; set; }
        public virtual DbSet<PensionerDetails> pensionerDetails { get; set; }

        public virtual DbSet<mMemberAssembly> mMemberAssembly { get; set; }

        public virtual DbSet<SiteSettings> mSiteSettings { get; set; }



        public virtual DbSet<mConstituency> mConstituency { get; set; }

        public virtual DbSet<mAssembly> mAssembly { get; set; }
        public virtual DbSet<mMemberAccountsDetails> mMemberAccountsDetails { get; set; }
        public virtual DbSet<pensionerTenuresDetails> pensionerTenuresDetails { get; set; }
        public virtual DbSet<mMemberDesignation> mMemberDesignations { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "getXMemberList": { return getXMemberList(); }
                case "getMemberDetailsByID": { return getMemberDetailsByID(param.Parameter); }
                case "savepensionerTenuresDetails": { return savepensionerTenuresDetails(param.Parameter); }
                case "savePensioner": { return savePensioner(param.Parameter); }
                case "pensionReport": { return pensionReport(); }
                case "checkMemberList": { return checkMemberList(param.Parameter); }
                case "deleteTenureDetails": { return deleteTenureDetails(param.Parameter); }
                case "FamilypensionReport": { return FamilypensionReport(); }


            }
            return null;
        }

        public static object getXMemberList()
        {
            using (pensionContext context = new pensionContext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);

                var cMemberCodeList = (from Assembly in context.mMemberAssembly
                                       where Assembly.AssemblyID == aid
                                       select Assembly.MemberID).ToList();

                var result = (from member in context.mMember
                              where !(cMemberCodeList.Contains(member.MemberCode))
                              select member
                                   );
                return result.ToList();
            }

            return null;
        }

        public static object checkMemberList(object param)
        {
            using (pensionContext context = new pensionContext())
            {
                int memberID = Convert.ToInt32(param);
                var count = (from list in context.pensionerDetails
                             where list.memberCode == memberID
                             select list).Count();
                return count;
            }
        }

        public static object deleteTenureDetails(object param)
        {
            using (pensionContext context = new pensionContext())
            {
                int memberID = Convert.ToInt32(param);
                var ped = (from list in context.pensionerTenuresDetails
                           where list.pensionerID == memberID
                           select list).ToList();
                foreach (var item in ped)
                {
                    context.pensionerTenuresDetails.Remove(item);
                    context.SaveChanges();
                }
                context.Close();
                return null;

            }
        }
        public static object getMemberDetailsByID(object param)
        {
            using (pensionContext context = new pensionContext())
            {
                int memberCode = Convert.ToInt32(param);
                var result = (from member in context.mMember
                              where member.MemberCode == memberCode
                              select member
                                 ).FirstOrDefault();
                var assesmblyInfo = (from _assemblyInfo in context.mMemberAssembly
                                     join assembly in context.mAssembly
                                     on _assemblyInfo.AssemblyID equals assembly.AssemblyCode
                                     orderby assembly.AssemblyID descending
                                     where _assemblyInfo.MemberID == memberCode
                                     select _assemblyInfo).FirstOrDefault();
                var cons = (from _cons in context.mConstituency
                            where _cons.ConstituencyCode == assesmblyInfo.ConstituencyCode
                            select _cons.ConstituencyName).FirstOrDefault();
                var bankDtails = (from _bankDetails in context.mMemberAccountsDetails
                                  where _bankDetails.MemberCode == memberCode
                                  select _bankDetails).FirstOrDefault();

                var assemblyinfo = (from assembly in context.mMemberAssembly
                                    join mAssemblyOrder in context.mAssembly
                                    on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                    where assembly.MemberID == memberCode
                                    orderby mAssemblyOrder.AssemblyID ascending
                                    select assembly
                               );

                pensionerDetailsviewModel _pDetails = new pensionerDetailsviewModel();
                _pDetails.memberCode = memberCode;
                _pDetails.pensionerNo = 0;
                _pDetails.PensionerName = result.Name;
                _pDetails.DesignationCode = assesmblyInfo.DesignationID;
                _pDetails.DesignationName = (from designation in context.mMemberDesignations
                                             where designation.memDesigcode == assesmblyInfo.DesignationID
                                             select designation.memDesigname).FirstOrDefault();
                _pDetails.MobileNo = result.Mobile;
                _pDetails.Address = result.PermanentAddress;

                _pDetails.constituencyCode = assesmblyInfo.ConstituencyCode;
                _pDetails.ConstituencyName = cons;
                if (bankDtails != null)
                {
                    _pDetails.bankAccountNumber = bankDtails.AccountNo.HasValue ? bankDtails.AccountNo : 0;
                    _pDetails.BankName = bankDtails.BankName;
                    _pDetails.IFSCCode = bankDtails.IFSCCode;
                    _pDetails.nomineeBankAccountNumber = bankDtails.NomineeAccountNo.HasValue ? bankDtails.NomineeAccountNo : 0;
                    _pDetails.nomineeBankName = bankDtails.BankName;
                    _pDetails.nomineeName = bankDtails.NomineeName;
                }

                _pDetails.PPOCode = result.PPONO;
                mMemberAssembly frstAsmbly = assemblyinfo.Take(1).FirstOrDefault();
                if (frstAsmbly != null)
                {
                    _pDetails.firstTermStart = Convert.ToDateTime(frstAsmbly.MemberstartDate);
                    _pDetails.firstTermEnd = Convert.ToDateTime(frstAsmbly.MemberEndDate);

                }




                List<pensionerTenuresDetails> ptlist = new List<pensionerTenuresDetails>();

                foreach (var titem in assemblyinfo.ToList())
                {
                    ptlist.Add(new pensionerTenuresDetails() { startDate = Convert.ToDateTime(titem.MemberstartDate), endDate = Convert.ToDateTime(titem.MemberEndDate) });

                }
                _pDetails._pensionerTenuresDetails = ptlist;
                _pDetails.photoCode = memberCode;
                _pDetails.description = result.Description;
                return _pDetails;


            }
        }

        #region save pensioner details

        public static object savePensioner(object param)
        {
            PensionerDetails _pensionerDetails = param as PensionerDetails;

            using (pensionContext context = new pensionContext())
            {
                try
                {
                    context.pensionerDetails.Add(_pensionerDetails);
                    context.SaveChanges();
                    context.Close();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return null;
            }
        }

        public static object savepensionerTenuresDetails(object param)
        {
            pensionerTenuresDetails _pensionerTenuresDetails = param as pensionerTenuresDetails;
            using (pensionContext context = new pensionContext())
            {
                try
                {
                    context.pensionerTenuresDetails.Add(_pensionerTenuresDetails);
                    context.SaveChanges();
                    context.Close();
                }
                catch (Exception ex)
                {
                    throw;
                }
                return null;
            }
        }

        #endregion save pensioner details

        #region pension report
        public static mMemberAssembly getLastAssembly(int? memberID)
        {
            using (pensionContext context = new pensionContext())
            {
                var assemblyID = (from assembly in context.mMemberAssembly
                                  join mAssemblyOrder in context.mAssembly
                                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                  where assembly.MemberID == memberID
                                  orderby mAssemblyOrder.AssemblyID descending
                                  select assembly.AssemblyID
                            ).FirstOrDefault();
                var memberAssemblies = (from memberAssembly in context.mMemberAssembly
                                        where memberAssembly.AssemblyID == assemblyID
                                        && memberAssembly.MemberID == memberID
                                        select memberAssembly
                                        ).FirstOrDefault();
                return memberAssemblies;
            }
        }
        public static object pensionReport()
        {
            using (pensionContext context = new pensionContext())
            {
                List<mMember> mlist = (List<mMember>)getXMemberList();
                List<PensionReport> plist = new List<PensionReport>();
                foreach (var item in mlist)
                {
                    var result = (from list in context.mMember
                                  join mma in context.mMemberAccountsDetails
                                  on list.MemberCode equals mma.MemberCode
                                  into gj
                                  from subpet in gj.DefaultIfEmpty()
                                  where list.MemberCode == item.MemberCode
                                  select new
                                  {
                                      memberinfo = list,
                                      accountInfo = subpet
                                  }
                                    ).FirstOrDefault();

                    var assemblyinfo = (from assembly in context.mMemberAssembly
                                        join mAssemblyOrder in context.mAssembly
                                        on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                        where assembly.MemberID == item.MemberCode
                                        orderby mAssemblyOrder.AssemblyID ascending
                                        select assembly
                               );

                    PensionReport pdtls = new PensionReport();
                    if (result.accountInfo != null)
                    {
                        pdtls.bankName = result.accountInfo.nameOfTreasury + " - " + result.accountInfo.BankName;
                        pdtls.PPONO = result.accountInfo.PPONum;

                    }
                    if (result.memberinfo != null)
                    {
                        pdtls.address = result.memberinfo.PermanentAddress;
                        pdtls.pensionerName = result.memberinfo.Name;
                    }
                    mMemberAssembly frstAsmbly = assemblyinfo.Take(1).FirstOrDefault();
                    if (frstAsmbly != null)
                    {
                        pdtls.firstDate = Convert.ToDateTime(frstAsmbly.MemberstartDate);
                        pdtls.lastDate = Convert.ToDateTime(frstAsmbly.MemberEndDate);

                    }
                    pdtls.pensionerID = item.MemberCode;


                    List<pensionerTenuresDetails> ptlist = new List<pensionerTenuresDetails>();

                    foreach (var titem in assemblyinfo.ToList())
                    {
                        ptlist.Add(new pensionerTenuresDetails() { startDate = Convert.ToDateTime(titem.MemberstartDate), endDate = Convert.ToDateTime(titem.MemberEndDate) });

                    }
                    pdtls._pensionerTenuresDetails = ptlist;
                    pdtls.pensionAmount = "22000";


                    //var result = (from list in context.pensionerDetails
                    //              join member in context.mMember
                    //              on list.memberCode equals member.MemberCode
                    //              select new PensionReport
                    //              {
                    //                  address = member.PermanentAddress,
                    //                  bankName = list.bankName,
                    //                  extraYear = list.extrayearfT,
                    //                  firstDate = list.firstTermStrat,
                    //                  lastDate = list.firstTermEnd,
                    //                  pensionAmount = "22000",
                    //                  pensionerName = list.pensionerName,
                    //                  PPONO = list.pPONO,
                    //                  pensionerID = list.memberCode
                    //              }
                    //                ).ToList();
                    //foreach (var item in result)
                    //{
                    //    item._pensionerTenuresDetails = (from ptd in context.pensionerTenuresDetails
                    //                                     where ptd.pensionerID == item.pensionerID
                    //                                     select ptd).ToList();
                    //}
                    plist.Add(pdtls);
                }
                return plist;
            }
        }

        //public static object getNomineeList()
        //{
        //    using (pensionContext context = new pensionContext())
        //    {
        //        var allNomineeActiveList = (from mmn in context.mMemberNominee
        //                                    join member in context.mMember
        //                                    on mmn.MemberCode equals member.MemberCode
        //                                    where mmn.Isactive == true && ((mmn.NomineeRelationship == "Spouse") || ((mmn.NomineeRelationship == "Daughter") && (mmn.NomineeMaritalStatus == false)) || ((mmn.NomineeRelationship == "Son") && (mmn.IsDependent == true)))
        //                                    select new
        //                                    {
        //                                        nominee = mmn,
        //                                        member = member
        //                                    }
        //                                      ).ToList();
        //    }
        //}
        public static object FamilypensionReport()
        {
            using (pensionContext context = new pensionContext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);

                var cMemberCodeList = (from Assembly in context.mMemberAssembly
                                       where Assembly.AssemblyID == aid
                                       select Assembly.MemberID).ToList();

                var allNomineeActiveList = (from mmn in context.mMemberNominee
                                            join member in context.mMember
                                            on mmn.MemberCode equals member.MemberCode
                                            where mmn.Isactive == true && !(cMemberCodeList.Contains(member.MemberCode)) && ((mmn.NomineeRelationship == "Spouse") || ((mmn.NomineeRelationship == "Daughter") && (mmn.NomineeMaritalStatus == false)) || ((mmn.NomineeRelationship == "Son") && (mmn.IsDependent == true)))
                                            select new
                                            {
                                                nominee = mmn,
                                                member = member
                                            }
                                              ).ToList();


                List<PensionReport> plist = new List<PensionReport>();
                foreach (var item in allNomineeActiveList)
                {
                    var result = (from list in context.mMember
                                  join mma in context.mMemberAccountsDetails
                                  on list.MemberCode equals mma.MemberCode
                                  into gj
                                  from subpet in gj.DefaultIfEmpty()
                                  where list.MemberCode == item.member.MemberCode
                                  select new
                                  {
                                      memberinfo = list,
                                      accountInfo = subpet
                                  }
                                    ).FirstOrDefault();

                    var assemblyinfo = (from assembly in context.mMemberAssembly
                                        join mAssemblyOrder in context.mAssembly
                                        on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                        where assembly.MemberID == item.member.MemberCode
                                        orderby mAssemblyOrder.AssemblyID ascending
                                        select assembly
                               );

                    PensionReport pdtls = new PensionReport();
                    if (result.accountInfo != null)
                    {
                        pdtls.bankName = result.accountInfo.nameOfTreasury + " - " + result.accountInfo.BankName;
                        pdtls.PPONO = result.accountInfo.PPONum;

                    }
                    if (result.memberinfo != null)
                    {
                        string midName = "";
                        if (item.nominee.NomineeRelationship == "Spouse")
                        {
                            if (item.member.Sex == "MALE")
                            {
                                midName = "W/O";
                            }
                            else
                            {
                                midName = "H/O";
                            }
                        }

                        if (item.nominee.NomineeRelationship == "Daughter")
                        {
                            midName = "D/O";

                        }
                        if (item.nominee.NomineeRelationship == "Daughter")
                        {
                            midName = "S/O";

                        }
                        pdtls.address = result.memberinfo.PermanentAddress;
                        pdtls.pensionerName = item.nominee.NomineeName + " " + midName + " " + item.member.Prefix + " " + item.member.Name;
                    }
                    mMemberAssembly frstAsmbly = assemblyinfo.Take(1).FirstOrDefault();
                    if (frstAsmbly != null)
                    {
                        pdtls.firstDate = Convert.ToDateTime(frstAsmbly.MemberstartDate);
                        pdtls.lastDate = Convert.ToDateTime(frstAsmbly.MemberEndDate);

                    }
                    pdtls.pensionerID = item.member.MemberCode;


                    List<pensionerTenuresDetails> ptlist = new List<pensionerTenuresDetails>();

                    foreach (var titem in assemblyinfo.ToList())
                    {
                        ptlist.Add(new pensionerTenuresDetails() { startDate = Convert.ToDateTime(titem.MemberstartDate), endDate = Convert.ToDateTime(titem.MemberEndDate) });

                    }
                    pdtls._pensionerTenuresDetails = ptlist;
                    pdtls.pensionAmount = "11000";


                    //var result = (from list in context.pensionerDetails
                    //              join member in context.mMember
                    //              on list.memberCode equals member.MemberCode
                    //              select new PensionReport
                    //              {
                    //                  address = member.PermanentAddress,
                    //                  bankName = list.bankName,
                    //                  extraYear = list.extrayearfT,
                    //                  firstDate = list.firstTermStrat,
                    //                  lastDate = list.firstTermEnd,
                    //                  pensionAmount = "22000",
                    //                  pensionerName = list.pensionerName,
                    //                  PPONO = list.pPONO,
                    //                  pensionerID = list.memberCode
                    //              }
                    //                ).ToList();
                    //foreach (var item in result)
                    //{
                    //    item._pensionerTenuresDetails = (from ptd in context.pensionerTenuresDetails
                    //                                     where ptd.pensionerID == item.pensionerID
                    //                                     select ptd).ToList();
                    //}
                    plist.Add(pdtls);
                }
                return plist;
            }
        }
        #endregion pension report
    }
}