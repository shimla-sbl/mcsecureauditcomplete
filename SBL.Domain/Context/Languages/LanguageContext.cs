﻿
namespace SBL.Domain.Context.Languages
{
    #region Namespaces
    using SBL.DAL;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SBL.DomainModel.Models.Language;
    using System.Data;
    #endregion
    public  class LanguageContext :DBBase<LanguageContext>
    {
       public LanguageContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
       public virtual DbSet<Languages> Languages { get; set; }

       public static object Excute(SBL.Service.Common.ServiceParameter param)
       {
           if (null == param)
           {
               return null;
           }
           switch(param.Method)
           {
               case "GetAllLanguages": { return GetAllLanguages(); }
               case "CreateLanguage": { CreateLanguage(param.Parameter); break; }
               case "UpdateLanguage": { return UpdateLanguage(param.Parameter); }
               case "GetLanguageBasedOnId": { return GetLanguageBasedOnId(param.Parameter); }
               case "DeleteLanguage": { return DeleteLanguage(param.Parameter); }
               case "GetAllActiveLanguages": { return GetAllActiveLanguages(); }
           }
           return null;
       }

       static Languages GetAllActiveLanguages()
       {
           try
           {
               using(LanguageContext db= new LanguageContext())
               {
                   var data = (from l in db.Languages
                               where l.Defaultlang == true
                               select l).ToList();
                   //return data;
                  // var data = db.Languages.ToList();
                   if (data.Count > 0)
                   {
                       var query = data[0];
                       return query;
                   }
                   else
                   {
                       return null;
                   }
               }
           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }

       static List<Languages> GetAllLanguages()
       {
           try
           {
               using(LanguageContext db= new LanguageContext())
               {
                   var data = db.Languages.Where(g=>g.IsDeleted==null).OrderByDescending(l => l.LanguageId).ToList();
                   return data;
               }

           }
           catch (Exception ex)
           {
               
               throw ex;
           }

       }

       static void CreateLanguage(object param)
       {
           try
           {
               using(LanguageContext db= new LanguageContext())
               {
                   Languages model = param as Languages;
                   model.CreatedDate = DateTime.Now;
                   model.ModifiedDate = DateTime.Now;
                   db.Languages.Add(model);
                   db.SaveChanges();
                   db.Close();
               }
           }
           catch (Exception ex)
           {
               
               throw ex;
           }
           
       }

       static object UpdateLanguage(object param)
       {
           try
           {
               using(LanguageContext db= new LanguageContext())
               {
                   Languages model = param as Languages;
                   model.CreatedDate = DateTime.Now;
                   model.ModifiedDate = DateTime.Now;
                   db.Languages.Attach(model);
                   db.Entry(model).State = EntityState.Modified;
                   db.SaveChanges();
                   db.Close();
               }
               return GetAllLanguages();

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }

       static object GetLanguageBasedOnId(object param)
       {
           try
           {
               using(LanguageContext db = new LanguageContext())
               {
                   Languages model = param as Languages;
                   var data = db.Languages.SingleOrDefault(a => a.LanguageId == model.LanguageId);
                   return data;
               }

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }

       static object DeleteLanguage(object param)
       {
           try
           {
               using(LanguageContext db= new LanguageContext())
               {
                   Languages model = param as Languages;
                   Languages langRemove = db.Languages.SingleOrDefault(e => e.LanguageId == model.LanguageId);
                   if(langRemove!=null)
                   {
                       langRemove.IsDeleted = true;
                   }
                   //db.Languages.Remove(langRemove);
                   db.SaveChanges();
                   db.Close();

               }
               return GetAllLanguages();

           }
           catch (Exception ex)
           {
               
               throw ex;
           }
       }

    }
}
