﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.States;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.MemberAccountDetails
{
    internal class mMemberAccountDetailsContext : DBBase<mMemberAccountDetailsContext>
    {
        public mMemberAccountDetailsContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<mMemberAccountsDetails> mMemberAccountsDetails { get; set; }
        public virtual DbSet<mMemberNominee> mMemberNominee { get; set; }
        public virtual DbSet<mMember> mMembers { get; set; }
        public virtual DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public virtual DbSet<SiteSettings> SiteSettings { get; set; }
        public virtual DbSet<mConstituency> mConstituency { get; set; }
        public virtual DbSet<DistrictModel> mDistricts { get; set; }
        public virtual DbSet<mStates> mStates { get; set; }
        public virtual DbSet<mMinistry> mMinistry { get; set; }
        public virtual DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }
        public virtual DbSet<mAssembly> mAssemblies { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "GetAllMembersDetails": { return GetAllMembersDetails(); }
                case "GetCurrentMembersDetails": { return GetCurrentMembersDetails(); }
                // case "GetMemberAccountDetailsById": { return GetMemberAccountDetailsById(param.Parameter); }
                case "SaveMembersAccountDetails": { return SaveMembersAccountDetails(param.Parameter); }
                case "GetExMembersDetails": { return GetExMembersDetails(); }

                case "GetNomineeList": { return GetNomineeList(param.Parameter); }
                case "GetAllSpeaker": { return GetAllSpeaker(); }
                case "GetAllDeputySpeaker": { return GetAllDeputySpeaker(); }
                case "getMemberListByDesignation": { return getMemberListByDesignation(param.Parameter); }
                case "getMemberList": { return getMemberList(); }
                case "getCurrentMemberList": { return getCurrentMemberList(param.Parameter); }
                case "getCurrentMemberListAll": { return getCurrentMemberListAll(param.Parameter); }
                case "getXMemberList": { return getXMemberList(param.Parameter); }
                case "getMemberListByID": { return getMemberListByID(param.Parameter); }
                case "GetMemberAccountDetailsById": { return GetMemberAccountDetailsById(param.Parameter); }
                case "saveNominee": { return saveNominee(param.Parameter); }
                case "updateNominee": { return updateNominee(param.Parameter); }
                case "deleteNominee": { return deleteNominee(param.Parameter); }
                case "getNomineeDetails": { return getNomineeDetails(param.Parameter); }
                case "GetChiefWhipDetails": { return GetChiefWhipDetails(); }
            }
            return null;
        }

        #region

        #region Method to Call the last Assembly of Member
        public static mMemberAssembly getLastAssembly(int? memberID)
        {
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var assemblyID = (from assembly in context.mMemberAssembly
                                  join mAssemblyOrder in context.mAssemblies
                                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                  where assembly.MemberID == memberID
                                  orderby mAssemblyOrder.AssemblyID descending
                                  select assembly.AssemblyID
                            ).FirstOrDefault();

                var memberAssemblies = (from memberAssembly in context.mMemberAssembly
                                        where memberAssembly.AssemblyID == assemblyID
                                        && memberAssembly.MemberID == memberID
                                        select memberAssembly
                                        ).FirstOrDefault();
                return memberAssemblies;
            }
        }
        #endregion

        #region Get All Speaker
        public static object GetAllSpeaker()
        {
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var q = (from assemblyid in context.SiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                
                int aid = Convert.ToInt32(q);
                List<int?> list = new List<int?>() { 1 };
                var query = (from member in context.mMembers
                             join massembly in context.mMemberAssembly
                                 on member.MemberCode equals massembly.MemberID
                             where list.Contains(massembly.DesignationID) && massembly.AssemblyID == aid
                             orderby massembly.DesignationID
                             select member);
                return query.ToList().OrderBy(m => m.Name).ToList();
            }
        }
        #endregion
        #region Get All Deputy Speaker
        public static object GetAllDeputySpeaker()
        {
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var q = (from assemblyid in context.SiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);
                List<int?> list = new List<int?>() { 14 };
                var query = (from member in context.mMembers
                             join massembly in context.mMemberAssembly
                                 on member.MemberCode equals massembly.MemberID
                             where list.Contains(massembly.DesignationID) && massembly.AssemblyID == aid
                             orderby massembly.DesignationID
                             select member);
                return query.ToList().OrderBy(m => m.Name).ToList();
            }
        }
        #endregion
        //get all members records of all
        #region All Members Account details
        private static object GetAllMembersDetails()
        {
            try
            {
                using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
                {
                    //To get the Current settings from site settings
                    var q = (from assemblyid in db.SiteSettings
                             where assemblyid.SettingName == "Assembly"
                             select assemblyid.SettingValue).FirstOrDefault();
                    int aid = Convert.ToInt32(q);

                    // To fetch All Member's list
                    var data = (from member in db.mMembers
                                select member
                                       ).ToList();

                    List<mMemberAccountsDetails> result = new List<mMemberAccountsDetails>();
                    foreach (var x in data)
                    {
                        mMemberAccountsDetails objAccountDetails = new mMemberAccountsDetails();
                        mMemberAssembly m = getLastAssembly(x.MemberCode);
                        mMemberAccountsDetails mDetail = (from mmemberAccountDetails in db.mMemberAccountsDetails
                                                          where mmemberAccountDetails.MemberCode == x.MemberCode
                                                          select mmemberAccountDetails).FirstOrDefault();
                        if (mDetail != null)
                        {
                            objAccountDetails.BankName = mDetail.BankName;
                            objAccountDetails.AccountNo = mDetail.AccountNo;
                            objAccountDetails.NomineeName = mDetail.NomineeName;
                        }
                        objAccountDetails.MemberCode = x.MemberCode;
                        objAccountDetails.MemberName = x.Name;
                        objAccountDetails.PhotoCode = x.MemberCode;

                        objAccountDetails.ConstituencyName = (from cons in db.mConstituency
                                                              where cons.ConstituencyCode == m.ConstituencyCode
                                                              select cons.ConstituencyName).FirstOrDefault();
                        result.Add(objAccountDetails);
                    }

                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion All Members Account details

        #region Get member Detail by MemberCode
        private static object GetCurrentMemberDetailByMemberCode(object param)
        {
            try
            {
                using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
                {
                    mMember parameter = param as mMember;
                    var query = db.mMembers.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
                    return query;

                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Current members records

        //Getting all current members records
        #region Current members records
        private static object GetCurrentMembersDetails()
        {
            try
            {
                using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
                {
                    //to get the current assembly value
                    var objcurrentAssembly = (from f in db.SiteSettings where (f.SettingName == "Assembly") select new { f.SettingValue }).FirstOrDefault();
                    var intValue = Convert.ToInt16(objcurrentAssembly.SettingValue);

                    var data = (from m in db.mMembers
                                join n in db.mMemberAssembly on m.MemberCode equals n.MemberID
                                join r in db.mConstituency on n.ConstituencyCode equals r.ConstituencyCode
                                where (m.MemberCode == n.MemberID) && (n.AssemblyID == intValue) && (r.AssemblyID == intValue) && (m.Active == true)
                                join a in db.mMemberAccountsDetails on m.MemberCode equals a.MemberCode into t
                                from z in t.DefaultIfEmpty()
                                orderby m.MemberCode
                                select new
                                {
                                    m.Name,
                                    m.MemberCode,
                                    z.BankName,
                                    z.AccountNo,
                                    z.NomineeName,
                                    r.ConstituencyName
                                }).ToList();

                    List<mMemberAccountsDetails> result = new List<mMemberAccountsDetails>();

                    foreach (var x in data)
                    {
                        mMemberAccountsDetails objAccountDetails = new mMemberAccountsDetails();

                        objAccountDetails.MemberCode = x.MemberCode;
                        objAccountDetails.MemberName = x.Name;
                        objAccountDetails.PhotoCode = x.MemberCode;
                        objAccountDetails.BankName = x.BankName;
                        objAccountDetails.AccountNo = x.AccountNo;
                        objAccountDetails.NomineeName = x.NomineeName;
                        objAccountDetails.ConstituencyName = x.ConstituencyName;
                        result.Add(objAccountDetails);
                    }

                    return result.OrderBy(m => m.MemberName).ToList(); ;
                }
            }
            catch
            {
                throw;
            }
        }
        private static object GetChiefWhipDetails()
        {
            try
            {
                using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
                {
                    //to get the current assembly value
                    var objcurrentAssembly = (from f in db.SiteSettings where (f.SettingName == "Assembly") select new { f.SettingValue }).FirstOrDefault();
                    var intValue = Convert.ToInt16(objcurrentAssembly.SettingValue);

                    var data = (from m in db.mMembers
                                join n in db.mMemberAssembly on m.MemberCode equals n.MemberID
                                join r in db.mConstituency on n.ConstituencyCode equals r.ConstituencyCode
                                where (m.MemberCode == n.MemberID) && (n.AssemblyID == intValue) 
                                && (r.AssemblyID == intValue) && (m.Active == true)
                                && (n.DesignationID==58)
                                join a in db.mMemberAccountsDetails on m.MemberCode equals a.MemberCode into t
                                from z in t.DefaultIfEmpty()
                                orderby m.MemberCode
                                select new
                                {
                                    m.Name,
                                    m.MemberCode,
                                    z.BankName,
                                    z.AccountNo,
                                    z.NomineeName,
                                    r.ConstituencyName
                                }).ToList();

                    List<mMemberAccountsDetails> result = new List<mMemberAccountsDetails>();

                    foreach (var x in data)
                    {
                        mMemberAccountsDetails objAccountDetails = new mMemberAccountsDetails();

                        objAccountDetails.MemberCode = x.MemberCode;
                        objAccountDetails.MemberName = x.Name;
                        objAccountDetails.PhotoCode = x.MemberCode;
                        objAccountDetails.BankName = x.BankName;
                        objAccountDetails.AccountNo = x.AccountNo;
                        objAccountDetails.NomineeName = x.NomineeName;
                        objAccountDetails.ConstituencyName = x.ConstituencyName;
                        result.Add(objAccountDetails);
                    }

                    return result.OrderBy(m => m.MemberName).ToList(); ;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion Current members records

        //For Fetching ex members records
        #region Ex Members records

        public static object GetExMembersDetails()
        {
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var q = (from assemblyid in context.SiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);

                var cMemberCodeList = (from Assembly in context.mMemberAssembly
                                       where Assembly.AssemblyID == aid
                                       select Assembly.MemberID).ToList();

                var result = (from member in context.mMembers
                              where !(cMemberCodeList.Contains(member.MemberCode))
                              select member
                                   ).Distinct();
                return result.ToList();
            }
        }

        public static object GetNomineeList(object param)
        {
            mMemberNominee obj = param as mMemberNominee;
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {


                var result = (from NOM in context.mMemberNominee
                              where NOM.MemberCode == obj.MemberCode && NOM.Isactive == true
                              select NOM
                                   ).Distinct();
                return result.ToList();
            }
        }

        //private static object GetExMembersDetails()
        //{
        //    try
        //    {
        //        using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
        //        {
        //            //TO get Current site Settings
        //            var q = (from assemblyid in db.SiteSettings
        //                     where assemblyid.SettingName == "Assembly"
        //                     select assemblyid.SettingValue).FirstOrDefault();
        //            int aid = Convert.ToInt32(q);

        //            //To  fetch current member list
        //            var cMemberCodeList = (from Assembly in db.mMemberAssembly
        //                                   where Assembly.AssemblyID == aid
        //                                   select Assembly.MemberID).ToList();

        //            //To get ex-members list by simply putting not condition
        //            var data = (from member in db.mMembers
        //                        where !(cMemberCodeList.Contains(member.MemberCode))
        //                        select member
        //                               ).ToList();



        //            List<mMemberAccountsDetails> result = new List<mMemberAccountsDetails>();
        //            foreach (var x in data)
        //            {
        //                mMemberAccountsDetails objAccountDetails = new mMemberAccountsDetails();
        //                mMemberAssembly m = getLastAssembly(x.MemberCode);
        //                mMemberAccountsDetails mDetail = (from mmemberAccountDetails in db.mMemberAccountsDetails
        //                                                  where mmemberAccountDetails.MemberCode == x.MemberCode
        //                                                  select mmemberAccountDetails).FirstOrDefault();
        //                if (mDetail != null)
        //                {
        //                    objAccountDetails.BankName = mDetail.BankName;
        //                    objAccountDetails.AccountNo = mDetail.AccountNo;
        //                    objAccountDetails.NomineeName = mDetail.NomineeName;
        //                    objAccountDetails.NomineeAccountNo = mDetail.NomineeAccountNo;
        //                    objAccountDetails.NomineeRelationShip = mDetail.NomineeRelationShip;
        //                    objAccountDetails.NomineeBankName = mDetail.NomineeBankName;
        //                    objAccountDetails.NomineeAccountNo = mDetail.NomineeAccountNo;
        //                    objAccountDetails.NomineeActive = mDetail.NomineeActive;
        //                    objAccountDetails.NomineeIFSCCode = mDetail.NomineeIFSCCode;
        //                }
        //                objAccountDetails.MemberCode = x.MemberCode;
        //                objAccountDetails.MemberName = x.Name;
        //                objAccountDetails.PhotoCode = x.MemberCode;

        //                // objAccountDetails.ConstituencyName = (from cons in db.mConstituency
        //                // where cons.ConstituencyCode == m.ConstituencyCode
        //                // select cons.ConstituencyName).FirstOrDefault();
        //                result.Add(objAccountDetails);
        //            }

        //            return result.OrderBy(m => m.MemberName).ToList();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}
        #endregion Ex Members records

        //Getting Records of single member
        #region Get Member Account Details By Id
        //private static object GetMemberAccountDetailsById(object param)
        //{
        //    using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
        //    {
        //        mMemberAccountsDetails member = param as mMemberAccountsDetails;
        //        int memberCode = Convert.ToInt32(member.MemberCode);
        //        mMemberAccountsDetails _mDetails = new mMemberAccountsDetails();
        //        var mMember = (from _list in db.mMembers
        //                       where _list.MemberCode == memberCode
        //                       select _list).FirstOrDefault();

        //        mMemberAssembly mAssemb = getLastAssembly(memberCode);

        //        var accountDetails = (from _accountList in db.mMemberAccountsDetails
        //                              where _accountList.MemberCode == memberCode
        //                              select _accountList).FirstOrDefault();
        //        if (mMember != null)
        //        {
        //            _mDetails.MemberCode = mMember.MemberCode;
        //            _mDetails.MemberDetail = mMember.Description;
        //            _mDetails.MemberName = mMember.Name;
        //            _mDetails.MobileNumber = mMember.Mobile;
        //            _mDetails.PhotoCode = mMember.MemberCode;
        //            _mDetails.MemberAddress = mMember.PermanentAddress;
        //            _mDetails.constituencyCode = mAssemb.ConstituencyCode;
        //            _mDetails.ConstituencyName = (from cons in db.mConstituency
        //                                          where cons.ConstituencyCode == mAssemb.ConstituencyCode
        //                                          select cons.ConstituencyName).FirstOrDefault();
        //            _mDetails.MemberAddress = mMember.ShimlaAddress;
        //        }
        //        if (accountDetails != null)
        //        {
        //            _mDetails.AccountNo = accountDetails.AccountNo;
        //            _mDetails.BankName = accountDetails.BankName;

        //            _mDetails.EPABXNo = accountDetails.EPABXNo;
        //            _mDetails.HBAAccountNo = accountDetails.HBAAccountNo;
        //            _mDetails.IFSCCode = accountDetails.IFSCCode;
        //            _mDetails.MCAAccountNo = accountDetails.MCAAccountNo;

        //            _mDetails.NomineeAccountNo = accountDetails.NomineeAccountNo;
        //            _mDetails.NomineeActive = accountDetails.NomineeActive;
        //            _mDetails.NomineeBankName = accountDetails.NomineeBankName;
        //            _mDetails.NomineeIFSCCode = accountDetails.NomineeIFSCCode;
        //            _mDetails.NomineeName = accountDetails.NomineeName;
        //            _mDetails.NomineeRelationShip = accountDetails.NomineeRelationShip;
        //        }
        //        return _mDetails;
        //    }
        //}
        #endregion Get Member Account Details By Id

        //adding the data or editing the data if already exist
        #region Save  Members Account Details
        private static object SaveMembersAccountDetails(object param)
        {
            try
            {
                mMemberAccountsDetails MemberToEdit = param as mMemberAccountsDetails;
                using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
                {
                    var content = (from a in db.mMemberAccountsDetails
                                   where a.MemberCode == MemberToEdit.MemberCode
                                   select a).FirstOrDefault();

                    //adding if doesn't exit
                    if (content == null)
                    {
                        db.mMemberAccountsDetails.Add(MemberToEdit);
                        db.SaveChanges();
                        db.Close();
                    }
                    //editing if already exist
                    else
                    {
                        content.MemberCode = MemberToEdit.MemberCode;
                        content.AccountNo = MemberToEdit.AccountNo;
                        content.AccountNoS = MemberToEdit.AccountNoS;
                        content.PanNumber = MemberToEdit.PanNumber;
                        content.BankName = MemberToEdit.BankName;
                        content.EPABXNo = MemberToEdit.EPABXNo;
                        content.HBAAccountNo = MemberToEdit.HBAAccountNo;
                        content.IFSCCode = MemberToEdit.IFSCCode;
                        content.MCAAccountNo = MemberToEdit.MCAAccountNo;
                        content.NomineeAccountNo = MemberToEdit.NomineeAccountNo;
                        content.NomineeBankName = MemberToEdit.NomineeBankName;
                        content.NomineeName = MemberToEdit.NomineeName;
                        content.PhotoCode = MemberToEdit.PhotoCode;
                        content.NomineeActive = MemberToEdit.NomineeActive;
                        content.NomineeRelationShip = MemberToEdit.NomineeRelationShip;
                        content.NomineeIFSCCode = MemberToEdit.NomineeIFSCCode;
                        content.PPONum = MemberToEdit.PPONum;
                        content.MemberPayeeCode = MemberToEdit.MemberPayeeCode;
                        db.mMemberAccountsDetails.Attach(content);
                        db.Entry(content).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                return MemberToEdit;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion Save  Members Account Details
        #endregion
        #region durgesh code
        #region search result
        //mMemberAccountDetailsContext
        public static object getMemberListByDesignation(object param)
        {
            int designatioID = int.Parse(param.ToString());
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var result = (from list in context.mMembers
                              join designation in context.mMemberAssembly
                              on list.MemberCode equals designation.MemberID
                              where designation.DesignationID == designatioID
                              select new memberAccountSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,
                                  designation = designation.Designation,
                                  Address = list.PermanentAddress,
                                  constituencyCode = designation.ConstituencyCode,
                                  constituencyName = (from con in context.mConstituency
                                                      where con.ConstituencyCode == designation.ConstituencyCode
                                                      select con.ConstituencyName).FirstOrDefault(),
                                  contactNumber = list.Mobile,


                              }

                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    var accountDetails = (from _accountList in context.mMemberAccountsDetails
                                          where _accountList.MemberCode == item.memberCode
                                          select _accountList).FirstOrDefault();
                    if (accountDetails != null)
                    {
                        item.BankDetails = accountDetails.AccountNo + " - " + accountDetails.BankName;
                    }

                }


                return result;
            }
        }
        public static object getCurrentMemberList(object param)
        {
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var q = (from assemblyid in context.SiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);
                var result = (from list in context.mMembers
                              join designation in context.mMemberAssembly
                              on list.MemberCode equals designation.MemberID
                              where designation.AssemblyID == aid

                              select new memberAccountSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,
                                  designation = designation.Designation,
                                  Address = list.PermanentAddress,
                                  constituencyCode = designation.ConstituencyCode,
                                  constituencyName = (from con in context.mConstituency
                                                      where con.ConstituencyCode == designation.ConstituencyCode
                                                      select con.ConstituencyName).FirstOrDefault(),
                                  contactNumber = list.Mobile,


                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    var accountDetails = (from _accountList in context.mMemberAccountsDetails
                                          where _accountList.MemberCode == item.memberCode
                                          select _accountList).FirstOrDefault();
                    if (accountDetails != null)
                    {
                        item.BankDetails = accountDetails.AccountNoS + " - " + accountDetails.BankName;
                        item.BankName = accountDetails.BankName;
                        item.AccountNumber = (accountDetails.AccountNo != null ? Convert.ToInt64(accountDetails.AccountNo) : 0);
                        item.IFSCCode = accountDetails.IFSCCode;
                    }

                }

                return result;
            }
        }

        public static object getCurrentMemberListAll(object param)
        {
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                //var q = (from assemblyid in context.SiteSettings
                //         where assemblyid.SettingName == "Assembly"
                //         select assemblyid.SettingValue).FirstOrDefault();
                //int aid = Convert.ToInt32(q);
                var result = (from list in context.mMembers
                              join designation in context.mMemberAssembly
                              on list.MemberCode equals designation.MemberID
                              //where designation.AssemblyID == aid

                              select new memberAccountSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,
                                  designation = designation.Designation,
                                  Address = list.PermanentAddress,
                                  constituencyCode = designation.ConstituencyCode,
                                  constituencyName = (from con in context.mConstituency
                                                      where con.ConstituencyCode == designation.ConstituencyCode
                                                      select con.ConstituencyName).FirstOrDefault(),
                                  contactNumber = list.Mobile,


                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    var accountDetails = (from _accountList in context.mMemberAccountsDetails
                                          where _accountList.MemberCode == item.memberCode
                                          select _accountList).FirstOrDefault();
                    if (accountDetails != null)
                    {
                        item.BankDetails = accountDetails.AccountNoS + " - " + accountDetails.BankName;
                        item.BankName = accountDetails.BankName;
                        item.AccountNumber = (accountDetails.AccountNo != null ? Convert.ToInt64(accountDetails.AccountNo) : 0);
                        item.IFSCCode = accountDetails.IFSCCode;
                    }

                }

                return result;
            }
        }
        public static object getMemberList()
        {

            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var result = (from list in context.mMembers



                              select new memberAccountSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,


                                  Address = list.PermanentAddress,

                                  contactNumber = list.Mobile,


                              }

                                     ).Distinct().ToList();
                foreach (var item in result)
                {

                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    if (m != null)
                    {
                        item.designation = m.Designation;
                        item.constituencyCode = m.ConstituencyCode;
                        item.constituencyName = (from con in context.mConstituency
                                                 where con.ConstituencyCode == m.ConstituencyCode && con.AssemblyID==m.AssemblyID
                                                 select con.ConstituencyName).FirstOrDefault();
                    }
                    var accountDetails = (from _accountList in context.mMemberAccountsDetails
                                          where _accountList.MemberCode == item.memberCode
                                          select _accountList).FirstOrDefault();
                    if (accountDetails != null)
                    {
                        item.BankDetails = accountDetails.AccountNoS + " - " + accountDetails.BankName;
                        item.BankName = accountDetails.BankName;
                        item.AccountNumber = (accountDetails.AccountNo != null ? Convert.ToInt64(accountDetails.AccountNo) : 0);
                        item.IFSCCode = accountDetails.IFSCCode;
                    }

                }


                return result;
            }
        }
        public static object getXMemberList(object param)
        {
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var q = (from assemblyid in context.SiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);

                var cMemberCodeList = (from Assembly in context.mMemberAssembly
                                       where Assembly.AssemblyID == aid
                                       select Assembly.MemberID).ToList();

                var result = (from list in context.mMembers
                              where !(cMemberCodeList.Contains(list.MemberCode))

                              select new memberAccountSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,

                                  contactNumber = list.Mobile,

                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    if (m != null)
                    {
                        item.designation = m.Designation;
                        item.constituencyCode = m.ConstituencyCode;
                        item.constituencyName = (from con in context.mConstituency
                                                 where con.ConstituencyCode == m.ConstituencyCode
                                                 select con.ConstituencyName).FirstOrDefault();
                    }
                    var accountDetails = (from _accountList in context.mMemberAccountsDetails
                                          where _accountList.MemberCode == item.memberCode
                                          select _accountList).FirstOrDefault();
                    if (accountDetails != null)
                    {
                        item.BankDetails = accountDetails.AccountNoS + " - " + accountDetails.BankName;
                    }

                }
                return result;
            }
        }
        public static object getMemberListByID(object param)
        {
            int memID = Convert.ToInt32(param);
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                var result = (from list in context.mMembers

                              where list.MemberCode == memID

                              select new memberAccountSearchResult
                              {
                                  memberName = list.Name,
                                  memberCode = list.MemberCode,

                                  Address = list.PermanentAddress,



                              }
                                     ).Distinct().ToList();
                foreach (var item in result)
                {
                    mMemberAssembly m = getLastAssembly(item.memberCode);
                    if (m != null)
                    {
                        item.designation = m.Designation;
                        item.constituencyCode = m.ConstituencyCode;
                        item.constituencyName = (from con in context.mConstituency
                                                 where con.ConstituencyCode == m.ConstituencyCode
                                                 select con.ConstituencyName).FirstOrDefault();
                    }
                    var accountDetails = (from _accountList in context.mMemberAccountsDetails
                                          where _accountList.MemberCode == item.memberCode
                                          select _accountList).FirstOrDefault();
                    if (accountDetails != null)
                    {
                        item.BankDetails = accountDetails.AccountNoS + " - " + accountDetails.BankName;
                    }
                }
                return result;
            }
        }
        public static object GetMemberAccountDetailsById(object param)
        {
            using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
            {
                int memberCode = Convert.ToInt32(param);
                mMemberAccountsDetails _mDetails = new mMemberAccountsDetails();
                var mMember = (from _list in db.mMembers
                               where _list.MemberCode == memberCode
                               select _list).FirstOrDefault();

                mMemberAssembly mAssemb = getLastAssembly(memberCode);

                var accountDetails = (from _accountList in db.mMemberAccountsDetails
                                      where _accountList.MemberCode == memberCode
                                      select _accountList).FirstOrDefault();
                if (mMember != null)
                {
                    _mDetails.MemberCode = mMember.MemberCode;
                    _mDetails.MemberDetail = mMember.Description;
                    _mDetails.MemberName = mMember.Name;
                    _mDetails.MobileNumber = mMember.Mobile;
                    _mDetails.PhotoCode = mMember.MemberCode;
                    _mDetails.MemberAddress = mMember.PermanentAddress;
                    if (mAssemb != null)
                    {
                        _mDetails.designation = mAssemb.Designation;
                        _mDetails.constituencyCode = mAssemb.ConstituencyCode;
                        _mDetails.ConstituencyName = (from cons in db.mConstituency
                                                      where cons.ConstituencyCode == mAssemb.ConstituencyCode
                                                      select cons.ConstituencyName).FirstOrDefault();

                    }
                    _mDetails.MemberAddress = mMember.ShimlaAddress;
                }
                if (accountDetails != null)
                {
                    _mDetails.AccountNo = accountDetails.AccountNo;
                    _mDetails.AccountNoS = accountDetails.AccountNoS;
                    _mDetails.PanNumber = accountDetails.PanNumber;
                    _mDetails.BankName = accountDetails.BankName;
                    _mDetails.PPONum = accountDetails.PPONum;
                    _mDetails.EPABXNo = accountDetails.EPABXNo;
                    _mDetails.HBAAccountNo = accountDetails.HBAAccountNo;
                    _mDetails.IFSCCode = accountDetails.IFSCCode;
                    _mDetails.MCAAccountNo = accountDetails.MCAAccountNo;
                    _mDetails.MemberPayeeCode = accountDetails.MemberPayeeCode;

                }
                var nomineeList = (from list in db.mMemberNominee
                                   where list.MemberCode == memberCode
                                   select list).ToList();
                if (nomineeList != null)
                {
                    _mDetails.mMemberNominee = nomineeList;
                }

                return _mDetails;
            }
        }
        public static object saveNominee(object param)
        {
            mMemberNominee nominee = param as mMemberNominee;
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                if (nominee.Isactive)
                {
                    var result = (from list in context.mMemberNominee
                                  where list.MemberCode == nominee.MemberCode && list.NomineeRelationship != "Spouse"
                                  select list).ToList();
                    foreach (var item in result)
                    {
                        item.Isactive = false;
                        context.Entry(item).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                }
                context.mMemberNominee.Add(nominee);
                context.SaveChanges();
                context.Close();
                return null;
            }
        }
        public static object updateNominee(object param)
        {
            mMemberNominee nominee = param as mMemberNominee;
            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                if (nominee.Isactive)
                {
                    var result = (from list in context.mMemberNominee
                                  where list.MemberCode == nominee.MemberCode && list.NomineeRelationship != "Spouse"
                                  select list).ToList();
                    foreach (var item in result)
                    {
                        item.Isactive = false;
                        context.Entry(item).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                }
                var nominees = (from _list in context.mMemberNominee
                                where _list.NomineeId == nominee.NomineeId
                                select _list).FirstOrDefault();
                nominees.Isactive = nominee.Isactive;
                nominees.IsDependent = nominee.IsDependent;
                nominees.IsHandicaped = nominee.IsHandicaped;
                nominees.NomineeAccountNo = nominee.NomineeAccountNo;
                nominees.NomineeAccountNoS = nominee.NomineeAccountNoS;
                nominees.NomineeBankName = nominee.NomineeBankName;
                nominees.NomineeIFSCCode = nominee.NomineeIFSCCode;
                nominees.NomineeMaritalStatus = nominee.NomineeMaritalStatus;
                nominees.NomineeName = nominee.NomineeName;
                nominees.NomineeRelationship = nominee.NomineeRelationship;

                context.Entry(nominees).State = EntityState.Modified;
                context.SaveChanges();
                context.Close();
                return null;
            }
        }
        public static object deleteNominee(object param)
        {

            using (mMemberAccountDetailsContext context = new mMemberAccountDetailsContext())
            {
                int nomineeCode = Convert.ToInt32(param);

                var nominee = (from _list in context.mMemberNominee
                               where _list.NomineeId == nomineeCode
                               select _list).FirstOrDefault();


                context.Entry(nominee).State = EntityState.Deleted;
                context.SaveChanges();
                context.Close();

                return nominee.MemberCode;
            }
        }
        public static object getNomineeDetails(object param)
        {
            using (mMemberAccountDetailsContext db = new mMemberAccountDetailsContext())
            {
                int nomineeCode = Convert.ToInt32(param);

                var nominee = (from _list in db.mMemberNominee
                               where _list.NomineeId == nomineeCode
                               select _list).FirstOrDefault();
                return nominee;
            }
        }
        #endregion
        #endregion


    }
}