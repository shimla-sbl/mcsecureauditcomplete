﻿using SBL.DAL;
using SBL.DomainModel.Models.Library;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Library
{
    public class SaleCounterContext : DBBase<SaleCounterContext>
    {
        public SaleCounterContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mSaleCounter> mSaleCounter { get; set; }
        public DbSet<mDebateSaleCounter> mDebateSaleCounter { get; set; }
        public DbSet<mDebateInfo> mDebateInfo { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateSaleCounter":
                    {
                        return CreateSaleCounter(param.Parameter);
                    }
                case "UpdateSaleCounter":
                    {
                        return UpdateSaleCounter(param.Parameter);
                    }
                case "DeleteSaleCounter":
                    {
                        return DeleteSaleCounter(param.Parameter);
                    }
                case "GetAllSaleCounterDetails":
                    {
                        return GetAllSaleCounterDetails(param.Parameter);
                    }
                case "GetSaleCounterById":
                    {
                        return GetSaleCounterById(param.Parameter);
                    }
                case "SearchSaleCounter":
                    {
                        return SearchSaleCounter(param.Parameter);
                    }
                case "LocateSaleCounter":
                    {
                        return LocateSaleCounter(param.Parameter);
                    }

                case "CreateDebateSaleCounter":
                    {
                        return CreateDebateSaleCounter(param.Parameter);
                    }
                case "DeleteDebateSaleCounter":
                    {
                        return DeleteDebateSaleCounter(param.Parameter);
                    }
                case "UpdateDebateSaleCounter":
                    {
                        return UpdateDebateSaleCounter(param.Parameter);
                    }
                case "GetAllDebateSaleCounterDetails":
                    {
                        return GetAllDebateSaleCounterDetails(param.Parameter);
                    }
                case "GetDebateSaleCounterById":
                    {
                        return GetDebateSaleCounterById(param.Parameter);
                    }
                case "SearchDebateSaleCounter":
                    {
                        return SearchDebateSaleCounter(param.Parameter);
                    }
                case "LocateDebateSaleCounter":
                    {
                        return LocateDebateSaleCounter(param.Parameter);
                    }

                case "CreateDebateInfo":
                    {
                        return CreateDebateInfo(param.Parameter);
                    }
                case "UpdateDebateInfo":
                    {
                        return UpdateDebateInfo(param.Parameter);
                    }
                case "DeleteDebateInfo":
                    {
                        return DeleteDebateInfo(param.Parameter);
                    }
                case "GetAllDebateInfoDetails":
                    {
                        return GetAllDebateInfoDetails(param.Parameter);
                    }
                case "SearchDebateInfo":
                    {
                        return SearchDebateInfo(param.Parameter);
                    }
                case "LocateDebateInfo":
                    {
                        return LocateDebateInfo(param.Parameter);
                    }
                case "GetDebateInfoById":
                    {
                        return GetDebateInfoById(param.Parameter);
                    }
            }

            return null;
        }

        #region Sale Counter
        public static object CreateSaleCounter(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mSaleCounter model = param as mSaleCounter;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mSaleCounter.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateSaleCounter(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mSaleCounter model = param as mSaleCounter;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mSaleCounter.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteSaleCounter(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mSaleCounter parameter = param as mSaleCounter;
                    mSaleCounter stateToRemove = db.mSaleCounter.SingleOrDefault(a => a.SaleCounterID == parameter.SaleCounterID);
                    db.mSaleCounter.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllSaleCounterDetails(object param)
        {

            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from a in db.mSaleCounter
                                orderby a.SaleCounterID
                                select a).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object SearchSaleCounter(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string AuthorName = Result[0];
            string Title = Result[1];
            string Edition = Result[2];
            string Volume = Result[3];
            string PublishingYear = Result[4];
            string PriceFrom = Result[5];
            string PriceToS = Result[6];
            string Source = Result[7];
            string AvailabilityS = Result[8];

            decimal PriceToD = 0;
            decimal PriceFD = 0;
            int Availability = 0;
            decimal.TryParse(PriceToS, out PriceToD);
            decimal.TryParse(PriceFrom, out PriceFD);
            int.TryParse(AvailabilityS, out Availability);
            SaleCounterContext db = new SaleCounterContext();


            var SearchData = (from PC in db.mSaleCounter
                              orderby PC.CreationDate
                              where PC.AuthorName.Contains(AuthorName.Trim()) || PC.Title.Contains(Title.Trim()) || PC.Edition.Contains(Edition.Trim())
                              || PC.Volume.Contains(Volume.Trim()) || PC.PublishingYear.Contains(PublishingYear.Trim()) || PC.Source.Contains(Source.Trim())
                                || (PC.Price >= PriceFD && PC.Price <= PriceToD) || PC.Availability == Availability
                              select PC).ToList();


            return SearchData;
        }


        public static object LocateSaleCounter(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string AuthorName = Result[0];
            string Title = Result[1];
            string Edition = Result[2];
            string Volume = Result[3];
            string PublishingYear = Result[4];
            string PriceFrom = Result[5];
            string PriceToS = Result[6];
            string Source = Result[7];
            string AvailabilityS = Result[8];

            string Building = Result[9];
            string Floor = Result[10];
            string Almirah = Result[11];

            decimal PriceToD = 0;
            decimal PriceFD = 0;
            int Availability = 0;
            decimal.TryParse(PriceToS, out PriceToD);
            decimal.TryParse(PriceFrom, out PriceFD);
            int.TryParse(AvailabilityS, out Availability);
            SaleCounterContext db = new SaleCounterContext();

            var SearchData = (from PC in db.mSaleCounter
                              orderby PC.CreationDate
                              where PC.AuthorName.Contains(AuthorName.Trim()) || PC.Title.Contains(Title.Trim()) || PC.Edition.Contains(Edition.Trim())
                              || PC.Volume.Contains(Volume.Trim()) || PC.PublishingYear.Contains(PublishingYear.Trim()) || PC.Source.Contains(Source.Trim())
                                || (PC.Price >= PriceFD && PC.Price <= PriceToD) || PC.Availability == Availability
                                || PC.Building.Contains(Building.Trim()) || PC.Floor.Contains(Floor.Trim()) || PC.Almirah.Contains(Almirah.Trim())
                              select PC).ToList();


            return SearchData;
        }

        public static object GetSaleCounterById(object param)
        {
            mSaleCounter parameter = param as mSaleCounter;

            SaleCounterContext db = new SaleCounterContext();

            var query = db.mSaleCounter.SingleOrDefault(a => a.SaleCounterID == parameter.SaleCounterID);

            return query;
        }

        #endregion

        #region Debate for Sale Counter

        public static object CreateDebateSaleCounter(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mDebateSaleCounter model = param as mDebateSaleCounter;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mDebateSaleCounter.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateDebateSaleCounter(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mDebateSaleCounter model = param as mDebateSaleCounter;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mDebateSaleCounter.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteDebateSaleCounter(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mDebateSaleCounter parameter = param as mDebateSaleCounter;
                    mDebateSaleCounter stateToRemove = db.mDebateSaleCounter.SingleOrDefault(a => a.DebateSaleCounterID == parameter.DebateSaleCounterID);
                    db.mDebateSaleCounter.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllDebateSaleCounterDetails(object param)
        {

            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from a in db.mDebateSaleCounter
                                orderby a.DebateSaleCounterID
                                select a).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object SearchDebateSaleCounter(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string Khand = Result[0];
            string Ank = Result[1];
            string DateOfDebateFrom = Result[2];
            string DateOfDebateTo = Result[3];
            string HindiDate = Result[4];
            string HindiMonth = Result[5];
            string Pages = Result[6];
            string Assembly = Result[7];
            string Session = Result[8];

            string PriceFrom = Result[9];
            string PriceToS = Result[10];
            string AvailabilityS = Result[11];

            decimal PriceToD = 0;
            decimal PriceFD = 0;
            int Availability = 0;
            decimal.TryParse(PriceToS, out PriceToD);
            decimal.TryParse(PriceFrom, out PriceFD);
            int.TryParse(AvailabilityS, out Availability);

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(DateOfDebateFrom, out FromDate);
            DateTime.TryParse(DateOfDebateTo, out ToDate);

            SaleCounterContext db = new SaleCounterContext();


            var SearchData = (from PC in db.mDebateSaleCounter
                              orderby PC.DebateSaleCounterID
                              where PC.Khand.Contains(Khand.Trim()) || PC.Ank.Contains(Ank.Trim()) || PC.HindiDate.Contains(HindiDate.Trim())
                              || PC.HindiMonth.Contains(HindiMonth.Trim()) || PC.Pages.Contains(Pages.Trim()) || PC.Assembly.Contains(Assembly.Trim())
                                || PC.Session.Contains(Session.Trim()) || (PC.DateOfDebateE >= FromDate && PC.DateOfDebateE <= ToDate)
                                || (PC.Price >= PriceFD && PC.Price <= PriceToD) || PC.Availability == Availability
                              select PC).ToList();


            return SearchData;
        }


        public static object LocateDebateSaleCounter(object param)
        {


            string Data = param as string;
            string[] Result = Data.Split(',');
            string Khand = Result[0];
            string Ank = Result[1];
            string DateOfDebateFrom = Result[2];
            string DateOfDebateTo = Result[3];
            string HindiDate = Result[4];
            string HindiMonth = Result[5];
            string Pages = Result[6];
            string Assembly = Result[7];
            string Session = Result[8];

            string PriceFrom = Result[9];
            string PriceToS = Result[10];
            string AvailabilityS = Result[11];

            string Building = Result[12];
            string Floor = Result[13];
            string Almirah = Result[14];


            decimal PriceToD = 0;
            decimal PriceFD = 0;
            int Availability = 0;
            decimal.TryParse(PriceToS, out PriceToD);
            decimal.TryParse(PriceFrom, out PriceFD);
            int.TryParse(AvailabilityS, out Availability);

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(DateOfDebateFrom, out FromDate);
            DateTime.TryParse(DateOfDebateTo, out ToDate);

            SaleCounterContext db = new SaleCounterContext();


            var SearchData = (from PC in db.mDebateSaleCounter
                              orderby PC.DebateSaleCounterID
                              where PC.Khand.Contains(Khand.Trim()) || PC.Ank.Contains(Ank.Trim()) || PC.HindiDate.Contains(HindiDate.Trim())
                              || PC.HindiMonth.Contains(HindiMonth.Trim()) || PC.Pages.Contains(Pages.Trim()) || PC.Assembly.Contains(Assembly.Trim())
                                || PC.Building.Contains(Building.Trim()) || PC.Floor.Contains(Floor.Trim()) || PC.Almirah.Contains(Almirah.Trim())
                                || PC.Session.Contains(Session.Trim()) || (PC.DateOfDebateE >= FromDate && PC.DateOfDebateE <= ToDate)
                                || (PC.Price >= PriceFD && PC.Price <= PriceToD) || PC.Availability == Availability
                              select PC).ToList();


            return SearchData;


        }

        public static object GetDebateSaleCounterById(object param)
        {
            mDebateSaleCounter parameter = param as mDebateSaleCounter;

            SaleCounterContext db = new SaleCounterContext();

            var query = db.mDebateSaleCounter.SingleOrDefault(a => a.DebateSaleCounterID == parameter.DebateSaleCounterID);

            return query;
        }
        #endregion


        #region Debate Info

        public static object CreateDebateInfo(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mDebateInfo model = param as mDebateInfo;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mDebateInfo.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateDebateInfo(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mDebateInfo model = param as mDebateInfo;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mDebateInfo.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteDebateInfo(object param)
        {
            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    mDebateInfo parameter = param as mDebateInfo;
                    mDebateInfo stateToRemove = db.mDebateInfo.SingleOrDefault(a => a.DebateInfoID == parameter.DebateInfoID);
                    db.mDebateInfo.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllDebateInfoDetails(object param)
        {

            try
            {
                using (SaleCounterContext db = new SaleCounterContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from a in db.mDebateInfo
                                orderby a.DebateInfoID
                                select a).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object SearchDebateInfo(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string Title = Result[0];
            string Khand = Result[1];
            string Ank = Result[2];
            string DateOfDebateFrom = Result[3];
            string DateOfDebateTo = Result[4];
            string HindiDate = Result[5];
            string HindiMonth = Result[6];
            string HindiYear = Result[7];

            string Assembly = Result[8];
            string Session = Result[9];

            int TitleInt = 0;

            int.TryParse(Title, out TitleInt);

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(DateOfDebateFrom, out FromDate);
            DateTime.TryParse(DateOfDebateTo, out ToDate);

            SaleCounterContext db = new SaleCounterContext();


            var SearchData = (from PC in db.mDebateInfo
                              orderby PC.DebateInfoID
                              where PC.Title == TitleInt || PC.Khand.Contains(Khand.Trim()) || PC.Ank.Contains(Ank.Trim()) || PC.HindiDate.Contains(HindiDate.Trim())
                              || PC.HindiMonth.Contains(HindiMonth.Trim()) || PC.HindiYear.Contains(HindiYear.Trim()) || PC.Assembly.Contains(Assembly.Trim())
                                || PC.Session.Contains(Session.Trim()) || (PC.DateOfDebateE >= FromDate && PC.DateOfDebateE <= ToDate)
                              select PC).ToList();


            return SearchData;
        }


        public static object LocateDebateInfo(object param)
        {


            string Data = param as string;
            string[] Result = Data.Split(',');
            string Title = Result[0];
            string Khand = Result[1];
            string Ank = Result[2];
            string DateOfDebateFrom = Result[3];
            string DateOfDebateTo = Result[4];
            string HindiDate = Result[5];
            string HindiMonth = Result[6];
            string HindiYear = Result[7];

            string Assembly = Result[8];
            string Session = Result[9];

            string Building = Result[10];
            string Floor = Result[11];
            string Almirah = Result[12];

            int TitleInt = 0;

            int.TryParse(Title, out TitleInt);

            DateTime FromDate = new DateTime();
            DateTime ToDate = new DateTime();
            DateTime.TryParse(DateOfDebateFrom, out FromDate);
            DateTime.TryParse(DateOfDebateTo, out ToDate);

            SaleCounterContext db = new SaleCounterContext();


            var SearchData = (from PC in db.mDebateInfo
                              orderby PC.DebateInfoID
                              where PC.Title == TitleInt || PC.Khand.Contains(Khand.Trim()) || PC.Ank.Contains(Ank.Trim()) || PC.HindiDate.Contains(HindiDate.Trim())
                              || PC.HindiMonth.Contains(HindiMonth.Trim()) || PC.HindiYear.Contains(HindiYear.Trim()) || PC.Assembly.Contains(Assembly.Trim())
                                || PC.Building.Contains(Building.Trim()) || PC.Floor.Contains(Floor.Trim()) || PC.Almirah.Contains(Almirah.Trim())
                                || PC.Session.Contains(Session.Trim()) || (PC.DateOfDebateE >= FromDate && PC.DateOfDebateE <= ToDate)

                              select PC).ToList();


            return SearchData;


        }

        public static object GetDebateInfoById(object param)
        {
            mDebateInfo parameter = param as mDebateInfo;

            SaleCounterContext db = new SaleCounterContext();

            var query = db.mDebateInfo.SingleOrDefault(a => a.DebateInfoID == parameter.DebateInfoID);

            return query;
        }
        #endregion
    }
}
