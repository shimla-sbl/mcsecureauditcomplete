﻿using SBL.DAL;
using SBL.DomainModel.Models.Library;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Library
{
    class PaperLaidContext : DBBase<PaperLaidContext>
    {

        public PaperLaidContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }


        public DbSet<tPaperLaid> tPaperLaid { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreatePaperLaid":
                    {
                        return CreatePaperLaid(param.Parameter);
                    }
                case "UpdatePaperLaid":
                    {
                        return UpdatePaperLaid(param.Parameter);
                    }
                case "DeletePaperLaid":
                    {
                        return DeletePaperLaid(param.Parameter);
                    }
                case "GetAllPaperLaidDetails":
                    {
                        return GetAllPaperLaidDetails(param.Parameter);
                    }
                case "GetPaperLaidById":
                    {
                        return GetPaperLaidById(param.Parameter);
                    }

            }

            return null;
        }

        public static object CreatePaperLaid(object param)
        {
            try
            {
                using (PaperLaidContext db = new PaperLaidContext())
                {
                    tPaperLaid model = param as tPaperLaid;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tPaperLaid.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdatePaperLaid(object param)
        {
            try
            {
                using (PaperLaidContext db = new PaperLaidContext())
                {
                    tPaperLaid model = param as tPaperLaid;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tPaperLaid.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeletePaperLaid(object param)
        {
            try
            {
                using (PaperLaidContext db = new PaperLaidContext())
                {
                    tPaperLaid parameter = param as tPaperLaid;
                    tPaperLaid stateToRemove = db.tPaperLaid.SingleOrDefault(a => a.PaperLaidId == parameter.PaperLaidId);
                    db.tPaperLaid.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllPaperLaidDetails(object param)
        {

            PaperLaidContext db = new PaperLaidContext();

            var data = (from PC in db.tPaperLaid.OrderByDescending(a => a.PaperLaidId)
                        select PC).ToList();


            return data;
        }




        public static object GetPaperLaidById(object param)
        {
            tPaperLaid parameter = param as tPaperLaid;

            PaperLaidContext db = new PaperLaidContext();

            var query = db.tPaperLaid.SingleOrDefault(a => a.PaperLaidId == parameter.PaperLaidId);

            return query;
        }


    }
}
