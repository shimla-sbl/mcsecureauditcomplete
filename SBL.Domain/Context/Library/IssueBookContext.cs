﻿using SBL.DAL;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Library;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.PaidMemeberSubscription;
using SBL.DomainModel.Models.StaffManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Library
{
    public class IssueBookContext : DBBase<IssueBookContext>
    {
        public IssueBookContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<PaidMemeberSubs> PaidMemeberSubs { get; set; }
        public DbSet<mIssueBook> mIssueBook { get; set; }
        public DbSet<mMember> mMembers { get; set; }
        public DbSet<mStaff> mStaff { get; set; }
        public DbSet<mMemberDesignation> mMemberDesignations { get; set; }
        public DbSet<mIssueBookAccessionNum> mIssueBookAccessionNum { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateIssueBook":
                    {
                        return CreateIssueBook(param.Parameter);
                    }
                case "UpdateIssueBook":
                    {
                        return UpdateIssueBook(param.Parameter);
                    }
                case "DeleteIssueBook":
                    {
                        return DeleteIssueBook(param.Parameter);
                    }
                case "GetAllIssueBookDetails":
                    {
                        return GetAllIssueBookDetails(param.Parameter);
                    }
                case "GetIssueBookById":
                    {
                        return GetIssueBookById(param.Parameter);
                    }
                case "SearchIssueBook":
                    {
                        return SearchIssueBook(param.Parameter);
                    }
                case "CreateAccessionNumber":
                    {
                        return CreateAccessionNumber(param.Parameter);
                    }
                case "GetAllAccessNumDetails":
                    {
                        return GetAllAccessNumDetails(param.Parameter);
                    }
                case "DeleteIssueBookAccessionNum":
                    {
                        return DeleteIssueBookAccessionNum(param.Parameter);
                    }
                case "SearchAccesionNum":
                    {
                        return SearchAccesionNum(param.Parameter);
                    }

                case "GetAccessionNumById":
                    {
                        return GetAccessionNumById(param.Parameter);
                    }
                case "UpdateAccessionNum":
                    {
                        return UpdateAccessionNum(param.Parameter);
                    }
                case "GetAllIssueBookDetailsForStaff":
                    {
                        return GetAllIssueBookDetailsForStaff(param.Parameter);
                    }
                case "PaidMemberSubscriptionList":
                    {
                        return PaidMemberSubscriptionList(param.Parameter);
                    }
                case "GetPaidMemberDetails":
                    {
                        return GetPaidMemberDetails(param.Parameter);
                    }
                case "GetAllIssueBookDetailsForPaid":
                    {
                        return GetAllIssueBookDetailsForPaid(param.Parameter);
                    }
                case "DesignationOfstaffWhomBookIssued":
                    {
                        return DesignationOfstaffWhomBookIssued(param.Parameter);
                    }
                case "SubsOfstaffWhomBookIssued":
                    {
                        return SubsOfstaffWhomBookIssued(param.Parameter);
                    }
                case "DesignationOfMemberWhomBookIssued":
                    {
                        return DesignationOfMemberWhomBookIssued(param.Parameter);
                    }
                case "SubsOfMemberWhomBookIssued":
                    {
                        return SubsOfMemberWhomBookIssued(param.Parameter);
                    }
                case "SubsOfPaidWhomBookIssued":
                    {
                        return SubsOfPaidWhomBookIssued(param.Parameter);
                    }
                case "SearchIssueBookForStaff":
                    {
                        return SearchIssueBookForStaff(param.Parameter);
                    }
                case "SearchIssueBookForPaid":
                    {
                        return SearchIssueBookForPaid(param.Parameter);
                    }
            }

            return null;
        }

        #region Issue Book
        public static object CreateIssueBook(object param)
        {
            try
            {
                mIssueBook model = param as mIssueBook;

                using (IssueBookContext db = new IssueBookContext())
                {

                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    try
                    {
                        db.mIssueBook.Add(model);
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException e) { foreach (var eve in e.EntityValidationErrors) { Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State); foreach (var ve in eve.ValidationErrors) { Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage); } } throw; }
                    db.Close();
                }

                return model.IssueBookID;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateIssueBook(object param)
        {
            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    mIssueBook model = param as mIssueBook;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mIssueBook.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteIssueBook(object param)
        {
            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    mIssueBook parameter = param as mIssueBook;
                    mIssueBook stateToRemove = db.mIssueBook.SingleOrDefault(a => a.IssueBookID == parameter.IssueBookID);
                    db.mIssueBook.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllIssueBookDetails(object param)
        {

            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    //Organization Organization = param as Organization;



                    var data = (from P in db.mIssueBook
                                join M in db.mMembers on P.SubscriberId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                //join D in db.mMemberDesignations on new { Desigcode = P.SubscriberDesignationId } equals new { Desigcode = D.memDesigcode } into D_join
                                //from D in D_join.DefaultIfEmpty()
                                where P.IsMember == true && P.IsPaidMember == false
                                orderby P.IssueBookID
                                select new
                                {
                                    P,
                                    Name = M.Name,
                                    Prefix = M.Prefix,
                                    Address = M.ShimlaAddress,
                                    Designame = P.SubscriberDesignationId,
                                    Email = M.Email,
                                    Mobile = M.Mobile,
                                    isMember = P.IsMember

                                }).ToList();

                    List<mIssueBook> result = new List<mIssueBook>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            mIssueBook partdata = new mIssueBook();
                            partdata = item.P;
                            partdata.SubscriberName = item.Name;
                            partdata.Prefix = item.Prefix;
                            partdata.SubscriberDesignation = item.Designame;
                            partdata.Email = item.Email;
                            partdata.Mobile = item.Mobile;

                            partdata.SubscriberDesignation = item.isMember ? (from des in db.mMemberDesignations
                                                                              where des.memDesigcode == partdata.Designationid
                                                                              select des.memDesigname).FirstOrDefault() : item.Designame;
                            result.Add(partdata);
                        }


                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllIssueBookDetailsForStaff(object param)
        {

            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    var data = (from P in db.mIssueBook
                                //join M in db.mMembers on P.SubscriberId equals M.MemberCode into M_join
                                //from M in M_join.DefaultIfEmpty()
                                //join D in db.mMemberDesignations on new { Desigcode = P.SubscriberDesignationId } equals new { Desigcode = D.memDesigcode } into D_join
                                //from D in D_join.DefaultIfEmpty()
                                where P.IsMember == false && P.IsPaidMember == false
                                orderby P.IssueBookID
                                select new
                                {
                                    P,
                                    subid = P.SubscriberId,
                                    Designame = P.SubscriberDesignationId,
                                    isMember = P.IsMember

                                }).ToList();

                    List<mIssueBook> result = new List<mIssueBook>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            mIssueBook partdata = new mIssueBook();
                            partdata = item.P;
                            partdata.SubscriberId = item.subid;
                            partdata.SubscriberName = (from des in db.mStaff
                                                       where des.StaffID == partdata.SubscriberId
                                                       select des.StaffName).FirstOrDefault();
                            //partdata.Prefix = item.Prefix;
                            partdata.SubscriberDesignation = item.Designame;
                            //partdata.Email = item.Email;
                            //partdata.Mobile = item.Mobile;
                            result.Add(partdata);
                        }


                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllIssueBookDetailsForPaid(object param)
        {

            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    var data = (from P in db.mIssueBook
                                join M in db.PaidMemeberSubs on P.SubscriberId equals M.PaidMemeberSubsID into M_join
                                from M in M_join.DefaultIfEmpty()
                                //join D in db.mMemberDesignations on new { Desigcode = P.SubscriberDesignationId } equals new { Desigcode = D.memDesigcode } into D_join
                                //from D in D_join.DefaultIfEmpty()
                                where P.IsPaidMember == true
                                orderby P.IssueBookID
                                select new
                                {
                                    P,
                                    subid = P.SubscriberId,
                                    MemName = M.NameOfSubscriber,
                                    Designame = M.Designation,
                                    isMember = P.IsMember

                                }).ToList();

                    List<mIssueBook> result = new List<mIssueBook>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            mIssueBook partdata = new mIssueBook();
                            partdata = item.P;
                            partdata.SubscriberId = item.subid;
                            partdata.SubscriberName = item.MemName;
                            //partdata.Prefix = item.Prefix;
                            partdata.SubscriberDesignation = item.Designame;
                            //partdata.Email = item.Email;
                            //partdata.Mobile = item.Mobile;
                            result.Add(partdata);
                        }


                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

       

        public static object GetIssueBookById(object param)
        {
            mIssueBook parameter = param as mIssueBook;

            IssueBookContext db = new IssueBookContext();

            var data = (from P in db.mIssueBook
                        join M in db.mMembers on P.SubscriberId equals M.MemberCode into M_join
                        from M in M_join.DefaultIfEmpty()
                        // join D in db.mMemberDesignations on new { Desigcode = P.SubscriberDesignationId } equals new { Desigcode = D.memDesigcode } into D_join
                        // from D in D_join.DefaultIfEmpty()
                        where P.IssueBookID == parameter.IssueBookID
                        select new
                        {
                            P,
                            Name = M.Name,
                            Prefix = M.Prefix,
                            Address = M.ShimlaAddress,
                            // Designame = D.memDesigname,
                            Email = M.Email,
                            Mobile = M.Mobile,
                            SubscriberDesignationId = P.SubscriberDesignationId,
                            IsMember = P.IsMember

                        }).FirstOrDefault();



            mIssueBook partdata = new mIssueBook();
            if (data != null)
            {
                partdata = data.P;
                partdata.SubscriberName = data.Name;
                partdata.Prefix = data.Prefix;
                // partdata.SubscriberDesignation = data.Designame;
                if (data.IsMember)
                {
                    int DesignationId = int.Parse(data.SubscriberDesignationId);
                    partdata.SubscriberDesignation = (from D in db.mMemberDesignations
                                                      where D.memDesigcode == DesignationId
                                                      select new { D.memDesigname }).ToString();
                }
                else
                {

                    partdata.SubscriberDesignation = data.SubscriberDesignationId;
                }
                partdata.Email = data.Email;
                partdata.Mobile = data.Mobile;
            }
            return partdata;
        }

        #endregion

        #region Accession Number

        public static object CreateAccessionNumber(object param)
        {
            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    mIssueBookAccessionNum model = param as mIssueBookAccessionNum;
                    db.mIssueBookAccessionNum.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Get Accesion Number List by Issue Book Id
        /// </summary>
        /// <param name="param">IssueBookID</param>
        /// <returns>AccessNumberList</returns>
        /// 
        public static object GetAllAccessNumDetails(object param)
        {
            string Id = param as string;
            int IssueBookId = 0;
            int.TryParse(Id, out IssueBookId);
            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from a in db.mIssueBookAccessionNum
                                orderby a.IssueBookAccessionNumID
                                where a.IssueBookId == IssueBookId
                                select a).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Delete Accesion Number by Issue Book Id
        /// </summary>
        /// <param name="param">IssueBookID</param>
        /// <returns></returns>
        public static object DeleteIssueBookAccessionNum(object param)
        {
            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    mIssueBook model = param as mIssueBook;
                    var data = (from a in db.mIssueBookAccessionNum
                                orderby a.IssueBookAccessionNumID
                                where a.IssueBookId == model.IssueBookID
                                select a).ToList();
                    foreach (var item in data)
                    {
                        mIssueBookAccessionNum stateToRemove = db.mIssueBookAccessionNum.SingleOrDefault(a => a.IssueBookAccessionNumID == item.IssueBookAccessionNumID);
                        db.mIssueBookAccessionNum.Remove(stateToRemove);
                        db.SaveChanges();
                    }
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Get Accesion Number List by Subscriber Designation & Subscriber Name
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static object SearchAccesionNum(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string SubscriberDesignationS = Result[0];
            string MemberCodeS = Result[1];



            int MemberCode = 0;
            //int SubscriberDesignation = 0;
            //int AccessNumber = 0;

            int.TryParse(MemberCodeS, out MemberCode);
            // int.TryParse(SubscriberDesignationS, out SubscriberDesignation);
            // int.TryParse(AccessNumberS, out AccessNumber);

            IssueBookContext db = new IssueBookContext();


            var SearchData = (from IBAM in db.mIssueBookAccessionNum
                              join IB in db.mIssueBook on IBAM.IssueBookId equals IB.IssueBookID
                              orderby IB.IssueBookID
                              where IB.SubscriberId == MemberCode && IB.SubscriberDesignationId == SubscriberDesignationS
                              select IBAM).ToList();


            return SearchData;
        }


        public static object UpdateAccessionNum(object param)
        {
            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    mIssueBookAccessionNum model = param as mIssueBookAccessionNum;
                    db.mIssueBookAccessionNum.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }
        public static object GetAccessionNumById(object param)
        {
            mIssueBookAccessionNum parameter = param as mIssueBookAccessionNum;

            IssueBookContext db = new IssueBookContext();

            var query = db.mIssueBookAccessionNum.SingleOrDefault(a => a.IssueBookAccessionNumID == parameter.IssueBookAccessionNumID);

            return query;

        }
        #endregion

        #region for showing list of paid subscriber
        public static object PaidMemberSubscriptionList(object param)
        {
            try
            {
                using (IssueBookContext db = new IssueBookContext())
                {
                    var list = (from a in db.PaidMemeberSubs
                                select a).ToList();
                    return list;
                }
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        #region for showing paidmember details
        public static object GetPaidMemberDetails(object param)
        {
            try
            {
                int id = Convert.ToInt16(param);
                using (IssueBookContext db = new IssueBookContext())
                {
                    var list = (from a in db.PaidMemeberSubs
                                where a.PaidMemeberSubsID == id
                                select a
                                    ).FirstOrDefault();
                    return list;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        public static object DesignationOfstaffWhomBookIssued(object param)
        {
            try
            {   
                IssueBookContext db = new IssueBookContext();

                var query = (from D in db.mStaff
                             join M in db.mIssueBook on D.Designation equals M.SubscriberDesignationId into M_join
                             from M in M_join.DefaultIfEmpty()
                             where M.IsMember == false && M.IsPaidMember == false
                             select D).GroupBy(x => x.Designation).Select(y => y.FirstOrDefault()).ToList();
                return query;               
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SubsOfstaffWhomBookIssued(object param)
        {
            try
            {
                string designationID = param.ToString();
                IssueBookContext db = new IssueBookContext();

                var staffByDesg = (from staff in db.mStaff
                                     join staffIssueBook in db.mIssueBook
                                     on staff.StaffID equals staffIssueBook.SubscriberId
                                   where staffIssueBook.IsMember == false && staffIssueBook.IsPaidMember == false && staffIssueBook.SubscriberDesignationId == designationID
                                   select staff).Distinct().ToList();

                return staffByDesg;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object DesignationOfMemberWhomBookIssued(object param)
        {
            try
            {   

                IssueBookContext db = new IssueBookContext();

                var list = (from M in db.mIssueBook
                            where M.IsMember == true && M.IsPaidMember == false
                            select M.SubscriberDesignationId).Cast<long>().ToList();

                var query = (from D in db.mMemberDesignations
                             where list.Contains(D.memDesigcode)
                             select D).ToList();
                             
                return query;      
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SubsOfMemberWhomBookIssued(object param)
        {
            try
            {
                string designationID = param.ToString();
                IssueBookContext db = new IssueBookContext();

                var membersByDesg = (from members in db.mMembers
                                     join memberIssueBook in db.mIssueBook
                                     on members.MemberCode equals memberIssueBook.SubscriberId
                                     where memberIssueBook.IsMember==true && memberIssueBook.IsPaidMember ==false && memberIssueBook.SubscriberDesignationId ==designationID
                                     select members).Distinct().ToList();

                return membersByDesg;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static object SubsOfPaidWhomBookIssued(object param)
        {
            try
            {
                string designationID = param.ToString();
                IssueBookContext db = new IssueBookContext();

                var staffByDesg = (from paid in db.PaidMemeberSubs
                                   join paidIssueBook in db.mIssueBook
                                   on paid.PaidMemeberSubsID equals paidIssueBook.SubscriberId
                                   where paidIssueBook.IsMember == false && paidIssueBook.IsPaidMember == true
                                   select paid).Distinct().ToList();

                return staffByDesg;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #region for searching book

        public static object SearchIssueBook(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string MemberCodeS = Result[0];
            string IssueDateFromS = Result[1];
            string IssueDateToS = Result[2];
            string AccessNumberS = Result[3];
            string ReturnDateFromS = Result[4];
            string ReturnDateToS = Result[5];
            string SubscriberDesignationS = Result[6];


            DateTime IssueDateFrom = new DateTime();
            DateTime IssueDateTo = new DateTime();
            DateTime ReturnDateFrom = new DateTime();
            DateTime ReturnDateTo = new DateTime();

            int MemberCode = 0;
            int SubscriberDesignation = 0;
            //int AccessNumber = 0;

            DateTime.TryParse(IssueDateFromS, out IssueDateFrom);
            DateTime.TryParse(IssueDateToS, out IssueDateTo);
            DateTime.TryParse(ReturnDateFromS, out ReturnDateFrom);
            DateTime.TryParse(ReturnDateToS, out ReturnDateTo);

            int.TryParse(MemberCodeS, out MemberCode);
            //int.TryParse(SubscriberDesignationS, out SubscriberDesignation);
            // int.TryParse(AccessNumberS, out AccessNumber);

            IssueBookContext db = new IssueBookContext();


            var SearchData = (from IB in db.mIssueBook
                              join IBAM in db.mIssueBookAccessionNum on IB.IssueBookID equals IBAM.IssueBookId
                              join M in db.mMembers on IB.SubscriberId equals M.MemberCode into M_join
                              from M in M_join.DefaultIfEmpty()
                              //join D in db.mMemberDesignations on new { Desigcode = IB.SubscriberDesignationId } equals new { Desigcode = D.memDesigcode.ToString() } into D_join
                              //from D in D_join.DefaultIfEmpty()
                              orderby IB.IssueBookID
                              where IB.SubscriberId == MemberCode || IB.SubscriberDesignationId == SubscriberDesignationS || IBAM.AccessNumber.Equals(AccessNumberS)
                              || (IB.DateOfIssue >= IssueDateFrom && IB.DateOfIssue <= IssueDateTo) || (IBAM.DateOfReturn >= ReturnDateFrom && IBAM.DateOfReturn <= ReturnDateTo) && IB.IsMember == true && IB.IsPaidMember == false
                              select new
                              {
                                  IB,
                                  Name = M.Name,
                                  Prefix = M.Prefix,
                                  Address = M.ShimlaAddress,
                                  Designame = IB.SubscriberDesignationId,
                              }
                              ).Distinct().ToList();
            List<mIssueBook> result = new List<mIssueBook>();
            if (SearchData != null && SearchData.Count() > 0)
            {
                foreach (var item in SearchData)
                {
                    mIssueBook partdata = new mIssueBook();
                    partdata = item.IB;
                    partdata.SubscriberName = item.Name;
                    partdata.Prefix = item.Prefix;
                    int DesignationId = int.Parse(item.Designame);
                    partdata.SubscriberDesignation = (from D in db.mMemberDesignations
                                                      where D.memDesigcode == DesignationId
                                                      select D.memDesigname).FirstOrDefault();
                    result.Add(partdata);
                }
            }
            else {
                var SearchDataForAll = (from IB in db.mIssueBook
                                  join IBAM in db.mIssueBookAccessionNum on IB.IssueBookID equals IBAM.IssueBookId
                                  join M in db.mMembers on IB.SubscriberId equals M.MemberCode into M_join
                                  from M in M_join.DefaultIfEmpty()
                                  //join D in db.mMemberDesignations on new { Desigcode = IB.SubscriberDesignationId } equals new { Desigcode = D.memDesigcode.ToString() } into D_join
                                  //from D in D_join.DefaultIfEmpty()
                                  orderby IB.IssueBookID
                                  where IB.IsMember == true && IB.IsPaidMember == false
                                  select new
                                  {
                                      IB,
                                      Name = M.Name,
                                      Prefix = M.Prefix,
                                      Address = M.ShimlaAddress,
                                      Designame = IB.SubscriberDesignationId,
                                  }
                                 ).Distinct().ToList();
                foreach (var item in SearchDataForAll)
                {
                    mIssueBook partdata = new mIssueBook();
                    partdata = item.IB;
                    partdata.SubscriberName = item.Name;
                    partdata.Prefix = item.Prefix;
                    int DesignationId = int.Parse(item.Designame);
                    partdata.SubscriberDesignation = (from D in db.mMemberDesignations
                                                      where D.memDesigcode == DesignationId
                                                      select D.memDesigname).FirstOrDefault();
                    result.Add(partdata);
                }
            }

            return result;
        }

        public static object SearchIssueBookForStaff(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string MemberCodeS = Result[0];
            string IssueDateFromS = Result[1];
            string IssueDateToS = Result[2];
            string AccessNumberS = Result[3];
            string ReturnDateFromS = Result[4];
            string ReturnDateToS = Result[5];
            string SubscriberDesignationS = Result[6];


            DateTime IssueDateFrom = new DateTime();
            DateTime IssueDateTo = new DateTime();
            DateTime ReturnDateFrom = new DateTime();
            DateTime ReturnDateTo = new DateTime();

            int MemberCode = 0;
            int SubscriberDesignation = 0;
            //int AccessNumber = 0;

            DateTime.TryParse(IssueDateFromS, out IssueDateFrom);
            DateTime.TryParse(IssueDateToS, out IssueDateTo);
            DateTime.TryParse(ReturnDateFromS, out ReturnDateFrom);
            DateTime.TryParse(ReturnDateToS, out ReturnDateTo);

            int.TryParse(MemberCodeS, out MemberCode);
            //int.TryParse(SubscriberDesignationS, out SubscriberDesignation);
            // int.TryParse(AccessNumberS, out AccessNumber);

            IssueBookContext db = new IssueBookContext();


            var SearchData = (from IB in db.mIssueBook
                              join IBAM in db.mIssueBookAccessionNum on IB.IssueBookID equals IBAM.IssueBookId
                              join staff in db.mStaff on IB.SubscriberDesignationId equals staff.Designation into staff_join
                              from staff in staff_join.DefaultIfEmpty() 
                              //join M in db.mMembers on IB.SubscriberId equals M.MemberCode into M_join
                              //from M in M_join.DefaultIfEmpty()
                              //join D in db.mMemberDesignations on new { Desigcode = IB.SubscriberDesignationId } equals new { Desigcode = D.memDesigcode.ToString() } into D_join
                              //from D in D_join.DefaultIfEmpty()
                              orderby IB.IssueBookID
                              where IB.SubscriberId == MemberCode || IB.SubscriberDesignationId == SubscriberDesignationS || IBAM.AccessNumber.Equals(AccessNumberS)
                              || (IB.DateOfIssue >= IssueDateFrom && IB.DateOfIssue <= IssueDateTo) || (IBAM.DateOfReturn >= ReturnDateFrom && IBAM.DateOfReturn <= ReturnDateTo) && IB.IsMember==false && IB.IsPaidMember==false
                              select new
                              {
                                  IB,
                                  Name = staff.StaffName,
                                  Designame = IB.SubscriberDesignationId,
                              }
                              ).Distinct().ToList();
            List<mIssueBook> result = new List<mIssueBook>();
            if (SearchData != null && SearchData.Count() > 0)
            {
                foreach (var item in SearchData)
                {
                    mIssueBook partdata = new mIssueBook();
                    partdata = item.IB;
                    partdata.SubscriberName = item.Name;
                    partdata.SubscriberDesignation = item.Designame;
                    result.Add(partdata);
                }
            }
            else {
                var SearchDataForAll = (from IB in db.mIssueBook
                                  join IBAM in db.mIssueBookAccessionNum on IB.IssueBookID equals IBAM.IssueBookId
                                  join staff in db.mStaff on IB.SubscriberDesignationId equals staff.Designation into staff_join
                                  from staff in staff_join.DefaultIfEmpty()                                 
                                  orderby IB.IssueBookID
                                  where IB.IsMember == false && IB.IsPaidMember == false
                                  select new
                                  {
                                      IB,
                                      Name = staff.StaffName,
                                      Designame = IB.SubscriberDesignationId,
                                  }
                                 ).Distinct().ToList();
                foreach (var item in SearchDataForAll)
                {
                    mIssueBook partdata = new mIssueBook();
                    partdata = item.IB;
                    partdata.SubscriberName = item.Name;
                    partdata.SubscriberDesignation = item.Designame;
                    result.Add(partdata);
                }
            }
            return result;
        }

        public static object SearchIssueBookForPaid(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string MemberCodeS = Result[0];
            string IssueDateFromS = Result[1];
            string IssueDateToS = Result[2];
            string AccessNumberS = Result[3];
            string ReturnDateFromS = Result[4];
            string ReturnDateToS = Result[5];
            string SubscriberDesignationS = Result[6];


            DateTime IssueDateFrom = new DateTime();
            DateTime IssueDateTo = new DateTime();
            DateTime ReturnDateFrom = new DateTime();
            DateTime ReturnDateTo = new DateTime();

            int MemberCode = 0;
            int SubscriberDesignation = 0;
            //int AccessNumber = 0;

            DateTime.TryParse(IssueDateFromS, out IssueDateFrom);
            DateTime.TryParse(IssueDateToS, out IssueDateTo);
            DateTime.TryParse(ReturnDateFromS, out ReturnDateFrom);
            DateTime.TryParse(ReturnDateToS, out ReturnDateTo);

            int.TryParse(MemberCodeS, out MemberCode);
            //int.TryParse(SubscriberDesignationS, out SubscriberDesignation);
            // int.TryParse(AccessNumberS, out AccessNumber);

            IssueBookContext db = new IssueBookContext();


            var SearchData = (from IB in db.mIssueBook
                              join IBAM in db.mIssueBookAccessionNum on IB.IssueBookID equals IBAM.IssueBookId
                              join paid in db.PaidMemeberSubs on IB.SubscriberId equals paid.PaidMemeberSubsID into paid_join
                              from paids in paid_join.DefaultIfEmpty()
                              //join M in db.mMembers on IB.SubscriberId equals M.MemberCode into M_join
                              //from M in M_join.DefaultIfEmpty()
                              //join D in db.mMemberDesignations on new { Desigcode = IB.SubscriberDesignationId } equals new { Desigcode = D.memDesigcode.ToString() } into D_join
                              //from D in D_join.DefaultIfEmpty()
                              orderby IB.IssueBookID
                              where (IB.IsMember == false) && (IB.IsPaidMember == true) &&
                              IB.SubscriberId == MemberCode || IBAM.AccessNumber.Equals(AccessNumberS)
                              || (IB.DateOfIssue >= IssueDateFrom && IB.DateOfIssue <= IssueDateTo) || (IBAM.DateOfReturn >= ReturnDateFrom && IBAM.DateOfReturn <= ReturnDateTo)
                              select new
                              {
                                  IB,
                                  Name = paids.NameOfSubscriber,
                                  Address = paids.Address,
                                  Designame = paids.Designation,
                              }
                              ).Distinct().ToList();
            List<mIssueBook> result = new List<mIssueBook>();
            if (SearchData != null && SearchData.Count() > 0)
            {
                foreach (var item in SearchData)
                {
                    mIssueBook partdata = new mIssueBook();
                    partdata = item.IB;
                    partdata.SubscriberName = item.Name;
                    partdata.SubscriberDesignation = item.Designame;
                    result.Add(partdata);
                }
            }
            else {
                var SearchDataForAll = (from IB in db.mIssueBook
                                  join IBAM in db.mIssueBookAccessionNum on IB.IssueBookID equals IBAM.IssueBookId
                                  join paid in db.PaidMemeberSubs on IB.SubscriberId equals paid.PaidMemeberSubsID into paid_join
                                  from paids in paid_join.DefaultIfEmpty()
                                  orderby IB.IssueBookID
                                  where (IB.IsMember == false) && (IB.IsPaidMember == true)
                                  select new
                                  {
                                      IB,
                                      Name = paids.NameOfSubscriber,
                                      Address = paids.Address,
                                      Designame = paids.Designation,
                                  }
                                 ).Distinct().ToList();

                foreach (var item in SearchDataForAll)
                {
                    mIssueBook partdata = new mIssueBook();
                    partdata = item.IB;
                    partdata.SubscriberName = item.Name;
                    partdata.SubscriberDesignation = item.Designame;
                    result.Add(partdata);
                }
            }
            return result;
        }

        #endregion
    }
}
