﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.LibraryVS;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Library
{
    class BillInformationContext : DBBase<BillInformationContext>
    {
        public BillInformationContext() : base("eVidhan") { }

        #region Library refferences
        public virtual DbSet<VsLibraryBillInfo> VsLibraryBillInfo { get; set; }
        public virtual DbSet<mBills> mBills { get; set; }
        #endregion Library refferences

        #region Service Parameters
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "BillCheck":
                    {
                        return BillCheck(param.Parameter);
                    }
                case "SaveBillInfo":
                    {
                        return SaveBillInfo(param.Parameter);
                    }   
                case "UpdateBillInfo":
                    {
                        return UpdateBillInfo(param.Parameter);
                    }
                case "BillCheckForUpdation":
                    {
                        return BillCheckForUpdation(param.Parameter);
                    }
                case "SearchBillInfoForAll":
                    {
                        return SearchBillInfoForAll(param.Parameter);
                    }
                case "SearchBillLocation":
                    {
                        return SearchBillLocation(param.Parameter);
                    }
                case "UpdateBillInfoWithSerialNo":
                    {
                        return UpdateBillInfoWithSerialNo(param.Parameter);
                    }
                case "GetBillNoInfo":
                    {
                        return GetBillNoInfo(param.Parameter);
                    }
                case "GetBillNoInfoTitle":
                    {
                        return GetBillNoInfoTitle(param.Parameter);
                    }
            }
            return null;
        }
        #endregion Service Parameters

        #region Bill Check
        public static object BillCheck(object param)
        {
            VsLibraryBillInfo BillInfo = param as VsLibraryBillInfo;
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBillInfo
                                   where a.Title == BillInfo.Title
                                   //&& a.Year == BillInfo.Year
                                   && a.BillNumber  == BillInfo.BillNumber
                                   && a.status == BillInfo.status
                                   && a.Building == BillInfo.Building
                                   && a.Floor == BillInfo.Floor
                                   && a.Almirah == BillInfo.Almirah
                                   && a.Rack == BillInfo.Rack
                                   select a).FirstOrDefault();

                    return content;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Bill Check for Updation
        public static object BillCheckForUpdation(object param)
        {
            VsLibraryBillInfo BillInfo = param as VsLibraryBillInfo;
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBillInfo
                                   where a.Title.Contains(BillInfo.Title)
                                   //|| a.Year.Contains(BillInfo.Year)
                                   || a.BillNumber.Contains(BillInfo.BillNumber)
                                   || a.status.Contains(BillInfo.status)
                                   || a.Building.Contains(BillInfo.Building)
                                   || a.Floor.Contains(BillInfo.Floor)
                                   || a.Almirah.Contains(BillInfo.Almirah)
                                   || a.Rack == BillInfo.Rack
                                   select a).ToList();

                    return content;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Search Bill Info For anyone
        public static object SearchBillInfoForAll(object param)
        {
            VsLibraryBillInfo BillInfo = param as VsLibraryBillInfo;
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBillInfo
                                   where a.Title.Contains(BillInfo.Title)
                                   //|| a.Year.Contains(BillInfo.Year)
                                   || a.BillNumber.Contains(BillInfo.BillNumber)
                                   || a.status.Contains(BillInfo.status)
                                   select a).ToList();

                    return content;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Search Bill Location
        public static object SearchBillLocation(object param)
        {
            VsLibraryBillInfo BillInfo = param as VsLibraryBillInfo;
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBillInfo
                                   where a.Title.Contains(BillInfo.Title)
                                   //|| a.Year.Contains(BillInfo.Year)
                                   || a.BillNumber.Contains(BillInfo.BillNumber)
                                   || a.status.Contains(BillInfo.status)
                                   || a.Building.Contains(BillInfo.Building)
                                   || a.Floor.Contains(BillInfo.Floor)
                                   || a.Almirah.Contains(BillInfo.Almirah)
                                   || a.Rack.Contains(BillInfo.Rack)
                                   select a).ToList();

                    return content;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region To save the New Bill Info
        public static object SaveBillInfo(object param)
        {
            VsLibraryBillInfo BillInfo = param as VsLibraryBillInfo;
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    //var content = (from a in context.VsLibraryBillInfo
                    //               where a.BillNumber == BillInfo.BillNumber
                    //               select a).FirstOrDefault();


                    context.VsLibraryBillInfo.Add(BillInfo);
                    context.SaveChanges();
                    context.Close();


                    return BillInfo;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region To Update The Bill Info
        public static object UpdateBillInfo(object param)
        {
            VsLibraryBillInfo BillInfo = param as VsLibraryBillInfo;
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBillInfo
                                   where a.SerialNumber == BillInfo.SerialNumber
                                   select a).FirstOrDefault();

                    content.Almirah = BillInfo.Almirah;
                    content.BillNumber = BillInfo.BillNumber;
                    content.Building = BillInfo.Building;
                    content.Floor = BillInfo.Floor;
                    content.Rack = BillInfo.Rack;
                    content.status = BillInfo.status;
                    content.Title = BillInfo.Title;
                    //content.Year = BillInfo.Year;

                    context.VsLibraryBillInfo.Attach(content);
                    context.Entry(content).State = EntityState.Modified;
                    context.SaveChanges();

                    return BillInfo;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region To return bill info with serial no
        public static object UpdateBillInfoWithSerialNo(object param)
        {
            var SerialNoForUpdation = Convert.ToInt16(param);
            //VsLibraryBulletin LibraryBulletin = param as VsLibraryBulletin;
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    var BillInfo = (from a in context.VsLibraryBillInfo
                                    where a.SerialNumber == SerialNoForUpdation
                                    select a).SingleOrDefault();
                    return BillInfo;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        public static object GetBillNoInfo(object param)
        {
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    var list = (from a in context.mBills
                                select new fillListGenric
                                {
                                    Text = a.BillNo,
                                    value = a.BillNo
                                }
                                ).ToList();
                    return list;
                }
                catch (Exception)
                {                   
                    throw;
                }
            }
            
        }

        public static object GetBillNoInfoTitle(object param)
        {
            using (BillInformationContext context = new BillInformationContext())
            {
                try
                {
                    var data = param as string;
                    var list = (from a in context.mBills
                                where a.BillNo == data
                                select a.BillTitle).SingleOrDefault();
                    return list;
                }
                catch (Exception)
                {
                    
                    throw;
                }
            }
        }
    }
}