﻿using SBL.DAL;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.LibraryVS;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.StaffManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;

using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;


namespace SBL.Domain.Context.Library
{
    public class IssueDocumentContext : DBBase<IssueDocumentContext>
    {
        public IssueDocumentContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public DbSet<VsDocumentIssue> VsDocumentIssue { get; set; }

        public DbSet<mMemberDesignation> mMemberDesignation { get; set; }

        public DbSet<mMember> mMembers { get; set; }

        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }

        public DbSet<mStaff> mStaff { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateDocumentIssue":
                    {
                        return CreateDocumentIssue(param.Parameter);
                    }
                case "GetMemberListByDesignation":
                    {
                        return GetMemberListByDesignation(param.Parameter);
                    }
                case "GetAllDesignations":
                    {
                        return GetAllDesignations(param.Parameter);
                    }
                case "GetMembersWithUnReturnedDocs":
                    {
                        return GetMembersWithUnReturnedDocs(param.Parameter);
                    }
                case "GetStaffWithUnReturnedDocs":
                    {
                        return GetStaffWithUnReturnedDocs(param.Parameter);
                    }
                case "GetUnReturnedDocumentsForMember":
                    {
                        return GetUnReturnedDocumentsForMember(param.Parameter);
                    }
                case "GetUnReturnedDocumentsForStaff":
                    {
                        return GetUnReturnedDocumentsForStaff(param.Parameter);
                    }
                case "GetAllDesignationsForStaff":
                    {
                        return GetAllDesignationsForStaff(param.Parameter);
                    }
                case "GetMemberListByDesignationForStaff":
                    {
                        return GetMemberListByDesignationForStaff(param.Parameter);
                    }
                case "UpdateReturnedDocumentsForMembers":
                    {
                        return UpdateReturnedDocumentsForMembers(param.Parameter);
                    }
                case "UpdateReturnedDocumentsForStaff":
                    {
                        return UpdateReturnedDocumentsForStaff(param.Parameter);
                    }
                case "GetReportWIthCriteria":
                    {
                        return GetReportWIthCriteria(param.Parameter);
                    }
            }
            return null;
        }

        #region Get All Designations For Staff
        public static object GetAllDesignationsForStaff(object param)
        {
            try
            {
                //int designationID = int.Parse(param.ToString());

                IssueDocumentContext context = new IssueDocumentContext();

                var membersByDesg = (from staff in context.mStaff
                                     select staff).GroupBy(x => x.Designation).Select(y => y.FirstOrDefault()).ToList();

                // var membersByDesg = (from memb in context.mStaff
                //                    group memb by memb.StaffName
                //                        into disitinctMembers
                //select disitinctMembers).ToList();
                return membersByDesg;

            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Get MemberList By Designation For Staff
        public static object GetMemberListByDesignationForStaff(object param)
        {
            try
            {
                string designationName = param.ToString();

                IssueDocumentContext context = new IssueDocumentContext();

                var membersByDesg = (from staff in context.mStaff
                                     where staff.Designation == designationName
                                     select staff).Distinct().ToList();

                return membersByDesg;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Get MemberList By Designation
        public static object GetMemberListByDesignation(object param)
        {
            try
            {
                int designationID = int.Parse(param.ToString());

                IssueDocumentContext context = new IssueDocumentContext();

                var membersByDesg = (from members in context.mMembers
                                     join memberAssembly in context.mMemberAssembly
                                     on members.MemberCode equals memberAssembly.MemberID
                                     where memberAssembly.DesignationID == designationID
                                     select members).Distinct().ToList();

                return membersByDesg;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion Get MemberList By Designation

        #region Create Document Issue
        public static object CreateDocumentIssue(object param)
        {
            try
            {
                VsDocumentIssue documentIssue = param as VsDocumentIssue;
                using (IssueDocumentContext context = new IssueDocumentContext())
                {
                    context.VsDocumentIssue.Add(documentIssue);
                    context.SaveChanges();
                    context.Close();
                }
                return documentIssue;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion Create Document Issue

        #region Get Designation for member
        public static object GetAllDesignations(object param)
        {
            try
            {
                IssueDocumentContext context = new IssueDocumentContext();

                var query = (from designations in context.mMemberDesignation
                             select designations).ToList();
                return query;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Get Members Name With UnReturned Docs
        public static object GetMembersWithUnReturnedDocs(object param)
        {
            try
            {
                IssueDocumentContext context = new IssueDocumentContext();
                VsDocumentIssue list = new VsDocumentIssue();
                var query = (from members in context.mMembers
                             join membersname in context.VsDocumentIssue
                             on members.MemberCode equals membersname.SubscriberId
                             where membersname.IsReturned == false && membersname.IsMember == true
                             select members).Distinct().ToList();

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get staff Name with UnReturned Docs
        public static object GetStaffWithUnReturnedDocs(object param)
        {
            try
            {
                IssueDocumentContext context = new IssueDocumentContext();
                VsDocumentIssue list = new VsDocumentIssue();
                var query = (from staff in context.mStaff
                             join vsDocs in context.VsDocumentIssue
                             on staff.StaffID equals vsDocs.SubscriberId
                             where vsDocs.IsReturned == false && vsDocs.IsMember == false
                             select staff).Distinct().ToList();

                return query;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get documents data List which are Un returned for member
        public static object GetUnReturnedDocumentsForMember(object param)
        {
            try
            {
                IssueDocumentContext context = new IssueDocumentContext();
                int SubScriberDesig = Convert.ToInt16(param);
                var query = (from members in context.VsDocumentIssue
                             where members.SubscriberId == SubScriberDesig
                             && members.IsReturned == false
                             && members.IsMember == true
                             select members).ToList();
                foreach (var item in query)
                {
                    item.DisplayedDOI = Convert.ToString(item.DateOfIssue.ToShortDateString());
                }
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion Get documents List which are Un returned

        #region Get documents data List which are Un returned for Staff
        public static object GetUnReturnedDocumentsForStaff(object param)
        {
            try
            {
                IssueDocumentContext context = new IssueDocumentContext();
                int SubScriberDesig = Convert.ToInt16(param);
                var query = (from members in context.VsDocumentIssue
                             where members.SubscriberId == SubScriberDesig
                             && members.IsReturned == false
                             && members.IsMember == false
                             select members).ToList();
                foreach (var item in query)
                {
                    item.DisplayedDOI = Convert.ToString(item.DateOfIssue.ToShortDateString());
                }
                return query;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update Returned Documents for members
        public static object UpdateReturnedDocumentsForMembers(object param)
        {
            try
            {
                IssueDocumentContext context = new IssueDocumentContext();
                int IssuedId = Convert.ToInt16(param);
                var data = (from a in context.VsDocumentIssue
                            where a.IssueID == IssuedId
                            select a).SingleOrDefault();
                data.IsReturned = true;
                data.DateOfReturned = DateTime.Now;
                data.TimeOfReturn = DateTime.Now.ToShortTimeString();
                context.Entry(data).State = EntityState.Modified;
                context.SaveChanges();
                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Update Returned Documents for staff
        public static object UpdateReturnedDocumentsForStaff(object param)
        {
            try
            {
                IssueDocumentContext context = new IssueDocumentContext();
                int IssuedId = Convert.ToInt16(param);
                var data = (from a in context.VsDocumentIssue
                            where a.IssueID == IssuedId
                            select a).SingleOrDefault();
                data.IsReturned = true;
                data.DateOfReturned = DateTime.Now;
                context.Entry(data).State = EntityState.Modified;
                context.SaveChanges();
                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Get report with criteria
        public static object GetReportWIthCriteria(object param)
        {
            try
            {
                VsDocumentIssue data = param as VsDocumentIssue;

                IssueDocumentContext context = new IssueDocumentContext();
                
                if (data.IssueStartRange != null && data.IssueEndRange != null && data.ReturnStartRange == null && data.ReturnEndRange == null)
                {
                    var ResultList = (from vsDoc in context.VsDocumentIssue
                                      where vsDoc.SubscriberDesignationId == data.SubscriberDesignationId
                                      && vsDoc.SubscriberId == data.SubscriberId
                                      && EntityFunctions.TruncateTime(vsDoc.DateOfIssue) >= EntityFunctions.TruncateTime(data.IssueStartRange)
                                      && EntityFunctions.TruncateTime(vsDoc.DateOfIssue) <= EntityFunctions.TruncateTime(data.IssueEndRange)
                                      orderby vsDoc.IssueID descending
                                      select vsDoc).ToList();
                    return ResultList;
                }
                if (data.ReturnStartRange != null && data.ReturnEndRange != null && data.IssueStartRange == null && data.IssueEndRange == null)
                {
                    var ResultList = (from vsDoc in context.VsDocumentIssue
                                      where vsDoc.SubscriberDesignationId == data.SubscriberDesignationId
                                      && vsDoc.SubscriberId == data.SubscriberId
                                      && EntityFunctions.TruncateTime(vsDoc.DateOfReturned) >= EntityFunctions.TruncateTime(data.ReturnStartRange)
                                      && EntityFunctions.TruncateTime(vsDoc.DateOfReturned) <= EntityFunctions.TruncateTime(data.ReturnEndRange)
                                      orderby vsDoc.IssueID descending
                                      select vsDoc).ToList();
                    return ResultList;
                }
                if (data.ReturnStartRange != null && data.ReturnEndRange != null && data.IssueStartRange != null && data.IssueEndRange != null)
                {
                    var ReturnAllList = (from vsDoc in context.VsDocumentIssue
                                         where vsDoc.SubscriberDesignationId == data.SubscriberDesignationId
                                         && vsDoc.SubscriberId == data.SubscriberId
                                         && EntityFunctions.TruncateTime(vsDoc.DateOfIssue) >= EntityFunctions.TruncateTime(data.IssueStartRange)
                                         && EntityFunctions.TruncateTime(vsDoc.DateOfIssue) <= EntityFunctions.TruncateTime(data.IssueEndRange)
                                         && EntityFunctions.TruncateTime(vsDoc.DateOfReturned) >= EntityFunctions.TruncateTime(data.ReturnStartRange)
                                         && EntityFunctions.TruncateTime(vsDoc.DateOfReturned) <= EntityFunctions.TruncateTime(data.ReturnEndRange)
                                         orderby vsDoc.IssueID descending
                                         select vsDoc).ToList();
                    return ReturnAllList;
                }
                if (data.ReturnStartRange == null && data.ReturnEndRange == null && data.IssueStartRange == null && data.IssueEndRange == null)
                {
                    var ReturnAllList = (from vsDoc in context.VsDocumentIssue
                                         where vsDoc.SubscriberDesignationId == data.SubscriberDesignationId
                                         && vsDoc.SubscriberId == data.SubscriberId                                        
                                         orderby vsDoc.IssueID descending
                                         select vsDoc).ToList();
                    return ReturnAllList;
                }
                //var DataReport = (from vsDoc in context.VsDocumentIssue
                //                  where vsDoc.SubscriberDesignationId == data.SubscriberDesignationId
                //                  && vsDoc.SubscriberId == data.SubscriberId
                //                  select vsDoc);
                //if (data.IssueStartRange != null && data.IssueEndRange != null)
                //{
                //    DataReport = DataReport.Where(x => x.IssueStartRange <= data.DateOfIssue.Date && x.IssueEndRange >= data.DateOfIssue.Date);
                //}
                //if (data.ReturnStartRange != null && data.ReturnEndRange != null)
                //{
                //    DataReport = DataReport.Where(x => x.ReturnStartRange <= data.DateOfReturned.Date && x.ReturnEndRange >= data.DateOfReturned);
                //}
                //List<VsDocumentIssue> ls = DataReport.ToList();
                //return DataReport.ToList();
                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
    }

}