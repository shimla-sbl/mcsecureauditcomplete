﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.LibraryVS;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.Library
{
    internal class LibraryBookManagementContext : DBBase<LibraryBookManagementContext>
    {
        public LibraryBookManagementContext()
            : base("eVidhan"){}

        #region Library refferences

        public virtual DbSet<VsLibraryBooks> VsLibraryBooks { get; set; }
        public virtual DbSet<DocumentIssueViewModel> DocumentIssueViewModel { get; set; }

        #endregion Library refferences

        #region Service Parameters

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "AddBookInfo":
                    {
                        return AddBookInfo(param.Parameter);
                    }
                case "GetBookInfoByIdForUpdate":
                    {
                        return GetBookInfoByIdForUpdate(param.Parameter);
                    }
                case "SearchBooksWithAccessionNo":
                    {
                        return SearchBooksWithAccessionNo(param.Parameter);
                    }
                case "SearchBookWithLocation":
                    {
                        return SearchBookWithLocation(param.Parameter);
                    }
                case "AccessionNumberCheck":
                    {
                        return AccessionNumberCheck(param.Parameter);
                    }
                case "SearchBookForPublicUser":
                    {
                        return SearchBookForPublicUser(param.Parameter);
                    }
                case "getAccessionNumberList":
                    {
                        return getAccessionNumberList();
                    }
                case "updateWriteOff":
                    {
                        return updateWriteOff(param.Parameter);
                    }
                case "verifiedBook":
                        {
                            return verifiedBook(param.Parameter);
                        }
                case "SearchBookForReport":
                        {
                            return SearchBookForReport(param.Parameter);
                        }
                case "UpdateBookInfoForDifferentAccNo":
                        {
                            return UpdateBookInfoForDifferentAccNo(param.Parameter);
                        }      
            }
            return null;
        }

        #endregion Service Parameters

        #region Add and Update book info

        public static object AddBookInfo(object param)
        {
            VsLibraryBooks BooksInfo = param as VsLibraryBooks;
            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBooks
                                   where a.AccessionNumber == BooksInfo.AccessionNumber
                                   select a).FirstOrDefault();

                    if (content == null)//Add logic
                    {   
                        BooksInfo.VerficationYear="n/a";
                        context.VsLibraryBooks.Add(BooksInfo);
                        context.SaveChanges();
                        context.Close();
                    }
                    else    //update Logic
                    {
                        content.AccessionNumber = BooksInfo.AccessionNumber;
                        content.DateOfEntry = BooksInfo.DateOfEntry;
                        content.Almirah = BooksInfo.Almirah;
                        content.AuthorName = BooksInfo.AuthorName;
                        //content.Available = BooksInfo.Available;
                        content.BillDate = BooksInfo.BillDate;
                        content.BillNumber = BooksInfo.BillNumber;
                        content.BookNumber = BooksInfo.BookNumber;
                        content.Building = BooksInfo.Building;
                        content.CallNumber = BooksInfo.CallNumber;
                        content.Cost = BooksInfo.Cost;
                        //content.Damaged = BooksInfo.Damaged;
                        content.DateOfEntry = BooksInfo.DateOfEntry;
                        content.Edition = BooksInfo.Edition;
                        content.EditorName = BooksInfo.EditorName;
                        content.Floor = BooksInfo.Floor;
                        //content.Issued = BooksInfo.Issued;
                        content.LanguageOption = BooksInfo.LanguageOption;
                        content.Pages = BooksInfo.Pages;
                        content.PODate = BooksInfo.PODate;
                        content.PONumber = BooksInfo.PONumber;
                        content.PublicationPlace = BooksInfo.PublicationPlace;
                        content.PublicationYear = BooksInfo.PublicationYear;
                        content.PublisherName = BooksInfo.PublisherName;
                        content.Rack = BooksInfo.Rack;
                        content.Remarks = BooksInfo.Remarks;
                        //content.SerialNumber = BooksInfo.SerialNumber;
                        content.SourceOrSuplierName = BooksInfo.SourceOrSuplierName;
                        content.SupplierPlace = BooksInfo.SupplierPlace;
                        content.TitleOfBook = BooksInfo.TitleOfBook;
                        content.Volume = BooksInfo.Volume;
                        content.Status = BooksInfo.Status;
                        context.VsLibraryBooks.Attach(content);
                        context.Entry(content).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                    return BooksInfo;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        // incase the accesion no gets changed so....different function for that case
        public static object UpdateBookInfoForDifferentAccNo(object param)
        {
            try
            {
                     return null;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        #endregion Add and Update book info

        #region To update the book record with accession no

        public static object GetBookInfoByIdForUpdate(object param)
        {
            string BooksInfo = param as string;
            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBooks
                                   where a.AccessionNumber == BooksInfo
                                   select a).FirstOrDefault();

                    return content;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion To update the book record with accession no

        #region To check accession no. exit or not

        public static object AccessionNumberCheck(object param)
        {
            string AccessionNumber = param as string;

            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBooks
                                   where a.AccessionNumber == AccessionNumber
                                   select a).FirstOrDefault();
                    return content;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public static object getAccessionNumberList()
        {
            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                var list = (from _list in context.VsLibraryBooks
                            select new AccessionNumberList
                            {
                                accessionNumber = _list.AccessionNumber,
                                BookName = _list.TitleOfBook
                            }).ToList();
                return list;
            }
        }

        public static object updateWriteOff(object param)
        {
            VsLibraryBooks book = param as VsLibraryBooks;
            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                var result = (from list in context.VsLibraryBooks
                              where list.AccessionNumber == book.AccessionNumber
                              select list).FirstOrDefault();
                result.WriteOff = book.WriteOff;
                result.WriteOffDate = book.WriteOffDate;
                result.WriteOffRemarks = book.WriteOffRemarks;
                result.WriteOffValue = book.WriteOffValue;
                //result.WriteOffDate = DateTime.Now.ToString();
                result.Rack = "NA";
                result.Floor = "NA";
                result.Almirah = "NA";
                result.Building = "NA";
                result.Status = "WrittenOff";
                context.Entry(result).State = EntityState.Modified;

                context.SaveChanges();
                context.Close();
                return null;
            }
        }

        #endregion To check accession no. exit or not

        public static object SearchBooksWithAccessionNo(object param)
        {
            string accessNo = param as string;
            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                var result = (from list in context.VsLibraryBooks
                              where list.AccessionNumber == accessNo
                              select list).ToList();
                return result;
            }
        }
        #region list for Book report
        public static object SearchBookForReport(object param)
        {
            VsLibraryBooks data = param as VsLibraryBooks;
            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                //var result = (from list in context.VsLibraryBooks
                //              where list.Status==content.Status
                //              select list).ToList();
                //return result;

                var content = (from a in context.VsLibraryBooks select a);
                if (data.TitleOfBook != null)
                {
                    content = content.Where(q => q.TitleOfBook.Contains(data.TitleOfBook));
                }
                if (data.AuthorName != null)
                {
                    content = content.Where(q => q.AuthorName.Contains(data.AuthorName));
                }
                if (data.EditorName != null)
                {
                    content = content.Where(q => q.EditorName.Contains(data.EditorName));
                }
                if (data.Edition != null)
                {
                    content = content.Where(q => q.Edition.Contains(data.Edition));
                }
                if (data.Volume != null)
                {
                    content = content.Where(q => q.Volume.Contains(data.Volume));
                }
                if (data.PublisherName != null)
                {
                    content = content.Where(q => q.PublisherName.Contains(data.PublisherName));
                }
                if (data.PublicationPlace != null)
                {
                    content = content.Where(q => q.PublicationPlace.Contains(data.PublicationPlace));
                }
                if (data.PublicationYear != null)
                {
                    content = content.Where(q => q.PublicationYear.Contains(data.PublicationYear));
                }
                if (data.Status != null)
                {
                    content = content.Where(q => q.Status.Contains(data.Status));
                }
                if (data.DateOfEntry != null)
                {
                    content = content.Where(q => q.Status.Contains(data.DateOfEntry));
                }
                if (data.SourceOrSuplierName != null)
                {
                    content = content.Where(q => q.Status.Contains(data.SourceOrSuplierName));
                }
                if (data.SupplierPlace != null)
                {
                    content = content.Where(q => q.Status.Contains(data.SupplierPlace));
                }
                if (data.CallNumber != null)
                {
                    content = content.Where(q => q.Status.Contains(data.CallNumber));
                }
                if (data.BillNumber != null)
                {
                    content = content.Where(q => q.Status.Contains(data.BillNumber));
                }
                if (data.BillDate != null)
                {
                    content = content.Where(q => q.Status.Contains(data.BillDate));
                }
                if (data.Cost != null)
                {
                    content = content.Where(q => q.Status.Contains(data.Cost));
                }
                if (data.PONumber != null)
                {
                    content = content.Where(q => q.Status.Contains(data.PONumber));
                }
                if (data.PODate != null)
                {
                    content = content.Where(q => q.Status.Contains(data.PODate));
                }
                if (data.Remarks != null)
                {
                    content = content.Where(q => q.Status.Contains(data.Remarks));
                }
                if (data.Floor != null)
                {
                    content = content.Where(q => q.Status.Contains(data.Floor));
                }
                if (data.Building != null)
                {
                    content = content.Where(q => q.Status.Contains(data.Building));
                }
                if (data.Almirah != null)
                {
                    content = content.Where(q => q.Status.Contains(data.Almirah));
                }
                if (data.Rack != null)
                {
                    content = content.Where(q => q.Status.Contains(data.Rack));
                }

                //List<VsLibraryBooks> ls = content.ToList();
                // return ls.OrderBy(a =>a.AuthorName).Take(500);
                List<VsLibraryBooks> ls = content.OrderBy(a => a.AuthorName).Take(500).ToList();
                return ls;


            }
        }
        #endregion

        public static object verifiedBook(object param)
        {
            string accessno = param as string;
            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                var result = (from list in context.VsLibraryBooks
                              where list.AccessionNumber == accessno
                              select list).FirstOrDefault();
                result.Verfication = "1";
                result.VerficationYear = Convert.ToString(DateTime.Now.Year);
                context.Entry(result).State = EntityState.Modified;
                context.SaveChanges();
                context.Close();
                return null;
            }
        }
        public static object SearchBookWithLocation(object param)
        {
            string[] arr = param as string[];
            string building = arr[0];
            string floor = arr[1];
            string almirah = arr[2];
            string rack = arr[3];
           // List<VsLibraryBooks> result = null;
            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                //if (string.IsNullOrEmpty(rack))
                //{
                //    result = (from list in context.VsLibraryBooks
                //              where list.Building == building
                //              && list.Floor == floor
                //              && list.Almirah == almirah
                            

                //              select list).ToList();
                //}
                //else
                //{
                //    result = (from list in context.VsLibraryBooks
                //              where list.Building == building
                //              && list.Floor == floor
                //              && list.Almirah == almirah
                //              && list.Rack == rack

                //              select list).ToList();
                //}
                //return result;
                var content = (from a in context.VsLibraryBooks select a);
                if (building != "")
                {
                    content = content.Where(q => q.Building.Contains(building));
                }
                if (floor != "")
                {
                    content = content.Where(q => q.Floor.Contains(floor));
                }
                if (almirah != "")
                {
                    content = content.Where(q => q.Almirah.Contains(almirah));
                }
                if (rack != "")
                {
                    content = content.Where(q => q.Rack.Contains(rack));
                }                
                List<VsLibraryBooks> ls = content.ToList();
                return ls;

            }
        }

        public static object SearchBookForPublicUser(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');
            string TitleOfBook = Result[0];
            string AuthorName = Result[1];
            string EditorName = Result[2];
            string Edition = Result[3];
            string Volume = Result[4];
            string PublisherName = Result[5];
            string PublicationPlace = Result[6];
            string PublicationYear = Result[7];
            string PublicationStatus = Result[8]; 

            using (LibraryBookManagementContext context = new LibraryBookManagementContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBooks select a);
                    if (TitleOfBook != " ") {
                        content = content.Where(q => q.TitleOfBook.Contains(TitleOfBook));
                    }
                    if (AuthorName != " ")
                    {
                        content = content.Where(q => q.AuthorName.Contains(AuthorName));
                    }
                    if (EditorName != " ")
                    {
                        content = content.Where(q => q.EditorName.Contains(EditorName));
                    }
                    if (Edition != " ")
                    {
                        content = content.Where(q => q.Edition.Contains(Edition));
                    }
                    if (Volume != " ")
                    {
                        content = content.Where(q => q.Volume.Contains(Volume));
                    }
                    if (PublisherName != " ")
                    {
                        content = content.Where(q => q.PublisherName.Contains(PublisherName));
                    }
                    if (PublicationPlace != " ")
                    {
                        content = content.Where(m => m.PublicationPlace.Contains(PublicationPlace));
                    }
                    if (PublicationYear != " ")
                    {
                        content = content.Where(q => q.PublicationYear.Contains(PublicationYear));
                    }
                    if (PublicationStatus != "--Select--")
                    {
                        content = content.Where(q => q.Status.Contains(PublicationStatus));
                    }
                    //List<VsLibraryBooks> ls = content.ToList();
                   // return ls.OrderBy(a =>a.AuthorName).Take(500);
                    List<VsLibraryBooks> ls = content.OrderBy(a => a.AuthorName).Take(500).ToList();
                    return ls;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}