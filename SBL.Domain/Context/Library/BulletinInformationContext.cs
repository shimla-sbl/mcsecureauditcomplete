﻿using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.LibraryVS;
using SBL.DomainModel.Models.Session;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Library
{
    class BulletinInformationContext : DBBase<BulletinInformationContext>
    {
        public BulletinInformationContext() : base("eVidhan") { }

        #region Library refferences
        public virtual DbSet<VsLibraryBulletin> VsLibraryBulletin { get; set; }
        public virtual DbSet<mSession> mSession { get; set; }
        public virtual DbSet<mAssembly> mAssembly { get; set; }
        #endregion Library refferences

        #region Service Parameters
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "SessionCheck":
                    {
                        return SessionCheck(param.Parameter);
                    }
                case "SaveBulletinInfo":
                    {
                        return SaveBulletinInfo(param.Parameter);
                    }
                case "AssemblyAndSession":
                    {
                        return AssemblyAndSession(param.Parameter);
                    }
                case "SearchBulletinForUpdate":
                    {
                        return SearchBulletinForUpdate(param.Parameter);
                    }
                case "SearchBulletin":
                    {
                        return SearchBulletin(param.Parameter);
                    }
                case "SearchLocation":
                    {
                        return SearchLocation(param.Parameter);
                    }
                case "UpdateWithSerialNo":
                    {
                        return UpdateWithSerialNo(param.Parameter);
                    }
            }
            return null;
        }

       
        #endregion Service Parameters

        #region Sending Serailno Info For Update
        public static object UpdateWithSerialNo(object param)
        {
            var SerialNoForUpdation = Convert.ToInt16(param);
            //VsLibraryBulletin LibraryBulletin = param as VsLibraryBulletin;
            using (BulletinInformationContext context = new BulletinInformationContext())
            {
                try
                {
                    var Bulletin = (from a in context.VsLibraryBulletin
                                    where a.SerialNumber == SerialNoForUpdation
                                    select a).SingleOrDefault();
                    return Bulletin;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion


        #region To fetch Session and assembly in system
        public static object AssemblyAndSession(object param)
        {
            //VsLibraryBulletin LibraryBulletin = param as VsLibraryBulletin;
            using (BulletinInformationContext context = new BulletinInformationContext())
            {
                try
                {
                    //var content = (from a in context.VsLibraryBulletin
                    //               where a.Session == LibraryBulletin.Session
                    //               select a).FirstOrDefault();

                    return null;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Search Bulletin for Updation
        public static object SearchBulletinForUpdate(object param)
        {
            VsLibraryBulletin LibraryBulletin = param as VsLibraryBulletin;
            using (BulletinInformationContext context = new BulletinInformationContext())
            {
                try
                {
                    var Bulletin = (from a in context.VsLibraryBulletin
                                   where a.Session == LibraryBulletin.Session
                                   || a.Assembly==LibraryBulletin.Assembly
                                   || a.Almirah.Contains(LibraryBulletin.Almirah)
                                   || a.Building.Contains(LibraryBulletin.Building)
                                   || a.EndDate.Contains(LibraryBulletin.EndDate)
                                   || a.Floor.Contains(LibraryBulletin.Floor)
                                   || a.StartDate.Contains(LibraryBulletin.StartDate)
                                   || a.Title.Contains(LibraryBulletin.Title)
                                    select a).ToList();

                    return Bulletin;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Search Bulletin for normal display
        public static object SearchBulletin(object param)
        {
            VsLibraryBulletin LibraryBulletin = param as VsLibraryBulletin;
            using (BulletinInformationContext context = new BulletinInformationContext())
            {
                try
                {
                    var Bulletin = (from a in context.VsLibraryBulletin
                                    where a.Session == LibraryBulletin.Session
                                    || a.Title.Contains(LibraryBulletin.Title)
                                    || a.Assembly == LibraryBulletin.Assembly
                                    || a.EndDate.Contains(LibraryBulletin.EndDate)
                                    || a.StartDate.Contains(LibraryBulletin.StartDate)
                                    select a).ToList();
                    return Bulletin;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Search Location of Bulletin
        public static object SearchLocation(object param)
        {
            VsLibraryBulletin LibraryBulletin = param as VsLibraryBulletin;
            using (BulletinInformationContext context = new BulletinInformationContext())
            {
                try
                {
                    var Bulletin = (from a in context.VsLibraryBulletin
                                    where a.Session == LibraryBulletin.Session
                                    || a.Assembly == LibraryBulletin.Assembly
                                    || a.Title.Contains(LibraryBulletin.Title)
                                    || a.EndDate.Contains(LibraryBulletin.EndDate)
                                    || a.StartDate.Contains(LibraryBulletin.StartDate)
                                    || a.Building.Contains(LibraryBulletin.Building)
                                    || a.Floor.Contains(LibraryBulletin.Floor)
                                    || a.Almirah.Contains(LibraryBulletin.Almirah)
                                    select a).ToList();
                    return Bulletin;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Save Bulletin Info
        public static object SaveBulletinInfo(object param)
        {
            VsLibraryBulletin LibraryBulletin = param as VsLibraryBulletin;
            using (BulletinInformationContext context = new BulletinInformationContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBulletin
                                   where a.SerialNumber == LibraryBulletin.SerialNumber
                                   select a).FirstOrDefault();

                    if (content == null)//Add logic
                    {
                        context.VsLibraryBulletin.Add(LibraryBulletin);
                        context.SaveChanges();
                        context.Close();
                    }
                    else    //update Logic
                    {
                        content.Almirah = LibraryBulletin.Almirah;
                        content.Assembly = LibraryBulletin.Assembly;
                        content.Building = LibraryBulletin.Building;
                        content.EndDate = LibraryBulletin.EndDate;
                        content.Floor = LibraryBulletin.Floor;
                        content.Rack = LibraryBulletin.Rack;
                        content.Session = LibraryBulletin.Session;
                        content.StartDate = LibraryBulletin.StartDate;
                        content.Title = LibraryBulletin.Title;

                        context.VsLibraryBulletin.Attach(content);
                        context.Entry(content).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                    return LibraryBulletin;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion

        #region Session Check
        public static object SessionCheck(object param)
        {
            VsLibraryBulletin LibraryBulletin = param as VsLibraryBulletin;
            using (BulletinInformationContext context = new BulletinInformationContext())
            {
                try
                {
                    var content = (from a in context.VsLibraryBulletin
                                   where a.Session == LibraryBulletin.Session  
                                   && a.SerialNumber!=LibraryBulletin.SerialNumber
                                   select a).FirstOrDefault();

                    return content;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
        #endregion
    }
}
