﻿using SBL.DAL;
using SBL.DomainModel.Models.Library;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Library
{
    class FloorVersionContext : DBBase<FloorVersionContext>
    {

        public FloorVersionContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }


        public DbSet<tFloorVersion> tFloorVersion { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateFloorVersion":
                    {
                        return CreateFloorVersion(param.Parameter);
                    }
                case "UpdateFloorVersion":
                    {
                        return UpdateFloorVersion(param.Parameter);
                    }
                case "DeleteFloorVersion":
                    {
                        return DeleteFloorVersion(param.Parameter);
                    }
                case "GetAllFloorVersionDetails":
                    {
                        return GetAllFloorVersionDetails(param.Parameter);
                    }
                case "GetFloorVersionById":
                    {
                        return GetFloorVersionById(param.Parameter);
                    }

            }

            return null;
        }

        public static object CreateFloorVersion(object param)
        {
            try
            {
                using (FloorVersionContext db = new FloorVersionContext())
                {
                    tFloorVersion model = param as tFloorVersion;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tFloorVersion.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateFloorVersion(object param)
        {
            try
            {
                using (FloorVersionContext db = new FloorVersionContext())
                {
                    tFloorVersion model = param as tFloorVersion;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tFloorVersion.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteFloorVersion(object param)
        {
            try
            {
                using (FloorVersionContext db = new FloorVersionContext())
                {
                    tFloorVersion parameter = param as tFloorVersion;
                    tFloorVersion stateToRemove = db.tFloorVersion.SingleOrDefault(a => a.FloorVersionId == parameter.FloorVersionId);
                    db.tFloorVersion.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllFloorVersionDetails(object param)
        {

            FloorVersionContext db = new FloorVersionContext();

            var data = (from PC in db.tFloorVersion.OrderByDescending(a => a.FloorVersionId)
                        select PC).ToList();


            return data;
        }




        public static object GetFloorVersionById(object param)
        {
            tFloorVersion parameter = param as tFloorVersion;

            FloorVersionContext db = new FloorVersionContext();

            var query = db.tFloorVersion.SingleOrDefault(a => a.FloorVersionId == parameter.FloorVersionId);

            return query;
        }


    }
}
