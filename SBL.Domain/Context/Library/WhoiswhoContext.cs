﻿using SBL.DAL;
using SBL.DomainModel.Models.Library;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Library
{
    class WhoiswhoContext : DBBase<WhoiswhoContext>
    {


        public WhoiswhoContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }


        public DbSet<tWhoiswho> tWhoiswho { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateWhoiswho":
                    {
                        return CreateWhoiswho(param.Parameter);
                    }
                case "UpdateWhoiswho":
                    {
                        return UpdateWhoiswho(param.Parameter);
                    }
                case "DeleteWhoiswho":
                    {
                        return DeleteWhoiswho(param.Parameter);
                    }
                case "GetAllWhoiswhoDetails":
                    {
                        return GetAllWhoiswhoDetails(param.Parameter);
                    }
                case "GetWhoiswhoById":
                    {
                        return GetWhoiswhoById(param.Parameter);
                    }

            }

            return null;
        }

        public static object CreateWhoiswho(object param)
        {
            try
            {
                using (WhoiswhoContext db = new WhoiswhoContext())
                {
                    tWhoiswho model = param as tWhoiswho;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tWhoiswho.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateWhoiswho(object param)
        {
            try
            {
                using (WhoiswhoContext db = new WhoiswhoContext())
                {
                    tWhoiswho model = param as tWhoiswho;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tWhoiswho.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteWhoiswho(object param)
        {
            try
            {
                using (WhoiswhoContext db = new WhoiswhoContext())
                {
                    tWhoiswho parameter = param as tWhoiswho;
                    tWhoiswho stateToRemove = db.tWhoiswho.SingleOrDefault(a => a.WhoiswhoId == parameter.WhoiswhoId);
                    db.tWhoiswho.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllWhoiswhoDetails(object param)
        {

            WhoiswhoContext db = new WhoiswhoContext();

            var data = (from PC in db.tWhoiswho.OrderByDescending(a => a.WhoiswhoId)
                        select PC).ToList();


            return data;
        }




        public static object GetWhoiswhoById(object param)
        {
            tWhoiswho parameter = param as tWhoiswho;

            WhoiswhoContext db = new WhoiswhoContext();

            var query = db.tWhoiswho.SingleOrDefault(a => a.WhoiswhoId == parameter.WhoiswhoId);

            return query;
        }


    }
}
