﻿using SBL.DAL;
using SBL.DomainModel.Models.AssemblyFileSystem;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.TypeofDocuments
{
    public class AssemblyTypeofDocumentsContext : DBBase<AssemblyTypeofDocumentsContext>
    {

        public AssemblyTypeofDocumentsContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        
        public DbSet<mAssemblyTypeofDocuments> mAssemblyTypeofDocuments { get; set; }
        
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateDoc": { CreateDoc(param.Parameter); break; }

                case "UpdateDoc": { return UpdateDoc(param.Parameter); }
                case "DeleteDoc": { return DeleteDoc(param.Parameter); }
                case "GetAllDoc": { return GetAllDoc(); }
                case "GetDocById": { return GetDocById(param.Parameter); }
                
            }
            return null;
        }

        static void CreateDoc(object param)
        {
            try
            {
                using (AssemblyTypeofDocumentsContext db = new AssemblyTypeofDocumentsContext())
                {
                    mAssemblyTypeofDocuments model = param as mAssemblyTypeofDocuments;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mAssemblyTypeofDocuments.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateDoc(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (AssemblyTypeofDocumentsContext db = new AssemblyTypeofDocumentsContext())
            {
                mAssemblyTypeofDocuments model = param as mAssemblyTypeofDocuments;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mAssemblyTypeofDocuments.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllDoc();
        }

        static object DeleteDoc(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (AssemblyTypeofDocumentsContext db = new AssemblyTypeofDocumentsContext())
            {
                mAssemblyTypeofDocuments parameter = param as mAssemblyTypeofDocuments;
                mAssemblyTypeofDocuments stateToRemove = db.mAssemblyTypeofDocuments.SingleOrDefault(a => a.TypeofDocumentId == parameter.TypeofDocumentId);
                if (stateToRemove != null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllDoc();
        }

        static List<mAssemblyTypeofDocuments> GetAllDoc()
        {
            AssemblyTypeofDocumentsContext db = new AssemblyTypeofDocumentsContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.mAssemblyTypeofDocuments
                         orderby a.TypeofDocumentId descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mAssemblyTypeofDocuments GetDocById(object param)
        {
            mAssemblyTypeofDocuments parameter = param as mAssemblyTypeofDocuments;
            AssemblyTypeofDocumentsContext db = new AssemblyTypeofDocumentsContext();
            var query = db.mAssemblyTypeofDocuments.SingleOrDefault(a => a.TypeofDocumentId == parameter.TypeofDocumentId);
            return query;
        }


    }
}
