﻿using SBL.DAL;
using SBL.DomainModel.Models.Forms;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.TypeofDocuments
{
    public class FormTypeofDocumentsContext : DBBase<FormTypeofDocumentsContext>
    {
        public FormTypeofDocumentsContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<tFormsTypeofDocuments> tFormsTypeofDocuments { get; set; }
        
        
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateDoc": { CreateDoc(param.Parameter); break; }

                case "UpdateDoc": { return UpdateDoc(param.Parameter); }
                case "DeleteDoc": { return DeleteDoc(param.Parameter); }
                case "GetAllDoc": { return GetAllDoc(); }
                case "GetDocById": { return GetDocById(param.Parameter); }
                
            }
            return null;
        }





        static void CreateDoc(object param)
        {
            try
            {
                using (FormTypeofDocumentsContext db = new FormTypeofDocumentsContext())
                {
                    tFormsTypeofDocuments model = param as tFormsTypeofDocuments;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.tFormsTypeofDocuments.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateDoc(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (FormTypeofDocumentsContext db = new FormTypeofDocumentsContext())
            {
                tFormsTypeofDocuments model = param as tFormsTypeofDocuments;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.tFormsTypeofDocuments.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllDoc();
        }

        static object DeleteDoc(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (FormTypeofDocumentsContext db = new FormTypeofDocumentsContext())
            {
                tFormsTypeofDocuments parameter = param as tFormsTypeofDocuments;
                tFormsTypeofDocuments stateToRemove = db.tFormsTypeofDocuments.SingleOrDefault(a => a.TypeofFormId == parameter.TypeofFormId);
                if (stateToRemove != null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllDoc();
        }

        static List<tFormsTypeofDocuments> GetAllDoc()
        {
            FormTypeofDocumentsContext db = new FormTypeofDocumentsContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.tFormsTypeofDocuments
                         orderby a.TypeofFormId descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static tFormsTypeofDocuments GetDocById(object param)
        {
            tFormsTypeofDocuments parameter = param as tFormsTypeofDocuments;
            FormTypeofDocumentsContext db = new FormTypeofDocumentsContext();
            var query = db.tFormsTypeofDocuments.SingleOrDefault(a => a.TypeofFormId == parameter.TypeofFormId);
            return query;
        }


    }
}
