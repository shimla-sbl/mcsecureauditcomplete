﻿using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.salaryhead;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;


namespace SBL.Domain.Context.DeputyOrJoint
{
    public class DeputyOrJointcontext : DBBase<DeputyOrJointcontext>
    {
        public DeputyOrJointcontext()
            : base("eVidhan")
        {
        }   public virtual DbSet<mMemberAssembly> mMemberAssembly { get; set; }

        public virtual DbSet<mMember> mMember { get; set; }

        public virtual DbSet<membersMonthlySalaryDetails> membersMonthlySalaryDetails { get; set; }

        public virtual DbSet<mAssembly> mAssembly { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "getSalaryBillList": { return getSalaryBillList(param.Parameter); }
                case "changeBillStatus": { return changeBillStatus(param.Parameter); }
                case "getBillInfo": { return getBillInfo(param.Parameter); }
            } return null;
        }

        public static object getSalaryBillList(object param)
        {
            int[] arr = param as int[];
            int monthID = arr[0];
            int yearID = arr[1];
            int status = arr[2];
            int assemblyid = arr[3];
            int ispart = arr[4];
            List<membersMonthlySalaryDetails> result = null;
            using (DeputyOrJointcontext context = new DeputyOrJointcontext())
            {
                switch (status)
                {
                    case 1:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.AssemblyId == assemblyid && list.Ispart == ispart
                                      && list.JDCStatus == 1
                                      select list
                                   ).ToList();
                            break;
                        }
                    case 2:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                      && list.AssemblyId == assemblyid && list.Ispart == ispart
                                      && list.JDCStatus == 2 || list.SOStatus == 2
                                      select list
                                  ).ToList();
                            break;
                        }
                    case 3:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                       && list.AssemblyId == assemblyid && list.Ispart == ispart
                                      && list.SOStatus == 0
                                      select list
                                     ).ToList();
                            break;
                        }
                    case 0:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                       && list.AssemblyId == assemblyid && list.Ispart == ispart
                                      && list.JDCStatus == 0
                                      select list
                                ).ToList();
                            break;
                        }
                    case 4:
                        {
                            result = (from list in context.membersMonthlySalaryDetails
                                      where list.month == monthID && list.year == yearID
                                       && list.AssemblyId == assemblyid && list.Ispart == ispart
                                      select list
                                 ).ToList();
                            break;
                        }
                }

                List<salaryBillInfo> _sbList = new List<salaryBillInfo>();
                foreach (var item in result)
                {
                    int mId = item.membersid;
                    salaryBillInfo sbi = new salaryBillInfo();
                    sbi.AutoID = item.autoid;
                    sbi.memberName = (from members in context.mMember
                                      where members.MemberCode == mId
                                      select members.Prefix + " " + members.Name + " ").FirstOrDefault();
                    //var assemblyID = (from assembly in context.mMemberAssembly
                    //                  join mAssemblyOrder in context.mAssembly
                    //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                    //                  where assembly.MemberID == mId
                    //                  orderby mAssemblyOrder.AssemblyID descending
                    //                  select assembly.AssemblyID
                    //                   ).FirstOrDefault();
                    sbi.Designation = (from memberAssemblies in context.mMemberAssembly
                                       where memberAssemblies.AssemblyID == assemblyid
                                       && memberAssemblies.MemberID == mId
                                       select memberAssemblies.Designation
                                   ).FirstOrDefault();
                    sbi.memberID = mId;
                    sbi.netSalary = item.grandtotal;
                    sbi.totalAllowances = item.totalAllowances;
                    sbi.totalDeductions = item.totalAGDeductionsA;
                    if (item.SOStatus == 0)
                    {
                        sbi.status = 0;
                    }
                    else
                    {
                        if (item.SOStatus == 2)
                        {
                            sbi.status = 2;
                        }
                        else
                        {
                            if (item.JDCStatus == 0)
                            {
                                sbi.status = 0;
                            }
                            else
                            {
                                if (item.JDCStatus == 1)
                                {
                                    sbi.status = 1;
                                }
                                else
                                {
                                    sbi.status = 2;
                                }
                            }
                        }
                    }
                    sbi.generateStatus = item.GenerateStatus;
                    sbi.viewRemark = item.remark;
                    _sbList.Add(sbi);
                }
                return _sbList;
            }
        }

        public static object changeBillStatus(object param)
        {
            try
            {
                string[] arr = param as string[];
                int status = Convert.ToInt32(arr[0]);
                int autoID = Convert.ToInt32(arr[1]);
                string user = arr[2];
                using (DeputyOrJointcontext context = new DeputyOrJointcontext())
                {
                    var monthlydetails = (from list in context.membersMonthlySalaryDetails
                                          where list.autoid == autoID
                                          select list).FirstOrDefault();
                    if (user == "so")
                    {
                        if (status != 1)
                        {
                            monthlydetails.SOStatus = status;

                        }
                        else
                        {
                            monthlydetails.SOStatus = status;
                            monthlydetails.JDCStatus = 0;
                        }
                        monthlydetails.SOActionDate = DateTime.Now;
                    }
                    else
                    {
                        if (user.ToLower() == "jdc")
                        {
                            if (status != 1)
                            {
                                monthlydetails.JDCStatus = status;

                            }
                            else
                            {
                                monthlydetails.SOStatus = status;
                                monthlydetails.JDCStatus = status;
                            }
                            monthlydetails.JDCActionDate = DateTime.Now;
                        }
                    }

                    context.Entry(monthlydetails).State = EntityState.Modified;
                    context.SaveChanges();
                    context.Close();
                    return autoID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public static object getBillInfo(object param)
        {
            using (DeputyOrJointcontext context = new DeputyOrJointcontext())
            {
                int billID = Convert.ToInt32(param);
                var result = (from list in context.membersMonthlySalaryDetails
                              join member in context.mMember
                              on list.membersid equals member.MemberCode
                              where list.autoid == billID
                              select new
                              {
                                  salary = list,
                                  member = member
                              }).FirstOrDefault();

                return result.member.Prefix + " " + result.member.Name + "'s Salary For the Month Of " + (new DateTime(1990, result.salary.month, 1).ToString("MMMM")).ToUpper() + "," + result.salary.year + " Has been " + (result.salary.JDCStatus == 1 ? "Approved" : "Rejected");
            }
        }

    }
}