﻿using SBL.DAL;
using SBL.DomainModel.Models.States;
using SBL.DomainModel.Models.District;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Member;

namespace SBL.Domain.Context.State
{
    public class StateContext : DBBase<StateContext>
    {

        public StateContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mStates> mStates { get; set; }
        public DbSet<mMember> mMember { get; set; }
        public DbSet<DistrictModel> Districts { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateState": { CreateState(param.Parameter); break; }

                case "UpdateState": { return UpdateState(param.Parameter); }
                case "DeleteState": { return DeleteState(param.Parameter); }
                case "GetAllState": { return GetAllState(); }
                case "GetStateById": { return GetStateById(param.Parameter); }
                case "IsStateIdChildExist": { return IsStateIdChildExist(param.Parameter); }
            }
            return null;
        }
        private static object IsStateIdChildExist(object param)
        {
            try
            {
                using (StateContext db = new StateContext())
                {
                    mStates model = (mStates)param;
                    var Res = (from e in db.mMember
                               where (e.StateNameID == model.mStateID)
                               select e).Count();

                    if (Res == 0)
                    {
                        var Mmd = (from e in db.Districts
                                   where (e.StateCode == model.mStateID)
                                   select e).Count();

                        if (Mmd == 0)
                        {

                            return false;


                        }
                    }
                    else
                    {
                        return true;

                    }

                }
                return true;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateState(object param)
        {
            try
            {
                using (StateContext db = new StateContext())
                {
                    mStates model = param as mStates;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mStates.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateState(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (StateContext db = new StateContext())
            {
                mStates model = param as mStates;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mStates.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllState();
        }

        static object DeleteState(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (StateContext db = new StateContext())
            {
                mStates parameter = param as mStates;
                mStates stateToRemove = db.mStates.SingleOrDefault(a => a.mStateID == parameter.mStateID);
                if (stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllState();
        }

        static List<mStates> GetAllState()
        {
            StateContext db = new StateContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.mStates
                         orderby a.mStateID descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mStates GetStateById(object param)
        {
            mStates parameter = param as mStates;
            StateContext db = new StateContext();
            var query = db.mStates.SingleOrDefault(a => a.mStateID == parameter.mStateID);
            return query;
        }



    }
}
