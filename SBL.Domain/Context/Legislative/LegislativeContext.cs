﻿namespace SBL.Domain.Context.Legislative
{
    #region Namespace Reffrences
    using SBL.DAL;
    using SBL.Domain.Context.Diaries;
    using SBL.Domain.Context.Session;
    using SBL.DomainModel;
    using SBL.DomainModel.ComplexModel;
    using SBL.DomainModel.Models.Bill;
    using SBL.DomainModel.Models.Committee;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Diaries;
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Event;
    using SBL.DomainModel.Models.LOB;
    using SBL.DomainModel.Models.Member;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Notice;
    using SBL.DomainModel.Models.PaperLaid;
    using SBL.DomainModel.Models.Question;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Entity;
    using System.Linq;


    #endregion

    public class LegislativeContext : DBBase<LegislativeContext>
    {
        public LegislativeContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<tPaperLaidV> tPaperLaidVS { get; set; }
        public virtual DbSet<tBillRegister> tBillRegisters { get; set; }
        public virtual DbSet<tQuestion> tQuestions { get; set; }
        public virtual DbSet<tSubQuestion> tSubQuestions { get; set; }
        public virtual DbSet<tMemberNotice> tMemberNotices { get; set; }
        public virtual DbSet<tCommittee> tCommittees { get; set; }
        public virtual DbSet<mEvent> mEvents { get; set; }
        public virtual DbSet<mPaperCategoryType> mPaperCategoryTypes { get; set; }
        public virtual DbSet<mMinistry> mMinistry { get; set; }
        public virtual DbSet<mDepartment> mDepartment { get; set; }
        public virtual DbSet<mMember> mMember { get; set; }
        public virtual DbSet<mchangeDepartmentAuditTrail> mchangeDepartmentAuditTrail { get; set; }
        
        //updated by nitin

        public DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }
        public virtual DbSet<AdminLOB> AdminLOB { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                #region Paper Laid Summery For All Paper Category Type

                case "GetStarredQuestionSentCount":
                    {
                        return GetStarredQuestionSentCount();
                    }

                case "GetStarredQuestionPendingCount":
                    {
                        return GetStarredQuestionPendingCount();
                    }

                case "GetStarredQuestionPaperRecievedCount":
                    {
                        return GetStarredQuestionPaperRecievedCount();
                    }

                case "GetStarredQuestionPaperCurrentLobCount":
                    {
                        return GetStarredQuestionPaperCurrentLobCount();
                    }
                case "GetStarredQuestionPaperLaidInHomeCount":
                    {
                        return GetStarredQuestionPaperLaidInHomeCount();
                    }

                case "GetUnStarredQuestionPaperRecievedCount":
                    {
                        return GetUnStarredQuestionPaperRecievedCount();
                    }

                case "GetUnStarredQuestionSentCount":
                    {
                        return GetUnStarredQuestionSentCount();
                    }
                case "GetUnStarredQuestionPendingCount":
                    {
                        return GetUnStarredQuestionPendingCount();
                    }
                case "GetUnStarredQuestionPaperCurrentLobCount":
                    {
                        return GetUnStarredQuestionPaperCurrentLobCount();
                    }
                case "GetUnStarredQuestionPaperLaidInHomeCount":
                    {
                        return GetUnStarredQuestionPaperLaidInHomeCount();
                    }
                case "GetNoticePaperRecievedCount":
                    {
                        return GetNoticePaperRecievedCount();
                    }


                case "GetNoticeSentCount":
                    {
                        return GetNoticeSentCount();
                    }
                case "GetNoticePendingCount":
                    {
                        return GetNoticePendingCount();
                    }

                case "GetNoticePaperCurrentLobCount":
                    {
                        return GetNoticePaperCurrentLobCount();
                    }
                case "GetNoticePaperLaidInHomeCount":
                    {
                        return GetNoticePaperLaidInHomeCount();
                    }
                case "GetBillsPaperRecievedCount":
                    {
                        return GetBillsPaperRecievedCount();
                    }
                case "GetBillsPaperCurrentLobCount":
                    {
                        return GetBillsPaperCurrentLobCount();
                    }
                case "GetBillsPaperLaidInHomeCount":
                    {
                        return GetBillsPaperLaidInHomeCount();
                    }


                case "GetCommittePaperRecievedCount":
                    {
                        return GetCommittePaperRecievedCount();
                    }
                case "GetCommittePaperCurrentLobCount":
                    {
                        return GetCommittePaperCurrentLobCount();
                    }
                case "GetCommittePaperLaidInHomeCount":
                    {
                        return GetCommittePaperLaidInHomeCount();
                    }

                case "GetPaperToLayRecievedCount":
                    {
                        return GetOtherPaperToLayRecievedCount();
                    }

                case "GetPaperToLayCurrentLobCount":
                    {
                        return GetOtherPaperToLayCurrentLobCount();
                    }

                case "GetPaperToLayLaidInHomeCount":
                    {
                        return GetOtherPaperToLayLaidInHomeCount();
                    }

                case "GetStarredQuestionSentList":
                    {
                        return GetStarredQuestionSentList();
                    }

                case "GetStarredQuestionPaperRecievedList":
                    {
                        return GetStarredQuestionPaperRecievedList();
                    }

                case "GetStarredQuestionPaperCurrentLobList":
                    {
                        return GetStarredQuestionPaperCurrentLobList();
                    }
                case "GetStarredQuestionPaperLaidInHomeList":
                    {
                        return GetStarredQuestionPaperLaidInHomeList();
                    }

                case "GetUnStarredQuestionSentList":
                    {
                        return GetUnStarredQuestionSentList();
                    }

                case "GetUnStarredQuestionPaperRecievedList":
                    {
                        return GetUnStarredQuestionPaperRecievedList();
                    }
                case "GetUnStarredQuestionPaperCurrentLobList":
                    {
                        return GetUnStarredQuestionPaperCurrentLobList();
                    }
                case "GetUnStarredQuestionPaperLaidInHomeList":
                    {
                        return GetUnStarredQuestionPaperLaidInHomeList();
                    }

                case "GetNoticeSentList":
                    {
                        return GetNoticeSentList();
                    }
                case "GetNoticePaperRecievedList":
                    {
                        return GetNoticePaperRecievedList();
                    }
                case "GetNoticePaperCurrentLobList":
                    {
                        return GetNoticePaperCurrentLobList();
                    }
                case "GetNoticePaperLaidInHomeList":
                    {
                        return GetNoticePaperLaidInHomeList();
                    }
                case "GetBillsPaperRecievedList":
                    {
                        return GetBillsPaperRecievedList();
                    }
                case "GetBillsPaperCurrentLobList":
                    {
                        return GetBillsPaperCurrentLobList();
                    }
                case "GetBillsPaperLaidInHomeList":
                    {
                        return GetBillsPaperLaidInHomeList();
                    }

                case "GetCommittePaperRecievedList":
                    {
                        return GetCommittePaperRecievedList();
                    }
                case "GetCommittePaperCurrentLobList":
                    {
                        return GetCommittePaperCurrentLobList();
                    }
                case "GetCommittePaperLaidInHomeList":
                    {
                        return GetCommittePaperLaidInHomeList();
                    }

                case "GetPaperToLayRecievedList":
                    {
                        return GetOtherPaperToLayRecievedList();
                    }

                case "GetPaperToLayCurrentLobList":
                    {
                        return GetOtherPaperToLayCurrentLobList();
                    }

                case "GetPaperToLayLaidInHomeList":
                    {
                        return GetOtherPaperToLayLaidInHomeList();
                    }
             
                #endregion

                #region Detailed Paper Laid Summery For A Paper Category Type By Minister/Department/Status
                case "GetPaperLaidSummeryForPaperCategory":
                    {
                        return GetPaperLaidSummeryForPaperCategory(param.Parameter);
                    }
                case "GetCompletePaperLaidDetailsByMinistry":
                    {
                        return GetCompletePaperLaidDetailsByMinistry(param.Parameter);
                    }

                case "GetCompletePaperLaidDetailsByPaperLaid":
                    {
                        return GetCompletePaperLaidDetailsByPaperLaid(param.Parameter);
                    }

                case "GetCompletePaperLaidDetailsByDepartment":
                    {
                        return GetCompletePaperLaidDetailsByDepartment(param.Parameter);
                    }
                case "GetCompletePaperLaidDetailsByStatus":
                    {
                        return GetCompletePaperLaidDetailsByStatus(param.Parameter);
                    }
                #endregion

                #region other methods

                case "GetPapercategoryDetailsById":
                    {
                        return GetPapercategoryDetailsById(param.Parameter);
                    }
                case "GetPaperLaidDetailsById":
                    {
                        return GetPaperLaidDetailsById(param.Parameter);
                    }
                case "GetQuestionDetailsById":
                    {
                        return GetQuestionDetailsById(param.Parameter);
                    }
                case "GetNoticeDetailsById":
                    {
                        return GetNoticeDetailsById(param.Parameter);
                    }
                case "GetDepartmentById":
                    {
                        return GetDepartmentById(param.Parameter);
                    }

                case "GetMinistryById":
                    {
                        return GetMinistryById(param.Parameter);
                    }


                case "UpdateDepartmentwisePriority":
                    {
                        return UpdateDepartmentwisePriority(param.Parameter);
                    }
                case "UpdateMinistrywisePriority":
                    {
                        return UpdateMinistrywisePriority(param.Parameter);
                    }
                case "UpdatePaperLaidPriority":
                    {
                        return UpdatePaperLaidPriority(param.Parameter);
                    }

                #endregion

                #region Added By Nitin
                case "GetUpdatedOtherPaperLaidCounters":
                    {
                        return GetUpdatedOtherPaperLaidCounters(param.Parameter);
                    }
                case "GetOtherPaperPendingToLayListCount":
                    {
                        return GetOtherPaperPendingToLayListCount();
                    }
                case "GetStarredQuestionPaperPendingToLayListCount":
                    {
                        return GetStarredQuestionPaperPendingToLayListCount();
                    }
                case "GetUnStarredQuestionPaperPendingToLayListCount":
                    {
                        return GetUnStarredQuestionPaperPendingToLayListCount();
                    }

                case "GetOtherPaperLaidPaperCategoryType":
                    {
                        return GetOtherPaperLaidPaperCategoryType(param.Parameter);
                    }
                case "GetUpdatedOtherPaperList":
                    {
                        return GetUpdatedOtherPaperList(param.Parameter);
                    }
                case "ShowUpdatedOtherPaperLaidDetailByID":
                    {
                        return ShowUpdatedOtherPaperLaidDetailByID(param.Parameter);
                    }
                case "GetDetailsByPaperLaidId":
                    {
                        return GetDetailsByPaperLaidId(param.Parameter);
                    }
                case "UpdateLOBTempIdByPaperLiadID":
                    {
                        return UpdateLOBTempIdByPaperLiadID(param.Parameter);
                    }

                case "GetNoticePaperPendingToLayListCount":
                    {
                        return GetNoticePaperPendingToLayListCount();
                    }
                case "GetBillsPaperPendingToLayListCount":
                    {
                        return GetBillsPaperPendingToLayListCount();
                    }
                case "GetCommittePaperPendingToLayListCount":
                    {
                        return GetCommittePaperPendingToLayListCount();
                    }
                //Method added By Sunil
                case "GetMemberNameByID":
                    {
                        return GetMemberNameByID(param.Parameter);
                    }

                case "GetQuestionByIdForEdit":
                    {
                        return GetQuestionByIdForEdit(param.Parameter);
                    }

                case "GetNoticeByIdForEdit":
                    {
                        return GetNoticeByIdForEdit(param.Parameter);
                    }
                case "GetDepartmentByMinister":
                    {
                        return GetDepartmentByMinister(param.Parameter);
                    }
                case "GetConstByMemberId":
                    {
                        return GetConstByMemberId(param.Parameter);
                    }
                case "SaveQuestion":
                    {
                        return SaveQuestion(param.Parameter);
                    }
                case "SaveNotice":
                    {
                        return SaveNotice(param.Parameter);
                    }
                case "GetQuestionDetailById":
                    {
                        return GetQuestionDetailById(param.Parameter);
                    }
                case "GetNoticeDetailById":
                    {
                        return GetNoticeDetailById(param.Parameter);
                    }

                case "CheckQuestionNo":
                    {
                        return CheckQuestionNo(param.Parameter);
                    }
                case "GetAllDiariedRecord":
                    {
                        return GetAllDiariedRecord(param.Parameter);
                    }
            
                    
                #endregion
            }
            return null;
        }

        #region Other Methods

        static object UpdatePaperLaidPriority(object param)
        {
            tPaperLaidV parameter = param as tPaperLaidV;
            long PaperlaidId = parameter.PaperLaidId;
            string Prioritytype = parameter.MinistryName;
            int Priority = parameter.PaperlaidPriority;



            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {

                    var paperlaidToUpdate = db.tPaperLaidVS.SingleOrDefault(q => q.PaperLaidId == parameter.PaperLaidId);

                    switch (Prioritytype)
                    {
                        case "PaperLaid": paperlaidToUpdate.PaperlaidPriority = Priority; break;
                        case "Ministry": paperlaidToUpdate.MinistrywisePriority = Priority; break;
                        case "Department": paperlaidToUpdate.DepartmentwisePriority = Priority; break;
                    }
                    db.tPaperLaidVS.Attach(paperlaidToUpdate);
                    db.Entry(paperlaidToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                    return null;
                }
            }
            catch
            {
                throw;
            }

        }

        static object UpdateDepartmentwisePriority(object param)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    List<PaperLaidPriority> priority = param as List<PaperLaidPriority>;
                    priority = priority.Where(a => a.FilteredBy == 1).ToList();
                    var PaperLaids = db.tPaperLaidVS.ToList();
                    foreach (var Department in PaperLaids)
                    {
                        foreach (var item in priority)
                        {
                            if (Department.PaperLaidId == item.PaperlaidId)
                            {
                                var DepartmaentToUpdate = db.tPaperLaidVS.SingleOrDefault(b => b.PaperLaidId == item.PaperlaidId);
                                DepartmaentToUpdate.DepartmentwisePriority = item.Priority;
                                db.tPaperLaidVS.Attach(DepartmaentToUpdate);
                                db.Entry(DepartmaentToUpdate).State = EntityState.Modified;
                            }

                        }
                    }
                    db.SaveChanges();
                    db.Close();
                    return null;
                }
            }
            catch
            {
                throw;
            }
        }

        static object UpdateMinistrywisePriority(object param)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    List<PaperLaidPriority> priority = param as List<PaperLaidPriority>;
                    priority = priority.Where(a => a.FilteredBy == 2).ToList();
                    var PaperLaids = db.tPaperLaidVS.ToList();
                    foreach (var ministry in PaperLaids)
                    {
                        foreach (var item in priority)
                        {
                            if (ministry.PaperLaidId == item.PaperlaidId)
                            {
                                var MinistryToUpdate = db.tPaperLaidVS.SingleOrDefault(b => b.PaperLaidId == item.PaperlaidId);
                                MinistryToUpdate.MinistrywisePriority = item.Priority;
                                db.tPaperLaidVS.Attach(MinistryToUpdate);
                                db.Entry(MinistryToUpdate).State = EntityState.Modified;
                            }

                        }
                    }
                    db.SaveChanges();
                    db.Close();
                    return null;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetPapercategoryDetailsById(object param)
        {
            mPaperCategoryType paper = param as mPaperCategoryType;
            int Id = paper.PaperCategoryTypeId;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from P in db.mPaperCategoryTypes
                                  where P.PaperCategoryTypeId == Id
                                  select P).SingleOrDefault();

                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetPaperLaidDetailsById(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;
            long Id = paper.PaperLaidId;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from P in db.tPaperLaidVS
                                  where P.PaperLaidId == Id
                                  select P).SingleOrDefault();

                    return result;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Paper Laid Summery For All Paper Category Types

        #region Qusetion

        //pending count
        static int GetStarredQuestionPendingCount()
        {
            try
            {
                var result = (List<tQuestion>)GetStarredQuestionPendingList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionPendingList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from Q in db.tQuestions
                                  where Q.QuestionStatus == (int)Questionstatus.QuestionPending
                                  && (Q.DiaryNumber != null)
                                  && (Q.AssemblyID == 12)
                                  && (Q.SessionID == 4)// for question
                                  && (Q.QuestionType == 1)// for starred
                                  select Q).ToList();
                    return result.ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        //sent count
        static int GetStarredQuestionSentCount()
        {
            try
            {
                var result = (List<tQuestion>)GetStarredQuestionSentList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionSentList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from Q in db.tQuestions
                                  where Q.DepartmentID != null && Q.MinisterID != null && Q.IsSubmitted != null // for question
                                    && Q.QuestionType == 1// for starred
                                  select Q).ToList();
                    return result.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        //Recieved count
        static int GetStarredQuestionPaperRecievedCount()
        {
            try
            {
                var result = (List<tQuestion>)GetStarredQuestionPaperRecievedList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionPaperRecievedList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  join PLTemp in db.tPaperLaidTemp on Q.PaperLaidId equals PLTemp.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId                                 
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 1 // for question
                                    && Q.QuestionType == 1// for starred
                                    && PLTemp.DeptSubmittedBy != null
                                   && PLTemp.DeptSubmittedDate != null
                                   && PL.DeptActivePaperId == PLTemp.PaperLaidTempId
                                  select Q).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        //Current LOB  count
        static int GetStarredQuestionPaperCurrentLobCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetStarredQuestionPaperCurrentLobList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionPaperCurrentLobList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  join PLTemp in db.tPaperLaidTemp on Q.PaperLaidId equals PLTemp.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId                                 
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for starred
                                    && PLTemp.DeptSubmittedBy != null
                                    && PLTemp.DeptSubmittedDate != null
                                    && PL.DeptActivePaperId == PLTemp.PaperLaidTempId
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        //Laid in home count
        static int GetStarredQuestionPaperLaidInHomeCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetStarredQuestionPaperLaidInHomeList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetStarredQuestionPaperLaidInHomeList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  join PLTemp in db.tPaperLaidTemp on Q.PaperLaidId equals PLTemp.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId                                 
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for starred
                                    && PLTemp.DeptSubmittedBy != null
                                    && PLTemp.DeptSubmittedDate != null
                                    && PL.DeptActivePaperId == PLTemp.PaperLaidTempId
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        //Pending To Lay list
        static object GetStarredQuestionPaperPendingToLayList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  join PLTemp in db.tPaperLaidTemp on Q.PaperLaidId equals PLTemp.PaperLaidId
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId                                 
                                  where PL.LOBRecordId == null && PL.LaidDate == null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for starred
                                    && PLTemp.DeptSubmittedBy != null
                                    && PLTemp.DeptSubmittedDate != null
                                    && PL.DeptActivePaperId == PLTemp.PaperLaidTempId
                                  select Q).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        //pending count
        static int GetUnStarredQuestionPendingCount()
        {
            try
            {
                var result = (List<tQuestion>)GetUnStarredQuestionPendingList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }
        static object GetUnStarredQuestionPendingList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from Q in db.tQuestions
                                  where Q.QuestionStatus == (int)Questionstatus.QuestionPending
                                  && (Q.DiaryNumber != null)
                                  && (Q.AssemblyID == 12)
                                  && (Q.SessionID == 4)// for question
                                  && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)// for unStarred
                                  select Q).ToList();

                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        //sent count
        static int GetUnStarredQuestionSentCount()
        {
            try
            {
                var result = (List<tQuestion>)GetUnStarredQuestionSentList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetUnStarredQuestionSentList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from Q in db.tQuestions
                                  where Q.DepartmentID != null && Q.MinisterID != null && Q.IsSubmitted != null // for question
                                    && Q.QuestionType == 2// for unstarred
                                  select Q).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        //Recieved count
        static int GetUnStarredQuestionPaperRecievedCount()
        {
            try
            {
                var result = (List<tQuestion>)GetUnStarredQuestionPaperRecievedList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetUnStarredQuestionPaperRecievedList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select Q).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        //Current LOB  count
        static int GetUnStarredQuestionPaperCurrentLobCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetUnStarredQuestionPaperCurrentLobList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetUnStarredQuestionPaperCurrentLobList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        //Laid in Home
        static int GetUnStarredQuestionPaperLaidInHomeCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetUnStarredQuestionPaperLaidInHomeList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetUnStarredQuestionPaperLaidInHomeList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        //Pending To Lay
        static object GetUnStarredQuestionPaperPendingToLayList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Notice
        static int GetNoticePaperRecievedCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetNoticePaperRecievedList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePaperRecievedList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static int GetNoticeSentCount()
        {
            try
            {
                var result = (List<tMemberNotice>)GetNoticeSentList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticeSentList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from MN in db.tMemberNotices
                                  where MN.DepartmentId != null && MN.MinistryId != null && MN.IsSubmitted != null  // for notice
                                  select MN).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        static int GetNoticePendingCount()
        {
            try
            {
                var result = (List<tMemberNotice>)GetNoticePendingList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePendingList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from MN in db.tMemberNotices
                                  where MN.NoticeNumber != null
                                  && (MN.NoticeStatus == (int)NoticeStatusEnum.NoticePending)
                                  && (MN.AssemblyID == 12)
                                  && (MN.SessionID == 4)
                                  // for noticepending
                                  select MN).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        //Total List(Pending and Sent)
        static object GetAllStarredQuestionList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var pending = (from Q in db.tQuestions
                                   where Q.QuestionStatus == (int)Questionstatus.QuestionPending
                                   && (Q.DiaryNumber != null)
                                   && (Q.AssemblyID == 12)
                                   && (Q.SessionID == 4)// for question
                                   && (Q.QuestionType == (int)QuestionType.StartedQuestion)// for starred
                                   select Q).ToList();
                    var sent = (from Q in db.tQuestions
                                where Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                && (Q.IsSubmitted == true)
                                && (Q.AssemblyID == 12)
                                && (Q.SessionID == 4)// for question
                                && (Q.QuestionType == (int)QuestionType.StartedQuestion)// for starred
                                select Q).ToList().ToList();

                    var result = new List<tQuestion>();
                    foreach (var item in pending)
                    {
                        item.IsPending = true;
                        result.Add(item);
                    }
                    foreach (var item in sent)
                    {
                        item.IsPending = false;
                        result.Add(item);
                    }
                    return result.ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        //Total List(Pending and Sent)
        static object GetAllUnStarredQuestionList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var pending = (from Q in db.tQuestions
                                   where Q.QuestionStatus == (int)Questionstatus.QuestionPending
                                   && (Q.DiaryNumber != null)
                                   && (Q.AssemblyID == 12)
                                   && (Q.SessionID == 4)// for question
                                   && (Q.QuestionType == 2)// for starred
                                   select Q).ToList();
                    var sent = (from Q in db.tQuestions
                                where Q.QuestionStatus == (int)Questionstatus.QuestionSent
                                && (Q.IsSubmitted == true)
                                && (Q.AssemblyID == 12)
                                && (Q.SessionID == 4)// for question
                                && (Q.QuestionType == (int)QuestionType.UnstaredQuestion)// for Unstarred
                                select Q).ToList().ToList();

                    var result = new List<tQuestion>();
                    foreach (var item in pending)
                    {
                        item.IsPending = true;
                        result.Add(item);
                    }
                    foreach (var item in sent)
                    {
                        item.IsPending = false;
                        result.Add(item);
                    }
                    return result.ToList();
                }
            }
            catch
            {
                throw;
            }
        }
        //Total List(Pending and Sent)
        static object GetAllNoticeList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var pending = (from MN in db.tMemberNotices
                                   where MN.NoticeNumber != null
                                   && (MN.NoticeStatus == (int)NoticeStatusEnum.NoticePending)
                                   && (MN.AssemblyID == 12)
                                   && (MN.SessionID == 4)
                                   // for noticepending
                                   select MN).ToList();
                    var sent = (from MN in db.tMemberNotices
                                where MN.NoticeNumber != null
                                && (MN.NoticeStatus == (int)NoticeStatusEnum.NoticeSent)
                                && (MN.IsSubmitted == true)
                                && (MN.AssemblyID == 12)
                                && (MN.SessionID == 4)
                                // for noticepending
                                select MN).ToList();

                    var result = new List<tMemberNotice>();
                    foreach (var item in pending)
                    {
                        item.IsPending = true;
                        result.Add(item);
                    }
                    foreach (var item in sent)
                    {
                        item.IsPending = false;
                        result.Add(item);
                    }
                    return result.ToList();
                }
            }
            catch
            {
                throw;
            }
        }

        static int GetNoticePaperCurrentLobCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetNoticePaperCurrentLobList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePaperCurrentLobList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        static int GetNoticePaperLaidInHomeCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetNoticePaperLaidInHomeList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePaperLaidInHomeList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticePaperPendingToLayList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        #endregion

        #region Bills
        static int GetBillsPaperRecievedCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetBillsPaperRecievedList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperRecievedList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 2 // for bills
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static int GetBillsPaperCurrentLobCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetBillsPaperCurrentLobList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperCurrentLobList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 2 // for bills
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }

        }

        static int GetBillsPaperLaidInHomeCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetBillsPaperLaidInHomeList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperLaidInHomeList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 2 // for bills
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetBillsPaperPendingToLayList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && E.PaperCategoryTypeId == 2 // for bills
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Other Paper Types
        static int GetOtherPaperToLayRecievedCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetOtherPaperToLayRecievedList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }


        static object GetOtherPaperToLayRecievedList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId == null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        static int GetOtherPaperToLayCurrentLobCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetOtherPaperToLayCurrentLobList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetOtherPaperToLayCurrentLobList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }



        static int GetOtherPaperToLayLaidInHomeCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetOtherPaperToLayLaidInHomeList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetOtherPaperToLayLaidInHomeList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LaidDate != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetOtherPaperPendingToLayList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Committee

        static int GetCommittePaperRecievedCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetCommittePaperRecievedList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetCommittePaperRecievedList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId == null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }


        static int GetCommittePaperCurrentLobCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetCommittePaperCurrentLobList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetCommittePaperCurrentLobList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }


        }

        static int GetCommittePaperLaidInHomeCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetCommittePaperLaidInHomeList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static object GetCommittePaperLaidInHomeList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LaidDate != null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetCommittePaperPendingToLayList()
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId == null && PL.LaidDate == null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion





        #endregion

        #region Paper Laid Summery For A Paper Category Type By Minister/Department/Status :Not Required

        #region Qusetion


        static List<PaperLaidByPaperCategoryType> GetStarredQuestionPaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for unstarred
                                  select PL).ToList();

                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetStarredQuestionPaperCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }


        static List<PaperLaidByPaperCategoryType> GetStarredQuestionPaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 1  // for question
                                    && Q.QuestionType == 1// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetUnStarredQuestionPaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetUnStarredQuestionPaperCurrentLob(bool ByMinister)
        {

            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetUnStarredQuestionPaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join Q in db.tQuestions on PL.PaperLaidId equals Q.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 1 // for question
                                     && Q.QuestionType == 2// for unstarred
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Notice
        static List<PaperLaidByPaperCategoryType> GetNoticePaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetNoticePaperCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetNoticePaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join MN in db.tMemberNotices on PL.PaperLaidId equals MN.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 4 // for notice
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }
        #endregion

        #region Bills
        static List<PaperLaidByPaperCategoryType> GetBillsPaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId == null && E.PaperCategoryTypeId == 2 // for bills
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetBillsPaperCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LOBRecordId != null && E.PaperCategoryTypeId == 2 // for bills
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetBillsPaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join B in db.tBillRegisters on PL.PaperLaidId equals B.PaperLaidId
                                  where PL.LaidDate != null && E.PaperCategoryTypeId == 2 // for bills
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Other Paper Types
        static List<PaperLaidByPaperCategoryType> GetPaperToLayRecieved(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId == null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetPaperToLayCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetPaperToLayLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  where PL.LaidDate != null && PC.PaperCategoryTypeId == 5 // for paper lay 
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Committee

        static List<PaperLaidByPaperCategoryType> GetCommittePaperRecieved(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId == null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsRecieved = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsRecieved = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetCommittePaperCurrentLob(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LOBRecordId != null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DepartmentId, Id = group.Key.DeparmentName, Count = group.Count(), IsCurrentLob = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        static List<PaperLaidByPaperCategoryType> GetCommittePaperLaidInHome(bool ByMinister)
        {
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  join PC in db.mPaperCategoryTypes on E.PaperCategoryTypeId equals PC.PaperCategoryTypeId
                                  join C in db.tCommittees on PL.CommitteeId equals C.CommitteeId
                                  where PL.LaidDate != null && PC.PaperCategoryTypeId == 3 // for committee
                                  select PL).ToList();
                    if (ByMinister)
                    {
                        return result.GroupBy(m => new { m.MinistryName, m.MinistryId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.MinistryName, Id = group.Key.MinistryId.ToString(), Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                    else
                    {
                        return result.GroupBy(m => new { m.DeparmentName, m.DepartmentId }).Select(group =>
                            new PaperLaidByPaperCategoryType { Name = group.Key.DeparmentName, Id = group.Key.DepartmentId, Count = group.Count(), IsLaidInHome = true }).ToList();
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region Common
        static object GetPaperLaidSummeryForPaperCategory(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;

            int PaperCategoryTypeId = paper.EventId;
            bool IsStarredQuestion = paper.AssemblyId == 0 ? false : true;
            bool IsMinistryLevel = paper.SessionId == 0 ? false : true;

            List<PaperLaidByPaperCategoryType> result = new List<PaperLaidByPaperCategoryType>();

            if (PaperCategoryTypeId == 1)
            {
                if (IsStarredQuestion)
                {
                    foreach (var item in GetStarredQuestionPaperRecieved(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    foreach (var item in GetStarredQuestionPaperCurrentLob(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    foreach (var item in GetStarredQuestionPaperLaidInHome(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    return result;

                }
                else
                {

                    foreach (var item in GetUnStarredQuestionPaperRecieved(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    foreach (var item in GetUnStarredQuestionPaperCurrentLob(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    foreach (var item in GetUnStarredQuestionPaperLaidInHome(IsMinistryLevel))
                    {
                        result.Add(item);
                    }
                    return result;

                }
            }
            if (PaperCategoryTypeId == 2)
            {
                foreach (var item in GetBillsPaperRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetBillsPaperCurrentLob(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetBillsPaperLaidInHome(IsMinistryLevel))
                {
                    result.Add(item);
                }
                return result;
            }
            if (PaperCategoryTypeId == 3)
            {

                foreach (var item in GetCommittePaperRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetCommittePaperCurrentLob(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetCommittePaperLaidInHome(IsMinistryLevel))
                {
                    result.Add(item);
                }
                return result;
            }
            if (PaperCategoryTypeId == 4)
            {
                foreach (var item in GetNoticePaperRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetNoticePaperRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetNoticePaperLaidInHome(IsMinistryLevel))
                {
                    result.Add(item);
                }
                return result;
            }
            if (PaperCategoryTypeId == 5)
            {
                foreach (var item in GetPaperToLayRecieved(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetPaperToLayCurrentLob(IsMinistryLevel))
                {
                    result.Add(item);
                }
                foreach (var item in GetPaperToLayLaidInHome(IsMinistryLevel))
                {
                    result.Add(item);
                }
                return result;
            }


            return null;
        }

        static object GetCompletePaperLaidDetailsByMinistry(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;
            var MinistryId = paper.MinistryId;
            var PaperCategoryId = paper.PaperCategoryTypeId;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  where PL.MinistryId == MinistryId && E.PaperCategoryTypeId == PaperCategoryId
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetCompletePaperLaidDetailsByPaperLaid(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;

            var PaperCategoryId = paper.PaperCategoryTypeId;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  where E.PaperCategoryTypeId == PaperCategoryId
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetCompletePaperLaidDetailsByDepartment(object param)
        {
            tPaperLaidV paper = param as tPaperLaidV;
            var DepartmentId = paper.DepartmentId;
            var PaperCategoryId = paper.PaperCategoryTypeId;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    var result = (from PL in db.tPaperLaidVS
                                  join E in db.mEvents on PL.EventId equals E.EventId
                                  where PL.DepartmentId == DepartmentId && E.PaperCategoryTypeId == PaperCategoryId
                                  select PL).ToList();
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetCompletePaperLaidDetailsByStatus(object param)
        {
            tPaperLaidV parameter = param as tPaperLaidV;
            int PaperCategoryTypeId = parameter.PaperCategoryTypeId;
            bool IsStarred = parameter.QuestionID == 0 ? false : true;
            string Status = parameter.Title;
            var result = new List<tPaperLaidV>();
            switch (Status)
            {
                case "All"://for pending+sent
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetAllStarredQuestionList();
                            else
                                return GetAllUnStarredQuestionList();
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetAllNoticeList();
                        }
                        break;
                    }
                case "PendingList":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPendingList();
                            else
                                return GetUnStarredQuestionPendingList();
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePendingList();
                        }
                        break;
                    }
                case "Sent":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionSentList();
                            else
                                return GetUnStarredQuestionSentList();
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticeSentList();
                        }
                        break;
                    }
                case "Recieved":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPaperRecievedList();
                            else
                                return GetUnStarredQuestionPaperRecievedList();
                        }
                        else if (PaperCategoryTypeId == 2)
                        {
                            return GetBillsPaperRecievedList();
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetCommittePaperRecievedList();
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePaperRecievedList();
                        }
                        else if (PaperCategoryTypeId == 5)
                        {
                            return GetOtherPaperToLayRecievedList();
                        }
                        break;
                    }
                case "LaidInHome":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPaperLaidInHomeList();
                            else
                                return GetUnStarredQuestionPaperLaidInHomeList();
                        }
                        else if (PaperCategoryTypeId == 2)
                        {
                            return GetBillsPaperLaidInHomeList();
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetCommittePaperLaidInHomeList();
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePaperLaidInHomeList();
                        }
                        else if (PaperCategoryTypeId == 5)
                        {
                            return GetOtherPaperToLayLaidInHomeList();
                        }
                        break;
                    }
                case "CurrentLob":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPaperCurrentLobList();
                            else
                                return GetUnStarredQuestionPaperCurrentLobList();
                        }
                        else if (PaperCategoryTypeId == 2)
                        {
                            return GetBillsPaperCurrentLobList();
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetCommittePaperCurrentLobList();
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePaperCurrentLobList();
                        }
                        else if (PaperCategoryTypeId == 5)
                        {
                            return GetOtherPaperToLayCurrentLobList();
                        }
                        break;
                    }
                case "Pending":
                    {
                        if (PaperCategoryTypeId == 1)
                        {
                            if (IsStarred)
                                return GetStarredQuestionPaperPendingToLayList();
                            else
                                return GetUnStarredQuestionPaperPendingToLayList();
                        }
                        else if (PaperCategoryTypeId == 2)
                        {
                            return GetBillsPaperPendingToLayList();
                        }
                        else if (PaperCategoryTypeId == 3)
                        {
                            return GetCommittePaperPendingToLayList();
                        }
                        else if (PaperCategoryTypeId == 4)
                        {
                            return GetNoticePaperPendingToLayList();
                        }
                        else if (PaperCategoryTypeId == 5)
                        {
                            return GetOtherPaperPendingToLayList();
                        }
                        break;
                    }
                default: return null;
            }
            return null;
        }

        static object GetQuestionDetailsById(object param)
        {
            tQuestion parameter = param as tQuestion;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    return db.tQuestions.SingleOrDefault(a => a.QuestionID == parameter.QuestionID);
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetNoticeDetailsById(object param)
        {
            tMemberNotice parameter = param as tMemberNotice;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    return db.tMemberNotices.SingleOrDefault(a => a.NoticeId == parameter.NoticeId);
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetMinistryById(object param)
        {
            tQuestion parameter = param as tQuestion;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    return db.mMinistry.SingleOrDefault(a => a.MinistryID == parameter.MinistryId);
                }
            }
            catch
            {
                throw;
            }
        }


        static object GetDepartmentById(object param)
        {
            mDepartment parameter = param as mDepartment;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    return db.mDepartment.SingleOrDefault(a => a.deptId == parameter.deptId);
                }
            }
            catch
            {
                throw;
            }
        }


        #endregion


        #endregion


        #region added by nitin



        static int GetOtherPaperPendingToLayListCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetOtherPaperPendingToLayList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }
        static int GetCommittePaperPendingToLayListCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetCommittePaperPendingToLayList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }





        static int GetStarredQuestionPaperPendingToLayListCount()
        {
            try
            {
                var result = (List<tQuestion>)GetStarredQuestionPaperPendingToLayList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static int GetBillsPaperPendingToLayListCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetBillsPaperPendingToLayList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }
        static int GetNoticePaperPendingToLayListCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetNoticePaperPendingToLayList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }


        static int GetUnStarredQuestionPaperPendingToLayListCount()
        {
            try
            {
                var result = (List<tPaperLaidV>)GetUnStarredQuestionPaperPendingToLayList();
                return result.ToList().Count();
            }
            catch
            {
                throw;
            }
        }

        static List<mEvent> GetOtherPaperLaidPaperCategoryType(object param)
        {
            LegislativeContext db = new LegislativeContext();

            mEvent model = param as mEvent;


            var query = (from dist in db.mEvents
                         where dist.PaperCategoryTypeId == 5
                         select dist);

            //string searchText = (model.EventName != null && model.EventName != "") ? model.EventName.ToLower() : "";
            //if (searchText != "")
            //{
            //    query = query.Where(a =>
            //        a.EventName != null && a.EventName.ToLower().Contains(searchText));

            //}

            return query.ToList();
        }
        static object GetUpdatedOtherPaperLaidCounters(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            model.TotalOtherpaperLaidUpdated = GetUpdatedOtherPaperList(model).ResultCount;
            return model;
        }
        static tPaperLaidV GetUpdatedOtherPaperList(object param)
        {
            LegislativeContext db = new LegislativeContext();
            tPaperLaidV model = param as tPaperLaidV;
            int paperCategoryId = 5;
            int? EventId = model.PaperCategoryTypeId;
            if (model.PageIndex == 0)
            {
                model.PageIndex = 1;
            }
            if (model.PAGE_SIZE == 0)
            {
                model.PAGE_SIZE = 20;
            }



            //          var query = (from  paperLaidTemp in pCtxt.tPaperLaidTemp 
            //                       join paperLaid in pCtxt.tPaperLaidV on paperLaidTemp.PaperLaidId equals paperLaid.PaperLaidId
            //                       where  paperLaid.DepartmentId == model.DepartmentId
            //&& paperLaidTemp.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
            //&& paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
            //&& paperLaid.DesireLayingDate > DateTime.Now && paperLaid.LaidDate == null && paperLaid.LOBRecordId != null
            //&& paperLaid.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId



            var query = (from m in db.tPaperLaidVS
                         join c in db.mEvents on m.EventId equals c.EventId
                         join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID

                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         where m.DeptActivePaperId == t.PaperLaidTempId
                         && t.DeptSubmittedBy != null
                         && t.DeptSubmittedDate != null
                         && t.MinisterSubmittedBy != null
                         && t.MinisterSubmittedDate != null
                         && m.DesireLayingDate > DateTime.Now
                         && m.LaidDate == null
                         && m.LOBRecordId != null
                         && m.MinisterActivePaperId == t.PaperLaidTempId
                         && c.PaperCategoryTypeId == paperCategoryId
                         && m.LOBPaperTempId < m.MinisterActivePaperId
                         select new PaperMovementModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             EventId = c.EventId,
                             Title = m.Title,
                             DeparmentName = m.DeparmentName,
                             MinisterName = ministery.MinistryName,
                             PaperTypeID = (int)m.PaperLaidId,
                             actualFilePath = t.FilePath + t.FileName,
                             PaperCategoryTypeId = c.PaperCategoryTypeId,
                             BusinessType = c.EventName,
                             version = t.Version,
                             FilePath = t.FileName,
                             DeptSubmittedDate = t.DeptSubmittedDate,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryTypes join e in db.mEvents on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault()

                         }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.DepartmentSubmitList = results;


            return model;
        }
        static object ShowUpdatedOtherPaperLaidDetailByID(object param)
        {
            LegislativeContext db = new LegislativeContext();
            PaperMovementModel model = param as PaperMovementModel;

            var query = (from tPaper in db.tPaperLaidVS
                         where model.PaperLaidId == tPaper.PaperLaidId
                         join mEvents in db.mEvents on tPaper.EventId equals mEvents.EventId
                         join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                         join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                         join tPaperTemp in db.tPaperLaidTemp on tPaper.DeptActivePaperId equals tPaperTemp.PaperLaidTempId
                         join mPaperCategory in db.mPaperCategoryTypes on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                         select new PaperMovementModel
                         {

                             EventName = mEvents.EventName,
                             MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
                             DeparmentName = mDept.deptname,
                             PaperLaidId = tPaper.PaperLaidId,
                             actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
                             ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                             FilePath = tPaperTemp.FilePath,
                             Title = tPaper.Title,
                             Description = tPaper.Description,
                             Remark = tPaper.Remark,
                             PaperCategoryTypeId = mEvents.PaperCategoryTypeId,
                             PaperCategoryTypeName = mPaperCategory.Name,
                             PaperSubmittedDate = tPaperTemp.DeptSubmittedDate

                         }).SingleOrDefault();
            model = query;
            model.SubmittedFileListByPaperLaidId = (from tPaperTemp in db.tPaperLaidTemp
                                                    where tPaperTemp.PaperLaidId == query.PaperLaidId && tPaperTemp.DeptSubmittedBy != null && tPaperTemp.DeptSubmittedDate
                            != null
                                                    select tPaperTemp).ToList();


            return model;
        }
        static object GetDetailsByPaperLaidId(object param)
        {
            LegislativeContext db = new LegislativeContext();
            PaperMovementModel model = param as PaperMovementModel;
            var query = (from mdl in db.tPaperLaidVS
                         where mdl.PaperLaidId == model.PaperLaidId
                         join mdlTemp in db.tPaperLaidTemp on mdl.DeptActivePaperId equals mdlTemp.PaperLaidTempId
                         join mdlAdminLOB in db.AdminLOB on mdl.LOBRecordId equals mdlAdminLOB.Id
                         select new PaperMovementModel
                         {
                             PaperLaidId = model.PaperLaidId,
                             MinisterActivePaperId = mdl.MinisterActivePaperId,
                             DeptActivePaperId = mdl.DeptActivePaperId,
                             actualFilePath = mdlTemp.SignedFilePath,
                             FilePath = mdlAdminLOB.PDFLocation
                         }).SingleOrDefault();
            return query;

        }

        static object UpdateLOBTempIdByPaperLiadID(object param)
        {
            PaperMovementModel model = param as PaperMovementModel;
            LegislativeContext db = new LegislativeContext();
            var query = (from mdl in db.tPaperLaidVS where model.PaperLaidId == mdl.PaperLaidId select mdl).SingleOrDefault();
            query.LOBPaperTempId = Convert.ToInt32(model.MinisterActivePaperId);
            db.SaveChanges();
            db.Close();
            return model;
        }
        #endregion

        #region "This is method for Edit of Question And Notice Copied from Diary context By Sunil"
        static object GetMemberNameByID(object param)
        {
            mMember parameter = param as mMember;
            try
            {
                using (LegislativeContext db = new LegislativeContext())
                {
                    return db.mMember.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
                }
            }

            catch
            {
                throw;
            }
        }
        public static object GetQuestionByIdForEdit(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;
            DiaryModel Obj2 = new DiaryModel();
            var record = (from Ques in DiaCtx.objQuestion
                          where Ques.QuestionID == Obj.QuestionId
                          select new DiaryModel
                          {
                              AssemblyID = Ques.AssemblyID,
                              SessionID = Ques.SessionID,
                              QuestionId = Ques.QuestionID,
                              QuestionTypeId = Ques.QuestionType,
                              DiaryNumber = Ques.DiaryNumber,
                              MemberId = Ques.MemberID,
                              ConstituencyName = Ques.ConstituencyName,
                              EventId = Ques.EventId,
                              CatchWordSubject = Ques.SubInCatchWord,
                              Subject = Ques.Subject,
                              MinistryId = Ques.MinistryId,
                              DepartmentId = Ques.DepartmentID,
                              MainQuestion = Ques.MainQuestion,
                              SessionDateId = Ques.SessionDateId,
                              QuestionNumber = Ques.QuestionNumber,
                          }).FirstOrDefault();
            Obj2 = record;

            /*Get All Session date according to session id */
            var SDt = (from SD in SCtx.mSessDate
                       where SD.SessionId == record.SessionID
                       select SD).ToList();

            foreach (var item in SDt)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }
            Obj2.SessDateList = SDt;



            /*Get All Event or Category*/

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 1
                           select Evnt).ToList();
            Obj2.eventList = EvtList;

            /*Get All MinistryMinister*/
            var MinMisList = (from Min in DiaCtx.objmMinistry
                              join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
                              orderby Min.OrderID
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
                              }).ToList();
            Obj2.memMinList = MinMisList;

            /*Get All Department*/
            if (record.MinistryId != null && record.MinistryId != 0)
            {
                var DeptList = (from minsDept in DiaCtx.objMinisDepart
                                join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
                                where minsDept.MinistryID == record.MinistryId
                                select new DiaryModel
                                {
                                    DepartmentId = dept.deptId,
                                    DepartmentName = dept.deptname

                                }).ToList();

                Obj2.DiaryList = DeptList;
            }
            /*Get  Member Name*/

            if (record.MemberId != 0)
            {
                var Member = (from Mem in DiaCtx.objmMember
                              where Mem.MemberCode == record.MemberId
                              select Mem).FirstOrDefault();

                Obj2.MemberName = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
            }

            DiaCtx.Close();


            return Obj2;

        }

        public static object GetNoticeByIdForEdit(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();

            var PndList = (from Ntces in DiaCtx.ObjNotices
                           where Ntces.NoticeId == Obj.NoticeId
                           select new DiaryModel
                           {
                               NoticeNumber = Ntces.NoticeNumber,
                               MemId = Ntces.MemberId,
                               EventId = Ntces.NoticeTypeID,
                               CatchWordSubject = Ntces.SubInCatchWord,
                               Subject = Ntces.Subject,
                               MinistryId = Ntces.MinistryId,
                               DepartmentId = Ntces.DepartmentId,
                               DateForBind = Ntces.NoticeDate,
                               TimeForBind = Ntces.NoticeTime,
                               NoticeId = Ntces.NoticeId,
                               Notice = Ntces.Notice,
                           }).FirstOrDefault();
            Obj2 = PndList;

            ///*Get All Session date according to session id */
            //var SDt = (from SD in SCtx.mSessDate
            //           where SD.SessionId == Obj.SessionID
            //           select SD).ToList();

            //foreach (var item in SDt)
            //{
            //    item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            //}
            //Obj2.SessDateList = SDt;



            /*Get All Event or Category*/

            var EvtList = (from Evnt in DiaCtx.objmEvent
                           where Evnt.PaperCategoryTypeId == 4
                           select Evnt).ToList();
            Obj2.eventList = EvtList;

            /*Get All MinistryMinister*/
            var MinMisList = (from Min in DiaCtx.objmMinistry
                              join minis in DiaCtx.objmMinistryMinister on Min.MinistryID equals minis.MinistryID
                              orderby Min.OrderID
                              select new mMinisteryMinisterModel
                              {
                                  MinistryID = minis.MinistryID,
                                  MinisterMinistryName = minis.MinisterName + ", " + Min.MinistryName
                              }).ToList();
            Obj2.memMinList = MinMisList;

            /*Get All Department*/
            if (PndList.MinistryId != null && PndList.MinistryId != 0)
            {
                var DeptList = (from minsDept in DiaCtx.objMinisDepart
                                join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
                                where minsDept.MinistryID == PndList.MinistryId
                                select new DiaryModel
                                {
                                    DepartmentId = dept.deptId,
                                    DepartmentName = dept.deptname

                                }).ToList();

                Obj2.DiaryList = DeptList;
            }
            /*Get All Member*/
            if (PndList.MemId != 0 && PndList.MemId != null)
            {
                var Member = (from Mem in DiaCtx.objmMember
                              where Mem.MemberCode == PndList.MemId
                              select Mem).FirstOrDefault();

                Obj2.MemberName = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
            }

            DiaCtx.Close();


            return Obj2;
        }

        public static object GetDepartmentByMinister(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            var DeptList = (from minsDept in DiaCtx.objMinisDepart
                            join dept in DiaCtx.objmDepartment on minsDept.DeptID equals dept.deptId
                            where minsDept.MinistryID == Obj.MinistryId
                            select new DiaryModel
                            {
                                DepartmentId = dept.deptId,
                                DepartmentName = dept.deptname

                            }).ToList();
            DiaCtx.Close();

            return DeptList;

        }

        public static object GetConstByMemberId(object param)
        {
            if (null == param)
            {
                return null;
            }

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            var Constituency = (from C in DiaCtx.ObjConst
                                join MA in DiaCtx.ObjMemAssem on C.ConstituencyCode equals MA.ConstituencyCode
                                where MA.MemberID == Obj.MemberId
                                && C.AssemblyID == Obj.AssemblyID
                                && MA.AssemblyID == Obj.AssemblyID
                                select new DiaryModel
                                {
                                    ConstituencyCode = C.ConstituencyCode,
                                    ConstituencyName = C.ConstituencyName

                                }).FirstOrDefault();
            return Constituency;
        }

        public static object SaveQuestion(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tQuestion Obj2 = new tQuestion();
            string msg = "";
            Obj2 = DiaCtx.objQuestion.Single(m => m.QuestionID == Obj.QuestionId);
            DiaCtx.objQuestion.Attach(Obj2);
            try
            {

                if (Obj.ViewType == "QInbox")
                {
                    if (Obj.BtnCaption == "Save")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.SessionDateId = Obj.SessionDateId;
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionPending;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Saved !";
                    }
                    else if (Obj.BtnCaption == "Send")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.SessionDateId = Obj.SessionDateId;
                        Obj2.IsQuestionFreeze = true;
                        Obj2.IsQuestionFreezeDate = DateTime.Now;
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionSent;
                        Obj2.IsSubmitted = true;
                        Obj2.SubmittedDate = DateTime.Now;
                        Obj2.SubmittedBy = Obj.SubmittedBy;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Sent !";
                    }

                }
                else if (Obj.ViewType == "QDraft")
                {
                    if (Obj.BtnCaption == "Save")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.SessionDateId = Obj.SessionDateId;
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionPending;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Saved !";
                    }
                    else if (Obj.BtnCaption == "Send")
                    {
                        Obj2.EventId = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.DepartmentID = Obj.DepartmentId;
                        Obj2.MemberID = Obj.MemberId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.MainQuestion = Obj.MainQuestion;
                        Obj2.ConstituencyName = Obj.ConstituencyName;
                        Obj2.ConstituencyNo = Obj.ConstituencyCode.ToString();
                        Obj2.SessionDateId = Obj.SessionDateId;
                        Obj2.IsQuestionFreeze = true;
                        Obj2.IsQuestionFreezeDate = DateTime.Now;
                        Obj2.QuestionStatus = (int)Questionstatus.QuestionSent;
                        Obj2.IsSubmitted = true;
                        Obj2.SubmittedDate = DateTime.Now;
                        Obj2.SubmittedBy = Obj.SubmittedBy;
                        Obj2.QuestionNumber = Obj.QuestionNumber;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Freeze !";
                    }

                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            DiaCtx.Close();

            return msg;

        }

        public static object SaveNotice(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            tMemberNotice Obj2 = new tMemberNotice();
            string msg = "";
            Obj2 = DiaCtx.ObjNotices.Single(m => m.NoticeId == Obj.NoticeId);
            DiaCtx.ObjNotices.Attach(Obj2);
            try
            {

                if (Obj.ViewType == "NInbox")
                {
                    if (Obj.BtnCaption == "Save")
                    {
                        Obj2.NoticeTypeID = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Saved !";
                    }
                    else if (Obj.BtnCaption == "Send")
                    {
                        Obj2.NoticeTypeID = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        Obj2.IsNoticeFreeze = true;
                        Obj2.IsNoticeFreezeDate = DateTime.Now;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticeSent;
                        Obj2.IsSubmitted = true;
                        Obj2.SubmittedDate = DateTime.Now;
                        Obj2.SubmittedBy = Obj.SubmittedBy;
                        Obj2.Actice = true;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Sent !";
                    }

                }
                else if (Obj.ViewType == "NDraft")
                {
                    if (Obj.BtnCaption == "Save")
                    {
                        Obj2.NoticeTypeID = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticePending;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Saved !";
                    }
                    else if (Obj.BtnCaption == "Send")
                    {
                        Obj2.NoticeTypeID = Obj.EventId;
                        Obj2.MinistryId = Obj.MinistryId;
                        Obj2.MinisterName = Obj.MinistryName;
                        Obj2.DepartmentId = Obj.DepartmentId;
                        Obj2.DepartmentName = Obj.DepartmentName;
                        Obj2.MemberId = Obj.MemId;
                        Obj2.Subject = Obj.Subject;
                        Obj2.Notice = Obj.Notice;
                        Obj2.IsNoticeFreeze = true;
                        Obj2.IsNoticeFreezeDate = DateTime.Now;
                        Obj2.NoticeStatus = (int)NoticeStatusEnum.NoticeSent;
                        Obj2.IsSubmitted = true;
                        Obj2.SubmittedDate = DateTime.Now;
                        Obj2.SubmittedBy = Obj.SubmittedBy;
                        Obj2.Actice = true;
                        DiaCtx.SaveChanges();
                        msg = "Successfully Freeze !";
                    }

                }
            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            DiaCtx.Close();

            return msg;

        }

        public static object GetQuestionDetailById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();

            var record = (from Ques in DiaCtx.objQuestion
                          join sdt in DiaCtx.objSessDate on Ques.SessionDateId equals sdt.Id
                          join evt in DiaCtx.objmEvent on Ques.EventId equals evt.EventId
                          join Mem in DiaCtx.objmMember on Ques.MemberID equals Mem.MemberCode

                          where Ques.QuestionID == Obj.QuestionId
                          select new DiaryModel
                          {
                              QuestionId = Ques.QuestionID,
                              QuestionTypeId = Ques.QuestionType,
                              DiaryNumber = Ques.DiaryNumber,
                              QuestionNumber = Ques.QuestionNumber,
                              MemberName = Mem.Name,
                              ConstituencyName = Ques.ConstituencyName,
                              EventName = evt.EventName,
                              CatchWordSubject = Ques.SubInCatchWord,
                              Subject = Ques.Subject,
                              MinistryId = Ques.MinistryId,
                              DepartmentId = Ques.DepartmentID,
                              MainQuestion = Ques.MainQuestion,
                              NoticeDate = sdt.SessionDate,

                          }).FirstOrDefault();

            var MinMisList = "";
            var Dept = "";
            if (record.MinistryId != null && record.MinistryId != 0)
            {
                MinMisList = (from Min in DiaCtx.objmMinistry
                              where Min.MinistryID == record.MinistryId
                              select Min.MinisterName).FirstOrDefault();

            }


            if (record.DepartmentId != null && record.DepartmentId != "")
            {
                Dept = (from dept in DiaCtx.objmDepartment
                        where dept.deptId == record.DepartmentId
                        select dept.deptname).FirstOrDefault();
            }

            Obj2 = record;
            Obj2.MinisterName = MinMisList.ToString().Trim();
            Obj2.DepartmentName = Dept.ToString().Trim();
            DiaCtx.Close();

            return Obj2;
        }
        public static object GetNoticeDetailById(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            SessionContext SCtx = new SessionContext();
            DiaryModel Obj = param as DiaryModel;

            DiaryModel Obj2 = new DiaryModel();

            var record = (from Ntces in DiaCtx.ObjNotices
                          where Ntces.NoticeId == Obj.NoticeId
                          join evt in DiaCtx.objmEvent on Ntces.NoticeTypeID equals evt.EventId
                          join Mem in DiaCtx.objmMember on Ntces.MemberId equals Mem.MemberCode
                          select new DiaryModel
                          {
                              NoticeNumber = Ntces.NoticeNumber,
                              MemberName = Mem.Name,
                              EventName = evt.EventName,
                              CatchWordSubject = Ntces.SubInCatchWord,
                              Subject = Ntces.Subject,
                              MinistryId = Ntces.MinistryId,
                              DepartmentId = Ntces.DepartmentId,
                              DateForBind = Ntces.NoticeDate,
                              TimeForBind = Ntces.NoticeTime,
                              NoticeId = Ntces.NoticeId,
                              Notice = Ntces.Notice,
                          }).FirstOrDefault();
            var MinMisList = "";
            var Dept = "";
            if (record.MinistryId != null && record.MinistryId != 0)
            {
                MinMisList = (from Min in DiaCtx.objmMinistry
                              where Min.MinistryID == record.MinistryId
                              select Min.MinisterName).FirstOrDefault();

            }


            if (record.DepartmentId != null && record.DepartmentId != "")
            {
                Dept = (from dept in DiaCtx.objmDepartment
                        where dept.deptId == record.DepartmentId
                        select dept.deptname).FirstOrDefault();
            }

            Obj2 = record;
            Obj2.MinisterName = MinMisList.ToString().Trim();
            Obj2.DepartmentName = Dept.ToString().Trim();
            DiaCtx.Close();


            return Obj2;
        }

        public static object CheckQuestionNo(object param)
        {

            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel updateSQ = param as DiaryModel;

            var query = (from tQ in DiaCtx.objQuestion
                         where tQ.QuestionNumber == updateSQ.QuestionNumber
                         && (tQ.AssemblyID == updateSQ.AssemblyID)
                         && (tQ.QuestionType == updateSQ.QuestionTypeId)
                         select tQ).ToList().Count();

            return query.ToString();
        }


        public static object GetAllDiariedRecord(object param)
        {
            DiariesContext DiaCtx = new DiariesContext();
            DiaryModel Obj = param as DiaryModel;
            DiaryModel Obj2 = new DiaryModel();

            if (Obj.PaperEntryType == "Starred")
            {
                IEnumerable<DiaryModel> starredDiaried = (from Ques in DiaCtx.objQuestion
                                                          join mem in DiaCtx.objmMember on Ques.MemberID equals mem.MemberCode
                                                          where (Ques.AssemblyID == Obj.AssemblyID)
                                                          && (Ques.SessionID == Obj.SessionID)
                                                          && (Ques.QuestionType == (int)QuestionType.StartedQuestion)
                                                          && (Ques.DiaryNumber != null)
                                                          && (Ques.QuestionStatus != 0)
                                                          orderby Ques.QuestionID
                                                          select new DiaryModel
                                                          {
                                                              DiaryNumber = Ques.DiaryNumber,
                                                              MemberId = Ques.MemberID,
                                                              //AskedBy = mem.Name,
                                                              DateForBind = Ques.NoticeRecievedDate,
                                                              QuestionId = Ques.QuestionID,
                                                          }).ToList();

                foreach (var item in starredDiaried)
                {
                    if (item.MemberId != 0)
                    {
                        var Member = (from Mem in DiaCtx.objmMember
                                      where Mem.MemberCode == item.MemberId
                                      select Mem).FirstOrDefault();

                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
                    }
                }
                var results = starredDiaried.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();
                Obj2.DiaryList = results.ToList();
                Obj2.ResultCount = starredDiaried.ToList().Count();
                DiaCtx.Close();
                return Obj2;
            }
            else if (Obj.PaperEntryType == "Unstarred")
            {
                IEnumerable<DiaryModel> UnstarredDiaried = (from Ques in DiaCtx.objQuestion
                                                            join mem in DiaCtx.objmMember on Ques.MemberID equals mem.MemberCode
                                                            where (Ques.AssemblyID == Obj.AssemblyID)
                                                            && (Ques.SessionID == Obj.SessionID)
                                                            && (Ques.QuestionType == (int)QuestionType.UnstaredQuestion)
                                                            && (Ques.DiaryNumber != null)
                                                             && (Ques.QuestionStatus != 0)
                                                            orderby Ques.QuestionID
                                                            select new DiaryModel
                                                            {
                                                                DiaryNumber = Ques.DiaryNumber,
                                                                MemberId = Ques.MemberID,
                                                                //AskedBy = mem.Name,
                                                                DateForBind = Ques.NoticeRecievedDate,
                                                                QuestionId = Ques.QuestionID,
                                                            }).ToList();
                foreach (var item in UnstarredDiaried)
                {
                    if (item.MemberId != 0)
                    {
                        var Member = (from Mem in DiaCtx.objmMember
                                      where Mem.MemberCode == item.MemberId
                                      select Mem).FirstOrDefault();

                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
                    }
                }

                var results = UnstarredDiaried.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();
                Obj2.DiaryList = results.ToList();
                Obj2.ResultCount = UnstarredDiaried.ToList().Count();
                DiaCtx.Close();
                return Obj2;
            }
            else if (Obj.PaperEntryType == "Notice")
            {
                IEnumerable<DiaryModel> NoticeDiaried = (from Ntces in DiaCtx.ObjNotices
                                                         join mem in DiaCtx.objmMember on Ntces.MemberId equals mem.MemberCode
                                                         where (Ntces.AssemblyID == Obj.AssemblyID)
                                                         && (Ntces.SessionID == Obj.SessionID)
                                                         && (Ntces.NoticeNumber != null)
                                                          && (Ntces.NoticeStatus != 0)
                                                         orderby Ntces.NoticeId
                                                         select new DiaryModel
                                                         {
                                                             NoticeNumber = Ntces.NoticeNumber,
                                                             MemId = Ntces.MemberId,
                                                             //AskedBy = mem.Name,
                                                             DateForBind = Ntces.NoticeDate,
                                                             NoticeId = Ntces.NoticeId,
                                                         }).ToList();

                foreach (var item in NoticeDiaried)
                {
                    if (item.MemId != 0 && item.MemId != null)
                    {
                        var Member = (from Mem in DiaCtx.objmMember
                                      where Mem.MemberCode == item.MemId
                                      select Mem).FirstOrDefault();

                        item.AskedBy = Member.Prefix.ToString().Trim() + " " + Member.Name.ToString().Trim();
                    }
                }


                var results = NoticeDiaried.Skip((Obj.PageIndex - 1) * Obj.PageSize).Take(Obj.PageSize).ToList();
                Obj2.DiaryList = results.ToList();
                Obj2.ResultCount = NoticeDiaried.ToList().Count();
                DiaCtx.Close();
                return Obj2;
            }
            return null;
        }

        #endregion




    
    }




}
