﻿
using SBL.DAL;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.PaperLaid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.Event
{
    public class EventContext : DBBase<EventContext>
    {
        public EventContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public virtual DbSet<mEvent> mEvents { get; set; }

        public virtual DbSet<mPaperCategoryType> mPaperCategoryTypes { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {

            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {

                case "GetAllEventsList": { return GetAllEventsList(); }
                case "CreateEvents": { CreateEvents(param.Parameter); break; }
                case "UpdateEvents": { return UpdateEvents(param.Parameter); }
                case "DeleteEvents": { return DeleteEvents(param.Parameter); }
                case "GetEventBasedOnId": { return GetEventBasedOnId(param.Parameter); }
                case "GetAllPaperCatTypes": { return GetAllPaperCatTypes(); }

                case "GetAllEvents":
                    {
                        return GetAllEventsDetails(param.Parameter);
                    }
                case "GetPaperCategoryID":
                    {
                        return GetPaperCategoryID(param.Parameter);
                    }
                case "GetEventsByPaperCategoryID":
                    {
                        return GetEventsByPaperCategoryID(param.Parameter);
                    }
                case "GetEventsList":
                    {
                        return GetEventsList(param.Parameter);
                    }
                case "GetEventsById":
                    {
                        return GetEventsById(param.Parameter);
                    }
                case "GetAllNoticeEventsDetails":
                    {
                        return GetAllNoticeEventsDetails(param.Parameter);
                    }
                case "GetQuestionEvent":
                    {
                        return GetAllQuestionEvents(param.Parameter);

                    }
                case "GetEventsListByPaperCategoryID":
                    {
                        return GetEventsListByPaperCategoryID(param.Parameter);
                    }

            } return null;
        }
        //For mEvent Masterform
        static List<mPaperCategoryType> GetAllPaperCatTypes()
        {
            try
            {
                using (EventContext db = new EventContext())
                {
                    var data = (from p in db.mPaperCategoryTypes
                                orderby p.Name ascending
                                where p.IsDeleted == null
                                select p).ToList();
                    return data;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static List<mEvent> GetAllEventsList()
        {
            try
            {
                using (EventContext db = new EventContext())
                {
                    var data = (from events in db.mEvents
                                join Papcat in db.mPaperCategoryTypes on events.PaperCategoryTypeId equals Papcat.PaperCategoryTypeId into JoinPapName
                                from papcate in JoinPapName.DefaultIfEmpty()
                                where events.IsDeleted == null
                                select new
                                {
                                    events,
                                    papcate.Name
                                });
                    List<mEvent> list= new List<mEvent>();
                    foreach (var item in data)
                    {
                        item.events.GetPapCatName = item.Name;
                        list.Add(item.events);
                    }
                    return list.OrderByDescending(e => e.EventId).ToList();
                   
                }


            }
            catch (System.Exception ex)
            {

                throw ex;
            }

        }

        static void CreateEvents(object param)
        {
            try
            {
                using (EventContext db = new EventContext())
                {
                    mEvent model = param as mEvent;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mEvents.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch (System.Exception ex)
            {

                throw ex;
            }
        }

        static object UpdateEvents(object param)
        {
            try
            {
                using (EventContext db = new EventContext())
                {
                    mEvent model = param as mEvent;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mEvents.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();


                }

                return GetAllEventsList();

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        static object DeleteEvents(object param)
        {
            try
            {
                using (EventContext db = new EventContext())
                {
                    mEvent model = param as mEvent;
                    var data = db.mEvents.SingleOrDefault(q => q.EventId == model.EventId);
                    if(data!=null)
                    {
                        data.IsDeleted = true;
                    }
                    //db.mEvents.Remove(data);
                    db.SaveChanges();
                    db.Close();
                }
                return GetAllEventsList();

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        static object GetEventBasedOnId(object param)
        {
            try
            {
                using (EventContext db = new EventContext())
                {
                    mEvent model = param as mEvent;
                    var data = db.mEvents.SingleOrDefault(q => q.EventId == model.EventId);
                    return data;

                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        //*************End mEvent Masterform******************

        static List<mEvent> GetEventsByPaperCategoryID(object param)
        {
            EventContext db = new EventContext();
            mEvent model = param as mEvent;
            var query = (from mdl in db.mEvents
                         where mdl.PaperCategoryTypeId == model.PaperCategoryTypeId && model.PaperCategoryTypeId != 3
                         select mdl);
            return query.ToList();
        }

        static List<mEvent> GetAllEventsDetails(object param)
        {
            EventContext db = new EventContext();
            mEvent model = param as mEvent;
            var query = (from dist in db.mEvents
                         where dist.PaperCategoryTypeId == 3
                         select dist);
            string searchText = (model.EventName != null && model.EventName != "") ? model.EventName.ToLower() : "";
            if (searchText != "")
            {
                query = query.Where(a =>
                    a.EventName != null && a.EventName.ToLower().Contains(searchText));

            }

            return query.ToList();
        }

        static List<mEvent> GetAllNoticeEventsDetails(object param)
        {
            EventContext db = new EventContext();
            mEvent model = param as mEvent;
            var query = (from dist in db.mEvents
                         where dist.PaperCategoryTypeId == 4
                         select dist);
            string searchText = (model.EventName != null && model.EventName != "") ? model.EventName.ToLower() : "";
            if (searchText != "")
            {
                query = query.Where(a =>
                    a.EventName != null && a.EventName.ToLower().Contains(searchText));

            }

            return query.ToList();
        }

        static List<mEvent> GetAllQuestionEvents(object param)
        {
            EventContext db = new EventContext();
            mEvent model = param as mEvent;
            var query = (from dist in db.mEvents
                         where dist.PaperCategoryTypeId == 1
                         select dist);
            string searchText = (model.EventName != null && model.EventName != "") ? model.EventName.ToLower() : "";
            if (searchText != "")
            {
                query = query.Where(a =>
                    a.EventName != null && a.EventName.ToLower().Contains(searchText));

            }

            return query.ToList();
        }

        static object GetPaperCategoryID(object param)
        {
            EventContext db = new EventContext();
            mEvent model = param as mEvent;
            var query = (from mdl in db.mEvents
                         where mdl.EventId == model.EventId
                         select mdl.PaperCategoryTypeId).SingleOrDefault();


            return query;
        }

        static object GetEventsList(object param)
        {
            EventContext db = new EventContext();
            var Events = (from events in db.mEvents orderby events.EventName select events).ToList();
            return Events;
        }

        static object GetEventsById(object param)
        {
            mEvent model = param as mEvent;
            EventContext db = new EventContext();
            var Events = (from events in db.mEvents where events.EventId == model.EventId orderby events.EventName select events).FirstOrDefault();
            return Events;
        }


        static List<mEvent> GetEventsListByPaperCategoryID(object param)
        {
            EventContext db = new EventContext();
            mEvent model = param as mEvent;
            var query = (from mdl in db.mEvents
                         where mdl.PaperCategoryTypeId == model.PaperCategoryTypeId orderby mdl.EventId descending
                         select mdl);
            return query.ToList();
        }

    }
}
