﻿using SBL.DAL;
using SBL.DomainModel.Models.Event;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Event
{
        public class SubEventContext : DBBase<SubEventContext>
        
    {
        public SubEventContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mSubEvents> mSubEvents { get; set; }
        
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateSubEvent": { CreateSubEvent(param.Parameter); break; }

                case "UpdateSubEvent": { return UpdateSubEvent(param.Parameter); }
                case "DeleteSubEvent": { return DeleteSubEvent(param.Parameter); }
                case "GetAllSubEvent": { return GetAllSubEvent(); }
                case "GetSubEventById": { return GetSubEventById(param.Parameter); }
                
            }
            return null;
        }


        static void CreateSubEvent(object param)
        {
            try
            {
                using (SubEventContext db = new SubEventContext())
                {
                    mSubEvents model = param as mSubEvents;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mSubEvents.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateSubEvent(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SubEventContext db = new SubEventContext())
            {
                mSubEvents model = param as mSubEvents;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mSubEvents.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllSubEvent();
        }

        static object DeleteSubEvent(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SubEventContext db = new SubEventContext())
            {
                mSubEvents parameter = param as mSubEvents;
                mSubEvents stateToRemove = db.mSubEvents.SingleOrDefault(a => a.SubEventId == parameter.SubEventId);
                if (stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.mStates.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllSubEvent();
        }

        static List<mSubEvents> GetAllSubEvent()
        {
            SubEventContext db = new SubEventContext();

            //var query = db.mStates.ToList();
            var query = (from a in db.mSubEvents
                         orderby a.SubEventId descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mSubEvents GetSubEventById(object param)
        {
            mSubEvents parameter = param as mSubEvents;
            SubEventContext db = new SubEventContext();
            var query = db.mSubEvents.SingleOrDefault(a => a.SubEventId == parameter.SubEventId);
            return query;
        }



    }
}
