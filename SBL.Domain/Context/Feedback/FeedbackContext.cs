﻿using SBL.DAL;
using SBL.DomainModel.Models.ContactUs;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Feedback
{
    public class FeedbackContext : DBBase<FeedbackContext>
        
    {
        public FeedbackContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<ContactUs> ContactUs { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                
                case "GetAllFeedback": { return GetAllFeedback(); }
                case "GetFeedbackById":
                    {
                        return GetFeedbackById(param.Parameter);
                    }
                case "DeleteFeedback":
                    {
                        return DeleteFeedback(param.Parameter);
                    }
            }
            return null;
        }
        static List<ContactUs> GetAllFeedback()
        {
            FeedbackContext db = new FeedbackContext();

            // var query = db.mParty.ToList();
            var query = (from a in db.ContactUs
                         orderby a.Id descending
                         
                         select a).ToList();

            return query;
        }

        static ContactUs GetFeedbackById(object param)
        {
            ContactUs Mdl = param as ContactUs;
            FeedbackContext db = new FeedbackContext();
            var query = db.ContactUs.SingleOrDefault(a => a.Id == Mdl.Id);
            return query;
        }

        static object DeleteFeedback(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (FeedbackContext db = new FeedbackContext())
            {
                ContactUs parameter = param as ContactUs;
                ContactUs sessionToRemove = db.ContactUs.SingleOrDefault(a => a.Id == parameter.Id);
                
                db.ContactUs.Remove(sessionToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllFeedback();
        }

    }
}
