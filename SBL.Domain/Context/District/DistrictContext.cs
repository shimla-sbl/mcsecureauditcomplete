﻿using SBL.DAL;
using SBL.DomainModel.Models.States;
using SBL.DomainModel.Models.District;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.Member;

namespace SBL.Domain.Context.District
{
    public class DistrictContext : DBBase<DistrictContext>
    {

        public DistrictContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<DistrictModel> DistrictModel { get; set; }
        public DbSet<mMember> mMember { get; set; }
        public DbSet<mStates> mStates { get; set; }
        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateDistrict": { CreateDistrict(param.Parameter); break; }

                case "UpdateDistrict": { return UpdateDistrict(param.Parameter); }
                case "DeleteDistrict": { return DeleteDistrict(param.Parameter); }
                case "GetAllDistrict": { return GetAllDistrict(); }
                case "GetDistrictById": { return GetDistrictById(param.Parameter); }
                case "GetAllState": { return GetAllState(); }
                case "IsDistrictChildExist": { return IsDistrictChildExist(param.Parameter); }
                case "GetAllDistrictIDs": { return GetAllDistrictIDs(); }
            }
            return null;
        }

        private static object IsDistrictChildExist(object param)
        {
            try
            {
                using (DistrictContext db = new DistrictContext())
                {
                    DistrictModel model = (DistrictModel)param;
                    var Res = (from e in db.mMember
                               where (e.DistrictID == model.DistrictId)
                               select e).Count();

                    if (Res == 0)
                    {
                        var MemAssem = (from m in db.mMemberAssembly
                                        where (m.Location == model.DistrictName)
                                        select m).Count();
                        if (MemAssem == 0)
                        {
                            return false;


                        }


                    }

                    else
                    {
                        return true;

                    }

                }
                return true;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }

        static void CreateDistrict(object param)
        {
            try
            {
                using (DistrictContext db = new DistrictContext())
                {
                    DistrictModel model = param as DistrictModel;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.DistrictModel.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateDistrict(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DistrictContext db = new DistrictContext())
            {
                DistrictModel model = param as DistrictModel;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.DistrictModel.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllDistrictIDs();
        }

        static object DeleteDistrict(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DistrictContext db = new DistrictContext())
            {
                DistrictModel parameter = param as DistrictModel;
                DistrictModel stateToRemove = db.DistrictModel.SingleOrDefault(a => a.DistrictId == parameter.DistrictId);
                if (stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
                //db.DistrictModel.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllDistrictIDs();
        }

        static List<DistrictModel> GetAllDistrict()
        {
            DistrictContext db = new DistrictContext();

            //var query = db.DistrictModel.ToList();
            var query = (from a in db.DistrictModel
                         orderby a.DistrictName 
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static DistrictModel GetDistrictById(object param)
        {
            DistrictModel parameter = param as DistrictModel;
            DistrictContext db = new DistrictContext();
            var query = db.DistrictModel.SingleOrDefault(a => a.DistrictId == parameter.DistrictId);
            return query;
        }

        static List<mStates> GetAllState()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mStates
                        orderby m.StateAbbreviation ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;

        }



        static List<DistrictModel> GetAllDistrictIDs()
        {
            DistrictContext db = new DistrictContext();
            var data = (from sect in db.DistrictModel
                        join m in db.mStates on sect.StateCode equals m.mStateID into joinStateAbbreviation
                        from n in joinStateAbbreviation.DefaultIfEmpty()
                        where sect.IsDeleted==null
                        //join district in db.Districts on consti.DistrictCode equals district.DistrictId into joinDistrictName
                        //from distr in joinDistrictName.DefaultIfEmpty()
                        select new
                        {
                            sect,
                            n.StateAbbreviation,
                            //distr.DistrictCode

                        }).ToList();
            //var data1=
            List<DistrictModel> list = new List<DistrictModel>();
            foreach (var item in data)
            {
                item.sect.GetStateName = item.StateAbbreviation;
                //item.consti.GetDistrictName = item.DistrictCode;
                list.Add(item.sect);

            }
            return list.OrderByDescending(c => c.DistrictId).ToList();
        }




    }
}
