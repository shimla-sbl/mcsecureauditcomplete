﻿namespace SBL.Domain.Context.Department
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SBL.DAL;
    using System.Data.Entity;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.PaperLaid;
    using SBL.DomainModel.Models.Secretory;
    using SBL.DomainModel.Models.Employee;
    using SBL.DomainModel.Models.Ministery;
    using SBL.DomainModel.Models.Question;

    using SBL.Service.Common;
    using System.Data;
    using System.Text;
    using System.Threading.Tasks;
    using SBL.DomainModel.Models.DeptRegisterModel;
    using SBL.DomainModel.Models.User;
    using SBL.DomainModel.Models.Grievance;
    using SBL.DomainModel.Models.Office;
    using SBL.DomainModel.Models.PISModules;

    #endregion

    public class DepartmentContext : DBBase<DepartmentContext>
    {
        public DepartmentContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mDepartment> objmDepartment { get; set; }
        public DbSet<mSecretoryDepartment> SecDept { get; set; }
        public virtual DbSet<mSecretory> mSecretory { get; set; }
        public virtual DbSet<mDesignation> mDesignation { get; set; }
        public virtual DbSet<mMinistryDepartment> mMinistryDepartments { get; set; }
        public virtual DbSet<mEmployee> mEmployee { get; set; }
        public virtual DbSet<tGrievanceOfficerDeleted> tGrievanceOfficerDeleted { get; set; }
        public virtual DbSet<tDepartmentOfficeMapped> tDepartmentOfficeMapped { get; set; }
        public virtual DbSet<mOffice> mOffice { get; set; }
        public virtual DbSet<tPISEmployeePersonal> tPISEmployeePersonal { get; set; }
        
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetDepartment":
                    {
                        return GetDepartment();
                    }
                case "GetDepartmentByAadharId":
                    {
                        return GetDepartmentByAadharId(param.Parameter);
                    }
                case "GetAllMinistryDepartment":
                    {
                        return GetAllMinistryDepartment(param.Parameter);
                    }

                case "GetChangeDepartment":
                    {
                        return GetChangeDepartment();
                    }
                case "GetDepartmentSorted":
                    {
                        return GetDepartmentSorted();
                    }
                case "AddDepartment":
                    {
                        return AddDepartment(param.Parameter);
                    }
                case "EditDepartment":
                    {
                        return EditDepartment(param.Parameter);
                    }
                case "UpdateDepartment":
                    {
                        return UpdateDepartment(param.Parameter);
                    }
                case "DeleteDepartment":
                    {
                        return DeleteDepartment(param.Parameter);
                    }
                case "GetDepartmentByID":
                    {
                        return GetDepartmentByID(param.Parameter);
                    }

                case "GetDepartmentByIDs":
                    {
                        return GetDepartmentByIDs(param.Parameter);
                    }

                case "GetDepartmentByIDsLocal":
                    {
                        return GetDepartmentByIDsLocal(param.Parameter);
                    }

                case "GetDepartmentByMultipleIDs":
                    {
                        return GetDepartmentByMultipleIDs(param.Parameter);
                    }
                case "GetDepartmentDetailsById":
                    {
                        return GetDepartmentDetailsById(param.Parameter);
                    }


                case "CreateDepartment": { CreateDepartment(param.Parameter); break; }

                case "UpdDepartment": { return UpdDepartment(param.Parameter); }
                case "DelDepartment": { return DelDepartment(param.Parameter); }
                case "GetAllDepartment": { return GetAllDepartment(); }
                case "GetDepartmentById": { return GetDepartmentById(param.Parameter); }
                case "CheckId":
                    {
                        return CheckId(param.Parameter);
                    }
                case "IsDepartmentIdExist":
                    {
                        return IsDepartmentIdExist(param.Parameter);
                    }
                case "GetReportDepartmentByIDs":
                    {
                        return GetReportDepartmentByIDs(param.Parameter);
                    }
                case "GetDeptNameById":
                    {
                        return GetDeptNameById(param.Parameter);
                    }
                case "GetOfficeByAadharId":
                    {
                        return GetOfficeByAadharId(param.Parameter);
                    }

                case "DeleteIsDepartmentIdExist":
                    {
                        return DeleteIsDepartmentIdExist(param.Parameter);
                    }
                case "GetOfficesList":
                    {
                        return GetOfficesList(param.Parameter);
                    }
                case "GetDepartmentNameById":
                    {
                        return GetDepartmentNameById(param.Parameter);
                    }              
                    
            }
            return null;
        }
        static object GetDepartment()
        {
            try
            {
                DepartmentContext deptContext = new DepartmentContext();
                //List<mDepartment> mDepartment = (from dept in deptContext.objmDepartment select dept).OrderBy(x => x.deptname).ToList();
                //return mDepartment;

                //mDepartment model = param as mDepartment;
                List<mDepartment> newmodel = new List<mDepartment>();

                var data = (from mdl in deptContext.objmDepartment where mdl.deptname.Contains(">") select mdl).OrderBy(z => z.deptname).ToList();
                var data2 = (from mdl in deptContext.objmDepartment where !mdl.deptname.Contains(">") select mdl).OrderBy(z => z.deptname).ToList();
                newmodel = data2.ToList<mDepartment>();
                    foreach (var session in data)
                    {
                        newmodel.Add(new mDepartment()
                        {
                            deptname = session.deptname,
                            deptId = session.deptId
                        });
                    }

                    var retdata = newmodel;

                    return retdata;
               
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetDepartmentByAadharId(object param)
        {
            try
            {
                string AadharId = param as string;


                DepartmentContext Context = new DepartmentContext();
                List<mDepartment> mDepartment = (from dept in Context.objmDepartment
                                                 join DeptOM in Context.tDepartmentOfficeMapped on dept.deptId equals DeptOM.DeptId
                                                 join FW in Context.tGrievanceOfficerDeleted on DeptOM.MemberCode equals FW.MemberCode
                                                 where FW.AdharId == AadharId
                                                 select dept).Distinct().OrderBy(x => x.deptname).ToList();

                return mDepartment;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static object GetOfficeByAadharId(object param)
        {
            try
            {
                //string AadharId = param as string;

                tPISEmployeePersonal model = param as tPISEmployeePersonal;
                if (model.POfficeId != null || model.POfficeId != 0)
                {
                    DepartmentContext Context = new DepartmentContext();
                    List<mOffice> officelist = (from dept in Context.objmDepartment
                                                join DeptOM in Context.tDepartmentOfficeMapped on dept.deptId equals DeptOM.DeptId
                                                join FW in Context.tGrievanceOfficerDeleted on DeptOM.MemberCode equals FW.MemberCode
                                                join off in Context.mOffice on DeptOM.OfficeId equals off.OfficeId
                                                where FW.AdharId == model.AadharID && DeptOM.DeptId == model.deptid && off.OfficeId == model.POfficeId
                                                select off).Distinct().OrderBy(x => x.officename).ToList();

                    return officelist;
                }
                else   if (model.POfficeId != null || model.POfficeId != 0)
                {
                    DepartmentContext Context = new DepartmentContext();
                    List<mOffice> officelist = (from dept in Context.objmDepartment
                                                join DeptOM in Context.tDepartmentOfficeMapped on dept.deptId equals DeptOM.DeptId
                                               // join FW in Context.tGrievanceOfficerDeleted on DeptOM.MemberCode equals FW.MemberCode
                                                join off in Context.mOffice on DeptOM.OfficeId equals off.OfficeId
                                                where DeptOM.DeptId == model.deptid && off.OfficeId == model.POfficeId
                                                select off).Distinct().OrderBy(x => x.officename).ToList();

                    return officelist;
                }
                else
                {
                    DepartmentContext Context = new DepartmentContext();
                    List<mOffice> officelist = (from dept in Context.objmDepartment
                                                join DeptOM in Context.tDepartmentOfficeMapped on dept.deptId equals DeptOM.DeptId
                                                join FW in Context.tGrievanceOfficerDeleted on DeptOM.MemberCode equals FW.MemberCode
                                                join off in Context.mOffice on DeptOM.OfficeId equals off.OfficeId
                                                where FW.AdharId == model.AadharID && DeptOM.DeptId == model.deptid 
                                                select off).Distinct().OrderBy(x => x.officename).ToList();

                    return officelist;
                }
              
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static object GetChangeDepartment()
        {

            try
            {
                DepartmentContext deptContext = new DepartmentContext();
                List<mDepartment> mDepartment = (from dept in deptContext.objmDepartment
                                                 join mSecDept in deptContext.SecDept on dept.deptId equals mSecDept.DepartmentID
                                                 select dept).OrderByDescending(x => x.deptId).ToList();
                return mDepartment;
            }
            catch (Exception ex)
            {
            }
            return null;
        }


        static object GetDepartmentSorted()
        {
            try
            {
                DepartmentContext deptContext = new DepartmentContext();
                List<mDepartment> mDepartment = (from dept in deptContext.objmDepartment select dept).OrderBy(x => x.deptname).ToList();
                return mDepartment;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetAllMinistryDepartment(object param)
        {
            int? assemblyId = param as int?;
            DepartmentContext db = new DepartmentContext();
            var result = (from s in db.mMinistryDepartments
                          join prod in db.objmDepartment on s.DeptID equals prod.deptId
                          orderby prod.deptname ascending

                          where s.AssemblyID == assemblyId
                          select new SearchModel
                          {
                              Department_Id = s.MinistryDepartmentsID,
                              DepartmentName = prod.deptname.Trim(),
                          }).Distinct().ToList();

            return result;
        }

        static object AddDepartment(object param)
        {
            try
            {
                DepartmentContext deptContext = new DepartmentContext();
                mDepartment model = param as mDepartment;
                deptContext.objmDepartment.Add(model);
                var retVal = deptContext.SaveChanges();
                deptContext.Close();
                return retVal;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static object GetDeptNameById(object param)
        {

            DepartmentContext deptContext = new DepartmentContext();
            mUsers model = param as mUsers;
            string DeptName = "";
            foreach (var sr in model.AllDepartmentIds)
            {
                DeptName += (from s in deptContext.objmDepartment where s.deptId == sr select s.deptname).SingleOrDefault() + ",";
            }
           
            DeptName = DeptName.TrimEnd(',');
            return DeptName;

        }
        static object EditDepartment(object param)
        {
            try
            {
                DepartmentContext deptContext = new DepartmentContext();
                mDepartment model = param as mDepartment;
                string deptId = model.deptId;
                model = (from dept in deptContext.objmDepartment where dept.deptId == deptId select dept).FirstOrDefault();
                return model;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object UpdateDepartment(object param)
        {
            try
            {
                DepartmentContext deptContext = new DepartmentContext();
                mDepartment model = param as mDepartment;
                deptContext.Entry(model).State = EntityState.Modified;
                var resultVal = deptContext.SaveChanges();
                deptContext.Close();
                return resultVal;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetDepartmentByID(object param)
        {
            DepartmentContext deptContext = new DepartmentContext();
            mDepartment model = param as mDepartment;
            string deptId = model.deptId;
            model = (from dept in deptContext.objmDepartment where dept.deptId == deptId select dept).FirstOrDefault();

            return model;
        }




        static object GetDepartmentByIDsLocal(object param)
        {
            tPaperLaidV model1 = param as tPaperLaidV;
            DepartmentContext deptContext = new DepartmentContext();
            mDepartment model = param as mDepartment;

            string csv = model1.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            List<mDepartment> deptList = new List<mDepartment>();
            foreach (var deptid in ids)
            {
                var deptDetails = (from dept in deptContext.objmDepartment where dept.deptId == deptid select dept).FirstOrDefault();
                deptList.Add(deptDetails);
            }
            return deptList;
        }

        static object GetDepartmentByIDs(object param)
        {
            tPaperLaidV model1 = param as tPaperLaidV;
            DepartmentContext deptContext = new DepartmentContext();
            mDepartment model = param as mDepartment;

            string csv = model1.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            List<mDepartment> deptList = new List<mDepartment>();
            foreach (var deptid in ids)
            {
                var deptDetails = (from dept in deptContext.objmDepartment where dept.deptId == deptid select dept).FirstOrDefault();
                deptList.Add(deptDetails);
            }
            return deptList;
        }


        static object DeleteDepartment(object param)
        {
            //try
            //{
            //    DepartmentContext deptContext = new DepartmentContext();
            //    mDepartment model = new mDepartment();
            //    model.deptId=(int)param;

            //    deptContext.Entry(model).State = System.Data.EntityState.Deleted;
            //    var resultVal = deptContext.SaveChanges();
            //    deptContext.Close();
            //    return resultVal;
            //}
            //catch (Exception ex)
            //{
            //}
            return null;
        }
        static object GetDepartmentByMultipleIDs(object param)
        {
            DepartmentContext deptContext = new DepartmentContext();
            mDepartment model = param as mDepartment;
            string[] deptId = model.deptId.Split(',');
            ICollection<mDepartment> DeptModel = new List<mDepartment>();
            foreach (string dId in deptId)
            {
                model = (from dept in deptContext.objmDepartment where dept.deptId == dId select dept).FirstOrDefault();
                DeptModel.Add(model);
            }
            return DeptModel;
        }




        static void CreateDepartment(object param)
        {
            try
            {
                using (DepartmentContext db = new DepartmentContext())
                {
                    mDepartment model = param as mDepartment;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.objmDepartment.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdDepartment(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DepartmentContext db = new DepartmentContext())
            {
                mDepartment model = param as mDepartment;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.objmDepartment.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllDepartment();
        }

        static object DelDepartment(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (DepartmentContext db = new DepartmentContext())
            {
                mDepartment parameter = param as mDepartment;
                mDepartment stateToRemove = db.objmDepartment.SingleOrDefault(a => a.deptId == parameter.deptId);
                if (stateToRemove != null)
                {
                    stateToRemove.IsDeleted = true;
                }
                // db.objmDepartment.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllDepartment();
        }

        static List<mDepartment> GetAllDepartment()
        {
            DepartmentContext db = new DepartmentContext();

            //var query = db.objmDepartment.ToList();
            var query = (from a in db.objmDepartment
                         orderby a.deptId descending
                         where a.IsDeleted == null
                         select a).ToList();

            return query;
        }

        static mDepartment GetDepartmentById(object param)
        {
            mDepartment parameter = param as mDepartment;
            DepartmentContext db = new DepartmentContext();
            var query = db.objmDepartment.SingleOrDefault(a => a.deptId == parameter.deptId);
            return query;
        }



        static object CheckId(object param)
        {

            DepartmentContext qCtxDB = new DepartmentContext();
            mDepartment updateSQ = param as mDepartment;

            var query = (from tQ in qCtxDB.objmDepartment
                         where tQ.deptId == updateSQ.deptId
                         select tQ).ToList().Count();

            return query.ToString();
        }

        static object GetDepartmentDetailsById(object param)
        {
            mDepartment parameter = param as mDepartment;
            DepartmentContext db = new DepartmentContext();
            var query = db.objmDepartment.SingleOrDefault(a => a.deptId == parameter.deptId);
            return query;
        }

        private static object IsDepartmentIdExist(object param)
        {
            try
            {
                using (DepartmentContext db = new DepartmentContext())
                {
                    mDepartment model = (mDepartment)param;
                    mSecretory ms = new mSecretory();
                    mSecretoryDepartment msd = new mSecretoryDepartment();

                    var Res = (from e in db.mSecretory
                               where (e.DeptID == model.deptId)
                               select e).Count();


                    if (Res == 0)
                    {
                        var Msd = (from e in db.SecDept
                                   where (e.DepartmentID == model.deptId)
                                   select e).Count();

                        if (Msd == 0)
                        {
                            var Des = (from e in db.mDesignation
                                       where (e.deptid == model.deptId)
                                       select e).Count();

                            if (Des == 0)
                            {
                                var Mmd = (from e in db.mMinistryDepartments
                                           where (e.DeptID == model.deptId)
                                           select e).Count();

                                if (Mmd == 0)
                                {
                                    var Emp = (from e in db.mEmployee
                                               where (e.deptid == model.deptId)
                                               select e).Count();

                                    if (Emp == 0)
                                    {
                                        return false;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        return true;

                    }





                }

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }



        private static object DeleteIsDepartmentIdExist(object param)
        {
            try
            {
                using (DepartmentContext db = new DepartmentContext())
                {
                    mDepartment model = (mDepartment)param;
                    mSecretory ms = new mSecretory();
                    mSecretoryDepartment msd = new mSecretoryDepartment();

                    string[] Ids = model.deptId.Split(',');

                    for (int i = 0; i < Ids.Length; i++)
                    {
                        string id = Ids[i];



                        var Res = (from e in db.mSecretory
                                   where (e.DeptID == id)
                                   select e).Count();


                        if (Res == 0)
                        {
                            var Msd = (from e in db.SecDept
                                       where (e.DepartmentID == id)
                                       select e).Count();

                            if (Msd == 0)
                            {
                                var Des = (from e in db.mDesignation
                                           where (e.deptid == id)
                                           select e).Count();

                                if (Des == 0)
                                {
                                    var Mmd = (from e in db.mMinistryDepartments
                                               where (e.DeptID == id)
                                               select e).Count();

                                    if (Mmd == 0)
                                    {
                                        var Emp = (from e in db.mEmployee
                                                   where (e.deptid == id)
                                                   select e).Count();

                                        if (Emp == 0)
                                        {
                                            return false;
                                        }
                                    }
                                }
                            }
                        }


                        else
                        {
                            return true;

                        }

                    }



                }

                return true;
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }


        static object GetReportDepartmentByIDs(object param)
        {
            DeptRegisterModel model1 = param as DeptRegisterModel;
            DepartmentContext deptContext = new DepartmentContext();
            mDepartment model = param as mDepartment;

            string csv = model1.DepartmentId;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);

            List<mDepartment> deptList = new List<mDepartment>();
            foreach (var deptid in ids)
            {
                var deptDetails = (from dept in deptContext.objmDepartment where dept.deptId == deptid select dept).FirstOrDefault();
                deptList.Add(deptDetails);
            }
            model1.mDepartment = deptList;
            return model1;
        }

        static object GetOfficesList(object param)
        {
            try
            {
                //string AadharId = param as string;

                tPISEmployeePersonal model = param as tPISEmployeePersonal;
                DepartmentContext Context = new DepartmentContext();

                //List<mOffice> officelist = (from dept in Context.objmDepartment
                //                            join DeptOM in Context.tDepartmentOfficeMapped on dept.deptId equals DeptOM.DeptId
                //                            join off in Context.mOffice on DeptOM.OfficeId equals off.OfficeId
                //                            where DeptOM.DeptId == model.deptid //&& off.IsNodalOffice == null
                //                            select off).Distinct().OrderBy(x => x.officename).ToList();
                List<mOffice> officelist = (from dept in Context.objmDepartment
                                            join DeptOM in Context.tDepartmentOfficeMapped on dept.deptId equals DeptOM.DeptId
                                            join FW in Context.tGrievanceOfficerDeleted on DeptOM.MemberCode equals FW.MemberCode
                                            join off in Context.mOffice on DeptOM.OfficeId equals off.OfficeId
                                            where FW.AdharId == model.AadharID && DeptOM.DeptId == model.deptid && off.IsNodalOffice == null
                                            select off).Distinct().OrderBy(x => x.officename).ToList();
            
                return officelist;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static string GetDepartmentNameById(object param)
        {
            string deptName = "";
            string parameter = param as string;
            DepartmentContext db = new DepartmentContext();
            deptName = (from dept in db.objmDepartment where dept.deptId == parameter select dept.deptname).FirstOrDefault();
            return deptName;
        }
    }
}
