﻿using System.Linq;

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SBL.Service.Common;
using System.Data;


namespace SBL.Domain.Context.Secretory
{
    using SBL.DAL;
    using SBL.DomainModel.Models.Secretory;
    using System.Data.Entity;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Assembly;
    public class SecretoryContext : DBBase<SecretoryContext>
    {
        public SecretoryContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        public DbSet<mSecretory> mSecretory { get; set; }
        public DbSet<mSecretoryDepartment> mSecretoryDepartment { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetAllSecratoryDetails":
                    {
                        return GetAllSecratoryDetails(param.Parameter);
                    }
                case "GetSecretaryByDetails":
                    {
                        return GetSecretaryByDetails(param.Parameter);
                    }
                case "GetSecretaryByEmpID":
                    {
                        return GetSecretaryByEmpID(param.Parameter);
                    }
                case "GetSecretoryDetailsById":
                    {
                        return GetSecretoryDetailsById(param.Parameter);
                    }
                case "CheckSecretaryExist":
                    {
                        return CheckSecretaryExist(param.Parameter);
                    }
                case "GetSecretaryByAdharid":
                    {
                        return GetSecretaryByAdharid(param.Parameter);
                    }
                    
                #region ForSecretory
                case "CreateSecretory": { CreateSecretory(param.Parameter); break; }
                case "UpdateSecretory": { return UpdateSecretory(param.Parameter); }
                case "UpdateSecretoryByAdharId": { UpdateSecretoryByAdharId(param.Parameter); break; }
                case "DeleteSecretory": { return DeleteSecretory(param.Parameter); }
                case "GetAllSecretory": { return GetAllSecretory(); }
                case "GetSecretoryById": { return GetSecretoryById(param.Parameter); }
                case "GetAllDepartment": { return GetAllDepartment(); }
                case "IsSecIdChildExist": { return IsSecIdChildExist(param.Parameter); }
                case "GetAllSecretoryIDs": { return GetAllSecretoryIDs(); }
                //*******************************
                #endregion



            } return null;
        }


        private static object IsSecIdChildExist(object param)
        {
            try
            {
                using (SecretoryContext db = new SecretoryContext())
                {
                    mSecretory model = (mSecretory)param;
                    var Res = (from e in db.mSecretoryDepartment
                               where (e.SecretoryID == model.SecretoryID)
                               select e).Count();

                    if (Res == 0)
                    {

                        return false;


                    }

                    else
                    {
                        return true;

                    }

                }
                //return null;


            }


            catch (Exception ex)
            {

                throw ex;
            }

        }

        public static object GetAllSecratoryDetails(object param)
        {
            SecretoryContext SecCxt = new SecretoryContext();
            mSecretory model = param as mSecretory;
            var query = (from mdl in SecCxt.mSecretory select mdl).OrderBy(m => m.SecretoryName).ToList();
            return query;
        }

        public static object GetSecretaryByDetails(object param)
        {
            SecretoryContext SecCxt = new SecretoryContext();
            mSecretory model = param as mSecretory;
            var query = (from mdl in SecCxt.mSecretory where mdl.DeptID == model.DeptID && mdl.EmpCode == model.EmpCode && mdl.IsActive == true select mdl).FirstOrDefault();
            return query;
        }

        static void CreateSecretory(object param)
        {
            try
            {
                using (SecretoryContext db = new SecretoryContext())
                {
                    mSecretory model = param as mSecretory;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    model.DeptID = "HPD0005";
                    db.mSecretory.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateSecretory(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SecretoryContext db = new SecretoryContext())
            {
                mSecretory model = param as mSecretory;
                model.CreationDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mSecretory.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllSecretoryIDs();
        }
        static void UpdateSecretoryByAdharId(object param)
        {
            if (null == param)
            {
                //return null;
            }
            using (SecretoryContext db = new SecretoryContext())
            {
                mSecretory model = param as mSecretory;
                mSecretory modelto = new mSecretory();
                modelto = (from sb in db.mSecretory where sb.AadhaarId == model.AadhaarId select sb).SingleOrDefault();
                modelto.SecretoryName = model.SecretoryName;
                modelto.DesignationDescription = model.DesignationDescription;
                modelto.Email = model.Email;
                modelto.Mobile = model.Mobile;
                modelto.ModifiedWhen = DateTime.Now;

                db.mSecretory.Attach(modelto);
                db.Entry(modelto).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
                //IEnumerable<mSecretoryDepartment> secdept = new List<mSecretoryDepartment>();
                //foreach (var tval in model.AllDepartmentIds)
                //{
                //    secdept = (from sb in db.mSecretoryDepartment where  sb.SecretoryID==modelto.SecretoryID select sb);
                //    foreach (var tr in secdept)
                //    {
                //        mSecretoryDepartment deptmodel = new mSecretoryDepartment();
                        

                //    }
                //}
               

                
            }
            //return GetAllSecretoryIDs();
        }
        static object DeleteSecretory(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SecretoryContext db = new SecretoryContext())
            {
                mSecretory parameter = param as mSecretory;
                mSecretory stateToRemove = db.mSecretory.SingleOrDefault(a => a.SecretoryID == parameter.SecretoryID);
                if(stateToRemove!=null)
                {
                    stateToRemove.IsDeleted = true;
                }
               // db.mSecretory.Remove(stateToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllSecretoryIDs();
        }

        static List<mSecretory> GetAllSecretory()
        {
            SecretoryContext db = new SecretoryContext();

            //var query = db.mSecretory.ToList(); 
            var query = (from a in db.mSecretory
                         orderby a.SecretoryID descending
                         select a).ToList();

            return query;
        }

        static mSecretory GetSecretoryById(object param)
        {
            mSecretory parameter = param as mSecretory;
            SecretoryContext db = new SecretoryContext();
            var query = db.mSecretory.SingleOrDefault(a => a.SecretoryID == parameter.SecretoryID);
            return query;
        }

        static List<mDepartment> GetAllDepartment()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mDepartments
                        orderby m.deptname ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;

        }


        public static object GetSecretaryByEmpID(object param)
        {
            SecretoryContext SecCxt = new SecretoryContext();
            mSecretory model = param as mSecretory;
            var query = (from mdl in SecCxt.mSecretory where mdl.SecretoryID == model.SecretoryID && mdl.IsActive == true select mdl).FirstOrDefault();
            return query;
        }



        static List<mSecretory> GetAllSecretoryIDs()
        {
            SecretoryContext db = new SecretoryContext();
            var data = (from sect in db.mSecretory
                        join m in db.mDepartment on sect.DeptID equals m.deptId into joindeptname
                        from n in joindeptname.DefaultIfEmpty()
                        //join district in db.Districts on consti.DistrictCode equals district.DistrictId into joinDistrictName
                        //from distr in joinDistrictName.DefaultIfEmpty()
                        where sect.IsDeleted==null
                        select new
                        {
                            sect,
                            n.deptname,
                            //distr.DistrictCode

                        }).ToList();
            //var data1=
            List<mSecretory> list = new List<mSecretory>();
            foreach (var item in data)
            {
                item.sect.GetDepartmentName = item.deptname;
                //item.consti.GetDistrictName = item.DistrictCode;
                list.Add(item.sect);

            }
            return list.OrderByDescending(c => c.SecretoryID).ToList();
        }

        public static object GetSecretaryByAdharid(object param)
        {
            SecretoryContext SecCxt = new SecretoryContext();
            mSecretory model = param as mSecretory;
            var query = (from mdl in SecCxt.mSecretory where mdl.AadhaarId == model.AadhaarId select mdl).SingleOrDefault();
            return query;
        }

        public static object GetSecretoryDetailsById(object param)
        {
            mSecretory parameter = param as mSecretory;
            SecretoryContext db = new SecretoryContext();
            var query = db.mSecretory.SingleOrDefault(a => a.SecretoryID == parameter.SecretoryID);
            return query;
        }
        private static object CheckSecretaryExist(object p)
        {
            //SecretoryContext SecCxt = new SecretoryContext();
            mSecretory model = p as mSecretory;
            using (var ctx = new SecretoryContext())
            {
                var isexist = (from usrdschash in ctx.mSecretory where usrdschash.AadhaarId == model.AadhaarId select usrdschash).Count() > 0;

                return isexist;
            }
        }




    }
}
