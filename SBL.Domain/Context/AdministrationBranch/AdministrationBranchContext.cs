﻿namespace SBL.Domain.Context.AdministrationBranch
{
    #region Namespace reffrences.
    using SBL.DAL;
    using SBL.Domain.Context.Assembly;
    using SBL.Domain.Context.Session;
    using SBL.DomainModel.Models.Assembly;
    using SBL.DomainModel.Models.Enums;
    using SBL.DomainModel.Models.Passes;
    using SBL.DomainModel.Models.Session;
    using SBL.Domain.Context.SiteSetting;
    using SBL.DomainModel.ComplexModel.MediaComplexModel;
    using SBL.DomainModel.Models.Media;
    using SBL.Service.Common;
    using System;
    using System.Data;
    using System.Data.Entity;
    using System.Data;
    using System.Linq;
    using System.Xml.Linq;
    using System.Collections.Generic;
    using System.Data.Entity.Core.Objects;


    #endregion

    public class AdministrationBranchContext : DBBase<AdministrationBranchContext>
    {
        #region Constructor Block
        public AdministrationBranchContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        #endregion

        #region Data Table Definition Block
        public virtual DbSet<mPasses> masterPasses { get; set; }
        public DbSet<PassCategory> passCategories { get; set; }
        public DbSet<mDepartmentPasses> mDepartmentPasses { get; set; }
        public virtual DbSet<PublicPass> PublicPasses { get; set; }
        public virtual DbSet<tVerifiedPasses> verifiedPasses { get; set; }
        public virtual DbSet<VisitorData> visitorData { get; set; }
        public virtual DbSet<JournalistsForPass> JournalistsForPass { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
       // public DbSet<mPasses> mPasses { get; set; }
        public DbSet<mSession> mSession { get; set; }
        #endregion

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "CreateNewMasterPass": { return CreateNewMasterPass(param.Parameter); }
                case "CreateNewMasterPassForPublic": { return CreateNewMasterPassForPublic(param.Parameter); }
                case "CreateNewRejectedMasterPass": { return CreateNewRejectedMasterPass(param.Parameter); }

                case "GetMasterPassesByStatus": { return GetMasterPassesByStatus(param.Parameter); }
                case "GetMaxPublicPassCodeForSession": { return GetMaxPublicPassCodeForSession(param.Parameter); }


                case "GetMasterPassesByStatusCount": { return GetMasterPassesByStatusCount(param.Parameter); }

                case "GetMaxPassCodeForSession": { return GetMaxPassCodeForSession(param.Parameter); }
                case "GetMasterPassById": { return GetMasterPassById(param.Parameter); }
                case "GetPassCategoryById": { return GetPassCategoryById(param.Parameter); }

                case "GetDepartmentPassRequests": { return GetDepartmentPassRequests(param.Parameter); }
                case "GetDepartmentPassRequestsCount": { return GetDepartmentPassRequestsCount(param.Parameter); }
                // case "GetDepartmentPassesRequestsCount": { return GetDepartmentPassesRequestsCount(param.Parameter); }
                //case "GetPublicPassesRequestsCount": { return GetPublicPassesRequestsCount(param.Parameter); }
                case "GetPublicDepartmentPassCount": { return GetPublicDepartmentPassCount(param.Parameter); }
                case "GetAdharIdExist": { return GetAdharIdExist(param.Parameter); }
                case "GetDepartmentPassDetails": { return GetDepartmentPassDetails(param.Parameter); }
                case "DeleteMasterPassByID": { return DeleteMasterPassByID(param.Parameter); }

                //
                case "GetDepartmentPassByPassID": { return GetDepartmentPassByPassID(param.Parameter); }

                case "GetReceptionPassRequests": { return GetReceptionPassRequests(param.Parameter); }
                case "GetReceptionPassRequestsCount": { return GetReceptionPassRequestsCount(param.Parameter); }

                case "GetPublicPassesByPassID": { return GetPublicPassesByPassID(param.Parameter); }
                case "UpdateMasterPass": { return UpdateMasterPass(param.Parameter); }

                case "GetSpeakerGalaryPassesByStatus": { return GetSpeakerGalaryPassesByStatus(param.Parameter); }
                case "GetPassesApprovedByDate": { return GetPassesApprovedByDate(param.Parameter); }

                //Helper Methods for Pass Verification application.
                case "GetMasterPassByPassCode": { return GetMasterPassByPassCode(param.Parameter); }

                case "GetVerifiedPassesForSessionDate": { return GetVerifiedPassesForSessionDate(param.Parameter); }
                case "InsertVerifiedPassData": { return InsertVerifiedPassData(param.Parameter); }
                case "GetAdminAssemblyList": { return GetAdminAssemblyList(param.Parameter); }
                case "GetMasterPassesByPassCodeAndSessionCode": { return GetMasterPassesByPassCodeAndSessionCode(param.Parameter); }
                case "GetPassesApprovedReport": { return GetPassesApprovedReport(param.Parameter); }
                case "GetPassesCategoryCount": { return GetPassesCategoryCount(param.Parameter); }
                case "GetPassesDepartmentCount": { return GetPassesDepartmentCount(param.Parameter); }

                case "GetPassesApprovedDeptViewDate": { return GetPassesApprovedDeptViewDate(param.Parameter); }
                case "GetmPassListForDepartmentPrint": { return GetmPassListForDepartmentPrint(param.Parameter); }
                case "GetPublicPassesByAssemblyAndSession": { return GetPublicPassesByAssemblyAndSession(param.Parameter); }
                case "GetAllMPasses": { return GetAllMPasses(param.Parameter); }
                case "GetVisitorDataList": { return GetVisitorDataList(param.Parameter); }
                    
            }
            return null;
        }

        #region Methods Block
        static object CreateNewMasterPass(object param)
        {
            mPasses PassToCreate = new mPasses();
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    PassToCreate = param as mPasses;

                    //Before save get the latest Pass Code.
                    mPasses getPassCode = new mPasses();
                    getPassCode = PassToCreate;
                    PassToCreate.PassCode = GetMaxPassCodeForSession(getPassCode);
                    _context.masterPasses.Add(PassToCreate);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return PassToCreate;
        }

        // For Public Passes
        static object CreateNewMasterPassForPublic(object param)
        {
            mPasses PassToCreate = new mPasses();
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    PassToCreate = param as mPasses;

                    //Before save get the latest Pass Code.
                    mPasses getPassCode = new mPasses();
                    getPassCode = PassToCreate;
                    if (!PassToCreate.PassCode.HasValue)
                    {
                        PassToCreate.PassCode = GetMaxPublicPassCodeForSession(getPassCode);
                    }
                    _context.masterPasses.Add(PassToCreate);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return PassToCreate;
        }

        static object CreateNewRejectedMasterPass(object param)
        {
            mPasses PassToCreate = new mPasses();
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    PassToCreate = param as mPasses;

                    PassToCreate.PassCode = null;
                    _context.masterPasses.Add(PassToCreate);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return PassToCreate;
        }

        static object GetMasterPassesByStatus(object param)
        {
            try
            {
                mPasses model = param as mPasses;
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    var masterPassList = (from materPasses in _context.masterPasses
                                          where materPasses.Status == model.Status && materPasses.AssemblyCode == model.AssemblyCode
                                          && materPasses.SessionCode == model.SessionCode
                                          select materPasses).OrderByDescending(a => a.PassID).ToList();//.GroupBy(a => new { a.Name, a.ApprovedPassCategoryID, a.Address }).Select(ab => ab.FirstOrDefault()).OrderByDescending(a => a.PassID).ToList();
                    return masterPassList;
                }
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        //static object GetPublicPassesByStatus(object param)
        //{
        //    try
        //    {
        //        mPasses model = param as mPasses;
        //        using (AdministrationBranchContext _context = new AdministrationBranchContext())
        //        {
        //            var masterPassList = (from materPasses in _context.PublicPasses
        //                                  where materPasses.Status == model.Status && materPasses.AssemblyCode == model.AssemblyCode
        //                                  && materPasses.SessionCode == model.SessionCode
        //                                  orderby materPasses.PassCode descending
        //                                  select materPasses).GroupBy(a => new { a.Name, a.PassCategoryID, a.Address }).Select(ab => ab.FirstOrDefault()).ToList();
        //            return masterPassList;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //        throw;
        //    }
        //}

        static object GetMasterPassesByPassCodeAndSessionCode(object param)
        {
            try
            {
                mPasses model = param as mPasses;

                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    var MasterpassDetail = _context.masterPasses.SingleOrDefault(a => a.PassCode == model.PassCode && a.AssemblyCode == model.AssemblyCode && a.SessionCode == model.SessionCode) as mPasses;

                    return MasterpassDetail;
                }
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }
        static object GetAdminAssemblyList(object param)
        {
            mPasses model = param as mPasses;

            AdministrationBranchContext pCtxt = new AdministrationBranchContext();
            SessionContext sessionContext = new SessionContext();
            SiteSettingContext settingContext = new SiteSettingContext();
            AssemblyContext AsmCtx = new AssemblyContext();
            if (model.AssemblyCode == 0 && model.SessionCode == 0)
            {
                // Get SiteSettings 
                var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
                var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

                model.SessionCode = Convert.ToInt16(SessionCode);
                model.AssemblyCode = Convert.ToInt16(AssemblyCode);
            }
            string SessionName = (from m in sessionContext.mSession where m.SessionCode == model.SessionCode && m.AssemblyID == model.AssemblyCode select m.SessionName).SingleOrDefault();
            string AssemblyName = (from m in AsmCtx.mAssemblies where m.AssemblyCode == model.AssemblyCode select m.AssemblyName).SingleOrDefault();

            model.SessionName = SessionName;
            model.AssesmblyName = AssemblyName;
            model.mAssemblyList = (from A in pCtxt.mAssembly
                                   select A).ToList();
            model.sessionList = (from S in pCtxt.mSession
                                 where S.AssemblyID == model.AssemblyCode
                                 select S).ToList();
            return model;
        }

        static object GetMasterPassesByStatusCount(object param)
        {
            try
            {
                mPasses model = param as mPasses;

                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {


                    var masterPassList = (from materPasses in _context.masterPasses
                                          where materPasses.Status == model.Status && materPasses.AssemblyCode == model.AssemblyCode
                                          && materPasses.SessionCode == model.SessionCode
                                          orderby materPasses.PassCode descending
                                          select materPasses).GroupBy(a => new { a.Name, a.ApprovedPassCategoryID, a.Address }).Select(ab => ab.FirstOrDefault()).ToList();
                    //  masterPassList = masterPassList.GroupBy(a => a.Name).Select(a => a.First()).ToList();
                    return masterPassList.Count;
                }
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }
        static object GetSpeakerGalaryPassesByStatus(object param)
        {
            try
            {
                mPasses model = param as mPasses;
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    var masterPassList = (from materPasses in _context.masterPasses
                                          where materPasses.Status == model.Status && materPasses.AssemblyCode == model.AssemblyCode
                                          && materPasses.SessionCode == model.SessionCode && materPasses.RequestedPassCategoryID == (Int32)PassAllowsCreation.SpeakerGalaryPassID
                                          orderby materPasses.PassCode descending
                                          select materPasses).ToList();
                    return masterPassList;
                }
            }
            catch (Exception)
            {
                return null;
                throw;
            }
        }

        static Int32 GetMaxPassCodeForSession(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mPasses model = param as mPasses;



                    var masterPassMaxPassCode = (from materPasses in _context.masterPasses
                                                 where materPasses.AssemblyCode == model.AssemblyCode
                                                 && materPasses.SessionCode == model.SessionCode && materPasses.PassCode != null && (materPasses.RecommendationType == "Department" || materPasses.RecommendationType == "Minister"
                                                 || materPasses.RequestedPassCategoryID == 11 || materPasses.RequestedPassCategoryID == 6 || materPasses.RequestedPassCategoryID == 5 || materPasses.RequestedPassCategoryID == 9 || materPasses.RequestedPassCategoryID == 10 || materPasses.RequestedPassCategoryID == 20 || materPasses.RequestedPassCategoryID == 8)
                                                 select materPasses).OrderByDescending(m => m.PassID).Take(1).FirstOrDefault();



                    //var masterPassMaxPassCode = (from materPasses in _context.masterPasses
                    //                             where materPasses.AssemblyCode == model.AssemblyCode
                    //                             && materPasses.SessionCode == model.SessionCode && (materPasses.RecommendationType == "Department" || materPasses.RecommendationType == "Minister" || materPasses.RecommendationType == "Media" || materPasses.ApprovedPassCategoryID == 5 || materPasses.ApprovedPassCategoryID == 9 || materPasses.ApprovedPassCategoryID == 10 || materPasses.ApprovedPassCategoryID == 20 || materPasses.ApprovedPassCategoryID == 8)
                    //                             select materPasses).Count();
                    if (masterPassMaxPassCode != null)
                    {

                        return Convert.ToInt32(masterPassMaxPassCode.PassCode) + 1;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }

        static Int32 GetMaxPublicPassCodeForSession(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mPasses model = param as mPasses;

                    var masterPassMaxPassCode = (from materPasses in _context.masterPasses
                                                 where materPasses.AssemblyCode == model.AssemblyCode
                                                 && materPasses.SessionCode == model.SessionCode && materPasses.PassCode != null && (materPasses.RequestedPassCategoryID == 7 || materPasses.RequestedPassCategoryID == 17)
                                                 orderby materPasses.PassCode descending
                                                 select materPasses.PassCode).FirstOrDefault();
                    if (masterPassMaxPassCode != 0)
                    {
                        //int PassCode = Convert.ToInt32(string.Format("{0}{1}", 7, Convert.ToInt32(masterPassMaxPassCode.PassCode) + 1));
                        return Convert.ToInt32(masterPassMaxPassCode) + 1;
                    }
                    else
                    {
                        return 1;
                    }
                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }
        static object GetMasterPassById(object param)
        {
            mPasses masterPass = param as mPasses;
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    return _context.masterPasses.SingleOrDefault(a => a.PassID == masterPass.PassID);
                }
            }
            catch
            {
                throw;
            }
        }

        public static string GetPassCategoryNameByID(int passCategoryID)
        {
            var passCategoryModel = GetPassCategoryById(new PassCategory { PassCategoryID = passCategoryID }) as PassCategory;
            if (passCategoryModel != null)
            {
                return passCategoryModel.Name;
            }
            return "";
        }

        static object GetPassCategoryById(object param)
        {
            PassCategory PassCategory = param as PassCategory;
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    return _context.passCategories.SingleOrDefault(a => a.PassCategoryID == PassCategory.PassCategoryID);
                }
            }
            catch
            {
                throw;
            }
        }

        static Object GetDepartmentPassRequests(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mPasses model = (mPasses)param;
                    var result = (from DepartmentPasses in _context.mDepartmentPasses
                                  where DepartmentPasses.AssemblyCode == model.AssemblyCode
                                  && DepartmentPasses.SessionCode == model.SessionCode && DepartmentPasses.IsRequested == true
                                  && DepartmentPasses.RequestedDate != null && DepartmentPasses.IsApproved == false
                                  && DepartmentPasses.ApprovedBy == null && DepartmentPasses.ApprovedDate == null && DepartmentPasses.Status == (int)PassStatus.Pending
                                  orderby (DepartmentPasses.RequestedDate) descending
                                  select DepartmentPasses).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static Object GetDepartmentPassRequestsCount(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mPasses model = (mPasses)param;
                    var result = (from DepartmentPasses in _context.mDepartmentPasses
                                  where DepartmentPasses.AssemblyCode == model.AssemblyCode
                                  && DepartmentPasses.SessionCode == model.SessionCode && DepartmentPasses.IsRequested == true
                                  && DepartmentPasses.RequestedDate != null && DepartmentPasses.IsApproved == false
                                  && DepartmentPasses.ApprovedBy == null && DepartmentPasses.ApprovedDate == null && DepartmentPasses.Status == (int)PassStatus.Pending
                                  orderby DepartmentPasses.PassID descending
                                  select DepartmentPasses).ToList();
                    return result.Count;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static Int32 GetPublicDepartmentPassCount(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mPasses model = param as mPasses;

                    var masterPassMaxPassCode = (from materPasses in _context.PublicPasses
                                                 where materPasses.AssemblyCode == model.AssemblyCode
                                                 && materPasses.SessionCode == model.SessionCode && (materPasses.PassCategoryID == 7 || materPasses.PassCategoryID == 11 || materPasses.PassCategoryID == 17) && materPasses.Status == 2
                                                 select materPasses).Count();


                    var masterPassMaxPassCode1 = (from materPasses in _context.PublicPasses
                                                  where materPasses.AssemblyCode == model.AssemblyCode
                                                  && materPasses.SessionCode == model.SessionCode && (materPasses.PassCategoryID == 5 || materPasses.PassCategoryID == 8 || materPasses.PassCategoryID == 9 || materPasses.PassCategoryID == 10 || materPasses.PassCategoryID == 20)
                                                  select materPasses).Count();

                    var DepartmentPassCount = (from materPasses in _context.mDepartmentPasses
                                               where materPasses.AssemblyCode == model.AssemblyCode
                                               && materPasses.SessionCode == model.SessionCode && materPasses.IsRequested == true
                                               select materPasses).Count();

                    return masterPassMaxPassCode + masterPassMaxPassCode1 + DepartmentPassCount;

                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }
        static Int32 GetAdharIdExist(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mPasses model = param as mPasses;

                    int Aadhar = 0;
                    var Depart = (from materPasses in _context.mDepartmentPasses
                                  where materPasses.AssemblyCode == model.AssemblyCode
                                  && materPasses.SessionCode == model.SessionCode && materPasses.AadharID == model.AadharID
                                  select materPasses).Count();

                    var Master = (from mdl in _context.masterPasses
                                  where mdl.AssemblyCode == model.AssemblyCode
                                  && mdl.SessionCode == model.SessionCode && mdl.RejectionDate != null && mdl.AadharID == model.AadharID
                                  select mdl).Count();
                    if (Depart != 0)
                    {
                        if (Master != 0)
                        {
                            Aadhar = 0;
                        }
                        else
                        {
                            Aadhar = Depart;
                        }
                    }

                    return Aadhar;

                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }

        static Object GetDepartmentPassByPassID(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mDepartmentPasses model = (mDepartmentPasses)param;
                    var result = (from DepartmentPasses in _context.mDepartmentPasses
                                  where DepartmentPasses.PassID == model.PassID && DepartmentPasses.AssemblyCode == model.AssemblyCode
                                  && DepartmentPasses.SessionCode == model.SessionCode && DepartmentPasses.IsRequested == true
                                  && DepartmentPasses.RequestedDate != null
                                  select DepartmentPasses).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static Object GetDepartmentPassDetails(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mDepartmentPasses model = (mDepartmentPasses)param;
                    var result = (from DepartmentPasses in _context.mDepartmentPasses
                                  where DepartmentPasses.RecommendationType == model.RecommendationType && DepartmentPasses.Name == model.Name
                                  && DepartmentPasses.PassCode == model.PassCode && DepartmentPasses.RequestdID == model.RequestdID
                                  && DepartmentPasses.AssemblyCode == model.AssemblyCode && DepartmentPasses.SessionCode == model.SessionCode && DepartmentPasses.IsRequested == true
                                  && DepartmentPasses.RequestedDate != null
                                  select DepartmentPasses).FirstOrDefault();
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        public static object DeleteMasterPassByID(object param)
        {
            try
            {
                using (AdministrationBranchContext db = new AdministrationBranchContext())
                {
                    mPasses parameter = param as mPasses;
                    mPasses stateToRemove = db.masterPasses.SingleOrDefault(a => a.PassID == parameter.PassID);
                    db.masterPasses.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        static Object GetReceptionPassRequests(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mPasses model = (mPasses)param;

                    var result = (from PublicPasses in _context.PublicPasses
                                  where PublicPasses.AssemblyCode == model.AssemblyCode
                                  && PublicPasses.SessionCode == model.SessionCode && PublicPasses.Status == (int)PassStatus.Pending
                                  && PublicPasses.DeptApprovalNeeded == true && PublicPasses.IsApproved == false
                                  && PublicPasses.RejectionDate == null && PublicPasses.RejectionReason == null
                                  orderby PublicPasses.RequestedDate descending
                                  select PublicPasses).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static Object GetReceptionPassRequestsCount(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    mPasses model = (mPasses)param;

                    var result = (from PublicPasses in _context.PublicPasses
                                  where PublicPasses.AssemblyCode == model.AssemblyCode
                                  && PublicPasses.SessionCode == model.SessionCode && PublicPasses.Status == (int)PassStatus.Pending
                                  && PublicPasses.DeptApprovalNeeded == true && PublicPasses.IsApproved == false
                                  && PublicPasses.RejectionDate == null && PublicPasses.RejectionReason == null
                                  select PublicPasses).ToList();
                    return result.Count;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        static object GetPublicPassesByPassID(object param)
        {
            PublicPass publicPass = param as PublicPass;
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    return _context.PublicPasses.SingleOrDefault(a => a.PassID == publicPass.PassID);
                }
            }
            catch
            {
                throw;
            }
        }
        static object GetPublicPassesByAssemblyAndSession(object param)
        {
            PublicPass publicPass = param as PublicPass;
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    var masterPassList = (from PP in _context.PublicPasses
                                          where PP.AssemblyCode == publicPass.AssemblyCode && PP.SessionCode == publicPass.SessionCode &&
                                          PP.PassCategoryID == publicPass.PassCategoryID
                                          orderby PP.PassCode descending
                                          select PP).OrderByDescending(a => a.PassID).ToList();

                    return masterPassList;
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetAllMPasses(object param)
        {
            mPasses publicPass = param as mPasses;
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    return _context.masterPasses.Where(a => a.AssemblyCode == publicPass.AssemblyCode &&
                        a.SessionCode == publicPass.SessionCode && a.ApprovedPassCategoryID == publicPass.ApprovedPassCategoryID).OrderByDescending(a => a.Name).ToList();
                     
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetVisitorDataList(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    VisitorData model = param as VisitorData;

                    var result = (from vd in _context.visitorData
                                  where vd.AssemblyCode == model.AssemblyCode && vd.SessionCode == model.SessionCode
                                  orderby vd.VerifiedPassID ascending
                                  select vd).ToList();

                    return result;

                }

            }
            catch (Exception ee)
            {
                throw ee;
            }

        }
        static object UpdateMasterPass(object param)
        {
            mPasses PassToUpdate = param as mPasses;
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    _context.masterPasses.Attach(PassToUpdate);
                    _context.Entry(PassToUpdate).State = EntityState.Modified;
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return PassToUpdate;
        }

        static object GetPassesApprovedByDate(object param)
        {
            try
            {
                mPasses model = param as mPasses;
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    if (model.SessionDateT != null && model.SessionDateF != null)
                    {
                        var masterPassList = (from materPasses in _context.masterPasses
                                              where EntityFunctions.TruncateTime(materPasses.ApprovedDate) >= EntityFunctions.TruncateTime(model.SessionDateF) && EntityFunctions.TruncateTime(materPasses.ApprovedDate) <= EntityFunctions.TruncateTime(model.SessionDateT) && materPasses.SessionCode == model.SessionCode && materPasses.AssemblyCode == model.AssemblyCode
                                              orderby materPasses.PassCode descending
                                              select materPasses).OrderByDescending(a => a.PassID).ToList();//.GroupBy(a => new { a.Name, a.ApprovedPassCategoryID, a.Address }).Select(ab => ab.FirstOrDefault()).OrderByDescending(a => a.PassID).ToList();
                        return masterPassList;

                    }
                    else if (model.DepartmentID != null)
                    {
                        var masterPassList = (from materPasses in _context.masterPasses
                                              where materPasses.DepartmentID == model.DepartmentID && materPasses.SessionCode == model.SessionCode && materPasses.AssemblyCode == model.AssemblyCode
                                              orderby materPasses.PassCode descending
                                              select materPasses).GroupBy(a => new { a.Name, a.ApprovedPassCategoryID, a.Address }).Select(ab => ab.FirstOrDefault()).OrderByDescending(a => a.PassID).ToList();

                        return masterPassList;
                    }
                    else
                    {
                        var masterPassList = (from materPasses in _context.masterPasses
                                              where materPasses.ApprovedPassCategoryID == model.ApprovedPassCategoryID && materPasses.SessionCode == model.SessionCode && materPasses.AssemblyCode == model.AssemblyCode
                                              orderby materPasses.PassCode descending
                                              select materPasses).GroupBy(a => new { a.Name, a.ApprovedPassCategoryID, a.Address }).Select(ab => ab.FirstOrDefault()).OrderByDescending(a => a.PassID).ToList();
                        return masterPassList;
                    }
                }


            }
            catch (Exception)
            {
                return null;
                throw;
            }

        }
        static object GetPassesApprovedDeptViewDate(object param)
        {
            try
            {
                mPasses model = param as mPasses;
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {

                    var masterPassList = (from materPasses in _context.masterPasses
                                          where EntityFunctions.TruncateTime(materPasses.ApprovedDate) >= EntityFunctions.TruncateTime(model.SessionDateF) && EntityFunctions.TruncateTime(materPasses.ApprovedDate) <= EntityFunctions.TruncateTime(model.SessionDateT)
                                          && materPasses.DepartmentID == model.DepartmentID
                                          && materPasses.SessionCode == model.SessionCode && materPasses.AssemblyCode == model.AssemblyCode
                                          orderby materPasses.PassCode descending
                                          select materPasses).ToList();
                    return masterPassList;


                }


            }
            catch (Exception)
            {
                return null;
                throw;
            }

        }
        static object GetPassesApprovedReport(object param)
        {
            try
            {
                mPasses model = param as mPasses;
                List<mPasses> ListFilterModel = new List<mPasses>();

                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    var masterPassList = (from P in _context.masterPasses
                                          join C in _context.passCategories on new { PassCategoryID = P.ApprovedPassCategoryID } equals new { PassCategoryID = C.PassCategoryID }
                                          where
                                           P.SessionCode == model.SessionCode &&
                                           P.AssemblyCode == model.AssemblyCode
                                          group new { P, C } by new
                                          {
                                              P.OrganizationName,
                                              P.ApprovedPassCategoryID,
                                              P.DepartmentID,
                                              C.Name
                                          } into g
                                          select new
                                          {
                                              g.Key.Name,
                                              g.Key.OrganizationName,
                                              ApprovedPassCategoryID = (int?)g.Key.ApprovedPassCategoryID,
                                              TotalCount = g.Count(p => p.P.ApprovedPassCategoryID != null),
                                              g.Key.DepartmentID
                                          }).ToList();

                    foreach (var item in masterPassList)
                    {

                        mPasses FilterModel = new mPasses();
                        FilterModel.DepartmentID = item.DepartmentID;
                        FilterModel.OrganizationName = item.OrganizationName;
                        FilterModel.ApprovedPassCategoryID = (int)item.ApprovedPassCategoryID;
                        FilterModel.PassTypeName = item.Name;
                        FilterModel.TotalCount = item.TotalCount;
                        ListFilterModel.Add(FilterModel);

                    }
                    return ListFilterModel;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static object GetPassesDepartmentCount(object param)
        {
            try
            {
                mPasses model = param as mPasses;
                List<mPasses> ListFilterModel = new List<mPasses>();
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {

                    var masterPassList = (from MPasses in _context.masterPasses
                                          where
                                            MPasses.SessionCode == model.SessionCode &&
                                            MPasses.AssemblyCode == model.AssemblyCode
                                          group MPasses by new
                                          {
                                              MPasses.OrganizationName,
                                              MPasses.DepartmentID
                                          } into g
                                          select new
                                          {
                                              g.Key.OrganizationName,
                                              g.Key.DepartmentID,
                                              TotalDeptCount = g.Count(p => p.DepartmentID != null)
                                          }).ToList();

                    foreach (var item in masterPassList)
                    {

                        mPasses FilterModel = new mPasses();
                        FilterModel.DepartmentID = item.DepartmentID;
                        FilterModel.OrganizationName = item.OrganizationName;

                        FilterModel.TotalOfTotalcount = item.TotalDeptCount;
                        ListFilterModel.Add(FilterModel);

                    }
                    return ListFilterModel;


                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }



        static object GetPassesCategoryCount(object param)
        {
            try
            {
                mPasses model = param as mPasses;
                List<mPasses> ListFilterModel = new List<mPasses>();
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {

                    var masterPassList = (from P in _context.masterPasses
                                          join PS in _context.passCategories on new { PassCategoryID = P.ApprovedPassCategoryID } equals new { PassCategoryID = PS.PassCategoryID }
                                          where
                                            P.SessionCode == model.SessionCode &&
                                            P.AssemblyCode == model.AssemblyCode
                                          group new { PS, P } by new
                                          {
                                              PS.Name,
                                              PS.PassCategoryID
                                          } into g
                                          select new
                                          {
                                              g.Key.Name,
                                              g.Key.PassCategoryID,
                                              TotlCount = g.Count(p => p.P.ApprovedPassCategoryID != null)
                                          }).ToList();
                    foreach (var item in masterPassList)
                    {

                        mPasses FilterModel = new mPasses();
                        FilterModel.ApprovedPassCategoryID = item.PassCategoryID;
                        FilterModel.PassTypeName = item.Name;
                        FilterModel.TotalCount = item.TotlCount;
                        ListFilterModel.Add(FilterModel);

                    }
                    return ListFilterModel;

                }
            }
            catch (Exception ee)
            {
                throw ee;
            }
        }



        #region Helper Methods for the Pass Verification application.
        static object GetMasterPassByPassCode(object param)
        {
            Int32 masterPassID = (Int32)param;

            SiteSettingContext settingContext = new SiteSettingContext();

            // Get SiteSettings 
            var AssemblyCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Assembly" select mdl.SettingValue).SingleOrDefault();
            var SessionCode = (from mdl in settingContext.SiteSettings where mdl.SettingName == "Session" select mdl.SettingValue).SingleOrDefault();

            int AssemblyID = Convert.ToInt16(AssemblyCode);
            int SessionID = Convert.ToInt16(SessionCode);
            if (masterPassID != 0)
            {
                try
                {
                    using (AdministrationBranchContext _context = new AdministrationBranchContext())
                    {
                        string passDetailsAsXML = "";
                        var passDetails = _context.masterPasses.SingleOrDefault(a => a.PassCode == masterPassID && a.AssemblyCode == AssemblyID && a.SessionCode == SessionID);//&& a.SessionDateFrom.CompareTo(startDateAsString) <= 0 && a.SessionDateFrom.CompareTo(startDateAsString) >= 0);
                        if (passDetails != null)
                        {

                            DateTime fromDate = new DateTime(Convert.ToInt32(passDetails.SessionDateFrom.Split('/')[2]), Convert.ToInt32(passDetails.SessionDateFrom.Split('/')[1]), Convert.ToInt32(passDetails.SessionDateFrom.Split('/')[0]));
                            DateTime ToDate = new DateTime(Convert.ToInt32(passDetails.SessionDateTo.Split('/')[2]), Convert.ToInt32(passDetails.SessionDateTo.Split('/')[1]), Convert.ToInt32(passDetails.SessionDateTo.Split('/')[0]));

                            if (fromDate.Date <= DateTime.Now.Date && ToDate.Date >= DateTime.Now.Date)
                            {
                                passDetailsAsXML = XMLForQRCode(passDetails);
                            }
                            else
                            {
                                passDetailsAsXML = "";
                            }

                        }
                        return passDetailsAsXML;
                    }
                }
                catch
                {
                    throw;
                }
            }
            return null;
        }

        public static string XMLForQRCode(mPasses model)
        {
            XElement passParentNode = new System.Xml.Linq.XElement("PassCode");

            XElement passChildChildNode1 = new System.Xml.Linq.XElement("Name");
            passParentNode.Add(passChildChildNode1);

            XElement passChildChildNode2 = new System.Xml.Linq.XElement("Department");
            passParentNode.Add(passChildChildNode2);

            XElement passChildChildNode3 = new System.Xml.Linq.XElement("Designation");
            passParentNode.Add(passChildChildNode3);

            XElement passChildChildNode4 = new System.Xml.Linq.XElement("Address");
            passParentNode.Add(passChildChildNode4);

            XElement passChildChildNode5 = new System.Xml.Linq.XElement("From");
            passParentNode.Add(passChildChildNode5);

            XElement passChildChildNode6 = new System.Xml.Linq.XElement("To");
            passParentNode.Add(passChildChildNode6);

            XElement passChildChildNode7 = new System.Xml.Linq.XElement("PassCategoryName");
            passParentNode.Add(passChildChildNode7);

            XElement passChildChildNode8 = new System.Xml.Linq.XElement("VehicleNumber");
            passParentNode.Add(passChildChildNode8);

            XElement passChildChildNode9 = new System.Xml.Linq.XElement("Photo");
            passParentNode.Add(passChildChildNode9);

            XElement passChildChildNode10 = new System.Xml.Linq.XElement("GateNumber");
            passParentNode.Add(passChildChildNode10);

            XElement passChildChildNode11 = new System.Xml.Linq.XElement("Gender");
            passParentNode.Add(passChildChildNode11);

            XElement passChildChildNode12 = new System.Xml.Linq.XElement("Age");
            passParentNode.Add(passChildChildNode12);

            XElement passChildChildNode13 = new System.Xml.Linq.XElement("AssemblyCode");
            passParentNode.Add(passChildChildNode13);

            XElement passChildChildNode14 = new System.Xml.Linq.XElement("SessionCode");
            passParentNode.Add(passChildChildNode14);

            XElement passChildChildNode15 = new System.Xml.Linq.XElement("AssemblyName");
            passParentNode.Add(passChildChildNode15);

            XElement passChildChildNode16 = new System.Xml.Linq.XElement("SessionName");
            passParentNode.Add(passChildChildNode16);

            XElement passChildChildNode17 = new System.Xml.Linq.XElement("SessionDate");
            passParentNode.Add(passChildChildNode17);

            passParentNode.SetAttributeValue("ID", model.PassCode);

            string information = model.Name + " (" + model.Gender + ", " + model.Age + " Years)";

            passChildChildNode1.ReplaceNodes(model.Name != null ? information : "");
            passChildChildNode2.ReplaceNodes(model.OrganizationName != null ? model.OrganizationName : "");
            passChildChildNode3.ReplaceNodes(model.Designation != null ? model.Designation : "");
            passChildChildNode4.ReplaceNodes(model.Address != null ? model.Address : "");
            passChildChildNode5.ReplaceNodes(model.SessionDateFrom != null ? model.SessionDateFrom : "");
            passChildChildNode6.ReplaceNodes(model.SessionDateTo != null ? model.SessionDateTo : "");
            passChildChildNode7.ReplaceNodes(model.ApprovedPassCategoryID != 0 ? GetPassCategoryNameByID(model.ApprovedPassCategoryID) : "");
            passChildChildNode8.ReplaceNodes(model.VehicleNumber != null ? model.VehicleNumber : "");
            passChildChildNode9.ReplaceNodes(!string.IsNullOrEmpty(model.Photo) ? model.Photo : string.Empty);
            passChildChildNode10.ReplaceNodes(!string.IsNullOrEmpty(model.GateNumber) ? model.GateNumber : string.Empty);

            passChildChildNode11.ReplaceNodes(!string.IsNullOrEmpty(model.Gender) ? model.Gender : string.Empty);
            passChildChildNode12.ReplaceNodes(model.Age);
            passChildChildNode13.ReplaceNodes(model.AssemblyCode);
            passChildChildNode14.ReplaceNodes(model.SessionCode);

            mSession Mdl = new mSession();
            Mdl.SessionCode = model.SessionCode;
            Mdl.AssemblyID = model.AssemblyCode;
            mAssembly assmblyMdl = new mAssembly();
            assmblyMdl.AssemblyID = model.AssemblyCode;

            AssemblyContext assemblyContext = new AssemblyContext();

            string SessionName = Convert.ToString(SessionContext.GetSessionNameBySessionCode(Mdl));
            string AssesmblyName = Convert.ToString(AssemblyContext.GetAssemblyNameByAssemblyCode(assmblyMdl));

            passChildChildNode15.ReplaceNodes(!string.IsNullOrEmpty(SessionName) ? SessionName : ""); //Assembly Name.
            passChildChildNode16.ReplaceNodes(!string.IsNullOrEmpty(AssesmblyName) ? AssesmblyName : ""); //Session Name.

            passChildChildNode17.ReplaceNodes(DateTime.Now);

            return passParentNode.ToString();
        }

        static object GetVerifiedPassesForSessionDate(object param)
        {
            try
            {
                VisitorData model = param as VisitorData;
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    var masterPassList = (from verifiedPasses in _context.visitorData
                                          where verifiedPasses.AssemblyCode == model.AssemblyCode
                                          && verifiedPasses.SessionCode == model.SessionCode
                                          orderby verifiedPasses.PassCode descending
                                          select verifiedPasses).ToList();
                    return masterPassList;
                }
            }
            catch (Exception)
            {
                return null;
                throw;
            }
            return null;
        }

        static object InsertVerifiedPassData(object param)
        {
            VisitorData verifiedPassToCreate = new VisitorData();
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {
                    verifiedPassToCreate = param as VisitorData;

                    //Create a new Verified Pass Entry.
                    _context.visitorData.Add(verifiedPassToCreate);
                    _context.SaveChanges();
                    _context.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return verifiedPassToCreate;
        }
        #endregion

        #endregion
        #region get Mpass and Department Pass List for Print
        static object GetmPassListForDepartmentPrint(object param)
        {
            try
            {
                using (AdministrationBranchContext _context = new AdministrationBranchContext())
                {

                    mPasses model = (mPasses)param;
                    // string csv = model.DepartmentID;
                    //IEnumerable<string> ids = csv.Split(',').Select(str => str);
                    var result = (from mdl in _context.masterPasses
                                  join mdlsec in _context.mDepartmentPasses on mdl.PassCode equals mdlsec.PassCode
                                  where 
                                  mdl.SessionCode == model.SessionCode 
                                  && mdl.AssemblyCode == model.AssemblyCode
                                  && mdlsec.AssemblyCode == model.AssemblyCode
                                  && mdlsec.SessionCode == model.SessionCode 
                                  && mdl.Status == 2 
                                  && mdlsec.IsRequestUserID == model.IsRequestUserID 
                                  && mdlsec.ApprovedPassCategoryID != 0
                                 select mdl).GroupBy(a => new { a.Name, a.ApprovedPassCategoryID, a.Address }).Select(ab => ab.FirstOrDefault()).OrderByDescending(a => a.PassID).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        #endregion
    }
}
