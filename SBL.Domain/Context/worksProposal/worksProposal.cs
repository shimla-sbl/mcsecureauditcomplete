﻿using SBL.DAL;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.ConstituencyVS;
using SBL.DomainModel.Models.CutMotionDemand;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.District;
using SBL.DomainModel.Models.Grievance;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Office;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.User;
using SBL.Service.Common;
using System.Data.Entity;

namespace SBL.Domain.Context.worksProposal
{
    public class worksProposal : DBBase<worksProposal>
    {
        public worksProposal()
            : base("eVidhan")
        {
        }

        #region tables

        public virtual DbSet<Demands> Demands { get; set; }

        public DbSet<DistrictModel> Districts { get; set; }

        public DbSet<SchemeMastersVS> SchemeMastersVS { get; set; }

        public DbSet<SchemeMastersConstituency> SchemeMastersConstituency { get; set; }

        public DbSet<mConstituency> mConstituency { get; set; }

        public DbSet<SiteSettings> SiteSettings { get; set; }

        public DbSet<mDepartment> mDepartment { get; set; }

        public DbSet<mPanchayat> mPanchayat { get; set; }

        public DbSet<ConstituencyWorkProgressImages> ConstituencyWorkProgressImages { get; set; }

        public DbSet<ConstituencySchemesBasicDetails> ConstituencySchemesBasicDetails { get; set; }

        public DbSet<ConstituencyWorkProgress> ConstituencyWorkProgress { get; set; }

        public DbSet<mProgramme> mProgramme { get; set; }

        public DbSet<workNature> workNature { get; set; }

        public DbSet<mworkStatus> mworkStatus { get; set; }

        public DbSet<workType> workType { get; set; }

        public DbSet<tConstituencyPanchayat> tConstituencyPanchayat { get; set; }

        public DbSet<mControllingAuthority> mControllingAuthority { get; set; }

        public DbSet<mOffice> mOffice { get; set; }

        public DbSet<tDepartmentOfficeMapped> tDepartmentOfficeMapped { get; set; }

        public DbSet<mUsers> mUsers { get; set; }

        public DbSet<ForwardGrievance> ForwardGrievance { get; set; }

        public DbSet<AdhaarDetails> AdhaarDetails { get; set; }

        public DbSet<tCutMotionDemand> tCutMotionDemand { get; set; }

        public DbSet<mMember> Member { get; set; }

        public DbSet<workTrans> workTrans { get; set; }

        #endregion tables

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "GetAllDemand": { return null; }
            }
            return null;
        }
    }
}