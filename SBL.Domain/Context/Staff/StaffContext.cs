﻿using SBL.DAL;
using SBL.DomainModel.ComplexModel;
using SBL.DomainModel.Models.Promotion;
using SBL.DomainModel.Models.StaffManagement;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using SBL.DomainModel.Models.Committee;

namespace SBL.Domain.Context.Staff
{
    public class StaffContext : DBBase<StaffContext>
    {


        public StaffContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }
        public DbSet<mStaff> mStaff { get; set; }
        public DbSet<mStaffNominee> mStaffNominee { get; set; }
        public DbSet<mStaffDesignation> mStaffDesignation { get; set; }
        public DbSet<tPensiondetail> tPensiondetail { get; set; }
        public DbSet<tPromotions> tPromotion { get; set; }

        public DbSet<tLeaveDetail> tLeaveDetail { get; set; }

        public DbSet<tTrainingDetails> tTrainingDetails { get; set; }
        public DbSet<mBranches> mBranches { get; set; }
        
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {

                case "StaffList": { return StaffList(); }
                case "StaffDesignationList": { return StaffDesignationList(); }
                case "GetAllStaffList": { return GetAllStaffList(param.Parameter); }
                case "newStaff": { return newStaff(param.Parameter); }
                case "NewstaffDesignation": { return NewstaffDesignation(param.Parameter); }
                case "updateStaff": { return updateStaff(param.Parameter); }
                case "updateStaffDesignation": { return updateStaffDesignation(param.Parameter); }

                case "getDesignationList": { return getDesignationList(); }
                case "getStaffDetailsByID": { return getStaffDetailsByID(param.Parameter); ; }
                case "getStaffDesignationDetailsByIDForUpdate": { return getStaffDesignationDetailsByIDForUpdate(param.Parameter); ; }

                case "getStaffDesignationDetailsByID": { return getStaffDesignationDetailsByID(param.Parameter); }



                case "getUpdateStaffDesignationDetailsByID": { return getUpdateStaffDesignationDetailsByID(param.Parameter); }
                case "getDeleteStaffDesignationDetailsByID": { return getDeleteStaffDesignationDetailsByID(param.Parameter); }



                case "StaffNomineeList": { return StaffNomineeList(param.Parameter); }
                case "saveNominee": { return saveNominee(param.Parameter); }
                case "getNomineeDetails": { return getNomineeDetails(param.Parameter); }
                case "updateNominee": { return updateNominee(param.Parameter); }
                case "deleteNominee": { return deleteNominee(param.Parameter); }
                case "GetStaffListByGroup": { return GetStaffListByGroup(param.Parameter); }
                case "GetExStaffList": { return GetExStaffList(param.Parameter); }
                case "GetAllStaffNameList": { return GetAllStaffNameList(param.Parameter); }

                case "NewPromotion": { return NewPromotion(param.Parameter); }


                case "getpromotionsDetailsByID": { return getpromotionsDetailsByID(param.Parameter); }

                case "promotionList": { return promotionList(param.Parameter); }

                case "getPromotionDetailsByIDForUpdate": { return getPromotionDetailsByIDForUpdate(param.Parameter); }

                case "getUpdatePromotionByID": { return getUpdatePromotionByID(param.Parameter); }

                case "updatePromotion": { return updatePromotion(param.Parameter); }


                case "getDeletePromotionDetailsByID": { return getDeletePromotionDetailsByID(param.Parameter); }

                case "LeaveList": { return LeaveList(param.Parameter); }
                case "NewLeave": { return NewLeave(param.Parameter); }

                case "getLeaveDetailsByIDForUpdate": { return getLeaveDetailsByIDForUpdate(param.Parameter); }

                case "updateLeave": { return updateLeave(param.Parameter); }

                case "getDeleteLeaveDetailsByID": { return getDeleteLeaveDetailsByID(param.Parameter); }

                case "updatPromotionDetails": { return updatPromotionDetails(param.Parameter); }

                case "updateStaffPension": { return updateStaffPension(param.Parameter); }
                case "newStaffPension": { return newStaffPension(param.Parameter); }
                case "getStaffPensionDetailsByID": { return getStaffPensionDetailsByID(param.Parameter); }
                case "GetAdharIdExist": { return GetAdharIdExist(param.Parameter); }


                case "TrainingList": { return TrainingList(param.Parameter); }

                case "NewTraining": { return NewTraining(param.Parameter); }

                case "getTrainingDetailsByIDForUpdate": { return getTrainingDetailsByIDForUpdate(param.Parameter); }

                case "updateTraining": { return updateTraining(param.Parameter); }

                case "getDeleteTrainingDetailsByID": { return getDeleteTrainingDetailsByID(param.Parameter); }

                case "GetAllStaffByClass": { return GetAllStaffByClass(param.Parameter); }
                case "getStaffDetailsByAadharNum": { return getStaffDetailsByAadharNum(param.Parameter); }
                case "getBranchListbyID": { return getBranchListbyID(param.Parameter); }
                case "getBranchList": { return getBranchList(); }
                    
            }
            return null;
        }




        public static object GetAllStaffNameList(object param)
        {
            using (StaffContext context = new StaffContext())
            {

                var List = context.mStaff.Where(m => m.isActive == true && m.isAlive == true && m.IsReject == false).OrderBy(m => m.StaffName).ToList();
                List<mStaff> result = new List<mStaff>();
                if (List != null && List.Count() > 0)
                {
                    foreach (var item in List)
                    {
                        mStaff obj = new mStaff();
                        obj = item;
                        obj.NameWithDesignation = item.StaffName;
                        result.Add(obj);
                    }
                }
                return result;

            }
        }
        public static object StaffDesignationList()
        {
            using (StaffContext context = new StaffContext())
            {
                var result = (from staff in context.mStaffDesignation
                              select staff
                                 ).ToList();
                return result;
            }
        }

        public static object StaffList()
        {
            using (StaffContext context = new StaffContext())
            {
                var result = (from staff in context.mStaff
                              join staffdesignation in context.mStaffDesignation
                              on staff.Designation equals staffdesignation.designation
                              orderby staffdesignation.orderNO
                              //  where staff.IsReject == false
                              select staff
                                 ).ToList();
                return result;
            }
        }


        public static object newStaff(object param)
        {
            try
            {
                using (StaffContext context = new StaffContext())
                {
                    mStaff staff = param as mStaff;
                    context.mStaff.Add(staff);
                    context.SaveChanges();

                    var result = context.mStaff.ToList().OrderBy(m => m.StaffName).ToList();
                    context.Close();
                    return result;
                }
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }

        }



        public static object updateStaff(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                mStaff staff = param as mStaff;
                context.Entry(staff).State = EntityState.Modified;
                context.SaveChanges();
                var result = (from mstaff in context.mStaff
                              join staffdesignation in context.mStaffDesignation
                              on mstaff.Designation equals staffdesignation.designation
                              orderby staffdesignation.orderNO
                              select mstaff
                                  ).ToList();
                context.Close();
                return result;


            }
        }
        public static object deleteStaff(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                mStaff staff = param as mStaff;
                context.Entry(staff).State = EntityState.Deleted;
                context.SaveChanges();
                context.Close();
                return null;
            }
        }
        public static object getDesignationList()
        {
            using (StaffContext context = new StaffContext())
            {
                var result = (from list in context.mStaffDesignation
                              where list.designation != null
                              select new FillDesignation
                              {
                                  designationID = 0,
                                  designationName = list.designation
                              }).Distinct().ToList();
                return result;
            }
        }





        public static object getPromotionDetailsByIDForUpdate(object param)
        {
            int PromotionID = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                return context.tPromotion.Where(a => a.PromotionID == PromotionID).FirstOrDefault();

            }
        }

        public static object getStaffDesignationDetailsByIDForUpdate(object param)
        {
            int DesignationID = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                return context.mStaffDesignation.Where(a => a.DesignationID == DesignationID).FirstOrDefault();

            }
        }

        public static object getStaffDetailsByID(object param)
        {
            int staffid = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                return context.mStaff.Where(a => a.StaffID == staffid).FirstOrDefault();

            }
        }

        public static object getStaffDetailsByAadharNum(object param)
        {
            mStaff obj = param as mStaff;
            using (StaffContext context = new StaffContext())
            {
                return context.mStaff.Where(a => a.AadharID == obj.AadharID && a.StaffID != obj.StaffID).FirstOrDefault();

            }
        }

        public static object getStaffDesignationDetailsByID(object param)
        {
            try
            {
                StaffContext context = new StaffContext();

                var query = (from constituencies in context.mStaffDesignation

                             select constituencies).ToList();

                return query;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        static object getDeleteStaffDesignationDetailsByID(object param)
        {

            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            int DesignationID = Convert.ToInt32(param);

            mStaffDesignation staff = param as mStaffDesignation;

            var StaffDesignationUpdate = (from mdl in context.mStaffDesignation where mdl.DesignationID == DesignationID select mdl).SingleOrDefault();



            if (StaffDesignationUpdate != null)
            {

                context.mStaffDesignation.Remove(StaffDesignationUpdate);
                context.SaveChanges();

                context.Close();

            }
            return null;

        }

        static object getUpdateStaffDesignationDetailsByID(object param)
        {

            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            int DesignationID = Convert.ToInt32(param);

            mStaffDesignation staff = param as mStaffDesignation;

            var StaffDesignationUpdate = (from mdl in context.mStaffDesignation where mdl.DesignationID == DesignationID select mdl).SingleOrDefault();
            if (StaffDesignationUpdate != null)
            {
                StaffDesignationUpdate.DesignationCode = DesignationID;
                context.SaveChanges();
                context.Close();

            }
            return staff;

        }



        public static object StaffNomineeList(object param)
        {
            int staffid = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                return context.mStaffNominee.Where(a => a.StaffID == staffid).ToList();

            }
        }
        public static object saveNominee(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                mStaffNominee staff = param as mStaffNominee;
                context.mStaffNominee.Add(staff);
                context.SaveChanges();


                context.Close();
                return null;
            }
        }
        public static object getNomineeDetails(object param)
        {
            int staffNomineeid = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                return context.mStaffNominee.Where(a => a.NomineeID == staffNomineeid).FirstOrDefault();

            }
        }
        public static object updateNominee(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                mStaffNominee staff = param as mStaffNominee;
                context.Entry(staff).State = EntityState.Modified;
                context.SaveChanges();

                context.Close();
                return null;


            }
        }
        public static object GetStaffListByGroup(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                mStaff staff = param as mStaff;

                var List = context.mStaff.Where(m => m.Group == staff.Group && m.isActive == true && m.isAlive == true && m.IsReject == false).OrderBy(m => m.StaffName).ToList();
                List<mStaff> result = new List<mStaff>();
                if (List != null && List.Count() > 0)
                {
                    foreach (var item in List)
                    {
                        mStaff obj = new mStaff();
                        obj = item;
                        obj.NameWithDesignation = item.StaffName + "-" + item.Designation;
                        result.Add(obj);
                    }
                }
                return result;

            }
        }
        public static object GetAllStaffList(object param)
        {
            using (StaffContext context = new StaffContext())
            {

                var List = context.mStaff.Where(m => m.isActive == true && m.isAlive == true && m.IsReject == false).OrderBy(m => m.StaffName).ToList();
                List<mStaff> result = new List<mStaff>();
                if (List != null && List.Count() > 0)
                {
                    foreach (var item in List)
                    {
                        mStaff obj = new mStaff();
                        obj = item;
                        obj.NameWithDesignation = item.StaffName + "-" + item.Designation;
                        result.Add(obj);
                    }
                }
                return result;

            }
        }


        public static object GetAllStaffByClass(object param)
        {
            int Class = Convert.ToInt32(param);

            using (StaffContext context = new StaffContext())
            {

                var List = context.mStaff.Where(m => m.isActive == true && m.isAlive == true && m.IsReject == false && m.Classes == Class).OrderBy(m => m.StaffName).ToList();
                List<mStaff> result = new List<mStaff>();
                if (List != null && List.Count() > 0)
                {
                    foreach (var item in List)
                    {
                        mStaff obj = new mStaff();
                        obj = item;
                        obj.NameWithDesignation = item.StaffName + "-" + item.Designation;
                        result.Add(obj);
                    }
                }
                return result;

            }
        }
        public static object GetExStaffList(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                // mStaff staff = param as mStaff;

                var List = context.mStaff.Where(m => m.isActive == false && m.isAlive == true && m.IsReject == false).OrderBy(m => m.StaffName).ToList();
                List<mStaff> result = new List<mStaff>();
                if (List != null && List.Count() > 0)
                {
                    foreach (var item in List)
                    {
                        mStaff obj = new mStaff();
                        obj = item;
                        obj.NameWithDesignation = item.StaffName + "-" + item.Designation;
                        result.Add(obj);
                    }
                }
                return result;

            }
        }
        public static object deleteNominee(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                int nomineeID = Convert.ToInt32(param);
                var staff = (from list in context.mStaffNominee
                             where list.NomineeID == nomineeID
                             select list).FirstOrDefault();
                context.Entry(staff).State = EntityState.Deleted;
                context.SaveChanges();
                context.Close();
                return staff.StaffID;
            }
        }


        public static object NewstaffDesignation(object param)
        {
            try
            {
                using (StaffContext context = new StaffContext())
                {
                    mStaffDesignation staff = param as mStaffDesignation;
                    context.mStaffDesignation.Add(staff);
                    context.SaveChanges();

                    var result = context.mStaffDesignation.ToList().OrderBy(m => m.orderNO).ToList();
                    context.Close();
                    return result;
                }
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }

        }


        public static object updateStaffDesignation(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                mStaffDesignation staff = param as mStaffDesignation;
                context.Entry(staff).State = EntityState.Modified;
                context.SaveChanges();
                var result = (from mstaff in context.mStaffDesignation
                              //join staffdesignation in context.mStaffDesignation
                              //on mstaff.Designation equals staffdesignation.designation
                              //orderby staffdesignation.orderNO
                              select mstaff
                                  ).ToList();
                context.Close();
                return result;


            }
        }



        public static object NewPromotion(object param)
        {
            try
            {
                using (StaffContext context = new StaffContext())
                {
                    tPromotions promotion = param as tPromotions;
                    context.tPromotion.Add(promotion);
                    context.SaveChanges();

                    var result = context.tPromotion.ToList().Where(m => m.StaffID == promotion.StaffID).OrderBy(m => m.OrderNumber).ToList();
                    context.Close();
                    return result;
                }
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }

        }


        public static object getpromotionsDetailsByID(object param)
        {
            try
            {
                StaffContext context = new StaffContext();

                var query = (from promotion in context.tPromotion

                             select promotion).ToList();

                return query;
            }
            catch (Exception ex)
            {
                return null;
            }
        }



        public static object promotionList(object param)
        {
            int StaffId = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                var result = (from staff in context.tPromotion
                              where staff.StaffID==StaffId

                              select staff

                                 ).ToList();
                return result;
            }
        }







        static object getUpdatePromotionByID(object param)
        {

            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            int PromotionID = Convert.ToInt32(param);

            tPromotions staff = param as tPromotions;

            var PromotionUpdate = (from mdl in context.tPromotion where mdl.PromotionID == PromotionID select mdl).SingleOrDefault();
            if (PromotionUpdate != null)
            {
                PromotionUpdate.PromotionID = PromotionID;
                context.SaveChanges();
                context.Close();

            }
            return staff;

        }


        public static object updatePromotion(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                tPromotions staff = param as tPromotions;
                context.Entry(staff).State = EntityState.Modified;
                context.SaveChanges();
                var result = (from mstaff in context.tPromotion
                              //join staffdesignation in context.mStaffDesignation
                              //on mstaff.Designation equals staffdesignation.designation
                              //orderby staffdesignation.orderNO
                              select mstaff
                                  ).ToList();
                context.Close();
                return result;


            }
        }



        static object getDeletePromotionDetailsByID(object param)
        {

            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            int PromotionID = Convert.ToInt32(param);

            tPromotions staff = param as tPromotions;

            var PromotionUpdate = (from mdl in context.tPromotion where mdl.PromotionID == PromotionID select mdl).SingleOrDefault();



            if (PromotionUpdate != null)
            {

                context.tPromotion.Remove(PromotionUpdate);
                context.SaveChanges();

                context.Close();

            }
            return null;

        }




        public static object LeaveList(object param)
        {
            int StaffId = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                var result = (from staff in context.tLeaveDetail
                              where staff.StaffID == StaffId
                              select staff

                                 ).ToList();
                return result;
            }
        }



        public static object NewLeave(object param)
        {
            try
            {
                using (StaffContext context = new StaffContext())
                {
                    tLeaveDetail Leave = param as tLeaveDetail;
                    context.tLeaveDetail.Add(Leave);
                    context.SaveChanges();

                    var result = context.tLeaveDetail.ToList().Where(m => m.StaffID == Leave.StaffID).OrderBy(m => m.LeaveID).ToList();
                    context.Close();
                    return result;
                }
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }

        }


        public static object getLeaveDetailsByIDForUpdate(object param)
        {
            int LeaveID = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                return context.tLeaveDetail.Where(a => a.LeaveID == LeaveID).FirstOrDefault();

            }
        }



        public static object updateLeave(object param)
        {


            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            tLeaveDetail staff = param as tLeaveDetail;
            var Leave = (from mdl in context.tLeaveDetail where mdl.LeaveID == staff.LeaveID select mdl).SingleOrDefault();
            if (Leave != null)
            {
                Leave.LeaveType = staff.LeaveType;
                Leave.NumofDays = staff.NumofDays;
                Leave.From = staff.From;
                Leave.To = staff.To;
                Leave.StaffID = Leave.StaffID;
                context.SaveChanges();
                context.Close();

            }
            return Leave;



        }






        static object getDeleteLeaveDetailsByID(object param)
        {

            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            int LeaveID = Convert.ToInt32(param);

            tLeaveDetail staff = param as tLeaveDetail;

            var LeaveDelete = (from mdl in context.tLeaveDetail where mdl.LeaveID == LeaveID select mdl).SingleOrDefault();



            if (LeaveDelete != null)
            {

                context.tLeaveDetail.Remove(LeaveDelete);
                context.SaveChanges();

                context.Close();

            }
            return null;

        }



        public static object updatPromotionDetails(object param)
        {


            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            tPromotions staff = param as tPromotions;
            var Promotion = (from mdl in context.tPromotion where mdl.PromotionID == staff.PromotionID select mdl).SingleOrDefault();
            if (Promotion != null)
            {
                Promotion.OrderNumber = staff.OrderNumber;
                Promotion.Designation = staff.Designation;
                Promotion.Branch = staff.Branch;
                Promotion.PayScale = staff.PayScale;
                Promotion.DOJ = staff.DOJ;
                Promotion.From = staff.From;
                Promotion.To = staff.To;
                Promotion.StaffID = Promotion.StaffID;
                context.SaveChanges();
                context.Close();

            }
            return Promotion;



        }

        public static object getBranchListbyID(object param)
        {
            int tBranchID = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                var result = (from list in context.mBranches

                              where list.BranchName != null && list.BranchId == tBranchID
                              select new FillBranch
                              {
                                  BranchId = list.BranchId,
                                  BranchName = list.BranchName
                              }).Distinct().ToList();
                return result;
            }
        }

        public static object getBranchList()
        {
            using (StaffContext context = new StaffContext())
            {
                var result = (from list in context.mBranches

                              where list.BranchName != null
                              select new FillBranch
                              {
                                  BranchId = list.BranchId,
                                  BranchName = list.BranchName
                              }).Distinct().ToList();
                return result;
            }
        }
        #region Establish
        public static object newStaffPension(object param)
        {
            try
            {
                using (StaffContext context = new StaffContext())
                {
                    tPensiondetail staff = param as tPensiondetail;
                    context.tPensiondetail.Add(staff);
                    context.SaveChanges();

                    var result = context.tPensiondetail.ToList().OrderBy(m => m.StaffName).ToList();
                    context.Close();
                    return result;
                }
            }
            catch (DbEntityValidationException ex)
            {

                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);
                var fullErrorMessage = string.Join("; ", errorMessages);
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }

        }

        public static object updateStaffPension(object param)
        {
            using (StaffContext context = new StaffContext())
            {
                tPensiondetail staff = param as tPensiondetail;
                context.Entry(staff).State = EntityState.Modified;
                context.SaveChanges();
                var result = (from mstaff in context.tPensiondetail
                              select mstaff
                                  ).ToList();
                context.Close();
                return result;


            }
        }

        public static object getStaffPensionDetailsByID(object param)
        {
            tPensiondetail model = param as tPensiondetail;
            try
            {
                int staffid = Convert.ToInt32(model.StaffID);
                string staffName = model.StaffName;
                using (StaffContext context = new StaffContext())
                {
                    return context.tPensiondetail.Where(a => a.StaffID == staffid).FirstOrDefault();

                }
            }
            catch (DbEntityValidationException ex)
            {
                throw ex;
            }
        }

        static Int32 GetAdharIdExist(object param)
        {
            try
            {
                using (StaffContext _context = new StaffContext())
                {
                    mStaff model = param as mStaff;
                    var AdharCount = (from materPasses in _context.mStaff
                                      where materPasses.AadharID == model.AadharID
                                      select materPasses).Count();

                    return AdharCount;

                }
            }
            catch (Exception)
            {
                return 0;
                throw;
            }

        }



        public static object TrainingList(object param )
        {
            int StaffId = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                var result = (from training in context.tTrainingDetails
                              where training.StaffID == StaffId
                              select training

                                 ).ToList();
                return result;
            }
        }

        public static object NewTraining(object param)
        {
            try
            {
                using (StaffContext context = new StaffContext())
                {
                    tTrainingDetails Training = param as tTrainingDetails;
                    context.tTrainingDetails.Add(Training);
                    context.SaveChanges();

                    var result = context.tTrainingDetails.ToList().Where(m => m.StaffID == Training.StaffID).OrderBy(m => m.TrainingID).ToList();
                    context.Close();
                    return result;
                }
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }

        }


        public static object getTrainingDetailsByIDForUpdate(object param)
        {
            int TrainingID = Convert.ToInt32(param);
            using (StaffContext context = new StaffContext())
            {
                return context.tTrainingDetails.Where(a => a.TrainingID == TrainingID).FirstOrDefault();

            }
        }


        public static object updateTraining(object param)
        {


            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            tTrainingDetails Training = param as tTrainingDetails;
            var Trainings = (from mdl in context.tTrainingDetails where mdl.TrainingID == Training.TrainingID select mdl).SingleOrDefault();
            if (Training != null)
            {
                Trainings.TrainingType = Training.TrainingType;
                Trainings.Subject = Training.Subject;
                Trainings.From = Training.From;
                Trainings.To = Training.To;
                Trainings.StaffID = Training.StaffID;
                context.Entry(Trainings).State = EntityState.Modified;
                context.SaveChanges();
                context.Close();

            }
            return Trainings;



        }


        static object getDeleteTrainingDetailsByID(object param)
        {

            if (null == param)
            {
                return null;
            }
            StaffContext context = new StaffContext();

            int TrainingID = Convert.ToInt32(param);

            tTrainingDetails staff = param as tTrainingDetails;

            var TrainingDelete = (from mdl in context.tTrainingDetails where mdl.TrainingID == TrainingID select mdl).SingleOrDefault();



            if (TrainingDelete != null)
            {

                context.tTrainingDetails.Remove(TrainingDelete);
                context.SaveChanges();

                context.Close();

            }
            return null;

        }

        #endregion

    }


}



