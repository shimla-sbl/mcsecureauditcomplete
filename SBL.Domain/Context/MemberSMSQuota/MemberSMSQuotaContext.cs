﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SBL.DAL;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.MemberSMSQuota;
using System.Data;
using System.Data.Entity;

namespace SBL.Domain.Context.MemberSMSQuota
{
    class MemberSMSQuotaContext : DBBase<MemberSMSQuotaContext>
    {

        public MemberSMSQuotaContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mMemberSMSQuotaAddOn> mMemberSMSQuotaAddOn { get; set; }
        public DbSet<mMember> mMember { get; set; }
        public DbSet<mUsers> mUser { get; set; }

        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {

                case "CreateMemberSMSQuota": { CreateMemberSMSQuota(param.Parameter); break; }
                case "UpdateMemberSMSQuota": { return UpdateMemberSMSQuota(param.Parameter); }
                case "DeleteMemberSMSQuota": { return DeleteMemberSMSQuota(param.Parameter); }
                case "GetAllMemberSMSQuota1": { return GetAllMemberSMSQuota1(); }
                case "GetAllMemberSMSQuotaList": { return GetAllMemberSMSQuotaList(); }
                case "GetMemberSMSQuotaById": { return GetMemberSMSQuotaById(param.Parameter); }
                case "GetAllMembers": { return GetAllMembers(); }
                case "GetAllMembers1": { return GetAllMembers1(); }
                //case "IsMemberSMSQuotaIdChildExist": { return IsMemberSMSQuotaIdChildExist(param.Parameter); }



            } return null;
        }

        //private static object IsMinistryIdChildExist(object param)
        //{
        //    try
        //    {
        //        using (MinisteryContext db = new MinisteryContext())
        //        {
        //            mMinistry model = (mMinistry)param;
        //            var Res = (from e in db.mMinsitryMinisteries
        //                       where (e.MinistryID == model.MinistryID)
        //                       select e).Count();

        //            if (Res == 0)
        //            {
        //                var MinDept = (from e in db.mMinistryDepartments
        //                           where (e.MinistryID == model.MinistryID)
        //                           select e).Count();
        //                if (MinDept == 0)
        //                {
        //                    return false;
        //                }


        //            }

        //            else
        //            {
        //                return true;

        //            }

        //        }
        //        return true;


        //    }


        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }

        //}

        static void CreateMemberSMSQuota(object param)
        {
            try
            {
                using (MemberSMSQuotaContext db = new MemberSMSQuotaContext())
                {
                    mMemberSMSQuotaAddOn model = param as mMemberSMSQuotaAddOn;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mMemberSMSQuotaAddOn.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateMemberSMSQuota(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberSMSQuotaContext db = new MemberSMSQuotaContext())
            {
                mMemberSMSQuotaAddOn model = param as mMemberSMSQuotaAddOn;
                model.CreatedWhen = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mMemberSMSQuotaAddOn.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllMemberSMSQuota1();
        }

        static object DeleteMemberSMSQuota(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (MemberSMSQuotaContext db = new MemberSMSQuotaContext())
            {
                mMemberSMSQuotaAddOn parameter = param as mMemberSMSQuotaAddOn;
                mMemberSMSQuotaAddOn MemberSMSQuotaToRemove = db.mMemberSMSQuotaAddOn.SingleOrDefault(a => a.Id == parameter.Id);
                if (MemberSMSQuotaToRemove != null)
                {
                    MemberSMSQuotaToRemove.IsDeleted = true;
                }
                //db.mMinsitry.Remove(ministryToRemove);
                db.SaveChanges();
                db.Close();
            }
            return GetAllMemberSMSQuota1();
        }

        static mMemberSMSQuotaAddOn GetMemberSMSQuotaById(object param)
        {
            mMemberSMSQuotaAddOn parameter = param as mMemberSMSQuotaAddOn;
            MemberSMSQuotaContext db = new MemberSMSQuotaContext();
            var query = db.mMemberSMSQuotaAddOn.SingleOrDefault(a => a.Id == parameter.Id);
            return query;
        }

        static List<mMemberSMSQuotaAddOn> GetAllMemberSMSQuotaList()
        {
            MemberSMSQuotaContext db = new MemberSMSQuotaContext();

            var data = (from ministry in db.mMemberSMSQuotaAddOn
                        //join Assem in db.mAssembly on ministry.AssemblyID equals Assem.AssemblyCode into JoinAssemblyName
                        //from assem in JoinAssemblyName.DefaultIfEmpty()
                        join MemName in db.mMember on ministry.MemberCode equals MemName.MemberCode into JoinMemName
                        from memname in JoinMemName.DefaultIfEmpty()
                        where ministry.IsDeleted == false
                        orderby ministry.Id descending
                        select new
                        {
                            ministry,
                            //assem.AssemblyName,
                            memname.Name,
                           
                        });
            List<mMemberSMSQuotaAddOn> list = new List<mMemberSMSQuotaAddOn>();
            foreach (var item in data)
            {
                //item.ministry.GetAssemblyName = item.AssemblyName;
                item.ministry.GetMemberName = item.Name;
                list.Add(item.ministry);

            }
            return list.ToList();


        }

        static List<mMemberSMSQuotaAddOn> GetAllMemberSMSQuota1()
        {
            MemberSMSQuotaContext db = new MemberSMSQuotaContext();

            var data = (from m in db.mMemberSMSQuotaAddOn
                        orderby m.Id ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;
            //var query = db.mMinsitry.ToList();
            //return query;

        }

        static List<mMember> GetAllMembers()
        {
            DBManager db = new DBManager();
            var data = (from m in db.mMembers
                        orderby m.MemberCode ascending
                        where m.IsDeleted == null
                        select m).ToList();
            return data;
            //var query = db.mMembers.ToList();
            //return query;
        }


        static List<mMember> GetAllMembers1()
        {
            MemberSMSQuotaContext db = new MemberSMSQuotaContext();
    
            var data1 = (from a in db.mMember
                         join b in db.mUser on a.AadhaarNo equals b.AadarId
                         where a.AadhaarNo != null && b.AadarId != null && a.IsDeleted == null
                         select a
                         ).Distinct().ToList();

           
            return data1;

        }



    }
}
