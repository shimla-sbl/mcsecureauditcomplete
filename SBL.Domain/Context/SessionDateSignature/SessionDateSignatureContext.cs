﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;

using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.SessionDateSignature;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.SiteSetting;
using SBL.Service.Common;

namespace SBL.Domain.Context.SessionDateSignature
{
    public class SessionDateSignatureContext : DBBase<SessionDateSignatureContext>
    {


        public SessionDateSignatureContext() : base("eVidhan") { }

        public DbSet<tSessionDateSignature> tSessionDateSignature { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mSession> mSession { get; set; }
        public DbSet<mSessionDate> mSessionDate { get; set; }

        public DbSet<SiteSettings> SiteSettings { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "GetAllSessionDateSignature": { return GetAllSessionDateSignature(param.Parameter); }

                    case "AddSessionDateSignature": { return AddSessionDateSignature(param.Parameter); }

                    case "UpdateSessionDateSignature": { return UpdateSessionDateSignature(param.Parameter); }

                    case "GetSessionDateSignatureById": { return GetSessionDateSignatureById(param.Parameter); }

                    case "DeleteSessionDateSignatureById": { return DeleteSessionDateSignatureById(param.Parameter); }


                }
            }
            return null;
        }



        private static object AddSessionDateSignature(object p)
        {
            try
            {
                using (SessionDateSignatureContext db = new SessionDateSignatureContext())
                {
                    tSessionDateSignature data = p as tSessionDateSignature;

                    data.ModifiedWhen = DateTime.Now;
                    data.CreatedWhen = DateTime.Now;


                    db.tSessionDateSignature.Add(data);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        private static object UpdateSessionDateSignature(object p)
        {

            try
            {
                using (SessionDateSignatureContext db = new SessionDateSignatureContext())
                {
                    tSessionDateSignature updateresult = p as tSessionDateSignature;
                    var actualResult = (from SessionDateSignature in db.tSessionDateSignature
                                        where
                                            SessionDateSignature.SessionDateSignatureDetailsId == updateresult.SessionDateSignatureDetailsId
                                        select SessionDateSignature).FirstOrDefault();

                    actualResult.AssemblyId = updateresult.AssemblyId;
                    actualResult.SessionDateId = updateresult.SessionDateId;
                    actualResult.SessionId = updateresult.SessionId;

                    actualResult.ModifiedWhen = DateTime.Now;
                    actualResult.ModifiedBy = updateresult.ModifiedBy;
                    actualResult.SignatureName = updateresult.SignatureName;
                    actualResult.SignatureNameLocal = updateresult.SignatureNameLocal;
                    actualResult.HeaderText = updateresult.HeaderText;
                    actualResult.HeaderTextLocal = updateresult.HeaderTextLocal;
                    actualResult.SignaturePlace = updateresult.SignaturePlace;
                    actualResult.SignaturePlaceLocal = updateresult.SignaturePlaceLocal;
                    actualResult.SignatureDate = updateresult.SignatureDate;
                    actualResult.SignatureDateLocal = updateresult.SignatureDateLocal;
                    actualResult.SignatureDesignations = updateresult.SignatureDesignations;
                    actualResult.SignatureDesignationsLocal = updateresult.SignatureDesignationsLocal;
                    actualResult.IsActive = updateresult.IsActive;
                    db.tSessionDateSignature.Attach(actualResult);
                    db.Entry(actualResult).State = EntityState.Modified;
                    db.SaveChanges();


                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return true;

        }

        private static object DeleteSessionDateSignatureById(object p)
        {
            try
            {
                using (SessionDateSignatureContext db = new SessionDateSignatureContext())
                {
                    tSessionDateSignature id = p as tSessionDateSignature;

                    var result = (from SessionDateSignature in db.tSessionDateSignature
                                  where
                                      SessionDateSignature.SessionDateSignatureDetailsId == id.SessionDateSignatureDetailsId
                                  select SessionDateSignature).FirstOrDefault();

                    db.tSessionDateSignature.Remove(result);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private static object GetAllSessionDateSignature(object p)
        {
            try
            {
                using (SessionDateSignatureContext db = new SessionDateSignatureContext())
                {
                    var relaresult = (from asblyFiles in db.tSessionDateSignature
                                      join assembly in db.mAssembly on asblyFiles.AssemblyId equals assembly.AssemblyCode
                                      join session in db.mSession on
                                         new { SessionCode = asblyFiles.SessionId, assemblyId = asblyFiles.AssemblyId } equals new { SessionCode = session.SessionCode, assemblyId = session.AssemblyID }
                                      join sessiondate
                                          in db.mSessionDate on asblyFiles.SessionDateId equals sessiondate.Id into nullsessiondate
                                      from sessdate in nullsessiondate.DefaultIfEmpty()
                                      orderby asblyFiles.SessionDateSignatureDetailsId descending
                                      select
                                          new
                                          {
                                              assemblyFile = asblyFiles,
                                              assemblyName = assembly.AssemblyName,
                                              sessionName = session.SessionName,
                                              sessiondateName = sessdate != null ? sessdate.SessionDate : default(DateTime),

                                          }).ToList();


                    List<tSessionDateSignature> assemblyFileList = new List<tSessionDateSignature>();


                    foreach (var item in relaresult)
                    {
                        tSessionDateSignature assemblyFile = new tSessionDateSignature();
                        assemblyFile = item.assemblyFile;
                        assemblyFile.AssemblyName = item.assemblyName;
                        assemblyFile.SessionName = item.sessionName;
                        assemblyFile.SessionDate = item.sessiondateName.ToString("dd/MM/yyyy") == "01/01/0001" ? "" : item.sessiondateName.ToString("dd/MM/yyyy");

                        assemblyFileList.Add(assemblyFile);
                    }

                    return assemblyFileList;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object GetSessionDateSignatureById(object p)
        {
            try
            {
                using (SessionDateSignatureContext db = new SessionDateSignatureContext())
                {
                    tSessionDateSignature id = p as tSessionDateSignature;

                    var result = (from SessionDateSignature in db.tSessionDateSignature
                                  where
                                      SessionDateSignature.SessionDateSignatureDetailsId == id.SessionDateSignatureDetailsId
                                  select SessionDateSignature).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



    }



}

