﻿using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Committee;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Notice;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.User;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace SBL.Domain.Context.Minister
{
    class MinisterContext : DBBase<MinisterContext>
    {
        public MinisterContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<tPaperLaidV> tPaperLaidV { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public virtual DbSet<mPaperCategoryType> mPaperCategoryType { get; set; }
        public virtual DbSet<mEvent> mCategoryType { get; set; }
        public virtual DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }
        public virtual DbSet<tCommittee> tCommittee { get; set; }
        public virtual DbSet<tBillRegister> tBillRegister { get; set; }
        public virtual DbSet<tQuestion> tQuestion { get; set; }
        public virtual DbSet<tMemberNotice> tMemberNotice { get; set; }
        public virtual DbSet<mMember> mMembers { get; set; }
      
        public virtual DbSet<mMinistry> mMinistry { get; set; }
        public DbSet<mMinsitryMinister> objmMinistryMinister { get; set; }
        public DbSet<mSessionDate> mSessionDates { get; set; }
        public DbSet<mUserDSHDetails> mUserDSHDetails { get; set; }
        public DbSet<mSiteSettingsVS> mSiteSetting { get; set; }
        public static object Execute(ServiceParameter param)
        {

            switch (param.Method)
            {
                case "GetHashKeyByUIserId":
                    {
                        return GetHashKeyByUIserId(param.Parameter);
                    }


                case "GetMinisterNameByMinistryID":
                    {
                        return GetMinisterNameByMinistryID(param.Parameter);
                    }

                case "GetCategoryByPaperCategoryTypeId":
                    {
                        return GetCategoryByPaperCategoryTypeId(param.Parameter);
                    }

                case "SubmittedMinisterPaperLaidDetails":
                    {
                        return SubmittedMinisterPaperLaidDetails(param.Parameter);
                    }


                case "GetMinisterPaperAttachment":
                    {
                        return GetMinisterPaperAttachment(param.Parameter);
                    }


                case "GetChangedPaperDetails":
                    {
                        return GetChangedPaperDetails(param.Parameter);
                    }
                case "GetSubmitChangedPaperDetails":
                    {
                        return GetSubmitChangedPaperDetails(param.Parameter);
                    }

                case "MinisterPaperLaidDetails":
                    {
                        return MinisterPaperLaidDetails(param.Parameter);
                    }

                case "ChangedGetMinisterPaperAttachment":
                    {
                        return ChangedGetMinisterPaperAttachment(param.Parameter);
                    }

                case "GetMinisterPaperAttachmentByIds":
                    {
                        return GetMinisterPaperAttachmentByIds(param.Parameter);
                    }

                case "InsertAttachment":
                    {
                        return InsertAttachment(param.Parameter);
                    }
                case "InsertSignPathAttachment":
                    {
                        return InsertSignPathAttachment(param.Parameter);
                    }
                case "GetInitialCount":
                    {
                        return GetInitialCount(param.Parameter);
                    }

                case "GetPendings":
                    {
                        return GetPendings();
                    }
                case "SearchByCategory":
                    {
                        return SearchByCategory(param.Parameter);
                    }
                case "GetSubmitted":
                    {
                        return GetSubmitted();
                    }
                case "MinisterName":
                    {
                        return MinisterName(param.Parameter);
                    }
                case "GetDepartmentPaperAttachmentByIds":
                    {
                        return GetDepartmentPaperAttachmentByIds(param.Parameter);
                    }

                /*For Chairperson*/
                case "GetInitialCountForChairpeson":
                    {
                        return GetInitialCountForChairpeson(param.Parameter);
                    }
                case "GetPendingsForChairperson":
                    {
                        return GetPendingsForChairperson(param.Parameter);
                    }
                case "GetSubmittedForChairperson":
                    {
                        return GetSubmittedForChairperson(param.Parameter);
                    }
                case "SearchByCategoryForChairperson": { return SearchByCategoryForChairperson(param.Parameter); }

            }
            return null;
        }

        static object SubmittedMinisterPaperLaidDetails(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV; 
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var query = (from m in db.tPaperLaidV

                         join c in db.mCategoryType on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId

                         join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
                         //where (t.MinisterSubmittedBy == null) && (t.MinisterSubmittedDate == null)
                         //&& (t.PaperLaidId == m.PaperLaidId)
                         where model.PaperLaidId == m.PaperLaidId
                         && m.MinisterActivePaperId == t.PaperLaidTempId
                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             MinisterName = minis.MinisterName + ", " + minis.MinistryName,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             ProvisionUnderWhich = m.ProvisionUnderWhich,
                             DeparmentName = m.DeparmentName,
                             Remark = m.Remark,
                             FilePath = t.FilePath,
                             Description = m.Description,
                             SignedFilePath = t.SignedFilePath,
                             FileName = t.FileName,
                             DesireLayingDate = m.DesireLayingDate,
                             MinisterSubmittedDate = t.MinisterSubmittedDate,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault(),
                             MemberName = (from p in db.tQuestion join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMembers on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).FirstOrDefault();

            var item = query;

            switch (item.PaperCategoryTypeId)
            {
                case 1:
                    item.Number = (from m in db.tQuestion where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                    break;
                case 2:
                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                    break;
                case 4:
                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                    break;
                default:
                    item.Number = "";
                    break;
            }

            return query;

        }


        public static object GetSubmitted()
        {
            MinisterContext CommCtx = new MinisterContext();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();

            var CategoryList = (from C in CommCtx.mCategoryType
                                where C.PaperCategoryTypeId != 3
                                select C).ToList();

            int SubListCnt = (from m in CommCtx.tPaperLaidV
                              join c in CommCtx.mCategoryType on m.EventId equals c.EventId
                              join t in CommCtx.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId

                              where m.DeptActivePaperId == t.PaperLaidTempId

                              && t.MinisterSubmittedDate != null
                              && t.MinisterSubmittedBy != null

                              select new mMinisteryMinisterModel
                              {


                              }).ToList().Count;


            CommCtx.Close();
            Obj.EventList = CategoryList;
            //Obj.ComModel = PndList;
            Obj.MinSentCount = SubListCnt;

            return Obj;
        }



        public static object SearchByCategory(object param)
        {
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel Obj = param as mMinisteryMinisterModel;
            tPaperLaidTemp temp = new tPaperLaidTemp();


            int? EventId = null;

            if (Obj.EventId != 0)
                EventId = Obj.EventId;
            int memberCode = (from m in db.mMembers where m.LoginID == Obj.LoginId select m.MemberCode).SingleOrDefault();


            if (Obj.ActionType == "Pending")
            {


                if (memberCode > 0)
                {
                    int ministryId = (from m in db.mMinistry where m.MemberCode == memberCode select m.MinistryID).SingleOrDefault();
                    if (ministryId > 0)
                    {
                        IEnumerable<mMinisteryMinisterModel> PndList = (from m in db.tPaperLaidV
                                                                        join c in db.mCategoryType on m.EventId equals c.EventId
                                                                        join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                                                        join mins in db.objmMinistryMinister on m.MinistryId equals mins.MinistryID
                                                                        where m.DeptActivePaperId == t.PaperLaidTempId
                                                                            // && m.MinistryId == mins.MinsitryMinistersID
                                                                        && t.MinisterSubmittedDate == null
                                                                        && t.MinisterSubmittedBy == null
                                                                        && (!EventId.HasValue || m.EventId == Obj.EventId)
                                                                        && m.MinistryId == ministryId
                                                                        && ((from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault() != "Committee")
                                                                        select new mMinisteryMinisterModel
                                                                        {
                                                                            DeptActivePaperId = m.DeptActivePaperId,
                                                                            PaperLaidId = m.PaperLaidId,
                                                                            EventName = c.EventName,
                                                                            Title = m.Title,
                                                                            DeparmentName = m.DeparmentName,
                                                                            FilePath = t.FilePath,
                                                                            PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                                                                            PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault(),

                                                                        }).OrderBy(m => m.PaperLaidId).Skip((Obj.PageIndex - 1) * Obj.PAGE_SIZE).Take(Obj.PAGE_SIZE).ToList();

                        foreach (var item in PndList)
                        {
                            switch (item.PaperCategoryTypeId)
                            {
                                case 1:
                                    item.Number = (from m in db.tQuestion where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                                    break;
                                case 2:
                                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                                    break;
                                case 4:
                                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                                    break;
                                default:
                                    break;
                            }

                        }

                        Obj.MinInboxCount = PndList.Count();
                        //Obj.MinSentCount = PndList.Count();
                        Obj.ResultCount = PndList.Count();
                        Obj.ComModel = PndList.ToList();
                    }

                }

            }
            else if (Obj.ActionType == "Submitted")
            {

                if (memberCode > 0)
                {
                    int ministryId = (from m in db.mMinistry where m.MemberCode == memberCode select m.MinistryID).SingleOrDefault();
                    if (ministryId > 0)
                    {
                        IEnumerable<mMinisteryMinisterModel> PndList = (from m in db.tPaperLaidV
                                                                        join c in db.mCategoryType on m.EventId equals c.EventId
                                                                        join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                                                        where m.MinisterActivePaperId == t.PaperLaidTempId
                                                                        && t.MinisterSubmittedDate != null
                                                                        && t.MinisterSubmittedBy != null
                                                                        && (!EventId.HasValue || m.EventId == Obj.EventId)
                                                                        && m.MinistryId == ministryId
                                                                        && ((from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault() != "Committee")
                                                                        select new mMinisteryMinisterModel
                                                                        {
                                                                            DeptActivePaperId = m.DeptActivePaperId,
                                                                            PaperLaidId = m.PaperLaidId,
                                                                            EventName = c.EventName,
                                                                            Title = m.Title,
                                                                            DeparmentName = m.DeparmentName,
                                                                            FilePath = t.FilePath,
                                                                            PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                                                                            PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault(),

                                                                        }).OrderBy(m => m.PaperLaidId).Skip((Obj.PageIndex - 1) * Obj.PAGE_SIZE).Take(Obj.PAGE_SIZE).ToList();


                        foreach (var item in PndList)
                        {
                            switch (item.PaperCategoryTypeId)
                            {
                                case 1:
                                    item.Number = (from m in db.tQuestion where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                                    break;
                                case 2:
                                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                                    break;
                                case 4:
                                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                                    break;
                                default:
                                    item.Number = "";
                                    break;
                            }

                        }

                        //Obj.MinInboxCount = PndList.Count();
                        Obj.MinSentCount = PndList.Count();
                        Obj.ResultCount = PndList.Count();
                        Obj.ComModel = PndList.ToList();

                    }
                }

            }

            db.Close();
            return Obj;

        }
        public static object GetPendings()
        {

            MinisterContext CommCtx = new MinisterContext();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();
            //tPaperLaidTemp temp = param as tPaperLaidTemp;
            var CategoryList = (from C in CommCtx.mCategoryType
                                where C.PaperCategoryTypeId != 3
                                select C).OrderBy(m=>m.PaperCategoryTypeId).ToList();
            int PndListCnt = (from m in CommCtx.tPaperLaidV
                              join c in CommCtx.mCategoryType on m.EventId equals c.EventId
                              join t in CommCtx.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                              join mins in CommCtx.objmMinistryMinister on m.MinistryId equals mins.MinistryID
                              where m.DeptActivePaperId == t.PaperLaidTempId
                              && m.MinistryId == mins.MinsitryMinistersID
                              && t.MinisterSubmittedDate == null
                              && t.MinisterSubmittedBy == null

                              select new mMinisteryMinisterModel
                              {


                              }).ToList().Count;


            CommCtx.Close();
            //Obj.ComModel = PndList;
            Obj.EventList = CategoryList;
            Obj.MinInboxCount = PndListCnt;

            return Obj;
        }

        static mEvent GetCategoryByPaperCategoryTypeId(object param)
        {
            //List<tPaperLaidV> eventList = new List<tPaperLaidV>();
            //mEvent s = new mEvent();
            mEvent s = param as mEvent;
            using (var dbContext = new MinisterContext())
            {
                s.EventList = (from events in dbContext.mCategoryType
                               where events.PaperCategoryTypeId == s.PaperCategoryTypeId

                               select events).ToList();

            }

            return s;
        }



        static List<mMinisteryMinisterModel> GetMinisterPaperAttachment(object param)
        {
            MinisterContext db = new MinisterContext();

            int deptid = Convert.ToInt32(param);


            var query = (from ministerModel in db.tPaperLaidV
                         join paperldontb in db.tPaperLaidTemp on ministerModel.DeptActivePaperId equals paperldontb.PaperLaidTempId
                         where paperldontb.PaperLaidTempId == deptid
                         select new mMinisteryMinisterModel
                         {
                             FilePath = paperldontb.FilePath + paperldontb.FileName,
                             DeptActivePaperId = ministerModel.DeptActivePaperId

                         });


            return query.ToList();
        }


        static List<mMinisteryMinisterModel> GetChangedPaperDetails(object param)
        {
            MinisterContext db = new MinisterContext();
            int PaperLaidId = Convert.ToInt32(param);

            var query = (from tempmodel in db.tPaperLaidTemp
                         join c in db.tPaperLaidV on tempmodel.PaperLaidId equals c.DeptActivePaperId
                         where tempmodel.PaperLaidId == PaperLaidId
                         select new mMinisteryMinisterModel
                         {
                             FileName = tempmodel.FileName,
                             Version = tempmodel.Version,
                             FilePath = tempmodel.FilePath,
                             DesireLayingDate = c.DesireLayingDate,
                             MinisterSubmittedDate = tempmodel.MinisterSubmittedDate,
                             PaperLaidId = tempmodel.PaperLaidId,
                             PaperLaidTempId = tempmodel.PaperLaidTempId,
                             DeptActivePaperId = c.DeptActivePaperId
                         });

            return query.ToList();

        }


        static List<mMinisteryMinisterModel> GetSubmitChangedPaperDetails(object param)
        {
            MinisterContext db = new MinisterContext();
            int PaperLaidId = Convert.ToInt32(param);

            var query = (from tempmodel in db.tPaperLaidTemp
                         join c in db.tPaperLaidV on tempmodel.PaperLaidId equals c.MinisterActivePaperId
                         where tempmodel.PaperLaidId == PaperLaidId
                         && c.MinisterActivePaperId == tempmodel.PaperLaidTempId
                         select new mMinisteryMinisterModel
                         {
                             FileName = tempmodel.FileName,
                             Version = tempmodel.Version,
                             FilePath = tempmodel.FilePath,
                             SignedFilePath = tempmodel.SignedFilePath,
                             MinisterSubmittedDate = tempmodel.MinisterSubmittedDate,
                             PaperLaidId = tempmodel.PaperLaidId,
                             PaperLaidTempId = tempmodel.PaperLaidTempId,
                             MinisterActivePaperId = c.MinisterActivePaperId,
                             MemberName = (from p in db.tQuestion join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMembers on p.MemberID equals k.MemberCode  select k.Name).FirstOrDefault(),
                         });


            return query.ToList();

        }



        static object MinisterPaperLaidDetails(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV; 
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var query = (from m in db.tPaperLaidV

                         join c in db.mCategoryType on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
                         where model.PaperLaidId == m.PaperLaidId
                         && m.DeptActivePaperId == t.PaperLaidTempId
                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             MinisterName = minis.MinisterName + ", " + minis.MinistryName,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             ProvisionUnderWhich = m.ProvisionUnderWhich,
                             DeparmentName = m.DeparmentName,
                             Remark = m.Remark,
                             FilePath = t.FilePath,
                             Description = m.Description,
                             SignedFilePath = t.SignedFilePath + t.FileName,
                             FileName = t.FileName,
                             DesireLayingDate = m.DesireLayingDate,
                             MinisterSubmittedDate = t.MinisterSubmittedDate,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault(),
                             MemberName = (from p in db.tQuestion join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMembers on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                         }).FirstOrDefault();

            var item = query;

            switch (item.PaperCategoryTypeId)
            {
                case 1:
                    item.Number = (from m in db.tQuestion where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                    break;
                case 2:
                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                    break;
                case 4:
                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                    break;
                default:
                    item.Number = "";
                    break;
            }

            return query;

        }

        static List<mMinisteryMinisterModel> ChangedGetMinisterPaperAttachment(object param)
        {
            MinisterContext db = new MinisterContext();

            int paperlaididid = Convert.ToInt32(param);


            var query = (from ministerModel in db.tPaperLaidTemp

                         where ministerModel.PaperLaidTempId == paperlaididid
                         select new mMinisteryMinisterModel
                         {
                             FilePath = ministerModel.FilePath + ministerModel.FileName,
                             PaperLaidTempId = ministerModel.PaperLaidTempId,
                             SignedFilePath = ministerModel.SignedFilePath 
                       
                         });


            return query.ToList();
        }

        static List<mMinisteryMinisterModel> GetMinisterPaperAttachmentByIds(object param)
        {
            MinisterContext db = new MinisterContext();
            string obj = param as string;
            string[] obja = obj.Split(',');
            List<long> ids = new List<long>();
            foreach (string item in obja)
            {
                ids.Add(Convert.ToInt32(item));
            }

            var query = (from ministerModel in db.tPaperLaidV
                         join ministryModel in db.tPaperLaidTemp on ministerModel.DeptActivePaperId equals ministryModel.PaperLaidTempId
                         where ids.Contains(ministerModel.PaperLaidId)

                         select new mMinisteryMinisterModel
                         {
                             FilePath = ministryModel.FilePath,
                             FileName = ministryModel.FileName,
                            DeptActivePaperId = ministerModel.DeptActivePaperId,

                         });


            return query.ToList();
        }

        static List<mMinisteryMinisterModel> GetDepartmentPaperAttachmentByIds(object param)
        {
            MinisterContext db = new MinisterContext();
            string obj = param as string;
            string[] obja = obj.Split(',');
            List<long> ids = new List<long>();
            foreach (string item in obja)
            {
                ids.Add(Convert.ToInt16(item));
            }

            var query = (from ministerModel in db.tPaperLaidV
                         join ministryModel in db.tPaperLaidTemp on ministerModel.PaperLaidId equals ministryModel.PaperLaidId
                         where ministerModel.DeptActivePaperId==ministryModel.PaperLaidTempId
                         && ids.Contains(ministerModel.PaperLaidId)

                         select new mMinisteryMinisterModel
                         {
                             FilePath = ministryModel.FilePath,
                             FileName = ministryModel.FileName,
                             DeptActivePaperId = ministerModel.DeptActivePaperId,

                         });


            return query.ToList();
        }

        public static object InsertAttachment(object param)
        {
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel CM = param as mMinisteryMinisterModel;
            tPaperLaidTemp Obj = new tPaperLaidTemp();

            string msg = "";
            try
            {

                var MinisterObj = db.tPaperLaidTemp.Where(m => m.PaperLaidId == Obj.PaperLaidId).FirstOrDefault();
                Obj = db.tPaperLaidTemp.Single(m => m.PaperLaidId == CM.PaperLaidId);

                Obj.MinisterSubmittedBy = MinisterObj.MinisterSubmittedBy;

                Obj.MinisterSubmittedDate = DateTime.Now;
                Obj.SignedFilePath = MinisterObj.SignedFilePath;
                db.SaveChanges();

                msg = "Success";

            }
            catch (Exception ex)
            {
                msg = ex.Message;
            }
            db.Close();
            return msg;
        }

        public static object InsertSignPathAttachment(object param)
        {
            tPaperLaidTemp pT = param as tPaperLaidTemp;
            string[] obja = pT.FilePath.Split(',');

            foreach (var item in obja)
            {
                using (var obj1 = new MinisterContext())
                {
                    long id = Convert.ToInt16(item);

                   


                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
                                        join ministryModel in obj1.tPaperLaidTemp on PaperLaidModel.DeptActivePaperId equals ministryModel.PaperLaidTempId
                                        where PaperLaidModel.PaperLaidId == id
                                        select ministryModel).FirstOrDefault();
                    if (PaperLaidObj != null)
                    {
                        var filename = PaperLaidObj.FileName;
                        string[] Afile = filename.Split('.');
                        filename = Afile[0];
                        var path = "/PaperLaid/" + pT.AssemblyId + "/" + pT.SessionId + "/Signed/" + filename + "_signed.pdf";

                       

                        PaperLaidObj.SignedFilePath = path;
                        PaperLaidObj.MinisterSubmittedBy = pT.MinisterSubmittedBy;
                        PaperLaidObj.MinisterSubmittedDate = pT.MinisterSubmittedDate;
                    }
                    //obj.tBillRegisterVs.Add(model);
                    obj1.SaveChanges();
                    obj1.Close();
                }
            }

            string meg = "";
            return meg;
        }


        public static object GetInitialCount(object param)
        {
            // tPaperLaidV obj = param as tPaperLaidV;
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel obj = param as mMinisteryMinisterModel;
            tPaperLaidTemp temp = param as tPaperLaidTemp;
            int? EventId = null;
            if (obj.EventId != 0)
                EventId = obj.EventId;
            int memberCode = (from m in db.mMembers where m.LoginID == obj.LoginId select m.MemberCode).SingleOrDefault();
            if (memberCode > 0)
            {
                int ministryId = (from m in db.mMinistry where m.MemberCode == memberCode select m.MinistryID).SingleOrDefault();
                if (ministryId > 0)
                {

                    int PndListCnt = (from m in db.tPaperLaidV
                                      join c in db.mCategoryType on m.EventId equals c.EventId
                                      join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                      join mins in db.objmMinistryMinister on m.MinistryId equals mins.MinistryID
                                      where m.DeptActivePaperId == t.PaperLaidTempId

                                      && t.MinisterSubmittedDate == null
                                      && t.MinisterSubmittedBy == null
                                      && (!EventId.HasValue || m.EventId == obj.EventId)
                                      && m.MinistryId == ministryId
                                      && ((from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault() != "Committee")
                                      select new mMinisteryMinisterModel
                                      {

                                      }).ToList().Count;

                    //int PndListCnt = (from m in db.tPaperLaidV
                    //                  join c in db.mCategoryType on m.EventId equals c.EventId
                    //                  join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                    //                  join mins in db.objmMinistryMinister on m.MinistryId equals mins.MinistryID
                    //                  where m.DeptActivePaperId == t.PaperLaidTempId
                    //                 // && m.MinistryId == mins.MinsitryMinistersID
                    //                  && t.MinisterSubmittedDate == null
                    //                  && t.MinisterSubmittedBy == null
                    //                  && (!EventId.HasValue || m.EventId == obj.EventId)
                    //                  && m.MinistryId == ministryId
                    //                  && ((from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault() != "Committee")
                    //                  select new mMinisteryMinisterModel
                    //                  {


                    //                  }).ToList().Count;



                    int SubListCnt = (from m in db.tPaperLaidV
                                      join c in db.mCategoryType on m.EventId equals c.EventId
                                      join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                      join mins in db.objmMinistryMinister on m.MinistryId equals mins.MinistryID
                                      where m.MinisterActivePaperId == t.PaperLaidTempId

                                      && t.MinisterSubmittedDate != null
                                      && t.MinisterSubmittedBy != null
                                      && (!EventId.HasValue || m.EventId == obj.EventId)
                                      && m.MinistryId == ministryId
                                      && ((from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault() != "Committee")
                                      select new mMinisteryMinisterModel
                                      {

                                      }).ToList().Count;

                    obj.MinInboxCount = PndListCnt;
                    obj.MinSentCount = SubListCnt;
                }
            }
            return obj;
        }



        static object MinisterName(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV; 
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var query = (from m in db.tPaperLaidV

                         join c in db.mCategoryType on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         join minis in db.objmMinistryMinister on m.MinistryId equals minis.MinistryID
                         //where (t.MinisterSubmittedBy == null) && (t.MinisterSubmittedDate == null)
                         //&& (t.PaperLaidId == m.PaperLaidId)
                         where model.PaperLaidId == m.PaperLaidId

                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             MinisterName = minis.MinisterName,

                         }).FirstOrDefault();



            return query;

        }

        private static tPaperLaidV GetMinisterNameByMinistryID(object param)
        {

            tPaperLaidV model = param as tPaperLaidV;
            MinisterContext db = new MinisterContext();
            tPaperLaidTemp temp = new tPaperLaidTemp();
            int MemCode = Convert.ToInt32(model.LoginId);

            int memberCode = (from m in db.mMembers where m.MemberCode == MemCode select m.MemberCode).SingleOrDefault();
            var q = (from assemblyid in db.mSiteSetting
                     where assemblyid.SettingName == "Assembly"
                     select assemblyid.SettingValue).FirstOrDefault();
            int aid = Convert.ToInt32(q);

            if (memberCode > 0)
            {

               // int ministryId = (from m in db.mMinistry where m.MemberCode == memberCode select m.MinistryID).SingleOrDefault();
                int ministryId = (from m in db.objmMinistryMinister where m.MemberCode == memberCode && m.AssemblyID==aid select m.MinistryID).SingleOrDefault();
                if (ministryId > 0)
                {
                    var PndList = (from m in db.tPaperLaidV

                                   join mins in db.objmMinistryMinister on m.MinistryId equals mins.MinistryID
                                   where
                                      m.MinistryId == mins.MinsitryMinistersID
                                      && m.MinistryId == ministryId

                                   select new mMinisteryMinisterModel
                                   {
                                       DeptActivePaperId = m.DeptActivePaperId,
                                       PaperLaidId = m.PaperLaidId,

                                       MinisterName = mins.MinisterName,

                                   }).ToList();


                    model.MinInboxCount = PndList.Count();
                    //Obj.MinSentCount = PndList.Count();
                    model.ResultCount = PndList.Count();

                    model.mMinisteryMinister = PndList.ToList();
                }
                model.MinistryId = ministryId;
               
            }

            return model;

        }


        private static tPaperLaidV GetHashKeyByUIserId(object param)
        {

            tPaperLaidV model = param as tPaperLaidV;
            MinisterContext db = new MinisterContext();
            tPaperLaidTemp temp = new tPaperLaidTemp();


            string HashKey = (from m in db.mUserDSHDetails where m.UserId == model.UserID select m.HashKey).SingleOrDefault();
            model.HashKey = HashKey;
           
                //var PndList = (from m in db.tPaperLaidV

                //               join mins in db.mUserDSHDetails on m.UserID equals mins.UserId
                //               where
                //                 m.UserID == mins.UserId
                                

                //                   select new mMinisteryMinisterModel
                //                   {
                //                      HashKey = mins.HashKey

                //                   }).ToList();


                  
            return model;

        }


        #region "CommitteeChairperson"

        public static object GetInitialCountForChairpeson(object param)
        {
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel obj = param as mMinisteryMinisterModel;

            int PndListCnt = (from Pap in db.tPaperLaidV
                              join tempP in db.tPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
                              join evnt in db.mCategoryType on Pap.EventId equals evnt.EventId
                              where (tempP.MinisterSubmittedBy == null) && (tempP.MinisterSubmittedDate == null)
                              && Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId
                              orderby tempP.PaperLaidId
                              select new mMinisteryMinisterModel
                              {
                              }).ToList().Count;

            int SubListCnt = (from Pap in db.tPaperLaidV
                              join tempP in db.tPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
                              join evnt in db.mCategoryType on Pap.EventId equals evnt.EventId
                              where (tempP.MinisterSubmittedBy != null) && (tempP.MinisterSubmittedDate != null)
                              && (Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId)
                              orderby tempP.PaperLaidId
                              select new mMinisteryMinisterModel
                              {
                              }).ToList().Count;


            obj.MinInboxCount = PndListCnt;
            obj.MinSentCount = SubListCnt;

            return obj;
        }

        public static object GetPendingsForChairperson(object param)
        {
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();

            var CategoryList = (from C in db.mCategoryType
                                where C.PaperCategoryTypeId == 3
                                select C).ToList();
            int PndListCnt = (from Pap in db.tPaperLaidV
                              join tempP in db.tPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
                              join evnt in db.mCategoryType on Pap.EventId equals evnt.EventId
                              where (tempP.MinisterSubmittedBy == null) && (tempP.MinisterSubmittedDate == null)
                              && Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId
                              orderby tempP.PaperLaidId
                              select new mMinisteryMinisterModel
                              {
                              }).ToList().Count;
            db.Close();

            Obj.EventList = CategoryList;
            Obj.ComInboxCount = PndListCnt;
            return Obj;
        }

        public static object GetSubmittedForChairperson(object param)
        {
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel Obj = new mMinisteryMinisterModel();

            var CategoryList = (from C in db.mCategoryType
                                where C.PaperCategoryTypeId == 3
                                select C).ToList();
            int SubListCnt = (from Pap in db.tPaperLaidV
                              join tempP in db.tPaperLaidTemp on Pap.PaperLaidId equals tempP.PaperLaidId
                              join evnt in db.mCategoryType on Pap.EventId equals evnt.EventId
                              where (tempP.MinisterSubmittedBy != null) && (tempP.MinisterSubmittedDate != null)
                              && Pap.CommitteeActivePaeperId == tempP.PaperLaidTempId
                              orderby tempP.PaperLaidId
                              select new mMinisteryMinisterModel
                              {
                              }).ToList().Count;
            db.Close();

            Obj.EventList = CategoryList;
            Obj.ComSentCount = SubListCnt;
            return Obj;
        }

        public static object SearchByCategoryForChairperson(object param)
        {
            MinisterContext db = new MinisterContext();
            mMinisteryMinisterModel Obj = param as mMinisteryMinisterModel;
            tPaperLaidTemp temp = new tPaperLaidTemp();

            int? EventId = null;
            if (Obj.EventId != 0)
                EventId = Obj.EventId;

            if (Obj.ActionType == "Pending")
            {
                IEnumerable<mMinisteryMinisterModel> PndList = (from m in db.tPaperLaidV
                                                                join c in db.mCategoryType on m.EventId equals c.EventId
                                                                join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                                                where m.CommitteeActivePaeperId == t.PaperLaidTempId
                                                                && t.MinisterSubmittedDate == null
                                                                && t.MinisterSubmittedBy == null
                                                                && (!EventId.HasValue || m.EventId == Obj.EventId)
                                                                && ((from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault() == "Committee")
                                                                select new mMinisteryMinisterModel
                                                                {
                                                                    DeptActivePaperId = m.CommitteeActivePaeperId,
                                                                    PaperLaidId = m.PaperLaidId,
                                                                    EventName = c.EventName,
                                                                    Title = m.Title,
                                                                    DeparmentName = m.DeparmentName,
                                                                    FilePath = t.FilePath,
                                                                    FileName = t.FileName,
                                                                    PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                                                                    PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault(),

                                                                }).OrderBy(m => m.PaperLaidId).Skip((Obj.PageIndex - 1) * Obj.PAGE_SIZE).Take(Obj.PAGE_SIZE).ToList();



                //foreach (var item in PndList)
                //{
                //    switch (item.PaperCategoryTypeId)
                //    {
                //        case 1:
                //            item.Number = (from m in db.tQuestion where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                //            break;
                //        case 2:
                //            item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                //            break;
                //        case 4:
                //            item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                //            break;
                //        default:
                //            break;
                //    }

                //}

                Obj.ResultCount = PndList.Count();
                Obj.ComModel = PndList.ToList();
            }
            else if (Obj.ActionType == "Submitted")
            {
                IEnumerable<mMinisteryMinisterModel> PndList = (from m in db.tPaperLaidV
                                                                join c in db.mCategoryType on m.EventId equals c.EventId
                                                                join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                                                                where m.CommitteeActivePaeperId == t.PaperLaidTempId
                                                                && t.MinisterSubmittedDate != null
                                                                && t.MinisterSubmittedBy != null
                                                                && (!EventId.HasValue || m.EventId == Obj.EventId)
                                                                && ((from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault() == "Committee")
                                                                select new mMinisteryMinisterModel
                                                                {
                                                                    DeptActivePaperId = m.CommitteeActivePaeperId,
                                                                    PaperLaidId = m.PaperLaidId,
                                                                    EventName = c.EventName,
                                                                    Title = m.Title,
                                                                    DeparmentName = m.DeparmentName,
                                                                    FilePath = t.FilePath,
                                                                    FileName = t.FileName,
                                                                    PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                                                                    PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mCategoryType on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault(),

                                                                }).OrderBy(m => m.PaperLaidId).Skip((Obj.PageIndex - 1) * Obj.PAGE_SIZE).Take(Obj.PAGE_SIZE).ToList();



                //foreach (var item in PndList)
                //{
                //    switch (item.PaperCategoryTypeId)
                //    {
                //        case 1:
                //            item.Number = (from m in db.tQuestion where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                //            break;
                //        case 2:
                //            item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                //            break;
                //        case 4:
                //            item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                //            break;
                //        default:
                //            break;
                //    }

                //}

                Obj.ResultCount = PndList.Count();
                Obj.ComModel = PndList.ToList();


            }

            db.Close();
            return Obj;

        }
        #endregion

    }

}

