﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DAL;
using SBL.DomainModel.Models.RecipientGroups;
using SBL.DomainModel.Models.SystemModule;
using SBL.Service.Common;


namespace SBL.Domain.Context.SystemModule
{
    public class SystemModuleContex : DBBase<SystemModuleContex>
    {
        public SystemModuleContex() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public virtual DbSet<SBL.DomainModel.Models.SystemModule.SystemModule> SystemModule { get; set; }
        public virtual DbSet<ModuleAction> ModuleAction { get; set; }
        public virtual DbSet<RecipientGroup> RecipientGroups { get; set; }
        public DbSet<RecipientGroupMember> RecipientGroupMembers { get; set; }
        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetModuleList":
                    {
                        return GetModuleList(param.Parameter);
                    }
                case "SaveEntryModules":
                    {
                        return SaveEntryModules(param.Parameter);
                    }
                case "GetModuleDataById":
                    {
                        return GetModuleDataById(param.Parameter);
                    }
                case "UpdateEntryModules":
                    {
                        return UpdateEntryModules(param.Parameter);
                    }
                case "DeleteModule":
                    {
                        return DeleteModule(param.Parameter);
                    }
                case "GetModuleActionList":
                    {
                        return GetModuleActionList(param.Parameter);
                    }
                case "GetModules":
                    {
                        return GetModules(param.Parameter);
                    }
                case "GetRecipientList":
                    {
                        return GetRecipientList(param.Parameter);
                    }
                case "SaveModulesAction":
                    {
                        return SaveModulesAction(param.Parameter);
                    }
                case "GetRecipList":
                    {
                        return GetRecipList(param.Parameter);
                    }
                case "UpdateModulesAction":
                    {
                        return UpdateModulesAction(param.Parameter);
                    }
                case "GetActionModuleDataById":
                    {
                        return GetActionModuleDataById(param.Parameter);
                    }
                case "GetTemplate":
                    {
                        return GetTemplate(param.Parameter);
                    }
                case "GetValue":
                    {
                        return GetValue(param.Parameter);
                    }
                case "UpdateModulesActionTemplates":
                    {
                        return UpdateModulesActionTemplates(param.Parameter);
                    }
                    
                case "GetSystemModuleByName": { return GetSystemModuleByName(param.Parameter);}
                case "GetModuleActionByName": { return GetModuleActionByName(param.Parameter); }
            }
            return null;
        }
        public static object GetModuleList(object param)
        {

            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex pCtxt = new SystemModuleContex();
            var query = (from List in pCtxt.SystemModule
                         select new SystemModuleModel
                         {
                             SystemModuleID = List.ModuleIdID,
                             Name = List.ModuleName,
                             Description = List.ModuleDescription
                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.objList = results;



            return model;

        }
        static object SaveEntryModules(object param)
        {
            SBL.DomainModel.Models.SystemModule.SystemModule Save = param as SBL.DomainModel.Models.SystemModule.SystemModule;
            SystemModuleContex pCtxt = new SystemModuleContex();
            Save.ModuleName = Save.ModuleName;
            Save.ModuleDescription = Save.ModuleDescription;
            pCtxt.SystemModule.Add(Save);
            pCtxt.SaveChanges();
            string msg = "";

            return msg;
        }
        static SystemModuleModel GetModuleDataById(object param)
        {

            SystemModuleModel model = param as SystemModuleModel;

            SystemModuleContex pCtxt = new SystemModuleContex();

            var query = (from Data in pCtxt.SystemModule
                         where (Data.ModuleIdID == model.SystemModuleID)
                         select new SystemModuleModel
                         {
                             SystemModuleID = Data.ModuleIdID,
                             Name = Data.ModuleName,
                             Description = Data.ModuleDescription,

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.objList = results;

            return model;
        }
        static object UpdateEntryModules(object param)
        {
            //SBL.DomainModel.Models.SystemModule.SystemModule Save = param as SBL.DomainModel.Models.SystemModule.SystemModule;
            //SystemModuleContex pCtxt = new SystemModuleContex();
            //Save.ModuleName = Save.ModuleName;
            //Save.ModuleDescription = Save.ModuleDescription;
            //pCtxt.SystemModule.Add(Save);
            //pCtxt.SaveChanges();

            SystemModuleContex QCtxt = new SystemModuleContex();
            SBL.DomainModel.Models.SystemModule.SystemModule update = param as SBL.DomainModel.Models.SystemModule.SystemModule;

            SBL.DomainModel.Models.SystemModule.SystemModule obj = QCtxt.SystemModule.Single(m => m.ModuleIdID == update.ModuleIdID);
            obj.ModuleName = update.ModuleName;
            obj.ModuleDescription = update.ModuleDescription;


            QCtxt.SaveChanges();


            string msg = "";

            return msg;
        }
        static object DeleteModule(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (SystemModuleContex db = new SystemModuleContex())
            {
                SBL.DomainModel.Models.SystemModule.SystemModule parameter = param as SBL.DomainModel.Models.SystemModule.SystemModule;
                SBL.DomainModel.Models.SystemModule.SystemModule constituencyToRemove = db.SystemModule.SingleOrDefault(a => a.ModuleIdID == parameter.ModuleIdID);
                db.SystemModule.Remove(constituencyToRemove);
                db.SaveChanges();
                db.Close();
            }
            string msg = "";

            return msg;
        }

        static object GetSystemModuleByName(object param)
        {
            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex pCtxt = new SystemModuleContex();
            var query = (from Data in pCtxt.SystemModule
                         where (Data.ModuleName.Contains(model.Name))
                         select new SystemModuleModel
                         {
                             SystemModuleID = Data.ModuleIdID,
                             Name = Data.ModuleName,
                             Description = Data.ModuleDescription
                         }).FirstOrDefault();

            return query;
        }

        static object GetModuleActionByName(object param)
        {
            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex pCtxt = new SystemModuleContex();
            var query = (from Data in pCtxt.ModuleAction
                         where (Data.ActionName.Contains(model.ActionName))
                         select new SystemModuleModel
                         {
                             SendEmail = Data.SendEmail,
                             SendSMS = Data.SendSMS,
                             EmailTemplate = Data.EmailTemplate,
                             SMSTemplate = Data.SMSTemplate,
                             AssociatedContactGroups = Data.AssociatedContactGroups
                         }).FirstOrDefault();

            return query;
        }
        public static object GetModuleActionList(object param)
        {

            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex pCtxt = new SystemModuleContex();
            var query = (from List in pCtxt.ModuleAction
                         select new SystemModuleModel
                         {
                             SystemModuleID = List.SystemFunctionID,
                             ActionName = List.ActionName,
                             //Name = List.Name,
                             AssociatedContactGroups = List.AssociatedContactGroups,
                             Name = (from mc in pCtxt.SystemModule where mc.ModuleIdID == List.ModuleId select (mc.ModuleName)).FirstOrDefault(),
                             //AssociatedContactGroups = (from mc in pCtxt.RecipientGroup where mc.GroupID == List.AssociatedContactGroups select (mc.ModuleName)).FirstOrDefault(),
                         }).ToList();

            foreach (var val in query)
            {
                if (val.AssociatedContactGroups != null)
                {
                    string s = val.AssociatedContactGroups;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int GroupId = int.Parse(values[i]);
                            var item = (from Val in pCtxt.RecipientGroups
                                        where Val.GroupID == GroupId
                                        select new SystemModuleModel
                                        {
                                            RecipName = Val.Name
                                        }).FirstOrDefault();

                            if (string.IsNullOrEmpty(val.RecipName))
                            {
                                val.groupNameIDPair.Add(new KeyValuePair<Int32, string>(GroupId, item.RecipName));
                                val.RecipName = item.RecipName;
                            }
                            else
                            {
                                val.groupNameIDPair.Add(new KeyValuePair<Int32, string>(GroupId, item.RecipName));
                                val.RecipName = val.RecipName + "," + item.RecipName;
                            }


                        }
                    }

                }
            }
            int totalRecords = query.Count();
            var results = query.ToList();
            model.objList = results;

            return model;

        }
        static object GetModules(object param)
        {
            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex Ustxt = new SystemModuleContex();
            var query = (from User in Ustxt.SystemModule
                         orderby User.ModuleName ascending
                         select new SystemModuleModel
                         {
                             SystemModuleID = User.ModuleIdID,
                             Name = User.ModuleName

                         }).ToList();


            var results = query.ToList();
            model.objList = results;

            return model;
        }
        static object GetRecipientList(object param)
        {
            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex Ustxt = new SystemModuleContex();

            var query = (from User in Ustxt.RecipientGroups
                         orderby User.Name ascending
                         select new SystemModuleModel
                         {
                             RecipGroupID = User.GroupID,
                             RecipName = User.Name

                         }).ToList();


            var results = query.ToList();
            model.RecipList = results;

            return model;
        }
        static object SaveModulesAction(object param)
        {
            ModuleAction Save = param as ModuleAction;
            SystemModuleContex pCtxt = new SystemModuleContex();
            Save.ModuleId = Save.ModuleId;
            Save.ActionName = Save.ActionName;
            Save.Parameters = Save.Parameters;
            Save.AssociatedContactGroups = Save.AssociatedContactGroups;
            Save.Description = Save.Description;
            Save.SendEmail = Save.SendEmail;
            Save.SendSMS = Save.SendSMS;
            pCtxt.ModuleAction.Add(Save);
            pCtxt.SaveChanges();
            string msg = "";

            return msg;
        }
        static object GetRecipList(object param)
        {
            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex Ustxt = new SystemModuleContex();
            var query = (from User in Ustxt.RecipientGroupMembers
                         where (User.GroupID == model.GroupId)
                         select new SystemModuleModel
                         {
                             Name = User.Name,
                             Designation = User.Designation,
                             EMail = User.Email,
                             Mobile = User.MobileNo


                         }).ToList();


            var results = query.ToList();
            model.objList = results;

            return model;
        }
        static object UpdateModulesAction(object param)
        {


            SystemModuleContex QCtxt = new SystemModuleContex();
            ModuleAction update = param as ModuleAction;

            ModuleAction obj = QCtxt.ModuleAction.Single(m => m.SystemFunctionID == update.SystemFunctionID);
            obj.ModuleId = update.ModuleId;
            obj.ActionName = update.ActionName;
            obj.Parameters = update.Parameters;
            obj.AssociatedContactGroups = update.AssociatedContactGroups;
            obj.Description = update.Description;
            obj.SendEmail = update.SendEmail;
            obj.SendSMS = update.SendSMS;

            QCtxt.SaveChanges();


            string msg = "";

            return msg;
        }
        static SystemModuleModel GetActionModuleDataById(object param)
        {

            SystemModuleModel model = param as SystemModuleModel;

            SystemModuleContex pCtxt = new SystemModuleContex();

            var query = (from Data in pCtxt.ModuleAction
                         where (Data.SystemFunctionID == model.SystemModuleID)
                         select new SystemModuleModel
                         {
                             UAId = Data.SystemFunctionID,
                             ModuleId = Data.ModuleId,
                             ActionName = Data.ActionName,
                             Parameters = Data.Parameters,
                             AssociatedContactGroups = Data.AssociatedContactGroups,
                             Description = Data.Description,
                             SendEmail = Data.SendEmail,
                             SendSMS = Data.SendSMS,

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            model.UpdatevalueList = results;

            return model;
        }
        static object GetTemplate(object param)
        {
            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex Ustxt = new SystemModuleContex();
            var query = (from User in Ustxt.ModuleAction
                         where (User.SystemFunctionID == model.SystemModuleID)
                         select new SystemModuleModel
                         {
                             SystemModuleID = User.SystemFunctionID,
                             Parameters = User.Parameters,
                         }).ToList();
            List<string> assignedtoList = new List<string>();

            foreach (var val in query)
            {
                if (val.Parameters != null)
                {
                    string s = val.Parameters;
                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {

                            model.Parameters = "--select Parameters--";

                            // model.ParamList.AddRange(assignedtoList);
                            assignedtoList.Add(values[i]);


                        }
                    }

                }
            }

            var results = query.ToList();
            model.objList = results;
            model.ParamList.AddRange(assignedtoList);
            return model;
        }
        static object GetValue(object param)
        {
            SystemModuleModel model = param as SystemModuleModel;
            SystemModuleContex Ustxt = new SystemModuleContex();
            var query = (from User in Ustxt.ModuleAction
                         where (User.SystemFunctionID == model.SystemModuleID)
                         select new SystemModuleModel
                         {
                             SMSTemplate = User.SMSTemplate,
                             EmailTemplate = User.EmailTemplate,
                         }).FirstOrDefault();

            model.SMSTemplate = query.SMSTemplate;
            model.EmailTemplate = query.EmailTemplate;
            //var results = query.ToList();
            //model.objList = results;


            return model;
        }
        static object UpdateModulesActionTemplates(object param)
        {
            SystemModuleContex QCtxt = new SystemModuleContex();
            ModuleAction update = param as ModuleAction;
            ModuleAction obj = QCtxt.ModuleAction.Single(m => m.SystemFunctionID == update.SystemFunctionID);
            obj.EmailTemplate = update.EmailTemplate;
            obj.SMSTemplate = update.SMSTemplate;
            QCtxt.SaveChanges();
            string msg = "";

            return msg;
        }
    }
}
