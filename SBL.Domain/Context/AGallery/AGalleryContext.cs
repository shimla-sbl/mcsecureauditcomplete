﻿

namespace SBL.Domain.Context.AGallery
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using SBL.DAL;
    using SBL.Service.Common;
    using System.Data;
    using System.Data.Entity;
    using SBL.DomainModel.Models.Gallery;
    using SBL.DomainModel.Models.Category;
    using SBL.DomainModel.Models.SiteSetting;
    using SBL.DomainModel.Models.GalleryThumb;
    using SBL.DomainModel.Models.AlbumCategory;
    public class AGalleryContext : DBBase<AGalleryContext>
    {
        public AGalleryContext() : base("eVidhan") { }

        public DbSet<tGallery> tGallery { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<SiteSettings> mSiteSettings { get; set; }
        public virtual DbSet<tGalleryThumb> tGalleryThumb { get; set; }
        public virtual DbSet<AlbumCategory> tAlbumcategory { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "AddGallery": { return AddGallery(param.Parameter); }
                    case "GetAllGallery": { return GetAllGallery(param.Parameter); }
                    case "GetAllGallery1": { return GetAllGallery1(param.Parameter); }
                    case "UpdateGallery": { return UpdateGallery(param.Parameter); }
                    case "DeleteGallery": { DeleteGallery(param.Parameter); break; }
                    case "GetGalleryById": { return GetGalleryById(param.Parameter); }
                    case "GetAllCategoryType": { return GetAllCategoryType(param.Parameter); }
                    case "GetAllCategoryType1": { return GetAllCategoryType1(param.Parameter); }


                    case "AddGalleryCategory": { return AddGalleryCategory(param.Parameter); }
                    case "GetAllGalleryCategory": { return GetAllGalleryCategory(param.Parameter); }
                    case "GetAllGalleryCategory1": { return GetAllGalleryCategory1(param.Parameter); }
                    case "UpdateGalleryCategory": { return UpdateGalleryCategory(param.Parameter); }
                    case "DeleteGalleryCategory": { DeleteGalleryCategory(param.Parameter); break; }
                    case "GetGalleryByIdCategory": { return GetGalleryByIdCategory(param.Parameter); }
                    case "IsGalleryCategoryChildExist": { return IsGalleryCategoryChildExist(param.Parameter); }
                    case "GetGalleryImagesByCategoryId": { return GetGalleryImagesByCategoryId(param.Parameter); }
                    case "DeleteGalleryThumbForCategory": { return DeleteGalleryThumbForCategory(param.Parameter); }
                    case "GetDropDownData":
                        {
                            return GetDropDownData();
                        }
                    case "GetDropDownData1":
                        {
                            return GetDropDownData1(param.Parameter);
                        }

                }
            }
            return null;
        }

        private static object GetDropDownData()
        {
            AGalleryContext db = new AGalleryContext();
            var Res = (from e in db.tAlbumcategory select e).ToList();
            return Res;
        }

      

        private static object DeleteGalleryThumbForCategory(object p)
        {
            try
            {
                tGallery DataTodelete = p as tGallery;
                //int id = DataTodelete.GalleryId;
                using (AGalleryContext db = new AGalleryContext())
                {
                    var galleryThumbToDelete = (from a in db.tGalleryThumb where a.GalleryId == DataTodelete.GalleryId select a).FirstOrDefault();
                    if (galleryThumbToDelete != null)
                    {
                        db.tGalleryThumb.Remove(galleryThumbToDelete);
                        db.SaveChanges();
                        db.Close();
                    }
                }
            }
            catch
            {
                throw;
            }

            return true;
        }

        private static object GetGalleryImagesByCategoryId(object p)
        {
            try
            {

                var category = (Category)p;
                using (AGalleryContext db = new AGalleryContext())
                {
                    //Category cc= param as Category;
                    //Organization Organization = param as Organization;
                    var data = (from value in db.tGallery where value.CategoryID == category.CategoryID && value.Status == 1 select value).ToList();
                    //var data = db.Category.Where(

                    return data;
                }
            }
            catch
            {
                throw;
            }
        }


        static object GetAllCategoryType(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {
                    //Category cc= param as Category;
                    //Organization Organization = param as Organization;
                    var data = (from value in db.Category where value.CategoryType == 3 && value.Status == 1 select value).ToList();
                    //var data = db.Category.Where(

                    return data;
                }
            }
            catch
            {
                throw;
            }
        }

        static object AddGallery(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {
                    tGallery gallery = param as tGallery;
                    gallery.CreatedDate = DateTime.Now;
                    gallery.ModifiedDate = DateTime.Now;
                    gallery.StartPublish = DateTime.Now;
                    gallery.StopPublish = DateTime.Now;
                    //gallery.CreatedOn = DateTime.Now;
                    db.tGallery.Add(gallery);
                    db.SaveChanges();
                    db.Close();
                    return gallery.GalleryId;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        static object GetAllGallery(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {
                    int Id = Convert.ToInt32(param);

                    var data1 = (from a in db.tAlbumcategory
                                 join c in db.Category
                                     on a.AlbumCategoryID equals c.AlbumCategoryId
                                 join b in db.tGallery on c.CategoryID equals b.CategoryID
                                 //where a.MemberCode == Id
                                 orderby b.GalleryId descending
                                 select new { Gallery = b, CategoryTypeName = c.CategoryName }).ToList();
                    //  var data1 = (from a in db.tAlbumcategory join c in db.Category on a.AlbumCategoryID equals c.AlbumCategoryId where a.MemberCode == Id select a).ToList();
                    // var data = (from a in db.tGallery join category in db.Category on a.CategoryID equals category.CategoryID select new { Gallery = a, CategoryTypeName = category.CategoryName }).ToList();//db.mNews.ToList();

                    var siteSettingData = (from a in db.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();

                    List<tGallery> result = new List<tGallery>();

                    foreach (var a in data1)
                    {
                        tGallery partdata = new tGallery();
                        partdata = a.Gallery;
                        partdata.CategoryName = a.CategoryTypeName;
                        partdata.ImageAcessurl = siteSettingData.SettingValue + a.Gallery.FilePath + "/Thumb/" + a.Gallery.FileName;
                        result.Add(partdata);
                    }
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }

        static object UpdateGallery(object param)
        {

            try
            {
                tGallery UpdateGallery = param as tGallery;
                int tChangedgalCategoryId = UpdateGallery.CategoryID;
                int tActualdalCategoryId = 0;
                using (AGalleryContext db = new AGalleryContext())
                {

                    var updategallery = (from a in db.tGallery where a.GalleryId == UpdateGallery.GalleryId select a).FirstOrDefault();
                    tActualdalCategoryId = updategallery.CategoryID;
                    //UpdateGallery.CreatedOn = DateTime.Now;
                    // UpdateGallery.CreatedDate = DateTime.Now;
                    updategallery.ModifiedDate = DateTime.Now;
                    updategallery.StartPublish = DateTime.Now;
                    updategallery.StopPublish = DateTime.Now;
                    updategallery.GalleryDescription = UpdateGallery.GalleryDescription;
                    updategallery.GalleryTitle = UpdateGallery.GalleryTitle;
                    updategallery.LocalGalleryDescription = UpdateGallery.LocalGalleryDescription;
                    updategallery.LocalGalleryTitle = UpdateGallery.LocalGalleryTitle;
                    updategallery.ModifiedBy = UpdateGallery.ModifiedBy;
                    updategallery.Status = UpdateGallery.Status;
                    updategallery.CategoryID = UpdateGallery.CategoryID;
                    updategallery.FileName = UpdateGallery.FileName;
                    updategallery.FilePath = UpdateGallery.FilePath;
                    updategallery.GalleryType = UpdateGallery.GalleryType;
                    updategallery.ThumbName = UpdateGallery.ThumbName;
                    db.tGallery.Attach(updategallery);
                    db.Entry(updategallery).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                if (tActualdalCategoryId != tChangedgalCategoryId)
                {
                    using (AGalleryContext db = new AGalleryContext())
                    {
                        var updategallerythumb = (from a in db.tGalleryThumb where a.GalleryId == UpdateGallery.GalleryId && a.CategoryId == tActualdalCategoryId select a).FirstOrDefault();

                        if (updategallerythumb != null)
                        {
                            db.tGalleryThumb.Remove(updategallerythumb);
                            db.SaveChanges();
                            db.Close();
                        }
                    }
                }
                return UpdateGallery;
            }
            catch
            {
                throw;
            }
        }

        static void DeleteGallery(object param)
        {
            try
            {
                tGallery DataTodelete = param as tGallery;
                int id = DataTodelete.GalleryId;
                using (AGalleryContext db = new AGalleryContext())
                {
                    var GalleryToDelete = db.tGallery.SingleOrDefault(a => a.GalleryId == id);
                    db.tGallery.Remove(GalleryToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetGalleryById(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {
                    tGallery GalleryToEdit = param as tGallery;

                    var data = db.tGallery.SingleOrDefault(o => o.GalleryId == GalleryToEdit.GalleryId);
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }


        static object GetAllGalleryCategory(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {

                    int Id = Convert.ToInt32(param);
                    var data1 = (from e in db.tAlbumcategory
                                 join c in db.Category on e.AlbumCategoryID equals c.AlbumCategoryId
                                 join galthmcat in db.tGalleryThumb on c.CategoryID equals galthmcat.CategoryId
                                     into CatGal
                                 from catGalData in CatGal.DefaultIfEmpty()
                                 where c.CategoryType == 3 && c.Status == 1 
                                 //&& e.MemberCode == Id
                                 select new { e.AlbumCategoryID, c, GalleryId = (catGalData != null ? catGalData.GalleryId : 0) }).ToList();

                    //var data = (from categroy in db.Category
                    //            join galthmcat in db.tGalleryThumb on categroy.CategoryID equals galthmcat.CategoryId
                    //                into CatGal
                    //            from catGalData in CatGal.DefaultIfEmpty()
                    //            where categroy.CategoryType == 3 && categroy.Status == 1
                    //            select new { categroy, GalleryId = (catGalData != null ? catGalData.GalleryId : 0) }).ToList();

                    var categoryData = (from category in data1
                                        join gallery in db.tGallery on category.GalleryId equals gallery.GalleryId
                                            into catgal
                                        from galCat in catgal.DefaultIfEmpty()
                                        select
                                            new { category, Galleryimagepath = (galCat != null ? galCat.FilePath + "/Thumb/" + galCat.FileName : String.Empty) }).ToList();

                    List<Category> outputResult = new List<Category>();
                    foreach (var item in categoryData)
                    {
                        Category catData = new Category();

                        catData.CategoryID = item.category.c.CategoryID;
                        catData.GalleryId = item.category.GalleryId;
                        catData.CategoryName = item.category.c.CategoryName;
                        catData.LocalCategoryName = item.category.c.LocalCategoryName;
                        catData.Status = item.category.c.Status;
                        catData.GalleryThmImgUrl = item.Galleryimagepath;
                        catData.CreatedDate = item.category.c.CreatedDate;

                        outputResult.Add(catData);

                    }


                    return outputResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        static object AddGalleryCategory(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {
                    Category gallery = param as Category;
                    gallery.CategoryType = 3;
                    gallery.CreatedDate = DateTime.Now;
                    gallery.ModifiedDate = DateTime.Now;
                    gallery.StartPublish = DateTime.Now;
                    gallery.StopPublish = DateTime.Now;
                    db.Category.Add(gallery);
                    db.SaveChanges();
                    // db.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

        static object UpdateGalleryCategory(object param)
        {

            try
            {
                Category UpdateNews = param as Category;
                using (AGalleryContext db = new AGalleryContext())
                {
                    UpdateNews.CategoryType = 3;
                    UpdateNews.CreatedDate = DateTime.Now;
                    UpdateNews.ModifiedDate = DateTime.Now;
                    UpdateNews.StartPublish = DateTime.Now;
                    UpdateNews.StopPublish = DateTime.Now;
                    UpdateNews.LocalCategoryName = UpdateNews.LocalCategoryName;
                    UpdateNews.LocalCategoryDescription = UpdateNews.LocalCategoryDescription;
                    db.Category.Attach(UpdateNews);
                    db.Entry(UpdateNews).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }


                if (UpdateNews.GalleryId != 0)
                {
                    using (AGalleryContext db = new AGalleryContext())
                    {
                        var galleryThumbData = (from a in db.tGalleryThumb where a.CategoryId == UpdateNews.CategoryID select a).FirstOrDefault();

                        if (galleryThumbData != null)
                        {
                            galleryThumbData.GalleryId = UpdateNews.GalleryId;
                            db.tGalleryThumb.Attach(galleryThumbData);
                            db.Entry(galleryThumbData).State = EntityState.Modified;
                            db.SaveChanges();


                        }
                        else
                        {
                            tGalleryThumb newthumb = new tGalleryThumb();
                            newthumb.GalleryId = UpdateNews.GalleryId;
                            newthumb.CategoryId = UpdateNews.CategoryID;
                            db.tGalleryThumb.Add(newthumb);
                            db.SaveChanges();
                        }

                    }
                }
                return UpdateNews;
            }
            catch
            {
                throw;
            }
        }

        static void DeleteGalleryCategory(object param)
        {
            try
            {
                Category DataTodelete = param as Category;
                int id = DataTodelete.CategoryID;
                using (AGalleryContext db = new AGalleryContext())
                {
                    var NewsToDelete = db.Category.SingleOrDefault(a => a.CategoryID == id);
                    db.Category.Remove(NewsToDelete);
                    db.SaveChanges();
                    db.Close();
                }
            }
            catch
            {
                throw;
            }
        }

        static object GetGalleryByIdCategory(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {
                    Category NewsToEdit = param as Category;

                    var data = db.Category.SingleOrDefault(o => o.CategoryID == NewsToEdit.CategoryID);

                    var gallerythumbimgdata = (from gallerythumb in db.tGalleryThumb where gallerythumb.CategoryId == NewsToEdit.CategoryID select gallerythumb).FirstOrDefault();
                    if (gallerythumbimgdata != null)
                    {
                        data.GalleryId = gallerythumbimgdata.GalleryId;
                    }
                    else
                    {
                        data.GalleryId = 0;
                    }
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }


        private static object IsGalleryCategoryChildExist(object p)
        {
            try
            {
                using (AGalleryContext ctx = new AGalleryContext())
                {
                    Category data = (Category)p;

                    var isExist = (from a in ctx.tGallery where a.CategoryID == data.CategoryID select a).Count() > 0;

                    return isExist;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }




        static object GetAllGalleryCategory1(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {

                    int Id = Convert.ToInt32(param);
                    var data1 = (from e in db.tAlbumcategory
                                 join c in db.Category on e.AlbumCategoryID equals c.AlbumCategoryId
                                 join galthmcat in db.tGalleryThumb on c.CategoryID equals galthmcat.CategoryId
                                     into CatGal
                                 from catGalData in CatGal.DefaultIfEmpty()
                                 where 
                                 //c.CategoryType == 3 && 
                                 c.Status == 1
                                 && e.MemberCode == Id
                                 select new { e.AlbumCategoryID, c, GalleryId = (catGalData != null ? catGalData.GalleryId : 0) }).ToList();

                    //var data = (from categroy in db.Category
                    //            join galthmcat in db.tGalleryThumb on categroy.CategoryID equals galthmcat.CategoryId
                    //                into CatGal
                    //            from catGalData in CatGal.DefaultIfEmpty()
                    //            where categroy.CategoryType == 3 && categroy.Status == 1
                    //            select new { categroy, GalleryId = (catGalData != null ? catGalData.GalleryId : 0) }).ToList();

                    var categoryData = (from category in data1
                                        join gallery in db.tGallery on category.GalleryId equals gallery.GalleryId
                                            into catgal
                                        from galCat in catgal.DefaultIfEmpty()
                                        select
                                            new { category, Galleryimagepath = (galCat != null ? galCat.FilePath + "/Thumb/" + galCat.FileName : String.Empty) }).ToList();

                    List<Category> outputResult = new List<Category>();
                    foreach (var item in categoryData)
                    {
                        Category catData = new Category();

                        catData.CategoryID = item.category.c.CategoryID;
                        catData.GalleryId = item.category.GalleryId;
                        catData.CategoryName = item.category.c.CategoryName;
                        catData.LocalCategoryName = item.category.c.LocalCategoryName;
                        catData.Status = item.category.c.Status;
                        catData.GalleryThmImgUrl = item.Galleryimagepath;
                        catData.CreatedDate = item.category.c.CreatedDate;

                        outputResult.Add(catData);

                    }


                    return outputResult;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        private static object GetDropDownData1(object param)
        {
            int Id = Convert.ToInt32(param);
            AGalleryContext db = new AGalleryContext();
            var Res = (from e in db.tAlbumcategory where e.MemberCode == Id select e).ToList();
            return Res;
        }



        static object GetAllCategoryType1(object param)
        {
            try
            {
                int Id = Convert.ToInt32(param);
                using (AGalleryContext db = new AGalleryContext())
                {
                    //Category cc= param as Category;
                    //Organization Organization = param as Organization;
                    var data = (from value in db.Category
                                join c in db.tAlbumcategory on value.AlbumCategoryId equals c.AlbumCategoryID
                                where 
                                //value.CategoryType == 3 && 
                                value.Status == 1 && c.MemberCode == Id
                                select value).ToList();


                    //var data = (from value in db.Category where value.CategoryType == 3 && value.Status == 1 && value.AlbumCategoryId== 26 select value).ToList();
                    


                    
                    return data;
                }
            }
            catch
            {
                throw;
            }
        }



        static object GetAllGallery1(object param)
        {
            try
            {
                using (AGalleryContext db = new AGalleryContext())
                {
                    int Id = Convert.ToInt32(param);

                    var data1 = (from a in db.tAlbumcategory
                                 join c in db.Category
                                     on a.AlbumCategoryID equals c.AlbumCategoryId
                                 join b in db.tGallery on c.CategoryID equals b.CategoryID
                                 where a.MemberCode == Id
                                 orderby b.GalleryId descending
                                 select new { Gallery = b, CategoryTypeName = c.CategoryName }).ToList();
                    //  var data1 = (from a in db.tAlbumcategory join c in db.Category on a.AlbumCategoryID equals c.AlbumCategoryId where a.MemberCode == Id select a).ToList();
                    // var data = (from a in db.tGallery join category in db.Category on a.CategoryID equals category.CategoryID select new { Gallery = a, CategoryTypeName = category.CategoryName }).ToList();//db.mNews.ToList();

                    var siteSettingData = (from a in db.mSiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();

                    List<tGallery> result = new List<tGallery>();

                    foreach (var a in data1)
                    {
                        tGallery partdata = new tGallery();
                        partdata = a.Gallery;
                        partdata.CategoryName = a.CategoryTypeName;
                        partdata.ImageAcessurl = siteSettingData.SettingValue + a.Gallery.FilePath + "/Thumb/" + a.Gallery.FileName;
                        result.Add(partdata);
                    }
                    return result;
                }
            }
            catch
            {
                throw;
            }
        }




    }
}
