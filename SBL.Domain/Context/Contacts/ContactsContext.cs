﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using SBL.DAL;
using SBL.Service.Common;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.ComplexModel;

namespace SBL.Domain.Context.Contacts
{
    public class ContactsContext : DBBase<ContactsContext>
    {
        public ContactsContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mUsers> mUsers { get; set; }
        public DbSet<mEmployee> mEmployee { get; set; }
        public DbSet<mMember> mMembers { get; set; }
        public DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public DbSet<mConstituency> mConstituency { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mMinistry> mMinistry { get; set; }
        public DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }
        public DbSet<SiteSettings> mSiteSettings { get; set; }
        public DbSet<mDesignation> mDesignation { get; set; }
        public DbSet<AdhaarDetails> mAdharDetails { get; set; }

        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {
                case "GetAllMinisterEMailIds":
                    {
                        return GetAllMinisterEMailIds();
                    }
                case "GetAllMinisterMobileNos":
                    {
                        return GetAllMinisterMobileNos();
                    }
                case "GetAllMembersEMailIds":
                    {
                        return GetAllMembersEMailIds();
                    }
                case "GetAllMembersMobileNos":
                    {
                        return GetAllMembersMobileNos();
                    }
                case "GetAllEmployeeEmailIds":
                    {
                        return GetAllEmployeeEmailIds();
                    }
                case "GetAllEmployeePhoneNos":
                    {
                        return GetAllEmployeePhoneNos();
                    }
            }
            return null;
        }

        public static List<MinisterContactViewModel> GetAllMinisterEMailIds()
        {
            ContactsContext db = new ContactsContext();

            var data = (from members in db.mMembers
                        join ministers in db.mMinsitryMinister on members.MemberCode equals ministers.MemberCode
                        join ministry in db.mMinistry on ministers.MinistryID equals ministry.MinistryID
                        where members.Email != null
                        select new
                        {
                            ministers.MinisterName,
                            ministry.MinistryName,
                            members.Email,
                            members.Mobile
                        }).ToList();

            List<MinisterContactViewModel> list = new List<MinisterContactViewModel>();
            foreach (var item in data)
            {
                list.Add(new MinisterContactViewModel()
                {
                    MinisterName = item.MinisterName,
                    MinistryName = item.MinistryName,
                    EMail = item.Email
                });
            }
            return list;
        }

        public static List<MinisterContactViewModel> GetAllMinisterMobileNos()
        {
            ContactsContext db = new ContactsContext();

            var data = (from members in db.mMembers
                        join ministers in db.mMinsitryMinister on members.MemberCode equals ministers.MemberCode
                        join ministry in db.mMinistry on ministers.MinistryID equals ministry.MinistryID
                        where members.Mobile != null
                        select new
                        {
                            ministers.MinisterName,
                            ministry.MinistryName,
                            members.Email,
                            members.Mobile
                        }).ToList();

            List<MinisterContactViewModel> list = new List<MinisterContactViewModel>();
            foreach (var item in data)
            {
                list.Add(new MinisterContactViewModel()
                {
                    MinisterName = item.MinisterName,
                    MinistryName = item.MinistryName,
                    Mobile = item.Mobile
                });
            }
            return list;
        }

        //public static List<MemberContactViewModel> GetAllMembersEMailIds()
        //{
        //    ContactsContext db = new ContactsContext();

        //    var query = (from siteSetting in db.mSiteSettings
        //                 where siteSetting.SettingName == "Assembly"
        //                 select siteSetting.SettingValue).FirstOrDefault();

        //    int lastAssemblyId = (!string.IsNullOrEmpty(query.ToString())) ? Convert.ToInt16(query.ToString()) : 0;

        //    var data = (from members in db.mMembers
        //                join memberAssemblies in db.mMemberAssembly on members.MemberCode equals memberAssemblies.MemberID
        //                join constituency in db.mConstituency on memberAssemblies.ConstituencyCode equals constituency.ConstituencyCode
        //                where memberAssemblies.AssemblyID == lastAssemblyId
        //                && constituency.AssemblyID == lastAssemblyId
        //                select new
        //                {
        //                    members.Name,
        //                    constituency.ConstituencyName,
        //                    members.Email,
        //                    members.Mobile
        //                }).ToList().Where(x => x.Email != null);

        //    List<MemberContactViewModel> list = new List<MemberContactViewModel>();
        //    foreach (var item in data)
        //    {
        //        list.Add(new MemberContactViewModel()
        //        {
        //            MemberName = item.Name,
        //            ConstituencyName = item.ConstituencyName,
        //            EMail = item.Email
        //        });
        //    }
        //    return list;
        //}

        public static object GetAllMembersEMailIds()
        {
            ContactsContext db = new ContactsContext();

            var query = (from siteSetting in db.mSiteSettings
                         where siteSetting.SettingName == "Assembly"
                         select siteSetting.SettingValue).FirstOrDefault();

            int lastAssemblyId = (!string.IsNullOrEmpty(query.ToString())) ? Convert.ToInt16(query.ToString()) : 0;

            var data = (from members in db.mMembers
                        join memberAssemblies in db.mMemberAssembly on members.MemberCode equals memberAssemblies.MemberID
                        join constituency in db.mConstituency on memberAssemblies.ConstituencyCode equals constituency.ConstituencyCode
                        where memberAssemblies.AssemblyID == lastAssemblyId
                        && constituency.AssemblyID == lastAssemblyId
                        && members.Email != null
                        select new MemberContactViewModel
                        {
                            MemberName = members.Name,
                            ConstituencyName = constituency.ConstituencyName,
                            EMail = members.Email,
                            Mobile = members.Mobile
                        }).ToList();
            return data;
        }

        public static List<MemberContactViewModel> GetAllMembersMobileNos()
        {
            ContactsContext db = new ContactsContext();

            var query = (from siteSetting in db.mSiteSettings
                         where siteSetting.SettingName == "Assembly"
                         select siteSetting.SettingValue).FirstOrDefault();

            int lastAssemblyId = (!string.IsNullOrEmpty(query.ToString())) ? Convert.ToInt16(query.ToString()) : 0;

            var data = (from members in db.mMembers
                        join memberAssemblies in db.mMemberAssembly on members.MemberCode equals memberAssemblies.MemberID
                        join constituency in db.mConstituency on memberAssemblies.ConstituencyCode equals constituency.ConstituencyCode
                        where memberAssemblies.AssemblyID == lastAssemblyId
                        && constituency.AssemblyID == lastAssemblyId
                        select new
                        {
                            members.Name,
                            constituency.ConstituencyName,
                            members.Email,
                            members.Mobile
                        }).ToList().Where(x => x.Email != null);

            List<MemberContactViewModel> list = new List<MemberContactViewModel>();
            foreach (var item in data)
            {
                list.Add(new MemberContactViewModel()
                {
                    MemberName = item.Name,
                    ConstituencyName = item.ConstituencyName,
                    Mobile = item.Mobile
                });
            }
            return list;
        }

        public static List<mUsers> GetAllEmployeeEmailIds()
        {
            ContactsContext db = new ContactsContext();

            var data = (from users in db.mUsers
                        join employees in db.mAdharDetails on users.AadarId equals employees.AdhaarID
                        where users.EmailId != null
                        select new
                        {
                            employees.Name,
                            users.Designation,
                            users.EmailId,
                            users.MobileNo
                        }).ToList();

            List<mUsers> list = new List<mUsers>();
            foreach (var item in data)
            {
                list.Add(new mUsers()
                {
                    UserName = item.Name,
                    Designation = item.Designation,
                    EmailId = item.EmailId,
                    Mobile = item.MobileNo
                });
            }
            return list;
        }

        public static List<mUsers> GetAllEmployeePhoneNos()
        
        {
            ContactsContext db = new ContactsContext();

            var data = (from users in db.mUsers 
                        join employees in db.mAdharDetails on users.AadarId equals employees.AdhaarID
                        where users.MobileNo != null
                        select new
                        {
                            employees.Name,
                            users.Designation,
                            users.EmailId,
                            users.MobileNo
                        }).ToList();

            List<mUsers> list = new List<mUsers>();
            foreach (var item in data)
            {
                list.Add(new mUsers()
                {
                    UserName = item.Name,
                    Designation = item.Designation,
                    EmailId = item.EmailId,
                    Mobile = item.MobileNo
                });
            }
            return list;
        }
    }
}
