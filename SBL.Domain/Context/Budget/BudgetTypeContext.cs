﻿using SBL.DAL;
using SBL.DomainModel.Models.Budget;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Budget
{
    
    public class BudgetTypeContext : DBBase<BudgetTypeContext>
    {

        public BudgetTypeContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        
        public DbSet<mBudgetType> mBudgetType { get; set; }

       
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateBudgetType": { CreateBudgetType(param.Parameter); break; }

                case "UpdateBudgetType": { return UpdateBudgetType(param.Parameter); }

                case "GetAllBudgetType": { return GetAllBudgetType(); }
                case "GetBudgetTypeById": { return GetBudgetTypeById(param.Parameter); }
                
            }
            return null;
        }


        static void CreateBudgetType(object param)
        {
            try
            {
                using (BudgetTypeContext db = new BudgetTypeContext())
                {
                    mBudgetType model = param as mBudgetType;

                    db.mBudgetType.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateBudgetType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (BudgetTypeContext db = new BudgetTypeContext())
            {
                mBudgetType model = param as mBudgetType;


                db.mBudgetType.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllBudgetType();
        }



        static List<mBudgetType> GetAllBudgetType()
        {
            BudgetTypeContext db = new BudgetTypeContext();


            var query = (from a in db.mBudgetType
                         orderby a.BudgetTypeID descending
                         
                         select a).ToList();

            return query;
        }

        static mBudgetType GetBudgetTypeById(object param)
        {
            mBudgetType parameter = param as mBudgetType;
            BudgetTypeContext db = new BudgetTypeContext();
            var query = db.mBudgetType.SingleOrDefault(a => a.BudgetTypeID == parameter.BudgetTypeID);
            return query;
        }



    }
}
