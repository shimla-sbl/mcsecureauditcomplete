﻿using SBL.DAL;
using SBL.DomainModel.Models.Budget;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.Budget
{
    
    public class BudgetBillTypeContext : DBBase<BudgetBillTypeContext>
    {

        public BudgetBillTypeContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        
        
        public DbSet<mBudgetBillTypes> mBudgetBillTypes { get; set; }
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "CreateBudgetBillType": { CreateBudgetBillType(param.Parameter); break; }

                case "UpdateBudgetBillType": { return UpdateBudgetBillType(param.Parameter); }

                case "GetAllBudgetBillType": { return GetAllBudgetBillType(); }
                case "GetBudgetBillTypeById": { return GetBudgetBillTypeById(param.Parameter); }
                
            }
            return null;
        }


        static void CreateBudgetBillType(object param)
        {
            try
            {
                using (BudgetBillTypeContext db = new BudgetBillTypeContext())
                {
                    mBudgetBillTypes model = param as mBudgetBillTypes;
                    
                    db.mBudgetBillTypes.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateBudgetBillType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (BudgetBillTypeContext db = new BudgetBillTypeContext())
            {
                mBudgetBillTypes model = param as mBudgetBillTypes;
                

                db.mBudgetBillTypes.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllBudgetBillType();
        }



        static List<mBudgetBillTypes> GetAllBudgetBillType()
        {
            BudgetBillTypeContext db = new BudgetBillTypeContext();

            
            var query = (from a in db.mBudgetBillTypes
                         orderby a.BillTypeID descending
                         
                         select a).ToList();

            return query;
        }

        static mBudgetBillTypes GetBudgetBillTypeById(object param)
        {
            mBudgetBillTypes parameter = param as mBudgetBillTypes;
            BudgetBillTypeContext db = new BudgetBillTypeContext();
            var query = db.mBudgetBillTypes.SingleOrDefault(a => a.BillTypeID == parameter.BillTypeID);
            return query;
        }



    }

}
