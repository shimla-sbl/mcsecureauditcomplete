﻿using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Budget;
using SBL.DomainModel.Models.Employee;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.salaryhead;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.Models.StaffManagement;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

namespace SBL.Domain.Context.Budget
{
    public class BudgetContext : DBBase<BudgetContext>
    {
        public BudgetContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;


        }
        public DbSet<mBudget> mBudget { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<AllocateBudget> AllocateBudgets { get; set; }
        public DbSet<AdditionFunds> AdditionFunds { get; set; }
        public DbSet<mReimbursementBill> mReimbursementBill { get; set; }
        public DbSet<mComments> mComments { get; set; }
        public DbSet<mDebitReceiptExp> mDebitReceiptExp { get; set; }
        public virtual DbSet<mMember> mMembers { get; set; }
        public DbSet<mAuthorityLetter> mAuthorityLetter { get; set; }
        public DbSet<mMemberAccountsDetails> mMemberAccountsDetails { get; set; }
        public DbSet<mEstablishBill> mEstablishBill { get; set; }
        public DbSet<mStaff> mStaff { get; set; }
        public DbSet<mSubVoucher> mSubVoucher { get; set; }
        public DbSet<mMemberNominee> mMemberNominee { get; set; }

        public DbSet<mOtherFirm> mOtherFirm { get; set; }
        public DbSet<mRtgsSoft> mRtgsSoft { get; set; }
        public DbSet<mMedicine> mMedicine { get; set; }
        public DbSet<mApprovedHospital> mApprovedHospital { get; set; }
        public DbSet<mMedicalTestCharge> mMedicalTestCharge { get; set; }

        public DbSet<mBudgetType> mBudgetType { get; set; }
        public DbSet<mBudgetBillTypes> mBudgetBillTypes { get; set; }
        public DbSet<BudgetMapWithDesig> BudgetMapWithDesig { get; set; }
        public virtual DbSet<mMemberAssembly> mMemberAssembly { get; set; }
        public virtual DbSet<SiteSettings> mSiteSettings { get; set; }
        public virtual DbSet<mMemberDesignation> mMemberDesignations { get; set; }

        public virtual DbSet<mMinsitryMinister> mMinsitryMinister { get; set; }

        public DbSet<membersHead> membersHead { get; set; }
        public DbSet<salaryheads> salaryheads { get; set; }
        public DbSet<mMemberIncomeTax> mMemberIncomeTax { get; set; }
        public DbSet<mSpeakerIncomeTax> mSpeakerIncomeTax { get; set; }
        public DbSet<mSalaryTypeDetailForTax> mSalaryTypeDetailForTax { get; set; }
        public DbSet<SalaryVoucherNumbers> mSalaryVoucherNumbers { get; set; }
        public DbSet<mStaffNominee> mStaffNominee { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            switch (param.Method)
            {
                case "CreateBudget":
                    {
                        return CreateBudget(param.Parameter);
                    }
                case "UpdateBudget":
                    {
                        return UpdateBudget(param.Parameter);
                    }
                case "DeleteBudget":
                    {
                        return DeleteBudget(param.Parameter);
                    }
                case "GetAllBudgetDetails":
                    {
                        return GetAllBudgetDetails(param.Parameter);
                    }
                case "GetBudgetWithAllocateFund":
                    {
                        return GetBudgetWithAllocateFund(param.Parameter);
                    }
                case "GetBudgetWithTotalFund":
                    {
                        return GetBudgetWithTotalFund(param.Parameter);
                    }
                case "GetAllBudgetByBudgetType":
                    {
                        return GetAllBudgetByBudgetType(param.Parameter);
                    }

                case "GetBudgetById":
                    {
                        return GetBudgetById(param.Parameter);
                    }
                case "GetTotalBillExpenseForSummaryReport":
                    {
                        return GetTotalBillExpenseForSummaryReport(param.Parameter);
                    }
                case "GetGroupedBudget":
                    {
                        return GetGroupedBudget(param.Parameter);
                    }
                case "GetBudgetByBudgetName":
                    {
                        return GetBudgetByBudgetName(param.Parameter);
                    }
                case "GetBudgetByMajorCode":
                    {
                        return GetBudgetByMajorCode(param.Parameter);
                    }

                case "GetAllocateBudget":
                    {
                        return GetAllocateBudget(param.Parameter);
                    }
                case "AddAllocateBudget":
                    {
                        return AddAllocateBudget(param.Parameter);
                    }
                case "UpdateAllocateBudget":
                    {
                        return UpdateAllocateBudget(param.Parameter);
                    }
                case "GetAllocateBudgetById":
                    {
                        return GetAllocateBudgetById(param.Parameter);
                    }

                case "AddAdditionFund":
                    {
                        return AddAdditionFund(param.Parameter);
                    }
                case "UpdateAdditionFund":
                    {
                        return UpdateAdditionFund(param.Parameter);
                    }
                case "GetAllEstablishBillForReport":
                    {
                        return GetAllEstablishBillForReport(param.Parameter);
                    }
                case "DeleteAdditionFund":
                    {
                        return DeleteAdditionFund(param.Parameter);
                    }
                case "GetAllAdditionFund":
                    {
                        return GetAllAdditionFund(param.Parameter);
                    }
                case "GetTotalAdditionById":
                    {
                        return GetTotalAdditionById(param.Parameter);
                    }
                case "GetAdditionFundById":
                    {
                        return GetAdditionFundById(param.Parameter);
                    }
                case "GetAllSuplementaryFund":
                    {
                        return GetAllSuplementaryFund(param.Parameter);
                    }
                case "CreateReimbursementBill":
                    {
                        return CreateReimbursementBill(param.Parameter);
                    }
                case "UpdateReimbursementBill":
                    {
                        return UpdateReimbursementBill(param.Parameter);
                    }
                case "DeleteReimbursementBill":
                    {
                        return DeleteReimbursementBill(param.Parameter);
                    }
                case "GetAllReimbursementBill":
                    {
                        return GetAllReimbursementBill(param.Parameter);
                    }
                case "GetAllReimbursementBillForReport":
                    {
                        return GetAllReimbursementBillForReport(param.Parameter);
                    }
                case "GetAllReimbursementBillForSalary":
                    {
                        return GetAllReimbursementBillForSalary(param.Parameter);
                    }
                case "GetAllReimbursementBillByMultipleEstablishId":
                    {
                        return GetAllReimbursementBillByMultipleEstablishId(param.Parameter);
                    }
                case "SearchReimbursementBillForMembers":
                    {
                        return SearchReimbursementBillForMembers(param.Parameter);
                    }
                case "GetReimbursementBillById":
                    {
                        return GetReimbursementBillById(param.Parameter);
                    }
                case "GetAllocateBudgetByBudgetId":
                    {
                        return GetAllocateBudgetByBudgetId(param.Parameter);
                    }

                case "CreateComments":
                    {
                        return CreateComments(param.Parameter);
                    }
                case "UpdateComments":
                    {
                        return UpdateComments(param.Parameter);
                    }
                case "DeleteComments":
                    {
                        return DeleteComments(param.Parameter);
                    }

                case "GetAllBillByEstablishBillId":
                    {
                        return GetAllBillByEstablishBillId(param.Parameter);
                    }
                case "GetAllComments":
                    {
                        return GetAllComments(param.Parameter);
                    }
                case "CreateDebitReceiptExp":
                    {
                        return CreateDebitReceiptExp(param.Parameter);
                    }
                case "UpdateDebitReceiptExp":
                    {
                        return UpdateDebitReceiptExp(param.Parameter);
                    }
                case "DeleteDebitReceiptExp":
                    {
                        return DeleteDebitReceiptExp(param.Parameter);
                    }
                case "GetAllDebitReceiptExp":
                    {
                        return GetAllDebitReceiptExp(param.Parameter);
                    }
                case "GetDebitReceiptExpById":
                    {
                        return GetDebitReceiptExpById(param.Parameter);
                    }
                case "GetAllReimbursementBillForEncashment":
                    {
                        return GetAllReimbursementBillForEncashment(param.Parameter);
                    }
                case "SearchReimbursementBill":
                    {
                        return SearchReimbursementBill(param.Parameter);
                    }
                case "CreateAuthorityLetter":
                    {
                        return CreateAuthorityLetter(param.Parameter);
                    }
                case "UpdateAuthorityLetter":
                    {
                        return UpdateAuthorityLetter(param.Parameter);
                    }
                case "DeleteAuthorityLetter":
                    {
                        return DeleteAuthorityLetter(param.Parameter);
                    }
                case "GetAllAuthorityLetter":
                    {
                        return GetAllAuthorityLetter(param.Parameter);
                    }
                case "GetAuthorityLetterById":
                    {
                        return GetAuthorityLetterById(param.Parameter);
                    }
                case "GetAllReimbursementBillForALMemeberAndOther":
                    {
                        return GetAllReimbursementBillForALMemeberAndOther(param.Parameter);
                    }
                case "GetAllBillForOtherExStaffExMember":
                    {
                        return GetAllBillForOtherExStaffExMember(param.Parameter);
                    }

                case "GetAllAccountDetail":
                    {
                        return GetAllAccountDetail(param.Parameter);
                    }
                case "GetAllReimbursementBillForSpeakerAndDeputySpeaker":
                    {
                        return GetAllReimbursementBillForSpeakerAndDeputySpeaker(param.Parameter);
                    }
                case "GetAllReimburBillForCustom":
                    {
                        return GetAllReimburBillForCustom(param.Parameter);
                    }

                case "GetAllEstablishBillForStaff":
                    {
                        return GetAllEstablishBillForStaff(param.Parameter);
                    }
                case "GetTotalBillExpense":
                    {
                        return GetTotalBillExpense(param.Parameter);
                    }
                case "GetBudgetByFY":
                    {
                        return GetBudgetByFY(param.Parameter);
                    }
                case "GetAllReimbursementBillByParam":
                    {
                        return GetAllReimbursementBillByParam(param.Parameter);
                    }
                case "GetAllEstablishBillForExpense":
                    {
                        return GetAllEstablishBillForExpense(param.Parameter);
                    }
                case "GetTotalBillExpenseForExpenseReport":
                    {
                        return GetTotalBillExpenseForExpenseReport(param.Parameter);
                    }
                case "GetTotalBillExpenseForExpenseReportForSalary":
                    {
                        return GetTotalBillExpenseForExpenseReportForSalary(param.Parameter);
                    }

                case "GetAllReimbursementBillForMember":
                    {
                        return GetAllReimbursementBillForMember(param.Parameter);
                    }

                case "GetAllReimbursementBillByEstablishId":
                    {
                        return GetAllReimbursementBillByEstablishId(param.Parameter);
                    }

                // this is for Establish Bill
                case "CreateEstablishBill":
                    {
                        return CreateEstablishBill(param.Parameter);
                    }
                case "UpdateEstablishBill":
                    {
                        return UpdateEstablishBill(param.Parameter);
                    }
                case "CancelEstablishBill":
                    {
                        return CancelEstablishBill(param.Parameter);
                    }
                case "DeleteEstablishBill":
                    {
                        return DeleteEstablishBill(param.Parameter);
                    }
                case "GetAllEstablishBill":
                    {
                        return GetAllEstablishBill(param.Parameter);
                    }
                case "GetEstablishBillById":
                    {
                        return GetEstablishBillById(param.Parameter);
                    }
                case "GetEstablishBillByBillNumber":
                    {
                        return GetEstablishBillByBillNumber(param.Parameter);
                    }

                case "GetLastEstablishBill":
                    {
                        return GetLastEstablishBill(param.Parameter);
                    }
                case "UpdateBillId":
                    {
                        return UpdateBillId(param.Parameter);
                    }


                case "CreateOtherFirm":
                    {
                        return CreateOtherFirm(param.Parameter);
                    }
                case "UpdateOtherFirm":
                    {
                        return UpdateOtherFirm(param.Parameter);
                    }
                case "DeleteOtherFirm":
                    {
                        return DeleteOtherFirm(param.Parameter);
                    }
                case "GetAllOtherFirm":
                    {
                        return GetAllOtherFirm(param.Parameter);
                    }
                case "GetOtherFirmById":
                    {
                        return GetOtherFirmById(param.Parameter);
                    }


                case "CreateRtgsSoft":
                    {
                        return CreateRtgsSoft(param.Parameter);
                    }
                case "UpdateRtgsSoft":
                    {
                        return UpdateRtgsSoft(param.Parameter);
                    }
                case "DeleteRtgsSoft":
                    {
                        return DeleteRtgsSoft(param.Parameter);
                    }
                case "GetAllRtgsSoft":
                    {
                        return GetAllRtgsSoft(param.Parameter);
                    }
                case "GetRtgsSoftById":
                    {
                        return GetRtgsSoftById(param.Parameter);
                    }

                case "GetAllRtgsSoftByBillId":
                    {
                        return GetAllRtgsSoftByBillId(param.Parameter);
                    }

                case "CreateMedicine":
                    {
                        return CreateMedicine(param.Parameter);
                    }
                case "UpdateMedicine":
                    {
                        return UpdateMedicine(param.Parameter);
                    }
                case "DeleteMedicine":
                    {
                        return DeleteMedicine(param.Parameter);
                    }
                case "GetAllMedicine":
                    {
                        return GetAllMedicine(param.Parameter);
                    }
                case "GetMedicineById":
                    {
                        return GetMedicineById(param.Parameter);
                    }
                case "GetMedicineExist":
                    {
                        return GetMedicineExist(param.Parameter);
                    }
                case "SearchMedicine":
                    {
                        return SearchMedicine(param.Parameter);
                    }


                case "CreateApprovedHospital":
                    {
                        return CreateApprovedHospital(param.Parameter);
                    }
                case "UpdateApprovedHospital":
                    {
                        return UpdateApprovedHospital(param.Parameter);
                    }
                case "DeleteApprovedHospital":
                    {
                        return DeleteApprovedHospital(param.Parameter);
                    }
                case "GetAllApprovedHospital":
                    {
                        return GetAllApprovedHospital(param.Parameter);
                    }
                case "GetApprovedHospitalById":
                    {
                        return GetApprovedHospitalById(param.Parameter);
                    }
                case "GetApprovedHospitalExist":
                    {
                        return GetApprovedHospitalExist(param.Parameter);
                    }
                case "SearchApprovedHospital":
                    {
                        return SearchApprovedHospital(param.Parameter);
                    }




                case "CreateMedicalTestCharge":
                    {
                        return CreateMedicalTestCharge(param.Parameter);
                    }
                case "UpdateMedicalTestCharge":
                    {
                        return UpdateMedicalTestCharge(param.Parameter);
                    }
                case "DeleteMedicalTestCharge":
                    {
                        return DeleteMedicalTestCharge(param.Parameter);
                    }
                case "GetAllMedicalTestCharge":
                    {
                        return GetAllMedicalTestCharge(param.Parameter);
                    }
                case "GetMedicalTestChargeById":
                    {
                        return GetMedicalTestChargeById(param.Parameter);
                    }
                case "GetMedicalTestChargeExist":
                    {
                        return GetMedicalTestChargeExist(param.Parameter);
                    }
                case "SearchMedicalTestCharge":
                    {
                        return SearchMedicalTestCharge(param.Parameter);
                    }
                case "CreateBudgetType":
                    {
                        return CreateBudgetType(param.Parameter);
                    }
                case "UpdateBudgetType":
                    {
                        return UpdateBudgetType(param.Parameter);
                    }
                case "DeleteBudgetType":
                    {
                        return DeleteBudgetType(param.Parameter);
                    }
                case "GetAllBudgetType":
                    {
                        return GetAllBudgetType(param.Parameter);
                    }
                case "GetBudgetTypeById":
                    {
                        return GetBudgetTypeById(param.Parameter);
                    }
                case "CreateBillType":
                    {
                        return CreateBillType(param.Parameter);
                    }
                case "UpdateBillType":
                    {
                        return UpdateBillType(param.Parameter);
                    }
                case "DeleteBillType":
                    {
                        return DeleteBillType(param.Parameter);
                    }
                case "GetAllBillType":
                    {
                        return GetAllBillType(param.Parameter);
                    }
                case "GetBillTypeById":
                    {
                        return GetBillTypeById(param.Parameter);
                    }
                case "GetBCodebyFYear":
                    {
                        return GetBCodebyFYear(param.Parameter);
                    }
                case "GetAllReimbursementBillByEstablishIdAndClaimantId":
                    {
                        return GetAllReimbursementBillByEstablishIdAndClaimantId(param.Parameter);
                    }
                case "GetAllMembers":
                    {
                        return GetAllMembers();
                    }
                case "GetAllowance":
                    {
                        return GetAllowance(param.Parameter);
                    }
                case "SaveSpeakerIncomeTax":
                    {
                        return SaveSpeakerIncomeTax(param.Parameter);
                    }
                case "SaveSalryTypeDetail":
                    {
                        return SaveSalryTypeDetail(param.Parameter);
                    }
                case "GetSpeakerIncomeTaxById":
                    {
                        return GetSpeakerIncomeTaxById(param.Parameter);
                    }
                case "GetSalaryDetailIncomeTaxById":
                    {
                        return GetSalaryDetailIncomeTaxById(param.Parameter);
                    }
                case "GetAllSpeakerIncomeTax":
                    {
                        return GetAllSpeakerIncomeTax(param.Parameter);
                    }

                case "GetMemberIncomeTaxById":
                    {
                        return GetMemberIncomeTaxById(param.Parameter);
                    }
                case "SaveMemberIncomeTax":
                    {
                        return SaveMemberIncomeTax(param.Parameter);
                    }
                case "GetAllMemberIncomeTax":
                    {
                        return GetAllMemberIncomeTax(param.Parameter);
                    }
                case "GetAllMembersOnly":
                    {
                        return GetAllMembersOnly(param.Parameter);
                    }
                case "GetMemberAllowance":
                    {
                        return GetMemberAllowance(param.Parameter);
                    }

                case "GetSalaryVoucherNumbersFinYear":
                    {
                        return GetSalaryVoucherNumbersFinYear(param.Parameter);
                    }

                case "SaveSalaryVoucherNumbers":
                    {
                        return SaveSalaryVoucherNumbers(param.Parameter);
                    }
                case "GetSalaryVoucherNumbersFinYearAndMonth":
                    {
                        return GetSalaryVoucherNumbersFinYearAndMonth(param.Parameter);
                    }

                case "AddBudgetMappedwithDesignation":
                    {
                        return AddBudgetMappedwithDesignation(param.Parameter);
                    }
                case "UpdateBudgetMappedwithDesignation":
                    {
                        return UpdateBudgetMappedwithDesignation(param.Parameter);
                    }
                case "DeleteBudgetMappedwithDesignation":
                    {
                        return DeleteBudgetMappedwithDesignation(param.Parameter);
                    }
                case "GetBudgetMapWithDesigById":
                    {
                        return GetBudgetMapWithDesigById(param.Parameter);
                    }
                case "GetAllBudgetMapWithDesig":
                    {
                        return GetAllBudgetMapWithDesig(param.Parameter);
                    }
                case "GetEstablishBillIdByBillNo":
                    {
                        return GetEstablishBillIdByBillNo(param.Parameter);
                    }

            }

            return null;
        }

        public static object CreateBudget(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mBudget model = param as mBudget;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mBudget.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateBudget(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mBudget model = param as mBudget;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mBudget.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteBudget(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mBudget parameter = param as mBudget;
                    mBudget stateToRemove = db.mBudget.SingleOrDefault(a => a.BudgetID == parameter.BudgetID);
                    db.mBudget.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllBudgetDetails(object param)
        {

            try
            {
                mBudget obj = param as mBudget;
                List<mBudget> result = new List<mBudget>();
                BudgetContext db = new BudgetContext();

                var data = (from BU in db.mBudget
                            join BT in db.mBudgetType on BU.BudgetType equals BT.BudgetTypeID
                            orderby BU.OrderBy
                            where BU.FinancialYear == obj.FinancialYear
                            select new
                            {
                                BU,
                                BudgetTypeName = BT.TypeName,


                                AllocateCount = (from AB in db.AllocateBudgets
                                                 where AB.BudgetId == BU.BudgetID
                                                 select AB).Count()

                            }).ToList();


                foreach (var a in data)
                {
                    mBudget partdata = new mBudget();
                    partdata = a.BU;
                    partdata.BudgetTypeName = a.BudgetTypeName;
                    partdata.AllocateCount = a.AllocateCount;
                    result.Add(partdata);
                }

                return result.OrderBy(m => m.OrderBy).ToList(); ;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllBudgetByBudgetType(object param)
        {

            try
            {
                mBudget budget = param as mBudget;
                List<mBudget> result = new List<mBudget>();
                BudgetContext db = new BudgetContext();

                if (budget.BudgetType == 3)
                {
                    var data = (from BU in db.mBudget
                                join BT in db.mBudgetType on BU.BudgetType equals BT.BudgetTypeID
                                join BM in db.BudgetMapWithDesig on BU.BudgetID equals BM.BudgetId
                                orderby BU.BudgetID descending
                                where BU.BudgetType == budget.BudgetType && BU.FinancialYear == budget.FinancialYear && BM.DesignationId == budget.BudgetHeadOrder // this is for designation
                                select new
                                {
                                    BU,
                                    BudgetTypeName = BT.TypeName,
                                    AllocateCount = (from AB in db.AllocateBudgets
                                                     where AB.BudgetId == BU.BudgetID
                                                     select AB).Count()

                                }).ToList();

                    foreach (var a in data)
                    {
                        mBudget partdata = new mBudget();
                        partdata = a.BU;
                        partdata.AllocateCount = a.AllocateCount;
                        partdata.BudgetTypeName = a.BudgetTypeName;
                        result.Add(partdata);
                    }

                    return result;

                }
                else
                {
                    var data = (from BU in db.mBudget
                                join BT in db.mBudgetType on BU.BudgetType equals BT.BudgetTypeID
                                join BM in db.BudgetMapWithDesig on BU.BudgetID equals BM.BudgetId
                                orderby BU.BudgetID
                                where BU.BudgetType == budget.BudgetType && BU.FinancialYear == budget.FinancialYear && BM.DesignationId == budget.BudgetHeadOrder
                                select new
                                {
                                    BU,
                                    BudgetTypeName = BT.TypeName,
                                    AllocateCount = (from AB in db.AllocateBudgets
                                                     where AB.BudgetId == BU.BudgetID
                                                     select AB).Count()

                                }).ToList();
                    foreach (var a in data)
                    {
                        mBudget partdata = new mBudget();
                        partdata = a.BU;
                        partdata.AllocateCount = a.AllocateCount;
                        partdata.BudgetTypeName = a.BudgetTypeName;
                        result.Add(partdata);
                    }

                    return result;

                }




            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static object GetBudgetById(object param)
        {
            mBudget parameter = param as mBudget;

            BudgetContext db = new BudgetContext();

            var data = (from BU in db.mBudget
                        join BT in db.mBudgetType on BU.BudgetType equals BT.BudgetTypeID
                        orderby BU.BudgetCode
                        where BU.BudgetID == parameter.BudgetID
                        select new
                        {
                            BU,
                            BudgetTypeName = BT.TypeName,
                        }).FirstOrDefault();

            mBudget partdata = new mBudget();
            if (data != null)
            {
                partdata = data.BU;
                partdata.BudgetTypeName = data.BudgetTypeName;
            }
            return partdata;
        }

        public static object GetBudgetByBudgetName(object param)
        {
            mBudget parameter = param as mBudget;

            BudgetContext db = new BudgetContext();

            var query = db.mBudget.SingleOrDefault(a => a.BudgetName == parameter.BudgetName && a.BudgetID != parameter.BudgetID && a.FinancialYear == parameter.FinancialYear);

            return query;
        }

        public static object GetBudgetWithAllocateFund(object param)
        {
            string FinancialYear = param as string;

            BudgetContext db = new BudgetContext();
            List<mBudget> result = new List<mBudget>();


            var ResultData = (from B in db.mBudget
                              join AB in db.AllocateBudgets on B.BudgetID equals AB.BudgetId into Abs
                              from Abst in Abs.DefaultIfEmpty()
                              where B.FinancialYear == FinancialYear && B.IsShowExpenseReport == true
                              select new
                              {
                                  B,
                                  AllocateBudgetID = (int?)Abst.AllocateBudgetID,
                                  Sanctionedbudget = (decimal?)Abst.Sanctionedbudget,
                                  AllocateBudgetDate = (DateTime?)Abst.AllocateBudgetDate
                              }).ToList();



            if (ResultData != null && ResultData.Count() > 0)
            {
                foreach (var a in ResultData)
                {
                    mBudget partdata = new mBudget();
                    partdata = a.B;
                    partdata.AllocateFund = Convert.ToDecimal(a.Sanctionedbudget);
                    result.Add(partdata);
                }
            }

            return result.OrderBy(m => m.BudgetID).ToList();
        }


        public static object GetBudgetWithTotalFund(object param)
        {
            mBudget obj = param as mBudget;

            BudgetContext db = new BudgetContext();
            List<mBudget> result = new List<mBudget>();


            var ResultData = (from B in db.mBudget
                              join AB in db.AllocateBudgets on B.BudgetID equals AB.BudgetId into Abs
                              from Abst in Abs.DefaultIfEmpty()
                              join AF in db.AdditionFunds on B.BudgetID equals AF.BudgetId into AFS
                              from AFST in AFS.DefaultIfEmpty()
                              where B.FinancialYear == obj.FinancialYear && B.Gazetted == obj.Gazetted && B.MajorHead == obj.MajorHead &&
                              B.SubMajorHead == obj.SubMajorHead && B.MinorHead == obj.MinorHead && B.SubHead == obj.SubHead &&
                              B.VotedCharged == obj.VotedCharged && B.Plan == obj.Plan
                              select new
                              {
                                  B,
                                  AllocateBudgetID = (int?)Abst.AllocateBudgetID,
                                  Sanctionedbudget = (decimal?)Abst.Sanctionedbudget,
                                  AdditionalFund = (decimal?)AFST.AdditionalFund,
                                  AllocateBudgetDate = (DateTime?)Abst.AllocateBudgetDate
                              }).ToList();



            if (ResultData != null && ResultData.Count() > 0)
            {
                foreach (var a in ResultData)
                {
                    mBudget partdata = new mBudget();
                    partdata = a.B;
                    partdata.AllocateFund = Convert.ToDecimal(a.Sanctionedbudget) + Convert.ToDecimal(a.AdditionalFund);
                    result.Add(partdata);
                }
            }

            return result;
        }

        public static object GetGroupedBudget(object param)
        {
            string FinancialYear = param as string;

            BudgetContext db = new BudgetContext();


            var ResultData = (from B in db.mBudget
                              where B.FinancialYear == FinancialYear && B.IsShowExpenseReport == true
                              group B by new
                              {
                                  B.MajorHead,
                                  B.SubMajorHead,
                                  B.MinorHead,
                                  B.SubHead,
                                  B.Gazetted,
                                  B.VotedCharged,
                                  B.Plan,
                                  B.FinancialYear
                              } into GroupedBudget
                              select
                             new
                             {
                                 GroupedBudget.Key.MajorHead,
                                 GroupedBudget.Key.SubMajorHead,
                                 GroupedBudget.Key.MinorHead,
                                 GroupedBudget.Key.SubHead,
                                 GroupedBudget.Key.Gazetted,
                                 GroupedBudget.Key.VotedCharged,
                                 GroupedBudget.Key.Plan,
                                 GroupedBudget.Key.FinancialYear
                             }).ToList();

            List<mBudget> result = new List<mBudget>();
            if (ResultData != null && ResultData.Count() > 0)
            {
                foreach (var a in ResultData)
                {
                    mBudget partdata = new mBudget();
                    partdata.MajorHead = a.MajorHead;
                    partdata.SubMajorHead = a.SubMajorHead;
                    partdata.MinorHead = a.MinorHead;
                    partdata.SubHead = a.SubHead;
                    partdata.Gazetted = a.Gazetted;
                    partdata.VotedCharged = a.VotedCharged;
                    partdata.Plan = a.Plan;
                    partdata.FinancialYear = a.FinancialYear;
                    result.Add(partdata);
                }
            }
            return result;
        }

        public static object GetBudgetByMajorCode(object param)
        {
            mBudget obj = param as mBudget;

            BudgetContext db = new BudgetContext();



            var ResultData = (from B in db.mBudget
                              where B.FinancialYear == obj.FinancialYear && B.Gazetted == obj.Gazetted && B.MajorHead == obj.MajorHead &&
                              B.SubMajorHead == obj.SubMajorHead && B.MinorHead == obj.MinorHead && B.SubHead == obj.SubHead &&
                              B.VotedCharged == obj.VotedCharged && B.Plan == obj.Plan// && B.MajorHead == obj.MajorHead
                              select B).ToList();
            return ResultData;
        }

        #region AllocateBudget

        public static object AddAllocateBudget(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    AllocateBudget model = param as AllocateBudget;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.AllocateBudgets.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateAllocateBudget(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    AllocateBudget model = param as AllocateBudget;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.AllocateBudgets.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteAllocateBudget(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    AllocateBudget parameter = param as AllocateBudget;
                    AllocateBudget stateToRemove = db.AllocateBudgets.SingleOrDefault(a => a.AllocateBudgetID == parameter.AllocateBudgetID);
                    db.AllocateBudgets.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// Getting All Budget on the behlf of Finacial Year (FYFromDate)
        /// </summary>
        /// <param name="param">FromDate,ToDate</param>
        /// <returns></returns>
        public static object GetAllocateBudget(object param)
        {
            string Data = param as string;
            string[] Result = Data.Split(',');

            string FinancialYear = Result[0];

            BudgetContext db = new BudgetContext();
            List<AllocateBudget> result = new List<AllocateBudget>();


            var ResultData = (from B in db.mBudget
                              join AB in db.AllocateBudgets on B.BudgetID equals AB.BudgetId into Abs
                              from Abst in Abs.DefaultIfEmpty()
                              orderby B.OrderBy
                              where B.FinancialYear == FinancialYear && B.IsShowExpenseReport == true
                              select new
                              {
                                  B,
                                  AllocateBudgetID = (int?)Abst.AllocateBudgetID,
                                  Sanctionedbudget = (decimal?)Abst.Sanctionedbudget,
                                  AllocateBudgetDate = (DateTime?)Abst.AllocateBudgetDate
                              }).ToList();



            if (ResultData != null && ResultData.Count() > 0)
            {
                foreach (var a in ResultData)
                {
                    AllocateBudget partdata = new AllocateBudget();
                    partdata.AllocateBudgetID = int.Parse(a.AllocateBudgetID != null ? a.AllocateBudgetID.ToString() : "0");
                    partdata.BudgetId = a.B.BudgetID;
                    partdata.BudgetName = a.B.BudgetName; //string.Format("{0}-{1}-{2}-{3}-{4}({5}) {6}-{7}-{8}", a.B.MajorHead, a.B.SubMajorHead, a.B.MinorHead, a.B.SubHead, a.B.Gazetted, a.B.VotedCharged, a.B.Plan, a.B.ObjectCode.ToString(), a.B.ObjectCodeText);
                    partdata.Sanctionedbudget = decimal.Parse(a.Sanctionedbudget != null ? a.Sanctionedbudget.ToString() : "0");
                    partdata.AllocateBudgetDate = DateTime.Parse(a.AllocateBudgetDate != null ? a.AllocateBudgetDate.ToString() : ((DateTime)System.Data.SqlTypes.SqlDateTime.MinValue).ToShortDateString());
                    result.Add(partdata);
                }
            }
            //else
            //{

            //    var data = (from B in db.mBudget
            //                orderby B.BudgetID
            //                select new { B, AllocateBudgetID = 0, BudgetId = B.BudgetID, Sanctionedbudget = 0, AllocateBudgetDate = (DateTime)System.Data.SqlTypes.SqlDateTime.MinValue }).ToList();

            //    foreach (var a in data)
            //    {
            //        AllocateBudget partdata = new AllocateBudget();

            //        partdata.AllocateBudgetID = a.AllocateBudgetID;
            //        partdata.BudgetId = a.BudgetId;
            //        partdata.BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}-{8}", a.B.MajorHead, a.B.SubHead, a.B.MinorHead, a.B.SubHead, a.B.Gazetted, a.B.VotedCharged, a.B.Plan, a.B.ObjectCode.ToString(), a.B.ObjectCodeText);
            //        partdata.Sanctionedbudget = a.Sanctionedbudget;
            //        partdata.AllocateBudgetDate = a.AllocateBudgetDate;
            //        result.Add(partdata);
            //    }

            //}


            return result.OrderBy(m => m.AllocateBudgetID).ToList();
        }

        public static object GetAllocateBudgetById(object param)
        {
            AllocateBudget parameter = param as AllocateBudget;

            BudgetContext db = new BudgetContext();

            var query = db.AllocateBudgets.SingleOrDefault(a => a.AllocateBudgetID == parameter.AllocateBudgetID);

            return query;
        }
        /// <summary>
        /// Getting all Allocate Budget by BudgetId
        /// </summary>
        /// <param name="param">BudgetId</param>
        /// <returns></returns>
        public static object GetAllocateBudgetByBudgetId(object param)
        {
            AllocateBudget parameter = param as AllocateBudget;

            BudgetContext db = new BudgetContext();

            var query = db.AllocateBudgets.Where(a => a.BudgetId == parameter.BudgetId).FirstOrDefault();

            return query;
        }
        #endregion

        #region AdditionFund

        public static object AddAdditionFund(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    AdditionFunds model = param as AdditionFunds;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.AdditionFunds.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateAdditionFund(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    AdditionFunds model = param as AdditionFunds;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.AdditionFunds.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteAdditionFund(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    AdditionFunds parameter = param as AdditionFunds;
                    AdditionFunds stateToRemove = db.AdditionFunds.SingleOrDefault(a => a.AdditionFundsID == parameter.AdditionFundsID);
                    db.AdditionFunds.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllAdditionFund(object param)
        {
            string Data = param as string;
            int BudgetId = 0;
            int.TryParse(Data, out BudgetId);
            BudgetContext db = new BudgetContext();

            var ResultData = (from AF in db.AdditionFunds
                              orderby AF.AdditionFundsID
                              where AF.BudgetId == BudgetId
                              select AF).ToList();


            return ResultData;
        }
        /// <summary>
        /// Getting All Suplementary Fund y BudgetId
        /// </summary>
        /// <param name="param">BudgetId,FundType</param>
        /// <returns></returns>
        public static object GetAllSuplementaryFund(object param)
        {
            string Data = param as string;
            int BudgetId = 0;
            int.TryParse(Data, out BudgetId);
            BudgetContext db = new BudgetContext();

            var ResultData = (from AF in db.AdditionFunds
                              orderby AF.AdditionFundsID
                              where AF.BudgetId == BudgetId && AF.FundType == "Suplementary"
                              select AF).ToList();


            return ResultData;
        }

        public static object GetAdditionFundById(object param)
        {
            AdditionFunds parameter = param as AdditionFunds;

            BudgetContext db = new BudgetContext();

            var query = db.AdditionFunds.SingleOrDefault(a => a.AdditionFundsID == parameter.AdditionFundsID);

            return query;
        }
        /// <summary>
        /// Get Total Addition By BudgetId (Both Additional & Suplementary)
        /// </summary>
        /// <param name="param">BudgetId</param>
        /// <returns></returns>
        public static object GetTotalAdditionById(object param)
        {
            AdditionFunds parameter = param as AdditionFunds;
            BudgetContext db = new BudgetContext();

            var ResultData = (from AF in db.AdditionFunds
                              orderby AF.AdditionFundsID
                              where AF.BudgetId == parameter.BudgetId
                              select AF).ToList().Sum(m => m.AdditionalFund);


            return ResultData;
        }
        #endregion

        #region Reimbursement Bill

        public static object CreateReimbursementBill(object param)
        {
            try
            {
                mReimbursementBill model = param as mReimbursementBill;
                using (BudgetContext db = new BudgetContext())
                {

                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mReimbursementBill.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.ReimbursementBillID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateReimbursementBill(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mReimbursementBill model = param as mReimbursementBill;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mReimbursementBill.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteReimbursementBill(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mReimbursementBill parameter = param as mReimbursementBill;
                    mReimbursementBill stateToRemove = db.mReimbursementBill.SingleOrDefault(a => a.ReimbursementBillID == parameter.ReimbursementBillID);
                    db.mReimbursementBill.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllReimbursementBillForReport(object param)
        {

            try
            {

                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join S in db.mStaff on Bill.ClaimantId equals S.StaffID into S_join
                                from S in S_join.DefaultIfEmpty()
                                join F in db.mOtherFirm on Bill.ClaimantId equals F.FirmID into F_join
                                from F in F_join.DefaultIfEmpty()
                                //  where Bill.CreatedBy == obj.CreatedBy
                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    Email = M.Email,
                                    StaffName = S.StaffName,
                                    StaffMobileNo = S.MobileNo,
                                    OtherFirm = F.Name,
                                    FirmMobileNo = F.MobileNo,
                                    BillTypeName = BT.TypeName,

                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;
                            if (a.Bill.Designation == 6)
                            {
                                if (!string.IsNullOrEmpty(a.Bill.ClaimantName))
                                    partdata.ClaimantName = a.Bill.ClaimantName;
                                else
                                    partdata.ClaimantName = a.OtherFirm;

                                if (!string.IsNullOrEmpty(a.FirmMobileNo))
                                    partdata.Mobile = a.FirmMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else if (a.Bill.Designation == 4 || a.Bill.Designation == 5 || a.Bill.Designation == 7)
                            {
                                partdata.ClaimantName = a.StaffName;
                                if (!string.IsNullOrEmpty(a.StaffMobileNo))
                                    partdata.Mobile = a.StaffMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else
                            {
                                partdata.ClaimantName = a.Prefix + ". " + a.MemberName;
                                partdata.Mobile = a.Mobile;
                            }

                            partdata.Prefix = a.Prefix;

                            partdata.Email = a.Email;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public static object GetAllReimbursementBill(object param)
        {

            try
            {
                mReimbursementBill obj = param as mReimbursementBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join S in db.mStaff on Bill.ClaimantId equals S.StaffID into S_join
                                from S in S_join.DefaultIfEmpty()
                                join F in db.mOtherFirm on Bill.ClaimantId equals F.FirmID into F_join
                                from F in F_join.DefaultIfEmpty()
                                join Nom in db.mMemberNominee on Bill.NomineeId equals Nom.NomineeId into N_join
                                from Nom in N_join.DefaultIfEmpty()
                                join SNom in db.mStaffNominee on Bill.NomineeId equals SNom.NomineeID into SN_join
                                from SNom in SN_join.DefaultIfEmpty()

                                where Bill.CreatedBy == obj.CreatedBy
                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    Email = M.Email,
                                    StaffName = S.StaffName,
                                    StaffMobileNo = S.MobileNo,
                                    OtherFirm = F.Name,
                                    FirmMobileNo = F.MobileNo,
                                    BillTypeName = BT.TypeName,
                                    NomineName = Nom.NomineeName,
                                    StaffNominee = SNom.NomineeName
                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;
                            if (a.Bill.Designation == 6)
                            {
                                if (!string.IsNullOrEmpty(a.Bill.ClaimantName))
                                    partdata.ClaimantName = a.Bill.ClaimantName;
                                else
                                    partdata.ClaimantName = a.OtherFirm;

                                if (!string.IsNullOrEmpty(a.FirmMobileNo))
                                    partdata.Mobile = a.FirmMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else if (a.Bill.Designation == 4 || a.Bill.Designation == 5 || a.Bill.Designation == 7)
                            {
                                if (a.Bill.NomineeId != null && a.Bill.NomineeId > 0)
                                    partdata.ClaimantName = a.StaffNominee;
                                else
                                    partdata.ClaimantName = a.StaffName;

                                if (!string.IsNullOrEmpty(a.StaffMobileNo))
                                    partdata.Mobile = a.StaffMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else
                            {
                                if (a.Bill.NomineeId != null && a.Bill.NomineeId > 0)
                                    partdata.ClaimantName = a.NomineName;
                                else
                                    partdata.ClaimantName = a.Prefix + ". " + a.MemberName;
                                partdata.Mobile = a.Mobile;
                            }

                            partdata.Prefix = a.Prefix;

                            partdata.Email = a.Email;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static object GetAllReimbursementBillForSalary(object param)
        {

            try
            {
                string parameter = param as string;
                string[] sArray = parameter.Split(',');
                int MemberCode = int.Parse(sArray[0]);
                int Month = int.Parse(sArray[1]);
                int Year = int.Parse(sArray[2]);
                DateTime FirstDateOfMonth = new DateTime(Year, Month, 1);
                FirstDateOfMonth = FirstDateOfMonth.AddDays(-(FirstDateOfMonth.Day - 1));
                //  string dd = FirstDateOfMonth.ToString("dd/MM/yyyy");
                //  DateTime sss = Convert.ToDateTime(dd);
                //FirstDateOfMonth=  DateTime.ParseExact(FirstDateOfMonth.ToString("dd/MM/yyyy"), "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                //  FirstDateOfMonth = Convert.ToDateTime(dd);

                DateTime LastDateOfMonth = new DateTime(Year, Month, 1);
                LastDateOfMonth = LastDateOfMonth.AddMonths(1);
                LastDateOfMonth = LastDateOfMonth.AddDays(-(LastDateOfMonth.Day));
                //string ddd = LastDateOfMonth.ToString("dd/MM/yyyy");
                //LastDateOfMonth = DateTime.ParseExact(ddd, "MM/dd/yyyy HH:mm", CultureInfo.InvariantCulture);
                //DateTime ssss = Convert.ToDateTime(ddd);
                //LastDateOfMonth = Convert.ToDateTime(ddd);


                mReimbursementBill obj = param as mReimbursementBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill

                                where Bill.ClaimantId == MemberCode && (Bill.DateOfPresentation >= FirstDateOfMonth && Bill.DateOfPresentation <= LastDateOfMonth)
                                && Bill.IsSetEncashment == true && Bill.IsCanceled == false && (Bill.BillType == 1 || Bill.BillType == 2)
                                select Bill).ToList();


                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllReimbursementBillByEstablishId(object param)
        {

            try
            {
                mReimbursementBill obj = param as mReimbursementBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join S in db.mStaff on Bill.ClaimantId equals S.StaffID into S_join
                                from S in S_join.DefaultIfEmpty()
                                join F in db.mOtherFirm on Bill.ClaimantId equals F.FirmID into F_join
                                from F in F_join.DefaultIfEmpty()
                                join Nom in db.mMemberNominee on Bill.NomineeId equals Nom.NomineeId into N_join
                                from Nom in N_join.DefaultIfEmpty()
                                join SNom in db.mStaffNominee on Bill.NomineeId equals SNom.NomineeID into SN_join
                                from SNom in SN_join.DefaultIfEmpty()
                                where Bill.EstablishBillId == obj.EstablishBillId
                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    Email = M.Email,
                                    StaffName = S.StaffName,
                                    OtherFirm = F.Name,
                                    BillTypeName = BT.TypeName,
                                    StaffMobileNo = S.MobileNo,
                                    FirmMobileNo = F.MobileNo,
                                    NomineName = Nom.NomineeName,
                                    StaffNominee = SNom.NomineeName
                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;

                            if (a.Bill.Designation == 6)
                            {
                                if (!string.IsNullOrEmpty(a.Bill.ClaimantName))
                                    partdata.ClaimantName = a.Bill.ClaimantName;
                                else
                                    partdata.ClaimantName = a.OtherFirm;

                                if (!string.IsNullOrEmpty(a.FirmMobileNo))
                                    partdata.Mobile = a.FirmMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else if (a.Bill.Designation == 4 || a.Bill.Designation == 5 || a.Bill.Designation == 7)
                            {
                                if (a.Bill.NomineeId != null && a.Bill.NomineeId > 0)
                                    partdata.ClaimantName = a.StaffNominee;
                                else
                                    partdata.ClaimantName = a.StaffName;


                                if (!string.IsNullOrEmpty(a.StaffMobileNo))
                                    partdata.Mobile = a.StaffMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else
                            {
                                if (a.Bill.NomineeId != null && a.Bill.NomineeId > 0)
                                    partdata.ClaimantName = a.NomineName;
                                else
                                    partdata.ClaimantName = a.Prefix + ". " + a.MemberName;
                                partdata.Mobile = a.Mobile;
                            }

                            partdata.Prefix = a.Prefix;

                            partdata.Email = a.Email;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllReimbursementBillByEstablishIdAndClaimantId(object param)
        {

            try
            {
                mReimbursementBill obj = param as mReimbursementBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join S in db.mStaff on Bill.ClaimantId equals S.StaffID into S_join
                                from S in S_join.DefaultIfEmpty()
                                join F in db.mOtherFirm on Bill.ClaimantId equals F.FirmID into F_join
                                from F in F_join.DefaultIfEmpty()
                                join Nom in db.mMemberNominee on Bill.NomineeId equals Nom.NomineeId into N_join
                                from Nom in N_join.DefaultIfEmpty()
                                join SNom in db.mStaffNominee on Bill.NomineeId equals SNom.NomineeID into SN_join
                                from SNom in SN_join.DefaultIfEmpty()
                                where Bill.EstablishBillId == obj.EstablishBillId && Bill.ClaimantId == obj.ClaimantId
                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    Email = M.Email,
                                    StaffName = S.StaffName,
                                    OtherFirm = F.Name,
                                    BillTypeName = BT.TypeName,
                                    StaffMobileNo = S.MobileNo,
                                    FirmMobileNo = F.MobileNo,
                                    NomineName = Nom.NomineeName,
                                    StaffNominee = SNom.NomineeName
                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;

                            if (a.Bill.Designation == 6)
                            {
                                if (!string.IsNullOrEmpty(a.Bill.ClaimantName))
                                    partdata.ClaimantName = a.Bill.ClaimantName;
                                else
                                    partdata.ClaimantName = a.OtherFirm;

                                if (!string.IsNullOrEmpty(a.FirmMobileNo))
                                    partdata.Mobile = a.FirmMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else if (a.Bill.Designation == 4 || a.Bill.Designation == 5 || a.Bill.Designation == 7)
                            {
                                if (a.Bill.NomineeId != null && a.Bill.NomineeId > 0)
                                    partdata.ClaimantName = a.StaffNominee;
                                else
                                    partdata.ClaimantName = a.StaffName;

                                if (!string.IsNullOrEmpty(a.StaffMobileNo))
                                    partdata.Mobile = a.StaffMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else
                            {
                                if (a.Bill.NomineeId != null && a.Bill.NomineeId > 0)
                                    partdata.ClaimantName = a.NomineName;
                                else
                                    partdata.ClaimantName = a.Prefix + " " + a.MemberName;
                                partdata.Mobile = a.Mobile;
                            }

                            partdata.Prefix = a.Prefix;

                            partdata.Email = a.Email;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllReimbursementBillByMultipleEstablishId(object param)
        {

            try
            {
                string Parameter = param as string;

                string[] StringArray = Parameter.Split(',');
                StringArray = StringArray.Where(x => !string.IsNullOrEmpty(x)).ToArray();

                int[] EstablishIntArray = Array.ConvertAll(StringArray, s => int.Parse(s.ToString()));

                //    mReimbursementBill obj = param as mReimbursementBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join S in db.mStaff on Bill.ClaimantId equals S.StaffID into S_join
                                from S in S_join.DefaultIfEmpty()
                                join F in db.mOtherFirm on Bill.ClaimantId equals F.FirmID into F_join
                                from F in F_join.DefaultIfEmpty()
                                join Nom in db.mMemberNominee on Bill.NomineeId equals Nom.NomineeId into N_join
                                from Nom in N_join.DefaultIfEmpty()
                                join SNom in db.mStaffNominee on Bill.NomineeId equals SNom.NomineeID into SN_join
                                from SNom in SN_join.DefaultIfEmpty()
                                where (EstablishIntArray).Contains(Bill.EstablishBillId)
                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    Email = M.Email,
                                    StaffName = S.StaffName,
                                    OtherFirm = F.Name,
                                    BillTypeName = BT.TypeName,
                                    StaffMobileNo = S.MobileNo,
                                    FirmMobileNo = F.MobileNo,
                                    NomineName = Nom.NomineeName,
                                    StaffNominee = SNom.NomineeName
                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;

                            if (a.Bill.Designation == 6)
                            {
                                if (!string.IsNullOrEmpty(a.Bill.ClaimantName))
                                    partdata.ClaimantName = a.Bill.ClaimantName;
                                else
                                    partdata.ClaimantName = a.OtherFirm;

                                if (!string.IsNullOrEmpty(a.FirmMobileNo))
                                    partdata.Mobile = a.FirmMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else if (a.Bill.Designation == 4 || a.Bill.Designation == 5 || a.Bill.Designation == 7)
                            {
                                if (a.Bill.NomineeId != null && a.Bill.NomineeId > 0)
                                    partdata.ClaimantName = a.StaffNominee;
                                else
                                    partdata.ClaimantName = a.StaffName;

                                if (!string.IsNullOrEmpty(a.StaffMobileNo))
                                    partdata.Mobile = a.StaffMobileNo;
                                else
                                    partdata.Mobile = string.Empty;
                            }
                            else
                            {
                                if (a.Bill.NomineeId != null && a.Bill.NomineeId > 0)
                                    partdata.ClaimantName = a.NomineName;
                                else
                                    partdata.ClaimantName = a.Prefix + " " + a.MemberName;
                                partdata.Mobile = a.Mobile;
                            }

                            partdata.Prefix = a.Prefix;

                            partdata.Email = a.Email;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        // this method for getting Bill for Member
        public static object GetAllReimbursementBillForMember(object param)
        {

            try
            {
                int[] DesignationIds = new int[4];
                DesignationIds[0] = 0;
                DesignationIds[1] = 1;
                DesignationIds[2] = 2;
                DesignationIds[3] = 3;

                mReimbursementBill model = param as mReimbursementBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                where Bill.ClaimantId == model.ClaimantId
                                && (DesignationIds).Contains(Bill.Designation)
                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    Email = M.Email,
                                    BillTypeName = BT.TypeName
                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;
                            partdata.ClaimantName = a.Prefix + ". " + a.MemberName;
                            partdata.Prefix = a.Prefix;
                            partdata.Mobile = a.Mobile;
                            partdata.Email = a.Email;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static object GetReimbursementBillById(object param)
        {
            mReimbursementBill parameter = param as mReimbursementBill;

            BudgetContext db = new BudgetContext();
            if (parameter.ReimbursementBillID > 0)
            {
                var data = (from Bill in db.mReimbursementBill
                            join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                            join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                            from M in M_join.DefaultIfEmpty()
                            join S in db.mStaff on Bill.ClaimantId equals S.StaffID into S_join
                            from S in S_join.DefaultIfEmpty()
                            join F in db.mOtherFirm on Bill.ClaimantId equals F.FirmID into F_join
                            from F in F_join.DefaultIfEmpty()
                            where Bill.ReimbursementBillID == parameter.ReimbursementBillID
                            select new
                            {
                                Bill,
                                MemberName = M.Name,
                                Prefix = M.Prefix,
                                Mobile = M.Mobile,
                                Email = M.Email,
                                StaffName = S.StaffName,
                                OtherFirm = F.Name,
                                BillTypeName = BT.TypeName,
                                StaffMobileNo = S.MobileNo,
                                FirmMobileNo = F.MobileNo,
                            }).OrderByDescending(a => a.Bill.ReimbursementBillID).FirstOrDefault();

                mReimbursementBill partdata = new mReimbursementBill();

                partdata = data.Bill;
                if (data.Bill.Designation == 6)
                {
                    if (!string.IsNullOrEmpty(data.Bill.ClaimantName))
                        partdata.ClaimantName = data.Bill.ClaimantName;
                    else
                        partdata.ClaimantName = data.OtherFirm;

                    if (!string.IsNullOrEmpty(data.FirmMobileNo))
                        partdata.Mobile = data.FirmMobileNo;
                    else
                        partdata.Mobile = string.Empty;
                }
                else if (data.Bill.Designation == 4 || data.Bill.Designation == 5 || data.Bill.Designation == 7)
                {
                    partdata.ClaimantName = data.StaffName;
                    if (!string.IsNullOrEmpty(data.StaffMobileNo))
                        partdata.Mobile = data.StaffMobileNo;
                    else
                        partdata.Mobile = string.Empty;
                }
                else
                {
                    partdata.ClaimantName = data.Prefix + " " + data.MemberName;
                    partdata.Mobile = data.Mobile;
                }
                partdata.Prefix = data.Prefix;
                partdata.Email = data.Email;
                partdata.BillTypeName = data.BillTypeName;
                return partdata;
            }
            else
                return db.mReimbursementBill.SingleOrDefault(a => a.ReimbursementBillID == parameter.ReimbursementBillID);
        }
        /// <summary>
        /// Getting All bill for encashment on the behlf of Approved & Rejected
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static object GetAllReimbursementBillForEncashment(object param)
        {

            try
            {

                using (BudgetContext db = new BudgetContext())
                {

                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                where Bill.IsRejected == false && Bill.IsSanctioned == true
                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    BillTypeName = BT.TypeName
                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;
                            partdata.ClaimantName = a.Prefix + ". " + a.MemberName;
                            partdata.Prefix = a.Prefix;
                            partdata.Mobile = a.Mobile;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// Getting Bill's Total Gross Amount on the behlf of Budget Id
        /// </summary>
        /// <param name="param">BudgetId</param>
        /// <returns></returns>
        public static object GetTotalBillExpense(object param)
        {

            try
            {
                mEstablishBill parameter = param as mEstablishBill;
                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from Bill in db.mEstablishBill
                                where Bill.BudgetId == parameter.BudgetId && Bill.EstablishBillID <= parameter.EstablishBillID && Bill.IsCanceled == false
                                select Bill).ToList().Sum(m => m.TotalAmount);

                    var DebitReceiptSum = (from DR in db.mDebitReceiptExp
                                           where DR.BudgetId == parameter.BudgetId
                                           select DR).ToList().Sum(m => m.DebitReceiptAmount);

                    return data + DebitReceiptSum;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// This is for Getting Budget Head by Financial Year
        /// </summary>
        /// <param name="param">Financial Year</param>
        /// <returns></returns>
        public static object GetBudgetByFY(object param)
        {
            string Data = param as string;
            BudgetContext db = new BudgetContext();

            List<mBudget> result = new List<mBudget>();

            var ResultData = (from B in db.mBudget
                              join BT in db.mBudgetType on B.BudgetType equals BT.BudgetTypeID
                              join A in db.AllocateBudgets on B.BudgetID equals A.BudgetId
                              orderby B.OrderBy
                              where B.FinancialYear == Data && B.DisplayInReport == true && B.IsShowExpenseReport == true
                              select new { B, BudgetTypeName = BT.TypeName }).OrderByDescending(m => m.B.BudgetID).ToList();

            foreach (var a in ResultData)
            {
                mBudget partdata = new mBudget();
                partdata = a.B;
                partdata.BudgetTypeName = a.BudgetTypeName;
                result.Add(partdata);
            }

            return result;
        }



        /// <summary>
        /// this is for getting bill for Member and Other on the behlf of encashment date & Bill type -- Valid Authority False
        /// </summary>
        /// <param name="param">BudgetIdS,BDateFrom,BDateTo</param>
        /// <returns></returns>
        public static object GetAllReimbursementBillByParam(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string BudgetIdS = Result[0];
                string BDateFrom = Result[1];
                string BDateTo = Result[2];
                int BudgetId = 0;
                DateTime FirstDateOfMonth = new DateTime();
                DateTime LastDateOfMonth = new DateTime();

                int.TryParse(BudgetIdS, out BudgetId);
                DateTime.TryParse(BDateFrom, out FirstDateOfMonth);
                DateTime.TryParse(BDateTo, out LastDateOfMonth);

                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join Bud in db.mBudget on Bill.BudgetId equals Bud.BudgetID
                                where Bill.BudgetId == BudgetId && Bill.IsRejected == false && Bill.IsSetEncashment == true //&& Bill.IsApproved == true
                                && (Bill.EncashmentDate >= FirstDateOfMonth && Bill.EncashmentDate <= LastDateOfMonth)
                                select new { Bill, Bud, BillTypeName = BT.TypeName }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();//db.mNews.ToList();

                    List<mReimbursementBill> result = new List<mReimbursementBill>();

                    foreach (var a in data)
                    {
                        mReimbursementBill partdata = new mReimbursementBill();
                        partdata = a.Bill;
                        partdata.BudgetName = a.Bud.BudgetName;
                        partdata.FinancialYear = a.Bud.FinancialYear;
                        partdata.BillTypeName = a.BillTypeName;
                        result.Add(partdata);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// this is for getting Establish bill for Member and Other on the behlf of encashment date & Bill type -- Valid Authority False
        /// </summary>
        /// <param name="param">BudgetIdS,BDateFrom,BDateTo</param>
        /// <returns></returns>
        public static object GetAllEstablishBillForExpense(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string BudgetIdS = Result[0];
                string BDateFrom = Result[1];
                string BDateTo = Result[2];
                int BudgetId = 0;
                //DateTime FirstDateOfMonth = new DateTime();
                //DateTime LastDateOfMonth = new DateTime();

                int.TryParse(BudgetIdS, out BudgetId);
                DateTime FirstDateOfMonth = new DateTime(Convert.ToInt32(BDateFrom.Split('/')[2]), Convert.ToInt32(BDateFrom.Split('/')[0]), Convert.ToInt32(BDateFrom.Split('/')[1]));
                DateTime LastDateOfMonth = new DateTime(Convert.ToInt32(BDateTo.Split('/')[2]), Convert.ToInt32(BDateTo.Split('/')[0]), Convert.ToInt32(BDateTo.Split('/')[1]));
                // DateTime.TryParse(BDateFrom, out FirstDateOfMonth);
                //DateTime.TryParse(BDateTo, out LastDateOfMonth);

                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from Bill in db.mEstablishBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join Bud in db.mBudget on Bill.BudgetId equals Bud.BudgetID
                                where Bill.BudgetId == BudgetId && Bill.IsReject == false && Bill.IsEncashment == true && Bill.IsSanctioned == true && Bill.IsCanceled == false
                                && (Bill.EncashmentDate >= FirstDateOfMonth && Bill.EncashmentDate <= LastDateOfMonth)
                                select new { Bill, Bud, BillTypeName = BT.TypeName }).OrderBy(a => a.Bill.EstablishBillID).ToList();//db.mNews.ToList();

                    List<mEstablishBill> result = new List<mEstablishBill>();

                    foreach (var a in data)
                    {
                        mEstablishBill partdata = new mEstablishBill();

                        partdata = a.Bill;
                        partdata.IsDebitReceiptExpense = false;
                        partdata.BudgetName = a.Bud.BudgetName;
                        partdata.FinancialYear = a.Bud.FinancialYear;
                        partdata.BillTypeName = a.BillTypeName;
                        result.Add(partdata);
                    }

                    var DebitReceiptList = (from DR in db.mDebitReceiptExp
                                            join Bud in db.mBudget on DR.BudgetId equals Bud.BudgetID
                                            where (DR.DebitReceiptDate >= FirstDateOfMonth && DR.DebitReceiptDate <= LastDateOfMonth) && DR.BudgetId == BudgetId
                                            select new { DR, Bud }).OrderByDescending(a => a.DR.DebitReceiptExpID).ToList();
                    foreach (var DR in DebitReceiptList)
                    {
                        mEstablishBill partdata = new mEstablishBill();
                        partdata.TotalAmount = DR.DR.DebitReceiptAmount;
                        partdata.EncashmentDate = DR.DR.DebitReceiptDate;
                        partdata.DebitReceiptRemarks = DR.DR.Remarks;
                        partdata.IsDebitReceiptExpense = true;
                        partdata.BudgetName = DR.Bud.BudgetName;
                        partdata.FinancialYear = DR.Bud.FinancialYear;
                        result.Add(partdata);
                    }
                    return result.OrderBy(m => m.BillNumber).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        ///  Getting Total Bill GrossAmount for Expense Report on the behlf of BudgetIds,Encahment Date
        /// </summary>
        /// <param name="param">BudgetIdS,BDateFrom,BDateTo</param>
        /// <returns></returns>
        public static object GetTotalBillExpenseForExpenseReport(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string BudgetIdS = Result[0];
                string BDateFrom = Result[1];
                string BDateTo = Result[2];

                int BudgetId = 0;
                //DateTime FirstDateOfPreFirstMonth = new DateTime();
                //DateTime LastDateOfPreLastMonth = new DateTime();

                int.TryParse(BudgetIdS, out BudgetId);

                DateTime FirstDateOfPreFirstMonth = new DateTime(Convert.ToInt32(BDateFrom.Split('/')[2]), Convert.ToInt32(BDateFrom.Split('/')[0]), Convert.ToInt32(BDateFrom.Split('/')[1]));
                DateTime LastDateOfPreLastMonth = new DateTime(Convert.ToInt32(BDateTo.Split('/')[2]), Convert.ToInt32(BDateTo.Split('/')[0]), Convert.ToInt32(BDateTo.Split('/')[1]));


                //DateTime.TryParse(BDateFrom, out FirstDateOfPreFirstMonth);
                //DateTime.TryParse(BDateTo, out LastDateOfPreLastMonth);

                List<string> EstablishId = new List<string>();


                using (BudgetContext db = new BudgetContext())
                {
                    //var EstablishArray = (from Establish in db.mEstablishBill
                    //                      where Establish.BudgetId == BudgetId && Establish.IsReject == false && Establish.IsEncashment == true && Establish.IsSanctioned == true
                    //              && (Establish.EncashmentDate >= FirstDateOfPreFirstMonth && Establish.EncashmentDate <= LastDateOfPreLastMonth)
                    //                      select new { EstablishIds = Establish.EstablishBillID }).ToList();//db.mNews.ToList();
                    //List<int> EtsablishIdList = new List<int>();
                    //foreach (var item in EstablishArray)
                    //{
                    //    EtsablishIdList.Add(item.EstablishIds);
                    //}

                    //int[] EstablishIntArray = EtsablishIdList.ToArray();//Array.ConvertAll(StringArray, s => int.Parse(s.ToString()));


                    //var data = (from Bill in db.mReimbursementBill
                    //            where Bill.BudgetId == BudgetId && Bill.IsRejected == false && Bill.IsSetEncashment == true && Bill.IsSanctioned == true
                    //           && (EstablishIntArray).Contains(Bill.EstablishBillId)
                    //            select Bill).ToList().Sum(m => m.SactionAmount); 


                    var data = (from Establish in db.mEstablishBill
                                where Establish.BudgetId == BudgetId && Establish.IsReject == false && Establish.IsEncashment == true && Establish.IsSanctioned == true && Establish.IsCanceled == false
                        && (Establish.EncashmentDate >= FirstDateOfPreFirstMonth && Establish.EncashmentDate <= LastDateOfPreLastMonth)
                                select Establish).ToList().Sum(m => m.TotalAmount);

                    var DebitReceiptSum = (from DR in db.mDebitReceiptExp
                                           where (DR.DebitReceiptDate >= FirstDateOfPreFirstMonth && DR.DebitReceiptDate <= LastDateOfPreLastMonth) && DR.BudgetId == BudgetId
                                           select DR).ToList().Sum(m => m.DebitReceiptAmount);

                    return data + DebitReceiptSum;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetTotalBillExpenseForSummaryReport(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string[] StringArray = Result[0].Split('-');
                StringArray = StringArray.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                string BDateFrom = Result[1];
                string BDateTo = Result[2];

                decimal TotalAmount = 0;


                DateTime FirstDateOfPreFirstMonth = new DateTime(Convert.ToInt32(BDateFrom.Split('/')[2]), Convert.ToInt32(BDateFrom.Split('/')[0]), Convert.ToInt32(BDateFrom.Split('/')[1]));
                DateTime LastDateOfPreLastMonth = new DateTime(Convert.ToInt32(BDateTo.Split('/')[2]), Convert.ToInt32(BDateTo.Split('/')[0]), Convert.ToInt32(BDateTo.Split('/')[1]));

                List<string> EstablishId = new List<string>();


                using (BudgetContext db = new BudgetContext())
                {


                    int[] EstablishIntArray = Array.ConvertAll(StringArray, s => int.Parse(s.ToString()));





                    var data = (from Establish in db.mEstablishBill
                                where (EstablishIntArray).Contains(Establish.BudgetId) && Establish.IsReject == false && Establish.IsEncashment == true && Establish.IsSanctioned == true && Establish.IsCanceled == false
                        && (Establish.EncashmentDate >= FirstDateOfPreFirstMonth && Establish.EncashmentDate <= LastDateOfPreLastMonth)
                                select Establish).ToList();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var item in data)
                        {
                            if (item.BillType == 7)
                                TotalAmount = TotalAmount + item.TotalGrossAmount;
                            else
                                TotalAmount = TotalAmount + item.TotalAmount;
                        }
                    }

                    var DebitReceiptSum = (from DR in db.mDebitReceiptExp
                                           where (DR.DebitReceiptDate >= FirstDateOfPreFirstMonth && DR.DebitReceiptDate <= LastDateOfPreLastMonth) && (EstablishIntArray).Contains(DR.BudgetId)
                                           select DR).ToList().Sum(m => m.DebitReceiptAmount);

                    return TotalAmount + DebitReceiptSum;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static object GetTotalBillExpenseForExpenseReportForSalary(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string BudgetIdS = Result[0];
                string BDateFrom = Result[1];
                string BDateTo = Result[2];

                int BudgetId = 0;

                int.TryParse(BudgetIdS, out BudgetId);

                DateTime FirstDateOfPreFirstMonth = new DateTime(Convert.ToInt32(BDateFrom.Split('/')[2]), Convert.ToInt32(BDateFrom.Split('/')[0]), Convert.ToInt32(BDateFrom.Split('/')[1]));
                DateTime LastDateOfPreLastMonth = new DateTime(Convert.ToInt32(BDateTo.Split('/')[2]), Convert.ToInt32(BDateTo.Split('/')[0]), Convert.ToInt32(BDateTo.Split('/')[1]));



                List<string> EstablishId = new List<string>();


                using (BudgetContext db = new BudgetContext())
                {


                    var data = (from Establish in db.mEstablishBill
                                where Establish.BudgetId == BudgetId && Establish.IsReject == false && Establish.IsEncashment == true && Establish.IsSanctioned == true && Establish.IsCanceled == false
                        && (Establish.EncashmentDate >= FirstDateOfPreFirstMonth && Establish.EncashmentDate <= LastDateOfPreLastMonth)
                                select Establish).ToList().Sum(m => m.TotalGrossAmount);

                    var DebitReceiptSum = (from DR in db.mDebitReceiptExp
                                           where (DR.DebitReceiptDate >= FirstDateOfPreFirstMonth && DR.DebitReceiptDate <= LastDateOfPreLastMonth) && DR.BudgetId == BudgetId
                                           select DR).ToList().Sum(m => m.DebitReceiptAmount);

                    return data + DebitReceiptSum;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// This is for getting Bill which is need for Sactioned on the behalf of Saction No. & Saction Date of Sub Voucher
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        /// 

        public static object SearchReimbursementBill(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string BillTypeS = Result[0];
                string StausS = Result[1];
                string SubmissionDateFromS = Result[2];
                string SubmissionDateToS = Result[3];
                string MemberS = Result[4];
                int BillType = 0;
                int.TryParse(BillTypeS, out BillType);
                int Staus = 0;
                int.TryParse(StausS, out Staus);
                int MemberCode = 0;
                int.TryParse(MemberS, out MemberCode);

                DateTime SubmissionDateFrom = new DateTime();
                DateTime.TryParse(SubmissionDateFromS, out SubmissionDateFrom);

                DateTime SubmissionDateTo = new DateTime();
                DateTime.TryParse(SubmissionDateToS, out SubmissionDateTo);




                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                where Bill.BillType == BillType || Bill.Status == Staus ||
                                (Bill.DateOfPresentation >= SubmissionDateFrom && Bill.DateOfPresentation <= SubmissionDateTo)
                                || Bill.ClaimantId == MemberCode

                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    BillTypeName = BT.TypeName
                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;
                            partdata.ClaimantName = a.Prefix + ". " + a.MemberName;
                            partdata.Prefix = a.Prefix;
                            partdata.Mobile = a.Mobile;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }



                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object SearchReimbursementBillForMembers(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string BillTypeS = Result[0];
                string StausS = Result[1];
                string SubmissionDateFromS = Result[2];
                string SubmissionDateToS = Result[3];
                string MemberS = Result[4];
                int BillType = 0;
                int.TryParse(BillTypeS, out BillType);

                int Staus = 0;
                int.TryParse(StausS, out Staus);

                int MemberCode = 0;
                int.TryParse(MemberS, out MemberCode);

                DateTime SubmissionDateFrom = new DateTime();
                DateTime.TryParse(SubmissionDateFromS, out SubmissionDateFrom);

                DateTime SubmissionDateTo = new DateTime();
                DateTime.TryParse(SubmissionDateToS, out SubmissionDateTo);




                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mMembers on Bill.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                where (Bill.BillType == BillType && Bill.Status == Staus &&
                                (Bill.DateOfPresentation >= SubmissionDateFrom && Bill.DateOfPresentation <= SubmissionDateTo))
                                 && Bill.ClaimantId == MemberCode

                                select new
                                {
                                    Bill,
                                    MemberName = M.Name,
                                    Prefix = M.Prefix,
                                    Mobile = M.Mobile,
                                    BillTypeName = BT.TypeName
                                }).OrderByDescending(a => a.Bill.ReimbursementBillID).ToList();


                    List<mReimbursementBill> result = new List<mReimbursementBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mReimbursementBill partdata = new mReimbursementBill();

                            partdata = a.Bill;
                            partdata.ClaimantName = a.Prefix + ". " + a.MemberName;
                            partdata.Prefix = a.Prefix;
                            partdata.Mobile = a.Mobile;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }



                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        static object GetBCodebyFYear(object param)
        {

            mBudget budget = param as mBudget;
            try
            {
                using (BudgetContext db = new BudgetContext())
                {

                    var data = (from BU in db.mBudget
                                where BU.FinancialYear == budget.FinancialYear
                                select BU.BudgetCode).FirstOrDefault();
                    return data;
                }

                return null;
            }
            catch
            {
                throw;
            }






        }
        #endregion


        #region Comments

        public static object CreateComments(object param)
        {
            try
            {
                int CommentsId = 0;
                using (BudgetContext db = new BudgetContext())
                {
                    mComments model = param as mComments;
                    db.mComments.Add(model);
                    db.SaveChanges();
                    db.Close();
                    CommentsId = model.CommentsID;

                }

                return CommentsId;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateComments(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mComments model = param as mComments;
                    db.mComments.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteComments(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mComments parameter = param as mComments;
                    mComments stateToRemove = db.mComments.SingleOrDefault(a => a.CommentsID == parameter.CommentsID);
                    db.mComments.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllComments(object param)
        {
            mComments parameter = param as mComments;

            BudgetContext db = new BudgetContext();

            var data = (from Comm in db.mComments
                        where Comm.ParentId == parameter.ParentId && Comm.ParentName == parameter.ParentName
                        select Comm).OrderByDescending(a => a.CommentsID).ToList();


            return data;
        }

        #endregion

        #region Debit Recipt Expense
        public static object CreateDebitReceiptExp(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mDebitReceiptExp model = param as mDebitReceiptExp;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mDebitReceiptExp.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateDebitReceiptExp(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mDebitReceiptExp model = param as mDebitReceiptExp;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mDebitReceiptExp.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteDebitReceiptExp(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mDebitReceiptExp parameter = param as mDebitReceiptExp;
                    mDebitReceiptExp stateToRemove = db.mDebitReceiptExp.SingleOrDefault(a => a.DebitReceiptExpID == parameter.DebitReceiptExpID);
                    db.mDebitReceiptExp.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllDebitReceiptExp(object param)
        {

            try
            {

                using (BudgetContext db = new BudgetContext())
                {

                    var data = (from DR in db.mDebitReceiptExp
                                join Bud in db.mBudget on DR.BudgetId equals Bud.BudgetID
                                select new { DR, Bud }).OrderByDescending(a => a.DR.DebitReceiptExpID).ToList();

                    List<mDebitReceiptExp> result = new List<mDebitReceiptExp>();

                    foreach (var a in data)
                    {
                        mDebitReceiptExp partdata = new mDebitReceiptExp();

                        partdata = a.DR;
                        partdata.BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}({5}) {6}-{7}-{8}", a.Bud.MajorHead, a.Bud.SubMajorHead, a.Bud.MinorHead, a.Bud.SubHead, a.Bud.Gazetted, a.Bud.VotedCharged, a.Bud.Plan, a.Bud.ObjectCode.ToString(), a.Bud.ObjectCodeText);

                        result.Add(partdata);
                    }

                    return result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetDebitReceiptExpById(object param)
        {
            mDebitReceiptExp parameter = param as mDebitReceiptExp;

            BudgetContext db = new BudgetContext();

            var query = db.mDebitReceiptExp.SingleOrDefault(a => a.DebitReceiptExpID == parameter.DebitReceiptExpID);

            return query;
        }
        #endregion

        #region Authority Letter
        public static object CreateAuthorityLetter(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mAuthorityLetter model = param as mAuthorityLetter;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mAuthorityLetter.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object UpdateAuthorityLetter(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mAuthorityLetter model = param as mAuthorityLetter;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mAuthorityLetter.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object DeleteAuthorityLetter(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mAuthorityLetter parameter = param as mAuthorityLetter;
                    mAuthorityLetter stateToRemove = db.mAuthorityLetter.SingleOrDefault(a => a.AuthorityLetterID == parameter.AuthorityLetterID);
                    db.mAuthorityLetter.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllAuthorityLetter(object param)
        {

            try
            {

                using (BudgetContext db = new BudgetContext())
                {

                    var data = (from AL in db.mAuthorityLetter
                                orderby AL.AuthorityLetterID
                                select AL).OrderByDescending(m => m.AuthorityLetterID).ToList();

                    //List<mDebitReceiptExp> result = new List<mDebitReceiptExp>();

                    //foreach (var a in data)
                    //{
                    //    mDebitReceiptExp partdata = new mDebitReceiptExp();

                    //    partdata = a.DR;
                    //    partdata.BudgetName = string.Format("{0}-{1}-{2}-{3}-{4}({5}) {6}-{7}-{8}", a.Bud.MajorHead, a.Bud.SubMajorHead, a.Bud.MinorHead, a.Bud.SubHead, a.Bud.Gazetted, a.Bud.VotedCharged, a.Bud.Plan, a.Bud.ObjectCode.ToString(), a.Bud.ObjectCodeText);

                    //    result.Add(partdata);
                    //}

                    return data;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAuthorityLetterById(object param)
        {
            mAuthorityLetter parameter = param as mAuthorityLetter;

            BudgetContext db = new BudgetContext();

            var query = db.mAuthorityLetter.SingleOrDefault(a => a.AuthorityLetterID == parameter.AuthorityLetterID);

            return query;
        }

        /// <summary>
        /// this is for getting bill for Member and Other on the behlf of encashment date & Bill type -- Valid Authority False
        /// </summary>
        /// <param name="param">EncashmentDate,BillType</param>
        /// <returns></returns>
        public static object GetAllReimbursementBillForALMemeberAndOther(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string EncashmentDateSt = Result[0];
                int DesignationId = int.Parse(Result[1]);

                //  DateTime EncashmentDate = new DateTime();
                DateTime EncashmentDate = new DateTime(Convert.ToInt32(EncashmentDateSt.Split('/')[2]), Convert.ToInt32(EncashmentDateSt.Split('/')[0]), Convert.ToInt32(EncashmentDateSt.Split('/')[1]));
                // DateTime.TryParse(EncashmentDateSt, out EncashmentDate);

                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from Bill in db.mEstablishBill
                                join Bud in db.mBudget on Bill.BudgetId equals Bud.BudgetID
                                where Bill.EncashmentDate == EncashmentDate && Bill.Designation == DesignationId &&
                                Bill.IsValidAuthority == false && Bill.IsReject == false && Bill.IsSanctioned == true && Bill.IsEncashment == true && Bill.IsCanceled == false
                                select new { Bill, Bud }).OrderBy(a => a.Bill.EstablishBillID).ToList();

                    List<mEstablishBill> result = new List<mEstablishBill>();

                    foreach (var a in data)
                    {
                        mEstablishBill partdata = new mEstablishBill();

                        partdata = a.Bill;
                        partdata.BudgetName = a.Bud.BudgetName; //string.Format("{0}-{1}-{2}-{3}-{4}({5}) {6}-{7}-{8}", a.Bud.MajorHead, a.Bud.SubMajorHead, a.Bud.MinorHead, a.Bud.SubHead, a.Bud.Gazetted, a.Bud.VotedCharged, a.Bud.Plan, a.Bud.ObjectCode.ToString(), a.Bud.ObjectCodeText);
                        partdata.FinancialYear = a.Bud.FinancialYear;
                        result.Add(partdata);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllBillForOtherExStaffExMember(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string EncashmentDateSt = Result[0];
                string Designation = Result[1];
                string[] Designations = Designation.Split('-'); ;
                int[] DesignationIds = Array.ConvertAll(Designations, s => int.Parse(s));
                //  DateTime EncashmentDate = new DateTime();
                DateTime EncashmentDate = new DateTime(Convert.ToInt32(EncashmentDateSt.Split('/')[2]), Convert.ToInt32(EncashmentDateSt.Split('/')[0]), Convert.ToInt32(EncashmentDateSt.Split('/')[1]));
                // DateTime.TryParse(EncashmentDateSt, out EncashmentDate);

                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from Bill in db.mEstablishBill
                                join Bud in db.mBudget on Bill.BudgetId equals Bud.BudgetID
                                where Bill.EncashmentDate == EncashmentDate && (DesignationIds).Contains(Bill.Designation) &&
                                Bill.IsValidAuthority == false && Bill.IsReject == false && Bill.IsSanctioned == true && Bill.IsEncashment == true && Bill.IsCanceled == false
                                select new { Bill, Bud }).OrderBy(a => a.Bill.EstablishBillID).ToList();

                    List<mEstablishBill> result = new List<mEstablishBill>();

                    foreach (var a in data)
                    {
                        mEstablishBill partdata = new mEstablishBill();

                        partdata = a.Bill;
                        partdata.BudgetName = a.Bud.BudgetName; //string.Format("{0}-{1}-{2}-{3}-{4}({5}) {6}-{7}-{8}", a.Bud.MajorHead, a.Bud.SubMajorHead, a.Bud.MinorHead, a.Bud.SubHead, a.Bud.Gazetted, a.Bud.VotedCharged, a.Bud.Plan, a.Bud.ObjectCode.ToString(), a.Bud.ObjectCodeText);
                        partdata.FinancialYear = a.Bud.FinancialYear;
                        result.Add(partdata);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static object GetAllEstablishBillForStaff(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string EncashmentDateSt = Result[0];
                string Designation = Result[1];
                string[] Designations = Designation.Split('-'); ;
                int[] DesignationIds = Array.ConvertAll(Designations, s => int.Parse(s));

                DateTime EncashmentDate = new DateTime(Convert.ToInt32(EncashmentDateSt.Split('/')[2]), Convert.ToInt32(EncashmentDateSt.Split('/')[0]), Convert.ToInt32(EncashmentDateSt.Split('/')[1]));

                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from Bill in db.mEstablishBill
                                join Bud in db.mBudget on Bill.BudgetId equals Bud.BudgetID
                                where Bill.EncashmentDate == EncashmentDate && (DesignationIds).Contains(Bill.Designation) &&
                                Bill.IsValidAuthority == false && Bill.IsReject == false && Bill.IsSanctioned == true && Bill.IsEncashment == true && Bill.IsCanceled == false
                                select new { Bill, Bud }).OrderBy(a => a.Bill.EstablishBillID).ToList();

                    List<mEstablishBill> result = new List<mEstablishBill>();

                    foreach (var a in data)
                    {
                        mEstablishBill partdata = new mEstablishBill();

                        partdata = a.Bill;
                        partdata.BudgetName = a.Bud.BudgetName;
                        partdata.FinancialYear = a.Bud.FinancialYear;
                        result.Add(partdata);
                    }

                    return result.OrderBy(m => m.EstablishBillID).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        /// <summary>
        ///  /// this is for getting bill for Speaker And Deputy Speaker on the behlf of encashment date & Bill type -- Valid Authority False
        /// </summary>
        /// <param name="param">EncashmentDate,BillType</param>
        /// <returns></returns>
        public static object GetAllReimbursementBillForSpeakerAndDeputySpeaker(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                string EncashmentDateSt = Result[0];
                string Designation = Result[1];
                string[] Designations = Designation.Split('-'); ;


                int[] DesignationIds = Array.ConvertAll(Designations, s => int.Parse(s));
                DateTime EncashmentDate = new DateTime(Convert.ToInt32(EncashmentDateSt.Split('/')[2]), Convert.ToInt32(EncashmentDateSt.Split('/')[0]), Convert.ToInt32(EncashmentDateSt.Split('/')[1]));

                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;

                    var data = (from RB in db.mReimbursementBill
                                join EB in db.mEstablishBill on new { EstablishBillId = RB.EstablishBillId } equals new { EstablishBillId = EB.EstablishBillID }
                                join B in db.mBudget on new { BudgetID = RB.BudgetId } equals new { BudgetID = B.BudgetID }
                                join M in db.mMembers on RB.ClaimantId equals M.MemberCode
                                join MA in db.mMemberAccountsDetails on RB.ClaimantId equals MA.MemberCode into MA_join
                                from MA in MA_join.DefaultIfEmpty()
                                where
                                  EB.EncashmentDate == EncashmentDate && (DesignationIds).Contains(EB.Designation) &&
                                   (DesignationIds).Contains(RB.Designation) && RB.IsSanctioned == true && RB.IsSetEncashment == true && RB.IsCanceled == false &&
                                    EB.IsValidAuthority == false && RB.IsRejected == false
                                select new
                                {
                                    M.Name,
                                    M.MemberCode,
                                    EstablishId = (int?)RB.EstablishBillId,
                                    RB.BillType,
                                    BudgetName = B.BudgetName,
                                    B.ObjectCodeText,
                                    B.FinancialYear,
                                    AccountNo = MA.AccountNo,
                                    RB.SactionAmount,
                                    BillNumber = EB.BillNumber
                                }).ToList();



                    List<AccountDetail> result = new List<AccountDetail>();

                    foreach (var a in data)
                    {
                        AccountDetail partdata = new AccountDetail();

                        partdata.MemberCode = int.Parse(a.MemberCode.ToString());
                        partdata.ClaimantName = a.Name;
                        partdata.BudgetName = a.BudgetName;
                        partdata.FinancialYear = a.FinancialYear;
                        partdata.EstablishId = int.Parse(a.EstablishId.ToString());
                        partdata.ObjectCodeText = a.ObjectCodeText;

                        partdata.AccountNo = (a.AccountNo != null ? a.AccountNo.Value.ToString() : "0");
                        partdata.VoucherAmount = a.SactionAmount;
                        partdata.BillNumber = a.BillNumber;
                        result.Add(partdata);
                    }



                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllAccountDetail(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Remove(Data.Length - 1, 1).Split(',');
                int[] BillIds = Array.ConvertAll(Result, s => int.Parse(s));



                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;


                    var data = (from RB in db.mReimbursementBill
                                join EB in db.mEstablishBill on new { EstablishBillId = RB.EstablishBillId } equals new { EstablishBillId = EB.EstablishBillID }
                                join B in db.mBudget on new { BudgetID = EB.BudgetId } equals new { BudgetID = B.BudgetID }
                                join M in db.mMembers on RB.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join S in db.mStaff on RB.ClaimantId equals S.StaffID into S_join
                                from S in S_join.DefaultIfEmpty()
                                join F in db.mOtherFirm on RB.ClaimantId equals F.FirmID into F_join
                                from F in F_join.DefaultIfEmpty()
                                join MA in db.mMemberAccountsDetails on RB.ClaimantId equals MA.MemberCode into MA_join
                                from MA in MA_join.DefaultIfEmpty()
                                join Nom in db.mMemberNominee on RB.NomineeId equals Nom.NomineeId into N_join
                                from Nom in N_join.DefaultIfEmpty()
                                join SNom in db.mStaffNominee on RB.NomineeId equals SNom.NomineeID into SN_join
                                from SNom in SN_join.DefaultIfEmpty()
                                where
                                  (BillIds).Contains(EB.EstablishBillID) && EB.AdvanceBillNo == 0
                                orderby
                               RB.ReimbursementBillID
                                select new
                                {
                                    MemberCode = (int?)M.MemberCode,
                                    M.Name,
                                    EstablishId = (int?)RB.EstablishBillId,
                                    BillNumber = EB.BillNumber,
                                    B.ObjectCodeText,
                                    MA.AccountNoS,
                                    MA.BankName,
                                    MA.IFSCCode,
                                    M.PPONO,
                                    MA.MemberPayeeCode,
                                    RB.SactionAmount,
                                    RB.ClaimantName,
                                    RB.NomineeId,
                                    S.StaffName,
                                    RB.Designation,
                                    OtherFirm = F.Name,
                                    OtherFirmAccNo = F.AccNo,
                                    OtherFirmIfscCode = F.IFSCCode,
                                    OtherFirmBankName = F.BankName,
                                    StaffAccNo = S.BankAccountNo,
                                    StaffIfscCode = S.IFSC,
                                    StaffBankName = S.BankName,
                                    NomineName = Nom.NomineeName,
                                    NomAccountNoS = Nom.NomineeAccountNoS,
                                    NomBankName = Nom.NomineeBankName,
                                    NomIfscCode = Nom.NomineeIFSCCode,
                                    AdvanceBillNumber = EB.AdvanceBillNo,
                                    AdvanceGrossAmount = EB.TotalGrossAmount,
                                    StaffNominee = SNom.NomineeName,
                                    StaffNomAccNo = SNom.BankAccountNo,
                                    StaffNomIFSC = SNom.IFSC,
                                    StaffNomBankName = SNom.BankName
                                }).ToList();


                    List<AccountDetail> result = new List<AccountDetail>();

                    foreach (var a in data)
                    {
                        AccountDetail partdata = new AccountDetail();

                        partdata.MemberCode = (a.MemberCode != null ? int.Parse(a.MemberCode.ToString()) : 0);
                        if (a.Designation == 6)
                        {
                            if (!string.IsNullOrEmpty(a.ClaimantName))
                                partdata.ClaimantName = a.ClaimantName;
                            else
                                partdata.ClaimantName = a.OtherFirm;

                            partdata.AccountNo = (a.OtherFirmAccNo != null ? a.OtherFirmAccNo : "0");
                            partdata.BankName = a.OtherFirmBankName;
                            partdata.IFSCCode = a.OtherFirmIfscCode;
                            partdata.MemberPayeeCode = a.MemberPayeeCode;
                        }
                        else if (a.Designation == 4 || a.Designation == 5 || a.Designation == 7)
                        {
                            if (a.NomineeId > 0)
                            {
                                partdata.ClaimantName = a.StaffNominee;
                                partdata.AccountNo = (a.StaffNomAccNo != null ? a.StaffNomAccNo : "0");
                                partdata.BankName = a.StaffNomBankName;
                                partdata.IFSCCode = a.StaffNomIFSC;
                                partdata.MemberPayeeCode = a.MemberPayeeCode;


                            }
                            else
                            {
                                partdata.ClaimantName = a.StaffName;
                                partdata.AccountNo = (a.StaffAccNo != null ? a.StaffAccNo : "0");
                                partdata.BankName = a.StaffBankName;
                                partdata.IFSCCode = a.StaffIfscCode;
                                partdata.MemberPayeeCode = a.MemberPayeeCode;
                            }
                        }
                        else
                        {
                            if (a.NomineeId > 0)
                            {

                                partdata.ClaimantName = a.NomineName;
                                partdata.AccountNo = (!string.IsNullOrEmpty(a.NomAccountNoS) ? a.NomAccountNoS : "0");
                                partdata.BankName = a.NomBankName;
                                partdata.IFSCCode = a.NomIfscCode;
                                partdata.MemberPayeeCode = a.MemberPayeeCode;
                            }
                            else
                            {
                                partdata.ClaimantName = a.Name;
                                partdata.AccountNo = (!string.IsNullOrEmpty(a.AccountNoS) ? a.AccountNoS : "0");
                                partdata.BankName = a.BankName;
                                partdata.IFSCCode = a.IFSCCode;
                                partdata.MemberPayeeCode = a.MemberPayeeCode;
                            }
                        }

                        partdata.EstablishId = int.Parse(a.EstablishId.ToString());
                        partdata.ObjectCodeText = a.ObjectCodeText;

                        partdata.BillNumber = a.BillNumber;

                        partdata.VoucherAmount = a.SactionAmount;
                        partdata.AdvanceBillNumber = a.AdvanceBillNumber;
                        partdata.AdvanceBillGrossAmt = a.AdvanceGrossAmount;
                        result.Add(partdata);

                    }

                    var AdvanceBill = (from EBs in db.mEstablishBill
                                       where (BillIds).Contains(EBs.EstablishBillID) && EBs.AdvanceBillNo > 0
                                       select EBs).ToList();
                    foreach (var item in AdvanceBill)
                    {
                        var SingleVoucher = (from RB in db.mReimbursementBill
                                             join EB in db.mEstablishBill on new { EstablishBillId = RB.EstablishBillId } equals new { EstablishBillId = EB.EstablishBillID }
                                             join B in db.mBudget on new { BudgetID = EB.BudgetId } equals new { BudgetID = B.BudgetID }
                                             join M in db.mMembers on RB.ClaimantId equals M.MemberCode into M_join
                                             from M in M_join.DefaultIfEmpty()
                                             join S in db.mStaff on RB.ClaimantId equals S.StaffID into S_join
                                             from S in S_join.DefaultIfEmpty()
                                             join F in db.mOtherFirm on RB.ClaimantId equals F.FirmID into F_join
                                             from F in F_join.DefaultIfEmpty()
                                             join MA in db.mMemberAccountsDetails on RB.ClaimantId equals MA.MemberCode into MA_join
                                             from MA in MA_join.DefaultIfEmpty()
                                             join Nom in db.mMemberNominee on RB.NomineeId equals Nom.NomineeId into N_join
                                             from Nom in N_join.DefaultIfEmpty()
                                             join SNom in db.mStaffNominee on RB.NomineeId equals SNom.NomineeID into SN_join
                                             from SNom in SN_join.DefaultIfEmpty()
                                             where
                                             EB.EstablishBillID == item.EstablishBillID
                                             orderby
                                            RB.ReimbursementBillID
                                             select new
                                             {
                                                 MemberCode = (int?)M.MemberCode,
                                                 M.Name,
                                                 EstablishId = (int?)RB.EstablishBillId,
                                                 BillNumber = EB.BillNumber,
                                                 B.ObjectCodeText,
                                                 MA.AccountNoS,
                                                 MA.BankName,
                                                 MA.IFSCCode,
                                                 MA.MemberPayeeCode,
                                                 RB.SactionAmount,
                                                 RB.ClaimantName,
                                                 RB.NomineeId,
                                                 S.StaffName,
                                                 RB.Designation,
                                                 OtherFirm = F.Name,
                                                 OtherFirmAccNo = F.AccNo,
                                                 OtherFirmIfscCode = F.IFSCCode,
                                                 OtherFirmBankName = F.BankName,
                                                 StaffAccNo = S.BankAccountNo,
                                                 StaffIfscCode = S.IFSC,
                                                 StaffBankName = S.BankName,
                                                 NomineName = Nom.NomineeName,
                                                 NomAccountNoS = Nom.NomineeAccountNoS,
                                                 NomBankName = Nom.NomineeBankName,
                                                 NomIfscCode = Nom.NomineeIFSCCode,
                                                 AdvanceBillNumber = EB.AdvanceBillNo,
                                                 AdvanceGrossAmount = EB.TotalGrossAmount,
                                                 StaffNominee = SNom.NomineeName,
                                                 StaffNomAccNo = SNom.BankAccountNo,
                                                 StaffNomIFSC = SNom.IFSC,
                                                 StaffNomBankName = SNom.BankName
                                             }).FirstOrDefault();


                        if (SingleVoucher != null)
                        {
                            AccountDetail partdata = new AccountDetail();

                            partdata.MemberCode = (SingleVoucher.MemberCode != null ? int.Parse(SingleVoucher.MemberCode.ToString()) : 0);
                            if (SingleVoucher.Designation == 6)
                            {
                                if (!string.IsNullOrEmpty(SingleVoucher.ClaimantName))
                                    partdata.ClaimantName = SingleVoucher.ClaimantName;
                                else
                                    partdata.ClaimantName = SingleVoucher.OtherFirm;

                                partdata.AccountNo = (SingleVoucher.OtherFirmAccNo != null ? SingleVoucher.OtherFirmAccNo : "0");
                                partdata.BankName = SingleVoucher.OtherFirmBankName;
                                partdata.IFSCCode = SingleVoucher.OtherFirmIfscCode;
                                partdata.MemberPayeeCode = SingleVoucher.MemberPayeeCode;
                            }
                            else if (SingleVoucher.Designation == 4 || SingleVoucher.Designation == 5 || SingleVoucher.Designation == 7)
                            {



                                if (SingleVoucher.NomineeId > 0)
                                {
                                    partdata.ClaimantName = SingleVoucher.StaffNominee;
                                    partdata.AccountNo = (SingleVoucher.StaffNomAccNo != null ? SingleVoucher.StaffNomAccNo : "0");
                                    partdata.BankName = SingleVoucher.StaffNomBankName;
                                    partdata.IFSCCode = SingleVoucher.StaffNomIFSC;
                                    partdata.MemberPayeeCode = SingleVoucher.MemberPayeeCode;


                                }
                                else
                                {
                                    partdata.ClaimantName = SingleVoucher.StaffName;
                                    partdata.AccountNo = (SingleVoucher.StaffAccNo != null ? SingleVoucher.StaffAccNo : "0");
                                    partdata.BankName = SingleVoucher.StaffBankName;
                                    partdata.IFSCCode = SingleVoucher.StaffIfscCode;
                                    partdata.MemberPayeeCode = SingleVoucher.MemberPayeeCode;
                                }

                            }
                            else
                            {
                                if (SingleVoucher.NomineeId > 0)
                                {

                                    partdata.ClaimantName = SingleVoucher.NomineName;
                                    partdata.AccountNo = (!string.IsNullOrEmpty(SingleVoucher.NomAccountNoS) ? SingleVoucher.NomAccountNoS : "0");
                                    partdata.BankName = SingleVoucher.NomBankName;
                                    partdata.IFSCCode = SingleVoucher.NomIfscCode;
                                    partdata.MemberPayeeCode = SingleVoucher.MemberPayeeCode;
                                }
                                else
                                {
                                    partdata.ClaimantName = SingleVoucher.Name;
                                    partdata.AccountNo = (!string.IsNullOrEmpty(SingleVoucher.AccountNoS) ? SingleVoucher.AccountNoS : "0");
                                    partdata.BankName = SingleVoucher.BankName;
                                    partdata.IFSCCode = SingleVoucher.IFSCCode;
                                    partdata.MemberPayeeCode = SingleVoucher.MemberPayeeCode;
                                }
                            }

                            partdata.EstablishId = int.Parse(SingleVoucher.EstablishId.ToString());
                            partdata.ObjectCodeText = SingleVoucher.ObjectCodeText;

                            partdata.BillNumber = SingleVoucher.BillNumber;

                            partdata.VoucherAmount = SingleVoucher.AdvanceGrossAmount;
                            partdata.AdvanceBillNumber = SingleVoucher.AdvanceBillNumber;
                            partdata.AdvanceBillGrossAmt = SingleVoucher.AdvanceGrossAmount;
                            result.Add(partdata);
                        }

                    }
                    var query = (from cm in result
                                 group cm by new
                                 {
                                     cm.ClaimantName,
                                     cm.AccountNo,
                                     cm.BankName,
                                     cm.MemberPayeeCode,
                                     cm.IFSCCode,
                                     cm.EstablishId,
                                     cm.ObjectCodeText,
                                     cm.BillNumber,
                                     cm.AdvanceBillNumber,
                                     cm.AdvanceBillGrossAmt
                                 } into g
                                 select
                                 new
                                 {
                                     ClaimantName = g.Key.ClaimantName,
                                     AccountNo = g.Key.AccountNo,
                                     BankName = g.Key.BankName,
                                     IFSCCode = g.Key.IFSCCode,
                                     MemberPayeeCode = g.Key.MemberPayeeCode,
                                     EstablishId = g.Key.EstablishId,
                                     ObjectCodeText = g.Key.ObjectCodeText,
                                     BillNumber = g.Key.BillNumber,
                                     AdvanceBillNumber = g.Key.AdvanceBillNumber,
                                     AdvanceBillGrossAmt = g.Key.AdvanceBillGrossAmt,
                                     VoucherAmount = g.Sum(x => x.VoucherAmount)
                                 }).ToList().OrderBy(m => m.EstablishId);


                    List<AccountDetail> MainResult = new List<AccountDetail>();
                    if (query != null)
                    {
                        foreach (var item in query)
                        {
                            AccountDetail obj = new AccountDetail();
                            obj.ClaimantName = Convert.ToString(item.ClaimantName);
                            obj.AccountNo = item.AccountNo;
                            obj.BankName = Convert.ToString(item.BankName);
                            obj.IFSCCode = Convert.ToString(item.IFSCCode);
                            obj.MemberPayeeCode = item.MemberPayeeCode;
                            obj.EstablishId = Convert.ToInt16(item.EstablishId);
                            obj.ObjectCodeText = Convert.ToString(item.ObjectCodeText);
                            obj.BillNumber = Convert.ToInt16(item.BillNumber);
                            if (obj.AdvanceBillNumber > 0)
                                obj.VoucherAmount = Convert.ToDecimal(item.AdvanceBillGrossAmt);
                            else
                                obj.VoucherAmount = Convert.ToDecimal(item.VoucherAmount);

                            MainResult.Add(obj);
                        }

                    }
                    return MainResult;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        ///   this is for getting bill for Custom on the behlf of encashment date & Bill type -- Valid Authority False
        /// </summary>
        /// <param name="param">EncashmentDate,BillIds</param>
        /// <returns></returns>

        public static object GetAllReimburBillForCustom(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split('-');
                string EncashmentDateSt = Result[0];
                string[] Ids = Result[1].Split(',');


                int[] BillIds = Array.ConvertAll(Ids, s => int.Parse(s));

                DateTime EncashmentDate = new DateTime(Convert.ToInt32(EncashmentDateSt.Split('/')[2]), Convert.ToInt32(EncashmentDateSt.Split('/')[0]), Convert.ToInt32(EncashmentDateSt.Split('/')[1]));

                //DateTime EncashmentDate = new DateTime();

                //DateTime.TryParse(EncashmentDateSt, out EncashmentDate);

                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from Bill in db.mEstablishBill
                                join Bud in db.mBudget on Bill.BudgetId equals Bud.BudgetID
                                where Bill.EncashmentDate == EncashmentDate && (BillIds).Contains(Bill.EstablishBillID) &&
                                Bill.IsValidAuthority == false && Bill.IsReject == false && Bill.IsEncashment == true && Bill.IsSanctioned == true && Bill.IsCanceled == false
                                select new { Bill, Bud }).OrderBy(a => a.Bill.EstablishBillID).ToList();//db.mNews.ToList();

                    List<mEstablishBill> result = new List<mEstablishBill>();

                    foreach (var a in data)
                    {
                        mEstablishBill partdata = new mEstablishBill();

                        partdata = a.Bill;
                        partdata.BudgetName = a.Bud.BudgetName;
                        partdata.FinancialYear = a.Bud.FinancialYear;
                        result.Add(partdata);
                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        #endregion


        #region Establish Bill

        public static object CreateEstablishBill(object param)
        {
            try
            {
                mEstablishBill model = param as mEstablishBill;
                using (BudgetContext db = new BudgetContext())
                {

                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mEstablishBill.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.EstablishBillID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateEstablishBill(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mEstablishBill model = param as mEstablishBill;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mEstablishBill.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteEstablishBill(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mEstablishBill parameter = param as mEstablishBill;
                    mEstablishBill stateToRemove = db.mEstablishBill.SingleOrDefault(a => a.EstablishBillID == parameter.EstablishBillID);
                    db.mEstablishBill.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllEstablishBill(object param)
        {

            try
            {
                mEstablishBill parameter = param as mEstablishBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mEstablishBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mBudget on Bill.BudgetId equals M.BudgetID into M_join
                                from M in M_join.DefaultIfEmpty()
                                where Bill.FinancialYear == parameter.FinancialYear
                                select new
                                {
                                    Bill,
                                    BudgetName = M.BudgetName,
                                    BillTypeName = BT.TypeName
                                }).OrderBy(a => a.Bill.EstablishBillID).ToList();


                    List<mEstablishBill> result = new List<mEstablishBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mEstablishBill partdata = new mEstablishBill();
                            partdata = a.Bill;
                            partdata.BudgetName = a.BudgetName;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetAllEstablishBillForReport(object param)
        {

            try
            {
                mEstablishBill obj = param as mEstablishBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mEstablishBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mBudget on Bill.BudgetId equals M.BudgetID into M_join
                                from M in M_join.DefaultIfEmpty()
                                where Bill.FinancialYear == obj.FinancialYear
                                select new
                                {
                                    Bill,
                                    BudgetName = M.BudgetName,
                                    BillTypeName = BT.TypeName
                                }).OrderBy(a => a.Bill.EstablishBillID).ToList();


                    List<mEstablishBill> result = new List<mEstablishBill>();
                    if (data != null && data.Count() > 0)
                    {
                        foreach (var a in data)
                        {
                            mEstablishBill partdata = new mEstablishBill();
                            partdata = a.Bill;
                            partdata.BudgetName = a.BudgetName;
                            partdata.BillTypeName = a.BillTypeName;
                            result.Add(partdata);
                        }
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public static object GetLastEstablishBill(object param)
        {

            try
            {
                mEstablishBill obj = param as mEstablishBill;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Bill in db.mEstablishBill
                                join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                                join M in db.mBudget on Bill.BudgetId equals M.BudgetID into M_join
                                from M in M_join.DefaultIfEmpty()
                                where Bill.FinancialYear == obj.FinancialYear
                                select new
                                {
                                    Bill,
                                    BudgetName = M.BudgetName,
                                    BillTypeName = BT.TypeName
                                }).OrderByDescending(a => a.Bill.EstablishBillID).FirstOrDefault();


                    mEstablishBill partdata = new mEstablishBill();
                    if (data != null)
                    {
                        partdata = data.Bill;
                        partdata.BudgetName = data.BudgetName;
                        partdata.BillTypeName = data.BillTypeName;
                    }
                    return partdata;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public static object GetEstablishBillById(object param)
        {
            mEstablishBill parameter = param as mEstablishBill;

            BudgetContext db = new BudgetContext();
            if (parameter.EstablishBillID > 0)
            {
                var data = (from Bill in db.mEstablishBill
                            join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                            join M in db.mBudget on Bill.BudgetId equals M.BudgetID into M_join
                            from M in M_join.DefaultIfEmpty()
                            where Bill.EstablishBillID == parameter.EstablishBillID
                            select new
                            {
                                Bill,
                                BudgetName = M.BudgetName,
                                BillTypeName = BT.TypeName
                            }).OrderByDescending(a => a.Bill.EstablishBillID).FirstOrDefault();

                mEstablishBill partdata = new mEstablishBill();
                if (data != null)
                {
                    partdata = data.Bill;
                    partdata.BudgetName = data.BudgetName;
                    partdata.BillTypeName = data.BillTypeName;
                }
                return partdata;
            }
            else
                return db.mEstablishBill.SingleOrDefault(a => a.EstablishBillID == parameter.EstablishBillID);
        }

        public static object GetEstablishBillByBillNumber(object param)
        {
            mEstablishBill parameter = param as mEstablishBill;

            BudgetContext db = new BudgetContext();

            var data = (from Bill in db.mEstablishBill
                        join BT in db.mBudgetBillTypes on Bill.BillType equals BT.BillTypeID
                        join M in db.mBudget on Bill.BudgetId equals M.BudgetID into M_join
                        from M in M_join.DefaultIfEmpty()
                        where Bill.BillNumber == parameter.BillNumber && Bill.FinancialYear == parameter.FinancialYear
                        select new
                        {
                            Bill,
                            BudgetName = M.BudgetName,
                            BillTypeName = BT.TypeName
                        }).OrderByDescending(a => a.Bill.EstablishBillID).FirstOrDefault();

            mEstablishBill partdata = new mEstablishBill();
            if (data != null)
            {
                partdata = data.Bill;
                partdata.BudgetName = data.BudgetName;
                partdata.BillTypeName = data.BillTypeName;
            }
            return partdata;

        }

        public static object UpdateBillId(object param)
        {
            string parameter = param as string;
            string[] parameterArray = parameter.Split('-');

            BudgetContext db = new BudgetContext();
            db.Database.SqlQuery<int>("UpdateBillId @EstablishBillId, @BillIDs", new SqlParameter("EstablishBillId", parameterArray[0]),
   new SqlParameter("BillIDs", parameterArray[1])).ToString();


            var ietsParameter = new SqlParameter("@EstablishBillId", parameterArray[0]);
            var nogIetsParameter = new SqlParameter("@BillIDs", parameterArray[1]);
            db.Database.SqlQuery<int>("UpdateBillId @EstablishBillId, @BillIDs", ietsParameter, nogIetsParameter);

            db.SaveChanges();
            db.Close();

            //SqlParameter idParameter = new SqlParameter("@productId", productId);
            //product = context.Database.SqlQuery<Product>("Product_GetByID @productId", idParameter).FirstOrDefault();
            return null;
        }

        public static object CancelEstablishBill(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mEstablishBill model = param as mEstablishBill;
                    model.CreationDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mEstablishBill.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }


        public static object GetAllBillByEstablishBillId(object param)
        {

            try
            {
                mEstablishBill obj = param as mEstablishBill;

                using (BudgetContext db = new BudgetContext())
                {

                    var data = (from RB in db.mReimbursementBill
                                join BT in db.mBudgetBillTypes on RB.BillType equals BT.BillTypeID
                                join M in db.mMembers on RB.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join S in db.mStaff on RB.ClaimantId equals S.StaffID into S_join
                                from S in S_join.DefaultIfEmpty()
                                join F in db.mOtherFirm on RB.ClaimantId equals F.FirmID into F_join
                                from F in F_join.DefaultIfEmpty()
                                join Nom in db.mMemberNominee on RB.NomineeId equals Nom.NomineeId into N_join
                                from Nom in N_join.DefaultIfEmpty()
                                join SNom in db.mStaffNominee on RB.NomineeId equals SNom.NomineeID into SN_join
                                from SNom in SN_join.DefaultIfEmpty()
                                join MA in db.mMemberAccountsDetails on RB.ClaimantId equals MA.MemberCode into MA_join
                                from MA in MA_join.DefaultIfEmpty()
                                where
                                  RB.EstablishBillId == obj.EstablishBillID
                                orderby
                                  RB.ReimbursementBillID
                                select new
                                {
                                    RB,
                                    StaffName = S.StaffName,
                                    Designation = S.Designation,
                                    MemberName = M.Name,
                                    OtherFirm = F.Name,
                                    Prefix = M.Prefix,
                                    BillTypeName = BT.TypeName,
                                    NomineName = Nom.NomineeName,
                                    StaffNominee = SNom.NomineeName,
                                    StaffPPoNum = S.PPONum,
                                    MemberPPoNum = MA.PPONum
                                }).ToList();



                    List<mReimbursementBill> result = new List<mReimbursementBill>();

                    foreach (var a in data)
                    {
                        mReimbursementBill partdata = new mReimbursementBill();

                        partdata = a.RB;
                        if (a.RB.Designation == 6)
                        {
                            if (!string.IsNullOrEmpty(a.RB.ClaimantName))
                                partdata.ClaimantName = a.RB.ClaimantName;
                            else
                                partdata.ClaimantName = a.OtherFirm;
                        }
                        else if (a.RB.Designation == 4 || a.RB.Designation == 5 || a.RB.Designation == 7)
                        {
                            if (a.RB.NomineeId != null && a.RB.NomineeId > 0)
                                partdata.ClaimantName = a.StaffNominee;
                            else
                                partdata.ClaimantName = a.StaffName;
                            partdata.DesignationName = a.Designation;
                            partdata.PPONum = a.StaffPPoNum;
                        }
                        else
                        {
                            if (a.RB.NomineeId != null && a.RB.NomineeId > 0)
                                partdata.ClaimantName = a.NomineName;
                            else
                            {
                                partdata.ClaimantName = a.Prefix + ". " + a.MemberName;
                                partdata.PPONum = a.MemberPPoNum;
                            }

                        }

                        partdata.BillTypeName = a.BillTypeName;
                        result.Add(partdata);
                    }

                    return result;


                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }




        #endregion

        #region OtherFirm

        public static object CreateOtherFirm(object param)
        {
            try
            {
                mOtherFirm model = param as mOtherFirm;
                using (BudgetContext db = new BudgetContext())
                {


                    db.mOtherFirm.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.FirmID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateOtherFirm(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mOtherFirm model = param as mOtherFirm;
                    db.mOtherFirm.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteOtherFirm(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mOtherFirm parameter = param as mOtherFirm;
                    mOtherFirm stateToRemove = db.mOtherFirm.SingleOrDefault(a => a.FirmID == parameter.FirmID);
                    db.mOtherFirm.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllOtherFirm(object param)
        {

            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Firm in db.mOtherFirm
                                orderby Firm.Name ascending
                                select Firm).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetOtherFirmById(object param)
        {
            mOtherFirm parameter = param as mOtherFirm;

            BudgetContext db = new BudgetContext();
            if (parameter.FirmID > 0)
            {
                var data = (from Firm in db.mOtherFirm
                            where Firm.FirmID == parameter.FirmID
                            select Firm).OrderByDescending(a => a.FirmID).FirstOrDefault();


                return data;
            }
            else
                return db.mOtherFirm.SingleOrDefault(a => a.FirmID == parameter.FirmID);
        }

        #endregion


        #region Rtgs Soft

        public static object CreateRtgsSoft(object param)
        {
            try
            {
                mRtgsSoft model = param as mRtgsSoft;
                using (BudgetContext db = new BudgetContext())
                {


                    db.mRtgsSoft.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.RtgsSoftID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateRtgsSoft(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mRtgsSoft model = param as mRtgsSoft;
                    db.mRtgsSoft.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteRtgsSoft(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mRtgsSoft parameter = param as mRtgsSoft;
                    mRtgsSoft stateToRemove = db.mRtgsSoft.SingleOrDefault(a => a.RtgsSoftID == parameter.RtgsSoftID);
                    db.mRtgsSoft.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllRtgsSoft(object param)
        {

            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from RTGS in db.mRtgsSoft
                                join Firm in db.mOtherFirm on RTGS.FirmId equals Firm.FirmID
                                orderby RTGS.RtgsSoftID
                                select new
                                {
                                    RTGS,
                                    FirmAcNo = Firm.AccNo,
                                    FirmName = Firm.Name,
                                    FirmAdd = Firm.Address,
                                    EmailId = Firm.EmailId,
                                    IFSCCode = Firm.IFSCCode
                                }).ToList();
                    List<mRtgsSoft> Result = new List<mRtgsSoft>();
                    if (data != null)
                    {
                        foreach (var item in data)
                        {
                            mRtgsSoft obj = new mRtgsSoft();
                            obj = item.RTGS;
                            obj.FirmAcNo = item.FirmAcNo;
                            obj.FirmAdd = item.FirmAdd;
                            obj.FirmName = item.FirmName;
                            obj.EmailId = item.EmailId;
                            obj.IFSCCode = item.IFSCCode;
                            Result.Add(obj);
                        }
                    }

                    return Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetRtgsSoftById(object param)
        {
            mRtgsSoft parameter = param as mRtgsSoft;

            BudgetContext db = new BudgetContext();
            var data = (from Firm in db.mRtgsSoft
                        where Firm.RtgsSoftID == parameter.RtgsSoftID
                        select Firm).OrderByDescending(a => a.RtgsSoftID).FirstOrDefault();

            return data;

        }

        public static object GetAllRtgsSoftByBillId(object param)
        {

            try
            {

                mRtgsSoft RtgsSoft = param as mRtgsSoft;

                string[] Result = RtgsSoft.BillIds.Remove(RtgsSoft.BillIds.Length - 1, 1).Split(',');
                int[] BillIds = Array.ConvertAll(Result, s => int.Parse(s));



                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;


                    var data = (from RB in db.mReimbursementBill
                                join EB in db.mEstablishBill on new { EstablishBillId = RB.EstablishBillId } equals new { EstablishBillId = EB.EstablishBillID }
                                join B in db.mBudget on new { BudgetID = EB.BudgetId } equals new { BudgetID = B.BudgetID }
                                join M in db.mMembers on RB.ClaimantId equals M.MemberCode into M_join
                                from M in M_join.DefaultIfEmpty()
                                join S in db.mStaff on RB.ClaimantId equals S.StaffID into S_join
                                from S in S_join.DefaultIfEmpty()
                                join F in db.mOtherFirm on RB.ClaimantId equals F.FirmID into F_join
                                from F in F_join.DefaultIfEmpty()
                                join MA in db.mMemberAccountsDetails on RB.ClaimantId equals MA.MemberCode into MA_join
                                from MA in MA_join.DefaultIfEmpty()
                                join Nom in db.mMemberNominee on RB.NomineeId equals Nom.NomineeId into N_join
                                from Nom in N_join.DefaultIfEmpty()
                                join SNom in db.mStaffNominee on RB.NomineeId equals SNom.NomineeID into SN_join
                                from SNom in SN_join.DefaultIfEmpty()
                                where
                                  (BillIds).Contains(EB.EstablishBillID) && EB.AdvanceBillNo == 0
                                orderby
                               RB.ReimbursementBillID
                                select new
                                {
                                    ClaimantId = RB.ClaimantId,
                                    BillType = EB.BillType,
                                    MemberCode = (int?)M.MemberCode,
                                    M.Name,
                                    EstablishId = (int?)RB.EstablishBillId,
                                    BillNumber = EB.BillNumber,
                                    B.ObjectCodeText,
                                    MA.AccountNo,
                                    MA.BankName,
                                    MA.IFSCCode,
                                    RB.SactionAmount,
                                    RB.ClaimantName,
                                    S.StaffName,
                                    RB.Designation,
                                    OtherFirm = F.Name,
                                    OtherFirmAccNo = F.AccNo,
                                    OtherFirmIfscCode = F.IFSCCode,
                                    OtherFirmBankName = F.BankName,
                                    StaffAccNo = S.BankAccountNo,
                                    StaffIfscCode = S.IFSC,
                                    StaffBankName = S.BankName,
                                    OtherFrimAdd = F.Address,
                                    OtherFrimEmail = F.EmailId,
                                    MemberShimlaAdd = M.ShimlaAddress,
                                    MemberPemanentAdd = M.PermanentAddress,
                                    MemberEmail = M.Email,
                                    RB,
                                    NomineName = Nom.NomineeName,
                                    NomineAcc = Nom.NomineeAccountNo,
                                    NomineBankName = Nom.NomineeBankName,
                                    NomineIFSC = Nom.NomineeIFSCCode,
                                    AdvanceBillNumber = EB.AdvanceBillNo,
                                    AdvanceGrossAmount = EB.TotalGrossAmount,
                                    StaffNominee = SNom.NomineeName,
                                    StaffNomAccNo = SNom.BankAccountNo,
                                    StaffNomIFSC = SNom.IFSC,
                                    StaffNomBankName = SNom.BankName


                                }).ToList();

                    List<AccountDetail> result = new List<AccountDetail>();

                    foreach (var a in data)
                    {
                        AccountDetail partdata = new AccountDetail();
                        partdata.BillType = a.BillType;
                        partdata.ClaimantId = a.ClaimantId;
                        partdata.MemberCode = (a.MemberCode != null ? int.Parse(a.MemberCode.ToString()) : 0);
                        if (a.Designation == 6)
                        {
                            if (!string.IsNullOrEmpty(a.ClaimantName))
                                partdata.ClaimantName = a.ClaimantName;
                            else
                                partdata.ClaimantName = a.OtherFirm;

                            partdata.AccountNo = (a.OtherFirmAccNo != null ? a.OtherFirmAccNo : "0");
                            partdata.BankName = a.OtherFirmBankName;
                            partdata.IFSCCode = a.OtherFirmIfscCode;
                            partdata.EmailId = a.OtherFrimEmail;
                            partdata.Address = a.OtherFrimAdd;

                        }
                        else if (a.Designation == 4 || a.Designation == 5 || a.Designation == 7)
                        {
                            if (a.RB.NomineeId != null && a.RB.NomineeId > 0)
                            {
                                partdata.ClaimantName = a.StaffNominee;
                                partdata.AccountNo = (a.StaffNomAccNo != null ? a.StaffNomAccNo : "0");
                                partdata.BankName = a.StaffNomBankName;
                                partdata.IFSCCode = a.StaffNomIFSC;
                            }
                            else
                            {
                                partdata.ClaimantName = a.StaffName;
                                partdata.AccountNo = (a.StaffAccNo != null ? a.StaffAccNo : "0");
                                partdata.BankName = a.StaffBankName;
                                partdata.IFSCCode = a.StaffIfscCode;
                            }
                            partdata.EmailId = string.Empty;
                            partdata.Address = string.Empty;
                        }
                        else
                        {
                            if (a.RB.NomineeId != null && a.RB.NomineeId > 0)
                            {
                                partdata.ClaimantName = a.NomineName;
                                partdata.AccountNo = (a.NomineAcc != null ? a.NomineAcc.Value.ToString() : "0");
                                partdata.BankName = a.NomineBankName;
                                partdata.IFSCCode = a.NomineIFSC;
                            }
                            else
                            {
                                partdata.ClaimantName = a.Name;
                                partdata.AccountNo = (a.AccountNo != null ? a.AccountNo.Value.ToString() : "0");
                                partdata.BankName = a.BankName;
                                partdata.IFSCCode = a.IFSCCode;
                            }


                            partdata.EmailId = a.MemberEmail;
                            partdata.Address = (!string.IsNullOrEmpty(a.MemberPemanentAdd) ? a.MemberPemanentAdd : a.MemberShimlaAdd); ;
                        }

                        partdata.EstablishId = int.Parse(a.EstablishId.ToString());
                        partdata.ObjectCodeText = a.ObjectCodeText;

                        partdata.BillNumber = a.BillNumber;
                        partdata.VoucherAmount = a.SactionAmount;
                        partdata.AdvanceBillNumber = a.AdvanceBillNumber;
                        partdata.AdvanceBillGrossAmt = a.AdvanceGrossAmount;
                        result.Add(partdata);

                    }

                    var AdvanceBill = (from EBs in db.mEstablishBill
                                       where (BillIds).Contains(EBs.EstablishBillID) && EBs.AdvanceBillNo > 0
                                       select EBs).ToList();
                    foreach (var item in AdvanceBill)
                    {
                        var SingleAdvanceBill = (from RB in db.mReimbursementBill
                                                 join EB in db.mEstablishBill on new { EstablishBillId = RB.EstablishBillId } equals new { EstablishBillId = EB.EstablishBillID }
                                                 join B in db.mBudget on new { BudgetID = EB.BudgetId } equals new { BudgetID = B.BudgetID }
                                                 join M in db.mMembers on RB.ClaimantId equals M.MemberCode into M_join
                                                 from M in M_join.DefaultIfEmpty()
                                                 join S in db.mStaff on RB.ClaimantId equals S.StaffID into S_join
                                                 from S in S_join.DefaultIfEmpty()
                                                 join F in db.mOtherFirm on RB.ClaimantId equals F.FirmID into F_join
                                                 from F in F_join.DefaultIfEmpty()
                                                 join MA in db.mMemberAccountsDetails on RB.ClaimantId equals MA.MemberCode into MA_join
                                                 from MA in MA_join.DefaultIfEmpty()
                                                 join Nom in db.mMemberNominee on RB.NomineeId equals Nom.NomineeId into N_join
                                                 from Nom in N_join.DefaultIfEmpty()
                                                 join SNom in db.mStaffNominee on RB.NomineeId equals SNom.NomineeID into SN_join
                                                 from SNom in SN_join.DefaultIfEmpty()
                                                 where EB.EstablishBillID == item.EstablishBillID
                                                 orderby RB.ReimbursementBillID
                                                 select new
                                                 {
                                                     ClaimantId = RB.ClaimantId,
                                                     BillType = EB.BillType,
                                                     MemberCode = (int?)M.MemberCode,
                                                     M.Name,
                                                     EstablishId = (int?)RB.EstablishBillId,
                                                     BillNumber = EB.BillNumber,
                                                     B.ObjectCodeText,
                                                     MA.AccountNo,
                                                     MA.BankName,
                                                     MA.IFSCCode,
                                                     RB.SactionAmount,
                                                     RB.ClaimantName,
                                                     S.StaffName,
                                                     RB.Designation,
                                                     OtherFirm = F.Name,
                                                     OtherFirmAccNo = F.AccNo,
                                                     OtherFirmIfscCode = F.IFSCCode,
                                                     OtherFirmBankName = F.BankName,
                                                     StaffAccNo = S.BankAccountNo,
                                                     StaffIfscCode = S.IFSC,
                                                     StaffBankName = S.BankName,
                                                     OtherFrimAdd = F.Address,
                                                     OtherFrimEmail = F.EmailId,
                                                     MemberShimlaAdd = M.ShimlaAddress,
                                                     MemberPemanentAdd = M.PermanentAddress,
                                                     MemberEmail = M.Email,
                                                     RB,
                                                     NomineName = Nom.NomineeName,
                                                     NomineAcc = Nom.NomineeAccountNo,
                                                     NomineBankName = Nom.NomineeBankName,
                                                     NomineIFSC = Nom.NomineeIFSCCode,
                                                     AdvanceBillNumber = EB.AdvanceBillNo,
                                                     AdvanceGrossAmount = EB.TotalGrossAmount,

                                                     StaffNominee = SNom.NomineeName,
                                                     StaffNomAccNo = SNom.BankAccountNo,
                                                     StaffNomIFSC = SNom.IFSC,
                                                     StaffNomBankName = SNom.BankName


                                                 }).FirstOrDefault();
                        if (SingleAdvanceBill != null)
                        {
                            AccountDetail partdata = new AccountDetail();
                            partdata.BillType = SingleAdvanceBill.BillType;
                            partdata.ClaimantId = SingleAdvanceBill.ClaimantId;
                            partdata.MemberCode = (SingleAdvanceBill.MemberCode != null ? int.Parse(SingleAdvanceBill.MemberCode.ToString()) : 0);
                            if (SingleAdvanceBill.Designation == 6)
                            {
                                if (!string.IsNullOrEmpty(SingleAdvanceBill.ClaimantName))
                                    partdata.ClaimantName = SingleAdvanceBill.ClaimantName;
                                else
                                    partdata.ClaimantName = SingleAdvanceBill.OtherFirm;

                                partdata.AccountNo = (SingleAdvanceBill.OtherFirmAccNo != null ? SingleAdvanceBill.OtherFirmAccNo : "0");
                                partdata.BankName = SingleAdvanceBill.OtherFirmBankName;
                                partdata.IFSCCode = SingleAdvanceBill.OtherFirmIfscCode;
                                partdata.EmailId = SingleAdvanceBill.OtherFrimEmail;
                                partdata.Address = SingleAdvanceBill.OtherFrimAdd;

                            }
                            else if (SingleAdvanceBill.Designation == 4 || SingleAdvanceBill.Designation == 5 || SingleAdvanceBill.Designation == 7)
                            {
                                if (SingleAdvanceBill.RB.NomineeId != null && SingleAdvanceBill.RB.NomineeId > 0)
                                {
                                    partdata.ClaimantName = SingleAdvanceBill.StaffNominee;
                                    partdata.AccountNo = (SingleAdvanceBill.StaffNomAccNo != null ? SingleAdvanceBill.StaffNomAccNo : "0");
                                    partdata.BankName = SingleAdvanceBill.StaffNomBankName;
                                    partdata.IFSCCode = SingleAdvanceBill.StaffNomIFSC;

                                }
                                else
                                {
                                    partdata.ClaimantName = SingleAdvanceBill.StaffName;
                                    partdata.AccountNo = (SingleAdvanceBill.StaffAccNo != null ? SingleAdvanceBill.StaffAccNo : "0");
                                    partdata.BankName = SingleAdvanceBill.StaffBankName;
                                    partdata.IFSCCode = SingleAdvanceBill.StaffIfscCode;
                                }
                                partdata.EmailId = string.Empty;
                                partdata.Address = string.Empty;
                            }
                            else
                            {
                                if (SingleAdvanceBill.RB.NomineeId != null && SingleAdvanceBill.RB.NomineeId > 0)
                                {
                                    partdata.ClaimantName = SingleAdvanceBill.NomineName;
                                    partdata.AccountNo = (SingleAdvanceBill.NomineAcc != null ? SingleAdvanceBill.NomineAcc.Value.ToString() : "0");
                                    partdata.BankName = SingleAdvanceBill.NomineBankName;
                                    partdata.IFSCCode = SingleAdvanceBill.NomineIFSC;
                                }
                                else
                                {
                                    partdata.ClaimantName = SingleAdvanceBill.Name;
                                    partdata.AccountNo = (SingleAdvanceBill.AccountNo != null ? SingleAdvanceBill.AccountNo.Value.ToString() : "0");
                                    partdata.BankName = SingleAdvanceBill.BankName;
                                    partdata.IFSCCode = SingleAdvanceBill.IFSCCode;
                                }


                                partdata.EmailId = SingleAdvanceBill.MemberEmail;
                                partdata.Address = (!string.IsNullOrEmpty(SingleAdvanceBill.MemberPemanentAdd) ? SingleAdvanceBill.MemberPemanentAdd : SingleAdvanceBill.MemberShimlaAdd); ;
                            }

                            partdata.EstablishId = int.Parse(SingleAdvanceBill.EstablishId.ToString());
                            partdata.ObjectCodeText = SingleAdvanceBill.ObjectCodeText;

                            partdata.BillNumber = SingleAdvanceBill.BillNumber;
                            partdata.VoucherAmount = SingleAdvanceBill.AdvanceGrossAmount;
                            partdata.AdvanceBillNumber = SingleAdvanceBill.AdvanceBillNumber;
                            partdata.AdvanceBillGrossAmt = SingleAdvanceBill.AdvanceGrossAmount;
                            result.Add(partdata);
                        }
                    }
                    var query = (from cm in result
                                 group cm by new
                                 {
                                     cm.ClaimantId,
                                     cm.ClaimantName,
                                     cm.AccountNo,
                                     cm.BankName,
                                     cm.IFSCCode,
                                     cm.EstablishId,
                                     cm.ObjectCodeText,
                                     cm.BillNumber,
                                     cm.Address,
                                     cm.EmailId,
                                     cm.BillType,
                                     cm.AdvanceBillNumber,
                                     cm.AdvanceBillGrossAmt
                                 } into g
                                 select
                                 new
                                 {
                                     ClaimantId = g.Key.ClaimantId,
                                     ClaimantName = g.Key.ClaimantName,
                                     AccountNo = g.Key.AccountNo,
                                     BankName = g.Key.BankName,
                                     IFSCCode = g.Key.IFSCCode,
                                     EstablishId = g.Key.EstablishId,
                                     ObjectCodeText = g.Key.ObjectCodeText,
                                     BillNumber = g.Key.BillNumber,
                                     EmailId = g.Key.EmailId,
                                     Address = g.Key.Address,
                                     BillType = g.Key.BillType,
                                     AdvanceBillNumber = g.Key.AdvanceBillNumber,
                                     AdvanceBillGrossAmt = g.Key.AdvanceBillGrossAmt,
                                     VoucherAmount = g.Sum(x => x.VoucherAmount)
                                 }).ToList().OrderBy(m => m.EstablishId);


                    List<mRtgsSoft> MainResult = new List<mRtgsSoft>();
                    if (query != null)
                    {
                        foreach (var item in query)
                        {

                            mRtgsSoft obj = new mRtgsSoft();
                            obj.FirmName = item.ClaimantName;
                            obj.ClaimantId = item.ClaimantId;
                            obj.FirmAcNo = Convert.ToString(item.AccountNo);
                            obj.BankName = item.BankName;
                            obj.IFSCCode = item.IFSCCode;
                            obj.FirmAdd = item.Address;
                            obj.EmailId = item.EmailId;
                            obj.BillType = Convert.ToInt16(item.BillType);
                            obj.ComissionAmt = 0;
                            if (item.AdvanceBillNumber > 0)
                                obj.TransferAmt = Convert.ToDecimal(item.AdvanceBillGrossAmt);
                            else
                                obj.TransferAmt = Convert.ToDecimal(item.VoucherAmount);

                            // obj.TransferAmt = Convert.ToDecimal(item.VoucherAmount);
                            obj.RemAcNo = RtgsSoft.RemAcNo;
                            obj.RemAdd = RtgsSoft.RemAdd;
                            obj.RemName = RtgsSoft.RemName;
                            obj.EmpEmailId = RtgsSoft.EmpEmailId;
                            obj.EstablishId = item.EstablishId;
                            MainResult.Add(obj);

                        }

                    }
                    return MainResult;
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region Medicine
        public static object CreateMedicine(object param)
        {
            try
            {
                mMedicine model = param as mMedicine;
                using (BudgetContext db = new BudgetContext())
                {
                    db.mMedicine.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.MedicineID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateMedicine(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mMedicine model = param as mMedicine;
                    db.mMedicine.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteMedicine(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mMedicine parameter = param as mMedicine;
                    mMedicine stateToRemove = db.mMedicine.SingleOrDefault(a => a.MedicineID == parameter.MedicineID);
                    db.mMedicine.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllMedicine(object param)
        {

            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mMedicine
                                orderby Medi.MedicineName
                                select Medi).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetMedicineById(object param)
        {
            mMedicine parameter = param as mMedicine;

            BudgetContext db = new BudgetContext();

            var data = (from Medi in db.mMedicine
                        where Medi.MedicineID == parameter.MedicineID
                        select Medi).FirstOrDefault();

            return data;

        }
        public static object GetMedicineExist(object param)
        {

            try
            {
                mMedicine parameter = param as mMedicine;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mMedicine
                                orderby Medi.MedicineID
                                where Medi.MeidicineType == parameter.MeidicineType && Medi.MedicineName == parameter.MedicineName
                                select Medi).ToList().Count();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object SearchMedicine(object param)
        {

            try
            {
                string SearchWord = param as string;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mMedicine
                                orderby Medi.MedicineID descending
                                where Medi.MedicineName.Contains(SearchWord)
                                select Medi).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


        #region Approved Hospital
        public static object CreateApprovedHospital(object param)
        {
            try
            {
                mApprovedHospital model = param as mApprovedHospital;
                using (BudgetContext db = new BudgetContext())
                {
                    db.mApprovedHospital.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.HospitalID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateApprovedHospital(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mApprovedHospital model = param as mApprovedHospital;
                    db.mApprovedHospital.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteApprovedHospital(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mApprovedHospital parameter = param as mApprovedHospital;
                    mApprovedHospital stateToRemove = db.mApprovedHospital.SingleOrDefault(a => a.HospitalID == parameter.HospitalID);
                    db.mApprovedHospital.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllApprovedHospital(object param)
        {

            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mApprovedHospital
                                orderby Medi.HospitalName
                                select Medi).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetApprovedHospitalById(object param)
        {
            mApprovedHospital parameter = param as mApprovedHospital;

            BudgetContext db = new BudgetContext();

            var data = (from Medi in db.mApprovedHospital
                        where Medi.HospitalID == parameter.HospitalID
                        select Medi).FirstOrDefault();

            return data;

        }
        public static object GetApprovedHospitalExist(object param)
        {

            try
            {
                mApprovedHospital parameter = param as mApprovedHospital;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mApprovedHospital
                                orderby Medi.HospitalID
                                where Medi.HospitalID == parameter.HospitalID
                                select Medi).ToList().Count();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object SearchApprovedHospital(object param)
        {

            try
            {
                string SearchWord = param as string;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mApprovedHospital
                                orderby Medi.HospitalID descending
                                where Medi.HospitalName.Contains(SearchWord)
                                select Medi).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Medical Test Charges

        public static object CreateMedicalTestCharge(object param)
        {
            try
            {
                mMedicalTestCharge model = param as mMedicalTestCharge;
                using (BudgetContext db = new BudgetContext())
                {
                    db.mMedicalTestCharge.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.TestChargeID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateMedicalTestCharge(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mMedicalTestCharge model = param as mMedicalTestCharge;
                    db.mMedicalTestCharge.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteMedicalTestCharge(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mMedicalTestCharge parameter = param as mMedicalTestCharge;
                    mMedicalTestCharge stateToRemove = db.mMedicalTestCharge.SingleOrDefault(a => a.TestChargeID == parameter.TestChargeID);
                    db.mMedicalTestCharge.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllMedicalTestCharge(object param)
        {

            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mMedicalTestCharge
                                orderby Medi.TestChargeID descending
                                select Medi).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetMedicalTestChargeById(object param)
        {
            mMedicalTestCharge parameter = param as mMedicalTestCharge;

            BudgetContext db = new BudgetContext();

            var data = (from Medi in db.mMedicalTestCharge
                        where Medi.TestChargeID == parameter.TestChargeID
                        select Medi).FirstOrDefault();

            return data;

        }
        public static object GetMedicalTestChargeExist(object param)
        {

            try
            {
                mMedicalTestCharge parameter = param as mMedicalTestCharge;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mMedicalTestCharge
                                orderby Medi.TestChargeID
                                where Medi.TestName == parameter.TestName
                                select Medi).ToList().Count();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object SearchMedicalTestCharge(object param)
        {

            try
            {
                string SearchWord = param as string;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from Medi in db.mMedicalTestCharge
                                orderby Medi.TestChargeID descending
                                where Medi.TestName.Contains(SearchWord)
                                select Medi).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion


        #region Budget Type

        public static object CreateBudgetType(object param)
        {
            try
            {
                mBudgetType model = param as mBudgetType;
                using (BudgetContext db = new BudgetContext())
                {
                    db.mBudgetType.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.BudgetTypeID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateBudgetType(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mBudgetType model = param as mBudgetType;
                    db.mBudgetType.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteBudgetType(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mBudgetType parameter = param as mBudgetType;
                    mBudgetType stateToRemove = db.mBudgetType.SingleOrDefault(a => a.BudgetTypeID == parameter.BudgetTypeID);
                    db.mBudgetType.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllBudgetType(object param)
        {

            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from BT in db.mBudgetType
                                orderby BT.TypeName
                                where BT.IsActive == true
                                select BT).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetBudgetTypeById(object param)
        {
            mBudgetType parameter = param as mBudgetType;

            BudgetContext db = new BudgetContext();

            var data = (from BT in db.mBudgetType
                        where BT.BudgetTypeID == parameter.BudgetTypeID
                        select BT).FirstOrDefault();

            return data;

        }

        #endregion

        #region Bill Type

        public static object CreateBillType(object param)
        {
            try
            {
                mBudgetBillTypes model = param as mBudgetBillTypes;
                using (BudgetContext db = new BudgetContext())
                {
                    db.mBudgetBillTypes.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.BillTypeID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateBillType(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mBudgetBillTypes model = param as mBudgetBillTypes;
                    db.mBudgetBillTypes.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteBillType(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    mBudgetBillTypes parameter = param as mBudgetBillTypes;
                    mBudgetBillTypes stateToRemove = db.mBudgetBillTypes.SingleOrDefault(a => a.BillTypeID == parameter.BillTypeID);
                    db.mBudgetBillTypes.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllBillType(object param)
        {

            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from BT in db.mBudgetBillTypes
                                orderby BT.TypeName
                                where BT.IsActive == true
                                select BT).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetBillTypeById(object param)
        {
            mBudgetBillTypes parameter = param as mBudgetBillTypes;

            BudgetContext db = new BudgetContext();

            var data = (from BT in db.mBudgetBillTypes
                        where BT.BillTypeID == parameter.BillTypeID
                        select BT).FirstOrDefault();

            return data;

        }

        #endregion


        #region Budget Head Mapped with Designation

        public static object AddBudgetMappedwithDesignation(object param)
        {
            try
            {
                BudgetMapWithDesig model = param as BudgetMapWithDesig;
                using (BudgetContext db = new BudgetContext())
                {
                    db.BudgetMapWithDesig.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.MappedID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object UpdateBudgetMappedwithDesignation(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    BudgetMapWithDesig model = param as BudgetMapWithDesig;
                    db.BudgetMapWithDesig.Attach(model);
                    db.Entry(model).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object DeleteBudgetMappedwithDesignation(object param)
        {
            try
            {
                using (BudgetContext db = new BudgetContext())
                {
                    BudgetMapWithDesig parameter = param as BudgetMapWithDesig;
                    BudgetMapWithDesig stateToRemove = db.BudgetMapWithDesig.SingleOrDefault(a => a.MappedID == parameter.MappedID);
                    db.BudgetMapWithDesig.Remove(stateToRemove);
                    db.SaveChanges();
                    db.Close();
                }

                return null;
            }
            catch
            {
                throw;
            }
        }

        public static object GetAllBudgetMapWithDesig(object param)
        {

            try
            {
                string FiancialYear = param as string;
                using (BudgetContext db = new BudgetContext())
                {
                    var data = (from BT in db.BudgetMapWithDesig
                                join AB in db.mBudget on BT.BudgetId equals AB.BudgetID
                                orderby BT.MappedID descending
                                where BT.FinancialYear == FiancialYear
                                select new { BT, BName = AB.BudgetName }).ToList();
                    List<BudgetMapWithDesig> Result = new List<BudgetMapWithDesig>();
                    foreach (var item in data)
                    {
                        BudgetMapWithDesig obj = new BudgetMapWithDesig();
                        obj = item.BT;
                        obj.BudgetName = item.BName;
                        Result.Add(obj);
                    }
                    return Result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static object GetBudgetMapWithDesigById(object param)
        {
            BudgetMapWithDesig parameter = param as BudgetMapWithDesig;

            BudgetContext db = new BudgetContext();

            var data = (from BT in db.BudgetMapWithDesig
                        where BT.MappedID == parameter.MappedID
                        select BT).FirstOrDefault();

            return data;

        }

        #endregion
        #region Speaker Income Tax

        public static object GetSpeakerIncomeTaxById(object param)
        {
            mSpeakerIncomeTax parameter = param as mSpeakerIncomeTax;
            mSpeakerIncomeTax result = new mSpeakerIncomeTax();
            BudgetContext db = new BudgetContext();
            var data = (from MIT in db.mSpeakerIncomeTax
                        join MEM in db.mMembers on MIT.EmpId equals MEM.MemberCode
                        //   join Des in db.mMemberDesignations on MIT.EmpId equals Des.memDesigcode
                        where MIT.IncomeTaxID == parameter.IncomeTaxID
                        select new { MIT, Prefix = MEM.Prefix, EmpName = MEM.Name }).FirstOrDefault();

            var assemblyID = (from assembly in db.mMemberAssembly
                              join mAssemblyOrder in db.mAssembly
                              on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                              where assembly.MemberID == data.MIT.EmpId
                              orderby mAssemblyOrder.AssemblyID descending
                              select assembly.AssemblyID
                                         ).FirstOrDefault();
            var Designation = (from memberAssemblies in db.mMemberAssembly
                               join Design in db.mMemberDesignations on memberAssemblies.DesignationID equals Design.memDesigcode
                               where memberAssemblies.AssemblyID == assemblyID
                               && memberAssemblies.MemberID == data.MIT.EmpId
                               select Design.memDesigname
                                    ).FirstOrDefault();


            if (data != null)
            {
                result = data.MIT;
                result.EmpName = data.Prefix + " " + data.EmpName;
                result.Designation = Designation;
            }
            return result;

        }
        public static object GetSalaryDetailIncomeTaxById(object param)
        {
            mSalaryTypeDetailForTax parameter = param as mSalaryTypeDetailForTax;

            BudgetContext db = new BudgetContext();

            var data = (from MIT in db.mSalaryTypeDetailForTax
                        where MIT.IncomeTaxId == parameter.IncomeTaxId
                        select MIT).ToList();

            return data;

        }
        public static object GetAllMembers()
        {
            using (BudgetContext context = new BudgetContext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);


                List<int> ministers = new List<int>();

                var mid = (from minister in context.mMinsitryMinister
                           where minister.AssemblyID == aid
                           select minister.MemberCode).ToList();
                ministers = mid;

                var query = (from e in context.mMembers
                             join massembly in context.mMemberAssembly
                                   on e.MemberCode equals massembly.MemberID
                             where !(mid.Contains(e.MemberCode))
                             &&
                             massembly.AssemblyID == aid && e.Active == true

                             orderby e.Name
                             select e).ToList();
                return query;
            }
        }

        public static object GetAllowance(object param)
        {
            int MemberId = (int)param;

            using (BudgetContext context = new BudgetContext())
            {
                int memberID = Convert.ToInt32(param);

                var assemblyID = (from assembly in context.mMemberAssembly
                                  join mAssemblyOrder in context.mAssembly
                                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                  where assembly.MemberID == memberID
                                  orderby mAssemblyOrder.AssemblyID descending
                                  select assembly.AssemblyID
                                          ).FirstOrDefault();
                var Designation = (from memberAssemblies in context.mMemberAssembly
                                   join Design in context.mMemberDesignations on memberAssemblies.DesignationID equals Design.memDesigcode
                                   where memberAssemblies.AssemblyID == assemblyID
                                   && memberAssemblies.MemberID == memberID
                                   select Design.memDesigname
                                        ).FirstOrDefault();
                int CatId = 3;
                if (Designation == "Speaker")
                    CatId = 1;
                else if (Designation == "Deputy Speaker")
                    CatId = 2;


                int[] HeadIds = new int[] { 1, 2, 3, 4, 5 };

                var MembersAllowance = (from MH in context.membersHead
                                        join SH in context.salaryheads
                                  on MH.sHeadId equals SH.sHeadId
                                        where (HeadIds).Contains(SH.sHeadId) && MH.membersCatId == CatId
                                        orderby SH.sHeadId
                                        select new { MH.amount, SH.sHeadName }
                                         ).ToList();
                List<mSpeakerIncomeTax> AllowanceList = new List<mSpeakerIncomeTax>();
                if (MembersAllowance != null && MembersAllowance.Count() > 0)
                {
                    foreach (var item in MembersAllowance)
                    {
                        mSpeakerIncomeTax obj = new mSpeakerIncomeTax();
                        obj.AllowanceAmount = item.amount.Value;
                        obj.HeadName = item.sHeadName;
                        AllowanceList.Add(obj);
                    }
                }

                return AllowanceList;
            }
        }
        public static object SaveSpeakerIncomeTax(object param)
        {
            try
            {
                mSpeakerIncomeTax model = param as mSpeakerIncomeTax;
                using (BudgetContext db = new BudgetContext())
                {

                    model.CreationDate = DateTime.Now;
                    db.mSpeakerIncomeTax.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.IncomeTaxID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object SaveSalryTypeDetail(object param)
        {
            try
            {
                mSalaryTypeDetailForTax model = param as mSalaryTypeDetailForTax;
                using (BudgetContext db = new BudgetContext())
                {


                    db.mSalaryTypeDetailForTax.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.TypeID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public static object GetAllSpeakerIncomeTax(object param)
        {
            mSpeakerIncomeTax parameter = param as mSpeakerIncomeTax;
            List<mSpeakerIncomeTax> result = new List<mSpeakerIncomeTax>();
            BudgetContext db = new BudgetContext();
            var data = (from MIT in db.mSpeakerIncomeTax
                        join MEM in db.mMembers on MIT.EmpId equals MEM.MemberCode
                        //  join Des in db.mMemberDesignations on MIT.EmpId equals Des.memDesigcode
                        //  where MIT.IncomeTaxID == parameter.IncomeTaxID
                        select new { MIT, Prefix = MEM.Prefix, EmpName = MEM.Name }).ToList();




            if (data != null && data.Count() > 0)
            {

                foreach (var item in data)
                {
                    var assemblyID = (from assembly in db.mMemberAssembly
                                      join mAssemblyOrder in db.mAssembly
                                      on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                                      where assembly.MemberID == item.MIT.EmpId
                                      orderby mAssemblyOrder.AssemblyID descending
                                      select assembly.AssemblyID
                                        ).FirstOrDefault();
                    var Designation = (from memberAssemblies in db.mMemberAssembly
                                       join Design in db.mMemberDesignations on memberAssemblies.DesignationID equals Design.memDesigcode
                                       where memberAssemblies.AssemblyID == assemblyID
                                       && memberAssemblies.MemberID == item.MIT.EmpId
                                       select Design.memDesigname
                                            ).FirstOrDefault();
                    mSpeakerIncomeTax obj = new mSpeakerIncomeTax();
                    obj = item.MIT;
                    obj.EmpName = item.Prefix + " " + item.EmpName;
                    obj.Designation = Designation;
                    result.Add(obj);
                }


            }
            return result;

        }
        #endregion


        #region Member Income Tax

        public static object GetMemberIncomeTaxById(object param)
        {
            mMemberIncomeTax parameter = param as mMemberIncomeTax;
            mMemberIncomeTax result = new mMemberIncomeTax();
            BudgetContext db = new BudgetContext();
            var data = (from MIT in db.mMemberIncomeTax
                        join MEM in db.mMembers on MIT.EmpId equals MEM.MemberCode
                        //   join Des in db.mMemberDesignations on MIT.EmpId equals Des.memDesigcode
                        where MIT.MIncomeTaxID == parameter.MIncomeTaxID
                        select new { MIT, Prefix = MEM.Prefix, EmpName = MEM.Name }).FirstOrDefault();

            var assemblyID = (from assembly in db.mMemberAssembly
                              join mAssemblyOrder in db.mAssembly
                              on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                              where assembly.MemberID == data.MIT.EmpId
                              orderby mAssemblyOrder.AssemblyID descending
                              select assembly.AssemblyID
                                         ).FirstOrDefault();
            var Designation = (from memberAssemblies in db.mMemberAssembly
                               join Design in db.mMemberDesignations on memberAssemblies.DesignationID equals Design.memDesigcode
                               where memberAssemblies.AssemblyID == assemblyID
                               && memberAssemblies.MemberID == data.MIT.EmpId
                               select Design.memDesigname
                                    ).FirstOrDefault();


            if (data != null)
            {
                result = data.MIT;
                result.EmpName = data.Prefix + " " + data.EmpName;
                result.Designation = Designation;
            }
            return result;

        }

        public static object GetAllMembersOnly(object param)
        {
            using (BudgetContext context = new BudgetContext())
            {
                var q = (from assemblyid in context.mSiteSettings
                         where assemblyid.SettingName == "Assembly"
                         select assemblyid.SettingValue).FirstOrDefault();
                int aid = Convert.ToInt32(q);


                List<int> ministers = new List<int>();

                var mid = (from minister in context.mMinsitryMinister
                           where minister.AssemblyID == aid
                           select minister.MemberCode).ToList();
                ministers = mid;

                var query = (from e in context.mMembers
                             join massembly in context.mMemberAssembly
                                   on e.MemberCode equals massembly.MemberID
                             where !(mid.Contains(e.MemberCode))
                             &&
                             massembly.AssemblyID == aid && e.Active == true

                             orderby e.Name
                             select e).ToList();
                return query;
            }
        }


        public static object SaveMemberIncomeTax(object param)
        {
            try
            {
                mMemberIncomeTax model = param as mMemberIncomeTax;
                using (BudgetContext db = new BudgetContext())
                {

                    model.CreationDate = DateTime.Now;
                    db.mMemberIncomeTax.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

                return model.MIncomeTaxID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }


        public static object GetMemberAllowance(object param)
        {
            // int MemberId = (int)param;

            using (BudgetContext context = new BudgetContext())
            {
                //  int memberID = Convert.ToInt32(param);

                //var assemblyID = (from assembly in context.mMemberAssembly
                //                  join mAssemblyOrder in context.mAssembly
                //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                //                  where assembly.MemberID == memberID
                //                  orderby mAssemblyOrder.AssemblyID descending
                //                  select assembly.AssemblyID
                //                          ).FirstOrDefault();
                //var Designation = (from memberAssemblies in context.mMemberAssembly
                //                   join Design in context.mMemberDesignations on memberAssemblies.DesignationID equals Design.memDesigcode
                //                   where memberAssemblies.AssemblyID == assemblyID
                //                   && memberAssemblies.MemberID == memberID
                //                   select Design.memDesigname
                //                        ).FirstOrDefault();
                int CatId = 3;





                var MembersAllowance = (from MH in context.membersHead
                                        join SH in context.salaryheads
                                  on MH.sHeadId equals SH.sHeadId
                                        where MH.membersCatId == CatId && MH.htype == "cr" && MH.status == true
                                        orderby SH.sHeadId
                                        select new { MH.amount, SH.sHeadName }
                                         ).ToList();
                List<mSpeakerIncomeTax> AllowanceList = new List<mSpeakerIncomeTax>();
                if (MembersAllowance != null && MembersAllowance.Count() > 0)
                {
                    foreach (var item in MembersAllowance)
                    {
                        mSpeakerIncomeTax obj = new mSpeakerIncomeTax();
                        obj.AllowanceAmount = item.amount.Value;
                        obj.HeadName = item.sHeadName;
                        AllowanceList.Add(obj);
                    }
                }

                return AllowanceList;
            }
        }
        public static object GetAllMemberIncomeTax(object param)
        {
            mMemberIncomeTax parameter = param as mMemberIncomeTax;
            List<mMemberIncomeTax> result = new List<mMemberIncomeTax>();
            BudgetContext db = new BudgetContext();
            var data = (from MIT in db.mMemberIncomeTax
                        join MEM in db.mMembers on MIT.EmpId equals MEM.MemberCode
                        join MAD in db.mMemberAccountsDetails on MIT.EmpId equals MAD.MemberCode
                        where MIT.FinancialYear == parameter.FinancialYear && MIT.Quarter == parameter.Quarter
                        select new { MIT, Prefix = MEM.Prefix, EmpName = MEM.Name, PANNumber = MAD.PanNumber }).ToList();




            if (data != null && data.Count() > 0)
            {

                foreach (var item in data)
                {
                    //var assemblyID = (from assembly in db.mMemberAssembly
                    //                  join mAssemblyOrder in db.mAssembly
                    //                  on assembly.AssemblyID equals mAssemblyOrder.AssemblyCode
                    //                  where assembly.MemberID == item.MIT.EmpId
                    //                  orderby mAssemblyOrder.AssemblyID descending
                    //                  select assembly.AssemblyID
                    //                    ).FirstOrDefault();
                    //var Designation = (from memberAssemblies in db.mMemberAssembly
                    //                   join Design in db.mMemberDesignations on memberAssemblies.DesignationID equals Design.memDesigcode
                    //                   where memberAssemblies.AssemblyID == assemblyID
                    //                   && memberAssemblies.MemberID == item.MIT.EmpId
                    //                   select Design.memDesigname
                    //                        ).FirstOrDefault();
                    mMemberIncomeTax obj = new mMemberIncomeTax();
                    obj = item.MIT;
                    obj.EmpName = item.Prefix + " " + item.EmpName;
                    obj.PanNumber = item.PANNumber;
                    // obj.Designation = Designation;
                    result.Add(obj);
                }


            }
            return result;

        }
        #endregion


        #region SalaryVoucherNumbers
        public static object GetSalaryVoucherNumbersFinYear(object param)
        {
            try
            {
                string obj = param as string;
                BudgetContext db = new BudgetContext();
                var data = (from BU in db.mSalaryVoucherNumbers
                            where BU.FinancialYear == obj
                            select BU).ToList();
                return data;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object GetSalaryVoucherNumbersFinYearAndMonth(object param)
        {
            try
            {
                SalaryVoucherNumbers objSalaryVoucherNumbers = param as SalaryVoucherNumbers;
                using (BudgetContext db = new BudgetContext())
                {
                    var obj = (from data in db.mSalaryVoucherNumbers
                               where data.FinancialYear == objSalaryVoucherNumbers.FinancialYear && data.Months == objSalaryVoucherNumbers.Months
                               select data).FirstOrDefault();
                    return obj;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public static object SaveSalaryVoucherNumbers(object param)
        {
            try
            {
                SalaryVoucherNumbers objSalaryVoucherNumbers = param as SalaryVoucherNumbers;

                using (BudgetContext db = new BudgetContext())
                {
                    //SalaryVoucherNumbers obj = db.mSalaryVoucherNumbers.Single(m => m.FinancialYear == objSalaryVoucherNumbers.FinancialYear && m.Months==objSalaryVoucherNumbers.Months);
                    var obj = (from data in db.mSalaryVoucherNumbers where data.FinancialYear == objSalaryVoucherNumbers.FinancialYear && data.Months == objSalaryVoucherNumbers.Months select data).SingleOrDefault();
                    if (obj != null)
                    {
                        obj.FinancialYear = objSalaryVoucherNumbers.FinancialYear;
                        obj.Months = objSalaryVoucherNumbers.Months;
                        obj.MonthId = objSalaryVoucherNumbers.MonthId;
                        obj.ClassI = objSalaryVoucherNumbers.ClassI;
                        obj.ClassII = objSalaryVoucherNumbers.ClassII;
                        obj.ClassIII = objSalaryVoucherNumbers.ClassIII;
                        obj.ClassIV = objSalaryVoucherNumbers.ClassIV;
                        obj.ClassIV_CPF = objSalaryVoucherNumbers.ClassIV_CPF;
                        obj.ClassIII_CPF = objSalaryVoucherNumbers.ClassIII_CPF;
                        db.SaveChanges();
                    }
                    else
                    {
                        db.mSalaryVoucherNumbers.Add(objSalaryVoucherNumbers);
                        db.SaveChanges();
                        db.Close();
                    }
                }

                return objSalaryVoucherNumbers.SalaryVoucherNumbersID;
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        #endregion


        public static object GetEstablishBillIdByBillNo(object param)
        {

            try
            {
                string Data = param as string;
                string[] Result = Data.Split(',');
                int BillNo = Convert.ToInt32(Result[0]);
                int MonthId = Convert.ToInt32(Result[1]);
                int YearId = Convert.ToInt32(Result[2]);
                int IsPart = Convert.ToInt32(Result[3]);
                int Assemblyid = Convert.ToInt32(Result[4]);
                using (BudgetContext db = new BudgetContext())
                {
                    //Organization Organization = param as Organization;
                    var data = (from Bill in db.mEstablishBill
                                where Bill.BillNumber == BillNo
                                && Bill.sMOnthID == MonthId
                                && Bill.sYearID == YearId
                                && Bill.ispart == IsPart
                                && Bill.AssemblyId == Assemblyid
                                select Bill);
                    return data.ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
