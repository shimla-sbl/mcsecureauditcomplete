namespace SBL.Domain.Migrations
{
    using SBL.DomainModel.Models.ConstituencyVS;
    using SBL.DomainModel.Models.Question;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DBManager>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true; 

        }

        protected override void Seed(SBL.Domain.DBManager context)
        {
            //context.Database.ExecuteSqlCommand("CREATE INDEX IX_tQuestions_SessionID ON tQuestions (AssemblyID,SessionID) ");
            //base.Seed(context);
            //List<mworkStatus> list = new List<mworkStatus>();
            //list.Add(new mworkStatus() { workStatusID = 1, workStatus = "Not started" });
            //list.Add(new mworkStatus() { workStatusID = 2, workStatus = "In progress" });
            //list.Add(new mworkStatus() { workStatusID = 3, workStatus = "Disrupted" });
            //list.Add(new mworkStatus() { workStatusID = 4, workStatus = "Completed" });

            //foreach (mworkStatus item in list)
            //{
            //    context.mworkStatus.Add(item);
            //}

            //Seeding work Nature Data
            //List<workNature> mworkNaturelist = new List<workNature>();
            //mworkNaturelist.Add(new workNature() { WorkNatureCode = 1, WorkNatureName = "New" });
            //mworkNaturelist.Add(new workNature() { WorkNatureCode = 2, WorkNatureName = "On-going" });
            //mworkNaturelist.Add(new workNature() { WorkNatureCode = 3, WorkNatureName = "Original" });
            //mworkNaturelist.Add(new workNature() { WorkNatureCode = 4, WorkNatureName = "Special Repair" });
            //mworkNaturelist.Add(new workNature() { WorkNatureCode = 5, WorkNatureName = "Reconstruction/Remodelling" });
            //mworkNaturelist.Add(new workNature() { WorkNatureCode = 6, WorkNatureName = "Upgradation" });
            //foreach (workNature item in mworkNaturelist)
            //{
            //    context.workNature.Add(item);
            //}
            // base.Seed(context);

        }
    }
}
