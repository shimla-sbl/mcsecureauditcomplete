﻿using SBL.DAL;
using SBL.DomainModel.Models.Role;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.Service.Common;
using System.Data;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.ActionButtons;

namespace SBL.Domain.Context.UserManagement
{
    public class RoleContext : DBBase<RoleContext>
    {


        public RoleContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
        //  public virtual DbSet<mRolesModel> mRolesModel { get; set; }

        public virtual DbSet<mRoles> mRoles { get; set; }
        public virtual DbSet<tUserRoles> tUserRoles { get; set; }
        public virtual DbSet<mUserType> mUserType { get; set; }
        public virtual DbSet<mSubUserType> mSubUserType { get; set; }
        public virtual DbSet<mSubUserTypeRoles> mSubUserTypeRoles { get; set; }
        public virtual DbSet<ActionButtonControl> ActionButtonControl { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {

                case "GetUserRole":
                    {
                        return GetUserRole();
                    }

                case "CreateRole": { CreateRole(param.Parameter); break; }


                case "GetRoleDataById":
                    {
                        return GetRoleDataById(param.Parameter);
                    }

                case "DeleteRoles":
                    {
                        return DeleteRoles(param.Parameter);
                    }
                case "UpdateEntryModules":
                    {
                        return UpdateEntryModules(param.Parameter);
                    }
                case "CheckRoleIDExist":
                    {
                        return CheckRoleIDExist(param.Parameter);
                    }

                case "CheckUserTypeExist":
                    {
                        return CheckUserTypeExist(param.Parameter);
                    }

                case "CheckUserRoleExist":
                    {
                        return CheckUserRoleExist(param.Parameter);
                    }

                case "CheckSubUserTypeExist":
                    {
                        return CheckSubUserTypeExist(param.Parameter);
                    }

                case "GetRoleDropDown":
                    {
                        return GetRoleDropDown(param.Parameter);
                    }
                case "CreateSubUserRole":
                    {
                        CreateSubUserRole(param.Parameter); break;
                    }
                case "CheckSubUserRoleExist":
                    {
                        return CheckSubUserRoleExist(param.Parameter);
                    }
                case "UpdateSubUserRole":
                    {
                        UpdateSubUserRole(param.Parameter); break;
                    }

                case "MapRollToUsers":
                    {
                        MapRollToUsers(param.Parameter); break; ;
                    }

                case "RemoveUserRole":
                    {
                        RemoveUserRole(param.Parameter); break; ;
                    }
                case "CheckSubUserRoleExistBySubUserID":
                    {
                        return CheckSubUserRoleExistBySubUserID(param.Parameter);
                    }

                case "GetAllSubUserTypeRoles":
                    {
                        return GetAllSubUserTypeRoles();
                    }

                case "GetSubUserTypeRoleById":
                    {
                        return GetSubUserTypeRoleById(param.Parameter);
                    }
                case "CheckUserRoleExistByUserID":
                    {
                        return CheckUserRoleExistByUserID(param.Parameter);
                    }
                case "GetUserTypeRoleList":
                    {
                        return GetUserTypeRoleList(param.Parameter);
                    }
                case "GetUserRoleByUserID":
                    {
                        return GetUserRoleByUserID(param.Parameter);
                    }
                case "GetUserTypeRoleListByMultipleID":
                    {
                        return GetUserTypeRoleListByMultipleID(param.Parameter);
                    }

            }



            return null;
        }



        static List<mRoles> GetUserRole()
        {
            RoleContext db = new RoleContext();

            var query = db.mRoles.ToList();

            return query;
        }

        //public static object GetUserRole(object param)
        //{

        //    mRolesModel model = param as mRolesModel;
        //    RoleContext pCtxt = new RoleContext();
        //    var query = (from List in pCtxt.mRolesModel
        //                 select new mRolesModel
        //                 {
        //                     RoleId = List.RoleId,
        //                     RoleName = List.RoleName,
        //                     RoleDescription = List.RoleDescription
        //                 }).ToList();

        //    int totalRecords = query.Count();
        //    var results = query.ToList();
        //    model.objList = results;



        //    return model;

        //}



        static void CreateRole(object param)
        {
            try
            {
                using (RoleContext db = new RoleContext())
                {
                    mRoles model = param as mRoles;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    model.RoleId = Guid.NewGuid();
                    db.mRoles.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }
        static void CreateSubUserRole(object param)
        {
            try
            {
                using (RoleContext db = new RoleContext())
                {
                    mSubUserTypeRoles model = param as mSubUserTypeRoles;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedDate = DateTime.Now;
                    db.mSubUserTypeRoles.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }
        static void UpdateSubUserRole(object param)
        {
            RoleContext QCtxt = new RoleContext();
            mSubUserTypeRoles update = param as mSubUserTypeRoles;

            mSubUserTypeRoles obj = QCtxt.mSubUserTypeRoles.Single(m => m.ID == update.ID);
            obj.RoleId = update.RoleId;
            obj.SubUserTypeID = update.SubUserTypeID;
            obj.ModifiedDate = DateTime.Now;
            QCtxt.SaveChanges();

        }
        static object GetRoleDropDown(object param)
        {
            // mSubUserType model = param as mSubUserType;

            RoleContext QCtxt = new RoleContext();
            var query = (from User in QCtxt.mRoles
                         orderby User.RoleName ascending
                         select User).ToList();

            var results = query.ToList();
            return results;
        }
        private static object CheckSubUserRoleExist(object p)
        {
            mSubUserTypeRoles modl = p as mSubUserTypeRoles;
            using (var ctx = new RoleContext())
            {
                var isexist = (from usrdschash in ctx.mSubUserTypeRoles where usrdschash.SubUserTypeID == modl.SubUserTypeID && usrdschash.RoleId == modl.RoleId select usrdschash).Count() > 0;

                return isexist;
            }
        }

        static object UpdateEntryModules(object param)
        {
            RoleContext QCtxt = new RoleContext();
            mRoles update = param as mRoles;

            mRoles obj = QCtxt.mRoles.Single(m => m.RoleId == update.RoleId);
            obj.RoleName = update.RoleName;
            obj.ModifiedDate = update.ModifiedDate;
            obj.RoleNameLocal = update.RoleNameLocal;
            obj.RoleDescription = update.RoleDescription;
            obj.Isactive = update.Isactive;

            QCtxt.SaveChanges();


            string msg = "";

            return msg;
        }


        static mRoles GetRoleDataById(object param)
        {
            mRoles roleMdl = param as mRoles;
            RoleContext db = new RoleContext();
            var query = db.mRoles.SingleOrDefault(a => a.RoleId == roleMdl.RoleId);
            return query;
        }

        static object DeleteRoles(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (RoleContext db = new RoleContext())
            {
                mRoles parameter = param as mRoles;
                mRoles RolesToRemove = db.mRoles.SingleOrDefault(a => a.RoleId == parameter.RoleId);
                db.mRoles.Remove(RolesToRemove);
                db.SaveChanges();
                db.Close();
            }
            return param;
        }


        private static object CheckRoleIDExist(object p)
        {
            var RoleID = (System.Guid)p;
            using (var ctx = new RoleContext())
            {
                var isexist = (from usrdschash in ctx.tUserRoles where usrdschash.Roleid == RoleID select usrdschash).Count() > 0;

                return isexist;
            }
        }


        private static object CheckUserTypeExist(object p)
        {
            var UserTypeName = (string)p;
            using (var ctx = new RoleContext())
            {
                var isexist = (from usrdschash in ctx.mUserType where usrdschash.UserTypeName == UserTypeName select usrdschash).Count() > 0;

                return isexist;
            }
        }

        private static object CheckSubUserTypeExist(object p)
        {
            var SubUserTypeName = (string)p;
            using (var ctx = new RoleContext())
            {
                var isexist = (from usrdschash in ctx.mSubUserType where usrdschash.SubUserTypeName == SubUserTypeName select usrdschash).Count() > 0;

                return isexist;
            }
        }



        private static object CheckUserRoleExist(object p)
        {
            var RoleName = (string)p;
            using (var ctx = new RoleContext())
            {
                var isexist = (from usrdschash in ctx.mRoles where usrdschash.RoleName == RoleName select usrdschash).Count() > 0;

                return isexist;
            }
        }

        #region CheckForUserRoll

        static void MapRollToUsers(object param)
        {
            try
            {
                using (RoleContext db = new RoleContext())
                {
                    tUserRoles model = param as tUserRoles;
                    var uid1 = Convert.ToString(model.UserID);
                    
                    if (model.Roleid == Guid.Parse("0fc60e41-c78e-46b0-b8ac-f6eb708be995"))
                    {
                        //ActionButtonControl actionButton = db.ActionButtonControl.FirstOrDefault(m => m.UserId == uid1);
                        //var actionButton = db.ActionButtonControl.Where(m => m.UserId == uid1).ToList();
                        //IEnumerable<ActionButtonControl> FullList = db.ActionButtonControl.ToList();
                        List<ActionButtonControl> actionButton = db.ActionButtonControl.Where(m => m.UserId == uid1).ToList();
                        if (actionButton.Count() == 0)
                        {                            
                            ActionButtonControl objNew1 = new ActionButtonControl();
                            objNew1.UserId = Convert.ToString(model.UserID);
                            objNew1.ButtonCaption = "Save";
                            objNew1.IsValid = true;
                            db.ActionButtonControl.Add(objNew1);
                            db.SaveChanges();

                            ActionButtonControl objNew2 = new ActionButtonControl();
                            objNew2.UserId = Convert.ToString(model.UserID);
                            objNew2.ButtonCaption = "Freeze";
                            objNew2.IsValid = false;
                            db.ActionButtonControl.Add(objNew2);
                            db.SaveChanges();

                            ActionButtonControl objNew3 = new ActionButtonControl();
                            objNew3.UserId = Convert.ToString(model.UserID);
                            objNew3.ButtonCaption = "Save&Back";
                            objNew3.IsValid = true;
                            db.ActionButtonControl.Add(objNew3);
                            db.SaveChanges();                          
                        }
                        else
                        {
                            foreach (var item in actionButton)
                            {
                                db.ActionButtonControl.Remove(item);
                                db.SaveChanges();
                            } 

                            ActionButtonControl objNew1 = new ActionButtonControl();
                            objNew1.UserId = Convert.ToString(model.UserID);
                            objNew1.ButtonCaption = "Save";
                            objNew1.IsValid = true;
                            db.ActionButtonControl.Add(objNew1);
                            db.SaveChanges();

                            ActionButtonControl objNew2 = new ActionButtonControl();
                            objNew2.UserId = Convert.ToString(model.UserID);
                            objNew2.ButtonCaption = "Freeze";
                            objNew2.IsValid = false;
                            db.ActionButtonControl.Add(objNew2);
                            db.SaveChanges();

                            ActionButtonControl objNew3 = new ActionButtonControl();
                            objNew3.UserId = Convert.ToString(model.UserID);
                            objNew3.ButtonCaption = "Save&Back";
                            objNew3.IsValid = true;
                            db.ActionButtonControl.Add(objNew3);
                            db.SaveChanges();                           
                        }
                    }

                    if (model.Roleid == Guid.Parse("0fc60e41-c78e-46b0-b8ac-f6eb708be996"))
                    {
                        List<ActionButtonControl> actionButton = db.ActionButtonControl.Where(m => m.UserId == uid1).ToList();
                        if (actionButton == null)
                        {
                           
                            ActionButtonControl objNew1 = new ActionButtonControl();
                            objNew1.UserId = Convert.ToString(model.UserID);
                            objNew1.ButtonCaption = "Save";
                            objNew1.IsValid = false;
                            db.ActionButtonControl.Add(objNew1);
                            db.SaveChanges();

                            ActionButtonControl objNew2 = new ActionButtonControl();
                            objNew2.UserId = Convert.ToString(model.UserID);
                            objNew2.ButtonCaption = "Freeze";
                            objNew2.IsValid = true;
                            db.ActionButtonControl.Add(objNew2);
                            db.SaveChanges();

                            ActionButtonControl objNew3 = new ActionButtonControl();
                            objNew3.UserId = Convert.ToString(model.UserID);
                            objNew3.ButtonCaption = "Save&Back";
                            objNew3.IsValid = true;
                            db.ActionButtonControl.Add(objNew3);
                            db.SaveChanges();                           
                        }
                        else
                        {
                            foreach (var item in actionButton)
                            {
                                db.ActionButtonControl.Remove(item);
                                db.SaveChanges();
                            } 

                            ActionButtonControl objNew1 = new ActionButtonControl();
                            objNew1.UserId = Convert.ToString(model.UserID);
                            objNew1.ButtonCaption = "Save";
                            objNew1.IsValid = false;
                            db.ActionButtonControl.Add(objNew1);
                            db.SaveChanges();

                            ActionButtonControl objNew2 = new ActionButtonControl();
                            objNew2.UserId = Convert.ToString(model.UserID);
                            objNew2.ButtonCaption = "Freeze";
                            objNew2.IsValid = true;
                            db.ActionButtonControl.Add(objNew2);
                            db.SaveChanges();

                            ActionButtonControl objNew3 = new ActionButtonControl();
                            objNew3.UserId = Convert.ToString(model.UserID);
                            objNew3.ButtonCaption = "Save&Back";
                            objNew3.IsValid = true;
                            db.ActionButtonControl.Add(objNew3);
                            db.SaveChanges();
                          
                        }
                    }

                    tUserRoles obj = db.tUserRoles.SingleOrDefault(m => m.UserID == model.UserID);
                    if (obj != null)
                    {
                        obj.Roleid = model.Roleid;
                        obj.UserID = model.UserID;
                        obj.IsActive = obj.IsActive;
                        db.SaveChanges();
                        db.Close();
                    }
                    else
                    {
                        model.IsActive = true;
                        db.tUserRoles.Add(model);
                        db.SaveChanges();
                        db.Close();
                    }

                }

            }
            catch
            {
                throw;
            }
        }

        static void RemoveUserRole(object param)
        {
            try
            {
                using (RoleContext db = new RoleContext())
                {
                    tUserRoles parameter = param as tUserRoles;
                    tUserRoles RolesToRemove = db.tUserRoles.SingleOrDefault(a => a.UserID == parameter.UserID);
                    if (RolesToRemove != null)
                    {
                        db.tUserRoles.Remove(RolesToRemove);
                        db.SaveChanges();
                        db.Close();
                    }
                }
            }
            catch
            {
                throw;
            }

        }

        private static object CheckSubUserRoleExistBySubUserID(object p)
        {
            mSubUserTypeRoles modl = p as mSubUserTypeRoles;
            using (var ctx = new RoleContext())
            {
                var isexist = (from usrdschash in ctx.mSubUserTypeRoles where usrdschash.SubUserTypeID == modl.SubUserTypeID select usrdschash).Count() > 0;

                return isexist;
            }
        }

        static List<mSubUserTypeRoles> GetAllSubUserTypeRoles()
        {
            RoleContext db = new RoleContext();

            var query = db.mSubUserTypeRoles.ToList();

            return query;
        }

        static mSubUserTypeRoles GetSubUserTypeRoleById(object param)
        {
            mSubUserTypeRoles ObjmSubUserTypeRoles = param as mSubUserTypeRoles;
            RoleContext db = new RoleContext();
            var query = db.mSubUserTypeRoles.SingleOrDefault(a => a.SubUserTypeID == ObjmSubUserTypeRoles.SubUserTypeID);
            return query;
        }

        public static object GetUserRoleByUserID(object param)
        {
            tUserRoles ObjtUserRoles = param as tUserRoles;
            RoleContext pCtxt = new RoleContext();
            var query = (from r in pCtxt.mRoles
                         join ur in pCtxt.tUserRoles on r.RoleId equals ur.Roleid
                         where ur.UserID == ObjtUserRoles.UserID
                         select r).SingleOrDefault();

            return query;
        }


        private static object CheckUserRoleExistByUserID(object param)
        {
            tUserRoles ObjtUserRoles = param as tUserRoles;
            using (var ctx = new RoleContext())
            {
                var isexist = (from usrdschash in ctx.tUserRoles where usrdschash.UserID == ObjtUserRoles.UserID select usrdschash).Count() > 0;

                return isexist;
            }
        }

        public static object GetUserTypeRoleList(object param)
        {
            mRoles model = param as mRoles;
            RoleContext pCtxt = new RoleContext();
            var query = (from r in pCtxt.mRoles
                         join ur in pCtxt.mSubUserTypeRoles on r.RoleId equals ur.RoleId
                         where ur.SubUserTypeID == model.SubUserTypeID
                         select r).ToList();
            return query;
        }

        public static object GetUserTypeRoleListByMultipleID(object param)
        {
            List<int> lst = param as List<int>;
            mRoles model = param as mRoles;
            RoleContext pCtxt = new RoleContext();
            var query = (from r in pCtxt.mRoles
                         join ur in pCtxt.mSubUserTypeRoles on r.RoleId equals ur.RoleId
                         where lst.Contains(ur.SubUserTypeID)
                         select r).ToList();
            return query;
        }


        #endregion
    }
}
