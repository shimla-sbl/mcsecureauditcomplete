﻿using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.DomainModel.Models.SubDivision;
using SBL.DomainModel.Models.Constituency;
using System.Data.Entity;
using SBL.Service.Common;
using SBL.Domain.Migrations;


namespace SBL.Domain.Context.UserManagement
{
  public class SubDivisionContext:DBBase<SubDivisionContext>
   {
      public SubDivisionContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }
      public DbSet<mSubDivision> mSubDivision { get; set; }
      public DbSet<mConstituency> mConstituency { get; set; }

      public static object Execute(ServiceParameter param)
      {
          if (null == param)
          {
              return null;
          }

          switch (param.Method)
          {
              case "CreateSubDivision": { CreateSubDivision(param.Parameter); break; }
              case "UpdateSubDivision": { return UpdateSubDivision(param.Parameter); }
              case "DeleteSubDivision": { return DeleteSubDivision(param.Parameter); }
              case "GetAllSubDivision": { return GetAllSubDivision(); }
              case "GetSubDivisionById": { return GetSubDivisionById(param.Parameter); }
              case "GetAllConstituency": { return GetAllConstituency(); }
              case "GetConsByAid": { return GetConsByAid(param.Parameter); }
                 

          }
          return null;
      }
       static void CreateSubDivision(object param)
      {
          try
          {
              using (SubDivisionContext db = new SubDivisionContext())
              {
                  mSubDivision model = param as mSubDivision;
                  model.CreatedWhen = DateTime.Now;
                  model.ModifiedWhen = DateTime.Now;
                  db.mSubDivision.Add(model);
                  db.SaveChanges();
                  db.Close();
              }

          }
          catch
          {
              throw;
          }
      }

      static object UpdateSubDivision(object param)
      {
          if (null == param)
          {
              return null;
          }
          using (SubDivisionContext db = new SubDivisionContext())
          {
              mSubDivision model = param as mSubDivision;

              model.CreatedWhen = DateTime.Now;
              model.ModifiedWhen = DateTime.Now;
              db.mSubDivision.Attach(model);
              db.Entry(model).State = EntityState.Modified;
              db.SaveChanges();
              db.Close();
          }
          return GetAllSubDivision();
      }


    
      static object DeleteSubDivision(object param)
      {
          if (null == param)
          {
              return null;
          }
          using (SubDivisionContext db = new SubDivisionContext())
          {
              mSubDivision parameter = param as mSubDivision;
              mSubDivision stateToRemove = db.mSubDivision.SingleOrDefault(a => a.mSubDivisionId == parameter.mSubDivisionId);
              if (stateToRemove != null)
              {
                  stateToRemove.IsDeleted = true;
              }
              //db.mStates.Remove(stateToRemove);
              db.SaveChanges();
              db.Close();
          }
          return GetAllSubDivision();
      }
      static List<mConstituency> GetAllConstituency()
      {
          DBManager db = new DBManager();
          var data = (from d in db.mConstituency
                      orderby d.ConstituencyName ascending
                      where d.IsDeleted == null
                      select d).ToList();
          return data;
          //var query = db.mDepartments.ToList();
          //return query;
      }

      static List<mSubDivision> GetAllSubDivision()
      {
          SubDivisionContext db = new SubDivisionContext();
          var data = (from Desig in db.mSubDivision
                      join Dept in db.mConstituency on Desig.ConstituencyID equals Dept.ConstituencyID into joinConstituencyName
                      from deptn in joinConstituencyName.DefaultIfEmpty()
                      where Desig.IsDeleted == null
                      select new
                      {
                          Desig,
                          deptn.ConstituencyName,


                      });
          //var data1=
          List<mSubDivision> list = new List<mSubDivision>();
          foreach (var item in data)
          {
              item.Desig.GetConstituencyName = item.ConstituencyName;

              list.Add(item.Desig);

          }
          return list.OrderByDescending(c => c.mSubDivisionId).ToList();

      }


      static mSubDivision GetSubDivisionById(object param)
      {
          mSubDivision parameter = param as mSubDivision;
          SubDivisionContext db = new SubDivisionContext();
          var query = db.mSubDivision.SingleOrDefault(a => a.mSubDivisionId == parameter.mSubDivisionId);
          return query;
      }
      static List<mConstituency> GetConsByAid(object param)
      {
          DBManager db = new DBManager();
          mSubDivision model = param as mSubDivision;
          var data = (from d in db.mConstituency
                      orderby d.ConstituencyName ascending
                      where d.AssemblyID == model.AssemblyId
                      select d).ToList();
          return data;
          //var query = db.mDepartments.ToList();
          //return query;
      }
   }
}