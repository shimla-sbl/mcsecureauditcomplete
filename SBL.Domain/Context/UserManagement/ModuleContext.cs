﻿using SBL.DAL;
using SBL.DomainModel.Models.UserModule;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SBL.Service.Common;
using System.Data;
using SBL.DomainModel.Models.User;
using SBL.DomainModel.Models.UserAction;
using System.Data.SqlClient;
using SBL.DomainModel.Models.Adhaar;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Role;

namespace SBL.Domain.Context.UserManagement
{
    public class ModuleContext : DBBase<ModuleContext>
    {
        public ModuleContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public virtual DbSet<mUserModules> mUserModules { get; set; }
        public virtual DbSet<mUserType> mUserType { get; set; }
        public virtual DbSet<mSubUserType> mSubUserType { get; set; }
        public virtual DbSet<mSubUserTypeRoles> mSubUserTypeRoles { get; set; }
        public virtual DbSet<mUserActions> mUserActions { get; set; }
        public virtual DbSet<tUserAccessActions> tUserAccessActions { get; set; }
        public virtual DbSet<mUserSubModules> mUserSubModules { get; set; }
        public virtual DbSet<mDepartment> mDepartment { get; set; }
        public virtual DbSet<tUserAccessRequest> tUserAccessRequest { get; set; }
        public virtual DbSet<tUserAccessRequestLog> tUserAccessRequestLog { get; set; }
        public virtual DbSet<mUsers> mUsers { get; set; }
        public virtual DbSet<mRoles> mRoles { get; set; }
        public DbSet<AdhaarDetails> AdhaarDetails { get; set; }
        public static object Execute(SBL.Service.Common.ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {

                case "GetUserSubModules":
                    {
                        return GetUserSubModules();
                    }
                case "CreateSubModule": { CreateSubModule(param.Parameter); break; }

                case "CreateSubUserType": { CreateSubUserType(param.Parameter); break; }

                case "GetSubModuleDataById":
                    {
                        return GetSubModuleDataById(param.Parameter);
                    }

                case "GetSUserTypeDataById":
                    {
                        return GetSUserTypeDataById(param.Parameter);
                    }
                case "DeleteSubModule":
                    {
                        return DeleteSubModule(param.Parameter);
                    }

                case "DeleteSubUserType":
                    {
                        return DeleteSubUserType(param.Parameter);
                    }


                case "UpdateEntrySubModules":
                    {
                        return UpdateEntrySubModules(param.Parameter);
                    }

                case "UpdateEntrySubUserType":
                    {
                        return UpdateEntrySubUserType(param.Parameter);
                    }
                case "CheckSubUserAccessExist":
                    {
                        return CheckSubUserAccessExist(param.Parameter);
                    }
                case "GetUserModules":
                    {
                        return GetUserModules();
                    }

                case "CreateModule": { CreateModule(param.Parameter); break; }


                case "GetModuleDataById":
                    {
                        return GetModuleDataById(param.Parameter);
                    }
                case "GetActionDataById":
                    {
                        return GetActionDataById(param.Parameter);
                    }

                case "GetAssiciatedDepartmentDataById":
                    {
                        return GetAssiciatedDepartmentDataById(param.Parameter);
                    }

                case "GetUserAccessActionDataById":
                    {
                        return GetUserAccessActionDataById(param.Parameter);
                    }
                case "DeleteModule":
                    {
                        return DeleteModule(param.Parameter);
                    }
                case "GetSubModuleByModuleId":
                    {
                        return GetSubModuleByModuleId(param.Parameter);

                    }
                case "DeleteAction":
                    {
                        return DeleteAction(param.Parameter);
                    }
                case "DeleteUserAccessAction":
                    {
                        return DeleteUserAccessAction(param.Parameter);
                    }

                case "UpdateEntryModules":
                    {
                        return UpdateEntryModules(param.Parameter);
                    }

                case "UpdateEntryAction":
                    {
                        return UpdateEntryAction(param.Parameter);
                    }


                case "GetUserType":
                    {
                        return GetUserType();
                    }

                case "GetSubUserType":
                    {
                        return GetSubUserType();
                    }

                case "CreateUserType": { CreateUserType(param.Parameter); break; }

                case "GetUserTypeDataById":
                    {
                        return GetUserTypeDataById(param.Parameter);
                    }
                case "ExistUserAccessRequestById":
                    {
                        return ExistUserAccessRequestById(param.Parameter);
                    }
                case "UpdateEntryUserType":
                    {
                        return UpdateEntryUserType(param.Parameter);
                    }

                case "DeleteUserType":
                    {
                        return DeleteUserType(param.Parameter);
                    }
                case "GetUserTypeDropDown":
                    {
                        return GetUserTypeDropDown(param.Parameter);
                    }

                case "GetUserAccessDropDown":
                    {
                        return GetUserAccessDropDown(param.Parameter);
                    }

                case "GetSubUserTypeDropDown":
                    {
                        return GetSubUserTypeDropDown(param.Parameter);
                    }

                case "GetUserActionDropDown":
                    {
                        return GetUserActionDropDown(param.Parameter);
                    }

                case "GetUserAction":
                    {
                        return GetUserAction();
                    }

                case "GetUserAccessAction":
                    {
                        return GetUserAccessAction();
                    }
                case "SaveUserAction": { SaveUserAction(param.Parameter); break; }

                case "SaveUserAccessAction": { SaveUserAccessAction(param.Parameter); break; }

                case "UpdateUserAccessAction":
                    {
                        return UpdateUserAccessAction(param.Parameter);
                    }
                case "UpdateAccessActionOrder":
                    {
                        return UpdateAccessActionOrder(param.Parameter);
                    }
                    
                case "CheckUserAccessExist":
                    {
                        return CheckUserAccessExist(param.Parameter);
                    }
                case "CheckUserRequestExist":
                    {
                        return CheckUserRequestExist(param.Parameter);
                    }


                case "CheckUserActionExist":
                    {
                        return CheckUserActionExist(param.Parameter);
                    }
                case "CheckUserAccessActionExist":
                    {
                        return CheckUserAccessActionExist(param.Parameter);
                    }
                case "CheckUserAccessIdExist":
                    {
                        return CheckUserAccessIdExist(param.Parameter);
                    }
                case "GetUsers":
                    {
                        return GetUsers();
                    }
                case "GetWebAdminAccessRequestrGrid":
                    {
                        return GetWebAdminAccessRequestrGrid(param.Parameter);
                    }
                case "GetWebAdminAccessRequestrGridByUserId":
                    {
                        return GetWebAdminAccessRequestrGridByUserId(param.Parameter);
                    }

                case "UpdateAcceptRequest":
                    {
                        return UpdateAcceptRequest(param.Parameter);
                    }

                case "UpdateRejectRequest":
                    {
                        return UpdateRejectRequest(param.Parameter);
                    }
                case "GetWebAdminAcceptedGrid":
                    {
                        return GetWebAdminAcceptedGrid(param.Parameter);
                    }

                case "GetWebAdminAcceptedGridByUSerId":
                    {
                        return GetWebAdminAcceptedGridByUSerId(param.Parameter);
                    }

                case "GetWebAdminRejectedGridByUSerId":
                    {
                        return GetWebAdminRejectedGridByUSerId(param.Parameter);
                    }



                case "GetWebAdminAcceptedGridForEdit":
                    {
                        return GetWebAdminAcceptedGridForEdit(param.Parameter);
                    }
                case "GetWebAdminAcceptRejectGridForEdit":
                    {
                        return GetWebAdminAcceptRejectGridForEdit(param.Parameter);
                    }


                case "GetWebAdminRejectedGridForEdit":
                    {
                        return GetWebAdminRejectedGridForEdit(param.Parameter);
                    }


                case "GetWebAdminRejectedGrid":
                    {
                        return GetWebAdminRejectedGrid(param.Parameter);
                    }
                case "DeleteUserAccepted":
                    {
                        return DeleteUserAccepted(param.Parameter);
                    }
                case "DeleteWebAcceptRejectUser":
                    {
                        return DeleteWebAcceptRejectUser(param.Parameter);
                    }
                case "DeleteUserRejected":
                    {
                        return DeleteUserRejected(param.Parameter);
                    }
                case "UpdateUserAccessRequest":
                    {
                        UpdateUserAccessRequest(param.Parameter);
                        break;
                    }
                case "DeleteUserAccessRequestByID":
                    {
                        DeleteUserAccessRequestByID(param.Parameter);
                        break;
                    }

                case "GetUserAccessActionByUserType":
                    {
                        return GetUserAccessActionByUserType(param.Parameter);
                    }
                case "GetUserAccessActionByUserTypeForEdit":
                    {
                        return GetUserAccessActionByUserTypeForEdit(param.Parameter);
                    }

                case "SaveUserAccessRequest":
                    {
                        SaveUserAccessRequest(param.Parameter);
                        break;
                    }
                case "SaveUserAccessRequestLog":
                    {
                        SaveUserAccessRequestLog(param.Parameter);
                        break;
                    }

                case "GetUserAccessRequestByID":
                    {
                        return GetUserAccessRequestByID(param.Parameter);

                    }
                case "GetSubUserTypeByUserTypeId":
                    {
                        return GetSubUserTypeByUserTypeId(param.Parameter);

                    }
                case "ExistUserAccessRequestByTwoId":
                    {
                        return ExistUserAccessRequestByTwoId(param.Parameter);

                    }

                case "GetUseraccessBySubUserTypeId":
                    {
                        return GetUseraccessBySubUserTypeId(param.Parameter);

                    }

                case "GetUserSubAccessDropDown":
                    {
                        return GetUserSubAccessDropDown(param.Parameter);

                    }
                case "UpdateMeargeActionID":
                    {
                        UpdateMeargeActionID(param.Parameter);
                        break;
                    }
                case "CheckUserExist":
                    {
                        return CheckUserExist(param.Parameter);

                    }

                case "GetAllRoles":
                    {
                        return GetAllRoles();
                    }

                case "GetSubUserTypeRoleList":
                    {
                        return GetSubUserTypeRoleList(param.Parameter);

                    }
                case "GetEditSubUsertypeRole":
                    {
                        return GetEditSubUsertypeRole(param.Parameter);
                    }

                case "GetUseModulesByUserID":
                    {
                        return GetUseModulesByUserID(param.Parameter);
                    }
                //Added code venkat for Dynamic menu
                case "GetPendingUserRequestCount":
                    {
                        return GetPendingUserRequestCount(param.Parameter);

                    }
                case "GetAcceptedUserRequestCount":
                    {
                        return GetAcceptedUserRequestCount(param.Parameter);

                    }
                case "GetRejectedUserRequestCount":
                    {
                        return GetRejectedUserRequestCount(param.Parameter);

                    }
                case "getOrderNo": { return getOrderNo(param.Parameter); }
                case "getOrderNoForSubModule": { return getOrderNoForSubModule(param.Parameter); }

                case "UpdateSubAccessByAccess":
                    {
                        UpdateSubAccessByAccess(param.Parameter);
                        break;
                    }
                case "GetUserAccessActionByUserAccessActionsId":
                    {
                        return GetUserAccessActionByUserAccessActionsId(param.Parameter);
                    }
                case "CheckAccessRequestApproved":
                    {
                        return CheckAccessRequestApproved(param.Parameter);
                    }
                case "GetUserId":
                    {
                        return GetUserId(param.Parameter);
                    }
                case "GetUsersAdharDetails":
                    {
                        return GetUsersAdharDetails();
                    }
                case "UpdateAccessOrder":
                    {
                        UpdateAccessOrder(param.Parameter);
                        break;
                    }
                case "UpdateSDMRequest":
                    {
                        UpdateSDMRequest(param.Parameter);
                        break;
                    }
                case "ExistUserAccessRequestByThreeId":
                    {
                        return ExistUserAccessRequestByThreeId(param.Parameter);

                    }
                case "UpdateOfficerAccessRequest":
                    {
                        UpdateOfficerAccessRequest(param.Parameter);
                        break;

                    }
                case "GetUsersAdharDetailsForCommittee":
                    {
                        return GetUsersAdharDetailsForCommittee();
                    }
                    
            }

            return null;
        }


        static List<tUserAccessActions> GetUserAccessActionByUserTypeForEdit(object param)
        {
            ModuleContext db = new ModuleContext();
            // Select B.ModuleName, A.UserAccessActionsId, MergeActionId from tUserAccessActions A inner join mUserModules B on A.ModuleId =B.ModuleId where UserTypeID=3
            tUserAccessActions moduleMdl = param as tUserAccessActions;

            var query = (from User in db.tUserAccessActions
                         join module in db.mUserModules on new { ModuleId = User.ModuleId } equals new { ModuleId = module.ModuleId }

                         select new
                         {
                             User,
                             module

                         }).ToList();
            List<tUserAccessActions> outputResult = new List<tUserAccessActions>();
            foreach (var item in query)
            {
                tUserAccessActions catData = new tUserAccessActions();

                catData.ModuleName = item.module.ModuleName;
                catData.ModuleId = item.module.ModuleId;
                catData.MergeActionId = item.User.MergeActionId;
                catData.UserTypeID = item.User.UserTypeID;
                catData.UserTypeName = item.User.UserTypeName;
                catData.UserAccessActionsId = item.User.UserAccessActionsId;
                outputResult.Add(catData);

            }

            return outputResult;

        }

        static object CheckAccessRequestApproved(object param)
        {

            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    mUsers model = param as mUsers;

                    var obj = (from tu in db.tUserAccessRequest where tu.UserID == model.UserId && tu.IsAcceptedDate != null select tu).Count() > 0;
                    return obj;
                }

            }
            catch
            {
                throw;
            }
        }
        static void UpdateUserAccessRequest(object param)
        {

            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    tUserAccessRequest model = param as tUserAccessRequest;

                    tUserAccessRequest obj = db.tUserAccessRequest.Single(m => m.ID == model.ID);
                    obj.MergeActionId = model.MergeActionId;
                    //obj.SelectedAssociateDepartmentId = model.SelectedAssociateDepartmentId;
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }
        static object DeleteUserAccepted(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');

            foreach (var item in obja)
            {

                using (var obj1 = new ModuleContext())
                {
                    long id = Convert.ToInt16(item);

                    var UserRequestobj = (from Userrequest in obj1.tUserAccessRequest
                                          where Userrequest.ID == id
                                          select Userrequest).FirstOrDefault();


                    obj1.tUserAccessRequest.Remove(UserRequestobj);

                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return param;

        }

        static object DeleteWebAcceptRejectUser(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');

            foreach (var item in obja)
            {

                using (var obj1 = new ModuleContext())
                {
                    long id = Convert.ToInt16(item);

                    var UserRequestobj = (from Userrequest in obj1.tUserAccessRequest
                                          where Userrequest.ID == id
                                          select Userrequest).FirstOrDefault();


                    obj1.tUserAccessRequest.Remove(UserRequestobj);

                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return param;

        }



        static object DeleteUserRejected(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');

            foreach (var item in obja)
            {

                using (var obj1 = new ModuleContext())
                {
                    long id = Convert.ToInt16(item);

                    var UserRequestobj = (from Userrequest in obj1.tUserAccessRequest
                                          where Userrequest.ID == id
                                          select Userrequest).FirstOrDefault();


                    obj1.tUserAccessRequest.Remove(UserRequestobj);

                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return param;

        }



        static object GetWebAdminAcceptedGridForEdit(object param)
        {
            tUserAccessRequest model = param as tUserAccessRequest;

            ModuleContext db = new ModuleContext();
            var query = (from useraccessaction in db.tUserAccessRequest


                         join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                         //join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID

                         where useraccessaction.DomainAdminstratorId == model.DomainAdminstratorId
                         && useraccessaction.TypedDomainId != null
                         && useraccessaction.IsAcceptedDate != null
                         && useraccessaction.ID == model.ID

                         // join ActionName in db.mUserActions on useraccessaction.ActionId equals ActionName.ActionId

                         select new
                         {
                             useraccessaction,
                             ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),

                             UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                             SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                             UserName.UserName,
                             useraccessaction.ID,
                             // ActionName.ActionName
                             UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                             Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                             UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                         }).ToList();


            List<tUserAccessRequest> list = new List<tUserAccessRequest>();

            foreach (var item in query)
            {

                string s = item.useraccessaction.ActionControlId;
                string DepartmentIds = item.useraccessaction.AssociateDepartmentId;
                string SelectDeptID = item.useraccessaction.SelectedAssociateDepartmentId;
                item.useraccessaction.ModuleName = item.ModuleName;
                item.useraccessaction.UserName = item.UserNameDetails;
                item.useraccessaction.ID = item.ID;
                item.useraccessaction.usertypename = item.UserTypeName;
                item.useraccessaction.SubUserTypeName = item.SubUserTypeName;


                if (DepartmentIds != null && DepartmentIds != "")
                {
                    string[] values = DepartmentIds.Split(',');
                    String DepartmentName = "";
                    for (int i = 0; i < values.Length; i++)
                    {

                        string DeptId = values[i];

                        var item1 = (from deptm in db.mDepartment
                                     select new tUserAccessRequestModel
                                     {
                                         AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                     }).FirstOrDefault();


                        if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                        {
                            DepartmentName = item1.AssociateDepartmentId;
                        }
                        else
                        {
                            //item.useraccessaction.AssociateDepartmentId + ","
                            DepartmentName += item1.AssociateDepartmentId + ",";
                        }



                    }

                    if (DepartmentName.LastIndexOf(",") > 0)
                    {
                        DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                    }

                    item.useraccessaction.DepartmentName = DepartmentName;
                    item.useraccessaction.AssociateDepartmentId = DepartmentIds;
                    item.useraccessaction.SelectedAssociateDepartmentId = SelectDeptID;
                }

                if (s != null && s != "")
                {
                    string[] values = s.Split(',');



                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item1 = (from useraction in db.mUserActions
                                     select new tUserAccessRequestModel
                                     {
                                         ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                     }).FirstOrDefault();


                        if (string.IsNullOrEmpty(item.useraccessaction.ActionControlId))
                        {
                            item.useraccessaction.ActionName = item1.ActionName;
                        }
                        else
                        {
                            item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                        }

                    }
                }
                list.Add(item.useraccessaction);

            }

            return list.ToList();


        }

        static object GetWebAdminAcceptRejectGridForEdit(object param)
        {
            tUserAccessRequest model = param as tUserAccessRequest;
            ModuleContext db = new ModuleContext();
            var query = (from useraccessaction in db.tUserAccessRequest

                         join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                         // join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID

                         where useraccessaction.DomainAdminstratorId == model.DomainAdminstratorId &&
                         useraccessaction.TypedDomainId != null
                         && useraccessaction.IsAcceptedDate == null
                              && useraccessaction.IsRejectedDate == null
                         && useraccessaction.ID == model.ID

                         select new
                         {
                             useraccessaction,
                             ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                             UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                             UserName.UserName,
                             useraccessaction.ID,
                             SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                             UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                             Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                             UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                         }).ToList();


            List<tUserAccessRequest> list = new List<tUserAccessRequest>();

            foreach (var item in query)
            {

                string s = item.useraccessaction.ActionControlId;
                string DepartmentIds = item.useraccessaction.AssociateDepartmentId;
                string SelectDeptID = item.useraccessaction.SelectedAssociateDepartmentId;
                item.useraccessaction.ModuleName = item.ModuleName;
                item.useraccessaction.UserName = item.UserNameDetails;
                item.useraccessaction.ID = item.ID;
                item.useraccessaction.usertypename = item.UserTypeName;
                item.useraccessaction.SubUserTypeName = item.SubUserTypeName;


                if (DepartmentIds != null && DepartmentIds != "")
                {
                    string[] values = DepartmentIds.Split(',');
                    String DepartmentName = "";
                    for (int i = 0; i < values.Length; i++)
                    {

                        string DeptId = values[i];

                        var item1 = (from deptm in db.mDepartment
                                     select new tUserAccessRequestModel
                                     {
                                         AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                     }).FirstOrDefault();


                        if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                        {
                            DepartmentName = item1.AssociateDepartmentId;
                        }
                        else
                        {
                            //item.useraccessaction.AssociateDepartmentId + ","
                            DepartmentName += item1.AssociateDepartmentId + ",";
                        }



                    }

                    if (DepartmentName.LastIndexOf(",") > 0)
                    {
                        DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                    }

                    item.useraccessaction.DepartmentName = DepartmentName;
                    item.useraccessaction.AssociateDepartmentId = DepartmentIds;
                    item.useraccessaction.SelectedAssociateDepartmentId = SelectDeptID;
                }

                if (s != null && s != "")
                {
                    string[] values = s.Split(',');



                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item1 = (from useraction in db.mUserActions
                                     select new tUserAccessRequestModel
                                     {
                                         ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                     }).FirstOrDefault();


                        if (string.IsNullOrEmpty(item.useraccessaction.ActionControlId))
                        {
                            item.useraccessaction.ActionName = item1.ActionName;
                        }
                        else
                        {
                            item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                        }

                    }
                }
                list.Add(item.useraccessaction);

            }

            return list.ToList();


        }

        static object GetWebAdminRejectedGridForEdit(object param)
        {
            tUserAccessRequest model = param as tUserAccessRequest;
            ModuleContext db = new ModuleContext();
            var query = (from useraccessaction in db.tUserAccessRequest


                         join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                         //join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID

                         where useraccessaction.DomainAdminstratorId == model.DomainAdminstratorId && useraccessaction.TypedDomainId != null && useraccessaction.IsRejectedDate != null && useraccessaction.ID == model.ID

                         // join ActionName in db.mUserActions on useraccessaction.ActionId equals ActionName.ActionId

                         select new
                         {
                             useraccessaction,
                             ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                             UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                             UserName.UserName,
                             useraccessaction.ID,
                             SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                             UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                             Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                             UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                         }).ToList();


            List<tUserAccessRequest> list = new List<tUserAccessRequest>();
            foreach (var item in query)
            {

                string s = item.useraccessaction.ActionControlId;

                item.useraccessaction.ModuleName = item.ModuleName;
                item.useraccessaction.UserName = item.UserNameDetails;
                item.useraccessaction.ID = item.ID;
                item.useraccessaction.usertypename = item.UserTypeName;
                item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item1 = (from useraction in db.mUserActions
                                     select new tUserAccessRequestModel
                                     {
                                         ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                     }).FirstOrDefault();


                        if (string.IsNullOrEmpty(item.useraccessaction.ActionControlId))
                        {
                            item.useraccessaction.ActionName = item1.ActionName;
                        }
                        else
                        {
                            item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                        }

                    }
                }

                list.Add(item.useraccessaction);

            }

            return list.ToList();


        }
        static object GetWebAdminAcceptedGrid(object param)
        {

            ModuleContext db = new ModuleContext();

            tUserAccessRequest moduleMdl = param as tUserAccessRequest;
            var USerId = moduleMdl.UserID;
            List<tUserAccessRequest> list = new List<tUserAccessRequest>();
            if (moduleMdl.SecreataryId != 0 && moduleMdl.SecreataryId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate != null
                             && useraccessaction.IsRejectedDate == null
                              && useraccessaction.SecreataryId == moduleMdl.SecreataryId
                             //|| UserName.DepartmentIDs != null && UserName.DepartmentIDs != ""
                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                // UserTypeName = (from usr in db.mUserType where usr.UserTypeID == UserName.OfficeLevel select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                                 UserName.OfficeLevel,
                                 UserName.IsNodalOfficer

                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    int utypeid = Convert.ToInt32(item.OfficeLevel);
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();
                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    item.useraccessaction.IsNodalOfficer = item.IsNodalOfficer;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else if (moduleMdl.MemberId != 0 && moduleMdl.MemberId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate != null
                             && useraccessaction.IsRejectedDate == null
                              && useraccessaction.MemberId == moduleMdl.MemberId

                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                                 UserName.OfficeLevel

                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.UserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else if (moduleMdl.HODId != 0 && moduleMdl.HODId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate != null
                             && useraccessaction.IsRejectedDate == null
                              && useraccessaction.HODId == moduleMdl.HODId
                             //|| UserName.DepartmentIDs != null && UserName.DepartmentIDs != ""
                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                // UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                                 UserName.OfficeLevel

                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.UserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else if (moduleMdl.DomainAdminstratorId == 1)
            {
                var query = (from useraccessaction in db.tUserAccessRequest
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate != null
                             && useraccessaction.IsRejectedDate == null
                             // || UserName.DepartmentIDs != null && UserName.DepartmentIDs != ""

                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                                 UserName.OfficeLevel

                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.UserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();
                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            return list.ToList();

        }

        static object GetWebAdminAcceptedGridByUSerId(object param)
        {
            tUserAccessRequest moduleMdl = param as tUserAccessRequest;
            List<tUserAccessRequest> list = new List<tUserAccessRequest>();
            ModuleContext db = new ModuleContext();
            var querynew= (from user in db.mUsers where user.UserId == moduleMdl.UserID select user).FirstOrDefault();
            if (querynew.UserType == 38 && moduleMdl.MemberId != null && moduleMdl.MemberId != 0)
            {
                var query = (from useraccessaction in db.tUserAccessRequest

                             //join AccessName in db.mUserModules on useraccessaction.AccessID equals AccessName.ModuleId
                             join AccessName in db.tUserAccessActions on useraccessaction.AccessID equals AccessName.UserAccessActionsId
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             //join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID

                             where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.TypedDomainId != null
                             && useraccessaction.IsAcceptedDate != null
                             && moduleMdl.UserID == UserName.UserId
                             && useraccessaction.MemberId==moduleMdl.MemberId
                             // join ActionName in db.mUserActions on useraccessaction.ActionId equals ActionName.ActionId

                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),


                             }).ToList();


           
                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    item.useraccessaction.usertypename = item.UserTypeName;
                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else
            {
                var query = (from useraccessaction in db.tUserAccessRequest

                             //join AccessName in db.mUserModules on useraccessaction.AccessID equals AccessName.ModuleId
                             join AccessName in db.tUserAccessActions on useraccessaction.AccessID equals AccessName.UserAccessActionsId
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             //join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID

                             where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.TypedDomainId != null
                             && useraccessaction.IsAcceptedDate != null
                             && moduleMdl.UserID == UserName.UserId

                             // join ActionName in db.mUserActions on useraccessaction.ActionId equals ActionName.ActionId

                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),


                             }).ToList();


              
                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    item.useraccessaction.usertypename = item.UserTypeName;
                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            return list.ToList();


        }

        static object GetWebAdminRejectedGridByUSerId(object param)
        {
            tUserAccessRequest moduleMdl = param as tUserAccessRequest;
            List<tUserAccessRequest> list = new List<tUserAccessRequest>();

            ModuleContext db = new ModuleContext();
              var querynew= (from user in db.mUsers where user.UserId == moduleMdl.UserID select user).FirstOrDefault();
              if (querynew.UserType == 38 && moduleMdl.MemberId!=null && moduleMdl.MemberId!=0)
              {
                  var query = (from useraccessaction in db.tUserAccessRequest

                               //join AccessName in db.mUserModules on useraccessaction.AccessID equals AccessName.ModuleId
                               join AccessName in db.tUserAccessActions on useraccessaction.AccessID equals AccessName.UserAccessActionsId
                               join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                               //join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID

                               where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                               && useraccessaction.TypedDomainId != null
                               && useraccessaction.IsRejectedDate != null
                               && moduleMdl.UserID == UserName.UserId
                               && useraccessaction.MemberId==moduleMdl.MemberId
                               // join ActionName in db.mUserActions on useraccessaction.ActionId equals ActionName.ActionId

                               select new
                               {
                                   useraccessaction,
                                   ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                   UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                   SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                   UserName.UserName,
                                   useraccessaction.ID,
                                   // ActionName.ActionName
                                   UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                   Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                   UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                               }).ToList();

             
                  foreach (var item in query)
                  {

                      string s = item.useraccessaction.MergeActionId;
                      string DepartmentIds = item.useraccessaction.AssociateDepartmentId;

                      item.useraccessaction.ModuleName = item.ModuleName;
                      item.useraccessaction.UserName = item.UserNameDetails;
                      item.useraccessaction.ID = item.ID;
                      item.useraccessaction.usertypename = item.SubUserTypeName;
                      item.useraccessaction.name = item.UserNameDetails;
                      item.useraccessaction.Adharid = item.Adharid;
                      item.useraccessaction.SubUserTypeName = item.SubUserTypeName;

                      if (DepartmentIds != null && DepartmentIds != "")
                      {
                          string[] values = DepartmentIds.Split(',');
                          String DepartmentName = "";
                          for (int i = 0; i < values.Length; i++)
                          {

                              string DeptId = values[i];

                              var item1 = (from deptm in db.mDepartment
                                           select new tUserAccessRequestModel
                                           {
                                               AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                           }).FirstOrDefault();


                              if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                              {
                                  DepartmentName = item1.AssociateDepartmentId;
                              }
                              else
                              {
                                  //item.useraccessaction.AssociateDepartmentId + ","
                                  DepartmentName += item1.AssociateDepartmentId + ",";
                              }



                          }

                          if (DepartmentName.LastIndexOf(",") > 0)
                          {
                              DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                          }

                          item.useraccessaction.AssociateDepartmentId = DepartmentName;


                      }



                      if (s != null && s != "")
                      {
                          string[] values = s.Split(',');

                          for (int i = 0; i < values.Length; i++)
                          {
                              int MemId = int.Parse(values[i]);
                              var item1 = (from useraction in db.mUserActions
                                           select new tUserAccessRequestModel
                                           {
                                               ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                           }).FirstOrDefault();


                              if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                              {
                                  item.useraccessaction.ActionName = item1.ActionName;
                              }
                              else
                              {
                                  item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                              }

                          }
                      }
                      list.Add(item.useraccessaction);

                  }
              }
              else
              {
                  var query = (from useraccessaction in db.tUserAccessRequest

                               //join AccessName in db.mUserModules on useraccessaction.AccessID equals AccessName.ModuleId
                               join AccessName in db.tUserAccessActions on useraccessaction.AccessID equals AccessName.UserAccessActionsId
                               join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                               //join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID

                               where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                               && useraccessaction.TypedDomainId != null
                               && useraccessaction.IsRejectedDate != null
                               && moduleMdl.UserID == UserName.UserId

                               // join ActionName in db.mUserActions on useraccessaction.ActionId equals ActionName.ActionId

                               select new
                               {
                                   useraccessaction,
                                   ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                   UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                   SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                   UserName.UserName,
                                   useraccessaction.ID,
                                   // ActionName.ActionName
                                   UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                   Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                   UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                               }).ToList();

              

                  foreach (var item in query)
                  {

                      string s = item.useraccessaction.MergeActionId;
                      string DepartmentIds = item.useraccessaction.AssociateDepartmentId;

                      item.useraccessaction.ModuleName = item.ModuleName;
                      item.useraccessaction.UserName = item.UserNameDetails;
                      item.useraccessaction.ID = item.ID;
                      item.useraccessaction.usertypename = item.SubUserTypeName;
                      item.useraccessaction.name = item.UserNameDetails;
                      item.useraccessaction.Adharid = item.Adharid;
                      item.useraccessaction.SubUserTypeName = item.SubUserTypeName;

                      if (DepartmentIds != null && DepartmentIds != "")
                      {
                          string[] values = DepartmentIds.Split(',');
                          String DepartmentName = "";
                          for (int i = 0; i < values.Length; i++)
                          {

                              string DeptId = values[i];

                              var item1 = (from deptm in db.mDepartment
                                           select new tUserAccessRequestModel
                                           {
                                               AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                           }).FirstOrDefault();


                              if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                              {
                                  DepartmentName = item1.AssociateDepartmentId;
                              }
                              else
                              {
                                  //item.useraccessaction.AssociateDepartmentId + ","
                                  DepartmentName += item1.AssociateDepartmentId + ",";
                              }



                          }

                          if (DepartmentName.LastIndexOf(",") > 0)
                          {
                              DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                          }

                          item.useraccessaction.AssociateDepartmentId = DepartmentName;


                      }



                      if (s != null && s != "")
                      {
                          string[] values = s.Split(',');

                          for (int i = 0; i < values.Length; i++)
                          {
                              int MemId = int.Parse(values[i]);
                              var item1 = (from useraction in db.mUserActions
                                           select new tUserAccessRequestModel
                                           {
                                               ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                           }).FirstOrDefault();


                              if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                              {
                                  item.useraccessaction.ActionName = item1.ActionName;
                              }
                              else
                              {
                                  item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                              }

                          }
                      }
                      list.Add(item.useraccessaction);

                  }
              }
            return list.ToList();


        }



        static object GetWebAdminRejectedGrid(object param)
        {
            ModuleContext db = new ModuleContext();
            tUserAccessRequest model = param as tUserAccessRequest;
            var UserId = model.UserID;
            List<tUserAccessRequest> list = new List<tUserAccessRequest>();
            if (model.SecreataryId != 0 && model.SecreataryId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest

                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where useraccessaction.DomainAdminstratorId == model.DomainAdminstratorId
                             && useraccessaction.TypedDomainId != null
                             && useraccessaction.IsRejectedDate != null
                              && useraccessaction.SecreataryId == model.SecreataryId
                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                            UserName.OfficeLevel

                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.AssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.SubUserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;


                    }



                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else if (model.MemberId != 0 && model.MemberId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest

                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where useraccessaction.DomainAdminstratorId == model.DomainAdminstratorId
                             && useraccessaction.TypedDomainId != null
                             && useraccessaction.IsRejectedDate != null
                              && useraccessaction.MemberId == model.MemberId
                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                             UserName.OfficeLevel
                             
                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.AssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.SubUserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;


                    }



                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            if (model.HODId != 0 && model.HODId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest

                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where useraccessaction.DomainAdminstratorId == model.DomainAdminstratorId
                             && useraccessaction.TypedDomainId != null
                             && useraccessaction.IsRejectedDate != null
                              && useraccessaction.HODId == model.HODId
                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                             UserName.OfficeLevel
                             
                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.AssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.SubUserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;


                    }



                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else if (model.DomainAdminstratorId == 1)
            {
                var query = (from useraccessaction in db.tUserAccessRequest

                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where useraccessaction.DomainAdminstratorId == model.DomainAdminstratorId
                             && useraccessaction.TypedDomainId != null
                             && useraccessaction.IsRejectedDate != null

                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                             UserName.OfficeLevel
                             
                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.AssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.SubUserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;


                    }



                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            return list.ToList();


        }
        public static object UpdateAcceptRequest(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');

            foreach (var item in obja)
            {

                using (var obj1 = new ModuleContext())
                {
                    long id = Convert.ToInt16(item);

                    var UserRequestobj = (from Userrequest in obj1.tUserAccessRequest
                                          where Userrequest.ID == id
                                          select Userrequest).FirstOrDefault();


                    UserRequestobj.IsAcceptedDate = DateTime.Now;

                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return meg;
        }
        public static object GetUserId(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');
            Guid UserId = new Guid();
            int i = 0;
            if (i == 0)
            {
                foreach (var item in obja)
                {

                    using (var obj1 = new ModuleContext())
                    {
                        long id = Convert.ToInt16(item);

                        UserId = (from Userrequest in obj1.tUserAccessRequest
                                  where Userrequest.ID == id
                                  select Userrequest.UserID).FirstOrDefault();
                        i = i + 1;
                    }
                }
            }
            return UserId;
        }


        public static object UpdateRejectRequest(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');

            foreach (var item in obja)
            {

                using (var obj1 = new ModuleContext())
                {
                    long id = Convert.ToInt16(item);

                    var UserRequestobj = (from Userrequest in obj1.tUserAccessRequest
                                          where Userrequest.ID == id
                                          select Userrequest).FirstOrDefault();


                    UserRequestobj.IsRejectedDate = DateTime.Now;

                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return meg;
        }



        static List<mRoles> GetAllRoles()
        {
            try
            {
                ModuleContext db = new ModuleContext();
                //List<mUsers> mUsers = (from dept in SCtx.mUsers select dept).OrderByDescending(x => x.UserId).ToList();
                //return mUsers;
                var data = db.mRoles.ToList();
                return data;

            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static List<mUsers> GetUsers()
        {
            try
            {
                ModuleContext db = new ModuleContext();
                //List<mUsers> mUsers = (from dept in SCtx.mUsers select dept).OrderByDescending(x => x.UserId).ToList();
                //return mUsers;
                var data = (from user in db.mUsers select user).ToList();

                return data;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static List<mUsers> GetUsersAdharDetails()
        {
            try
            {
                ModuleContext db = new ModuleContext();
                var data = (from user in db.mUsers
                            where user.UserType==5
                            select new
                            {
                                user,
                                Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == user.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == user.AadarId select (adhar.Name)).FirstOrDefault(),
                            }).ToList();
                List<mUsers> list = new List<mUsers>();
                foreach (var item in data)
                {
                    if(!string.IsNullOrEmpty(item.Adharid))
                    {
                    item.user.UserId = item.user.UserId;
                    item.user.Name = item.UserNameDetails + " <=> " + item.Adharid;
                    list.Add(item.user);
                    }

                }
                return list.GroupBy(x => x.AadarId).Select(g => g.First()).OrderBy(o=>o.Name).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        static object GetWebAdminAccessRequestrGrid(object param)
        {
            ModuleContext db = new ModuleContext();
            List<tUserAccessRequest> list = new List<tUserAccessRequest>();
            tUserAccessRequest moduleMdl = param as tUserAccessRequest;
            var USerId = moduleMdl.UserID;

            if (moduleMdl.SecreataryId != 0 && moduleMdl.SecreataryId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate == null
                             && useraccessaction.IsRejectedDate == null
                             && useraccessaction.SecreataryId == moduleMdl.SecreataryId) || (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate != null
                             && useraccessaction.IsRejectedDate == null
                             && useraccessaction.SecreataryId == moduleMdl.SecreataryId && UserName.DepartmentIDs != null && UserName.DepartmentIDs != "")
                             || (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsRejectedDate == null
                             && useraccessaction.SecreataryId == moduleMdl.SecreataryId && UserName.RequestedAdditionalDept != null && UserName.RequestedAdditionalDept != "")


                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                                 UserName.OfficeLevel,
                                 UserName.IsNodalOfficer
                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.UserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;
                    item.useraccessaction.IsNodalOfficer = item.IsNodalOfficer;
                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else if (moduleMdl.MemberId != 0 && moduleMdl.MemberId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate == null
                             && useraccessaction.IsRejectedDate == null
                             && useraccessaction.MemberId == moduleMdl.MemberId 
                             //&& useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             //&& useraccessaction.IsAcceptedDate != null
                             //&& useraccessaction.IsRejectedDate == null
                            // && useraccessaction.MemberId == moduleMdl.MemberId && UserName.DepartmentIDs != null && UserName.DepartmentIDs != ""
                            )


                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                                 UserName.OfficeLevel

                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.UserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else if (moduleMdl.HODId != 0 && moduleMdl.HODId != null)
            {
                var query = (from useraccessaction in db.tUserAccessRequest
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate == null
                             && useraccessaction.IsRejectedDate == null
                             && useraccessaction.HODId == moduleMdl.HODId) || (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate != null
                             && useraccessaction.IsRejectedDate == null
                             && useraccessaction.HODId == moduleMdl.HODId && UserName.DepartmentIDs != null && UserName.DepartmentIDs != "")
                             || (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsRejectedDate == null
                             && useraccessaction.HODId == moduleMdl.HODId && UserName.RequestedAdditionalDept != null && UserName.RequestedAdditionalDept != "")

                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                                 UserName.OfficeLevel

                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.UserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            else if (moduleMdl.DomainAdminstratorId == 1)
            {
                var query = (from useraccessaction in db.tUserAccessRequest
                             join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                             where (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate == null
                             && useraccessaction.IsRejectedDate == null) || (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                             && useraccessaction.IsAcceptedDate != null
                             && useraccessaction.IsRejectedDate == null && UserName.DepartmentIDs != null && UserName.DepartmentIDs != "")
                             || (useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                               && useraccessaction.IsAcceptedDate != null
                             && useraccessaction.IsRejectedDate == null && UserName.RequestedAdditionalDept != null && UserName.RequestedAdditionalDept != "")


                             select new
                             {
                                 useraccessaction,
                                 ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                                 //UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                                 SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                                 UserName.UserName,
                                 useraccessaction.ID,
                                 // ActionName.ActionName
                                 UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                                 Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                 UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                                 UserName.OfficeLevel

                             }).ToList();



                foreach (var item in query)
                {

                    string s = item.useraccessaction.MergeActionId;
                    string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                    item.useraccessaction.ModuleName = item.ModuleName;
                    item.useraccessaction.UserName = item.UserNameDetails;
                    item.useraccessaction.ID = item.ID;
                    //item.useraccessaction.usertypename = item.UserTypeName;
                    int finalResult;
                    bool output;
                    output = int.TryParse(item.OfficeLevel, out finalResult);
                    int utypeid = finalResult;
                    item.useraccessaction.usertypename = (from usr in db.mUserType where usr.UserTypeID == utypeid select (usr.UserTypeName)).FirstOrDefault();

                    item.useraccessaction.name = item.UserNameDetails;
                    item.useraccessaction.Adharid = item.Adharid;
                    item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                    //item.useraccessaction.ActionName = item.ActionName;

                    if (DepartmentIds != null && DepartmentIds != "")
                    {
                        string[] values = DepartmentIds.Split(',');
                        String DepartmentName = "";
                        for (int i = 0; i < values.Length; i++)
                        {

                            string DeptId = values[i];

                            var item1 = (from deptm in db.mDepartment
                                         select new tUserAccessRequestModel
                                         {
                                             AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                            {
                                DepartmentName = item1.AssociateDepartmentId;
                            }
                            else
                            {
                                //item.useraccessaction.AssociateDepartmentId + ","
                                DepartmentName += item1.AssociateDepartmentId + ",";
                            }



                        }

                        if (DepartmentName.LastIndexOf(",") > 0)
                        {
                            DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                        }

                        item.useraccessaction.AssociateDepartmentId = DepartmentName;



                    }

                    item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;


                    if (s != null && s != "")
                    {
                        string[] values = s.Split(',');

                        for (int i = 0; i < values.Length; i++)
                        {
                            int MemId = int.Parse(values[i]);
                            var item1 = (from useraction in db.mUserActions
                                         select new tUserAccessRequestModel
                                         {
                                             ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                         }).FirstOrDefault();


                            if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                            {
                                item.useraccessaction.ActionName = item1.ActionName;
                            }
                            else
                            {
                                item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                            }

                        }
                    }
                    list.Add(item.useraccessaction);

                }
            }
            return list.ToList();


        }

        static object GetWebAdminAccessRequestrGridByUserId(object param)
        {
            ModuleContext db = new ModuleContext();
              List<tUserAccessRequest> list = new List<tUserAccessRequest>();
            tUserAccessRequest moduleMdl = param as tUserAccessRequest;
            var USerId = moduleMdl.UserID;

            var querynew = (from user in db.mUsers where user.UserId == moduleMdl.UserID select user).FirstOrDefault();
            if (querynew.UserType == 38 && moduleMdl.MemberId != null && moduleMdl.MemberId != 0)
            {
             var query = (from useraccessaction in db.tUserAccessRequest
                          join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                          //join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID
                          where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                              //&& useraccessaction.TypedDomainId != null
                          && useraccessaction.IsAcceptedDate == null
                          && useraccessaction.IsRejectedDate == null
                          && UserName.UserId == USerId
                          && useraccessaction.MemberId==moduleMdl.MemberId

                          // join ActionName in db.mUserActions on useraccessaction.ActionId equals ActionName.ActionId

                          select new
                          {
                              useraccessaction,
                              ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                              UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                              SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                              UserName.UserName,
                              useraccessaction.ID,
                              // ActionName.ActionName
                              UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                              Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                              UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                              //departmentids=UserName.DepartmentIDs,

                          }).ToList();


           
             foreach (var item in query)
             {

                 string s = item.useraccessaction.MergeActionId;
                 string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                 item.useraccessaction.ModuleName = item.ModuleName;
                 item.useraccessaction.UserName = item.UserNameDetails;
                 item.useraccessaction.ID = item.ID;
                 item.useraccessaction.usertypename = item.UserTypeName;
                 item.useraccessaction.name = item.UserNameDetails;
                 item.useraccessaction.Adharid = item.Adharid;
                 item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                 //item.useraccessaction.ActionName = item.ActionName;

                 if (DepartmentIds != null && DepartmentIds != "")
                 {
                     string[] values = DepartmentIds.Split(',');
                     String DepartmentName = "";
                     for (int i = 0; i < values.Length; i++)
                     {

                         string DeptId = values[i];

                         var item1 = (from deptm in db.mDepartment
                                      select new tUserAccessRequestModel
                                      {
                                          AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                      }).FirstOrDefault();


                         if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                         {
                             DepartmentName = item1.AssociateDepartmentId;
                         }
                         else
                         {
                             //item.useraccessaction.AssociateDepartmentId + ","
                             DepartmentName += item1.AssociateDepartmentId + ",";
                         }



                     }

                     if (DepartmentName.LastIndexOf(",") > 0)
                     {
                         DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                     }

                     item.useraccessaction.AssociateDepartmentId = DepartmentName;



                 }

                 item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;
                 // item.departmentids=

                 if (s != null && s != "")
                 {
                     string[] values = s.Split(',');

                     for (int i = 0; i < values.Length; i++)
                     {
                         int MemId = int.Parse(values[i]);
                         var item1 = (from useraction in db.mUserActions
                                      select new tUserAccessRequestModel
                                      {
                                          ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                      }).FirstOrDefault();


                         if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                         {
                             item.useraccessaction.ActionName = item1.ActionName;
                         }
                         else
                         {
                             item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                         }

                     }
                 }
                 list.Add(item.useraccessaction);

             }
         }
         else
         {
                     var query = (from useraccessaction in db.tUserAccessRequest
                         join UserName in db.mUsers on useraccessaction.UserID equals UserName.UserId
                         //join UserType in db.mUserType on useraccessaction.DomainAdminstratorId equals UserType.UserTypeID
                         where useraccessaction.DomainAdminstratorId == moduleMdl.DomainAdminstratorId
                         && useraccessaction.IsAcceptedDate == null
                         && useraccessaction.IsRejectedDate == null
                         && UserName.UserId == USerId

                         // join ActionName in db.mUserActions on useraccessaction.ActionId equals ActionName.ActionId

                         select new
                         {
                             useraccessaction,
                             ModuleName = (from mc in db.mUserModules join uA in db.tUserAccessActions on mc.ModuleId equals uA.ModuleId where uA.UserAccessActionsId == useraccessaction.AccessID select (mc.ModuleName)).FirstOrDefault(),
                             UserTypeName = (from usr in db.mUserType where usr.UserTypeID == useraccessaction.TypedDomainId select (usr.UserTypeName)).FirstOrDefault(),
                             SubUserTypeName = (from usr in db.mSubUserType where usr.SubUserTypeID == useraccessaction.SubUserTypeID select (usr.SubUserTypeName)).FirstOrDefault(),
                             UserName.UserName,
                             useraccessaction.ID,
                             // ActionName.ActionName
                             UserId = (from User in db.mUsers where User.UserId == UserName.UserId select (User.UserId)).FirstOrDefault(),

                             Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                             UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == UserName.AadarId select (adhar.Name)).FirstOrDefault(),
                             //departmentids=UserName.DepartmentIDs,

                         }).ToList();
           

            
            foreach (var item in query)
            {

                string s = item.useraccessaction.MergeActionId;
                string DepartmentIds = item.useraccessaction.SelectedAssociateDepartmentId;

                item.useraccessaction.ModuleName = item.ModuleName;
                item.useraccessaction.UserName = item.UserNameDetails;
                item.useraccessaction.ID = item.ID;
                item.useraccessaction.usertypename = item.UserTypeName;
                item.useraccessaction.name = item.UserNameDetails;
                item.useraccessaction.Adharid = item.Adharid;
                item.useraccessaction.SubUserTypeName = item.SubUserTypeName;
                //item.useraccessaction.ActionName = item.ActionName;

                if (DepartmentIds != null && DepartmentIds != "")
                {
                    string[] values = DepartmentIds.Split(',');
                    String DepartmentName = "";
                    for (int i = 0; i < values.Length; i++)
                    {

                        string DeptId = values[i];

                        var item1 = (from deptm in db.mDepartment
                                     select new tUserAccessRequestModel
                                     {
                                         AssociateDepartmentId = (from mc in db.mDepartment where mc.deptId == DeptId select (mc.deptname)).FirstOrDefault(),

                                     }).FirstOrDefault();


                        if (string.IsNullOrEmpty(item.useraccessaction.AssociateDepartmentId))
                        {
                            DepartmentName = item1.AssociateDepartmentId;
                        }
                        else
                        {
                            //item.useraccessaction.AssociateDepartmentId + ","
                            DepartmentName += item1.AssociateDepartmentId + ",";
                        }



                    }

                    if (DepartmentName.LastIndexOf(",") > 0)
                    {
                        DepartmentName = DepartmentName.Substring(0, DepartmentName.LastIndexOf(","));
                    }

                    item.useraccessaction.AssociateDepartmentId = DepartmentName;



                }

                item.useraccessaction.SelectedAssociateDepartmentId = DepartmentIds;
                // item.departmentids=

                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item1 = (from useraction in db.mUserActions
                                     select new tUserAccessRequestModel
                                     {
                                         ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                     }).FirstOrDefault();


                        if (string.IsNullOrEmpty(item.useraccessaction.MergeActionId))
                        {
                            item.useraccessaction.ActionName = item1.ActionName;
                        }
                        else
                        {
                            item.useraccessaction.ActionName = item.useraccessaction.ActionName + "," + item1.ActionName;
                        }

                    }
                }
                list.Add(item.useraccessaction);

            }
         }
            return list.ToList();


        }

        static List<mUserSubModules> GetUserSubModules()
        {
            ModuleContext db = new ModuleContext();

            //var query = db.mUserSubModules.ToList();




            var query = (from sm in db.mUserSubModules

                         join m in db.mUserModules on sm.ModuleId equals m.ModuleId

                         select new
                         {
                             //useraccessaction,
                             SubModuleId = sm.SubModuleId,
                             SubModuleName = sm.SubModuleName,
                             SubModuleDescription = sm.SubModuleDescription,
                             ModuleId = m.ModuleId,
                             ModuleName = m.ModuleName,
                             ModifiedWhen = sm.ModifiedWhen,

                         }).ToList();

            List<mUserSubModules> list = new List<mUserSubModules>();

            foreach (var m in query)
            {
                mUserSubModules mdl = new mUserSubModules();
                mdl.SubModuleId = m.SubModuleId;
                mdl.SubModuleName = m.SubModuleName;
                mdl.SubModuleDescription = m.SubModuleDescription;
                mdl.ModuleId = m.ModuleId;
                mdl.ModuleName = m.ModuleName;
                mdl.ModifiedWhen = m.ModifiedWhen;
                list.Add(mdl);
            }

            return list.OrderByDescending(a => a.ModifiedWhen).ToList();
        }

        static void CreateSubModule(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    mUserSubModules model = param as mUserSubModules;

                    //reOrderingForSubModule(Convert.ToInt32(model.SubModuleOrder), 0, model.ModuleId, "save");

                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;

                    db.mUserSubModules.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static void CreateSubUserType(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    mSubUserType model = param as mSubUserType;

                    db.mSubUserType.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }



        static mUserSubModules GetSubModuleDataById(object param)
        {
            mUserSubModules moduleMdl = param as mUserSubModules;
            ModuleContext db = new ModuleContext();
            var query = db.mUserSubModules.SingleOrDefault(a => a.SubModuleId == moduleMdl.SubModuleId);
            return query;
        }

        static mSubUserType GetSUserTypeDataById(object param)
        {
            mSubUserType moduleMdl = param as mSubUserType;
            ModuleContext db = new ModuleContext();
            var query = db.mSubUserType.SingleOrDefault(a => a.SubUserTypeID == moduleMdl.SubUserTypeID);
            return query;
        }


        static object UpdateEntrySubModules(object param)
        {

            ModuleContext QCtxt = new ModuleContext();
            mUserSubModules update = param as mUserSubModules;

            mUserSubModules obj = QCtxt.mUserSubModules.Single(m => m.SubModuleId == update.SubModuleId);

            //reOrderingForSubModule(Convert.ToInt32(update.SubModuleOrder), Convert.ToInt32(obj.SubModuleOrder), update.ModuleId, "update");

            obj.ModuleName = update.ModuleName;
            obj.ModuleId = update.ModuleId;
            obj.SubModuleName = update.SubModuleName;
            obj.SubModuleNameLocal = update.SubModuleNameLocal;
            obj.SubModuleDescription = update.SubModuleDescription;
            obj.Isactive = update.Isactive;
            obj.UserTypeID = update.UserTypeID;
            obj.UserTypeName = update.UserTypeName;
            obj.SubModuleOrder = update.SubModuleOrder;
            QCtxt.SaveChanges();
            string msg = "";

            return msg;
        }

        static object UpdateEntrySubUserType(object param)
        {

            ModuleContext QCtxt = new ModuleContext();
            mSubUserType update = param as mSubUserType;

            mSubUserType obj = QCtxt.mSubUserType.Single(m => m.SubUserTypeID == update.SubUserTypeID);
            obj.SubUserTypeName = update.SubUserTypeName;
            obj.SubUserTypeDescription = update.SubUserTypeDescription;
            obj.IsActive = update.IsActive;
            obj.UserTypeID = update.UserTypeID;
            obj.ModifiedWhen = update.ModifiedWhen;
            obj.UserTypeName = update.UserTypeName;
            QCtxt.SaveChanges();
            string msg = "";

            return msg;
        }




        static object DeleteModule(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ModuleContext db = new ModuleContext())
            {
                mUserModules parameter = param as mUserModules;
                mUserModules ModuleToRemove = db.mUserModules.SingleOrDefault(a => a.ModuleId == parameter.ModuleId);
                // reOrdering(0, result.orderNo, result.hType, "delete");
                reOrdering(0, Convert.ToInt32(ModuleToRemove.ModuleOrder), ModuleToRemove.SubUserTypeID, "delete");

                db.mUserModules.Remove(ModuleToRemove);
                db.SaveChanges();
                db.Close();
            }
            return param;
        }

        static object DeleteSubModule(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ModuleContext db = new ModuleContext())
            {
                mUserSubModules parameter = param as mUserSubModules;
                mUserSubModules ModuleToRemove = db.mUserSubModules.SingleOrDefault(a => a.SubModuleId == parameter.SubModuleId);

                reOrderingForSubModule(0, Convert.ToInt32(ModuleToRemove.SubModuleOrder), ModuleToRemove.ModuleId, "delete");

                db.mUserSubModules.Remove(ModuleToRemove);
                db.SaveChanges();
                db.Close();
            }
            return param;
        }

        static object DeleteSubUserType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ModuleContext db = new ModuleContext())
            {
                mSubUserType parameter = param as mSubUserType;
                mSubUserType ModuleToRemove = db.mSubUserType.SingleOrDefault(a => a.SubUserTypeID == parameter.SubUserTypeID);
                db.mSubUserType.Remove(ModuleToRemove);
                db.SaveChanges();
                db.Close();
            }
            return param;
        }

        static List<mUserSubModules> GetSubModuleByModuleId(object param)
        {
            ModuleContext db = new ModuleContext();
            mUserSubModules model = param as mUserSubModules;
            var query = (from dist in db.mUserSubModules
                         where dist.ModuleId == model.ModuleId
                         select dist);

            return query.ToList();
        }

        private static object CheckSubUserAccessExist(object p)
        {

            ModuleContext QCtxt = new ModuleContext();
            mUserSubModules update = p as mUserSubModules;

            var isexist = (from usrdschash in QCtxt.mUserSubModules where usrdschash.SubModuleName == update.SubModuleName && usrdschash.ModuleId == update.ModuleId select usrdschash).Count() > 0;
            return isexist;

        }

        static object GetUserAccessAction()
        {
            tUserAccessActions Test = new tUserAccessActions();
            ModuleContext db = new ModuleContext();
            var query = (from useraccessaction in db.tUserAccessActions

                         join UserType in db.mUserType on useraccessaction.UserTypeID equals UserType.UserTypeID
                         join SubUserType in db.mSubUserType on useraccessaction.SubUserTypeID equals SubUserType.SubUserTypeID
                         join AccessName in db.mUserModules on useraccessaction.ModuleId equals AccessName.ModuleId
                         select new tUserAccessActionsModel
                         {
                             //useraccessaction,
                             SubUserTypeID=SubUserType.SubUserTypeID,
                             UserTypeName = SubUserType.SubUserTypeName,
                             ModuleName = AccessName.ModuleName,
                             ModifiedWhen = useraccessaction.ModifiedWhen,
                             MergeActionId = useraccessaction.MergeActionId,
                             UserAccessActionsId = useraccessaction.UserAccessActionsId
                         }).ToList();



            List<tUserAccessActions> list = new List<tUserAccessActions>();

            tUserAccessActionsModel tempModel = new tUserAccessActionsModel();

            foreach (var item in query)
            {

                string s = item.MergeActionId;

                item.ModuleName = item.ModuleName;

                if (s != null && s != "")
                {
                    string[] values = s.Split(',');

                    for (int i = 0; i < values.Length; i++)
                    {
                        int MemId = int.Parse(values[i]);
                        var item1 = (from useraction in db.mUserActions
                                     select new tUserAccessActionsModel
                                     {
                                         ActionName = (from mc in db.mUserActions where mc.ActionId == MemId select (mc.ActionName)).FirstOrDefault(),

                                     }).FirstOrDefault();



                        if (string.IsNullOrEmpty(item.MergeActionId))
                        {
                            tempModel.ActionName = item1.ActionName;

                        }
                        else
                        {
                            item.ActionName = item.ActionName + "," + item1.ActionName;

                        }

                    }

                }

                tempModel.UserTypeName = item.UserTypeName;
                tempModel.ModuleName = item.ModuleName;
                tempModel.ActionName = item.ActionName;
                tempModel.ModifiedWhen = item.ModifiedWhen;
            }

            var results = query.ToList();
            tempModel.objModelList = results;

            return tempModel;
            // return Test;
        }


        static List<mUserModules> GetUserModules()
        {
            ModuleContext db = new ModuleContext();

            var query = db.mUserModules.ToList();

            return query;
        }

        static void CreateModule(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    mUserModules model = param as mUserModules;
                    //reOrdering(Convert.ToInt32(model.ModuleOrder), 0, model.SubUserTypeID, "save");
                    db.mUserModules.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static mUserModules GetModuleDataById(object param)
        {
            mUserModules moduleMdl = param as mUserModules;
            ModuleContext db = new ModuleContext();
            var query = db.mUserModules.SingleOrDefault(a => a.ModuleId == moduleMdl.ModuleId);
            return query;
        }

        static mUserActions GetActionDataById(object param)
        {
            mUserActions actionMdl = param as mUserActions;
            ModuleContext db = new ModuleContext();
            var query = db.mUserActions.SingleOrDefault(a => a.ActionId == actionMdl.ActionId);
            return query;
        }

        static mDepartment GetAssiciatedDepartmentDataById(object param)
        {
            mDepartment actionMdl = param as mDepartment;
            ModuleContext db = new ModuleContext();
            var query = db.mDepartment.SingleOrDefault(a => a.deptId == actionMdl.deptId);
            return query;
        }

        static tUserAccessActionsModel GetUserAccessActionDataById(object param)
        {

            tUserAccessActionsModel model = param as tUserAccessActionsModel;

            ModuleContext db = new ModuleContext();

            var query = (from Data in db.tUserAccessActions
                         where (Data.UserAccessActionsId == model.UserAccessActionsId)
                         select new tUserAccessActionsModel
                         {

                             ModuleId = Data.ModuleId,
                             MergeActionId = Data.MergeActionId,
                             //ActionName = Data.ActionName,
                             //ModuleName = Data.ModuleName,
                             //  UserTypeName = Data.UserTypeName,
                             UserTypeID = Data.UserTypeID,
                             SubUserTypeID = Data.SubUserTypeID,
                             UserAccessActionsId = Data.UserAccessActionsId,
                             MergeSubModuleId = Data.MergeSubModuleId,
                             AccessActionOrder=Data.AccessActionOrder

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.ToList();
            foreach (var item in query)
            {
                model.ModuleId = item.ModuleId;
                model.UserTypeID = item.UserTypeID;
                model.MergeActionId = item.MergeActionId;
                model.SubUserTypeID = item.SubUserTypeID;
                model.UserAccessActionsId = item.UserAccessActionsId;
                model.MergeSubModuleId = item.MergeSubModuleId;
                model.AccessActionOrder = item.AccessActionOrder;
            }

            return model;
        }

        static object UpdateEntryModules(object param)
        {

            ModuleContext QCtxt = new ModuleContext();
            mUserModules update = param as mUserModules;



            mUserModules obj = QCtxt.mUserModules.Single(m => m.ModuleId == update.ModuleId);

            //reOrdering(Convert.ToInt32(update.ModuleOrder), Convert.ToInt32(obj.ModuleOrder), update.SubUserTypeID, "update");

            obj.ModuleName = update.ModuleName;
            obj.SubUserTypeID = update.SubUserTypeID;
            obj.SubUserTypeName = update.SubUserTypeName;
            obj.ModuleNameLocal = update.ModuleNameLocal;
            obj.ModuleDescription = update.ModuleDescription;
            obj.Isactive = update.Isactive;
            obj.UserTypeID = update.UserTypeID;
            obj.UserTypeName = update.UserTypeName;
            obj.ModifiedWhen = update.ModifiedWhen;
            obj.ModuleOrder = update.ModuleOrder;
            QCtxt.SaveChanges();
            string msg = "";

            return msg;
        }

        static object UpdateEntryAction(object param)
        {

            ModuleContext QCtxt = new ModuleContext();
            mUserActions update = param as mUserActions;

            mUserActions obj = QCtxt.mUserActions.Single(m => m.ActionId == update.ActionId);
            obj.ActionName = update.ActionName;
            obj.ActionDescription = update.ActionDescription;
            obj.Isactive = update.Isactive;
            obj.UserTypeID = update.UserTypeID;
            obj.UserTypeName = update.UserTypeName;
            QCtxt.SaveChanges();
            string msg = "";

            return msg;
        }

        //static object DeleteModule(object param)
        //{
        //    if (null == param)
        //    {
        //        return null;
        //    }
        //    using (ModuleContext db = new ModuleContext())
        //    {
        //        mUserModules parameter = param as mUserModules;
        //        mUserModules ModuleToRemove = db.mUserModules.SingleOrDefault(a => a.ModuleId == parameter.ModuleId);
        //        db.mUserModules.Remove(ModuleToRemove);
        //        db.SaveChanges();
        //        db.Close();
        //    }
        //    return param;
        //}

        static object DeleteAction(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ModuleContext db = new ModuleContext())
            {
                mUserActions parameter = param as mUserActions;
                mUserActions ActionToRemove = db.mUserActions.SingleOrDefault(a => a.ActionId == parameter.ActionId);
                db.mUserActions.Remove(ActionToRemove);
                db.SaveChanges();
                db.Close();
            }
            return param;
        }

        static object DeleteUserAccessAction(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ModuleContext db = new ModuleContext())
            {
                tUserAccessActions parameter = param as tUserAccessActions;
                tUserAccessActions ActionToRemove = db.tUserAccessActions.SingleOrDefault(a => a.UserAccessActionsId == parameter.UserAccessActionsId);
                db.tUserAccessActions.Remove(ActionToRemove);
                db.SaveChanges();
                db.Close();
            }
            return param;
        }
        static object DeleteUserAccessRequestByID(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ModuleContext db = new ModuleContext())
            {
                tUserAccessRequest parameter = param as tUserAccessRequest;
                List<tUserAccessRequest> sts = new List<tUserAccessRequest>();
                sts = (from data in db.tUserAccessRequest where data.UserID == parameter.UserID select data).ToList(); //db.tUserAccessRequest(a => a.UserID == parameter.UserID);

                //db.tUserAccessRequest.Where(x => x.UserID == parameter.UserID).ToList().ForEach(ActionToRemove);
                //obj.SaveChanges();
                foreach (var tes in sts)
                {
                    db.tUserAccessRequest.Remove(tes);
                    db.SaveChanges();

                }

            }
            return param;
        }

        static List<mUserType> GetUserType()
        {
            ModuleContext db = new ModuleContext();

            var query = db.mUserType.ToList();

            return query;
        }

        static List<mSubUserType> GetSubUserType()
        {
            ModuleContext db = new ModuleContext();

            var query = db.mSubUserType.ToList();

            return query;
        }


        static void CreateUserType(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    mUserType model = param as mUserType;

                    db.mUserType.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static mUserType GetUserTypeDataById(object param)
        {
            mUserType moduleMdl = param as mUserType;
            ModuleContext db = new ModuleContext();
            var query = db.mUserType.SingleOrDefault(a => a.UserTypeID == moduleMdl.UserTypeID);
            return query;
        }

        static object UpdateEntryUserType(object param)
        {

            ModuleContext QCtxt = new ModuleContext();
            mUserType update = param as mUserType;

            mUserType obj = QCtxt.mUserType.Single(m => m.UserTypeID == update.UserTypeID);
            obj.UserTypeName = update.UserTypeName;
            obj.UserTypeNameLocal = update.UserTypeNameLocal;
            obj.ModifiedWhen = update.ModifiedWhen;
            obj.IsActive = update.IsActive;

            QCtxt.SaveChanges();
            string msg = "";

            return msg;
        }

        static object DeleteUserType(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (ModuleContext db = new ModuleContext())
            {
                mUserType parameter = param as mUserType;
                mUserType UserTypeToRemove = db.mUserType.SingleOrDefault(a => a.UserTypeID == parameter.UserTypeID);
                db.mUserType.Remove(UserTypeToRemove);
                db.SaveChanges();
                db.Close();
            }
            return param;
        }


        static object GetUserTypeDropDown(object param)
        {
            mUserType model = param as mUserType;

            ModuleContext Ustxt = new ModuleContext();
            var query = (from User in Ustxt.mUserType
                         where User.UserTypeID != 1
                         orderby User.UserTypeID ascending
                         select User).ToList();

            var results = query.ToList();
            return results;
        }
        static object GetSubUserTypeDropDown(object param)
        {
            mSubUserType model = param as mSubUserType;

            ModuleContext Ustxt = new ModuleContext();
            var query = (from User in Ustxt.mSubUserType
                         orderby User.SubUserTypeID ascending
                         select User).ToList();

            var results = query.ToList();
            return results;
        }
        static object GetUserAccessDropDown(object param)
        {
            mUserModules model = param as mUserModules;

            ModuleContext Ustxt = new ModuleContext();
            var query = (from User in Ustxt.mUserModules
                         orderby User.ModuleName ascending
                         select User).ToList();

            var results = query.ToList();
            return results;
        }

        //static object GetSubUserTypeDropDown(object param)
        //{
        //    mSubUserType model = param as mSubUserType;

        //    ModuleContext Ustxt = new ModuleContext();
        //    var query = (from User in Ustxt.mSubUserType
        //                 orderby User.SubUserTypeName ascending
        //                 select User).ToList();

        //    var results = query.ToList();
        //    return results;
        //}


        static object GetUserActionDropDown(object param)
        {
            mUserActions model = param as mUserActions;

            ModuleContext Ustxt = new ModuleContext();
            var query = (from User in Ustxt.mUserActions
                         orderby User.ActionName ascending
                         select User).ToList();

            var results = query.ToList();
            return results;
        }

        static List<mUserActions> GetUserAction()
        {
            ModuleContext db = new ModuleContext();

            var query = db.mUserActions.ToList();

            return query;
        }

        static void SaveUserAction(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    mUserActions model = param as mUserActions;
                    model.CreatedWhen = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;

                    db.mUserActions.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }
        static object GetUserSubAccessDropDown(object param)
        {
            mUserSubModules model = param as mUserSubModules;

            ModuleContext Ustxt = new ModuleContext();
            var query = (from User in Ustxt.mUserSubModules
                         orderby User.SubModuleName ascending
                         select User).ToList();

            var results = query.ToList();
            return results;
        }
        static void SaveUserAccessAction(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    tUserAccessActions model = param as tUserAccessActions;
                    //model.CreatedWhen = DateTime.Now;
                    //model.ModifiedWhen = DateTime.Now;
                    model.MergeActionId = model.ActionComma;
                    model.ModuleId = model.ModuleId;
                    model.UserTypeID = model.UserTypeID;
                    model.SubUserTypeID = model.SubUserTypeID;
                    model.MergeSubModuleId = model.SubModuleComma;
                    model.AccessActionOrder = model.AccessActionOrder;
                    db.tUserAccessActions.Add(model);

                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }
        //static List<tUserAccessActions> GetUserAccessAction()
        //{
        //    ModuleContext db = new ModuleContext();

        //    var query = db.tUserAccessActions.ToList();


        //    return query;
        //}

        static object UpdateUserAccessAction(object param)
        {

            ModuleContext QCtxt = new ModuleContext();
            tUserAccessActions update = param as tUserAccessActions;

            tUserAccessActions obj = QCtxt.tUserAccessActions.Single(m => m.UserAccessActionsId == update.UserAccessActionsId);

            obj.MergeActionId = update.ActionComma;
            obj.UserTypeID = update.UserTypeID;

            obj.SubUserTypeID = update.SubUserTypeID;
            obj.ModuleId = update.ModuleId;
            obj.MergeSubModuleId = update.SubModuleComma;
            obj.AccessActionOrder = update.AccessActionOrder;
            QCtxt.SaveChanges();
            string msg = "";

            return msg;
        }

        static object UpdateAccessActionOrder(object param)
        {

            ModuleContext QCtxt = new ModuleContext();
            tUserAccessActions update = param as tUserAccessActions;
            tUserAccessActions obj = QCtxt.tUserAccessActions.Single(m => m.UserAccessActionsId == update.UserAccessActionsId);
            obj.AccessActionOrder = update.AccessActionOrder;
            QCtxt.SaveChanges();
            string msg = "";

            return msg;
        }


        private static object CheckUserAccessExist(object p)
        {
            var UserAccessName = (string)p;
            using (var ctx = new ModuleContext())
            {
                var isexist = (from usrdschash in ctx.mUserModules where usrdschash.ModuleName == UserAccessName select usrdschash).Count() > 0;

                return isexist;
            }
        }
        private static object ExistUserAccessRequestById(object p)
        {
            ModuleContext QCtxt = new ModuleContext();
            tUserAccessRequest update = p as tUserAccessRequest;

            var isexist = (from usrdschash in QCtxt.tUserAccessRequest where usrdschash.UserID == update.UserID select usrdschash).Count() > 0;
            if (isexist != true)
            {
                isexist = false;
            }
            return isexist;
        }
        private static object ExistUserAccessRequestByThreeId(object p)
        {
            ModuleContext QCtxt = new ModuleContext();
            tUserAccessRequest update = p as tUserAccessRequest;

            var isexist = (from usrdschash in QCtxt.tUserAccessRequest where usrdschash.UserID == update.UserID && usrdschash.AccessID == update.AccessID && usrdschash.MemberId == update.MemberId select usrdschash).Count() > 0;
            if (isexist != true)
            {
                isexist = false;
            }
            return isexist;
        }
        private static object ExistUserAccessRequestByTwoId(object p)
        {
            ModuleContext QCtxt = new ModuleContext();
            tUserAccessRequest update = p as tUserAccessRequest;

            var isexist = (from usrdschash in QCtxt.tUserAccessRequest where usrdschash.UserID == update.UserID && usrdschash.AccessID == update.AccessID select usrdschash).Count() > 0;
            if (isexist != true)
            {
                isexist = false;
            }
            return isexist;
        }
        private static object CheckUserRequestExist(object p)
        {
            var Id = (string)p;
            using (var ctx = new ModuleContext())
            {
                var isexist = (from usrdschash in ctx.tUserAccessRequest where usrdschash.ID == Convert.ToInt16(Id) select usrdschash).Count() > 0;

                return isexist;
            }
        }

        private static object CheckUserActionExist(object p)
        {
            var UserActionName = (string)p;
            using (var ctx = new ModuleContext())
            {
                var isexist = (from usrdschash in ctx.mUserActions where usrdschash.ActionName == UserActionName select usrdschash).Count() > 0;

                return isexist;
            }
        }

        private static object CheckUserAccessActionExist(object p)
        {

            ModuleContext QCtxt = new ModuleContext();
            tUserAccessActions update = p as tUserAccessActions;

            var isexist = (from usrdschash in QCtxt.tUserAccessActions where usrdschash.SubUserTypeID == update.SubUserTypeID && usrdschash.ModuleId == update.ModuleId select usrdschash).Count() > 0;
            return isexist;

        }

        private static object CheckUserAccessIdExist(object p)
        {
            var UserAccessId = (int)p;
            using (var ctx = new ModuleContext())
            {
                var isexist = (from usrdschash in ctx.tUserAccessActions where usrdschash.ModuleId == UserAccessId select usrdschash).Count() > 0;

                return isexist;
            }
        }
        private static object CheckUserExist(object p)
        {

            ModuleContext QCtxt = new ModuleContext();
            tUserAccessRequest update = p as tUserAccessRequest;

            var isexist = (from usrdschash in QCtxt.tUserAccessRequest where usrdschash.UserID == update.UserID && usrdschash.TypedDomainId == update.TypedDomainId select usrdschash).SingleOrDefault();
            return isexist;

        }
        /////////// added by dharmendra

        private static void UpdateMeargeActionID(object param)
        {
            ModuleContext db = new ModuleContext();
            tUserAccessRequest moduleMdl = param as tUserAccessRequest;
            tUserAccessRequest obj = db.tUserAccessRequest.Single(m => m.UserID == moduleMdl.UserID && m.AccessID == moduleMdl.AccessID);
            obj.MergeActionId = moduleMdl.MergeActionId;
            obj.Modifiedwhen = moduleMdl.Modifiedwhen;
            //obj.AccessID = moduleMdl.AccessID;
            //obj.DomainAdminstratorId = moduleMdl.DomainAdminstratorId;
            //obj.TypedDomainId=moduleMdl.TypedDomainId;
            // obj.MemberId = moduleMdl.MemberId;
            // obj.SecreataryId = moduleMdl.SecreataryId;
            db.SaveChanges();

            //return obj = "";
        }
        private static void UpdateSDMRequest(object param)
        {
            ModuleContext db = new ModuleContext();
            tUserAccessRequest moduleMdl = param as tUserAccessRequest;
            tUserAccessRequest obj = db.tUserAccessRequest.Single(m => m.UserID == moduleMdl.UserID && m.AccessID == moduleMdl.AccessID && m.MemberId==moduleMdl.MemberId);
            obj.MergeActionId = moduleMdl.MergeActionId;
            obj.Modifiedwhen = moduleMdl.Modifiedwhen;
            //obj.AccessID = moduleMdl.AccessID;
            //obj.DomainAdminstratorId = moduleMdl.DomainAdminstratorId;
            //obj.TypedDomainId=moduleMdl.TypedDomainId;
            // obj.MemberId = moduleMdl.MemberId;
            // obj.SecreataryId = moduleMdl.SecreataryId;
            db.SaveChanges();

            //return obj = "";
        }
        private static void UpdateOfficerAccessRequest(object param)
        {
            ModuleContext db = new ModuleContext();
            tUserAccessRequest moduleMdl = param as tUserAccessRequest;
            List<tUserAccessRequest> reqlist = new List<tUserAccessRequest>();
            reqlist = (from sr in db.tUserAccessRequest where sr.UserID==moduleMdl.UserID select sr).ToList();
            foreach (var sd in reqlist)
            {
                sd.IsAcceptedDate = DateTime.Now;
                db.SaveChanges();
            }
          
        }
        
        static List<tUserAccessActions> GetUserAccessActionByUserType(object param)
        {
            ModuleContext db = new ModuleContext();
            // Select B.ModuleName, A.UserAccessActionsId, MergeActionId from tUserAccessActions A inner join mUserModules B on A.ModuleId =B.ModuleId where UserTypeID=3
            tUserAccessActions moduleMdl = param as tUserAccessActions;

            var query = (from User in db.tUserAccessActions
                         join module in db.mUserModules on new { ModuleId = User.ModuleId } equals new { ModuleId = module.ModuleId }

                         where User.SubUserTypeID == moduleMdl.SubUserTypeID
                         && module.Isactive == true
                         select new
                         {
                             User,
                             module

                         }).ToList();
            List<tUserAccessActions> outputResult = new List<tUserAccessActions>();
            foreach (var item in query)
            {
                tUserAccessActions catData = new tUserAccessActions();

                catData.ModuleName = item.module.ModuleName;
                catData.ModuleId = item.module.ModuleId;
                catData.MergeActionId = item.User.MergeActionId;
                catData.UserTypeID = item.User.UserTypeID;
                catData.UserTypeName = item.User.UserTypeName;
                catData.SubUserTypeID = item.User.SubUserTypeID;
                catData.SubUserTypeName = item.User.SubUserTypeName;
                catData.UserAccessActionsId = item.User.UserAccessActionsId;
                outputResult.Add(catData);

            }

            return outputResult;

        }
        static object GetUserAccessRequestByID(object param)
        {

            ModuleContext db = new ModuleContext();
            // Select B.ModuleName, A.UserAccessActionsId, MergeActionId from tUserAccessActions A inner join mUserModules B on A.ModuleId =B.ModuleId where UserTypeID=3
            tUserAccessRequest moduleMdl = param as tUserAccessRequest;

            var query = (from User in db.tUserAccessRequest
                         join ust in db.tUserAccessActions on User.AccessID equals ust.UserAccessActionsId
                         join module in db.mUserModules on new { ModuleId = ust.ModuleId } equals new { ModuleId = module.ModuleId }

                         where User.UserID == moduleMdl.UserID
                         && module.Isactive==true 
                         orderby User.ID descending
                         select new
                         {
                             User,
                             ust,
                             module

                         }).ToList();
            List<tUserAccessRequest> outputResult = new List<tUserAccessRequest>();
            foreach (var item in query)
            {
                tUserAccessRequest catData = new tUserAccessRequest();
                catData.ID = item.User.ID;
                catData.ModuleName = item.module.ModuleName;
                catData.MergeActionId = item.User.MergeActionId;
                catData.ActionControlId = item.User.ActionControlId;
                catData.TypedDomainId = item.User.TypedDomainId;
                catData.domainAdminName = item.User.domainAdminName;
                catData.usertypename = item.User.usertypename;
                catData.TypedDomainId = item.User.TypedDomainId;
                catData.AccessID = item.User.AccessID;
                catData.Modifiedwhen = item.User.Modifiedwhen;
                catData.IsAcceptedDate = item.User.IsAcceptedDate;
                catData.IsRejectedDate = item.User.IsRejectedDate;
                catData.MemberId = item.User.MemberId;
                catData.SecreataryId = item.User.SecreataryId;
                catData.AccessOrder = item.ust.AccessActionOrder;
                outputResult.Add(catData);

            }

            return outputResult;
        }
        static void SaveUserAccessRequest(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    tUserAccessRequest model = param as tUserAccessRequest;

                    db.tUserAccessRequest.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static void SaveUserAccessRequestLog(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    tUserAccessRequestLog model = param as tUserAccessRequestLog;

                    db.tUserAccessRequestLog.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }
        static List<mSubUserType> GetSubUserTypeByUserTypeId(object param)
        {
            ModuleContext db = new ModuleContext();
            mSubUserType model = param as mSubUserType;
            var query = (from dist in db.mSubUserType
                         where dist.UserTypeID == model.UserTypeID
                         select dist);

            return query.ToList();
        }


        static List<mUserModules> GetUseraccessBySubUserTypeId(object param)
        {
            ModuleContext db = new ModuleContext();
            mUserModules model = param as mUserModules;
            var query = (from dist in db.mUserModules
                         where dist.SubUserTypeID == model.SubUserTypeID
                         select dist);

            return query.ToList();
        }


        static List<mSubUserTypeRoles> GetSubUserTypeRoleList(object param)
        {
            ModuleContext db = new ModuleContext();
            // Select B.ModuleName, A.UserAccessActionsId, MergeActionId from tUserAccessActions A inner join mUserModules B on A.ModuleId =B.ModuleId where UserTypeID=3
            mSubUserTypeRoles moduleMdl = param as mSubUserTypeRoles;

            var query = (from role in db.mRoles
                         join subrole in db.mSubUserTypeRoles on role.RoleId equals subrole.RoleId
                         join subuser in db.mSubUserType on subrole.SubUserTypeID equals subuser.SubUserTypeID

                         select new
                         {
                             role,
                             subrole,
                             subuser

                         }).ToList();
            List<mSubUserTypeRoles> outputResult = new List<mSubUserTypeRoles>();
            foreach (var item in query)
            {
                mSubUserTypeRoles catData = new mSubUserTypeRoles();

                catData.RoleId = item.role.RoleId;
                catData.RoleName = item.role.RoleName;
                catData.SubUserTypeID = item.subuser.SubUserTypeID;
                catData.SubUserTypeName = item.subuser.SubUserTypeName;
                catData.CreatedDate = item.subrole.CreatedDate;
                catData.ModifiedDate = item.subrole.ModifiedDate;
                catData.Mode = item.subrole.Mode;
                catData.IsActive = item.subrole.IsActive;
                catData.ID = item.subrole.ID;
                outputResult.Add(catData);

            }

            return outputResult;

        }
        static mSubUserTypeRoles GetEditSubUsertypeRole(object param)
        {
            ModuleContext db = new ModuleContext();
            // Select B.ModuleName, A.UserAccessActionsId, MergeActionId from tUserAccessActions A inner join mUserModules B on A.ModuleId =B.ModuleId where UserTypeID=3
            mSubUserTypeRoles moduleMdl = param as mSubUserTypeRoles;

            var query = (from role in db.mRoles
                         join subrole in db.mSubUserTypeRoles on role.RoleId equals subrole.RoleId
                         join subuser in db.mSubUserType on subrole.SubUserTypeID equals subuser.SubUserTypeID
                         join user in db.mUserType on subuser.UserTypeID equals user.UserTypeID
                         where subrole.ID == moduleMdl.ID
                         select new
                         {
                             role,
                             subrole,
                             subuser,
                             user

                         }).SingleOrDefault();

            mSubUserTypeRoles catData = new mSubUserTypeRoles();
            // mSubUserTypeRoles outputResult = new mSubUserTypeRoles();
            catData.RoleId = query.role.RoleId;
            catData.RoleName = query.role.RoleName;
            catData.SubUserTypeID = query.subuser.SubUserTypeID;
            catData.SubUserTypeName = query.subuser.SubUserTypeName;
            catData.CreatedDate = query.subrole.CreatedDate;
            catData.ModifiedDate = query.subrole.ModifiedDate;
            catData.Mode = query.subrole.Mode;
            catData.IsActive = query.subrole.IsActive;
            catData.ID = query.subrole.ID;
            catData.UserTypeID = query.user.UserTypeID;

            //outputResult.Add(catData);


            return catData;

        }


        private static object GetUseModulesByUserID(object p)
        {
            tUserAccessRequest objtUserAccessRequest = p as tUserAccessRequest;
            using (var ctx = new ModuleContext())
            {
                var result = (from a in ctx.tUserAccessRequest
                              join b in ctx.tUserAccessActions on a.AccessID equals b.UserAccessActionsId
                              where a.UserID == objtUserAccessRequest.UserID
                              select b).ToList();

                return result;
            }
        }

        #region Added code venkat for dynamic menu
        static object GetPendingUserRequestCount(object param)
        {
            tUserAccessRequest mod = param as tUserAccessRequest;
            ModuleContext db = new ModuleContext();


            int count = 0;
            if (mod.SecreataryId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         join u in db.mUsers on m.UserID equals u.UserId
                         where (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate == null
                             && m.IsRejectedDate == null
                              && m.SecreataryId == mod.SecreataryId) ||
                              (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate != null
                             && m.IsRejectedDate == null
                              && m.SecreataryId == mod.SecreataryId
                              && u.DepartmentIDs != null && u.DepartmentIDs != "") ||
                              (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsRejectedDate == null
                              && m.SecreataryId == mod.SecreataryId
                              && u.RequestedAdditionalDept != null && u.RequestedAdditionalDept != "")
                         select m).GroupBy(s => s.UserID).Count();
                      
            }
            else if (mod.MemberId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         join u in db.mUsers on m.UserID equals u.UserId
                         where (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate == null
                             && m.IsRejectedDate == null
                              && m.MemberId == mod.MemberId) ||
                              (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate != null
                             && m.IsRejectedDate == null
                              && m.MemberId == mod.MemberId
                              && u.DepartmentIDs != null && u.DepartmentIDs != "")
                         select m).GroupBy(s => s.UserID).Count();
            }
            else if (mod.HODId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         join u in db.mUsers on m.UserID equals u.UserId
                         where (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate == null
                             && m.IsRejectedDate == null
                              && m.HODId == mod.HODId) ||
                              (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate != null
                             && m.IsRejectedDate == null
                              && m.HODId == mod.HODId
                              && u.DepartmentIDs != null && u.DepartmentIDs != "")||
                              (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsRejectedDate == null
                              && m.HODId == mod.HODId
                              && u.RequestedAdditionalDept != null && u.RequestedAdditionalDept != "")
                         select m).GroupBy(s => s.UserID).Count();
            }
            else
            {
                count = (from m in db.tUserAccessRequest
                         join u in db.mUsers on m.UserID equals u.UserId
                         where (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate == null
                             && m.IsRejectedDate == null) ||
                              (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate != null
                             && m.IsRejectedDate == null
                              && u.DepartmentIDs != null && u.DepartmentIDs != "")||
                              (m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsRejectedDate == null
                              && u.RequestedAdditionalDept != null && u.RequestedAdditionalDept != "")
                         select m).GroupBy(s => s.UserID).Count();
            }
            mod.Count = count;
            return mod;
        }
        //Added code venkat for Dynamic menu
        static object GetAcceptedUserRequestCount(object param)
        {
            tUserAccessRequest mod = param as tUserAccessRequest;
            ModuleContext db = new ModuleContext();


            int count = 0;
            if (mod.SecreataryId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         where m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate != null
                             && m.IsRejectedDate == null
                              && m.SecreataryId == mod.SecreataryId
                         select m).GroupBy(s => s.UserID).Count();
            }
            else if (mod.MemberId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         where m.DomainAdminstratorId == mod.DomainAdminstratorId
                           && m.IsAcceptedDate != null
                             && m.IsRejectedDate == null
                              && m.MemberId == mod.MemberId
                         select m).GroupBy(s => s.UserID).Count();
            }
            else if (mod.HODId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         where m.DomainAdminstratorId == mod.DomainAdminstratorId
                            && m.IsAcceptedDate != null
                             && m.IsRejectedDate == null
                              && m.HODId == mod.HODId
                         select m).GroupBy(s => s.UserID).Count();
            }
            else
            {
                count = (from m in db.tUserAccessRequest
                         where m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate != null
                             && m.IsRejectedDate == null

                         select m).GroupBy(s => s.UserID).Count();
            }
            mod.Count = count;
            return mod;
        }
        //Added code venkat for Dynamic menu
        static object GetRejectedUserRequestCount(object param)
        {
            tUserAccessRequest mod = param as tUserAccessRequest;
            ModuleContext db = new ModuleContext();


            int count = 0;
            if (mod.SecreataryId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         where m.DomainAdminstratorId == mod.DomainAdminstratorId
                            && m.IsAcceptedDate == null
                             && m.IsRejectedDate != null
                              && m.SecreataryId == mod.SecreataryId
                         select m).GroupBy(s => s.UserID).Count();
            }
            else if (mod.MemberId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         where m.DomainAdminstratorId == mod.DomainAdminstratorId
                          && m.IsAcceptedDate == null
                             && m.IsRejectedDate != null
                              && m.MemberId == mod.MemberId
                         select m).GroupBy(s => s.UserID).Count();
            }
            else if (mod.HODId != 0)
            {
                count = (from m in db.tUserAccessRequest
                         where m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate == null
                             && m.IsRejectedDate != null
                              && m.HODId == mod.HODId
                         select m).GroupBy(s => s.UserID).Count();
            }
            else
            {
                count = (from m in db.tUserAccessRequest
                         where m.DomainAdminstratorId == mod.DomainAdminstratorId
                             && m.IsAcceptedDate == null
                             && m.IsRejectedDate != null

                         select m).GroupBy(s => s.UserID).Count();
            }

            mod.Count = count;
            return mod;
        }
        #endregion



        public static object getOrderNo(object param)
        {
            int type = Convert.ToInt32(param);
            using (ModuleContext context = new ModuleContext())
            {
                var result = (from list in context.mUserModules
                              where list.SubUserTypeID == type
                              orderby list.ModuleOrder descending
                              select new ModuleOrderList
                              {
                                  _OrderID = list.ModuleOrder
                              }
                               ).ToList();
                return result;
            }
        }

        public static void reOrdering(int newID, int CurrentID, int SubUserTypeID, string Mode)
        {
            int _UserTypeID = SubUserTypeID;
            using (ModuleContext context = new ModuleContext())
            {
                int count = (from list in context.mUserModules
                             where list.SubUserTypeID == _UserTypeID
                             select list).Count();
                if (Mode == "save")
                {
                    if (newID <= count)
                    {
                        try
                        {
                            // count++;
                            for (int start = count; start >= newID; start--)
                            {
                                mUserModules _mUserModules = (from _list in context.mUserModules
                                                              where _list.SubUserTypeID == _UserTypeID && _list.ModuleOrder == start
                                                              orderby _list.ModuleId descending
                                                              select _list

                                                                ).FirstOrDefault();
                                if (_mUserModules != null)
                                {
                                    _mUserModules.ModuleOrder = _mUserModules.ModuleOrder + 1;
                                    context.Entry(_mUserModules).State = EntityState.Modified;
                                    context.SaveChanges();
                                }
                                // context.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                else
                {
                    if (Mode == "update")
                    {
                        if (newID < CurrentID)
                        {
                            if (newID <= count)
                            {
                                try
                                {
                                    // count++;
                                    for (int start = CurrentID; start >= newID; start--)
                                    {
                                        mUserModules _mUserModules = (from _list in context.mUserModules
                                                                      where _list.SubUserTypeID == _UserTypeID && _list.ModuleOrder == start
                                                                      orderby _list.ModuleId descending
                                                                      select _list

                                                                        ).FirstOrDefault();
                                        if (_mUserModules != null)
                                        {

                                            if (start == CurrentID)
                                            {
                                                _mUserModules.ModuleOrder = 0;
                                            }
                                            else
                                            {
                                                _mUserModules.ModuleOrder = _mUserModules.ModuleOrder + 1;
                                            }

                                            context.Entry(_mUserModules).State = EntityState.Modified;
                                            context.SaveChanges();
                                        }
                                        // context.Close();
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                        else
                        {
                            if (newID > CurrentID)
                            {
                                for (int start = CurrentID; start <= newID; start++)
                                {
                                    mUserModules _mUserModules = (from _list in context.mUserModules
                                                                  where _list.SubUserTypeID == _UserTypeID && _list.ModuleOrder == start
                                                                  orderby _list.ModuleId descending
                                                                  select _list

                                                                    ).FirstOrDefault();
                                    if (_mUserModules != null)
                                    {
                                        if (start == CurrentID)
                                        {
                                            _mUserModules.ModuleOrder = 0;
                                        }
                                        else
                                        {
                                            _mUserModules.ModuleOrder = _mUserModules.ModuleOrder - 1;
                                        }

                                        context.Entry(_mUserModules).State = EntityState.Modified;
                                        context.SaveChanges();
                                    }
                                    // context.Close();
                                }
                            }
                            else
                            {
                                if (newID == CurrentID)
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Mode == "delete")
                        {
                            for (int start = CurrentID + 1; start <= count; start++)
                            {
                                mUserModules _mUserModules = (from _list in context.mUserModules
                                                              where _list.SubUserTypeID == _UserTypeID && _list.ModuleOrder == start
                                                              orderby _list.ModuleId descending
                                                              select _list

                                                                ).FirstOrDefault();
                                if (_mUserModules != null)
                                {
                                    _mUserModules.ModuleOrder = _mUserModules.ModuleOrder - 1;
                                    context.Entry(_mUserModules).State = EntityState.Modified;
                                    context.SaveChanges();
                                }
                                // context.Close();
                            }
                        }
                    }
                }
            }
        }


        public static object getOrderNoForSubModule(object param)
        {
            int type = Convert.ToInt32(param);
            using (ModuleContext context = new ModuleContext())
            {
                var result = (from list in context.mUserSubModules
                              where list.ModuleId == type
                              orderby list.SubModuleOrder descending
                              select new SubModuleOrderList
                              {
                                  _OrderID = list.SubModuleOrder
                              }
                               ).ToList();
                return result;
            }
        }

        public static void reOrderingForSubModule(int newID, int CurrentID, int ModuleID, string Mode)
        {
            int _ModuleID = ModuleID;
            using (ModuleContext context = new ModuleContext())
            {
                int count = (from list in context.mUserSubModules
                             where list.ModuleId == _ModuleID
                             select list).Count();
                if (Mode == "save")
                {
                    if (newID <= count)
                    {
                        try
                        {
                            // count++;
                            for (int start = count; start >= newID; start--)
                            {
                                mUserSubModules _mUserSubModules = (from _list in context.mUserSubModules
                                                                    where _list.ModuleId == _ModuleID && _list.SubModuleOrder == start
                                                                    orderby _list.ModuleId descending
                                                                    select _list

                                                                ).FirstOrDefault();
                                if (_mUserSubModules != null)
                                {
                                    _mUserSubModules.SubModuleOrder = _mUserSubModules.SubModuleOrder + 1;
                                    context.Entry(_mUserSubModules).State = EntityState.Modified;
                                    context.SaveChanges();
                                }
                                // context.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                else
                {
                    if (Mode == "update")
                    {
                        if (newID < CurrentID)
                        {
                            if (newID <= count)
                            {
                                try
                                {
                                    // count++;
                                    for (int start = CurrentID; start >= newID; start--)
                                    {
                                        mUserSubModules _mUserSubModules = (from _list in context.mUserSubModules
                                                                            where _list.ModuleId == _ModuleID && _list.SubModuleOrder == start
                                                                            orderby _list.ModuleId descending
                                                                            select _list

                                                                        ).FirstOrDefault();
                                        if (_mUserSubModules != null)
                                        {

                                            if (start == CurrentID)
                                            {
                                                _mUserSubModules.SubModuleOrder = 0;
                                            }
                                            else
                                            {
                                                _mUserSubModules.SubModuleOrder = _mUserSubModules.SubModuleOrder + 1;
                                            }

                                            context.Entry(_mUserSubModules).State = EntityState.Modified;
                                            context.SaveChanges();
                                        }
                                        // context.Close();
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                        else
                        {
                            if (newID > CurrentID)
                            {
                                for (int start = CurrentID; start <= newID; start++)
                                {
                                    mUserSubModules _mUserSubModules = (from _list in context.mUserSubModules
                                                                        where _list.ModuleId == _ModuleID && _list.SubModuleOrder == start
                                                                        orderby _list.ModuleId descending
                                                                        select _list

                                                                    ).FirstOrDefault();
                                    if (_mUserSubModules != null)
                                    {
                                        if (start == CurrentID)
                                        {
                                            _mUserSubModules.SubModuleOrder = 0;
                                        }
                                        else
                                        {
                                            _mUserSubModules.SubModuleOrder = _mUserSubModules.SubModuleOrder - 1;
                                        }

                                        context.Entry(_mUserSubModules).State = EntityState.Modified;
                                        context.SaveChanges();
                                    }
                                    // context.Close();
                                }
                            }
                            else
                            {
                                if (newID == CurrentID)
                                {
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Mode == "delete")
                        {
                            for (int start = CurrentID + 1; start <= count; start++)
                            {
                                mUserSubModules _mUserSubModules = (from _list in context.mUserSubModules
                                                                    where _list.ModuleId == _ModuleID && _list.SubModuleOrder == start
                                                                    orderby _list.ModuleId descending
                                                                    select _list

                                                                ).FirstOrDefault();
                                if (_mUserSubModules != null)
                                {
                                    _mUserSubModules.SubModuleOrder = _mUserSubModules.SubModuleOrder - 1;
                                    context.Entry(_mUserSubModules).State = EntityState.Modified;
                                    context.SaveChanges();
                                }
                                // context.Close();
                            }
                        }
                    }
                }
            }
        }

        static void UpdateSubAccessByAccess(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    tUserAccessRequest model = param as tUserAccessRequest;
                    tUserAccessRequest obj = db.tUserAccessRequest.Single(m => m.ID == model.ID);
                    obj.SubAccessByUser = model.SubAccessByUser;
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }


        static List<mUserSubModules> GetUserAccessActionByUserAccessActionsId(object param)
        {
            tUserAccessActions model = param as tUserAccessActions;
            ModuleContext db = new ModuleContext();
            var query = (from Data in db.tUserAccessActions
                         where Data.UserAccessActionsId == model.UserAccessActionsId
                         select Data).SingleOrDefault();
            var lst = new List<mUserSubModules>();
            if (query != null)
            {
                mUserSubModules objmUserSubModules = new mUserSubModules();
                objmUserSubModules.ModuleId = query.ModuleId;
                lst = GetSubModuleByModuleId(objmUserSubModules).ToList();

            }
            return lst;
        }


        static void UpdateAccessOrder(object param)
        {
            try
            {
                using (ModuleContext db = new ModuleContext())
                {
                    tUserAccessRequest model = param as tUserAccessRequest;
                    tUserAccessRequest obj = db.tUserAccessRequest.Single(m => m.ID == model.ID);
                    obj.AccessOrder = model.AccessOrder;
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static List<mUsers> GetUsersAdharDetailsForCommittee()
        {
            try
            {
                ModuleContext db = new ModuleContext();
                var data = (from user in db.mUsers
                            where user.UserType == 5
                            select new
                            {
                                user,
                                Adharid = (from adhar in db.AdhaarDetails where adhar.AdhaarID == user.AadarId select (adhar.AdhaarID)).FirstOrDefault(),
                                UserNameDetails = (from adhar in db.AdhaarDetails where adhar.AdhaarID == user.AadarId select (adhar.Name)).FirstOrDefault(),
                            }).ToList();
                List<mUsers> list = new List<mUsers>();
                foreach (var item in data)
                {
                    if (!string.IsNullOrEmpty(item.Adharid))
                    {
                        item.user.UserId = item.user.UserId;
                        item.user.Name = item.UserNameDetails;// +" <=> " + item.Adharid;
                        list.Add(item.user);
                    }

                }
                return list.GroupBy(x => x.AadarId).Select(g => g.First()).OrderBy(o => o.Name).ToList();
            }
            catch (Exception ex)
            {
            }
            return null;
        }
    }
}
