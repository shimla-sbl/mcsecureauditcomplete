﻿using SBL.Service.Common;
using SBL.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using SBL.Domain.Context.Department;
using SBL.Domain.Context.Session;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.PaperLaid;
using SBL.DomainModel.Models.Event;
using SBL.DomainModel.Models.Document;
using SBL.DomainModel.Models.Bill;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Notice;
using System.Text.RegularExpressions;
using SBL.DomainModel.ComplexModel;
using System.Data;
using SBL.DomainModel;
using SBL.DomainModel.Models.Enums;
using SBL.DomainModel.Models.User;
using SBL.Domain.Context.BillPaperLaid;
using SBL.DomainModel.Models.Passes;
using SBL.DomainModel.Models.Departmentpdfpath;
using SBL.DomainModel.Models.UserModule;
using SBL.Domain.Context.UserManagement;
using SBL.DomainModel.Models.Diaries;
using SBL.DomainModel.Models.SiteSetting;


namespace SBL.Domain.Context.PaperLaidMinister
{
    public class PaperLaidMinisterContext : DBBase<PaperLaidMinisterContext>
    {
        public PaperLaidMinisterContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<mMember> mMember { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mDepartment> mDepartment { get; set; }
        public DbSet<mSession> mSession { get; set; }
        public DbSet<mDepartmentPdfPath> mDepartmentPdfPath { get; set; }
        //Pending List
        public DbSet<mPaperCategoryType> mPaperCategoryType { get; set; }
        public DbSet<tPaperLaidTemp> tPaperLaidTemp { get; set; }
        public DbSet<mEvent> mEvent { get; set; }
        public DbSet<tPaperLaidV> tPaperLaidV { get; set; }
        public DbSet<mUserDSHDetails> mUserDSHDetails { get; set; }

        //PaperLaidEntry
        public DbSet<mDocumentType> mDocumentTypes { get; set; }
        public DbSet<mSessionDate> mSessionDates { get; set; }
        public DbSet<mBillType> mBillTypes { get; set; }

        public DbSet<tQuestion> tQuestions { get; set; }
        public DbSet<tQuestionType> tQuestionTypes { get; set; }
        public DbSet<mNoticeType> mNoticeTypes { get; set; }
        public DbSet<tMemberNotice> tMemberNotice { get; set; }
        public DbSet<mMinistry> mMinistry { get; set; }
        public DbSet<mMinistryDepartment> mMInistryDepartment { get; set; }
        public DbSet<tBillRegister> tBillRegister { get; set; }
        public DbSet<SiteSettings> SiteSettings { get; set; }

        // Notice
        //  public virtual DbSet<tMemberNotice> tMemberNotices { get; set; }
        //  public virtual DbSet<mEvent> mCategoryType { get; set; }




        public static object Execute(ServiceParameter param)
        {

            if (null == param)
            {
                return null;
            }
            switch (param.Method)
            {
                case "GetMinisterMinistryName":
                    {
                        return GetMinisterMinistryName(param.Parameter);
                    }
                case "GetPaperLaidDepartmentDetails":
                    {
                        return GetPaperLaidDepartmentDetails(param.Parameter);
                    }
                case "GetAllPaperCategoryType":
                    {
                        return GetAllPaperCategoryType(param.Parameter);
                    }
                case "GetPendingDepartmentPaperLaidList":
                    {
                        return GetPendingDepartmentPaperLaidList(param.Parameter);
                    }
                case "GetPaperLaidByEventId":
                    {
                        return GetPaperLaidByEventId(param.Parameter);
                    }
                case "GetPaperLaidTempById":
                    {
                        return GetPaperLaidTempById(param.Parameter);
                    }
                case "UpdateLOBRecordIdIntPaperLaidVS":
                    {
                        return UpdateLOBRecordIdIntPaperLaidVS(param.Parameter);
                    }
                //                    updated by nitin
                case "GetPaperLaidCategoryDetails":
                    {
                        return GetPaperLaidCategoryDetails(param.Parameter);
                    }
                case "GetDocumentType":
                    {
                        return GetDocumentType(param.Parameter);
                    }
                case "GetSessionDates":
                    {
                        return GetSessionDates(param.Parameter);
                    }
                case "GetQuestionNumber":
                    {
                        return GetQuestionNumber(param.Parameter);
                    }
                case "GetNotices":
                    {
                        return GetNotices(param.Parameter);
                    }
                case "SubmittPaperLaidEntry":
                    {
                        return SubmittPaperLaidEntry(param.Parameter);
                    }
                case "GetFileSaveDetails":
                    {
                        return GetFileSaveDetails(param.Parameter);
                    }
                case "SaveDetailsInTempPaperLaid":
                    {
                        return SaveDetailsInTempPaperLaid(param.Parameter);
                    }
                case "UpdateDeptActiveId":
                    {
                        return UpdateDeptActiveId(param.Parameter);

                    }
                case "GetQuestionDetails":
                    {
                        return GetQuestionDetails(param.Parameter);
                    }
                case "UpdateQuestionPaperLaidId":
                    {
                        return UpdateQuestionPaperLaidId(param.Parameter);
                    }
                case "GetNoticeDetails":
                    {
                        return GetNoticeDetails(param.Parameter);
                    }
                case "CheckBillNUmber":
                    {
                        return CheckBillNUmber(param.Parameter);
                    }
                case "UpdateNoticePaperLaidId":
                    {
                        return UpdateNoticePaperLaidId(param.Parameter);
                    }
                case "InsertBillDetails":
                    {
                        return InsertBillDetails(param.Parameter);
                    }
                case "SubmitPendingDepartmentPaperLaid":
                    {
                        return SubmitPendingDepartmentPaperLaid(param.Parameter);
                    }
                case "EditPendingPaperLaidDetials":
                    {
                        return EditPendingPaperLaidDetials(param.Parameter);
                    }
                case "UpdatePaperLaidEntry":
                    {
                        return UpdatePaperLaidEntry(param.Parameter);
                    }

                case "GetExisitngFileDetails":
                    {
                        return GetExisitngFileDetails(param.Parameter);
                    }

                case "UpdateDetailsInTempPaperLaid":
                    {
                        return UpdateDetailsInTempPaperLaid(param.Parameter);
                    }
                case "GetSubmittedPaperLaidList":
                    {
                        return GetSubmittedPaperLaidList(param.Parameter);
                    }
                case "DeletePaperLaidByID":
                    {
                        return DeletePaperLaidByID(param.Parameter);
                    }
                case "UpdatePaperLaidFileByID":
                    {
                        return UpdatePaperLaidFileByID(param.Parameter);
                    }
                case "ShowPaperLaidDetailByID":
                    {
                        return ShowPaperLaidDetailByID(param.Parameter);
                    }
                case "GetSubmittedPaperLaidByID":
                    {
                        return GetSubmittedPaperLaidByID(param.Parameter);
                    }
                case "GetFileVersion":
                    {
                        return GetFileVersion(param.Parameter);
                    }
                case "GetPaperLaidById":
                    {
                        return GetPaperLaidById(param.Parameter);
                    }
                case "GetDepartmentNameMinistryDetailsByMinisterID":
                    {
                        return GetDepartmentNameMinistryDetailsByMinisterID(param.Parameter);
                    }
                case "GetPendingQuestionsByType":
                    {
                        return GetPendingQuestionsByType(param.Parameter);
                    }
                case "GetSubmittedQuestionsByType":
                    {
                        return GetSubmittedQuestionsByType(param.Parameter);
                    }
                case "GetLaidInHouseQuestionsByType":
                    {
                        return GetLaidInHouseQuestionsByType(param.Parameter);
                    }
                case "GetPendingForSubQuestionsByType":
                    {
                        return GetPendingForSubQuestionsByType(param.Parameter);
                    }
                case "GetPendingToLayQuestionsByType":
                    {
                        return GetPendingToLayQuestionsByType(param.Parameter);
                    }
                case "GetCountForQuestionTypes":
                    {
                        return GetCountForQuestionTypes(param.Parameter);
                    }
                case "GetQuetionEventID":
                    {
                        return GetQuetionEventID(param.Parameter);
                    }
                case "UpdateMinisterActivePaperId":
                    {
                        return UpdateMinisterActivePaperId(param.Parameter);
                    }
                case "GetQuestionByPaperLaidID":
                    {
                        return GetQuestionByPaperLaidID(param.Parameter);
                    }
                case "GetPaperLaidByPaperLaidId":
                    {
                        return GetPaperLaidByPaperLaidId(param.Parameter);
                    }
                case "UpcomingLOBByQuestionType":
                    {
                        return UpcomingLOBByQuestionType(param.Parameter);
                    }
                case "GetStarredQuestionCounter":
                    {
                        return GetStarredQuestionCounter(param.Parameter);
                    }
                case "GetUnstarredQuestionCounter":
                    {
                        return GetUnstarredQuestionCounter(param.Parameter);
                    }
                //Notic
                case "GetNoticeInBoxList":
                    {
                        return GetNoticeInBoxList(param.Parameter);
                    }
                case "GetNoticeReplySentList":
                    {
                        return GetNoticeReplySentList(param.Parameter);
                    }
                case "GetCountForBillsandNotice":
                    {
                        return GetCountForBillsandNotice(param.Parameter);
                    }
                case "GetNoticeULOBList":
                    {
                        return GetNoticeULOBList(param.Parameter);
                    }
                case "GetNoticeLIHList":
                    {
                        return GetNoticeLIHList(param.Parameter);
                    }
                case "GetNoticePLIHList":
                    {
                        return GetNoticePLIHList(param.Parameter);
                    }
                //Bills

                case "GetDraftsBill":
                    {
                        return GetDraftsBill(param.Parameter);
                    }


                case "GetsentBill":
                    {
                        return GetsentBill(param.Parameter);
                    }
                case "GetBillULOBList":
                    {
                        return GetBillULOBList(param.Parameter);
                    }
                case "GetBillLIHList":
                    {
                        return GetBillLIHList(param.Parameter);
                    }
                case "GetBillPLIHList":
                    {
                        return GetBillPLIHList(param.Parameter);
                    }

                case "GetOtherPaperLaidPendingsDetails":
                    {
                        return GetOtherPaperLaidPendingsDetails(param.Parameter);
                    }

                //OtherPaperLaid
                case "GetOtherPaperLaidCounters":
                    {
                        return GetOtherPaperLaidCounters(param.Parameter);
                    }
                case "GetOtherPaperLaidPaperCategoryType":
                    {
                        return GetOtherPaperLaidPaperCategoryType(param.Parameter);
                    }
                case "GetCountForOtherPaperLaidTypes":
                    {
                        return GetCountForOtherPaperLaidTypes(param.Parameter);
                    }
                case "GetSubmittedOtherPaperLaidList":
                    {
                        return GetSubmittedOtherPaperLaidList(param.Parameter);
                    }
                case "UpcomingLOBByOtherpaperLaidId":
                    {
                        return UpcomingLOBByOtherpaperLaidId(param.Parameter);
                    }
                case "GetOtherPaperLaidInHouseByType":
                    {
                        return GetOtherPaperLaidInHouseByType(param.Parameter);
                    }
                case "GetPendingToLayOtherPaperLaidByType":
                    {
                        return GetPendingToLayOtherPaperLaidByType(param.Parameter);
                    }
                //DSC
                case "InsertSignPathAttachment":
                    {
                        return InsertSignPathAttachment(param.Parameter);
                    }
                case "MinisterPaperLaidDetails":
                    {
                        return MinisterPaperLaidDetails(param.Parameter);
                    }
                case "SubmittedMinisterPaperLaidDetails":
                    {
                        return SubmittedMinisterPaperLaidDetails(param.Parameter);
                    }
                case "GetChangedPaperDetails":
                    {
                        return GetChangedPaperDetails(param.Parameter);
                    }

                case "GetSubmitChangedPaperDetails":
                    {
                        return GetSubmitChangedPaperDetails(param.Parameter);
                    }
                case "ChangedGetMinisterPaperAttachment":
                    {
                        return ChangedGetMinisterPaperAttachment(param.Parameter);
                    }
                case "GetAllOtherPaperLaid":
                    {
                        return GetAllOtherPaperLaid(param.Parameter);
                    }

                case "GetAllBills":
                    {
                        return GetAllBills(param.Parameter);
                    }

                case "GetAllNotices":
                    {
                        return GetAllNotices(param.Parameter);
                    }

                case "GetAllUnstaredQuestionByType":
                    {
                        return GetAllUnstaredQuestionByType(param.Parameter);
                    }
                case "GetAllStarredQuestions":
                    {
                        return GetAllStarredQuestions(param.Parameter);
                    }

                case "BillsPaperLaidDetails":
                    {
                        return BillsPaperLaidDetails(param.Parameter);
                    }

                case "SubmittedBillsPaperLaidDetails":
                    {
                        return SubmittedBillsPaperLaidDetails(param.Parameter);
                    }
                case "NoticePaperLaidDetails":
                    {
                        return NoticePaperLaidDetails(param.Parameter);
                    }
                case "SubmittedNoticePaperLaidDetails":
                    {
                        return SubmittedNoticePaperLaidDetails(param.Parameter);
                    }
                case "GetDepartmentMinisterDashboardItemsCounter":
                    {
                        return GetDepartmentMinisterDashboardItemsCounter(param.Parameter);
                    }
                case "GetMinisterProgressCounter":
                    {
                        return GetMinisterProgressCounter(param.Parameter);
                    }

                case "ShowNoticeInboxById":
                    {
                        return ShowNoticeInboxById(param.Parameter);
                    }

                case "BeforeFixation":
                    {
                        return BeforeFixation(param.Parameter);
                    }
                case "AfterFixation":
                    {
                        return AfterFixation(param.Parameter);
                    }
                //Added code  venkat for dynamic menu
                case "DDlEvent":
                    {
                        return DDlEvent();
                    }

                case "GetSubMenuByMenuId":
                    {
                        return GetSubMenuByMenuId(param.Parameter);
                    }

                case "GetSubMenuByMenuIdLocal":
                    {
                        return GetSubMenuByMenuIdLocal(param.Parameter);
                    }
                    
                case "GetStarredQuestionCounterforMinisters":
                    {
                        return GetStarredQuestionCounterforMinisters(param.Parameter);
                    }
                case "GetUnStarredQuestionCounterforMinisters":
                    {
                        return GetUnStarredQuestionCounterforMinisters(param.Parameter);
                    }
                case "GetNoticesCounterforMinisters":
                    {
                        return GetNoticesCounterforMinisters(param.Parameter);
                    }
                case "GetBillsCounterforMinisters":
                    {
                        return GetBillsCounterforMinisters(param.Parameter);
                    }
                case "GetCountForOtherPaperLaidTypesMinisters":
                    {
                        return GetCountForOtherPaperLaidTypesMinisters(param.Parameter);
                    }
                case "GetOtherPapersCounterforMinisters":
                    {
                        return GetOtherPapersCounterforMinisters(param.Parameter);
                    }
            } return null;
        }



        static object AfterFixation(object param)
        {

            PaperLaidMinisterContext db = new PaperLaidMinisterContext();

            tPaperLaidV objPaperLaid = param as tPaperLaidV;
            //mSessionDate mdept = param as mSessionDate;
            var data = (from dept in db.mSessionDates where dept.AssemblyId == objPaperLaid.AssemblyId && dept.SessionId == objPaperLaid.SessionId select dept).ToList();

            //mSessionDate mdept = param as mSessionDate;
            //var data = db.mSessionDates.ToList();
            return data;
        }

        static object GetMinisterProgressCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            decimal Progress = 0;
            List<tPaperLaidV> lstObj = new List<tPaperLaidV>();

            //Starred Questions

            tPaperLaidV obj = new tPaperLaidV();
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;
            model.TotalStaredReceived = GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            obj.ProgressText = "Starred Questions";
            if (model.TotalStaredReceived > 0)
            {
                Progress = ((decimal)model.TotalStaredSubmitted / (decimal)model.TotalStaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }

            lstObj.Add(obj);

            //Unstarred Questions
            obj = new tPaperLaidV();
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;
            model.TotalStaredReceived = GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            obj.ProgressText = "Unstarred Questions";
            if (model.TotalStaredReceived > 0)
            {
                Progress = ((decimal)model.TotalStaredSubmitted / (decimal)model.TotalStaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }
            lstObj.Add(obj);


            //Notices
            obj = new tPaperLaidV();
            model.TotalUnstaredReceived = GetCountNoticeSent(model) + GetCountNoticePending(model);
            model.TotalUnstaredSubmitted = GetCountNoticeSent(model);
            obj.ProgressText = "Notices";
            if (model.TotalUnstaredReceived > 0)
            {
                Progress = ((decimal)model.TotalUnstaredSubmitted / (decimal)model.TotalUnstaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }
            lstObj.Add(obj);


            //Bills
            obj = new tPaperLaidV();
            model.TotalUnstaredReceived = GetCountBillsPending(model) + GetCountBillsSent(model);
            model.TotalUnstaredSubmitted = GetCountBillsSent(model);
            obj.ProgressText = "Bills";
            if (model.TotalUnstaredReceived > 0)
            {
                Progress = ((decimal)model.TotalUnstaredSubmitted / (decimal)model.TotalUnstaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }
            lstObj.Add(obj);

            //Others Paper Type
            obj = new tPaperLaidV();
            model.TotalUnstaredReceived = GetCountOthersPending(model) + GetCountOthersSent(model);
            model.TotalUnstaredSubmitted = GetCountBillsSent(model);
            obj.ProgressText = "Others Pepers";
            if (model.TotalUnstaredReceived > 0)
            {
                Progress = ((decimal)model.TotalUnstaredSubmitted / (decimal)model.TotalUnstaredReceived) * 100;
                Progress = decimal.Round(Progress, 2);
                obj.ProgressValue = Convert.ToString(Progress) + "%";
            }
            else
            {
                obj.ProgressValue = "0%";
            }
            lstObj.Add(obj);


            model.DepartmentProgressList = lstObj;
            return model;
        }
        static object GetDepartmentMinisterDashboardItemsCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            //Starred Questions Count
            model.QuestionTypeId = (int)QuestionType.StartedQuestion;


            model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;
            model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;

            model.TotalStaredQuestions = GetPendingQuestionsByType(model).ResultCount + GetSubmittedQuestionsByType(model).ResultCount;


            //UnStarred Questions Count
            model.QuestionTypeId = (int)QuestionType.UnstaredQuestion;

            model.TotalUnstaredReceived = GetPendingQuestionsByType(model).ResultCount;
            model.TotalUnstaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            model.TotalUnstaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            //model.TotalUnstaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            //model.TotalUnstaredQuestions = model.TotalUnstaredReceived + model.TotalUnstaredSubmitted + model.TotalUnstaredPendingForSubmission; 

            model.TotalUnstaredQuestions = GetPendingQuestionsByType(model).ResultCount + GetSubmittedQuestionsByType(model).ResultCount + GetPendingForSubQuestionsByType(model).ResultCount;


            //Notices

            model.NoticeInboxCount = GetNoticeInBoxList(model).ResultCount;
            model.NoticeReplaySent = GetNoticeReplySentList(model).ResultCount;
            model.NoticeUPLOBCount = GetNoticeULOBList(model).ResultCount;
            model.NoticeLIHCount = GetNoticeLIHList(model).ResultCount;
            model.NoticePLIHCount = GetNoticePLIHList(model).ResultCount;

            model.NoticeTotalCount = GetNoticeInBoxList(model).ResultCount + GetNoticeReplySentList(model).ResultCount;
            model.NoticeInboxCount = GetCountNoticePending(model);

            //Bills
            model.TotalDeptBillsCount = GetCountBillsPending(model) + GetCountBillsSent(model);

            //Others Papers
            model.TotalOtherPapersCount = GetCountOthersPending(model) + GetCountOthersSent(model);

            return model;
        }

        private static int GetCountNoticePending(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            using (var ctx = new BillPaperLaidContext())
            {
                var noticependingReplycount = (from a in ctx.tPaperLaidV
                                               join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                               join c in ctx.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                               where a.AssemblyId == model.AssemblyCode
                                                   && a.SessionId == model.SessionCode
                                                   && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                   && b.DeptSubmittedBy == null
                                                   && b.DeptSubmittedDate == null
                                                   && a.DeptActivePaperId == b.PaperLaidTempId
                                                   && a.MinistryId == model.MinistryId
                                               select b).Count();

                return noticependingReplycount;
            }

        }

        private static int GetCountNoticeSent(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            using (var ctx = new BillPaperLaidContext())
            {
                var noticesentReplycount = (from a in ctx.tPaperLaidV
                                            join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                            join c in ctx.tMemberNotice on a.PaperLaidId equals c.PaperLaidId
                                            where a.AssemblyId == model.AssemblyCode
                                                && a.SessionId == model.SessionCode
                                                && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                && b.DeptSubmittedBy != null
                                                && b.DeptSubmittedDate != null
                                                && a.DeptActivePaperId == b.PaperLaidTempId
                                                && a.MinistryId == model.MinistryId
                                            select b).Count();

                return noticesentReplycount;
            }

        }



        //Others Papers
        private static int GetCountOthersSent(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            int paperCategoryId = 5;
            using (var ctx = new BillPaperLaidContext())
            {
                var noticesentReplycount = (from a in ctx.tPaperLaidV
                                            join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                            join c in ctx.mEvent on a.EventId equals c.EventId
                                            where a.AssemblyId == model.AssemblyCode
                                                && a.SessionId == model.SessionCode
                                                && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                && b.DeptSubmittedBy != null
                                                && b.DeptSubmittedDate != null
                                                && a.DeptActivePaperId == b.PaperLaidTempId
                                                && a.MinistryId == model.MinistryId
                                                && c.PaperCategoryTypeId == paperCategoryId
                                                && a.MinistryId == model.MinistryId
                                                && b.MinisterSubmittedBy != null
                                                && b.MinisterSubmittedBy != null

                                            select b).Count();

                return noticesentReplycount;
            }

        }
        private static int GetCountOthersPending(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            int paperCategoryId = 5;
            using (var ctx = new BillPaperLaidContext())
            {
                var noticependingReplycount = (from a in ctx.tPaperLaidV
                                               join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                               join c in ctx.mEvent on a.EventId equals c.EventId
                                               where a.AssemblyId == model.AssemblyCode
                                                   && a.SessionId == model.SessionCode
                                                   && (a.DeptActivePaperId != null)
                                                   && b.DeptSubmittedBy != null
                                                   && b.DeptSubmittedDate != null
                                                   && a.MinistryId == model.MinistryId
                                                   && c.PaperCategoryTypeId == paperCategoryId
                                                   && b.MinisterSubmittedBy == null
                                                   && b.MinisterSubmittedBy == null

                                               select b).Count();

                return noticependingReplycount;
            }

        }

        //Bills
        private static int GetCountBillsSent(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            using (var ctx = new BillPaperLaidContext())
            {
                var noticesentReplycount = (from a in ctx.tPaperLaidV
                                            join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                            join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                            where a.AssemblyId == model.AssemblyCode
                                                && a.SessionId == model.SessionCode
                                                && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0)
                                                && b.DeptSubmittedBy != null
                                                && b.DeptSubmittedDate != null
                                                && a.DeptActivePaperId == b.PaperLaidTempId
                                                && a.MinistryId == model.MinistryId
                                                && b.MinisterSubmittedBy != null
                                                && b.MinisterSubmittedBy != null

                                            select b).Count();

                return noticesentReplycount;
            }

        }
        private static int GetCountBillsPending(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            using (var ctx = new BillPaperLaidContext())
            {
                var noticependingReplycount = (from a in ctx.tPaperLaidV
                                               join b in ctx.tPaperLaidTemp on a.PaperLaidId equals b.PaperLaidId
                                               join c in ctx.tBillRegister on a.PaperLaidId equals c.PaperLaidId
                                               where a.AssemblyId == model.AssemblyCode
                                                   && a.SessionId == model.SessionCode
                                                   && (a.DeptActivePaperId != null)
                                                   && b.DeptSubmittedBy != null
                                                   && b.DeptSubmittedDate != null
                                                   && a.MinistryId == model.MinistryId
                                                   && b.MinisterSubmittedBy == null
                                                   && b.MinisterSubmittedBy == null

                                               select b).Count();

                return noticependingReplycount;
            }

        }

        static object GetAllStarredQuestions(object param)
        {
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            tPaperLaidV model = param as tPaperLaidV;


            List<QuestionModelCustom> model1 = new List<QuestionModelCustom>();


            var PendingForReply = (from questions in pCtxt.tQuestions
                                   join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                   join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                   where questions.QuestionType == model.QuestionTypeId
                                   && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                   && paperLaid.MinistryId == model.MinistryId
                                   && questions.PaperLaidId != null
                                   && paperLaidTemp.DeptSubmittedBy == null
                                   && paperLaidTemp.DeptSubmittedDate == null
                                   && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                   select new QuestionModelCustom
                                   {
                                       QuestionID = questions.QuestionID,
                                       QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                       Subject = paperLaid.Title,
                                       PaperLaidId = questions.PaperLaidId,
                                       Version = paperLaidTemp.Version,
                                       DeptActivePaperId = paperLaid.DeptActivePaperId,
                                       DiaryNumber = questions.DiaryNumber,
                                       Status = (int)QuestionDashboardStatus.DraftReplies,
                                       PaperSent = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                       MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                       DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                   }).ToList();


            foreach (var item in PendingForReply)
            {
                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }

                else
                {
                    item.DesiredLayingDate = "";
                }
            }

            var InboxForReply = (from questions in pCtxt.tQuestions
                                 where questions.QuestionType == model.QuestionTypeId
                                 && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                 && questions.MinistryId == model.MinistryId
                                 && questions.PaperLaidId == null
                                 select new QuestionModelCustom
                                 {
                                     QuestionID = questions.QuestionID,
                                     QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                     Subject = questions.Subject,
                                     PaperLaidId = questions.PaperLaidId,
                                     IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                     IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                     ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                     IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                     Status = (int)QuestionDashboardStatus.DraftReplies,
                                     DiaryNumber = questions.DiaryNumber,
                                     MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                     DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                 }).ToList();

            foreach (var item in InboxForReply)
            {

                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }

                else
                {
                    item.DesiredLayingDate = "";
                }
            }

            //Get Submitted UnstaredQuestion.



            var RepliesSent = (from questions in pCtxt.tQuestions
                               join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                               join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                               where questions.QuestionType == model.QuestionTypeId && questions.MinistryId == model.MinistryId
                                   && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                   && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null
                                   && paperLaidTemp.DeptSubmittedDate != null
                                   && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                               select new QuestionModelCustom
                               {
                                   QuestionID = questions.QuestionID,
                                   QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                   Subject = paperLaid.Title,
                                   PaperLaidId = questions.PaperLaidId,
                                   Version = paperLaidTemp.Version,
                                   DeptActivePaperId = paperLaid.DeptActivePaperId,
                                   IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                   IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                   ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                   IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                   DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                                   PaperSent = paperLaidTemp.SignedFilePath,
                                   FileName = paperLaidTemp.FileName,
                                   Status = (int)QuestionDashboardStatus.RepliesSent,
                                   DiaryNumber = questions.DiaryNumber,
                                   MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                   DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                               }).ToList();


            foreach (var item in RepliesSent)
            {

                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }

                else
                {
                    item.DesiredLayingDate = "";
                }

                if (item.DeptSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;
                }

                else
                {
                    item.DepartmentSubmittedDate = "";
                }



            }

            model1.AddRange(InboxForReply);
            model1.AddRange(PendingForReply);
            model1.AddRange(RepliesSent);

            model.ResultCount = PendingForReply.Count() + RepliesSent.Count() + InboxForReply.Count();
            model.tQuestionModel = model1;
            model.tQuestionModel = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;
        }

        static object GetAllUnstaredQuestionByType(object param)
        {

            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            tPaperLaidV model = param as tPaperLaidV;
            // int paperCategoryId = 5;

            List<QuestionModelCustom> model1 = new List<QuestionModelCustom>();



            var PendingForReply = (from questions in pCtxt.tQuestions
                                   join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                   join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                   where questions.QuestionType == model.QuestionTypeId
                                   && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                   && paperLaid.MinistryId == model.MinistryId
                                   && questions.PaperLaidId != null
                                   && paperLaidTemp.DeptSubmittedBy == null
                                   && paperLaidTemp.DeptSubmittedDate == null
                                   && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                   select new QuestionModelCustom
                                   {
                                       QuestionID = questions.QuestionID,
                                       QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                       Subject = paperLaid.Title,
                                       PaperLaidId = questions.PaperLaidId,
                                       Version = paperLaidTemp.Version,
                                       IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                       IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                       ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                       IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                       DeptActivePaperId = paperLaid.DeptActivePaperId,
                                       Status = (int)QuestionDashboardStatus.DraftReplies,
                                       PaperSent = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                       DiaryNumber = questions.DiaryNumber,
                                       MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                       DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                   }).ToList();


            foreach (var item in PendingForReply)
            {


                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }
                else
                {
                    item.DesiredLayingDate = "";
                }

            }

            var InboxForReply = (from questions in pCtxt.tQuestions
                                 where questions.QuestionType == model.QuestionTypeId
                                 && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                 && questions.MinistryId == model.MinistryId
                                 && questions.PaperLaidId == null
                                 select new QuestionModelCustom
                                 {
                                     QuestionID = questions.QuestionID,
                                     QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                     Subject = questions.Subject,
                                     IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                     IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                     ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                     IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                     PaperLaidId = questions.PaperLaidId,
                                     Status = (int)QuestionDashboardStatus.DraftReplies,
                                     DiaryNumber = questions.DiaryNumber,
                                     MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                     DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                 }).ToList();


            foreach (var item in InboxForReply)
            {


                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }
                else
                {
                    item.DesiredLayingDate = "";
                }

            }

            //Get Submitted UnstaredQuestion.

            var RepliesSent = (from questions in pCtxt.tQuestions
                               join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                               join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                               where questions.QuestionType == model.QuestionTypeId && paperLaid.MinistryId == model.MinistryId
                                   && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                   && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null
                                   && paperLaidTemp.DeptSubmittedDate != null
                                   && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                               select new QuestionModelCustom
                               {
                                   QuestionID = questions.QuestionID,
                                   QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                   Subject = paperLaid.Title,
                                   PaperLaidId = questions.PaperLaidId,
                                   Version = paperLaidTemp.Version,
                                   DeptActivePaperId = paperLaid.DeptActivePaperId,
                                   IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                   IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                   ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                   IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                   DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                                   PaperSent = paperLaidTemp.SignedFilePath,
                                   FileName = paperLaidTemp.FileName,
                                   DiaryNumber = questions.DiaryNumber,
                                   Status = (int)QuestionDashboardStatus.RepliesSent,
                                   MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                   DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                               }).ToList();

            foreach (var item in RepliesSent)
            {
                if (item.DeptSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;
                }
                else
                {
                    item.DepartmentSubmittedDate = "";
                }

                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }
                else
                {
                    item.DesiredLayingDate = "";
                }

            }

            model1.AddRange(PendingForReply);
            model1.AddRange(RepliesSent);
            model1.AddRange(InboxForReply);

            model.ResultCount = PendingForReply.Count() + RepliesSent.Count() + InboxForReply.Count();
            model.tQuestionModel = model1;
            model.tQuestionModel = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;
        }

        static object GetAllOtherPaperLaid(object param)
        {

            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();

            List<MinisterPaperMovementModel> model1 = new List<MinisterPaperMovementModel>();

            int paperCategoryId = 5;
            int? EventId = model.PaperCategoryTypeId;


            var PendingForReply = (from PaperLaidV in db.tPaperLaidV
                                   join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                                   join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                                   join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                                   //join Questions in db.tQuestions on PaperLaidV.PaperLaidId equals Questions.PaperLaidId
                                   where
                                    Event.PaperCategoryTypeId == paperCategoryId
                                   && PaperLaidTemp.DeptSubmittedBy != null
                                   && PaperLaidTemp.DeptSubmittedDate != null
                                    && PaperLaidTemp.MinisterSubmittedBy == null
                                    && PaperLaidTemp.MinisterSubmittedDate == null
                                     && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                                     && ministery.MinistryID == model.MinistryId
                                     && PaperLaidV.AssemblyId == model.AssemblyId
                                      && PaperLaidV.SessionId == model.SessionId
                                   select new MinisterPaperMovementModel
                                   {
                                       DeptActivePaperId = PaperLaidV.DeptActivePaperId,
                                       PaperLaidId = PaperLaidV.PaperLaidId,
                                       EventName = Event.EventName,
                                       EventId = Event.EventId,
                                       Title = PaperLaidV.Title,
                                       DeparmentName = PaperLaidV.DeparmentName,
                                       MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                                       PaperTypeID = (int)PaperLaidV.PaperLaidId,
                                       actualFilePath = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                       PaperCategoryTypeId = Event.PaperCategoryTypeId,
                                       BusinessType = Event.EventName,
                                       Status = (int)MinisterDashboardStatus.PendingForReply,
                                       version = PaperLaidTemp.Version,
                                       PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == PaperLaidV.EventId select p.Name).FirstOrDefault(),
                                       // MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                       MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),


                                   }).ToList();


            //Get Submitted OtherPaperLaid.


            var RepliesSent = (from PaperLaidV in db.tPaperLaidV
                               join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                               join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                               join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                               where
                                Event.PaperCategoryTypeId == paperCategoryId
                               && PaperLaidTemp.DeptSubmittedBy != null
                               && PaperLaidTemp.DeptSubmittedDate != null
                                && PaperLaidTemp.MinisterSubmittedBy != null
                                && PaperLaidTemp.MinisterSubmittedDate != null
                                 && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                                 && ministery.MinistryID == model.MinistryId
                                  && PaperLaidV.AssemblyId == model.AssemblyId
                                      && PaperLaidV.SessionId == model.SessionId
                               select new MinisterPaperMovementModel
                               {
                                   DeptActivePaperId = PaperLaidV.DeptActivePaperId,
                                   PaperLaidId = PaperLaidV.PaperLaidId,
                                   EventName = Event.EventName,
                                   EventId = Event.EventId,
                                   Title = PaperLaidV.Title,
                                   DeparmentName = PaperLaidV.DeparmentName,
                                   MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                                   PaperTypeID = (int)PaperLaidV.PaperLaidId,
                                   actualFilePath = PaperLaidTemp.SignedFilePath,
                                   PaperCategoryTypeId = Event.PaperCategoryTypeId,
                                   BusinessType = Event.EventName,
                                   version = PaperLaidTemp.Version,

                                   Status = (int)MinisterDashboardStatus.RepliesSent,
                                   DeptSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                                   PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == PaperLaidV.EventId select p.Name).FirstOrDefault(),
                                   MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),


                               }).ToList();

            foreach (var item in RepliesSent)
            {
                if (item.DeptSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;
                }
                else
                {
                    item.DepartmentSubmittedDate = "";
                }

            }

            model1.AddRange(PendingForReply);
            model1.AddRange(RepliesSent);

            model.ResultCount = PendingForReply.Count() + RepliesSent.Count();
            model.MinisterPendingList = model1;
            model.MinisterPendingList = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;
        }

        static tPaperLaidV GetPaperLaidByPaperLaidId(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidMinisterContext plCntxt = new PaperLaidMinisterContext();
            paperLaid = (from mdl in plCntxt.tPaperLaidV where mdl.PaperLaidId == paperLaid.PaperLaidId select mdl).FirstOrDefault();

            return paperLaid;
        }

        static object GetQuestionByPaperLaidID(object param)
        {
            PaperLaidMinisterContext DBContext = new PaperLaidMinisterContext();
            tQuestion questionModel = param as tQuestion;

            var query = (from questions in DBContext.tQuestions
                         where questions.PaperLaidId == questionModel.PaperLaidId && questions.DepartmentID == questionModel.DepartmentID
                         select questions).FirstOrDefault();

            return query;
        }

        public static object UpdateMinisterActivePaperId(object param)
        {
            string obj = param as string;
            string[] obja = obj.Split(',');

            foreach (var item in obja)
            {

                using (var obj1 = new PaperLaidMinisterContext())
                {
                    long id = Convert.ToInt16(item);

                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
                                        where PaperLaidModel.PaperLaidId == id
                                        select PaperLaidModel).FirstOrDefault();
                    if (PaperLaidObj != null)
                    {
                        PaperLaidObj.MinisterActivePaperId = PaperLaidObj.DeptActivePaperId;
                    }
                    //obj.tBillRegisterVs.Add(model);
                    obj1.SaveChanges();
                    obj1.Close();
                }
            }


            string meg = "";
            return meg;
        }


        static tPaperLaidV GetDepartmentNameMinistryDetailsByMinisterID(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidMinisterContext plCntxt = new PaperLaidMinisterContext();
            var mMinistry = (from mdl in plCntxt.mMinistry where mdl.MinistryID == paperLaid.MinistryId select mdl).SingleOrDefault();
            paperLaid.MinistryName = mMinistry.MinistryName;
            paperLaid.MinistryNameLocal = mMinistry.MinistryNameLocal;
            var DepartmentName = (from mod in plCntxt.mDepartment where paperLaid.DepartmentId == mod.deptId select mod.deptname).SingleOrDefault();
            paperLaid.DeparmentName = DepartmentName;
            return paperLaid;

        }

        static object GetQuetionEventID(object param)
        {
            mEvent mEvents = param as mEvent;
            PaperLaidMinisterContext qxt = new PaperLaidMinisterContext();
            int paperCategoryTypeId = (from mdl in qxt.mPaperCategoryType
                                       where mdl.Name.Contains("ques")
                                       select mdl.PaperCategoryTypeId).SingleOrDefault();

            mEvents = (from mdl in qxt.mEvent
                       where mdl.PaperCategoryTypeId == paperCategoryTypeId && mdl.EventName.Contains("hour")
                       select mdl).SingleOrDefault();
            return mEvents;
        }

        static tPaperLaidV GetPaperLaidById(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidMinisterContext plCntxt = new PaperLaidMinisterContext();
            paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV where mdl.PaperLaidId == paperLaid.PaperLaidId select mdl).ToList();

            return paperLaid;

        }

        static object GetMinisterMinistryName(object param)
        {
            string DeptId = param.ToString();
            PaperLaidMinisterContext ctxt = new PaperLaidMinisterContext();

            var query = (from mdl in ctxt.mMInistryDepartment
                         where mdl.DeptID == DeptId
                         join mdl1 in ctxt.mMinistry on mdl.MinistryID equals mdl1.MinistryID
                         select new mMinisteryMinisterModel
                         {
                             MinistryID = mdl1.MinistryID
                         }).SingleOrDefault();
            return query;

        }

        static tPaperLaidV GetPaperLaidByEventId(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidMinisterContext plCntxt = new PaperLaidMinisterContext();
            paperLaid.ListtPaperLaidV = (from mdl in plCntxt.tPaperLaidV where mdl.EventId == paperLaid.EventId && mdl.MinisterActivePaperId != null && mdl.DeptActivePaperId != null && mdl.LOBRecordId == null orderby mdl.PaperlaidPriority ascending select mdl).ToList();

            return paperLaid;

        }

        static tPaperLaidTemp GetPaperLaidTempById(object param)
        {
            tPaperLaidTemp paperLaid = param as tPaperLaidTemp;
            PaperLaidMinisterContext plCntxt = new PaperLaidMinisterContext();
            var result = (from mdl in plCntxt.tPaperLaidTemp where mdl.PaperLaidTempId == paperLaid.PaperLaidTempId select mdl).FirstOrDefault();
            //where mdl.PaperLaidTempId == paperLaid.PaperLaidTempId select mdl).FirstOrDefault()
            paperLaid = result;
            return paperLaid;

        }


        static string UpdateLOBRecordIdIntPaperLaidVS(object param)
        {
            tPaperLaidV paperLaid = param as tPaperLaidV;
            PaperLaidMinisterContext plCntxt = new PaperLaidMinisterContext();

            using (var obj = new PaperLaidMinisterContext())
            {
                var PaperLaidObj = obj.tPaperLaidV.Where(m => m.LOBRecordId == paperLaid.LOBRecordId).FirstOrDefault();
                if (PaperLaidObj != null)
                {
                    PaperLaidObj.LOBRecordId = null;
                    PaperLaidObj.LOBPaperTempId = null;
                }
                obj.SaveChanges();
                obj.Close();
            }


            using (var obj = new PaperLaidMinisterContext())
            {
                var PaperLaidObj = obj.tPaperLaidV.Where(m => m.PaperLaidId == paperLaid.PaperLaidId).FirstOrDefault();
                if (PaperLaidObj != null)
                {
                    PaperLaidObj.LOBRecordId = paperLaid.LOBRecordId;
                    PaperLaidObj.DesireLayingDate = paperLaid.DesireLayingDate;
                    PaperLaidObj.LOBPaperTempId = paperLaid.LOBPaperTempId;
                }
                //obj.tBillRegisterVs.Add(model);
                obj.SaveChanges();
                obj.Close();
            }
            return null;
        }


        static List<mPaperCategoryType> GetAllPaperCategoryType(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();

            mPaperCategoryType model = param as mPaperCategoryType;


            var query = (from dist in db.mPaperCategoryType
                         select dist);

            string searchText = (model.Name != null && model.Name != "") ? model.Name.ToLower() : "";
            if (searchText != "")
            {
                query = query.Where(a =>
                    a.Name != null && a.Name.ToLower().Contains(searchText));

            }

            return query.ToList();
        }


        static object GetPaperLaidDepartmentDetails(object param)
        {

            tPaperLaidV model = new tPaperLaidV();
            //    AssemblyContext assemblyContext = new AssemblyContext();
            DepartmentContext departmentContext = new DepartmentContext();
            //    MinisteryContext ministryContext = new MinisteryContext();
            SessionContext SessionContext = new SessionContext();

            //model.mAssembly = (from a in assemblyContext.mAssemblies
            //                   orderby a.AssemblyID descending
            //                   select a).ToList();
            //mAssembly obj = new mAssembly();
            //obj.AssemblyID = 0;
            //obj.AssemblyName = "Select Assembly";
            //model.mAssembly.Add(obj);

            //model.mMinister = (from minister in ministryContext.mMinisteryVS
            //                   orderby minister.MinisteryName ascending
            //                   select minister).ToList();

            //mMinisteryVS ministerModel = new mMinisteryVS();
            //ministerModel.MinisteryID = 0;
            //ministerModel.MinisteryName = "Select Minister";
            //model.mMinister.Add(ministerModel);

            //model.mDepartment = (from department in departmentContext.objmDepartment
            //                     orderby department.deptname ascending
            //                     select department).ToList();

            //mDepartment mdepartment = new mDepartment();
            //mdepartment.deptId = "0";
            //mdepartment.deptname = "Select Department";
            //model.mDepartment.Add(mdepartment);

            //model.mSession = (from session in SessionContext.mSession
            //                  orderby session.SessionName ascending
            //                  select session).ToList();

            //mSession mSession = new mSession();
            //mSession.SessionID = 0;
            //mSession.SessionName = "Select Session";
            //model.mSession.Add(mSession);



            return model;
        }

        // Paper pending list
        static List<PaperMovementModel> GetPendingDepartmentPaperLaidList(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            PaperMovementModel model = param as PaperMovementModel;

            int? EventId = null;
            int? PaperCategoryTypeId = null;
            if (model.EventId != 0 && model.PaperCategoryTypeId != 0)
            {
                EventId = model.EventId;
            }
            else if (model.PaperCategoryTypeId != 0)
            {
                PaperCategoryTypeId = model.PaperCategoryTypeId;
            }

            var query = (from m in db.tPaperLaidV
                         join c in db.mEvent on m.EventId equals c.EventId
                         join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         where m.CommitteeId == null && t.DeptSubmittedDate == null
                         && t.DeptSubmittedBy == null
                         && (!EventId.HasValue || m.EventId == model.EventId)
                         && m.AssemblyId == model.AssemblyId
                         && m.SessionId == model.SessionId
                         select new PaperMovementModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             DeparmentName = m.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)m.PaperLaidId,
                             actualFilePath = t.FilePath + t.FileName,
                             PaperCategoryTypeId = c.PaperCategoryTypeId,
                             BusinessType = c.EventName,
                             version = t.Version,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault()

                         }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            foreach (var item in query)
            {
                switch (item.PaperCategoryTypeId)
                {
                    case 1:
                        item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                        break;
                    case 2:
                        item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                        break;
                    case 4:
                        item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                        break;
                    default:
                        break;
                }
            }

            if (PaperCategoryTypeId != null)
            {
                var filterQuery = query.Where(foo => foo.PaperCategoryTypeId == PaperCategoryTypeId);
                return filterQuery.ToList();
            }
            return query.ToList();
        }


        // Nitin code

        static object GetSessionDates(object param)
        {
            PaperLaidMinisterContext plCntxt = new PaperLaidMinisterContext();
            mSessionDate sessionDates = new mSessionDate();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            paperLaid.mSessionDates = (from mdl2 in plCntxt.mSessionDates where mdl2.SessionId == paperLaid.SessionId select mdl2).ToList();
            foreach (var item in paperLaid.mSessionDates)
            {
                item.DisplayDate = item.SessionDate.ToString("dd/MM/yyyy");

            }
            mSessionDate obj = new mSessionDate();
            obj.Id = 0;
            obj.DisplayDate = "Select Desired Date to be Lay";
            paperLaid.mSessionDates.Add(obj);

            return paperLaid.mSessionDates;

        }
        static object GetDocumentType(object param)
        {
            PaperLaidMinisterContext plCntxt = new PaperLaidMinisterContext();
            mDocumentType documents = new mDocumentType();
            mSessionDate sessionDates = new mSessionDate();
            tPaperLaidV paperLaid = param as tPaperLaidV;
            paperLaid.mDocumentTypes = (from mdl in plCntxt.mDocumentTypes select mdl).ToList();
            mDocumentType obj = new mDocumentType();
            obj.DocumentTypeID = 0;
            obj.DocumentType = "Select Paper Type";
            paperLaid.mDocumentTypes.Add(obj);
            return paperLaid.mDocumentTypes;

        }

        static object GetPaperLaidCategoryDetails(object param)
        {
            int[] ids = (int[])param;


            int id = Convert.ToInt32(ids[0]);
            int noticeTypeId = ids[1];
            PaperLaidMinisterContext cnxt = new PaperLaidMinisterContext();
            tPaperLaidV model = new tPaperLaidV();

            string categoryName = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == id select mdl.Name).SingleOrDefault();

            if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                model.tQuestioType = (from mdl in cnxt.tQuestionTypes select mdl).ToList();
                tQuestionType obj = new tQuestionType();
                obj.QuestionTypeName = "select Question Type";
                obj.QuestionTypeId = 0;
                model.tQuestioType.Add(obj);
                model.categoryType = "question";
            }
            else if (categoryName.IndexOf("notice", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                model.tMemberNotice = (from mdl in cnxt.tMemberNotice where mdl.NoticeTypeID == noticeTypeId select mdl).ToList();
                tMemberNotice ob = new tMemberNotice();
                ob.NoticeId = 0;
                ob.NoticeNumber = "Select Notice Number";
                model.tMemberNotice.Add(ob);
                model.categoryType = "notice";
            }
            else if (categoryName.IndexOf("bill", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                model.mBillTypes = (from mdl in cnxt.mBillTypes select mdl).ToList();
                model.categoryType = "bill";
            }
            else
            { model.categoryType = "other"; }
            return model;
        }

        static object GetFileSaveDetails(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext cnxt = new PaperLaidMinisterContext();
            int? catedoryId = (from mdl in cnxt.mEvent
                               where mdl.EventId == model.EventId
                               select mdl.PaperCategoryTypeId).SingleOrDefault();
            string respectivePaper = (from mdl in cnxt.mPaperCategoryType where mdl.PaperCategoryTypeId == catedoryId select mdl.Name).SingleOrDefault();

            var FileUpdate = (from paperLaidTemp in cnxt.tPaperLaidTemp
                              where paperLaidTemp.PaperLaidId == model.PaperLaidId
                                  && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
                              select paperLaidTemp).FirstOrDefault();
            if (FileUpdate != null)
            {
                model.Count = Convert.ToInt32(FileUpdate.Version);
            }
            else
            {
                int count = (from mdl in cnxt.tPaperLaidTemp where mdl.PaperLaidId == model.PaperLaidId select mdl.PaperLaidId).Count();
                int newVersion = count + 1;
                model.Count = newVersion;
            }

            model.RespectivePaper = respectivePaper;

            return model;
        }


        static tPaperLaidV GetQuestionNumber(object param)
        {
            tPaperLaidV mod = param as tPaperLaidV;
            // int questionId = Convert.ToInt32(param);
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            tPaperLaidV paperMdel = new tPaperLaidV();
            paperMdel.tQuestion = (from mdl in pCtxt.tQuestions
                                   where mdl.QuestionType == mod.QuestionTypeId && mdl.DepartmentID == mod.DepartmentId
                                       && mdl.PaperLaidId == null
                                   select mdl).ToList();
            return paperMdel;
        }

        static object GetQuestionDetails(object param)
        {
            tQuestion tQuestions = new tQuestion();

            tQuestion model = param as tQuestion;

            //int questionNumber = Convert.ToInt32(param);
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            tPaperLaidV paperMdel = new tPaperLaidV();
            tQuestions = (from mdl in pCtxt.tQuestions
                          where mdl.QuestionNumber == model.QuestionNumber
                              && mdl.QuestionID == model.QuestionID && mdl.QuestionType == model.QuestionType
                          select mdl).FirstOrDefault();
            return tQuestions;
        }
        static object GetNoticeDetails(object param)
        {
            string noticeNumber = param.ToString();
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            tMemberNotice model = new tMemberNotice();
            model = (from mdl in pCtxt.tMemberNotice where mdl.NoticeNumber == noticeNumber select mdl).SingleOrDefault();
            model.Subject = Regex.Replace(model.Subject, @"<[^>]+>|&nbsp;", "").Trim();
            model.Notice = Regex.Replace(model.Notice, @"<[^>]+>|&nbsp;", "").Trim();
            string[] date = model.NoticeDate.ToString().Split(' ');
            model.DateNotice = date[0];
            return model;
        }
        static object CheckBillNUmber(object param)
        {
            string billNumber = param.ToString();
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            tBillRegister model = new tBillRegister();
            model = (from mdl in pCtxt.tBillRegister where mdl.BillNumber == billNumber select mdl).SingleOrDefault();
            return model;
        }


        static object GetNotices(object param)
        {
            int noticeId = Convert.ToInt32(param);
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            tPaperLaidV paperMdel = new tPaperLaidV();
            paperMdel.tMemberNotice = (from mdl in pCtxt.tMemberNotice where mdl.NoticeTypeID == noticeId && mdl.PaperLaidId == null select mdl).ToList();
            return paperMdel;
        }
        static object SubmittPaperLaidEntry(object param)
        {
            if (null == param)
            {
                return null;
            }

            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();
            tPaperLaidV submitPaperEntry = param as tPaperLaidV;

            if (submitPaperEntry.PaperLaidId == 0)
            {
                paperLaidCntxtDB.tPaperLaidV.Add(submitPaperEntry);
            }
            else
            {
                paperLaidCntxtDB.tPaperLaidV.Attach(submitPaperEntry);
                paperLaidCntxtDB.Entry(submitPaperEntry).State = EntityState.Modified;
            }
            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperEntry;

        }
        static object SaveDetailsInTempPaperLaid(object param)
        {

            if (null == param)
            {
                return null;
            }
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;

            var query = (from paperLaidTemp in paperLaidCntxtDB.tPaperLaidTemp
                         where paperLaidTemp.PaperLaidId == submitPaperTemp.PaperLaidId
                         && paperLaidTemp.DeptSubmittedBy == null && paperLaidTemp.DeptSubmittedDate == null
                         select paperLaidTemp).SingleOrDefault();

            if (query != null)
            {
                paperLaidCntxtDB.tPaperLaidTemp.Attach(query);
                query.FileName = submitPaperTemp.FileName;
                query.FilePath = submitPaperTemp.FilePath;
            }
            else
            {
                submitPaperTemp.DeptSubmittedDate = null;
                paperLaidCntxtDB.tPaperLaidTemp.Add(submitPaperTemp);
            }

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;
        }

        static object UpdateDeptActiveId(object param)
        {

            if (null == param)
            {
                return null;
            }
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
            var tPaperLaid = (from mdl in paperLaidCntxtDB.tPaperLaidV where mdl.PaperLaidId == submitPaperTemp.PaperLaidId select mdl).SingleOrDefault();
            if (tPaperLaid != null)
            {
                tPaperLaid.DeptActivePaperId = submitPaperTemp.PaperLaidTempId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return tPaperLaid;

        }
        static object UpdateQuestionPaperLaidId(object param)
        {

            if (null == param)
            {
                return null;
            }
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();

            tQuestion updatePaperLaid = param as tQuestion;
            var tQuestion = (from mdl in paperLaidCntxtDB.tQuestions where mdl.QuestionID == updatePaperLaid.QuestionID select mdl).SingleOrDefault();
            if (tQuestion != null)
            {
                tQuestion.PaperLaidId = updatePaperLaid.PaperLaidId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();

            }
            return tQuestion;

        }

        static object UpdateNoticePaperLaidId(object param)
        {
            if (null == param)
            {
                return null;
            }
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();

            tMemberNotice updatePaperLaid = param as tMemberNotice;
            var tMemberNotice = (from mdl in paperLaidCntxtDB.tMemberNotice where mdl.NoticeId == updatePaperLaid.NoticeId select mdl).SingleOrDefault();
            if (tMemberNotice != null)
            {
                tMemberNotice.PaperLaidId = updatePaperLaid.PaperLaidId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }
            return tMemberNotice;

        }
        static object InsertBillDetails(object param)
        {
            if (null == param)
            { return null; }
            tBillRegister bills = new tBillRegister();
            tPaperLaidV mdl = param as tPaperLaidV;
            bills.PaperLaidId = (int)mdl.PaperLaidId;
            bills.BillDate = Convert.ToDateTime(mdl.BillDate);
            bills.BillNumber = mdl.BillNUmberValue + " of " + mdl.BillNumberYear;
            bills.AssemblyId = mdl.AssemblyCode;
            bills.SessionId = mdl.SessionCode;
            bills.MinisterId = mdl.MinistryId;
            bills.DepartmentId = mdl.DepartmentId;
            bills.BillTypeId = mdl.BillTypeId;
            bills.IsBillAct = false;
            bills.IsBillReturn = false;
            bills.IsBillSigned = false;
            bills.IsCirculation = false;
            bills.IsClauseConsideration = false;
            bills.IsElicitingOpinion = false;
            bills.IsFinancialBillRule1 = false;
            bills.IsFinancialBillRule2 = false;
            bills.IsForwordedToMinister = false;
            bills.IsIntentioned = false;
            bills.IsIntentionedToMinister = false;
            bills.IsIntentionReturn = false;
            bills.IsIntentionSigned = false;
            bills.IsMoneyBill = false;
            bills.IsRecommendedArticle = false;
            bills.IsReferSelectCommitte = false;
            bills.IsSubmitted = false;
            bills.IsThirdReading = false;
            bills.IsWithdrawal = false;
            bills.ShortTitle = mdl.Description;
            using (PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext())
            {
                bills = pCtxt.tBillRegister.Add(bills);
                pCtxt.SaveChanges();
                pCtxt.Close();
            }
            return bills;
        }
        static object SubmitPendingDepartmentPaperLaid(object param)
        {
            tPaperLaidTemp mdl = param as tPaperLaidTemp;
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();

            var tPaperLaidTemps = (from md in paperLaidCntxtDB.tPaperLaidTemp
                                   where md.PaperLaidId == mdl.PaperLaidId && md.Version ==
                                       (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == mdl.PaperLaidId select bdMax.Version).Max()
                                   select md).SingleOrDefault();
            //var tPaperLaidTemps = (from model in paperLaidCntxtDB.tPaperLaidTemp
            //                       where model.PaperLaidId == mdl.PaperLaidId && model.PaperLaidTempId == query.PaperLaidTempId
            //                       select model).SingleOrDefault();

            if (tPaperLaidTemps != null)
            {
                tPaperLaidTemps.DeptSubmittedDate = mdl.DeptSubmittedDate;
                tPaperLaidTemps.DeptSubmittedBy = mdl.DeptSubmittedBy;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }


            return tPaperLaidTemps;
        }

        static object EditPendingPaperLaidDetials(object param)
        {
            tPaperLaidV mdl = param as tPaperLaidV;
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();
            mdl = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == mdl.PaperLaidId select model).SingleOrDefault();
            var fileInfo = (from model in paperLaidCntxtDB.tPaperLaidTemp
                            where model.PaperLaidId == mdl.PaperLaidId
                            where model.DeptSubmittedBy == null && model.DeptSubmittedDate == null
                            select model).SingleOrDefault();
            mdl.FileName = fileInfo.FileName;
            mdl.FilePath = fileInfo.FilePath + fileInfo.FileName;
            return mdl;
        }


        static object UpdatePaperLaidEntry(object param)
        {
            tPaperLaidV updatedDetails = param as tPaperLaidV;
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();
            var existingDetails = (from model in paperLaidCntxtDB.tPaperLaidV where model.PaperLaidId == updatedDetails.PaperLaidId select model).SingleOrDefault();

            if (existingDetails != null)
            {
                existingDetails.EventId = updatedDetails.EventId;
                existingDetails.MinistryId = updatedDetails.MinistryId;
                existingDetails.DepartmentId = updatedDetails.DepartmentId;
                existingDetails.SessionId = updatedDetails.SessionCode;
                existingDetails.PaperTypeID = updatedDetails.PaperTypeID;
                existingDetails.Title = updatedDetails.Title;
                existingDetails.Description = updatedDetails.Description;
                existingDetails.Remark = updatedDetails.Remark;
                existingDetails.PaperLaidId = updatedDetails.PaperLaidId;
                paperLaidCntxtDB.SaveChanges();
                paperLaidCntxtDB.Close();
            }
            return existingDetails;
        }
        static object UpdateDetailsInTempPaperLaid(object param)
        {

            if (null == param)
            {
                return null;
            }
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();
            tPaperLaidTemp submitPaperTemp = param as tPaperLaidTemp;
            var existingTPaperLaidTemp = (from mdl in paperLaidCntxtDB.tPaperLaidTemp where mdl.PaperLaidTempId == submitPaperTemp.PaperLaidTempId select mdl).SingleOrDefault();
            if (existingTPaperLaidTemp != null)
            {
                existingTPaperLaidTemp.FileName = submitPaperTemp.FileName;
                //    existingTPaperLaidTemp.FilePath = submitPaperTemp.FilePath;
            }

            paperLaidCntxtDB.SaveChanges();
            paperLaidCntxtDB.Close();
            return submitPaperTemp;
        }
        static object GetExisitngFileDetails(object param)
        {

            if (null == param)
            {
                return null;
            }
            PaperLaidMinisterContext paperLaidCntxtDB = new PaperLaidMinisterContext();
            tPaperLaidV details = param as tPaperLaidV;

            var existingTPaperLaidTemp = (from md in paperLaidCntxtDB.tPaperLaidTemp
                                          where md.PaperLaidId == details.PaperLaidId && md.Version ==
                                              (from bdMax in paperLaidCntxtDB.tPaperLaidTemp where bdMax.PaperLaidId == details.PaperLaidId select bdMax.Version).Max()
                                          select md).SingleOrDefault();
            if (existingTPaperLaidTemp != null)
            {
                return existingTPaperLaidTemp;
            }

            return null;
        }

        static tPaperLaidV UpcomingLOBByQuestionType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            var query = (from questions in pCtxt.tQuestions
                         join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                         where questions.QuestionType == model.QuestionTypeId && paperLaid.DepartmentId == model.DepartmentId
                             //New
                             && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId

                             && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
                             && paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
                             && paperLaid.DesireLayingDate > DateTime.Now && paperLaid.LaidDate == null && paperLaid.LOBRecordId != null
                             && paperLaid.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId
                         //paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                         select new PaperSendModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                             Subject = paperLaid.Title,
                             PaperLaidId = questions.PaperLaidId,
                             Version = paperLaidTemp.Version,
                             DeptActivePaperId = paperLaid.DeptActivePaperId,
                             DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                             PaperSent = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                             IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                             IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                             ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                             IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                             DeActivateFlag = questions.DeActivateFlag,
                             Status = (int)QuestionDashboardStatus.RepliesSent,
                             FileName = paperLaidTemp.FileName,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select mc.Name).FirstOrDefault(),
                             DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null

                         }).ToList();

            foreach (var item in query)
            {
                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }
                else
                {
                    item.DesiredLayingDate = "";
                }

            }


            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionSendModel = results;

            return model;
        }

        //static tPaperLaidV UnstarredUpcomingLOB(object param)
        //{
        //    tPaperLaidV model = param as tPaperLaidV;
        //    PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
        //    int totalRecords = 0;
        //    model.ResultCount = totalRecords;
        //    return model;
        //}

        // Paper pending list
        static List<PaperMovementModel> GetSubmittedPaperLaidList(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            PaperMovementModel model = param as PaperMovementModel;

            int? EventId = null;
            int? PaperCategoryTypeId = null;
            if (model.EventId != 0 && model.PaperCategoryTypeId != 0)
            {
                EventId = model.EventId;
            }
            else if (model.PaperCategoryTypeId != 0)
            {
                PaperCategoryTypeId = model.PaperCategoryTypeId;
            }

            var query = (from m in db.tPaperLaidV
                         join c in db.mEvent on m.EventId equals c.EventId
                         join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         where m.DeptActivePaperId == t.PaperLaidTempId
                         && (!EventId.HasValue || m.EventId == model.EventId)
                         && m.AssemblyId == model.AssemblyId
                         && m.SessionId == model.SessionId
                         select new PaperMovementModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             DeparmentName = m.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)m.PaperLaidId,
                             actualFilePath = t.FilePath + t.FileName,
                             PaperCategoryTypeId = c.PaperCategoryTypeId,
                             BusinessType = c.EventName,
                             version = t.Version,
                             DeptSubmittedBy = t.DeptSubmittedBy,
                             DeptSubmittedDate = t.DeptSubmittedDate,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault()

                         }).OrderBy(m => m.PaperLaidId).Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            foreach (var item in query)
            {
                switch (item.PaperCategoryTypeId)
                {
                    case 1:
                        item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                        break;
                    case 2:
                        item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                        break;
                    case 4:
                        item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                        break;
                    default:
                        break;
                }
            }

            if (PaperCategoryTypeId != null)
            {
                var filterQuery = query.Where(foo => foo.PaperCategoryTypeId == PaperCategoryTypeId);
                return filterQuery.ToList();
            }

            return query.ToList();
        }

        static object DeletePaperLaidByID(object param)
        {
            tPaperLaidV tPaperLaid = param as tPaperLaidV;
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();

            //remove from tPaperLaidV

            var query = (from mdl in db.tPaperLaidV where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            db.tPaperLaidV.Remove(query);


            //Remove from tPaperLaidTemp

            var query1 = (from mdl in db.tPaperLaidTemp where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            db.tPaperLaidTemp.Remove(query1);

            //Remove From Bill if exist

            var query2 = (from mdl in db.tBillRegister where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            if (query2 != null)
            {
                db.tBillRegister.Remove(query2);
            }

            //Remove Id form tQuestion if exist

            var query3 = (from mdl in db.tQuestions where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            if (query3 != null)
            {
                query3.PaperLaidId = null;
            }
            //Remove Id form tMemberNotice if exist

            var query4 = (from mdl in db.tMemberNotice where mdl.PaperLaidId == tPaperLaid.PaperLaidId select mdl).SingleOrDefault();
            if (query4 != null)
            {
                query4.PaperLaidId = null;
            }
            db.SaveChanges();
            db.Close();
            return null;
        }
        static object UpdatePaperLaidFileByID(object param)
        {
            tPaperLaidTemp model = param as tPaperLaidTemp;

            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            if (model == null)
            {
                return null;
            }
            model.DeptSubmittedDate = null;
            model.DeptSubmittedBy = null;
            model = db.tPaperLaidTemp.Add(model);
            model.PaperLaidTempId = model.PaperLaidTempId;
            db.SaveChanges();
            db.Close();
            return model;
        }

        static object ShowPaperLaidDetailByID(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            PaperMovementModel model = param as PaperMovementModel;

            var query = (from tPaper in db.tPaperLaidV
                         where model.PaperLaidId == tPaper.PaperLaidId

                         join questions in db.tQuestions on tPaper.PaperLaidId equals questions.PaperLaidId

                         join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                         join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                         join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                         join tPaperTemp in db.tPaperLaidTemp on tPaper.DeptActivePaperId equals tPaperTemp.PaperLaidTempId
                         join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                         select new PaperMovementModel
                         {
                             EventName = mEvents.EventName,
                             MinisterName = mMinistrys.MinisterName + ", " + mMinistrys.MinistryName,
                             DeparmentName = mDept.deptname,
                             PaperLaidId = tPaper.PaperLaidId,
                             actualFilePath = tPaperTemp.FileName,
                             ReplyPathFileLocation = tPaperTemp.FilePath + tPaperTemp.FileName,
                             ProvisionUnderWhich = tPaper.ProvisionUnderWhich,
                             Title = tPaper.Title,
                             Description = tPaper.Description,
                             Remark = tPaper.Remark,
                             PaperCategoryTypeId = mEvents.PaperCategoryTypeId,
                             PaperCategoryTypeName = mPaperCategory.Name,
                             PaperSubmittedDate = tPaperTemp.DeptSubmittedDate,
                             LaidDate = tPaper.LaidDate,
                             QuestionTypeId = questions.QuestionType
                         }).FirstOrDefault();
            var categoryName = query.PaperCategoryTypeName;
            if (categoryName.IndexOf("question", 0, StringComparison.CurrentCultureIgnoreCase) != -1)
            {
                int? Number = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId select mdl.QuestionNumber).FirstOrDefault();

                //New
                var questionDet = (from mdl in db.tQuestions where mdl.PaperLaidId == model.PaperLaidId select mdl).FirstOrDefault();
                if (questionDet != null)
                {
                    query.MemberName = (from member in db.mMember
                                        where member.MemberCode == questionDet.MemberCode
                                        select member.Name).FirstOrDefault();
                }
                query.Number = Number.ToString();
            }
            return query;
        }

        static object GetFileVersion(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            PaperMovementModel paper = param as PaperMovementModel;
            tPaperLaidV model = new tPaperLaidV();

            model.Count = (from tPaperTemp in db.tPaperLaidTemp
                           where tPaperTemp.PaperLaidId == paper.PaperLaidId
                           select tPaperTemp.PaperLaidId).Count();
            model.Count = model.Count + 1;
            return model;
        }

        static object GetSubmittedPaperLaidByID(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();

            tPaperLaidV model = param as tPaperLaidV;

            model.DepartmentSubmitList = (from tPaper in db.tPaperLaidV
                                          where model.PaperLaidId == tPaper.PaperLaidId
                                          join mEvents in db.mEvent on tPaper.EventId equals mEvents.EventId
                                          join mMinistrys in db.mMinistry on tPaper.MinistryId equals mMinistrys.MinistryID
                                          join mDept in db.mDepartment on tPaper.DepartmentId equals mDept.deptId
                                          join tPaperTemp in db.tPaperLaidTemp on tPaper.PaperLaidId equals tPaperTemp.PaperLaidId
                                          join mPaperCategory in db.mPaperCategoryType on mEvents.PaperCategoryTypeId equals mPaperCategory.PaperCategoryTypeId
                                          select new PaperMovementModel
                                          {
                                              DeptSubmittedDate = tPaperTemp.DeptSubmittedDate,
                                              PaperTypeName = mPaperCategory.Name,
                                              actualFilePath = tPaperTemp.FilePath + tPaperTemp.FileName,
                                              FilePath = tPaperTemp.FilePath,
                                              DeptSubmittedBy = tPaperTemp.DeptSubmittedBy,
                                              PaperLaidId = model.PaperLaidId,
                                              DeptActivePaperId = tPaper.DeptActivePaperId,
                                              version = tPaperTemp.Version
                                          }).ToList();
            int i = 0;
            foreach (var mod in model.DepartmentSubmitList)
            {
                if (i == 0)
                {
                    model.FilePath = mod.FilePath;
                    model.PaperLaidId = mod.PaperLaidId;
                    i++;
                }
            }
            return model;
        }



        #region Newly Added (Ram)
        //Added code venkat for dynamic 
        static tPaperLaidV GetPendingQuestionsByType(object param)
        {
            //Added code venkat for dynamic 
            string DiaryNumber = "";
            string Subject = "";
            int? Name = 0;
            int? QNumber = 0;
           
            tPaperLaidV model = param as tPaperLaidV;
            if (model.DiaryNumber != "null" && model.DiaryNumber != "" && model.DiaryNumber != null)
            {
                DiaryNumber = model.DiaryNumber;
            }
            if (model.Subject != "null" && model.Subject != "" && model.Subject != null)
            {
                Subject = model.Subject;
            }
            if (model.QuestionNumber != null && model.QuestionNumber != 0)
            {
                QNumber = model.QuestionNumber;

            }
            if (model.MinID != null && model.MinID != 0)
            {
                Name = model.MinID;

            }

            if(model.RequestMessage == "ChiefMinisterAdmin")
            {
                string csv = Convert.ToString(model.MinistryId);
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

                List<QuestionModelCustom> model1 = new List<QuestionModelCustom>();

                var siteSettingData = (from a in pCtxt.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
                string FileStructurePath = siteSettingData.SettingValue;


                var query = (from questions in pCtxt.tQuestions
                             where questions.QuestionType == model.QuestionTypeId
                             && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                            // && questions.MinistryId == model.MinistryId
                             && questions.PaperLaidId == null
                             && (questions.QuestionStatus == (int)Questionstatus.QuestionSent)
                                 //Added code venkat for dynamic
                             && (DiaryNumber == "" || questions.DiaryNumber == DiaryNumber)
                             && (Subject == "" || questions.Subject.Contains(Subject))
                             && (QNumber == 0 || questions.QuestionNumber == QNumber)
                             && (Name == 0 || questions.MemberID == Name)
                             //
                             select new QuestionModelCustom
                             {
                                 QuestionID = questions.QuestionID,
                                 QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                 //QuestionNumber = questions.QuestionNumber,
                                 Subject = questions.Subject,
                                 PaperLaidId = questions.PaperLaidId,
                                 DeActivateFlag = questions.DeActivateFlag,
                                 //Version = paperLaidTemp.Version,
                                 //PaperSent = FileStructurePath + paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                 Status = (int)QuestionDashboardStatus.DraftReplies,
                                 IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                 IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                 ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                 IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                 DiaryNumber = questions.DiaryNumber,
                                 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                 //DesireLayingDate = questions.SessionDateId == null ? (DateTime?)null : (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
                             }).ToList();

                foreach (var item in query)
                {
                    if (item.DesireLayingDate != null)
                    {
                        var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                        var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                        var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                        item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                    }

                    else
                    {
                        item.DesiredLayingDate = "";
                    }

                }



                var InboxForReply = (from questions in pCtxt.tQuestions
                                     join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                     join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                     where questions.QuestionType == model.QuestionTypeId
                                     && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                     //&& questions.MinistryId == model.MinistryId
                                     && questions.PaperLaidId != null
                                     && paperLaidTemp.DeptSubmittedBy == null

                                     && (questions.QuestionStatus == (int)Questionstatus.QuestionSent)
                                         //Added code venkat for dynamic
                                     && (DiaryNumber == "" || questions.DiaryNumber == DiaryNumber)
                                     && (Subject == "" || questions.Subject.Contains(Subject))
                                     && (QNumber == 0 || questions.QuestionNumber == QNumber)
                                     && (Name == 0 || questions.MemberID == Name)
                                     //
                                     select new QuestionModelCustom
                                     {
                                         QuestionID = questions.QuestionID,
                                         QuestionNumber = questions.QuestionNumber,
                                         Subject = questions.Subject,
                                         PaperLaidId = questions.PaperLaidId,
                                         DeActivateFlag = questions.DeActivateFlag,
                                         Version = paperLaidTemp.Version,
                                         PaperSent = FileStructurePath + paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                         Status = (int)QuestionDashboardStatus.DraftReplies,
                                         IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                         IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                         ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                         IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                         DiaryNumber = questions.DiaryNumber,
                                         MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                         DesireLayingDate = questions.SessionDateId == null ? (DateTime?)null : (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
                                     }).ToList();


                foreach (var item in InboxForReply)
                {
                    if (item.DesireLayingDate != null)
                    {
                        var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                        var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                        var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                        item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                    }

                    else
                    {
                        item.DesiredLayingDate = "";
                    }

                }

                model1.AddRange(query);
                model1.AddRange(InboxForReply);

                model.ResultCount = query.Count() + InboxForReply.Count();

                model.MinInboxCount = InboxForReply.Count();
                model.tQuestionModel = model1;
                model.tQuestionModel = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();



            }
            else
            {

                string csv = Convert.ToString(model.MinistryId);
                IEnumerable<string> ids = csv.Split(',').Select(str => str);
                PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

                List<QuestionModelCustom> model1 = new List<QuestionModelCustom>();

                var siteSettingData = (from a in pCtxt.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
                string FileStructurePath = siteSettingData.SettingValue;


                var query = (from questions in pCtxt.tQuestions
                             where questions.QuestionType == model.QuestionTypeId
                             && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                             && questions.MinistryId == model.MinistryId
                             && questions.PaperLaidId == null
                             && (questions.QuestionStatus == (int)Questionstatus.QuestionSent)
                                 //Added code venkat for dynamic
                             && (DiaryNumber == "" || questions.DiaryNumber == DiaryNumber)
                             && (Subject == "" || questions.Subject.Contains(Subject))
                             && (QNumber == 0 || questions.QuestionNumber == QNumber)
                             && (Name == 0 || questions.MemberID == Name)
                             //
                             select new QuestionModelCustom
                             {
                                 QuestionID = questions.QuestionID,
                                 QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                 //QuestionNumber = questions.QuestionNumber,
                                 Subject = questions.Subject,
                                 PaperLaidId = questions.PaperLaidId,
                                 DeActivateFlag = questions.DeActivateFlag,
                                 //Version = paperLaidTemp.Version,
                                 //PaperSent = FileStructurePath + paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                 Status = (int)QuestionDashboardStatus.DraftReplies,
                                 IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                 IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                 ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                 IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                 DiaryNumber = questions.DiaryNumber,
                                 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                 //DesireLayingDate = questions.SessionDateId == null ? (DateTime?)null : (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
                             }).ToList();

                foreach (var item in query)
                {
                    if (item.DesireLayingDate != null)
                    {
                        var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                        var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                        var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                        item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                    }

                    else
                    {
                        item.DesiredLayingDate = "";
                    }

                }



                var InboxForReply = (from questions in pCtxt.tQuestions
                                     join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                     join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                     where questions.QuestionType == model.QuestionTypeId
                                     && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                     && questions.MinistryId == model.MinistryId
                                     && questions.PaperLaidId != null
                                     && paperLaidTemp.DeptSubmittedBy == null

                                     && (questions.QuestionStatus == (int)Questionstatus.QuestionSent)
                                         //Added code venkat for dynamic
                                     && (DiaryNumber == "" || questions.DiaryNumber == DiaryNumber)
                                     && (Subject == "" || questions.Subject.Contains(Subject))
                                     && (QNumber == 0 || questions.QuestionNumber == QNumber)
                                     && (Name == 0 || questions.MemberID == Name)
                                     //
                                     select new QuestionModelCustom
                                     {
                                         QuestionID = questions.QuestionID,
                                         QuestionNumber = questions.QuestionNumber,
                                         Subject = questions.Subject,
                                         PaperLaidId = questions.PaperLaidId,
                                         DeActivateFlag = questions.DeActivateFlag,
                                         Version = paperLaidTemp.Version,
                                         PaperSent = FileStructurePath + paperLaidTemp.FilePath + paperLaidTemp.FileName,
                                         Status = (int)QuestionDashboardStatus.DraftReplies,
                                         IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                         IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                         ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                         IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                         DiaryNumber = questions.DiaryNumber,
                                         MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                         DesireLayingDate = questions.SessionDateId == null ? (DateTime?)null : (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault()
                                     }).ToList();


                foreach (var item in InboxForReply)
                {
                    if (item.DesireLayingDate != null)
                    {
                        var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                        var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                        var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                        item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                    }

                    else
                    {
                        item.DesiredLayingDate = "";
                    }

                }

                model1.AddRange(query);
                model1.AddRange(InboxForReply);

                model.ResultCount = query.Count() + InboxForReply.Count();

                model.MinInboxCount = InboxForReply.Count();
                model.tQuestionModel = model1;
                model.tQuestionModel = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();




            }
            //end----------------------------------
            




            return model;
        }
        //Added code venkat for dynamic 
        static tPaperLaidV GetSubmittedQuestionsByType(object param)
        {
            //Added code venkat for dynamic 
            string DiaryNumber = "";
            string Subject = "";
            int? Name = 0;
            int? QNumber = 0;


            tPaperLaidV model = param as tPaperLaidV;
            if (model.DiaryNumber != "null" && model.DiaryNumber != "" && model.DiaryNumber != null)
            {
                DiaryNumber = model.DiaryNumber;
            }
            if (model.Subject != "null" && model.Subject != "" && model.Subject != null)
            {
                Subject = model.Subject;
            }
            if (model.QuestionNumber != null && model.QuestionNumber != 0)
            {
                QNumber = model.QuestionNumber;

            }
            if (model.MinID != null && model.MinID != 0)
            {
                Name = model.MinID;

            }
            
            //end------------------------------------------------



            if (model.RequestMessage == "ChiefMinisterAdmin")
            {
                PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

                var siteSettingData = (from a in pCtxt.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
                string FileStructurePath = siteSettingData.SettingValue;

                var RepliesSent = (from questions in pCtxt.tQuestions
                                   join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                   join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                   where questions.QuestionType == model.QuestionTypeId && paperLaid.MinistryId == model.MinistryId
                                       && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                       && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null
                                       && paperLaidTemp.DeptSubmittedDate != null
                                       && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                       //Added code venkat for dynamic 
                                         && (DiaryNumber == "" || questions.DiaryNumber == DiaryNumber)
                                      && (Subject == "" || questions.Subject.Contains(Subject) || questions.Subject == Subject)
                                       && (QNumber == 0 || questions.QuestionNumber == QNumber)
                                     && (Name == 0 || questions.MemberID == Name)
                                   //end----------------------------------------------
                                   select new PaperSendModelCustom
                                   {
                                       QuestionID = questions.QuestionID,
                                       QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                       Subject = paperLaid.Title,
                                       PaperLaidId = questions.PaperLaidId,
                                       Version = paperLaidTemp.Version,
                                       DeptActivePaperId = paperLaid.DeptActivePaperId,
                                       DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                                       // PaperSent = paperLaidTemp.SignedFilePath,
                                       evidhanReferenceNumber = paperLaidTemp.evidhanReferenceNumber,
                                       PaperSent = FileStructurePath + paperLaidTemp.SignedFilePath,
                                       FileName = paperLaidTemp.FileName,
                                       IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                       IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                       ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                       IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                       DeActivateFlag = questions.DeActivateFlag,
                                       Status = (int)QuestionDashboardStatus.RepliesSent,
                                       DiaryNumber = questions.DiaryNumber,
                                       MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                       DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                   }).ToList();

                foreach (var item in RepliesSent)
                {
                    if (item.DeptSubmittedDate != null)
                    {
                        var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                        var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                        var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                        item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                    }
                    else
                    {
                        item.DepartmentSubmittedDate = "";
                    }

                    if (item.DesireLayingDate != null)
                    {
                        var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                        var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                        var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                        item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                    }
                    else
                    {
                        item.DesiredLayingDate = "";
                    }
                }

                int totalRecords = RepliesSent.Count();
                var results = RepliesSent.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                model.tQuestionSendModel = results;
            }
            else
            {
                PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

                var siteSettingData = (from a in pCtxt.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
                string FileStructurePath = siteSettingData.SettingValue;

                var RepliesSent = (from questions in pCtxt.tQuestions
                                   join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                                   join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                                   where questions.QuestionType == model.QuestionTypeId && paperLaid.MinistryId == model.MinistryId
                                       && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId
                                       && questions.PaperLaidId != null && paperLaidTemp.DeptSubmittedBy != null
                                       && paperLaidTemp.DeptSubmittedDate != null
                                       && paperLaid.DeptActivePaperId == paperLaidTemp.PaperLaidTempId
                                       //Added code venkat for dynamic 
                                         && (DiaryNumber == "" || questions.DiaryNumber == DiaryNumber)
                                      && (Subject == "" || questions.Subject.Contains(Subject) || questions.Subject == Subject)
                                       && (QNumber == 0 || questions.QuestionNumber == QNumber)
                                     && (Name == 0 || questions.MemberID == Name)
                                   //end----------------------------------------------
                                   select new PaperSendModelCustom
                                   {
                                       QuestionID = questions.QuestionID,
                                       QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                                       Subject = paperLaid.Title,
                                       PaperLaidId = questions.PaperLaidId,
                                       Version = paperLaidTemp.Version,
                                       DeptActivePaperId = paperLaid.DeptActivePaperId,
                                       DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                                       // PaperSent = paperLaidTemp.SignedFilePath,
                                       evidhanReferenceNumber = paperLaidTemp.evidhanReferenceNumber,
                                       PaperSent = FileStructurePath + paperLaidTemp.SignedFilePath,
                                       FileName = paperLaidTemp.FileName,
                                       IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                                       IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                                       ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                                       IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                                       DeActivateFlag = questions.DeActivateFlag,
                                       Status = (int)QuestionDashboardStatus.RepliesSent,
                                       DiaryNumber = questions.DiaryNumber,
                                       MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                       DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null
                                   }).ToList();

                foreach (var item in RepliesSent)
                {
                    if (item.DeptSubmittedDate != null)
                    {
                        var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                        var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                        var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                        item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                    }
                    else
                    {
                        item.DepartmentSubmittedDate = "";
                    }

                    if (item.DesireLayingDate != null)
                    {
                        var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                        var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                        var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                        item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                    }
                    else
                    {
                        item.DesiredLayingDate = "";
                    }
                }

                int totalRecords = RepliesSent.Count();
                var results = RepliesSent.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                model.tQuestionSendModel = results;
            }


           

            return model;
        }

        //static tPaperLaidV GetPendingForSubQuestionsByType(object param)
        //{
        //    PaperLaidMinisterContext db = new PaperLaidMinisterContext();
        //    tPaperLaidV model = param as tPaperLaidV;

        //    //int? EventId = null;
        //    //int? PaperCategoryTypeId = null;
        //    //if (model.EventId != 0 && model.PaperCategoryTypeId != 0)
        //    //{
        //    //    EventId = model.EventId;
        //    //}
        //    //else if (model.PaperCategoryTypeId != 0)
        //    //{
        //    //    PaperCategoryTypeId = model.PaperCategoryTypeId;
        //    //}

        //    var query = (from m in db.tPaperLaidV
        //                 join c in db.mEvent on m.EventId equals c.EventId
        //                 join ministery in db.mMinistry on m.MinistryId equals ministery.MinistryID
        //                 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
        //                 join questions in db.tQuestions on model.QuestionTypeId equals questions.QuestionType
        //                 where m.CommitteeId == null && t.DeptSubmittedDate == null
        //                 && t.DeptSubmittedBy == null 
        //                 //&& c.PaperCategoryTypeId == model.PaperCategoryTypeId && (!EventId.HasValue || m.EventId == model.EventId)
        //                 select new PaperMovementModel
        //                 {
        //                     DeptActivePaperId = m.DeptActivePaperId,
        //                     PaperLaidId = m.PaperLaidId,
        //                     EventName = c.EventName,
        //                     Title = m.Title,
        //                     DeparmentName = m.DeparmentName,
        //                     MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
        //                     PaperTypeID = (int)m.PaperLaidId,
        //                     actualFilePath = t.FilePath + t.FileName,
        //                     PaperCategoryTypeId = c.PaperCategoryTypeId,
        //                     BusinessType = c.EventName,
        //                     version = t.Version,
        //                     QuestionNumber = questions.QuestionNumber,
        //                     PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault()
        //                 }).OrderBy(m => m.PaperLaidId).ToList();

        //    int totalRecords = query.Count();
        //    var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

        //    model.ResultCount = totalRecords;
        //    model.DepartmentPendingList = results;

        //    return model;
        //}

        static tPaperLaidV GetPendingForSubQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            var query = (from questions in pCtxt.tQuestions
                         join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         join paperLaid in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaid.PaperLaidId
                         where questions.QuestionType == model.QuestionTypeId
                         && paperLaid.MinistryId == model.MinistryId
                         && questions.PaperLaidId != null
                         && paperLaidTemp.DeptSubmittedBy == null
                         && paperLaidTemp.DeptSubmittedDate == null
                         && questions.AssemblyID == model.AssemblyId
                         && questions.SessionID == model.SessionId
                         select new QuestionModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                             FileName = paperLaidTemp.FilePath + paperLaidTemp.FileName,
                             Subject = paperLaid.Title,
                             PaperLaidId = questions.PaperLaidId,
                             Version = paperLaidTemp.Version,
                             DeptActivePaperId = paperLaid.DeptActivePaperId,
                             DiaryNumber = questions.DiaryNumber,
                             MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionModel = results;

            return model;
        }

        //Get all the questions Which are laid in house.
        //TODO : Change the way it is working at the moment (As we are dealing with questions here we should be able to pass and get instance of Question model)
        static tPaperLaidV GetLaidInHouseQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;

            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();


            var query = (from questions in pCtxt.tQuestions
                         join paperLaidVS in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaidVS.PaperLaidId
                         join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         where questions.QuestionType == model.QuestionTypeId && paperLaidVS.MinistryId == model.MinistryId

                             //New
                             && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId

                             && questions.PaperLaidId != null && paperLaidVS.LOBRecordId != null
                             && paperLaidVS.DeptActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidVS.MinisterActivePaperId != null
                             && paperLaidVS.LaidDate <= DateTime.Now && paperLaidVS.LaidDate != null
                             && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
                             && paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
                         select new PaperSendModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                             Subject = paperLaidVS.Title,
                             PaperLaidId = questions.PaperLaidId,
                             IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                             IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                             ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                             IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                             DeActivateFlag = questions.DeActivateFlag,
                             Status = (int)QuestionDashboardStatus.RepliesSent,
                             Version = paperLaidTemp.Version,
                             DeptActivePaperId = paperLaidVS.DeptActivePaperId,
                             DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                             PaperSent = paperLaidTemp.SignedFilePath,
                             FileName = paperLaidTemp.FileName,
                             DiaryNumber = questions.DiaryNumber,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null

                         }).ToList();

            foreach (var item in query)
            {
                if (item.DeptSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;
                }
                else
                {
                    item.DepartmentSubmittedDate = "";
                }

                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }
                else
                {
                    item.DesiredLayingDate = "";
                }

            }


            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionSendModel = results;

            return model;
        }

        static tPaperLaidV GetPendingToLayQuestionsByType(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            var query = (from questions in pCtxt.tQuestions
                         join paperLaidC in pCtxt.tPaperLaidV on questions.PaperLaidId equals paperLaidC.PaperLaidId
                         join paperLaidTemp in pCtxt.tPaperLaidTemp on questions.PaperLaidId equals paperLaidTemp.PaperLaidId
                         where questions.QuestionType == model.QuestionTypeId && paperLaidC.MinistryId == model.MinistryId

                             //New
                             && questions.AssemblyID == model.AssemblyId && questions.SessionID == model.SessionId

                             && questions.PaperLaidId != null && paperLaidC.LaidDate == null && paperLaidC.DesireLayingDate > DateTime.Now
                             && paperLaidC.MinisterActivePaperId == paperLaidTemp.PaperLaidTempId && paperLaidC.LOBRecordId == null
                             && paperLaidTemp.DeptSubmittedBy != null && paperLaidTemp.DeptSubmittedDate != null
                             && paperLaidTemp.MinisterSubmittedBy != null && paperLaidTemp.MinisterSubmittedDate != null
                         select new PaperSendModelCustom
                         {
                             QuestionID = questions.QuestionID,
                             QuestionNumber = questions.IsFinalApproved == true ? questions.QuestionNumber : (int?)null,
                             Subject = paperLaidC.Title,
                             PaperLaidId = questions.PaperLaidId,
                             IsPostpone = questions.IsFinalApproved == true ? questions.IsPostpone : (bool?)null,
                             IsPostponeDate = questions.IsFinalApproved == true ? questions.IsPostponeDate : (DateTime?)null,
                             ReferenceMemberCode = questions.IsFinalApproved == true ? questions.ReferenceMemberCode : "",
                             IsClubbed = questions.IsFinalApproved == true ? questions.IsClubbed : (bool?)null,
                             DeActivateFlag = questions.DeActivateFlag,
                             Status = (int)QuestionDashboardStatus.RepliesSent,
                             Version = paperLaidTemp.Version,
                             DeptActivePaperId = paperLaidC.DeptActivePaperId,
                             DeptSubmittedDate = paperLaidTemp.DeptSubmittedDate,
                             //Fetch the signed file by minister.
                             PaperSent = paperLaidTemp.SignedFilePath,
                             FileName = paperLaidTemp.FileName,
                             DiaryNumber = questions.DiaryNumber,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DesireLayingDate = questions.IsFinalApproved == true ? (from sessionDates in pCtxt.mSessionDates where sessionDates.Id == questions.SessionDateId select sessionDates.SessionDate).FirstOrDefault() : (DateTime?)null

                         }).ToList();

            foreach (var item in query)
            {
                if (item.DeptSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;
                }
                else
                {
                    item.DepartmentSubmittedDate = "";
                }

                if (item.DesireLayingDate != null)
                {
                    var curr_Desidate = Convert.ToDateTime(item.DesireLayingDate).ToString("dd");
                    var curr_Desimonth = Convert.ToDateTime(item.DesireLayingDate).ToString("MM");
                    var curr_Desiyear = Convert.ToDateTime(item.DesireLayingDate).ToString("yyyy");
                    item.DesiredLayingDate = curr_Desidate + "/" + curr_Desimonth + "/" + curr_Desiyear;
                }
                else
                {
                    item.DesiredLayingDate = "";
                }

            }


            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.tQuestionSendModel = results;

            return model;
        }



        static object GetCountForQuestionTypes(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            using (var ctx = new PaperLaidMinisterContext())
            {
                var draftcount = (from a in ctx.tPaperLaidV

                                  where a.AssemblyId == model.AssemblyCode
                                      && a.SessionId == model.SessionCode && (a.DeptActivePaperId == null || a.DeptActivePaperId == 0) &&
                                      (a.EventId == 5 || a.EventId == 6)
                                  select a).Count();
                model.TotalBillDraftCount = draftcount;
            }

            using (var ctx = new PaperLaidMinisterContext())
            {
                var sentcount = (from a in ctx.tPaperLaidV
                                 where a.AssemblyId == model.AssemblyCode
                                     && a.SessionId == model.SessionCode && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0) &&
                                     (a.EventId == 5 || a.EventId == 6)
                                 select a).Count();
                model.TotalBillSentCount = sentcount;
            }

            model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;
            model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            model.TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalStaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
            model.TotalStaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
            return model;
        }

        static object GetStarredQuestionCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;


            model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;
            model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            model.TotalStarredUpcomingLOB = UpcomingLOBByQuestionType(model).ResultCount;
            model.TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalStaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
            model.TotalStaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
            model.TotalStaredQuestions = model.TotalStaredReceived + model.TotalStaredSubmitted;
            return model;
        }

        static object GetUnstarredQuestionCounter(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            model.TotalUnstaredReceived = GetPendingQuestionsByType(model).ResultCount;
            model.TotalUnstaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
            model.TotalUnstarredUpcomingLOB = UpcomingLOBByQuestionType(model).ResultCount;
            model.TotalUnstaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            model.TotalUnstaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
            model.TotalUnstaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
            model.TotalUnstaredQuestions = model.TotalUnstaredReceived + model.TotalUnstaredSubmitted + model.TotalUnstaredPendingForSubmission; ;
            return model;
        }

        //static object GetCountForQuestionTypes(object param)
        //{
        //    tPaperLaidV model = param as tPaperLaidV;
        //    using (var ctx = new PaperLaidMinisterContext())
        //    {
        //        var draftcount = (from a in ctx.tPaperLaidV

        //                          where a.AssemblyId == model.AssemblyCode
        //                              && a.SessionId == model.SessionCode && (a.DeptActivePaperId == null || a.DeptActivePaperId == 0) &&
        //                              (a.EventId == 5 || a.EventId == 6)
        //                          select a).Count();
        //        model.TotalBillDraftCount = draftcount;
        //    }

        //    using (var ctx = new PaperLaidMinisterContext())
        //    {
        //        var sentcount = (from a in ctx.tPaperLaidV
        //                         where a.AssemblyId == model.AssemblyCode
        //                             && a.SessionId == model.SessionCode && (a.DeptActivePaperId != null || a.DeptActivePaperId != 0) &&
        //                             (a.EventId == 5 || a.EventId == 6)
        //                         select a).Count();
        //        model.TotalBillSentCount = sentcount;
        //    }

        //    model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;
        //    model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
        //    model.TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
        //    model.TotalStaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
        //    model.TotalStaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
        //    return model;
        //}

        //static object GetStarredQuestionCounter(object param)
        //{
        //    tPaperLaidV model = param as tPaperLaidV;
        //    model.TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;
        //    model.TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
        //    model.TotalStarredUpcomingLOB = UpcomingLOBByQuestionType(model).ResultCount;
        //    model.TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
        //    model.TotalStaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
        //    model.TotalStaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
        //    return model;
        //}

        //static object GetUnstarredQuestionCounter(object param)
        //{
        //    tPaperLaidV model = param as tPaperLaidV;
        //    model.TotalUnstaredReceived = GetPendingQuestionsByType(model).ResultCount;
        //    model.TotalUnstaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;
        //    model.TotalUnstarredUpcomingLOB = UpcomingLOBByQuestionType(model).ResultCount;
        //    model.TotalUnstaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
        //    model.TotalUnstaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
        //    model.TotalUnstaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
        //    return model;
        //}

        #endregion


        #region "Notice"



        static object GetAllNotices(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            List<MinisterNoticecustom> model1 = new List<MinisterNoticecustom>();

            var PendingNotices = (from PaperLaidV in pCtxt.tPaperLaidV
                                  join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                                  join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId
                                  where PaperLaidV.AssemblyId == model.AssemblyId
                                          && PaperLaidV.SessionId == model.SessionId
                                          && PaperLaidTemp.DeptSubmittedBy == null
                                          && PaperLaidTemp.DeptSubmittedDate == null
                                          && PaperLaidV.MinistryId == model.MinistryId
                                          && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                                  select new MinisterNoticecustom
                                  {

                                      PaperLaidId = PaperLaidV.PaperLaidId,
                                      MinistryId = PaperLaidV.MinistryId,
                                      NoticeNumber = MemberNotices.NoticeNumber,
                                      MinistryName = PaperLaidV.MinistryName,
                                      Version = PaperLaidTemp.Version,
                                      DepartmentId = PaperLaidV.DepartmentId,
                                      DeparmentName = PaperLaidV.DeparmentName,
                                      FileName = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                      FilePath = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                      PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                      tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                                      FileVersion = PaperLaidTemp.Version,
                                      Title = PaperLaidV.Title,
                                      Status = (int)MinisterDashboardStatus.PendingForReply,
                                      DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                                      Subject = MemberNotices.Subject,
                                      NoticeID = MemberNotices.NoticeId,
                                      MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                      //MemberName = (from mc in pCtxt.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                      // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                                      DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()
                                  }).ToList();





            var NoticesPendingForReply = (from NT in pCtxt.tMemberNotice
                                          where NT.AssemblyID == model.AssemblyId
                                                   && NT.SessionID == model.SessionId
                                                   && NT.PaperLaidId == null
                                                   && NT.MinistryId == model.MinistryId
                                                   && NT.MinistryId != null
                                                   && NT.IsSubmitted == true
                                          select new MinisterNoticecustom
                                          {
                                              NoticeNumber = NT.NoticeNumber,
                                              Subject = NT.Subject,
                                              NoticeID = NT.NoticeId,
                                              Status = (int)MinisterDashboardStatus.PendingForReply,

                                              MemberName = (from mc in pCtxt.mMember where mc.MemberCode == NT.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                                          }).ToList();

            //Get Submitted Notices.


            var sentNotices = (from PaperLaidV in pCtxt.tPaperLaidV
                               join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                               join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId
                               where PaperLaidV.AssemblyId == model.AssemblyId
                                        && PaperLaidV.SessionId == model.SessionId
                                        && (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                                        && PaperLaidTemp.DeptSubmittedBy != null
                                        && PaperLaidTemp.DeptSubmittedDate != null
                                        && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                                        && PaperLaidV.MinistryId == model.MinistryId
                               select new MinisterNoticecustom
                               {

                                   PaperLaidId = PaperLaidV.PaperLaidId,
                                   NoticeNumber = MemberNotices.NoticeNumber,
                                   MinistryId = PaperLaidV.MinistryId,
                                   MinistryName = PaperLaidV.MinistryName,
                                   DepartmentId = PaperLaidV.DepartmentId,
                                   DeparmentName = PaperLaidV.DeparmentName,
                                   FileName = PaperLaidTemp.SignedFilePath,
                                   FilePath = PaperLaidTemp.SignedFilePath,
                                   Version = PaperLaidTemp.Version,
                                   tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                                   FileVersion = PaperLaidTemp.Version,
                                   PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                   Title = PaperLaidV.Title,
                                   Status = (int)MinisterDashboardStatus.RepliesSent,
                                   DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                                   MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                                   Subject = MemberNotices.Subject,
                                   NoticeID = MemberNotices.NoticeId,
                                   MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                                   //MemberName = (from mc in pCtxt.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                   //MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                                   DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()
                               }).ToList();

            model1.AddRange(PendingNotices);
            model1.AddRange(sentNotices);
            model1.AddRange(NoticesPendingForReply);

            model.ResultCount = PendingNotices.Count() + sentNotices.Count() + NoticesPendingForReply.Count();
            model.MinisterNoticecustomList = model1;
            model.MinisterNoticecustomList = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;
        }

        //Added code venkat for dynamic 
        private static tPaperLaidV GetNoticeInBoxList(object param)
        {//Added code venkat for dynamic 
            string NoticeNumber = "";
            string Subject = "";
            int? Name = 0;
            tPaperLaidV model = param as tPaperLaidV;
            if (model.NoticeNumber != null && model.NoticeNumber != "null" && model.NoticeNumber != "")
            {
                NoticeNumber = model.NoticeNumber;
            }
            if (model.Subject != null && model.Subject != "null" && model.Subject != "")
            {
                Subject = model.Subject;
            }
            if (model.MinID != null && model.MinID != 0)
            {
                Name = model.MinID;

            }
            if (model.RequestMessage == "ChiefMinisterAdmin")
            {
                PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

                List<MinisterNoticecustom> model1 = new List<MinisterNoticecustom>();

                var query = (from PaperLaidV in pCtxt.tPaperLaidV
                             join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                             join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId
                             join E in pCtxt.mEvent on MemberNotices.NoticeTypeID equals E.EventId

                             //join Questions in pCtxt.tQuestions on PaperLaidV.PaperLaidId equals Questions.PaperLaidId
                             where PaperLaidV.AssemblyId == model.AssemblyId
                                              && PaperLaidV.SessionId == model.SessionId
                                              && PaperLaidTemp.DeptSubmittedBy == null
                                              && PaperLaidTemp.DeptSubmittedDate == null
                                              //&& PaperLaidV.MinistryId == model.MinistryId
                                              && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                             ////Added code venkat for dynamic
                             //              && (NoticeNumber == "" || MemberNotices.NoticeNumber == NoticeNumber)
                             //           && (Subject == "" || MemberNotices.Subject == Subject || MemberNotices.Subject.Contains(Subject))
                             //           && (Name == 0 || MemberNotices.MemberId == Name)
                             //           //end-----------------------------------
                             select new MinisterNoticecustom
                             {

                                 PaperLaidId = PaperLaidV.PaperLaidId,
                                 MinistryId = PaperLaidV.MinistryId,
                                 NoticeNumber = MemberNotices.NoticeNumber,
                                 MinistryName = PaperLaidV.MinistryName,
                                 DepartmentId = PaperLaidV.DepartmentId,
                                 DeparmentName = PaperLaidV.DeparmentName,
                                 FileName = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                 PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                 FilePath = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                 Version = PaperLaidTemp.Version,
                                 tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                                 FileVersion = PaperLaidTemp.Version,
                                 Title = PaperLaidV.Title,
                                 Status = (int)MinisterDashboardStatus.DraftReplies,
                                 DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                                 Subject = MemberNotices.Subject,
                                 RuleNo = E.RuleNo,
                                 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 //OriDiaryFileName = MemberNotices.OriDiaryFileName,
                                 //OriDiaryFilePath = MemberNotices.OriDiaryFilePath,


                                 // MemberName = (from mc in pCtxt.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                                 DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()

                             }).ToList();


                var NoticesPendingForReply = (from NT in pCtxt.tMemberNotice
                                              join E in pCtxt.mEvent on NT.NoticeTypeID equals E.EventId
                                              where NT.AssemblyID == model.AssemblyId
                                                       && NT.SessionID == model.SessionId
                                                       && NT.PaperLaidId == null
                                                       //&& NT.MinistryId == model.MinistryId
                                                       && NT.MinistryId != null
                                                       && NT.IsSubmitted == true
                                                  ////Added code venkat for dynamic
                                                        && (NoticeNumber == "" || NT.NoticeNumber == NoticeNumber)
                                                       && (Subject == "" || NT.Subject == Subject || NT.Subject.Contains(Subject))
                                                       && (Name == 0 || NT.MemberId == Name)
                                              //end-------------------------------------
                                              select new MinisterNoticecustom
                                              {
                                                  NoticeNumber = NT.NoticeNumber,
                                                  Subject = NT.Subject,
                                                  NoticeID = NT.NoticeId,
                                                  Status = (int)MinisterDashboardStatus.PendingForReply,
                                                  RuleNo = E.RuleNo,
                                                  MemberName = (from mc in pCtxt.mMember where mc.MemberCode == NT.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                                              }).ToList();

                model1.AddRange(query);
                model1.AddRange(NoticesPendingForReply);


                model.ResultCount = query.Count() + NoticesPendingForReply.Count();
                model.MinisterNoticecustomList = model1;
                model.MinisterNoticecustomList = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            }
            else
            {

                PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

                List<MinisterNoticecustom> model1 = new List<MinisterNoticecustom>();

                var query = (from PaperLaidV in pCtxt.tPaperLaidV
                             join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                             join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId
                             join E in pCtxt.mEvent on MemberNotices.NoticeTypeID equals E.EventId

                             //join Questions in pCtxt.tQuestions on PaperLaidV.PaperLaidId equals Questions.PaperLaidId
                             where PaperLaidV.AssemblyId == model.AssemblyId
                                              && PaperLaidV.SessionId == model.SessionId
                                              && PaperLaidTemp.DeptSubmittedBy == null
                                              && PaperLaidTemp.DeptSubmittedDate == null
                                              && PaperLaidV.MinistryId == model.MinistryId
                                              && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                             ////Added code venkat for dynamic
                             //              && (NoticeNumber == "" || MemberNotices.NoticeNumber == NoticeNumber)
                             //           && (Subject == "" || MemberNotices.Subject == Subject || MemberNotices.Subject.Contains(Subject))
                             //           && (Name == 0 || MemberNotices.MemberId == Name)
                             //           //end-----------------------------------
                             select new MinisterNoticecustom
                             {

                                 PaperLaidId = PaperLaidV.PaperLaidId,
                                 MinistryId = PaperLaidV.MinistryId,
                                 NoticeNumber = MemberNotices.NoticeNumber,
                                 MinistryName = PaperLaidV.MinistryName,
                                 DepartmentId = PaperLaidV.DepartmentId,
                                 DeparmentName = PaperLaidV.DeparmentName,
                                 FileName = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                 PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                 FilePath = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                 Version = PaperLaidTemp.Version,
                                 tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                                 FileVersion = PaperLaidTemp.Version,
                                 Title = PaperLaidV.Title,
                                 Status = (int)MinisterDashboardStatus.DraftReplies,
                                 DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                                 Subject = MemberNotices.Subject,
                                 RuleNo = E.RuleNo,
                                 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 //OriDiaryFileName = MemberNotices.OriDiaryFileName,
                                 //OriDiaryFilePath = MemberNotices.OriDiaryFilePath,


                                 // MemberName = (from mc in pCtxt.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                                 DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()

                             }).ToList();


                var NoticesPendingForReply = (from NT in pCtxt.tMemberNotice
                                              join E in pCtxt.mEvent on NT.NoticeTypeID equals E.EventId
                                              where NT.AssemblyID == model.AssemblyId
                                                       && NT.SessionID == model.SessionId
                                                       && NT.PaperLaidId == null
                                                       && NT.MinistryId == model.MinistryId
                                                       && NT.MinistryId != null
                                                       && NT.IsSubmitted == true
                                                  ////Added code venkat for dynamic
                                                        && (NoticeNumber == "" || NT.NoticeNumber == NoticeNumber)
                                                       && (Subject == "" || NT.Subject == Subject || NT.Subject.Contains(Subject))
                                                       && (Name == 0 || NT.MemberId == Name)
                                              //end-------------------------------------
                                              select new MinisterNoticecustom
                                              {
                                                  NoticeNumber = NT.NoticeNumber,
                                                  Subject = NT.Subject,
                                                  NoticeID = NT.NoticeId,
                                                  Status = (int)MinisterDashboardStatus.PendingForReply,
                                                  RuleNo = E.RuleNo,
                                                  MemberName = (from mc in pCtxt.mMember where mc.MemberCode == NT.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                                              }).ToList();

                model1.AddRange(query);
                model1.AddRange(NoticesPendingForReply);


                model.ResultCount = query.Count() + NoticesPendingForReply.Count();
                model.MinisterNoticecustomList = model1;
                model.MinisterNoticecustomList = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            }
            //end----------------------------------



            return model;
        }

        //Added code venkat for dynamic 
        private static tPaperLaidV GetNoticeReplySentList(object param)
        {
            //  tPaperLaidV data = (tPaperLaidV)p;
            //Added code venkat for dynamic 
            string NoticeNumber = "";
            string Subject = "";
            int? Name = 0;
            tPaperLaidV model = param as tPaperLaidV;
            if (model.NoticeNumber != null && model.NoticeNumber != "null" && model.NoticeNumber != "")
            {
                NoticeNumber = model.NoticeNumber;
            }
            if (model.Subject != null && model.Subject != "null" && model.Subject != "")
            {
                Subject = model.Subject;
            }
            if (model.MinID != null && model.MinID != 0)
            {
                Name = model.MinID;

            }
            //end------------------------------

            if (model.RequestMessage == "ChiefMinisterAdmin")
            {
                PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

                var query = (from PaperLaidV in pCtxt.tPaperLaidV
                             join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                             join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId
                             join E in pCtxt.mEvent on MemberNotices.NoticeTypeID equals E.EventId
                             //join Questions in pCtxt.tQuestions on PaperLaidV.PaperLaidId equals Questions.PaperLaidId
                             where PaperLaidV.AssemblyId == model.AssemblyId
                                            && PaperLaidV.SessionId == model.SessionId
                                            && (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                                            && PaperLaidTemp.DeptSubmittedBy != null
                                            && PaperLaidTemp.DeptSubmittedDate != null
                                            && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                                           // && PaperLaidV.MinistryId == model.MinistryId
                                 //Added code venkat for dynamic 
                                             && (NoticeNumber == "" || MemberNotices.NoticeNumber == NoticeNumber)
                                            && (Subject == "" || MemberNotices.Subject == Subject || MemberNotices.Subject.Contains(Subject))
                                            && (Name == 0 || MemberNotices.MemberId == Name)
                             //end------------------------------------------------
                             select new MinisterNoticecustom
                             {

                                 PaperLaidId = PaperLaidV.PaperLaidId,
                                 NoticeNumber = MemberNotices.NoticeNumber,
                                 MinistryId = PaperLaidV.MinistryId,
                                 MinistryName = PaperLaidV.MinistryName,
                                 DepartmentId = PaperLaidV.DepartmentId,
                                 DeparmentName = PaperLaidV.DeparmentName,
                                 Version = PaperLaidTemp.Version,
                                 FileName = PaperLaidTemp.SignedFilePath,
                                 FilePath = PaperLaidTemp.SignedFilePath,
                                 PaperSent = PaperLaidTemp.SignedFilePath,
                                 tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                                 evidhanReferenceNumber = PaperLaidTemp.evidhanReferenceNumber,
                                 FileVersion = PaperLaidTemp.Version,
                                 Title = PaperLaidV.Title,
                                 Subject = MemberNotices.Subject,
                                 DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                                 Status = (int)MinisterDashboardStatus.RepliesSent,
                                 MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                                 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 RuleNo = E.RuleNo,
                                 //MemberName = (from mc in pCtxt.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                                 DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()
                             }).ToList();

                foreach (var item in query)
                {

                    //var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    //var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    //var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                }

                int totalRecords = query.Count();
                var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                model.MinisterNoticecustomList = results;
                model.MinisterNoticecustomList = results.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            }
            else
            {
                PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
                var query = (from PaperLaidV in pCtxt.tPaperLaidV
                             join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                             join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId
                             join E in pCtxt.mEvent on MemberNotices.NoticeTypeID equals E.EventId
                             //join Questions in pCtxt.tQuestions on PaperLaidV.PaperLaidId equals Questions.PaperLaidId
                             where PaperLaidV.AssemblyId == model.AssemblyId
                                            && PaperLaidV.SessionId == model.SessionId
                                            && (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                                            && PaperLaidTemp.DeptSubmittedBy != null
                                            && PaperLaidTemp.DeptSubmittedDate != null
                                            && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                                           // && PaperLaidV.MinistryId == model.MinistryId
                                 //Added code venkat for dynamic 
                                             && (NoticeNumber == "" || MemberNotices.NoticeNumber == NoticeNumber)
                                            && (Subject == "" || MemberNotices.Subject == Subject || MemberNotices.Subject.Contains(Subject))
                                            && (Name == 0 || MemberNotices.MemberId == Name)
                             //end------------------------------------------------
                             select new MinisterNoticecustom
                             {

                                 PaperLaidId = PaperLaidV.PaperLaidId,
                                 NoticeNumber = MemberNotices.NoticeNumber,
                                 MinistryId = PaperLaidV.MinistryId,
                                 MinistryName = PaperLaidV.MinistryName,
                                 DepartmentId = PaperLaidV.DepartmentId,
                                 DeparmentName = PaperLaidV.DeparmentName,
                                 Version = PaperLaidTemp.Version,
                                 FileName = PaperLaidTemp.SignedFilePath,
                                 FilePath = PaperLaidTemp.SignedFilePath,
                                 PaperSent = PaperLaidTemp.SignedFilePath,
                                 tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                                 evidhanReferenceNumber = PaperLaidTemp.evidhanReferenceNumber,
                                 FileVersion = PaperLaidTemp.Version,
                                 Title = PaperLaidV.Title,
                                 Subject = MemberNotices.Subject,
                                 DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                                 Status = (int)MinisterDashboardStatus.RepliesSent,
                                 MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                                 MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 RuleNo = E.RuleNo,
                                 //MemberName = (from mc in pCtxt.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                                 // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                                 DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()
                             }).ToList();

                foreach (var item in query)
                {

                    //var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    //var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    //var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                }

                int totalRecords = query.Count();
                var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

                model.ResultCount = totalRecords;
                model.MinisterNoticecustomList = results;
                model.MinisterNoticecustomList = results.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            }


            return model;

        }


        private static tPaperLaidV GetNoticeULOBList(object param)
        {


            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            var currentdate = DateTime.Now;


            var query = (from PaperLaidV in pCtxt.tPaperLaidV
                         join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId

                         where PaperLaidTemp.DeptSubmittedDate != null
                                       && PaperLaidTemp.DeptSubmittedBy != null
                                       && PaperLaidTemp.MinisterSubmittedBy != 0
                                       && PaperLaidTemp.MinisterSubmittedDate != null
                                       && ((from m in pCtxt.mSessionDates where m.SessionId == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault() >= currentdate)
                                       && PaperLaidV.LaidDate == null
                                       && PaperLaidV.LOBRecordId != null
                                       && MemberNotices.DepartmentId == model.DepartmentId

                         select new MinisterNoticecustom
                         {

                             PaperLaidId = PaperLaidV.PaperLaidId,
                             MinistryId = PaperLaidV.MinistryId,
                             NoticeNumber = MemberNotices.NoticeNumber,
                             MinistryName = PaperLaidV.MinistryName,
                             DepartmentId = PaperLaidV.DepartmentId,
                             Version = PaperLaidTemp.Version,
                             DeparmentName = PaperLaidV.DeparmentName,
                             FileName = PaperLaidTemp.SignedFilePath,
                             FilePath = PaperLaidTemp.SignedFilePath,
                             tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                             PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                             FileVersion = PaperLaidTemp.Version,
                             Title = PaperLaidV.Title,
                             Subject = MemberNotices.Subject,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                             // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()
                         }).ToList();




            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterNoticecustomList = results;

            return model;

        }


        private static tPaperLaidV GetNoticeLIHList(object param)
        {

            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            var currentdate = DateTime.Now;


            var query = (from PaperLaidV in pCtxt.tPaperLaidV
                         join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId

                         where PaperLaidTemp.DeptSubmittedDate != null
                                       && PaperLaidTemp.DeptSubmittedBy != null
                                       && PaperLaidTemp.MinisterSubmittedBy != 0
                                       && PaperLaidTemp.MinisterSubmittedDate != null
                                       && ((from m in pCtxt.mSessionDates where m.SessionId == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault() >= currentdate)
                                       && PaperLaidV.LaidDate == null
                                       && PaperLaidV.LOBRecordId != null
                                       && MemberNotices.DepartmentId == model.DepartmentId

                         select new MinisterNoticecustom
                         {

                             PaperLaidId = PaperLaidV.PaperLaidId,
                             MinistryId = PaperLaidV.MinistryId,
                             NoticeNumber = MemberNotices.NoticeNumber,
                             MinistryName = PaperLaidV.MinistryName,
                             DepartmentId = PaperLaidV.DepartmentId,
                             Version = PaperLaidTemp.Version,
                             DeparmentName = PaperLaidV.DeparmentName,
                             FileName = PaperLaidTemp.SignedFilePath,
                             FilePath = PaperLaidTemp.SignedFilePath,
                             tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                             PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                             FileVersion = PaperLaidTemp.Version,
                             Title = PaperLaidV.Title,
                             Subject = MemberNotices.Subject,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                             // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()
                         }).ToList();




            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterNoticecustomList = results;

            return model;

        }



        private static tPaperLaidV GetNoticePLIHList(object param)
        {


            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();
            var currentdate = DateTime.Now;


            var query = (from PaperLaidV in pCtxt.tPaperLaidV
                         join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         join MemberNotices in pCtxt.tMemberNotice on PaperLaidV.PaperLaidId equals MemberNotices.PaperLaidId

                         where PaperLaidTemp.DeptSubmittedDate != null
                                       && PaperLaidTemp.DeptSubmittedBy != null
                                       && PaperLaidTemp.MinisterSubmittedBy != 0
                                       && PaperLaidTemp.MinisterSubmittedDate != null
                                       && ((from m in pCtxt.mSessionDates where m.SessionId == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault() >= currentdate)
                                       && PaperLaidV.LaidDate == null
                                       && PaperLaidV.LOBRecordId != null
                                       && MemberNotices.DepartmentId == model.DepartmentId

                         select new MinisterNoticecustom
                         {

                             PaperLaidId = PaperLaidV.PaperLaidId,
                             MinistryId = PaperLaidV.MinistryId,
                             NoticeNumber = MemberNotices.NoticeNumber,
                             MinistryName = PaperLaidV.MinistryName,
                             DepartmentId = PaperLaidV.DepartmentId,
                             Version = PaperLaidTemp.Version,
                             DeparmentName = PaperLaidV.DeparmentName,
                             FileName = PaperLaidTemp.SignedFilePath,
                             FilePath = PaperLaidTemp.SignedFilePath,
                             tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                             PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                             FileVersion = PaperLaidTemp.Version,
                             Title = PaperLaidV.Title,
                             Subject = MemberNotices.Subject,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                             MemberName = (from mc in pCtxt.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

                             // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             DesireLayingDate = PaperLaidV.DesireLayingDateId == 0 ? (DateTime?)null : (from m in pCtxt.mSessionDates where m.Id == PaperLaidV.DesireLayingDateId select m.SessionDate).FirstOrDefault()
                         }).ToList();




            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterNoticecustomList = results;

            return model;




        }



        #endregion


        #region  "Bills"

        static object GetAllBills(object param)
        {

            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            List<PaperLaidByPaperCategoryType> model1 = new List<PaperLaidByPaperCategoryType>();

            var DraftsBill = (from PaperLaidV in pCtxt.tPaperLaidV
                              join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                              join BillRegister in pCtxt.tBillRegister on PaperLaidV.PaperLaidId equals BillRegister.PaperLaidId
                              join Event in pCtxt.mEvent on PaperLaidV.EventId equals Event.EventId
                              //join Questions in pCtxt.tQuestions on PaperLaidV.PaperLaidId equals Questions.PaperLaidId
                              where PaperLaidV.AssemblyId == model.AssemblyId
                                  && PaperLaidV.SessionId == model.SessionId
                                  && (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                                  && PaperLaidTemp.DeptSubmittedBy != null
                                  && PaperLaidTemp.DeptSubmittedDate != null
                                  && PaperLaidTemp.MinisterSubmittedDate == null
                                  && PaperLaidTemp.MinisterSubmittedBy == null
                                  && PaperLaidV.MinistryId == model.MinistryId
                                  && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId

                              select new PaperLaidByPaperCategoryType
                              {
                                  EventName = Event.EventName,
                                  PaperLaidId = PaperLaidV.PaperLaidId,
                                  MinistryId = PaperLaidV.MinistryId,
                                  NoticeNumber = BillRegister.BillNumber,
                                  MinistryName = PaperLaidV.MinistryName,

                                  Version = PaperLaidTemp.Version,
                                  DepartmentId = PaperLaidV.DepartmentId,
                                  DeparmentName = PaperLaidV.DeparmentName,
                                  FileName = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                  FilePath = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                  PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                                  tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                                  FileVersion = PaperLaidTemp.Version,
                                  Title = PaperLaidV.Title,
                                  Status = (int)MinisterDashboardStatus.PendingForReply,
                                  DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                                  MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate

                              }).ToList();

            foreach (var item in DraftsBill)
            {
                if (item.MinisterSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("yyyy");
                    item.MinisterSubmittedDatemini = curr_date + "/" + curr_month + "/" + curr_year;
                }
                else
                {
                    item.MinisterSubmittedDatemini = "";
                }

            }

            //Get Submitted Bills.


            var sentBill = (from PaperLaidV in pCtxt.tPaperLaidV
                            join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                            join BillRegister in pCtxt.tBillRegister on PaperLaidV.PaperLaidId equals BillRegister.PaperLaidId
                            join Event in pCtxt.mEvent on PaperLaidV.EventId equals Event.EventId
                            where PaperLaidV.AssemblyId == model.AssemblyId
                                && PaperLaidV.SessionId == model.SessionId
                                // && (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                                && (PaperLaidV.MinisterActivePaperId != null && PaperLaidV.MinisterActivePaperId != 0)
                                && PaperLaidTemp.DeptSubmittedBy != null
                                && PaperLaidTemp.DeptSubmittedDate != null
                                && PaperLaidTemp.MinisterSubmittedDate != null
                                && PaperLaidTemp.MinisterSubmittedBy != null
                                && PaperLaidV.MinistryId == model.MinistryId
                                && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                            // && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                            select new PaperLaidByPaperCategoryType
                            {
                                EventName = Event.EventName,
                                PaperLaidId = PaperLaidV.PaperLaidId,
                                MinistryId = PaperLaidV.MinistryId,
                                NoticeNumber = BillRegister.BillNumber,
                                MinistryName = PaperLaidV.MinistryName,
                                DepartmentId = PaperLaidV.DepartmentId,
                                DeparmentName = PaperLaidV.DeparmentName,
                                FileName = PaperLaidTemp.SignedFilePath,
                                FilePath = PaperLaidTemp.SignedFilePath,
                                Version = PaperLaidTemp.Version,
                                tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                                PaperSent = PaperLaidTemp.SignedFilePath,
                                FileVersion = PaperLaidTemp.Version,
                                Title = PaperLaidV.Title,
                                Status = (int)MinisterDashboardStatus.RepliesSent,
                                DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                                MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate

                            }).ToList();

            foreach (var item in sentBill)
            {
                if (item.MinisterSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("yyyy");
                    item.MinisterSubmittedDatemini = curr_date + "/" + curr_month + "/" + curr_year;
                }
                else
                {
                    item.MinisterSubmittedDatemini = "";
                }

            }

            model1.AddRange(DraftsBill);
            model1.AddRange(sentBill);

            model.ResultCount = DraftsBill.Count() + sentBill.Count();
            model.PaperLaidByPaperCategoryTypeList = model1;
            model.PaperLaidByPaperCategoryTypeList = model1.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;
        }

        //Added code venkat for dynamic 
        private static tPaperLaidV GetDraftsBill(object param)
        {
            //Added code venkat for dynamic 
            string BillNumber = "";
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;
            if (model.BillNumber != null && model.BillNumber != "null")
            {
                BillNumber = model.BillNumber;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end-----------------------
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            var query = (from PaperLaidV in pCtxt.tPaperLaidV
                         join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         join BillRegister in pCtxt.tBillRegister on PaperLaidV.PaperLaidId equals BillRegister.PaperLaidId
                         join Event in pCtxt.mEvent on PaperLaidV.EventId equals Event.EventId
                         where PaperLaidV.AssemblyId == model.AssemblyId
                             && PaperLaidV.SessionId == model.SessionId
                             //&& (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                             && PaperLaidTemp.DeptSubmittedBy == null
                             && PaperLaidTemp.DeptSubmittedDate == null
                             && PaperLaidTemp.MinisterSubmittedDate == null
                             && PaperLaidTemp.MinisterSubmittedBy == null
                             && PaperLaidV.MinistryId == model.MinistryId
                             && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                             //Added code venkat for dynamic 
                                  && (BillNumber == "" || BillRegister.BillNumber == BillNumber)
                            && (Subject == "" || BillRegister.ShortTitle == Subject)
                         //end--------------------------
                         select new MinisterBillsDraftModel
                        {
                            EventName = Event.EventName,
                            PaperLaidId = PaperLaidV.PaperLaidId,
                            MinistryId = PaperLaidV.MinistryId,
                            NoticeNumber = BillRegister.BillNumber,
                            MinistryName = PaperLaidV.MinistryName,
                            DepartmentId = PaperLaidV.DepartmentId,
                            DeparmentName = PaperLaidV.DeparmentName,
                            FileName = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                            FilePath = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                            PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                            // PaperSent = PaperLaidTemp.SignedFilePath,
                            tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                            //evidhanReferenceNumber = PaperLaidTemp.evidhanReferenceNumber,
                            Version = PaperLaidTemp.Version,
                            FileVersion = PaperLaidTemp.Version,
                            Title = PaperLaidV.Title,
                           
                            DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                            Status = (int)MinisterDashboardStatus.PendingForReply,
                            MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate
                            //MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                        }).ToList();

            foreach (var item in query)
            {
                if (item.DeptSubmittedDate != null)
                {
                    //var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    //var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    //var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                    model.PaperLaidId = item.PaperLaidId;
                }
                else
                {
                    item.DepartmentSubmittedDate = "";
                    model.PaperLaidId = item.PaperLaidId;
                }
            }

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterBillsDraftModel = results;
            model.MinisterBillsDraftModel = results.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;


        }


        //Added code venkat for dynamic 
        private static tPaperLaidV GetsentBill(object param)
        {

            //Added code venkat for dynamic 
            string BillNumber = "";
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;
            if (model.BillNumber != null && model.BillNumber != "null")
            {
                BillNumber = model.BillNumber;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end-------------------------------
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            var query = (from PaperLaidV in pCtxt.tPaperLaidV
                         join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         join BillRegister in pCtxt.tBillRegister on PaperLaidV.PaperLaidId equals BillRegister.PaperLaidId
                         join Event in pCtxt.mEvent on PaperLaidV.EventId equals Event.EventId
                         where PaperLaidV.AssemblyId == model.AssemblyId
                             && PaperLaidV.SessionId == model.SessionId
                             //&& (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                             && (PaperLaidV.MinisterActivePaperId != null && PaperLaidV.MinisterActivePaperId != 0)
                             && PaperLaidTemp.DeptSubmittedBy != null
                             && PaperLaidTemp.DeptSubmittedDate != null
                             //&& PaperLaidTemp.MinisterSubmittedDate != null
                             //&& PaperLaidTemp.MinisterSubmittedBy != null
                           && PaperLaidV.MinistryId == model.MinistryId
                             && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId

                         //Added code venkat for dynamic 
                                && (BillNumber == "" || BillRegister.BillNumber == BillNumber)
                            && (Subject == "" || BillRegister.ShortTitle == Subject)
                         //end--------------------------
                         select new PaperLaidByPaperCategoryType
                         {
                             EventName = Event.EventName,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             MinistryId = PaperLaidV.MinistryId,
                             NoticeNumber = BillRegister.BillNumber,
                             MinistryName = PaperLaidV.MinistryName,
                             DepartmentId = PaperLaidV.DepartmentId,
                             DeparmentName = PaperLaidV.DeparmentName,
                             Version = PaperLaidTemp.Version,
                             FileName = PaperLaidTemp.SignedFilePath,
                             FilePath = PaperLaidTemp.SignedFilePath,
                             tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                             PaperSent = PaperLaidTemp.SignedFilePath,
                             FileVersion = PaperLaidTemp.Version,
                             Title = PaperLaidV.Title,
                             evidhanReferenceNumber = PaperLaidTemp.evidhanReferenceNumber,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             MinisterSubmittedDate = PaperLaidTemp.DeptSubmittedDate
                             // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).ToList();

            foreach (var item in query)
            {
                if (item.MinisterSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("yyyy");
                    item.MinisterSubmittedDatemini = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                }
                else
                {
                    item.MinisterSubmittedDatemini = "";
                }
            }


            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.PaperLaidByPaperCategoryTypeList = results;
            model.PaperLaidByPaperCategoryTypeList = results.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;

        }
        //Added code venkat for dynamic 
        private static tPaperLaidV GetBillULOBList(object param)
        {
            //Added code venkat for dynamic 
            string BillNumber = "";
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;
            if (model.BillNumber != null && model.BillNumber != "null")
            {
                BillNumber = model.BillNumber;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end------------------------
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            var query = (from PaperLaidV in pCtxt.tPaperLaidV
                         join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         join BillRegister in pCtxt.tBillRegister on PaperLaidV.PaperLaidId equals BillRegister.PaperLaidId
                         join Event in pCtxt.mEvent on PaperLaidV.EventId equals Event.EventId
                         where PaperLaidV.AssemblyId == model.AssemblyId
                             && PaperLaidV.SessionId == model.SessionId
                             && (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                             && (PaperLaidV.MinisterActivePaperId != null && PaperLaidV.MinisterActivePaperId != 0)
                             && PaperLaidTemp.DeptSubmittedBy != null
                             && PaperLaidTemp.DeptSubmittedDate != null
                             && PaperLaidTemp.MinisterSubmittedDate != null
                             && PaperLaidTemp.MinisterSubmittedBy != null
                           && PaperLaidV.MinistryId == model.MinistryId
                             && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                             && PaperLaidV.DesireLayingDate > DateTime.Now
                             && PaperLaidV.LaidDate == null && PaperLaidV.LOBRecordId != null
                             //Added code venkat for dynamic 
                                 && (BillNumber == "" || BillRegister.BillNumber == BillNumber)
                            && (Subject == "" || BillRegister.ShortTitle == Subject)
                         //end--------------------
                         select new PaperLaidByPaperCategoryType
                         {
                             EventName = Event.EventName,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             MinistryId = PaperLaidV.MinistryId,
                             NoticeNumber = BillRegister.BillNumber,
                             MinistryName = PaperLaidV.MinistryName,
                             DepartmentId = PaperLaidV.DepartmentId,
                             DeparmentName = PaperLaidV.DeparmentName,
                             FileName = PaperLaidTemp.SignedFilePath,
                             FilePath = PaperLaidTemp.SignedFilePath,
                             Version = PaperLaidTemp.Version,
                             tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                             PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                             FileVersion = PaperLaidTemp.Version,
                             Title = PaperLaidV.Title,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate
                             // MemberName = (from p in pCtxt.tQuestions join e in pCtxt.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in pCtxt.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).ToList();




            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.PaperLaidByPaperCategoryTypeList = results;
            model.PaperLaidByPaperCategoryTypeList = results.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;
        }

        //Added code venkat for dynamic 
        private static tPaperLaidV GetBillLIHList(object param)
        {
            //Added code venkat for dynamic 
            string BillNumber = "";
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;
            if (model.BillNumber != null && model.BillNumber != "null")
            {
                BillNumber = model.BillNumber;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end----------------------
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            var query = (from PaperLaidV in pCtxt.tPaperLaidV
                         join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         join Event in pCtxt.mEvent on PaperLaidV.EventId equals Event.EventId
                         join BillRegister in pCtxt.tBillRegister on PaperLaidV.PaperLaidId equals BillRegister.PaperLaidId
                         where PaperLaidV.AssemblyId == model.AssemblyId
                             && PaperLaidV.SessionId == model.SessionId
                             && (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                             && (PaperLaidV.MinisterActivePaperId != null && PaperLaidV.MinisterActivePaperId != 0)
                             && PaperLaidTemp.DeptSubmittedBy != null
                             && PaperLaidTemp.DeptSubmittedDate != null
                             && PaperLaidTemp.MinisterSubmittedDate != null
                             && PaperLaidTemp.MinisterSubmittedBy != null
                            && PaperLaidV.MinistryId == model.MinistryId
                             && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                             && PaperLaidV.LaidDate <= DateTime.Now
                             && PaperLaidV.LaidDate != null && PaperLaidV.LOBRecordId != null
                             //Added code venkat for dynamic 
                                && (BillNumber == "" || BillRegister.BillNumber == BillNumber)
                            && (Subject == "" || BillRegister.ShortTitle == Subject)
                         //end----------------------------------

                         select new PaperLaidByPaperCategoryType
                         {
                             EventName = Event.EventName,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             MinistryId = PaperLaidV.MinistryId,
                             NoticeNumber = BillRegister.BillNumber,
                             MinistryName = PaperLaidV.MinistryName,
                             DepartmentId = PaperLaidV.DepartmentId,
                             DeparmentName = PaperLaidV.DeparmentName,
                             FileName = PaperLaidTemp.SignedFilePath,
                             Version = PaperLaidTemp.Version,
                             FilePath = PaperLaidTemp.SignedFilePath,
                             tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                             PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                             FileVersion = PaperLaidTemp.Version,
                             Title = PaperLaidV.Title,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.PaperLaidByPaperCategoryTypeList = results;
            model.PaperLaidByPaperCategoryTypeList = results.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;
        }

        private static tPaperLaidV GetBillPLIHList(object param)
        {

            tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext pCtxt = new PaperLaidMinisterContext();

            var query = (from PaperLaidV in pCtxt.tPaperLaidV
                         join PaperLaidTemp in pCtxt.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         join BillRegister in pCtxt.tBillRegister on PaperLaidV.PaperLaidId equals BillRegister.PaperLaidId
                         join Event in pCtxt.mEvent on PaperLaidV.EventId equals Event.EventId
                         where PaperLaidV.AssemblyId == model.AssemblyId
                             && PaperLaidV.SessionId == model.SessionId
                             && (PaperLaidV.DeptActivePaperId != null && PaperLaidV.DeptActivePaperId != 0)
                             && (PaperLaidV.MinisterActivePaperId != null && PaperLaidV.MinisterActivePaperId != 0)
                             && PaperLaidTemp.DeptSubmittedBy != null
                             && PaperLaidTemp.DeptSubmittedDate != null
                             && PaperLaidTemp.MinisterSubmittedDate != null
                             && PaperLaidTemp.MinisterSubmittedBy != null
                             && PaperLaidV.MinistryId == model.MinistryId
                             && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                             && PaperLaidV.LaidDate == null
                             && PaperLaidV.LOBRecordId != null

                         select new PaperLaidByPaperCategoryType
                         {
                             EventName = Event.EventName,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             MinistryId = PaperLaidV.MinistryId,
                             NoticeNumber = BillRegister.BillNumber,
                             MinistryName = PaperLaidV.MinistryName,
                             DepartmentId = PaperLaidV.DepartmentId,
                             DeparmentName = PaperLaidV.DeparmentName,
                             FileName = PaperLaidTemp.SignedFilePath,
                             FilePath = PaperLaidTemp.SignedFilePath,
                             Version = PaperLaidTemp.Version,
                             tpaperLaidTempId = PaperLaidTemp.PaperLaidTempId,
                             PaperSent = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                             FileVersion = PaperLaidTemp.Version,
                             Title = PaperLaidV.Title,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                             Status = (int)MinisterDashboardStatus.PendingForReply,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.PaperLaidByPaperCategoryTypeList = results;
            model.PaperLaidByPaperCategoryTypeList = results.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();
            return model;

        }

        private static object GetCountForBillsandNotice(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;


            ////------notice-----///
            model.NoticeInboxCount = GetNoticeInBoxList(model).ResultCount;
            model.NoticeReplaySent = GetNoticeReplySentList(model).ResultCount;
            model.NoticeUPLOBCount = GetNoticeULOBList(model).ResultCount;
            model.NoticeLIHCount = GetNoticeLIHList(model).ResultCount;
            model.NoticePLIHCount = GetNoticePLIHList(model).ResultCount;

            //Bills
            model.TotalBillDraftCount = GetDraftsBill(model).ResultCount;
            model.TotalBillSentCount = GetsentBill(model).ResultCount;
            model.BillUPLOBCount = GetBillULOBList(model).ResultCount;
            model.BillLIHCount = GetBillLIHList(model).ResultCount;

            model.BillPLIHCount = GetBillPLIHList(model).ResultCount;
            return model;
        }

        #endregion



        #region "OtherPaperLaid"

        //static tPaperLaidV GetOtherPaperLaidPendingsDetails(object param)
        //{
        //    PaperLaidMinisterContext db = new PaperLaidMinisterContext();

        //    tPaperLaidV model = param as tPaperLaidV;
        //    int paperCategoryId = 5;



        //    var query = (from PaperLaidV in db.tPaperLaidV
        //                 join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
        //                 join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
        //                 join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
        //                 where
        //                  Event.PaperCategoryTypeId == paperCategoryId
        //                 && PaperLaidTemp.DeptSubmittedBy != null
        //                 && PaperLaidTemp.DeptSubmittedDate != null
        //                 && PaperLaidTemp.MinisterSubmittedBy == null
        //                 && PaperLaidTemp.MinisterSubmittedDate == null
        //                 && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId

        //                 select new MinisterPaperMovementModel
        //                 {
        //                     DeptActivePaperId = PaperLaidV.DeptActivePaperId,
        //                     PaperLaidId = PaperLaidV.PaperLaidId,

        //                     EventName = Event.EventName,
        //                     EventId = Event.EventId,
        //                     Title = PaperLaidV.Title,
        //                     DeparmentName = PaperLaidV.DeparmentName,
        //                     MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
        //                     PaperTypeID = (int)PaperLaidV.PaperLaidId,
        //                     actualFilePath = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,

        //                     PaperCategoryTypeId = Event.PaperCategoryTypeId,
        //                     BusinessType = Event.EventName,
        //                     version = PaperLaidTemp.Version,
        //                     Status = (int)MinisterDashboardStatus.PendingForReply,
        //                     PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == PaperLaidV.EventId select p.Name).FirstOrDefault(),
        //                     MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),


        //                 }).ToList();

        //    int totalRecords = query.Count();
        //    var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

        //    model.ResultCount = totalRecords;
        //    model.MinisterPendingList = results;

        //    return model;
        //}

        //Added code venkat for dynamic 
        //Added code venkat for dynamic 
        static tPaperLaidV GetOtherPaperLaidPendingsDetails(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            //Added code venkat for dynamic 
            int? BusinessType = 0;
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;

            if (model.BType != null && model.MinID != 0)
            {
                BusinessType = model.BType;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end---------------------------------------
            int paperCategoryId = 5;



            var query = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                         && PaperLaidTemp.DeptSubmittedBy == null
                         && PaperLaidTemp.DeptSubmittedDate == null
                         && PaperLaidTemp.MinisterSubmittedBy == null
                         && PaperLaidTemp.MinisterSubmittedDate == null
                         && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                         && ministery.MinistryID == model.MinistryId
                         && PaperLaidV.AssemblyId == model.AssemblyId
                         && PaperLaidV.SessionId == model.SessionId
                             //Added code venkat for dynamic 
                            && (BusinessType == 0 || Event.EventId == BusinessType)
                            && (Subject == "" || PaperLaidV.Title.Contains(Subject))
                         //end-----------------------------
                         select new MinisterOtherPaperDraft
                         {
                             DeptActivePaperId = PaperLaidV.DeptActivePaperId,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,

                             EventName = Event.EventName,
                             EventId = Event.EventId,
                             Title = PaperLaidV.Title,
                             DeparmentName = PaperLaidV.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)PaperLaidV.PaperLaidId,
                             actualFilePath = PaperLaidTemp.FilePath + PaperLaidTemp.FileName,
                             //actualFilePath = PaperLaidTemp.SignedFilePath,
                             PaperCategoryTypeId = Event.PaperCategoryTypeId,
                             BusinessType = Event.EventName,
                             version = PaperLaidTemp.Version,
                             Status = (int)MinisterDashboardStatus.PendingForReply,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == PaperLaidV.EventId select p.Name).FirstOrDefault(),
                             MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),


                         }).ToList();



            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterOtherPaperDraft = results;


           


            foreach (var item in query)
            {
                if (item.DeptSubmittedDate != null)
                {
                   
                    item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                    model.PaperLaidId = item.PaperLaidId;
                }
                else
                {
                    item.DepartmentSubmittedDate = "";
                    model.PaperLaidId = item.PaperLaidId;
                }
                //item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
            }


            return model;
        }


        //Added code venkat for dynamic 
        static tPaperLaidV GetSubmittedOtherPaperLaidList(object param)
        {

            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            //Added code venkat for dynamic 
            int? BusinessType = 0;
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;
            if (model.BType != null && model.MinID != 0)
            {
                BusinessType = model.BType;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end-------------------------------
            int paperCategoryId = 5;



            var query = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                         && PaperLaidTemp.DeptSubmittedBy != null
                         && PaperLaidTemp.DeptSubmittedDate != null
                         //&& PaperLaidTemp.MinisterSubmittedBy != null
                         //&& PaperLaidTemp.MinisterSubmittedDate != null
                        && (PaperLaidV.MinisterActivePaperId != null && PaperLaidV.MinisterActivePaperId != 0)
                         && PaperLaidV.MinistryId == model.MinistryId
                         && PaperLaidV.AssemblyId == model.AssemblyId
                         && PaperLaidV.SessionId == model.SessionId
                         && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                             //Added code venkat for dynamic 
                           && (BusinessType == 0 || Event.EventId == BusinessType)
                            && (Subject == "" || PaperLaidV.Title.Contains(Subject))
                         //end----------------------------------
                         select new MinisterPaperMovementModel
                         {
                             DeptActivePaperId = PaperLaidV.DeptActivePaperId,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             EventName = Event.EventName,
                             EventId = Event.EventId,
                             Title = PaperLaidV.Title,
                             DeparmentName = PaperLaidV.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)PaperLaidV.PaperLaidId,
                             actualFilePath = PaperLaidTemp.SignedFilePath,
                             PaperCategoryTypeId = Event.PaperCategoryTypeId,
                             BusinessType = Event.EventName,
                             evidhanReferenceNumber = PaperLaidTemp.evidhanReferenceNumber,
                             version = PaperLaidTemp.Version,
                             DeptSubmittedDate = PaperLaidTemp.DeptSubmittedDate,
                             MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == PaperLaidV.EventId select p.Name).FirstOrDefault()

                         }).ToList();

            foreach (var item in query)
            {
                if (item.DeptSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.DeptSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.DeptSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = Convert.ToDateTime(item.DeptSubmittedDate).ToString("dd/MM/yyyy hh:mm:ss tt");
                }
                else
                {
                    item.DepartmentSubmittedDate = "";
                }
            }

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterPendingList = results;

            return model;

        }
        //Added code venkat for dynamic 
        static tPaperLaidV UpcomingLOBByOtherpaperLaidId(object param)
        {

            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            //Added code venkat for dynamic 
            int? BusinessType = 0;
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;
            if (model.BType != null && model.MinID != 0)
            {
                BusinessType = model.BType;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end-----------------------
            int paperCategoryId = 5;



            var query = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                         && PaperLaidTemp.DeptSubmittedBy != null
                         && PaperLaidTemp.DeptSubmittedDate != null
                          && PaperLaidTemp.MinisterSubmittedBy != null
                          && PaperLaidTemp.MinisterSubmittedDate != null
                         && PaperLaidV.DesireLayingDate > DateTime.Now
                         && PaperLaidV.LaidDate == null
                         && PaperLaidV.LOBRecordId != null
                         && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                         && ministery.MinistryID == model.MinistryId
                         && PaperLaidV.AssemblyId == model.AssemblyId
                         && PaperLaidV.SessionId == model.SessionId
                             //Added code venkat for dynamic 
                          && (BusinessType == 0 || Event.EventId == BusinessType)
                            && (Subject == "" || PaperLaidV.Title.Contains(Subject))
                         //end-------------------------------------------------------
                         select new MinisterPaperMovementModel
                         {
                             DeptActivePaperId = PaperLaidV.DeptActivePaperId,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             EventName = Event.EventName,
                             EventId = Event.EventId,
                             Title = PaperLaidV.Title,
                             DeparmentName = PaperLaidV.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)PaperLaidV.PaperLaidId,
                             actualFilePath = PaperLaidTemp.SignedFilePath,
                             PaperCategoryTypeId = Event.PaperCategoryTypeId,
                             BusinessType = Event.EventName,
                             version = PaperLaidTemp.Version,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                             MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == PaperLaidV.EventId select p.Name).FirstOrDefault()

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterPendingList = results;

            return model;


        }

        //Added code venkat for dynamic 
        static tPaperLaidV GetOtherPaperLaidInHouseByType(object param)
        {

            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            //Added code venkat for dynamic 
            int? BusinessType = 0;
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;
            if (model.BType != null && model.MinID != 0)
            {
                BusinessType = model.BType;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end------------------------------
            int paperCategoryId = 5;



            var query = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                         && PaperLaidTemp.DeptSubmittedBy != null
                         && PaperLaidTemp.DeptSubmittedDate != null
                          && PaperLaidTemp.MinisterSubmittedBy != null
                          && PaperLaidTemp.MinisterSubmittedDate != null
                         && PaperLaidV.LaidDate <= DateTime.Now
                         && PaperLaidV.LaidDate != null
                         && PaperLaidV.LOBRecordId != null
                         && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                          && ministery.MinistryID == model.MinistryId
                           && PaperLaidV.AssemblyId == model.AssemblyId
                         && PaperLaidV.SessionId == model.SessionId
                             //Added code venkat for dynamic 
                            && (BusinessType == 0 || Event.EventId == BusinessType)
                            && (Subject == "" || PaperLaidV.Title.Contains(Subject))
                         //end-----------------------------
                         select new MinisterPaperMovementModel
                         {
                             DeptActivePaperId = PaperLaidV.DeptActivePaperId,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             EventName = Event.EventName,
                             EventId = Event.EventId,
                             Title = PaperLaidV.Title,
                             DeparmentName = PaperLaidV.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)PaperLaidV.PaperLaidId,
                             actualFilePath = PaperLaidTemp.SignedFilePath,
                             PaperCategoryTypeId = Event.PaperCategoryTypeId,
                             BusinessType = Event.EventName,
                             version = PaperLaidTemp.Version,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                             MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             Status = (int)MinisterDashboardStatus.RepliesSent,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == PaperLaidV.EventId select p.Name).FirstOrDefault()

                         }).ToList();

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterPendingList = results;

            return model;

        }

        //Added code venkat for dynamic 
        static tPaperLaidV GetPendingToLayOtherPaperLaidByType(object param)
        {

            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            //Added code venkat for dynamic 
            int? BusinessType = 0;
            string Subject = "";

            tPaperLaidV model = param as tPaperLaidV;

            if (model.BType != null && model.BType != 0)
            {
                BusinessType = model.BType;
            }
            if (model.Subject != null && model.Subject != "null")
            {
                Subject = model.Subject;
            }
            //end-------------------------------------
            int paperCategoryId = 5;



            var query = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                         && PaperLaidTemp.DeptSubmittedBy != null
                         && PaperLaidTemp.DeptSubmittedDate != null
                         && PaperLaidTemp.MinisterSubmittedBy != null
                         && PaperLaidTemp.MinisterSubmittedDate != null
                         && PaperLaidV.LaidDate == null
                         && PaperLaidV.LOBRecordId != null
                         && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                         && ministery.MinistryID == model.MinistryId
                         && PaperLaidV.AssemblyId == model.AssemblyId
                         && PaperLaidV.SessionId == model.SessionId
                             //Added code venkat for dynamic 
                           && (BusinessType == 0 || Event.EventId == BusinessType)
                            && (Subject == "" || PaperLaidV.Title.Contains(Subject))
                         //end----------------------------------------------------
                         select new MinisterPaperMovementModel
                         {
                             DeptActivePaperId = PaperLaidV.DeptActivePaperId,
                             PaperLaidId = PaperLaidV.PaperLaidId,
                             EventName = Event.EventName,
                             EventId = Event.EventId,
                             Title = PaperLaidV.Title,
                             DeparmentName = PaperLaidV.DeparmentName,
                             MinisterName = ministery.MinisterName + ", " + ministery.MinistryName,
                             PaperTypeID = (int)PaperLaidV.PaperLaidId,
                             actualFilePath = PaperLaidTemp.SignedFilePath,
                             PaperCategoryTypeId = Event.PaperCategoryTypeId,
                             BusinessType = Event.EventName,
                             version = PaperLaidTemp.Version,
                             MinisterSubmittedDate = PaperLaidTemp.MinisterSubmittedDate,
                             MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             Status = (int)MinisterDashboardStatus.PendingForReply,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == PaperLaidV.EventId select p.Name).FirstOrDefault()

                         }).ToList();


            foreach (var item in query)
            {
                if (item.MinisterSubmittedDate != null)
                {
                    var curr_date = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("dd");
                    var curr_month = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("MM");
                    var curr_year = Convert.ToDateTime(item.MinisterSubmittedDate).ToString("yyyy");
                    item.DepartmentSubmittedDate = curr_date + "/" + curr_month + "/" + curr_year;
                }
                else
                {
                    item.DepartmentSubmittedDate = "";
                }
            }

            int totalRecords = query.Count();
            var results = query.Skip((model.PageIndex - 1) * model.PAGE_SIZE).Take(model.PAGE_SIZE).ToList();

            model.ResultCount = totalRecords;
            model.MinisterPendingList = results;

            return model;

        }

        static List<mEvent> GetOtherPaperLaidPaperCategoryType(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();

            mEvent model = param as mEvent;


            var query = (from dist in db.mEvent
                         where dist.PaperCategoryTypeId == 5
                         select dist);

            return query.ToList();
        }


        static object GetCountForOtherPaperLaidTypes(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            //  PaperMovementModel model = param as PaperMovementModel;
            tPaperLaidV mod = param as tPaperLaidV;
            int paperCategoryId = 5;
            int count = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                             && PaperLaidTemp.DeptSubmittedBy != null
                             && PaperLaidTemp.DeptSubmittedDate != null
                              && PaperLaidTemp.MinisterSubmittedBy == null
                              && PaperLaidTemp.MinisterSubmittedDate == null
                           && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                           && ministery.MinistryID == mod.MinistryId
                           && PaperLaidV.AssemblyId == mod.AssemblyId
                           && PaperLaidV.SessionId == mod.SessionId
                         select new MinisterPaperMovementModel
                         {
                         }).Count();

            var query = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                         && PaperLaidTemp.DeptSubmittedBy != null
                         && PaperLaidTemp.DeptSubmittedDate != null
                             && PaperLaidTemp.MinisterSubmittedBy != null
                          && PaperLaidTemp.MinisterSubmittedDate != null
                           && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                           && ministery.MinistryID == mod.MinistryId
                           && PaperLaidV.AssemblyId == mod.AssemblyId
                           && PaperLaidV.SessionId == mod.SessionId
                         select new MinisterPaperMovementModel
                         {
                         }).Count();


            db.Close();
            mod.OtherPaperCount = count;
            mod.Count = query;
            return mod;
        }

        static object GetOtherPaperLaidCounters(object param)
        {
            tPaperLaidV model = param as tPaperLaidV;


            model.TotalOtherPaperLaidUpComingLOB = UpcomingLOBByOtherpaperLaidId(model).ResultCount;
            model.TotalOtherPaperLaidInTheHouse = GetOtherPaperLaidInHouseByType(model).ResultCount;
            model.TotalOtherPaperPendingToLay = GetPendingToLayOtherPaperLaidByType(model).ResultCount;

            return model;
        }

        #endregion


        public static object InsertSignPathAttachment(object param)
        {
            tPaperLaidTemp pT = param as tPaperLaidTemp;
            string[] obja = pT.FilePath.Split(',');

            foreach (var item in obja)
            {
                using (var obj1 = new PaperLaidMinisterContext())
                {
                    long id = Convert.ToInt16(item);

                    var PaperLaidObj = (from PaperLaidModel in obj1.tPaperLaidV
                                        join ministryModel in obj1.tPaperLaidTemp on PaperLaidModel.DeptActivePaperId equals ministryModel.PaperLaidTempId
                                        where PaperLaidModel.PaperLaidId == id
                                        select ministryModel).FirstOrDefault();
                    if (PaperLaidObj != null)
                    {
                        var filename = PaperLaidObj.FileName;
                        string[] Afile = filename.Split('.');
                        filename = Afile[0];
                        var path = "/PaperLaid/" + pT.AssemblyId + "/" + pT.SessionId + "/Signed/" + filename + "_signed.pdf";
                        PaperLaidObj.SignedFilePath = path;
                        PaperLaidObj.MinisterSubmittedBy = pT.MinisterSubmittedBy;
                        PaperLaidObj.MinisterSubmittedDate = pT.MinisterSubmittedDate;
                    }
                    //obj.tBillRegisterVs.Add(model);
                    obj1.SaveChanges();
                    obj1.Close();
                }
            }

            string meg = "";
            return meg;
        }


        #region DetailsForPaper

        //static object MinisterPaperLaidDetails(object param)
        //{


        //    // tPaperLaidV model = param as tPaperLaidV;
        //    PaperLaidMinisterContext db = new PaperLaidMinisterContext();
        //    mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
        //    var query = (from m in db.tPaperLaidV

        //                 join c in db.mEvent on m.EventId equals c.EventId
        //                 join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
        //                 join Questions in db.tQuestions on m.PaperLaidId equals Questions.PaperLaidId
        //                 join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
        //                 //where (t.MinisterSubmittedBy == null) && (t.MinisterSubmittedDate == null)
        //                 //&& (t.PaperLaidId == m.PaperLaidId)
        //                 where model.PaperLaidId == m.PaperLaidId
        //                 && m.MinisterActivePaperId == t.PaperLaidTempId
        //                 select new mMinisteryMinisterModel
        //                 {
        //                     DeptActivePaperId = m.DeptActivePaperId,
        //                     MinisterName = minis.MinisterName + ", " + minis.MinistryName,
        //                     PaperLaidId = m.PaperLaidId,
        //                     EventName = c.EventName,
        //                     Title = m.Title,
        //                     Version = t.Version,
        //                     ProvisionUnderWhich = m.ProvisionUnderWhich,
        //                     DeparmentName = m.DeparmentName,
        //                     Remark = m.Remark,
        //                     FilePath = t.FilePath,
        //                     Description = m.Description,
        //                     SignedFilePath = t.SignedFilePath,
        //                     FileName = t.FileName,
        //                     DesireLayingDate = m.DesireLayingDate,
        //                     MinisterSubmittedDate = t.MinisterSubmittedDate,
        //                     MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),

        //                     PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
        //                     PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault()
        //                     //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

        //                 }).FirstOrDefault();

        //    var item = query;

        //    switch (item.PaperCategoryTypeId)
        //    {
        //        case 1:
        //            item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

        //            break;
        //        case 2:
        //            item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
        //            break;
        //        case 4:
        //            item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
        //            break;
        //        default:
        //            item.Number = (from m in db.tPaperLaidV where m.PaperLaidId == item.PaperLaidId select m.PaperLaidId).FirstOrDefault().ToString();
        //            break;
        //    }

        //    return query;



        //    //// tPaperLaidV model = param as tPaperLaidV;
        //    //PaperLaidMinisterContext db = new PaperLaidMinisterContext();
        //    //mMinisteryMinisterModel model = param as mMinisteryMinisterModel;

        //    //var query = (from m in db.tPaperLaidV

        //    //             join c in db.mEvent on m.EventId equals c.EventId
        //    //             join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
        //    //             join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
        //    //             //join PaperCategoryType in db.mPaperCategoryType on c.PaperCategoryTypeId equals PaperCategoryType.PaperCategoryTypeId
        //    //             join Questions in db.tQuestions on m.PaperLaidId equals Questions.PaperLaidId


        //    //             where m.PaperLaidId == model.PaperLaidId
        //    //             && m.DeptActivePaperId == t.PaperLaidTempId
        //    //             select new mMinisteryMinisterModel
        //    //             {
        //    //                 DeptActivePaperId = m.DeptActivePaperId,
        //    //                 MinisterName = minis.MinisterName + ", " + minis.MinistryName,
        //    //                 PaperLaidId = m.PaperLaidId,
        //    //                 EventName = c.EventName,
        //    //                 Title = m.Title,
        //    //                 Version = t.Version,
        //    //                 ProvisionUnderWhich = m.ProvisionUnderWhich,
        //    //                 DeparmentName = m.DeparmentName,
        //    //                 Remark = m.Remark,
        //    //                 FilePath = t.FilePath,
        //    //                 Description = m.Description,
        //    //                 SignedFilePath = t.SignedFilePath,
        //    //                 FileName = t.FileName,
        //    //                 DesireLayingDate = (from sessionDates in db.mSessionDates where sessionDates.Id == m.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
        //    //                 MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
        //    //                 MinisterSubmittedDate = t.MinisterSubmittedDate,
        //    //                 PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
        //    //                 //PaperCategoryTypeName = PaperCategoryType.Name,//(from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
        //    //                 //PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault(),
        //    //                 //MemberName = (from mc in db.mMember where mc.MemberCode == questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
        //    //                 PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault()
        //    //                 //PaperCategoryTypeId = PaperCategoryType.PaperCategoryTypeId,//(from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault(),
        //    //                 //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
        //    //             }).FirstOrDefault();

        //    //var item = query;

        //    //switch (item.PaperCategoryTypeId)
        //    //{
        //    //    case 1:
        //    //        item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

        //    //        break;
        //    //    case 2:
        //    //        item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
        //    //        break;
        //    //    case 4:
        //    //        item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
        //    //        break;
        //    //    default:
        //    //        item.Number = (from m in db.tPaperLaidV where m.PaperLaidId == item.PaperLaidId select m.PaperLaidId).FirstOrDefault().ToString();
        //    //        break;
        //    //}

        //    //return item;

        //}

        static object MinisterPaperLaidDetails(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;
            var query = (from m in db.tPaperLaidV

                         join c in db.mEvent on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         join Questions in db.tQuestions on m.PaperLaidId equals Questions.PaperLaidId
                         join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
                         //where (t.MinisterSubmittedBy == null) && (t.MinisterSubmittedDate == null)
                         //&& (t.PaperLaidId == m.PaperLaidId)
                         where model.PaperLaidId == m.PaperLaidId
                              && m.DeptActivePaperId == t.PaperLaidTempId
                         //&& m.AssemblyId == model.AssemblyId
                         //&& m.SessionId == model.SessionId
                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             MinisterName = minis.MinisterName + ", " + minis.MinistryName,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             Version = t.Version,
                             ProvisionUnderWhich = m.ProvisionUnderWhich,
                             DeparmentName = m.DeparmentName,
                             Remark = m.Remark,
                             FilePath = FileStructurePath,
                             Description = m.Description,
                             SignedFilePath = FileStructurePath + t.FilePath + t.FileName,
                             FileName = t.FileName,
                             SupFileName = t.SupFileName,
                             DocFileName = t.DocFileName,
                             SupDocFileName = t.SupDocFileName,
                             IsFinalApprove = Questions.IsFinalApproved,
                             DesireLayingDate = (from sessionDates in db.mSessionDates where sessionDates.Id == m.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
                             // MinisterSubmittedDate = t.MinisterSubmittedDate,
                             MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault()
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).FirstOrDefault();

            var item = query;

            switch (item.PaperCategoryTypeId)
            {
                case 1:
                    if (item.IsFinalApprove == true)
                    {
                        item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();
                    }
                    else
                    {
                        item.Number = "";
                    }

                    break;
                case 2:
                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                    break;
                case 4:
                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                    break;
                default:
                    item.Number = (from m in db.tPaperLaidV where m.PaperLaidId == item.PaperLaidId select m.PaperLaidId).FirstOrDefault().ToString();
                    break;
            }

            return query;

        }

        static object SubmittedMinisterPaperLaidDetails(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "SecureFileAccessingUrlPath" select a).FirstOrDefault();
            string FileStructurePath = siteSettingData.SettingValue;
            var query = (from m in db.tPaperLaidV

                         join c in db.mEvent on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         join Questions in db.tQuestions on m.PaperLaidId equals Questions.PaperLaidId
                         join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
                         //where (t.MinisterSubmittedBy == null) && (t.MinisterSubmittedDate == null)
                         //&& (t.PaperLaidId == m.PaperLaidId)
                         where model.PaperLaidId == m.PaperLaidId
                         && m.MinisterActivePaperId == t.PaperLaidTempId
                         //&& m.AssemblyId == model.AssemblyId
                         //    && m.SessionId == model.SessionId
                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             MinisterName = minis.MinisterName + ", " + minis.MinistryName,
                             PaperLaidId = m.PaperLaidId,
                             DeptSubmittedDate = t.DeptSubmittedDate,
                             EventName = c.EventName,
                             Title = m.Title,
                             Version = t.Version,
                             ProvisionUnderWhich = m.ProvisionUnderWhich,
                             DeparmentName = m.DeparmentName,
                             Remark = m.Remark,
                             //FilePath = t.FilePath,
                             FilePath = FileStructurePath,
                             SupFileName = FileStructurePath + t.SupFileName,
                             DocFileName = FileStructurePath + t.DocFileName,
                             SupDocFileName = FileStructurePath + t.SupDocFileName,
                             Description = m.Description,
                             SignedFilePath = FileStructurePath + t.SignedFilePath,
                             FileName = t.FileName,
                             SupFileVersion = t.SupVersion,

                             DesireLayingDate = (from sessionDates in db.mSessionDates where sessionDates.Id == m.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
                             // MinisterSubmittedDate = t.MinisterSubmittedDate,
                             MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault()
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).FirstOrDefault();

            var item = query;

            switch (item.PaperCategoryTypeId)
            {
                case 1:
                    item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId && m.IsFinalApproved == true select m.QuestionNumber).FirstOrDefault().ToString();

                    break;
                case 2:
                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                    break;
                case 4:
                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                    break;
                default:
                    item.Number = (from m in db.tPaperLaidV where m.PaperLaidId == item.PaperLaidId select m.PaperLaidId).FirstOrDefault().ToString();
                    break;
            }
            return query;

        }

        static List<mMinisteryMinisterModel> GetChangedPaperDetails(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            int PaperLaidId = Convert.ToInt32(param);

            var query = (from tempmodel in db.tPaperLaidTemp
                         join c in db.tPaperLaidV on tempmodel.PaperLaidId equals c.PaperLaidId
                         where tempmodel.PaperLaidId == PaperLaidId
                         orderby tempmodel.PaperLaidTempId descending
                         select new mMinisteryMinisterModel
                         {
                             FileName = tempmodel.FileName,
                             Version = tempmodel.Version,
                             FilePath = tempmodel.FilePath,
                             SignedFilePath = tempmodel.SignedFilePath,
                             MinisterSubmittedDate = tempmodel.MinisterSubmittedDate,
                             PaperLaidId = tempmodel.PaperLaidId,
                             PaperLaidTempId = tempmodel.PaperLaidTempId,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == c.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == c.EventId select p.PaperCategoryTypeId).FirstOrDefault()

                         });

            return query.ToList();

        }


        static List<mMinisteryMinisterModel> GetSubmitChangedPaperDetails(object param)
        {

            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            int PaperLaidId = Convert.ToInt32(param);

            var query = (from tempmodel in db.tPaperLaidTemp
                         join c in db.tPaperLaidV on tempmodel.PaperLaidId equals c.PaperLaidId
                         where tempmodel.PaperLaidId == PaperLaidId
                         && tempmodel.DeptSubmittedBy != null
                         && tempmodel.DeptSubmittedDate != null
                         && tempmodel.MinisterSubmittedDate != null

                         orderby tempmodel.PaperLaidTempId descending
                         select new mMinisteryMinisterModel
                         {
                             FileName = tempmodel.FileName,
                             Version = tempmodel.Version,
                             FilePath = tempmodel.FilePath,
                             SignedFilePath = tempmodel.SignedFilePath,
                             MinisterSubmittedDate = tempmodel.MinisterSubmittedDate,
                             PaperLaidId = tempmodel.PaperLaidId,
                             PaperLaidTempId = tempmodel.PaperLaidTempId,
                             MinisterActivePaperId = c.MinisterActivePaperId,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == c.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == c.EventId select p.PaperCategoryTypeId).FirstOrDefault(),
                             MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                         });


            return query.ToList();

        }


        static List<mMinisteryMinisterModel> ChangedGetMinisterPaperAttachment(object param)
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();

            int paperlaididid = Convert.ToInt32(param);


            var query = (from ministerModel in db.tPaperLaidTemp

                         where ministerModel.PaperLaidTempId == paperlaididid
                         select new mMinisteryMinisterModel
                         {
                             FilePath = ministerModel.FilePath + ministerModel.FileName,
                             PaperLaidTempId = ministerModel.PaperLaidTempId,
                             SignedFilePath = ministerModel.SignedFilePath

                         });


            return query.ToList();
        }



        static object BillsPaperLaidDetails(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var query = (from m in db.tPaperLaidV

                         join c in db.mEvent on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         //join Questions in db.tQuestions on m.PaperLaidId equals Questions.PaperLaidId
                         join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
                         // join d in db.mDocumentTypes on m.PaperTypeID equals d.DocumentTypeID
                         //where (t.MinisterSubmittedBy == null) && (t.MinisterSubmittedDate == null)
                         //&& (t.PaperLaidId == m.PaperLaidId)
                         where model.PaperLaidId == m.PaperLaidId
                         && m.DeptActivePaperId == t.PaperLaidTempId
                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             PaperTypeID = m.PaperTypeID,
                             MinisterName = minis.MinisterName + ", " + minis.MinistryName,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             Version = t.Version,
                             ProvisionUnderWhich = m.ProvisionUnderWhich,
                             DeparmentName = m.DeparmentName,
                             Remark = m.Remark,
                             FilePath = t.FilePath,
                             Description = m.Description,
                             SignedFilePath = t.SignedFilePath,
                             FileName = t.FileName,
                             DocFileName = t.DocFileName,
                             SupFileName = t.SupFileName,
                             SupDocFileName = t.SupDocFileName,
                             DesireLayingDate = (from sessionDates in db.mSessionDates where sessionDates.Id == m.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
                             // MinisterSubmittedDate = t.MinisterSubmittedDate,
                             // MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault()
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).FirstOrDefault();

            var item = query;

            if (item.PaperTypeID != null || item.PaperTypeID != 0)
            {
                item.PaperTypeName = (from mpapType in db.mDocumentTypes where mpapType.DocumentTypeID == item.PaperTypeID select mpapType.DocumentType).FirstOrDefault();
            }

            switch (item.PaperCategoryTypeId)
            {
                case 1:
                    item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                    break;
                case 2:
                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                    break;
                case 4:
                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                    break;
                default:
                    item.Number = (from m in db.tPaperLaidV where m.PaperLaidId == item.PaperLaidId select m.PaperLaidId).FirstOrDefault().ToString();
                    break;
            }

            return query;

        }
        static object SubmittedBillsPaperLaidDetails(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var query = (from m in db.tPaperLaidV

                         join c in db.mEvent on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId

                         join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
                         //where (t.MinisterSubmittedBy == null) && (t.MinisterSubmittedDate == null)
                         //&& (t.PaperLaidId == m.PaperLaidId)
                         where model.PaperLaidId == m.PaperLaidId
                         && m.MinisterActivePaperId == t.PaperLaidTempId
                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             MinisterName = minis.MinisterName + ", " + minis.MinistryName,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             Version = t.Version,
                             ProvisionUnderWhich = m.ProvisionUnderWhich,
                             DeparmentName = m.DeparmentName,
                             Remark = m.Remark,
                             FilePath = t.FilePath,
                             Description = m.Description,
                             SignedFilePath = t.SignedFilePath,
                             FileName = t.FileName,
                             //DesireLayingDate = m.DesireLayingDate,
                             DesireLayingDate = (from sessionDates in db.mSessionDates where sessionDates.Id == m.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
                             MinisterSubmittedDate = t.MinisterSubmittedDate,
                             //MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DeptSubmittedDate =t.DeptSubmittedDate,
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault()
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).FirstOrDefault();

            var item = query;

           

            switch (item.PaperCategoryTypeId)
            {
                case 1:
                    item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();
                  

                    break;
                case 2:
                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                    break;
                case 4:
                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                    break;
                default:
                    item.Number = (from m in db.tPaperLaidV where m.PaperLaidId == item.PaperLaidId select m.PaperLaidId).FirstOrDefault().ToString();
                    break;
            }

            return query;

        }



        static object SubmittedNoticePaperLaidDetails(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var query = (from m in db.tPaperLaidV
                         join c in db.mEvent on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
                         join MemberNotices in db.tMemberNotice on m.PaperLaidId equals MemberNotices.PaperLaidId
                         where model.PaperLaidId == m.PaperLaidId
                         && m.DeptActivePaperId == t.PaperLaidTempId
                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             MinisterName = minis.MinisterName + ", " + minis.MinistryName,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             Version = t.Version,
                             ProvisionUnderWhich = m.ProvisionUnderWhich,
                             DeparmentName = m.DeparmentName,
                             DeptSubmittedDate = t.DeptSubmittedDate,
                             Remark = m.Remark,
                             FilePath = t.FilePath,
                             Description = m.Description,
                             DocFileName = t.DocFileName,
                             SignedFilePath = t.SignedFilePath,
                             OriDiaryFileName = MemberNotices.OriDiaryFileName,
                             OriDiaryFilePath = MemberNotices.OriDiaryFilePath,
                             NoticePdfPath = MemberNotices.NoticePdfPath,
                             FileName = t.FileName,
                             MemberName = (from mc in db.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DesireLayingDate = (from sessionDates in db.mSessionDates where sessionDates.Id == m.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
                             // MinisterSubmittedDate = t.MinisterSubmittedDate,
                             // MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault()
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).FirstOrDefault();

            var item = query;

            switch (item.PaperCategoryTypeId)
            {
                case 1:
                    item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                    break;
                case 2:
                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                    break;
                case 4:
                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                    break;
                default:
                    item.Number = (from m in db.tPaperLaidV where m.PaperLaidId == item.PaperLaidId select m.PaperLaidId).FirstOrDefault().ToString();
                    break;
            }

            return query;

        }

        static object NoticePaperLaidDetails(object param)
        {
            // tPaperLaidV model = param as tPaperLaidV;
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            mMinisteryMinisterModel model = param as mMinisteryMinisterModel;
            var query = (from m in db.tPaperLaidV
                         join c in db.mEvent on m.EventId equals c.EventId
                         join t in db.tPaperLaidTemp on m.PaperLaidId equals t.PaperLaidId
                         join minis in db.mMinistry on m.MinistryId equals minis.MinistryID
                         join MemberNotices in db.tMemberNotice on m.PaperLaidId equals MemberNotices.PaperLaidId
                         where model.PaperLaidId == m.PaperLaidId
                         && m.DeptActivePaperId == t.PaperLaidTempId
                         select new mMinisteryMinisterModel
                         {
                             DeptActivePaperId = m.DeptActivePaperId,
                             MinisterName = minis.MinisterName + ", " + minis.MinistryName,
                             PaperLaidId = m.PaperLaidId,
                             EventName = c.EventName,
                             Title = m.Title,
                             Version = t.Version,
                             ProvisionUnderWhich = m.ProvisionUnderWhich,
                             DeparmentName = m.DeparmentName,
                             DeptSubmittedDate = t.DeptSubmittedDate,
                             Remark = m.Remark,
                             FilePath = t.FilePath,
                             Description = m.Description,
                             OriDiaryFileName = MemberNotices.OriDiaryFileName,
                             OriDiaryFilePath = MemberNotices.OriDiaryFilePath,
                             SignedFilePath = t.SignedFilePath,
                             FileName = t.FileName,
                             NoticePdfPath = MemberNotices.NoticePdfPath,
                             MemberName = (from mc in db.mMember where mc.MemberCode == MemberNotices.MemberId select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             DesireLayingDate = (from sessionDates in db.mSessionDates where sessionDates.Id == m.DesireLayingDateId select sessionDates.SessionDate).FirstOrDefault(),
                             // MinisterSubmittedDate = t.MinisterSubmittedDate,
                             // MemberName = (from mc in db.mMember where mc.MemberCode == Questions.MemberID select (mc.Prefix + " " + mc.Name)).FirstOrDefault(),
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),
                             PaperCategoryTypeName = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.Name).FirstOrDefault(),
                             PaperCategoryTypeId = (from p in db.mPaperCategoryType join e in db.mEvent on p.PaperCategoryTypeId equals e.PaperCategoryTypeId where e.EventId == m.EventId select p.PaperCategoryTypeId).FirstOrDefault()
                             //MemberName = (from p in db.tQuestions join e in db.tPaperLaidV on p.PaperLaidId equals e.PaperLaidId join k in db.mMember on p.MemberID equals k.MemberCode select k.Name).FirstOrDefault(),

                         }).FirstOrDefault();

            var item = query;

            switch (item.PaperCategoryTypeId)
            {
                case 1:
                    item.Number = (from m in db.tQuestions where m.PaperLaidId == item.PaperLaidId select m.QuestionNumber).FirstOrDefault().ToString();

                    break;
                case 2:
                    item.Number = (from m in db.tBillRegister where m.PaperLaidId == item.PaperLaidId select m.BillNumber).FirstOrDefault().ToString();
                    break;
                case 4:
                    item.Number = (from m in db.tMemberNotice where m.PaperLaidId == item.PaperLaidId select m.NoticeNumber).FirstOrDefault().ToString();
                    break;
                default:
                    item.Number = (from m in db.tPaperLaidV where m.PaperLaidId == item.PaperLaidId select m.PaperLaidId).FirstOrDefault().ToString();
                    break;
            }

            return query;

        }




        //&& tQ.Actice == true

        //mc.Prefix + " " + 
        static object ShowNoticeInboxById(object param)
        {

            MinisterNoticecustom model = param as MinisterNoticecustom;
            try
            {
                PaperLaidMinisterContext db = new PaperLaidMinisterContext();

                MinisterNoticecustom NoticeModel = param as MinisterNoticecustom;
                var query = (from tQ in db.tMemberNotice
                             join a in db.mEvent on tQ.NoticeTypeID equals a.EventId
                             where tQ.NoticeId == model.NoticeID && tQ.Actice == true
                             orderby tQ.NoticeId descending
                             select new MinisterNoticecustom
                             {

                                 MemberName = (from member in db.mMember where member.MemberCode == tQ.MemberId select member.Name).FirstOrDefault(),


                                 MinisterName = (from member in db.mMinistry where member.MinistryID == tQ.MinistryId select member.MinisterName).FirstOrDefault(),
                                 Subject = tQ.Subject,
                                 NoticeNumber = tQ.NoticeNumber,
                                 DepartmentName = tQ.DepartmentName,
                                 BussinessType = a.EventName,
                                 Notice = tQ.Notice,
                                 OriDiaryFileName = tQ.OriDiaryFileName,
                                 OriDiaryFilePath = tQ.OriDiaryFilePath


                             }).FirstOrDefault();

                return query;
            }
            catch (Exception ex)
            {

                return null;
            }

        }

        #endregion




        static object BeforeFixation(object param)
        {

            mDepartmentPasses objPaperLaid = param as mDepartmentPasses;
            string csv = objPaperLaid.DepartmentID;
            IEnumerable<string> ids = csv.Split(',').Select(str => str);
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            var data = (from dept in db.mDepartmentPdfPath where ids.Contains(dept.deptId) && dept.AssemblyID == objPaperLaid.AssemblyCode && dept.SessionID == objPaperLaid.SessionCode select dept).ToList();
            return data;

            //mDepartmentPasses objPaperLaid = param as mDepartmentPasses;
            //string csv = objPaperLaid.DepartmentID;
            //IEnumerable<string> ids = csv.Split(',').Select(str => str);
            //PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            //var data = (from dept in db.mDepartment where ids.Contains(dept.deptId) select dept).ToList();
            //return data;

        }


        #region added code venkat for dynamic menu
        private static object DDlEvent()
        {
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            var Res = (from e in db.mEvent select e);


            return Res.ToList();
        }




        private static object GetSubMenuByMenuIdLocal(object param)
        {


            mUserSubModules model = param as mUserSubModules;
            ModuleContext MCtx = new ModuleContext();
            var Menulist = (from c in MCtx.mUserModules where c.ModuleId == model.ModuleId select new { c.ModuleNameLocal }).ToList();

            List<mUserSubModules> USMList = new List<mUserSubModules>();



            //Get Sub Menu Dynamically
            var SubMenuList = (from M in MCtx.mUserSubModules
                               join
                                   c in MCtx.mUserModules on M.ModuleId equals c.ModuleId

                               where M.ModuleId == model.ModuleId
                               select new { M.SubModuleId, M.SubModuleNameLocal, c.ModuleNameLocal,c.ModuleId }).ToList();
            if (SubMenuList != null)
            {
                foreach (var item in SubMenuList)
                {
                    mUserSubModules USM = new mUserSubModules();
                    USM.SubModuleId = item.SubModuleId;
                    USM.SubModuleNameLocal = item.SubModuleNameLocal;
                    USM.ModuleNameLocal = item.ModuleNameLocal;
                    USM.ModuleId = item.ModuleId;
                    USMList.Add(USM);
                }
            }
            model.AccessList = USMList;
            return model;
        }

        private static object GetSubMenuByMenuId(object param)
        {


            mUserSubModules model = param as mUserSubModules;
            ModuleContext MCtx = new ModuleContext();
            var Menulist = (from c in MCtx.mUserModules where c.ModuleId == model.ModuleId select new { c.ModuleName }).ToList();

            List<mUserSubModules> USMList = new List<mUserSubModules>();



            //Get Sub Menu Dynamically
            var SubMenuList = (from M in MCtx.mUserSubModules
                               join
                                   c in MCtx.mUserModules on M.ModuleId equals c.ModuleId

                               where M.ModuleId == model.ModuleId
                               select new { M.SubModuleId, M.SubModuleName, c.ModuleName,c.ModuleId }).ToList();
            if (SubMenuList != null)
            {
                foreach (var item in SubMenuList)
                {
                    mUserSubModules USM = new mUserSubModules();
                    USM.SubModuleId = item.SubModuleId;
                    USM.SubModuleName = item.SubModuleName;
                    USM.ModuleName = item.ModuleName;
                    USM.ModuleId = item.ModuleId;
                    USMList.Add(USM);
                }
            }
            model.AccessList = USMList;
            return model;
        }

        static object GetOtherPapersCounterforMinisters(object param)
        {
            DiaryModel Dmdl = new DiaryModel();
            tPaperLaidV model = param as tPaperLaidV;
            // int TotalStaredReceived;
            int Count = 0;
            string SubMenu;
            SubMenu = model.Type;
            switch (SubMenu)
            {

                case "Upcoming Lob":
                    {
                        Count = UpcomingLOBByOtherpaperLaidId(model).ResultCount;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Laid In The House":
                    {
                        Count = GetOtherPaperLaidInHouseByType(model).ResultCount;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Pending To Lay":
                    {
                        Count = GetPendingToLayOtherPaperLaidByType(model).ResultCount;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }

            }


            return Count;
        }

        static object GetCountForOtherPaperLaidTypesMinisters(object param)
        {
            //DiaryModel Dmdl = new DiaryModel();
            PaperLaidMinisterContext db = new PaperLaidMinisterContext();
            //  PaperMovementModel model = param as PaperMovementModel;
            tPaperLaidV Dmdl = param as tPaperLaidV;
            int paperCategoryId = 5;
            int count = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                             && PaperLaidTemp.DeptSubmittedBy == null
                             && PaperLaidTemp.DeptSubmittedDate == null
                              && PaperLaidTemp.MinisterSubmittedBy == null
                              && PaperLaidTemp.MinisterSubmittedDate == null
                           && PaperLaidV.DeptActivePaperId == PaperLaidTemp.PaperLaidTempId
                           && ministery.MinistryID == Dmdl.MinistryId
                           && PaperLaidV.AssemblyId == Dmdl.AssemblyId
                           && PaperLaidV.SessionId == Dmdl.SessionId
                         select new MinisterPaperMovementModel
                         {
                         }).Count();

            var query = (from PaperLaidV in db.tPaperLaidV
                         join Event in db.mEvent on PaperLaidV.EventId equals Event.EventId
                         join ministery in db.mMinistry on PaperLaidV.MinistryId equals ministery.MinistryID
                         join PaperLaidTemp in db.tPaperLaidTemp on PaperLaidV.PaperLaidId equals PaperLaidTemp.PaperLaidId
                         where
                          Event.PaperCategoryTypeId == paperCategoryId
                         && PaperLaidTemp.DeptSubmittedBy != null
                         && PaperLaidTemp.DeptSubmittedDate != null
                             
                           && PaperLaidV.MinisterActivePaperId == PaperLaidTemp.PaperLaidTempId
                           && ministery.MinistryID == Dmdl.MinistryId
                           && PaperLaidV.AssemblyId == Dmdl.AssemblyId
                           && PaperLaidV.SessionId == Dmdl.SessionId
                         select new MinisterPaperMovementModel
                         {
                         }).Count();


            db.Close();
            Dmdl.OtherPaperCount = count;
            Dmdl.Count = query;
            if (Dmdl.Type != "Pending by Department")
            {
                Dmdl.ResultCount = query;
            }
            else
            {
                Dmdl.ResultCount = count;
            }
            return Dmdl;

        }

        static object GetBillsCounterforMinisters(object param)
        {
           // DiaryModel Dmdl = new DiaryModel();
            tPaperLaidV Dmdl = param as tPaperLaidV;
            // int TotalStaredReceived;
            int Count = 0;
            string SubMenu;
            SubMenu = Dmdl.Type;
            switch (SubMenu)
            {
                case "Pending By Department":
                    {
                        Count = GetDraftsBill(Dmdl).ResultCount;

                        Dmdl.ResultCount = Count;
                        return Dmdl;
                    }
                case "Sent By Department":
                    {
                        Count = GetsentBill(Dmdl).ResultCount;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Upcoming Lob":
                    {
                        Count = GetBillULOBList(Dmdl).ResultCount;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Laid In The House":
                    {
                        Count = GetBillLIHList(Dmdl).ResultCount;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Pending To Lay":
                    {
                        Count = GetBillPLIHList(Dmdl).ResultCount;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }

            }


            return Count;
        }

        static object GetNoticesCounterforMinisters(object param)
        {
           // DiaryModel Dmdl = new DiaryModel();
            tPaperLaidV Dmdl = param as tPaperLaidV;
            // int TotalStaredReceived;
            int Count = 0;
            string SubMenu;
            SubMenu = Dmdl.Type;

           

            switch (SubMenu)
            {
                case "Pending By Department":
                    {
                        Count = GetNoticeInBoxList(Dmdl).ResultCount;

                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Sent By Department":
                    {
                        Count = GetNoticeReplySentList(Dmdl).ResultCount;

                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Upcoming Lob":
                    {
                        Count = GetNoticeULOBList(Dmdl).ResultCount;

                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Laid In The House":
                    {
                        Count = GetNoticeLIHList(Dmdl).ResultCount;

                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Pending To Lay":
                    {
                        Count = GetNoticePLIHList(Dmdl).ResultCount;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }



            }


            return Count;
        }

        static object GetUnStarredQuestionCounterforMinisters(object param)
        {
           // DiaryModel Dmdl = new DiaryModel();
            tPaperLaidV model = param as tPaperLaidV;
             int TotalStaredReceived;
            int Count = 0;
            string SubMenu;
            SubMenu = model.Type;
            if (model.RequestMessage == "ChiefMinisterAdmin")
            {

            }
            switch (SubMenu)
            {
                case "Pending By Department":
                    {
                        TotalStaredReceived = GetPendingQuestionsByType(model).ResultCount;

                        Count = TotalStaredReceived;
                        model.ResultCount = Count;
                        return model;

                        //Count = GetPendingQuestionsByType(model).MinInboxCount;
                        //model.MinInboxCount = Count;
                        //return model;
                       

                    }
                case "Sent By Department":
                    {
                        Count = GetSubmittedQuestionsByType(model).ResultCount;

                        model.ResultCount = Count;
                        return model;

                    }
                case "Upcoming Lob":
                    {
                        Count = UpcomingLOBByQuestionType(model).ResultCount;

                        model.ResultCount = Count;
                        return model;

                    }
                case "Laid In The House":
                    {
                        Count = GetLaidInHouseQuestionsByType(model).ResultCount;

                        model.ResultCount = Count;
                        return model;

                    }
                case "Pending To Lay":
                    {
                        Count = GetPendingToLayQuestionsByType(model).ResultCount;

                        model.ResultCount = Count;
                        return model;

                    }
                case "List As Pdf":
                    {
                        Count = 0;
                        model.ResultCount = Count;
                        return model;

                    }


            }


            return Count;
        }

        static object GetStarredQuestionCounterforMinisters(object param)
        {

            //tPaperLaidV Dmdl = new tPaperLaidV();

            tPaperLaidV Dmdl = param as tPaperLaidV;
            int TotalStaredReceived;
            int Count = 0;
            string SubMenu;
            SubMenu = Dmdl.Type;
            switch (SubMenu)
            {
                case "Pending By Department":
                    {
                        TotalStaredReceived = GetPendingQuestionsByType(Dmdl).ResultCount;

                        Count = TotalStaredReceived;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Sent By Department":
                    {
                        TotalStaredReceived = GetSubmittedQuestionsByType(Dmdl).ResultCount;
                        Count = TotalStaredReceived;
                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Upcoming Lob":
                    {
                        Count = UpcomingLOBByQuestionType(Dmdl).ResultCount; ;

                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Laid In The House":
                    {
                        Count = GetLaidInHouseQuestionsByType(Dmdl).ResultCount;

                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "Pending To Lay":
                    {
                        Count = GetPendingToLayQuestionsByType(Dmdl).ResultCount;

                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }
                case "List As Pdf":
                    {
                        Count = GetPendingForSubQuestionsByType(Dmdl).ResultCount;

                        Dmdl.ResultCount = Count;
                        return Dmdl;

                    }






            }




            //var TotalStaredSubmitted = GetSubmittedQuestionsByType(model).ResultCount;



            //var TotalStaredPendingForSubmission = GetPendingForSubQuestionsByType(model).ResultCount;
            //var TotalStaredLaidInHouse = GetLaidInHouseQuestionsByType(model).ResultCount;
            //var TotalStaredPendingToLay = GetPendingToLayQuestionsByType(model).ResultCount;
            //var TotalStaredQuestions = model.TotalStaredReceived + model.TotalStaredSubmitted;







            return Dmdl;
        }
        #endregion

    }
}
