﻿using SBL.DAL;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.CutMotionDemand;
using SBL.DomainModel.Models.Session;
using SBL.Service.Common;
using System.Data;
using System.Data.Entity;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.CutMotionDemand
{
    class CutMotionDemandContext : DBBase<CutMotionDemandContext>
    {


        public CutMotionDemandContext() : base("eVidhan") { }

        public DbSet<tCutMotionDemand> tCutMotionDemand { get; set; }
        public DbSet<mAssembly> mAssembly { get; set; }
        public DbSet<mSession> mSession { get; set; }


        public static object Execute(ServiceParameter param)
        {
            if (param != null)
            {
                switch (param.Method)
                {
                    case "GetAllCutMotionDemand": { return GetAllCutMotionDemand(param.Parameter); }

                    case "AddCutMotionDemand": { return AddCutMotionDemand(param.Parameter); }

                    case "UpdateCutMotionDemand": { return UpdateCutMotionDemand(param.Parameter); }

                    case "GetCutMotionDemandById": { return GetCutMotionDemandById(param.Parameter); }

                    case "DeleteCutMotionDemandById": { return DeleteCutMotionDemandById(param.Parameter); }


                }
            }
            return null;
        }



        private static object AddCutMotionDemand(object p)
        {
            try
            {
                using (CutMotionDemandContext db = new CutMotionDemandContext())
                {
                    tCutMotionDemand data = p as tCutMotionDemand;

                    data.ModifiedDate = DateTime.Now;
                    data.CreatedDate = DateTime.Now;

                    db.tCutMotionDemand.Add(data);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }


        }

        private static object UpdateCutMotionDemand(object p)
        {

            try
            {
                using (CutMotionDemandContext db = new CutMotionDemandContext())
                {
                    tCutMotionDemand updateresult = p as tCutMotionDemand;
                    var actualResult = (from rotmin in db.tCutMotionDemand
                                        where
                                            rotmin.CMDemandId == updateresult.CMDemandId
                                        select rotmin).FirstOrDefault();

                    actualResult.AssemblyID = updateresult.AssemblyID;
                    actualResult.DemandName = updateresult.DemandName;
                    actualResult.SessionID = updateresult.SessionID;
                    actualResult.DemandName_Local = updateresult.DemandName_Local;
                    actualResult.DemandNo = updateresult.DemandNo;
                    //actualResult.DemandAmountType = updateresult.DemandAmountType;
                    //actualResult.DemandAmountType_Local = updateresult.DemandAmountType_Local;
                    actualResult.RevenueAmount = updateresult.RevenueAmount;
                    actualResult.CapitalAmount = updateresult.CapitalAmount;
                    actualResult.IsActive = updateresult.IsActive;
                    actualResult.IsDeleted = updateresult.IsDeleted;
                    actualResult.ModifiedDate = DateTime.Now;
                    actualResult.ModifiedBy = updateresult.ModifiedBy;


                    db.tCutMotionDemand.Attach(actualResult);
                    db.Entry(actualResult).State = EntityState.Modified;
                    db.SaveChanges();


                }

            }
            catch (Exception ex)
            {

                throw ex;
            }

            return true;

        }

        private static object DeleteCutMotionDemandById(object p)
        {
            try
            {
                using (CutMotionDemandContext db = new CutMotionDemandContext())
                {
                    tCutMotionDemand id = p as tCutMotionDemand;

                    //var result = (from rotmin in db.tCutMotionDemand
                    //              where
                    //                  rotmin.CMDemandId == id.CMDemandId
                    //              select rotmin).FirstOrDefault();

                    //db.tCutMotionDemand.Remove(result);
                    tCutMotionDemand cutmotionToRemove = db.tCutMotionDemand.SingleOrDefault(a => a.CMDemandId == id.CMDemandId);


                    if (cutmotionToRemove != null)
                    {
                        cutmotionToRemove.IsDeleted = true;
                    }




                    db.SaveChanges();

                    db.Close();

                    return true;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



        //static object DeleteParty(object param)
        //{
        //    if (null == param)
        //    {
        //        return null;
        //    }
        //    using (PartyContext db = new PartyContext())
        //    {
        //        mParty parameter = param as mParty;
        //        mParty partyToRemove = db.mParty.SingleOrDefault(a => a.PartyID == parameter.PartyID);
        //        if (partyToRemove != null)
        //        {
        //            partyToRemove.IsDeleted = true;
        //        }
        //        //db.mParty.Remove(partyToRemove);
        //        db.SaveChanges();
        //        db.Close();
        //    }
        //    return GetAllParty();
        //}





        private static object GetAllCutMotionDemand(object p)
        {
            try
            {
                using (CutMotionDemandContext db = new CutMotionDemandContext())
                {
                    var relaresult = (from asblyFiles in db.tCutMotionDemand


                                      join assembly in db.mAssembly on asblyFiles.AssemblyID equals assembly.AssemblyCode
                                      join session in db.mSession on
                                         new { SessionCode = asblyFiles.SessionID, assemblyId = asblyFiles.AssemblyID } equals new { SessionCode = session.SessionCode, assemblyId = session.AssemblyID }

                                      orderby asblyFiles.CMDemandId descending
                                      where asblyFiles.IsDeleted == false
                                      select
                                          new
                                          {
                                              assemblyFile = asblyFiles,
                                              assemblyName = assembly.AssemblyName,
                                              sessionName = session.SessionName,

                                          }).ToList();


                    List<tCutMotionDemand> assemblyFileList = new List<tCutMotionDemand>();




                    foreach (var item in relaresult)
                    {
                        tCutMotionDemand assemblyFile = new tCutMotionDemand();
                        assemblyFile = item.assemblyFile;

                        assemblyFile.AssemblyName = item.assemblyName;
                        assemblyFile.SessionName = item.sessionName;


                        assemblyFileList.Add(assemblyFile);
                    }

                    return assemblyFileList;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static object GetCutMotionDemandById(object p)
        {
            try
            {
                using (CutMotionDemandContext db = new CutMotionDemandContext())
                {
                    tCutMotionDemand id = p as tCutMotionDemand;

                    var result = (from rotmin in db.tCutMotionDemand
                                  where
                                      rotmin.CMDemandId == id.CMDemandId
                                  select rotmin).FirstOrDefault();

                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }



    }
}
