﻿

namespace SBL.Domain.Context.EmployeeDSCAuthorization
{
    #region Namespace Reffrences

    using SBL.DAL;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Employee;
    using SBL.DomainModel.Models.Secretory;
    using SBL.DomainModel.Models.User;
    using SBL.Service.Common;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    //using System.Web.Mvc;

    #endregion


    public class SecrtEmpDSCAuthorizationContext : DBBase<SecrtEmpDSCAuthorizationContext>
    {
        public SecrtEmpDSCAuthorizationContext() : base("eVidhan") { }

        public virtual DbSet<mDepartment> mDepartment { get; set; }

        public virtual DbSet<mSecretory> mSecretory { get; set; }

        public virtual DbSet<mSecretoryDepartment> mSecretoryDepartment { get; set; }

        public virtual DbSet<mUsers> mUsers { get; set; }

        public virtual DbSet<mEmployee> mEmployees { get; set; }

        public virtual DbSet<mUserDSHDetails> mUserDscDetails { get; set; }

        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "GetSecretaryDepartmentList":
                    {
                        return GetSecretaryDepartmentList(param.Parameter);
                    }

                case "GetDepartmentEmployeeList":
                    {
                        return GetDepartmentEmployeeList(param.Parameter);
                    }

                case "AuthoriseSecDeptEmployes":
                    {
                        return AuthoriseSecDeptEmployes(param.Parameter);
                    }

                case "UnAuthoriseSecDeptEmployes":
                    {
                        return UnAuthoriseSecDeptEmployes(param.Parameter);
                    }

                case "CheckDSCHasExist":
                    {

                        return CheckDSCHasExist(param.Parameter);
                    }

            }
            return null;
        }

        private static object CheckDSCHasExist(object p)
        {
            var dschash = (string)p;
            using (var ctx = new SecrtEmpDSCAuthorizationContext())
            {
                var isexist = (from usrdschash in ctx.mUserDscDetails where usrdschash.HashKey == dschash select usrdschash).Count()>0;

                return isexist;
            }
        }

        private static object UnAuthoriseSecDeptEmployes(object p)
        {
            string[] userIds = (string[])p;

            using (var ctx = new SecrtEmpDSCAuthorizationContext())
            {
                foreach (var item in userIds)
                {
                    Guid userid = Guid.Parse(item);
                    var deptEmpRecord = (from a in ctx.mUsers
                                    where a.UserId == userid
                                      select a).FirstOrDefault();
                    ctx.mUsers.Attach(deptEmpRecord);
              var validationdata = ctx.GetValidationErrors();
                    deptEmpRecord.IsDSCAuthorized = false;
                    ctx.Configuration.ValidateOnSaveEnabled = false;

                    ctx.SaveChanges();
                }

            }

            return true;
        }

        private static object AuthoriseSecDeptEmployes(object p)
        {
            string[] userIds = (string[])p;
            foreach (var item in userIds)
            {
                using (var ctx = new SecrtEmpDSCAuthorizationContext())
                {

                    Guid userid = Guid.Parse(item);
                    var deptEmpRecord = (from a in ctx.mUsers
                                       where a.UserId == userid
                                       select a).FirstOrDefault();


                    ctx.mUsers.Attach(deptEmpRecord);
                    var validationdata = ctx.GetValidationErrors();
                    deptEmpRecord.IsDSCAuthorized = true;
                    ctx.Configuration.ValidateOnSaveEnabled = false;
                    ctx.SaveChanges();
                }

            }

            return true;
        }

        private static object GetDepartmentEmployeeList(object p)
        {
            var departmentId = (string)p;

            using (var ctx = new SecrtEmpDSCAuthorizationContext())
            {
                var deptEmpList = (from users in ctx.mUsers
                                   join emp in ctx.mEmployees on new { c = users.UserName, d = users.DeptId } equals new { c = emp.empcd, d = emp.deptid }
                                   where users.DeptId == departmentId //&& a.DSCHash != null
                                   select new
                                   {
                                       UserName = emp.empfname,
                                       UserId = users.UserId,
                                       DeptId = users.DeptId,
                                       EmailId = users.EmailId,
                                       EmpCode = users.UserName,
                                       IsDCSAUthorized = users.IsDSCAuthorized,
                                       Designation = users.Designation
                                   }).ToList();


                List<mUsers> userlist = new List<mUsers>();


                if (deptEmpList != null)
                {
                    foreach (var item in deptEmpList)
                    {
                        mUsers user = new mUsers();
                        user.UserId = item.UserId;
                        user.DeptId = item.DeptId;
                        user.EmailId = item.EmailId;
                        user.UserName = item.UserName;
                        user.EmpId = item.EmpCode;
                        user.IsDSCAuthorized = item.IsDCSAUthorized;
                        user.Designation = item.Designation;
                        userlist.Add(user);
                    }
                }

                return userlist;
            }

        }

        private static object GetSecretaryDepartmentList(object p)
        {
            string guid = (string)p;
            Guid userId = Guid.Parse(guid);
            //SelectListItem firstelement = new SelectListItem { Value = "0000", Text = "Select Department", Selected = true };
            using (var ctx = new SecrtEmpDSCAuthorizationContext())
            {
                var secrId = (from a in ctx.mUsers where a.UserId == userId && a.SecretoryId != null select a.SecretoryId).FirstOrDefault();

                var deptList = (from
                                 secdept in ctx.mSecretoryDepartment
                                join dept in ctx.mDepartment on secdept.DepartmentID equals dept.deptId
                                where secdept.SecretoryID == secrId
                                select dept).ToList();
                //deptList.Add(firstelement);
                return deptList;
            }


        }

    }
}
