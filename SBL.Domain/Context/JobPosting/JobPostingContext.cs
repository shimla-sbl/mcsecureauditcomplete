﻿using SBL.DAL;
using SBL.DomainModel.Models.JobsPosting;
using SBL.Service.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.Domain.Context.JobPosting
{
    public class JobPostingContext : DBBase<JobPostingContext>
    {
        public JobPostingContext() : base("eVidhan") { this.Configuration.ProxyCreationEnabled = false; }

        public DbSet<Job> Job { get; set; }
        public DbSet<EvidhanPost> EvidhanPost { get; set; }
        public DbSet<JobWithPostDetail> JobWithPostDetail { get; set; }
        public DbSet<Candidates> Candidates { get; set; }
        public DbSet<ExperiencesDetails> ExperiencesDetails { get; set; }
        public DbSet<ExamDetails> ExamDetails { get; set; }
        public DbSet<tCandidateAdmitDetails> tCandidateAdmitDetail { get; set; }
    
        public static object Execute(ServiceParameter param)
        {
            if (null == param)
            {
                return null;
            }

            switch (param.Method)
            {
                case "AddJob": { return AddJob(param.Parameter); }
                case "UpdateJob": { return UpdateJob(param.Parameter); }
                case "GetAllJob": { return GetAllJob(); }
                case "GetJobById": { return GetJobById(param.Parameter); }

                case "AddJobWithPost": { AddJobWithPost(param.Parameter); break; }
                case "UpdateJobWithPost": { return UpdateJobWithPost(param.Parameter); }
                case "GetAllJobWithPost": { return GetAllJobWithPost(); }
                case "GetJobWithPostById": { return GetJobWithPostById(param.Parameter); }


                case "UpdateCandidates": { return UpdateCandidates(param.Parameter); }
                case "GetAllCandidates": { return GetAllCandidates(); }
                case "GetAllApprovedCandidates": { return GetAllApprovedCandidates(); }
                case "GetCandidatesById": { return GetCandidatesById(param.Parameter); }
                //region by robin
                case "AddPost": { AddPost(param.Parameter); break; }
                case "UpdatePost": { return UpdatePost(param.Parameter); }
                case "GetAllActivePost": { return GetAllActivePost(); }
                case "GetAllPost": { return GetAllPost(); }
                case "GetPostById": { return GetPostById(param.Parameter); }
                case "GetAllExperiencesDetails": { return GetAllExperiencesDetails(param.Parameter); }
                case "GetAllExamDetails": { return GetAllExamDetails(param.Parameter); }
                //admitcarddetailsbyrobin  
                case "AddAdmitCardDetails": { return AddAdmitCardDetails(param.Parameter); }
                case "GetAdmitCardDetailsById": { return GetAdmitCardDetailsById(param.Parameter); }
                case "LockUnlockCandidateDetails": { return LockUnlockCandidateDetails(param.Parameter); }
                case "GetCandidatesPostById": { return GetCandidatesPostById(param.Parameter); }
                case "GetAllCandidatesForPost": { return GetAllCandidatesForPost(param.Parameter); }
                case "GetAllActivePostForCandidate": { return GetAllActivePostForCandidate(); }


            }
            return null;
        }


        static int AddJob(object param)
        {
            try
            {
                using (JobPostingContext db = new JobPostingContext())
                {
                    Job model = param as Job;

                    db.Job.Add(model);
                    db.SaveChanges();
                    db.Close();
                    return model.JOBID;
                }

            }
            catch
            {
                throw;
            }
        }

        static object AddAdmitCardDetails(object param)
        {
            try
            {
                using (JobPostingContext db = new JobPostingContext())
                {
                    tCandidateAdmitDetails model = param as tCandidateAdmitDetails;

                    var query = (from a in db.tCandidateAdmitDetail
                                 where a.PID == model.PID
                                 select a).AsNoTracking().FirstOrDefault();

                    var query2 = (from a in db.Candidates
                                  where a.CandidatesID == model.CandidateID
                                  select a).FirstOrDefault();

                    if (query == null)
                    {
                        db.tCandidateAdmitDetail.Add(model);
                        db.SaveChanges();

                        //for other table also..
                        query2.IsAdmitCardDetailsEntered = true;
                        db.Candidates.Attach(query2);
                        db.Entry(query2).State = EntityState.Modified;
                        db.SaveChanges();

                        db.Close();
                    }
                    else
                    {
                        db.tCandidateAdmitDetail.Attach(model);
                        db.Entry(model).State = EntityState.Modified;
                        db.SaveChanges();
                        db.Close();
                    }
                    return null;
                }

            }
            catch
            {
                throw;
            }
        }

        static object LockUnlockCandidateDetails(object param)
        {            
            using (JobPostingContext db = new JobPostingContext())
            {
                Candidates model = param as Candidates;
                var data = (from a in db.Candidates
                        where a.CandidatesID == model.CandidatesID
                             select a ).FirstOrDefault();
                if (data != null)
                {
                    data.IsAdmitCardDetailsLocked = model.IsAdmitCardDetailsLocked;
                    db.Candidates.Attach(data);
                    db.Entry(data).State = EntityState.Modified;
                    db.SaveChanges();
                    db.Close();
                }
            }
            return null;
        }


        static object UpdateJob(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (JobPostingContext db = new JobPostingContext())
            {
                Job model = param as Job;

                db.Job.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllJob();
        }



        static List<Job> GetAllJob()
        {
            JobPostingContext db = new JobPostingContext();


            var query = (from a in db.Job
                         orderby a.JOBID descending

                         select a).ToList();

            return query;
        }

        static Job GetJobById(object param)
        {
            Job parameter = param as Job;
            JobPostingContext db = new JobPostingContext();
            var query = db.Job.SingleOrDefault(a => a.JOBID == parameter.JOBID);
            return query;
        }

        static tCandidateAdmitDetails GetAdmitCardDetailsById(object param)
        {
            tCandidateAdmitDetails parameter = param as tCandidateAdmitDetails;
            JobPostingContext db = new JobPostingContext();
            var query = db.tCandidateAdmitDetail.FirstOrDefault(a => a.CandidateID == parameter.CandidateID);
            return query;
        }


        static void AddJobWithPost(object param)
        {
            try
            {
                using (JobPostingContext db = new JobPostingContext())
                {
                    JobWithPostDetail model = param as JobWithPostDetail;

                    db.JobWithPostDetail.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdateJobWithPost(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (JobPostingContext db = new JobPostingContext())
            {
                JobWithPostDetail model = param as JobWithPostDetail;

                db.JobWithPostDetail.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllJob();
        }

        static List<JobWithPostDetail> GetAllJobWithPost()
        {
            JobPostingContext db = new JobPostingContext();


            var query = (from a in db.JobWithPostDetail
                         orderby a.ID descending

                         select a).ToList();

            return query;
        }

        static JobWithPostDetail GetJobWithPostById(object param)
        {
            JobWithPostDetail parameter = param as JobWithPostDetail;
            JobPostingContext db = new JobPostingContext();
            var query = db.JobWithPostDetail.SingleOrDefault(a => a.ID == parameter.ID);
            return query;
        }
     
        static object UpdateCandidates(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (JobPostingContext db = new JobPostingContext())
            {
                Candidates model = param as Candidates;

                db.Candidates.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllJob();
        }

        static List<Candidates> GetAllCandidates()
        {
            JobPostingContext db = new JobPostingContext();


            var query = (from a in db.Candidates
                         join job in db.Job on a.JobId equals job.JOBID
                         join d in db.JobWithPostDetail on a.postId equals d.ID
                         join postn in db.EvidhanPost on d.PostId equals postn.PostId
                         where job.IsActive == true
                         orderby a.CandidatesID descending
                         select new { a, job.JobTitle, postn.PostName }).ToList();
            List<Candidates> Result = new List<Candidates>();

            foreach (var item in query)
            {
                Candidates obj = new Candidates();
                obj = item.a;
                obj.JobName = item.JobTitle;
                obj.PostName = item.PostName;
                Result.Add(obj);

            }

            return Result;
        }



        static List<Candidates> GetAllApprovedCandidates()
        {
            JobPostingContext db = new JobPostingContext();


            var query = (from a in db.Candidates

                         join job in db.Job on a.JobId equals job.JOBID
                         join d in db.JobWithPostDetail on a.postId equals d.ID
                         join postn in db.EvidhanPost on d.PostId equals postn.PostId
                         where a.IsApprove == true && job.IsActive == true
                         orderby a.CandidatesID descending

                         select new { a, job.JobTitle }).ToList();
            List<Candidates> Result = new List<Candidates>();

            foreach (var item in query)
            {
                Candidates obj = new Candidates();
                obj = item.a;
                obj.JobName = item.JobTitle;
                Result.Add(obj);

            }

            return Result;
        }


        public static object GetCandidatesById(object param)
        {
            Candidates parameter = param as Candidates;
            JobPostingContext db = new JobPostingContext();

            var data = (from Candidat in db.Candidates
                        where Candidat.CandidatesID == parameter.CandidatesID
                        select Candidat).FirstOrDefault();
            return data;
        }


        static List<ExperiencesDetails> GetAllExperiencesDetails(object param)
        {
            JobPostingContext db = new JobPostingContext();
            ExperiencesDetails parameter = param as ExperiencesDetails;

            var query = (from a in db.ExperiencesDetails
                         where a.PostId == parameter.PostId
                         orderby a.ID 
                         select a).ToList();

            return query;
        }

        static List<ExamDetails> GetAllExamDetails(object param)
        {
            JobPostingContext db = new JobPostingContext();
            ExamDetails parameter = param as ExamDetails;
            var query = (from a in db.ExamDetails
                         where a.PostId==parameter.PostId
                         orderby a.ID descending

                         select a).ToList();

            return query;
        }
        public static string GetCandidatesPostById(object param)
        {
            Candidates parameter = param as Candidates;
            JobPostingContext db = new JobPostingContext();

            var X = (from postn in db.EvidhanPost
                     join job in db.JobWithPostDetail
                     on postn.PostId equals job.PostId
                     where job.ID == parameter.postId
                     select postn.PostName).FirstOrDefault();
            return X;
        }
        #region by robin

        static void AddPost(object param)
        {
            try
            {
                using (JobPostingContext db = new JobPostingContext())
                {
                    EvidhanPost model = param as EvidhanPost;

                    db.EvidhanPost.Add(model);
                    db.SaveChanges();
                    db.Close();
                }

            }
            catch
            {
                throw;
            }
        }

        static object UpdatePost(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (JobPostingContext db = new JobPostingContext())
            {
                EvidhanPost model = param as EvidhanPost;

                db.EvidhanPost.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return null;
        }

        static List<EvidhanPost> GetAllActivePost()
        {
            JobPostingContext db = new JobPostingContext();


            var query = (from a in db.EvidhanPost
                         where a.PostStatus == true
                         //orderby a.PostId descending

                         select a).ToList();

            return query;
        }


        static List<EvidhanPost> GetAllPost()
        {
            JobPostingContext db = new JobPostingContext();


            var query = (from a in db.EvidhanPost
                         //orderby a.PostId descending

                         select a).ToList();

            return query;
        }


        static EvidhanPost GetPostById(object param)
        {
            //EvidhanPost parameter = param as EvidhanPost;
            int val = Convert.ToInt16(param);
            JobPostingContext db = new JobPostingContext();
            var query = db.EvidhanPost.SingleOrDefault(a => a.PostId == val);
            return query;
        }
        #endregion


        static List<Candidates> GetAllCandidatesForPost(object param)
        {
            Candidates model = param as Candidates;
            JobPostingContext db = new JobPostingContext();
            var query = (from a in db.Candidates
                         join job in db.Job on a.JobId equals job.JOBID
                         join d in db.JobWithPostDetail on a.postId equals d.ID
                         join postn in db.EvidhanPost on d.PostId equals postn.PostId
                         where job.IsActive == true && postn.PostId == model.postId
                         orderby a.CandidatesID descending
                         select new { a, job.JobTitle, postn.PostName }).ToList();
            List<Candidates> Result = new List<Candidates>();

            foreach (var item in query)
            {
                Candidates obj = new Candidates();
                obj = item.a;
                obj.JobName = item.JobTitle;
                obj.PostName = item.PostName;
                Result.Add(obj);

            }

            return Result;
        }


        static List<Candidates> GetAllActivePostForCandidate()
        {
            JobPostingContext db = new JobPostingContext();
            var query = (

                         from d in db.JobWithPostDetail
                         join postn in db.EvidhanPost on d.PostId equals postn.PostId
                         where postn.PostStatus == true
                         orderby postn.PostId descending
                         select new { postn.PostId, postn.PostName }).ToList();
            List<Candidates> Result = new List<Candidates>();

            foreach (var item in query)
            {
                Candidates obj = new Candidates();
                obj.postId = item.PostId;
                obj.PostName = item.PostName;
                Result.Add(obj);

            }

            return Result;
        }

    }
}
