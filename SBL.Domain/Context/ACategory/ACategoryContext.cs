﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using SBL.DAL;
using SBL.DomainModel.Models.Category;
using SBL.Service.Common;
using System.Data;

namespace SBL.Domain.Context.ACategory
{
    public class ACategoryContext : DBBase<ACategoryContext>
    {
        public ACategoryContext()
            : base("eVidhan")
        {
        }
        public DbSet<Category> Category { get; set; }
        //public DbSet<mpAClass> mpAClass { get; set; }
        public static object Execute(ServiceParameter param)
        {
           
            if (param != null)
            {
                switch (param.Method)
                {
                    case "AddCategory": { return AddCategory(param.Parameter); }
                    case "GetCategory": { return GetCategory(param.Parameter); }
                    case "EditCategory": { return EditCategory(param.Parameter); }
                    case "UpdateCategory": { return UpdateCategory(param.Parameter); }
                    case "DeleteCategory": { return DeleteCategory(param.Parameter); }
                    
                }
            }
            return null;
        }


        /// <summary>
        /// Add notice
        /// </summary>
        /// <param name="param">param</param>
        /// <returns></returns>

        static object AddCategory(object param)
        {
            try
            {
                using (ACategoryContext categoryContext = new ACategoryContext())
                {
                    Category category = param as Category;
                    //category.StartPublish = DateTime.Now;
                    //category.StopPublish = DateTime.Now;
                    category.CreatedDate = DateTime.Now;
                    category.ModifiedDate = DateTime.Now;
                    categoryContext.Category.Add(category);
                    categoryContext.SaveChanges();
                    categoryContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        /// <summary>
        /// Get Notice
        /// </summary>
        /// <param name="param">param</param>
        /// <returns></returns>
        static object GetCategory(object param)
        {
            try
            {
                using (ACategoryContext categoryContext = new ACategoryContext())
                {
                    List<Category> category = (from value in categoryContext.Category select value).ToList();
                    return category;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        /// <summary>
        /// Edit Notice
        /// </summary>
        /// <param name="param">param</param>
        /// <returns></returns>
        static object EditCategory(object param)
        {
            try
            {
                using (ACategoryContext categoryContext = new ACategoryContext())
                {
                    int categoryID = Convert.ToInt32(param);
                    Category category = (from value in categoryContext.Category where value.CategoryID == categoryID select value).FirstOrDefault();
                    return category;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        /// <summary>
        /// Update Notice
        /// </summary>
        /// <param name="param">param</param>
        /// <returns></returns>
        static object UpdateCategory(object param)
        {
            try
            {
                using (ACategoryContext categoryContext = new ACategoryContext())
                {
                    Category category = param as Category;
                    //category.StartPublish = DateTime.Now;
                    //category.StopPublish = DateTime.Now;
                    category.CreatedDate = DateTime.Now;
                    category.ModifiedDate = DateTime.Now;
                    categoryContext.Category.Add(category);
                    categoryContext.Entry(category).State = EntityState.Modified;
                    categoryContext.SaveChanges();
                    categoryContext.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        /// <summary>
        /// Delete Notice
        /// </summary>
        /// <param name="param">param</param>
        /// <returns></returns>
        static object DeleteCategory(object param)
        {
            
            try
            {
                using (ACategoryContext categoryContext = new ACategoryContext())
                {
                    Category category = new Category();
                    category.CategoryID = Convert.ToInt32(param);
                    categoryContext.Entry(category).State = EntityState.Deleted;
                    categoryContext.SaveChanges();
                    //List<mNews> data = (from value in noticeContext.notice select value).ToList();
                    categoryContext.Close();
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return null;
        }
        


    }
}
