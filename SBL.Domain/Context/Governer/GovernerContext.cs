﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using SBL.DAL;
using System.Data.Entity;

using SBL.DomainModel.Models.Governor;
using SBL.Service.Common;
using SBL.DomainModel.Models.SiteSetting;
using SBL.DomainModel.ComplexModel;
using System.Data;

using SBL.DomainModel.Models.States;
using System.Data.SqlClient;


namespace SBL.Domain.Context.Governer
{
    public class GovernerContext : DBBase<GovernerContext>
    
    {

        public GovernerContext()
            : base("eVidhan")
        {
            this.Configuration.ProxyCreationEnabled = false;
        }

        
        public virtual DbSet<SiteSettings> SiteSettings { get; set; }
        
        public virtual DbSet<mStates> mStates { get; set; }
        public DbSet<mGoverner> mGoverner { get; set; }
        
        public static object Execute(ServiceParameter param)
        {
            switch (param.Method)
            {
                
                
                case "GetAllGovernerList": { return GetAllGovernerList(); }
                
                case "CreateGoverner": { return CreateGoverner(param.Parameter); }
                case "UpdateGoverner": { return UpdateGoverner(param.Parameter); }
                case "DeleteGoverner": { return DeleteGoverner(param.Parameter); }
                
                case "GetGovernerById1": { return GetGovernerById1(param.Parameter); }
                
                

            }

            return null;
        }

        

      

        static mGoverner GetGovernerById1(object param)
        {
            mGoverner parameter = param as mGoverner;
            GovernerContext db = new GovernerContext();
            //var query = db.mMembers.SingleOrDefault(a => a.MemberCode == parameter.MemberCode);
            var query = db.mGoverner.SingleOrDefault(a => a.mGovernerID == parameter.mGovernerID);
            return query;
        }

        static object DeleteGoverner(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (GovernerContext db = new GovernerContext())
            {
                mGoverner parameter = param as mGoverner;
                mGoverner memberToRemove = db.mGoverner.SingleOrDefault(a => a.mGovernerID == parameter.mGovernerID);
                if (memberToRemove != null)
                {
                    memberToRemove.IsDeleted = true;
                }
                
                db.SaveChanges();
                db.Close();
            }
            return GetAllGovernerList();
        }
        static object UpdateGoverner(object param)
        {
            if (null == param)
            {
                return null;
            }
            using (GovernerContext db = new GovernerContext())
            {
                mGoverner model = param as mGoverner;
                model.CreatedDate = DateTime.Now;
                model.ModifiedWhen = DateTime.Now;

                db.mGoverner.Attach(model);
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                db.Close();
            }
            return GetAllGovernerList();
        }
        //static void CreateMember(object param)
        static object CreateGoverner(object param)
        {
            try
            {
                using (GovernerContext db = new GovernerContext())
                {
                    mGoverner model = param as mGoverner;
                    model.CreatedDate = DateTime.Now;
                    model.ModifiedWhen = DateTime.Now;
                    db.mGoverner.Add(model);
                    db.SaveChanges();
                    db.Close();
                    return model.mGovernerID;
                }

            }
            catch
            {
                throw;
            }
        }

        

        static List<mGoverner> GetAllGovernerList()
        {

            GovernerContext db = new GovernerContext();
            var data = (from Memb in db.mGoverner
                        join state in db.mStates on Memb.mStateID equals state.mStateID into joinStateName
                        from Stat in joinStateName.DefaultIfEmpty()
                        
                        where Memb.IsDeleted == null
                        select new
                        {
                            Memb,
                            Stat.StateName,
                            

                        }).ToList();
            
            var siteSettingData = (from a in db.SiteSettings where a.SettingName == "FileAccessingUrlPath" select a).FirstOrDefault();
            List<mGoverner> list = new List<mGoverner>();
            foreach (var item in data)
            {
                item.Memb.GetStateName = item.StateName;
                

                item.Memb.ImageAcessurl = siteSettingData.SettingValue + item.Memb.FilePath + "/Thumb/" + item.Memb.FileName;


                list.Add(item.Memb);

            }
            return list.OrderByDescending(c => c.mGovernerID).ToList();
            
        }

             


    }
}
