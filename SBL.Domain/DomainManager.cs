﻿namespace SBL.Domain
{
    #region Namespace Reffrences

    using SBL.Service.Common;
    using System;
    using System.Xml;

    #endregion Namespace Reffrences

    public class DomainManager
    {
        public static bool InitializeDB(XmlNode DBConfig)
        {
            if (DBManager.AddDBConfigurations(DBConfig))
            {
                try
                {
                    //DBManager h = new DBManager();
                    DBManager.InitializeDB();
                }
                catch (Exception ex)
                {
                    string Ex = ex.Message;
                }
                return true;
            }
            return false;
        }

        public static object Execute(ServiceParameter param)
        {
            if (null != param)
            {
                switch (param.Module)
                {
                    case "Assembly":
                        {
                            return Context.Assembly.AssemblyContext.Execute(param);
                        }
                    case "AssemblyRemarks":
                        {
                            return Context.AssemblyRemarks.AssemblyRemarksContext.Execute(param);
                        }
                    case "MemberAssemblyRemarks":
                        {
                            return Context.MemberAssemblyRemarks.MemberAssemblyRemarksContext.Execute(param);
                        }
                    case "Session":
                        {
                            return Context.Session.SessionContext.Execute(param);
                        }

                    case "SessionType":
                        {
                            return Context.SessionType.SessionTypeContext.Execute(param);
                        }
                    case "PaperLaid":
                        {
                            return Context.paperLaid.PaperLaidContext.Execute(param);
                        }

                    case "PaperLaidMinister":
                        {
                            return Context.PaperLaidMinister.PaperLaidMinisterContext.Execute(param);
                        }
                    case "Department":
                        {
                            return Context.Department.DepartmentContext.Execute(param);
                        }
                    case "Events":
                        {
                            return Context.Event.EventContext.Execute(param);
                        }
                    case "OtherPaperLaid":
                        {
                            return Context.OtherPaperLaid.OtherPaperLaidContext.Execute(param);
                        }
                    case "MinistryMinister":
                        {
                            return Context.Ministry.MinisteryContext.Execute(param);
                        }

                    case "Ministry":
                        {
                            return Context.Ministry.MinisteryContext.Execute(param);
                        }
                    case "Notice":
                        {
                            return Context.Notice.NoticeContext.Execute(param);
                        }
                    case "Member":
                        {
                            return Context.Member.MemberContext.Execute(param);
                        }
                    case "Minister":
                        {
                            return Context.Minister.MinisterContext.Execute(param);
                        }
                    case "Committee":
                        {
                            return Context.Committee.CommitteeContext.Execute(param);
                        }
                    case "User":
                        {
                            return Context.User.UserContext.Execute(param);
                        }
                    case "SiteSetting":
                        {
                            return Context.SiteSetting.SiteSettingContext.Execute(param);
                        }
                    case "Questions":
                        {
                            return Context.Questions.QuestionsContext.Execute(param);
                        }
                    case "Legislative":
                        {
                            return Context.Legislative.LegislativeContext.Execute(param);
                        }
                    case "Language": { return Context.Languages.LanguageContext.Excute(param); }

                    case "BillPaperLaid":
                        {
                            return Context.BillPaperLaid.BillPaperLaidContext.Execute(param);
                        }
                    case "Diary":
                        {
                            return Context.Diaries.DiariesContext.Execute(param);
                        }
                    case "Employee":
                        {
                            return Context.Employee.EmployeeContext.Execute(param);
                        }
                    case "Secretory":
                        {
                            return Context.Secretory.SecretoryContext.Execute(param);
                        }
                    case "SecretoryDepartment":
                        {
                            return Context.SecretoryDepartment.SecretoryDepartmentContext.Execute(param);
                        }
                    case "SecrtEmpDSCAuthorization":
                        {
                            return Context.EmployeeDSCAuthorization.SecrtEmpDSCAuthorizationContext.Execute(param);
                        }
                    case "OTPDetails":
                        {
                            return Context.OTP.OTPContext.Execute(param);
                        }
                    case "Media":
                        {
                            return Context.Media.MediaContext.Execute(param);
                        }
                    case "Pass":
                        {
                            return Context.Passes.PassContext.Execute(param);
                        }
                    case "Passes":
                        {
                            return Context.Passes.DepartmentPassesContext.Execute(param);
                        }
                    case "LOB":
                        {
                            return Context.LOB.LOBContext.Execute(param);
                        }
                    case "LegislationFixation":
                        {
                            return Context.LegislationFixation.LegislationFixationContext.Execute(param);
                        }
                    case "PublicPass":
                        {
                            return Context.PublicPasses.PublicPassContext.Execute(param);
                        }
                    case "MovementDetails":
                        {
                            return Context.PaperMovement.PaperMovementDetailsContaxt.Execute(param);
                        }
                    case "AdministrationBranch":
                        {
                            return Context.AdministrationBranch.AdministrationBranchContext.Execute(param);
                        }

                    case "ANotice":
                        {
                            return Context.ANotice.ANoticeContext.Execute(param);
                        }
                    case "ANews":
                        {
                            return Context.ANews.ANewsContext.Execute(param);
                        }
                    case "Gallery":
                        {
                            return Context.AGallery.AGalleryContext.Execute(param);
                        }
                    case "ACategory":
                        {
                            return Context.ACategory.ACategoryContext.Execute(param);
                        }

                    case "ASpeech":
                        {
                            return Context.ASpeech.ASpeechContext.Execute(param);
                        }

                    case "AContent":
                        {
                            return Context.AContent.AContentContext.Execute(param);
                        }

                    case "ATour":
                        {
                            return Context.ATour.ATourContext.Execute(param);
                        }
                    case "AssemblyFileSystem":
                        {
                            return Context.AssemblyFileSystem.AssemblyFileSystemContext.Execute(param);
                        }
                    case "Constituency":
                        {
                            return Context.Constituency.ConstituencyContext.Execute(param);
                        }
                    case "ConstituencyPanchayat":
                        {
                            return Context.ConstituencyPanchayat.tConstituencyPanchayatContext.Execute(param);
                        }
                    case "Panchayat":
                        {
                            return Context.Panchayat.mPanchayatContext.Excute(param);
                        }
                    case "AuditTrail":
                        {
                            return Context.AuditTrail.AuditTrailContext.Execute(param);
                        }

                    case "Designation":
                        {
                            return Context.Designations.DesignationsContext.Execute(param);
                        }

                    case "Party":
                        {
                            return Context.Party.PartyContext.Execute(param);
                        }
                    case "Bill":
                        {
                            return Context.Bill.BillContext.Execute(param);
                        }
                    case "ContactGroups":
                        {
                            return Context.RecipientGroups.RecipientGroupsContext.Execute(param);
                        }
                    case "State":
                        {
                            return Context.State.StateContext.Execute(param);
                        }
                    case "District":
                        {
                            return Context.District.DistrictContext.Execute(param);
                        }
                    case "SystemModule":
                        {
                            return Context.SystemModule.SystemModuleContex.Execute(param);
                        }

                    case "PaperCategoryType":
                        {
                            return Context.PaperCategoryType.PaperCategoryTypeContext.Execute(param);
                        }
                    case "Role":
                        {
                            return Context.UserManagement.RoleContext.Execute(param);
                        }

                    case "Module":
                        {
                            return Context.UserManagement.ModuleContext.Execute(param);
                        }
                    case "Authority":
                        {
                            return Context.Authority.AuthorityContext.Execute(param);
                        }
                    case "AlbumCategory":
                        {
                            return Context.AlbumCategory.AlbumCategorycontext.Execute(param);
                        }

                    case "Village":
                        {
                            return Context.Village.VillageContext.Excute(param);
                        }
                    case "salaryheads":
                        {
                            return Context.salaryhead.salaryheadcontext.Execute(param);
                        }
                    case "loan":
                        {
                            return Context.Loan.LoanContext.Execute(param);
                        }
                    case "QuestionListPDF":
                        {
                            return Context.QuestionListPDF.QuestionListPDFContext.Execute(param);
                        }
                    case "HOD":
                        {
                            return Context.HOD.HODContext.Execute(param);
                        }
                    case "PressClipping":
                        {
                            return Context.PressClipping.PressClipContext.Execute(param);
                        }
                    case "PaidMemeberSubs":
                        {
                            return Context.PaidMemeberSubscription.PaidMemeberSubsContext.Execute(param);
                        }
                    case "MemberAccountDetails":
                        {
                            return Context.MemberAccountDetails.mMemberAccountDetailsContext.Execute(param);
                        }
                    case "Contacts":
                        {
                            return Context.Contacts.ContactsContext.Execute(param);
                        }
                    case "pension":
                        {
                            return Context.Pension.pensionContext.Execute(param);
                        }
                    case "SessionDateSignature":
                        {
                            return Context.SessionDateSignature.SessionDateSignatureContext.Execute(param);
                        }
                    case "RoadPermit":
                        {
                            return Context.RoadPermit.RoadPermitContext.Execute(param);
                        }
                    case "Job":
                        {
                            return Context.JobPosting.JobPostingContext.Execute(param);
                        }
                    case "OneTimeUserRegistration":
                        {
                            return Context.OneTimeUserRegistration.OneTimeUserRegistrationContex.Execute(param);
                        }
                    case "ItemSubscription":
                        {
                            return Context.ItemSubscription.ItemSubscriptionContext.Execute(param);
                        }
                    case "Budget":
                        {
                            return Context.Budget.BudgetContext.Execute(param);
                        }
                    case "Grievance":
                        {
                            return Context.Grievance.GrievanceContext.Execute(param);
                        }
                    case "eFile":
                        {
                            return Context.eFile.eFileContext.Execute(param);
                        }
                    case "UploadImages":
                        {
                            return Context.UploadImages.UploadImagesContext.Execute(param);
                        }
                    case "VsLibraryBooks":
                        {
                            return Context.Library.LibraryBookManagementContext.Execute(param);
                        }
                    case "VsIssueDocument":
                        {
                            return Context.Library.IssueDocumentContext.Execute(param);
                        }
                    case "IssueBook":
                        {
                            return Context.Library.IssueBookContext.Execute(param);
                        }
                    case "VsLibraryBulletin":
                        {
                            return Context.Library.BulletinInformationContext.Execute(param);
                        }
                    case "VsLibraryBillInfo":
                        {
                            return Context.Library.BillInformationContext.Execute(param);
                        }
                    case "DeputyOrJoint":
                        {
                            return Context.DeputyOrJoint.DeputyOrJointcontext.Execute(param);
                        }
                    case "BillWorks":
                        {
                            return Context.BillWorks.BillWorksContex.Execute(param);
                        }
                    case "SaleCounter":
                        {
                            return Context.Library.SaleCounterContext.Execute(param);
                        }
                    case "PrintingPress":
                        {
                            return Context.PrintingPress.PrintingPressContex.Execute(param);
                        }
                    case "RotationofMinisters":
                        {
                            return Context.RotationofMinisters.RotationofMinistersContext.Execute(param);
                        }
                    case "CommitteeReport":
                        {
                            return Context.CommitteeReport.CommitteeReportContext.Execute(param);
                        }
                    case "PaperLaidLibrary":
                        {
                            return Context.Library.PaperLaidContext.Execute(param);
                        }
                    case "FloorVersion":
                        {
                            return Context.Library.FloorVersionContext.Execute(param);
                        }
                    case "Whoiswho":
                        {
                            return Context.Library.WhoiswhoContext.Execute(param);
                        }
                    case "SmsAndEmailReport":
                        {
                            return Context.SmsAndEmail.SmsAndEmailContext.Execute(param);
                        }
                    case "MemberSMSQuota":
                        {
                            return Context.MemberSMSQuota.MemberSMSQuotaContext.Execute(param);
                        }
                    case "CutMotionDemand":
                        {
                            return Context.CutMotionDemand.CutMotionDemandContext.Execute(param);
                        }
                    case "staff":
                        {
                            return Context.Staff.StaffContext.Execute(param);
                        }
                    case "ConstituencyVS":
                        {
                            return Context.ConstituencyVS.ConstituencyContext.Execute(param);
                        }
                    case "Office":
                        {
                            return Context.Office.OfficeContext.Execute(param);
                        }

                    case "Mobiles":
                        {
                            return Context.Mobiles.MobileContext.Excute(param);
                        }
                    case "PublishDocument":
                        {
                            return Context.PublishDocument.PublishDocumentContext.Execute(param);
                        }
                    case "PISModule":
                        {
                            return Context.PISModule.PISDesignationContext.Execute(param);
                        }
                    case "Feedback":
                        {
                            return Context.Feedback.FeedbackContext.Execute(param);
                        }

                    case "FooterPublicDatas":
                        {
                            return Context.FooterPublicDatas.FooterPublicDataContext.Execute(param);
                        }
                    case "Forms":
                        {
                            return Context.Forms.FormsContext.Execute(param);
                        }
                    case "EPWBills":
                        {
                            return Context.Bill.EPWBillsContext.Execute(param);
                        }

                    case "TR2":
                        {
                            return Context.TR2.TR2Contex.Execute(param);
                        }
                    case "UploadMetaFile":
                        {
                            return Context.UploadMetaFile.UploadMetaFileContext.Execute(param);
                        }

                    case "FormDoc":
                        {
                            return Context.TypeofDocuments.FormTypeofDocumentsContext.Execute(param);
                        }
                    case "AssemblyDoc":
                        {
                            return Context.TypeofDocuments.AssemblyTypeofDocumentsContext.Execute(param);
                        }
                    case "PanchayatVillage":
                        {
                            return Context.Panchayat.PanchayatVillageContext.Execute(param);
                        }
                    case "ConstituencyCategory":
                        {
                            return Context.Constituency.ConstituencyCategoryContext.Execute(param);
                        }
                    case "HighestQualification":
                        {
                            return Context.Member.HighestQualificationContext.Execute(param);
                        }

                    case "BudgetBillType":
                        {
                            return Context.Budget.BudgetBillTypeContext.Execute(param);
                        }
                    case "BudgetType":
                        {
                            return Context.Budget.BudgetTypeContext.Execute(param);
                        }
                    case "Governer":
                        {
                            return Context.Governer.GovernerContext.Execute(param);
                        }
                    case "worksProposal": return Context.worksProposal.worksProposal.Execute(param);

                    case "SubEvent":
                        {
                            return Context.Event.SubEventContext.Execute(param);
                        }
                    case "OldSalaryContext":
                        {
                            return Context.salaryhead.membersheadscontext.Execute(param);
                        }
                    case "eFileCommiteeMeetings":
                        {
                            return Context.eFileCommiteeMeetings.eFileContext.Execute(param);
                        }
                    case "CommitteReplyPendency":
                        {
                            return Context.CommitteeReport.CommitteReplyPendencyContext.Execute(param);
                        }
                    case "eFileCommiteePaperLaid":
                        {
                            return Context.eFileCommiteePaperLaid.eFileContext.Execute(param);
                        }

                    case "ContituencySchemeEnums":
                        {
                            return Context.ConstituencyVS.ContituencySchemeEnumsContext.Execute(param);
                        }

                    case "ContituencyWorks":
                        {
                            return Context.ConstituencyVS.ContituencyWorksContext.Execute(param);
                        }


                    case "HouseEmpCommittee":
                        {
                            return Context.Committee.HouseEmpCommitteeContext.Execute(param);
                        }


                    case "SerialNo":
                        {
                            return Context.eFile.SerialNoContext.Execute(param);
                        }

                    case "SubDivision":
                        {
                            return Context.UserManagement.SubDivisionContext.Execute(param);
                        }
                    case "tReference":
                        {
                            return Context.tReference.tReferenceContext.Execute(param);
                        }
                }
            }


            return null;
        }
    }
}
