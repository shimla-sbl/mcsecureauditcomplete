﻿namespace SBL.HPMS.Service
{
    #region Namespace Reffrences

    using SBL.Domain;
    using SBL.Service.Common;
    using System;
    using System.Xml;

    #endregion Namespace Reffrences

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name
    //       "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc
    //       or Service1.svc.cs at the Solution Explorer and start debugging.
    public class HouseControllerService : IHouseControllerService
    {
        public HouseControllerService()
        {
            try
            {
                string filePath = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                string dbConfigFileName = System.IO.Path.Combine(filePath, "DBConfig.xml");
                if (System.IO.File.Exists(dbConfigFileName))
                {
                    XmlDocument dbConfigDom = new XmlDocument();
                    dbConfigDom.Load(dbConfigFileName);
                    if (null != dbConfigDom.DocumentElement)
                    {
                        DomainManager.InitializeDB(dbConfigDom.DocumentElement);
                    }
                }
            }
            catch (Exception ex)
            {
                string excep = ex.Message;
                throw;
            }
        }

        public Byte[] Execute(Byte[] param)
        {
            try
            {
                if (null != param)
                {
                    ServiceParameter paramObj = ServiceParameter.Create(param);
                    if (null != paramObj)
                    {
                        object returnObj = DomainManager.Execute(paramObj);
                        if (null != returnObj)
                        {
                            return Common.ToByteArray(returnObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                 throw;
            }
            return null;
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}