﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegislator.HPMS.ServiceAdaptor.Menu
{
    public class MenuItem
    {
        public string ID = string.Empty;
        public String Text = String.Empty;
        public String Action = String.Empty;
        public String ActionType = String.Empty;
        public String ActionValue = String.Empty;
        public String Controller = String.Empty;
        public String Module = String.Empty;
        public Dictionary<string, MenuItem> Items = new Dictionary<string, MenuItem>();

        public MenuItem this[string MenuId]
        {
            get
            {
                try
                {
                    String nmuIt = MenuId.Trim().ToUpper();
                    if ((null != Items) && (Items.ContainsKey(nmuIt)))
                    {
                        return Items[MenuId];
                    }
                    else
                    {
                        return GetMenuItem(this, nmuIt);
                    }
                }
                catch { }
                return null;
            }
        }

        private MenuItem GetMenuItem(MenuItem currentItem, string MenuId)
        {
            if ((null == currentItem) || (null == currentItem.Items))
            {
                return null;
            }

            foreach (MenuItem curChild in currentItem.Items.Values)
            {
                if ((null != curChild) && (null != curChild.Items))
                {
                    if (curChild.Items.ContainsKey(MenuId))
                    {
                        return curChild.Items[MenuId];
                    }
                    else
                    {
                        MenuItem retVal = GetMenuItem(curChild, MenuId);
                        if (null != retVal)
                        {
                            return retVal;
                        }
                    }
                }
            }

            return null;
        }

        public int Count
        {
            get
            {
                if (null != Items)
                {
                    return Items.Count();
                }
                return 0;
            }
        }
    }
}