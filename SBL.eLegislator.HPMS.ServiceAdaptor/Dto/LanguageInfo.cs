﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegislator.HPMS.ServiceAdaptor.Dto
{
    public class LanguageInfo
    {
        public string Name = string.Empty;
        public string ID = string.Empty;
    }
}