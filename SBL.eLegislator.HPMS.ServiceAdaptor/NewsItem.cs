﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBL.eLegislator.HPMS.ServiceAdaptor
{
    public class NewsItem
    {
        public int ID = 0;
        public String Image = String.Empty;
        public String Text = String.Empty;

        public String Title = String.Empty;

    }
    public class Partyposition
    {
        public String PartyName = String.Empty;
        public int PartyCount = 0;

    }

    public class Reservedseat
    {
        public String SeatName = String.Empty;
        public int SeatCount = 0;

    }
}