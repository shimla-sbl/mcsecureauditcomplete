﻿using SBL.eLegislator.HPMS.ServiceAdaptor.Dto;
using SBL.eLegislator.HPMS.ServiceAdaptor.Menu;
using SBL.eLegislator.HPMS.ServiceAdaptor.SBLWebServices;
using SBL.Service.Common;
using SBL.ServiceLayer.Helpers;
using SBL.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using SBL.DomainModel.ComplexModel.ErrorLog;

namespace SBL.eLegislator.HPMS.ServiceAdaptor
{
    public class ServiceAdaptor
    {
        public object CallService(ServiceParameter Obj)
        {
            object ObjectReturn = null;
            if (null != Obj)
            {
                CFService.Service1Client scMainClient = new CFService.Service1Client();
                //ServiceMain.ServiceMainClient scMainClient = new ServiceMain.ServiceMainClient();
                //ServiceReference1.Service1Client scMainClient = new ServiceReference1.Service1Client();
                Byte[] returnResult = scMainClient.Execute(Obj.ToByteArray());
                if (null != returnResult)
                {
                    try
                    {
                        ObjectReturn = SBL.Service.Common.Common.ObjectFromByteArray(returnResult);
                    }
                    catch (Exception ee)
                    {
                        string ssss = ee.Message;
                    }
                }
            }
            return ObjectReturn;
        }


        #region Language
        public static List<LanguageInfo> GetLanguageInfo()
        {
            List<LanguageInfo> returnValue = new List<LanguageInfo>();

            LanguageInfo newitem = new LanguageInfo();
            newitem.ID = "7C4F28F6-02FC-4627-993D-E255037531AD";
            newitem.Name = "ENGLISH";
            returnValue.Add(newitem);
            newitem = new LanguageInfo();
            newitem.ID = "43177CD4-B51D-497A-A3BC-E81F23B07450";
            newitem.Name = "हिन्दी";
            returnValue.Add(newitem);

            return returnValue;
        }
        #endregion

        #region PartWidget
        public static String PartyPosition
        {
            get
            {
                return "PartyPosition";
            }

        }

        public static String ReservedSeats
        {
            get
            {
                return "ReservedSeats";
            }

        }

        #endregion

        #region MenuModule
        public static List<string> GetAvailableModules(string UserId)
        {
            List<string> modList = new List<string>();
            try
            {
                if (UserId == "")
                {
                    UserId = "00000000-0000-0000-0000-000000000000";
                }

                var methodParameter = new List<KeyValuePair<string, string>>();
                methodParameter.Add(new KeyValuePair<string, string>("@userId", UserId));
                DataSet dataSetModules = null;
                    //ServiceAdaptor.GetDataSetFromService("eVidhan", "eVidhanDb", "SelectMSSql", "[dbo].[usp_GetRolebasedModule]", methodParameter);

                if ((null != dataSetModules) && (null != dataSetModules.Tables) && (dataSetModules.Tables.Count > 0))
                {
                    foreach (DataRow dRo in dataSetModules.Tables[0].Rows)
                    {
                        if ((null != dRo) && (null != dRo["ModuleId"]))
                        {
                            string moduleID = dRo["ModuleId"].ToString().Trim().ToUpper();
                            if ((!string.IsNullOrWhiteSpace(moduleID)) && (!modList.Contains(moduleID)))
                            {
                                modList.Add(moduleID);
                            }
                        }
                    }

                }

            }
            catch { }
            return modList;
        }

        #endregion

        #region GenericWebService
        public static String GetStartPageID()
        {
            String returnValue = "98046BE5-5E66-4A00-9A1E-09F13DCFDF49";
            return returnValue;
        }


        public static DataSet GetDataSetFromService(string ApplicationId, string ConnectionId, string MethodType, string MethodName, List<KeyValuePair<string, string>> methodPara)
        {
            byte[] result = GetbyteFromService(ApplicationId, ConnectionId, MethodType, MethodName, methodPara);
            return XmlHelper.ConvertEncryptedXMLToDataSet(result);
        }


        public static string GetStringFromService(string ApplicationId, string ConnectionId, string MethodType, string MethodName, List<KeyValuePair<string, string>> methodPara)
        {
            try
            {

                byte[] result = GetbyteFromService(ApplicationId, ConnectionId, MethodType, MethodName, methodPara);
                if (result != null && result.Length != 0)
                    return XmlHelper.DecryptByte(result);
                return "";
            }
            catch(Exception ex)
            {
                ErrorLog.WriteToLog(ex, "Mathod Name getworkDetailsByUpdateID  ");
                return null;
            }
            // return XmlHelper.ConvertEncryptedXMLToDataSet(result);
        }


        public static byte[] GetDataSetFromService(string ApplicationId, string ConnectionId, string MethodType, string MethodName, DataSet methodPara)
        {
            SBLWebServiceSoapClient objWebService = new SBLWebServiceSoapClient();
            ServiceRequestXml objServiceRequestXml = new ServiceRequestXml();
            byte[] parameters = objServiceRequestXml.CreateServiceRequestEncryptedXmlBytes(ApplicationId, ConnectionId, MethodType, MethodName, methodPara);
            byte[] result = objWebService.WebService(parameters);
            return result;
            // return XmlHelper.ConvertEncryptedXMLToDataSet(result);
        }

        public static byte[] GetbyteFromService(string ApplicationId, string ConnectionId, string MethodType, string MethodName, List<KeyValuePair<string, string>> methodPara)
        {
            try
            {
                SBLWebServiceSoapClient objWebService = new SBLWebServiceSoapClient();
                ServiceRequestXml objServiceRequestXml = new ServiceRequestXml();
                byte[] parameters = objServiceRequestXml.CreateServiceRequestEncryptedXmlBytes(ApplicationId, ConnectionId, MethodType, MethodName, methodPara);
                byte[] result = objWebService.WebService(parameters);
                return result;
            }
            catch
            {
                return null;
            }
            // return XmlHelper.ConvertEncryptedXMLToDataSet(result);
        }

        #endregion

        #region Menu
        /*  latest get menu items  */
        public static MenuItem GetMenuList(string UserId, String LanguageId)
        {
            MenuItem returnValue = new MenuItem();
            SBLWebServices.SBLWebServiceSoapClient objWebService = new SBLWebServices.SBLWebServiceSoapClient();
            ServiceRequestXml objServiceRequestXml = new ServiceRequestXml();
            var methodParameter = new List<KeyValuePair<string, string>>();
            if (UserId == "")
            {
                UserId = "00000000-0000-0000-0000-000000000000";
            }
            methodParameter.Add(new KeyValuePair<string, string>("@UserId", UserId.ToString()));
            methodParameter.Add(new KeyValuePair<string, string>("@LangId", LanguageId.ToString()));
            byte[] parameters = objServiceRequestXml.CreateServiceRequestEncryptedXmlBytes("emenu", "eVidhanDb", "SelectMSSql", "usp_GetRolebasedMenu", methodParameter);
            byte[] result = objWebService.WebService(parameters);
            DataSet dataSetCertificate = XmlHelper.ConvertEncryptedXMLToDataSet(result);
            AddMenuItem(dataSetCertificate, returnValue);
            return returnValue;
        }

        private static void AddMenuItem(DataSet ds, MenuItem parentItem)
        {
            if (ds == null)
            {
                return;
            }
            DataTable dtSelectedColumns = ds.Tables[0];
            DataRow[] drs = dtSelectedColumns.Select("Submenu = '00000000-0000-0000-0000-000000000000'");

            foreach (DataRow d in drs)
            {
                string id = d["ID"].ToString();
                MenuItem newMenueItem = new MenuItem();
                newMenueItem.ID = id;
                newMenueItem.Text = d["text"].ToString();
                if (null != d["action"].ToString())
                {
                    newMenueItem.Action = d["action"].ToString().Trim();
                }
                if (null != d["actiontype"].ToString())
                {
                    newMenueItem.ActionType = d["actiontype"].ToString().Trim();
                }
                if (null != d["controller"].ToString())
                {
                    newMenueItem.Controller = d["controller"].ToString().Trim();
                }
                if (null != d["module"].ToString())
                {
                    newMenueItem.Module = d["module"].ToString().Trim();
                }
                if (null != d["actionvalue"].ToString())
                {
                    newMenueItem.ActionValue = d["actionvalue"].ToString().Trim();
                }

                if (!parentItem.Items.ContainsKey(id))
                {

                    parentItem.Items.Add(id, newMenueItem);
                }

                if (null != d["Exist"].ToString())
                {
                    foreach (var mitems in parentItem.Items.Values)
                    {
                        if (int.Parse(d["Exist"].ToString()) == 1)
                        {
                            DataRow[] drsubmenu = dtSelectedColumns.Select("Submenu='" + id + "'");

                            foreach (DataRow drsub in drsubmenu)
                            {
                                if (mitems.ID.ToString().ToUpper() == drsub["Submenu"].ToString())
                                {
                                    string id1 = drsub["id"].ToString().ToUpper();
                                    MenuItem newMenueItem1 = new MenuItem();
                                    newMenueItem1.ID = id1;
                                    newMenueItem1.Text = drsub["text"].ToString();
                                    if (null != drsub["action"].ToString())
                                    {
                                        newMenueItem1.Action = drsub["action"].ToString().Trim();
                                    }
                                    if (null != drsub["actiontype"].ToString())
                                    {
                                        newMenueItem1.ActionType = drsub["actiontype"].ToString().Trim();
                                    }
                                    if (null != drsub["controller"].ToString())
                                    {
                                        newMenueItem1.Controller = drsub["controller"].ToString().Trim();
                                    }
                                    if (null != drsub["module"].ToString())
                                    {
                                        newMenueItem1.Module = drsub["module"].ToString().Trim();
                                    }
                                    if (null != drsub["actionvalue"].ToString())
                                    {
                                        newMenueItem1.ActionValue = drsub["actionvalue"].ToString().Trim();
                                    }

                                    if (!parentItem.Items.ContainsKey(id1))
                                    {
                                        mitems.Items.Add(id1, newMenueItem1);
                                    }

                                    if (int.Parse(drsub["Exist"].ToString()) == 1)
                                    {
                                        foreach (var subMenuitems in mitems.Items.Values)
                                        {
                                            DataRow[] drsubmenu_1 = dtSelectedColumns.Select("Submenu='" + id1 + "'");

                                            foreach (DataRow drsubm in drsubmenu_1)
                                            {
                                                if (subMenuitems.ID.ToString().ToUpper() == drsubm["Submenu"].ToString())
                                                {
                                                    string subId = drsubm["id"].ToString().ToUpper();
                                                    MenuItem newMenueSubItem = new MenuItem();
                                                    newMenueSubItem.ID = subId;
                                                    newMenueSubItem.Text = drsubm["text"].ToString();
                                                    if (null != drsubm["action"].ToString())
                                                    {
                                                        newMenueSubItem.Action = drsubm["action"].ToString().Trim();
                                                    }
                                                    if (null != drsubm["actiontype"].ToString())
                                                    {
                                                        newMenueSubItem.ActionType = drsubm["actiontype"].ToString().Trim();
                                                    }
                                                    if (null != drsubm["controller"].ToString())
                                                    {
                                                        newMenueSubItem.Controller = drsubm["controller"].ToString().Trim();
                                                    }
                                                    if (null != drsubm["module"].ToString())
                                                    {
                                                        newMenueSubItem.Module = drsubm["module"].ToString().Trim();
                                                    }
                                                    if (null != drsubm["actionvalue"].ToString())
                                                    {
                                                        newMenueSubItem.ActionValue = drsubm["actionvalue"].ToString().Trim();
                                                    }

                                                    if (!mitems.Items.ContainsKey(subId))
                                                    {
                                                        subMenuitems.Items.Add(subId, newMenueSubItem);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    }

                }
            }
        }

        //public static MenuItem GetMenuList(string UserId, String LanguageId)
        //{
        //    MenuItem returnValue = new MenuItem();
        //    SBLWebServices.SBLWebServiceSoapClient objWebService = new SBLWebServices.SBLWebServiceSoapClient();
        //    ServiceRequestXml objServiceRequestXml = new ServiceRequestXml();
        //    var methodParameter = new List<KeyValuePair<string, string>>();
        //    if (UserId == "")
        //    {
        //        UserId = "00000000-0000-0000-0000-000000000000";
        //    }
        //    methodParameter.Add(new KeyValuePair<string, string>("@UserId", UserId.ToString()));
        //    methodParameter.Add(new KeyValuePair<string, string>("@LangId", LanguageId.ToString()));
        //    byte[] parameters = objServiceRequestXml.CreateServiceRequestEncryptedXmlBytes("emenu", "eVidhanDb", "SelectMSSql", "usp_GetRolebasedMenu", methodParameter);

        //    byte[] result = objWebService.WebService(parameters);

        //    DataSet dataSetCertificate = XmlHelper.ConvertEncryptedXMLToDataSet(result);

        //    //AddMenuItem(dataSetCertificate, returnValue);


        //    return returnValue;

        //}


        #endregion

        #region LoginData

        public bool MatchPassword(string UserId, string Password)
        {
            return true;
            //SBLWebServiceSoapClient objWebService = new SBLWebServiceSoapClient();
            //ServiceRequestXml objServiceRequestXml = new ServiceRequestXml();

            //Guid g = Guid.NewGuid();
            //string Enpassword = string.Empty;

            //var methodParameter = new List<KeyValuePair<string, string>>();
            //methodParameter.Add(new KeyValuePair<string, string>("@UserId", UserId));


            //byte[] parameters = objServiceRequestXml.CreateServiceRequestEncryptedXmlBytes("emenu", "eVidhanDb", "SelectMSSql", "usp_SelectUserInfoById", methodParameter);

            //byte[] result = objWebService.WebService(parameters);

            //DataSet dataSetCertificate = XmlHelper.ConvertEncryptedXMLToDataSet(result);
            //if (null != dataSetCertificate && dataSetCertificate.Tables[0].Rows.Count != 0)
            //{
            //    Enpassword = Convert.ToString(dataSetCertificate.Tables[0].Rows[0]["password"]);
            //    Guid iduser = new Guid(Convert.ToString(dataSetCertificate.Tables[0].Rows[0]["UserId"]));
            //    if (VerifyHash(Password, "SHA1", Enpassword) == true)
            //    {

            //        return true;
            //    }
            //    return false;
            //}
            //else
            //{

            //    return false;
            //}
        }



        public string GetUserLoginData(string UserName, string Password)
        {
          //  int i = Convert.ToInt32("w");

            SBLWebServiceSoapClient objWebService = new SBLWebServiceSoapClient();
            ServiceRequestXml objServiceRequestXml = new ServiceRequestXml();

            Guid g = Guid.NewGuid();
            string Enpassword = string.Empty;

            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@UserName", UserName));


            byte[] parameters = objServiceRequestXml.CreateServiceRequestEncryptedXmlBytes("emenu", "eVidhanDb", "SelectMSSql", "usp_SelectUserInfo", methodParameter);

            byte[] result = objWebService.WebService(parameters);

            DataSet dataSetCertificate = XmlHelper.ConvertEncryptedXMLToDataSet(result);
            if (null != dataSetCertificate && dataSetCertificate.Tables[0].Rows.Count != 0)
            {
                Enpassword = Convert.ToString(dataSetCertificate.Tables[0].Rows[0]["password"]);
                Guid iduser = new Guid(Convert.ToString(dataSetCertificate.Tables[0].Rows[0]["UserId"]));
                if (VerifyHash(Password, "SHA1", Enpassword) == true)
                {

                    return iduser.ToString();
                }
                return Guid.Empty.ToString();
            }
            else
            {

                return Guid.Empty.ToString();
            }



        }
        public string updatePassword(string UserId, string Password)
        {
            var methodParameter = new List<KeyValuePair<string, string>>();
            methodParameter.Add(new KeyValuePair<string, string>("@UserID", UserId));
            methodParameter.Add(new KeyValuePair<string, string>("@NewPwd", ComputeHash(Password, "SHA1", null)));
            String Str = ServiceAdaptor.GetStringFromService("eVidhan", "eVidhanDb", "UpdateMSSql", "SUpdatePwd", methodParameter);
            if (Str != null)
            {
                return Str;
            }
            return "";
        }


        #endregion

        #region Graph
        public static void Reservationxml()
        {

            string path1 = AppDomain.CurrentDomain.BaseDirectory;
            System.IO.File.WriteAllText(path1 + "/XmlGraph/Data.xml", string.Empty);
            XmlDocument XD = new XmlDocument();
            XmlNode root = XD.AppendChild(XD.CreateElement("chart"));
            XD.DocumentElement.SetAttribute("caption", "Reserved Seats");
            XD.DocumentElement.SetAttribute("xAxisName", "Party");
            XD.DocumentElement.SetAttribute("yAxisName", "Number of seats");
            var reservList = ReservedSeatGet();
            foreach (var reserv in reservList)
            {
                XmlElement setnode = XD.CreateElement("Set");
                setnode.SetAttribute("label", reserv.SeatName);
                setnode.SetAttribute("value", reserv.SeatCount.ToString());
                XD.DocumentElement.AppendChild(setnode);
            }
            XD.Save(path1 + "/XmlGraph/Data.xml");

        }
        public static List<Reservedseat> ReservedSeatGet()
        {
            List<Reservedseat> newreserv = new List<Reservedseat>();

            ServiceRequestXml objServiceRequestXml = new ServiceRequestXml();
            SBLWebServiceSoapClient objWebService = new SBLWebServiceSoapClient();
            var methodParameter = new List<KeyValuePair<string, string>>();

            byte[] parameters = objServiceRequestXml.CreateServiceRequestEncryptedXmlBytes("ereserv", "eVidhanDb", "SelectMSSql", "SelectReservedSeat", methodParameter);
            byte[] result = objWebService.WebService(parameters);

            DataSet dataSetCertificate = XmlHelper.ConvertEncryptedXMLToDataSet(result);
            for (int i = 0; i < dataSetCertificate.Tables[0].Rows.Count; i++)
            {
                Reservedseat reserve = new Reservedseat();
                reserve.SeatCount = Convert.ToInt32(dataSetCertificate.Tables[0].Rows[i]["SeatCount"]);
                reserve.SeatName = Convert.ToString(dataSetCertificate.Tables[0].Rows[i]["SeatName"]);
                newreserv.Add(reserve);

            }

            return newreserv;
        }



        public static List<Partyposition> PartyPositionGet()
        {
            List<Partyposition> newparty = new List<Partyposition>();

            ServiceRequestXml objServiceRequestXml = new ServiceRequestXml();
            SBLWebServiceSoapClient objWebService = new SBLWebServiceSoapClient();
            var methodParameter = new List<KeyValuePair<string, string>>();

            byte[] parameters = objServiceRequestXml.CreateServiceRequestEncryptedXmlBytes("eparty", "eVidhanDb", "SelectMSSql", "SelectPartyPosition", methodParameter);
            byte[] result = objWebService.WebService(parameters);

            DataSet dataSetCertificate = XmlHelper.ConvertEncryptedXMLToDataSet(result);
            for (int i = 0; i < dataSetCertificate.Tables[0].Rows.Count; i++)
            {
                Partyposition party = new Partyposition();
                party.PartyCount = Convert.ToInt32(dataSetCertificate.Tables[0].Rows[i]["PartyCount"]);
                party.PartyName = Convert.ToString(dataSetCertificate.Tables[0].Rows[i]["PartyName"]);
                newparty.Add(party);

            }

            return newparty;
        }

        public static void PartyPositionxml()
        {

            string path1 = AppDomain.CurrentDomain.BaseDirectory;
            System.IO.File.WriteAllText(path1 + "/XmlGraph/Datapyramid.xml", string.Empty);
            XmlDocument XD = new XmlDocument();
            XmlNode root = XD.AppendChild(XD.CreateElement("chart"));
            XD.DocumentElement.SetAttribute("isSliced", "1");
            XD.DocumentElement.SetAttribute("bgColor", " A3A3A3");
            var PartyList = PartyPositionGet();
            foreach (var party in PartyList)
            {
                XmlElement setnode = XD.CreateElement("Set");
                setnode.SetAttribute("label", party.PartyName);
                setnode.SetAttribute("value", party.PartyCount.ToString());
                XD.DocumentElement.AppendChild(setnode);
            }
            XD.Save(path1 + "/XmlGraph/Datapyramid.xml");



        }

        #endregion

        #region Encryption/Decription
        public static string ComputeHash(string plainText, string hashAlgorithm, byte[] saltBytes)
        {
            // If salt is not specified, generate it on the fly.
            if (saltBytes == null)
            {
                // Define min and max salt sizes.
                int minSaltSize = 4;
                int maxSaltSize = 8;

                // Generate a random number for the size of the salt.
                Random random = new Random();
                int saltSize = random.Next(minSaltSize, maxSaltSize);

                // Allocate a byte array, which will hold the salt.
                saltBytes = new byte[saltSize];

                // Initialize a random number generator.
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

                // Fill the salt with cryptographically strong byte values.
                rng.GetNonZeroBytes(saltBytes);
            }

            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
                plainTextWithSaltBytes[i] = plainTextBytes[i];

            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];


            HashAlgorithm hash;
            // Make sure hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Initialize appropriate hashing algorithm class.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hash = new SHA1Managed();
                    break;

                case "SHA256":
                    hash = new SHA256Managed();
                    break;

                case "SHA384":
                    hash = new SHA384Managed();
                    break;

                case "SHA512":
                    hash = new SHA512Managed();
                    break;

                default:
                    hash = new MD5CryptoServiceProvider();
                    break;
            }

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

            // Create array which will hold hash and original salt bytes.
            byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                saltBytes.Length];

            // Copy hash bytes into resulting array.
            for (int i = 0; i < hashBytes.Length; i++)
                hashWithSaltBytes[i] = hashBytes[i];

            // Append salt bytes to the result.
            for (int i = 0; i < saltBytes.Length; i++)
                hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashWithSaltBytes);

            // Return the result.
            return hashValue;
        }

        //Not in Use....
        public string DecryptData(string encryptedtext)
        {
            TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider();
            // Convert the encrypted text string to a byte array.
            byte[] encryptedBytes = Convert.FromBase64String(encryptedtext);

            // Create the stream.
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            // Create the decoder to write to the stream.
            CryptoStream decStream = new CryptoStream(ms, tripleDes.CreateDecryptor(), System.Security.Cryptography.CryptoStreamMode.Write);

            // Use the crypto stream to write the byte array to the stream.
            decStream.Write(encryptedBytes, 0, encryptedBytes.Length);
            //decStream.Write(encryptedBytes,0,
            decStream.FlushFinalBlock();

            // Convert the plaintext stream to a string.
            return System.Text.Encoding.Unicode.GetString(ms.ToArray());
        }

        public static Boolean VerifyHash(string plainText, string hashAlgorithm, string hashValue)
        {
            // Convert base64-encoded hash value into a byte array.
            byte[] hashWithSaltBytes = Convert.FromBase64String(hashValue);

            // We must know size of hash (without salt).
            int hashSizeInBits, hashSizeInBytes;

            // Make sure that hashing algorithm name is specified.
            if (hashAlgorithm == null)
                hashAlgorithm = "";

            // Size of hash is based on the specified algorithm.
            switch (hashAlgorithm.ToUpper())
            {
                case "SHA1":
                    hashSizeInBits = 160;
                    break;

                case "SHA256":
                    hashSizeInBits = 256;
                    break;

                case "SHA384":
                    hashSizeInBits = 384;
                    break;

                case "SHA512":
                    hashSizeInBits = 512;
                    break;

                default: // Must be MD5
                    hashSizeInBits = 128;
                    break;
            }

            // Convert size of hash from bits to bytes.
            hashSizeInBytes = hashSizeInBits / 8;

            // Make sure that the specified hash value is long enough.
            if (hashWithSaltBytes.Length < hashSizeInBytes)
                return false;

            // Allocate array to hold original salt bytes retrieved from hash.
            byte[] saltBytes = new byte[hashWithSaltBytes.Length -
                                        hashSizeInBytes];

            // Copy salt from the end of the hash to the new array.
            for (int i = 0; i < saltBytes.Length; i++)
                saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];

            // Compute a new hash string.
            string expectedHashString =
                        ComputeHash(plainText, hashAlgorithm, saltBytes);

            // If the computed hash matches the specified hash,
            // the plain text value must be correct.

            return (hashValue == expectedHashString);
        }

        #endregion

        #region FileTransferService

        public static void CallUploadFile(string FileContent, string FileName, int FileLength, string FileFolder, string Type)
        {
            FileTransferServices.FileTransferServiceSoapClient fileTranfer = new FileTransferServices.FileTransferServiceSoapClient();
            fileTranfer.SaveImages(FileContent, FileName, FileLength, FileFolder, Type);
        }

        #endregion
    }
}
