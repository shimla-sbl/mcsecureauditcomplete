﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Xml;


namespace SBL.ServiceLayer.Helpers
{
    public class EncryptionHelper
    {
        static string RSAPUBLICKEY = "";
        static string RSAPRIVATEKEY = "";
        public static string GenerateKeys()
        {

            try
            {
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    RSAPUBLICKEY = RSA.ToXmlString(false);
                    RSAPRIVATEKEY = RSA.ToXmlString(true);
                }
            }
            catch 
            {
            }
            return (RSAPUBLICKEY);
        }

        public XmlDocument EncryptAsymmetric(string xml)
        {
            string EncryptedXML = "";


            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.LoadXml(xml);

            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                XmlElement xmlElemt;
                xmlElemt = xmlDoc.DocumentElement;
                EncryptedXml xmlEnc = new EncryptedXml(xmlDoc);
                // GenerateKeys();
                RSAPUBLICKEY = "<RSAKeyValue><Modulus>65pXiiOp7BuM6C90o0/37+iwyITRyy4G8ZKaXM+CQz+cIKDlHj1MoQSqtmSPP8VtxnfZmxCt55/GGRGN5yk3BmukH56NyI03C3ROY8Jj8y1eZSAPfEutLeYYmKA/lx6C4nEoKErfSURBKmGWTc4RK0Ypz2z8WzM4dlMPt/GIQsE=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
                RSA.FromXmlString(RSAPUBLICKEY);
                xmlEnc.AddKeyNameMapping("asyncKey", RSA);
                
                EncryptedData encXml = xmlEnc.Encrypt(xmlElemt, "asyncKey");
                EncryptedXml.ReplaceElement(xmlElemt, encXml, false);
            }
            EncryptedXML = xmlDoc.OuterXml;
            return xmlDoc;

        }

        public XmlDocument DecryptAsymmetric(string xml)
        {
            string RSAPRIVATEKEY = "<RSAKeyValue><Modulus>65pXiiOp7BuM6C90o0/37+iwyITRyy4G8ZKaXM+CQz+cIKDlHj1MoQSqtmSPP8VtxnfZmxCt55/GGRGN5yk3BmukH56NyI03C3ROY8Jj8y1eZSAPfEutLeYYmKA/lx6C4nEoKErfSURBKmGWTc4RK0Ypz2z8WzM4dlMPt/GIQsE=</Modulus><Exponent>AQAB</Exponent><P>+Fi+aJF9BL7j8RwfaSGieG0HnXstMORKvK/01vKECPiJ7I30Ka+5MO9BcnZmOaG7IGfRfK1UyvTxDl25QzP9Ew==</P><Q>8t0PZLmV/afEU2o7ka3LLdY6dokMNPmpUPE43sOZw7GKT85qgEtBqvWwhnPig2N+owZ64WXOtLbx5Ec8CPgfWw==</Q><DP>cmYGyAqEyWPZgl6PBZGt0sV+pYdxKL1ww/xVz5IFSlCa0DIP0AgXSbhcsIpjypZ6qZHJSSJbFebBB/oadh+Dqw==</DP><DQ>IswFnprwoK1e9cysyEysZd7h9YXhV93ForFNQq2n5GAVvyWGIOenewVEy57i/4xL4rPU+2KI4V+s/NYwBeD3LQ==</DQ><InverseQ>ignVViW/J8blktdP24Ir1ZHrQvsSr0iT/MKThPl6Bo8IOv5lthZC4+8jzPC0ULu8ASgYCDQO1pr0sB9jLW3pXA==</InverseQ><D>id+4es6EEffNbdLXnvqdTXgOfEm9u/kjdxsj2kxVHqWK6E0/x4J35YKlpDcU3Wzb4NkiLuyD2JhXhTQQpsYnvCqsZJThRPwHFwRPO1QQRlPaPZg+vtddfax/tcGq0aRkC8g5ubS95mSMEzat9AkVxC+NLBT9xer8yQITd/TtCEE=</D></RSAKeyValue>";
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            EncryptedXml enc1Xml = new EncryptedXml(doc);
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
                RSA.FromXmlString(RSAPRIVATEKEY);
                enc1Xml.AddKeyNameMapping("asyncKey", RSA);
                enc1Xml.DecryptDocument();
            }
            return doc;

        }

        public byte[] EncryptXmlToByte(string xml)
        {
            
         
            byte[] byteDataReturn;
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            {
              
                RSAPUBLICKEY = "<RSAKeyValue><Modulus>65pXiiOp7BuM6C90o0/37+iwyITRyy4G8ZKaXM+CQz+cIKDlHj1MoQSqtmSPP8VtxnfZmxCt55/GGRGN5yk3BmukH56NyI03C3ROY8Jj8y1eZSAPfEutLeYYmKA/lx6C4nEoKErfSURBKmGWTc4RK0Ypz2z8WzM4dlMPt/GIQsE=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
                RSA.FromXmlString(RSAPUBLICKEY);
                byte[] byteData = System.Text.ASCIIEncoding.ASCII.GetBytes(xml);
               byteDataReturn= RSA.Encrypt(byteData, false);              
            }

            return byteDataReturn;

        }

        public string DecryptXml( byte[] xml)
        {
           string returndata;
            string RSAPRIVATEKEY = "<RSAKeyValue><Modulus>65pXiiOp7BuM6C90o0/37+iwyITRyy4G8ZKaXM+CQz+cIKDlHj1MoQSqtmSPP8VtxnfZmxCt55/GGRGN5yk3BmukH56NyI03C3ROY8Jj8y1eZSAPfEutLeYYmKA/lx6C4nEoKErfSURBKmGWTc4RK0Ypz2z8WzM4dlMPt/GIQsE=</Modulus><Exponent>AQAB</Exponent><P>+Fi+aJF9BL7j8RwfaSGieG0HnXstMORKvK/01vKECPiJ7I30Ka+5MO9BcnZmOaG7IGfRfK1UyvTxDl25QzP9Ew==</P><Q>8t0PZLmV/afEU2o7ka3LLdY6dokMNPmpUPE43sOZw7GKT85qgEtBqvWwhnPig2N+owZ64WXOtLbx5Ec8CPgfWw==</Q><DP>cmYGyAqEyWPZgl6PBZGt0sV+pYdxKL1ww/xVz5IFSlCa0DIP0AgXSbhcsIpjypZ6qZHJSSJbFebBB/oadh+Dqw==</DP><DQ>IswFnprwoK1e9cysyEysZd7h9YXhV93ForFNQq2n5GAVvyWGIOenewVEy57i/4xL4rPU+2KI4V+s/NYwBeD3LQ==</DQ><InverseQ>ignVViW/J8blktdP24Ir1ZHrQvsSr0iT/MKThPl6Bo8IOv5lthZC4+8jzPC0ULu8ASgYCDQO1pr0sB9jLW3pXA==</InverseQ><D>id+4es6EEffNbdLXnvqdTXgOfEm9u/kjdxsj2kxVHqWK6E0/x4J35YKlpDcU3Wzb4NkiLuyD2JhXhTQQpsYnvCqsZJThRPwHFwRPO1QQRlPaPZg+vtddfax/tcGq0aRkC8g5ubS95mSMEzat9AkVxC+NLBT9xer8yQITd/TtCEE=</D></RSAKeyValue>";
            RSACryptoServiceProvider RSA = new RSACryptoServiceProvider();
            {
                RSA.FromXmlString(RSAPRIVATEKEY);
                byte[] byteDataReturn = RSA.Decrypt(xml, false);
                returndata=ASCIIEncoding.ASCII.GetString(byteDataReturn);
            }
            return returndata;
        }

        public byte[] Encrypt(string xml)
        {
             Guid g = Guid.NewGuid();
             string password = Convert.ToBase64String(g.ToByteArray());
             password = password.Replace("=", "");
             password = password.Replace("+", "");

             string encryptedData = EncryptStringSymmetric(xml, password);
             byte[] byteData = System.Text.ASCIIEncoding.ASCII.GetBytes(encryptedData);

             byte[] encryptedPass = EncryptXmlToByte(password);
            byte[] returnData=new byte[byteData.Length+encryptedPass.Length];
            Buffer.BlockCopy(byteData, 0, returnData, 0, byteData.Length);
            Buffer.BlockCopy(encryptedPass, 0, returnData, byteData.Length , encryptedPass.Length);
            return returnData;
            
        }

        public string Decrypt(byte[] data)
        {
            if (data == null) return "";
            byte[] encryptedPass = new byte[128];
            byte[] dataByte = new byte[data.Length-128];

            Buffer.BlockCopy(data, data.Length-128, encryptedPass, 0,128);
            string password = DecryptXml(encryptedPass);

            Buffer.BlockCopy(data, 0, dataByte, 0,data.Length-128);
            string encryptedData=ASCIIEncoding.ASCII.GetString(dataByte);
            string xmlData = DecryptString(encryptedData,password);
            return xmlData;
        }

        private static string EncryptStringSymmetric(string InputText, string Password)
        {
            // Rihndael class.

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            // First we need to turn the input strings into a byte array.

            byte[] PlainText = System.Text.Encoding.Unicode.GetBytes(InputText);

            // We are using salt to make it harder to guess our key

            // using a dictionary attack.

            byte[] Salt = Encoding.ASCII.GetBytes(Password.Length.ToString());

            // The (Secret Key) will be generated from the specified

            // password and salt.

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(Password, Salt);

            // Create a encryptor from the existing SecretKey bytes.

            // We use 32 bytes for the secret key

            // (the default Rijndael key length is 256 bit = 32 bytes) and

            // then 16 bytes for the IV (initialization vector),

            // (the default Rijndael IV length is 128 bit = 16 bytes)

            ICryptoTransform Encryptor = RijndaelCipher.CreateEncryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

            // Create a MemoryStream that is going to hold the encrypted bytes

            MemoryStream memoryStream = new MemoryStream();

            // Create a CryptoStream through which we are going to be processing our data.

            // CryptoStreamMode.Write means that we are going to be writing data

            // to the stream and the output will be written in the MemoryStream

            // we have provided. (always use write mode for encryption)

            CryptoStream cryptoStream = new CryptoStream(memoryStream, Encryptor, CryptoStreamMode.Write);

            // Start the encryption process.

            cryptoStream.Write(PlainText, 0, PlainText.Length);

            // Finish encrypting.

            cryptoStream.FlushFinalBlock();

            // Convert our encrypted data from a memoryStream into a byte array.

            byte[] CipherBytes = memoryStream.ToArray();

            // Close both streams.

            memoryStream.Close();

            cryptoStream.Close();

            // Convert encrypted data into a base64-encoded string.

            // A common mistake would be to use an Encoding class for that.

            // It does not work, because not all byte values can be

            // represented by characters. We are going to be using Base64 encoding

            // That is designed exactly for what we are trying to do.

            string EncryptedData = Convert.ToBase64String(CipherBytes);

            // Return encrypted string.

            return EncryptedData;

        }

        private static string DecryptString(string InputText, string Password)
        {

            RijndaelManaged RijndaelCipher = new RijndaelManaged();

            byte[] EncryptedData = Convert.FromBase64String(InputText);

            byte[] Salt = Encoding.ASCII.GetBytes(Password.Length.ToString());

            PasswordDeriveBytes SecretKey = new PasswordDeriveBytes(Password, Salt);

            // Create a decryptor from the existing SecretKey bytes.

            ICryptoTransform Decryptor = RijndaelCipher.CreateDecryptor(SecretKey.GetBytes(32), SecretKey.GetBytes(16));

            MemoryStream memoryStream = new MemoryStream(EncryptedData);

            // Create a CryptoStream. (always use Read mode for decryption).

            CryptoStream cryptoStream = new CryptoStream(memoryStream, Decryptor, CryptoStreamMode.Read);

            // Since at this point we don’t know what the size of decrypted data

            // will be, allocate the buffer long enough to hold EncryptedData;

            // DecryptedData is never longer than EncryptedData.

            byte[] PlainText = new byte[EncryptedData.Length];

            // Start decrypting.

            int DecryptedCount = cryptoStream.Read(PlainText, 0, PlainText.Length);

            memoryStream.Close();

            cryptoStream.Close();

            // Convert decrypted data into a string.

            string DecryptedData = Encoding.Unicode.GetString(PlainText, 0, DecryptedCount);

            // Return decrypted string.

            return DecryptedData;

        }

    }
}
