﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.SmsGateway
{
    [Table("tSmsGateway")]
    [Serializable]
   public partial class tSmsGateway
   {
       [Key]
       [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int ID { get; set; }

       public string SmsGatewayId { get; set; }

       public string SMSText { get; set; }
       public string SMSTextReplaceValue { get; set; }

   }
}
