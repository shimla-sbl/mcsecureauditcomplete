﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.HOD
{
    
    [Table("mHOD")]
    [Serializable]
    public class mHOD
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int HODID { get; set; }
        
        public int? EmpCode { get; set; }

        public string DeptID { get; set; }

        [StringLength(20)]
        public string Prefix { get; set; }

        [Required]
        public string HODName { get; set; }

        public string HODNameLocal { get; set; }

        public string DesignationDescription { get; set; }

        [StringLength(20)]
        public string IASRank { get; set; }

        public string Email { get; set; }

        public string PhoneResidence { get; set; }

        public string PhoneOffice { get; set; }

        public string PhoneOfficeAlternate { get; set; }

        public string Mobile { get; set; }

        public bool? IsDeleted { get; set; }

        public bool? IsActive { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string AadhaarId { get; set; }

        public int? OrderId { get; set; }

        [NotMapped]
        public string GetDepartmentName { get; set; }
    }
}
