﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SBL.DomainModel.Models.salaryhead
{
    [Serializable]
    [Table("membersHeads")]
    public class membersHead
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoId { get; set; }
        public int membersCatId { get; set; }
        public int sHeadId { get; set; }
        public double? amount { get; set; }
        public bool status { get; set; }
        [NotMapped]
        public int memberId { get; set; }

        public bool Fixed { get; set; }

        public string amountType { get; set; }

        public string htype { get; set; }
        [NotMapped]
        public string headName { get; set; }
        [NotMapped]

        public string memberName { get; set; }

        [NotMapped]
        public string designation { get; set; }

        public double? total_allowances { get; set; }
        [NotMapped]
        public double? total_deductions { get; set; }
        [NotMapped]
        public double? total { get; set; }

        [NotMapped]
        public int? month { get; set; }

        [NotMapped]
        public int? year { get; set; }
        [NotMapped]
        public string Mode { get; set; }



    }
}
