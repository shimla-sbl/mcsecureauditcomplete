﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.salaryhead
{
    [Serializable]
    [Table("membersMonthlySalaryDetails")]
    public class membersMonthlySalaryDetails
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoid { get; set; }
        public int membersid { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public double totalAllowances { get; set; }
        public double totalAGDeductionsA { get; set; }
        public double? totalTODeducationsB { get; set; }
        public int catID { get; set; }
        public double grandtotal { get; set; }
        public DateTime createdDate { get; set; }
        public DateTime? modifiedDate { get; set; }
        public int SOStatus { get; set; }
        public DateTime? SOActionDate { get; set; }
        public DateTime? JDCActionDate { get; set; }
        public int JDCStatus { get; set; }
        public string remark { get; set; }
        public int createdBy { get; set; }
        public int GenerateStatus { get; set; }
        public DateTime? GeneratedDate { get; set; }
        public string filePath { get; set; }
        public DateTime? encashmentdate { get; set; }
        public int? EstablishmentID { get; set; }
        public int AssemblyId { get; set; }
        public int Ispart { get; set; }



    }
}
