﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.salaryhead
{
    [Serializable]
    [Table("membersSalary")]
    public class membersSalary
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoId { get; set; }

        public int membersId { get; set; }

        public int catID { get; set; }

        public int headId { get; set; }

        public double amount { get; set; }


        [StringLength(2)]
        public string headtype { get; set; }

        public string hsubType { get; set; }

        public int monthID { get; set; }

        public int Year { get; set; }

        public long? loanNumber { get; set; }

        public int CreatedBy { get; set; }

        public DateTime? createdDate { get; set; }

        [NotMapped]
        public string sHeadName { get; set; }

        public int AssemblyId { get; set; }

        public int ispart { get; set; }
    }
    [Serializable]
    public class salaryBillInfo
    {
        public int AutoID { get; set; }
        public int memberID { get; set; }
        public string memberName { get; set; }
        public string Designation { get; set; }
        public double netSalary { get; set; }
        public double totalAllowances { get; set; }
        public double totalDeductions { get; set; }
        public string viewRemark { get; set; }
        public int status { get; set; }
        public int? generateStatus { get; set; }

        public bool? checkStatus { get; set; }
    }
}

