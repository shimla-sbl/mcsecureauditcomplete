﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.salaryhead
{
    [Serializable]
    [Table("memberCategories")]
    public class memberCategory
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int autoId { get; set; }

        public string categoryName { get; set; }

    }
}
