﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.salaryhead
{
    [Serializable]
    [Table("salaryheads")]
    public class salaryheads
    {


        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int sHeadId { get; set; }

        [Required]
        public string sHeadName { get; set; }


        [StringLength(2)]
        public string hType { get; set; }

        public string hsubType { get; set; }
        [Required]
        public int orderNo { get; set; }

        [Required]
        public string amountType { get; set; }

        public int? perc { get; set; }

        public int? salaryComponentId { get; set; }

        public int? LoanID { get; set; }

        public string LoanComponentType { get; set; }

        public bool Status { get; set; }

        [NotMapped]
        public int membersCatID { get; set; }

        [NotMapped]
        public double? amount { get; set; }


        [NotMapped]
        public string MonthName { get; set; }

        [NotMapped]
        public int year { get; set; }

        [NotMapped]
        public string memberName { get; set; }

        [NotMapped]
        public int memberID { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public bool Fixed { get; set; }


    }

    [Serializable]
    public class salaryHeadList
    {
        public int sHeadID { get; set; }
        public string sHeadName { get; set; }

    }
    [Serializable]
    public class salaryComponentList
    {
        public int? componentID { get; set; }
        public int? perc { get; set; }
    }

}
