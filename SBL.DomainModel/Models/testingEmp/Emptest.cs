﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.testingEmp
{
    
  
        [Serializable]
        [Table("TestEmployee")]
        public class Emptest
        {
            [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]                     
            [Required(ErrorMessage = "Please Enter Employee Name")]
            public string EmployeeName { get; set; }
            public string Address { get; set; }
            [NotMapped]
            public string EntryType { get; set; }
        
    }
}
