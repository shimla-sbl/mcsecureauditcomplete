﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.VideoArkive
{
    
    [Serializable]
    [Table("mVideoArkive")]
    public class mVideoArkive
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VideoId { get; set; }

      
        public DateTime? SessionDate { get; set; }

        public string Description { get; set; }

        public string VideoPath { get; set; }

        public string VideoYouTubePath { get; set; }

        public bool? IsActive { get; set; }
    }
}
