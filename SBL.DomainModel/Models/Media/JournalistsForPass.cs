﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Media
{
    [Serializable]
    public class JournalistsForPass
    {
        [Key]
        public int JournalistsForPassId { get; set; }
        [ForeignKey("Journalist")]
        public int JournalistId { get; set; }
        public bool IsCommitteMember { get; set; }
        public string Year { get; set; }
        public bool IsRequested { get; set; }
        public DateTime? RequestedDate { get; set; }
        public Journalist Journalist { get; set; }

        public int? AssemblyCode { get; set; }
        public int? SessionCode { get; set; }

        //Changed Sanjay
        public bool IsJournalistIdForApproval { get; set; }
    }
}
