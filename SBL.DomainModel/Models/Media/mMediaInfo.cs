﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Media
{
    [Serializable]
    [Table("mMediaInfo")]
    public class mMediaInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 mUserId { get; set; }
        public string mUserCode { get; set; }
        public string mFirstName { get; set; }
        public string mLastName { get; set; }
        public string mMobileNumber { get; set; }
        public string mAadhaarId { get; set; }
        public string mProfilePic { get; set; }
    }
}
