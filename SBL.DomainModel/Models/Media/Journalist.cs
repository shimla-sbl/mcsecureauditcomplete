﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Media
{
    [Serializable]
    public class Journalist
    {
        [Key]
        public int JournalistID { get; set; }
        public int JournalistCode { get; set; }        

        public string Prefix { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string FatherName { get; set; }

        public string AcreditationNumber { get; set; }

        //[Required]
        public string Photo { get; set; }
       
        public string UID { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }

        public string Designation { get; set; }

        public string Gender { get; set; }

        public int? Age { get; set; }

        public string Address { get; set; }

        public bool? IsDeleted { get; set; }
      
        [ForeignKey("Organization")]
        public int OrganizationID { get; set; }

        public virtual Organization Organization { get; set; }

        public bool IsAcredited { get; set; }

        //Added By Ram.
        public Guid? UserID { get; set; }

        [NotMapped]
        public bool IsInCurrentYear { get; set; }

        [NotMapped]
        public bool IsInCurrentSession { get; set; }

        [NotMapped]
        public bool IsRequested { get; set; }

        [NotMapped]
        public int RequestId { get; set; }

        //Added by Umang

        public string FileName { get; set; }

        public string ThumbName { get; set; }

        [NotMapped]
        public string ImageAcessurl { get; set; }

    }
}
