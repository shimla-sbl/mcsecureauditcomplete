﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;

using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Media
{
    [Serializable]
    public class Organization
    {
        [Key]
        public int OrganizationID { get; set; }
        [Required]
        public string Name { get; set; }

        public string Address { get; set; }
        [Required]
        public string Type { get; set; }

        public bool IsActive { get; set; }

    }
}
