﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.EvSecretories
{
    [Serializable]
    [Table("mVSSecretaries")]
    public class eVSecretories
    {
        [Key, Column(Order = 1)]
        public int AssemblyID { get; set; }

        [Key, Column(Order = 2)]
        public int SecretaryID { get; set; }

        public string SecretaryName { get; set; }

        [Required]
        public DateTime SecretarystartDate { get; set; }

        public DateTime? SecretaryEndDate { get; set; }

        public bool? Active { get; set; }

        public string Remarks { get; set; }

        public string Photo_Location { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string PresentAddress { get; set; }

        public string PermanentAddress { get; set; }

        public DateTime? DateOfMarriage { get; set; }

        public string FatherName { get; set; }

        public DateTime? Dateofbirth { get; set; }

        public string PlaceOfBirth { get; set; }

        public string MaritalStatus { get; set; }

        public string SpouseName { get; set; }

        public string EducationalQualification { get; set; }

        public string Postings { get; set; }

        public string Training { get; set; }

        public string CountriesVisited { get; set; }

        public string SpecialInterest { get; set; }

        public string Para1 { get; set; }

        public string Para2 { get; set; }

        public string Para3 { get; set; }

    }
}
