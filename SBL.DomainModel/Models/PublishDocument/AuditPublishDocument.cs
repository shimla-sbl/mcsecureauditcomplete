﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PublishDocument
{
    [Serializable]
    [Table("AuditPublishDocument")]
    public class AuditPublishDocument
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int PublishId { get; set; }

        public string fromDepartmentId { get; set; }

        public int fromOfficeId { get; set; }

        public string Documenttype { get; set; }

        public DateTime auditDate { get; set; }

        public string Attachment { get; set; }

    }
}
