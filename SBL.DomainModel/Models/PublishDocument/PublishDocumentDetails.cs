﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PublishDocument
{
    [Serializable]
    [Table("PublishDocumentDetails")]
    public class PublishDocumentDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PublishDocumentID { get; set; }
        public string FromdepartmentId { get; set; }
        public string TomdepartmentId { get; set; }
        public int FromOfficeId { get; set; }
        public int ToOfficeId { get; set; }
        public string DocumentType { get; set; }
        public string Subject { get; set; }
        public string Attachment { get; set; }
        public DateTime DocumentCreatedDate { get; set; }
        [NotMapped]
        public Guid IsUserId { get; set; }
        [NotMapped]
        public string IsUserType { get; set; }
    }
}
