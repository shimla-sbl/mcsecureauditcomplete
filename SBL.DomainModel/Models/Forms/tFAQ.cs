﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Forms
{
    [Table("tFAQ")]
    [Serializable]
   public class tFAQ
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string FAQ { get; set; }

        public string FAA { get; set; }

        public bool IsPublished { get; set; }
    }

    //public class tFAQQ
    //{
    //    public string Mode { get; set; }
    //    public byte Status { get; set; }


    //}
    
}
