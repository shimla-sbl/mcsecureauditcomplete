﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Forms
{
    [Table("tSiteMap")]
    [Serializable]
    public class tSiteMap
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Section { get; set; }

        public string Title { get; set; }

        public string URLPath { get; set; }
    }

   
}
