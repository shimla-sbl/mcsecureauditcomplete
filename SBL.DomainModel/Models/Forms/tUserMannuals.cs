﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Forms
{
    [Table("tUserMannuals")]
    [Serializable]
   public class tUserMannuals
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MannualID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string FilePath { get; set; }
    }

}
