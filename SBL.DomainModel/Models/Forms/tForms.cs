﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SBL.DomainModel.Models.Forms
{
    [Table("tForms")]
    [Serializable]
    public class tForms
    {
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int FormId { get; set; }

        [Required]
        public int AssemblyId { get; set; }

        
        public int? TypeofFormId { get; set; }

        public string Description { get; set; }

        public string UploadFile { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool Status { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }

        [NotMapped]
        public string AssemblyName { get; set; }

        

        [NotMapped]
        public string FormDocTypeName { get; set; }

        [NotMapped]
        public string FileAcessPath { get; set; }

    }
}
