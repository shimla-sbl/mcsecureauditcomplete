﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Forms
{
    [Table("mSessionForms")]
    [Serializable]
    public class mSessionForms
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int FileType { get; set; }

        public string FileDescription { get; set; }

        public string FilePath { get; set; }




    }
}
