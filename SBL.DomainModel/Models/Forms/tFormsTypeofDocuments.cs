﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Forms
{
    [Table("tFormsTypeofDocuments")]
    [Serializable]
    public class tFormsTypeofDocuments
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TypeofFormId { get; set; }

        public string TypeofFormName { get; set; }

        public string Description { get; set; }

        public bool? IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool? IsDeleted { get; set; }

    }
}
