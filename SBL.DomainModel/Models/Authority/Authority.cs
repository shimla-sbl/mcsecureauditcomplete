﻿namespace SBL.DomainModel.Models.Authority
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("mAuthority")]
    public class mAuthority
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AuthorityId { get; set; }
        public string AuthorityName { get; set; }
        public string PriFixName { get; set; }
        public string Name { get; set; }
        public string Name_Local { get; set; }
        public string FatherName { get; set; }
        public string FatherName_Local { get; set; }
        public string DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public string MaritalStatus { get; set; }
        public string SpouseName { get; set; }
        public string SpouseName_Local { get; set; }
        public string EducationalQualification { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string Postings { get; set; }
        public string Training { get; set; }
        public string CountriesVisited { get; set; }
        public string SpecialInterest { get; set; }
        public string MeetingsAttended { get; set; }
        public string DesignationDescriptions { get; set; }
        [Column(TypeName = "image")]
        public byte[] Photo { get; set; }
        public string DateofMarriage { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }



        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public string FileName { get; set; }
        public string ThumbName { get; set; }

        public string FilePath { get; set; }
        [NotMapped]
        public string ImageAcessurl { get; set; }

        public int? SecretoryId { get; set; }

        [NotMapped]
        public string GetSecretoryName { get; set; }


    }
}
