﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.AssemblyRemarks
{
    [Serializable]
    [Table("mAssemblyRemarks")]
   public class mAssemblyRemarks
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AssemblyRemarksID { get; set; }

        public string AssemblyRemarks { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }
     
        public DateTime? CreatedDate { get; set; }

        public bool? Active { get; set; }

        public bool? IsDeleted { get; set; }

        public int AssemblyID { get; set; }

        [NotMapped]
        public string GetAssemblyName { get; set; }

    }
}
