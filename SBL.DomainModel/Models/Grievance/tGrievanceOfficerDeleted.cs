﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Grievance
{
    [Table("tGrievanceOfficerDeleted")]
    [Serializable]
    public class tGrievanceOfficerDeleted
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 Id { get; set; }
        public Int32 MemberCode { get; set; }
        public Int32 OfficeId { get; set; }
        public string AdharId { get; set; }
        public DateTime CreatedDate { get; set; }

    }
}
