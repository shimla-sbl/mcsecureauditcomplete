﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Grievance
{
    [Table("mUserInformation")]
    [Serializable]
    public class mUserInformation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 UserId { get; set; }

        public string UserCode { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MobileNumber { get; set; }

        public string Address { get; set; }

        public string AadhaarId { get; set; }

        public string ProfilePic { get; set; }

        public string IMEICode { get; set; }

        public string Constituency { get; set; }

        public string Panchayat { get; set; }

        public string District { get; set; }

        public string WardNumber { get; set; }

        public string PanchayatId { get; set; }

        public string ConsistuencyId { get; set; }

        public string AssemblyId { get; set; }

        public string DeviceToken { get; set; }

        public string DeviceType { get; set; }


        public string SubDivisionId { get; set; }

        public string SubDivisonName { get; set; }

        public bool? IsTrueForSelectedSms { get; set; }
        
        [NotMapped]
        public List<mUserInformation> userInfo { get; set; }

        [NotMapped]
        public mUserInformation userInfousercode { get; set; }

        [NotMapped]
        public virtual ICollection<mUserInformation> UserInformationGr { get; set; }
        
        
    }
}
