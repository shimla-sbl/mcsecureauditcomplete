﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Grievance
{
    [Table("ActionType")]
    [Serializable]
   public class ActionType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 SlNo { get; set; }

        public int? StatusId { get; set; }

        public string StatusCode { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

    }
}
