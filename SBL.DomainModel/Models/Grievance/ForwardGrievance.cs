﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Grievance
{
    [Table("ForwardGrievance")]
    [Serializable]
    public class ForwardGrievance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 ForwardId { get; set; }

        public string GrvCode { get; set; }

        public int MemberCode { get; set; }

        public string DeptId { get; set; }

        public Int64 OfficeId { get; set; }

        public string OfficerCode { get; set; }
        
        public DateTime ForwardDate { get; set; }

        public string Type { get; set; }

        [NotMapped]
        public Guid UserId { get; set; }

        [NotMapped]
        public string Name { get; set; }

        //[NotMapped]
        //public string DeptId { get; set; }

        [NotMapped]
        public string DeptName { get; set; }

        [NotMapped]
        public string AadarId { get; set; }

        [NotMapped]
        public string Officename { get; set; }

        [NotMapped]
        public string Designation { get; set; }
        [NotMapped]
        public string SubDivisionId { get; set; }
        [NotMapped]
        public string Mobile { get; set; }

        [NotMapped]
        public string Email { get; set; }
        [NotMapped]
        public int status { get; set; }

    }
}
