﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Grievance
{
    [Table("tGrievanceReply")]
    [Serializable]
    public class tGrievanceReply
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 GrievanceReplyID { get; set; }
        public string Reply { get; set; }
        public string OfficerCode { get; set; }
        public string GrvCode { get; set; }
        public DateTime ActionDate { get; set; }
        public int StatusId { get; set; }
        [NotMapped]
        public string OfficerName { get; set; }
        [NotMapped]
        public string MobileNumber { get; set; }
        public string DisposedBy { get; set; }
    }
}
