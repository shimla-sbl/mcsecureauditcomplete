﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Tour
{
	[Serializable]
	[Table("tMemberTour")]
	public class tMemberTour
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int TourId { get; set; }

		public int AssemblyId { get; set; }

		public int MemberId { get; set; }

		[Required]
		public string TourTitle { get; set; }

		public string LocalTourTitle { get; set; }

		public string TourDescrption { get; set; }

		public string LocalTourDescription { get; set; }

		public DateTime TourFromDateTime { get; set; }

		public DateTime TourToDateTime { get; set; }

		public string Attachement { get; set; }

		public string AliasAttachment { get; set; }

		public Guid CreatedBy { get; set; }

		public DateTime? CreatedDate { get; set; }

		public Guid? ModifiedBy { get; set; }

		public DateTime? ModifiedDate { get; set; }

		public int Hits { get; set; }

		[NotMapped]
		public string MemberName { get; set; }

		public string mode { get; set; }

		public string Purpose { get; set; }

		public string EditMode { get; set; }

		[NotMapped]
		public string NotifyDate { get; set; }

		[NotMapped]
		public string NotifyTime { get; set; }

		public string Departure { get; set; }

		public string Arrival { get; set; }

		public string DepartureTime { get; set; }

		public string ArrivalTime { get; set; }

		public bool IsPublished { get; set; }

		public DateTime? LastUpdatedDate { get; set; }

		public string TourLocation { get; set; }

        public bool isLocale { get; set; }

		// Add for Return msg and Count on 16/01/2015 by sunil
		[NotMapped]
		public string Msg { get; set; }

		[NotMapped]
		public int TodayToursUpdatedCnt { get; set; }

		[NotMapped]
		public int UpcomingToursUpdatedCnt { get; set; }

		[NotMapped]
		public int PreviousToursUpdatedCnt { get; set; }

		[NotMapped]
		public int UnPublishedToursUpdatedCnt { get; set; }

		[NotMapped]
		public string DeparturePlace { get; set; }
		[NotMapped]
		public string ArrivalPlace { get; set; }

	}
}
