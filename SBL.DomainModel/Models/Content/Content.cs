﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Content
{
    [Serializable]
    [Table("tContent")]
   public class Content
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContentID { get; set; }

        [Required]
        public string ContentTitle { get; set; }

        public string ContentLocalTitle { get; set; }

        public string ContentDescription { get; set; }

        public string ContentLocalDescription { get; set; }

       // public string Images { get; set; }

        public byte Status { get; set; }

        public int CategoryID { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public DateTime StartPublish { get; set; }

        public DateTime FinishPublish { get; set; }

        public int Ordering { get; set; }

        public int Hits { get; set; }

      //  public string FileName { get; set; }
        public string Attachments { get; set; }
        public byte IsMultiple { get; set; }

        [NotMapped]
        public string CategoryTypeName { get; set; }
        
    }
}
