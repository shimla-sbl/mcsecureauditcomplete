﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Pension
{
    [Serializable]
    [Table("pensionerDetails")]
    public class PensionerDetails
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoID{get;set;}
        public string pensionerID{get;set;}
        public string pensionerName{get;set;}
        public int memberCode{get;set;}
        public string pPONO{get;set;}
        public string nameOfTreasury { get; set; }
        public string bankName { get; set; }
        public int lastAssemblyID { get; set; }
        public bool isActiveMember { get; set; }
        public DateTime firstTermStrat { get; set; }
        public DateTime firstTermEnd { get; set; }
        public int noOfYears { get; set; }
        public int extrayearfT { get; set; }
        public int isPensioner { get; set; }
        public int isPensionerAlive { get; set; }
        public int isNomneeActivate { get; set; }


    }
}
