﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Pension
{
    [Serializable]
    [Table("pensionerTenuresDetails")]
  public  class pensionerTenuresDetails
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int tenuresID { get; set; }

        public int pensionerID{get;set;}

        public DateTime startDate{get;set;}

        public DateTime endDate{get;set;}

        public int years{get;set;}


    }
}
