﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using SBL.DomainModel.Models.UserModule;
using System.Threading.Tasks;
using SBL.DomainModel.Models.User;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.UserModule
{
    [Serializable]
    public class tUserAccessActionsModel
    {
        public tUserAccessActionsModel()
        {


        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserAccessActionsId { get; set; }


        public int UserTypeID { get; set; }

        [NotMapped]
        public string UserTypeName { get; set; }

        public int ModuleId { get; set; }

        [NotMapped]
        public string ModuleName { get; set; }

        public int SubUserTypeID { get; set; }

        public string SubUserTypeName { get; set; }

        public int ActionId { get; set; }

        //public bool IsApprove { get; set; }

        [NotMapped]
        public string ActionName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }
        [NotMapped]
        public List<tUserAccessActions> SubAccessList { get; set; }
        // [NotMapped]
        //public SelectList UserTypeName1 { get; set; }
        [NotMapped]
        public List<tUserAccessActionsModel> objModelList { get; set; }

        [NotMapped]
        public List<tUserAccessActions> objList { get; set; }
        [NotMapped]
        public List<tUserAccessActions> AccessList { get; set; }

        public List<tUserAccessActions> SubUserTypeList { get; set; }
        
       
        [NotMapped]
        public List<KeyValuePair<int, string>> ActionListKey { get; set; }
        [NotMapped]
        public List<mUserType> mUserTypeList { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public List<string> ActionIds { get; set; }
        [NotMapped]
        public string ActionComma { get; set; }

        public string MergeActionId { get; set; }
        public string MergeSubModuleId { get; set; }
        public virtual ICollection<tUserAccessActions> objAccActionList { get; set; }

        public List<tUserAccessActions> ActionList { get; set; }

        [NotMapped]
        public List<string> AllMergeSubModuleIds { get; set; }

      
        public int? AccessActionOrder { get; set; }
    }
}
