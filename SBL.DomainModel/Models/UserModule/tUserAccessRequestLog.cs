﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.User;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.UserModule
{
    [Table("tUserAccessRequestLog")]
    [Serializable]
    public partial class tUserAccessRequestLog
    {
        public tUserAccessRequestLog()
        {


        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public Guid UserID { get; set; }
        public int AccessID { get; set; }
        //public string AccessIDs { get; set; }
        public string MergeActionId { get; set; }
        public string ActionControlId { get; set; }
        public DateTime? IsAcceptedDate { get; set; }
        public DateTime? IsRejectedDate { get; set; }

        public int DomainAdminstratorId { get; set; }
        public int? TypedDomainId { get; set; }
        public int? SubUserTypeID { get; set; }
        public string AssociateDepartmentId { get; set; }
        public int DesignationId { get; set; }
        public int MemberId { get; set; }
        public int SecreataryId { get; set; }
        public int? HODId { get; set; }
        public DateTime? Modifiedwhen { get; set; }
        public string ModifiedBy { get; set; }

        [NotMapped]
        public string ModuleName { get; set; }
        [NotMapped]
        public string ActionName { get; set; }

        [NotMapped]
        public string UserName { get; set; }

        [NotMapped]
        public List<KeyValuePair<int, string>> ActionListKey { get; set; }

        [NotMapped]
        public List<KeyValuePair<int, string>> ActionListControlId { get; set; }

        [NotMapped]
        public List<KeyValuePair<int, string>> RestActionListControlId { get; set; }

        [NotMapped]
        public string usertypename { get; set; }
        [NotMapped]
        public string domainAdminName { get; set; }
        [NotMapped]
        public List<KeyValuePair<int, string>> SelectActionListKey { get; set; }

    }
}
