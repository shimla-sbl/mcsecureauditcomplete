﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.User;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.UserModule
{


    [Table("tUserAccessActions")]
    [Serializable]
    public partial class tUserAccessActions
    {
        public tUserAccessActions()
        {


        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserAccessActionsId { get; set; }


        public int UserTypeID { get; set; }

        public int SubUserTypeID { get; set; }

        [NotMapped]
        public string UserTypeName { get; set; }

        [NotMapped]
        public string SubUserTypeName { get; set; }

        public int ModuleId { get; set; }

        [NotMapped]
        public string ModuleName { get; set; }



        public int ActionId { get; set; }

        //public bool IsApprove { get; set; }
        public int? AccessActionOrder { get; set; }

        [NotMapped]
        public string ActionName { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        // [NotMapped]
        //public SelectList UserTypeName1 { get; set; }
        [NotMapped]
        public string SubModuleName { get; set; }
        [NotMapped]
        public int SubModuleId { get; set; }
        [NotMapped]
        public string SubModuleComma { get; set; }
        [NotMapped]
        public List<tUserAccessActions> objList { get; set; }

        [NotMapped]
        public List<tUserAccessActionsModel> objModelList { get; set; }

        [NotMapped]
        public List<tUserAccessActions> AccessList { get; set; }

        [NotMapped]
        public List<tUserAccessActions> ActionList { get; set; }

        [NotMapped]
        public List<tUserAccessActions> SubUserTypeList { get; set; }

        [NotMapped]
        public List<mUserType> mUserTypeList { get; set; }
        [NotMapped]
        public List<tUserAccessActions> SubAccessList { get; set; }
        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public List<string> ActionIds { get; set; }
        [NotMapped]
        public List<string> SubModuleIds { get; set; }
        [NotMapped]
        public string ActionComma { get; set; }

        public string MergeActionId { get; set; }
        public string MergeSubModuleId { get; set; }
        [NotMapped]
        public virtual ICollection<tUserAccessActions> objAccRejList { get; set; }

        [NotMapped]
        public List<KeyValuePair<int, string>> ActionListKey { get; set; }
        [NotMapped]
        public List<string> AllMergeSubModuleIds { get; set; }
    }
}
