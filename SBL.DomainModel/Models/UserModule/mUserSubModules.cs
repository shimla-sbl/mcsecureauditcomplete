﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.User;

namespace SBL.DomainModel.Models.UserModule
{
    [Table("mUserSubModules")]
    [Serializable]
    public partial class mUserSubModules
    {
        public mUserSubModules()
        {

            this.objList = new List<mUserSubModules>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubModuleId { get; set; }

        public int ModuleId { get; set; }

        [StringLength(250)]
        public string SubModuleName { get; set; }

        [StringLength(250)]
        public string SubModuleNameLocal { get; set; }

        [NotMapped]
        public string ModuleName { get; set; }

        [StringLength(500)]
        public string SubModuleDescription { get; set; }

        [StringLength(500)]
        public string SubModuleFunctionName { get; set; }

        public bool IsAdminEnable { get; set; }


        public bool Isactive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public int? SubModuleOrder { get; set; }

        [NotMapped]
        public int? ModuleOrderTemp { get; set; }
        //[NotMapped]
        //public virtual List<mUserModules> ListmRoles { get; set; }

        [NotMapped]
        public List<mUserSubModules> objList { get; set; }



        [NotMapped]
        public List<mUserType> mUserTypeList { get; set; }

        [NotMapped]
        public List<mUserSubModules> AccessList { get; set; }

        [NotMapped]
        public int UserTypeID { get; set; }

        [NotMapped]
        public string UserTypeName { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public int MemberId { get; set; }
        [NotMapped]
        public string ActionIds { get; set; }

        [NotMapped]
        public string ModuleNameLocal { get; set; }

        [NotMapped]
        public int SubUserTypeID { get; set; }

    }


    [Serializable]
    public class SubModuleOrderList
    {
        public int? _OrderID { get; set; }
        public string _OrderName { get; set; }

    }
}
