﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using SBL.DomainModel.Models.UserModule;
using System.Threading.Tasks;
using SBL.DomainModel.Models.User;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SBL.DomainModel.Models.Role;

namespace SBL.DomainModel.Models.UserModule
{
    [Serializable]
    public class tUserAccessRequestModel
    {
        public tUserAccessRequestModel()
        {


        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public Guid UserID { get; set; }
        public int AccessID { get; set; }
        //public string AccessIDs { get; set; }

        public string ActionControlId { get; set; }
        public DateTime? IsAcceptedDate { get; set; }
        public DateTime? IsRejectedDate { get; set; }

        public int DomainAdminstratorId { get; set; }
        public int TypedDomainId { get; set; }
        public string AssociateDepartmentId { get; set; }
        public int DesignationId { get; set; }
        public int MemberId { get; set; }
        public int SecreataryId { get; set; }

        public DateTime? Modifiedwhen { get; set; }
        public string ModifiedBy { get; set; }

        //  public List<tUserAccessRequestModel> objAccRejList { get; set; }


        [NotMapped]
        public string ModuleName { get; set; }
        [NotMapped]
        public string ActionName { get; set; }

        [NotMapped]
        public string CheckedActionName { get; set; }

        public SelectList FullName { get; set; }

        public List<mUsers> obUserList { get; set; }

        public virtual ICollection<mUsers> mUsersList { get; set; }

        public virtual ICollection<tUserAccessRequest> objAccRejList { get; set; }

        [NotMapped]
        public List<tUserAccessActions> UserAsscessActionlist { get; set; }

        public virtual ICollection<tUserAccessRequest> objactionList { get; set; }
        [NotMapped]
        public List<KeyValuePair<string, string>> DeptList { get; set; }
        [NotMapped]
        public List<KeyValuePair<string, string>> AdditionalDeptList { get; set; }
        public List<mUsers> List { get; set; }
        public List<mRoles> RoleListByType { get; set; }
        [NotMapped]
        public List<KeyValuePair<int, string>> ActionListKey { get; set; }
        [NotMapped]
        public List<KeyValuePair<string, string>> RoleKeyList { get; set; }

        [NotMapped]
        public string AssociatedDepartmentName { get; set; }
        [NotMapped]
        public string Deptid { get; set; }
        [NotMapped]
        public string RequestedDeptList { get; set; }
        [NotMapped]
        public string ApprovedDeptList { get; set; }
        [NotMapped]
        public string RequestedAdditionalDeptList { get; set; }
        [NotMapped]
        public string ApprovedAdditionalDeptList { get; set; }
        [NotMapped]
        public List<mRoles> AssignedRollListByUserType { get; set; }
    }
}
