﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.User;

namespace SBL.DomainModel.Models.UserModule
{
    [Table("mUserModules")]
    [Serializable]
    public partial class mUserModules
    {
        public mUserModules()
        {

            this.objList = new List<mUserModules>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int  ModuleId { get; set; }


        public int SubUserTypeID { get; set; }

        [NotMapped]
        public string SubUserTypeName { get; set; }

        [StringLength(250)]
        public string ModuleName { get; set; }

        [StringLength(250)]
        public string ModuleNameLocal { get; set; }

        [StringLength(500)]
        public string ModuleDescription { get; set; }

        [StringLength(500)]
        public string ModuleFunctionName { get; set; }

        public bool Isactive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }


        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public int? ModuleOrder { get; set; }
        
        [NotMapped]
        public int? ModuleOrderTemp { get; set; }
        //[NotMapped]
        //public virtual List<mUserModules> ListmRoles { get; set; }

        [NotMapped]
        public List<mUserModules> objList { get; set; }

         [NotMapped]
        public List<mUserModules> SubUserTypeList { get; set; }
        

        [NotMapped]
        public List<mUserType> mUserTypeList { get; set; }

        [NotMapped]
        public int UserTypeID { get; set; }

        [NotMapped]
        public string UserTypeName { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        [NotMapped]
        public string ActionIds { get; set; }

        [NotMapped]
        public int? MemberId { get; set; }
        [NotMapped]
        public int TotalCount { get; set; }
        
    }

    [Serializable]
    public class ModuleOrderList
    {
        public int? _OrderID { get; set; }
        public string _OrderName { get; set; }

    }
}
