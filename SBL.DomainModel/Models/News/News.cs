﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.News
{
    [Serializable]
    [Table("tLatestNews")]
    public class mNews
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int LatestNewsID { get; set; }

        [Required]
        public string NewsTitle { get; set; }

        public string LocalNewsTitle { get; set; }

        public string NewsDescription { get; set; }

        public string LocalNewsDescription { get; set; }

        public string Images { get; set; }

        public byte Status { get; set; }

        public int CategoryID { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public DateTime StartPublish { get; set; }

        public DateTime FinishPublish { get; set; }

        public int Ordering { get; set; }

        public int Hits { get; set; }

        public string FileName { get; set; }

        public DateTime? NewsEventDate { get; set; }


        [NotMapped]
        public string CategoryTypeName { get; set; }
        [NotMapped]
        public string ImageAcessurl { get; set; }
        public string ThumbName { get; set; }
        //public string FilePath { get; set; }
        //[NotMapped]
        public string Images1 { get; set; }
        //[NotMapped]
        public string FileName1 { get; set; }

        [NotMapped]
        public string ImageAcessurl1 { get; set; }
        //[NotMapped]
        public string ThumbName1 { get; set; }

        [NotMapped]
        public string FileAcessPath { get; set; }
        public bool isFlashNews
        {

            get;
            set;
        }

    }




    

}
