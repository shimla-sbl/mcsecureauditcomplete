﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.tWebCasting
{
    [Table("tWebCasting")]
    [Serializable]
    public class tWebCasting
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WebCastingId { get; set; }
        public string WebCastingText { get; set; }

        public string WebCastingUrl { get; set; }

        public string StateName { get; set; }

        public bool? IsActive { get; set; }
    }
}
