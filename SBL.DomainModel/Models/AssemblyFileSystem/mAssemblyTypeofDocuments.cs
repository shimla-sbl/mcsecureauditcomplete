﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.AssemblyFileSystem
{
    [Table("mAssemblyTypeofDocuments")]
    [Serializable]
    public class mAssemblyTypeofDocuments
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TypeofDocumentId { get; set; }

        public string TypeofDocumentName { get; set; }

        public string Description { get; set; }

        public bool? IsActive { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedWhen { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public bool? IsDeleted { get; set; }

    }
}
