﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.AssemblyFileSystem
{
    [Serializable]
    public class AssemblySessionFilePath
    {
        public bool IsExist { get; set; }

        public string FilePath { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowCutMotions { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowLOB { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowUnQuestion { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowSQuestion { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowBillPassedorIntroduced { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowBreifOfProceeding { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowProvisonalCalender { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowRotationalMinister { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath ShowHouseProceeding { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath StarredQuestionForm { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath PostUponQuestion { get; set; }
        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public AssemblySessionFilePath UnStarredQuestionForm { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public bool IsShowPaid { get; set; }
    }
}
