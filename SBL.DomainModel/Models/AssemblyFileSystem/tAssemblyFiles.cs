﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.AssemblyFileSystem
{
    [Table("tAssemblyFiles")]
    [Serializable]
    public class tAssemblyFiles
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AssemblyFileId { get; set; }

        [Required]
        public int AssemblyId { get; set; }

        [Required]
        public int SessionId { get; set; }

        public int? SessionDateId { get; set; }

        public int? TypeofDocumentId { get; set; }

        public string Description { get; set; }

        public string UploadFile { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public bool Status { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string ModifiedBy { get; set; }

        [NotMapped]
        public string AssemblyName { get; set; }

        [NotMapped]
        public string SessionName { get; set; }

        [NotMapped]
        public string SessionDate { get; set; }

        [NotMapped]
        public string AssemblyDocTypeName { get; set; }

        [NotMapped]
        public string FileAcessPath { get; set; }

    }
}
