﻿namespace SBL.DomainModel.Models.Language
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    #endregion

    [Serializable]
    [Table("LanguageText")]
    public partial class LanguageText
    {
        [Key, Column(Order = 1)]
        public System.Guid TextId { get; set; }

        [Key, Column(Order = 2)]
        public System.Guid LanguageId { get; set; }

        public string TextValue { get; set; }
        public int ID { get; set; }
        [Column(TypeName = "Varchar(MAX)")]
        public string PageName { get; set; }
    }
}
