﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Promotion
{
    [Serializable]

    [Table("tLeaveDetail")]
    public partial class tLeaveDetail
    {

        public tLeaveDetail()
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int LeaveID { get; set; }


        public int NumofDays { get; set; }

        public string LeaveType { get; set; }


        public string From { get; set; }
        public string To { get; set; }

        public int? StaffID { get; set; }

        [NotMapped]
        public string Mode { get; set; }


    }
}
