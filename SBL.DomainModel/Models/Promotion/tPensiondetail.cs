﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Promotion
{
    [Serializable]
    [Table("tPensiondetail")]
    public class tPensiondetail
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)] 
        public int PensionId { get; set; }

        public int StaffID { get; set; }

        public string StaffName { get; set; }

        [Display(Name = "Date of Submission")]
        public DateTime? DateofSubmission { get; set; }

        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "PostOffice")]
        public string PostOffice { get; set; }

        [Display(Name = "District")]
        public string District { get; set; }

        [Display(Name = "Pin Code")]
        public int PinCode { get; set; }

        [Display(Name = "State")]
        public string State { get; set; }

        [Display(Name = "Treasury Office Pension")]
        public string TreasuryPesnsionOffice { get; set; }

        [Display(Name = "Treasury Office Gratuity")]
        public string TreasuryGratuityOffice { get; set; }

        [Display(Name = "Pay Band + Grade Pay")]
        public string BandandGradePay { get; set; }

        [Display(Name = "Date of Retirement")]
        public DateTime? DateofRetirement { get; set; }

        [Display(Name = "Date of Death")]
        public DateTime? DateofDeath { get; set; }

        [Display(Name = "Forcefully Retire..")]
        public DateTime? ForcefullyRetirement { get; set; }

        [Display(Name = "Date of Commencement")]
        public DateTime? DateofCommencement { get; set; }

        [Display(Name = "Husband/Spouse/Others")]
        public string SpouseName { get; set; }

        [Display(Name = "FamilyPensionerDOB")]
        public DateTime? DobFamilyPensioner { get; set; }

        [Display(Name = "Medical Certificate")]
        public DateTime? DateOfMedicalCerti { get; set; }

        [Display(Name = "Lodging FIR ")]
        public DateTime? DateOfLodgingFir { get; set; }

        [Display(Name = "Foreign services")]
        public DateTime? Foreignservices { get; set; }

        [Display(Name = "Wheather Contri..")]
        public string WheatherContribution { get; set; }

        [Display(Name = " military service ")]
        public string militaryservice { get; set; }

        [Display(Name = "Gross Service")]
        public string GrossService { get; set; }

        [Display(Name = "NonQualifyingservice")]
        public string NonQualifyingservice { get; set; }

        [Display(Name = "NetQualifyingservice")]
        public string NetQualifyingservice { get; set; }

        [Display(Name = "Last pay drawn")]
        public double? Lastpaydrawn { get; set; }

    }
}
