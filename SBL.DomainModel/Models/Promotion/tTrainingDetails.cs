﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Promotion
{
    [Serializable]

    [Table("tTrainingDetails")]
   public partial class tTrainingDetails
   {
        public tTrainingDetails()
        {

        }


        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int TrainingID { get; set; }

        public string TrainingType { get; set; }
        public string Subject { get; set; }

        public string From { get; set; }
        public string To { get; set; }

        public int? StaffID { get; set; }

        [NotMapped]
        public string Mode { get; set; }
   }
}
