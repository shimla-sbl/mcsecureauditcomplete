﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Promotion
{
    [Serializable]
    [Table("tPromotions")]
   public partial class tPromotions
   {
        public tPromotions()
        {

        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public int  PromotionID {get;set;}


        public string OrderNumber { get; set; }
        
        [StringLength(200)]
        public string Designation { get; set; }
        [Display(Name = "Date of Joining")]
        public string DOJ { get; set; }
        [StringLength(200)]
        public string Branch { get; set; }
        [StringLength(200)]
        public string PayScale { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public int StaffID { get; set; }

        [NotMapped]
        public string Mode { get; set; }

         


   }
}
