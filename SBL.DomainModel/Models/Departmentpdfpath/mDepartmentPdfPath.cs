﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Departmentpdfpath
{
    [Serializable]
    [System.ComponentModel.DataAnnotations.Schema.Table("mDepartmentPdfPath")]
    public partial class mDepartmentPdfPath
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public string deptId { get; set; }
        public string deptname { get; set; }
        public string StarredBeforeFixPath { get; set; }
        public string UnStarredBeforeFixPath { get; set; }
        public DateTime? SLastUpdatedDate { get; set; }
        public DateTime? UnSLastUpdatedDate { get; set; }
    }
}
