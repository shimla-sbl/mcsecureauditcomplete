﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.RoadPermit
{
    [Table("mSealedRoads")]
    [Serializable]
    public class mSealedRoads
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SlNo { get; set; }

        public string RoadCode { get; set; }

        public string Description { get; set; }
    }
}
