﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace SBL.DomainModel.Models.RoadPermit
{
    [Table("mPermit")]
    [Serializable]
    public class mPermit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermitNo { get; set; }

        public string PermitCode { get; set; }

        public int MemberCode { get; set; }

        public string VehicleNo { get; set; }

        public string FileName { get; set; }

        public int AssemblyId { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public DateTime? CancelledDate { get; set; }
        [DefaultValue("false")]
        public bool IsCanceled { get; set; }

        public string Remarks { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }
           [NotMapped]
        public virtual ICollection<tPermitRestrictedRoads> tPermitRestrictedRoads { get; set; }
           [NotMapped]
        public virtual ICollection<tPermitSealedRoads> tPermitSealedRoads { get; set; }

        [NotMapped]
        public string MemberName { get; set; }
        [NotMapped]
        public string Prefix { get; set; }
        [NotMapped]
        public string Address { get; set; }
        [NotMapped]
        public string Designation { get; set; }
        [NotMapped]
        public string ConstituencyName { get; set; }

        public int DesignationCode { get; set; }

        public int ConstituencyCode { get; set; }



    }
}
