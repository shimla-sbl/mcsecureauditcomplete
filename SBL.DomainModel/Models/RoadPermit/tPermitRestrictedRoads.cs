﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.RoadPermit
{
    [Table("tPermitRestrictedRoads")]
    [Serializable]
    public class tPermitRestrictedRoads
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SlNo { get; set; }

        public int PermitNo { get; set; }

        public string PermitCode { get; set; }

        public string RoadCode { get; set; }
        [NotMapped]
        public string Description { get; set; }
    }
}
