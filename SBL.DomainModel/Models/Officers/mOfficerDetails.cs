﻿namespace SBL.DomainModel.Models.Officers
{
    #region Namespace Reffrences
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    #endregion

    [Table("mOfficerDetails")]
    [Serializable]
    public class mOfficerDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OfficerID { get; set; }

        public string Prefix { get; set; }

        public string Name { get; set; }

        public string Designation { get; set; }

        public string Address { get; set; }

        public int OfficerCode { get; set; }

        public bool IsActive { get; set; }
    }
}
