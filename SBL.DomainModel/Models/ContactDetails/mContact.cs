﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.ContactDetails
{
    [Table("mContact")]
    [Serializable]
   public class mContact
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ContactId { get; set; }
        [Required]
        public int CategoryId { get; set; }

        public string Designation { get; set; }

        public string Name { get; set; }

        public string RoomNo { get; set; }

        public string TelephoneOfficeNo { get; set; }

        public string EPABX  { get; set; }

        public string TelephoneResidenceNo { get; set; }

        public string Mobile { get; set; }

        public string FaxNo { get; set; }

        public string Email { get; set; }


    }
}
