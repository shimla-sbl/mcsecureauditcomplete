﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.OldSalary
{
    [Table("MlaAllowances")]
    [Serializable]
    public class MlaAllowances_old
    {
    [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoID { get; set; }
        public string MlaCode { get; set; }
        public string MonthYear { get; set; }
        public string ConstituencyCode { get; set; }
        public string DesignationFlag { get; set; }
        public string BasicPay { get; set; }
        public string SumptuaryAllowance { get; set; }
        public string HaltingAllowance { get; set; }
        public string CompensatoryAllowance { get; set; }
        public string ConstituencyAllowance { get; set; }
        public string TelephoneAllowance { get; set; }
        public string OfficeAllowance { get; set; }
        public string OtherAllowance { get; set; }
        public string HraDeduction { get; set; }
        public string MiscDeduction { get; set; }
        public string OtherDeduction { get; set; }
        public string DeductionDays { get; set; }
        public string ProfTax { get; set; }
    }

    [Table("MlaAdvances")]
    [Serializable]
    public class MlaAdvances_old
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoID { get; set; }
        public string MlaCode { get; set; }
        public string MonthYear { get; set; }
        public string LoanType { get; set; }
        public string TotalAmount { get; set; }
        public string InstallmentAmount { get; set; }
        public string InstallmentNo { get; set; }
        public string AmountPaid { get; set; }
        public string BalanceAmount { get; set; }
        public string InterestAmount { get; set; }
        public string Rate { get; set; }
        public string CumulativeInt { get; set; }
        public string LoanNo { get; set; }
        public string TotalInst { get; set; }
    }

    [Table("MlaDir")]
    [Serializable]
    public class MlaDir_old
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AutoID { get; set; }
        public string MlaCode { get; set; }
        public string ConstituencyCode { get; set; }
        public string Name { get; set; }
        public string DesignationFlag { get; set; }
        public string HBANO { get; set; }
        public string MCANO { get; set; }
        public string BankAccountNo { get; set; }
        public string Address { get; set; }
    }

     [Serializable]
    public class oldSalaryViewModel
    {
         public double totalAllowances { get; set; }
         public double totalDeduction { get; set; }
         public double total { get; set; }
        public MlaAllowances_old MlaAllowances_old { get; set; }

        public List<MlaAdvances_old> MlaAdvances_old { get; set; }

        public  MlaDir_old MlaDir_old { get; set; }
    }
}