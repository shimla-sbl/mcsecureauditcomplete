﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Constituency
{
    [Serializable]
    [Table("mUnitContituency")]
    public class mUnitContituency
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConstituencyID { get; set; }
       
        public int ConstituencyCode { get; set; }

        public int UnitID { get; set; }

        public string ConstituencyName { get; set; }

        public string ConstituencyNameLocal { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

    }
}
