﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Constituency
{
    [Serializable]
    [Table("mBlock")]
    public partial class mBlock
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int BlockCode { get; set; }

        public int? DisttCode { get; set; }

        public int? ConstituencyCode { get; set; }

        public string BlockName { get; set; }

        public string BlockNameLocal { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }
    }
}
