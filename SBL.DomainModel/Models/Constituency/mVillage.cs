﻿namespace SBL.DomainModel.Models.Constituency
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("mVillage")]
    public partial class mVillage
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int VillageID { get; set; }

        //[Key]
        //[Column(Order = 1)]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VillageCode { get; set; }

        public string VillageName { get; set; }
        public string VillageNameLocal { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsDeleted { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedWhen { get; set; }



        //For Paging

        [NotMapped]
        public int ClickCount { get; set; }

        [NotMapped]
        public List<mVillage> AllList { get; set; }
        [NotMapped]
        public string SearchText { get; set; }

        [NotMapped]
        public List<mVillage> VillageList { get; set; }

        [NotMapped]
        public int PAGE_SIZE { get; set; }

        [NotMapped]
        public int PageIndex { get; set; }

        [NotMapped]
        public int ResultCount { get; set; }

        [NotMapped]
        public int loopStart { get; set; }

        [NotMapped]
        public int loopEnd { get; set; }

        [NotMapped]
        public int selectedPage { get; set; }

        [NotMapped]
        public int PageNumber { get; set; }

        [NotMapped]
        public int RowsPerPage { get; set; }
        [NotMapped]
        public bool HasPreviousPage { get; set; }

        [NotMapped]
        public bool HasNextPage { get; set; }

    }
}
