﻿namespace SBL.DomainModel.Models.Constituency
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("mConstituency")]
    public partial class mConstituency
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConstituencyID { get; set; }

        public int? AssemblyID { get; set; }

        public int ConstituencyCode { get; set; }

        [Required]
        [StringLength(150)]
        public string ConstituencyName { get; set; }

        [StringLength(150)]
        public string ConstituencyName_Local { get; set; }

        //[StringLength(2)]
        public int? DistrictCode { get; set; }

        public string LGDistrictCode { get; set; }

        [StringLength(2)]
        public string ConstituencyCode_CmRefnic { get; set; }

        public DateTime? ConstituencyCreatedDate { get; set; }

        public bool? Active { get; set; }
        public bool? IsDeleted { get; set; }

        [StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedDate { get; set; }

        public int? ReservedCategory { get; set; }

        [NotMapped]
        public string GetReservedCategoryName { get; set; }

        [NotMapped]
        public string GetAssemblyName { get; set; }

        [NotMapped]
        public string GetDistrictName { get; set; }

        [NotMapped]
        public int? toAssemblyID { get; set; }

        //For Paging

        [NotMapped]
        public int ClickCount { get; set; }

        [NotMapped]
        public List<mConstituency> AllList { get; set; }

        [NotMapped]
        public string SearchText { get; set; }

        [NotMapped]
        public List<mConstituency> ConstituencyList { get; set; }

        [NotMapped]
        public int PAGE_SIZE { get; set; }

        [NotMapped]
        public int PageIndex { get; set; }

        [NotMapped]
        public int ResultCount { get; set; }

        [NotMapped]
        public int loopStart { get; set; }

        [NotMapped]
        public int loopEnd { get; set; }

        [NotMapped]
        public int selectedPage { get; set; }

        [NotMapped]
        public int PageNumber { get; set; }

        [NotMapped]
        public int RowsPerPage { get; set; }

        [NotMapped]
        public bool HasPreviousPage { get; set; }

        [NotMapped]
        public bool HasNextPage { get; set; }
    }
}