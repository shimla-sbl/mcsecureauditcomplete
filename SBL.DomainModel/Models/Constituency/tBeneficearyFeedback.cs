﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.Constituency
{
    [Serializable]
    [Table("tBeneficearyFeedback")]
    public class tBeneficearyFeedback
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int BeneficearySchemeTypeId { get; set; }
        public string BeneficearyName { get; set; }
        public string BeneficearyAdress { get; set; }
        public string Sex { get; set; }
        public string BeneficearyMobile { get; set; }
        public DateTime? BenefitDate { get; set; }
        public string BeneficearyComponent { get; set; }
        public string BeneficearyAmount { get; set; }

        public int BlockCode { get; set; }
        public int PanchayatCode { get; set; }
        public int ConstituencyCode { get; set; }

        public DateTime? FeedbackDate { get; set; }
        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string FeedBackDetails { get; set; }

        public int ActionTypeCode { get; set; }
        public DateTime? ActionDate { get; set; }
        public string ActionDetails { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }

       [NotMapped]
       public string BeneficearySchemeTypeName { get; set; }
       [NotMapped]
       public string BlockName { get; set; }
       [NotMapped]
       public string PanchayatName { get; set; }
       [NotMapped]
       public string ConstituencyName { get; set; }
       [NotMapped]
       public string ActionTypeName { get; set; }

       [NotMapped]
       public string SrNo { get; set; }
       [NotMapped]
       public List<tBeneficearyFeedback> tBeneficearyModalList { get; set; }
       [NotMapped]
       public string BenefitDateString { get; set; }
       [NotMapped]
       public string FeedbackDateString { get; set; }
       [NotMapped]
       public string ActionDateString { get; set; }

       //Paging Variables
       [NotMapped]
       public int ResultCount { get; set; }
       [NotMapped]
       public int RowsPerPage { get; set; }
       [NotMapped]
       public int selectedPage { get; set; }
       [NotMapped]
       public int loopStart { get; set; }
       [NotMapped]
       public int loopEnd { get; set; }
       [NotMapped]
       public int PageNumber { get; set; }

     
    }

    public class tempViewModel
    {
        public List<tBeneficearyFeedback> tBeneficearyFeedbackList { get; set; }
        
        public int ResultCount { get; set; }

        public int RowsPerPage { get; set; }

        public int selectedPage { get; set; }

        public int loopStart { get; set; }

        public int loopEnd { get; set; }

        public int PageNumber { get; set; }
        public int SrNo { get; set; }

    }
}
