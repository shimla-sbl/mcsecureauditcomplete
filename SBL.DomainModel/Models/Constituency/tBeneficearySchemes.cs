﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;   

namespace SBL.DomainModel.Models.Constituency
{
     [Serializable]
     [Table("tBeneficiarySchemes")]
    public class tBeneficiarySchemes
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string SchemeName { get; set; }

        public string SchemeNameLocal { get; set; }

        public string IconPath { get; set; }

        public string SchemeNameFull { get; set; }

        public string SchemeNameFullLocal { get; set; }

        public bool? IsActive { get; set; }

        public string DeptId { get; set; }
    }
}
