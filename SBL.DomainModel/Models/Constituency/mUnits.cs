﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Constituency
{
    [Serializable]
    [Table("mUnits")]
     public class mUnits
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UnitsID { get; set; }

        public int CountryID { get; set; }

        public int StateID { get; set; }

        public int UnitTypeID { get; set; }

        public string UnitName { get; set; }

        public string UnitNameLocal { get; set; }

        public string UnitImage { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }

    }
}
