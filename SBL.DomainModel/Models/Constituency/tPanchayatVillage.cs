﻿namespace SBL.DomainModel.Models.Constituency
{
    using SBL.DomainModel.Models.District;
    using SBL.DomainModel.Models.States;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("tPanchayatVillage")]
    public partial class tPanchayatVillage
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PanchayatVillageID { get; set; }      
        public int? StateCode { get; set; }  
        public int? DistrictCode { get; set; }
        public int? PanchayatCode { get; set; }
        public int? VillageCode { get; set; }       
        public bool? IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedWhen { get; set; }
        public bool? IsDeleted { get; set; }
        [NotMapped]
        public string GetStateName { get; set; }
        [NotMapped]
        public string GetDistrictName { get; set; }
        [NotMapped]
        public string GetPanchayatName { get; set; }
        [NotMapped]
        public string GetVillageName { get; set; }


        //For Paging



        [NotMapped]
        public int ClickCount { get; set; }


        [NotMapped]
        public string SearchText { get; set; }

        [NotMapped]
        public List<tPanchayatVillage> PanchayatVillageList { get; set; }

        [NotMapped]
        public int PAGE_SIZE { get; set; }

        [NotMapped]
        public int PageIndex { get; set; }

        [NotMapped]
        public int ResultCount { get; set; }

        [NotMapped]
        public int loopStart { get; set; }

        [NotMapped]
        public int loopEnd { get; set; }

        [NotMapped]
        public int selectedPage { get; set; }

        [NotMapped]
        public int PageNumber { get; set; }

        [NotMapped]
        public int RowsPerPage { get; set; }
        [NotMapped]
        public bool HasPreviousPage { get; set; }

        [NotMapped]
        public bool HasNextPage { get; set; }


    }
}
