﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Constituency
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Serializable]
    [Table("tConstituencyPanchayat")]
    public partial class tConstituencyPanchayat
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ConstituencyPanchayatID { get; set; }
        public int StateCode { get; set; }
        public int  ConstituencyCode { get; set; }
        public int DistrictCode { get; set; }
        public int BlockCode { get; set; }  
        public int PanchayatCode { get; set; }        
        public bool IsActive { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedWhen { get; set; }
        [NotMapped]
       
        public List<mPanchayat> MenuList { get; set; }
        [NotMapped]
        public string Ids { get; set; }
        [NotMapped]
        public string Names { get; set; }
        [NotMapped]
        public List<int> GetId { get; set; }
       
    }
}
