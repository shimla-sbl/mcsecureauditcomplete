﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.Constituency
{
    [Serializable]
    [Table("tMemberHelpline")]
    public class tMemberHelpline
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
     
        public string ApplicantName { get; set; }
        public string ApplicantAddress { get; set; }
        public string ApplicantMobile { get; set; }
        public string Sex { get; set; }
       
        public DateTime? CallingDate { get; set; }
        public string CallDetails { get; set; }

        public int SectorId { get; set; }
        public int BlockCode { get; set; }
        public int PanchayatCode { get; set; }
        public int ConstituencyCode { get; set; }

       
      
        public int ActionTypeCode { get; set; }
        public DateTime? ActionDate { get; set; }
        public string ActionDetails { get; set; }

        public DateTime? CreatedDate { get; set; }


        [NotMapped]
        public string SrNo { get; set; }
        [NotMapped]
        public string SectorName { get; set; }
        [NotMapped]
        public string BlockName { get; set; }
        [NotMapped]
        public string PanchayatName { get; set; }
        [NotMapped]
        public string ConstituencyName { get; set; }
        [NotMapped]
        public string ActionTypeName { get; set; }
        [NotMapped]
        public string CallingDateString { get; set; }
        [NotMapped]
        public string ActionDateString { get; set; }
        [NotMapped]
        public List<tMemberHelpline> tMemberHelplineModalList { get; set; }


        //Paging Variables
        [NotMapped]
        public int ResultCount { get; set; }
        [NotMapped]
        public int RowsPerPage { get; set; }
        [NotMapped]
        public int selectedPage { get; set; }
        [NotMapped]
        public int loopStart { get; set; }
        [NotMapped]
        public int loopEnd { get; set; }
        [NotMapped]
        public int PageNumber { get; set; }
    }
}
