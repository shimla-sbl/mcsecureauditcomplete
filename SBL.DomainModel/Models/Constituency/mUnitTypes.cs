﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Constituency
{
    [Serializable]
    [Table("mUnitType")]
    public class mUnitsType
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UnitTypeID { get; set; }

        public string UnitTypeName { get; set; }

        public string UnitTypeNameLocal { get; set; }

        public string UnitTypeImagePath { get; set; }

        public bool? IsActive { get; set; }

        public bool? IsDeleted { get; set; }


    }
}
