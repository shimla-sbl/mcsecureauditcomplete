﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    public class MemberSeatMappedData
    {
        public int SeatID { get; set; }
        public decimal? xAxis { get; set; }
        public decimal? yAxis { get; set; }
        public decimal? Angle { get; set; }
        public string IPAddress { get; set; }
        public int? MemberSeat_PK { get; set; }
        public int? MemberID { get; set; }

    }
}
