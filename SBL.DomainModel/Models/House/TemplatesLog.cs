﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{

    [Serializable]
    [Table("TemplatesDescriptions")]

    public class TemplatesDescriptions
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Templates { get; set; }
        public string UserName { get; set; }
        public DateTime EditedTime { get; set; }
        public int Count { get; set; }
        public string Shortkey { get; set; }

    }
}
