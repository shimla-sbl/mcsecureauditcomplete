﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("tBriefofProceedingsHistory")]
   public class ProceedingsHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public DateTime SessionDate { get; set; }
        public string Proceeding { get; set; }
        public string LoggedUser { get; set; }
        public DateTime EditedDate { get; set; }
        public bool IsApprove { get; set; }
    }
}
