﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("MemberAttendance")]
    public class MemberAttendance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AttendanceId { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public int MemberID { get; set; }
        public DateTime AttendanceDt { get; set; }
        public bool OfflineAttendance { get; set; }
        //public DateTime LoginTime { get; set; }

    }
}
