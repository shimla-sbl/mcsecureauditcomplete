﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.House
{
    [Serializable]
    [Table("tHouseProceedingsTimeSlots")]
    public class tHouseProceedingsTimeSlots
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public DateTime SessionDate { get; set; }
        public string TimeSlot { get; set; }
        public DateTime Time { get; set; }
        public string Descriptions { get; set; }
        public int MemberId { get; set; }
        public int mEventId { get; set; }
        [DefaultValue("false")]
        public bool IsAssurances { get; set; }
        [DefaultValue("false")]
        public bool walkout { get; set; }
        [DefaultValue("false")]
        public bool SpeakerDirection { get; set; }
        public string LoggedUser { get; set; }
        public string PageNumbers { get; set; }

        public int? TimeSlotOrderID { get; set; }
        public int? SubEventID { get; set; }
        public string RefNo { get; set; }

    }
}
