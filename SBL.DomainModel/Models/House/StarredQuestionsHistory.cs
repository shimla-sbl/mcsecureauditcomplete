﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("tStarredQuestionsHistory")]
    public class tStarredQuestionsHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public DateTime SessionDate { get; set; }
        public string EventID { get; set; }
        public string QuestionNo { get; set; }
        public string OriginalDescription { get; set; }
        public string EditedDescription { get; set; }
        public string QuestionDetails { get; set; }
        public string AttachmentName { get; set; }
        public bool? IsLaid { get; set; }
        public string LoggedUSer { get; set; }
        public DateTime LoggedTime { get; set; }
        public string AskedBy { get; set; }
        public int? MinisterId { get; set; }
        public int? DepartmentId { get; set; }
        public string RefPageNo { get; set; }
        public int? IsProofReaderApproved { get; set; }
        public int? IsOldData { get; set; }
        public bool IsPostponed { get; set; }
        public bool IsFreeze { get; set; }
    }
}
