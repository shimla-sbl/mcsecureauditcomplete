﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("tSpeakerpadHistory")]
    public class tSpeakerpadHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public DateTime SessionDate { get; set; }
        public string OriginalDescription { get; set; }
        public string EditedDescription { get; set; }
        public string LoggedUSer { get; set; }
        public DateTime LoggedTime { get; set; }
    }
}
