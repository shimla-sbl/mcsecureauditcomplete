﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("eBookLoginLog")]
 public class EbookLoginDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string MemberID { get; set; }
        public string IPAddress { get; set; }
        public string MACAddress { get; set; }
        public bool IsLogin { get; set; }
        public bool IsEnable { get; set; }
              
    }
}
