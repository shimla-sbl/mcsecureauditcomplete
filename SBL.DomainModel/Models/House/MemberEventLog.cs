﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("MemberEventLog")]
    public class MemberEventLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventLogPK { get; set; }
        public int SubEventFK { get; set; }
        public int MemberID { get; set; }
        public DateTime EventStartTime { get; set; }
        public DateTime EventStopTime { get; set; }
        public string EventStatus { get; set; }
        public bool Cancelled { get; set; }
    }
}
