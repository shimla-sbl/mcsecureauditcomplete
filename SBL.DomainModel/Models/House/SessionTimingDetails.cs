﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.House
{
    [Serializable]
    [Table("tSessionTimingDetails")]
    public class SessionTimingDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }
        public DateTime SessionDate { get; set; }
        public string SessionStartTime { get; set; }
        public string SessionEndTime { get; set; }
        public DateTime ? LoggedTime { get; set; }        
    }
}
