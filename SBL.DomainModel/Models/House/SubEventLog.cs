﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("SubEventLog")]
    public class SubEventLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventLogPK { get; set; }
        public int MainEventFK { get; set; }
        public string SubEventID { get; set; }
        public DateTime EventStartTime { get; set; }
        public DateTime? EventStopTime { get; set; }
        public bool? Cancelled { get; set; }
    }
}
