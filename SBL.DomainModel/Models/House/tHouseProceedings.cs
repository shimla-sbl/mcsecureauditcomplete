﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.House
{
    [Serializable]
    [Table("tHouseProceedings")]
    public class tHouseProceedings
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }
        public DateTime SessionDate { get; set; }
        public string TimeSlot { get; set; }
        public DateTime Time { get; set; }
        public int DocumentType { get; set; }
        public string Descriptions { get; set; }
        [DefaultValue("false")]
        public bool IsFreeze { get; set; }
        public DateTime LoggedTime { get; set; }
        public string LoggedUser { get; set; }
        public string SubmitedBy { get; set; }
        [DefaultValue("false")]
        public string ChiefApproved { get; set; }

    }
}
