﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("EvotingResultLog")]
 public  class EvotingResultLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public string EventId { get; set; }
        public DateTime LOBDate { get; set; }
        public string MemberId { get; set; }
        public string PartryId { get; set; }
        public string Result { get; set; }
    }
}
