﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
  public class MemberEventDetails
    {
        public string SubEventID { get; set; }
        public string MemberID { get; set; }
        public string EventStartTime { get; set; }
        public string EventStopTime { get; set; }

    }
}
