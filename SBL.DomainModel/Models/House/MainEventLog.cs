﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("MainEventLog")]
    public class MainEventLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventLogPK { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public DateTime LOBDate { get; set; }
        public string MainEventID { get; set; }
        public DateTime EventStartTime { get; set; }
        public DateTime? EventStopTime { get; set; }
        public bool? Cancelled { get; set; }
    }
}
