﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.House
{
    [Serializable]
    [Table("tHouseProceedingsEditedHistory")]
    public class HouseProceedingsEditedHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 Id { get; set; }       
        public DateTime SessionDate { get; set; }
        public int DescriptionsFK { get; set; }
        public string TimeSlot { get; set; }
        public string Descriptions { get; set; }
        public string LoggedUser { get; set; }
        public string LoggedTime { get; set; }
    }
}
