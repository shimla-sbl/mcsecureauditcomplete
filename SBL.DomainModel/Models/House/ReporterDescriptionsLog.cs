﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("tReporterDescriptionsLog")]

    public class ReporterDescriptionsLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int AssemblyId { get; set; }
        public int SessionId { get; set; }
        public DateTime LOBDate { get; set; }
        public string EventId { get; set; }
        public int MemberId { get; set; }
        public string TimeSlots { get; set; }
        public string FloorVersion { get; set; }
        public string EditedVersion { get; set; }
        public string FinalVersion { get; set; }
        public DateTime MemberStartTime { get; set; }
        public string UserName { get; set; }
        public DateTime EditedDate { get; set; }
        public string FloorVersionPageNumber { get; set; }
        public string EditedVersionPageNumber { get; set; }
        public string FinalVersionPageNumber { get; set; }
        public bool IsReporterapproved { get; set; }
        public bool IsChiefReporterapproved { get; set; }
        public string PdfFilePath { get; set; }
        public string EntryType { get; set; }
        public string RefNo { get; set; }
        public string Subject { get; set; }
        public int? mEventId { get; set; }
        public int? IsProofReaderApproved { get; set; }
        public int? OrderID { get; set; }
        public int? EventLogPK { get; set; }
        public int? QuestionType { get; set; }
        public bool IsFreeze { get; set; }
        public int? SubEventId { get; set; }
        public bool? IsAssuarance { get; set; }

        //Sujeet : Change done after new requirement
        public bool? IsMinistryEntry { get; set; }
        public int? CommitteeId { get; set; }
        public int? MinistryId { get; set; }
        [StringLength(30)]
        public string EditedPageNo { get; set; }


    }
}
