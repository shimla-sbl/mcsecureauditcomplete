﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.House
{

    [Table("tOtherPapers")]
    [Serializable]
    public class tOtherPapers
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PaperId { get; set; }

        
        public int? AssemblyId { get; set; }

        
        public int? SessionId { get; set; }

        
        public DateTime? Date { get; set; }

        
        public string PaperNo { get; set; }

        public string Subject { get; set; }

        public bool? IsFreeze { get; set; }



    }
}
