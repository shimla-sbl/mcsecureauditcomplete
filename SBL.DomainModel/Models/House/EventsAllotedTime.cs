﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel
{
    [Serializable]
    [Table("EventsAllotedTime")]
    public class EventsAllotedTime
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int EventsAllotedTimePK { get; set; }
        public int AssemblyID { get; set; }
        public int SessionID { get; set; }
        public DateTime LOBDate { get; set; }
        public string EventID { get; set; }
        public string AllotedTime { get; set; }
    }
}
