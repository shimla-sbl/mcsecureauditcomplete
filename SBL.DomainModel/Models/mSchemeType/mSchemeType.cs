﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.mSchemeType
{
    [Serializable]
    [Table("SchemeType")]
    public class mSchemeType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int mSchemeId { get; set; }
        [Required]
        public string SchemeName { get; set; }
        public string SchemeNameLocal { get; set; }       
        public string CreatedBy { get; set; }
        public DateTime? CreatedWhen { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }

    }
}
