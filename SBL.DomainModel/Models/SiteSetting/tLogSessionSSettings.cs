﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.SiteSetting
{
    [Serializable]
    [Table("tLogSessionSSettings")]
     public class tLogSessionSSettings
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SettingId { get; set; }
        public int AssemblyCode { get; set; }
        public int SessionCode { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }
    }
}
