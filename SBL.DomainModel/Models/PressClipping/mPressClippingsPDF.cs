﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PressClipping
{
    [Table("mPressClippingsPDF")]
    [Serializable]
    public class mPressClippingsPDF
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PressClipPDFID { get; set; }

        [Required]
       // [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime NewsDate { get; set; }

        //public string PDFId { get; set; }

        public string Category { get; set; }

        public string Title { get; set; }

        [NotMapped]
        public string Mode { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        public string PressClippingPdfPath { get; set; }        

    }
}
