﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PressClipping
{
    [Table("mPressClipping")]
    [Serializable]
    public class mPressClipping
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PressClipID { get; set; }


        [Required]
        public int NewsPaperId { get; set; }
        
        [Required]
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        public DateTime NewsDate { get; set; }

        //[Required]
        public string PageNumber { get; set; }

        [Required]
        public string Subject { get; set; }
        //[Required]
        public string ContentKeywords { get; set; }

        public string ScannedCopyImg { get; set; }

        public string AliasNameOfImg { get; set; }

        public string ContentText { get; set; }

        public string Category { get; set; }

        public bool? Status { get; set; }

        public string ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }

        public DateTime? CreationDate { get; set; }

        public string CreatedBy { get; set; }

        [NotMapped]
        public string NewsPaperName { get; set; }

    }
}
