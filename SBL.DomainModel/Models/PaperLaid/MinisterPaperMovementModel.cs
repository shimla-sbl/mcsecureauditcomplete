﻿using Lib.Web.Mvc.JQuery.JqGrid;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.PaperLaid
{
     [Serializable]
   public class MinisterPaperMovementModel
    {
         public MinisterPaperMovementModel()
        {
            this.SubmittedFileListByPaperLaidId = new List<tPaperLaidTemp>();
            this.ListPaperMovementModel = new List<MinisterPaperMovementModel>();
            this.PaperMovementCollection = new List<MinisterPaperMovementModel>();
        }

         public MinisterPaperMovementModel(MinisterPaperMovementModel model)
        {
            this.PaperLaidId = model.PaperLaidId;
            this.Title = model.Title;
            this.DeptActivePaperId = model.DeptActivePaperId;
            this.actualFilePath = model.actualFilePath;
            this.version = model.version;
            this.FilePath = model.FilePath;
            this.DeptSubmittedDate = model.DeptSubmittedDate;
            this.BusinessType = model.BusinessType;
            this.DeptSubmittedBy = model.DeptSubmittedBy;
            this.Status = model.Status;
            this.PaperLaidIdForCheckBox = Convert.ToString(model.PaperLaidId);
            this.DesireLayingDate = model.DesireLayingDate;
            this.DepartmentSubmittedDate = model.DepartmentSubmittedDate;
            this.evidhanReferenceNumber = model.evidhanReferenceNumber;
        }


        [HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.PaperLaidIdForCheckBoxFormatter")]
        [DisplayName("")]
        public string PaperLaidIdForCheckBox { get; set; }

          
           [DisplayName("Ref.Number")]
           public string evidhanReferenceNumber { get; set; }

        [DisplayName("Subject")]
        public string Title { get; set; }

        [DisplayName("Business Type")]
        public string BusinessType { get; set; }

        [DisplayName("Other Papers Sent")]
        [JqGridColumnLayout(Alignment = JqGridAlignments.Center)]
        [JqGridColumnFormatter("$.OtherpaperSentFormatter")]
        public string actualFilePath { get; set; }


        [HiddenInput(DisplayValue = false)]
        public int? version { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FilePath { get; set; }

      

       // [DisplayName("Sent On")]
       // [JqGridColumnFormatter("$.paperSentOnFormatter")]
        [HiddenInput(DisplayValue = false)]
        public DateTime? DeptSubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DeptSubmittedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long PaperLaidId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int AssemblyId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string AssemblyName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int SessionId { get; set; }

       

        [HiddenInput(DisplayValue = false)]
        public string SessionName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int Status { get; set; }     

        [HiddenInput(DisplayValue = false)]
        public int EventId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string EventName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int MinistryId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinistryName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinistryNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinisterName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinisterNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DepartmentId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DeparmentName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string DeparmentNameLocal { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Description { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PaperTypeID { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string PaperTypeName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? DesireLayingDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? CommitteeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string CommitteeName { get; set; }

        [HiddenInput(DisplayValue = false)]
        [JqGridColumnFormatter("$.OtherpaperSentFormatter")]
        public int? CommitteeChairmanMemberId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int LOBRecordId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? AcknowledgmentBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? AcknowledgmentDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? LaidDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? ModifiedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? ModifiedWhen { get; set; }

        //[DisplayName("Number")]
        [HiddenInput(DisplayValue = false)]
        public long? DeptActivePaperId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public long? MinisterActivePaperId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int PAGE_SIZE { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int PageIndex { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ResultCount { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int loopStart { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int loopEnd { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int selectedPage { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int PageNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int RowsPerPage { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string FileName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? FileVersion { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? PaperCategoryTypeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string PaperCategoryTypeName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Number { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int? QuestionNumber { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MinisterSubmittedBy { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? MinisterSubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public DateTime? PaperSubmittedDate { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ProvisionUnderWhich { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string Remark { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int QuestionTypeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string ReplyPathFileLocation { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string MemberName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int DesiredDateId { get; set; }

        //nitin
        [HiddenInput(DisplayValue = false)]
        public ICollection<tPaperLaidTemp> SubmittedFileListByPaperLaidId { get; set; }
        public ICollection<MinisterPaperMovementModel> PaperMovementCollection { get; set; }
        public ICollection<MinisterPaperMovementModel> ListPaperMovementModel { get; set; }

        //uday
          
        [HiddenInput(DisplayValue = false)]
        public string HashKey { get; set; }

         [DisplayName("Sent On")]
        public string DepartmentSubmittedDate { get; set; }

    
    }
}
