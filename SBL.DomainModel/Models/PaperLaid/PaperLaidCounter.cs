﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace SBL.DomainModel.Models.PaperLaid
{
    [Serializable]
    public class PaperLaidCounter
    {
        #region Total Count Stared

        


        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalStaredReceived { get; set; }

        [HiddenInput(DisplayValue = false)] 
        [NotMapped]
        public int TotalStaredPendingForSubmission { get; set; }


        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public int TotalStaredPendingForSubmissionNew { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalStaredSubmitted { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalStarredUpcomingLOB { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalStaredLaidInHouse { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalStaredPendingToLay { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalStaredQuestions { get; set; }
        #endregion

        #region Total Count Unstarred
        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalUnstaredReceived { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalUnstaredPendingForSubmission { get; set; }

          [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalUnStaredPendingForSubmissionNew { get; set; }

        

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalUnstaredSubmitted { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalUnstarredUpcomingLOB { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalUnstaredLaidInHouse { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalUnstaredPendingToLay { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalUnstaredQuestions { get; set; }

        #endregion 

        #region BillsCount

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalDeptBillsCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalBillDraftCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalBillSentCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int BillUPLOBCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int BillLIHCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int BillPLIHCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public DateTime? DepartmentSubmitDate { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public long tpaperLaidTempId { get; set; }
       

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public string BillNumber { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public string PaperTypeName { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalBillsCount { get; set; }
        #endregion

        #region NoticeCount

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int NoticeTotalCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int NoticeInboxCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int NoticeReplayDraft { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int NoticeReplaySent { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int NoticeUPLOBCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int NoticeLIHCount { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int NoticePLIHCount { get; set; }

       

        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalNoticesCount { get; set; }
        #endregion

        #region Other PaperLaid
        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalOtherPapersCount { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalOtherPaperLaidUpComingLOB { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalOtherPaperLaidInTheHouse { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalOtherPaperPendingToLay { get; set; }
        [NotMapped]
        [HiddenInput(DisplayValue = false)] 
        public int TotalOtherpaperLaidUpdated { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalOtherPaperSend { get; set; }
        #endregion

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalBillPendingCount { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string DepartmentSubmittedDateNew { get; set; }


        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalStaredReceivedBeforeSend { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public int TotalUnStaredReceivedBeforeSend { get; set; }
       
    }
}
