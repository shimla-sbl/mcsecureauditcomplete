﻿namespace SBL.DomainModel.Models.PaperLaid
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tPaperLaidDepartmentAuditTrial")]
    [Serializable]
    public partial class tPaperLaidDepartmentAuditTrial
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PaperLaidID { get; set; }

        public int? AssemblyID { get; set; }

        public int? SessionID { get; set; }

        public DateTime? SessionDate { get; set; }

        public int? CategoryID { get; set; }

        public int? MinisterID { get; set; }

        [StringLength(20)]
        public string DepartmentID { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public int? PaperTypeID { get; set; }

        public int? ReferenceNumber { get; set; }

        public int? PreviousReferenceNumber { get; set; }

        public bool? IsAssured { get; set; }

        public DateTime? DepartmentSubmittedDate { get; set; }

        public DateTime? MinisterSubmittedDate { get; set; }

        public int? AcknowledgmentBy { get; set; }

        public int? AcknowledgmentDate { get; set; }

        public DateTime? LayingDate { get; set; }

        public bool? IsReSubmitted { get; set; }

        [StringLength(50)]
        public string ReturnType { get; set; }

        public DateTime? ReturnDate { get; set; }

        [StringLength(250)]
        public string DocumentPath { get; set; }

        public int? ModifiedBy { get; set; }

        public DateTime? ModifiedWhen { get; set; }
    }
}
