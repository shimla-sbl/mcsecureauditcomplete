﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.PaperLaid
{
    [Serializable]
    [Table("tMailStatus")]
    public class tMailStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public int AssemblyID { get; set; }

        public int SessionID { get; set; }

        public int? SessionDateId { get; set; }

        public DateTime? SessionDate { get; set; }

        public int MemberID { get; set; }

        // public int? QuestionNumber { get; set; }

        public string Type { get; set; }

        public string RecEmailId { get; set; }

        public DateTime? EmailSentDate { get; set; }
    }
}
