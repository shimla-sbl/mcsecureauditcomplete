﻿namespace SBL.DomainModel.Models.PaperLaid
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("mchangeDepartmentAuditTrail")]
    [Serializable]
    public partial class mchangeDepartmentAuditTrail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ChangedDepID { get; set; }

           [NotMapped]
        public int PaperLaidID { get; set; }

        public int? AssemblyID { get; set; }

        public int? SessionID { get; set; }

        public int? QuestionNumber { get; set; }

        [NotMapped]
        public int? Number { get; set; }

        public int QuestionType { get; set; }

        public int NoticeTypeId { get; set; }

           [NotMapped]
        public DateTime? SessionDate { get; set; }

       
        [StringLength(20)]
        public string DepartmentID { get; set; }

        [StringLength(20)]
        public string ChangedDepartmentID { get; set; }

        public string ChangedBy { get; set; }

        public string NoticeNumber { get; set; }

        public DateTime? ChangedDate { get; set; }

        public string DiaryNumber { get; set; }
        public string AadharId { get; set; }

        public string UserName { get; set; }
        public string TransferByAadharId { get; set; }
        public string TransferByName { get; set; }

        [NotMapped]
        public string ChangedDateString { get; set; }

        [NotMapped]
        public List<mchangeDepartmentAuditTrail> objQuestList1 { get; set; }
    }
}



