﻿namespace SBL.DomainModel.Models.PaperLaid
{
    #region Namespace Reffrences
    #endregion

    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;

    [Table("tPaperLaidTemp")]
    [Serializable]
    public partial class tPaperLaidTemp
    {
        public tPaperLaidTemp()
        {
            this.ListPaperLaidTemp = new List<tPaperLaidTemp>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long PaperLaidTempId { get; set; }

        public long? PaperLaidId { get; set; }

        public int? Version { get; set; }

        public int? SupVersion { get; set; }

        public int? DocFileVersion { get; set; }

        [StringLength(500)]
        public string FilePath { get; set; }

       

        [StringLength(500)]
        public string SignedFilePath { get; set; }

        [StringLength(500)]
        public string SignedSupFilePath { get; set; }

        [StringLength(200)]
        public string FileName { get; set; }

        [StringLength(200)]
        public string DocFileName { get; set; }

        public DateTime? DeptSubmittedDate { get; set; }

        [StringLength(50)]
        public string DeptSubmittedBy { get; set; }

        public DateTime? MinisterSubmittedDate { get; set; }

        public int? MinisterSubmittedBy { get; set; }
        public DateTime? CommtSubmittedDate { get; set; }

        [StringLength(50)]
        public string CommtSubmittedBy { get; set; }
        [NotMapped]
        public IList<tPaperLaidTemp> ListPaperLaidTemp { get; set; }

        [NotMapped]
        public string SessionId { get; set; }
        

        [NotMapped]
        public string AssemblyId { get; set; }

        [StringLength(200)]
        public string SupFileName { get; set; }

        [StringLength(200)]
        public string SupDocFileName { get; set; }

        [NotMapped]
        public string RandomVal { get; set; }

        [NotMapped]
        [HiddenInput(DisplayValue = false)]
        public string FileStructurePath { get; set; }

        [HiddenInput(DisplayValue = false)]
        [NotMapped]
        public virtual ICollection<tPaperLaidTemp> ListPaperLaidTempVer { get; set; }
        [NotMapped]
        public List<tPaperLaidTemp> objList { get; set; }

       
        public string evidhanReferenceNumber { get; set; }
    }
}
