﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Category
{

    /// <summary>
    /// Category
    /// </summary>

    [Serializable]
    [Table("Category")]
    public class Category
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryID { get; set; }

        [Required]
        public string CategoryName { get; set; }

        public string LocalCategoryName { get; set; }

        public string LocalCategoryDescription { get; set; }

        public string CategoryDescription { get; set; }

        public int CategoryType { get; set; }

        public int ParentID { get; set; }

        public int LFT { get; set; }

        public int RGT { get; set; }

        public int CategoryLevel { get; set; }

        public byte Status { get; set; }

        public Guid CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid ModifiedBy { get; set; }

        public DateTime ModifiedDate { get; set; }

        public DateTime StartPublish { get; set; }

        public DateTime StopPublish { get; set; }

        public int Hits { get; set; }

        public int AlbumCategoryId { get; set; }

        [NotMapped]
        public int GalleryId { get; set; }

        [NotMapped]
        public string GalleryThmImgUrl { get; set; }

    }
}
