﻿namespace SBL.DomainModel.Models.Assembly
{
    #region Namespace Reffrences
    using SBL.DomainModel.Models.Session;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    #endregion

    [Table("mAssembly")]
    [Serializable]
    public partial class mAssembly
    {





        [Key]

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AssemblyID { get; set; }

        public int AssemblyCode { get; set; }

        // [Required]
        //[StringLength(200)]
        public string AssemblyName { get; set; }

        //[StringLength(200)]
        public string AssemblyNameLocal { get; set; }

        //[Column(TypeName = "date")]
        public DateTime? AssemblyStartDate { get; set; }

        //[Column(TypeName = "date")]
        public DateTime? AssemblyEndDate { get; set; }

        // [StringLength(12)]
        public string Period { get; set; }

        //[Column(TypeName = "text")]
        public string Rem1 { get; set; }

        //[Column(TypeName = "text")]
        public string Rem2 { get; set; }

        //[Column(TypeName = "text")]
        public string Rem3 { get; set; }

        //[Column(TypeName = "text")]
        public string Rem4 { get; set; }

        //[Column(TypeName = "text")]
        public string Rem5 { get; set; }

        //[StringLength(10)]
        public string ModifiedBy { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public string CreatedBy { get; set; }

        public bool? Active { get; set; }

        public bool? IsDeleted { get; set; }

        public DateTime? CreatedDate { get; set; }
        public bool IsAssemblyData { get; set; }
        [NotMapped]
        public virtual List<mSession> ListmSession { get; set; }

        [NotMapped]
        public virtual List<mAssembly> ListmAssembly { get; set; }


        //For Paging

        [NotMapped]
        public int ClickCount { get; set; }

        [NotMapped]
        public List<mAssembly>AssemblyList { get; set; }

        [NotMapped]
        public int PAGE_SIZE { get; set; }

        [NotMapped]
        public int PageIndex { get; set; }

        [NotMapped]
        public int ResultCount { get; set; }

        [NotMapped]
        public int loopStart { get; set; }

        [NotMapped]
        public int loopEnd { get; set; }

        [NotMapped]
        public int selectedPage { get; set; }

        [NotMapped]
        public int PageNumber { get; set; }

        [NotMapped]
        public int RowsPerPage { get; set; }
       
    }
}
