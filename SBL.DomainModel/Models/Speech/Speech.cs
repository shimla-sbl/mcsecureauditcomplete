﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SBL.DomainModel.Models.Speech
{
    [Serializable]
    [Table("tSpeeches")]
    public class tSpeech
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SpeechID { get; set; }
        [Required]
        public string SpeechTitle { get; set; }

        public string LocalSpeechTitle { get; set; }

        public string SpeechDescription { get; set; }

        public string LocalSpeechDescription { get; set; }

        public byte SpeechType { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public byte Status { get; set; }

        public int CategoryID { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public Guid CreatedBy { get; set; }

        public Guid ModifiedBy { get; set; }

        public string FileName1 { get; set; }

        public string FilePath1 { get; set; }


        public DateTime StartPublish { get; set; }

        public DateTime StopPublish { get; set; }

        public int Ordering { get; set; }

        public int Hits { get; set; }

        [NotMapped]
        public string CategoryName { get; set; }


        public int AssemblyId { get; set; }
        public int SessionID { get; set; }
        public DateTime? Date { get; set; }
        public string Year { get; set; }
        public string PresentedBy { get; set; }
        public bool? IsFreeze { get; set; }
        public bool? IsOld { get; set; }
        public bool IsHindi { get; set; }
        public int? SessionDateId { get; set; }

        [NotMapped]
        public string AssemblyName { get; set; }

        [NotMapped]
        public string SessionName { get; set; }

        [NotMapped]
        public string SessionDate { get; set; }

    }
}
