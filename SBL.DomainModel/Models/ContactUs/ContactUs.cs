﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.ContactUs
{
    [Serializable]
    [Table("ContactUs")]
   public  class ContactUs
    {
       [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
       public int Id { get; set; }
       public string Name { get; set; }
       public string Email { get; set; }
       public string Mobile { get; set; }
       public string Message { get; set; }
       public DateTime? Date { get; set; }
       public string FeedBackType { get; set; }
        
    }
}
