﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SBL.DomainModel.Models.FooterPublicData
{
     [Table("FooterPublicData")]
    [Serializable]
   public class FooterPublicData
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        public string TitleID { get; set; }

        public string TitleName { get; set; }

        public string TitleDescription { get; set; }

        

    }
}
