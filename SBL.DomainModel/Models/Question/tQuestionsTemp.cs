﻿namespace SBL.DomainModel.Models.Question
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tQuestionsTemp")]
    [Serializable]
    public partial class tQuestionsTemp
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionTempId { get; set; }

        public long? PaperLaidId { get; set; }

        public int? QuestionId { get; set; }

        public int? Version { get; set; }

        [StringLength(500)]
        public string FilePath { get; set; }

        [StringLength(200)]
        public string FileName { get; set; }

        public DateTime? DeptSubmittedDate { get; set; }

        public DateTime? MinisterSubmittedDate { get; set; }
    }
}
