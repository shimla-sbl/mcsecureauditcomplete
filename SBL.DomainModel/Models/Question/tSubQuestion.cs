﻿namespace SBL.DomainModel.Models.Question
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;        
    
    [Serializable]
    [Table("tSubQuestions")]
    public partial class tSubQuestion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SubQuestionID { get; set; }

        public int AssemblyID { get; set; }

        public int SessionID { get; set; }

        public int QuestionID { get; set; }

        public string SubQuestionNum { get; set; }

        public string SubQuestionNumH { get; set; }

        public string SubQuestion { get; set; }

        public string SubQuestionH { get; set; }

        public int Status { get; set; }

        public string SubAnswer { get; set; }

        public string AnswerAttachLocation { get; set; }

        public string AnswerRemarks { get; set; }

        public DateTime DeptDateOfReply { get; set; }

        public string RejectRemarks { get; set; }

        public string ApproveRemarks { get; set; }

        public virtual tQuestion tQuestion { get; set; }
    }
}
