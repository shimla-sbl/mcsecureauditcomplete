﻿namespace SBL.DomainModel.Models.Question
{
	using SBL.DomainModel.Models.Constituency;
	using SBL.DomainModel.Models.Department;
	using SBL.DomainModel.Models.Ministery;
	using System;
	using System.Collections.Generic;
	using System.ComponentModel.DataAnnotations;
	using System.ComponentModel.DataAnnotations.Schema;

	[Serializable]
	[Table("tCancelQuestionAuditTrial")]
	public partial class tCancelQuestionAuditTrial
	{
		 
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long SNo { get; set; }

		public int QuestionID { get; set; }

		public int? QuestionNumber { get; set; }

		[StringLength(150)]
		public string DiaryNumber { get; set; }
		public DateTime? IsFixedDate { get; set; }
		public DateTime? IsFinalApprovedDate { get; set; }
		public DateTime? CancelDate { get; set; }


	}
}
