﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Question
{
    [Serializable]
    [Table("mQuestionRules")]
    public class mQuestionRules
    {
        public mQuestionRules()
        {

        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionRuleId { get; set; }
        public string Rules { get; set; }
        public string RulesH { get; set; }
        public bool? Remarks { get; set; }
        public bool? IsTextUse { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public bool? IsDeleted { get; set; }

    }
}
