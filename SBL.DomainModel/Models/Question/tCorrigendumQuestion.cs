﻿namespace SBL.DomainModel.Models.Question
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("tCorrigendumQuestions")]
    [Serializable]
    public partial class tCorrigendumQuestion
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CorrigendumQuestionId { get; set; }

        public int QuestionId { get; set; }

        public string Type { get; set; }

        public string AttachedFilePath { get; set; }

        public string Remarks { get; set; }
    }
}
