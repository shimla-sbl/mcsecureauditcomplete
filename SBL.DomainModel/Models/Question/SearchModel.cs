﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Session;
using SBL.DomainModel.Models.Event;

namespace SBL.DomainModel.Models.Question
{
    [Serializable]
    public class SearchModel
    {
        public SearchModel()
        {
            this.objList = new List<SearchModel>();
        }
        public int ID { get; set; }
        public int AssemblyID { get; set; }
        public int FromAssemblyID { get; set; }
        public int ToAssemblyID { get; set; }
        public int AssemblyCode { get; set; }
        public string AssemblyName { get; set; }
        public int SessionId { get; set; }
        public int SessionCode { get; set; }
        public string SessionName { get; set; }
        public int? MemberID { get; set; }
        public int? MinsitryMinistersID { get; set; }
        public string MemberName { get; set; }
        public string MinisterName { get; set; }
        public string MinistryName { get; set; }
        public string DepartmentID { get; set; }
        public int? Department_Id { get; set; }
        public string DepartmentName { get; set; }
        public int? QuestionType { get; set; }
        public string QuestionNo { get; set; }
        public DateTime? Date { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string Event { get; set; }
        public string Subject { get; set; }
        public string Search { get; set; }
        public string Content { get; set; }
        public string ListId { get; set; }
        public int ValueId { get; set; }
        public string PageNos { get; set; }
        public string FilePath { get; set; }
        public string EventId { get; set; }
        public string FinalVersion { get; set; }
        public int? mEventId { get; set; }
        public string mEventName { get; set; }
        public string RefPageNo { get; set; }
        public bool IsFreeze { get; set; }

        public int ResultCount { get; set; }

        public List<mEvent> EventList { get; set; }
        public List<SearchModel> objList { get; set; }
        public List<SearchModel> DataList { get; set; }
        public List<mAssembly> AssemblyList { get; set; }
        public List<mSession> SessionList { get; set; }
        public List<tQuestionType> QuestionTypeList { get; set; }
        public List<mMinsitryMinister> MinisterList { get; set; }
        public List<mMember> MemberList { get; set; }
        public List<mDepartment> DepartmentList { get; set; }
        public List<string> SubjectList { get; set; }
        public List<SearchModel> mMinistryDepartments { get; set; }
    }
}
