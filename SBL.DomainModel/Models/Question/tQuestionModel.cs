﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SBL.DomainModel.Models.Session;
using System;
using System.Web;
using System.Web.Mvc;

using SBL.DomainModel.Models.Member;
using SBL.DomainModel.Models.Department;
using SBL.DomainModel.Models.Assembly;
using SBL.DomainModel.Models.Question;
using SBL.DomainModel.Models.Ministery;
using SBL.DomainModel.Models.Constituency;
using SBL.DomainModel.Models.Notice;
using Lib.Web.Mvc.JQuery.JqGrid.DataAnnotations;

namespace SBL.DomainModel.Models.Question
{
    [Serializable]
    public class tQuestionModel
    {
        public tQuestionModel()
        {
            this.mMinisterModel = new List<mMember>();
            this.mDepartmentModel = new List<mDepartment>();
            this.mMemberModel = new List<mMember>();
            this.mAssembly = new List<mAssembly>();
            this.mSession = new List<mSession>();
            this.mDepartment = new List<mDepartment>();
            
        }
        public string gggg { get; set; }
        public string DisplayDate { get; set; }
        public List<string> MemberIds { get; set; }
        public bool IsMerge { get; set; } 
        public int QuestionID { get; set; }
        public string QuestionType { get; set; }
        [Required(ErrorMessage = "Please Select Question Type")]
        public int QuestionTypeId { get; set; }
        public int PreviousQuestionType { get; set; }
        [Required(ErrorMessage = "Please Select Minister")]
        public int MinisterId { get; set; }
        [Required(ErrorMessage = "Please Select Assembly")]
        public int AssemblyId { get; set; }
        [Required(ErrorMessage = "Please Select Session")]
        public int SessionId { get; set; }
        [Required(ErrorMessage = "Please select Member")]
        public int MemberId { get; set; }
        public string MinsterName { get; set; }
        [Required(ErrorMessage = "Please Enter Subject")]
        [StringLength(250, ErrorMessage = "Only 250 characters are allowed")]
        public string Subject { get; set; }
        public string MinisterName { get; set; }
        [Column(TypeName = "ntext")]
        [StringLength(1000, ErrorMessage = "Only 1000 characters are allowed")]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        [Required(ErrorMessage = "Please Enter Main Question")]
        public string MainQuestion { get; set; }
        [AllowHtml()]
        public string MainQuestionCombination { get; set; }
        public bool HasSubQuestions { get; set; }
        public int SubQuestionID { get; set; }
        public string SubQuestionNum { get; set; }
        public string SubQuestion { get; set; }
        public string MQStatus { get; set; }
        public string SQStatus { get; set; }
        public DateTime QuestionDate { get; set; }
        public DateTime? QuestionLayDate { get; set; }
        public string NoticeId { get; set; }
        public string NoticeDate { get; set; }
        public string NoticeDescriptions { get; set; }
        [Required(ErrorMessage = "Please Enter Diary Number")]
        public string DiaryNumber { get; set; }
        public string FileNumber { get; set; }
        public int? FixedNumber { get; set; }
        [Required(ErrorMessage = "Please Enter Submitted By")]
        public string SubmittedBy { get; set; }
        public string ContituencyNo { get; set; }
        public string ContituencyName { get; set; }
        public string DepartmentN { get; set; }
        public string StatusN { get; set; }
        public string AssemblyN { get; set; }
        public string SessionN { get; set; }
        public string MemberN { get; set; }
        public string MinisterN { get; set; }
        public Nullable<DateTime> Date { get; set; }
        public Nullable<int> Priority { get; set; }
        public string Message { get; set; }
        public int StatusId { get; set; }
        public int QtypeId { get; set; }
        [Required(ErrorMessage = "Please Select Department")]
        public string DepartmentId { get; set; }

        public string DepartmentIdOnline { get; set; }
        public int MinisterIdOnline { get; set; }

        public int QuestionRuleId { get; set; }
        public string Rules { get; set; }
        public string RulesH { get; set; }
        [StringLength(250, ErrorMessage = "Only 250 characters are allowed")]
        public string Remarks { get; set; }
        public Nullable<bool> IsTextUse { get; set; }
        //[Required(ErrorMessage = "Please Write the answer")]
        public string AnsReply { get; set; }
        public string AddFiles { get; set; }
        public string remark { get; set; }
        public string AssemblyName { get; set; }
        public string SessionName { get; set; }
        public string LayDate { get; set; }
        public DateTime? QuestionLayingDate { get; set; }

        public bool? IsSubmitted { get; set; }
        public bool? IsSelected { get; set; }
        public bool? IsInitialApproved { get; set; }
        public DateTime? IsInitialApprovedDate { get; set; }
        public bool? IsReply { get; set; }
        public bool? IsFixed { get; set; }
        public DateTime? IsFixedDate { get; set; }
        public bool? IsClubbed { get; set; }
        public string IsClubbedRemarks { get; set; }
        public bool? IsFinalApproved { get; set; }
        public bool? IsRejected { get; set; }
        public bool? IsTypeChange { get; set; }
        public bool? IsLapse { get; set; }
        public bool? IsQuestionFreeze { get; set; }
        public DateTime? IsQuestionFreezeDate { get; set; }
        public bool? IsNoticeGenerated { get; set; }
        public DateTime? IsNoticeGeneratedDate { get; set; }

        public string StringDate { get; set; }
        [Column(TypeName = "ntext")]
        [MaxLength]
        [UIHint("tinymce_jquery_full"), AllowHtml]
        [Required(ErrorMessage = "Please Enter Main Question")]
        public string MainAnswer { get; set; }
        public string FileLocation { get; set; }
        public string StringTime { get; set; }
        [Required(ErrorMessage = "Please Enter Recieved Time")]
        public Nullable<TimeSpan> TOReceipt { get; set; }
        public DateTime? NoticeRecievedDate { get; set; }
        public TimeSpan? displayTime { get; set; }
        public Nullable<DateTime> DateforReply { get; set; }
        public string StringReplyDate { get; set; }
        public int? QuestionPriority { get; set; }
        [Required(ErrorMessage = "Please Enter Recieved Date")]
        public string NRDate { get; set; }
        public string BtnCaption { get; set; }
        public string View { get; set; }


       
        public List<tQuestionModel> objQuestList1 { get; set; }
        //public List<mQuestionRules> objRuleList { get; set; }
        public List<tQuestionModel> objQuestList { get; set; }
        public List<tQuestionModel> objSubQuestList { get; set; }
        public List<tQuestionModel> objQuestTemplateList { get; set; }
        public List<tQuestionModel> objQuestTemplateRespList { get; set; }
        public virtual List<SelectListItem> items { get; set; }
        public virtual List<SelectListItem> items2 { get; set; }
        //public virtual ICollection<tVStTemplate> tTemplate { get; set; }
        public virtual ICollection<tQuestionModel> mQuesList { get; set; }
        
        public string listAction { get; set; }
        public string TableName { get; set; }
        public bool? IsAccepted { get; set; }
        public bool? IsFreezed { get; set; }
        public DateTime? IsAcceptedDate { get; set; }
        public string AcceptDate { get; set; }
       
       // public List<mDepartment> DepartmentList { get; set; }
        public List<tMemberNotice> DepartmentList { get; set; }
        public List<mMinisteryMinisterModel> ministerList { get; set; }
        public List<mSessionDate> SessionDateList { get; set; }
        public virtual List<mMember> mMinisterModel { get; set; }
        public virtual ICollection<mDepartment> mDepartmentModel { get; set; }
        public virtual ICollection<mMember> mMemberModel { get; set; }
        public virtual ICollection<mAssembly> mAssembly { get; set; }
        //public virtual ICollection<Ministery> mMinister { get; set; }
        public virtual ICollection<mDepartment> mDepartment { get; set; }
      
        public virtual ICollection<mSession> mSession { get; set; }
        public virtual ICollection<tQuestionType> tQuestionType { get; set; }
        //public virtual ICollection<tVSNotificationGroups> tNotificationGroups { get; set; }

        [Required(ErrorMessage = "Please Enter Constituency No")]
        public string ConstituencyNumber { get; set; }
        public string ConstituencyNo { get; set; }
        [Required(ErrorMessage = "Please Enter Constituency Name")]
        public string ConstituencyName { get; set; }

        [MaxLength(500)]
        public string ChangeTypeRemarks { get; set; }
        [MaxLength(500)]
        public string RejectRemarks { get; set; }
        [MaxLength(500)]
        public string AnswerRemarks { get; set; }
        [MaxLength(500)]
        public string QuestionRemarks { get; set; }
        [MaxLength(500)]
        public string PosponedRemarks { get; set; }

        public int SelectedMinisterId { get; set; }

        public bool? IsMainQuestionEntry { get; set; }
        public DateTime? IsMainQuestionEntryDate { get; set; }
        public string ReturnPage { get; set; }

        public string ReferencedNumber { get; set; }
        public bool? DeActivate { get; set; }
        public string DeActivateFlag { get; set; }

        public bool IsNoticeSend { get; set; }
        public DateTime? IsNoticeSendDate { get; set; }

        public DateTime? NoticeReceivedDate { get; set; }
        public DateTime? NoticeReceivedTime { get; set; }


        public bool IsCorrigendum { get; set; }

        public bool IsPostpone { get; set; }

        public DateTime? IsCorrigendumDate { get; set; }

        public bool ShowPospond { get; set; }

        public bool? IsReplyFreeze { get; set; }
        public DateTime? IsReplyFreezeDate { get; set; }

        public string ModuleType { get; set; }

        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string TemplateDesc { get; set; }
        public string TemplatePath { get; set; }
        public string TemplateContent { get; set; }
        public string PdfTemplatePath { get; set; }

        //templates

        public DateTime? NoticeSentDate { get; set; }
        public DateTime? MaxReplyDate { get; set; }
        public string MinisteryName { get; set; }
        public string SecratoryName { get; set; }

        public string ReceipentEmail { get; set; }
        public string ReceipentPhone { get; set; }
        public string CategoryName { get; set; }
        public string ModuleName { get; set; }
        public int ReciepentCategoryId { get; set; }
        public string DepartmentAbbr { get; set; }
        public string UserName { get; set; }
        public string ReceipentIds { get; set; }
        public string InitialDraftNoticePath { get; set; }
        public string NoticeType { get; set; }

        [UIHint("tinymce_jquery_full"), AllowHtml]
        public string TemplateBottom { get; set; }

        public bool? IsFinalReject { get; set; }
        public DateTime? IsRejectedDate { get; set; }

        public int RulesId { get; set; }
        public List<tQuestionModel> objRuleListRegistered { get; set; }
        public bool? IsRejectSNotice { get; set; }
        public DateTime? IsRejectNoticeSDate { get; set; }
        public int TemplateType { get; set; }
        public string Email { get; set; }
        public string RejectNoticePath { get; set; }

        public List<tQuestionModel> objMailId { get; set; }

       
        public int SelectionTypeId { get; set; }
        public string SelectionTypeName { get; set; }
        public int QuesCount { get; set; }
        public string SQIds { get; set; }
        public int MaxQuesMem { get; set; }
        public int MaxQuesMins { get; set; }
        public string AllQuesId { get; set; }
        public int SessionCalendarId { get; set; }
  //[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string SessionDate { get; set; }
        public string Lang { get; set; }
        public string MemSLang { get; set; }
        public bool? IsHindi { get; set; }

        //Paging Properties
        public int PAGE_SIZE { get; set; }
        public int PageIndex { get; set; }
        public int ResultCount { get; set; }
        public int loopStart { get; set; }
        public int loopEnd { get; set; }
        public int selectedPage { get; set; }
        public int PageNumber { get; set; }
        public int RowsPerPage { get; set; }

        public string AssemblyN_Hindi { get; set; }
        public string SessionN_Hindi { get; set; }
        public string QuesTypeN_Hindi { get; set; }
        public string MemberN_Hindi { get; set; }
        public string DepartmentN_Hindi { get; set; }
        public string MinisterN_Hindi { get; set; }

        public string UserId { get; set; }

        public bool? IsNoticeSigned { get; set; }
        public DateTime? IsNoticeSignedDate { get; set; }

        public bool? IsQuestionDetailFreez { get; set; }
        public DateTime? IsQuestionDetailFreezDate { get; set; }

        public bool? IsRejectNoticeGenerated { get; set; }
        public DateTime? IsRejectNoticeGeneratedDate { get; set; }

        public bool? IsRejectNoticeSend { get; set; }
        public DateTime? IsRejectNoticeSendDate { get; set; }

        public bool? IsRejectNoticeSigned { get; set; }
        public DateTime? IsRejectNoticeSignedDate { get; set; }

        public int GroupId { get; set; }
        public string FunctionGroup { get; set; }

        public string MQEDate { get; set; }
        public string NSDate { get; set; }
        public string FixDate { get; set; }
        public string ReplyDate { get; set; }
        public DateTime? IsClubbedDate { get; set; }
        public string ClubDate { get; set; }
        public DateTime? IsTypeChangeDate { get; set; }
        public DateTime? IsReplyDate { get; set; }
        public string TCDate { get; set; }

        //Create Question Entry
        public string QuestionList { get; set; }
        public string QuestionCRNo { get; set; }
        public int? QuestionNumber { get; set; }
        public string Ddltextarea { get; set; }
        public string test1 { get; set; }
        public string claim { get; set; }
        public List<string> takeall { get; set; }
        public string QuestionNo { get; set; }
        public DateTime? QuestionFixDate { get; set; }
       public int bb { get; set; }
        public List<string> takeSr { get; set; }
        public string Tab { get; set; }

       
        public long? PaperLaidId { get; set; }

        //Sammer
        public string AnswerAttachLocation { get; set; }


        [HiddenInput(DisplayValue = false)]
        public string ReferenceMemberCode { get; set; }

        [HiddenInput(DisplayValue = false)]
        public string RMemberName { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsClubed { get; set; }

         [HiddenInput(DisplayValue = false)]
        public string MergeDiaryNo { get; set; }


         [HiddenInput(DisplayValue = false)]
         public bool ? IsBracket { get; set; }
         public string CurrentDateFrom { get; set; }
         public string CurrentDateTo { get; set; }
       
         [HiddenInput(DisplayValue = false)]
        // public virtual ICollection<tQuestionModel> DiaryNumList { get; set; }

         public List<tQuestionModel> DiaryNumList { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string CDiaryNumber { get;set;}

         [HiddenInput(DisplayValue = false)]
         public string BracketedWithDNo { get; set; }


         [HiddenInput(DisplayValue = false)]
         public string SubmittedDateString { get; set; }

         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         [JqGridColumnFormatter("$.paperSentOnFormatter")]
         public DateTime? SubmittedDate { get; set; }


         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public DateTime? ChangedDate { get; set; }

         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public string ChangedDateString { get; set; }

         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public string ChangedDepartmentID { get; set; }

         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public string ChangedDepName { get; set; }

         //public string AadharId { get; set; }

         //public string UserName { get; set; }
         [NotMapped]
         [HiddenInput(DisplayValue = false)]
         public DateTime? IsAcknowledgmentDate { get; set; }

         [HiddenInput(DisplayValue = false)]
         public string AcknowledgmentDateString { get; set; }

         public List<SBL.DomainModel.Models.PaperLaid.mchangeDepartmentAuditTrail> TransferDetailsList { get; set; }
         public TimeSpan? NoticeRecievedTime { get; set; }
         public DateTime? IsFinalApprovedDate { get; set; }
         public DateTime? IsFixedTodayDate { get; set; }
         public int? SessionDateId { get; set; }
    }
}
