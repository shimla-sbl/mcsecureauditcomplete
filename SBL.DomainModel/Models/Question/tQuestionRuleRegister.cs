﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SBL.DomainModel.Models.Question
{
    [Serializable]
    [Table("tQuestionRuleRegister")]
    public class tQuestionRuleRegister
    {
        public tQuestionRuleRegister()
        {
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RuleRegisterId { get; set; }
        public int RulesId { get; set; }
        public int QuestionId { get; set; }
        public bool? Remarks { get; set; }
    }
}
