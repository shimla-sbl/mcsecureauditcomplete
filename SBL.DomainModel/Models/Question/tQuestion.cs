﻿namespace SBL.DomainModel.Models.Question
{
    using SBL.DomainModel.ComplexModel;
    using SBL.DomainModel.Models.Constituency;
    using SBL.DomainModel.Models.Department;
    using SBL.DomainModel.Models.Ministery;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Web.Mvc;

	[Serializable]
	[Table("tQuestions")]
	public partial class tQuestion
	{
		public tQuestion()
		{
			tSubQuestions = new HashSet<tSubQuestion>();
		}

		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int QuestionID { get; set; }

		public long? PaperLaidId { get; set; }

		public int? QuestionNumber { get; set; }

		public int AssemblyID { get; set; }

		public int SessionID { get; set; }

		public int QuestionType { get; set; }

		public string SubmittedBy { get; set; }

		public int MinisterID { get; set; }

		public int MemberCode { get; set; }
		public string ConstituencyNo { get; set; }

        public bool isPrintTaken { get; set; }

		[StringLength(100)]
		public string ConstituencyName { get; set; }

		public int MemberID { get; set; }

		[StringLength(7)]
		public string DepartmentID { get; set; }

		public int MQStatus { get; set; }

		[StringLength(1000)]
		public string Subject { get; set; }


		public string MainQuestion { get; set; }


		public string MainQuestionH { get; set; }

		public bool HasSubQuestions { get; set; }

		public DateTime? QuestionDate { get; set; }

		public DateTime? AnswerReqDate { get; set; }

		public int? Priority { get; set; }

		public int? NoticeID { get; set; }

		public DateTime? NoticeIssueDate { get; set; }

		[StringLength(500)]
		public string MainAnswer { get; set; }

		[StringLength(250)]
		public string AnswerAttachLocation { get; set; }

		public DateTime? DeptDateOfReply { get; set; }

		public int? FixedNumber { get; set; }

		[StringLength(150)]
		public string FileNumber { get; set; }

		[StringLength(150)]
		public string DiaryNumber { get; set; }

		public int OldQuestionType { get; set; }

		public DateTime? QuestionLayingDate { get; set; }

		public DateTime? NoticeRecievedDate { get; set; }

		public TimeSpan? NoticeRecievedTime { get; set; }

		public DateTime? NoticeAcceptedDate { get; set; }

		public string NoticeDescription { get; set; }

		public string ReferencedNumber { get; set; }

		[StringLength(500)]
		public string ChangeTypeRemarks { get; set; }

		[StringLength(500)]
		public string RejectRemarks { get; set; }

		[StringLength(500)]
		public string AnswerRemarks { get; set; }

		[StringLength(500)]
		public string QuestionRemarks { get; set; }

		[StringLength(500)]
		public string PosponedRemarks { get; set; }

		public bool? IsSubmitted { get; set; }

		public bool? IsSelected { get; set; }

		public bool? IsInitialApproved { get; set; }

		public DateTime? IsInitialApprovedDate { get; set; }

		public bool? IsReply { get; set; }

		public DateTime? IsReplyDate { get; set; }

		public bool? IsFixed { get; set; }

		public DateTime? IsFixedDate { get; set; }

		public DateTime? IsFixedTodayDate { get; set; }
		public bool? IsClubbed { get; set; }

		public DateTime? IsClubbedDate { get; set; }

		public string IsClubbedRemarks { get; set; }

		public bool? IsFinalApproved { get; set; }

		public DateTime? IsFinalApprovedDate { get; set; }

		public bool? IsRejected { get; set; }

		public DateTime? IsRejectedDate { get; set; }

		public bool? IsTypeChange { get; set; }

		public DateTime? IsTypeChangeDate { get; set; }

		public bool? IsLapse { get; set; }

		public DateTime? IsLapseDate { get; set; }

		public bool? IsMainQuestionEntry { get; set; }

		public DateTime? IsMainQuestionEntryDate { get; set; }

		public bool? IsOnlineQuestionAccepted { get; set; }

		public bool? IsNoticeFreeze { get; set; }

		public DateTime? IsNoticeFreezeDate { get; set; }

		public bool? IsNoticeGenerated { get; set; }

		public DateTime? IsNoticeGeneratedDate { get; set; }

		public bool? IsReplyFreeze { get; set; }

		public DateTime? IsReplyFreezeDate { get; set; }

		public bool? IsQuestionFreeze { get; set; }

		public DateTime? IsQuestionFreezeDate { get; set; }

		public bool? IsProofReading { get; set; }

		public DateTime? IsProofReadingDate { get; set; }

		public string ProofReaderUserId { get; set; }

		public bool? DeActivate { get; set; }

		public string DeActivateFlag { get; set; }

		public bool IsNoticeSend { get; set; }

		public DateTime? IsNoticeSendDate { get; set; }

		public bool IsCorrigendum { get; set; }

		public DateTime? IsCorrigendumDate { get; set; }

		public bool IsPostpone { get; set; }

		public DateTime? IsPostponeDate { get; set; }

		public bool? IsFinalReject { get; set; }

		public bool? IsRejectNoticeGenerated { get; set; }

		public DateTime? IsRejectNoticeGeneratedDate { get; set; }

		public bool? IsRejectNoticeSend { get; set; }

		public DateTime? IsRejectNoticeSendDate { get; set; }

		public bool? IsRejectNoticeSigned { get; set; }

		public DateTime? IsRejectNoticeSignedDate { get; set; }

		public bool? IsFinalSelection { get; set; }

		public DateTime? IsFinalSelectionDate { get; set; }

		public int? SelectionTypeId { get; set; }

		public bool? IsHindi { get; set; }

		public bool? IsNoticeSigned { get; set; }

		public DateTime? IsNoticeSignedDate { get; set; }

		public bool? IsQuestionDetailFreez { get; set; }

		public DateTime? IsQuestionDetailFreezDate { get; set; }
		public string ReferenceMemberCode { get; set; }
		public bool IsMerge { get; set; }

		public int EventId { get; set; }
		public string OriDiaryFilePath { get; set; }
		public string OriDiaryFileName { get; set; }
		public int? QuestionStatus { get; set; }
		public int? MinistryId { get; set; }
		public DateTime? SubmittedDate { get; set; }
		public int? SessionDateId { get; set; }
		public string SubInCatchWord { get; set; }
		public bool IsTranslation { get; set; }
		public bool IsOnlineSubmitted { get; set; }
		public DateTime? IsOnlineSubmittedDate { get; set; }


		[NotMapped]
		public virtual ICollection<tSubQuestion> tSubQuestions { get; set; }
		[NotMapped]
		public virtual ICollection<mDepartment> mDepartment { get; set; }
		[NotMapped]
		public virtual List<mConstituency> mConstituency { get; set; }
		[NotMapped]
		public virtual List<mMinsitryMinister> mMinsitryMinister { get; set; }
		[NotMapped]
		public string Message { get; set; }
		[NotMapped]
		public bool? IsPending { get; set; }

		//Sameer
		public bool? IsContentFreeze { get; set; }
		public string TypistUserId { get; set; }
		[NotMapped]
		public string QuestionValue { get; set; }

		public DateTime? IsAssignTypsitDate { get; set; }
		public bool? IsTypistFreeze { get; set; }
		public bool? IsBracket { get; set; }
		public string BracketedWithDNo { get; set; }
		public string BracketedWithQNo { get; set; }
		public string MergeDiaryNo { get; set; }
        // Lakshay
        [NotMapped]
        public string eMailSentOn { get; set; }
        [NotMapped]
        public string Designation { get; set; }
        [NotMapped]
        public IEnumerable<QuestionModelCustom> QuestionListForeMail { get; set; }
		//Sunil
		[NotMapped]
		public virtual ICollection<tQuestionModel> tQuestionModel { get; set; }
		public int? QuestionOrderNum { get; set; }

		[NotMapped]
		public int ResultCount { get; set; }
		[NotMapped]
		public List<tQuestion> QuestionListForTrasData { get; set; }

		[NotMapped]
		public string StringTime { get; set; }

		[HiddenInput(DisplayValue = false)]
		public int? OldMinistryId { get; set; }


		[HiddenInput(DisplayValue = false)]
		public string OldDepartmentId { get; set; }

		[HiddenInput(DisplayValue = false)]
		public bool? IsDepartmentChangedApprove { get; set; }



		[NotMapped]
		[StringLength(200)]
		public string FileName { get; set; }

		[NotMapped]
		[StringLength(200)]
		public string FilePath { get; set; }

		public bool? IsQuestionInPart { get; set; }


		[NotMapped]
		[HiddenInput(DisplayValue = false)]
		public string RMemberName { get; set; }

		[NotMapped]
		[HiddenInput(DisplayValue = false)]
		public string MemberN { get; set; }
		public int? QuestionSequence { get; set; }

		public DateTime? IsAcknowledgmentDate { get; set; }
		public bool? IsLocktranslator { get; set; }


		//Added by Durgesh
		public bool? IsWithdrawbyMember { get; set; }

		public bool? IsExcessQuestion { get; set; }

		public bool? IsTimeBarred { get; set; }

		public string UserName { get; set; }

		public string AadharId { get; set; }

		public DateTime? WithdrawbyMemberDateTime { get; set; }

		[NotMapped]
		[HiddenInput(DisplayValue = false)]
		public string userId { get; set; }
		[NotMapped]
		[HiddenInput(DisplayValue = false)]
		public Guid UserGuId { get; set; }

		[NotMapped]
		[HiddenInput(DisplayValue = false)]
		public string RoleName { get; set; }
		[NotMapped]
		public string AssemblyName { get; set; }
		[NotMapped]
		public string SessionName { get; set; }
		[NotMapped]
		public string DeptName { get; set; }
		[NotMapped]
		public string ProofReaderUserName { get; set; }
		[NotMapped]
		public string TypistUserName { get; set; }
		[NotMapped]
		public string MinisterName { get; set; }
		[NotMapped]
		public string Notice { get; set; }
		[NotMapped]
		public string eVidhanRefNo { get; set; }
		[NotMapped]
		public string PaperlaidDocFilePath { get; set; }
		[NotMapped]
		public string PaperlaidDocFileName { get; set; }
		[NotMapped]
		public string PaperlaidPdfFilePath { get; set; }
		[NotMapped]
		public string PaperlaidPdfFileName { get; set; }
		[NotMapped]
		public long PaperlaidTempID { get; set; }
		[NotMapped]
		public DateTime PaperlaidSubmittedDate { get; set; }
        public string StatusPQ { get; set; }
        public string CurentStatusPQ { get; set; }
        public bool? IsMobile { get; set; }
        [NotMapped]
        public string DataType { get; set; }
        [NotMapped]
        public int CountInbox { get; set; }
        [NotMapped]
        public int CountDraft { get; set; }

        public int? DesiredQuestionDateId { get; set; }
		[StringLength(150)]
		public string ManualQuestionNumber { get; set; }
	}
}
