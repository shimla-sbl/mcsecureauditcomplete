﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SBL.DomainModel.Models.Question
{
    [Serializable]
   public partial class tQuestionType
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int QuestionTypeId { get; set; }

        public string QuestionTypeName { get; set; }
        public string QuestionTypeName_Hindi { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedWhen { get; set; }

        [NotMapped]
        public int? QuestionID { get; set; }
        [NotMapped]
        public int? QuestionNumber { get; set; }
    }
}
